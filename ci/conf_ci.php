<?php

$config = [];

if (getenv('TEST_TOKEN') !== false) {  // Using paratest
    if (!is_dir(TMP . 'testdb')) {
        mkdir(TMP . 'testdb', 0777, true);
    }
    $config['Datasources'] = [
        'test' => [
            'database' => $db = TMP . 'testdb/' . getenv('TEST_TOKEN') . '.sqlite',
        ]
    ];
    if (!defined('FIRST_UNLINK_DB')) {
        define('FIRST_UNLINK_DB', true);
        if (file_exists($db)) {
            unlink($db);
        }
    }
}

return $config;
