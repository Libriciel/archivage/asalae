#!/bin/bash

if [ "$SKIP_INIT" == "true" ]
then
  echo "initialization skipped"
  exit 0
fi

failed_with_error () {
  echo "$1" >&2
  exit 1
}

# appli env config
configure() {
  sudo -u "$APACHE_RUN_USER" /var/www/asalae-volume/bin/cake configuration set "$1" "$2" >/dev/null
}

# initialisation d'un nouveau volume
cp /data-entrypoint/asalae.conf /etc/apache2/sites-available/asalae.conf
sed -i'' -E \
  -e "s/\%\{HTTP_HOST\}/${HTTP_HOST}/" \
  /etc/apache2/sites-available/asalae.conf

APACHE_ERROR_LOG=/dev/stderr
APACHE_TRANSFER_LOG=/dev/null

if [ "$APACHE_LOG_FILES" == "true" ]
then
  APACHE_ERROR_LOG=/apache-logs/asalae_error.log
  APACHE_TRANSFER_LOG=/apache-logs/asalae_access.log
fi

sed -i'' -E \
  -e "s#\%\{APACHE_ERROR_LOG\}#${APACHE_ERROR_LOG}#" \
  /etc/apache2/sites-available/asalae.conf
sed -i'' -E \
  -e "s#\%\{APACHE_TRANSFER_LOG\}#${APACHE_TRANSFER_LOG}#" \
  /etc/apache2/sites-available/asalae.conf

sed -i'' -E \
  -e "s/\%\{APACHE_ADD_TO_VHOST\}/${APACHE_ADD_TO_VHOST}/" \
  /etc/apache2/sites-available/asalae.conf

if [ ! -f /etc/apache2/ssl/server.crt ]; then
    cp /data-entrypoint/server.crt /etc/apache2/ssl/server.crt
fi
if [ ! -f /etc/apache2/ssl/server.key ]; then
    cp /data-entrypoint/server.key /etc/apache2/ssl/server.key
fi

# Copie des fichiers de l'image vers le volume
cd /var/www/asalae/ || exit 1
xargs -I % sh -c 'rm -rf "/var/www/asalae-volume/%" && cp -aR "%" "/var/www/asalae-volume/" 2>/dev/null' < files_to_override_on_init.txt
cd /var/www/asalae-volume/ || exit 1
mkdir -p /var/www/asalae-volume/tmp /var/www/asalae-volume/logs
chown "$APACHE_RUN_USER" \
  /var/www/asalae-volume/tmp \
  /var/www/asalae-volume/logs \
  /apache-logs \
  /var/www/asalae/webroot/libersign \
  /var/www/asalae/webroot/org-entity-data

INI_FILE=/tmp/install.ini
CONFIG_DIR=/data/config
CONFIG_FILE=$CONFIG_DIR/app_local.json
RATCHET_HOST=${RATCHET_HOST:-asalae_ratchet}
POSTGRES_DBHOST=${POSTGRES_DBHOST:-asalae_db}
mkdir -p $CONFIG_DIR
mkdir -p /etc/apache2/ssl

# chown des volumes
chown "$APACHE_RUN_USER" -Rf \
  /etc/apache2/sites-available \
  /etc/apache2/ssl \
  /var/www/asalae/tmp \
  /var/www/asalae/logs \
  /var/www/asalae/webroot/org-entity-data

URL=${HTTP_HOST:-localhost}${HTTP_PORT:-}${HTTP_BASEURL:-}

if [ ! -f "$CONFIG_FILE" ]
then

  echo "
[config]
config_file = $CONFIG_FILE
data_dir = /data
admin_file = $CONFIG_DIR/administrators.json
debug = $DEBUG

[proxy]
host = $PROXY_HOST
port = $PROXY_PORT
username = $PROXY_USER
password = \"$PROXY_PASSWORD\"

[app]
locale = fr_FR
timezone = Europe/Paris
url = $HTTP_PROTOCOL://$URL

[ratchet]
url_ws = wss://$URL/wss

[database]
host = $POSTGRES_DBHOST
port = $POSTGRES_DBPORT
username = $POSTGRES_USER
password = \"$POSTGRES_PASSWORD\"
database = $POSTGRES_DB

[security]
type = random
value =

[email]
from = $MAIL_FROM
method = smtp
host = $POSTFIX_RELAYHOST
port = $POSTFIX_PORT
username = $POSTFIX_RELAYHOST_USERNAME
password = \"$POSTFIX_RELAYHOST_PASSWORD\"

[timestamp]
name = Openssl local
countryName = FR
stateOrProvinceName = France
localityName = Paris
organizationName = Ma collectivité
organizationalUnitName = Mon service
commonName = Mon nom
emailAddress = Mon email
;
; fichier extfile
;
extfile = \"
extensions = extend
[extend]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer
extendedKeyUsage=critical,timeStamping
\"
directory = /data/config/timestamping
; ==============================================================================
; fichier cnf
; {{dir}} sera remplacé par la valeur de \"directory\". ex: /data/config/timestamping
;
; Documentation
; https://www.phildev.net/ssl/opensslconf.html
; ==============================================================================
cnf = \"
#
# OpenSSL example configuration file.
# This is mostly being used for generation of certificate requests.
#

# This definition stops the following lines choking if HOME isn't
# defined.
HOME			= {{dir}}
RANDFILE		= \$ENV::HOME/.rnd

# Extra OBJECT IDENTIFIER info:
#oid_file		= \$ENV::HOME/.oid
oid_section		= new_oids

# To use this configuration file with the \"-extfile\" option of the
# \"openssl x509\" utility, name here the section containing the
# X.509v3 extensions to use:
# extensions		=
# (Alternatively, use a configuration file that has only
# X.509v3 extensions in its main [= default] section.)

[ new_oids ]

# We can add new OIDs in here for use by 'ca', 'req' and 'ts'.
# Add a simple OID like this:
# testoid1=1.2.3.4
# Or use config file substitution like this:
# testoid2=\${testoid1}.5.6

# Policies used by the TSA examples.
tsa_policy1 = 1.2.3.4.1
tsa_policy2 = 1.2.3.4.5.6
tsa_policy3 = 1.2.3.4.5.7

####################################################################
[ ca ]
default_ca	= CA_default		# The default ca section

####################################################################
[ CA_default ]

dir		= {{dir}}		# Where everything is kept
certs		= \$dir/certs		# Where the issued certs are kept
crl_dir		= \$dir/crl		# Where the issued crl are kept
database	= \$dir/index.txt	# database index file.
#unique_subject	= no			# Set to 'no' to allow creation of
# several ctificates with same subject.
new_certs_dir	= \$dir/newcerts		# default place for new certs.

certificate	= \$dir/cacert.pem 	# The CA certificate
serial		= \$dir/serial 		# The current serial number
crlnumber	= \$dir/crlnumber	# the current crl number
# must be commented out to leave a V1 CRL
crl		= \$dir/crl.pem 		# The current CRL
private_key	= \$dir/private/cakey.pem# The private key
RANDFILE	= \$dir/private/.rand	# private random number file

x509_extensions	= usr_cert		# The extentions to add to the cert

# Comment out the following two lines for the \"traditional\"
# (and highly broken) format.
name_opt 	= ca_default		# Subject Name options
cert_opt 	= ca_default		# Certificate field options

# Extension copying option: use with caution.
# copy_extensions = copy

# Extensions to add to a CRL. Note: Netscape communicator chokes on V2 CRLs
# so this is commented out by default to leave a V1 CRL.
# crlnumber must also be commented out to leave a V1 CRL.
# crl_extensions	= crl_ext

default_days	= 3650			# how long to certify for
default_crl_days= 30			# how long before next CRL
default_md	= default		# use public key default MD
preserve	= no			# keep passed DN ordering

# A few difference way of specifying how similar the request should look
# For type CA, the listed attributes must be the same, and the optional
# and supplied fields are just that :-)
policy		= policy_match

# For the CA policy
[ policy_match ]
countryName		= match
stateOrProvinceName	= match
organizationName	= match
organizationalUnitName	= optional
commonName		= supplied
emailAddress		= optional

# For the 'anything' policy
# At this point in time, you must list all acceptable 'object'
# types.
[ policy_anything ]
countryName		= optional
stateOrProvinceName	= optional
localityName		= optional
organizationName	= optional
organizationalUnitName	= optional
commonName		= supplied
emailAddress		= optional

####################################################################
[ req ]
default_bits		= 2048
default_keyfile 	= privkey.pem
distinguished_name	= req_distinguished_name
attributes		= req_attributes
x509_extensions	= v3_ca	# The extentions to add to the self signed cert

# Passwords for private keys if not present they will be prompted for
# input_password = secret
# output_password = secret

# This sets a mask for permitted string types. There are several options.
# default: PrintableString, T61String, BMPString.
# pkix	 : PrintableString, BMPString (PKIX recommendation before 2004)
# utf8only: only UTF8Strings (PKIX recommendation after 2004).
# nombstr : PrintableString, T61String (no BMPStrings or UTF8Strings).
# MASK:XXXX a literal mask value.
# WARNING: ancient versions of Netscape crash on BMPStrings or UTF8Strings.
string_mask = utf8only

# req_extensions = v3_req # The extensions to add to a certificate request

[ req_distinguished_name ]
countryName			= Country Name (2 letter code)
countryName_default		= AU
countryName_min			= 2
countryName_max			= 2

stateOrProvinceName		= State or Province Name (full name)
stateOrProvinceName_default	= Some-State

localityName			= Locality Name (eg, city)

0.organizationName		= Organization Name (eg, company)
0.organizationName_default	= Internet Widgits Pty Ltd

# we can do this but it is not needed normally :-)
#1.organizationName		= Second Organization Name (eg, company)
#1.organizationName_default	= World Wide Web Pty Ltd

organizationalUnitName		= Organizational Unit Name (eg, section)
#organizationalUnitName_default	=

commonName			= Common Name (e.g. server FQDN or YOUR name)
commonName_max			= 64

emailAddress			= Email Address
emailAddress_max		= 64

# SET-ex3			= SET extension number 3

[ req_attributes ]
challengePassword		= A challenge password
challengePassword_min		= 4
challengePassword_max		= 20

unstructuredName		= An optional company name

[ usr_cert ]

# These extensions are added when 'ca' signs a request.

# This goes against PKIX guidelines but some CAs do it and some software
# requires this to avoid interpreting an end user certificate as a CA.

basicConstraints=CA:FALSE

# Here are some examples of the usage of nsCertType. If it is omitted
# the certificate can be used for anything *except* object signing.

# This is OK for an SSL server.
# nsCertType			= server

# For an object signing certificate this would be used.
# nsCertType = objsign

# For normal client use this is typical
# nsCertType = client, email

# and for everything including object signing:
# nsCertType = client, email, objsign

# This is typical in keyUsage for a client certificate.
# keyUsage = nonRepudiation, digitalSignature, keyEncipherment

# This will be displayed in Netscape's comment listbox.
nsComment			= \"OpenSSL Generated Certificate\"

# PKIX recommendations harmless if included in all certificates.
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer

# This stuff is for subjectAltName and issuerAltname.
# Import the email address.
# subjectAltName=email:copy
# An alternative to produce certificates that aren't
# deprecated according to PKIX.
# subjectAltName=email:move

# Copy subject details
# issuerAltName=issuer:copy

#nsCaRevocationUrl		= http://www.domain.dom/ca-crl.pem
#nsBaseUrl
#nsRevocationUrl
#nsRenewalUrl
#nsCaPolicyUrl
#nsSslServerName

# This is required for TSA certificates.
# extendedKeyUsage = critical,timeStamping

[ v3_req ]

# Extensions to add to a certificate request

basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment

[ v3_ca ]


# Extensions for a typical CA


# PKIX recommendation.

subjectKeyIdentifier=hash

authorityKeyIdentifier=keyid:always,issuer

# This is what PKIX recommends but some broken software chokes on critical
# extensions.
#basicConstraints = critical,CA:true
# So we do this instead.
basicConstraints = CA:true

# Key usage: this is typical for a CA certificate. However since it will
# prevent it being used as an test self-signed certificate it is best
# left out by default.
# keyUsage = cRLSign, keyCertSign

# Some might want this also
# nsCertType = sslCA, emailCA

# Include email address in subject alt name: another PKIX recommendation
# subjectAltName=email:copy
# Copy issuer details
# issuerAltName=issuer:copy

# DER hex encoding of an extension: beware experts only!
# obj=DER:02:03
# Where 'obj' is a standard or added object
# You can even override a supported extension:
# basicConstraints= critical, DER:30:03:01:01:FF

[ crl_ext ]

# CRL extensions.
# Only issuerAltName and authorityKeyIdentifier make any sense in a CRL.

# issuerAltName=issuer:copy
authorityKeyIdentifier=keyid:always

[ proxy_cert_ext ]
# These extensions should be added when creating a proxy certificate

# This goes against PKIX guidelines but some CAs do it and some software
# requires this to avoid interpreting an end user certificate as a CA.

basicConstraints=CA:FALSE

# Here are some examples of the usage of nsCertType. If it is omitted
# the certificate can be used for anything *except* object signing.

# This is OK for an SSL server.
# nsCertType			= server

# For an object signing certificate this would be used.
# nsCertType = objsign

# For normal client use this is typical
# nsCertType = client, email

# and for everything including object signing:
# nsCertType = client, email, objsign

# This is typical in keyUsage for a client certificate.
# keyUsage = nonRepudiation, digitalSignature, keyEncipherment

# This will be displayed in Netscape's comment listbox.
nsComment			= \"OpenSSL Generated Certificate\"

# PKIX recommendations harmless if included in all certificates.
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer

# This stuff is for subjectAltName and issuerAltname.
# Import the email address.
# subjectAltName=email:copy
# An alternative to produce certificates that aren't
# deprecated according to PKIX.
# subjectAltName=email:move

# Copy subject details
# issuerAltName=issuer:copy

#nsCaRevocationUrl		= http://www.domain.dom/ca-crl.pem
#nsBaseUrl
#nsRevocationUrl
#nsRenewalUrl
#nsCaPolicyUrl
#nsSslServerName

# This really needs to be in place for it to be a proxy certificate.
proxyCertInfo=critical,language:id-ppl-anyLanguage,pathlen:3,policy:foo

####################################################################
[ tsa ]

default_tsa = tsa_config1	# the default TSA section

[ tsa_config1 ]

# These are used by the TSA reply generation only.
dir		= {{dir}}		# TSA root directory
serial		= \$dir/serial	# The current serial number (mandatory)
crypto_device	= builtin		# OpenSSL engine to use for signing
signer_cert	= \$dir/tsacert.pem 	# The TSA signing certificate
# (optional)
certs		= \$dir/tsacert.pem	# Certificate chain to include in reply
# (optional)
signer_key	= \$dir/tsakey.pem # The TSA private key (optional)
signer_digest   = sha256

default_policy	= tsa_policy1		# Policy if request did not specify it
# (optional)
other_policies	= tsa_policy2, tsa_policy3	# acceptable policies (optional)
digests		= md5, sha1		# Acceptable message digests (mandatory)
accuracy	= secs:1, millisecs:500, microsecs:100	# (optional)
clock_precision_digits  = 0	# number of digits after dot. (optional)
ordering		= yes	# Is ordering defined for timestamps?
# (optional, default: no)
tsa_name		= yes	# Must the TSA name be included in the reply?
# (optional, default: no)
ess_cert_id_chain	= no	# Must the ESS cert id chain be included?
# (optional, default: no)
\"

[exploitation_service]
name = \"Service d'Exploitation\"
identifier = se
installer_name = \"${INSTALLER_NAME:-Libriciel}\"
" > $INI_FILE

  ################################################################################
  # Définition du 1er Admin technique
  ################################################################################
  if [ "$INIT_FIRST_ADMIN_TECH" = "true" ]; then
    echo "[admins]
user1[username] = \"$INIT_FIRST_ADMIN_TECH_USERNAME\"
user1[name] = \"$INIT_FIRST_ADMIN_TECH_NAME\"
user1[email] = \"$INIT_FIRST_ADMIN_TECH_EMAIL\"
user1[password] = \"$INIT_FIRST_ADMIN_TECH_PASSWORD\"
" >> $INI_FILE
  fi

  ################################################################################
  # Définition des volumes
  ################################################################################
  if [ "$INIT_VOLUME1" = "true" ] || [ "$INIT_VOLUME2" = "true" ]; then
    echo "[volumes]" >> "$INI_FILE"
  fi

  add_volume() {
    local varname="$1"
    local driver="$2"
    local name="$3"
    local path="$4"
    local alert_rate="$5"
    local max_disk_usage="$6"
    local endpoint="$7"
    local region="$8"
    local bucket="$9"
    local credentials_key="${10}"
    local credentials_secret="${11}"
    local verify="${12}"
    local use_proxy="${13}"

    # Variables communes à tous les drivers
    echo "${varname}[driver] = \"$driver\"
${varname}[name] = \"$name\"
${varname}[alert_rate] = $alert_rate
${varname}[max_disk_usage] = $max_disk_usage" >> "$INI_FILE"

    if [ "$driver" = "FILESYSTEM" ]; then
      echo "${varname}[path] = \"$path\"
" >> "$INI_FILE"
    fi
    if [ "$driver" = "MINIO" ]; then
      echo "${varname}[endpoint] = \"$endpoint\"
${varname}[region] = \"$region\"
${varname}[bucket] = \"$bucket\"
${varname}[credentials_key] = \"$credentials_key\"
${varname}[credentials_secret] = \"$credentials_secret\"
${varname}[verify] = \"$verify\"
${varname}[use_proxy] = $use_proxy
" >> "$INI_FILE"
    fi
  }
  # end add_volume()

  if [ "$INIT_VOLUME1" = "true" ]; then
    add_volume \
      "vol1" \
      "$INIT_VOLUME1_DRIVER" \
      "$INIT_VOLUME1_NAME" \
      "$INIT_VOLUME1_PATH" \
      "$INIT_VOLUME1_ALERT_RATE" \
      "$INIT_VOLUME1_MAX_DISK_USAGE" \
      "$INIT_VOLUME1_ENDPOINT" \
      "$INIT_VOLUME1_REGION" \
      "$INIT_VOLUME1_BUCKET" \
      "$INIT_VOLUME1_CREDENTIALS_KEY" \
      "$INIT_VOLUME1_CREDENTIALS_SECRET" \
      "$INIT_VOLUME1_VERIFY" \
      "$INIT_VOLUME1_USE_PROXY"
  fi
  if [ "$INIT_VOLUME2" = "true" ]; then
    add_volume \
      "vol2" \
      "$INIT_VOLUME2_DRIVER" \
      "$INIT_VOLUME2_NAME" \
      "$INIT_VOLUME2_PATH" \
      "$INIT_VOLUME2_ALERT_RATE" \
      "$INIT_VOLUME2_MAX_DISK_USAGE" \
      "$INIT_VOLUME2_ENDPOINT" \
      "$INIT_VOLUME2_REGION" \
      "$INIT_VOLUME2_BUCKET" \
      "$INIT_VOLUME2_CREDENTIALS_KEY" \
      "$INIT_VOLUME2_CREDENTIALS_SECRET" \
      "$INIT_VOLUME2_VERIFY" \
      "$INIT_VOLUME2_USE_PROXY"
  fi

  ################################################################################
  # Définition de l'ECS
  ################################################################################

  # Calcul de la liste des volumes activés
  ecs_name=""
  volumes_list=""
  if [ "$INIT_VOLUME1" = "true" ]; then
    volumes_list="vol1"
  fi
  if [ "$INIT_VOLUME2" = "true" ]; then
    if [ -n "$volumes_list" ]; then
      volumes_list="${volumes_list},vol2"
    else
      volumes_list="vol2"
    fi
  fi

# Vérifier si l'espace sécurisé est activé
  if [ "$INIT_SECURE_DATA_SPACE" = "true" ]; then
    ecs_name="ecs1"
    echo "[secure_data_spaces]
ecs1[name] = \"$INIT_SECURE_DATA_SPACE_NAME\"
ecs1[description] = \"$INIT_SECURE_DATA_SPACE_DESCRIPTION\"
ecs1[is_default] = 1
ecs1[volumes] = \"$volumes_list\"
" >> "$INI_FILE"
  fi

################################################################################
# Définition du Service d'archives
################################################################################
  if [ "$INIT_FIRST_TENANT" = "true" ]; then
    echo "[archival_agencies]
sa[name] = \"$INIT_FIRST_TENANT_NAME\"
sa[short_name] =
sa[identifier] = \"$INIT_FIRST_TENANT_IDENTIFIER\"
sa[secure_data_spaces] = $ecs_name
sa[is_main_archival_agency] = 1
" >> "$INI_FILE"
  fi

  echo "Pas de configuration existante, initialisation"
  echo " "
  (sudo -u "$APACHE_RUN_USER" /var/www/asalae/bin/cake install config "$INI_FILE" \
    && rm "$INI_FILE") \
    || (rm -f "$CONFIG_FILE" && failed_with_error "install failed")
  echo ""
elif [[ "$(/usr/bin/php /var/www/asalae/bin/cake.php configuration get --raw Datasources.default.host)" == "localhost" ]]; then
  configure Datasources.default.host "$POSTGRES_DBHOST"
else
  echo ""
  echo "Configuration existante "
  echo ""
fi

configure Datasources.default.log null

/usr/bin/php /var/www/asalae/bin/cake.php migrations migrate || exit 1
/usr/bin/php /var/www/asalae/bin/cake.php migrations seed || exit 1
rm /var/www/asalae/tmp/cache -r
/usr/bin/php /var/www/asalae/bin/cake.php update || exit 1
/usr/bin/php /var/www/asalae/bin/cake.php roles_perms import --keep /var/www/asalae/resources/export_roles.json || exit 1

sudo -u "$APACHE_RUN_USER" timeout 60 sf -update

# config pour bin/cake console
APACHE_HOME_DIR=$(bash -c "cd ~$(printf %q "$APACHE_RUN_USER") && pwd")
sudo -u "$APACHE_RUN_USER" mkdir -p "$APACHE_HOME_DIR/.config/psysh"
sudo -u "$APACHE_RUN_USER" cp config/psych.php "$APACHE_HOME_DIR/.config/psysh/config.php"

if [ ! -f "/var/www/asalae-volume/config/path_to_local.php" ]
then
  echo "<?php return '$CONFIG_FILE';?>" > /var/www/asalae-volume/config/path_to_local.php
fi
chown "$APACHE_RUN_USER" /var/www/asalae-volume/config/path_to_local.php

# permissions
echo "setting permissions for $APACHE_RUN_USER:$APACHE_RUN_GROUP"
cp -a /var/www/asalae-volume/config/path_to_local.php /var/www/asalae/config/path_to_local.php
PATH_TO_LOCAL=$(php -r 'echo include "/var/www/asalae/config/path_to_local.php";')
chown "$APACHE_RUN_USER" /var/www/asalae/config/path_to_local.php
chmod 600 "$PATH_TO_LOCAL"
chmod 600 "$CONFIG_DIR/administrators.json"

# chown des volumes
chown "$APACHE_RUN_USER" -Rf \
  /etc/apache2/sites-available \
  /etc/apache2/ssl \
  /var/www/asalae/tmp \
  /var/www/asalae/logs \
  /var/www/asalae/webroot/org-entity-data

configure Beanstalk.client_host "${BEANSTALK_CLIENT_HOST:-asalae_beanstalk}"
if [ -n "$BEANSTALK_CLIENT_PORT" ]; then
  configure Beanstalk.client_port "$BEANSTALK_CLIENT_PORT"
fi
configure Ratchet.ping "${RATCHET_PING:-http://asalae_ratchet:8080}"
configure Ratchet.connect "wss://${HTTP_HOST}/wss"
if [ -n "$POSTGRES_DBPORT" ]; then
  configure Datasources.default.port "$POSTGRES_DBPORT"
fi
configure Antivirus.host "${CLAMAV_HOST:-asalae_clamav}"
if [ -n "$CLAMAV_PORT" ]; then
  configure Antivirus.port "$CLAMAV_PORT"
fi
configure Mercure.jwt_secret "${MERCURE_JWT_SECRET:-!ChangeThisMercureHubJWTSecretKey!}"

configure Proxy.host "${PROXY_HOST:-null}"
configure Proxy.port "${PROXY_PORT:-null}"
configure Proxy.username "${PROXY_USER:-null}"
configure Proxy.password "${PROXY_PASSWORD:-null}"

echo "initialization completed"
