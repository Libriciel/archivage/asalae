#!/bin/bash

failed_with_error () {
  echo "$1" >&2
  exit 1
}
CURRENT_DIR="$(realpath "$(dirname "$0")")"
cd "$CURRENT_DIR" || exit

# initialisation d'un nouveau volume
cp ./asalae-mink.conf /etc/apache2/sites-available/asalae.conf
sed -i'' -E \
  -e "s/\%\{HTTP_HOST\}/${HTTP_HOST}/" \
  /etc/apache2/sites-available/asalae.conf

APACHE_ERROR_LOG=/dev/stderr
APACHE_TRANSFER_LOG=/dev/null

if [ "$APACHE_LOG_FILES" == "true" ]
then
  APACHE_ERROR_LOG=/apache-logs/asalae_error.log
  APACHE_TRANSFER_LOG=/apache-logs/asalae_access.log
fi

sed -i'' -E \
  -e "s#\%\{APACHE_ERROR_LOG\}#${APACHE_ERROR_LOG}#" \
  /etc/apache2/sites-available/asalae.conf
sed -i'' -E \
  -e "s#\%\{APACHE_TRANSFER_LOG\}#${APACHE_TRANSFER_LOG}#" \
  /etc/apache2/sites-available/asalae.conf

sed -i'' -E \
  -e "s/\%\{APACHE_ADD_TO_VHOST\}/${APACHE_ADD_TO_VHOST}/" \
  /etc/apache2/sites-available/asalae.conf

if [ ! -f /etc/apache2/ssl/server.crt ]; then
    cp /data-entrypoint/server.crt /etc/apache2/ssl/server.crt
fi
if [ ! -f /etc/apache2/ssl/server.key ]; then
    cp /data-entrypoint/server.key /etc/apache2/ssl/server.key
fi

# Copie des fichiers de l'image vers le volume
cd /var/www/asalae/ || exit 1
xargs -I % sh -c 'rm -rf "/var/www/asalae-volume/%" && cp -aR "%" "/var/www/asalae-volume/" 2>/dev/null' < files_to_override_on_init.txt
cd /var/www/asalae-volume/ || exit 1
mkdir -p /var/www/asalae-volume/tmp /var/www/asalae-volume/logs
chown "$APACHE_RUN_USER" /var/www/asalae-volume/tmp /var/www/asalae-volume/logs /apache-logs

CONFIG_DIR=/data/config
CONFIG_FILE=$CONFIG_DIR/app_local.json
mkdir -p $CONFIG_DIR
mkdir -p /etc/apache2/ssl

# chown des volumes
chown "$APACHE_RUN_USER" -Rf \
  /etc/apache2/sites-available \
  /etc/apache2/ssl \
  /data \
  /var/www/asalae/tmp \
  /var/www/asalae/logs \
  /var/www/asalae/webroot/org-entity-data

if [ ! -f "$CONFIG_FILE" ]
then
  cp "$CURRENT_DIR/app_local_mink.json" "$CONFIG_FILE"
  cp "$CURRENT_DIR/administrators-mink.json" "$CONFIG_DIR/administrators.json"
  echo "<?php return '$CONFIG_FILE';?>" > /var/www/asalae-volume/config/path_to_local.php

  sudo -u "$APACHE_RUN_USER" cp -r /var/www/asalae/tests/Data/asalae_data/* /data

  /usr/bin/php /var/www/asalae/bin/cake.php migrations migrate || exit 1
  /usr/bin/php /var/www/asalae/bin/cake.php fixtures database --datasource default --with-volumes
  rm /var/www/asalae/tmp/cache -r
  /usr/bin/php /var/www/asalae/bin/cake.php update || exit 1
  /usr/bin/php /var/www/asalae/bin/cake.php roles_perms import --keep /var/www/asalae/resources/export_roles.json || exit 1

  sudo -u "$APACHE_RUN_USER" timeout 60 sf -update

  # config pour bin/cake console
  APACHE_HOME_DIR=$(bash -c "cd ~$(printf %q "$APACHE_RUN_USER") && pwd")
  sudo -u "$APACHE_RUN_USER" mkdir -p "$APACHE_HOME_DIR/.config/psysh"
  sudo -u "$APACHE_RUN_USER" cp config/psych.php "$APACHE_HOME_DIR/.config/psysh/config.php"
fi

# permissions
echo "setting permissions for $APACHE_RUN_USER:$APACHE_RUN_GROUP"
chown "$APACHE_RUN_USER":"$APACHE_RUN_GROUP" /data -Rf
PATH_TO_LOCAL=$(php -r 'echo include "/var/www/asalae-volume/config/path_to_local.php";')
chmod 600 "$PATH_TO_LOCAL"
chmod 600 "$CONFIG_DIR/administrators.json"

# chown des volumes
chown "$APACHE_RUN_USER" -Rf \
  /etc/apache2/sites-available \
  /etc/apache2/ssl \
  /data \
  /var/www/asalae/tmp \
  /var/www/asalae/logs \
  /var/www/asalae/webroot/org-entity-data

echo "initialization completed"
