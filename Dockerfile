FROM ubuntu:24.04 AS baseimage

ARG APACHE_UID=1000

ENV TIMEZONE=Europe/Paris
ENV APACHE_RUN_USER=asalae
ENV APACHE_RUN_GROUP=asalae
ENV DEBIAN_FRONTEND=noninteractive
ENV INITIALIZATION_DIR=/data-entrypoint
ENV APACHE_LOG_DIR=/data-logs
ENV APACHE_PID_FILE=/data-run/apache2.pid
ENV APACHE_RUN_DIR=/data-run

COPY ./bin/bash_cake_completion /etc/bash_completion.d/cake
COPY ./apache2-foreground /usr/local/bin/apache2-foreground
COPY ./build/apache-security.conf /etc/apache2/conf-available/
COPY build/asalae.conf /etc/apache2/sites-available/

RUN ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime \
    && echo $TIMEZONE > /etc/timezone \
    && apt-get update \
    && apt dist-upgrade -y

RUN apt install -y \
    sudo \
    vim \
    curl \
    wget \
    zip \
    unzip \
    rar \
    && apt install -y --allow-unauthenticated \
    apache2

RUN apt install -y \
    libapache2-mod-php8.3 \
    && apt install software-properties-common -y \
    && add-apt-repository ppa:ondrej/php -y \
    && apt install -y php8.3 \
    php8.3-curl \
    php8.3-gd \
    php8.3-http \
    php8.3-intl \
    php8.3-ldap \
    php8.3-mbstring \
    php8.3-pcov  \
    php8.3-pdo  \
    php8.3-pgsql \
    php8.3-simplexml \
    php8.3-soap \
    php8.3-sqlite3 \
    php8.3-xdebug \
    php8.3-xsl \
    php8.3-zip \
    php8.3-zmq \
    php8.3-raphf \
    && update-alternatives --set php /usr/bin/php8.3

RUN apt install -y bash-completion imagemagick \
    && chmod +x /usr/local/bin/apache2-foreground \
    && mkdir -p /data /data/config /data/data-asalae && chown www-data /var/www && chown www-data /data -R \
    && apt update && apt install -y locales \
    && sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen \
    && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && deluser ubuntu \
    && useradd --create-home --uid "$APACHE_UID" --user-group "$APACHE_RUN_USER" || true

RUN sed -i "s|APACHE_RUN_USER=.*|APACHE_RUN_USER=$APACHE_RUN_USER|g" /etc/apache2/envvars \
    && sed -i "s|APACHE_RUN_GROUP=.*|APACHE_RUN_GROUP=$APACHE_RUN_GROUP|g" /etc/apache2/envvars \
    && echo 'ServerName asalae' >> /etc/apache2/apache2.conf \
    && mkdir -p /etc/apache2/ssl \
    && openssl req -new -x509 -sha256 -newkey rsa:2048 -nodes -keyout /etc/apache2/ssl/server.key -out /etc/apache2/ssl/server.crt -days 365 -subj "/C=FR/ST=FR/L=Herault/O=AS/OU=PRA/CN=localhost" \
    && a2enmod headers proxy proxy_http rewrite ssl \
    && a2enconf apache-security.conf \
    && a2dissite 000-default.conf && a2ensite asalae.conf \
    && mkdir -p /var/www/asalae && chown $APACHE_RUN_USER /var/www/asalae -R \
    && mkdir -p "$INITIALIZATION_DIR" "$APACHE_LOG_DIR" "$APACHE_RUN_DIR" \
    && cp /etc/apache2/sites-available/asalae.conf "$INITIALIZATION_DIR"/ \
    && cp /etc/apache2/ssl/* "$INITIALIZATION_DIR"/ \
    && sed -i "s|APACHE_PID_FILE=.*|APACHE_PID_FILE=$APACHE_PID_FILE|g" /etc/apache2/envvars \
    && sed -i "s|APACHE_RUN_DIR=.*|APACHE_RUN_DIR=$APACHE_RUN_DIR|g" /etc/apache2/envvars \
    && sed -i "s|APACHE_LOG_DIR=.*|APACHE_LOG_DIR=$APACHE_LOG_DIR|g" /etc/apache2/envvars \
    && chown -R $APACHE_RUN_USER /etc/apache2/ "$INITIALIZATION_DIR" "$APACHE_LOG_DIR" "$APACHE_RUN_DIR"

RUN wget https://go.dev/dl/go1.20.3.linux-amd64.tar.gz \
    && tar -C /usr/local -xzf go1.20.3.linux-amd64.tar.gz \
    && update-alternatives --install "/usr/bin/go" "go" "/usr/local/go/bin/go" 0 \
    && update-alternatives --set go /usr/local/go/bin/go
#
RUN GOBIN=/usr/local/bin/ go install github.com/richardlehane/siegfried/cmd/sf@latest \
    && sudo -u $APACHE_RUN_USER sf -update \
    && rm go1.20.3.linux-amd64.tar.gz \
    && rm -rf /usr/local/go /root/go

RUN mkdir -p /var/www/asalae-volume

RUN add-apt-repository -y ppa:openjdk-r/ppa && apt update && apt install -y \
    catdoc \
    ffmpeg \
    handbrake-cli \
    openjdk-21-jre \
    libreoffice \
    libxml2-utils \
    poppler-utils \
    unoconv  \
    webp \
    wkhtmltopdf \
    xlsx2csv \
    xvfb \
    python3-setuptools

RUN apt update && apt install -y gnupg2 wget \
    && echo "deb [arch=amd64 signed-by=/usr/share/keyrings/google-linux-signing-key.gpg] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list \
    && wget -O- https://dl-ssl.google.com/linux/linux_signing_key.pub | gpg --dearmor -o /usr/share/keyrings/google-linux-signing-key.gpg \
    && apt-get update \
    && apt-get install -y google-chrome-stable

RUN apt install -y php8.3-imagick postgresql
RUN wget https://ressources.libriciel.fr/public/asalae/sonar-scanner-cli-4.5.0.2216-linux.zip \
    && unzip sonar-scanner-cli-4.5.0.2216-linux.zip \
    && mv sonar-scanner-4.5.0.2216-linux/ /opt/sonar-scanner/ \
    && rm sonar-scanner-cli-4.5.0.2216-linux.zip

RUN apt update && apt install -y bzip2 \
    && wget https://raw.githubusercontent.com/dilshod/xlsx2csv/master/xlsx2csv.py -O /usr/bin/xlsx2csv \
    && sed -i '1s|#!/usr/bin/env python|#!/usr/bin/python3|' /usr/bin/xlsx2csv \
    && chmod +x /usr/bin/xlsx2csv

RUN apt update && apt install -y weasyprint

COPY ./build/initialization.sh "$INITIALIZATION_DIR"/

RUN echo "export HOME=/home/asalae" >> /etc/apache2/envvars
RUN cp -r /home/asalae/.local/ /root

CMD ["apache2-foreground"]

#-------------------------------------------------------------------------------

FROM baseimage AS composer-install

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
RUN apt update && apt install -y git

COPY composer.json /var/www/asalae/composer.json
COPY composer.lock /var/www/asalae/composer.lock

WORKDIR /var/www/asalae

ARG COMPOSER_COMMAND="composer install --optimize-autoloader"
ENV COMPOSER_ALLOW_SUPERUSER=true
RUN $COMPOSER_COMMAND

#-------------------------------------------------------------------------------
FROM quay.io/keycloak/keycloak:22.0.0 AS keycloak_builder

ENV KC_DB=postgres
ENV KC_FEATURES="token-exchange,scripts,preview"

WORKDIR /opt/keycloak

RUN /opt/keycloak/bin/kc.sh build --cache=ispn --health-enabled=true --metrics-enabled=true

#-------------------------------------------------------------------------------
FROM quay.io/keycloak/keycloak:22.0.0 AS keycloak

LABEL image.version=22.0.0

COPY --from=keycloak_builder /opt/keycloak/ /opt/keycloak/

# https://github.com/keycloak/keycloak/issues/19185#issuecomment-1480763024
USER root
RUN sed -i '/disabledAlgorithms/ s/ SHA1,//' /etc/crypto-policies/back-ends/java.config
USER keycloak

RUN /opt/keycloak/bin/kc.sh show-config

ENTRYPOINT ["/opt/keycloak/bin/kc.sh"]

#-------------------------------------------------------------------------------

FROM composer-install AS final

ARG APACHE_UID=33
ARG APACHE_USERNAME=asalae

RUN useradd --create-home --uid "$APACHE_UID" "$APACHE_USERNAME" || true

ENV URL=asalae.local
ENV DEBUG=false
ENV ADMIN_USER=admin
ENV ADMIN_PASSWORD=password
ENV ADMIN_MAIL=local@localhost.local
ENV DATA_DIR=/data
ENV RATCHET_HOST=ratchet
ENV MAIL_FROM=local@localhost.local
ENV SV_NAME=sv
ENV SA_NAME=sa
ENV POSTGRES_USER=asalae2
ENV POSTGRES_PASSWORD=asalae2
ENV POSTGRES_DB=asalae2
ENV POSTGRES_DBHOST=db
ENV POSTGRES_DBPORT=5432
ENV POSTFIX_RELAYHOST=postfix
ENV POSTFIX_PORT=25
ENV TZ=Europe/Paris
ENV APACHE_LOG_FILES=true

COPY --chown=$APACHE_USERNAME . /var/www/asalae/

# empêche de mettre en cache les commandes suivantes
ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" skipcache
RUN date > /var/www/asalae/.image-dev
WORKDIR /var/www/asalae
RUN /var/www/asalae/bin/changelog_builder.php > CHANGELOG.md \
    && /var/www/asalae/bin/cake hash-dir \
    bin/cake.php \
    src/ \
    webroot/ \
    resources/ \
    templates/ \
    vendor/ \
    -r \
    --filter webroot/org-entity-data/\* \
    --filter resources/hashes.txt \
    --filter resources/hashes.txt.md5 \
    --filter webroot/libersign/\* \
    --filter webroot/libersign \
    --filter webroot/coverage/\* \
    --filter webroot/coverage \
    --filter '*.pot' \
    -o resources/hashes.txt

RUN rm -rf /var/www/asalae/tmp/* /var/www/asalae/logs/*

CMD ["/usr/sbin/apachectl", "-D FOREGROUND"]
