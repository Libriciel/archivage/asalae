<?php

use Cake\Routing\Router;

$root = dirname(__DIR__);
require $root . '/vendor/autoload.php';
require $root . '/config/bootstrap.php';

$url = Router::fullBaseUrl();

$yml = file_get_contents(__DIR__.'/api.yml');

header('Content-Type', 'text/plain');
echo str_replace(
    <<<EOT
  - url: "https://asalae-master.dev.libriciel.net"
    description: "Instance de DEV"
  - url: "https://asalae-3-0.recette.libriciel.net"
    description: "Instance de recette"
EOT,
    <<<EOT
  - url: "$url"
    description: "Instance locale"
EOT,
    $yml
);
