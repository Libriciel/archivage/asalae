/* globals $, __, AsalaeGlobal, AsalaeFilter */

if (typeof __ === 'undefined') {
    var __ = function (str) {
        return str;
    };
}

/**
 * Pagination ajax combinée avec TableGenerator
 */
class AsalaePaginator {
    /**
     * Constructeur de classe
     * @param element
     * @param params
     */
    constructor(element, params) {
        this.element = $(element);
        this.params = params;
        this.table = typeof params.table === 'string' ? eval(params.table) : params.table;
        this.url = params.url;
        this.first = params.first ? params.first : 1;
        this.last = params.last ? params.last : 1;
        this.modulus = params.modulus ? params.modulus : 2;
        var indexInstance = AsalaePaginator.instance(this);
        this.element.attr('data-paginator', indexInstance);
        this._listenClicks();
        this._listenForm();
        this.element.trigger('initialized.paginator');
    }

    /**
     * Ajoute/reconstruit les actions sur les clics
     * @private
     */
    _listenClicks() {
        this.element.find('ul.pagination li a').off('click.paginator');
        this.element.find('ul.pagination li:not(.active):not(.disabled) a').on('click.paginator', this._paginatorCallback());
        this.element.find('ul.pagination li.ellipsis button').off('click.paginator').on('click.paginator', this._ellipsisCallback());
        this.table.table.find('thead a').off('click.paginator').on('click.paginator', this._sortCallback());
    }

    /**
     * Gère l'envoi du formulaire de filtre
     * @private
     */
    _listenForm() {
        var that = this;
        this.table.table.off('created.form').on('created.form', function (e, data) {
            var form = data.form;
            var th = data.th;
            var container = $(form).on('submit', function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (th.find('button.remove-filter').length === 0) {
                    var button = $('<button type="button" class="btn btn-link remove-filter"></button>').append(th.find('span.field')).attr('title', __("Retirer le tri et les filtres")).on('click.removeFilter', function () {
                        var fields = th.find('button[data-fields].btn-filter').attr('data-fields').split(';');
                        th.removeClass('active');
                        $(this).parent().prepend($(this).find('span.field'));
                        $(this).remove();
                        $(that.table.table).find('th a').add($(that.element).find('a')).each(function () {
                            var href = $(this).attr('href');
                            for (var i = 0; i < fields.length; i++) {
                                href = AsalaePaginator._unsetUrlParam(href, fields[i]);
                            }
                            $(this).attr('href', href);
                        });
                        that.ajax(1, true);
                    });
                    th.prepend(button).addClass('active');
                }
                var url = that.url;
                url += url.indexOf('?') !== -1 ? '&' : '?';
                var serialized = $(this).serialize();
                var href = url + serialized;
                var body = that.element.closest('.modal-body');
                var offset = body.scrollTop();
                var formdata = AsalaeFilter.getFiltersData(this);
                AsalaeLoading.ajax({
                    url: url + formdata,
                    headers: {
                        Accept: 'application/json',
                        "X-Paginator-Count": 'true'
                    },
                    success: function (content, textStatus, jqXHR) {
                        that._headerParams(jqXHR);
                        that.table.data = content;
                        TableGenerator.appendActions(that.table.data, that.table.actions);
                        var table = $(that.table.table);
                        table.find('tbody').remove();
                        table.append(that.table._generateTbody());
                        that.element.trigger('paginator.ajax.success', { content: content, jqXHR: jqXHR });
                        var params = serialized.split('&');
                        table.find('thead').find('a.sortable, a.sort-asc, a.sort-desc').each(function () {
                            var href = $(this).attr('href');
                            var keyvalue;
                            for (var i = 0; i < params.length; i++) {
                                keyvalue = params[i].split('=');
                                href = AsalaePaginator._setUrlParam(href, keyvalue[0], keyvalue[1]);
                            }
                            $(this).attr('href', href);
                        });
                    },
                    error: function (e) {
                        that.element.trigger('paginator.ajax.error', e);
                    },
                    complete: function () {
                        that.element.enable();
                        that.rebuildPagination(href);
                        that.element.trigger('paginator.ajax.complete');
                        body.scrollTop(0).scrollTop(offset);
                    }
                });
                $(document).off('click.outside.form');
                container.toggle('drop', function () {
                    $(this).remove();
                });
            });
            setTimeout(function () {
                $(document).off('click.outside.form').on('click.outside.form keydown.outside.form', function (e) {
                    if (e.keyCode === 27 || !container.is(e.target) && container.has(e.target).length === 0) {
                        $(document).off('click.outside.form');
                        container.toggle('drop', function () {
                            $(this).remove();
                        });
                    }
                });
                container.find('input').on('keydown', function (e) {
                    if (e.keyCode === 13) {
                        e.preventDefault();
                        e.stopPropagation();
                        $(this).closest('form').submit();
                    }
                });
            }, 0);
        });
    }

    /**
     * garde en mémoire les instances de cette classe et donne l'index
     * @param paginator
     * @return {number}
     */
    static instance(paginator) {
        if (typeof AsalaePaginator.instances === 'undefined') {
            AsalaePaginator.instances = [];
        }
        var len = AsalaePaginator.instances.length;
        AsalaePaginator.instances.push(paginator);
        return len;
    }

    /**
     * Simule un click sur une page de pagination
     * @param {string|int} page
     * @param {boolean} count
     */
    ajax(page, count = false) {
        var url = this.url;
        if (url.indexOf('?') !== -1) {
            var m = url.match(/(\?page=|&page=)\d+/);
            if (m) {
                url.replace(m[0], m[1] + page);
            } else {
                url += '&page=' + page;
            }
        } else {
            url += '?page=' + page;
        }
        this._paginatorCallback(count)(url);
    }

    /**
     * Action du click sur les liens
     * @private
     */
    _paginatorCallback(count = false) {
        var that = this;
        /**
         * @var {string|Event} e
         */
        return function (e) {
            var href;
            var offset;
            if (typeof e !== 'string') {
                e.preventDefault();
                href = $(this).attr('href');
            } else {
                href = e;
            }
            if (!href) {
                return;
            }
            var headers = {
                Accept: 'application/json'
            };
            if (count) {
                headers['X-Paginator-Count'] = 'true';
            }
            var body = that.element.closest('.modal-body');
            offset = body.scrollTop();
            that.element.find('ul.pagination li a').attr('href', '#');
            AsalaeLoading.ajax({
                url: href,
                headers: headers,
                success: function (content, textStatus, jqXHR) {
                    that._headerParams(jqXHR);
                    that.table.data = content;
                    TableGenerator.appendActions(that.table.data, that.table.actions);
                    $(that.table.table).find('tbody').remove();
                    $(that.table.table).append(that.table._generateTbody());
                    that.element.trigger('paginator.ajax.success', { content: content, jqXHR: jqXHR });
                },
                error: function (e) {
                    that.element.trigger('paginator.ajax.error', e);
                },
                complete: function () {
                    that.element.enable();
                    that.rebuildPagination(href);
                    that.element.trigger('paginator.ajax.complete');
                    body.scrollTop(0).scrollTop(offset);
                }
            });
        };
    }

    /**
     * Action du click sur les tris
     * @private
     */
    _sortCallback() {
        var that = this;
        /**
         * @var {Event} e
         */
        return function (e) {
            e.preventDefault();

            var th = $(this).closest('th');
            var href = $(this).attr('href');
            var sort = AsalaePaginator._getUrlParam(href, 'sort');
            var direction = AsalaePaginator._getUrlParam(href, 'direction');
            if ($(this).hasClass('sortable')) {
                $(this).removeClass('sortable').addClass('sort-asc');
                $(this).attr('href', AsalaePaginator._setUrlParam(href, 'direction', 'desc'));
                $(this).closest('tr').find('th').removeClass('active');
            } else if ($(this).hasClass('sort-asc')) {
                $(this).removeClass('sort-asc').addClass('sort-desc');
                $(this).attr('href', AsalaePaginator._setUrlParam(href, 'direction', 'asc'));
            } else {
                $(this).removeClass('sort-desc').addClass('sort-asc');
                $(this).attr('href', AsalaePaginator._setUrlParam(href, 'direction', 'desc'));
            }
            that.element.find('ul.pagination li a').attr('href', '#');
            var sorts = that.table.table.find('thead a').disable();

            AsalaeLoading.ajax({
                url: href,
                headers: {
                    Accept: 'application/json'
                },
                success: function (content, textStatus, jqXHR) {
                    that._headerParams(jqXHR);
                    that.table.data = content;
                    TableGenerator.appendActions(that.table.data, that.table.actions);
                    $(that.table.table).find('tbody').remove();
                    $(that.table.table).append(that.table._generateTbody());

                    that.element.find('ul.pagination li a').each(function () {
                        var href = AsalaePaginator._setUrlParam($(this).attr('href'), 'sort', sort);
                        href = AsalaePaginator._setUrlParam(href, 'direction', direction);
                        $(this).attr('href', href);
                    });
                    if (th.find('button.remove-filter').length === 0) {
                        var button = $('<button type="button" class="btn btn-link remove-filter"></button>').append(th.find('span.field')).attr('title', __("Retirer le tri et les filtres")).on('click.removeFilter', function () {
                            var fields = th.find('button[data-fields].btn-filter').attr('data-fields').split(';');
                            th.removeClass('active');
                            $(this).parent().prepend($(this).find('span.field'));
                            $(this).remove();
                            $(that.table.table).find('th a').add($(that.element).find('a')).each(function () {
                                var href = $(this).attr('href');
                                for (var i = 0; i < fields.length; i++) {
                                    href = AsalaePaginator._unsetUrlParam(href, fields[i]);
                                }
                                $(this).attr('href', href);
                            });
                            that.ajax(1, true);
                        });
                        th.prepend(button).addClass('active');
                    }

                    that.element.trigger('paginator.ajax.success', { content: content, jqXHR: jqXHR });
                },
                error: function (e) {
                    that.element.trigger('paginator.ajax.error', e);
                },
                complete: function () {
                    that.element.enable();
                    that.rebuildPagination(href);
                    that.element.trigger('paginator.ajax.complete');
                    that.element.closest('.modal-body').scrollTop(0).scrollTop(0);
                    sorts.enable();
                }
            });
        };
    }

    /**
     * Permet de redéfinir les paramètres à partir des headers d'un requète ajax
     * @param jqXHR
     */
    applyXHR(jqXHR) {
        this._headerParams(jqXHR);
        this.rebuildPagination();
    }

    /**
     * Applique les paramètres passé par entêtes
     * @param jqXHR
     * @private
     */
    _headerParams(jqXHR) {
        var headers = ['count', 'end', 'limit', 'nextPage', 'page', 'pageCount', 'perPage', 'prevPage', 'start'];
        var header;
        for (var i = 0; i < headers.length; i++) {
            header = jqXHR.getResponseHeader('X-Paginator-' + headers[i][0].toUpperCase() + headers[i].substring(1));
            if (header) {
                if (header === 'false') {
                    header = false;
                }
                if (header === 'true') {
                    header = true;
                }
                if (header === 'null') {
                    header = null;
                }
                if (AsalaeGlobal.is_numeric(header)) {
                    header = parseInt(header, 10);
                }
                this.params[headers[i]] = header;
            }
        }
    }

    /**
     * Click sur les ...
     * @return {Function}
     * @private
     */
    _ellipsisCallback() {
        var that = this;
        return function () {
            var max = parseInt($(this).closest('.pagination').find('li:not(.next) a').last().text().replace(/\s/, ''), 10),
                url = $($(this).closest('.pagination').find('li:not(.active):not(.prev):not(.ellipsis) a:not([href=""])').get(1)); // Note: le 0 n'a pas de page=1
            $('input#pagination-select-input').remove();
            $(this).css({ position: 'relative' }).append($('<input id="pagination-select-input" type="text" min="1"/>').css({ position: 'absolute', top: '-30px', left: '0', width: '3em' }).attr('max', max).bind('blur keydown', function (event) {
                var pageNum = url.text().trim(),
                    value = parseInt($(this).val(), 10);

                if (event.type === 'blur' || event.keyCode === 13 || event.keyCode === 10) {
                    if (!AsalaeGlobal.is_numeric($(this).val()) === false && value >= 1 && value <= max) {
                        that.ajax($(this).val());
                    }
                    $('input#pagination-select-input').remove();
                }
            }));
            $('#pagination-select-input').focus();
        };
    }

    /**
     * Donne la valeur d'un paramètre d'url
     * @param {string} url
     * @param {string} key
     * @return {string|undefined}
     * @private
     */
    static _getUrlParam(url, key) {
        url = AsalaeGlobal.urldecode(url);
        key = AsalaeGlobal.urldecode(key);
        key = key.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
        var regex = new RegExp('(?:(?:\\?|\\?.*&(?:amp;)?)' + key + '=)([^&]*)');
        var match = url.match(regex);
        if (match) {
            return match[1];
        } else {
            return undefined;
        }
    }

    /**
     * Défini la valeur d'un paramètre d'url
     * @param {string} url
     * @param {string} key
     * @param {string} value
     * @return {string}
     * @private
     */
    static _setUrlParam(url, key, value) {
        url = AsalaeGlobal.urldecode(url);
        key = AsalaeGlobal.urldecode(key);
        if (url.indexOf('?') === -1) {
            url += '?' + key + '=' + value;
        } else {
            let actualValue = AsalaePaginator._getUrlParam(url, key);
            if (actualValue !== undefined) {
                let escapedKey = key.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
                let regex = new RegExp('((?:\\?|\\?.*&(?:amp;)?)' + escapedKey + '=)[^&]*');
                url = url.replace(regex, '$1' + value);
            } else {
                url += '&' + key + '=' + value;
            }
        }
        return url;
    }

    /**
     * Retire un paramètre d'url
     * @param {string} url
     * @param {string} key
     * @return {string}
     * @private
     */
    static _unsetUrlParam(url, key) {
        if (!url) {
            return '';
        }
        url = AsalaeGlobal.urldecode(url);
        key = AsalaeGlobal.urldecode(key);
        let escapedKey = key.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
        var regex = new RegExp('(\\?|&(?:amp;)?)(' + escapedKey + '=[^&]*)(&(?:amp;)?|$)');
        var match = url.match(regex);

        if (url.indexOf('?') !== -1 && match) {
            if (match[3] === '') {
                url = url.replace(match[0], '');
            } else {
                url = url.replace(match[2] + match[3], '');
            }
        }
        return url;
    }

    /**
     * Reconstruit la pagination à partir de la page sélectionnée
     * @param {string} url
     */
    rebuildPagination(url = this.url) {
        var page = parseInt(AsalaePaginator._getUrlParam(url, 'page'), 10);
        if (!page) {
            page = 1;
        }
        var count = this.params.count;
        var limit = this.params.limit;
        var pageCount = Math.max(Math.ceil(this.params.count / limit), 1);
        page = Math.min(page, pageCount);

        var start = 0;
        if (count >= 1) {
            start = (page - 1) * limit + 1;
        }
        var end = start + limit - 1;
        if (count < end) {
            end = count;
        }

        var prevPage = page > 1;
        var nextPage = count > page * limit;

        // Message de résultats
        var domid = this.element.attr('id');
        $('#' + domid + '-bloc3').html($('<small class="text-muted pagination-counters"></small>').html(__("Affichage des résultats de {0} à {1} sur les<br>{2} résultats", '<i class="start">' + start + '</i>', '<i class="end">' + end + '</i>', '<i class="count">' + count + '</i>')));
        var bloc4 = $('#' + domid + '-bloc4');

        // lien précédent
        var prev = bloc4.find('li.prev').toggleClass('disabled', !prevPage).find('a');
        if (prevPage) {
            prev.attr('href', AsalaePaginator._setUrlParam(url, 'page', page - 1)).attr('title', __("Précédent")).removeAttr('onclick');
        } else {
            prev.attr('href', '#').removeAttr('title').attr('onclick', 'return false;');
        }

        // Lien suivant
        var nextLi = bloc4.find('li.next').toggleClass('disabled', !nextPage);
        var next = nextLi.find('a');
        if (nextPage) {
            next.attr('href', AsalaePaginator._setUrlParam(url, 'page', page + 1)).attr('title', __("Suivant")).removeAttr('onclick');
        } else {
            next.attr('href', '#').removeAttr('title').attr('onclick', 'return false;');
        }

        // Liens numérotés
        bloc4.find('li:not(.next):not(.prev)').remove();
        var half = parseInt(this.modulus / 2, 10);
        end = Math.max(1 + this.modulus, page + half);
        start = Math.min(pageCount - this.modulus, page - half - this.modulus % 2);
        end = Math.min(pageCount, end);
        start = Math.max(1, start);

        // premiers liens (toujours présent)
        for (i = 1; i <= this.first; i++) {
            nextLi.before(AsalaePaginator._number(i, page === i, AsalaePaginator._setUrlParam(url, 'page', i)));
            if (start === i) {
                start++;
            }
        }
        // séparateur
        if (i + 1 <= start) {
            nextLi.before(AsalaePaginator._ellipsis());
        }
        // liens précédent la page active
        for (i = start; i < page; i++) {
            nextLi.before(AsalaePaginator._number(i, false, AsalaePaginator._setUrlParam(url, 'page', i)));
        }
        // page active
        if (start <= page) {
            nextLi.before(AsalaePaginator._number(page, true, ''));
        }
        // page suivant la page active
        for (i = page + 1; i <= end; i++) {
            nextLi.before(AsalaePaginator._number(i, false, AsalaePaginator._setUrlParam(url, 'page', i)));
        }
        // séparateur
        if (i + 1 <= pageCount - this.last) {
            nextLi.before(AsalaePaginator._ellipsis());
        } else if (i <= pageCount - this.last) {
            nextLi.before(AsalaePaginator._number(i, false, AsalaePaginator._setUrlParam(url, 'page', i)));
        }
        // derniers liens (toujours présent)
        for (var i = pageCount - this.last + 1; i <= pageCount && i > end; i++) {
            nextLi.before(AsalaePaginator._number(i, false, AsalaePaginator._setUrlParam(url, 'page', i)));
        }

        this._listenClicks();
    }

    /**
     * Donne le <li> d'un numero de pagination
     * @param {int} num page
     * @param {boolean} active
     * @param {string} url
     * @return {jQuery}
     * @private
     */
    static _number(num, active, url) {
        var li = $('<li></li>');
        var a = $('<a></a>').attr('data-mode', 'section-ajax').attr('title', __("Page: {0}", num)).text(num);
        if (!active) {
            a.attr('href', url);
        } else {
            li.addClass('active');
        }
        li.append(a);
        return li;
    }

    /**
     * Donne le bouton "…"
     * @return {jQuery}
     * @private
     */
    static _ellipsis() {
        return $('<li class="ellipsis"></li>').append($('<button class="btn-link" type="button">…</button>'));
    }
}

//# sourceMappingURL=asalae.paginator-compiled.js.map