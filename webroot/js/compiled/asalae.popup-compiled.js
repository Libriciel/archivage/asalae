/* globals $ */

/**
 * Affiche une popup
 */
class AsalaePopup {
    constructor(parentElement = $('body')) {
        var tagname = 'form';
        if (parentElement.is('form') || parentElement.closest('form').length) {
            tagname = 'div';
        }
        this.form = $('<' + tagname + ' class="generic-popup"></' + tagname + '>');
        let closeBtn = $('<button type="button" class="btn btn-link close">×</button>').attr('title', __("Fermer")).attr('aria-label', __("Fermer"));
        this.form.append(closeBtn);
        parentElement.css('position', 'relative');
        this.parentElement = parentElement;
    }

    element() {
        return this.form;
    }

    parent() {
        return this.parentElement;
    }

    show() {
        var visibles = this.parentElement.find('.generic-popup:visible');
        if (visibles.length) {
            let that = this;
            visibles.effect('drop', {}, 200, function () {
                $(this).remove();
                that.show();
            });
            return;
        }

        this.parentElement.prepend(this.form);
        this.form.find('button.close').click(AsalaePopup.closeBtnHandle);
        let css;
        let modal = this.parentElement.closest('.modal');
        if (modal.length) {
            this.form.css({ position: 'fixed' });
            var bounds = this.parentElement.get(0).getBoundingClientRect();
            var offset = {
                left: bounds.x,
                top: bounds.y
            };
            var top = offset.top - this.form.height() - 30;
            var left = offset.left;
            if (top + this.form.outerHeight() > $(window).height()) {
                top = $(window).height() - this.form.outerHeight();
            }
            if (top < 0) {
                top = 0;
            }
            if (left + this.form.outerWidth() > $(window).width()) {
                left = $(window).width() - this.form.outerWidth();
            }
            if (left < 0) {
                left = 0;
            }
            css = {
                top: top,
                left: left,
                "z-index": 100
            };
        } else {
            css = {
                position: 'absolute'
            };
        }
        this.form.css(css);
        this.form.find('input, select, textarea').first().focus();
        setTimeout(() => {
            $(document).off('click.generic-popup').on('click.generic-popup', e => {
                if (this.form.get(0) !== e.target && this.form.find(e.target).length === 0 && $(document).find(e.target).length) {
                    this.hide();
                }
            });
        }, 0);
    }

    hide() {
        if (this.form.is(':visible')) {
            this.form.effect('drop', {}, 400, function () {
                $(this).remove().css('display', '');
            });
        }
        $(document).off('click.generic-popup');
    }

    static closeBtnHandle() {
        $(this).closest('.generic-popup').effect('drop', {}, 400, function () {
            $(this).remove().css('display', '');
        });
    }
}

//# sourceMappingURL=asalae.popup-compiled.js.map