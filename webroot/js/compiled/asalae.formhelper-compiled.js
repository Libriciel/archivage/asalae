/**
 * Permet d'obtenir la même structure qu'avec le helper de cakephp FormHelper
 * Nécéssite de charger l'element jsFormTemplates au préalable
 */
class Form {
    /**
     * Créer un input
     *
     * @param {string} name
     * @param {object} params
     * @returns {jQuery}
     */
    static input(name, params) {
        var tmpl = $.templates("#jsFormTemplateInput"),
            element;
        Form._checkPrerequises();
        params = $.extend(true, {
            label: name,
            id: name,
            name: name,
            class: '',
            type: 'text',
            fullname: '',
            placeholder: '',
            help: '',
            pattern: '',
            min: '',
            max: '',
            required: '',
            value: undefined
        }, params);
        params.input = name;

        if (params.type === 'select') {
            return Form.select(name, params);
        }

        element = $(tmpl.render(params));
        var input = element.find('input');
        if (params.value !== undefined) {
            input.val(params.value);
        }
        for (var key in params) {
            if (!params[key]) {
                if (key === 'fullname') {
                    key = "data-" + key;
                }
                if (key === 'help') {
                    element.find('p.help-block').remove();
                } else if (!input.attr(key)) {
                    input.removeAttr(key);
                }
            }
        }
        return element;
    }

    /**
     * Créer un select
     *
     * @param {string} name
     * @param {object} params
     * @returns {jQuery}
     */
    static select(name, params) {
        var tmpl = $.templates("#jsFormTemplateSelect" + (params['multiple'] ? 'Multiple' : '')),
            optionTmpl = $.templates("#jsFormTemplateOption"),
            optgroupTmpl = $.templates("#jsFormTemplateOptGroup"),
            dom,
            select,
            key,
            k,
            optionParams,
            optgroup;
        Form._checkPrerequises();
        params = $.extend(true, {
            label: name,
            id: name,
            name: name,
            class: '',
            fullname: '',
            help: '',
            required: '',
            options: {},
            value: undefined
        }, params);
        params.input = name;
        dom = $(tmpl.render(params));
        select = dom.find('select');
        for (key in params.options) {
            if (!params.options.hasOwnProperty(key)) {
                continue;
            }

            optionParams = {
                value: key,
                traduction: params.options[key]
            };

            if (typeof params.options[key] === 'object') {
                optgroup = $(optgroupTmpl.render({ label: key }));
                for (k in params.options[key]) {
                    if (!params.options.hasOwnProperty(key)) {
                        continue;
                    }

                    optionParams = {
                        value: k,
                        traduction: params.options[key][k]
                    };
                    optgroup.append(optionTmpl.render(optionParams));
                }
                select.append(optgroup);
            } else {
                select.append(optionTmpl.render(optionParams));
            }
        }
        if (params.value !== undefined) {
            select.val(params.value);
        }
        for (key in params) {
            if (!params[key]) {
                if (key === 'fullname') {
                    key = "data-" + key;
                }
                if (key === 'help') {
                    dom.find('p.help-block').remove();
                } else if (!select.attr(key)) {
                    select.removeAttr(key);
                }
            }
        }
        return dom;
    }

    /**
     * Créer un fieldset avec une croix de fermeture et une barre de réduction.
     *
     * @param {string} legend
     * @returns {jQuery}
     */
    static closableFieldset(legend) {
        var tmpl = $.templates("#jsFormTemplateClosableFieldset"),
            fieldset = $(tmpl.render({ legend: legend }));
        Form._checkPrerequises();
        fieldset.find('.fa-times').click(function () {
            fieldset.slideUp(400, function () {
                $(this).remove();
            });
        });
        fieldset.find('.fa-window-minimize').click(function () {
            fieldset.find('.minifiable-target').slideToggle(400, function () {
                $(window).trigger('resize');
            });
            $(this).toggleClass('fa-window-minimize').toggleClass('fa-window-restore');
        });
        return fieldset;
    }

    /**
     * Vérifi que le template a bien été chargé
     */
    static _checkPrerequises() {
        if ($("#jsFormTemplateInput").length === 0) {
            throw "jsFormTemplates n'a pas été chargé !";
        }
    }

    /**
     * Génère un fil d'ariane (breadcrumb)
     *
     * @param {Array} units [{href: '', title: ''}, ...]
     * @returns {jQuery}
     */
    static ariane(units) {
        var container = $('<ol class="breadcrumb"></ol>');
        if (!Array.isArray(units)) {
            throw "Cette fonction attend un tableau en paramètre";
        }
        for (let i = 0; i < units.length - 1; i++) {
            let params = $.extend(true, {
                href: "javascript:",
                role: "button"
            }, units[i]),
                paramString = '',
                label = typeof params.label === 'string' ? params.label : params.title;
            for (let key in params) {
                if (key !== 'label') {
                    paramString += " " + key + '="' + params[key] + '"';
                }
            }
            container.append($('<li></li>').append('<a' + paramString + '>' + params.label + '</a>'));
        }
        container.append($('<li class="active"></li>').append(units[units.length - 1].label));
        return container;
    }

    /**
     * Créer un textarea
     *
     * @param {string} name
     * @param {object} params
     * @returns {jQuery}
     */
    static textarea(name, params) {
        var tmpl = $.templates("#jsFormTemplateTextarea"),
            element;
        Form._checkPrerequises();
        params = $.extend(true, {
            label: name,
            id: name,
            name: name,
            class: '',
            type: 'textarea',
            help: '',
            required: '',
            fullname: '',
            value: undefined
        }, params);
        params.input = name;

        if (params.type === 'select') {
            return Form.select(name, params);
        }

        element = $(tmpl.render(params));
        var textarea = element.find('textarea');
        if (params.value !== undefined) {
            textarea.val(params.value);
        }
        for (var key in params) {
            if (!params[key]) {
                if (key === 'fullname') {
                    key = "data-" + key;
                }
                if (key === 'help') {
                    element.find('p.help-block').remove();
                } else if (!textarea.attr(key)) {
                    textarea.removeAttr(key);
                }
            }
        }
        return element;
    }

    /**
     * Créer un input
     *
     * @param {object} params
     * @returns {jQuery}
     */
    static text(params) {
        return Form.input(params.name, params);
    }
}

if (typeof module !== 'undefined') {
    module.exports = {
        Form: Form
    };
}

//# sourceMappingURL=asalae.formhelper-compiled.js.map