/*global Flow, TableHelper, TableGenerator*/

/**
 * Permet de faire le pont entre asalae2 et Flow.js
 * Est utilisé par \App\View\Helper\UploadHelper
 *
 * @category    Helper
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

if (typeof __ === 'undefined') {
    var __ = function (str) {
        return str;
    };
}

/**
 * Une nouvelle instance de cette classe permet une nouvelle instance de Flow
 * Selon "config", l'objet sera couplé avec une "dropbox" et remplira une table.
 */
class AsalaeUploader {
    constructor(config) {
        var that = this;
        this.dropbox = $('#' + config.id + '.dropbox');
        this.config = config;
        this.uploader = new Flow({
            target: config.target,
            chunkSize: config.chunkSize,
            simultaneousUploads: config.simultaneousUploads,
            testChunks: true,
            headers: {
                "X-Requested-With": "XMLHttpRequest",
                "X-Asalae-Upload": "true"
            },
            xhr: function (file) {
                var xhr = new XMLHttpRequest();
                xhr.addEventListener("readystatechange", that._readystatechangeHandler(), false);
                return xhr;
            }
        });
        this.uploader.config = config;
        this.uploader.asalae = this;
        this.autoretry = config.autoretry;
        this.uploader.autoretry = config.autoretry;

        if (!this.uploader.support) {
            this.dropbox.css('background-color', '#f2dede').find('*').attr('disabled', 'disabled');
            this.dropbox.find('p').text(__("Votre navigateur ne supporte pas la fragmentation de fichiers, merci de le mettre à jour."));
            this.dropbox.find('label').text(__("Ou contactez un administrateur."));
            return;
        }

        this.uploader.assignDrop(this.dropbox.get(0));

        this.setBrowseFile();

        let events = ['fileAdded', 'filesSubmitted', 'complete', 'fileSuccess', 'fileError', 'fileProgress', 'uploadStart', 'catchAll'];

        for (let key in events) {
            // noinspection JSCheckFunctionSignatures
            this.uploader.on(events[key], eval('this.' + events[key]));
        }

        var input = this.dropbox.find('input[type="file"]').css({ visibility: 'visible', position: 'absolute', width: 0, height: 0 }).on('change', () => this._triggerChange(this.dropbox));

        var label = $('<label class="sr-only"></label>').text(__("Fichier"));
        label.insertBefore(input);
        label.append(input);

        if (typeof AsalaeUploader.instance === 'undefined') {
            AsalaeUploader.instance = [];
        }
        let i = 0;
        do {
            this.instanceIndex = i;
            i++;
        } while (typeof AsalaeUploader.instance[this.instanceIndex] !== 'undefined');
        AsalaeUploader.instance[this.instanceIndex] = this;
        AsalaeUploader.lastInstance = this;
        $(this.dropbox).attr('data-uploader-uid', this.instanceIndex);

        if (typeof config.table === 'string') {
            var table = window[this.config.table];
            table.uploader = this.uploader;
        }
        setTimeout(() => input.change(), 10);
    }

    _triggerChange(dropbox) {
        var required = dropbox.siblings('table').find('input:checked').length === 0;
        dropbox.siblings('.required-anchor').prop('required', required);
    }

    fileAdded(file) {
        var dropbox = $('#' + this.config.id + '.dropbox');
        var uploader = AsalaeUploader.instance[dropbox.attr('data-uploader-uid')];
        dropbox.siblings('table').off('change.deleted').on('change.deleted', () => uploader._triggerChange(dropbox)).removeClass('dragover').trigger('fileAdded.uploader', [file]);
        uploader.modalUploadingInc();
    }

    filesSubmitted(file) {
        var progressBar,
            pause,
            resume,
            cancel,
            prev,
            retry,
            table = eval(this.config.table);
        var that = this;
        var object;

        $('#' + this.config.id + '.dropbox').trigger('filesSubmitted', [file]);

        for (let i = 0; i < file.length; i++) {
            AsalaeUploader[file[i].uniqueIdentifier] = {
                seconds: 0.0,
                lasttimestamp: performance.now(),
                progresses: [],
                paused: false,
                duration: $('<span class="duration"></span>'),
                speed: $('<span class="speed"></span>'),
                estimated: $('<span class="estimated"></span>')
            };
            AsalaeUploader[file[i].uniqueIdentifier].interval = setInterval(function () {
                var newTime = performance.now();
                var difftime = newTime - AsalaeUploader[file[i].uniqueIdentifier].lasttimestamp;
                var seconds = AsalaeUploader[file[i].uniqueIdentifier].seconds + difftime / 1000;
                AsalaeUploader[file[i].uniqueIdentifier].lasttimestamp = newTime;
                if (AsalaeUploader[file[i].uniqueIdentifier].paused) {
                    return;
                }
                AsalaeUploader[file[i].uniqueIdentifier].seconds = seconds;
                // garde en mémoire les transferts des 10 dernières secondes
                AsalaeUploader[file[i].uniqueIdentifier].progresses = AsalaeUploader[file[i].uniqueIdentifier].progresses.filter(v => v + 10000 > newTime);
                var secs = (newTime - AsalaeUploader[file[i].uniqueIdentifier].progresses[0]) / 1000;
                var rate = AsalaeUploader[file[i].uniqueIdentifier].progresses.length / secs * file[i].flowObj.config.chunkSize;
                var transferred = file[i]._prevUploadedSize;
                var toUpload = file[i].size - transferred;
                var estimation = toUpload / (transferred / AsalaeUploader[file[i].uniqueIdentifier].seconds);
                var date = moment().startOf('day').seconds(Math.round(estimation));
                var s = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'];
                var e = Math.floor(Math.log(rate) / Math.log(1024));
                AsalaeUploader[file[i].uniqueIdentifier].duration.text(moment().startOf('day').seconds(seconds).format('HH:mm:ss'));
                AsalaeUploader[file[i].uniqueIdentifier].speed.text(Number.isFinite(e) ? (rate / Math.pow(1024, e)).toFixed(2) + " " + s[e] + "/s" : '0/s');
                if (toUpload >= 0) {
                    if (date.hour()) {
                        AsalaeUploader[file[i].uniqueIdentifier].estimated.text(date.hour() + 'h ' + date.minute() + 'm');
                    } else if (date.minute()) {
                        AsalaeUploader[file[i].uniqueIdentifier].estimated.text(date.minute() + 'm ' + date.second() + 's');
                    } else if (date.second()) {
                        AsalaeUploader[file[i].uniqueIdentifier].estimated.text(date.second() + 's');
                    } else {
                        AsalaeUploader[file[i].uniqueIdentifier].estimated.text(__("Finalisation..."));
                    }
                } else {
                    AsalaeUploader[file[i].uniqueIdentifier].estimated.text(__("Finalisation..."));
                }
            }, 500);

            AsalaeUploader[file[i].uniqueIdentifier].resume = function (element) {
                $(element).parent().parent().find('.progress.uploading .progress-bar').removeClass('progress-bar-warning');
                file[i].resume();
                $(element).hide();
                $(element).parent().find('button.pause').show();
                AsalaeUploader[file[i].uniqueIdentifier].paused = false;
            };
            AsalaeUploader[file[i].uniqueIdentifier].cancel = function (element, dec = true) {
                if (dec) {
                    that.asalae.modalUploadingDec();
                }
                file[i].cancel();
                table.removeDataId($(element).attr('data-target'));
                $(element).closest('tr').fadeOut(200, function () {
                    $(this).remove();
                    $(table.table).trigger('change');
                });
                clearInterval(this.interval);
            };
            AsalaeUploader[file[i].uniqueIdentifier].pause = function (element) {
                $(element).parent().parent().find('.progress.uploading .progress-bar').addClass('progress-bar-warning');
                file[i].pause();
                $(element).hide();
                $(element).parent().find('button.resume').show();
                AsalaeUploader[file[i].uniqueIdentifier].paused = true;
            };
            AsalaeUploader[file[i].uniqueIdentifier].retry = function (element) {
                that.asalae.modalUploadingInc();
                var tr = $('tr[data-id="' + file[i].uniqueIdentifier + '"]');
                tr.find('.uploading-message').show();
                tr.find('.error-message').remove();
                tr.find('.retry-message').remove();
                tr.find('.flow-file-progress .progress-bar').removeClass('progress-bar-danger');
                tr.find('button.pause, input').show();
                tr.find('button.retry').hide();
                AsalaeUploader[file[i].uniqueIdentifier].paused = false;
                AsalaeUploader[file[i].uniqueIdentifier].seconds = 0.0;
                AsalaeUploader[file[i].uniqueIdentifier].lasttimestamp = performance.now();
                AsalaeUploader[file[i].uniqueIdentifier].progresses = [];
                AsalaeUploader[file[i].uniqueIdentifier].paused = false;
                file[i].retry();
            };
            AsalaeUploader[file[i].uniqueIdentifier].message = $('<div></div>').addClass('uploading-message').append(AsalaeUploader[file[i].uniqueIdentifier].duration).append(AsalaeUploader[file[i].uniqueIdentifier].speed).append(AsalaeUploader[file[i].uniqueIdentifier].estimated);
            progressBar = $('<div></div>').append($('<div></div>').addClass('flow-file-progress').addClass('progress').addClass('uploading').append($('<div></div>').addClass('progress-bar').attr('role', 'progressbar').attr('aria-valuenow', '0').attr('aria-valuemin', '0').attr('aria-valuemax', '100').css('width', '0%'))).append(AsalaeUploader[file[i].uniqueIdentifier].message).append($('<span></span>').addClass('text-danger').addClass('error-span'));
            let pauseTitle = __("Mettre l'upload en pause");
            pause = $('<button></button>').addClass('btn-link').addClass('pause').attr('type', 'button').attr('onclick', "AsalaeUploader['" + file[i].uniqueIdentifier + "'].pause(this)").attr('title', pauseTitle).attr('aria-label', pauseTitle).html('<i class="fa fa-pause" aria-hidden="true"></i>');
            let resumeTitle = __("Reprise de l'upload");
            resume = $('<button></button>').addClass('btn-link').addClass('resume').hide().attr('type', 'button').attr('onclick', "AsalaeUploader['" + file[i].uniqueIdentifier + "'].resume(this)").attr('title', resumeTitle).attr('aria-label', resumeTitle).html('<i class="fa fa-play" aria-hidden="true"></i>');
            let cancelTitle = __("Annuler l'upload");
            cancel = $('<button></button>').addClass('btn-link').addClass('cancel').attr('type', 'button').attr('data-target', file[i].uniqueIdentifier).attr('onclick', "AsalaeUploader['" + file[i].uniqueIdentifier + "'].cancel(this)").attr('title', cancelTitle).attr('aria-label', cancelTitle).html('<i class="fa fa-times-circle" aria-hidden="true"></i>');
            let retryTitle = __("Relancer l'upload");
            retry = $('<button></button>').addClass('btn-link').addClass('retry').hide().attr('type', 'button').attr('data-target', file[i].uniqueIdentifier).attr('onclick', "AsalaeUploader['" + file[i].uniqueIdentifier + "'].retry(this)").attr('title', retryTitle).attr('aria-label', retryTitle).html('<i class="fa fa-redo" aria-hidden="true"></i>');

            table.data.unshift({
                id: file[i].uniqueIdentifier,
                prev: null,
                filename: file[i].name,
                size: file[i].size,
                message: progressBar,
                valid: true,
                actions: {
                    action_0: $('<div></div>').append(pause).append(resume).append(cancel).append(retry)
                }
            });
            table.generateTbody();

            $('#table-' + table.instanceIndex + '-tr-' + file[i].uniqueIdentifier + ' .td-checkbox input').remove();
            $(table.table).find('.progress-bar-danger').closest('tr').find('input').remove();
            $(table.table).find('.progress-bar-warning').closest('tr').find('input').remove();
        }
        this.resume();
    }

    complete() {
        $('#' + this.config.id + '.dropbox').trigger('complete');
    }

    modalUploadingInc() {
        var modal = this.dropbox.closest('.modal');
        if (modal.length === 1) {
            if (modal.attr('data-uploading-count') === undefined) {
                modal.attr('data-uploading-count', '0');
            }
            modal.attr('data-uploading-count', '' + (parseInt(modal.attr('data-uploading-count'), 10) + 1));
            if (!modal.hasClass('uploading')) {
                modal.addClass('uploading');
                modal.find('.modal-footer button').each(function () {
                    var isDisabled = $(this).prop('disabled') ? 'true' : 'false';
                    $(this).attr('data-disabled', isDisabled).disable();
                });
            }
        }
    }

    modalUploadingDec() {
        var modal = this.dropbox.closest('.modal');
        if (modal.length === 1) {
            if (modal.attr('data-uploading-count') === undefined) {
                modal.attr('data-uploading-count', '0');
            }
            var currentCount = Math.max(0, parseInt(modal.attr('data-uploading-count'), 10) - 1);
            modal.attr('data-uploading-count', '' + currentCount);
            if (currentCount === 0) {
                modal.find('.modal-footer button').each(function () {
                    $(this).disable($(this).attr('data-disabled') === 'true');
                });
                modal.removeClass('uploading');
            }
        }
    }

    fileSuccess(file, message, chunk) {
        this.asalae.modalUploadingDec();

        if (typeof message === 'string') {
            try {
                message = JSON.parse(message);
            } catch (e) {
                console.error("can't JSON.parse message: " + message);
                throw e;
            }
        }

        if (!message.report.completion || message.report.error) {
            return this.asalae.fileError(file, message, chunk);
        }
        $('#' + this.config.id + '.dropbox').trigger('fileSuccess', [file, message]);
        var table = eval(this.config.table);
        var data = table.getDataId(file.uniqueIdentifier);
        if (!data) {
            console.error('data not found');
            return;
        }
        var tr = table.table.find('tr[data-id="' + file.uniqueIdentifier + '"]');
        tr.find('.flow-file-progress .progress-bar').removeClass('progress-bar-warning').removeClass('progress-bar-danger').addClass('progress-bar-success').html('<i class="fa fa-check" aria-hidden="true"></i> ' + __("Terminé"));
        tr.find('.uploading-message, .error-message, .retry-message').remove();
        tr.find('input').enable();
        message.report.message = $(tr.find('td.message').html());
        var uploadMsg = tr.find('.uploading-message');
        clearInterval(AsalaeUploader[file.uniqueIdentifier].interval);
        uploadMsg.remove();

        delete data.actions;
        for (var key in message.report) {
            data[key] = message.report[key];
        }
        table.replaceDataId(file.uniqueIdentifier, TableGenerator.appendActions([data], table.actions)[0]);
        table.generateAll();
        $(table.table).trigger('upload.success', { file: file, message: message });
    }

    fileError(file, message, chunk) {
        var table = eval(this.config.table);
        if (typeof message === 'string') {
            try {
                message = JSON.parse(message);
            } catch (e) {
                message = { report: { error: __("Une erreur inattendue a eu lieu") } };
            }
        }
        $('#' + this.config.id + '.dropbox').trigger('fileError', [file, message]);
        var tr = $('tr[data-id="' + file.uniqueIdentifier + '"]');
        var uploadMsg = tr.find('.uploading-message');
        AsalaeUploader[file.uniqueIdentifier].paused = true;
        uploadMsg.hide();

        tr.find('.error-message').remove();
        tr.find('.retry-message').remove();
        var progressBar = tr.find('.flow-file-progress .progress-bar').addClass('progress-bar-danger').closest('td').append($('<span class="error-message"></span>').text(message.report.error));
        tr.find('button.pause, input').hide();
        tr.find('button.retry').show();
        var data = table.getDataId(file.uniqueIdentifier);
        data.message = $(tr.find('td.message').html());
        $(table.table).trigger('upload.failed', { file: file, message: message });

        if (chunk.xhr.getResponseHeader('X-Asalae-No-Retry') === null && table.table.is(':visible') && this.autoretry !== false && chunk.xhr.status !== 0 && chunk.xhr.status >= 400) {
            progressBar.append($('<div class="retry-message"></div>').text(__("Nouvelle tentative dans {0} secondes", 5)));
            setTimeout(function () {
                AsalaeUploader[file.uniqueIdentifier].retry();
            }, 5000);
        }
    }

    fileProgress(file) {
        var table = eval(this.config.table),
            percent = Math.floor(file.progress() * 100);
        $('#' + this.config.id + '.dropbox').trigger('fileProgress', [file]);
        var tr = $('tr[data-id="' + file.uniqueIdentifier + '"]');
        tr.find('.flow-file-progress .progress-bar').css('width', percent + '%').attr('aria-valuenow', percent).html(percent + '% ');
        var msgdiv = tr.find('.uploading-message');
        var duration = msgdiv.find('.duration');

        var last = msgdiv.find('.speed');
        if (typeof file.flowObj !== 'undefined') {
            AsalaeUploader[file.uniqueIdentifier].progresses.push(performance.now());
        }
    }

    uploadStart() {
        $('#' + this.config.id + '.dropbox').trigger('uploadStart');
    }

    catchAll(eventName, data) {
        $(document).trigger(eventName + '.AsalaeUploader', { data: data, instance: this });
    }

    _readystatechangeHandler() {
        var UploadObject = this;
        return function () {
            if (this.status >= 500) {
                return;
            }
            if (this.readyState === 3 && !/completion[": ]+false/.test(this.response)) {
                // === LOADING
                var response = this.response;
                try {
                    response = JSON.parse(response.substr(0, response.length - 2) + '}}');
                } catch (e) {
                    return;
                }

                response.report.id = response.report.uid;
                var file = {
                    uniqueIdentifier: response.report.uid,
                    progress: function () {
                        return 1;
                    }
                };
                UploadObject.fileProgress(file);

                var table = eval(UploadObject.config.table);
                $('#table-' + table.instanceIndex + '-tr-' + file.uniqueIdentifier + ' .flow-file-progress .progress-bar').addClass('progress-bar-warning').html(__("Validation...") + ' <i class="fa fa-spinner faa-spin animated" aria-hidden="true"></i>').closest('tr').find('input').disable();
                clearInterval(AsalaeUploader[file.uniqueIdentifier].interval);
                AsalaeUploader[file.uniqueIdentifier].message.text(__("Validation du fichier en cours, veuillez patienter"));
            }
        };
    }

    setBrowseFile() {
        let isDirectory = typeof this.config.isDirectory === 'undefined' ? false : this.config.isDirectory;
        let singleFile = typeof this.config.singleFile === 'undefined' ? false : this.config.singleFile;
        let attributes = typeof this.config.attributes === 'undefined' ? {} : this.config.attributes;
        let button = this.dropbox.find('div.btn').off('.upload').on('keydown', function (event) {
            if (event.originalEvent.keyCode === 13) {
                $(this).click();
            }
        });
        this.uploader.assignBrowse(button.get(0), isDirectory, singleFile, attributes);
    }

    /**
     * Création d'un champ d'upload
     * @param {string} label
     * @param {object} params
     */
    static createInput(label, params = {}) {
        var inputDiv = $('<div class="form-group fake-input">');
        var spanLabel = $('<span class="fake-label">').text(label);
        inputDiv.append(spanLabel);

        var dropboxId = params.dropboxId ? params.dropboxId : 'dropbox-' + Date.now();
        var tableId = params.tableId ? params.tableId : 'table-' + Date.now();
        var button = $('<div class="btn btn-success" tabindex="0">').append($('<i class="fas fa-folder-open fa-space" aria-hidden="true">')).append(__("Parcourir..."));
        var dropbox = $('<div class="dropbox">').attr('id', dropboxId).append($('<i class="fa fa-upload fa-3x text-primary">')).append($('<p>').text(__("Glissez-déposez vos fichiers ici"))).append(button);
        inputDiv.append(dropbox);
        var table = AsalaeUploader.createTable(params);
        inputDiv.append(table);
        if (params.inputRequired) {
            var selectionName = params.selectionName ? params.selectionName : 'fileupload_id';
            var requiredInput = $('<input class="required-anchor">').attr('name', selectionName).attr('type', 'radio').attr('style', 'transform : scale(0,0);').attr('tabindex', '-1').attr('aria-hidden', 'true').prop('required', true);
            inputDiv.append(requiredInput);
        }
        return inputDiv;
    }

    /**
     * Monte les évenements d'un input
     * @param {jquery} inputDiv
     * @param {object} params
     */
    static inputScripts(inputDiv, params = {}) {
        params.id = $(inputDiv).find('.dropbox[id]').attr('id');
        params.table = $(inputDiv).find('table[data-for]').attr('data-for');
        if (!params.target) {
            params.target = '/upload';
        }
        if (!params.chunkSize) {
            params.chunkSize = 1024 * 1024;
        }
        if (!params.simultaneousUploads) {
            params.simultaneousUploads = 3;
        }
        new GenericUploader(new AsalaeUploader(params));
    }

    /**
     * Création d'un champ d'upload puis insertion avant target
     * puis application des scripts
     * @param {string} label
     * @param {jquery} target
     * @param {object} params
     */
    static createAndInsertBefore(label, target, params = {}) {
        var divInput = AsalaeUploader.createInput(label, params);
        divInput.insertBefore(target);
        AsalaeUploader.inputScripts(divInput, params);
    }

    /**
     * Création d'un champ d'upload puis insertion avant target
     * puis application des scripts
     * @param {string} label
     * @param {jquery} target
     * @param {object} params
     */
    static createAndInsertAfter(label, target, params = {}) {
        var divInput = AsalaeUploader.createInput(label, params);
        divInput.insertAfter(target);
        AsalaeUploader.inputScripts(divInput, params);
    }

    /**
     * Création d'un champ d'upload puis insertion avant target
     * puis application des scripts
     * @param {string} label
     * @param {jquery} target
     * @param {object} params
     */
    static createAndInsertInto(label, target, params = {}) {
        var divInput = AsalaeUploader.createInput(label, params);
        $(target).append(divInput);
        AsalaeUploader.inputScripts(divInput, params);
    }

    /**
     * Callback des uploads sélectionnables
     * @param {string} name
     * @param {string} type
     * @returns {*|jQuery}
     */
    static selectableCallback(name, type) {
        return function (value, context) {
            return $('<input>').val(context.id).attr('name', name).attr('aria-label', context.name).attr('type', type).prop('checked', true);
        };
    }

    /**
     * Création de la liste d'upload
     * @param {object} params
     */
    static createTable(params = {}) {
        TableHelper.locale(params.locale ? params.locale : 'fr');
        var table = $('<table class="table table-striped table-hover">');
        var tableVar = params.tableVar ? params.tableVar : 'generatedTable' + Date.now();
        table.attr('data-for', tableVar);
        var tableObject = new TableGenerator(table);
        window[tableVar] = tableObject;
        tableObject.data = params.data ? params.data : [];
        var deleteTitle = params.deleteTitle ? params.deleteTitle : __("Supprimer {0}", '{1}');
        var deleteUrl = params.deleteUrl ? params.deleteUrl : '/upload/delete';
        tableObject.actions = {
            "action_0": {
                "onclick": params.deleteOnclick ? params.deleteOnclick : "GenericUploader.fileDelete(%s, '%s', {0})".format(tableVar, deleteUrl),
                "type": params.deleteType ? params.deleteType : "button",
                "class": params.deleteClass ? params.deleteClass : "btn-link delete",
                "displayEval": params.deleteDisplayEval ? params.deleteDisplayEval : "AsalaeGlobal.is_numeric(data[{index}].id)",
                "label": params.deleteLabel ? params.deleteLabel : "<i class=\"fas fa-trash text-danger\" aria-hidden=\"true\"><\/i>",
                "title": deleteTitle,
                "aria-label": deleteTitle,
                "params": ["id", "name"]
            }
        };
        TableGenerator.appendActions(tableObject.data, tableObject.actions);
        var tableParams = {
            thead: [[]],
            tbody: {
                "identifier": "id",
                "classEval": "data[{index}].valid !== false ? \"\" : \"danger\""
            }
        };
        var selectionName = params.selectionName ? params.selectionName : 'fileupload_id';
        var selectionType = params.selectionType ? params.selectionType : 'radio';
        if (selectionType === 'checkbox') {
            selectionName = selectionName + '[]';
        }
        tableParams.thead[0].push({
            "_id": "selection",
            "label": __("Sélection"),
            "title": "",
            "link": false,
            "type": "th",
            "data-view": "default",
            "display": true,
            "displayEval": null,
            "colspan": 1,
            "target": "selection",
            "thead": null,
            "callback": "AsalaeUploader.selectableCallback('%s', '%s')".format(selectionName, selectionType),
            "style": null,
            "class": "selection",
            "filter": null
        });
        tableParams.thead[0].push({
            "_id": "name",
            "label": __("Nom de fichier"),
            "title": "",
            "link": false,
            "type": "th",
            "data-view": "default",
            "display": true,
            "displayEval": null,
            "colspan": 1,
            "target": "name",
            "thead": null,
            "callback": "TableHelper.filenameToPopup",
            "style": null,
            "class": null,
            "filter": null
        });
        tableParams.thead[0].push({
            "_id": "message",
            "label": __("Message"),
            "title": "",
            "link": false,
            "type": "th",
            "data-view": "default",
            "display": true,
            "displayEval": null,
            "colspan": 1,
            "target": "message",
            "thead": null,
            "callback": null,
            "style": "min-width: 200px; max-width: 400px",
            "class": "message",
            "filter": null
        });
        tableParams.thead[0].push({
            "_id": "actions",
            "label": __("Actions"),
            "title": "",
            "link": false,
            "type": "th",
            "data-view": "column",
            "display": true,
            "colspan": 1,
            "target": "actions",
            "thead": {
                "action_0": {
                    "_id": "action_0",
                    "label": false,
                    "title": deleteTitle,
                    "link": false,
                    "type": "th",
                    "data-view": "default",
                    "display": true,
                    "colspan": 1,
                    "target": "action_0",
                    "thead": null,
                    "callback": null
                },
                "subactions": {
                    "_id": "subactions",
                    "type": "th",
                    "data-view": "default",
                    "target": "subactions"
                }
            },
            "callback": null,
            "class": "action"
        });
        tableObject.params(tableParams);
        tableObject.applyCookies();
        tableObject.generateAll();
        table.off('created.form').on('created.form', function (e, data) {
            var container = $(data.form).on('submit', function (e) {
                if (typeof AsalaeFilter === 'undefined') {
                    return;
                }
                e.preventDefault();
                e.stopPropagation();
                var url = "" + window.location;
                url += url.indexOf('?') !== -1 ? '&' : '?';
                var formdata = AsalaeFilter.getFiltersData(this);
                AsalaeGlobal.interceptedLinkToAjax(url + formdata);
                $(document).off('click.outside.form');
                container.toggle('drop', function () {
                    $(this).remove();
                });
            });
        });
        return table;
    }

    /**
     * Ajout un fichier à un tableau d'upload
     * @param {jquery}  inputDiv
     * @param {integer} fileupload_id
     * @param {string}  name
     */
    static addUploadDataToTable(inputDiv, fileupload_id, name) {
        var table = $(inputDiv).find('table[data-table-uid]').first();
        var tableObject = TableGenerator.instance[table.attr('data-table-uid')];
        table.removeClass('hide');
        tableObject.data.push({ id: fileupload_id, name: name });
        tableObject.generateAll();
    }
}

/**
 * Fonctionnement générique d'une dropbox d'upload
 */
var genericConfiguration = {
    tableGenerator: undefined,
    urlDelete: undefined,
    dropBox: undefined,
    paginationElement: undefined,
    selectUrl: undefined,
    messages: {
        confirmDelete: __("Êtes-vous sûr de vouloir supprimer ce fichier ?"),
        confirmDeleteAll: __("Êtes-vous sûr de vouloir supprimer les fichiers sélectionnés ?"),
        deleteError: __("Une erreur a eu lieu lors de la suppression"),
        selectionFailed: __("Une erreur inattendue a eu lieu")
    }
};
/**
 * Initialise une dropbox avec son tableau d'affichage pour une utilisation générique
 */
class GenericUploader {
    /**
     * Constructor.
     * @param {AsalaeUploader} asalae
     */
    constructor(asalae) {
        var table = eval(asalae.uploader.config.table);

        $(table.table).change(function () {
            if ($(this).find('tbody tr').length === 0) {
                $(this).addClass('hide').closest('section').find('button.btn-primary').parent().addClass('hide');
            }
        });
        $(table.table).find('thead input.checkall').change(function () {
            $(this).closest('table').find('tbody .td-checkbox input').prop('checked', $(this).prop('checked'));
        });
        if (table.data.length) {
            $(table.table).removeClass('hide').closest('section').find('button.btn-primary').parent().removeClass('hide');
        } else {
            asalae.uploader.on('fileAdded', function () {
                $(table.table).removeClass('hide').closest('section').find('button.btn-primary').parent().removeClass('hide');
            });
        }
        $(table.table).on('change', 'input', () => asalae._triggerChange(asalae.dropbox));
    }

    /**
     * Supprime un fichier uploadé
     *
     * @param {TableGenerator} table
     * @param {string} url
     * @param {string} id
     * @param {boolean} useConfirm
     * @return {Function}
     */
    static fileDelete(table, url, id, useConfirm = true) {
        if (useConfirm && !confirm(genericConfiguration.messages.confirmDelete)) {
            return;
        }
        var tr = $(table.table).find('tr[data-id="' + id + '"]');
        $.ajax({
            url: url + '/' + id,
            method: 'DELETE',
            success: function (message) {
                if (message.entity_deleted || message.file_deleted) {
                    if (!message.entity_deleted) {
                        this.error();
                    } else if (!message.file_deleted) {
                        this.error(__("Impossible de supprimer le fichier du disque"));
                    } else {
                        message.report = 'done';
                    }
                }
                if (message.report === 'done') {
                    // permet de télécharger à nouveau le fichier
                    if (AsalaeUploader[table.getDataId(id).uid]) {
                        AsalaeUploader[table.getDataId(id).uid].cancel(tr, false);
                    }

                    table.removeDataId(id);
                    tr.fadeOut(400, function () {
                        $(this.remove());
                        $(table.table).trigger('change.deleted');
                    });
                } else {
                    this.error();
                }
            },
            error: function (e) {
                var message = genericConfiguration.messages.deleteError;
                if (e.responseText) {
                    message = e.responseText;
                } else if (typeof e === 'string' && e) {
                    message = e;
                }
                var data;
                tr.addClass('danger').find('td.message span.error-span').text(message);
                data = table.getDataId(id);
                data.message = $(tr.find('td.message').html());
                data.valid = false;
                table.replaceDataId(id, data);
            }
        });
    }

    /**
     * Modal d'informations supplémentaire sur le fichier
     *
     * @param {string} id
     */
    static loadDataModal(id) {
        var data = genericConfiguration.tableGenerator.getDataId(id),
            div = $('<div></div>'),
            schema = [{ separator: 'h3', value: 'Informations générales' }, { title: 'Nom de fichier', value: data.name }, { title: 'Taille', value: data.size, callback: TableHelper.readableBytes }],
            value;

        if (typeof data.mediainfo !== 'undefined') {
            schema = schema.concat({ separator: 'h3', value: 'Mediainfo' }, { title: 'Format de fichier', value: data.mediainfo.format }, { title: 'Durée', value: data.mediainfo.duration });

            for (let i in data.mediainfo.mediainfo_videos) {
                schema = schema.concat({ separator: 'h4', value: 'Video' }, { title: 'Format', value: data.mediainfo.mediainfo_videos[i].format }, { title: 'Débit', value: data.mediainfo.mediainfo_videos[i].bit_rate }, { title: 'Largeur', value: data.mediainfo.mediainfo_videos[i].width }, { title: 'Hauteur', value: data.mediainfo.mediainfo_videos[i].height }, { title: 'Langue', value: data.mediainfo.mediainfo_videos[i].language });
            }

            for (let i in data.mediainfo.mediainfo_audios) {
                schema = schema.concat({ separator: 'h4', value: 'Audio' }, { title: 'Format', value: data.mediainfo.mediainfo_audios[i].format }, { title: 'Débit', value: data.mediainfo.mediainfo_audios[i].bit_rate }, { title: 'Langue', value: data.mediainfo.mediainfo_audios[i].language });
            }

            for (let i in data.mediainfo.mediainfo_texts) {
                schema = schema.concat({ separator: 'h4', value: 'Sous-titres' }, { title: 'Titre', value: data.mediainfo.mediainfo_texts[i].title }, { title: 'Langue', value: data.mediainfo.mediainfo_texts[i].language });
            }

            for (let i in data.mediainfo.mediainfo_images) {
                schema = schema.concat({ separator: 'h4', value: 'Image' }, { title: 'Format', value: data.mediainfo.mediainfo_images[i].format });
            }
        }

        if (typeof data.siegfrieds !== 'undefined' && data.siegfrieds.length) {
            schema = schema.concat({ separator: 'h3', value: 'Siegfried' });

            for (let i in data.siegfrieds) {
                schema = schema.concat({ title: 'Pronom', value: data.siegfrieds[i].pronom }, { title: 'Format', value: data.siegfrieds[i].format }, { title: 'Mime', value: data.siegfrieds[i].mime });
            }
        }

        for (let i in schema) {
            value = typeof schema[i].callback !== 'undefined' ? schema[i].callback(schema[i].value) : schema[i].value;

            if (typeof schema[i].separator !== 'undefined') {
                div.append($('<' + schema[i].separator + '></' + schema[i].separator + '>').append(schema[i].value));
            } else {
                div.append($('<span></span>').addClass('field').append(schema[i].title)).append($('<span></span>').addClass('value').append(value));
            }
        }

        $('#info-file').find('.modal-body').html('').append(div);
    }

    /**
     * Ajoute un fichier selectionné
     *
     * @param {TableGenerator} table
     * @param {string} url
     * @param {int|string} id
     * @param {function} callback
     * @param {array|string} args arguments du callback
     * @return {Function}
     */
    static fileSelect(table, url, id, callback = undefined, args = undefined) {
        var trId = table.table.find('tr[data-id="' + id + '"]');
        trId.addClass('warning').find('td.message').append($('<div class="text-center"><i aria-hidden="true" class="fa fa-2x fa-spinner faa-spin animated"></i></div>'));
        var actions = trId.find('td.action a').disable();
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                fileupload_id: id
            },
            success: function (content, textStatus, jqXHR) {
                var data;
                if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                    table.removeDataId(id);
                    trId.fadeOut(400, function () {
                        $(this).remove();
                        $(table.table).trigger('change');
                    });
                    if (callback) {
                        if (Array.isArray(args)) {
                            callback.apply(null, args);
                        } else {
                            console.log(callback, args);
                            callback.call(null, args);
                        }
                    }
                } else {
                    actions.enable();
                    trId.addClass('danger').removeClass('warning').find('.fa-spinner').parent().remove();
                    if (typeof content === 'object' && typeof content.report === 'string') {
                        trId.find('td.message span.error-span').text(content.report);
                    } else {
                        trId.find('td.message span.error-span').text(genericConfiguration.messages.selectionFailed);
                    }
                    data = table.getDataId(id);
                    data.message = $(trId.find('td.message').html());
                    data.valid = false;
                    table.replaceDataId(id, data);
                }
            },
            error: function () {
                var data;
                actions.enable();
                trId.addClass('danger').removeClass('warning').find('.fa-spinner').parent().remove();
                data = table.getDataId(id);
                data.valid = false;
                table.replaceDataId(id, data);
                alert("Une erreur a eu lieu");
            }
        });
    }

    /**
     * Ajoute les fichier selectionnés (table upload)
     * @param {TableGenerator} table
     */
    static addSelected(table) {
        table.table.find('input:checked').each(function () {
            $(this).disable();
            $(this).closest('tr').find('td.action button.fileSelect').click();
        }).prop('checked', false).enable();
    }
}

if (typeof module !== 'undefined') {
    module.exports = {
        AsalaeUploader: AsalaeUploader,
        GenericUploader: GenericUploader
    };
}

//# sourceMappingURL=asalae.upload-compiled.js.map