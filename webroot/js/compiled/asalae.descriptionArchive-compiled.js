/**
 * Script de chargement de la page
 */
$(document).ready(function () {

    $('fieldset > legend').each(function () {
        $(this).closest('fieldset').addClass('expanded');
        var icon = 'fa fa-minus-square-o';
        if ($(this).closest('fieldset').hasClass('folder')) {
            icon = 'fa fa-folder-open';
        } else if ($(this).closest('fieldset').hasClass('Contains')) {
            icon = 'fa fa-folder-open';
        } else if ($(this).closest('fieldset').hasClass('Document')) {
            icon = 'fa fa-file-o';
            collapse(this, 'plier');
        } else if ($(this).closest('fieldset').hasClass('RelatedData')) {
            icon = 'fa fa-paperclip';
        } else if ($(this).closest('fieldset').hasClass('tag')) {
            icon = 'fa fa-tags';
        } else if ($(this).closest('fieldset').hasClass('Appraisal')) {
            icon = 'fa fa-calendar';
        } else if ($(this).closest('fieldset').hasClass('rules')) {
            icon = 'fa fa-gavel';
            collapse(this, 'plier');
        } else if ($(this).closest('fieldset').hasClass('Keyword')) {
            icon = 'fa fa-flag-o';
            collapse(this, 'plier');
        } else if ($(this).parent('fieldset').hasClass('ContentDescriptive')) {
            icon = 'fa fa-flag';
        } else if ($(this).parent('fieldset').hasClass('Organisation')) {
            icon = 'fa fa-sitemap';
            if (!$(this).parents('div').hasClass('acteur')) {
                collapse(this, 'plier');
            }
        } else if ($(this).closest('fieldset').hasClass('Address')) {
            icon = 'fa fa-map-marker';
            collapse(this, 'plier');
        } else if ($(this).closest('fieldset').hasClass('Contact')) {
            icon = 'fa fa-envelope-o';
            collapse(this, 'plier');
        } else if ($(this).closest('fieldset').hasClass('ContentDescription')) {
            collapse(this, 'deplier');
            icon = 'fa fa-info';
        } else if ($(this).closest('fieldset').hasClass('Communication')) {
            icon = 'fa fa-bullhorn';
            collapse(this, 'plier');
        } else if ($(this).closest('fieldset').hasClass('CustodialHistory')) {
            icon = 'fa fa-clock-o';
        } else if ($(this).closest('fieldset').hasClass('RelatedObjectReference')) {
            icon = 'fa fa-link';
        } else if ($(this).closest('fieldset').hasClass('AccessRestriction')) {
            icon = 'fa fa-unlock';
        } else if ($(this).closest('fieldset').hasClass('Integrity')) {
            icon = 'fa fa-certificate';
            $(this).attr('title', 'cliquer pour replier');
            if ($(this).next().text() === '') {
                $(this).next().hide();
            }
        }
        $(this).prepend($('<i class="' + icon + ' icone-plier"></i>'));
    }).on('click', function () {
        collapse(this);
    });

    //Lien footer (SEDA)
    $('#footer').find('a').on('click', function () {
        window.open(this.href);
        return false;
    });
});

//# sourceMappingURL=asalae.descriptionArchive-compiled.js.map