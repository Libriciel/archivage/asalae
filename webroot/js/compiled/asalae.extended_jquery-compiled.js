/**
 * Permet de couvrir tout les evenements
 * ex: $(element).onAny(function (e) { console.log(e.type); });
 * @param cb
 * @returns {$}
 */
$.fn.onAny = function (cb) {
    for (var k in this[0]) {
        if (k.search('on') === 0) {
            this.on(k.slice(2), function (e) {
                // Probably there's a better way to call a callback function with right context, $.proxy() ?
                cb.apply(this, [e]);
            });
        }
    }
    return this;
};

/**
 * Log tous les evenements d'un element
 */
$.fn.extend({
    logEvents: function (cooldown = 1000) {
        return this.each(function () {
            $(this).onAny(function (event) {
                if (typeof event.target.blockedEvent === 'undefined') {
                    event.target.blockedEvent = event.type;
                    setTimeout(function () {
                        delete event.target.blockedEvent;
                    }, cooldown);
                }
            });
        });
    }
});

/**
 * Donne la liste des attributs d'un element
 */
(function (old) {
    $.fn.attr = function () {
        if (arguments.length === 0) {
            if (this.length === 0) {
                return null;
            }

            var obj = {};
            $.each(this[0].attributes, function () {
                if (this.specified) {
                    obj[this.name] = this.value;
                }
            });
            return obj;
        }

        return old.apply(this, arguments);
    };
})($.fn.attr);

/**
 * Ajoute getEvents sur jQuery
 * @returns {Object} ex {click: [{data: ..., handler: ..., type: 'click', ...}]}
 */
jQuery.fn.getEvents = function () {
    if (typeof jQuery._data === 'function') {
        return jQuery._data(this.get(0), 'events') || {};
    } else if (typeof this.data === 'function') {
        // jQuery version < 1.7.?
        return this.data('events') || {};
    }
    return {};
};

/**
 * Permet d'activer/désactiver tout type d'elements
 */
jQuery.fn.extend({
    disable: function (cond = true) {
        if (!cond) {
            return $(this).enable();
        }
        return this.each(function () {
            $(this).addClass('disabled').on('click.disabled', function (event) {
                event.preventDefault();
            }).attr('disabled', 'disabled');
        });
    },
    enable: function (cond = true) {
        if (!cond) {
            return $(this).disable();
        }
        return this.each(function () {
            $(this).removeClass('disabled').off('click.disabled').removeAttr('disabled');
        });
    }
});

/**
 * Textarea and select clone() bug workaround | Spencer Tipping
 * Licensed under the terms of the MIT source code license
 *
 * Motivation.
 * jQuery's clone() method works in most cases, but it fails to copy the value of textareas and select elements. This patch replaces jQuery's clone() method with a wrapper that fills in the
 * values after the fact.
 *
 * An interesting error case submitted by Piotr Przybył: If two <select> options had the same value, the clone() method would select the wrong one in the cloned box. The fix, suggested by Piotr
 * and implemented here, is to use the selectedIndex property on the <select> box itself rather than relying on jQuery's value-based val().
 */
(function (original) {
    jQuery.fn.clone = function () {
        var result = original.apply(this, arguments),
            my_textareas = this.find('textarea').add(this.filter('textarea')),
            result_textareas = result.find('textarea').add(result.filter('textarea')),
            my_selects = this.find('select').add(this.filter('select')),
            result_selects = result.find('select').add(result.filter('select'));

        for (var i = 0, l = my_textareas.length; i < l; ++i) {
            $(result_textareas[i]).val($(my_textareas[i]).val());
        }
        for (i = 0, l = my_selects.length; i < l; ++i) {
            $(result_selects[i]).val($(my_selects[i]).val());
        }

        return result;
    };
})(jQuery.fn.clone);

/**
 * Surcharge des méthodes on, one et off pour lister les evenements
 */
(function ($) {
    $.fn.old_on = $.fn.on;
    $.fn.old_one = $.fn.one;
    $.event.old_remove = $.event.remove;

    function addEventToTarget(event, target) {
        var events = target.data('events');
        if (!Array.isArray(events)) {
            events = [];
        }

        var types = event.types,
            selector = event.selector,
            data = event.data,
            fn = event.fn,
            one = event.one;

        // Code extrait des sources de jquery 3.2.1 function on()
        if (typeof types === "object") {
            if (typeof selector !== "string") {
                data = data || selector;
                selector = undefined;
            }
            for (let type in types) {
                addEventToTarget({ types: types[type], selector: selector, data: data, fn: fn, one: one }, target);
            }
            return;
        }

        if (data == null && fn == null) {
            fn = selector;
            data = selector = undefined;
        } else if (fn == null) {
            if (typeof selector === "string") {
                fn = data;
                data = undefined;
            } else {
                fn = data;
                data = selector;
                selector = undefined;
            }
        }
        if (fn === false) {
            fn = () => false;
        } else if (!fn) {
            return;
        }

        events.push({ types: types, selector: selector, data: data, fn: fn, one: one });
        target.data('events', events);
    }

    $.fn.on = function (types, selector, data, fn) {
        this.each(function () {
            addEventToTarget({ types: types, selector: selector, data: data, fn: fn, one: false }, $(this));
        });
        this.old_on(types, selector, data, fn);
        return this;
    };
    $.fn.one = function (types, selector, data, fn) {
        this.each(function () {
            addEventToTarget({ types: types, selector: selector, data: data, fn: fn, one: true }, $(this));
        });
        this.old_one(types, selector, data, fn);
        return this;
    };
    function getNamespaces(type) {
        var rtypenamespace = /^([^.]*)(?:\.(.+)|)/,
            tmp = rtypenamespace.exec(type) || [];
        return (tmp[2] || "").split(".").sort();
    }

    $.event.remove = function (elem, types, handler, selector, mappedTypes) {
        var j,
            origCount,
            tmp,
            events,
            t,
            handleObj,
            special,
            handlers,
            type,
            namespaces,
            origType,
            calledType = types,
            elemData = typeof $(elem).data === 'function' ? $(elem).data() : [];

        var rnothtmlwhite = /[^\x20\t\r\n\f]+/g,
            rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

        if (!elemData || !(events = elemData.events)) {
            $.event.old_remove(elem, types, handler, selector, mappedTypes);
            return;
        }

        // Once for each type.namespace in types; type may be omitted
        types = (types || "").match(rnothtmlwhite) || [""];
        t = types.length;
        while (t--) {
            tmp = rtypenamespace.exec(types[t]) || [];
            type = origType = tmp[1];
            namespaces = (tmp[2] || "").split(".").sort();
            // Unbind all events (on this namespace, if provided) for the element
            if (!type) {
                events = events.filter(function (event) {
                    var eventNamespaces = getNamespaces(event.types);
                    for (let i = 0; i < namespaces.length; i++) {
                        if (eventNamespaces.indexOf(namespaces[i]) >= 0 || namespaces[i] === "") {
                            return false;
                        }
                    }
                    return true;
                });
                $(elem).data('events', events);
                $.event.old_remove(elem, calledType, handler, selector, mappedTypes);
                continue;
            }

            special = $.event.special[type] || {};
            type = (selector ? special.delegateType : special.bindType) || type;

            for (let key in events) {
                let tmp2 = rtypenamespace.exec(events[key].types) || [],
                    eventType = tmp2[1],
                    eventNamespaces = (tmp2[2] || "").split(".").sort(),
                    namespaceMatch = false;
                eventNamespaces.push("");
                for (let j = 0; j < namespaces.length; j++) {
                    if (eventType === type && eventNamespaces.indexOf(namespaces[j]) >= 0) {
                        namespaceMatch = true;
                        break;
                    }
                }
                if ((!handler || handler.guid === events[key].fn.guid) && (!tmp || namespaceMatch) && (!selector || selector === events[key].selector || selector === "**" && events[key].selector)) {
                    delete events[key];
                }
            }
            events = events.filter(e => e);
        }
        $(elem).data('events', events.filter(e => e));
        $.event.old_remove(elem, calledType, handler, selector, mappedTypes);
    };

    /**
     * Permet d'ajouter un evenement qui sera executé avant les autres
     */
    $.fn.extend({
        prependEvent: function (types, selector, data, fn) {
            return this.each(function () {
                var events = $(this).data("events");
                if (!Array.isArray(events)) {
                    events = [];
                }
                $(this).off().on(types, selector, data, fn);
                for (let i = 0; i < events.length; i++) {
                    if (events[i].one) {
                        $(this).one(events[i].types, events[i].selector, events[i].data, events[i].fn);
                    } else {
                        $(this).on(events[i].types, events[i].selector, events[i].data, events[i].fn);
                    }
                }
            });
        }
    });
})(jQuery);

/**
 * Equivalent d'un $.on('click keydown') avec condition sur l'evenement keydown
 * pour n'autoriser que la touche "entrée"
 */
(function ($) {
    $.fn.extend({
        clickEnter: function (data, fn) {
            if (typeof fn === 'undefined') {
                fn = data;
                data = undefined;
            }
            function clickEnterHandler(event, data) {
                if (event.type === "click" || event.originalEvent.keyCode === 13) {
                    // Enter
                    fn.call(this, event, data);
                }
            }
            return this.each(function () {
                $(this).on('click.clickEnter keydown.clickEnter', null, data, clickEnterHandler);
            });
        }
    });
})(jQuery);

/**
 * Meilleur animation shake que celle de jquery-ui
 */
(function ($) {
    $.fn.extend({
        shake: function (options, oncomplete) {
            var distance = 5,
                time = 500,
                direction = 'left',
                start = new Date().getTime();
            if (typeof options === 'object') {
                if (typeof options.distance !== 'undefined') {
                    distance = options.distance;
                }
                if (typeof options.duration !== 'undefined') {
                    time = options.duration;
                }
                if (typeof options.direction === 'string') {
                    if (options.direction === 'up') {
                        direction = 'top';
                    } else if (options.direction === 'down') {
                        direction = 'bottom';
                    } else {
                        direction = options.direction;
                    }
                }
            }

            function animate(e, originalStyle) {
                var now = new Date().getTime();
                var elapsed = now - start;
                var fraction = elapsed / time;
                if (fraction < 1) {
                    var x = distance * Math.sin(fraction * 4 * Math.PI);
                    e.style[direction] = x + "px";
                    setTimeout(function () {
                        animate(e, originalStyle);
                    }, Math.min(25, time - elapsed));
                } else {
                    e.style.cssText = originalStyle;
                    if (oncomplete) {
                        oncomplete.call(e);
                    }
                }
            }

            return this.each(function () {
                var originalStyle = this.style.cssText;
                this.style.position = "relative";
                animate(this, originalStyle);
            });
        }
    });
})(jQuery);

/**
 * Permet d'obtenir un chemin css à partir d'un element jquery
 */
(function ($) {
    $.fn.path = function () {
        function identify() {
            if (!this.tagName) {
                return;
            }
            var value = this.tagName.toLowerCase();
            if (this.getAttribute('id')) {
                value += '#' + this.getAttribute('id');
            } else if (this.getAttribute("class")) {
                value += '.' + this.getAttribute("class").replace(/ /g, '.');
            }
            return value;
        }
        var identity = identify.apply(this.get(0));
        if (!identity) {
            return;
        }
        return this.parents().map(identify).get().reverse().join(" > ") + " > " + identity;
    };
})(jQuery);

/**
 * Permet de trouver les ids non unique
 * ex: $(document).ids()
 */
(function ($) {
    $.fn.ids = function () {
        return this.find('[id]').filter(function () {
            return $('[id="' + $(this).attr('id').replaceAll('"', '\\"') + '"]').length > 1 && $(this).closest('svg').length === 0;
        }).sort(function (a, b) {
            return a.id > b.id;
        }).map(function () {
            return [$(this).path(), this, ''];
        });
    };
})(jQuery);

/**
 * Permet de placer le html d'un noeud dans le presse papier
 */
(function ($) {
    $.fn.htmlToClipboard = function () {
        var html = $(this).html();
        var div = $('<div></div>').append($('<textarea></textarea>').val(html).css({ width: '100%' })).append($('<button>COPY</button>').click(function () {
            var textarea = div.find('textarea').get(0);
            textarea.select();
            textarea.setSelectionRange(0, 9999999);
            document.execCommand("copy");
            div.remove();
        })).css({
            position: 'fixed',
            "z-index": 1000000,
            top: 0,
            left: 0,
            right: 0,
            margin: '40vh auto',
            width: "400px",
            border: '1px solid #444',
            "background-color": 'rgba(240, 240, 240, 0.7)',
            padding: "10px",
            "text-align": 'center'
        });
        $('body').prepend(div);
        return div;
    };
})(jQuery);

/**
 * Ajoute l'option {dropdownPosition: 'below'} dans select2()
 */
(function ($) {

    var Defaults = $.fn.select2.amd.require('select2/defaults');

    $.extend(Defaults.defaults, {
        dropdownPosition: 'auto'
    });

    var AttachBody = $.fn.select2.amd.require('select2/dropdown/attachBody');

    var _positionDropdown = AttachBody.prototype._positionDropdown;

    AttachBody.prototype._positionDropdown = function () {

        var $window = $(window);

        var isCurrentlyAbove = this.$dropdown.hasClass('select2-dropdown--above');
        var isCurrentlyBelow = this.$dropdown.hasClass('select2-dropdown--below');

        var newDirection = null;

        var offset = this.$container.offset();

        offset.bottom = offset.top + this.$container.outerHeight(false);

        var container = {
            height: this.$container.outerHeight(false)
        };

        container.top = offset.top;
        container.bottom = offset.top + container.height;

        var dropdown = {
            height: this.$dropdown.outerHeight(false)
        };

        var viewport = {
            top: $window.scrollTop(),
            bottom: $window.scrollTop() + $window.height()
        };

        var enoughRoomAbove = viewport.top < offset.top - dropdown.height;
        var enoughRoomBelow = viewport.bottom > offset.bottom + dropdown.height;

        var css = {
            left: offset.left,
            top: container.bottom
        };

        // Determine what the parent element is to use for calciulating the offset
        var $offsetParent = this.$dropdownParent;

        // For statically positoned elements, we need to get the element
        // that is determining the offset
        if ($offsetParent.css('position') === 'static') {
            $offsetParent = $offsetParent.offsetParent();
        }

        var parentOffset = $offsetParent.offset();

        css.top -= parentOffset.top;
        css.left -= parentOffset.left;

        var dropdownPositionOption = this.options.get('dropdownPosition');

        if (dropdownPositionOption === 'above' || dropdownPositionOption === 'below') {
            newDirection = dropdownPositionOption;
        } else {

            if (!isCurrentlyAbove && !isCurrentlyBelow) {
                newDirection = 'below';
            }

            if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
                newDirection = 'above';
            } else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
                newDirection = 'below';
            }
        }

        if (newDirection == 'above' || isCurrentlyAbove && newDirection !== 'below') {
            css.top = container.top - parentOffset.top - dropdown.height;
        }

        if (newDirection != null) {
            this.$dropdown.removeClass('select2-dropdown--below select2-dropdown--above').addClass('select2-dropdown--' + newDirection);
            this.$container.removeClass('select2-container--below select2-container--above').addClass('select2-container--' + newDirection);
        }

        this.$dropdownContainer.css(css);
    };
})(window.jQuery);

/**
 * Ajoute $(input).getCaretPosition()
 * Returns the caret (cursor) position of the specified text field (oField).
 */
(function ($) {
    $.fn.getCaretPosition = function () {
        var oField = $(this).get(0);
        var iCaretPos = 0;
        if (document.selection) {
            oField.focus();
            var oSel = document.selection.createRange();
            oSel.moveStart('character', -oField.value.length);
            iCaretPos = oSel.text.length;
        } else if (oField.selectionStart || oField.selectionStart === '0') {
            iCaretPos = oField.selectionDirection === 'backward' ? oField.selectionStart : oField.selectionEnd;
        }
        return iCaretPos;
    };
})(jQuery);

//# sourceMappingURL=asalae.extended_jquery-compiled.js.map