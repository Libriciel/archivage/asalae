/**
 * plier/déplier un bloc
 * @param legend
 * @param action 'plier'|'deplier'
 */
function collapse(legend, action) {
    //Init DOM node
    var $fieldset = $(legend).closest('fieldset'),
        $icon = $(legend).find('i.icone-plier'),
        opener = 'fa fa-plus-square-o',
        closer = 'fa fa-minus-square-o',
        classopen = 'expanded',
        classclosed = 'collapsed',
        minimizetitle = 'cliquer pour replier',
        maximizetitle = 'cliquer pour déplier';

    if ($fieldset.hasClass('folder')) {
        closer = 'fa fa-folder-open';
        opener = 'fa fa-folder';
    } else if ($fieldset.hasClass('Document')) {
        closer = 'fa fa-file-text-o';
        opener = 'fa fa-file-o';
    } else if ($fieldset.hasClass('RelatedData')) {
        closer = 'fa fa-paperclip';
        opener = 'fa fa-paperclip';
    } else if ($fieldset.hasClass('tag')) {
        closer = 'fa fa-tags';
        opener = 'fa fa-tag';
    } else if ($fieldset.hasClass('Appraisal')) {
        closer = 'fa fa-calendar';
        opener = 'fa fa-calendar';
    } else if ($fieldset.hasClass('rules')) {
        closer = 'fa fa-gavel';
        opener = 'fa fa-gavel';
    } else if ($fieldset.hasClass('Keyword')) {
        closer = 'fa fa-flag';
        opener = 'fa fa-flag-o';
    } else if ($fieldset.hasClass('ContentDescriptive')) {
        closer = 'fa fa-flag';
        opener = 'fa fa-flag-o';
    } else if ($fieldset.hasClass('Organisation')) {
        closer = 'fa fa-sitemap';
        opener = 'fa fa-sitemap';
    } else if ($fieldset.hasClass('TransferringAgency')) {
        closer = 'fa fa-sitemap';
        opener = 'fa fa-sitemap';
    } else if ($fieldset.hasClass('ArchivalAgency')) {
        closer = 'fa fa-sitemap';
        opener = 'fa fa-sitemap';
    } else if ($fieldset.hasClass('OriginatingAgency')) {
        closer = 'fa fa-sitemap';
        opener = 'fa fa-sitemap';
    } else if ($fieldset.hasClass('Address')) {
        closer = 'fa fa-map-marker';
        opener = 'fa fa-map-marker';
    } else if ($fieldset.hasClass('Contact')) {
        closer = 'fa fa-envelope';
        opener = 'fa fa-envelope-o';
    } else if ($fieldset.hasClass('ContentDescription')) {
        closer = 'fa fa-info';
        opener = 'fa fa-info';
    } else if ($fieldset.hasClass('Communication')) {
        closer = 'fa fa-bullhorn';
        opener = 'fa fa-bullhorn';
    } else if ($fieldset.hasClass('CustodialHistory')) {
        closer = 'fa fa-clock-o';
        opener = 'fa fa-clock-o';
    } else if ($fieldset.hasClass('RelatedObjectReference')) {
        closer = 'fa fa-link';
        opener = 'fa fa-link';
    } else if ($fieldset.hasClass('AccessRestriction')) {
        closer = 'fa fa-unlock';
        opener = 'fa fa-lock';
    } else if ($fieldset.hasClass('Integrity')) {
        closer = 'fa fa-certificate';
        opener = 'fa fa-certificate';
    }

    if (action === undefined && $fieldset.hasClass(classopen) || action === 'plier') {
        //Etat: déplié; Action: plier;
        $fieldset.removeClass(classopen).addClass(classclosed);
        $icon.removeClass(closer).addClass(opener);

        $(legend).attr('title', maximizetitle);
        //Animation slide
        $(legend).next().slideUp();
    } else if (action === undefined && $fieldset.hasClass(classclosed) || action === 'deplier') {
        //Etat: plié; Action: déplier;
        $fieldset.removeClass(classclosed).addClass(classopen);
        $icon.removeClass(opener).addClass(closer);

        $(legend).attr('title', minimizetitle);
        //Animation slide
        if ($(legend).next().text() !== '') {
            $(legend).next().slideDown();
        }
    }
}

/**
 * tout plier/déplier
 * @param {boolean} visible
 */
function expandAll(visible) {
    var action = visible ? 'deplier' : 'plier';
    $('#content').find('fieldset > legend').each(function () {
        collapse(this, action);
    });
}

function invertDataName() {
    $('label, legend').each(function () {
        var value = $(this).find('span.text').text(),
            title = $(this).attr('data-name');
        if (title) {
            $(this).attr('data-name', value).find('span.text').text(title);
        }
    });
}

//# sourceMappingURL=asalae.seda2html-common-compiled.js.map