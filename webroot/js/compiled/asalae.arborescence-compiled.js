/**
 * Script de chargement de la page
 */
$(document).ready(function () {
    // noinspection JSCheckFunctionSignatures
    $('#arbonavigation').bind("loaded.jstree", function (event, data) {
        console.log(data);
        data.instance.open_all();
    }).bind("select_node.jstree", function (e, data) {
        $('#jstree').jstree('save_state');
    }).on("activate_node.jstree", function (e, data) {
        var target = data.node.a_attr.href;
        $('html, body').animate({
            scrollTop: $(target).offset().top
        }, 600);
    }).on('open_node.jstree', function (e, data) {
        //$('#' + data.node.id).find('i.jstree-icon.jstree-themeicon').chidren().addClass('fa-folder-o');
        $('a i.jstree-icon.jstree-themeicon').addClass('fa').addClass('fa-folder-o').addClass('fa-folder-open');
        $('#' + data.node.id).find('i.jstree-icon.jstree-themeicon').first()
        //.removeClass('fa-folder-o')
        .removeClass('fa-folder').addClass('fa-folder-open');
    }).on('close_node.jstree', function (e, data) {
        $('#' + data.node.id).find('i.jstree-icon.jstree-themeicon').first().removeClass('fa-folder-open').addClass('fa-folder');
    }).jstree();

    // draggable navigation
    $("#navigation").draggable({
        cursor: "grabbing",
        handle: "#headnavigation",
        containment: "parent"
    }).resizable({
        minHeight: 25,
        minWidth: 40,
        maxHeight: 600,
        handles: 'w, sw, se, e, s',
        containment: "parent"
    });
    // toggle menu
    $('#minimize').on('click', function () {
        togglemenu();
    });
    $('#headnavigation').on('click', function () {
        $(this).toggleClass('ui-draggable-handle');
        $(this).toggleClass('ui-draggable-grabbing');
    });

    $('.backtotop').on('click', function () {
        $('html, body').animate({
            scrollTop: $('#main').offset().top
        }, 200);
    });
});

/**
 * plier/déplier le menu
 */
function togglemenu() {
    var arbonavigation = $('#arbonavigation');
    if (arbonavigation.is(':visible')) {
        arbonavigation.hide();
        $("#navigation").css('width', '200px');
        arbonavigation.find("i").toggleClass('fa').toggleClass('fa-chevron-down');
    } else {
        arbonavigation.show();
        $('#navigation').css({
            position: 'fixed',
            width: '500px',
            height: 'auto',
            right: '15px',
            top: '10px'
        });
    }
}

//# sourceMappingURL=asalae.arborescence-compiled.js.map