/** global $, AsalaeGlobal, AsalaeLoading */

if (typeof __ === 'undefined') {
    var __ = function (str) {
        return str;
    };
}

/**
 * Gestion des filtres de recherche
 */
class AsalaeFilter {
    /**
     * Constructeur de classe
     * @param form
     * @param templates
     */
    constructor(form, templates) {
        this.templates = templates;
        this.form = form.on('change.filter', AsalaeFilter.formChangedHandler).trigger('change.filter').on('submit.filter', AsalaeFilter.formSubmitHandler);
        form.find('.select-filter-container select.filter').on('change.filter', this.selectHandlerSection());
    }

    /**
     * Ajoute un element de filtre
     *
     * @param {string|jQuery} element
     * @param {string} template
     */
    static append(element, template) {
        var filter = $(template),
            container = $('<div>').append($('<button></button>').addClass('close').attr('type', 'button').attr('aria-label', __('Retirer le filtre de recherche')).append('<span>×</span>').click(function () {
            $(this).parent().stop(true, true).slideUp(400, function () {
                $(this).remove();
            });
        }));

        // Remplace l'id et le name dans template
        filter.find('select, input, textarea').each(function () {
            var base_id = $(this).attr('id'),
                id = base_id + '_0',
                base_name = $(this).attr('name'),
                label = filter.find('label[for="' + base_id + '"]'),
                name,
                i = 0;

            while (id && $('#' + id).length) {
                i++;
                id = base_id + '_' + i;
            }

            if (label) {
                label.attr('for', id);
            }
            $(this).attr('id', id);

            var prependValueTo = $(this).attr('data-prepend-value-to');
            if (prependValueTo) {
                $(this).attr('data-prepend-value-to', prependValueTo + '-' + i);
            }

            if (!base_name) {
                return;
            }

            i = 0;
            name = AsalaeFilter._newName(base_name, i);
            while (name && $('[name="' + name + '"]').length) {
                i++;
                name = AsalaeFilter._newName(base_name, i);
            }
            $(this).attr('name', name);
        });

        container.append(filter);
        $(element).append(container);

        // Bouton Rechercher
        if (!$(element).parent().find('button[type="submit"]').length) {
            let btn = $('<button></button>').attr('type', 'button').addClass('btn').addClass('btn-default').html('<i class="fa fa-undo" aria-hidden="true"></i> ' + __('Réinitialiser les filtres')).click(AsalaeGlobal.confirmRemoveFilters);
            let saveBtn = $('<button></button>').attr('type', 'button').addClass('btn').addClass('btn-success').addClass('newSave').addClass('save').html('<i class="fa fa-plus-circle" aria-hidden="true"></i> ' + __("Nouvelle sauvegarde")).click(AsalaeGlobal.saveFilters);
            $(element).parent().append($('<div></div>').addClass('submit-btn-filter-container').addClass('modal-footer').append(btn).append(saveBtn).append($('<button></button>').attr('type', 'submit').addClass('btn').addClass('btn-primary').html('<i class="fa fa-filter" aria-hidden="true"></i> ' + __('Filtrer'))));
        }
    }

    /**
     * Transforme un nom pour y prefixer une valeur
     *
     * ex: myname => myname[0]
     * ex: another_name[256][] => another_name[0][256][]
     * ex: foo[] => foo[0][]
     * ex: foo[bar] => foo[0][bar]
     * ex: foo[bar][] => foo[0][bar][]
     *
     * @param {string} name
     * @param {number} index
     * @returns {string}
     */
    static _newName(name, index) {
        var search = name.search(/\[\d+](\[])*/); // recherche [number]([])?
        if (search !== -1) {
            return name.substr(0, search) + '[' + index + ']' + name.substr(search);
        }
        search = name.search(/(\[\w+])\[]$/); // recherche [string][]
        if (search !== -1) {
            return this._newName(name.substr(0, name.length - 2), index) + '[]';
        }
        search = name.search(/\[]$/); // recherche []
        if (search !== -1) {
            return name.substr(0, name.length - 2) + '[' + index + ']' + '[]';
        }
        search = name.search(/\[\w+]$/); // recherche [number]
        if (search !== -1) {
            return name.substr(0, search) + '[' + index + ']' + name.substr(search);
        }
        return name + '[' + index + ']';
    }

    /**
     * Evenements liés aux bouttons de la modale "Liste des sauvegardes de recherche"
     */
    static modalListButtons(urls, controller, action) {
        var div = $('.modal-list-buttons');
        div.find('button.delete').off('click.filter').on('click.filter', function () {
            var modal = $(this).closest('.modal'),
                select = modal.find('select.filterSelect'),
                value = select.val(),
                form = $('form.ModalFilters');
            if (value && !isNaN(value) && value >= 0 && typeof urls.delete !== 'undefined' && confirm(__("Cette action supprimera la sauvegarde de façon définitive. Voulez-vous continuer ?"))) {
                $('html').addClass('ajax-loading');
                $.ajax({
                    url: urls.delete + '/' + value,
                    success: function (content, textStatus, jqXHR) {
                        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                            select.find('option[value="' + value + '"]').remove();
                            select.val('-1');
                            setTimeout(function () {
                                $('select.filterSelect').stop(true, true).shake({ duration: 1000, distance: 2 }).parent().css('background-color', '#5cb85c').stop(true, true).animate({ 'background-color': 'transparent' });
                            }, 400);
                        } else {
                            this.error();
                        }
                    },
                    error: function () {
                        alert(PHP.messages.genericError);
                    },
                    complete: function () {
                        $('html').removeClass('ajax-loading');
                    }
                });
            }
        });
        div.find('button.overwrite').off('click.filter').on('click.filter', function () {
            var modal = $(this).closest('.modal'),
                value = modal.find('select.filterSelect').val(),
                form = $('form.ModalFilters');
            if (value && !isNaN(value) && value >= 0 && typeof urls.overwrite !== 'undefined' && confirm(__("Cette action va modifier la sauvegarde de façon définitive. Voulez-vous continuer ?"))) {
                let modal = $(this).closest('.modal'),
                    data = AsalaeFilter.getFiltersData(form);
                $('html').addClass('ajax-loading');
                $.ajax({
                    url: urls.overwrite + '/' + value,
                    headers: { Accept: 'application/json' },
                    method: 'POST',
                    data: data,
                    success: function (content, textStatus, jqXHR) {
                        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                            let newUrl = urls.overwrite.replace(/\/[\w\-]+\/[\w\-]+(\?.*)?$/, '/' + controller + '/' + action + '?' + data);
                            $('ul.filtersSelectWindow li.savedFilter[data-id="' + content.id + '"] a').attr('href', newUrl);
                            setTimeout(function () {
                                form.find('button.overwrite').stop(true, true).shake({ duration: 1000, distance: 2 }).disable();
                                form.find('.filter-container > div').css('background-color', '#5cb85c').stop(true, true).animate({ 'background-color': 'transparent' }).find('input').css('background-color', 'transparent');
                            }, 400);
                            setTimeout(function () {
                                $('select.filterSelect').stop(true, true).shake({ duration: 1000, distance: 2 }).parent().css('background-color', '#5cb85c').stop(true, true).animate({ 'background-color': 'transparent' });
                            }, 400);
                        } else {
                            this.error();
                        }
                    },
                    error: function () {
                        alert(PHP.messages.genericError);
                    },
                    complete: function () {
                        $('html').removeClass('ajax-loading');
                    }
                });
            }
        });
        div.find('button.load').off('click.filter').on('click.filter', function () {
            var modal = $(this).closest('.modal'),
                value = modal.find('select.filterSelect').val(),
                form = $('form.ModalFilters');
            if (value !== '-1') {
                $('html').addClass('ajax-loading');
                $.ajax({
                    url: urls.load + '/' + value,
                    type: 'POST',
                    data: {
                        helper: atob(form.find('input[name="ConfigFilter"]').val())
                    },
                    success: function (content) {
                        form.html(content);
                        form.find('select.filterSelect').val(value);
                        form.find('button.overwrite').disable();
                        form.find('button.load').disable();
                        setTimeout(function () {
                            form.find('button.load').stop(true, true).shake({ duration: 1000, distance: 2 });
                            form.find('button.delete').enable();
                            form.find('.filter-container > div').css('background-color', '#5cb85c').animate({ 'background-color': 'transparent' }).find('input').css('background-color', 'transparent');
                        }, 400);
                    },
                    error: function () {
                        alert("Une erreur a eu lieu");
                    },
                    complete: function () {
                        $('html').removeClass('ajax-loading');
                    }
                });
            }
        });
    }

    /**
     * Boutons de la modale d'ajout d'une sauvegarde
     * @param {string} url
     * @param {string} controller
     * @param {string} action
     */
    static modalAddSaveButtons(url, controller, action) {
        var div = $('.modal-save-buttons'),
            input = div.closest('.modal-content').find('input.filterNewSave');
        input.off('keypress.filter').on('keypress.filter', function (event) {
            if (event.originalEvent.keyCode === 13) {
                // Enter
                $(this).closest('.modal-content').find('button.btn-primary').click();
            }
        });
        div.find('button.btn-primary').off('click.filter').on('click.filter', function () {
            var value = input.val(),
                data = AsalaeFilter.getFiltersData($('form.ModalFilters')),
                parentDiv = input.parent();
            if (!data) {
                alert(__("Impossible de sauvegarder en cas de formulaire vide ou en erreur!"));
            } else if (!value) {
                let nf = $('<form>');
                parentDiv.append(nf.append(input));
                input.prop('required', true);
                nf.get(0).reportValidity();
                input.prop('required', false);
                nf.parent().append(input);
                nf.remove();
                input.focus();
            } else if (url) {
                let modal = $(this).closest('.modal');
                $('html').addClass('ajax-loading');
                $.ajax({
                    url: url,
                    headers: { Accept: 'application/json' },
                    method: 'POST',
                    data: data + '&savename=' + value + '&controller=' + controller + '&action=' + action,
                    success: function (content, textStatus, jqXHR) {
                        parentDiv.removeClass('has-error').find('.error-message').remove();
                        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                            input.val('');
                            modal.modal("hide");
                            $('.modal-list-buttons').closest('.modal-body').find('select.filterSelect').append('<option value="' + content.id + '">' + content.name + '</option>').val(content.id);
                            $('[name=SaveFilterSelect]').val(content.id);
                            let newUrl = url.replace(/\/[\w\-]+\/[\w\-]+(\?.*)?$/, '/' + controller + '/' + action + '?' + data);
                            if (/SaveFilterSelect=($|&|-1)/.test(newUrl)) {
                                newUrl = newUrl.replace("SaveFilterSelect=-1", "SaveFilterSelect=");
                                newUrl = newUrl.replace("SaveFilterSelect=", "SaveFilterSelect=" + content.id);
                            }
                            $('ul.filtersSelectWindow').append($('<li class="savedFilter" data-id="' + content.id + '"></li>').append($('<a class="navbar-link"><i class="fa fa-filter" aria-hidden="true"></i> </a>').append(content.name).attr('href', newUrl).click(AsalaeGlobal.interceptedLinkhandler)));
                            setTimeout(function () {
                                $('.modal-content button.listSaves').stop(true, true).shake({ duration: 1000, distance: 2 }).parent().css('background-color', '#5cb85c').stop(true, true).animate({ 'background-color': 'transparent' });
                            }, 400);
                        } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'false') {
                            let error = $('<span class="help-block error-message"></span>');
                            for (let key in content.name) {
                                error.append(content.name[key]);
                            }
                            parentDiv.addClass('has-error').append(error);
                        } else {
                            this.error();
                        }
                    },
                    error: function () {
                        alert(PHP.messages.genericError);
                    },
                    complete: function () {
                        $('html').removeClass('ajax-loading');
                    }
                });
            }
        });
    }

    /**
     * Permet de vérifier le formulaire et d'obtenir ses filtres
     * @param {Element|jQuery} form
     * @returns {string|boolean}
     */
    static getFiltersData(form) {
        if ($(form).get(0).checkValidity()) {
            let clone = $(form).clone();
            clone.find('input.configuration-filter').remove();
            clone.find('select.filterSelect').remove();
            clone.find('[name=SaveFilterSelect]').remove();
            clone.find('select[name="filter"]').remove();

            // retire les champs complémentaire si la valeur principale est vide
            // exemple: SI {foo[0][value] = ''} THEN remove foo[0][bar]
            clone.find('input, select, textarea').filter('[name$="[value]"]').filter(function () {
                return !$(this).val();
            }).each(function () {
                var name = $(this).attr('name');
                var basename = name.substr(0, name.indexOf('[value]'));
                clone.find('[name^="' + basename + '"]').remove();
            }).remove();
            clone.find('input, select, textarea').filter(function () {
                return !$(this).val();
            }).remove();
            return clone.serialize();
        } else {
            $(form).get(0).reportValidity();
            return false;
        }
    }

    /**
     * Ajoute selon la valeur de select dans le container, le filtre voulu à
     * l'aider du templates[value]
     * @param {string|jQuery} select
     * @param {string|jQuery} container
     * @param {Array} templates
     */
    static selectHandler(select, container, templates) {
        var form = $(select).closest('form');
        $(select).off('change.filter').on('change.filter', function () {
            var val = $(this).val();
            if (!val) {
                return;
            }
            $(this).val('');
            AsalaeFilter.append(container, templates[val]);
        });
        if (form.find('select.filterSelect').val() > 0) {
            form.find('button.delete').enable();
        }
    }

    static getContainerWithCloseBtn() {
        return $('<div>').append($('<button></button>').addClass('close').attr('type', 'button').attr('aria-label', __('Retirer le filtre de recherche')).append('<span>×</span>').click(function () {
            $(this).parent().stop(true, true).slideUp(400, function () {
                $(this).remove();
            });
        }));
    }

    /**
     * Handler du select "Ajouter un filtre"
     * @returns {function(...[*]=)}
     */
    selectHandlerSection() {
        var form = this.form;
        var templates = this.templates;
        return function () {
            var val = $(this).val();
            if (!val) {
                return;
            }
            $(this).val('');
            var filter = $(templates[val]),
                container = AsalaeFilter.getContainerWithCloseBtn();
            var base_with_value = val.match(/^(.*)\[value]$/);

            // Remplace l'id et le name dans template
            filter.find('select, input, textarea').each(function () {
                var base = base_with_value ? $(this).attr('id').match(/^(.*)-(\w+)$/) : null,
                    base_id = base ? base[1] : $(this).attr('id'),
                    id = base ? base_id + '-0-' + base[2] : base_id + '-0',
                    base_name = $(this).attr('name'),
                    label = filter.find('label[for="' + base_id + '"]'),
                    name,
                    i = 0;

                while (id && $('#' + id).length) {
                    i++;
                    id = base ? base_id + '-' + i + '-' + base[2] : base_id + '-' + i;
                }

                if (label) {
                    label.attr('for', id);
                }
                $(this).attr('id', id);

                var prependValueTo = $(this).attr('data-prepend-value-to');
                if (prependValueTo) {
                    $(this).attr('data-prepend-value-to', prependValueTo + '-' + i);
                }

                if (!base_name) {
                    return;
                }

                i = 0;
                name = AsalaeFilter._newName(base_name, i);
                while (name && $('[name="' + name + '"]').length) {
                    i++;
                    name = AsalaeFilter._newName(base_name, i);
                }
                $(this).attr('name', name);
            });

            container.append(filter);
            form.find('.filter-container').append(container);
            form.trigger('new-filter.filter', [container]);
        };
    }

    /**
     * onchange du formulaire pour "griser" les boutons au besoin
     * Fait appel aux data-callback (eval -> appel avec (element) si fn)
     */
    static formChangedHandler() {
        var selectedSave = $('select.filterSelect').val(),
            clone = $(this).clone(),
            listButtons = $(this).find('.modal-list-buttons'),
            filterData;
        clone.find('input.configuration-filter').remove();
        clone.find('select.filterSelect').remove();
        clone.find('[name=SaveFilterSelect]').remove();
        clone.find('select[name="filter"]').remove();
        filterData = clone.serialize();

        if (selectedSave && selectedSave > 0) {
            listButtons.find('button.delete').enable();
            listButtons.find('button.load').enable();
            // noinspection JSUnresolvedFunction
            if (filterData && this.checkValidity()) {
                listButtons.find('button.overwrite').enable();
            } else {
                listButtons.find('button.overwrite').disable();
            }
        } else {
            listButtons.find('button.delete').disable();
            listButtons.find('button.load').disable();
            listButtons.find('button.overwrite').disable();
        }
        $(this).find('[data-callback]').each(function () {
            var out = eval($(this).attr('data-callback'));
            if (typeof out === 'function') {
                out.call(this);
            }
        });
    }

    /**
     * Affichage de la liste des filtres
     * @param button
     */
    static toggleFilters(button) {
        button = $(button);
        var div = button.parent().siblings('.filters');
        var icon = button.find('i');
        if (div.is(':visible')) {
            div.show().slideUp('fast');
            icon.removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
        } else {
            div.hide().slideDown('fast');
            icon.removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
        }
    }

    /**
     * Handler du form submit
     * @param event
     */
    static formSubmitHandler(event) {
        event.preventDefault();
        var data = AsalaeFilter.getFiltersData(this);
        var action = $(this).attr('action');
        if (data) {
            action += (action.indexOf('?') >= 0 ? '&' : '?') + data;
        }
        $(this).remove();
        AsalaeGlobal.interceptedLinkToAjax(action);
    }

    /**
     * Envoi du formulaire de sauvegarde d'un nouveau filtre de recherche
     * @param form
     */
    static saveFormHandler(form) {
        form.on('submit.filter', function (event) {
            event.preventDefault();
            var data = AsalaeFilter.getFiltersData($($(this).attr('data-link')));
            form.find('input:required').get(0).setCustomValidity(data ? "" : __("Impossible d'enregistrer, aucun filtre n'a été sélectionné."));
            if (form.get(0).checkValidity()) {
                data += '&' + form.serialize();
                AsalaeLoading.ajax({
                    url: form.attr('action'),
                    method: 'post',
                    data: data,
                    headers: {
                        Accept: 'application/json'
                    },
                    success: function (content) {
                        var tableUid = $(form.attr('data-table')).attr('data-table-uid');
                        var table = TableGenerator.instance[tableUid];
                        table.data.push(content);
                        table.generateAll();
                        form.find('input:required').val('');
                        form.closest('.modal').modal('hide');
                    }
                });
            } else {
                form.get(0).reportValidity();
            }
        });
        setTimeout(function () {
            form.closest('.modal').find('button.accept').on('click.filter', function (event) {
                event.preventDefault();
                form.trigger('submit.filter');
            });
        }, 0);
    }

    /**
     * Effectue le tri sauvegardé
     * @param {object} filters
     * @param {string} formId
     */
    static actionFilter(filters, formId) {
        var url = $('#' + formId).attr('action');
        url += url.indexOf('?') >= 0 ? '&' : '?';
        for (var i = 0, len = filters.length; i < len; i++) {
            url += filters[i].key + '=' + $('<div>').html(filters[i].value).text();
            if (i + 1 < len) {
                url += '&';
            }
        }
        AsalaeGlobal.interceptedLinkToAjax(url);
    }

    /**
     * Effectue le tri sauvegardé
     * @param {int} id
     * @param {string} urlDelete
     * @param {string} tableId
     */
    static actionDelete(id, urlDelete, tableId) {
        AsalaeLoading.ajax({
            url: urlDelete + '/' + id,
            method: 'DELETE',
            headers: {
                Accept: 'application/json'
            },
            success: function (content, textStatus, jqXHR) {
                if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                    var table = TableGenerator.instance[$('#' + tableId).attr('data-table-uid')];
                    table.removeDataId(id);
                    table.generateAll();
                }
            }
        });
    }

    /**
     * Callback pour régénérer les datepickers
     */
    static datepickerCallback() {
        $(this).parent().find("input").each(function () {
            AsalaeGlobal.datepicker(this);
        });
    }

    /**
     * Callback pour régénérer les datetimepickers
     */
    static datetimepickerCallback() {
        $(this).parent().find("input").each(function () {
            AsalaeGlobal.datetimepicker(this);
        });
    }

    /**
     * Si on sélectionne un operateur type "Semaine en cours" il faut désactiver
     * l'input date
     * @param element
     */
    static dateOperatorChanged(element) {
        element = $(element);
        var val = element.val();
        var option = element.find('[value="' + val + '"]').first();
        var input = element.closest('.input-group').find('input.hasDatepicker');
        if (option.hasClass('disable-linked-date')) {
            input.disable().val('');
        } else {
            input.enable();
        }
    }
}

if (typeof module !== 'undefined') {
    module.exports = {
        AsalaeFilter: AsalaeFilter
    };
}

//# sourceMappingURL=asalae.filter-compiled.js.map