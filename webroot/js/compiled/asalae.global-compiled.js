/*global ab, AsalaeGlobal*/

/**
 * Initialisation de plusieurs comportements
 * Cette fonction permet de rétablir les comportements en cas de modification
 * de la page
 */
AsalaeGlobal.initContentScope = function () {
    /**
     * Transfert du debug hors body vers le body (ex: debug dans controller)
     */
    $('div#body-content div#zone-debug').append(function () {
        var div = $('<div></div>').addClass('escaped-debug-output');

        $('body > div.cake-debug-output, body > pre.cake-error').each(function () {
            var me = $(this).clone();
            div.append(me);
            $(this).remove();
        });

        return div.html();
    }());

    /**
     * Bouton pour réduire le debug
     */
    var compressTitle = typeof PHP === 'object' && PHP.messages.minifiable.compress ? PHP.messages.minifiable.compress : '';
    var expandTitle = typeof PHP === 'object' && PHP.messages.minifiable.expand ? PHP.messages.minifiable.expand : '';
    $('div.cake-debug-output').find('i.fa-compress').remove().end().prepend($('<i></i>').addClass('fa').addClass('fa-compress').attr('role', 'button').attr('title', compressTitle).click(function () {
        $(this).parent().find('pre.cake-debug').last().slideToggle();
        $(this).toggleClass('fa-compress').toggleClass('fa-expand');
        $(window).trigger('resize');
    }));

    /**
     * Permet de réduire une section (bouton)
     */
    $('section.minifiable .r-actions').find('i.fa-compress').remove().end().prepend($('<i></i>').addClass('fa').addClass('fa-compress').attr('role', 'button').attr('title', compressTitle).addClass('slide-icon').attr('tabindex', 0).clickEnter(function () {
        $(this).closest('section').find('.minifiable-target').slideToggle(400, function () {
            $(window).trigger('resize');
        });
        $(this).toggleClass('fa-compress').toggleClass('fa-expand');
        if ($(this).hasClass('fa-compress')) {
            $(this).attr('title', compressTitle);
        } else {
            $(this).attr('title', expandTitle);
        }
    }));

    $('.smart-td-size').off('change', AsalaeGlobal.smartTdSize).change(AsalaeGlobal.smartTdSize);

    /**
     * Dropbox
     */
    $('div.dropbox').off('.draggable-dropbox').on('dragover.draggable-dropbox', function () {
        $(this).addClass('dragover');
    }).on('dragleave.draggable-dropbox dragend.draggable-dropbox drop.draggable-dropbox', function () {
        $(this).removeClass('dragover');
    });

    /**
     * Cache le bouton filtre si il n'y a pas de filtre défini
     */
    $(function () {
        if ($('#modal-filters').find('select').length > 0) {
            $('#menu-li-filter').show();
        }
    });

    /**
     * Change la couleur du bouton filtre si il y a des filtres selectionnés
     * Ajoute un bouton filtre aux endroits voulu
     */
    $(function () {
        var permanents = $('.permanent-filters').find('select, input, textarea').filter(function () {
            return $(this).val();
        });
        if ($('.filter-container > div > button.close').length > 0 || permanents.length) {
            $('#menu-li-filter').addClass('active-dot');
            $('.display-active-filter').show().off('.click.filter').on('click.filter', function () {
                $('form.ModalFilters').empty().submit(); // deprecated
                $('form.filters-section-form').empty().submit();
            });
        } else {
            $('#menu-li-filter').removeClass('active-dot');
            $('.display-active-filter').hide();
        }
    });

    /**
     * Evenement custom d'envoi du formulaire de filtres
     */
    $(function () {
        $('form.ModalFilters').off('submit').submit(AsalaeGlobal.modalFiltersFormHandler);
    });

    /**
     * Déplacement de la fenetre modal
     */
    $(function () {
        $('div.modal-dialog').draggableCustom({ handle: 'div.modal-header, .bottom-handle' }).find('div.modal-header, .bottom-handle').css('cursor', 'move');
    });

    /**
     * Redirige vers la page indiqué dans l'input crée lors du clic sur les "..."
     */
    $(AsalaeGlobal.paginationSelectInput);

    /**
     * Permet la réduction des modals
     */
    $(function () {
        $('div.modal.minifiable').each(AsalaeGlobal.minifiableModal);
    });

    /**
     * Empeche la page de se recharger completement lors d'un clic sur <a>
     */
    $(document).off('.interceptLinkToAjax').on('click.interceptLinkToAjax keydown.interceptLinkToAjax', 'a', function (event) {
        if (event.type === 'click' || event.keyCode === 13) {
            AsalaeGlobal.interceptedLinkhandler.call(this, event);
        }
    });

    /**
     * Rend le contenu des modals redimensionnable (garde la taille actuelle comme minimale)
     */
    $(function () {
        $('.modal-content').each(function () {
            $(this).attr('data-width', $(this).actual('width')).attr('data-height', $(this).actual('height'));
            $(this).resizable({ alsoResize: $(this).find('.modal-body') }).on("resize", function () {
                var height = parseInt($(this).css('height'), 10),
                    width = parseInt($(this).css('width'), 10),
                    modalBody = $(this).find('.modal-body'),
                    iframe = $(this).find('iframe');
                if ($(this).width() <= 0 || $(this).height() <= 0) {
                    return;
                }
                if (modalBody.attr('style')) {
                    modalBody.css({ maxHeight: 'none', width: '100%' });
                }
                if (height < $(this).attr('data-height')) {
                    $(this).css({ height: '' });
                    if ($(this).height() > height) {
                        height = $(this).height();
                    }
                }
                if (width < $(this).attr('data-width')) {
                    $(this).css({ width: '' });
                    if ($(this).width() > width) {
                        width = $(this).width();
                    }
                }
                if (iframe) {
                    iframe.attr('height', height - 85);
                }
                $(this).attr('data-width', width).attr('data-height', height);
            });
        });
    });

    /**
     * Si un tri est effectué sur un tableau, fait apparaitre une icone (tri)
     */
    $(function () {
        $('table > thead > tr > th').find('a.sort-asc, a.sort-desc').closest('th').addClass('active').closest('section').find('> header .l-actions .display-active-sort').show().clickEnter(function () {
            var url = location.href,
                match = url.match(/((?:[&?])sort=[^&]+)(?:&|$)/);
            if (match) {
                url = url.substr(0, url.indexOf(match[1])) + url.substr(url.indexOf(match[1]) + match[1].length);
            } else {
                return;
            }
            match = url.match(/((?:[&?])direction=[^&]+)(?:&|$)/);
            if (match) {
                url = url.substr(0, url.indexOf(match[1])) + url.substr(url.indexOf(match[1]) + match[1].length);
            }
            AsalaeGlobal.interceptedLinkToAjax(url);
        });
    });

    /**
     * Permet d'afficher un debug/warning placé à l'interieur d'un <script>
     */
    $(function () {
        $('#zone-debug').append(AsalaeGlobal.extractDebug());
    });

    /**
     * Initialise les cookies sauvegardés
     */
    $(function () {
        if (typeof PHP === 'object' && typeof PHP.cookies === 'object') {
            AsalaeCookies.init(PHP.cookies);
        }
    });

    $(window).trigger('resize');
};
AsalaeGlobal.initContentScope();

/**
 * Améliore l'aparence d'un tableau en cas d'éclatement
 */
AsalaeGlobal.forceTableView = [];
AsalaeGlobal.resizeTimeout = null;
$(window).resize(function () {
    clearTimeout(AsalaeGlobal.resizeTimeout);
    // Limite grandement les ressources nécéssaire
    AsalaeGlobal.resizeTimeout = setTimeout(function () {
        AsalaeGlobal.smartTdSize();
        $('section.bg-white > table').each(function () {
            var parent = $(this).parent();
            if (parent.hasClass('exploded-container')) {
                parent.addClass('container').removeClass('exploded-container');
            }
            if ($(this).width() > parent.width() && $(this).is(':visible')) {
                parent.removeClass('container').addClass('exploded-container');
            }
        });

        var width = $(this).width();
        for (let i = 0; i < TableGenerator.instance.length; i++) {
            if (typeof AsalaeGlobal.forceTableView[i] === 'undefined') {
                AsalaeGlobal.forceTableView[i] = false;
            }
            if (AsalaeGlobal.forceTableView[i] && width > 766) {
                if (!TableGenerator.instance[i]._prevView) {
                    TableGenerator.instance[i]._prevView = TableGenerator.instance[i].view;
                }
                TableGenerator.instance[i].view = AsalaeGlobal.forceTableView[i];
                AsalaeGlobal.forceTableView[i] = false;
                TableGenerator.instance[i].generateAll();
            } else if (!AsalaeGlobal.forceTableView[i] && width <= 766) {
                if (!TableGenerator.instance[i]._prevView) {
                    TableGenerator.instance[i]._prevView = TableGenerator.instance[i].view;
                }
                AsalaeGlobal.forceTableView[i] = TableGenerator.instance[i].view;
                TableGenerator.instance[i].view = 'div';
                TableGenerator.instance[i].generateAll();
            }
        }

        // Déclanche le déplacement d'une modale - vérifi qu'elle est visible dans le cadre
        $('.modal-dialog:visible .modal-header').trigger('mousedown.draggableCustom');
        $(window).trigger('mousemove.draggableCustom').trigger('mouseup.draggableCustom');
    }, 100);
}).trigger('resize');

/**
 * Scroll dans la direction du drag
 * Permet d'atteindre la dropbox si elle n'est pas visible
 */
$(function () {
    var interval,
        removeInterval = function () {
        clearInterval(interval);
    };
    $(window).on("dragover", function (e) {
        removeInterval();

        if (e.originalEvent.clientY < 150) {
            interval = setInterval(function () {
                $(window).scrollTop($(window).scrollTop() - 5);
            }, 20);
        }

        if (e.originalEvent.clientY > $(window).height() - 150) {
            interval = setInterval(function () {
                $(window).scrollTop($(window).scrollTop() + 5);
            }, 20);
        }
    });
    $(window).on("dragout", removeInterval);
    $(window).on("mouseleave", removeInterval);
    $(window).on("mouseup", removeInterval);
    $(window).on("mousedown", removeInterval);
});

/**
 * Action lors d'un retour dans l'historique
 */
$(window).on('popstate', function () {
    if (window.location.href.indexOf('#') === -1) {
        AsalaeGlobal.interceptedLinkToAjax(window.location.href, false);
    }
});

/**
 * Initialisation des notifications
 */
if (typeof PHP === 'object' && PHP.user_id) {
    var config = {
        element: '#notifications-container',
        elementUl: '#notifications-ul-container',
        token: PHP.token,
        ajax: {
            deleteNotification: PHP.urls.deleteNotification
        }
    };
    var notifications = new AsalaeNotifications(config);
    notifications.connect(PHP.user_id, PHP.configs.Ratchet.connect).insert(PHP.notifications);

    /**
     * Permet à un admin de demander une reconnexion à l'utilisateur
     * les session est alors reconstruite suite à une requête ajax
     */
    $(notifications.element).on('connected.autobahn', function () {
        notifications.session.subscribe('session_' + PHP.user_id, function (topic, data) {
            data.last_archival_agency_id = PHP.archival_agency_id;
            $.ajax({
                url: PHP.urls.resetSession,
                method: 'POST',
                data: data,
                success: function (content, textStatus, jqXHR) {
                    if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                        notifications.insert([content], false);
                    }
                }
            });
        });
    });
}

/**
 * Déplacement des liens dans les "favoris"
 */
$(function () {
    var userMenu = $('#user-menu');
    // Ouvre le menu lorsque on lui apporte un element
    $('.dropdown-toggle').parent().on('dragover.usermenu', CustomMenu.dropdownToggleOnDragoverHandler).parent().on('dragleave.usermenu dragend.usermenu', CustomMenu.dragleaveHandler);

    // Survol des element li avec un autre element
    userMenu.find('.navbar-link:not(.disable-drag)').on('dragover.usermenu dragenter.usermenu', CustomMenu.dragoverHandler);
    userMenu.find('#drop-zone-custom-menu').on('dragover.usermenu dragenter.usermenu', CustomMenu.dragoverHandler);

    // Permet de copier un element pour le mettre dans le menu
    $('.navbar-link:not(.disable-drag)').on('dragstart.usermenu', CustomMenu.dragstartHandler).on('dragend.usermenu', CustomMenu.dragendHandler);

    userMenu.on('user-menu.changed', function () {
        var lis = [];
        $(this).find('.navbar-link:not(.disable-drag)').each(function () {
            let link = $(this).attr();
            link['@'] = $(this).html();
            if ($(this).parent().find('.close').length) {
                link['@close'] = true;
            }
            lis.push(link);
        });
        $.ajax({
            url: PHP.urls.ajaxSaveMenu,
            method: 'POST',
            data: {
                menu: JSON.stringify(lis)
            }
        });
    });

    // Remplace les croix statiques par des vrais
    userMenu.find('> ul > li > .close').each(function () {
        $(this).replaceWith(CustomMenu.getCloseBtn());
    });
});

/**
 * Alerte en cas de doublons d'ids en mode debug
 */
$(function () {
    $(document).off('change.ids').on('change.ids', function (e) {
        if (typeof PHP === 'object' && PHP.debug) {
            var ids = $(this).ids();

            // Vérifi que les fonctions dans les onclick soient définies
            setTimeout(function () {
                var notFounds = [];
                $('*[onclick]').each(function () {
                    var m = $(this).attr('onclick').match(/([\w.\[\]'"-]+)\(.*\)/);
                    try {
                        if (m && eval('typeof ' + m[1]) === 'undefined' && notFounds.indexOf(m[1]) === -1) {
                            notFounds.push(m[1]);
                            console.warn(this, m[1] + ' not found');
                        }
                    } catch (e) {}
                });
            }, 0);

            if (ids.length === 0) {
                return;
            }
            console.warn(ids);
            alert(__("Conflit d'id (DOM) détecté"));
            $(this).off('change.ids');
        }
    }).trigger('change.ids');
});

//# sourceMappingURL=asalae.global-compiled.js.map