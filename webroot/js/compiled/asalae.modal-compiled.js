/* globals $, __, AsalaeGlobal */

if (typeof __ === 'undefined') {
    var __ = function (str) {
        return str;
    };
}

var AsalaeModal = {
    loading: '<div class="text-center loading-container">' + '<i class="fa fa-4x fa-spinner faa-spin animated" aria-hidden="true"></i>' + '<span class="sr-only">Loading...</span></div>'
};

/**
 * Ajoute une modale à
 * @param {jQuery|Array} modal
 */
AsalaeModal.append = function (modal) {
    var body = $('body');
    if (Array.isArray(modal)) {
        for (var i = 0; i < modal.length; i++) {
            AsalaeModal.append(modal[i]);
        }
        return;
    }
    // Supprime les eventuels doublons
    $('[id="' + modal.attr('id') + '"]').each(function () {
        if (this !== modal.get(0)) {
            $(this).remove();
        }
    });
    AsalaeModal.modalList().append(modal);
    modal.find('div.modal-header, .bottom-handle').off('.draggableCustom');
    try {
        modal.find('> .modal-dialog').draggableCustom({ handle: '> .modal-content > .modal-header, > .modal-content > .bottom-handle' }).find('div.modal-header, .bottom-handle').css('cursor', 'move');
    } catch (e) {}

    // Remet la class modal-open lors de la fermeture d'une modal si il en reste une d'ouverte
    // Désactive le focus sur les elements en dehors des modales si il y en a une d'ouverte
    modal.off('hidden.bs.modal.appended').on('hidden.bs.modal.appended', function () {
        if ($('.modal:visible').length) {
            $('body').addClass('modal-open');
            $('.modal-body:visible').first().focus();
            $('#body-content').find(':focusable[tabindex]:not([data-tabindex])').each(function () {
                $(this).attr('data-tabindex', $(this).attr('tabindex'));
            });
            $('#body-content').find(':focusable').attr('tabindex', '-1');
        } else {
            window.onbeforeunload = null;
            $('#body-content').find(':focusable[tabindex]').removeAttr('tabindex');
            $('#body-content').find(':focusable[data-tabindex]').each(function () {
                $(this).attr('tabindex', $(this).attr('data-tabindex'));
            });
        }
    });
    // Fermeture de la modale si on presse plusieurs fois "Esc"
    modal.off('keydown.modal').on('keydown.modal', function (event) {
        var button = $(this).find('button.cancel');
        if (event.keyCode === 27) {
            let count = $(this).data('escape-count');
            if (!count) {
                count = 0;
            }
            count++;
            if (count >= 3) {
                $(this).removeClass('escaping').data('escape-count', count = 0);
                button.removeClass('active');
                $(this);
                if (!button.click().length) {
                    $(this).modal('hide');
                }
            } else {
                $(this).addClass('escaping').data('escape-count', count);
            }
            if (count > 1) {
                button.addClass('active');
            }
        } else {
            $(this).removeClass('escaping').data('escape-count', 0);
            button.removeClass('active');
        }
    });
};

/**
 * Renvoi le container de modals, le crée au besoin
 * @return {jQuery}
 */
AsalaeModal.modalList = function () {
    var modalList = $('body > div.modal-list');
    if (modalList.length === 0) {
        modalList = $('<div class="modal-list"></div>');
        $('body').append(modalList);
    }
    return modalList;
};

/**
 * Action d'un bouton d'ouverture d'une modale avec appel ajax
 * @param button
 * @param modal
 * @param url
 */
AsalaeModal.button = function (button, modal, url) {
    $(button).off().clickEnter(function () {
        AsalaeModal.append($(modal).modal('show'));
        $(modal).find('div.modal-body').html(AsalaeModal.loading);
        AsalaeGlobal.addWaitingAjaxResponse(modal);
        $.ajax({
            url: url,
            success: function (content) {
                $(modal).find('div.modal-body').html(content);
            },
            error: function (error) {
                AsalaeModal.error(error, modal);
            },
            complete: function () {
                AsalaeGlobal.removeWaitingAjaxResponse(modal);
            }
        });
    });
};

/**
 * Erreur générique d'un appel ajax pour une modal
 * @param error
 * @param modal
 */
AsalaeModal.error = function (error, modal) {
    console.error(error);
    var message = '',
        alertBox = $('<div class="alert alert-danger"></div>');
    if (!error) {
        alertBox.append(__("Une erreur inattendue a eu lieu"));
    } else if (typeof error === 'string') {
        alertBox.append(error);
    } else if (error.responseText && error.getResponseHeader('Content-Type').match(/html/)) {
        message = $('<div></div>').append(error.responseText);
    } else if (error.responseText && error.getResponseHeader('Content-Type').match(/json/)) {
        message = alertBox.append(__("Une erreur inattendue a eu lieu"));
        try {
            console.error(JSON.parse(error.responseText));
        } catch (e) {
            if (error.responseText.indexOf('cake-debug') > -1) {
                message = $('<div></div>').append(error.responseText);
            }
        }
    } else if (error.status === 403 && error.getResponseHeader('X-Connected') === 'false') {
        message = alertBox.append(__("Vous devez être connecté pour accéder à ce contenu"));
    } else if (error.status === 403) {
        message = alertBox.append(__("Vous n'avez pas l'autorisation d'accéder à ce contenu"));
    } else {
        message = alertBox.append(error.statusText);
    }
    $(modal).find('div.modal-body').empty().append(message);
};

/**
 * Gestion générique d'un formulaire de modal (rendu d'erreur html5 + chargement)
 * @param form
 * @param ajax
 */
AsalaeModal.submit = function (form, ajax) {
    if (typeof AsalaeGlobal === 'undefined') {
        console.error("AsalaeModal.submit() nécessite que AsalaeLoading soit chargé");
        alert(__("Une erreur inattendue a eu lieu"));
        return;
    }
    form = $(form);
    var dom = form.get(0);
    if (dom.checkValidity()) {
        var modal = form.closest('.modal');
        var buttons = modal.find('button.accept, button.cancel').disable();
        ajax.url = ajax.url ? ajax.url : form.attr('action');
        ajax.method = ajax.method ? ajax.method : 'POST';
        ajax.data = ajax.data ? ajax.data : form.serialize();
        if (ajax.complete) {
            var fn = params.complete;
            ajax.complete = function () {
                buttons.enable();
                fn.apply(ajax, arguments);
                AsalaeGlobal.colorTabsInError();
            };
        } else {
            ajax.complete = function () {
                buttons.enable();
                AsalaeGlobal.colorTabsInError();
            };
        }
        if (!ajax.error) {
            ajax.error = function (response) {
                form.html(response.responseText);
            };
        }
        AsalaeLoading.ajax(ajax);
    } else {
        dom.reportValidity();
        AsalaeGlobal.colorTabsInError();
    }
};

/**
 * Callback generic pour un formulaire à plusieurs étapes
 * @param content
 * @param textStatus
 * @param jqXHR
 */
AsalaeModal.stepCallback = function (content, textStatus, jqXHR) {
    var step = jqXHR.getResponseHeader('X-Asalae-Step');
    if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true' && step) {
        eval(step);
    } else {
        AsalaeGlobal.colorTabsInError();
    }
};

//# sourceMappingURL=asalae.modal-compiled.js.map