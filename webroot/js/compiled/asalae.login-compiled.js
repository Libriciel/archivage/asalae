/**
 * script supplémentaire pour la page de login
 */

/**
 * ctrl + shift + clic sur le logo redirige sur la console d'administration
 */
window.onload = function () {
    document.getElementById('asalae').addEventListener('click', function (event) {
        if (event.ctrlKey && event.shiftKey) {
            event.preventDefault();
            window.open(window.location.href.substr(0, window.location.href.toLowerCase().indexOf('/users/login')) + '/admins', "_self");
        }
    });
};

//# sourceMappingURL=asalae.login-compiled.js.map