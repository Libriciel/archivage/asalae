/* global __, TableGenerator, TableHelper, LiberSign, jQuery */

/**
 * Permet de une gestion des signatures de fichiers
 */

/**
 * Classe principale permettant d'obtenir la liste des certificats et de les
 * afficher avec TableGenerator
 *
 * @returns {AsalaeLiberSign}
 */
class AsalaeLiberSign {
    constructor(path = '/libersign/') {
        if (typeof LiberSign === 'undefined') {
            var err = __("L'extension de navigateur LiberSign est requise pour vous permettre d'effectuer une signature numérique.");
            setTimeout(function () {
                alert(err);
            }, 0);
            throw err;
        }
        var that = this;
        this.table = null;
        this.error = false;
        this.loaded = false;
        this.certificates = [];
        LiberSign.setUpdateUrl(path);
        LiberSign.getCertificates().then(function (certificates) {
            if (!certificates || certificates.length === 0) {
                let msg = __("Aucun certificat n'a été trouvé");
                that.error = true;
                $(that).trigger('AsalaeLiberSign.loadingError', msg);
                throw msg;
            }
            that.certificates = certificates;
            that.loaded = true;
            $(that).off('AsalaeLiberSign.loadingError');
            $(that).trigger('AsalaeLiberSign.loaded');
        }).catch(function (error) {
            let msg = __("Aucun certificat n'a été trouvé");
            that.error = true;
            $(that).trigger('AsalaeLiberSign.loadingError', msg);
            throw error;
        });
    }

    /**
     * Permet de construire un tableau pour le table-generator
     * @param {string|jQuery} element
     */
    buildTable(element) {
        if (this.error) {
            throw "Can't build table";
        }
        if (!this.loaded) {
            $(element).html(AsalaeLiberSign.getLoading());
            $(this).one('AsalaeLiberSign.loaded', () => this.buildTable(element)).one('AsalaeLiberSign.loadingError', (e, data) => $(element).html('<div class="alert alert-danger">' + data + '</div>'));
            return;
        }
        var table = $('<table class="table table-striped table-hover"></table>');
        var label = $('<label class="sr-only"></label>');
        var input = $('<input type="radio" name="selected_certificate" class="selected_certificate" required="required">');
        var id = $(element).attr('id');
        id = id ? id + '-' : '';
        $(element).html(table);
        this.table = new TableGenerator(table);
        this.table.params({
            thead: [[new TableHead('', { target: 'input' }), new TableHead(__("Identifiant"), { target: 'ID', display: false }), new TableHead(__("Nom"), { target: 'CN' }), new TableHead(__("E-mail"), { target: 'EMAILADDRESS' }), new TableHead(__("Émetteur"), { target: 'ISSUERDN' }), new TableHead(__("Date d'expiration"), { target: 'NOTAFTER', callback: TableHelper.date('LL') })]],
            tbody: {
                identifier: 'ID'
            }
        });
        this.table.data = [];
        for (var i = 0; i < this.certificates.length; i++) {
            this.table.data[i] = $.extend(true, {}, this.certificates[i]);
            this.table.data[i].input = $('<div></div>').append(label.clone().attr('for', 'radio-' + id + this.table.data[i].ID).append(__("Sélectionner {0}", this.table.data[i].CN))).append(input.clone().attr('id', 'radio-' + id + this.table.data[i].ID).val(this.table.data[i].ID));
        }
        this.table.applyCookies();
        this.table.generateAll();
    }

    /**
     * Permet d'obtenir le bouton de signature
     * @param hash
     * @return {*}
     */
    signButton(hash) {
        var parent = this.table.table.parent();
        // noinspection JSJQueryEfficiency
        var selected = parent.find('[name=selected_certificate]:checked');
        var checkbox = $('<input name="signature" type="checkbox" onclick="return false" required="required" class="checkbox-in-sign-button">');

        var button = $('<button type="button" class="btn btn-primary"></button>').append(checkbox).append('&nbsp;').append(__("Signer"));
        if (selected.length === 0) {
            button.disable();
            parent.find('[name=selected_certificate]').one('change', () => button.enable());
        }
        // noinspection JSJQueryEfficiency
        button.click(() => this.sign(hash, parent.find('[name=selected_certificate]:checked').val(), button));
        return $('<div></div>').append(button).append($('<div class="hidden-certificates"></div>'));
    }

    /**
     * Effectue la signature
     * @param {object} hashes {"id": 'hash'}
     * @param {string} certificate_id
     * @param {jQuery} button
     */
    sign(hashes, certificate_id, button = null) {
        var certificate;
        for (var i = 0; i < this.certificates.length; i++) {
            if (this.certificates[i].ID === certificate_id) {
                certificate = this.certificates[i];
                break;
            }
        }
        $(button).siblings('.hidden-certificates').empty();
        for (var key in hashes) {
            (function (params, key) {
                LiberSign.sign(certificate, params).then(function (signatures) {
                    $(button).find('.checkbox-in-sign-button').prop('checked', true).parent().removeClass('btn-primary').addClass('btn-success').removeClass('btn-danger');
                    $(button).siblings('.hidden-certificates').append($('<input type="hidden">').attr('name', 'signature[' + key + ']').val(signatures[0]));
                    $(button).trigger('new_signature');
                }).catch(function (error) {
                    $(button).find('.checkbox-in-sign-button').prop('checked', false).parent().removeClass('btn-primary').addClass('btn-danger');
                    alert(__("Une erreur a eu lieu lors de la signature"));
                    $(button).trigger('signature_failed');
                });
            })([{ format: 'CMS', hash: hashes[key] }], key);
        }
    }

    /**
     * Permet d'obtenir une animation de chargement
     * @return {jQuery}
     */
    static getLoading() {
        return $('<div class="text-center loading-container"></div>').append($('<i class="fa fa-4x fa-spinner faa-spin animated"></i>'));
    }
}

//# sourceMappingURL=asalae.libersign-compiled.js.map