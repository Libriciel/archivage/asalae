<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->template('MultiStepForm/add_org_entity')->step(1) . '<hr>';

echo $this->Form->create($entity, ['idPrefix' => 'add-org-entity'])
    . $this->Form->control('name', ['label' => __("Nom"), 'required'])
    . $this->Form->control('short_name', ['label' => __("Nom abrégé")])
    . $this->Form->control('identifier', ['label' => __("Identifiant unique"), 'required'])
    . $this->Form->control('description', ['label' => __("Description")])
    . $this->Form->control(
        'type_entity_id',
        [
            'label' => __("Type d'entité"),
            'required' => true,
            'options' => $typeEntities,
            'empty' => __("-- Sélectionner un type d'entité --"),
            'help' => $this->Html->tag('span#info-type-entities-add', ''),
        ]
    );
if (!$parentId) {
    echo $this->Form->control(
        'parent_id',
        [
            'label' => __("Entité parente"),
            'required' => true,
            'options' => $parents,
            'empty' => __("-- Sélectionner une entité parente --"),
        ]
    );
}

echo $this->Form->end();
?>
<script>
    /**
     * Cache les options interdites mais la laisse si le select est déjà
     * @param select
     * @param ids
     * @return {*}
     */
    function hideOptions(select, ids) {
        var selectValue = select.val();
        select.find('option').each(function () {
            var value = parseInt($(this).attr('value'), 10);
            if (value !== selectValue && ids.indexOf(value) !== -1) {
                $(this).hide().prop('disabled', true);
            }
        });
        return select;
    }

    var selectTypeEntities = $('#add-org-entity-type-entity-id');
    AsalaeGlobal.select2(hideOptions(selectTypeEntities, [<?=implode(', ', $typeEntitiesDisabled)?>]));

    selectTypeEntities.change(function () {
        var help = $('#info-type-entities-add');
        if (!selectTypeEntities.val()) {
            help.text('');
            return;
        }
        var info = selectTypeEntities.find('option[value=' + selectTypeEntities.val() + ']').attr('title').split('\n');
        var prefix = info.shift();
        help.text(prefix + ' ' + info.join(', '));
    }).trigger('change');
</script>
