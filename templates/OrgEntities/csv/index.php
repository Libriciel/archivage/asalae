<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;

$map = function (EntityInterface $entity): EntityInterface {
    $parents = $entity->get('parents');
    $entity->set('parents', $parents ? '- ' . implode("\n- ", $parents) : '');
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'name' => __("Nom"),
        'identifier' => __("Identifiant"),
        'parents' => __("Parents"),
        'type_entity.name' => __("Type"),
        'description' => __("Description"),
        'has_eaccpf' => __("Possède une notice d'autorité"),
        'created' => __("Date de création"),
        'active' => __("Actif"),
    ],
    $results,
    $map
);
