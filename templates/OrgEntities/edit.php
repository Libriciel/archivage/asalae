<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

echo $this->element('modal', ['idTable' => $tableIdEaccpf, 'paginate' => false]);
echo $this->element('modal', ['idTable' => $tableIdUser, 'paginate' => false]);

$Html = $this->Html;

/* @link index.php */
echo $this->ModalForm
    ->create('entity-add-eaccpf-modal')
    ->modal(__("Ajout d'une notice d'autorité"))
    ->javascriptCallback('afterAddEaccpf2')
    ->output(
        'function',
        'addEaccpf2',
        '/Eaccpfs/add'
    )
    ->generate();

/**
 * Génère le "-- action groupée --" en bas du tableau
 * @param string $id
 * @return string
 */
$generateSubTableActions = function (string $id) use ($Html): string {
    $options = [
        '0' => __("-- action groupée --"),
        '1' => __("Télécharger en ZIP"),
        '2' => __("Supprimer"),
    ];
    $first = $Html->tag('div', null, ['class' => 'form-group form-group-sm'])
        . $Html->tag(
            'label',
            __("Action sur les cases à cocher"),
            ['class' => 'sr-only', 'for' => 'select-' . $id]
        )
        . $Html->tag('select', null, ['class' => 'form-control w-sm inline', 'id' => 'select-' . $id]);
    foreach ($options as $value => $message) {
        $first .= $Html->tag('option', $message, compact('value'));
    }
    $first .= $Html->tag('/select') . $Html->tag('/div');
    $second = $Html->tag(
        'button',
        '<i class="fa fa-cog" aria-hidden="true"></i> ' . __("Executer"),
        ['class' => 'btn btn-primary', 'id' => 'btn-' . $id]
    );

    return '
    <div>
        <div class="col-md-2">' . $first . '</div>
        <div class="col-md-2">' . $second . '</div>
    </div>';
};

$tableEaccpf = $this->Table
    ->create($tableIdEaccpf, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Eaccpfs.record_id' => ['label' => __("Identifiant unique")],
            'Eaccpfs.name' => ['label' => __("Nom")],
            'Eaccpfs.entity_id' => ['label' => __("Identifiant ISNI")],
            'Eaccpfs.entity_type' => ['label' => 'Type', 'options' => $entity_type],
            'Eaccpfs.from_date' => ['label' => __("Date début"), 'type' => 'date'],
            'Eaccpfs.to_date' => ['label' => __("Date fin"), 'type' => 'date'],
            'Eaccpfs.created' => ['label' => __("Date de création"), 'type' => 'datetime', 'display' => false],
        ]
    )
    ->data($entity->get('eaccpfs'))
    ->params(
        [
            'identifier' => 'Eaccpfs.id',
            'checkbox' => true,
        ]
    )
    ->actions(
        [
            [
                'href' => "/Eaccpfs/download/{0}",
                'download' => '{2}',
                'target' => '_blank',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => __("Télécharger {0}", '{1}'),
                'aria-label' => __("Télécharger {0}", '{1}'),
                'params' => ['Eaccpfs.id', 'Eaccpfs.name', 'Eaccpfs.record_id'],
            ],
            [
                'onclick' => "loadEditModalEaccpf({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'params' => ['Eaccpfs.id', 'Eaccpfs.name'],
            ],
            [
                'onclick' => "actionDeleteEaccpf({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => __("Supprimer {0}", '{1}'),
                'aria-label' => __("Supprimer {0}", '{1}'),
                'params' => ['Eaccpfs.id', 'Eaccpfs.name'],
            ],
        ]
    );

$jsTableEaccpf = $tableEaccpf->tableObject;
$tableUser = $this->Table
    ->create($tableIdUser, ['class' => 'table table-striped table-hover smart-td-size'])
    ->url(
        $url = [
            'controller' => 'org-entities',
            'action' => 'paginate-users',
            $id,
            '?' => [
                'sort' => 'username',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'username' => ['label' => __("Identifiant de connexion")],
            'name' => ['label' => __("Nom d'utilisateur")],
            'role.name' => ['label' => __("Rôle")],
            'active' => ['label' => __("Actif"), 'type' => 'boolean'],
            'is_validator' => ['label' => __("Validateur"), 'type' => 'boolean'],
            'created' => ['label' => __("Date de création"), 'type' => 'datetime', 'display' => false],
            'modified' => ['label' => __("Date de modification"), 'type' => 'datetime', 'display' => false],
        ]
    )
    ->data($entity->get('users'))
    ->params(
        [
            'identifier' => 'id',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "loadEditModalUser({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'displayEval' => 'data[{index}].agent_type === "person"',
                'params' => ['id', 'name'],
            ],
            [
                'onclick' => "loadEditWsUser({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'displayEval' => 'data[{index}].agent_type === "software"',
                'params' => ['id', 'name'],
            ],
            [
                'onclick' => "actionDeleteUser({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => __("Supprimer {0}", '{1}'),
                'aria-label' => __("Supprimer {0}", '{1}'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'name'],
            ],
        ]
    );

$jsTableUser = $tableUser->tableObject;
$paginator = $this->AjaxPaginator->create('pagination-org-entity-users')
    ->url($url)
    ->table($tableUser)
    ->count($countUsers);

$addBtn = '';
if ($rolesUsers) {
    $addBtn .= $this->ModalForm
        ->create('edit-entity-add-user-modal')
        ->modal(__("Ajout d'un utilisateur"))
        ->javascriptCallback('afterAddUser')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un utilisateur")),
            '/Users/add/' . $entity->get('id')
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
if ($rolesWs) {
    $addBtn .= $this->ModalForm
        ->create('org-entity-edit-add-webservice-modal')
        ->modal(__("Ajout d'un accès de webservice"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTableUser . ', "Users")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un accès de webservice")),
            '/Users/addWebservice/' . $entity->get('id')
        )
        ->generate(['class' => 'btn btn-success']);
}

$usersTabContent = $this->Html->tag(
    'div',
    $paginator->scrollBottom(['style' => 'position: absolute; right: 20px; z-index:100'])
        . $addBtn
        . $this->ModalForm
            ->create('edit-entity-user-edit')
            ->modal(__("Modification d'un utilisateur"))
            ->javascriptCallback('afterEditUser')
            ->output('function', 'loadEditModalUser', '/Users/edit-by-admin')
            ->generate()
        . $this->ModalForm
            ->create('edit-entity-user-ws-edit')
            ->modal(__("Modification d'un webservice"))
            ->javascriptCallback('afterEditUser')
            ->output('function', 'loadEditWsUser', '/Webservices/edit')
            ->generate(),
    ['class' => 'separator padding10']
)
    . $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Liste des utilisateurs de l'entité"))
        . $this->Html->tag('div.r-actions.h4', $tableUser->getConfigureLink())
    )
    . $tableUser . $paginator . $paginator->scrollTop();

$code = $entity->get('type_entity') ? $entity->get('type_entity')->get('code') : '';
$tabs = $this->Tabs->create('tab-entity-container', ['class' => 'row no-padding'])
    ->add(
        'entity-section',
        $this->Fa->charte('Modifier', __("Informations générales")),
        $this->Form->create(
            $entity,
            [
                'id' => 'edit-org-entity-form',
                'idPrefix' => 'edit-org-entity',
                'url' => $this->Url->build('/org-entities/edit/' . $id),
            ]
        )
        . $this->Form->control('name', ['label' => __("Nom"), 'required'])
        . $this->Form->control('short_name', ['label' => __("Nom abrégé")])
        . $this->Form->control(
            'identifier',
            [
                'label' => __("Identifiant unique"),
                'disabled' => !$editableIdentifier,
            ]
        )
        . $this->Form->control(
            'description',
            [
                'label' => __("Description"),
            ]
        )
        . $this->Form->control(
            'type_entity_id',
            [
                'label' => __("Type d'entité"),
                'required' => true,
                'options' => $typeEntities,
                'empty' => __("-- Sélectionner un type d'entité --"),
                'disabled' => $code === 'SA',
                'help' => $this->Html->tag('span#info-type-entities-edit', ''),
            ]
        )
        . $this->Form->control(
            'parent_id',
            [
                'label' => __("Entité parente"),
                'options' => $parents,
                'required' => $code !== 'SA',
                'empty' => __("-- Sélectionner une entité parente --"),
                'disabled' => $code === 'SA',
            ]
        )
        . $this->Form->control(
            'active',
            [
                'label' => __("Actif (autorise son utilisation pour les transferts entrants)"),
            ]
        )
        . $this->Html->tag(
            'div',
            $this->Form->button(
                $this->Fa->charte(
                    'Enregistrer',
                    __("Enregistrer")
                ),
                ['bootstrap-type' => 'primary']
            ),
            ['class' => 'text-right']
        )
        . $this->Form->end()
    )
    ->add(
        'eaccpf-section',
        $this->Fa->i('fa-id-card', __("Notice d'autorité")),
        $this->Html->tag(
            'div',
            $this->Html->tag(
                'button',
                $this->Fa->charte('Ajouter', __("Ajouter une notice d'autorité")),
                [
                    'class' => 'btn btn-success',
                    'type' => 'button',
                    'onclick' => 'addEaccpf2(' . $entity->get('id') . ')',
                ]
            )
            . $this->ModalForm
                ->create('eac-cpf-edit', ['size' => 'modal-xxl'])
                ->modal(__("Modifier une notice d'autorité"))
                ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTableEaccpf . ', "Eaccpfs")')
                ->output('function', 'loadEditModalEaccpf', '/Eaccpfs/edit')
                ->generate(),
            ['class' => 'separator padding10']
        ) .
        $this->Html->tag(
            'header',
            $this->Html->tag('h2.h4', __("Liste des notices d'autorité"))
            . $this->Html->tag('div.r-actions.h4', $tableEaccpf->getConfigureLink())
        )
        . $tableEaccpf->generate()
        . $generateSubTableActions('eaccpfs-section'),
        ['class' => 'save-on-change']
    )
    ->add(
        'users-section',
        $this->Fa->charte('Utilisateurs', __x('tab', "Utilisateurs")),
        $usersTabContent,
        ['class' => 'save-on-change']
    );

echo $tabs->generate();
?>
<!--suppress JSJQueryEfficiency -->
<script>
    var selectTypeEntities = $('#edit-org-entity-type-entity-id');

    /**
     * Callback de l'action ajout
     *
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterAddUser(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var table;
            table = <?=$jsTableUser?>;
            table.data.push(content);
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
            if (typeof content.code === 'string') {
                setTimeout(function () {
                    window.open('<?=$this->Url->build('/auth-urls/activate')?>/' + content.code, '_blank');
                }, 100);// laisse le temps à la db d'enregistrer l'url
            }
        }
    }

    /**
     * Callback de l'action edit
     *
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterEditUser(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var table;
            table = <?=$jsTableUser?>;
            table.replaceDataId(content.id, content);
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
        }
    }

    /**
     * Callback de l'action add eaccpf
     *
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterAddEaccpf2(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var table;
            table = <?=$jsTableEaccpf ?? '{}'?>;
            table.data = content;
            for (var i = 0; i < content.length; i++) {
                table.data[i].Eaccpfs = content[i];
            }
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
            if (table.data.length === 1) {
                AsalaeGlobal.interceptedLinkToAjax('' + location);
            }
        }
    }

    /**
     * Action delete
     *
     * @param {number} id
     */
    function actionDeleteEaccpf(id) {
        var useConfirm = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
        var update = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
        if (useConfirm && !confirm("<?=__("Êtes-vous sûr de vouloir supprimer cette notice d'autorité ?")?>")) {
            return;
        }
        $.ajax({
            url: '<?=$this->Url->build('/Eaccpfs/delete')?>/' + id,
            success: function (message) {
                if (message.report === 'done') {
                    var table = {};
                    table = <?=$jsTableEaccpf ?? '{}'?>;
                    table.removeDataId(id);
                    $(table.table)
                        .find('tr[data-id="' + id + '"]')
                        .fadeOut(400, function () {
                            if (update && table.data.length === 0) {
                                AsalaeGlobal.interceptedLinkToAjax('' + location);
                            }
                        });
                } else {
                    throw message.report;
                }
            },
            error: function () {
                $(<?=$jsTableEaccpf . '.'?>table)
                    .find('tr[data-id="' + id + '"]')
                    .addClass('danger')
                    .find('td.Eaccpfs-record_id')
                    .text("<?=__("Une erreur a eu lieu lors de la suppression")?>");
            }
        });
    }

    /**
     * Action delete
     *
     * @param {number} id
     */
    function actionDeleteUser(id) {
        var useConfirm = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
        var update = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
        if (useConfirm && !confirm("<?=__("Êtes-vous sûr de vouloir supprimer cet utilisateur ?")?>")) {
            return;
        }
        var table = {};
        table = <?=$jsTableUser ?? '{}'?>;
        $.ajax({
            url: '<?=$this->Url->build('/Users/delete')?>/' + id,
            success: function (message) {
                if (message.report === 'done') {
                    table.removeDataId(id);
                    $(table.table)
                        .find('tr[data-id="' + id + '"]')
                        .fadeOut(400, function () {
                            $(this).remove();
                            table.decrementPaginator();
                        });
                } else {
                    throw message.report;
                }
            },
            error: function () {
                $(table.table)
                    .find('tr[data-id="' + id + '"]')
                    .addClass('danger')
                    .find('td.Eaccpfs-record_id')
                    .text("<?=__("Une erreur a eu lieu lors de la suppression")?>");
            }
        });
    }

    var entityForm = $('#edit-org-entity-form');

    /**
     * Cache les options interdites mais la laisse si le select est déjà
     * @param select
     * @param ids
     * @return {*}
     */
    function hideOptions(select, ids) {
        var selectValue = select.val();
        select.find('option').each(function () {
            var value = parseInt($(this).attr('value'), 10);
            if (value !== selectValue && ids.indexOf(value) !== -1) {
                $(this).hide().prop('disabled', true);
            }
        });
        return select;
    }

    AsalaeGlobal.select2(
        hideOptions(selectTypeEntities, [<?=implode(', ', $typeEntitiesDisabled)?>])
    );
    AsalaeGlobal.select2(
        hideOptions($('#edit-org-entity-parent-id'), [<?=implode(', ', $parentsDisabled)?>])
    );

    var initialFormValue = entityForm.serialize();
    $('.save-on-change').click(function () {
        var form = $('#edit-org-entity-form'),
            formValue = form.serialize();

        if (formValue !== initialFormValue
            && confirm(__("Vous avez apporté des modifications au formulaire. Souhaitez-vous les sauvegarder ?"))
        ) {
            afterEditOrgEntity.forceReload = true;
            entityForm.trigger('submit');
        }
        initialFormValue = formValue;
    });

    $('#btn-eaccpfs-section').off('click').click(function () {
        var entityTable = $('#eaccpfs-entity-table');
        var visibleCheckedCheckboxes = entityTable.find('tbody td.td-checkbox input[type=checkbox]:checked');
        $(this).disable();
        switch ($('#select-eaccpfs-section').val()) {
            case '2':
                if (confirm(__("Êtes-vous sûr de vouloir supprimer les fichiers sélectionnés ?"))) {
                    visibleCheckedCheckboxes.each(function () {
                        actionDeleteEaccpf($(this).val(), false);
                    });
                }
                break;
            case '1':
                var ids = [];
                visibleCheckedCheckboxes.each(function () {
                    ids.push('id[]=' + $(this).val());
                });
                var e = document.createEvent('MouseEvents');
                e.initEvent('click', true, true);
                $('<a href="<?=$this->Url->build('/Eaccpfs/zip')?>?'
                    + ids.join('&') + '" target="_blank" download></a>')
                    .get(0)
                    .dispatchEvent(e);
        }
        $(this).enable();
        entityTable.find('.checkall').prop('checked', false);
    });

    selectTypeEntities.change(function () {
        var help = $('#info-type-entities-edit');
        if (!selectTypeEntities.val()) {
            help.text('');
            return;
        }
        var info = selectTypeEntities.find('option[value=' + selectTypeEntities.val() + ']').attr('title').split('\n');
        var prefix = info.shift();
        help.text(prefix + ' ' + info.join(', '));
    }).trigger('change');
</script>
