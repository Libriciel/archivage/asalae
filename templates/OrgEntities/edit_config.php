<?php

/**
 * @var Asalae\View\AppView                         $this
 * @var Cake\Datasource\EntityInterface             $entity
 * @var Asalae\Form\ArchivalAgencyConfigurationForm $form
 */

use Asalae\Model\Table\ConfigurationsTable;
use Cake\Core\Configure;

?>
<!--suppress RequiredAttributes -->
<script>
    function selectableUploadRadio(value, context) {
        if (!AsalaeGlobal.is_numeric(context.id) && context.id !== 'local') {
            return '';
        }
        var input = $('<input type="radio" name="fileuploads[id]">').val(context.id);
        setTimeout(function () {
            $('#entity-upload-background-table')
                .find('input[name="fileuploads[id]"]')
                .prop('checked', false)
                .first()
                .prop('checked', true);
        }, 0);
        return input;
    }

    function urlToImg(value) {
        if (value && value.substr(0, 4) !== '<img') {
            return $('<img alt="prev">')
                .attr('src', value)
                .css({
                    "max-width": "160px",
                    "max-height": "160px"
                });
        }
        return value;
    }
</script>
<?php

$uploads = $this->Upload
    ->create('entity-upload-background', ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'select' => [
                'label' => __("Sélectionner"),
                'class' => 'radio-td',
                'callback' => 'selectableUploadRadio',
            ],
            'prev' => ['label' => __("Prévisualiser"), "callback" => "urlToImg", 'escape' => false],
            'name' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
            'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
            'message' => ['label' => __("Message"), 'style' => 'min-width: 100px', 'class' => 'message'],
        ]
    )
    ->data($uploadFiles)
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    )
    ->actions(
        [
            function ($table) {
                $url = $this->Url->build('/Upload/delete');
                return [
                    'onclick' => "GenericUploader.fileDelete({$table->tableObject}, '$url', {0})",
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name'],
                ];
            },
            function ($table) use ($entity) {
                $url = $this->Url->build('/OrgEntities/deleteBackground');
                return [
                    'onclick' => "deleteLocalBackground({$table->tableObject}, '$url', {$entity->get('id')})",
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'displayEval' => 'data[{index}].webroot',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name'],
                ];
            },
        ]
    )
    ->generate(
        [
            'attributes' => ['accept' => 'image/*'],
            'allowDuplicateUploads' => true,
            'target' => $this->Url->build('Upload/index/?replace=true'),
        ]
    );


$tabs = $this->Tabs->create('tabs-edit-archival-agency-configuration', ['class' => 'row no-padding compact']);

echo $this->Form->create(
    $form,
    [
        'id' => 'edit-archival-agency-config-form',
        'idPrefix' => 'edit-archival-agency-config',
    ]
);

/**
 * Apparence
 */
$tabs->add(
    'tab-archival-agency-configuration-style',
    $this->Fa->i('fa-picture-o', $this->Html->tag('span.text', __("Apparence"))),
    // -----------------------------------------------------------------------------

    $this->Form->control(
        ConfigurationsTable::CONF_HEADER_TITLE,
        [
            'label' => __("Titre affiché dans l'entête des pages"),
            'placeholder' => $entity->get('name'),
        ]
    )
    . $this->Html->tag(
        'div.form-group.fake-input',
        $this->Html->tag('span.fake-label', __("Logo client (visible sur toutes les pages)"))
        . $uploads
    )
    . $this->Form->control(
        ConfigurationsTable::CONF_BACKGROUND_POSITION,
        [
            'label' => __("Position"),
            'placeholder' => '50px 50px',
            'help' => $this->Html->link(
                'https://developer.mozilla.org/fr/docs/Web/CSS/background-position',
                null,
                ['target' => '_blank']
            ),
        ]
    )
    . $this->Html->tag(
        'div.form-group.fake-input',
        $this->Html->tag('span.fake-label', __("Prévisualisation de la position"))
        . $this->Html->tag(
            'div',
            '',
            [
                'id' => 'background-position-prev',
                'style' => [
                    "border: 2px #999 solid;",
                    "background-color: #f9f9f9;",
                    "height: 150px;",
                    'background-image: url("' . $this->Url->build('/favicon.ico') . '");',
                    "background-repeat: no-repeat;",
                ],
            ]
        )
    )
);

/**
 * E-mails
 */
$tabs->add(
    'tab-archival-agency-configuration-email',
    $this->Fa->i('fa-envelope', $this->Html->tag('span.text', __("E-mails"))),
    // -----------------------------------------------------------------------------

    $this->Form->control(
        ConfigurationsTable::CONF_MAIL_TITLE_PREFIX,
        [
            'label' => __("Préfixe des objets des mails envoyés par la plateforme"),
            'placeholder' => '[asalae]',
        ]
    )
    . $this->Form->control(
        ConfigurationsTable::CONF_MAIL_HTML_SIGNATURE,
        [
            'label' => __("Signature des mails au format html"),
            'placeholder' => '<strong>' . $entity->get('name') . '</strong>',
            'class' => 'mce mce-small',
        ]
    )
    . $this->Form->control(
        ConfigurationsTable::CONF_MAIL_TEXT_SIGNATURE,
        [
            'label' => __("Signature des mails au format text"),
            'placeholder' => '-- ' . $entity->get('name') . ' --',
        ]
    )
);

/**
 * Eliminations
 */
$tabs->add(
    'tab-archival-agency-configuration-elimination',
    $this->Fa->i('fa-eraser', $this->Html->tag('span.text', __("Eliminations"))),
    // -----------------------------------------------------------------------------

    $this->Form->control(
        ConfigurationsTable::CONF_DESTR_REQUEST_CHAIN,
        [
            'label' => __("Validation des demandes d'élimination"),
        ]
    )
);

/**
 * Communications d'archives
 */
$tabs->add(
    'tab-archival-agency-configuration-communication',
    $this->Fa->i('fa-share-square-o', $this->Html->tag('span.text', __("Communications d'archives"))),
    // -----------------------------------------------------------------------------

    $this->Form->control(
        ConfigurationsTable::CONF_DR_NO_DEROGATION_CHAIN,
        [
            'label' => __("Validation des demandes de communication sans dérogation"),
        ]
    )
    . $this->Form->control(
        ConfigurationsTable::CONF_DR_DEROGATION_CHAIN,
        [
            'label' => __("Validation des demandes de communication avec dérogation"),
        ]
    )
    . $this->Form->control(
        ConfigurationsTable::CONF_DELIVERY_RETENTION_PERIOD,
        [
            'label' => __("Délai de rétention des communications (en nombre de jours)"),
            'min' => 0,
        ]
    )
);

/**
 * Restitutions d'archives
 */
$tabs->add(
    'tab-archival-agency-configuration-restitution',
    $this->Fa->i('fa-eject', $this->Html->tag('span.text', __("Restitutions d'archives"))),
    // -----------------------------------------------------------------------------

    $this->Form->control(
        ConfigurationsTable::CONF_RR_CHAIN,
        [
            'label' => __("Validation des demandes de restitutions"),
        ]
    )
    . $this->Form->control(
        ConfigurationsTable::CONF_RR_DESTR_DELAY,
        [
            'label' => __(
                "Délai avant élimination automatique des archives "
                . "restituées après acquittement (en nombre de jours)"
            ),
            'min' => 0,
        ]
    )
);

/**
 * Transferts sortant
 */
$tabs->add(
    'tab-archival-agency-configuration-outgoing-transfer-request',
    $this->Fa->i('fa-forward', $this->Html->tag('span.text', __("Transferts sortants"))),
    // -----------------------------------------------------------------------------

    $this->Form->control(
        ConfigurationsTable::CONF_OTR_CHAIN,
        [
            'label' => __("Validation des demandes de transferts sortants"),
        ]
    )
    . $this->Form->control(
        ConfigurationsTable::CONF_OTR_DESTR_DELAY,
        [
            'label' => __("Délai avant élimination automatique des archives transférées (en nombre de jours)"),
            'min' => 0,
        ]
    )
);

$noConvText = $this->Html->tag(
    'ul',
    '<li>' . implode(
        '</li><li>',
        [
            'ODT - OpenDocument Text',
            'TXT - Text File',
        ]
    ) . '</li>'
);
$noConvPdf = $this->Html->tag(
    'ul',
    '<li>' . implode(
        '</li><li>',
        [
            'PDF/A - Portable Document Format',
        ]
    ) . '</li>'
);
$noConvCalc = $this->Html->tag(
    'ul',
    '<li>' . implode(
        '</li><li>',
        [
            'ODS - OpenDocument Spreadsheet',
        ]
    ) . '</li>'
);
$noConvPresentation = $this->Html->tag(
    'ul',
    '<li>' . implode(
        '</li><li>',
        [
            'ODP - OpenDocument Presentation',
        ]
    ) . '</li>'
);
$noConvImage = $this->Html->tag(
    'ul',
    '<li>' . implode(
        '</li><li>',
        [
            'PNG - Portable Network Graphics',
            'JPG - Joint Photographic Experts Group',
            'TIFF - Tagged Image File Format',
            'GeoTIFF - Geographical Tagged Image File Format',
            'JPEG 2000 - Joint Photographic Experts Group 2000',
            'GIF - Graphics Interchange Format',
            'SVG - Scalable Vector Graphics',
        ]
    ) . '</li>'
);
$noConvAudio = $this->Html->tag(
    'ul',
    '<li>' . implode(
        '</li><li>',
        [
            'FLAC - Free Lossless Audio Codec',
            'OGG - Vorbis',
            'OGG - Opus',
            'MP3 - MPEG-1/2 Audio Layer 3',
            'AAC - Advanced Audio Coding',
        ]
    ) . '</li>'
);
$noConvVideo = $this->Html->tag(
    'ul',
    '<li>' . __("Conteneurs") . ':'
    . $this->Html->tag(
        'ul',
        '<li>' . implode(
            '</li><li>',
            [
                'MP4 - MPEG-4 Part 14',
                'MKV - Matroska',
                'MPEG-TS - Moving Picture Expert Group - Transport StreamMPEG',
            ]
        ) . '</li>'
    ) . '</li>'
    . '<li>' . __("Codec video") . ':'
    . $this->Html->tag(
        'ul',
        '<li>' . implode(
            '</li><li>',
            [
                'VP8 - VP8',
                'MKV - Matroska',
                'H.264 / MPEG-4 AVC - Advanced Video Coding',
            ]
        ) . '</li>'
    ) . '</li>'
    . '<li>' . __("Codec audio") . ':'
    . $this->Html->tag(
        'ul',
        '<li>' . implode(
            '</li><li>',
            [
                'Vorbis - Vorbis',
                'AAC - Advanced Audio Coding',
                'FLAC - Free Lossless Audio Codec',
                'MP3 - MPEG-1/2 Audio Layer 3',
            ]
        ) . '</li>'
    ) . '</li>'
);

/**
 * Conversions
 */
$tabs->add(
    'tab-archival-agency-configuration-conversions',
    $this->Fa->i('fa-retweet', $this->Html->tag('span.text', __("Conversions"))),
    // -----------------------------------------------------------------------------

    $this->Html->tag(
        'p.alert.alert-info',
        __(
            "Conversion pour conservation : les formats des fichiers préconisés"
            . " par le RGI ne font pas l'objet d'une conversion pour conservation"
            . " (ex: paramétrage conservation Document text -> PDF/A, les fichiers"
            . " odt ne seront pas convertis mais les fichiers docx seront convertis en pdf/a)."
        )
        . '<br>'
        . __(
            "Conversion pour diffusion : la conversion est systématique et"
            . " permet d'avoir une cohérence sur l'ensemble du fond numérique (ex:"
            . " paramétrage diffusion Document text -> PDF/A, les fichiers odt, doc"
            . " et docx seront convertis en pdf/a)"
        )
    )

    /**
     * Document (text)
     */
    . $this->Form->fieldset(
        $this->Form->control(
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_TEXT,
            [
                'label' => __("Format de conservation") . ' (text)',
                'help' => __("Pas de conversion pour les formats suivants: {0}", $noConvText),
            ]
        )
        . $this->Form->control(
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_TEXT,
            [
                'label' => __("Format de diffusion") . ' (text)',
            ]
        ),
        ['legend' => __("Document") . ' (text)']
    )

    /**
     * Document PDF (pdf)
     */
    . $this->Form->fieldset(
        $this->Form->control(
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_PDF,
            [
                'label' => __("Format de conservation") . ' (pdf)',
                'help' => __("Pas de conversion pour les formats suivants: {0}", $noConvPdf),
            ]
        )
        . $this->Form->control(
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_PDF,
            [
                'label' => __("Format de diffusion") . ' (pdf)',
                'required' => false,
            ]
        ),
        ['legend' => __("Document PDF") . ' (pdf)']
    )

    /**
     * Feuille de calcul (calc)
     */
    . $this->Form->fieldset(
        $this->Form->control(
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_CALC,
            [
                'label' => __("Format de conservation") . ' (calc)',
                'help' => __("Pas de conversion pour les formats suivants: {0}", $noConvCalc),
            ]
        )
        . $this->Form->control(
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_CALC,
            [
                'label' => __("Format de diffusion") . ' (calc)',
            ]
        ),
        ['legend' => __("Feuille de calcul") . ' (calc)']
    )

    /**
     * Présentation (presentation)
     */
    . $this->Form->fieldset(
        $this->Form->control(
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_PRES,
            [
                'label' => __("Format de conservation") . ' (presentation)',
                'help' => __("Pas de conversion pour les formats suivants: {0}", $noConvPresentation),
            ]
        )
        . $this->Form->control(
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_PRES,
            [
                'label' => __("Format de diffusion") . ' (presentation)',
            ]
        ),
        ['legend' => __("Présentation") . ' (presentation)']
    )

    /**
     * Image (image)
     */
    . $this->Form->fieldset(
        $this->Form->control(
            ConfigurationsTable::CONF_CONV_PRESERV_IMAGE,
            [
                'label' => __("Format de conservation") . ' (image)',
                'help' => __("Pas de conversion pour les formats suivants: {0}", $noConvImage),
            ]
        )
        . $this->Form->control(
            ConfigurationsTable::CONF_CONV_DISSEM_IMAGE,
            [
                'label' => __("Format de diffusion") . ' (image)',
            ]
        ),
        ['legend' => __("Image") . ' (image)']
    )

    /**
     * Audio (audio)
     */
    . $this->Form->fieldset(
        $this->Form->control(
            ConfigurationsTable::CONF_CONV_PRESERV_AUDIO,
            [
                'label' => __("Format de conservation") . ' (audio)',
                'help' => __("Pas de conversion pour les formats suivants: {0}", $noConvAudio),
            ]
        )
        . $this->Form->control(
            ConfigurationsTable::CONF_CONV_DISSEM_AUDIO,
            [
                'label' => __("Format de diffusion") . ' (audio)',
            ]
        ),
        ['legend' => __("Audio") . ' (audio)']
    )

    /**
     * Video (video)
     */
    . $this->Form->fieldset(
        $this->Form->control(
            ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CONTAINER,
            [
                'label' => __("Format de conservation") . ' (container-video)',
            ]
        )
        . $this->Form->control(
            ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CODEC_VIDEO,
            [
                'label' => __("Codec video de conservation") . ' (codec-video)',
            ]
        )
        . $this->Form->control(
            ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CODEC_AUDIO,
            [
                'label' => __("Codec audio de conservation") . ' (codec-audio)',
                'help' => __("Pas de conversion pour les combinaisons de formats suivants: {0}", $noConvVideo),
            ]
        )

        . '<hr>'
        .
        $this->Form->control(
            ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CONTAINER,
            [
                'label' => __("Format de diffusion") . ' (container-video)',
            ]
        )
        . $this->Form->control(
            ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CODEC_VIDEO,
            [
                'label' => __("Codec video de diffusion") . ' (codec-video)',
            ]
        )
        . $this->Form->control(
            ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CODEC_AUDIO,
            [
                'label' => __("Codec audio de diffusion") . ' (codec-audio)',
            ]
        ),
        ['legend' => __("Video") . ' (video)']
    )
);

/**
 * Attestations
 */
$tabs->add(
    'tab-archival-agency-configuration-attestations',
    $this->Fa->i('fas fa-award', $this->Html->tag('span.text', __("Attestations"))),
    // -----------------------------------------------------------------------------

    $this->Form->fieldset(
        $this->Form->control(
            ConfigurationsTable::CONF_ATTEST_ARCHIVING_CERT,
            [
                'label' => __("Mode de production de l'attestation"),
                'help' => __(
                    "Les attestations peuvent être produites à la demande"
                    . " ou produites et stockées systématiquement à la création des"
                    . " archives. En cas de production à la demande, la date d'édition"
                    . " de l'attestation sera la date du jour de la demande. A noter"
                    . " que sélectionner le mode de production et stockage à la"
                    . " création des archives provoque un surplus d'occupation sur"
                    . " les volumes de stockage."
                ),
            ]
        )
        . $this->Form->control(
            ConfigurationsTable::CONF_ATTEST_ARCHIVING_CERT_COMMITMENT,
            [
                'label' => __("Engagement supplémentaire du service d'Archives"),
                'help' => __(
                    "Cette information, si elle est renseignée, sera "
                    . "ajoutée dans la section Le Service d'Archives s'engage"
                ),
            ]
        ),
        ['legend' => __("Attestations d'archivage")]
    )
    . $this->Form->fieldset(
        $this->Form->control(
            'destruction-delay',
            [
                'label' => __(
                    "Attestations d'élimination : délai de destruction des "
                    . "sauvegardes des fichiers des archives (en nombre de jours)"
                ),
                'help' => __(
                    "L'attestation d'élimination sera produite une fois "
                    . "le délai passé à partir de l'élimination des archives. "
                    . "Si le délai est fixé à 0 alors l'attestation sera "
                    . "immédiatement produite. Si le délai est supérieur à 0, "
                    . "il est possible d'activer la production d'attestations "
                    . "intermédiaires."
                ),
                'type' => 'number',
                'readonly' => true,
                'value' => Configure::read('Attestations.destruction_delay', 0),
            ]
        )
        . $this->Form->control(
            ConfigurationsTable::CONF_ATTEST_DESTR_CERT_INTERMEDIATE,
            [
                'label' => __("Activer la production des attestations intermédiaires"),
                'help' => __(
                    "Si activée, une attestation intermédiaire est "
                    . "produite immédiatement après l'opération d'élimination "
                    . "et l'attestation définitive sera produite après le délai "
                    . "précédent. Pour pouvoir activer la production des "
                    . "attestations intermédiaires, il faut saisir un délai "
                    . "supérieur à 0 jour."
                ),
            ]
        ),
        ['legend' => __("Attestation d'élimination")]
    )
);

echo $tabs;

echo $this->Form->end();
?>
<script>
    AsalaeGlobal.colorTabsInError();

    $('#edit-archival-agency-config-background-position').keyup(function () {
        $('#background-position-prev').css('background-position', $(this).val());
    }).keyup();

    /**
     * Suppression d'un background-image local (!= TableGenericAction.deleteAction)
     * @param table
     * @param url
     * @param id
     */
    function deleteLocalBackground(table, url, id) {
        var useConfirm = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
        if (useConfirm && !confirm(__("Êtes-vous sûr de vouloir supprimer cet enregistrement ?"))) {
            return;
        }
        $.ajax({
            url: url.replace(/\/$/, '') + '/' + id,
            method: 'DELETE',
            success: function (message) {
                if (message.report === 'done') {
                    table.removeDataId('local');
                    $(table.table)
                        .find('tr[data-id="local"]')
                        .fadeOut(400, function () {
                            $(this).remove();
                            table.decrementPaginator();
                        });
                } else {
                    throw message.report;
                }
            },
            error: function () {
                $(table.table).find('tr[data-id="' + id + '"]').addClass('danger');
            }
        });
    }

    $('#edit-archival-agency-config-form').accordion({header: "h3", heightStyle: "fill"});

    setTimeout(function () {
        tiny = tinymce.init({
            selector: 'textarea.mce-small',
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste help wordcount'
            ],
            // @see https://www.tiny.cloud/docs/advanced/editor-control-identifiers
            toolbar: 'undo redo | bold italic underline forecolor backcolor | fontsizeselect | bullist numlist | code',
            contextmenu: 'link | cut copy paste | formats removeformat | align | hr',
            branding: false,
            language: tinymceDefaultLanguage,
            setup: function (editor) {
                editor.on('keyup', function (e) {
                    editor.save();
                });
                editor.on('change', function (e) {
                    editor.save();
                    $(editor.getElement()).trigger('change');
                });
            }
        }).then(function (result) {
            var tiny = result[0];
            $('#notify-all-color').change(function () {
                var css = applyStyle($(this).val());
                tiny.getBody().setAttribute('style', css);
            }).change();
        });
        var modal = $('.modal:visible').last();
        modal.one('hidden.bs.modal', function () {
            tinymce.remove('textarea.mce-small');
        });
        $(document).off('.tinymodal').on('focusin.tinymodal', function (e) {
            var dialog = $(e.target).closest(".tox-dialog");
            if (dialog.length && modal.find(dialog).length === 0) {
                var wrapper = $('.tox-tinymce-aux');
                modal.append(wrapper);
            }
        });
    }, 0);
</script>
