<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

echo $this->Flash->render();
echo $this->Form->create($entity, ['idPrefix' => 'add-webservice']);

$idSelectRoles = 'add-webservice-role-id';

echo $this->Form->control(
    'orgentity',
    [
        'label' => __("Entité"),
        'options' => [$entity->get('org_entity')->get('id') => $entity->get('org_entity')->get('name')],
        'readonly',
    ]
);
echo '<label class="hide">autocomplete obliterator'
    . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';
echo $this->Html->tag(
    'div',
    $this->Form->control(
        'name',
        [
            'label' => __("Nom du webservice"),
            'required' => true,
        ]
    )
    . $this->Form->control(
        'email',
        [
            'label' => __("Email de contact"),
            'help' => __("Optionnel"),
        ]
    )
    . $this->Form->control(
        'role_id',
        [
            'label' => __("Rôle"),
            'required' => true,
            'options' => $roles,
            'empty' => __("-- Veuillez sélectionner un rôle --"),
        ]
    )
    . $this->Form->control(
        'active',
        [
            'label' => __("Actif"),
            'default' => true,
        ]
    )
);
echo $this->Form->fieldset(
    $this->Form->control(
        'username',
        [
            'label' => __("Identifiant de connexion"),
            'required' => true,
        ]
    )
    . $this->Form->control(
        'password',
        [
            'label' => __("Mot de passe de l'utilisateur"),
            'type' => 'password',
            'required' => true,
        ]
    )
    . $this->Form->control(
        'confirm-password',
        [
            'label' => __("Confirmer le mot de passe"),
            'type' => 'password',
            'required' => true,
        ]
    )
    . $this->Form->control(
        'sha256',
        [
            'label' => __("Hasher le mot de passe en sha256 (connecteur v1)"),
            'type' => 'checkbox',
        ]
    ),
    ['legend' => __("Identification (basic)")]
);
echo $this->Form->end();
