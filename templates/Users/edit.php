<?php

/**
 * @var Asalae\View\AppView $this
 * @var Asalae\Model\Entity\User $user
 */

echo $this->Form->create($user, ['autocomplete' => 'new-password', 'idPrefix' => 'edit-user']);
$labelPassword = __("Password (laisser vide pour ne rien changer)");
$idJauge = 'edit-user-jauge-password';
$displayRole = false;
$displayPassword = true;

$tabs = $this->Tabs->create('tab-edit-user', ['class' => 'row no-padding']);

ob_start();
require 'addedit-common.php';
$tabs1 = ob_get_clean();

$tabs->add(
    'user-main-section',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $this->Form->control('org_entity_id', ['type' => 'hidden'])
    . $this->Form->control(
        'orgentity',
        [
            'label' => __("Entité"),
            'options' => [$user->get('org_entity')->get('id') => $user->get('org_entity')->get('name')],
            'readonly',
        ]
    )
    . $tabs1
);
$prependCheckboxOpts = [
    'label' => __("Recevoir les notifications"),
    'type' => 'checkbox',
    'templates' => [
        'nestingLabel' => '{{hidden}}<label{{attrs}} class="sr-only">{{text}}</label>{{input}}',
        'checkboxContainer' => '{{content}}',
    ],
];

$tabs->add(
    'user-notification-section',
    $this->Fa->i('fa-mail-bulk', __("Notifications"), ['v5' => true]),
    $this->Html->tag(
        'p.alert.alert-info',
        __(
            "Vous recevrez un email à la fréquence sélectionnée "
            . "(chaque jour, chaque mois, ...) si la case à cocher est cochée."
        )
    )
    . $this->Form->control(
        'notify_download_pdf_frequency',
        [
            'label' => __("Fichier PDF volumineux prêt pour le téléchargement"),
            'options' => [
                '' => __("Immédiat"),
            ],
            'disabled' => true,
            'prepend' => $this->Form->control(
                'notify_download_pdf',
                $prependCheckboxOpts + [
                    'class' => 'disabled',
                    'checked' => true,
                    'readonly' => true,
                    'disabled' => true,
                    'onclick' => 'return false',
                ]
            ),
        ]
    )
    . $this->Form->control(
        'notify_download_zip_frequency',
        [
            'label' => __("Fichier ZIP volumineux prêt pour le téléchargement"),
            'options' => [
                '' => __("Immédiat"),
            ],
            'disabled' => true,
            'prepend' => $this->Form->control(
                'notify_download_zip',
                $prependCheckboxOpts + [
                    'class' => 'disabled',
                    'checked' => true,
                    'readonly' => true,
                    'disabled' => true,
                    'onclick' => 'return false',
                ]
            ),
        ]
    )
    . $this->Form->control(
        'notify_validation_frequency',
        [
            'label' => __("Validations à effectuer (transferts entrants, demandes)"),
            'options' => [
                '1' => __("Quotidien"),
                '7' => __("Hebdomadaire"),
                '30' => __("Mensuel"),
                '365' => __("Annuel"),
            ],
            'default' => '1',
            'prepend' => $this->Form->control('notify_validation', $prependCheckboxOpts),
        ]
    )
    . $this->Form->control(
        'notify_job_frequency',
        [
            'label' => __("Mes jobs en erreur"),
            'options' => [
                '1' => __("Quotidien"),
                '7' => __("Hebdomadaire"),
                '30' => __("Mensuel"),
                '365' => __("Annuel"),
            ],
            'default' => '1',
            'prepend' => $this->Form->control('notify_job', $prependCheckboxOpts),
        ]
    )
    . $this->Form->control(
        'notify_agreement_frequency',
        [
            'label' => __("Absence de transfert pendant une période fixée sur les accords de versement"),
            'options' => [
                '1' => __("Quotidien"),
                '7' => __("Hebdomadaire"),
                '30' => __("Mensuel"),
                '365' => __("Annuel"),
            ],
            'default' => '7',
            'prepend' => $this->Form->control('notify_agreement', $prependCheckboxOpts),
        ]
    )
    . $this->Form->control(
        'notify_transfer_report_frequency',
        [
            'label' => __("Récapitulatif des transferts acceptés, en attente, rejetés"),
            'options' => [
                '1' => __("Quotidien"),
                '7' => __("Hebdomadaire"),
                '30' => __("Mensuel"),
                '365' => __("Annuel"),
            ],
            'default' => '7',
            'prepend' => $this->Form->control('notify_transfer_report', $prependCheckboxOpts),
        ]
    )
);

echo $tabs;

echo $this->Form->end();
?>
<script>
    UserCommon.inputPassword = $('#edit-user-password');
    UserCommon.inputConfirm = $('#edit-user-confirm-password');
    UserCommon.progressBar
        = $('#edit-user-jauge-password').append(UserCommon.getJauge()).find('.progress .progress-bar');
    UserCommon.inputPassword.on('change keyup', UserCommon.passwordKeyup);
    UserCommon.inputPassword.change(UserCommon.inputPasswordChange);
    UserCommon.inputConfirm.on('change keyup', UserCommon.inputConfirmKeyup);

    $('#edit-user-notify-validation').on('change', function () {
        $('#edit-user-notify-validation-frequency').disable($(this).prop('checked') === false);
    }).change();
    $('#edit-user-notify-job').on('change', function () {
        $('#edit-user-notify-job-frequency').disable($(this).prop('checked') === false);
    }).change();
    $('#edit-user-notify-agreement').on('change', function () {
        $('#edit-user-notify-agreement-frequency').disable($(this).prop('checked') === false);
    }).change();
    $('#edit-user-notify-transfer-report').on('change', function () {
        $('#edit-user-notify-transfer-report-frequency').disable($(this).prop('checked') === false);
    }).change();

    var isValidator = $('#edit-user-is-validator');
    var useCert =  $('#edit-user-use-cert');
    isValidator
        .on('change', () => useCert.enable(isValidator.prop('checked'))
            .prop('checked', false));
</script>
