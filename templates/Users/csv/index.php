<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Csv->resultTable(
    [
        'username' => __("Identifiant"),
        'name' => __("Nom d'utilisateur"),
        'created' => __("Date de création"),
        'email' => __("E-mail"),
        'active' => __("Actif"),
        'is_validator' => __("Validateur"),
        'role.name' => __("Rôle"),
        'org_entity.name' => __("Entité"),
        'is_linked_to_ldap' => __("Lié à un LDAP"),
        'ldap.name' => __("Nom LDAP"),
    ],
    $results
);
