<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Html->tag('div.container');
if (!empty($notAuthorized)) {
    echo $this->Html->tag(
        'div.alert.alert-info',
        __(
            "La page demandée n'est pas/plus valide. La durée d'accès "
            . "peut être dépassée ou le traitement déjà effectué."
        )
    );
    echo $this->Html->tag('/div');
    return;
}

echo $this->Html->tag(
    'div.alert.alert-success',
    __("Vous vous êtes désabonné de la file {0}", $type)
);
echo $this->Html->tag('/div');
