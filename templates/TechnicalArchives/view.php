<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Cake\Datasource\EntityInterface;
use Cake\I18n\DateTime as CakeDateTime;

?><script>
    var downloadable = <?=$this->Acl->check($url = '/technical-archives/download-file') ? 'true' : 'false'?>;

    function filenameToLink(value, context) {
        if (downloadable) {
            var link = $('<a target="_blank"></a>').text(value).attr('download', value);
            link.attr('href', '<?=$this->Url->build($url)?>/' + context.id);
            return link;
        } else {
            return value;
        }
    }
</script>
<?php
$view = $this;

$tabs = $this->Tabs->create('view-technical-archive', ['class' => 'row no-padding']);

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($entity->get('name')));
$infos .= $this->Html->tag('p', h($entity->get('description')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Description"));
$infos .= $this->ViewTable->generate(
    $entity,
    [
        __("Identifiant") => 'archival_agency_identifier',
        __("Nom") => 'name',
        __("Description") => 'first_technical_archive_unit.description',
        __("Dates extrêmes") => '{first_technical_archive_unit.oldest_date|default("[nd]")} '
            . '- {first_technical_archive_unit.latest_date|default("[nd]")}',
        __("Type") => 'typetrad',
        __("Statut") => 'statetrad',
        __("Date de création") => 'created',
    ]
);

$infos .= $this->Html->tag('h4', __("Stockage"));
$infos .= $this->ViewTable->generate(
    $entity,
    [
        __("Rangement") => 'storage_path',
        __("Espace de conservation sécurisé") => 'secure_data_space.name',
        __("Intégrité des fichiers") => function (EntityInterface $entity) use ($view) {
            if ($entity->get('is_integrity_ok') === null || !$entity->get('integrity_date')) {
                $message = __("L'intégrité n'a jamais été vérifiée.");
            } else {
                /** @var CakeDateTime $date */
                $date = $entity->get('integrity_date');
                $message = $entity->get('is_integrity_ok')
                    ? __("Tous les fichiers sont intègres (vérification effectuée le {0})", $date->nice())
                    : __(
                        "Des fichiers non intègres ont été détectés (vérification effectuée le {0})",
                        $date->nice()
                    );
            }
            return $view->Html->tag('span#integrity-message-view', $message);
        },
        __("Fichiers stockés") => function (EntityInterface $entity) use ($view) {
            $counts = [
                'original_count',
                'timestamp_count',
            ];
            $count = 0;
            foreach ($counts as $field) {
                $count += $entity->get($field);
            }
            $counts = [
                'original_size',
                'timestamp_size',
            ];
            $size = 0;
            foreach ($counts as $field) {
                $size += $entity->get($field);
            }

            $td = __(
                "{0} et une taille de {1} ({2} Octets)",
                $count,
                $view->Number->toReadableSize($size),
                $size
            );
            $td .= $view->Html->tag('ul');

            $originalCount = __n(
                "{0} fichier original",
                "{0} fichiers originaux",
                $c = $entity->get('original_count'),
                $c
            );
            $originalSize = $c ? __n(
                "et une taille de {0} ({1} Octet)",
                "et une taille de {0} ({1} Octets)",
                $c = $entity->get('original_size'),
                '{original_size|toReadableSize}',
                $c
            ) : '';
            $td .= $view->Html->tag('li', "$originalCount $originalSize");

            $timestampCount = __n(
                "{0} fichier de jeton d'horodatage",
                "{0} fichiers de jetons d'horodatage",
                $c = $entity->get('timestamp_count'),
                $c
            );
            $timestampSize = $c ? __n(
                "et une taille de {0} ({1} Octet)",
                "et une taille de {0} ({1} Octets)",
                $c = $entity->get('timestamp_size'),
                '{timestamp_size|toReadableSize}',
                $c
            ) : '';
            $td .= $view->Html->tag('li', "$timestampCount $timestampSize");

            $td .= $view->Html->tag('/ul');
            return $td;
        },
    ]
);

$infos .= $this->Html->tag('/article');

$tabs->add(
    'tab-technical-archive-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);

$tableFiles = $this->Table
    ->create('view-technical-archive-files', ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'technical-archives',
            'action' => 'paginate-files',
            $id,
            '?' => [
                'sort' => 'filename',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'filename' => [
                'label' => __("Nom de fichier"),
                'callback' => 'filenameToLink',
                'order' => 'filename',
                'filter' => [
                    'filename[0]' => [
                        'id' => 'filter-technical-archive-file-filename-0',
                        'label' => false,
                        'aria-label' => __("Nom de fichier"),
                    ],
                ],
            ],
            'stored_file.size' => [
                'label' => __("Taille"),
                'callback' => 'TableHelper.readableBytes',
                'order' => 'size',
                'filter' => [
                    'size[0][value]' => [
                        'id' => 'filter-technical-archive-file-size-0',
                        'label' => false,
                        'aria-label' => __("Taille 1"),
                        'prepend' => $this->Input->operator('size[0][operator]', '>='),
                        'append' => $this->Input->mult('size[0][mult]'),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 1,
                    ],
                    'size[1][value]' => [
                        'id' => 'filter-technical-archive-file-size-1',
                        'label' => false,
                        'aria-label' => __("Taille 2"),
                        'prepend' => $this->Input->operator('size[1][operator]', '<='),
                        'append' => $this->Input->mult('size[1][mult]'),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 1,
                    ],
                ],
            ],
            'meta' => [
                'label' => __("Meta-donnés"),
                'target' => '',
                'thead' => [
                    'stored_file.hash' => ['label' => __("Empreinte du fichier")],
                    'stored_file.hash_algo' => ['label' => __("Algorithme de hashage")],
                    'mime' => ['label' => __("Type MIME")],
                    'format' => ['label' => __("Format")],
                ],
            ],
        ]
    )
    ->data($files)
    ->params(
        [
            'identifier' => 'id',
        ]
    );
$paginator = $this->AjaxPaginator->create('pagination-technical-archive-files')
    ->url($url)
    ->table($tableFiles)
    ->count($filesCount);

$tabs->add(
    'tab-technical-archive-files',
    $this->Fa->i('fa-paperclip', __("Fichiers")),
    $paginator->generateTable()
);

echo $tabs;
