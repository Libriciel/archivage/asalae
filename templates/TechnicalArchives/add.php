<script>
    function renderExt(value, context) {
        if (typeof context.name === 'undefined') {
            return null;
        }
        var ext = context.name.split('.').pop().toLowerCase();
        return '<div class="prev-ext"><i class="fa fa-file-o" aria-hidden="true"></i><span>' + ext + '</span></div>';
    }

    function selectableUpload(value, context) {
        return '<input type="checkbox" '
            + 'name="fileuploads[_ids][]" '
            + 'aria-label="' + context.name + '" '
            + 'value="' + context.id + '" '
            + 'checked="checked">';
    }
</script>
<?php
/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create(
    $entity,
    ['id' => 'add-technical-archive-form', 'idPrefix' => 'add-technical-archive']
);

echo $this->Form->control(
    'name',
    [
        'label' => __("Nom"),
    ]
);
echo $this->Form->control(
    'technical_archive_units.0.description',
    [
        'label' => __("Description"),
    ]
);
echo $this->Form->control(
    'technical_archive_units.0.history',
    [
        'label' => __("Historique de conservation"),
    ]
);
echo $this->Form->control(
    'technical_archive_units.0.oldest_date',
    [
        'label' => __("Date extrême la plus ancienne"),
        'type' => 'text',
        'append' => $this->Date->picker(
            '#add-technical-archive-technical-archive-units-0-oldest-date'
        ),
        'class' => 'datepicker',
    ]
);
echo $this->Form->control(
    'technical_archive_units.0.latest_date',
    [
        'label' => __("Date extrème la plus récente"),
        'type' => 'text',
        'append' => $this->Date->picker(
            '#add-technical-archive-technical-archive-units-0-latest-date'
        ),
        'class' => 'datepicker',
    ]
);

$uploads = $this->Upload
    ->create($uploadId, ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'select' => ['label' => __("Sélectionner"), 'class' => 'radio-td', 'callback' => 'selectableUpload'],
            'ext' => ['label' => false, 'callback' => 'renderExt'],
            'name' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
            'message' => ['label' => __("Message"), 'class' => 'message', 'style' => 'min-width: 250px'],
            'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
        ]
    )
    ->data($uploadData ?? [])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    )
    ->actions(
        [
            [
                'href' => "/download/file/{0}",
                'download' => '{1}',
                'target' => '_blank',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => __("Télécharger {0}", '{1}'),
                'aria-label' => __("Télécharger {0}", '{1}'),
                'params' => ['id', 'name'],
            ],
            function ($table) {
                /** @var Asalae\View\AppView $this */
                $url = $this->Url->build('/Upload/delete');
                return [
                    'onclick' => "GenericUploader.fileDelete({$table->tableObject}, '$url', {0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name'],
                ];
            },
        ]
    );
$jsUpload = $uploads->jsObject;
$jsTableUpload = $this->Upload->getTableId($uploadId);

echo $this->Html->tag(
    'div.form-group.fake-input',
    $this->Html->tag('span.fake-label', __("Téléversement de fichiers"))
    . $uploads->generate(
        [
            'singleFile' => false,
            'target' => $this->Url->build('/Upload/index/?replace=true'),
            'autoretry' => false,
        ]
    )
);

echo $this->Form->end();
