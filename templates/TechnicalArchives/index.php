<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Archives"));
$this->Breadcrumbs->add(__("Archives techniques"));

// Titre de la page
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-archive', __("Archives techniques")))
    . $this->Breadcrumbs->render()
);
$jsTable = $this->Table->getJsTableObject($tableId);

echo $this->ModalForm
    ->create('edit-technical-archive', ['size' => 'large'])
    ->modal(__("Modification de l'archive technique"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "TechnicalArchives")')
    ->output(
        'function',
        'editTechnicalArchive',
        '/technical-archives/edit'
    )
    ->generate();

$buttons = [];
if ($this->Acl->check('/users/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-technical-archive', ['size' => 'large'])
        ->modal(__("Ajouter une archive technique"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "TechnicalArchives")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter une archive technique")),
            '/TechnicalArchives/add'
        )
        ->generate(['class' => 'btn btn-success float-none', 'type' => 'button']);
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalView->create('modal-view-technical-archive', ['size' => 'large'])
    ->modal(__("Visualisation d'une archive technique"))
    ->output('function', 'actionViewTechnicalArchive', '/technical-archives/view')
    ->generate();

echo $this->Filter->create('index-users-filter')
    ->saves($savedFilters)
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'type',
        [
            'label' => __("Type d'archive"),
            'options' => $types,
            'multiple' => true,
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->generateSection();

require 'ajax_index.php';
?>
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });
</script>
