<?php

/**
 * @var Asalae\View\AppView $this
 */
if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
$deleteUrl = $this->Url->build('/technical-archives/delete');

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'TechnicalArchives.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-technical-archive-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
                'callback' => 'TableHelper.wordBreak()',
            ],
            'TechnicalArchives.created' => [
                'label' => __("Date de création"),
                'order' => 'created',
                'type' => 'datetime',
            ],
            'TechnicalArchives.typetrad' => [
                'label' => __("Type d'archive"),
            ],
            'TechnicalArchives.archival_agency_identifier' => [
                'label' => __("Identifiant"),
                'display' => false,
                'callback' => 'TableHelper.wordBreak()',
            ],
            'TechnicalArchives.original_size' => [
                'label' => __("Taille des fichiers"),
                'callback' => 'TableHelper.readableBytes',
                'display' => false,
            ],
            'TechnicalArchives.original_count' => [
                'label' => __("Nombre de fichiers"),
                'display' => false,
            ],
            'TechnicalArchives.timestamp_size' => [
                'label' => __("Taille de l'horodatage"),
                'callback' => 'TableHelper.readableBytes',
                'display' => false,
            ],
            'TechnicalArchives.timestamp_count' => [
                'label' => __("Nombre de jetons d'horodatage"),
                'display' => false,
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'TechnicalArchives.id',
            'favorites' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewTechnicalArchive({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/technical-archives/view'),
                'params' => ['TechnicalArchives.id', 'TechnicalArchives.name'],
            ],
            [
                'onclick' => "editTechnicalArchive({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'display' => $this->Acl->check('/technical-archives/edit'),
                'displayEval' => 'data[{index}].TechnicalArchives.editable',
                'params' => ['TechnicalArchives.id', 'TechnicalArchives.name'],
            ],
            [
                'data-callback'
                => "TableGenericAction.deleteAction({$jsTable}, '$deleteUrl')({0}, false)",
                'type' => 'button',
                'class' => 'btn-link delete',
                'data-action' => __("Supprimer"),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => __("Supprimer {0}", '{1}'),
                'aria-label' => __("Supprimer {0}", '{1}'),
                'confirm' => __("Êtes-vous sûr de vouloir supprimer cette archive technique ?"),
                'display' => $this->Acl->check('/technical-archives/delete'),
                'displayEval' => 'data[{index}].TechnicalArchives.deletable',
                'params' => ['TechnicalArchives.id', 'TechnicalArchives.name'],
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des archives techniques"),
        'table' => $table,
    ]
);
