<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Csv->resultTable(
    [
        'name' => __("Nom"),
        'created' => __("Date de création"),
        'typetrad' => __("Type d'archive"),
        'archival_agency_identifier' => __("Identifiant (côte)"),
        'original_size' => __("Taille des fichiers"),
        'original_count' => __("Nombre de fichiers"),
        'timestamp_size' => __("Taille de l'horodatage"),
        'timestamp_count' => __("Nombre de jetons d'horodatage"),
    ],
    $results
);
