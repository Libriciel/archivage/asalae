<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Utility\Hash;

?>
<script>
    function renderExt(value, context) {
        if (typeof context.name === 'undefined') {
            return null;
        }
        var ext = context.name.split('.').pop().toLowerCase();
        return '<div class="prev-ext"><i class="fa fa-file-o" aria-hidden="true"></i><span>' + ext + '</span></div>';
    }

    function selectableUpload(value, context) {
        return '<input type="checkbox" '
            + 'name="fileuploads[_ids][]" '
            + 'aria-label="' + context.name + '" '
            + 'value="' + context.id + '" '
            + 'checked="checked">';
    }

    var downloadable = <?=$this->Acl->check($url = '/technical-archives/download-file') ? 'true' : 'false'?>;

    function filenameToLink(value, context) {
        if (downloadable) {
            var link = $('<a target="_blank"></a>').text(value).attr('download', value);
            link.attr('href', '<?=$this->Url->build($url)?>/' + context.id);
            return link;
        } else {
            return value;
        }
    }
</script>
<?php
$tabs1 = $this->Form->create(
    $entity,
    ['id' => 'edit-technical-archive-form', 'idPrefix' => 'edit-technical-archive']
);

$tabs1 .= $this->Form->control(
    'name',
    [
        'label' => __("Nom"),
    ]
);
$tabs1 .= $this->Form->control(
    'technical_archive_units.0.description',
    [
        'label' => __("Description"),
    ]
);
$tabs1 .= $this->Form->control(
    'technical_archive_units.0.history',
    [
        'label' => __("Historique de conservation"),
    ]
);
$tabs1 .= $this->Form->control(
    'technical_archive_units.0.oldest_date',
    [
        'label' => __("Date extrême la plus ancienne"),
        'type' => 'text',
        'append' => $this->Date->picker(
            '#edit-technical-archive-technical-archive-units-0-oldest-date'
        ),
        'class' => 'datepicker',
    ]
);
$tabs1 .= $this->Form->control(
    'technical_archive_units.0.latest_date',
    [
        'label' => __("Date extrème la plus récente"),
        'type' => 'text',
        'append' => $this->Date->picker(
            '#edit-technical-archive-technical-archive-units-0-latest-date'
        ),
        'class' => 'datepicker',
    ]
);

$uploads = $this->Upload
    ->create($uploadId, ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'select' => ['label' => __("Sélectionner"), 'class' => 'radio-td', 'callback' => 'selectableUpload'],
            'ext' => ['label' => false, 'callback' => 'renderExt'],
            'name' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
            'message' => ['label' => __("Message"), 'class' => 'message', 'style' => 'min-width: 250px'],
            'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
        ]
    )
    ->data($uploadData ?? [])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    )
    ->actions(
        [
            [
                'href' => "/download/file/{0}",
                'download' => '{1}',
                'target' => '_blank',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => __("Télécharger {0}", '{1}'),
                'aria-label' => __("Télécharger {0}", '{1}'),
                'params' => ['id', 'name'],
            ],
            function ($table) {
                /** @var Asalae\View\AppView $this */
                $url = $this->Url->build('/Upload/delete');
                return [
                    'onclick' => "GenericUploader.fileDelete({$table->tableObject}, '$url', {0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name'],
                ];
            },
        ]
    );
$jsUpload = $uploads->jsObject;
$jsTableUpload = $this->Upload->getTableId($uploadId);

$tabs1 .= $this->Html->tag(
    'div.form-group.fake-input',
    $this->Html->tag('span.fake-label', __("Ajouter des fichiers"))
    . $uploads->generate(
        [
            'singleFile' => false,
            'target' => $this->Url->build('/Upload/index/?replace=true'),
            'autoretry' => false,
        ]
    )
);

$tabs1 .= $this->Form->end();

/**
 * Tab 2
 */
$tableId = 'edit-technical-archive-files';
$tableFiles = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover'])
    ->fields(
        [
            'filename' => [
                'label' => __("Nom de fichier"),
                'callback' => 'filenameToLink',
            ],
            'stored_file.size' => [
                'label' => __("Taille"),
                'callback' => 'TableHelper.readableBytes',
            ],
            'meta' => [
                'label' => __("Meta-donnés"),
                'target' => '',
                'thead' => [
                    'stored_file.hash' => ['label' => __("Empreinte du fichier")],
                    'stored_file.hash_algo' => ['label' => __("Algorithme de hashage")],
                    'mime' => ['label' => __("Type MIME")],
                    'format' => ['label' => __("Format")],
                ],
            ],
        ]
    )
    ->data(Hash::get($entity, 'technical_archive_units.0.archive_binaries') ?? [])
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'href' => "/technical-archives/download-file/{0}",
                'download' => '{1}',
                'target' => '_blank',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => __("Télécharger {0}", '{1}'),
                'aria-label' => __("Télécharger {0}", '{1}'),
                'display' => $this->Acl->check('/technical-archives/download-file'),
                'params' => ['id', 'filename'],
            ],
            function ($table) {
                /** @var Asalae\View\AppView $this */
                $url = $this->Url->build('/technical-archives/delete-file');
                return [
                    'onclick' => "GenericUploader.fileDelete({$table->tableObject}, '$url', {0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'display' => $this->Acl->check('/technical-archives/delete-file'),
                    'params' => ['id', 'filename'],
                ];
            },
        ]
    );

/**
 * Tabs
 */
$tabs = $this->Tabs->create('edit-technical-archive-tabs-content', ['class' => 'row no-padding']);
$tabs->add(
    'edit-technical-archive-editor-tab',
    $this->Fa->i('fa-file-code-o', __("Détails")),
    $tabs1
);

$tabs->add(
    'edit-technical-archive-files-tab',
    $this->Fa->i('fa-paperclip', __("Fichiers d'archive")),
    $tableFiles->generate()
);
echo $tabs;
