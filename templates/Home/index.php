<?php

/**
 * @var Asalae\View\AppView $this
 */

use Asalae\View\AppView;
use Cake\I18n\I18n;
use Cake\Utility\Hash;

$this->Breadcrumbs->add(__("Tableau de bord"));

// Titre de la page
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->charte('Tableau de bord', __("Tableau de bord")))
    . $this->Breadcrumbs->render()
);

$buttons = [];
if ($this->Acl->check('/transfers/add1')) {
    $buttons[] = $this->Html->link(
        $this->Fa->charte('Ajouter', __("Créer un transfert")),
        '/transfers/index-preparating?add=form',
        ['class' => 'btn btn-success', 'escape' => false]
    );
    $buttons[] = $this->Html->link(
        $this->Fa->charte('Ajouter', __("Créer un transfert par upload de bordereau")),
        '/transfers/index-preparating?add=upload',
        ['class' => 'btn btn-success', 'escape' => false]
    );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->ModalView->create('view-transfer-home', ['size' => 'large'])
    ->modal(__("Visualisation d'un transfert"))
    ->output('function', 'viewTransfer', '/Transfers/view')
    ->generate();

echo $this->ModalForm->create('validation-processes-home', ['size' => 'large'])
    ->modal(__("Prendre une décision"))
    ->javascriptCallback('afterValidate')
    ->output('function', 'processTransfer', '/validation-processes/process-my-transfer');

/**
 * Graph
 */
echo $this->Html->tag('div.container');
echo $this->Html->tag('div.row');
echo $this->Html->tag('div.col-lg-8.col-md-12');
echo $this->Html->tag('canvas#chart-transfers', '', ['width' => '400', 'height' => '400']);
echo $this->Html->tag('/div');
echo $this->Html->tag('div.col-lg-4.col-md-12');
echo $this->Html->tag('canvas#chart-ratio-conform', '', ['width' => '400', 'height' => '400']);
echo $this->Html->tag('/div');
echo $this->Html->tag('/div');
echo $this->Html->tag('/div');

/**
 * Rendu de la vignette
 * @param AppView $view
 * @param array   $values
 * @param bool    $showErrors
 */
$buildInnerVignetteValidation = function (AppView $view, array $values, bool $showErrors) {
    echo $view->Html->tag('tr');
    echo $view->Html->tag('td');

    // Date | identifier
    echo $view->Html->tag('div.td-group');
    echo $view->Html->tag('span.td-group-fieldvalue.h4.h5', h(Hash::get($values, 'transfer.transfer_date')));
    echo $view->Html->tag(
        'span.td-group-fieldvalue.h4.float-right',
        h(Hash::get($values, 'transfer.transfer_identifier'))
    );
    echo $view->Html->tag('/div');
    // étape
    echo $view->Html->tag('div.td-group');
    echo $view->Html->tag('span.td-group-fieldname', __("étape"));
    echo $view->Html->tag('span.td-group-fieldvalue', h(Hash::get($values, 'current_stage.name')));
    echo $view->Html->tag('/div');
    // versant
    echo $view->Html->tag('div.td-group');
    echo $view->Html->tag('span.td-group-fieldname', __("versant"));
    echo $view->Html->tag(
        'span.td-group-fieldvalue',
        h(Hash::get($values, 'transfer.transferring_agency.name'))
    );
    echo $view->Html->tag('/div');
    // erreurs
    if ($showErrors) {
        echo $view->Html->tag('div.td-group');
        echo $view->Html->tag('span.td-group-fieldname', __("erreurs"));
        echo $view->Html->tag('ul');
        foreach (Hash::extract($values, 'transfer.transfer_errors.{n}.message') as $error) {
            echo $view->Html->tag('li', h($error));
        }
        echo $view->Html->tag('/ul');
        echo $view->Html->tag('/div');
    }

    echo $view->Html->tag('/td');

    // actions
    echo $view->Html->tag('td.action');
    echo $view->Html->tag('div.td-group');
    echo $view->Html->tag(
        'button',
        $view->Fa->charte('Visualiser'),
        [
            'onclick' => 'viewTransfer(' . h(Hash::get($values, 'transfer.id')) . ')',
            'type' => 'button',
            'class' => 'btn-link',
            'title' => $title = __("Visualiser {0}", h(Hash::get($values, 'transfer.transfer_identifier'))),
            'aria-label' => $title,
        ]
    );
    echo $view->Html->tag('/div');
    echo $view->Html->tag('div.td-group');
    echo $view->Html->tag(
        'button',
        $view->Fa->i('fa-cogs'),
        [
            'onclick' => 'processTransfer(' . h(Hash::get($values, 'id')) . ')',
            'type' => 'button',
            'class' => 'btn-link',
            'title' => $title = __("Traiter {0}", h(Hash::get($values, 'transfer.transfer_identifier'))),
            'aria-label' => $title,
        ]
    );
    echo $view->Html->tag('/div');
    echo $view->Html->tag('/td');

    echo $view->Html->tag('/tr');
};

if ($this->Acl->check('validation-processes/my-transfers')) {
    echo $this->Html->tag('div.container.container-flex.home');

    /**
     * Conformes
     */
    echo $this->Html->tag('section.bg-white.vignette');
    echo $this->Html->tag(
        'header',
        $this->Html->tag(
            'h2.h4',
            $this->Html->link(
                __("Mes transferts à valider ({0})", $to_validate_count),
                '/validation-processes/my-transfers/validate'
            )
        )
    );
    echo $this->Html->tag('table.table.table-striped.table-hover.table-vignette.smart-td-size');
    echo $this->Html->tag('tbody');
    foreach ($to_validate as $values) {
        $buildInnerVignetteValidation($this, $values, false);
    }

    echo $this->Html->tag('/tbody');
    echo $this->Html->tag('/table');

    echo $this->Html->tag('/section');

    /**
     * Non conformes
     */
    echo $this->Html->tag('section.bg-white.vignette');
    echo $this->Html->tag(
        'header',
        $this->Html->tag(
            'h2.h4',
            $this->Html->link(
                __("Mes transferts non conformes ({0})", $not_conform_count),
                '/validation-processes/my-transfers/invalidate'
            )
        )
    );
    echo $this->Html->tag('table.table.table-striped.table-hover.table-vignette.smart-td-size');
    echo $this->Html->tag('tbody');
    foreach ($not_conform as $values) {
        $buildInnerVignetteValidation($this, $values, true);
    }
    echo $this->Html->tag('/tbody');
    echo $this->Html->tag('/table');
    echo $this->Html->tag('/section');

    echo $this->Html->tag('/div'); // div.container
}

[$lang] = explode('_', I18n::getLocale());
?>
<script>
    $(function () {
        moment.locale('<?=$lang?>');

        function getDayChartLabels() {
            var dt = new Date;
            var labels = [];
            dt.setDate(dt.getDate() - 7);
            for (var i = 7; i > 0; i--) {
                dt.setDate(dt.getDate() + 1);
                labels.push(moment(dt).format('dddd'));
            }
            return labels;
        }

        var ctx = document.getElementById('chart-transfers');
        var chartTransfers = new Chart(ctx, {
            type: 'line',
            data: {
                labels: getDayChartLabels(),
                datasets: [{
                    label: '# Acceptés',
                    data: <?=json_encode(array_values($dataChartTransfersAccepted))?>,
                    backgroundColor: '#90f69f',
                    borderWidth: 1
                }, {
                    label: '# Refusés',
                    data: <?=json_encode(array_values($dataChartTransfersRefused))?>,
                    backgroundColor: '#f69b90',
                    borderWidth: 1
                }, {
                    label: '# En cours',
                    data: <?=json_encode(array_values($dataChartTransfers))?>,
                    backgroundColor: 'rgba(0, 147, 255, 0.4)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true,
                maintainAspectRatio: false,
            }
        });
        var ctx2 = document.getElementById('chart-ratio-conform');
        var chartRatio = new Chart(ctx2, {
            type: 'pie',
            data: {
                labels: [__("acceptés"), __("refusés"), __("en cours")],
                datasets: [{
                    data: <?=json_encode($dataChartRatio)?>,
                    backgroundColor: [
                        'rgba(0, 255, 39, 0.4)',
                        'rgba(255, 29, 0, 0.4)',
                        'rgba(0, 147, 255, 0.4)'
                    ]
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
            }
        });
    });

    /**
     * Callback de l'action valider
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterValidate(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            AsalaeLoading.start();
            $('#validation-processes-home').one('hidden.bs.modal', function () {
                location.reload();
            });
        }
    }
</script>
