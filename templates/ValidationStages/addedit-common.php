<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->Form->control('name', ['label' => __("Nom"), 'required'])
    . $this->Form->control('description', ['label' => __("Description")])
    . $this->Form->control(
        'all_actors_to_complete',
        [
            'label' => __("Type"),
            'options' => $all_actors_to_completes,
            'default' => '0',
        ]
    );
