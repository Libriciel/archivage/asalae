<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->Form->create($entity, ['idPrefix' => 'add-validation-stage']);
require 'addedit-common.php';
echo $this->Form->end();
