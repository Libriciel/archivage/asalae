<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->Form->create($entity, ['idPrefix' => 'edit-validation-stage']);
require 'addedit-common.php';
echo $this->Form->end();
