<?php

/**
 * @var Asalae\View\AppView $this
 */

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover'])
    ->fields(
        [
            'ValidationStages.ord' => [
                'label' => __("Ordre"),
                'callback' => 'stageOrderChanger',
            ],
            'ValidationStages.name' => ['label' => __("Nom")],
            'ValidationStages.all_actors_to_completetrad' => [
                'label' => __("Type"),
            ],
            'ValidationStages.validation_actors' => [
                'label' => __("Acteurs"),
                'callback' => 'callbackDisplayActors',
            ],
            'ValidationStages.description' => [
                'label' => __("Description"),
                'display' => false,
            ],
            'ValidationStages.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
            ],
            'ValidationStages.modified' => [
                'label' => __("Date de modification"),
                'type' => 'datetime',
                'display' => false,
            ],
        ]
    )
    ->data($data)
    ->params(['identifier' => 'ValidationStages.id'])
    ->actions(
        [
            [
                'href' => $this->Url->build('/validation-actors/index') . '/{0}',
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-users'),
                'title' => $title = __("Acteurs de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/validation-actors/index'),
                'params' => ['ValidationStages.id', 'ValidationStages.name'],
            ],
            [
                'onclick' => "editValidationStage({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'display' => $this->Acl->check('/validation-stages/edit'),
                'params' => ['ValidationStages.id', 'ValidationStages.name'],
            ],
            function (AsalaeCore\View\Helper\Object\Table $table, Asalae\View\AppView $view) {
                return [
                    'type' => "button",
                    'class' => "btn-link",
                    'data-callback' => sprintf(
                        "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                        $table->tableObject,
                        $view->Url->build($url = '/validation-stages/delete')
                    ),
                    'display' => $view->Acl->check($url),
                    'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => $title = __("Supprimer {0}", '{1}'),
                    'aria-label' => $title,
                    'confirm' => __("Êtes-vous sûr de vouloir supprimer cette étape ?"),
                    'displayEval' => "data[{index}].ValidationStages.deletable",
                    'params' => ['ValidationStages.id', 'ValidationStages.name'],
                ];
            },
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => 'validation-chains-section',
        'title' => __("Liste des étapes de validation"),
        'table' => $table,
    ]
);
