<script>
    function stageOrderChanger(value, context) {
        var input = $('<input style="width:40px">').val(value).data('value', value);
        input.on('change', function () {
            var max = $(this).closest('tbody').find('tr').length;
            var newOrd = $(this).val();
            var id = $(this).closest('tr').attr('data-id');
            if (!AsalaeGlobal.is_numeric(newOrd) || newOrd > max || newOrd < 1) {
                $(this).val($(this).data('value'));
                $('#<?=$tableId?>').find('tr[data-id=' + id + ']')
                    .css({'background-color': '#a94442', 'background-image': 'none'})
                    .animate({'background-color': 'transparent', 'background-image': ''}, function () {
                        $(this).css({'background-color': '', 'background-image': ''});
                        $(this).find('input').focus();
                    });

                return;
            }
            $.ajax({
                url: '<?=$this->Url->build('/ValidationStages/changeOrder')?>/' + id + '/' + newOrd,
                headers: {
                    Accept: 'application/json'
                },
                success: changeOrderSuccess(id),
                error: function () {
                    alert(PHP.genericError);
                }
            });
        });
        return input;
    }

    function callbackDisplayActors(value) {
        if (!Array.isArray(value)) {
            return null;
        }
        var ul = $('<ul></ul>');
        var message;

        for (var i = 0; i < value.length; i++) {
            if (value[i].app_type === 'USER') {
                message = __(
                    "{0} ({1} par {2})",
                    value[i].app_typetrad,
                    value[i].user.name ? value[i].user.name : value[i].user.username,
                    value[i].type_validationtrad
                );
            } else {
                message = value[i].app_typetrad;
            }
            ul.append($('<li></li>').text($('<span></span>').html(message).text()));
        }

        return ul;
    }
</script>
<?php
/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Circuits de validation"));
$this->Breadcrumbs->add(__("Circuit {0}", h($chain->get('name'))), '/validation-chains');
$this->Breadcrumbs->add(__("Etapes"));
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-list-ol', __("Etapes de validation")))
    . $this->Breadcrumbs->render()
);

$jsTable = $this->Table->getJsTableObject($tableId);

echo $this->element('modal', ['idTable' => $tableId, 'paginate' => false]);

$buttons = [];
if ($this->Acl->check('/ValidationStages/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-validation-stage-modal')
        ->modal(__("Ajouter une étape"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "ValidationStages")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter une étape")),
            '/ValidationStages/add/' . $chain->get('id')
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}
echo $this->ModalForm
    ->create('edit-validation-stage', ['size' => 'large'])
    ->modal(__("Modifier une étape de circuit de validation"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "ValidationStages")')
    ->output(
        'function',
        'editValidationStage',
        '/ValidationStages/edit'
    )
    ->generate();

require 'ajax_index.php';

echo $this->Html->tag('div.container');
echo $this->Html->link(
    $this->Fa->i('fa-arrow-left', __("Retour aux circuits de validation")),
    $this->Url->build('/validation-chains'),
    ['escape' => false]
);
echo $this->Html->tag('/div');
?>
<script>
    var tableStages = {};
    tableStages = <?=$jsTable?>;

    function changeOrderSuccess(id) {
        return function (content, msg, jqXHR) {
            tableStages.data = [];
            for (var i = 0; i < content.length; i++) {
                tableStages.data.push({ValidationStages: content[i]});
            }
            TableGenerator.appendActions(tableStages.data, tableStages.actions);
            tableStages.generateAll();
            initOrdTable();
            $(tableStages.table).find('tr[data-id=' + id + ']')
                .css({'background-color': '#5cb85c', 'background-image': 'none'})
                .animate({'background-color': 'transparent', 'background-image': ''}, function () {
                    $(this).css({'background-color': '', 'background-image': ''});
                    $(this).find('input').focus();
                });
        }
    }

    function initOrdTable() {
        $('#<?=$tableId?>').tableDnD({
            onDrop: function (table, tr) {
                var newOrd = Array.prototype.indexOf.call(tr.parentNode.children, tr) + 1;
                var id = tr.getAttribute('data-id');
                $.ajax({
                    url: '<?=$this->Url->build('/ValidationStages/changeOrder')?>/'
                        + tr.getAttribute('data-id') + '/' + newOrd,
                    headers: {
                        Accept: 'application/json'
                    },
                    success: changeOrderSuccess(id),
                    error: function () {
                        alert(PHP.genericError);
                    }
                });
            }
        });
    }

    initOrdTable();
    $(tableStages.table).on('change', initOrdTable);
</script>
