<?php

/**
 * @var Asalae\View\AppView $this
 * @var array               $path
 */

echo $this->Html->tag('h4.h2', h(end($path))) . '<hr>';
foreach ($path as $title) {
    $this->Breadcrumbs->add($title);
}
echo $this->Breadcrumbs->render();

echo $this->Html->tag('table.table.table-striped.table-hover.fixed-left-th');
echo $this->Html->tag('tbody');

foreach ($nodes as $nodeName => $values) {
    echo $this->Html->tag('tr');
    echo $this->Html->tag('th', $nodeName, ['rowspan' => count($values)]);
    $prev = false;
    foreach ($values as $value) {
        if ($prev) {
            echo $this->Html->tag('/tr') . $this->Html->tag('tr');
        }
        $valueField = '';
        foreach ($value['attrs'] as $attrName => $attrValue) {
            $valueField .= $this->Html->tag('div.td-group-sub-sub');
            $valueField .= $this->Html->tag('span.td-group-fieldname', $this->Fa->i('fa-tag', $attrName));
            $valueField .= $this->Html->tag('span.td-group-fieldvalue', $attrValue);
            $valueField .= $this->Html->tag('/div');
        }
        $valueField = $valueField && trim($value['value']) ? $valueField . '<hr>' : $valueField;
        $valueField .= $value['value'];
        echo $this->Html->tag('td', $valueField);
        $prev = true;
    }
    echo $this->Html->tag('/tr');
}

echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');

echo $this->Html->tag(
    'button',
    $this->Fa->i('fa-times', __("Fermer")),
    [
        'type' => 'button',
        'class' => 'btn btn-default float-right',
        'onclick' => "$('#view-data-description').slideUp(400, function() { $(this).empty().show(); })",
    ]
);
