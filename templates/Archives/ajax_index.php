<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

$view = $this;
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Archives.archival_agency_identifier' => [
                'label' => __("Cote"),
                'order' => 'archival_agency_identifier',
                'callback' => 'TableHelper.wordBreak()',
                'filter' => [
                    'archival_agency_identifier[0]' => [
                        'id' => 'filter-archival_agency_identifier-0',
                        'label' => false,
                        'aria-label' => __("Cote"),
                    ],
                ],
            ],
            'Archives.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'callback' => 'TableHelper.wordBreak()',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'Archives.description' => [
                'label' => __("Description"),
                'display' => false,
            ],
            'Archives.statetrad' => [
                'label' => __("Etat"),
                'titleEval' => 'Archives.statetrad_title',
                'filter' => [
                    'state[0]' => [
                        'id' => 'filter-state-0',
                        'label' => false,
                        'aria-label' => __("Etat"),
                        'options' => $states,
                    ],
                ],
            ],
            'Archives.transferring_agency.name' => [
                'label' => __("Service versant"),
                'filter' => [
                    'transferring_agency_id[0]' => [
                        'id' => 'filter-transferring_agency_id-0',
                        'label' => false,
                        'aria-label' => __("Service versant"),
                        'options' => $transferring_agencies,
                    ],
                ],
            ],
            'Archives.originating_agency.name' => [
                'label' => __("Service producteur"),
                'filter' => [
                    'originating_agency_id[0]' => [
                        'id' => 'filter-originating_agency_id-0',
                        'label' => false,
                        'aria-label' => __("Service producteur"),
                        'options' => $originating_agencies,
                    ],
                ],
            ],
            'Archives.profile.name' => [
                'label' => __("Profil d'archive"),
                'filter' => [
                    'profile_id[0]' => [
                        'id' => 'filter-profile_id-0',
                        'label' => false,
                        'aria-label' => __("Profil d'archive"),
                        'options' => $profiles,
                    ],
                ],
            ],
            'Archives.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'Archives.agreement.name' => [
                'label' => __("Accord de versement"),
                'display' => false,
                'filter' => [
                    'agreement_id[0]' => [
                        'id' => 'filter-agreement_id-0',
                        'label' => false,
                        'aria-label' => __("Accord de versement"),
                        'options' => $agreements,
                    ],
                ],
            ],
            'Archives.dates' => [
                'label' => __("Dates extrêmes"),
                'display' => false,
            ],
            'Archives.appraisal_rule.appraisal_rule_code.name' => [
                'label' => __("DUA"),
                'display' => false,
            ],
            'Archives.appraisal_rule.end_date' => [
                'label' => __("Date de fin de la DUA"),
                'display' => false,
                'type' => 'date',
                'order' => 'dua_end',
            ],
            'Archives.appraisal_rule.final_action_codetrad' => [
                'label' => __("Sort final"),
                'display' => false,
                'filter' => [
                    'final_action_code[0]' => [
                        'id' => 'filter-final-action-code-0',
                        'label' => false,
                        'aria-label' => __("Sort final"),
                        'options' => $final_action_codes,
                    ],
                ],
            ],
            'Archives.access_rule.access_rule_code.code' => [
                'label' => __("Code de la règle de restriction d'accès"),
                'display' => false,
            ],
            'Archives.access_rule.end_date' => [
                'label' => __("Date de fin de la restriction d'accès"),
                'display' => false,
                'type' => 'date',
                'order' => 'access_end',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'access_end[0]' => [
                        'id' => 'filter-access_end-0',
                        'label' => __("Date de fin"),
                        'aria-label' => __("Date de fin de la restriction d'accès"),
                        'prepend' => $this->Input->operator('dateoperator_access_end[0]', '>='),
                        'append' => $this->Date->picker('#filter-access_end-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'access_end[1]' => [
                        'id' => 'filter-access_end-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_access_end[1]', '<='),
                        'append' => $this->Date->picker('#filter-access_end-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'Archives.original_size' => [
                'label' => __("Taille totale des fichiers"),
                'callback' => 'TableHelper.readableBytes',
                'filter' => [
                    'original_size[0][value]' => [
                        'id' => 'filter-original_size-0',
                        'label' => __("Taille totale des fichiers"),
                        'prepend' => $this->Input->operator(
                            'original_size[0][operator]',
                            '>=',
                            ['id' => 'filter-original_size-0-operator']
                        ),
                        'append' => $this->Input->mult(
                            'original_size[0][mult]',
                            'mo',
                            ['id' => 'filter-original_size-0-mult']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'original_size[1][value]' => [
                        'id' => 'filter-original_size-1',
                        'label' => false,
                        'aria-label' => __("Taille 2"),
                        'prepend' => $this->Input->operator(
                            'original_size[1][operator]',
                            '<=',
                            ['id' => 'filter-original_size-1-operator']
                        ),
                        'append' => $this->Input->mult(
                            'original_size[1][mult]',
                            'mo',
                            ['id' => 'filter-original_size-1-mult']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
            'Archives.original_count' => [
                'label' => __("Nombre de fichiers"),
                'display' => false,
                'order' => 'original_count',
                'filter' => [
                    'original_count[0][value]' => [
                        'id' => 'filter-original_count-0',
                        'label' => __("Nombre de fichiers"),
                        'prepend' => $this->Input->operator(
                            'original_count[0][operator]',
                            '>=',
                            ['id' => 'filter-original_count-0-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'original_count[1][value]' => [
                        'id' => 'filter-original_count-1',
                        'label' => false,
                        'aria-label' => __("Quantité 2"),
                        'prepend' => $this->Input->operator(
                            'original_count[1][operator]',
                            '<=',
                            ['id' => 'filter-original_count-1-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
            'Archives.units_count' => [
                'label' => __("Nombre d'unités d'archives"),
                'display' => false,
                'order' => 'units_count',
                'filter' => [
                    'units_count[0][value]' => [
                        'id' => 'filter-units_count-0',
                        'label' => __("Nombre d'unités d'archives"),
                        'prepend' => $this->Input->operator(
                            'units_count[0][operator]',
                            '>=',
                            ['id' => 'filter-units_count-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'units_count[1][value]' => [
                        'id' => 'filter-units_count-1',
                        'label' => false,
                        'aria-label' => __("Quantité 2"),
                        'prepend' => $this->Input->operator(
                            'units_count[1][operator]',
                            '<=',
                            ['id' => 'filter-units_count-1-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
            'Archives.transfers.0.message_versiontrad' => [
                'label' => __("Version SEDA"),
                'display' => false,
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'Archives.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewArchive({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archives/view'),
                'params' => ['Archives.id', 'Archives.name'],
            ],
            [
                'onclick' => "descriptionArchive({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archives/description'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'data[{index}].Archives.is_built'
                    : 'data[{index}].Archives.is_built && data[{index}].Archives.description_xml_archive_file.is_large',
                'params' => ['Archives.id', 'Archives.name'],
            ],
            [
                'href' => $this->Url->build('/archives/display-xml') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archives/display-xml'),
                'displayEval' => 'data[{index}].Archives.is_built '
                    . '&& !data[{index}].Archives.description_xml_archive_file.is_large',
                'params' => ['Archives.id', 'Archives.name'],
            ],
            [
                'href' => $this->Url->build('/archives/archiving-certificate') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fas fa-award'),
                'title' => $title = __("Afficher l'attestation d'archivage {0}", '{1}'),
                'data-action' => __("Attestation d'archivage"),
                'aria-label' => $title,
                'data-renewable' => true, // force l'analyse d'intégrité
                'display' => $this->Acl->check('/archives/archiving-certificate'),
                'displayEval' => 'data[{index}].Archives.is_built',
                'params' => ['Archives.id', 'Archives.name'],
            ],
            [
                'href' => "/Archives/download-pdf/{0}",
                'download' => '{2}',
                'target' => '_blank',
                'data-action' => __("Télécharger le PDF"),
                'label' => $this->Fa->i('fa-file-pdf'),
                'title' => $title = __("Télécharger le PDF de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archives/download-pdf'),
                'displayEval' => 'data[{index}].Archives.is_built '
                    . '&& !data[{index}].Archives.description_xml_archive_file.is_large',
                'params' => ['Archives.id', 'Archives.name', 'Archives.pdf_basename'],
            ],
            [
                'onclick' => 'archivePdfDownload({0})',
                'type' => 'button',
                'class' => 'btn-link',
                'data-action' => __("Télécharger le PDF  (large)"),
                'label' => $this->Fa->i('fa-file-pdf'),
                'title' => $title = __("Télécharger le PDF de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archives/download-pdf'),
                'displayEval' => 'data[{index}].Archives.is_built '
                    . '&& data[{index}].Archives.description_xml_archive_file.is_large',
                'params' => ['Archives.id', 'Archives.name'],
            ],
            [
                'onclick' => "lifecycleAction({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-list-ol'),
                'title' => $title = __("Afficher le cycle de vie {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archives/lifecycle'),
                'displayEval' => 'data[{index}].Archives.is_built',
                'params' => ['Archives.id', 'Archives.name'],
            ],
            [
                'type' => 'button',
                'data-callback' => 'archiveFileDownload({0})',
                'confirm' => __("Voulez-vous télécharger les fichiers de cette archive ?"),
                'label' => $view->Fa->i('fa-file-archive-o'),
                'class' => 'btn-link',
                'title' => $title = __("Télécharger les fichiers de {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Télécharger le zip"),
                'display' => $this->Acl->check('/archives/downloadFiles'),
                'displayEval' => "data[{index}].Archives.state === 'available' "
                    . "|| data[{index}].Archives.state === 'freezed'",
                'params' => ['Archives.id', 'Archives.name'],
            ],
            [
                'onclick' => "freezeAction({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('far fa-snowflake'),
                'title' => $title = __("Gel de l'archive {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Geler l'archive"),
                'display' => $this->Acl->check('/archives/freeze'),
                'displayEval' => 'data[{index}].Archives.freezable',
                'params' => ['Archives.id', 'Archives.name'],
            ],
            [
                'onclick' => "unfreezeAction({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fas fa-fire-alt'),
                'title' => $title = __("Dégel de l'archive {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Dégeler l'archive"),
                'display' => $this->Acl->check('/archives/unfreeze'),
                'displayEval' => 'data[{index}].Archives.unfreezable',
                'params' => ['Archives.id', 'Archives.name'],
            ],
        ]
    );

$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => $titleTable,
        'table' => $table,
    ]
);
