<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $entity
 */

use AsalaeCore\I18n\Date as CoreDate;
use Asalae\Model\Table\ArchivesTable;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

?>
<script>
    var downloadable = <?=$this->Acl->check($url = '/archive-binaries/download') ? 'true' : 'false'?>;

    function filenameToLink(value, context) {
        if (downloadable) {
            var link = $('<a target="_blank"></a>')
                .html(TableHelper.wordBreak("/")(value))
                .attr('download', context.basename);
            link.attr('href', '<?=$this->Url->build($url)?>/' + context.id + '/' + context.url_basename);
            return link;
        } else {
            return TableHelper.wordBreak("/")(value);
        }
    }
</script>
<?php
$view = $this;

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($entity->get('name')));
$infos .= $this->Html->tag('p', h($entity->get('description')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Description"));
$infos .= $this->ViewTable->generate(
    $entity,
    [
        __("Cote") => 'archival_agency_identifier',
        __("Nom") => 'name',
        __("Description") => 'description',
        __("Profil d'archive") => $entity->get('profile')
            ? '{profile.name} ({profile.identifier})'
            : null,
        __("Date de création") => 'created',
        __("Statut") => 'statetrad',
        __("Service versant") => '{transferring_agency.name} ({transferring_agency.identifier})',
        __("Service producteur") => '{originating_agency.name} ({originating_agency.identifier})',
        __("Accord de versement") => $entity->get('agreement')
            ? '{agreement.name} ({agreement.identifier})'
            : null,
        __("Dates extrêmes") => '{oldest_date|default("[nd]")} - {latest_date|default("[nd]")}',
        __("Communicabilité") => function (EntityInterface $entity) {
            $endDate = Hash::get($entity, 'access_rule.end_date');
            $reportable = ($endDate instanceof CakeDate || $endDate instanceof CoreDate)
                && $endDate->format('Y') <= (int)date('Y') + 120
                ? __("librement communicable : {0}", $endDate)
                : __("non communicable (illimité)");
            return __(
                "Date de départ du calcul : {0}, code : {1}, {2}",
                Hash::get($entity, 'access_rule.start_date'),
                Hash::get($entity, 'access_rule.access_rule_code.code'),
                $reportable
            );
        },
        __("Date de fin de dérogation (communicabilité)")
            => '{first_archive_unit.max_aggregated_access_end_date|default("[nd]")}',
        __("DUA et sort final") => __(
            "Date de départ de calcul : {0}, durée : {1}, sort final : {2}",
            '{appraisal_rule.start_date}',
            '{appraisal_rule.appraisal_rule_code.name}',
            '{appraisal_rule.final_action_codetrad}'
        ),
    ]
);

$infos .= $this->Html->tag('h4', __("Fichiers d'archives"));
/** @var EntityInterface|array $attestation */
$attestation = Hash::get($entity, 'attestation_pdf_archive_file') ?: [];
$infos .= $this->ViewTable->generate(
    $entity,
    [
        __("Description") => function (EntityInterface $entity) use ($view) {
            $filename = Hash::get($entity, 'description_xml_archive_file.filename');
            return $view->Acl->check('/archive-files/download')
                ? $view->Html->link(
                    urldecode($filename),
                    sprintf(
                        '/archive-files/download/%d/%s',
                        Hash::get($entity, 'description_xml_archive_file.id'),
                        basename($filename)
                    ),
                    ['download' => $filename]
                )
                : $filename;
        },
        __("Cycle de vie") => function (EntityInterface $entity) use ($view) {
            $filename = Hash::get($entity, 'lifecycle_xml_archive_file.filename');
            return $view->Acl->check('/archive-files/download')
                ? $view->Html->link(
                    urldecode($filename),
                    sprintf(
                        '/archive-files/download/%d/%s',
                        Hash::get($entity, 'lifecycle_xml_archive_file.id'),
                        basename($filename)
                    ),
                    ['download' => $filename]
                )
                : $filename;
        },
        __("Fichier de transfert") => function (EntityInterface $entity) use ($view) {
            $access = $view->Acl->check('/archive-files/download');
            $files = $entity->get('transfer_xml_archive_files');
            $dlLink = function ($id, $filename) use ($view) {
                return $view->Html->link(
                    urldecode($filename),
                    sprintf(
                        '/archive-files/download/%d/%s',
                        $id,
                        basename($filename)
                    ),
                    ['download' => $filename]
                );
            };
            if (count($files) <= 1) {
                $id = Hash::get($entity, 'transfer_xml_archive_files.0.id');
                $filename = Hash::get($entity, 'transfer_xml_archive_files.0.filename');
                return $access ? $dlLink($id, $filename) : $filename;
            }
            $files = [];
            foreach ($entity->get('transfer_xml_archive_files') as $archiveFile) {
                $filename = $archiveFile->get('filename');
                $li = $access ? $dlLink($archiveFile->id, $filename) : $filename;
                $files[] = $view->Html->tag('li', $li);
            }
            return $view->Html->tag('ul', implode("\n", $files));
        },
    ]
    + ($entity->get('attestation_pdf_archive_file')
        ? [
            __("Attestation d'archivage") => function (EntityInterface $entity) use ($view) {
                $filename = Hash::get($entity, 'attestation_pdf_archive_file.filename');
                return $view->Html->link(
                    urldecode($filename),
                    sprintf(
                        '/archive-files/download/%d/%s',
                        Hash::get($entity, 'attestation_pdf_archive_file.id'),
                        basename($filename)
                    ),
                    ['download' => $filename]
                );
            },
        ]
        : [])
);

$infos .= $this->Html->tag('h4', __("Stockage"));
$infos .= $this->ViewTable->generate(
    $entity,
    [
        __("Rangement") => 'storage_path',
        __("Espace de conservation sécurisé") => 'secure_data_space.name',
        __("Intégrité des fichiers") => function (EntityInterface $entity) use ($view) {
            if ($entity->get('is_integrity_ok') === null || !$entity->get('integrity_date')) {
                $message = __("L'intégrité n'a jamais été vérifiée.");
            } else {
                /** @var CakeDateTime $date */
                $date = $entity->get('integrity_date');
                $message = $entity->get('is_integrity_ok')
                    ? __("Tous les fichiers sont intègres (vérification effectuée le {0})", $date->nice())
                    : __(
                        "Des fichiers non intègres ont été détectés (vérification effectuée le {0})",
                        $date->nice()
                    );
            }
            $message = $view->Html->tag('span#integrity-message-view', $message);
            if ($view->Acl->check('/archives/check-integrity')) {
                $message .= $view->Html->tag(
                    'button',
                    __("Vérifier maintenant l'intégrité des fichiers"),
                    [
                        'type' => 'button',
                        'class' => 'btn btn-link',
                        'onclick' => 'actionCheckIntegrity()',
                    ]
                );
            }
            return $message;
        },
        __("Fichiers stockés") => function (EntityInterface $entity) use ($view) {
            $counts = [
                'original_count',
                'preservation_count',
                'dissemination_count',
                'timestamp_count',
                'management_count',
            ];
            $count = 0;
            foreach ($counts as $field) {
                $count += $entity->get($field);
            }
            $counts = [
                'original_size',
                'preservation_size',
                'dissemination_size',
                'timestamp_size',
                'management_size',
            ];
            $size = 0;
            foreach ($counts as $field) {
                $size += $entity->get($field);
            }

            $td = __(
                "{0} et une taille de {1} ({2} Octets)",
                $count,
                $view->Number->toReadableSize($size),
                $size
            );
            $td .= $view->Html->tag('ul');

            $originalCount = __n(
                "{0} fichier original",
                "{0} fichiers originaux",
                $c = $entity->get('original_count'),
                $c
            );
            $originalSize = $c ? __n(
                "et une taille de {0} ({1} Octet)",
                "et une taille de {0} ({1} Octets)",
                $c = $entity->get('original_size'),
                '{original_size|toReadableSize}',
                $c
            ) : '';
            $td .= $view->Html->tag('li', "$originalCount $originalSize");

            $ArchiveBinaryConversions = TableRegistry::getTableLocator()->get('ArchiveBinaryConversions');
            $preservationCount = __n(
                "{0} fichier de conservation",
                "{0} fichiers de conservation",
                $c = $entity->get('preservation_count'),
                $c
            );
            $preservationSize = $c ? __n(
                "et une taille de {0} ({1} Octet)",
                "et une taille de {0} ({1} Octets)",
                $c = $entity->get('preservation_size'),
                '{preservation_size|toReadableSize}',
                $c
            ) : '';
            $conversionPreservationCount = $ArchiveBinaryConversions->find()
                ->where(['archive_id' => $entity->id, 'type' => 'preservation'])
                ->count();
            $conversionPreservationCount = $conversionPreservationCount
                ? __("({0} en attente de conversion)", $conversionPreservationCount)
                : '';

            $td .= $view->Html->tag('li', "$preservationCount $preservationSize $conversionPreservationCount");

            $disseminationCount = __n(
                "{0} fichier de diffusion",
                "{0} fichiers de diffusion",
                $c = $entity->get('dissemination_count'),
                $c
            );
            $disseminationSize = $c ? __n(
                "et une taille de {0} ({1} Octet)",
                "et une taille de {0} ({1} Octets)",
                $c = $entity->get('dissemination_size'),
                '{dissemination_size|toReadableSize}',
                $c
            ) : '';
            $conversionDisseminationCount = $ArchiveBinaryConversions->find()
                ->where(['archive_id' => $entity->id, 'type' => 'dissemination'])
                ->count();
            $conversionDisseminationCount = $conversionDisseminationCount
                ? __("({0} en attente de conversion)", $conversionDisseminationCount)
                : '';
            $td .= $view->Html->tag('li', "$disseminationCount $disseminationSize $conversionDisseminationCount");

            $timestampCount = __n(
                "{0} fichier de jeton d'horodatage",
                "{0} fichiers de jetons d'horodatage",
                $c = $entity->get('timestamp_count'),
                $c
            );
            $timestampSize = $c ? __n(
                "et une taille de {0} ({1} Octet)",
                "et une taille de {0} ({1} Octets)",
                $c = $entity->get('timestamp_size'),
                '{timestamp_size|toReadableSize}',
                $c
            ) : '';
            $td .= $view->Html->tag('li', "$timestampCount $timestampSize");

            $managementCount = __n(
                "{0} fichier de management",
                "{0} fichiers de management",
                $c = $entity->get('management_count'),
                $c
            );
            $managementSize = $c ? __n(
                "et une taille de {0} ({1} Octet)",
                "et une taille de {0} ({1} Octets)",
                $c = $entity->get('management_size'),
                '{management_size|toReadableSize}',
                $c
            ) : '';
            $td .= $view->Html->tag('li', "$managementCount $managementSize");

            $td .= $view->Html->tag('/ul');
            return $td;
        },
    ]
);

$infos .= $this->Html->tag('/article');

$tabs = $this->Tabs->create('tabs-view-archive', ['class' => 'row no-padding']);

$tabs->add(
    'tab-view-archive-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);

if (
    in_array(
        $entity->get('state'),
        [
            ArchivesTable::S_AVAILABLE,
            ArchivesTable::S_FREEZED,
        ]
    )
) {
    $tableBinaries = $this->Table
        ->create('view-archive-binaries', ['class' => 'table table-striped table-hover'])
        ->url(
            $url = [
                'controller' => 'archives',
                'action' => 'view-binaries',
                $id,
                '?' => [
                    'sort' => 'filename',
                    'direction' => 'asc',
                ],
            ]
        )
        ->fields(
            [
                'filename' => [
                    'label' => __("Nom de fichier"),
                    'callback' => 'filenameToLink',
                    'order' => 'filename',
                    'filter' => [
                        'filename[0]' => [
                            'id' => 'filter-archive-binary-filename-0',
                            'label' => false,
                            'aria-label' => __("Nom de fichier"),
                        ],
                    ],
                ],
                'format' => [
                    'label' => __("Format"),
                    'filter' => [
                        'format[0]' => [
                            'id' => 'filter-archive-binary-format-0',
                            'label' => false,
                            'aria-label' => __("Format"),
                        ],
                    ],
                ],
                'mime' => [
                    'label' => __("Type MIME"),
                    'filter' => [
                        'mime[0]' => [
                            'id' => 'filter-archive-binary-mime-0',
                            'label' => false,
                            'aria-label' => __("Type MIME"),
                        ],
                    ],
                ],
                'stored_file.size' => [
                    'label' => __("Taille"),
                    'callback' => 'TableHelper.readableBytes',
                    'order' => 'size',
                    'filter' => [
                        'size[0][value]' => [
                            'id' => 'filter-archive-binary-size-0',
                            'label' => false,
                            'aria-label' => __("Taille 1"),
                            'prepend' => $this->Input->operator('size[0][operator]', '>='),
                            'append' => $this->Input->mult('size[0][mult]'),
                            'class' => 'with-select',
                            'type' => 'number',
                            'min' => 1,
                        ],
                        'size[1][value]' => [
                            'id' => 'filter-archive-binary-size-1',
                            'label' => false,
                            'aria-label' => __("Taille 2"),
                            'prepend' => $this->Input->operator('size[1][operator]', '<='),
                            'append' => $this->Input->mult('size[1][mult]'),
                            'class' => 'with-select',
                            'type' => 'number',
                            'min' => 1,
                        ],
                    ],
                ],
                'archive_units' => [
                    'label' => __("Lié à l'unité d'archive"),
                    'callback' => 'TableHelper.ul("relative_path")',
                ],
            ]
        )
        ->data($binaries)
        ->params(
            [
                'identifier' => 'id',
            ]
        )
        ->actions(
            [
                [
                    'onclick' => "viewBinary({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->i('fa-list'),
                    'title' => $title = __("Fichiers liés à {0}", '{1}'),
                    'aria-label' => $title,
                    'displayEval' => 'data[{index}].other_datas.length > 0',
                    'params' => ['id', 'filename'],
                ],
                [
                    'href' => "/archive-binaries/download/{0}/{3}",
                    'download' => '{2}',
                    'target' => '_blank',
                    'label' => $this->Fa->charte('Télécharger'),
                    'title' => $title = __("Télécharger {0}", '{1}'),
                    'aria-label' => $title,
                    'display' => $this->Acl->check('/archive-binaries/download'),
                    'params' => ['id', 'filename', 'basename', 'url_basename'],
                ],
                [
                    'href' => '/archive-binaries/open/{0}/{3}',
                    'target' => '_blank',
                    'label' => $this->Fa->i('fas fa-external-link-alt'),
                    'title' => $title = __("Ouvrir dans le navigateur {0}", '{1}'),
                    'aria-label' => $title,
                    'display' => $this->Acl->check('/archive-binaries/open'),
                    'displayEval' => 'data[{index}].openable',
                    'params' => ['id', 'filename', 'basename', 'url_basename'],
                ],
            ]
        );

    $paginator = $this->AjaxPaginator->create('pagination-transfer-view-attachments')
        ->url($url)
        ->table($tableBinaries)
        ->count($countBinaries);

    $archiveFilesTab = $paginator->generateTable();
} elseif (
    in_array(
        $entity->get('state'),
        [ArchivesTable::S_CREATING, ArchivesTable::S_DESCRIBING, ArchivesTable::S_STORING, ArchivesTable::S_MANAGING]
    )
) {
    $archiveFilesTab = $this->Html->tag(
        'p',
        __(
            "Les fichiers de l'archive sont en cours de stockage et seront"
            . " affichés lorsque l'état de l'archive sera 'disponible'."
        ),
        ['class' => 'alert alert-warning']
    );
} else {
    $archiveFilesTab = $this->Html->tag(
        'p',
        __("Les fichiers de l'archive ne sont plus disponibles."),
        ['class' => 'alert alert-warning']
    );
}

$tabs->add(
    'tab-view-archive-binaries',
    $this->Fa->i('fa-file-o', __("Fichiers")),
    $archiveFilesTab
);

$infos = '';
/** @var EntityInterface $transfer */
foreach ($entity->get('transfers') as $transfer) {
    $infos .= $this->Html->tag('article');
    $infos .= $this->Html->tag('header.bottom-space');
    $infos .= $this->Html->tag('h3', h($transfer->get('transfer_identifier')));
    $infos .= $this->Html->tag('p', h($transfer->get('transfer_comment')));
    $infos .= $this->Html->tag('/header');

    $infos .= $this->Html->tag('h4', __("Description"));
    $infos .= $this->ViewTable->generate(
        $transfer,
        [
            __("Identifiant") => 'transfer_identifier',
            __("Commentaire") => 'transfer_comment',
            __("Date de création") => 'created',
            __("Service versant") => 'transferring_agency.name',
            __("Taille totale des fichiers transmis") => 'data_size|default(0)|toReadableSize',
            __("Nombre total des fichiers transmis") => 'data_count',
        ]
    );
    $infos .= $this->Html->tag('/article');
}

$tabs->add(
    'tab-view-archive-transfer',
    $this->Fa->i('fa-exchange', __("Transfert")),
    $infos
);

echo $tabs;
?>
<script>
    function actionCheckIntegrity() {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/archives/check-integrity') . '/' . $id?>',
            success: function (content) {
                $('#integrity-message-view').text(content);
            }
        })
    }
</script>
