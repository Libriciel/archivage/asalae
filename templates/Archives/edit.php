<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Cake\Utility\Hash;

echo $this->Flash->render();

$jsTable = $this->Table->getJsTableObject($tableId = 'edit-archive-keywords');
$keywords = '';
if ($this->Acl->check('/ArchiveKeywords/add')) {
    $keywords = $this->ModalForm->create('add-archive-keyword', ['size' => 'large'])
        ->modal(__("Ajout d'un mot clé"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "ArchiveKeywords")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un mot clé")),
            '/ArchiveKeywords/add/' . $entity->get('id')
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
echo $this->ModalForm
    ->create('edit-archive-keyword', ['size' => 'large'])
    ->modal(__("Modification d'un mot clé"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "ArchiveKeywords")')
    ->output('function', 'loadEditModalArchiveKeyword', '/ArchiveKeywords/edit')
    ->generate();

$keywords .= $this->Table->create($tableId)
    ->fields(
        [
            'content' => ['label' => 'KeywordContent'],
            'reference' => ['label' => 'KeywordReference'],
            'type' => ['label' => 'KeywordType'],
        ]
    )
    ->data(Hash::get($entity, 'first_archive_unit.archive_keywords', []))
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].invalid ? "danger" : ""',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "loadEditModalArchiveKeyword({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-keywords/edit'),
                'params' => ['ArchiveKeywords.id', 'ArchiveKeywords.content'],
            ],
            [
                'type' => "button",
                'class' => "btn-link",
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/archive-keywords/delete')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer ce mot clé"),
                'aria-label' => $title,
                'confirm' => __("Êtes-vous sûr de vouloir supprimer ce mot clé ?"),
                'display' => $this->Acl->check('/archive-keywords/delete'),
                'params' => ['ArchiveKeywords.id'],
            ],
        ]
    )
    ->generate();

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => 'edit-archive',
        'id' => 'edit-archive-form',
        'class' => 'accordion',
    ]
);

echo $this->Html->tag('h3', __("Archive"));
echo $this->Html->tag(
    'div',
    $this->Form->control(
        'name',
        [
            'label' => __("Nom"),
        ]
    )
    . $this->Form->control(
        'agreement_id',
        [
            'label' => __("Accord de versement"),
            'options' => $agreements,
            'empty' => __("-- Sélectionner un accord de versement --"),
        ]
    )
    . $this->Form->control(
        'profile_id',
        [
            'label' => __("Profil d'archive"),
            'options' => $profiles,
            'empty' => __("-- Sélectionner un profil d'archive --"),
        ]
    )
);

echo $this->Html->tag('h3', __("Description"));
echo $this->Html->tag(
    'div',
    $this->Form->control(
        'first_archive_unit.archive_description.description',
        [
            'label' => __("Description"),
        ]
    )
    . $this->Form->control(
        'first_archive_unit.archive_description.oldest_date',
        [
            'label' => __("Date la plus ancienne"),
            'type' => 'text',
            'append' => $this->Date->picker('#edit-archive-first-archive-unit-archive-description-oldest-date'),
            'class' => 'datepicker',
        ]
    )
    . $this->Form->control(
        'first_archive_unit.archive_description.latest_date',
        [
            'label' => __("Date la plus récente"),
            'type' => 'text',
            'append' => $this->Date->picker('#edit-archive-first-archive-unit-archive-description-latest-date'),
            'class' => 'datepicker',
        ]
    )
    . $this->Form->control(
        'first_archive_unit.archive_description.history',
        [
            'label' => __("Historique"),
        ]
    )
    . $this->Form->control(
        'originating_agency_id',
        [
            'label' => __("Service producteur"),
            'options' => $originating_agencies,
            'empty' => __("-- Sélectionner un service producteur --"),
        ]
    )
    . $this->Form->fieldset(
        $keywords,
        ['legend' => __("Indexation")]
    )
    . $this->Html->tag(
        'div.alert.alert-warning',
        __(
            "Attention, les modifications sur les mots-clés sont immédiates"
            . " et ne seront pas annulées si vous cliquez sur le bouton \"Annuler\" du formulaire."
        )
    )
    . $this->Form->fieldset(
        $this->Form->control(
            'first_archive_unit.archive_description.access_rule.access_rule_code_id',
            [
                'label' => __("Délai de communicabilité"),
                'options' => $codes,
                'required' => true,
                'empty' => __("-- Sélectionner un délai --"),
            ]
        )
        . $this->Form->control(
            'first_archive_unit.archive_description.access_rule.start_date',
            [
                'label' => __("Date de départ du calcul (communicabilité)"),
                'type' => 'text',
                'append' => $this->Date->picker(
                    '#edit-archive-first-archive-unit-archive-description-access-rule-start-date'
                ),
                'class' => 'datepicker',
                'placeholder' => __("jj/mm/aaaa"),
            ]
        ),
        ['legend' => __("Restriction d'accès") . ' (' . __("de la description") . ')']
    )
);

echo $this->Html->tag('h3', __("Règles de gestion"));
echo $this->Html->tag(
    'div',
    $this->Form->fieldset(
        $this->Form->control(
            'access_rule.access_rule_code_id',
            [
                'label' => __("Délai de restriction"),
                'options' => $codes,
                'required' => true,
                'empty' => __("-- Sélectionner un délai --"),
            ]
        )
        . $this->Form->control(
            'access_rule.start_date',
            [
                'label' => __("Date de départ du calcul (communicabilité)"),
                'type' => 'text',
                'append' => $this->Date->picker('#edit-archive-access-rule-start-date'),
                'class' => 'datepicker',
                'placeholder' => __("jj/mm/aaaa"),
            ]
        ),
        ['legend' => __("Restriction d'accès")]
    )
    . $this->Form->fieldset(
        $this->Form->control(
            'appraisal_rule.final_action_code',
            [
                'label' => __("Sort final à appliquer"),
                'options' => $finals,
                'empty' => __("-- Choisir un sort final --"),
            ]
        )
        . $this->Form->control(
            'appraisal_rule.appraisal_rule_code_id',
            [
                'label' => __("Durée d'utilité administrative"),
                'options' => $durations,
                'empty' => __("-- Sélectionner une DUA -- "),
            ]
        )
        . $this->Form->control(
            'appraisal_rule.start_date',
            [
                'label' => __("Date de départ du calcul (sort final)"),
                'type' => 'text',
                'append' => $this->Date->picker('#edit-archive-appraisal-rule-start-date'),
                'class' => 'datepicker',
                'placeholder' => __("jj/mm/aaaa"),
            ]
        ),
        ['legend' => __("Sort final")]
    )
);
echo $this->Form->end();
?>
<script>
    $('#edit-archive-form').accordion({header: "h3", heightStyle: "fill"});
</script>
