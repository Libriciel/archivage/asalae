<?php

echo __("identifiant de l'archive") . ' : ' . $archive->get('archival_agency_identifier') . "<br>\n";
echo __("nom de l'archive") . ' : ' . $archive->get('name') . "<br>\n";
echo __("date de création") . ' : ' . $archive->get('created') . "<br>\n";
echo __("nombre fichiers") . ' : ' . $archive->get('original_count') . "<br>\n";
echo __("taille fichiers") . ' : ' . $archive->get('original_size') . "<br>\n";
echo __("statut intégrité") . ' : '
    . (($ok = $archive->get('is_integrity_ok')) === null
        ? __("non déterminé")
        : ($ok ? __("intègre") : __("non intègre"))
    ) . "<br>\n";
echo __("date intégrité") . ' : ' . ($archive->get('integrity_date') ?: 'N/A') . "<br>\n";
