<?php

/**
 * @var Asalae\View\AppView $this
 */

$tabs = $this->Tabs->create('lifecycle-archive', ['class' => 'row no-padding']);

$tableArchive = $this->Table
    ->create('lifecycle-events-archive', ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'archives',
            'action' => 'lifecycle',
            $id,
            '?' => ['type' => 'archive'],
        ]
    )
    ->fields(
        [
            'date' => [
                'label' => __("Date et heure"),
                'type' => 'datetime',
            ],
            'text' => [
                'label' => __("Texte"),
            ],
            'user' => [
                'label' => __("Utilisateur"),
            ],
        ]
    )
    ->data($eventsArchive)
    ->params(['identifier' => 'identifier'])
    ->actions(
        [
        ]
    );
$paginator = $this->AjaxPaginator->create('pagination-lifecycle-events-archive')
    ->url($url)
    ->table($tableArchive)
    ->count($countArchive)
    ->setPaginated($paginated_archive);

$tabs->add(
    'tab-lifecycle-events-archive',
    $this->Fa->i('fa-cogs', __("Gestion de l'archive")),
    $paginator->generateTable()
);

$tableStorage = $this->Table
    ->create('lifecycle-events-storage', ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'archives',
            'action' => 'lifecycle',
            $id,
            '?' => ['type' => 'storage'],
        ]
    )
    ->fields(
        [
            'date' => [
                'label' => __("Date et heure"),
                'type' => 'datetime',
            ],
            'text' => [
                'label' => __("Texte"),
            ],
            'user' => [
                'label' => __("Utilisateur"),
            ],
        ]
    )
    ->data($eventsStorage)
    ->params(['identifier' => 'identifier'])
    ->actions(
        [
        ]
    );
$paginator = $this->AjaxPaginator->create('pagination-lifecycle-events-storage')
    ->url($url)
    ->table($tableStorage)
    ->count($countStorage)
    ->setPaginated($paginated_storage);

$tabs->add(
    'tab-lifecycle-events-storage',
    $this->Fa->i('fa-hdd-o', __("Stockage des fichiers")),
    $paginator->generateTable()
);

$tableIntegrity = $this->Table
    ->create('lifecycle-events-integrity', ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'archives',
            'action' => 'lifecycle',
            $id,
            '?' => ['type' => 'integrity'],
        ]
    )
    ->fields(
        [
            'date' => [
                'label' => __("Date et heure"),
                'type' => 'datetime',
            ],
            'text' => [
                'label' => __("Texte"),
            ],
            'user' => [
                'label' => __("Utilisateur"),
            ],
        ]
    )
    ->data($eventsIntegrity)
    ->params(['identifier' => 'identifier'])
    ->actions(
        [
        ]
    );
$paginator = $this->AjaxPaginator->create('pagination-lifecycle-events-integrity')
    ->url($url)
    ->table($tableIntegrity)
    ->count($countIntegrity)
    ->setPaginated($paginated_integrity);

$tabs->add(
    'tab-lifecycle-events-integrity',
    $this->Fa->i('fa-stethoscope', __("Contrôles d'intégrité")),
    $paginator->generateTable()
);

echo $tabs;
