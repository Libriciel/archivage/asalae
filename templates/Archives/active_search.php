<script>
    function loadSaveSearchArchive(urls, controller, action) {
        console.log('loadSaveSearchArchive()', urls, controller, action);
    }
</script>
<?php
/**
 * @var Asalae\View\AppView $this
 * @var Asalae\Form\SearchArchiveForm $form
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Archives"));
$this->Breadcrumbs->add(__("Recherche sur les entrées"), '/archives/search');
$this->Breadcrumbs->add(__("Recherche non sauvegardée"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa fa-search', __("Recherche sur les entrées")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

echo $this->element('modal', ['idTable' => $tableId]);
$jsTableArchives = $this->Table->getJsTableObject($tableId);

echo $this->ModalView->create('search-view-archive', ['size' => 'modal-xxl'])
    ->modal(__("Visualisation d'une archive"))
    ->output('function', 'viewArchive', '/Archives/view')
    ->generate();

echo $this->ModalView->create('search-description-archive', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une archive"))
    ->output('function', 'descriptionArchive', '/Archives/description')
    ->generate();

echo $this->ModalView->create('search-view-binary-archive')
    ->modal(__("Fichiers liés à un fichier original d'une archive"))
    ->output('function', 'viewBinary', '/Archives/view-binary')
    ->generate();

echo $this->ModalView->create('life-cycle-archive', ['size' => 'large'])
    ->modal(__("Cycle de vie d'une archive"))
    ->output('function', 'lifecycleAction', '/Archives/lifecycle')
    ->generate();

echo $this->ModalForm
    ->create('archive-edit', ['size' => 'large'])
    ->modal(__("Modification d'une entrée"))
    ->javascriptCallback('afterEditArchive(' . $jsTableArchives . ', "Archives")')
    ->output('function', 'loadEditModalArchive', '/archive-units/edit')
    ->generate();


require 'ajax_search.php';
?>
<script>
    AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');

    /**
     * Inverse l'archive_unit avec archive
     * @param table
     * @param model
     * @returns {function(*, *, *): (undefined)}
     */
    function afterEditArchive(table, model) {
        return function (json, textStatus, jqXHR) {
            // note: $.extend() trigger "name is a readonly property"
            json.archive.first_archive_unit = JSON.parse(JSON.stringify(json));
            var content = json.archive;
            console.log(content);
            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                var data = table.getDataId(content.id);
                if (!data) {
                    console.warn('Unable to edit: content.id was not found');
                    return;
                }
                table.replaceDataId(content.id, $.extend(true, {[model]: content}, content));
                TableGenerator.appendActions(table.data, table.actions);
                table.generateAll();
            } else {
                AsalaeGlobal.colorTabsInError();
            }
        }
    }
</script>
