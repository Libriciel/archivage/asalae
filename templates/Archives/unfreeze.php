<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create(null, ['idPrefix' => 'freeze']);

echo $this->Form->control(
    'reason',
    [
        'label' => __("Motif du dégel de l'archive"),
        'required' => true,
        'type' => 'textarea',
    ]
);

echo $this->Form->end();
