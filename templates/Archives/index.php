<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Archives"));
$this->Breadcrumbs->add(__("Registre des entrées"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-list-ul', __("Registre des entrées")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$jsTableArchives = $this->Table->getJsTableObject($tableId);

$view = $this;
$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalView->create('view-archive', ['size' => 'modal-xxl'])
    ->modal(__("Visualisation d'une archive"))
    ->output('function', 'viewArchive', '/Archives/view')
    ->generate();

echo $this->ModalView->create('description-archive', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une archive"))
    ->output('function', 'descriptionArchive', '/Archives/description')
    ->generate();

echo $this->ModalView->create('view-binary-archive', ['size' => 'large'])
    ->modal(__("Fichiers liés à un fichier original d'une archive"))
    ->output('function', 'viewBinary', '/Archives/view-binary')
    ->generate();

echo $this->ModalView->create('life-cycle-archive', ['size' => 'large'])
    ->modal(__("Cycle de vie d'une archive"))
    ->output('function', 'lifecycleAction', '/Archives/lifecycle')
    ->generate();

echo $this->ModalForm
    ->create('archive-edit', ['size' => 'large'])
    ->modal(__("Modification d'une entrée"))
    ->javascriptCallback('afterEditArchive(' . $jsTableArchives . ', "Archives")')
    ->output('function', 'loadEditModalArchive', '/archive-units/edit')
    ->generate();

echo $this->ModalForm
    ->create('archive-freeze', ['size' => 'large'])
    ->modal(__("Gel d'une entrée"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTableArchives . ', "Archives")')
    ->output('function', 'freezeAction', '/archives/freeze')
    ->generate();

echo $this->ModalForm
    ->create('archive-unfreeze', ['size' => 'large'])
    ->modal(__("Dégel d'une entrée"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTableArchives . ', "Archives")')
    ->output('function', 'unfreezeAction', '/archives/unfreeze')
    ->generate();

echo $this->Filter->create('archive-filter')
    ->saves($savedFilters)
    ->filter(
        'archival_agency_identifier',
        [
            'label' => __("Cote"),
            'wildcard',
        ]
    )
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'description',
        [
            'label' => __("Description"),
            'wildcard',
        ]
    )
    ->filter(
        'appraisal_rules_end_date_vs_now',
        [
            'label' => __("Durée d'utilité Administrative"),
            'options' => [
                '<' => __("Terminé"),
                '>=' => __("En cours"),
            ],
            'empty' => __("-- Veuillez sélectionner une DUA --"),
        ]
    )
    ->filter(
        'final_action_code',
        [
            'label' => __("Sort final"),
            'options' => $final_action_codes,
            'empty' => __("-- Veuillez sélectionner une DUA --"),
        ]
    )
    ->filter(
        'access_rules_end_date_vs_now',
        [
            'label' => __("Communicabilité"),
            'options' => [
                '<' => __("Librement communicable"),
                '>=' => __("Non communicable"),
            ],
            'empty' => __("-- Veuillez sélectionner une valeur --"),
        ]
    )
    ->filter(
        'originating_agency_id',
        [
            'label' => __("Service producteur"),
            'options' => $originating_agencies,
            'empty' => __("-- Sélectionner un service producteur --"),
        ]
    )
    ->filter(
        'transferring_agency_id',
        [
            'label' => __("Service versant"),
            'options' => $transferring_agencies,
            'empty' => __("-- Sélectionner un service versant --"),
        ]
    )
    ->filter(
        'profile_id',
        [
            'label' => __("Profil d'archives"),
            'options' => $profiles,
            'empty' => __("-- Sélectionner un profil d'archives --"),
        ]
    )
    ->filter(
        'agreement_id',
        [
            'label' => __("Accord de versement"),
            'options' => $agreements,
            'empty' => __("-- Sélectionner un accord de versement --"),
        ]
    )
    ->filter(
        'state',
        [
            'label' => __("Etats"),
            'options' => $states,
            'multiple' => true,
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'original_size[value]',
        [
            'label' => __("Taille totale des fichiers"),
            'prepend' => $this->Input->operator(
                'original_size[operator]',
                '>=',
                ['id' => 'original-total-size-operator']
            ),
            'append' => $this->Input->mult(
                'original_size[mult]',
                'mo',
                ['id' => 'original-total-size-mult']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'original_count[value]',
        [
            'label' => __("Nombre de fichiers"),
            'prepend' => $this->Input->operator(
                'original_count[operator]',
                '>=',
                ['id' => 'original-total-count-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'access_end',
        [
            'label' => __("Date de fin de la restriction d'accès"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'units_count[value]',
        [
            'label' => __("Nombre d'unités d'archives"),
            'prepend' => $this->Input->operator(
                'units_count[operator]',
                '>=',
                ['id' => 'units-count-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'keyword',
        [
            'label' => __("Mot clé"),
            'data-callback' => 'searchKeywordPopup',
        ]
    )
    ->filter(
        'filename',
        [
            'label' => __("Nom de fichier"),
            'wildcard',
        ]
    )
    ->filter(
        'mime',
        [
            'label' => __("Mime de fichier"),
            'wildcard',
        ]
    )
    ->filter(
        'format',
        [
            'label' => __("Format de fichier"),
            'wildcard',
        ]
    )
    ->filter(
        'transferring_agency_identifier',
        [
            'label' => __("Identifiant d'archive fourni par le service versant"),
            'wildcard',
        ]
    )
    ->filter(
        'originating_agency_identifier',
        [
            'label' => __("Identifiant d'archive fourni par le service producteur"),
            'wildcard',
        ]
    )
    ->filter(
        'oldest_date',
        [
            'label' => __("Date la plus ancienne"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'latest_date',
        [
            'label' => __("Date la plus récente"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->generateSection();

require 'ajax_index.php';

use Cake\Core\Configure; ?>
<script>
    AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');

    function archiveFileDownload(id) {
        // ajax :
        // soit DL
        // soit message pour dire que mail ou erreur

        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/Archives/make-zip')?>/' + id,
            headers: {
                Accept: 'application/zip'
            },
            success: function (content, textStatus, jqXHR) {
                switch (jqXHR.getResponseHeader('X-Asalae-ZipFileDownload')) {
                    case 'email':
                    case 'fail':
                        alert(content);
                        break;
                    case 'download':
                        var disposition = jqXHR.getResponseHeader('content-disposition');
                        var matches = /"([^"]*)"/.exec(disposition);
                        var filename = (matches != null && matches[1] ? matches[1] : 'archive.zip');

                        var link = document.createElement('a');
                        document.body.appendChild(link);

                        link.href = '<?=$this->Url->build('/Archives/download-files/')?>' + id;
                        link.download = filename;
                        link.dataset.mode = 'no-ajax';
                        link.click();
                        document.body.removeChild(link);
                        break;
                }
            }
        });
    }

$(
    function () {
        AsalaeWebsocket.connect(
            '<?=Configure::read('Ratchet.connect')?>',
            function (session) {
                var uid = $('#archives-index-table').attr('data-table-uid');
                var table = TableGenerator.instance[uid];
                session.subscribe('new_archive', function (topic, data) {
                    if (data.message.sa_id === PHP.sa_id) {
                        AsalaeLoading.ajax({
                            url: '<?=$this->Url->build('/archives/indexOne')?>/' + data.message.id,
                            success: function (content) {
                                table.data.unshift({Archives: content});
                                TableGenerator.appendActions(table.data, table.actions);
                                table.generateAll();
                            }
                        });
                    }
                });
                session.subscribe('archive_state_update', function (topic, data) {
                    var tableData = table.getDataId(data.message.id);
                    if (tableData) {
                        tableData.Archives.state = data.message.state;
                        tableData.Archives.statetrad = data.message.statetrad;
                        if (data.message.original_size) {
                            tableData.Archives.original_size = data.message.original_size;
                        }
                        if (data.message.original_count) {
                            tableData.Archives.original_count = data.message.original_count;
                        }
                        if (data.message.units_count) {
                            tableData.Archives.units_count = data.message.units_count;
                        }
                        if (typeof data.message.is_large !== 'undefined') {
                            tableData.Archives.description_xml_archive_file = {is_large: data.message.is_large};
                        }
                        table.generateAll();
                    }
                });
            },
            function () {
            },
            {'skipSubprotocolCheck': true}
        );
    }
);

    /**
     * Inverse l'archive_unit avec archive
     * @param table
     * @param model
     * @returns {function(*, *, *): (undefined)}
     */
    function afterEditArchive(table, model) {
        return function (json, textStatus, jqXHR) {
            if (typeof json !== 'object') {
                console.error(json);
                return;
            }
            // note: $.extend() trigger "name is a readonly property"
            json.archive.first_archive_unit = JSON.parse(JSON.stringify(json));
            var content = json.archive;
            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                var data = table.getDataId(content.id);
                if (!data) {
                    console.warn('Unable to edit: content.id was not found');
                    return;
                }
                table.replaceDataId(content.id, $.extend(true, {[model]: content}, content));
                TableGenerator.appendActions(table.data, table.actions);
                table.generateAll();
            } else {
                AsalaeGlobal.colorTabsInError();
            }
        }
    }

    function archivePdfDownload(id) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/Archives/make-pdf')?>/' + id,
            headers: {
                Accept: 'application/zip'
            },
            success: function (content, textStatus, jqXHR) {
                switch (jqXHR.getResponseHeader('X-Asalae-PdfFileDownload')) {
                    case 'email':
                    case 'fail':
                        alert(content);
                        break;
                    case 'download':
                        var disposition = jqXHR.getResponseHeader('content-disposition');
                        var matches = /"([^"]*)"/.exec(disposition);
                        var filename = (matches != null && matches[1] ? matches[1] : 'archive.pdf');

                        var link = document.createElement('a');
                        document.body.appendChild(link);

                        link.href = '<?=$this->Url->build('/Archives/download-pdf/')?>' + id;
                        link.download = filename;
                        link.dataset.mode = 'no-ajax';
                        link.click();
                        document.body.removeChild(link);
                        break;
                }
            }
        });
    }
</script>
