<?php

/**
 * @var Asalae\View\AppView               $this
 * @var Cake\Datasource\EntityInterface   $archive
 * @var Cake\Datasource\EntityInterface   $transferReceipt
 * @var Cake\Datasource\EntityInterface   $archiveCreate
 * @var Cake\Datasource\EntityInterface   $firstIntegrity
 * @var Cake\Datasource\EntityInterface   $archival_agency
 * @var Cake\Datasource\EntityInterface   $transferring_agency
 * @var Cake\Datasource\EntityInterface[] $binaries
 * @var Cake\Datasource\EntityInterface[] $integrities
 */

use Cake\I18n\FrozenTime;
use Cake\I18n\Number;
use Cake\Utility\Hash;

$identifier = $archive->get('archival_agency_identifier');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr-FR" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= h($identifier) ?></title>
    <style type="text/css">
        <?php require dirname(__DIR__, 2) . '/vendor/libriciel/seda2pdf/templates/style.css'; ?>

        table#list-files {
            font-family: monospace;
        }

        table#list-files tr th {
            min-width: 0;
        }

        table#list-files tr td {
            width: auto;
        }
    </style>
</head>
<body>
<?php

echo $this->Html->tag(
    'h1.h1',
    __("Attestation d'intégrité et d'authenticité des archives")
);
echo $this->Html2pdf->table(
    [
        __("Date d'édition de l'attestation") => new FrozenTime(),
        __("Service d'archives") => $archival_agency->get('name'),
        __("Service versant") => $transferring_agency->get('name'),
    ]
);

echo '<hr>';

// Bloc Archive
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Archive"));
echo $this->Html2pdf->table(
    [
        __("Cote") => $identifier,
        __("Nom de l'archive") => $archive->get('name'),
        __("Profil d'archive") => Hash::get($archive, 'profile.name'),
    ]
);
echo $this->Html->tag('/div'); // div.section

// Bloc Transfert
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Transfert"));
echo $this->Html2pdf->table(
    [
        __("Date") => Hash::get($archive, 'transfers.0.transfer_date'),
        __("Identifiant") => Hash::get($archive, 'transfers.0.transfer_identifier'),
        __("Commentaire") => Hash::get($archive, 'transfers.0.transfer_comment'),
    ]
);
echo $this->Html->tag('/div'); // div.section

// Bloc Cycle de vie
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Cycle de vie"));
echo $this->Html2pdf->table(
    [
        __("Date de réception du transfert") => $transferReceipt->get('created'),
        __("Date de création de l'archive") => $archiveCreate->get('created'),
        __("Premier contrôle d'intégrité") => $firstIntegrity ? $firstIntegrity->get('created') : '',
        __("Avant dernier contrôle d'intégrité") => $integrities ? $integrities[0]->get('created') : '',
        __("Dernier contrôle d'intégrité") => $integrities ? $integrities[1]->get('created') : '',
    ]
);
echo $this->Html->tag('/div'); // div.section

// Bloc Fichiers
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Fichiers"));


echo $this->Html->tag('table');
echo $this->Html->tag('thead');
echo $this->Html->tag('th', __("Type de fichier"));
echo $this->Html->tag('th', __("Nombre de fichiers"));
echo $this->Html->tag('th', __("Taille des fichiers"));
echo $this->Html->tag('/thead');
echo $this->Html->tag('tbody');

echo $this->Html->tag('tr');
echo $this->Html->tag('th', __("Fichiers transférés"));
echo $this->Html->tag('td', $archive->get('transferred_count'));
echo $this->Html->tag('td', Number::toReadableSize($archive->get('transferred_size')));
echo $this->Html->tag('/tr');

echo $this->Html->tag('tr');
echo $this->Html->tag('th', __("Fichiers de management"));
echo $this->Html->tag('td', $archive->get('management_count'));
echo $this->Html->tag('td', Number::toReadableSize($archive->get('management_size')));
echo $this->Html->tag('/tr');

echo $this->Html->tag('tr');
echo $this->Html->tag('th', __("Fichiers originaux"));
echo $this->Html->tag('td', $archive->get('original_count'));
echo $this->Html->tag('td', Number::toReadableSize($archive->get('original_size')));
echo $this->Html->tag('/tr');

echo $this->Html->tag('tr');
echo $this->Html->tag('th', __("Fichiers de conservation"));
echo $this->Html->tag('td', $archive->get('preservation_count'));
echo $this->Html->tag('td', Number::toReadableSize($archive->get('preservation_size')));
echo $this->Html->tag('/tr');

echo $this->Html->tag('tr');
echo $this->Html->tag('th', __("Fichiers de diffusion"));
echo $this->Html->tag('td', $archive->get('dissemination_count'));
echo $this->Html->tag('td', Number::toReadableSize($archive->get('dissemination_size')));
echo $this->Html->tag('/tr');

echo $this->Html->tag('tr');
echo $this->Html->tag('th', __("Fichiers de jetons d'horodatage"));
echo $this->Html->tag('td', $archive->get('timestamp_count'));
echo $this->Html->tag('td', Number::toReadableSize($archive->get('timestamp_size')));
echo $this->Html->tag('/tr');

echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');

echo $this->Html->tag('h3', __("Liste des fichiers originaux"));
echo $this->Html->tag('table#list-files');
echo $this->Html->tag('thead');
echo $this->Html->tag('th', __("Nom de fichier"));
echo $this->Html->tag('th', __("Taille"));
echo $this->Html->tag('th', __("Pronom"));
echo $this->Html->tag('th', __("Type MIME"));
echo $this->Html->tag('th.algo', __("Algo."));
echo $this->Html->tag('th', __("Hash"));
echo $this->Html->tag('/thead');
echo $this->Html->tag('tbody');
foreach ($binaries as $binary) {
    $hash = implode(
        '<wbr>',
        str_split(
            h(
                Hash::get($binary, 'stored_file.hash')
            ),
            20
        )
    );
    $filename = str_replace(
        '/',
        '/<wbr>',
        implode(
            '<wbr>',
            str_split(
                h($binary->get('filename')),
                20
            )
        )
    );
    $size = Number::toReadableSize(Hash::get($binary, 'stored_file.size'));
    $size = str_replace(' ', '&nbsp;', $size);
    $format = str_replace('-', '&#8209;', h($binary->get('format')));
    echo $this->Html->tag('tr');
    echo $this->Html->tag('td', $filename);
    echo $this->Html->tag('td', $size);
    echo $this->Html->tag('td', $format);
    echo $this->Html->tag('td', h($binary->get('mime')));
    echo $this->Html->tag('td', h(Hash::get($binary, 'stored_file.hash_algo')));
    echo $this->Html->tag('td.pre', $hash);
    echo $this->Html->tag('/tr');
}
echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');
echo $this->Html->tag('/div'); // div.section
?>
</body>
</html>
