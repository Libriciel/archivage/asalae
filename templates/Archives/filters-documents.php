<?php

/**
 * @var Asalae\View\AppView $this
 */
return $this->Filter->create('search-archive-document-filter')
    ->filter(
        'Documents.name',
        [
            'label' => __("Nom du fichier de la pièce jointe"),
            'wildcard',
        ]
    )
    ->filter(
        'Documents.mime',
        [
            'label' => __("Code MIME des documents (archives, objets d'archive)"),
            'options' => $mimes,
            'empty' => __("-- Sélectionne un code MIME --"),
            'multiple' => true,
            'data-callback' => 'select2ify',
        ]
    );
