<?php

/**
 * @var Asalae\View\AppView $this
 * @var Asalae\Form\SearchArchiveForm $form
 */

use AsalaeCore\View\Helper\Object\Filter;

?>
<script>
    /**
     * Logique des boutons Supprimer / Enregistrer et Charger (la sauvegarde)
     * @param {object} urls
     * @param {string} controller
     * @param {string} action
     */
    function loadSaveSearchArchive(urls, controller, action) {
        var div = $('.modal-list-buttons');
        var buttonDelete = div.find('button.delete');
        var buttonOverwrite = div.find('button.overwrite');
        var buttonLoad = div.find('button.load');
        buttonDelete.off('click.filter').on('click.filter', function () {
            var select = $('#SearchArchiveGeneralFilter-select-save'),
                value = select.val(),
                form = $('form.ModalFilters');

            if (value && !isNaN(value) && value >= 0
                && typeof urls.delete !== 'undefined'
                && confirm(__("Cette action supprimera la sauvegarde de façon définitive. Voulez-vous continuer ?"))
            ) {
                $('html').addClass('ajax-loading');
                $.ajax({
                    url: urls.delete + '/' + value,
                    method: 'DELETE',
                    success: function (content, textStatus, jqXHR) {
                        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                            select.find('option[value="' + value + '"]').remove();
                            select.val('-1');
                            setTimeout(function () {
                                $('select.filterSelect').stop(true, true).shake(
                                    {duration: 1000, distance: 2}
                                ).parent()
                                    .css('background-color', '#5cb85c')
                                    .stop(true, true)
                                    .animate({'background-color': 'transparent'});
                            }, 400);
                        } else {
                            this.error();
                        }
                    },
                    error: function () {
                        alert(PHP.messages.genericError);
                    },
                    complete: function () {
                        $('html').removeClass('ajax-loading');
                    }
                });
            }
        });
        buttonOverwrite.off('click.filter').on('click.filter', function () {
            var select = $('#SearchArchiveGeneralFilter-select-save'),
                value = select.val(),
                form = $('#form-search-archives');

            if (value && !isNaN(value) && value >= 0
                && typeof urls.overwrite !== 'undefined'
                && confirm(__("Cette action va modifier la sauvegarde de façon définitive. Voulez-vous continuer ?"))
            ) {
                var data = AsalaeFilter.getFiltersData(form);
                $('html').addClass('ajax-loading');
                $.ajax({
                    url: urls.overwrite + '/' + value,
                    headers: {Accept: 'application/json'},
                    method: 'POST',
                    data: data,
                    success: function (content, textStatus, jqXHR) {
                        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                            var newUrl = urls.overwrite
                                .replace(/\/[\w\-]+\/[\w\-]+(\?.*)?$/, '/' + controller + '/' + action + '?' + data);
                            $('ul.filtersSelectWindow li.savedFilter[data-id="' + content.id + '"] a')
                                .attr('href', newUrl);

                            setTimeout(function () {
                                form.find('button.overwrite')
                                    .stop(true, true)
                                    .shake({duration: 1000, distance: 2})
                                    .disable();
                                form.find('.filter-container > div')
                                    .css('background-color', '#5cb85c')
                                    .stop(true, true)
                                    .animate({'background-color': 'transparent'})
                                    .find('input')
                                    .css('background-color', 'transparent');

                            }, 400);
                            setTimeout(function () {
                                $('select.filterSelect').stop(true, true).shake(
                                    {duration: 1000, distance: 2}
                                ).parent()
                                    .css('background-color', '#5cb85c')
                                    .stop(true, true)
                                    .animate({'background-color': 'transparent'});

                            }, 400);
                        } else {
                            this.error();
                        }
                    },
                    error: function () {
                        alert(PHP.messages.genericError);
                    },
                    complete: function () {
                        $('html').removeClass('ajax-loading');
                    }
                });
            }
        });
        buttonLoad.off('click.filter').on('click.filter', function () {
            var modal = $(this).closest('.modal'),
                value = $('#SearchArchiveGeneralFilter-select-save').val(),
                form = $('#form-search-archives');

            if (value !== '-1') {
                $('html').addClass('ajax-loading');
                $.ajax({
                    url: '<?=$this->Url->build('/archives/get-filters')?>/' + value,
                    type: 'GET',
                    headers: {
                        Accept: 'application/json'
                    },
                    success: function (content) {
                        $('#SearchArchiveArchiveFilter-container').html(content.archives);
                        $('#SearchArchiveArchiveUnitFilter-container').html(content.archive_units);
                        $('#SearchArchiveDocumentFilter-container').html(content.documents);
                        $('.filter-container')
                            .find('select, input, textarea')
                            .each(function () {
                                var callback = $(this).attr('data-callback');
                                if (callback && /[\w\-()'"]+/.test(callback)) {
                                    eval(callback + '(this)');
                                }
                            });

                        buttonOverwrite.disable();
                        buttonLoad.disable();
                        setTimeout(function () {
                            buttonLoad.stop(true, true).shake({duration: 1000, distance: 2});
                            buttonDelete.enable();
                            form.find('.filter-container > div')
                                .css('background-color', '#5cb85c')
                                .animate({'background-color': 'transparent'})
                                .find('input')
                                .css('background-color', 'transparent');

                        }, 400);
                    },
                    error: function (e) {
                        console.error(e);
                        alert("Une erreur a eu lieu");
                    },
                    complete: function () {
                        $('html').removeClass('ajax-loading');
                    }
                });
            }
        });
    }

    /**
     * Logique de la modale d'ajout de sauvegarde
     * @param {string} url
     * @param {string} controller
     * @param {string} action
     */
    function submitSaveFilters(url, controller, action) {
        var div = $('.modal-save-buttons'),
            input = div.closest('.modal-content').find('input.filterNewSave');

        input.off('keypress.filter').on('keypress.filter', function (event) {
            if (event.originalEvent.keyCode === 13) { // Enter
                $(this).closest('.modal-content').find('button.btn-primary').click();
            }
        });
        div.find('button.btn-primary').off('click.filter').on('click.filter', function () {
            var value = input.val(),
                data = AsalaeFilter.getFiltersData($('#form-search-archives')),
                parentDiv = input.parent();

            if (!data) {
                alert(__("Impossible de sauvegarder en cas de formulaire vide ou en erreur!"));
            } else if (!value) {
                var nf = $('<form>');
                parentDiv.append(nf.append(input));
                input.prop('required', true);
                nf.get(0).reportValidity();
                input.prop('required', false);
                nf.parent().append(input);
                nf.remove();
                input.focus();
            } else if (url) {
                var modal = $(this).closest('.modal');
                $('html').addClass('ajax-loading');
                $.ajax({
                    url: url,
                    headers: {Accept: 'application/json'},
                    method: 'POST',
                    data: data + '&savename=' + value + '&controller=' + controller + '&action=' + action,
                    success: function (content, textStatus, jqXHR) {
                        parentDiv.removeClass('has-error')
                            .find('.error-message').remove();

                        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                            input.val('');
                            modal.modal("hide");
                            $('#SearchArchiveGeneralFilter-select-save')
                                .append('<option value="' + content.id + '">' + content.name + '</option>')
                                .val(content.id);

                            $('#SearchArchiveGeneralFilter-savefilterselect').val(content.id);
                        } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'false') {
                            var error = $('<span class="help-block error-message"></span>');
                            for (var key in content.name) {
                                error.append(content.name[key]);
                            }
                            parentDiv.find('.help-block').remove();
                            parentDiv.addClass('has-error').append(error);
                        } else {
                            this.error();
                        }
                    },
                    error: function (e) {
                        console.error(e);
                        alert(PHP.messages.genericError);
                    },
                    complete: function () {
                        $('html').removeClass('ajax-loading');
                    }
                });
            }
        });
    }
</script>
<?php
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Archives"));
$this->Breadcrumbs->add(__("Recherche sur les entrées"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa fa-search', __("Recherche sur les entrées")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$view = $this;
$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

/**
 * Modal save (content)
 * @see AsalaeCore\View\Helper\Object\Filter::generateSaveSection()
 */
$saveName = $this->Html->tag(
    'div',
    $this->Form->control(
        'FilterName',
        [
            'label' => __('Nom de la nouvelle sauvegarde'),
            'class' => 'filterNewSave',
        ]
    ),
    ['class' => 'select-filter-save-container']
) . PHP_EOL;
$cancelButton = $this->Html->tag(
    'button',
    $this->Fa->charte('Annuler', __("Annuler")),
    [
        'type' => 'button',
        'class' => 'btn btn-default cancel',
        'data-toggle' => 'modal',
        'data-target' => '#save-search-archive-modal',
    ]
);
$newSaveButton = $this->Html->tag(
    'button',
    $this->Fa->charte('Enregistrer', __("Enregistrer")),
    [
        'type' => 'button',
        'data-toggle' => 'modal',
        'data-target' => '#save-search-archive-modal',
        'class' => 'btn btn-primary save',
    ]
);
$script = $this->Html->tag(
    'script',
    "submitSaveFilters('/filters/ajax-new-save', 'Archives', 'search');"
);
$saveDiv = $this->Html->tag(
    'div',
    $cancelButton . $newSaveButton . $script,
    ['class' => 'submit-btn-filter-container modal-footer modal-save-buttons']
);

echo $this->Modal->create(
    [
        'id' => 'save-search-archive-modal',
        'class' => 'filter-save-modal',
    ]
)
    . $this->Modal->header(__("Nouvelle sauvegarde de recherche"))
    . $this->Modal->body($saveName . $saveDiv)
    . $this->Modal->end();

echo $this->Html->tag('section.container.smaller.bg-white');
echo $this->Html->tag('h2.h4', __("Formulaire de recherche"));

$appraisalDuration = [];
for ($i = 0; $i <= 100; $i++) {
    $appraisalDuration['P' . $i . 'Y'] = __n("{0} an", "{0} ans", $i, $i);
}

$IlikeOperators = [
    'options' => [
        'like' => __("contient"),
        'equal' => __("est égal à"),
        'begin' => __("commence par"),
        'end' => __("finit par"),
    ],
    'templates' => [
        'inputContainer' => '{{content}}',
        'formGroup' => '{{input}}',
        'select' => '<select name="{{name}}">{{content}}</select>',
    ],
];
$communicabilites = [
    '<=' => __("Librement communicable"),
    '>' => __("Non communicable"),
];
$duas = [
    '<' => __("Terminé"),
    '>=' => __("En cours"),
];
$date_operators = [
    '=' => __x('interval', '='),
    '<=' => __x('interval', '<='),
    '>=' => __x('interval', '>='),
    '<' => __x('interval', '<'),
    '>' => __x('interval', '>'),
];

$filters = $this->Filter->create('search-archive-general-filter')
    ->saves($savedFilters);
echo $filters->generateMetaInputs();
echo $filters->generateSaveSection('loadSaveSearchArchive');

echo $this->Html->tag('/section');

echo $this->Form->create(
    $form,
    [
        'method' => 'get',
        'id' => 'form-search-archives',
        'templates' => [
            'hiddenBlock' => '',
        ],
    ]
);

/**
 * ---------------- Archive ----------------------------------------------------
 */
echo $this->Html->tag('section.container.smaller.bg-white');
echo $this->Html->tag('h2.h4', __("Archive"));

/** @var Filter $archiveFilters */
$archiveFilters = include 'filters-archives.php';
echo $archiveFilters->generateSelectFilterContainer();
echo $archiveFilters->generateFilterContainer();

echo $this->Html->tag('/section');

/**
 * ---------------- Objet d'Archive --------------------------------------------
 */
echo $this->Html->tag('section.container.smaller.bg-white');
echo $this->Html->tag('h2.h4', __("Objet d'Archive"));

/** @var Filter $archiveUnitsFilters */
$archiveUnitsFilters = include 'filters-archive-units.php';
echo $archiveUnitsFilters->generateSelectFilterContainer();
echo $archiveUnitsFilters->generateFilterContainer();

echo $this->Html->tag('/section');

/**
 * ---------------- Documents --------------------------------------------------
 */
echo $this->Html->tag('section.container.smaller.bg-white');
echo $this->Html->tag('h2.h4', __("Document"));

/** @var Filter $documentFilters */
$documentFilters = include 'filters-documents.php';
echo $documentFilters->generateSelectFilterContainer();
echo $documentFilters->generateFilterContainer();

echo $this->Html->tag('/section');

/**
 * ---------------- Boutons --------------------------------------------------
 */
echo $this->Html->tag('section.container.smaller');

$deleteBtn = $this->Form->button(
    $this->Fa->charte('Réinitialiser', __("Réinitialiser")),
    [
        'type' => 'button',
        'onclick' => "$('.filter-container').empty()",
        'class' => 'btn btn-default',
    ]
);
$newSaveButton = $this->Form->button(
    $this->Fa->charte('Ajouter', __("Sauvegarder la recherche")),
    [
        'type' => 'button',
        'onclick' => 'saveFilters()',
        'class' => 'btn btn-success save',
    ]
);
$searchButton = $this->Form->button(
    $this->Fa->charte('Rechercher', __("Rechercher")),
    ['class' => 'btn btn-primary']
);

echo $this->Html->tag(
    'div',
    $deleteBtn . $newSaveButton . $searchButton,
    ['class' => 'submit-btn-filter-container text-right']
);

echo $this->Html->tag('/section');

echo $this->Form->end();
?>
<!--suppress JSJQueryEfficiency -->
<script>
    /**
     * Contenu du container de filtres
     * @param {jQuery} filter templates issues du FilterHelper::getFiltersTemplates()
     */
    function renderContainerFilter(filter) {
        var container = $('<div></div>')
            .append(
                $('<button></button>')
                    .addClass('close')
                    .attr('type', 'button')
                    .attr('aria-label', __('Retirer le filtre de recherche'))
                    .append('<span>×</span>')
                    .click(function () {
                        $(this).parent().stop(true, true).slideUp(400, function () {
                            $(this).remove();
                        });
                    })
            );

        // Remplace l'id et le name dans template
        filter.find('select, input, textarea').each(function () {
            var base_id = $(this).attr('id'),
                id = base_id + '_0',
                base_name = $(this).attr('name'),
                label = filter.find('label[for="' + base_id + '"]'),
                name,
                i = 0;

            while (id && $('#' + id).length) {
                i++;
                id = base_id + '_' + i;
            }

            if (label) {
                label.attr('for', id);
            }
            $(this).attr('id', id);

            if (!base_name) {
                return;
            }

            i = 0;
            name = AsalaeFilter._newName(base_name, i);
            while (name && $('[name="' + name + '"]').length) {
                i++;
                name = AsalaeFilter._newName(base_name, i);
            }
            $(this).attr('name', name).prop('required', true);
            var callback = $(this).attr('data-callback');
            if (callback && /[\w\-()'"]+/.test(callback)) {
                eval(callback + '(this)');
            }
        });

        container.append(filter);
        return container;
    }

    /**
     * @see AsalaeFilter.selectHandler
     */
    function archiveSelectHandler(select, container, template) {
        $(select).off('change.filter').on('change.filter', function () {
            var val = $(this).val();
            if (!val) {
                return;
            }
            $(this).val('');
            var filter = $(template[val]);
            $(container).append(renderContainerFilter(filter));
        });
    }

    archiveSelectHandler(
        '#SearchArchiveArchiveFilter-select',
        '#SearchArchiveArchiveFilter-container',
        <?=$archiveFilters->getFiltersTemplates()?>
    );
    archiveSelectHandler(
        '#SearchArchiveArchiveUnitFilter-select',
        '#SearchArchiveArchiveUnitFilter-container',
        <?=$archiveUnitsFilters->getFiltersTemplates()?>
    );
    archiveSelectHandler(
        '#SearchArchiveDocumentFilter-select',
        '#SearchArchiveDocumentFilter-container',
        <?=$documentFilters->getFiltersTemplates()?>
    );

    /**
     * options du select de mots-clés
     * @param {string|jQuery} element select
     */
    function ajaxKeyword(element) {
        setTimeout(function () {
            $(element).select2({
                ajax: {
                    url: '<?=$this->Url->build('/Keywords/populateSelect')?>',
                    method: 'POST',
                    delay: 250
                },
                escapeMarkup: function (v) {
                    return $('<span></span>').html(v).text();
                },
                allowClear: true
            });
            var button = $(element).siblings('.help-block').find('button').hide();
            $(element).on('change', function () {
                var actualValue = $('.keyword-name').val();
                if (!actualValue) {
                    button.click();
                } else {
                    button.show();
                }
            });
        }, 0);
    }


    /**
     * Bouton de sauvegarde les filtres actifs
     */
    function saveFilters() {
        var form = $('#form-search-archives'),
            formData = AsalaeFilter.getFiltersData(form);

        if (formData === "") {
            var select = $('#SearchArchiveArchiveFilter-select');
            select.prop('required', true);
            form.get(0).reportValidity();
            select.prop('required', false);
        } else if (formData) {
            var saveModal = $('#save-search-archive-modal');
            saveModal.modal('show');
            setTimeout(function () {
                saveModal.find('input').first().focus();
            }, 500);
        }
    }

    /**
     * Retire le <hr> sous les boutons "Supprimer la sauvegarde / Enregistrer et Charger"
     */
    $('.modal-list-buttons').parent().find('> hr').remove();

    /**
     * Active / désactive les boutons selon l'état du formulaire
     */
    function formChangedHandler() {
        var selectedSave = $('#SearchArchiveGeneralFilter-select-save').val(),
            form = $('#form-search-archives'),
            clone = form.clone(),
            listButtons = $('.modal-list-buttons'),
            filterData = clone.serialize();

        if (selectedSave && selectedSave > 0) {
            listButtons.find('button.delete').enable();
            listButtons.find('button.load').enable();
            // noinspection JSUnresolvedFunction
            if (filterData && this.checkValidity()) {
                listButtons.find('button.overwrite').enable();
            } else {
                listButtons.find('button.overwrite').disable();
            }
        } else {
            listButtons.find('button.delete').disable();
            listButtons.find('button.load').disable();
            listButtons.find('button.overwrite').disable();
        }
    }

    $('#SearchArchiveGeneralFilter-select-save').change(formChangedHandler);
    $('#form-search-archives').change(formChangedHandler);

    function select2ify(element) {
        setTimeout(function () {
            $(element).select2();
        }, 0);
    }
</script>
