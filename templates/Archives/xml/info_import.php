<?php

use AsalaeCore\Utility\XmlUtility;

echo XmlUtility::arrayToXmlString(
    [
        'Archive' => [
            'archival_agency_identifier' => $archive->get('archival_agency_identifier'),
            'name' => $archive->get('name'),
            'created' => $archive->get('created'),
            'original_count' => $archive->get('original_count'),
            'original_size' => $archive->get('original_size'),
            'is_integrity_ok' => (($ok = $archive->get('is_integrity_ok')) === null
                ? __("non déterminé")
                : ($ok ? __("intègre") : __("non intègre"))),
            'integrity_date' => $archive->get('integrity_date') ?: 'N/A',
        ],
    ]
);
