<?php

use AsalaeCore\Utility\XmlUtility;
use Cake\I18n\DateTime as CakeDateTime;

if (!empty($errors)) {
    echo XmlUtility::arrayToXmlString(['Import' => ['errors' => $errors]]);
} elseif ($dry) {
    echo XmlUtility::arrayToXmlString(
        [
            'Import' => [
                'success' => __(
                    "Les paramètres ont été vérifiés le {0} et permettent l'envoi du fichier zip de l'archive",
                    (new CakeDateTime())->nice()
                ),
            ],
        ]
    );
} else {
    echo XmlUtility::arrayToXmlString(
        [
            'Import' => [
                'success' => __("L'archive a été importée le {0}", (new CakeDateTime())->nice()),
            ],
        ]
    );
}
