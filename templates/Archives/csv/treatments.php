<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Csv->resultTable(
    [
        'archival_agency_identifier' => __("Cote"),
        'archive_description.description' => __("Description"),
        'archive.transferring_agency.name' => __("Service versant"),
        'archive.originating_agency.name' => __("Service producteur"),
        'archive.profile.name' => __("Profil d'archive"),
        'archive.created' => __("Date de création"),
        'archive.agreement.name' => __("Accord de versement"),
        'dates' => __("Dates extrêmes"),
        'original_total_size' => __("Taille totale des fichiers"),
        'original_total_count' => __("Nombre de fichiers"),
        'archive.transfers.0.message_versiontrad' => __("Version SEDA"),
    ],
    $results
);
