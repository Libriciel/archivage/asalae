<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Csv->resultTable(
    [
        'archival_agency_identifier' => __("Cote"),
        'name' => __("Nom"),
        'description' => __("Description"),
        'statetrad' => __("Etat"),
        'transferring_agency.name' => __("Service versant"),
        'originating_agency.name' => __("Service producteur"),
        'profile.name' => __("Profil d'archive"),
        'created' => __("Date de création"),
        'agreement.name' => __("Accord de versement"),
        'dates' => __("Dates extrêmes"),
        'appraisal_rule.appraisal_rule_code.name' => __("DUA"),
        'appraisal_rule.end_date' => __("Date de fin de la DUA"),
        'appraisal_rule.final_action_codetrad' => __("Sort final"),
        'access_rule.access_rule_code.code' => __("Code de la règle de restriction d'accès"),
        'access_rule.end_date' => __("Date de fin de la restriction d'accès"),
        'original_size' => __("Taille totale des fichiers"),
        'original_count' => __("Nombre de fichiers"),
        'units_count' => __("Nombre d'unités d'archives"),
        'transfers.0.message_versiontrad' => __("Version SEDA"),
    ],
    $results
);
