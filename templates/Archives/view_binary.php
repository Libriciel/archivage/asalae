<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $entity
 */

use Cake\Datasource\EntityInterface;

$count = count($entity->get('other_datas'));
$data = [];
/** @var EntityInterface $other */
foreach ($entity->get('other_datas') as $other) {
    $data[$other->get('type')] = $other;
}

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($entity->get('filename')));
$infos .= $this->Html->tag(
    'p',
    __n(
        "Ce fichier original est lié à {0} fichier",
        "Ce fichier original est lié à {0} fichiers",
        $count,
        $count
    )
);
$infos .= $this->Html->tag('/header');

$tableBinaries = $this->Table
    ->create('view-others-archive-binaries', ['class' => 'table table-striped table-hover'])
    ->fields(
        [
            'filename' => [
                'label' => __("Nom de fichier"),
                'callback' => 'filenameToLink',
            ],
            'typetrad' => [
                'label' => __("Type"),
            ],
            'format' => [
                'label' => __("Format"),
            ],
            'mime' => [
                'label' => __("Type MIME"),
            ],
            'stored_file.size' => [
                'label' => __("Taille"),
                'callback' => 'TableHelper.readableBytes',
            ],
        ]
    )
    ->data($entity->get('other_datas'))
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'href' => "/archive-binaries/download/{0}/{3}",
                'download' => '{2}',
                'target' => '_blank',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => $title = __("Télécharger {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-binaries/download'),
                'params' => ['id', 'filename', 'basename', 'url_basename'],
            ],
            [
                'href' => '/archive-binaries/open/{0}/{3}',
                'target' => '_blank',
                'label' => $this->Fa->i('fas fa-external-link-alt'),
                'title' => $title = __("Ouvrir dans le navigateur {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-binaries/open'),
                'displayEval' => 'data[{index}].openable',
                'params' => ['id', 'filename', 'basename', 'url_basename'],
            ],
        ]
    );
$infos .= $tableBinaries->generate();

$infos .= $this->Html->tag('/article');

echo $infos;
