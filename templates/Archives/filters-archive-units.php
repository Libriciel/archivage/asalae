<?php

/**
 * @var Asalae\View\AppView $this
 */
return $this->Filter->create('search-archive-archive-unit-filter')
    ->filter(
        'ArchiveUnits.name',
        [
            'label' => __("Intitulé de l'objet d'archive"),
            'wildcard',
        ]
    )
    ->filter(
        'ArchiveUnits.description',
        [
            'label' => __("Description de l'objet d'archive"),
            'wildcard',
        ]
    )
    ->filter(
        'ArchiveUnits.keyword',
        [
            'label' => __("Mot clé de l'objet d'archive (saisie libre)"),
            'wildcard',
        ]
    )
    ->filter(
        'ArchiveUnits.keyword_list',
        [
            'label' => __("Mots-clés l'objet d'archive (liste)"),
            'options' => [],
            'empty' => $empty = __("-- Sélectionner les mots-clés --"),
            'data-placeholder' => $empty,
            'multiple' => true,
            'data-callback' => 'ajaxKeyword',
            'class' => 'ajax-options',
        ]
    )
    ->filter(
        'ArchiveUnits.dua',
        [
            'label' => __("Statut de la DUA de l'objet d'archive"),
            'options' => $duas,
            'empty' => __("-- Veuillez sélectionner une DUA --"),
        ]
    )
    ->filter(
        'ArchiveUnits.dua_duration',
        [
            'label' => __("Durée de la DUA de l'objet d'archive"),
            'class' => 'with-select',
            'options' => $appraisalDuration,
            'empty' => __("-- Sélectionner une durée --"),
            'prepend' => $this->Form->control(
                'Archives.description_operator',
                [
                    'options' => $date_operators,
                    'templates' => [
                        'inputContainer' => '{{content}}',
                        'formGroup' => '{{input}}',
                        'select' => '<select name="{{name}}">{{content}}</select>',
                    ],
                ]
            ),
        ]
    )
    ->filter(
        'ArchiveUnits.dua_end',
        [
            'label' => __("Date de fin de la DUA de l'objet d'archive"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'ArchiveUnits.appraisal_rules_final_action_code',
        [
            'label' => __("Sort final de l'objet d'archive"),
            'options' => $final_action_codes,
            'empty' => __("-- Veuillez sélectionner une DUA --"),
        ]
    )
    ->filter(
        'ArchiveUnits.access_rules_end_date_vs_now',
        [
            'label' => __("Communicabilité de l'objet d'archive"),
            'options' => $communicabilites,
            'empty' => __("-- Veuillez sélectionner une valeur --"),
        ]
    )
    ->filter(
        'ArchiveUnits.archival_agency_identifier',
        [
            'label' => __("Identifiant de l'objet d'archive donné par le service d'Archives"),
            'wildcard',
        ]
    )
    ->filter(
        'ArchiveUnits.transferring_agency_identifier',
        [
            'label' => __("Identifiant de l'objet d'archive donné par le service versant"),
            'wildcard',
        ]
    )
    ->filter(
        'ArchiveUnits.originating_agency_identifier',
        [
            'label' => __("Identifiant de l'objet d'archive donné par le service producteur"),
            'wildcard',
        ]
    );
