<?php

use Cake\I18n\DateTime as CakeDateTime;

if ($dry) {
    if (!empty($errors)) {
        echo __(
            "Les paramètres ont été vérifiés le {0} et ne permettent pas "
                . "l'envoi du fichier zip de l'archive pour les raisons suivantes :",
            (new CakeDateTime())->nice()
        ) . '<br><br>';
        echo ' - ' . implode('<br> - ', $errors);
    } else {
        echo __(
            "Les paramètres ont été vérifiés le {0} et permettent l'envoi du fichier zip de l'archive",
            (new CakeDateTime())->nice()
        );
    }
} else {
    if (!empty($errors)) {
        echo __("L'importation de l'archive a échoué pour les raisons suivantes :") . '<br><br>';
        echo ' - ' . implode('<br> - ', $errors);
    } else {
        echo __("L'archive a été importée le {0}", (new CakeDateTime())->nice());
    }
}
