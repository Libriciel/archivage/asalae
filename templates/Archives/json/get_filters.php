<?php

/**
 * @var Asalae\View\AppView $this
 */

use Asalae\Model\Entity\SavedFilter;
use AsalaeCore\View\Helper\Object\Filter;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

$this->loadHelper('AsalaeCore.Filter');
$this->loadHelper('AsalaeCore.Form');

$wildcard = __("? = n'importe quel caractère ; * = n'importe quelle chaîne de caractères");
$appraisalDuration = [];
for ($i = 0; $i <= 100; $i++) {
    $appraisalDuration['P' . $i . 'Y'] = __n("{0} an", "{0} ans", $i, $i);
}

$IlikeOperators = [
    'options' => [
        'like' => __("contient"),
        'equal' => __("est égal à"),
        'begin' => __("commence par"),
        'end' => __("finit par"),
    ],
    'templates' => [
        'inputContainer' => '{{content}}',
        'formGroup' => '{{input}}',
        'select' => '<select name="{{name}}">{{content}}</select>',
    ],
];
$communicabilites = [
    '<' => __("Librement communicable"),
    '>=' => __("Non communicable"),
];
$duas = [
    '<' => __("Terminé"),
    '>=' => __("En cours"),
];
$date_operators = [
    '=' => __x('interval', '='),
    '<=' => __x('interval', '<='),
    '>=' => __x('interval', '>='),
    '<' => __x('interval', '<'),
    '>' => __x('interval', '>'),
];

/** @var Filter $archiveFilters */
$archiveFilters = include dirname(__DIR__) . '/filters-archives.php';
/** @var Filter $archiveUnitsFilters */
$archiveUnitsFilters = include dirname(__DIR__) . '/filters-archive-units.php';
/** @var Filter $documentFilters */
$documentFilters = include dirname(__DIR__) . '/filters-documents.php';

$filtersArchives = new SavedFilter();
$filtersArchives->filters = array_filter(
    $savedFilters->get('filters'),
    function ($v) {
        return isset($v->key) && preg_match('/^Archives\[/', $v->key);
    }
);
$filtersArchiveUnits = new SavedFilter();
$filtersArchiveUnits->filters = array_filter(
    $savedFilters->get('filters'),
    function ($v) {
        return isset($v->key) && preg_match('/^ArchiveUnits\[/', $v->key);
    }
);
$filtersDocuments = new SavedFilter();
$filtersDocuments->filters = array_filter(
    $savedFilters->get('filters'),
    function ($v) {
        return isset($v->key) && preg_match('/^Documents\[/', $v->key);
    }
);

$output = [
    'archives' => $archiveFilters->loadFilters($filtersArchives)->generateLoadedFilterContent(),
    'archive_units' => $archiveUnitsFilters->loadFilters($filtersArchiveUnits)->generateLoadedFilterContent(),
    'documents' => $documentFilters->loadFilters($filtersDocuments)->generateLoadedFilterContent(),
];

// traitement spécial pour les select2 ajax
$Keywords = TableRegistry::getTableLocator()->get('Keywords');
foreach (['Archives', 'ArchiveUnits'] as $model) {
    $underscored = Inflector::underscore($model);
    if ($pos = strpos($output[$underscored], $model . '[keyword_list]')) {
        $lists = [];
        /** @var EntityInterface $filter */
        foreach ($savedFilters->get('filters') as $filter) {
            if (preg_match('/^' . $model . '\[keyword_list]\[(\d+)]/', $filter->get('key'), $match)) {
                if (!isset($lists[$match[1]])) {
                    $lists[$match[1]] = [];
                }
                $lists[$match[1]][] = $filter->get('value');
            }
        }
        if ($lists) {
            foreach ($lists as $index => $ids) {
                $ids = array_unique($ids);
                $options = [];
                $query = $Keywords->find()
                    ->select(['id', 'name', 'code'])
                    ->where(['id IN' => $ids]);
                /** @var EntityInterface $k */
                foreach ($query as $k) {
                    $options[] = '<option value="' . $k->get('id') . '" selected="selected">'
                        . $k->get('code') . ' - ' . h($k->get('name')) . '</option>';
                }
                $needle = $model . '[keyword_list][' . $index . ']';
                $offset = strpos($output[$underscored], $needle);
                $pos = strpos($output[$underscored], '</option></select>', $offset);

                $output[$underscored] = substr($output[$underscored], 0, $pos)
                    . implode("\n", $options)
                    . substr($output[$underscored], $pos);
            }
        }
    }
}

echo json_encode($output);
