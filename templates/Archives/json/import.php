<?php

use Cake\I18n\DateTime as CakeDateTime;

if (!empty($errors)) {
    echo json_encode(['errors' => $errors], JSON_PRETTY_PRINT);
} elseif ($dry) {
    echo json_encode(
        [
            'success' => __(
                "Les paramètres ont été vérifiés le {0} et permettent l'envoi du fichier zip de l'archive",
                (new CakeDateTime())->nice()
            ),
        ],
        JSON_PRETTY_PRINT
    );
} else {
    echo json_encode(
        [
            'success' => __("L'archive a été importée le {0}", (new CakeDateTime())->nice()),
        ],
        JSON_PRETTY_PRINT
    );
}
