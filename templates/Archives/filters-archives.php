<?php

/**
 * @var Asalae\View\AppView $this
 */
return $this->Filter->create('search-archive-archive-filter')
    ->filter(
        'Archives.name',
        [
            'label' => __("Intitulé de l'archive"),
            'wildcard',
            'data-group' => __("Archive"),
        ]
    )
    ->filter(
        'Archives.description',
        [
            'label' => __("Description de l'archive"),
            'wildcard',
            'data-group' => __("Archive"),
        ]
    )
    ->filter(
        'Archives.keyword',
        [
            'label' => __("Mot clé de l'archive (saisie libre)"),
            'wildcard',
            'data-group' => __("Mots-clés"),
        ]
    )
    ->filter(
        'Archives.keyword_list',
        [
            'label' => __("Mots-clés de l'archive (liste)"),
            'options' => [],
            'empty' => $empty = __("-- Sélectionner les mots-clés --"),
            'data-placeholder' => $empty,
            'multiple' => true,
            'data-callback' => 'ajaxKeyword',
            'data-group' => __("Mots-clés"),
        ]
    )
    ->filter(
        'Archives.dua',
        [
            'label' => __("Statut de la DUA"),
            'options' => $duas,
            'empty' => __("-- Veuillez sélectionner une DUA --"),
            'data-group' => __("DUA"),
        ]
    )
    ->filter(
        'Archives.dua_duration',
        [
            'label' => __("Durée de la DUA"),
            'class' => 'with-select',
            'options' => $appraisalDuration,
            'empty' => __("-- Sélectionner une durée --"),
            'prepend' => $this->Form->control(
                'Archives.dua_duration_operator',
                [
                    'options' => $date_operators,
                    'templates' => [
                        'inputContainer' => '{{content}}',
                        'formGroup' => '{{input}}',
                        'select' => '<select name="{{name}}">{{content}}</select>',
                    ],
                ]
            ),
            'data-group' => __("DUA"),
        ]
    )
    ->filter(
        'Archives.dua_end',
        [
            'label' => __("Date de fin de la DUA"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
            'data-group' => __("DUA"),
        ]
    )
    ->filter(
        'Archives.appraisal_rules_final_action_code',
        [
            'label' => __("Sort final"),
            'options' => $final_action_codes,
            'empty' => __("-- Veuillez sélectionner une DUA --"),
            'data-group' => __("Sort Final"),
        ]
    )
    ->filter(
        'Archives.access_rules_end_date_vs_now',
        [
            'label' => __("Communicabilité"),
            'options' => $communicabilites,
            'empty' => __("-- Veuillez sélectionner une valeur --"),
            'data-group' => __("Communicabilité"),
        ]
    )
    ->filter(
        'Archives.created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
            'data-group' => __("Archive"),
        ]
    )
    ->filter(
        'Archives.archival_agency_identifier',
        [
            'label' => __("Identifiant de l'archive donné par le service d'Archives (cote)"),
            'wildcard',
            'data-group' => __("Archive"),
        ]
    )
    ->filter(
        'Archives.transferring_agency_identifier',
        [
            'label' => __("Identifiant de l'archive donné par le service versant"),
            'wildcard',
            'data-group' => __("Archive"),
        ]
    )
    ->filter(
        'Archives.originating_agency_identifier',
        [
            'label' => __("Identifiant de l'archive donné par le service producteur"),
            'wildcard',
            'data-group' => __("Archive"),
        ]
    );
