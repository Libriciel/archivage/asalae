<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Archives"));
$this->Breadcrumbs->add($title = __("Traitements par lot"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-cogs', $title))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);
$url = $this->getRequest()->getRequestTarget();
$pos = strpos($url, '?');
$buttons = [];
if ($this->Acl->check('/batch-treatments/add1')) {
    $filters = ($pos ? substr($url, $pos) : '');
    $buttons[] = $this->ModalForm
        ->create('add-treatment', ['size' => 'modal-xxl'])
        ->modal(__("Effectuer un traitement par lot"))
        ->step('/batch-treatments/add1' . $filters, 'addBatchTreatmentStep1')
        ->step('/batch-treatments/add2' . $filters, 'addBatchTreatmentStep2')
        ->step('/batch-treatments/add3' . $filters, 'addBatchTreatmentStep3')
        ->javascriptCallback('afterAddBatchTreatment')
        ->output(
            'button',
            $this->Fa->i('fa fa-cogs', __("Effectuer un traitement par lot"))
        )
        ->generate(
            [
                'type' => 'button',
                'class' => 'btn btn-primary',
                'escape' => false,
                'disabled' => $count === 0,
            ]
        );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

/** @var string $tableId */
$jsTableArchives = $this->Table->getJsTableObject($tableId);

$view = $this;
$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalView->create('description-archiveunit', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une archive"))
    ->output('function', 'viewUnitDescription', '/ArchiveUnits/description')
    ->generate();

echo $this->ModalView
    ->create('view-archive-unit-modal', ['size' => 'large'])
    ->modal(__("Visualiser une unité d'archives"))
    ->output(
        'function',
        'actionViewArchiveUnit',
        '/ArchiveUnits/view'
    )
    ->generate();

echo $this->Filter->create('archive-filter')
    ->saves($savedFilters)
    ->permanentFilter(
        'all',
        [
            'id' => 'display-all-archive-unit',
            'label' => __("Affichage"),
            'options' => [
                'all' => __("Afficher toutes les unités d'archives"),
            ],
            'empty' => __("Afficher uniquement les archives"),
        ]
    )
    ->permanentFilter(
        'profile_id',
        [
            'label' => __("Profil d'archives"),
            'options' => $profiles,
            'empty' => __("-- Sélectionner un profil d'archives --"),
        ]
    )
    ->permanentFilter(
        'agreement_id',
        [
            'label' => __("Accords de versement"),
            'options' => $agreements,
            'empty' => __("-- Sélectionner un accord de versement --"),
        ]
    )
    ->permanentFilter(
        'transferring_agency_id',
        [
            'label' => __("Service versant"),
            'options' => $transferring_agencies,
            'empty' => __("-- Sélectionner un service versant --"),
        ]
    )
    ->permanentFilter(
        'originating_agency_id',
        [
            'label' => __("Service producteur"),
            'options' => $originating_agencies,
            'empty' => __("-- Sélectionner un service producteur --"),
        ]
    )
    ->filter(
        'archival_agency_identifier',
        [
            'label' => __("Cote"),
            'wildcard',
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'original_total_size[value]',
        [
            'label' => __("Taille totale des fichiers"),
            'prepend' => $this->Input->operator(
                'original_total_size[operator]',
                '>=',
                ['id' => 'original-total-size-operator']
            ),
            'append' => $this->Input->mult(
                'original_total_size[mult]',
                'mo',
                ['id' => 'original-total-size-mult']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'original_total_count[value]',
        [
            'label' => __("Nombre de fichiers"),
            'prepend' => $this->Input->operator(
                'original_total_count[operator]',
                '>=',
                ['id' => 'original-total-count-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'keyword',
        [
            'label' => __("Mot clé"),
            'data-callback' => 'searchKeywordPopup',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->generateSection();

require 'ajax_treatments.php';
?>
<script>
    AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');

    function afterAddBatchTreatment(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Step')) {
            AsalaeModal.stepCallback(content, textStatus, jqXHR);
        } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var url = '<?=$this->Url->build('/archives/treatments')?>';
            AsalaeGlobal.interceptedLinkToAjax(url);
        }
    }
</script>
