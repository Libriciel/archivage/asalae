<?php

/**
 * @var Asalae\Utility\SedaAttachmentsIterator $binaries
 * @var Asalae\View\AppView                    $this
 * @var Cake\Datasource\EntityInterface        $archive
 * @var Cake\Datasource\EntityInterface        $transferReceipt
 * @var Cake\Datasource\EntityInterface        $archiveCreate
 * @var Cake\Datasource\EntityInterface        $firstIntegrity
 * @var Cake\Datasource\EntityInterface        $archival_agency
 * @var Cake\Datasource\EntityInterface        $transferring_agency
 * @var Cake\Datasource\EntityInterface[]      $integrities
 */

use Cake\Datasource\EntityInterface;
use Cake\I18n\FrozenTime;
use Cake\I18n\Number;
use Cake\Utility\Hash;

$identifier = $archive->get('archival_agency_identifier');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr-FR" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= h($identifier) ?></title>
    <style type="text/css">
        <?php require dirname(__DIR__, 2) . '/vendor/libriciel/seda2pdf/templates/style.css'; ?>

        table#list-files {
            font-family: monospace;
        }

        table#list-files tr th {
            min-width: 0;
        }

        table#list-files tr td {
            width: auto;
        }

        h4.h3 {
            margin: 5px 0;
        }

        p {
            margin: 5px 0;
        }

        .info {
            background-color: #e1f1f7;
            padding: 10px;
        }

        ul {
            margin: 0;
        }

        li {
            list-style-position: inside;
            margin: 2px 0;
        }
    </style>
</head>
<body>
<?php

echo $this->Html->tag(
    'h1.h1',
    __("Attestation d’archivage")
);
echo $this->Html2pdf->table(
    [
        __("Date de l'édition de l'attestation") => new FrozenTime(),
        __("Identité du service d'Archives")
        => $archival_agency->get('identifier') . ' ' . $archival_agency->get('name'),
        __("Identité du service versant")
        => $transferring_agency->get('identifier') . ' ' . $transferring_agency->get('name'),
        __("Identité du service producteur")
        => $originating_agency->get('identifier') . ' ' . $originating_agency->get('name'),
    ]
);

echo '<hr>';

// Bloc Archive
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Archive"));
/** @var EntityInterface[] $transfers */
$transfers = $archive->get('transfers');
$transferTable = [
    __("Cote") => $identifier,
    __("Nom de l'archive") => $archive->get('name'),
    __("Profil d'archive") => Hash::get($archive, 'profile.name'),
];
$trs = [];
foreach ($transferTable as $label => $value) {
    $value = nl2br(htmlentities($value));
    $trs[] = "<tr><th>$label</th><td>$value</td></tr>\n";
}
if (count($transfers) <= 1) {
    $label = __("Identifiant du transfert");
    $value = nl2br(htmlentities(Hash::get($archive, 'transfers.0.transfer_identifier')));
} else {
    $label = __("Identifiants des transferts");
    $values = Hash::extract($archive, 'transfers.{n}.transfer_identifier');
    $value = '<ul><li>'
        . implode('</li><li>', array_map(fn($v) => htmlentities($v), $values))
        . '</li></ul>';
}
$trs[] = "<tr><th>$label</th><td>$value</td></tr>\n";
echo '<table><tbody>' . implode("\n", $trs) . '</tbody></table>';
echo $this->Html->tag('/div'); // div.section

// Bloc opération
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Opération"));
echo $this->Html2pdf->table(
    [
        __("Nature de l'opération") => 'ARCHIVAGE',
        __("Date d'opération") => $archive->get('created'),
    ]
);
echo $this->Html->tag('/div'); // div.section

// Bloc Engagement du SA
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Le Service d'Archives s'engage"));
// Le Service d'Archives s'engage
echo $this->Html->tag('ul.info');
echo $this->Html->tag(
    'li',
    __("en l'exactitude des informations portées dans la présente attestation ;")
);
$volumeList = $this->Html->tag('ul');
$volumes = Hash::get($archive, 'secure_data_space.volumes');
/** @var EntityInterface $volume */
foreach ($volumes as $volume) {
    $volumeList .= $this->Html->tag(
        'li',
        $volume->get('location') ?: $volume->get('name'),
        ['escape' => true]
    );
}
$volumeList .= $this->Html->tag('/ul');
echo $this->Html->tag(
    'li',
    __(
        "à ce que les archives soient dûment enregistrées sur "
        . "l’ensemble des sites de conservation qui sont : {0}",
        $volumeList
    ),
    ['escape' => false]
);
if ($slDescription = Hash::get($archive, 'service_level.commitment')) {
    echo $this->Html->tag('li', $slDescription, ['escape' => true]);
}
if ($saCommitment) {
    echo $this->Html->tag('li', $saCommitment, ['escape' => true]);
}
echo $this->Html->tag('/ul');
echo $this->Html->tag('/div'); // div.section

// Conseil pour la conservation de la présente attestation
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Conseil pour la conservation de la présente attestation"));
echo $this->Html->tag('ul.info');
echo $this->Html->tag(
    'li',
    __(
        "Il est conseillé au service producteur de conserver les "
        . "attestations d’archivage afin de faire valoir ce que de droit."
    )
);
echo $this->Html->tag(
    'li',
    __(
        "Il est conseillé au service d'Archives de conserver ces "
        . "attestations a minima jusqu’à prescription des effets contractuels "
        . "entre le service d'Archives et le service Producteur"
    )
);
echo $this->Html->tag('/ul');
echo $this->Html->tag('/div');

// Durée de validité
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Durée de validité"));
echo $this->Html->tag('ul.info');
echo $this->Html->tag(
    'li',
    __("L’attestation est valable sans limite de temps")
);
echo $this->Html->tag('/ul');
echo $this->Html->tag('/div');

echo $this->Html->tag('/div'); // div.section

// Bloc Fichiers
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Détail des objets concernés"));
$sum = fn($total, $value) => $total + $value;
$dataSize = Number::toReadableSize(
    array_reduce(
        Hash::extract($archive, 'transfers.{n}.data_size'),
        $sum
    )
);
$dataCount = array_reduce(
    Hash::extract($archive, 'transfers.{n}.data_count'),
    $sum
);
$firstDataObject = $binaries->current() ?? [];
echo $this->Html2pdf->table(
    [
        __("Nombre d'objets numériques") => $dataCount,
        __("Poids total des objets numériques") => $dataSize,
        __("Algorithme empreintes") => $firstDataObject['hash_algo'],
        __("Date de versement") => Hash::get($archive, 'transfers.0.created'),
    ]
);

echo $this->Html->tag('h3', __("Liste des objets numériques"));
echo $this->Html->tag('table#list-files');
echo $this->Html->tag('thead');
echo $this->Html->tag('th', __("Identifiant archive"));
echo $this->Html->tag('th', __("Nom fichier"));
echo $this->Html->tag('th', __("Taille"));
echo $this->Html->tag('th', __("Empreinte"));
echo $this->Html->tag('/thead');
echo $this->Html->tag('tbody');
foreach ($binaries as $binary) {
    $hash = implode(
        '<wbr>',
        str_split(
            h($binary['hash']),
            20
        )
    );
    $filename = str_replace(
        '/',
        '/<wbr>',
        implode(
            '<wbr>',
            str_split(
                h($binary['filename']),
                20
            )
        )
    );
    $size = is_numeric($binary['size']) ? Number::toReadableSize($binary['size']) : $binary['size'];
    $size = str_replace(' ', '&nbsp;', $size);
    echo $this->Html->tag('tr');
    echo $this->Html->tag('td', $binary['archival_agency_identifier']);
    echo $this->Html->tag('td', $filename);
    echo $this->Html->tag('td', $size);
    echo $this->Html->tag('td', $hash);
    echo $this->Html->tag('/tr');
}
echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');
echo $this->Html->tag('/div'); // div.section
?>
</body>
</html>
