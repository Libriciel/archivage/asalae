<?php

/**
 * @var Asalae\View\AppView $this
 */

?>

<script>
    /**
     * Dl du csv
     */
    function downloadCsv(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var blob = new Blob([content], {type: 'text/csv'});
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = jqXHR.getResponseHeader('Content-Disposition').match(/filename=['"]?([^'"]+)['"]?/)[1];
            link.click();
        }
    }

    /**
     * Affichage des filtres supplémentaires
     */
    function toggleFilters(button) {
        var div = $('#indicators-filters');
        var icon = $(button).find('i');
        if (div.is(':visible')) {
            div.show().slideUp();
            icon.removeClass('fa-angle-double-up')
                .addClass('fa-angle-double-down');
        } else {
            div.hide().slideDown();
            icon.removeClass('fa-angle-double-down')
                .addClass('fa-angle-double-up');
        }
    }

    function disableOptions() {
        $('.indicators-col-select option').enable();
        var col1 = $('#indicators-col1-select');
        var col2 = $('#indicators-col2-select');
        col1.find('option[value="' + col2.val() + '"]').disable();
        col2.find('option[value="' + col1.val() + '"]').disable();
    }

    function changedColFn() {
        var col1 = $('#indicators-col1-select').val();
        var col2 = $('#indicators-col2-select').val();
        var table1 = $('#indicators-entities-table');
        var table2 = $('#indicators-sum-table');
        table1.find('.removable').remove();
        table2.find('.removable').remove();
        disableOptions();

        mergeTable(table1, $('#hidden-table-' + col1));
        mergeTable(table2, $('#total-' + col1));
        if (col2) {
            mergeTable(table1, $('#hidden-table-' + col2));
            mergeTable(table2, $('#total-' + col2));
        }
    }

    function mergeTable(table1, table2) {
        var thead1 = table1.find('thead');
        var thead2 = table2.find('thead');
        if (thead1.find('tr').length < 2) {
            thead1.append('<tr></tr>');
        }
        var thead1Trs = thead1.find('tr');
        var thead2Trs = thead2.find('tr');

        // merge du thead
        thead2Trs.eq(0).find('th, td').each(function (i) {
            thead1Trs.eq(0).append($(this).clone().addClass('col-' + i));
        });
        thead2Trs.eq(1).find('th, td').each(function (i) {
            thead1Trs.eq(1).append($(this).clone().addClass('col-' + i));
        });

        // merge du tbody
        var tbody1 = table1.find('tbody');
        var tbody2 = table2.find('tbody');
        var trs1 = tbody1.find('tr');
        tbody2.find('tr').each(function (i) {
            $(this).find('th, td').each(function (j) {
                trs1.eq(i).append($(this).clone().addClass('col-' + j))
            });
        });
    }

    $('.indicators-col-select').on('change', changedColFn);
    changedColFn();
</script>
