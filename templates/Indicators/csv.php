<?php

/**
 * @var Asalae\View\AppView           $this
 * @var Asalae\Model\Entity\User $user
 * @var Asalae\Form\IndicatorsCsvForm $form
 */

echo $this->Form->create($form, ['id' => $formId = uniqid('export-indicators-csv-')])
    . $this->Form->control(
        'delimiter',
        [
            'label' => __("Caractère séparateur des colonnes dans le fichier csv"),
            'type' => 'select',
            'options' => $form->delimiterOptions(),
            'value' => 0,
        ]
    )
    . $this->Form->control(
        'enclosure',
        [
            'label' => __("Caractère délimitateur des textes dans le fichier csv"),
            'type' => 'select',
            'options' => $form->enclosureOptions(),
            'value' => 0,
        ]
    )
    . $this->Form->control(
        'unit',
        [
            'label' => __("Unité des volumes"),
            'type' => 'select',
            'options' => $form->unitOptions(),
            'value' => 2,
        ]
    )
    . $this->Form->end();
