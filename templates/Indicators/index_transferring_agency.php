<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\I18n\DateTime as CakeDateTime;
use Cake\I18n\Number;
use Cake\ORM\Entity;
use Cake\Utility\Hash;

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Indicateurs par service versant"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-tachometer', __("Indicateurs par service versant")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$url = $this->getRequest()->getRequestTarget();
$pos = strpos($url, '?');
$filters = ($pos ? ('&' . substr($url, $pos + 1)) : '');

echo $this->Html->tag(
    'div.container.btn-separator',
    $this->Form->button(
        $this->Fa->charte('Ajouter', __("Export au format CSV")),
        [
            'type' => 'button',
            'onclick' => 'csvExport()',
            'class' => 'btn btn-primary float-none',
            'secure' => 'skip',
        ]
    )
);

echo $this->ModalForm
    ->create(
        'csv-indicators-modal',
        [
            'size' => 'large',
            'acceptButton' => $this->Form->button(
                $this->Fa->charte('Télécharger', __("Télécharger")),
                ['bootstrap-type' => 'primary', 'class' => 'accept']
            ),
        ]
    )
    ->modal(__("Export au format CSV"))
    ->javascriptCallback('downloadCsv')
    ->output(
        'function',
        'csvExport',
        '/Indicators/csv?by=transferring' . $filters
    )
    ->generate();

echo $this->Filter->create('indicators-filter')
    ->saves($savedFilters)
    ->permanentFilter(
        'transferring_agency_id',
        [
            'label' => __("Service Versant"),
            'options' => $transferring_agencies ?? [],
            'empty' => __("-- Veuillez sélectionner un Service Versant --"),
        ]
    )
    ->permanentFilter(
        'agreement_id',
        [
            'label' => __("Accord de versement"),
            'options' => $agreements ?? [],
            'empty' => __("-- Veuillez sélectionner un Accord de versement --"),
        ]
    )
    ->permanentFilter(
        'profile_id',
        [
            'label' => __("Profil d'archives"),
            'options' => $profiles ?? [],
            'empty' => __("-- Veuillez sélectionner un Profil d'archives --"),
        ]
    )
    ->permanentFilter(
        'date_begin',
        [
            'label' => __("Date de départ"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
            'prepend' => '',
        ]
    )
    ->permanentFilter(
        'date_end',
        [
            'label' => __("Date de fin"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
            'prepend' => '',
        ]
    )
    ->generateSection();

echo $this->Html->tag(
    'section',
    null,
    ['id' => 'transfers-index-sent-table-section', 'class' => 'bg-white paginable-section exploded-container']
);

if (!isset($data)) {
    echo $this->Html->tag('p', __("Votre service n'est pas concerné par les indicateurs."));
} else {
    echo $this->Html->tag('header')
        . $this->Html->tag('h4', __("Indicateurs par service versant"));

    echo $this->Html->tag('div', null, ['class' => 'l-actions h4']);

    echo $this->Fa->charteBtn(
        'Filtrer',
        __("Réinitialiser les filtres de recherche"),
        [
            'class' => 'btn-link display-active-filter',
            'text' => $this->Fa->i('fa-times fa-small-append fa-hidden-append text-danger'),
        ]
    );

    echo $this->Html->tag('/div')
        . $this->Html->tag('/header');

    $tmpl = ['inputContainer' => '<div class="form-group col-lg-6 {{type}}{{required}}">{{content}}</div>'];
    $opts = [
        'archives' => __("Archives"),
        'communications' => __("Communications"),
        'eliminations' => __("Éliminations"),
        'outgoing' => __("Transferts sortants"),
        'transfers' => __("Transferts"),
    ];
    echo $this->Html->tag(
        'div.form-input.container',
        $this->Html->tag(
            'div.row',
            /**
             * Tableau 1
             */
            $this->Form->control(
                'indicators-col1-select',
                [
                    'label' => __("Tableau 1"),
                    'class' => 'indicators-col-select',
                    'options' => $opts,
                    'templates' => $tmpl,
                    'default' => 'transfers',
                ]
            )

            /**
             * Tableau 2
             */
            . $this->Form->control(
                'indicators-col2-select',
                [
                    'label' => __("Tableau 2"),
                    'class' => 'indicators-col-select',
                    'options' => $opts,
                    'templates' => $tmpl,
                    'default' => 'archives',
                    'empty' => __("-- Sélectionner le Tableau 2 --"),
                ]
            )
        )
    );

    $hiddenTable = function (
        array $cols,
        string $title,
        string $table,
        array $data,
        array $params = []
    ) {
        /** @var Asalae\View\AppView $this */
        $thead = $this->Html->tag(
            'thead',
            $this->Html->tag('tr', $this->Html->tag('th', $title, ['colspan' => count($cols), 'class' => 'removable']))
            . $this->Html->tag('tr', '<th class="removable">' . implode('</th><th class="removable">', $cols))
        );
        $trs = [];
        $len = strlen($table) + 1;
        /** @var Cake\Datasource\EntityInterface $entity */
        foreach ($data as $entity) {
            $tr = $this->Html->tag(
                'tr',
                null,
                Hash::get($entity, 'type_entity.code') === 'SO' ? ['class' => 'font-weight-bold'] : []
            );
            foreach ($entity->get('indicators') as $name => $value) {
                if (substr($name, 0, $len) === $table . '_') {
                    if (strpos($name, 'max_traitement')) {
                        $value = (new CakeDateTime($value ?: 0))->format('H:i:s');
                    } elseif (strpos($name, 'volume')) {
                        $value = Number::toReadableSize($value ?: 0);
                    }
                    $tr .= $this->Html->tag('td', $value, ['class' => 'removable']);
                }
            }
            $tr .= $this->Html->tag('/tr');
            $trs[] = $tr;
        }
        $tbody = $this->Html->tag('tbody', implode('', $trs));
        return $this->Html->tag(
            'table',
            $thead . $tbody,
            $params + ['class' => 'hidden', 'id' => 'hidden-table-' . $table]
        );
    };

    $totalData = [];
    if (isset($totalSA)) {
        $totalData[] = new Entity(
            [
                'type_entity' => [],
                'indicators' => $totalSA,
            ]
        );
    }

    /**
     * Archives
     */
    $cols = [
        __("Volume total"),
        __("Volume doc."),
        __("Volume cons."),
        __("Volume diff."),
        __("Volume horo."),
        __("Nb archives"),
        __("Nb unités archives"),
        __("Nb Doc."),
    ];
    echo $hiddenTable($cols, __("Archives"), 'archives', $data);
    echo $hiddenTable($cols, __("Archives"), 'archives', $totalData, ['id' => 'total-archives']);

    /**
     * Communications
     */
    $cols = [
        __("Volume total"),
        __("Nb de documents"),
        __("Nb de communications"),
        __("Nb de demandes rejetées"),
        __("Nb de dérogations instruites"),
        __("Nb de dérogations rejetées"),
    ];
    echo $hiddenTable($cols, __("Communications"), 'communications', $data);
    echo $hiddenTable($cols, __("Archives"), 'communications', $totalData, ['id' => 'total-communications']);

    /**
     * Éliminations
     */
    $cols = [
        __("Volume total"),
        __("Nb de documents"),
        __("Nb d'éliminations"),
        __("Nb de demandes rejetées"),
    ];
    echo $hiddenTable($cols, __("Éliminations"), 'eliminations', $data);
    echo $hiddenTable($cols, __("Archives"), 'eliminations', $totalData, ['id' => 'total-eliminations']);

    /**
     * Transferts sortants
     */
    $cols = [
        __("Volume total"),
        __("Nb de pièces jointes"),
        __("Nb de transfert"),
        __("Délai max de traitement"),
        __("Nb de transferts rejetés"),
    ];
    echo $hiddenTable($cols, __("Transferts sortants"), 'outgoing', $data);
    echo $hiddenTable($cols, __("Archives"), 'outgoing', $totalData, ['id' => 'total-outgoing']);

    /**
     * Transferts
     */
    $cols = [
        __("Volume total"),
        __("Nb PJs"),
        __("Nb Tr."),
        __("Délai max"),
        __("Nb Tr. rejetés"),
    ];
    echo $hiddenTable($cols, __("Transferts"), 'transfers', $data);
    echo $hiddenTable($cols, __("Archives"), 'transfers', $totalData, ['id' => 'total-transfers']);

    /**
     * Indicateurs
     */
    $thead = $this->Html->tag(
        'thead',
        $this->Html->tag('tr', $this->Html->tag('th', __("Services Versants"), ['rowspan' => 2]))
    );
    $tbody = '';
    foreach ($data as $entity) {
        $tbody .= $this->Html->tag(
            'tr',
            $this->Html->tag(
                'th',
                str_repeat('&nbsp;&nbsp;', $entity->get('lvl')) . h($entity->get('name'))
            ),
            Hash::get($entity, 'type_entity.code') === 'SO' ? ['class' => 'font-weight-bold'] : []
        );
    }
    echo $this->Html->tag(
        'table',
        $thead . $tbody,
        [
            'class' => 'table table-striped table-hover smart-td-size no-padding indicators-table',
            'id' => 'indicators-entities-table',
        ]
    );

    /**
     * Totaux Service d'archives
     */
    if (isset($totalSA)) {
        $thead = $this->Html->tag(
            'thead',
            $this->Html->tag('tr', $this->Html->tag('th', __("Service d'Archives"), ['rowspan' => 2]))
        );
        $tbody = $this->Html->tag('tr', $this->Html->tag('th', __("Service d'Archives")));
        echo $this->Html->tag(
            'table',
            $thead . $tbody,
            [
                'class' => 'table table-striped table-hover smart-td-size no-padding indicators-table',
                'id' => 'indicators-sum-table',
            ]
        );
    }
}

require 'index_common.php';
