<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => 'edit-validation-chain',
    ]
);
require 'addedit-common.php';
echo $this->Form->end();
?>
<script>
    var defaultC = $('#edit-validation-chain-default');
    var active = $('#edit-validation-chain-active');
    var hiddenActive = active.closest('.form-group').find('[type="hidden"]').first();

    // un circuit par défaut doit être actif
    defaultC.change(function () {
        if (defaultC.prop('checked')) {
            active.prop('checked', true).disable();
            hiddenActive.val(1);
        } else {
            active.enable();
            hiddenActive.val(0);
        }
    });
</script>
