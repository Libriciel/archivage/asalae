<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $entity
 * @var EntityInterface     $validationStage
 * @var EntityInterface     $validationActors
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$map = function (EntityInterface $entity): EntityInterface {
    $stages = [];
    foreach ($entity->get('validation_stages') as $validationStage) {
        $msg = sprintf(
            "%s (%s)\n",
            $validationStage->get('name'),
            $validationStage->get('all_actors_to_completetrad')
        );
        foreach ($validationStage->get('validation_actors') as $validationActors) {
            if ($validationActors->get('app_type') === 'USER') {
                $name = Hash::get($validationActors, 'user.name');
                $username = Hash::get($validationActors, 'user.username');
                $actor = __(
                    "{0} ({1} par {2})",
                    $validationActors->get('app_typetrad'),
                    $name ?: $username,
                    $validationActors->get('type_validationtrad')
                );
            } else {
                $actor = $validationActors->get('app_typetrad');
            }
            $msg .= sprintf("    - %s\n", $actor);
        }
        $stages[] = trim($msg);
    }
    $entity->set('validation_stages', $stages ? '- ' . implode("\n- ", $stages) : '');
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'name' => __("Nom"),
        'app_typetrad' => __("Type"),
        'description' => __("Description"),
        'validation_stages' => __("Etapes"),
        'default' => __("Défaut"),
        'active' => __("Actif"),
        'created' => __("Date de création"),
        'modified' => __("Date de modification"),
    ],
    $results,
    $map
);
