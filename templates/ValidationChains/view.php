<script>
    function callbackDisplayViewActors(value, context) {
        var divGroup = $('<div class="td-group-sub-sub"></div>');
        var span = $('<span class="td-group-fieldname"></span>').append(__("Type"));
        var div = $('<div class="td-group-fieldvalue"></div>').text(value.type);
        divGroup.append(span).append(div);

        var divGroup2 = $('<div class="td-group-sub-sub"></div>');
        var span2 = $('<span class="td-group-fieldname"></span>').append(__("Acteurs"));
        var div2 = $('<div class="td-group-fieldvalue"></div>').append(callbackDisplayActors(value.validation_actors))
        divGroup2.append(span2).append(div2);

        return $('<div class="td-group"></div>').append(divGroup).append(divGroup2);
    }
</script>
<?php
/**
 * @var Asalae\View\AppView $this
 * @var array               $fields
 */

echo $this->Html->tag('article');
echo $this->Html->tag('header.bottom-space');
echo $this->Html->tag('h3', h($chain->get('name')));
echo $this->Html->tag('p', h($chain->get('description')));
echo $this->Html->tag('/header');

echo $this->Html->tag(
    'table',
    $this->Html->tag(
        'tbody',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Type"))
            . $this->Html->tag('td', $chain->get('app_typetrad'))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Circuit par défaut"))
            . $this->Html->tag('td', $chain->get('default') ? __("Oui") : __("Non"))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Circuit utilisable"))
            . $this->Html->tag(
                'td' . ($chain->get('usable') ? '' : '.alert.alert-warning'),
                $chain->get('usable') ? __("Oui") : __("Non")
            )
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Activé"))
            . $this->Html->tag('td', $chain->get('active') ? __("Oui") : __("Non"))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Date de création"))
            . $this->Html->tag('td', $chain->get('created'))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Date de modification"))
            . $this->Html->tag('td', $chain->get('modified'))
        )
    ),
    ['class' => 'table table-striped table-hover fixed-left-th']
);


echo $this->Html->tag(
    'header.top-space',
    $this->Html->tag('h2', __("Liste des étapes de validation"), ['class' => 'h4'])
);

$data = [];
foreach ((array)$chain->get('validation_stages') as $stage) {
    $name = h($stage->get('name'));
    $id = preg_replace('/\W/', '_', $name);
    $fields[$id] = [
        'label' => $name,
        'callback' => 'callbackDisplayViewActors',
    ];
    $data[$id] = [
        'type' => $stage->get('all_actors_to_completetrad'),
        'validation_actors' => $stage->get('validation_actors'),
    ];
}
if ($data) {
    echo $this->Table
        ->create('stages-table', ['class' => 'table table-striped table-hover'])
        ->fields($fields)
        ->data([$data])
        ->params(
            [
                'identifier' => 'id',
            ]
        );
}
echo $this->Html->tag('/article');
