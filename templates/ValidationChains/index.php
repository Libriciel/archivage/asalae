<script>
    function callbackDisplayActors(value) {
        if (!Array.isArray(value)) {
            return null;
        }
        var ul = $('<ul></ul>');
        var message;

        for (var i = 0; i < value.length; i++) {
            if (value[i].app_type === 'USER') {
                message = __(
                    "{0} ({1} par {2})",
                    value[i].app_typetrad,
                    value[i].user.name ? value[i].user.name : value[i].user.username,
                    value[i].type_validationtrad
                );
            } else {
                message = value[i].app_typetrad;
            }
            ul.append($('<li></li>').text($('<span></span>').html(message).text()));
        }

        return ul;
    }

    function callbackDisplayStages(value) {
        if (!Array.isArray(value)) {
            return null;
        }
        var ol = $('<ol></ol>');
        var message;
        var li;
        for (var i = 0; i < value.length; i++) {
            message = "<b>%s</b> (%s)".format(
                $('<span></span>').html(value[i].name).text(),
                value[i].all_actors_to_completetrad
            );
            li = $('<li></li>').html(message);
            li.append(callbackDisplayActors(value[i].validation_actors));
            ol.append(li);
        }

        return ol;
    }
</script>
<?php
/**
 * @var Asalae\View\AppView $this
 */

echo $this->element('modal', ['idTable' => $tableId]);

$jsTable = $this->Table->getJsTableObject($tableId);

echo $this->ModalForm
    ->create('edit-validation-chain', ['size' => 'large'])
    ->modal(__("Modification du circuit de validation"))
    ->javascriptCallback('afterChangeAddValidationChain')
    ->output(
        'function',
        'editValidationChain',
        '/ValidationChains/edit'
    )
    ->generate();

echo $this->ModalView
    ->create('view-validation-chain', ['size' => 'large'])
    ->modal(__("Visualisation du circuit"))
    ->output(
        'function',
        'actionViewValidationChain',
        '/ValidationChains/view'
    )
    ->generate();

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Circuits de validation"));
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-road', __("Circuits de validation")))
    . $this->Breadcrumbs->render()
);

$buttons = [];
if ($this->Acl->check('/ValidationChains/add')) {
    echo $this->ModalForm
        ->create('add-validation-chain-modal')
        ->modal(__("Ajout d'un circuit de validation"))
        ->javascriptCallback('afterChangeAddValidationChain')
        ->output(
            'function',
            'addValidationChain',
            '/ValidationChains/add'
        )
        ->generate();
    $buttons[] = $this->Html->tag(
        'button',
        $this->Fa->charte('Ajouter', __("Ajouter un circuit de validation")),
        ['class' => 'btn btn-success', 'type' => 'button', 'onclick' => 'addValidationChain()']
    );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->Filter->create('validation-chains-filter')
    ->saves($savedFilters)
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'app_type',
        [
            'label' => __("Type de circuit"),
            'options' => $app_types,
            'multiple' => true,
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'modified',
        [
            'label' => __("Date de modification"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->filter(
        'active',
        [
            'label' => __("Actifs"),
            'options' => [
                '1' => __("Oui"),
                '0' => __("Non"),
            ],
        ]
    )
    ->filter(
        'default',
        [
            'label' => __("Défaut"),
            'options' => [
                '1' => __("Oui"),
                '0' => __("Non"),
            ],
        ]
    )
    ->generateSection();

require 'ajax_index.php';
?>
<script>

    /**
     * Callback d'après ajout ou édition d'une chaine de validation
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterChangeAddValidationChain(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
        }
    }

    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    var table = <?=$jsTable?>;

    function actionSetDefaultValidationChain(id) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/validation-chains/set-default')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            success: function () {
                AsalaeGlobal.updatePaginationAjax('#validation-chains-index-table-section');
            }
        })
    }
</script>
