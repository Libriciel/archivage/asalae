<?php
/**
 * @var Asalae\View\AppView $this
 */
?>
    <script>
        function sendMail(id) {
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build('/ValidationChains/resendMail')?>/' + id
            });
        }
    </script>
<?php
if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'ValidationChains.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'ValidationChains.app_typetrad' => [
                'label' => __("Type"),
                'escape' => false,
                'order' => 'app_type',
                'filter' => [
                    'app_type[0]' => [
                        'id' => 'filter-app_type-0',
                        'label' => false,
                        'aria-label' => __("Type"),
                        'options' => $app_types,
                    ],
                ],
            ],
            'ValidationChains.description' => [
                'label' => __("Description"),
                'display' => false,
            ],
            'ValidationChains.validation_stages' => [
                'label' => __("Etapes"),
                'callback' => 'callbackDisplayStages',
            ],
            'ValidationChains.default' => [
                'label' => __("Défaut"),
                'type' => 'boolean',
                'callback' => 'TableHelper.boolean',
                'filter' => [
                    'default[0]' => [
                        'id' => 'filter-default-0',
                        'label' => false,
                        'aria-label' => __("Défaut"),
                        'options' => [
                            '1' => __("Oui"),
                            '0' => __("Non"),
                        ],
                    ],
                ],
            ],
            'ValidationChains.active' => [
                'label' => __("Actif"),
                'type' => 'boolean',
                'callback' => 'TableHelper.boolean',
                'filter' => [
                    'active[0]' => [
                        'id' => 'filter-active-0',
                        'label' => false,
                        'aria-label' => __("Actif"),
                        'options' => [
                            '1' => __("Oui"),
                            '0' => __("Non"),
                        ],
                    ],
                ],
            ],
            'ValidationChains.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'ValidationChains.modified' => [
                'label' => __("Date de modification"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'modified',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'modified[0]' => [
                        'id' => 'filter-modified-0',
                        'label' => __("Date de modification"),
                        'prepend' => $this->Input->operator('dateoperator_modified[0]', '>='),
                        'append' => $this->Date->picker('#filter-modified-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'modified[1]' => [
                        'id' => 'filter-modified-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_modified[1]', '<='),
                        'append' => $this->Date->picker('#filter-modified-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'ValidationChains.id',
            'favorites' => true,
            'sortable' => true,
            'classEval' => 'data[{index}].ValidationChains.default ? "info" : ""',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionSetDefaultValidationChain({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-bookmark'),
                'data-action' => __("Définir par défaut"),
                'title' => $title = __("Définir ''{0}'' comme circuit de ''{1}'' par défaut", '{1}', '{2}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/validation-chains/set-default'),
                'displayEval' => 'data[{index}].ValidationChains.active && !data[{index}].ValidationChains.default',
                'params' => ['ValidationChains.id', 'ValidationChains.name', 'ValidationChains.app_typetrad'],
            ],
            [
                'href' => $this->Url->build('/validation-stages/index') . '/{0}',
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-list-ol'),
                'title' => $title = __("Etapes de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/validation-stages/index'),
                'params' => ['ValidationChains.id', 'ValidationChains.name'],
            ],
            [
                'onclick' => "actionViewValidationChain({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'data-action' => __("Visualiser"),
                'title' => __("Visualiser {0}", '{1}'),
                'aria-label' => __("Visualiser {0}", '{1}'),
                'display' => $this->Acl->check('/validation-chains/view'),
                'params' => ['ValidationChains.id', 'ValidationChains.name'],
            ],
            [
                'onclick' => "editValidationChain({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'display' => $this->Acl->check('/validation-chains/edit'),
                'params' => ['ValidationChains.id', 'ValidationChains.name'],
            ],
            [
                'type' => "button",
                'class' => "btn-link",
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/validation-chains/delete')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/validation-chains/delete'),
                'displayEval' => 'data[{index}].ValidationChains.deletable',
                'confirm' => __("Êtes-vous sûr de vouloir supprimer ce circuit ?"),
                'params' => ['ValidationChains.id', 'ValidationChains.name'],
            ],
            [
                'onclick' => "sendMail({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-envelope'),
                'data-action' => __("Renvoyer mail"),
                'title' => $title = __("Renvoyer les mails pour la validation par courrier électronique"),
                'aria-label' => $title,
                'displayEval' => 'data[{index}].ValidationChains.has_mail_agent',
                'params' => ['ValidationChains.id'],
            ],
            [
                'onclick' => "addValidationChain({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'data-action' => __("Dupliquer"),
                'label' => $this->Fa->i('fa-files-o'),
                'title' => __("Dupliquer le circuit de validation"),
                'aria-label' => __("Dupliquer {0}", '{1}'),
                'display' => $this->Acl->check('/ValidationChains/add'),
                'params' => ['ValidationChains.id', 'ValidationChains.name'],
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des circuits de validation"),
        'table' => $table,
    ]
);
