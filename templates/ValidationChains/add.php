<?php

/**
 * @var Asalae\View\AppView $this
 */
/** @var array $defaults */

echo $this->Form->create($entity, ['idPrefix' => 'add-validation-chain']);
require 'addedit-common.php';
echo $this->Form->end();
?>
<script>
    var defaultsValidationChains = {};
    defaultsValidationChains = <?=json_encode($defaults)?>;

    var defaultC = $('#add-validation-chain-default');
    var active = $('#add-validation-chain-active');
    var hiddenActive = active.closest('.form-group').find('[type="hidden"]').first();
    var hiddenDefault = defaultC.closest('.form-group').find('[type="hidden"]').first();
    $('#add-validation-chain-app-type').change(function () {
        var value = $(this).val();
        if (!value) {
            return;
        }
        if (defaultsValidationChains.indexOf(value) >= 0) {
            // ajout d'un circuit - type avec défaut - défaut optionnel
            active.enable();
            defaultC.enable();
            hiddenActive.val(0);
            hiddenDefault.val(0);
        } else {
            // ajout d'un circuit - type sans défaut - défaut obligatoire
            active.prop('checked', true).disable();
            defaultC.prop('checked', true).disable();
            hiddenActive.val(1);
            hiddenDefault.val(1);
        }
    });
    // un circuit par défaut doit être actif
    defaultC.change(function () {
        if (defaultC.prop('checked')) {
            active.prop('checked', true).disable();
            hiddenActive.val(1);
        } else {
            active.enable();
            hiddenActive.val(0);
        }
    });
</script>
