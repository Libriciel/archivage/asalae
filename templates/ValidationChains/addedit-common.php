<?php

/**
 * @var Asalae\View\AppView                 $this
 * @var array                               $app_types
 * @var Asalae\Model\Entity\ValidationChain $entity
 */
echo $this->Form->control('app_type', ['type' => 'hidden', 'id' => false])
    . $this->Form->control(
        'app_type',
        [
            'label' => __("Type de circuit de validation"),
            'required' => true,
            'options' => $app_types,
            'empty' => __("-- Sélectionner un type de circuit de validation --"),
        ] + (
        $entity->get('id')
            ? ['disabled' => true]
            : []
        )
    )
    . $this->Form->control('name', ['label' => __("Nom"), 'required'])
    . $this->Form->control('description', ['label' => __("Description")])
    . $this->Form->control(
        'active',
        [
            'label' => __("Actif"),
            'type' => 'checkbox',
        ] + (
            // entité par défaut ne peut être désactivé, sauf si on est en duplication
        ($entity->get('default') && $entity->get('id'))
            ? [
                'onclick' => 'return false',
                'style' => 'cursor: not-allowed',
                'checked',
            ]
            : []
        )
    )
    . $this->Form->control(
        'default',
        [
            'label' => __("Circuit de validation par défaut"),
            'type' => 'checkbox',
        ] + (
            // s'il est déjà par défaut (et qu'on est pas dans une duplication),
            // on ne peut pas décocher, et s'il est le seul actif de son type, on ne peut pas non plus
        ($entity->get('default') && $entity->get('id')) || ($isOnlyActiveOfThisType ?? false)
            ? [
                'onclick' => 'return false',
                'style' => 'cursor: not-allowed',
                'checked',
            ]
            : []
        )
    );
?>

<script>
    $('#add-validation-chain-default,#edit-validation-chain-default').change(function (e) {
        var value = $(this).val();
        if (!value) {
            return;
        }
        if (e.target.checked) {
            $('#add-validation-chain-active,#edit-validation-chain-active')
                .prop('checked', true)
                .disable();
            $('#modal-add-validation-chain-modal').find('form [name="active"][type="hidden"]').first().val(1);
            $('#modal-edit-validation-chain').find('form [name="active"][type="hidden"]').first().val(1);
        } else {
            $('#add-validation-chain-active,#edit-validation-chain-active')
                .enable();
            $('#modal-add-validation-chain-modal').find('form [name="active"][type="hidden"]').first().val(0);
            $('#modal-edit-validation-chain').find('form [name="active"][type="hidden"]').first().val(0);
        }
    });
</script>
