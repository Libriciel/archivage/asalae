<?php

/**
 * @var Asalae\View\AppView             $this
 * @var Cake\Datasource\EntityInterface $entity
 */

$formId = 'form-add-archive-keyword';
$searchId = 'add-archive-keyword-search';
$codeId = 'add-archive-keyword-access-rule-access-rule-code-id';
$startDateId = 'add-archive-keyword-access-rule-start-date';
$useParentRuleId = 'add-archive-keyword-use-parent-access-rule';
echo $this->Form->create($entity, ['id' => $formId, 'idPrefix' => 'add-archive-keyword']);
require 'addedit-common.php';
echo $this->Form->end();
