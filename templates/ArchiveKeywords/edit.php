<?php

/**
 * @var Asalae\View\AppView             $this
 * @var Cake\Datasource\EntityInterface $entity
 */

$formId = 'form-edit-archive-keyword';
$searchId = 'edit-archive-keyword-search';
$codeId = 'edit-archive-keyword-access-rule-access-rule-code-id';
$startDateId = 'edit-archive-keyword-access-rule-start-date';
$useParentRuleId = 'edit-archive-keyword-use-parent-access-rule';
echo $this->Form->create($entity, ['id' => $formId, 'idPrefix' => 'edit-archive-keyword', 'class' => 'ui-tabs']);
require 'addedit-common.php';
echo $this->Form->end();
