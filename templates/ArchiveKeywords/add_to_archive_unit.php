<?php

/**
 * @var Asalae\View\AppView             $this
 * @var Cake\Datasource\EntityInterface $entity
 */

$formId = 'form-add-to-archive-unit-archive-keyword';
$searchId = 'add-to-archive-unit-archive-keyword-search';
$codeId = 'add-to-archive-unit-archive-keyword-access-rule-access-rule-code-id';
$startDateId = 'add-to-archive-unit-archive-keyword-access-rule-start-date';
$useParentRuleId = 'add-to-archive-unit-archive-keyword-use-parent-access-rule';
echo $this->Form->create($entity, ['id' => $formId, 'idPrefix' => 'add-to-archive-unit-archive-keyword']);
require 'addedit-common.php';
echo $this->Form->end();
