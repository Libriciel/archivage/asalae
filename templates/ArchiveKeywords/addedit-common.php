<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

echo $this->Flash->render();

echo $this->Form->control(
    'search',
    [
        'label' => __("Rechercher un mot clé"),
        'options' => [],
        'empty' => true,
        'data-placeholder' => __("-- Sélectionner un mot clé -- "),
        'help' => '<button type="button" onclick="loadKeywordData(this)" class="btn btn-success">'
            . '<i class="fa fa-check-square-o" aria-hidden="true"></i> '
            . __("Sélectionner")
            . '</button>',
    ]
);
echo '<hr>';

$templates = function ($prepend = '', $append = '') {
    /** @var Asalae\View\AppView $this */
    return [
        'label' => "<label{{attrs}}>{{text}}</label> " . $this->Html->tag(
            'button.btn-link',
            $this->Fa->i('fa-tags')
                . $this->Html->tag('span.sr-only', __("Afficher les attributs")),
            [
                'type' => 'button',
                'onclick' => "$(this).closest('.form-group').find('.attributesList').toggle('blind')",
            ]
        ),
        'inputContainer'
        => '<div class="form-group {{type}}{{required}}">' . $prepend . '{{content}}' . $append . '</div>',
        'inputContainerError'
        => '<div class="form-group has-error {{type}}{{required}}">'
            . $prepend . '{{content}}{{error}}' . $append . '</div>',
    ];
};
echo $this->Form->control(
    'content',
    [
        'label' => 'KeywordContent',
        'class' => 'keyword-name',
        'templates' => $templates(
            '',
            $this->Html->tag(
                'div.attributesList',
                $this->Form->control(
                    'content_languageID',
                    [
                        'label' => 'languageID',
                        'help' => __("The identifier of the language used in the corresponding text string."),
                    ]
                )
                . $this->Form->control(
                    'content_role',
                    [
                        'label' => 'role',
                        'help' => __("Rôle du mot-clé."),
                    ]
                ),
                ['style' => 'display: none']
            )
        ),
    ]
);
$info = __(
    "Identifiant du mot clé dans un référentiel, par exemple "
    . "pour un lieu son Code Officiel Géographique selon l'INSEE."
);
echo $this->Form->control(
    'reference',
    [
        'label' => [
            'text' => 'KeywordReference',
            'title' => $info,
            'style' => 'cursor: help',
        ],
        'class' => 'keyword-code',
        'help' => __("Optionnel"),
        'templates' => $templates(
            $this->Html->tag(
                'button.btn-link',
                $this->Fa->i('fa-info-circle')
                . $this->Html->tag('span.sr-only', __("Aide : {0}", $info)),
                [
                    'type' => 'button',
                    'data-toggle' => 'tooltip',
                    'data-original-title' => $info,
                    'style' => 'cursor: help',
                ]
            ) . ' ',
            $this->Html->tag(
                'div.attributesList',
                $this->Form->control(
                    'reference_schemeID',
                    [
                        'label' => 'schemeID',
                        'help' => __("The identification of the identification scheme."),
                        'class' => 'keyword-list-id',
                    ]
                )
                . $this->Form->control(
                    'reference_schemeName',
                    [
                        'label' => 'schemeName',
                        'help' => __("The name of the identification scheme."),
                        'class' => 'keyword-list-name',
                    ]
                )
                . $this->Form->control(
                    'reference_schemeAgencyName',
                    [
                        'label' => 'schemeAgencyName',
                        'help' => __("The name of the agency that maintains the identification scheme."),
                    ]
                )
                . $this->Form->control(
                    'reference_schemeVersionID',
                    [
                        'label' => 'schemeVersionID',
                        'help' => __("The version of the identification scheme."),
                        'class' => 'keyword-list-version',
                    ]
                )
                . $this->Form->control(
                    'reference_schemeDataURI',
                    [
                        'label' => 'schemeDataURI',
                        'help' => __(
                            "The Uniform Resource Identifier that identifies "
                            . "where the identification scheme data is located."
                        ),
                    ]
                )
                . $this->Form->control(
                    'reference_schemeURI',
                    [
                        'label' => 'schemeURI',
                        'help' => __(
                            "The Uniform Resource Identifier that identifies"
                            . " where the identification scheme is located."
                        ),
                    ]
                ),
                ['style' => 'display: none']
            )
        ),
    ]
);
echo $this->Form->control(
    'type',
    [
        'label' => 'KeywordType : ' . __("Type de mot clé."),
        'help' => __("Optionnel"),
        'options' => $types,
        'empty' => __("-- Sélectionner un type --"),
        'templates' => $templates(
            '',
            $this->Html->tag(
                'div.attributesList',
                $this->Form->control(
                    'type_listVersionID',
                    [
                        'label' => 'listVersionID',
                        'help' => __("The version of the code list."),
                    ]
                ),
                ['style' => 'display: none']
            )
        ),
    ]
);

if ($sedaVersion === 'seda1.0' || $sedaVersion === 'seda0.2') {
    echo $this->Form->fieldset(
        $this->Form->control(
            'use_parent_access_rule',
            [
                'label' => __("Utilise les règles d'accès de la description d'archives"),
                'type' => 'checkbox',
                'default' => true,
            ]
        )
        . $this->Form->control(
            'access_rule.access_rule_code_id',
            [
                'label' => __("Délai de communicabilité"),
                'options' => $codes,
                'disabled' => $entity->get('use_parent_access_rule'),
                'required' => true,
                'empty' => __("-- Sélectionner un délai --"),
            ]
        )
        . $this->Form->control(
            'access_rule.start_date',
            [
                'label' => __("Date de départ du calcul (communicabilité)"),
                'type' => 'text',
                'disabled' => $entity->get('use_parent_access_rule'),
                'append' => $this->Date->picker('#' . $startDateId),
                'class' => 'datepicker',
                'placeholder' => __("jj/mm/aaaa"),
            ]
        ),
        ['legend' => __("Restriction d'accès")]
    );
}
?>
<script>
    function ajaxSearchKeyword(element) {
        $(element).select2({
            ajax: {
                url: '<?=$this->Url->build('/Keywords/populateSelect')?>',
                method: 'POST',
                delay: 250
            },
            escapeMarkup: function (v) {
                return $('<span></span>').html(v).text();
            },
            dropdownParent: $(element).closest('.modal'),
            allowClear: true
        });
        var button = $(element).siblings('.help-block').find('button').hide();
        $(element).on('change', function () {
            var actualValue = $('.keyword-name').val();
            if (!actualValue) {
                button.click();
            } else {
                button.show();
            }
        });
    }

    ajaxSearchKeyword('#<?=$searchId?>');

    function loadKeywordData(button) {
        var select = $(button).closest('.form-group').find('select');
        var id = select.val();
        if (!id) {
            return;
        }
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/Keywords/getData')?>/' + id,
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                $('.keyword-name').val(content.name);
                $('.keyword-code').val(content.code);
                $('.keyword-list-id').val(content.keyword_list.identifier);
                $('.keyword-list-name').val(content.keyword_list.name);
                $('.keyword-list-version').val(content.keyword_list.version);
            }
        });
    }

    $('#<?=$useParentRuleId?>').on('change', function () {
        var checked = $(this).prop('checked');
        $('#<?=$codeId?>, #<?=$startDateId?>').prop('disabled', checked);
    });
</script>
