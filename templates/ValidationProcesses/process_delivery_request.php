<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $deliveryRequest
 */

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;

end($prevs);
$lastPrev = key($prevs);
reset($prevs);
$form = $this->Form->create($entity, ['idPrefix' => 'validate-delivery-request'])
    . $this->Form->control(
        'action',
        [
            'label' => __("Action"),
            'options' => $actions,
            'empty' => __("-- Sélectionner une action --"),
        ]
    )
    . $this->Form->control('comment', ['label' => __("Commentaire")])
    . $this->Form->control(
        'prev_stage_id',
        [
            'label' => __("Retourner à l'étape"),
            'options' => $prevs,
            'default' => $lastPrev,
        ]
    );

if (!empty($hash)) {
    $form .= $this->Html->tag('div#sign-validate-delivery-request')
        . $this->LiberSign->insertSignForm('#sign-validate-delivery-request', [$deliveryRequest->id => $hash])
        . $this->Html->tag('/div');
}
$form .= $this->Form->end();

$tabs = $this->Tabs->create('tabs-view-delivery-request', ['class' => 'row no-padding']);
$tabs->add(
    'tab-validate-delivery-request-valitation',
    $this->Fa->i('fa-check-square-o', __("Validation")),
    $form
);

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($deliveryRequest->get('identifier')));
$infos .= $this->Html->tag('p', h($deliveryRequest->get('comment')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Demande de communication"));
$infos .= $this->ViewTable->generate(
    $deliveryRequest,
    [
        __("Identifiant") => 'identifier',
        __("Etat") => 'statetrad',
        __("Commentaire") => 'comment',
        __("Demande de dérogation") => 'derogation',
        __("Entité demandeur") => 'created_user.org_entity.name',
        __("Agent demandeur") => 'created_user.username',
        __("Date de création") => 'created',
        __("Date de modification") => 'modified',
    ]
);
$infos .= $this->Html->tag('/article');
$tabs->add(
    'tab-validate-delivery-request-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);

$tableUnits = $this->Table
    ->create('validate-delivery-request-archive-units', ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'delivery-requests',
            'action' => 'paginate-archive-units',
            $deliveryRequest->id,
            '?' => [
                'sort' => 'name',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'archival_agency_identifier' => [
                'label' => __("Identifiant"),
            ],
            'name' => [
                'label' => __("Nom"),
            ],
            'description' => [
                'label' => __("Description"),
            ],
            'dates' => [
                'label' => __("Dates extrêmes"),
            ],
            'original_total_count' => [
                'label' => __("Nombre de fichiers"),
            ],
            'original_total_size' => [
                'label' => __("Taille des fichiers"),
                'callback' => 'TableHelper.readableBytes',
            ],
        ]
    )
    ->data($deliveryRequest->get('archive_units') ?: [])
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "publicDescriptionArchive({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/description-public'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'href' => $this->Url->build('/archive-units/display-xml-public') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'data-renewable' => true,
                'display' => $this->Acl->check('/archive-units/display-xml-public'),
                'displayEval' => '!data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
        ]
    );
$tabs->add(
    'tab-validate-delivery-request-archive-units',
    $this->Fa->i('fa-file-code-o', __("Unités d'archives")),
    $this->AjaxPaginator->create('pagination-validation-delivery-request-archive-units')
        ->url($url)
        ->table($tableUnits)
        ->count($unitsCount)
        ->generateTable()
);

echo $tabs;
?>
<script>
    $('#validate-delivery-request-action').off('change').change(function () {
        $('#validate-delivery-request-prev-stage-id').closest('.form-group').toggle($(this).val() === 'stepback');
    }).trigger('change');
</script>
