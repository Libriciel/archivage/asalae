<?php
/**
 * @var Asalae\View\AppView $this
 */
?>
    <script>
        function sendMail(id) {
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build('/ValidationProcesses/resendMail')?>/' + id
            });
        }
    </script>
<?php
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'ValidationProcesses.delivery_request.last_state_update' => [
                'label' => __("Date d'envoi"),
                'type' => 'datetime',
                'order' => 'last_state_update',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'delivery_request_sent[0]' => [
                        'id' => 'filter-delivery_request_sent-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_delivery_request_sent[0]', '>='),
                        'append' => $this->Date->picker('#filter-delivery_request_sent-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'delivery_request_sent[1]' => [
                        'id' => 'filter-delivery_request_sent-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_delivery_request_sent[1]', '<='),
                        'append' => $this->Date->picker('#filter-delivery_request_sent-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'ValidationProcesses.delivery_request.identifier' => [
                'label' => __("Identifiant"),
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'ValidationProcesses.delivery_request.comment' => [
                'label' => __("Commentaire"),
            ],
            'ValidationProcesses.delivery_request.derogation' => [
                'label' => __("Dérogation"),
                'type' => 'boolean',
                'filter' => [
                    'derogation[0]' => [
                        'id' => 'filter-derogation-0',
                        'label' => __("Dérogation"),
                        'options' => [
                            '0' => __("Non"),
                            '1' => __("Oui"),
                        ],
                    ],
                ],
            ],
            'ValidationProcesses.delivery_request.archive_units' => [
                'label' => __("Unités d'archives"),
                'callback' => 'TableHelper.ul("name")',
                'escape' => false,
                'filter' => [
                    'archive_unit_identifier[0]' => [
                        'id' => 'filter-archive_unit_identifier-0',
                        'label' => __("Identifiant"),
                    ],
                    'archive_unit_name[0]' => [
                        'id' => 'filter-archive_unit_name-0',
                        'label' => __("Nom"),
                    ],
                ],
            ],
            'ValidationProcesses.delivery_request.created_user.username' => [
                'label' => __("Créée par"),
                'escape' => false,
                'order' => 'created_user.username',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created_user[0]' => [
                        'id' => 'filter-created_user-0',
                        'label' => __("Créée par"),
                        'options' => $created_users,
                    ],
                ],
            ],
            'ValidationProcesses.current_stage.name' => [
                'label' => __("Etape"),
                'order' => 'current_stage',
                'filter' => [
                    'validation_stage_id[0]' => [
                        'id' => 'filter-validation_stage_id-0',
                        'label' => false,
                        'aria-label' => __("Etape"),
                        'options' => $stages,
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'ValidationProcesses.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewDeliveryRequest({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/DeliveryRequests/view'),
                'params' => [
                    'ValidationProcesses.delivery_request.id',
                    'ValidationProcesses.delivery_request.identifier',
                ],
            ],
            [
                'href' => "/DeliveryRequests/download-pdf/{0}",
                'download' => '{2}',
                'target' => '_blank',
                'data-action' => __("Télécharger le PDF"),
                'label' => $this->Fa->i('fa-file-pdf'),
                'title' => $title = __("Télécharger le PDF de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/DeliveryRequests/download-pdf'),
                'params' => [
                    'ValidationProcesses.delivery_request.id',
                    'ValidationProcesses.delivery_request.identifier',
                    'ValidationProcesses.delivery_request.pdf_basename',
                ],
            ],
            [
                'onclick' => "processDeliveryRequest({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-cogs'),
                'title' => $title = __("Valider {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/validation-processes/process-delivery-request'),
                'params' => ['ValidationProcesses.id', 'ValidationProcesses.delivery_request.identifier'],
            ],
            [
                'onclick' => "sendMail({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-envelope'),
                'data-action' => __("Renvoyer mail"),
                'title' => $title = __("Renvoyer le mail pour la validation par courrier électronique"),
                'aria-label' => $title,
                'displayEval' => 'data[{index}].ValidationProcesses.delivery_request.state === "validating"
                && data[{index}].ValidationProcesses.has_mail_agent',
                'params' => ['ValidationProcesses.id'],
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des demandes de communication"),
        'table' => $table,
    ]
);
