<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Communications"));
$this->Breadcrumbs->add(__("Demandes de communication à valider"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag(
        'h1',
        $this->Fa->i(
            'fa-check-square-o',
            __("Demandes de communication à valider")
        )
    )
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

echo $this->ModalView
    ->create('validation-view-delivery-request', ['size' => 'large'])
    ->modal(__("Visualisation d'une demande de communication"))
    ->output('function', 'actionViewDeliveryRequest', '/delivery-requests/view')
    ->generate();

echo $this->ModalForm
    ->create('validate-delivery-request', ['size' => 'large'])
    ->modal(__("Validation d'une demande de communication"))
    ->javascriptCallback('afterValidateComm')
    ->output('function', 'processDeliveryRequest', '/validation-processes/process-delivery-request')
    ->generate();

echo $this->ModalView->create('public-description-delivery-request', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une unité d'archive"))
    ->output('function', 'publicDescriptionArchive', '/ArchiveUnits/description-public')
    ->generate();

$jsTable = $this->Table->getJsTableObject($tableId);
echo $this->element('modal', ['idTable' => $tableId]);

echo $this->Filter->create('validation-delivery-request-filter')
    ->saves($savedFilters)
    ->filter(
        'archive_unit_identifier',
        [
            'label' => __("Identifiant d'une unité d'archives"),
            'wildcard',
        ]
    )
    ->filter(
        'archive_unit_name',
        [
            'label' => __("Nom d'une unité d'archives"),
            'wildcard',
        ]
    )
    ->filter(
        'created_user',
        [
            'label' => __("Créée par"),
            'options' => $created_users,
        ]
    )
    ->filter(
        'delivery_request_created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'delivery_request_sent',
        [
            'label' => __("Date d'envoi"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'derogation',
        [
            'label' => __("Dérogation"),
            'options' => [
                '0' => __("Non"),
                '1' => __("Oui"),
            ],
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->filter(
        'identifier',
        [
            'label' => __("Identifiant"),
            'wildcard',
        ]
    )
    ->filter(
        'validation_stage_id',
        [
            'label' => __("Etape"),
            'options' => $stages,
        ]
    )
    ->generateSection();

require 'ajax_delivery_requests.php';
?>
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    function afterValidateComm(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
        }
    }
</script>
