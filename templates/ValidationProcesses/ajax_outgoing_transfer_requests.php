<?php
/**
 * @var Asalae\View\AppView $this
 */
?>
    <script>
        function sendMail(id) {
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build('/ValidationProcesses/resendMail')?>/' + id
            });
        }
    </script>
<?php
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'ValidationProcesses.outgoing_transfer_request.last_state_update' => [
                'label' => __("Date d'envoi"),
                'type' => 'datetime',
                'order' => 'last_state_update',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'outgoing_transfer_request_sent[0]' => [
                        'id' => 'filter-outgoing_transfer_request_sent-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator(
                            'dateoperator_outgoing_transfer_request_sent[0]',
                            '>='
                        ),
                        'append' => $this->Date->picker('#filter-outgoing_transfer_request_sent-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'outgoing_transfer_request_sent[1]' => [
                        'id' => 'filter-outgoing_transfer_request_sent-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator(
                            'dateoperator_outgoing_transfer_request_sent[1]',
                            '<='
                        ),
                        'append' => $this->Date->picker('#filter-outgoing_transfer_request_sent-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'ValidationProcesses.outgoing_transfer_request.identifier' => [
                'label' => __("Identifiant"),
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'ValidationProcesses.outgoing_transfer_request.comment' => [
                'label' => __("Commentaire"),
            ],
            'ValidationProcesses.outgoing_transfer_request.archive_units' => [
                'label' => __("Unités d'archives"),
                'callback' => 'TableHelper.ul("name")',
                'escape' => false,
                'filter' => [
                    'archive_unit_identifier[0]' => [
                        'id' => 'filter-archive_unit_identifier-0',
                        'label' => __("Identifiant"),
                    ],
                    'archive_unit_name[0]' => [
                        'id' => 'filter-archive_unit_name-0',
                        'label' => __("Nom"),
                    ],
                ],
            ],
            'ValidationProcesses.outgoing_transfer_request.created_user.username' => [
                'label' => __("Créée par"),
                'escape' => false,
                'order' => 'created_user.username',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created_user[0]' => [
                        'id' => 'filter-created_user-0',
                        'label' => __("Créée par"),
                        'options' => $created_users,
                    ],
                ],
            ],
            'ValidationProcesses.current_stage.name' => [
                'label' => __("Etape"),
                'order' => 'current_stage',
                'filter' => [
                    'validation_stage_id[0]' => [
                        'id' => 'filter-validation_stage_id-0',
                        'label' => false,
                        'aria-label' => __("Etape"),
                        'options' => $stages,
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'ValidationProcesses.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewOutgoingTransferRequest({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/OutgoingTransferRequests/view'),
                'params' => [
                    'ValidationProcesses.outgoing_transfer_request.id',
                    'ValidationProcesses.outgoing_transfer_request.identifier',
                ],
            ],
            [
                'onclick' => "processOutgoingTransferRequest({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-cogs'),
                'title' => $title = __("Valider {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/validation-processes/process-outgoing-transfer-request'),
                'params' => ['ValidationProcesses.id', 'ValidationProcesses.outgoing_transfer_request.identifier'],
            ],
            [
                'onclick' => "sendMail({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-envelope'),
                'data-action' => __("Renvoyer mail"),
                'title' => $title = __("Renvoyer le mail pour la validation par courrier électronique"),
                'aria-label' => $title,
                'displayEval' => 'data[{index}].ValidationProcesses.outgoing_transfer_request.state === "validating"
                && data[{index}].ValidationProcesses.has_mail_agent',
                'params' => ['ValidationProcesses.id'],
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des demandes de transferts sortants"),
        'table' => $table,
    ]
);
