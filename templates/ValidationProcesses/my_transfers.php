<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Transferts entrants"));
$this->Breadcrumbs->add(__("Transferts conformes à valider"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-check-square-o', __("Transferts conformes à valider")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$sectionId = 'section-validate-my-transfers';
$sectionTitle = __("Liste de mes transferts à valider");

echo $this->ModalForm->create('validation-processes-my-transfers', ['size' => 'large'])
    ->modal(__("Validation d'un transfert d'archives conforme"))
    ->javascriptCallback('afterValidate')
    ->output('function', 'processTransfer', '/validation-processes/process-my-transfer');

require 'transfers-common.php';
