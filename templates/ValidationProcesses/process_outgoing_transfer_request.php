<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $outgoingTransferRequest
 */

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;

end($prevs);
$lastPrev = key($prevs);
reset($prevs);
$form = $this->Form->create(
    $entity,
    ['idPrefix' => 'validate-outgoing-transfer-request']
)
    . $this->Form->control(
        'action',
        [
            'label' => __("Action"),
            'options' => $actions,
            'empty' => __("-- Sélectionner une action --"),
        ]
    )
    . ($freezedArchivesCount
        ? $this->Html->tag('div#alert-freezed-archives')
        . $this->Html->tag(
            'div.alert.alert-info',
            __n(
                "Une archive est gelée, empêchant la validation de la demande de transfert sortant.",
                "{0} archives sont gelées, empêchant la validation de la demande de transfert sortant.",
                $freezedArchivesCount,
                $freezedArchivesCount
            )
        )
        . $this->Html->tag('/div')
        : ''
    )
    . $this->Form->control('comment', ['label' => __("Commentaire")])
    . $this->Form->control(
        'prev_stage_id',
        [
            'label' => __("Retourner à l'étape"),
            'options' => $prevs,
            'default' => $lastPrev,
        ]
    );

if (!empty($hash)) {
    $form .= $this->Html->tag('div#sign-validate-outgoing-transfer-request')
        . $this->LiberSign->insertSignForm(
            '#sign-validate-outgoing-transfer-request',
            [$outgoingTransferRequest->id => $hash]
        )
        . $this->Html->tag('/div');
}
$form .= $this->Form->end();

$tabs = $this->Tabs->create('tabs-view-outgoing-transfer-request', ['class' => 'row no-padding']);
$tabs->add(
    'tab-validate-outgoing-transfer-request-valitation',
    $this->Fa->i('fa-check-square-o', __("Validation")),
    $form
);

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($outgoingTransferRequest->get('identifier')));
$infos .= $this->Html->tag('p', h($outgoingTransferRequest->get('comment')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Demande de transferts sortants"));
$infos .= $this->ViewTable->generate(
    $outgoingTransferRequest,
    [
        __("Identifiant") => 'identifier',
        __("Etat") => 'statetrad',
        __("Commentaire") => 'comment|nl2br',
        __("Système d'archivage électronique") => 'archiving_system.name',
        __("Agent demandeur") => 'created_user.username',
        __("Identifiant du service d'archives") => 'archival_agency_identifier',
        __("Nom du service d'archives") => 'archival_agency_name',
        __("Identifiant du service versant") => 'transferring_agency_identifier',
        __("Nom du service versant") => 'transferring_agency_name',
        __("Identifiant de l'accord de versement") => 'agreement_identifier',
        __("Identifiant du profil d'archives") => 'profile_identifier',
        __("Identifiant du niveau de service") => 'service_level_identifier',
        __("Nombre d'archives") => 'archives_count',
        __("Nombre d'unités d'archives") => 'archive_units_count',
        __("Fichiers d'unités d'archives") => 'original_count',
        __("Taille des unités d'archives") => '{original_size|toReadableSize} ({original_size} Octets)',
        __("Date de création") => 'created',
        __("Date de modification") => 'modified',
    ]
);
$infos .= $this->Html->tag('/article');
$tabs->add(
    'tab-validate-outgoing-transfer-request-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);

$tableUnits = $this->Table
    ->create('validate-outgoing-transfer-request-archive-units', ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'outgoing-transfer-requests',
            'action' => 'paginate-archive-units',
            $outgoingTransferRequest->id,
            '?' => [
                'sort' => 'name',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'archival_agency_identifier' => [
                'label' => __("Identifiant"),
            ],
            'name' => [
                'label' => __("Nom"),
            ],
            'description' => [
                'label' => __("Description"),
            ],
            'dates' => [
                'label' => __("Dates extrêmes"),
            ],
            'original_total_count' => [
                'label' => __("Nombre de fichiers"),
            ],
            'original_total_size' => [
                'label' => __("Taille des fichiers"),
                'callback' => 'TableHelper.readableBytes',
            ],
        ]
    )
    ->data($outgoingTransferRequest->get('archive_units') ?: [])
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewUnitDescription({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/description'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'href' => $this->Url->build('/archive-units/display-xml') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/display-xml'),
                'displayEval' => '!data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
        ]
    );
$tabs->add(
    'tab-validate-outgoing-transfer-request-archive-units',
    $this->Fa->i('fa-file-code-o', __("Unités d'archives")),
    $this->AjaxPaginator->create('pagination-validation-outgoing-transfer-request-archive-units')
        ->url($url)
        ->table($tableUnits)
        ->count($unitsCount)
        ->generateTable()
);

if (!empty($freezedArchives)) {
    $tableFreezed = $this->Table
        ->create('view-freezed-archives', ['class' => 'table table-striped table-hover'])
        ->url(
            $url = [
                'controller' => 'validation-processes',
                'action' => 'view-freezed-archives-outgoing-transfer',
                $process->get('id'),
                '?' => [
                    'sort' => 'id',
                    'direction' => 'asc',
                ],
            ]
        )
        ->fields(
            [
                'archival_agency_identifier' => [
                    'label' => __("Identifiant"),
                ],
                'name' => [
                    'label' => __("Nom"),
                ],
                'description' => [
                    'label' => __("Description"),
                ],
            ]
        )
        ->data($freezedArchives)
        ->params(
            [
                'identifier' => 'id',
            ]
        );
    $paginator = $this->AjaxPaginator->create('pagination-outgoing-transfer-request-view-freezed-archives')
        ->url($url)
        ->table($tableFreezed)
        ->count($freezedArchivesCount);

    $tabs->add(
        'tab-freezed-archives',
        $this->Fa->i('fa-snowflake', __("Archives gelées")),
        $paginator->generateTable()
    );
}

echo $tabs;
?>
<script>
    $('#validate-outgoing-transfer-request-action').off('change').change(function () {
        $('#validate-outgoing-transfer-request-prev-stage-id')
            .closest('.form-group')
            .toggle($(this).val() === 'stepback');
    }).trigger('change');
</script>
