<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Transferts sortants"));
$this->Breadcrumbs->add($h1 = __("Demandes de transferts sortants à valider"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag(
        'h1',
        $this->Fa->i('fa-check-square-o', $h1)
    )
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

echo $this->ModalView
    ->create('validation-view-outgoing-transfer-request', ['size' => 'modal-xxl'])
    ->modal(__("Visualisation d'une demande de transferts sortants"))
    ->output('function', 'actionViewOutgoingTransferRequest', '/outgoing-transfer-requests/view')
    ->generate();

echo $this->ModalForm
    ->create('validate-outgoing-transfer-request', ['size' => 'modal-xxl'])
    ->modal(__("Validation d'une demande de transferts sortants"))
    ->javascriptCallback('afterValidateElim')
    ->output('function', 'processOutgoingTransferRequest', '/validation-processes/process-outgoing-transfer-request')
    ->generate();

echo $this->ModalView->create('search-public-description-archive', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une unité d'archive"))
    ->output('function', 'viewUnitDescription', '/ArchiveUnits/description')
    ->generate();

$jsTable = $this->Table->getJsTableObject($tableId);
echo $this->element('modal', ['idTable' => $tableId]);

echo $this->Filter->create('validation-outgoing-transfer-request-filter')
    ->saves($savedFilters)
    ->filter(
        'archive_unit_identifier',
        [
            'label' => __("Identifiant d'une unité d'archives"),
            'wildcard',
        ]
    )
    ->filter(
        'archive_unit_name',
        [
            'label' => __("Nom d'une unité d'archives"),
            'wildcard',
        ]
    )
    ->filter(
        'created_user',
        [
            'label' => __("Créée par"),
            'options' => $created_users,
        ]
    )
    ->filter(
        'outgoing_transfer_request_created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'outgoing_transfer_request_sent',
        [
            'label' => __("Date d'envoi"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->filter(
        'identifier',
        [
            'label' => __("Identifiant"),
            'wildcard',
        ]
    )
    ->filter(
        'validation_stage_id',
        [
            'label' => __("Etape"),
            'options' => $stages,
        ]
    )
    ->generateSection();

require 'ajax_outgoing_transfer_requests.php';
?>
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    function afterValidateElim(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
        }
    }
</script>
