<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $process
 * @var Cake\Datasource\EntityInterface $entity
 * @var Cake\Datasource\EntityInterface $restitutionRequest
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

?>
<script>
    function validationActionToIcon(value) {
        switch (value) {
            case 'validate':
                return '<?=$this->Fa->i('fa-check text-success')?>';
            case 'invalidate':
                return '<?=$this->Fa->i('fa-times text-danger')?>';
            case 'stepback':
                return '<?=$this->Fa->i('fa-reply')?>';
            default:

                return value;
        }
    }
</script>
<?php
if (!empty($notAuthorized)) {
    echo $this->Html->tag('div.container');
    echo $this->Html->tag(
        'div.alert.alert-info',
        __(
            "La page demandée n'est pas/plus valide. La durée d'accès "
            . "peut être dépassée ou le traitement déjà effectué."
        )
    );
    echo $this->Html->tag('/div');
    return;
}

if ($done) {
    return;
}

if ($isOutdated) {
    echo $this->Html->tag('div.container');
    echo $this->Html->tag('div.alert.alert-info', __("Une décision à déjà été prise sur cette étape"));
    echo $this->Html->tag('/div');
} else {
    echo $this->Html->tag('section.container.bg-white');
    echo $this->Html->tag(
        'h2.h4',
        __("Visualisation d'une demande de restitution"),
        ['style' => 'margin: 0;']
    );

    $infos = $this->Html->tag('article');
    $infos .= $this->Html->tag('header.bottom-space');
    $infos .= $this->Html->tag('h3', h($restitutionRequest->get('identifier')));
    $infos .= $this->Html->tag('p', h($restitutionRequest->get('comment')));
    $infos .= $this->Html->tag('/header');

    $infos .= $this->Html->tag('h4', __("Demande de restitution"));
    $infos .= $this->ViewTable->generate(
        $restitutionRequest,
        [
            __("Identifiant") => 'identifier',
            __("Etat") => 'statetrad',
            __("Commentaire") => 'comment|nl2br',
            __("Demande de dérogation") => 'derogation',
            __("Entité demandeur") => 'created_user.org_entity.name',
            __("Agent demandeur") => 'created_user.username',
            __("Date de création") => 'created',
            __("Date de modification") => 'modified',
        ]
    );
    $infos .= $this->Html->tag('/article');

    $tabs = $this->Tabs->create('tabs-view-restitution-request');
    $tabs->add(
        'tab-view-restitution-request-infos',
        $this->Fa->i('fa-file-code-o', __("Informations principales")),
        $infos
    );

    $tableUnits = $this->Table
        ->create('view-restitution-request-archive-units', ['class' => 'table table-striped table-hover'])
        ->url(
            $url = [
                'controller' => 'restitution-requests',
                'action' => 'paginate-archive-units',
                $restitutionRequest->id,
                '?' => [
                    'sort' => 'name',
                    'direction' => 'asc',
                ],
            ]
        )
        ->fields(
            [
                'archival_agency_identifier' => [
                    'label' => __("Cote"),
                ],
                'name' => [
                    'label' => __("Nom"),
                ],
            ]
        )
        ->data($restitutionRequest->get('archive_units') ?: [])
        ->params(
            [
                'identifier' => 'id',
            ]
        );
    $tabs->add(
        'tab-view-restitution-request-archive-units',
        $this->Fa->i('fa-file-code-o', __("Unités d'archives")),
        $this->AjaxPaginator->create('pagination-validation-mail-restitution-request-archive-units')
            ->url($url)
            ->table($tableUnits)
            ->count($unitsCount)
            ->generateTable()
    );

    /** @var EntityInterface $process */
    $process = Hash::get($restitutionRequest, 'validation_processes.0');
    if ($process) {
        $histories = $this->Html->tag('h4', __("Historique de validation"));
        /** @var EntityInterface $history */
        foreach (Hash::get($process, 'validation_histories') as $history) {
            $history->setVirtual(['actor_info']);
        }
        $histories .= $this->Table
            ->create('validation-histories', ['class' => 'table table-striped table-hover smart-td-size'])
            ->fields(
                [
                    'action' => [
                        'label' => __("Décision"),
                        'callback' => 'validationActionToIcon',
                        'titleEval' => 'actiontrad',
                    ],
                    'validation_actor.validation_stage.name' => [
                        'label' => __("Etape de validation"),
                    ],
                    'actor_info' => [
                        'label' => __("Auteur de la validation"),
                        'escape' => false,
                    ],
                    'created' => [
                        'label' => __("Date"),
                        'type' => 'datetime',
                    ],
                    'comment' => [
                        'label' => __("Commentaire"),
                    ],
                ]
            )
            ->data(Hash::get($process, 'validation_histories'))
            ->params(
                [
                    'identifier' => 'id',
                ]
            );

        $info = [
            __("Etat de la validation") => 'processedtrad',
            __("Décision finale") => 'validatedtrad',
            __("Circuit de validation") => 'validation_chain.name',
        ];
        if (!$process->get('processed')) {
            $info[__("Etape de validation")] = 'steptrad';
            $info[__("Doit être validé par")] = 'current_stage.validation_actors.{n}.actor_info|raw';
        }

        $tabs->add(
            'tab-restitution-request-validation',
            $this->Fa->i('fa-check-square-o', __("Validation")),
            // ---------------------------------------------------------------------
            $this->Html->tag(
                'h3',
                __("Validation de {0}", h($restitutionRequest->get('identifier')))
            )
            . $this->ViewTable->generate($process, $info)
            . $histories
        );
    }

    echo $tabs;
    echo $this->Html->tag('/section');

    end($prevs);
    $lastPrev = key($prevs);
    reset($prevs);
    echo $this->Html->tag('section.container.bg-white', null, ['style' => 'max-width: 600px;']);
    echo $this->Html->tag(
        'h2.h4',
        __("Veuillez prendre une décision"),
        ['style' => 'margin: 0;']
    );
    $form = $this->Form->create($entity, ['idPrefix' => 'validate-restitution-request'])
        . $this->Form->control(
            'action',
            [
                'label' => __("Action"),
                'options' => $actions,
                'empty' => __("-- Sélectionner une action --"),
            ]
        )
        . $this->Form->control('comment', ['label' => __("Commentaire")])
        . $this->Form->control(
            'prev_stage_id',
            [
                'label' => __("Retourner à l'étape"),
                'options' => $prevs,
                'default' => $lastPrev,
            ]
        );

    if (!empty($hash)) {
        $form .= $this->Html->tag('div#sign-validate-restitution-request')
            . $this->LiberSign->insertSignForm(
                '#sign-validate-restitution-request',
                [$restitutionRequest->id => $hash]
            )
            . $this->Html->tag('/div');
    }

    $form .= $this->Html->tag(
        'div.modal-footer',
        $this->Form->button(
            $this->Fa->charte('Enregistrer', __("Enregistrer")),
            ['class' => 'btn-primary']
        )
    );
    $form .= $this->Form->end();

    echo $form;
}
?>
<script>
    $('#validate-restitution-request-action').off('change').change(function () {
        $('#validate-restitution-request-prev-stage-id').closest('.form-group').toggle($(this).val() === 'stepback');
    }).trigger('change');
</script>
