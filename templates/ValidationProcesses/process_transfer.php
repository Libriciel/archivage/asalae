<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $process
 * @var Cake\Datasource\EntityInterface $entity
 * @var Cake\Datasource\EntityInterface $transfer
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

if (!empty($notAuthorized)) {
    echo $this->Html->tag('div.container');
    echo $this->Html->tag(
        'div.alert.alert-info',
        __(
            "La page demandée n'est pas/plus valide. La durée d'accès "
            . "peut être dépassée ou le traitement déjà effectué."
        )
    );
    echo $this->Html->tag('/div');
    return;
}

echo $this->Html->tag('div.container.text-center');
echo $this->Html->tag(
    'h1',
    $this->Fa->i(
        'fa-cogs',
        __(
            "Prendre une décision pour le transfert {0}",
            Hash::get($process, 'transfer.transfer_identifier')
        )
    ),
    ['style' => 'display: inline-block']
);
echo $this->Html->tag('/div');

if ($done) {
    return;
}

if ($isOutdated) {
    echo $this->Html->tag('div.container');
    echo $this->Html->tag(
        'div.alert.alert-info',
        __("Une décision à déjà été prise sur cette étape")
    );
    echo $this->Html->tag('/div');
} else {
    echo $this->Html->tag('section.container.bg-white');
    echo $this->Html->tag(
        'h2.h4',
        __("Visualisation d'un transfert"),
        ['style' => 'margin: 0;']
    );
    $tabs = $this->Tabs->create('tabs-view-transfer');

    $trs = [
        __("Identifiant") => 'transfer_identifier',
        __("Date de création") => 'created',
        __("Service d'archives") => 'archival_agency.name',
        __("Service versant") => 'transferring_agency.name',
        __("Niveau de service") => 'service_level.name',
        __("Version du message") => 'message_versiontrad',
        __("Etat") => 'statetrad',
        __("Date du dernier changement d'état") => 'last_state_update',
        __("Est conforme") => 'is_conform',
        __("Est modifié") => 'is_modified',
        __("Est accepté") => 'is_accepted',
        __("Accord de versement") => 'agreement.name',
        __("Profils d'archives") => 'profile.name',
        __("Créé par") => 'created_user.name',
    ];

    $tableMessage = $this->Html->tag('table.table.table-striped.table-hover.fixed-left-th');
    $tableMessage .= $this->Html->tag('tbody');
    foreach ($trs as $th => $td) {
        $value = Hash::get($transfer, $td);
        if (is_bool($value)) {
            $value = $value ? __("Oui") : __("Non");
        } elseif ($value) {
            $value = h($value);
        }

        $tableMessage .= $this->Html->tag(
            'tr',
            $this->Html->tag('th', $th)
            . $this->Html->tag('td', $value)
        );
    }
    $tableMessage .= $this->Html->tag('/tbody');
    $tableMessage .= $this->Html->tag('/table');

    /**
     * Details
     */
    $tabs->add(
        'tab-data',
        $this->Fa->i('fa-file-code-o', __("Détails")),
        // ---------------------------------------------------------------------
        $this->Html->tag('article')
        . $this->Html->tag('header.bottom-space')
        . $this->Html->tag('h3', h($transfer->get('transfer_identifier')))
        . $this->Html->tag('p', h($transfer->get('transfer_comment')))
        . $this->Html->tag('/header')
        . $tableMessage
        . $this->Html->tag('/article')
        . $this->Html->tag(
            'section.btn-separator',
            $this->Html->link(
                $this->Fa->i('fa-file-code-o', __("Afficher le bordereau")),
                '/transfers/display-xml/' . $transfer->id,
                ['escape' => false, 'target' => '_blank', 'class' => 'btn btn-info']
            )
            . '<br>'
        )
    );

    $process = Hash::get($transfer, 'validation_processes.0');
    if ($process) {
        $etape = Hash::get($process, 'current_stage.name', __("Terminée"));

        $histories = $this->Html->tag('h4', __("Historique de validation"));
        /** @var EntityInterface $history */
        foreach (Hash::get($process, 'validation_histories') as $history) {
            $history->setVirtual(['actor_info']);
            $histories .= '<hr>' . $this->ViewTable->generate(
                $history,
                [
                    __("Auteur de la validation") => 'actor_info',
                    __("Date") => 'created',
                    __("Etape de validation") => 'validation_actor.validation_stage.name',
                    __("Décision") => 'actiontrad',
                    __("Commentaire") => 'comment',
                ]
            );
        }

        $tabs->add(
            'tab-transfer-validation',
            $this->Fa->i('fa-check-square-o', __("Validation")),
            // ---------------------------------------------------------------------
            $this->Html->tag(
                'h3',
                __("Validation de {0}", h($transfer->get('transfer_identifier')))
            )
            . $this->Html->tag(
                'p',
                $process->get('processed')
                    ? __(
                        "Le transfert est déclaré ''{0}'' à l'issue de la validation.",
                        $transfer->get('transfer_identifier')
                    )
                    : __("La validation est en cours ({0})", $etape)
            )
            . $this->ViewTable->generate(
                $process,
                [
                    __("Etat de la validation") => 'processedtrad',
                    __("Décision finale") => 'validatedtrad',
                    __("Prochaine étape de validation") => 'steptrad',
                    __("Acteurs de validation de la prochaine étape")
                    => 'current_stage.validation_actors.{n}.actor_info',
                ]
            )
            . $histories
        );
    }

    /**
     * Fichiers des pièces jointes
     */
    $requestParams = $this->getRequest()->getAttribute('params');
    $tableAttachments = $this->Table
        ->create('view-transfer-attachments', ['class' => 'table table-striped table-hover'])
        ->url(
            $url = [
                'controller' => $requestParams['controller'],
                'action' => $requestParams['action'],
            ] + $requestParams['pass']
                + [
                    '?' => [
                        'attachments' => 'true',
                        'sort' => 'filename',
                        'direction' => 'asc',
                    ],
                ]
        )
        ->fields(
            [
                'filename' => [
                    'label' => __("Nom de fichier"),
                    'order' => 'filename',
                    'filter' => [
                        'filename[0]' => [
                            'id' => 'filter-transfer-attachment-filename-0',
                            'label' => false,
                            'aria-label' => __("Nom de fichier"),
                        ],
                    ],
                ],
                'size' => [
                    'label' => __("Taille"),
                    'callback' => 'TableHelper.readableBytes',
                    'order' => 'size',
                    'filter' => [
                        'size[0][value]' => [
                            'id' => 'filter-size-0',
                            'label' => false,
                            'aria-label' => __("Taille 1"),
                            'prepend' => $this->Input->operator('size[0][operator]', '>='),
                            'append' => $this->Input->mult('size[0][mult]'),
                            'class' => 'with-select',
                            'type' => 'number',
                            'min' => 1,
                        ],
                        'size[1][value]' => [
                            'id' => 'filter-size-1',
                            'label' => false,
                            'aria-label' => __("Taille 2"),
                            'prepend' => $this->Input->operator('size[1][operator]', '<='),
                            'append' => $this->Input->mult('size[1][mult]'),
                            'class' => 'with-select',
                            'type' => 'number',
                            'min' => 1,
                        ],
                    ],
                ],
                'meta' => [
                    'label' => __("Meta-donnés"),
                    'target' => '',
                    'thead' => [
                        'hash' => ['label' => __("Empreinte du fichier")],
                        'hash_algo' => ['label' => __("Algorithme de hashage")],
                        'mime' => ['label' => __("Type MIME")],
                        'format' => ['label' => __("Format")],
                        'virus_name' => ['label' => __("VIRUS")],
                    ],
                ],
            ]
        )
        ->data(Hash::get($transfer, 'transfer_attachments', []))
        ->params(
            [
                'identifier' => 'id',
            ]
        );
    $paginator = $this->AjaxPaginator->create('pagination-transfer-view-attachments')
        ->url($url)
        ->table($tableAttachments)
        ->count($countUploads);

    $tabs->add(
        'tab-transfer-attachments',
        $this->Fa->i('fa-paperclip', __("Fichiers des pièces jointes")),
        $paginator->generateTable()
    );

    /**
     * Erreurs
     */

    $errors = Hash::get($transfer, 'transfer_errors');
    if (!empty($errors)) {
        $tableErrors = $tableAttachments = $this->Table
            ->create('view-transfer-errors', ['class' => 'table table-striped table-hover'])
            ->fields(
                [
                    'level' => ['label' => __("Niveau")],
                    'code' => ['label' => __("Code")],
                    'message' => ['label' => __("Message")],
                ]
            )
            ->data($errors)
            ->params(
                [
                    'identifier' => 'id',
                ]
            );
        $paginator = $this->AjaxPaginator->create('pagination-transfer-view-errors')
            ->url(
                [
                    'controller' => 'transfers',
                    'action' => 'view-errors',
                ]
            )
            ->table($tableErrors)
            ->count($countErrors);
        $tabs->add(
            'tab-transfer-errors',
            $this->Fa->i('fa-warning', __("Erreurs")),
            $paginator->generateTable()
        );
    }

    echo $tabs;
    echo $this->Html->tag('/section');

    /**
     * formulaire de prise de décision
     */
    end($prevs);
    $lastPrev = key($prevs);
    reset($prevs);
    echo $this->Html->tag('section.container.bg-white', null, ['style' => 'max-width: 600px;']);
    echo $this->Html->tag(
        'h2.h4',
        __("Veuillez prendre une décision"),
        ['style' => 'margin: 0;']
    );
    echo $this->Form->create($entity);

    echo $this->Form->control(
        'action',
        [
            'label' => __("Action"),
            'options' => $actions,
            'empty' => __("-- Sélectionner une action --"),
        ]
    );
    echo $this->Form->control(
        'comment',
        [
            'label' => __("Commentaire"),
        ]
    );
    echo $this->Form->control(
        'prev_stage_id',
        [
            'label' => __("Retourner à l'étape"),
            'options' => $prevs,
            'default' => $lastPrev,
        ]
    );

    echo $this->Html->tag(
        'div.modal-footer',
        $this->Form->button(
            $this->Fa->charte('Enregistrer', __("Enregistrer")),
            ['class' => 'btn-primary']
        )
    );

    echo $this->Form->end();
    echo $this->Html->tag('/section');
}
?>
<script>
    $('#action').off('change').change(function () {
        $('#prev-stage-id').closest('.form-group').toggle($(this).val() === 'stepback');
    }).trigger('change');
</script>
