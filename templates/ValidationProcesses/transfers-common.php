<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;
use Cake\Utility\Hash;

$view = $this;
$loading = $this->Html->tag(
    'div',
    $this->Fa->i('fa-4x fa-spinner faa-spin animated'),
    ['class' => 'text-center loading-container']
);

$jsTable = $this->Table->getJsTableObject($tableId);

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalView->create('view-transfer-preparating', ['size' => 'large'])
    ->modal(__("Visualisation d'un transfert"))
    ->output('function', 'viewTransfer', '/Transfers/view')
    ->generate();

echo $this->ModalView->create('validation-explore-transfer-common', ['size' => 'modal-xxl'])
    ->modal(__("Afficher le bordereau"))
    ->output('function', 'exploreTransfer', '/Transfers/explore')
    ->generate();

echo $this->ModalView->create('validation-view-transfer-common', ['size' => 'large'])
    ->modal(__("Visualisation d'un transfert"))
    ->output('function', 'viewTransfer', '/Transfers/view')
    ->generate();

echo $this->ModalView->create('validation-analyse-transfer-common', ['size' => 'large'])
    ->modal(
        __("Analyse d'un transfert"),
        $this->Html->tag(
            'div.alert.alert-info',
            __("Analyse en cours... Veuillez patienter, cette action peut durer plusieurs minutes.")
        )
        . $loading
        . $this->Html->tag('p#validation-feedback-analyse', '')
    )
    ->output('function', 'analyseTransfer', '/Transfers/analyse')
    ->javascriptCallback('unsubscribeAnalyse')
    ->generate();

echo $this->ModalForm
    ->create('edit-transfer', ['size' => 'modal-xxl'])
    ->modal(__("Edition d'un transfert"))
    ->output('function', 'actionEditTransfer', '/Transfers/edit')
    ->generate();

if (isset($stages) || $this->getRequest()->getQuery('validation_stage_id.0')) {
    echo $this->Html->tag('section.container.smaller.bg-white');
    echo $this->Html->tag('h2.h4', __("Valider en lot"));

    if ($checkbox = (bool)$this->getRequest()->getQuery('validation_stage_id')) {
        end($prevs);
        $lastPrev = key($prevs);
        reset($prevs);
        echo $this->Form->create(null, ['id' => 'form-process-multiple', 'idPrefix' => 'validate-processes']);
        echo $this->Html->tag('h3.h4', h($stageName));
        echo $this->Form->control(
            'ids',
            [
                'type' => 'hidden',
            ]
        );
        echo $this->Form->control(
            'action',
            [
                'label' => __("Action"),
                'options' => $actions,
                'empty' => __("-- Sélectionner une action --"),
                'required' => true,
                'onchange' => '$("#hidden-action").val($(this).val())',
            ]
        );
        echo $this->Form->control(
            'action',
            [
                'type' => 'hidden',
                'id' => 'hidden-action',
            ]
        );
        echo $this->Form->control(
            'comment',
            [
                'label' => __("Commentaire"),
                'type' => 'textarea',
            ]
        );
        echo $this->Form->control(
            'prev_stage_id',
            [
                'label' => __("Retourner à l'étape"),
                'options' => $prevs,
                'default' => $lastPrev,
            ]
        );
        $typeValidation = Hash::get($actor ?? [], 'type_validation');
        if (
            $typeValidation === 'S'
            || (!$typeValidation
            && $this->getRequest()->getSession()->read('Auth.use_cert'))
        ) {
            echo $this->Html->tag('div#sign-validate-transfer-multi')
                . $this->LiberSign->insertSignForm('#sign-validate-transfer-multi')
                . $this->Html->tag('/div')

                . $this->Html->tag('div')
                . $this->Html->tag(
                    'button#btn-sign-transfer-multi.btn.btn-primary',
                    '<input type="checkbox" aria-hidden="true" '
                    . 'name="signature" onclick="return false" required="required">'
                    . ' ' . __("Signer"),
                    ['type' => 'button', 'onclick' => 'signByCertificate()', 'style' => 'display: none']
                )
                . $this->Html->tag('div.hide.hidden-certificates', '')
                . $this->Html->tag('/div');
        }
        echo $this->Html->tag(
            'div',
            $this->Html->link(
                $this->Fa->i('fa-arrow-left', __("Retour")),
                $this->getRequest()->getPath(),
                [
                    'class' => 'btn btn-default',
                    'escape' => false,
                ]
            )
            . $this->Form->button(
                $this->Fa->charte('Enregistrer', __("Enregistrer")),
                ['bootstrap-type' => 'primary']
            ),
            ['class' => 'text-right']
        );
    } else {
        echo $this->Form->create(null, ['type' => 'get']);
        echo $this->Form->control(
            'validation_stage_id.0',
            [
                'label' => __("Etape de validation"),
                'options' => $stages,
                'empty' => __("-- Sélectionner une étape de validation --"),
                'onchange' => 'submitForm(this)',
            ]
        );
        foreach (Hash::flatten($this->getRequest()->getQueryParams()) as $key => $value) {
            $name = preg_replace_callback(
                '#\.([^.]+)#',
                fn($m) => '[' . $m[1] . ']',
                $key
            );
            echo '<input type="hidden" name="' . $name . '" value="' . $value . '">';
        }
    }

    echo $this->Form->end();
    echo $this->Html->tag('/section');
}

echo $this->Filter->create('transfer-filter')
    ->saves($savedFilters)
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => 'jj/mm/aaaa',
            'type' => 'date',
        ]
    )
    ->filter(
        'transfer_date',
        [
            'label' => __("Date du transfert"),
            'placeholder' => 'jj/mm/aaaa',
            'type' => 'date',
        ]
    )
    ->filter(
        'transfer_identifier',
        [
            'label' => __("Identifiant"),
            'placeholder' => 'ident?fi*',
        ]
    )
    ->filter(
        'transferring_agency_id',
        [
            'label' => __("Service versant"),
            'placeholder' => 'ver?an*',
            'options' => $transferring_agencies,
            'empty' => __("-- Sélectionner un service versant --"),
        ]
    )
    ->filter(
        'originating_agency_id',
        [
            'label' => __("Service producteur"),
            'options' => $originating_agencies,
            'empty' => __("-- Sélectionner un service producteur --"),
        ]
    )
    ->filter(
        'validation_chain_id',
        [
            'label' => __("Circuit de validation"),
            'multiple' => true,
            'options' => $chains,
            'empty' => __("-- Sélectionner un circuit de validation --"),
        ]
    )
    ->filter(
        'profile_id',
        [
            'label' => __("Profil d'archives"),
            'options' => $profiles ?? [],
            'empty' => __("-- Sélectionner un profil d'archives --"),
        ]
    )
    ->filter(
        'agreement_id',
        [
            'label' => __("Accord de versement"),
            'options' => $agreements ?? [],
            'empty' => __("-- Sélectionner un accord de versement --"),
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->generateSection();

require 'ajax_transfers-common.php';
?>
<!--suppress JSJQueryEfficiency - bug si on tente d'appliquer cette règle -->
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$sectionId?>');
    });

    /**
     * Callback de l'action valider
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterValidate(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            AsalaeLoading.start();
            $('#validation-processes-my-transfers').one('hidden.bs.modal', function () {
                location.reload();
            });
        }
    }

    /**
     * Charge la modal de visualisation d'un SEDA
     * @param id
     */
    function loadDisplayXmlModal(id) {
        var body = $('#<?=$modalXmlToHtml?>').find('.modal-body');
        body.html($('<iframe></iframe>')
            .attr('height', '600')
            .attr('width', '100%')
            .attr('src', '<?=$this->Url->build('/Transfers/display-xml/')?>' + id));
    }

    <?php /* TODO factoriser avec Transfers/index-common.php */ ?>
    function unsubscribeAnalyse() {
        $('#validation-analyse-transfer-common').off('.killable');
        if (typeof prevsession === 'object') {
            prevsession.unsubscribe('analyse_' + previd + '_' + PHP.user_id + '_' + PHP.token);
        }
        previd = null;
    }

    if (typeof prevsession === 'object' && typeof previd === 'string') {
        unsubscribeAnalyse();
    } else {
        var previd;
        var prevsession;
    }

    function wrapAnalyseTransfer(id) {
        AsalaeWebsocket.connect(
            '<?=Configure::read('Ratchet.connect')?>',
            function (session) {
                session.subscribe('analyse_' + id + '_' + PHP.user_id + '_' + PHP.token, function (topic, data) {
                    $('#validation-feedback-analyse').text(data.message);
                });
                previd = '' + id;
                prevsession = session;
            },
            function () {
            },
            {'skipSubprotocolCheck': true}
        );
        $('#validation-analyse-transfer-common').one('hide.bs.modal.killable', function () {
            unsubscribeAnalyse();
            AsalaeLoading.ajax({url: '<?=$this->Url->build('/Transfers/abortAnalyse')?>'});
        });
        return analyseTransfer(id + '?refresh=' + <?= $jsTable ?>.table.attr('id'));
    }

    function wrapEditTransfer(id) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/transfers/get-edit-meta')?>/' + id,
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                if (!content || !content.isDirty) {
                    actionEditTransfer(id);
                    return;
                }
                if (content.session === PHP.token) {
                    actionEditTransfer(id);
                    alert(
                        __(
                            "Attention, vous reprenez l'édition de ce transfert qui avait commencé le {0}",
                            moment(content.created).format('LLLL')
                        )
                    )
                } else if (confirm(
                    __(
                        "L'édition de ce transfert ne s'est pas correctement terminée. "
                        + "Voulez-vous reprendre l'édition? Attention si vous cliquez sur Annuler,"
                        + " les éventuelles modifications seront perdues."
                    )
                )
                ) {
                    actionEditTransfer(id);
                } else {
                    actionEditTransfer(id + '?renew=true');
                }
            }
        });
    }

    function submitForm(element) {
        if (!$(element).val()) {
            return;
        }
        $(element).closest('form').submit();
    }

    var tableId = '#' + <?=$jsTable?>.table.attr('id');
    $('#validate-processes-action').off('change').change(function () {
        $('#validate-processes-prev-stage-id').closest('.form-group').toggle($(this).val() === 'stepback');
        var checked = [];
        var tableElement = $(tableId);
        tableElement.find('input:checked').each(function () {
            checked.push($(this).attr('id'));
        });
        TableGenerator.instance[tableElement.attr('data-table-uid')].generateAll();
        $(checked).each(function () {
            $('#' + this).prop('checked', true);
        });
    }).trigger('change');

    $('#form-process-multiple').submit(function (e) {
        e.preventDefault();
        var val = [];
        $(tableId).find('tbody input:checked').each(function () {
            val.push($(this).val());
        });
        if (val.length === 0) {
            alert(__("Sélectionnez au moins un transfert"));
            return;
        }
        $('#validate-processes-ids').val(val.join(','));
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/validation-processes/process-multi-transfer')?>',
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            data: $(this).closest('form').serialize(),
            success: function (content) {
                if (content === 'done') {
                    location.reload();
                } else {
                    this.error(content);
                }
            },
            complete: function () {
                $('#zone-test-import').find('.loading-container').remove();
            }
        });
    });

    function signByCertificate() {
        var form = $('#form-process-multiple');
        if (!$('#validate-processes-action').val()) {
            form.get(0).reportValidity();
            return;
        }
        var table = <?=$jsTable?>;
        var hashes = {};
        var btn = $('#btn-sign-transfer-multi');
        var selected_certificate = $('#sign-validate-transfer-multi').find('[name=selected_certificate]');
        if (selected_certificate.filter(':checked').length === 0) {
            if (selected_certificate.length) {
                form.get(0).reportValidity();
            } else {
                alert(__("Veuillez sélectionner un certificat"));
            }
            return;
        }
        var c = 0;
        $('#<?=$sectionId?>').find('tbody input:checked').each(function () {
            hashes[$(this).val()] = table.getDataId($(this).val()).ValidationProcesses.transfer.hash;
            c++;
        });
        if (c === 0) {
            alert(__("Sélectionnez au moins un transfert"));
            return;
        }
        btn.disable();
        selected_certificate.one('change', function () {
            btn.enable();
        });
        btn.one('signature_failed', function () {
            $(this).removeClass('btn-primary').addClass('btn-danger');
        });
        btn.on('new_signature', function () {
            c--;
            if (c === 0) {
                $(this).removeClass('btn-primary')
                    .removeClass('btn-danger')
                    .addClass('btn-success')
                    .find('input').prop('checked', true);
                $(table.table).find('input, button, a').disable();
                $('#validate-processes-action').disable();
            }
        });
        libersign['#sign-validate-transfer-multi'].sign(
            hashes,
            selected_certificate.filter(':checked').val(),
            btn
        );
    }

    if (typeof libersign === 'object' && libersign['#sign-validate-transfer-multi']) {
        $(libersign['#sign-validate-transfer-multi']).one('AsalaeLiberSign.loaded', function () {
            $('#btn-sign-transfer-multi').fadeIn();
        });
    }
</script>
