<?php
/**
 * @var Asalae\View\AppView $this
 */
?>
    <script>
        function sendMail(id) {
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build('/ValidationProcesses/resendMail')?>/' + id
            });
        }
    </script>
<?php
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'ValidationProcesses.transfer.transfer_date' => [
                'label' => __("Date du transfert"),
                'type' => 'datetime',
                'order' => 'transfer_date',
            ],
            'ValidationProcesses.transfer.transfer_identifier' => [
                'label' => __("Identifiant du transfert"),
                'callback' => 'TableHelper.wordBreak()',
                'order' => 'transfer_identifier',
                'filter' => [
                    'transfer_identifier[0]' => [
                        'id' => 'filter-transfer-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'ValidationProcesses.transfer.transferring_agency.name' => [
                'label' => __("Service versant"),
                'order' => 'transferring_agency',
                'filter' => [
                    'transferring_agency_id[0]' => [
                        'id' => 'filter-transferring_agency_id-0',
                        'label' => false,
                        'aria-label' => __("Service versant"),
                        'options' => $transferring_agencies,
                    ],
                ],
            ],
            'Transfers.originating_agency.name' => [
                'label' => __("Service producteur"),
                'filter' => [
                    'originating_agency_id[0]' => [
                        'id' => 'filter-originating_agency_id-0',
                        'label' => false,
                        'aria-label' => __("Service producteur"),
                        'options' => $originating_agencies,
                    ],
                ],
                'display' => false,
            ],
            'ValidationProcesses.transfer.transfer_comment' => [
                'label' => __("commentaire"),
            ],
            'ValidationProcesses.validation_chain.name' => [
                'label' => __("Circuit de validation"),
                'filter' => [
                    'validation_chain_id[0]' => [
                        'id' => 'filter-validation_chain_id-0',
                        'label' => false,
                        'aria-label' => __("Circuit de validation"),
                        'options' => $chains,
                    ],
                ],
            ],
            'ValidationProcesses.current_stage.name' => [
                'label' => __("Etape"),
                'order' => 'current_stage',
                'filter' => [
                    'validation_stage_id[0]' => [
                        'id' => 'filter-validation_stage_id-0',
                        'label' => false,
                        'aria-label' => __("Etape"),
                        'options' => $stages,
                    ],
                ],
            ],
            'ValidationProcesses.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'created',
            ],
            'ValidationProcesses.modified' => [
                'label' => __("Date de modification"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'modified',
            ],
            'ValidationProcesses.transfer.data_size' => [
                'label' => __("Taille PJs"),
                'display' => false,
                'callback' => 'TableHelper.readableBytes',
                'order' => 'transfer_data_size',
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'ValidationProcesses.id',
            'favorites' => true,
            'sortable' => true,
            'checkbox' => (
                $checkbox ?? array_key_exists('validation_stage_id', $this->getRequest()->getParam('?', [])))
                ? "$('#validate-processes-action').val() !== 'validate'
                        || data[{index}].ValidationProcesses.next
                        || !data[{index}].ValidationProcesses.error"
                : false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Voir {0}", '{1}'),
                'aria-label' => __("Voir {0}", '{1}'),
                'display' => $this->Acl->check('/transfers/view'),
                'params' => ['ValidationProcesses.transfer.id', 'ValidationProcesses.transfer.transfer_identifier'],
            ],
            [
                'onclick' => "processTransfer({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-cogs'),
                'title' => __("Traiter {0}", '{1}'),
                'aria-label' => __("Traiter {0}", '{1}'),
                'display' => $this->Acl->check('/validation-processes/process-my-transfer'),
                'displayEval' => 'data[{index}].processable && data[{index}].transfer.is_conform !== null',
                'params' => ['ValidationProcesses.id', 'ValidationProcesses.transfer.transfer_identifier'],
            ],
            [
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-cogs'),
                'title' => __("Veuillez analyser {0} avant de pouvoir le valider", '{1}'),
                'aria-label' => __("Veuillez analyser {0} avant de pouvoir le valider", '{1}'),
                'display' => $this->Acl->check('/validation-processes/process-my-transfer'),
                'displayEval' => 'data[{index}].processable && data[{index}].transfer.is_conform === null',
                'params' => ['ValidationProcesses.id', 'ValidationProcesses.transfer.transfer_identifier'],
                'disabled' => 'true', // doit impérativement être après displayEval pour marcher
            ],
            [
                'href' => "/Transfers/download/{0}",
                'download' => '{1}',
                'target' => '_blank',
                'data-action' => __("Télécharger"),
                'label' => $this->Fa->charte('Télécharger'),
                'title' => $title = __("Télécharger {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/transfers/download'),
                'params' => ['ValidationProcesses.transfer.id', 'ValidationProcesses.transfer.filename'],
            ],
            [
                'onclick' => "wrapAnalyseTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-stethoscope'),
                'data-action' => __("Analyser"),
                'title' => __("Analyser {0}", '{1}'),
                'aria-label' => __("Analyser {0}", '{1}'),
                'display' => $this->Acl->check('/transfers/analyse'),
                'displayEval' => 'data[{index}].ValidationProcesses.transfer.state === "validating"',
                'params' => ['ValidationProcesses.transfer.id', 'ValidationProcesses.transfer.transfer_identifier'],
            ],
            [
                'href' => $this->Url->build('/transfers/display-xml') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'data-action' => __("Afficher"),
                'title' => __("Afficher le bordereau {0}", '{1}'),
                'aria-label' => __("Voir {0}", '{1}'),
                'display' => $this->Acl->check('/transfers/display-xml'),
                'displayEval' => '!data[{index}].ValidationProcesses.transfer.is_large',
                'params' => ['ValidationProcesses.transfer.id', 'ValidationProcesses.transfer.transfer_identifier'],
            ],
            [
                'onclick' => "exploreTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'data-action' => __("Afficher"),
                'data-toggle' => 'modal',
                'data-target' => '#' . $modalXmlToHtml,
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher le bordereau {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/transfers/explore'),
                'displayEval' => 'data[{index}].ValidationProcesses.transfer.is_large',
                'params' => ['ValidationProcesses.transfer.id', 'ValidationProcesses.transfer.transfer_identifier'],
            ],
            [
                'onclick' => "wrapEditTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'display' => $this->Acl->check('/transfers/edit'),
                'displayEval' => 'data[{index}].processable && '
                    . 'data[{index}].ValidationProcesses.transfer.state === "validating"',
                'params' => ['ValidationProcesses.transfer.id', 'ValidationProcesses.transfer.transfer_identifier'],
            ],
            [
                'onclick' => "sendMail({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-envelope'),
                'data-action' => __("Renvoyer mail"),
                'title' => $title = __("Renvoyer le mail pour la validation par courrier électronique"),
                'aria-label' => $title,
                'displayEval' => 'data[{index}].ValidationProcesses.transfer.state === "validating"
                && data[{index}].ValidationProcesses.has_mail_agent',
                'params' => ['ValidationProcesses.id'],
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => $sectionId,
        'title' => $sectionTitle,
        'table' => $table,
    ]
);
