<?php

$filterId = 'validation-processes-transfers-filter';
$modalViewTransferId = 'validation-processes-transfers-view';
$h1 = $this->Fa->i('fa-folder-open-o', __("Tous les transferts à traiter"));
$sectionId = 'validation-processes-transfers-section';
$sectionTitle = __("Liste des transferts à traiter");
require 'ajax_transfers-common.php';
