<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Csv->resultTable(
    [
        'transfer.transfer_date' => __("Date du transfert"),
        'transfer.transfer_identifier' => __("Identifiant du transfert"),
        'transfer.transferring_agency.name' => __("Service versant"),
        'transfer.originating_agency.name' => __("Service producteur"),
        'transfer.transfer_comment' => __("commentaire"),
        'validation_chain.name' => __("Circuit de validation"),
        'current_stage.name' => __("Etape"),
        'created' => __("Date de création"),
        'modified' => __("Date de modification"),
        'transfer.data_size' => __("Taille PJs"),
    ],
    $results
);
