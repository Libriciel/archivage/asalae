<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$map = function (EntityInterface $entity): EntityInterface {
    $archiveUnits = [];
    $deliveryRequest = $entity->get('delivery_request');
    foreach (Hash::get($deliveryRequest, 'archive_units', []) as $archiveUnit) {
        $archiveUnits[] = Hash::get($archiveUnit, 'name');
    }
    $deliveryRequest->set(
        'archive_units',
        $archiveUnits ? '- ' . implode("\n- ", $archiveUnits) : ''
    );
    $user = Hash::get($deliveryRequest, 'created_user.username');
    if ($name = Hash::get($deliveryRequest, 'created_user.name')) {
        $user .= " ($name)";
    }
    $orgEntityName = Hash::get($deliveryRequest, 'created_user.org_entity.name');
    $user .= "\n- $orgEntityName";
    $deliveryRequest->set('created_user', $user);
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'delivery_request.last_state_update' => __("Date d'envoi"),
        'delivery_request.identifier' => __("Identifiant"),
        'delivery_request.comment' => __("Commentaire"),
        'delivery_request.derogation' => __("Dérogation"),
        'delivery_request.archive_units' => __("Unités d'archives"),
        'delivery_request.created_user' => __("Demandeur"),
        'current_stage.name' => __("Etape"),
    ],
    $results,
    $map
);
