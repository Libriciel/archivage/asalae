<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$map = function (EntityInterface $entity): EntityInterface {
    $archiveUnits = [];
    $outgoingTransferRequest = $entity->get('outgoing_transfer_request');
    foreach (Hash::get($outgoingTransferRequest, 'archive_units', []) as $archiveUnit) {
        $archiveUnits[] = Hash::get($archiveUnit, 'name');
    }
    $outgoingTransferRequest->set(
        'archive_units',
        $archiveUnits ? '- ' . implode("\n- ", $archiveUnits) : ''
    );
    $user = Hash::get($outgoingTransferRequest, 'created_user.username');
    if ($name = Hash::get($outgoingTransferRequest, 'created_user.name')) {
        $user .= " ($name)";
    }
    $orgEntityName = Hash::get($outgoingTransferRequest, 'created_user.org_entity.name');
    $user .= "\n- $orgEntityName";
    $outgoingTransferRequest->set('created_user', $user);
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'outgoing_transfer_request.last_state_update' => __("Date d'envoi"),
        'outgoing_transfer_request.identifier' => __("Identifiant"),
        'outgoing_transfer_request.comment' => __("Commentaire"),
        'outgoing_transfer_request.archive_units' => __("Unités d'archives"),
        'outgoing_transfer_request.created_user' => __("Demandeur"),
        'current_stage.name' => __("Etape"),
    ],
    $results,
    $map
);
