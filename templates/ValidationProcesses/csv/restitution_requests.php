<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$map = function (EntityInterface $entity): EntityInterface {
    $archiveUnits = [];
    $restitutionRequest = $entity->get('restitution_request');
    foreach (Hash::get($restitutionRequest, 'archive_units', []) as $archiveUnit) {
        $archiveUnits[] = Hash::get($archiveUnit, 'name');
    }
    $restitutionRequest->set(
        'archive_units',
        $archiveUnits ? '- ' . implode("\n- ", $archiveUnits) : ''
    );
    $user = Hash::get($restitutionRequest, 'created_user.username');
    if ($name = Hash::get($restitutionRequest, 'created_user.name')) {
        $user .= " ($name)";
    }
    $orgEntityName = Hash::get($restitutionRequest, 'created_user.org_entity.name');
    $user .= "\n- $orgEntityName";
    $restitutionRequest->set('created_user', $user);
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'restitution_request.last_state_update' => __("Date d'envoi"),
        'restitution_request.identifier' => __("Identifiant"),
        'restitution_request.comment' => __("Commentaire"),
        'restitution_request.archive_units' => __("Unités d'archives"),
        'restitution_request.created_user' => __("Demandeur"),
        'current_stage.name' => __("Etape"),
    ],
    $results,
    $map
);
