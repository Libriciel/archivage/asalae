<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$map = function (EntityInterface $entity): EntityInterface {
    $archiveUnits = [];
    $destructionRequest = $entity->get('destruction_request');
    foreach (Hash::get($destructionRequest, 'archive_units', []) as $archiveUnit) {
        $archiveUnits[] = Hash::get($archiveUnit, 'name');
    }
    $destructionRequest->set(
        'archive_units',
        $archiveUnits ? '- ' . implode("\n- ", $archiveUnits) : ''
    );
    $user = Hash::get($destructionRequest, 'created_user.username');
    if ($name = Hash::get($destructionRequest, 'created_user.name')) {
        $user .= " ($name)";
    }
    $orgEntityName = Hash::get($destructionRequest, 'created_user.org_entity.name');
    $user .= "\n- $orgEntityName";
    $destructionRequest->set('created_user', $user);
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'destruction_request.last_state_update' => __("Date d'envoi"),
        'destruction_request.identifier' => __("Identifiant"),
        'destruction_request.comment' => __("Commentaire"),
        'destruction_request.archive_units' => __("Unités d'archives"),
        'destruction_request.created_user' => __("Demandeur"),
        'current_stage.name' => __("Etape"),
    ],
    $results,
    $map
);
