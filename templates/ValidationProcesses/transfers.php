<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Transferts entrants"));
$this->Breadcrumbs->add(__("Transferts en cours de validation"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-clock-o', __(" Transferts en cours de validation")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$sectionId = 'validation-processes-transfers-section';
$sectionTitle = __("Liste des transferts en cours de validation");

echo $this->ModalForm->create('validation-processes-my-transfers', ['size' => 'large'])
    ->modal(__("Prendre une décision"))
    ->javascriptCallback('afterValidate')
    ->output('function', 'processTransfer', '/validation-processes/process-my-transfer');

require 'transfers-common.php';
