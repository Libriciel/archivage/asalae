<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Transferts entrants"));
$this->Breadcrumbs->add(__("Transferts non conformes à valider"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-thumbs-down', __("Transferts non conformes à valider")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$sectionId = 'section-invalidate-my-transfers';
$sectionTitle = __("Liste de mes transferts non conformes");

echo $this->ModalForm->create('validation-processes-my-transfers', ['size' => 'large'])
    ->modal(__("Validation d'un transfert d'archives non conforme"))
    ->javascriptCallback('afterValidate')
    ->output('function', 'processTransfer', '/validation-processes/process-my-transfer');

require 'transfers-common.php';
