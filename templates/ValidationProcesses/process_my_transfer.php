<?php

/**
 * @var Asalae\View\AppView $this
 */

use Asalae\Model\Table\TransfersTable;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

end($prevs);
$lastPrev = key($prevs);
reset($prevs);
$form = $this->Form->create($entity, ['idPrefix' => 'process-my-transfer'])
    . $this->Form->control(
        'action',
        [
            'label' => __("Action"),
            'options' => $actions,
            'empty' => __("-- Sélectionner une action --"),
        ]
    )
    . $this->Form->control('comment', ['label' => __("Commentaire")])
    . $this->Form->control(
        'prev_stage_id',
        [
            'label' => __("Retourner à l'étape"),
            'options' => $prevs,
            'default' => $lastPrev,
        ]
    );

if (!empty($hash)) {
    $form .= $this->Html->tag('div#sign-validate-transfer')
        . $this->LiberSign->insertSignForm(
            '#sign-validate-transfer',
            [Hash::get($process, 'transfer.id') => $hash]
        )
        . $this->Html->tag('/div');
}
$form .= $this->Form->end();

$tabs = $this->Tabs->create('tabs-validate-view-transfer', ['class' => 'row no-padding']);
$tabs->add(
    'tab-validate-view-transfer-validation',
    $this->Fa->i('fa-check-square-o', __("Validation")),
    $form
);

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($transfer->get('transfer_identifier')));
$infos .= $this->Html->tag('p', h($transfer->get('transfer_comment')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Transfert"));


$notifications = [];
if ($entity->get('state') !== TransfersTable::S_PREPARATING) {
    $notifications[__("Accusé de réception")] = function (EntityInterface $entity) {
        $filename = $entity->get('acknowledgement_filename');
        return $filename
            ? $this->Html->link(
                $filename,
                '/transfers/message/' . $entity->id . '/acknowledgement',
                ['download' => $filename]
            )
            : null;
    };
}
if ($entity->get('state') === TransfersTable::S_REJECTED) {
    $notifications[__("Notification de rejet")] = function (EntityInterface $entity) {
        $filename = $entity->get('notify_filename');
        return $this->Html->link(
            $filename,
            '/transfers/message/' . $entity->id . '/reply',
            ['download' => $filename]
        );
    };
} elseif ($entity->get('state') === TransfersTable::S_ACCEPTED) {
    $notifications[__("Notification d'acceptation")] = function (EntityInterface $entity) {
        $filename = $entity->get('notify_filename');
        return $this->Html->link(
            $filename,
            '/transfers/message/' . $entity->id . '/reply',
            ['download' => $filename]
        );
    };
}

$tables = [
    __("Informations principales") => [
        [
            __("État") => 'statetrad',
            __("Date du dernier changement d'état") => 'last_state_update',
            __("Date de création") => 'created',
            __("Version du message") => 'message_versiontrad',
            __("Est conforme") => 'is_conform',
            __("Est modifié") => 'is_modified',
            __("Créé par") => 'created_user.name',
        ],
    ],
    __("Éléments du transferts") => [
        [
            __("Date du transfert") => 'transfer_date',
            __("Service d'archives") => 'archival_agency.name',
            __("Service versant") => 'transferring_agency.name',
            __("Accord de versement") => 'agreement.name',
            __("Profils d'archives") => 'profile.name',
            __("Niveau de service") => 'service_level.name',
            __("Nombre de pièces jointes") => 'data_count',
            __("Taille des pièces jointes") => '{data_size|default(0)|toReadableSize} ({data_size} Octets)',
            __("Pièces jointes supprimées après rétention") => 'files_deleted',
        ],
    ],
    __("Accusé de réception et notification") => [$notifications],
];
$infos .= $this->ViewTable->multiple($transfer, $tables);
$infos .= $this->Html->tag('/article');

$tabs->add(
    'tabs-transfer-view-transfer',
    $this->Fa->i('fa-file-code-o', __("Transfert")),
    $infos
);

$tableAttachments = $this->Table
    ->create('view-transfer-attachments', ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'transfers',
            'action' => 'view-attachments',
            $transfer->id,
            '?' => [
                'sort' => 'filename',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'filename' => [
                'label' => __("Nom de fichier"),
                'callback' => 'TableHelper.wordBreak("/")',
                'order' => 'filename',
                'filter' => [
                    'filename[0]' => [
                        'id' => 'filter-transfer-attachment-filename-0',
                        'label' => false,
                        'aria-label' => __("Nom de fichier"),
                    ],
                ],
            ],
            'size' => [
                'label' => __("Taille"),
                'callback' => 'TableHelper.readableBytes',
                'order' => 'size',
                'filter' => [
                    'size[0][value]' => [
                        'id' => 'filter-size-0',
                        'label' => false,
                        'aria-label' => __("Taille 1"),
                        'prepend' => $this->Input->operator('size[0][operator]', '>='),
                        'append' => $this->Input->mult('size[0][mult]'),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 1,
                    ],
                    'size[1][value]' => [
                        'id' => 'filter-size-1',
                        'label' => false,
                        'aria-label' => __("Taille 2"),
                        'prepend' => $this->Input->operator('size[1][operator]', '<='),
                        'append' => $this->Input->mult('size[1][mult]'),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 1,
                    ],
                ],
            ],
            'meta' => [
                'label' => __("Meta-donnés"),
                'target' => '',
                'thead' => [
                    'hash' => [
                        'label' => __("Empreinte du fichier"),
                        'callback' => 'TableHelper.wordBreak("_", 32)',
                    ],
                    'hash_algo' => ['label' => __("Algorithme de hashage")],
                    'mime' => ['label' => __("Type MIME")],
                    'format' => ['label' => __("Format")],
                    'virus_name' => ['label' => __("VIRUS")],
                ],
            ],
        ]
    )
    ->data(Hash::get($transfer, 'transfer_attachments', []))
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'href' => "/transfer-attachments/download/{0}/{3}",
                'download' => '{2}',
                'target' => '_blank',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => $title = __("Télécharger {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/transfer-attachments/download'),
                'params' => ['id', 'filename', 'basename', 'url_basename'],
            ],
            [
                'href' => '/transfer-attachments/open/{0}/{3}',
                'target' => '_blank',
                'label' => $this->Fa->i('fas fa-external-link-alt'),
                'title' => $title = __("Ouvrir dans le navigateur {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/transfer-attachments/open'),
                'displayEval' => 'data[{index}].openable',
                'params' => ['id', 'filename', 'basename', 'url_basename'],
            ],
        ]
    );
$paginator = $this->AjaxPaginator->create('pagination-transfer-view-attachments')
    ->url($url)
    ->table($tableAttachments)
    ->count($countUploads);

$transferAttachmentTabContent = $paginator->generateTable();

$tabs->add(
    'tab-transfer-attachments',
    $this->Fa->i('fa-paperclip', __("Fichiers des pièces jointes")),
    $transferAttachmentTabContent
);

$errors = Hash::get($transfer, 'transfer_errors');
if (!empty($errors)) {
    $tableErrors = $tableAttachments = $this->Table
        ->create('view-transfer-errors', ['class' => 'table table-striped table-hover'])
        ->fields(
            [
                'level' => ['label' => __("Niveau")],
                'code' => ['label' => __("Code")],
                'message' => ['label' => __("Message")],
            ]
        )
        ->data($errors)
        ->params(
            [
                'identifier' => 'id',
            ]
        );
    $paginator = $this->AjaxPaginator->create('pagination-transfer-view-errors')
        ->url(
            [
                'controller' => 'transfers',
                'action' => 'view-errors',
            ]
        )
        ->table($tableErrors)
        ->count($countErrors);
    $tabs->add(
        'tab-transfer-errors',
        $this->Fa->i('fa-warning', __("Erreurs")),
        $paginator->generateTable()
    );
}

echo $tabs;
?>
<script>
    $('#process-my-transfer-action').off('change').change(function () {
        $('#process-my-transfer-prev-stage-id').closest('.form-group').toggle($(this).val() === 'stepback');
    }).trigger('change');
</script>
