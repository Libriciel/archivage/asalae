<?php

/**
 * @var Asalae\View\AppView $this
 */

$idPrefix = 'destruction-request-add';
$jsTableId = 'add-destruction-request-archive-units';
$paginatorId = 'pagination-add-destruction-request-archive-units';
$modalId = 'add-destruction-request';

require 'addedit-common.php';
?>
<script>
    // suppression de la demande si on appuis sur annuler
    $('#<?=$modalId?>').find('.modal-footer button.cancel')
        .off('.delete-add')
        .on('click.delete-add', function () {
            $.ajax({
                url: '<?=$this->Url->build('/destruction-requests/delete/' . $entity->id)?>',
                method: 'DELETE'
            });
        });
</script>
