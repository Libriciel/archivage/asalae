<?php

/**
 * @var Asalae\View\AppView $this
 */

$idPrefix = 'destruction-request-edit';
$jsTableId = 'edit-destruction-request-archive-units';
$paginatorId = 'pagination-edit-destruction-request-archive-units';
$modalId = 'edit-destruction-request';

require 'addedit-common.php'; // Form dans le common
