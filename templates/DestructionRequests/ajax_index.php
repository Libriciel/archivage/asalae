<?php

/**
 * @var Asalae\View\AppView $this
 */

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'DestructionRequests.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'DestructionRequests.comment' => [
                'label' => __("Commentaire"),
            ],
            'DestructionRequests.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'DestructionRequests.archive_units' => [
                'label' => __("Unités d'archives"),
                'callback' => 'TableHelper.ul("name")',
                'escape' => false,
                'filter' => [
                    'archive_unit_identifier[0]' => [
                        'id' => 'filter-archive_unit_identifier-0',
                        'label' => __("Identifiant"),
                    ],
                    'archive_unit_name[0]' => [
                        'id' => 'filter-archive_unit_name-0',
                        'label' => __("Nom"),
                    ],
                ],
            ],
            'DestructionRequests.archive_units_count' => [
                'label' => __("Nombre d'unités d'archives"),
                'display' => false,
            ],
            'DestructionRequests.statetrad' => [
                'label' => __("Etat"),
                'filter' => [
                    'state[0]' => [
                        'id' => 'filter-state-0',
                        'label' => false,
                        'aria-label' => __("Etat"),
                        'options' => $states,
                    ],
                ],
            ],
        ]
        + (in_array($this->getRequest()->getParam('action'), ['indexPreparating', 'indexMy'])
            ? []
            : [
                'DestructionRequests.created_user.username' => [
                    'label' => __("Créée par"),
                    'escape' => false,
                    'order' => 'created_user.username',
                    'filterCallback' => 'AsalaeFilter.datepickerCallback',
                    'filter' => [
                        'created_user[0]' => [
                            'id' => 'filter-created_user-0',
                            'label' => __("Créée par"),
                            'options' => $created_users,
                        ],
                    ],
                ],
            ])
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'DestructionRequests.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewDestructionRequest({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/DestructionRequests/view'),
                'params' => ['DestructionRequests.id', 'DestructionRequests.identifier'],
            ],
            [
                'href' => "/DestructionRequests/download-pdf/{0}",
                'download' => '{2}',
                'target' => '_blank',
                'data-action' => __("Télécharger le PDF"),
                'label' => $this->Fa->i('fa-file-pdf'),
                'title' => $title = __("Télécharger le PDF de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/DestructionRequests/download-pdf'),
                'displayEval' => 'data[{index}].DestructionRequests.state !== "creating"',
                'params' => [
                    'DestructionRequests.id',
                    'DestructionRequests.identifier',
                    'DestructionRequests.pdf_basename',
                ],
            ],
            [
                'onclick' => "actionEditDestructionRequest({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/DestructionRequests/edit'),
                'displayEval' => 'data[{index}].DestructionRequests.editable '
                    . '&& data[{index}].DestructionRequests.created_user_id === PHP.user_id',
                'params' => ['DestructionRequests.id', 'DestructionRequests.identifier'],
            ],
            [
                'data-callback' => "sendDestructionRequest($jsTable, {0})",
                'type' => 'button',
                'class' => 'btn-link send',
                'display' => $this->Acl->check('/DestructionRequests/send'),
                'displayEval' => 'data[{index}].DestructionRequests.sendable '
                    . '&& data[{index}].DestructionRequests.created_user_id === PHP.user_id',
                'label' => $this->Fa->charte('Envoyer', '', 'text-success'),
                'title' => __("Envoyer {0}", '{1}'),
                'aria-label' => __("Envoyer {0}", '{1}'),
                'confirm' => __(
                    "Une fois la demande d'élimination envoyée, il ne sera "
                    . "plus possible de la modifier. Voulez-vous continuer ?"
                ),
                'params' => ['DestructionRequests.id', 'DestructionRequests.identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/DestructionRequests/delete')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Supprimer"),
                'displayEval' => 'data[{index}].DestructionRequests.deletable '
                    . '&& data[{index}].DestructionRequests.created_user_id === PHP.user_id',
                'confirm' => __("Êtes-vous sûr de vouloir supprimer cette demande d'élimination ?"),
                'params' => ['DestructionRequests.id', 'DestructionRequests.identifier'],
            ],
            [
                'href' => $this->Url->build('/destruction-notifications/destruction-certificate') . '/{0}',
                'download' => '{1}_destruction_certificate.pdf',
                'target' => '_blank',
                'type' => 'button',
                'class' => 'btn-link',
                'data-action' => __("Attestation d'élimination"),
                'label' => $this->Fa->i('fas fa-award'),
                'title' => $title = __("Attestation d’élimination de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/DestructionNotifications/destruction-certificate'),
                'displayEval' => 'data[{index}].certified !== null',
                'params' => ['destruction_notification.id', 'destruction_notification.identifier'],
            ],
        ]
    );

$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des demandes d'élimination"),
        'table' => $table,
    ]
);
