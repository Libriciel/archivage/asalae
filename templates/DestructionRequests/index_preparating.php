<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Eliminations"));
$this->Breadcrumbs->add($h1 = __("Mes demandes d'élimination en préparation"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-pencil-square-o', $h1))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

require 'index-common.php';
