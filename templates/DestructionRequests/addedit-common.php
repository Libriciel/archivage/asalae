<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $entity
 */

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;

$form = $this->Html->tag('article');
$form .= $this->Html->tag('header.bottom-space');
$form .= $this->Html->tag('h3', h($entity->get('identifier')));
$form .= $this->Html->tag('/header');

$form .= $this->Form->create($entity, ['idPrefix' => $idPrefix]);
$form .= $this->Form->control('id', ['type' => 'hidden']);
$form .= $this->Form->control(
    'comment',
    [
        'label' => __("Commentaire"),
    ]
);

$jsTable = $this->Table->getJsTableObject($jsTableId);

$tableUnits = $this->Table
    ->create($jsTableId, ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'destruction-requests',
            'action' => 'paginate-archive-units',
            $entity->id,
            '?' => [
                'sort' => 'archival_agency_identifier',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'archival_agency_identifier' => [
                'label' => __("Identifiant"),
            ],
            'name' => [
                'label' => __("Nom"),
            ],
            'description' => [
                'label' => __("Description"),
            ],
            'dates' => [
                'label' => __("Dates extrêmes"),
            ],
            'original_total_count' => [
                'label' => __("Nombre de fichiers"),
            ],
            'original_total_size' => [
                'label' => __("Taille des fichiers"),
                'callback' => 'TableHelper.readableBytes',
            ],
        ]
    )
    ->data($units)
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewUnitDescription({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/description'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'href' => $this->Url->build('/archive-units/display-xml') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/display-xml'),
                'displayEval' => '!data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'data-callback' => sprintf(
                    "actionRemoveArchiveUnit(%s, '%s')('{0}', false)",
                    $jsTable,
                    $this->Url->build('/destruction-requests/removeArchiveUnit') . '/' . $entity->id
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Retirer cette unité d'archives de la demande d'élimination ?"),
                'displayEval' => 'data.length > 1',
                'params' => ['id', 'archival_agency_identifier'],
            ],
        ]
    );

$form .= $this->AjaxPaginator->create($paginatorId)
    ->url($url)
    ->table($tableUnits)
    ->count($unitsCount)
    ->generateTable();

$form .= $this->Form->end();
$form .= $this->Html->tag('/article');

echo $form;
?>
<script>
    function actionRemoveArchiveUnit(table, url) {
        return function (id) {
            $(table.table).off('.jstable').one('deleted.tablejs', function () {
                table.generateAll();
            });
            TableGenericAction.deleteAction(table, url)(id, false);
        }
    }
</script>
