<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>

<AuthorizationOriginatingAgencyRequest xmlns="fr:gouv:culture:archivesdefrance:seda:v2.1">
    <?= $comment ? "    <Comment>$comment</Comment>" . PHP_EOL : '' ?>
    <?php
    // insertion des metadonnées de volumétrie dans un commentaire
    echo '    <Comment>' . __("Volumétrie de la demande d'élimination") . PHP_EOL;
    $metadata = [
        'archive_units_count',
        'original_count',
        'original_size',
    ];
    foreach ($metadata as $meta) {
        echo "        $meta: {$destructionRequest->get($meta)}\n";
    }
    echo '    </Comment>' . PHP_EOL;

    // insertion des metadonnées des archive_units
    foreach ($queryArchiveUnits as $au) {
        echo '    <Comment>' . PHP_EOL;
        $metadata = [
            'archival_agency_identifier',
            'name',
            'original_total_count',
            'original_total_size',
        ];
        foreach ($metadata as $meta) {
            $value = htmlspecialchars((string)$au[$meta], ENT_XML1);
            echo "        $meta: $value\n";
        }
        echo '    </Comment>' . PHP_EOL;
    }
    ?>
    <Date><?= $datetime ?></Date>
    <MessageIdentifier><?= $identifier ?></MessageIdentifier>
    <CodeListVersions></CodeListVersions>
    <AuthorizationRequestContent>
        <AuthorizationReason>ARCHIVE_DESTRUCTION_REQUEST</AuthorizationReason>
        <RequestDate><?= $date ?></RequestDate>
        <?php
        foreach ($queryArchiveUnits as $au) {
            $identifier = htmlspecialchars($au['archival_agency_identifier'], ENT_XML1, 'UTF-8');
            echo '        <UnitIdentifier>' . $identifier . '</UnitIdentifier>' . PHP_EOL;
        }
        ?>
        <Requester>
            <Identifier><?= $originatingAgency ?></Identifier>
        </Requester>
    </AuthorizationRequestContent>
    <ArchivalAgency>
        <Identifier><?= $archivalAgency ?></Identifier>
    </ArchivalAgency>
    <OriginatingAgency>
        <Identifier><?= $originatingAgency ?></Identifier>
    </OriginatingAgency>
</AuthorizationOriginatingAgencyRequest>