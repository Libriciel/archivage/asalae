<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $archiveUnit
 */

use AsalaeCore\I18n\Date as CoreDate;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Date as CakeDate;
use Cake\Utility\Hash;

$view = $this;

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($archiveUnit->get('name')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Description"));
$infos .= $this->ViewTable->generate(
    $archiveUnit,
    [
        __("Cote") => 'archival_agency_identifier',
        __("Nom") => 'name',
        __("Description") => 'archive_description.description',
        __("Profil d'archive") => Hash::get($archiveUnit, 'archive.profile')
            ? '{archive.profile.name} ({archive.profile.identifier})'
            : null,
        __("Date de création") => 'archive.created',
        __("Service versant")
        => '{archive.transferring_agency.name} ({archive.transferring_agency.identifier})',
        __("Service producteur")
        => '{archive.originating_agency.name} ({archive.originating_agency.identifier})',
        __("Accord de versement") => Hash::get($archiveUnit, 'archive.agreement')
            ? '{archive.agreement.name} ({archive.agreement.identifier})'
            : null,
        __("Dates extrêmes")
        => '{archive_description.oldest_date|default("[nd]")} - {archive_description.latest_date|default("[nd]")}',
        __("Communicabilité") => function (EntityInterface $entity) {
            $endDate = Hash::get($entity, 'access_rule.end_date');
            $reportable = ($endDate instanceof CakeDate || $endDate instanceof CoreDate)
                && $endDate->format('Y') <= (int)date('Y') + 120
                ? __("librement communicable : {0}", $endDate)
                : __("non communicable (illimité)");
            return __(
                "Date de départ du calcul : {0}, code : {1}, {2}",
                Hash::get($entity, 'access_rule.start_date'),
                Hash::get($entity, 'access_rule.access_rule_code.code'),
                $reportable
            );
        },
        __("Date de fin de dérogation (communicabilité)") => '{max_aggregated_access_end_date|default("[nd]")}',
        __("DUA et sort final") => __(
            "Date de départ de calcul : {0}, durée : {1}, sort final : {2}",
            '{appraisal_rule.start_date}',
            '{appraisal_rule.appraisal_rule_code.name}',
            '{appraisal_rule.final_action_codetrad}'
        ),
    ]
);

$infos .= $this->Html->tag('h4', __("Fichiers originaux"));
$infos .= $this->ViewTable->generate(
    $archiveUnit,
    [
        __("Directement liés à l'unité d'archive")
        => '{original_local_count} ({original_local_size|toReadableSize})',
        __("Liés à l'unité d'archive et à ses filles")
        => '{original_total_count} ({original_total_size|toReadableSize})',
    ]
);

$infos .= $this->Html->tag('/article');

$tabs = $this->Tabs->create('tabs-view-archive-unit', ['class' => 'row no-padding']);

$tabs->add(
    'tab-view-archive-unit-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);

$tableBinaries = $this->Table
    ->create('view-archive-unit-binaries', ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'archive-units',
            'action' => 'view-binaries',
            $id,
            '?' => [
                'sort' => 'filename',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'meta' => [
                'label' => __("Fichiers"),
                'target' => '',
                'thead' => [
                    'filename' => ['label' => __("Nom de fichier")],
                    'format' => ['label' => __("Format")],
                    'mime' => ['label' => __("Type MIME")],
                    'stored_file.size' => [
                        'label' => __("Taille"),
                        'callback' => 'TableHelper.readableBytes',
                        'order' => 'size',
                    ],
                    'stored_file.hash' => [
                        'label' => __("Empreinte du fichier"),
                        'callback' => 'TableHelper.wordBreak("_", 32)',
                    ],
                    'stored_file.hash_algo' => ['label' => __("Algorithme de hashage")],
                ],
                'filter' => [
                    'filename[0]' => [
                        'id' => 'filter-au-archive-binary-filename-0',
                        'label' => __("Nom de fichier"),
                    ],
                    'format[0]' => [
                        'id' => 'filter-au-archive-binary-format-0',
                        'label' => __("Format"),
                    ],
                    'mime[0]' => [
                        'id' => 'filter-au-archive-binary-mime-0',
                        'label' => __("Type MIME"),
                    ],
                    'size[0][value]' => [
                        'id' => 'filter-au-archive-binary-size-0',
                        'label' => __("Taille 1"),
                        'prepend' => $this->Input->operator('size[0][operator]', '>='),
                        'append' => $this->Input->mult('size[0][mult]'),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 1,
                    ],
                    'size[1][value]' => [
                        'id' => 'filter-au-archive-binary-size-1',
                        'label' => __("Taille 2"),
                        'prepend' => $this->Input->operator('size[1][operator]', '<='),
                        'append' => $this->Input->mult('size[1][mult]'),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 1,
                    ],
                ],
            ],
            'archive_units' => [
                'label' => __("Lié à l'unité d'archive"),
                'callback' => 'TableHelper.ul("relative_path")',
            ],
        ]
    )
    ->data($binaries)
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewBinary({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-list'),
                'title' => $title = __("Fichiers liés à {0}", '{1}'),
                'aria-label' => $title,
                'displayEval' => 'data[{index}].other_datas.length > 0',
                'params' => ['id', 'filename'],
            ],
            [
                'href' => "/archive-binaries/download/{0}/{3}",
                'download' => '{2}',
                'target' => '_blank',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => $title = __("Télécharger {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-binaries/download'),
                'params' => ['id', 'filename', 'basename', 'url_basename'],
            ],
            [
                'href' => '/archive-binaries/open/{0}/{3}',
                'target' => '_blank',
                'label' => $this->Fa->i('fas fa-external-link-alt'),
                'title' => $title = __("Ouvrir dans le navigateur {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-binaries/open'),
                'displayEval' => 'data[{index}].openable',
                'params' => ['id', 'filename', 'basename', 'url_basename'],
            ],
        ]
    );

$paginator = $this->AjaxPaginator->create('pagination-transfer-view-attachments')
    ->url($url)
    ->table($tableBinaries)
    ->count($countBinaries);

$archiveFilesTab = $paginator->generateTable();

$tabs->add(
    'tab-view-archive-binaries',
    $this->Fa->i('fa-file-o', __("Fichiers")),
    $archiveFilesTab
);

echo $tabs;
