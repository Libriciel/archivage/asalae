<?php

/**
 * @var Asalae\View\AppView $this
 */

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

echo $this->Html->tag(
    'div.alert.alert-info',
    __(
        "La taille de la description (> 2,5Mo) ne permet pas d'ouvrir "
        . "l'archive via la visionneuse. Vous pouvez accéder aux informations "
        . "en cliquant sur les éléments de l'arborescence."
    )
);

/**
 * Visionneur XML
 */
$jstreeId = 'jstree-view-archive';
$dataId = 'view-data-public-description';
echo $this->Html->tag('div#' . $jstreeId . '.col-md-4', $loading)
    . $this->Html->tag(
        'div#' . $dataId . '.col-md-8.jstree-div-form',
        $this->Form->create()
        . $this->Form->control('action', ['type' => 'hidden', 'default' => 'cancel'])
        . $this->Form->end()
    );
?>
<script>
    $('.jstree[role=tree]:not(:visible)').empty();
    var tree = $('#<?=$jstreeId?>');
    var modal = tree.closest('.modal');
    var zoneDescription = $('#<?=$dataId?>');
    var loadingAnim = $('<?=$loading?>');
    var ajaxData = {
        url: '<?=$this->Url->build('/archive-units/get-public-tree/' . $id)?>/',
        data: function (node) {
            return {
                id: node.id
            };
        }
    };

    $(document).off('.vakata.custom').off('.jstree.custom');
    tree.jstree({
        core: {
            data: ajaxData,
            check_callback: function (op, node, par, pos, more) {
                return true;
            }
        },
        search: {
            case_insensitive: true,
            show_only_matches: false
        },
        plugins: [
            "wholerow" // sélection sur la ligne (clic facilité et affichage + moderne)
        ]
    })
        .off('ready.jstree set_state.jstree') // affiche les pointillés (supprimés dans cet event par wholerow)
        .on('select_node.jstree', function (event, data) {
            zoneDescription.html(loadingAnim);
            if (data.node.original.navigation) {
                $.ajax({
                    url: '<?=$this->Url->build('/archive-units/get-public-tree/' . $id)?>/?id=' + data.node.id,
                    success: function (content) {
                        var jstreeObj = tree.jstree(true);
                        var parentId = data.node.parent;
                        var parent = jstreeObj.get_node(parentId);
                        if (parent.original.navigation) { // si le parent est une liste, on doit remonter d'un cran
                            parentId = parent.parent;
                            parent = jstreeObj.get_node(parentId);
                        }

                        if (typeof content === 'string') {
                            return this.error({responseText: content});
                        }

                        jstreeObj.delete_node(parent.children);
                        for (var i = 0; i < content.length; i++) {
                            jstreeObj.create_node(parentId, content[i]);
                        }
                        var title = data.node.id.split('_');
                        title.shift();
                        var page = title.shift();

                        zoneDescription.html($('<h4 class="h2"></h4>').text(title.pop() + ' - Page ' + page));
                    },
                    error: function (e) {
                        zoneDescription.html(e.responseText);
                    }
                });
            } else {
                $.ajax({
                    url: '<?=$this->Url->build('/archive-units/view-public-description-node/' . $id)?>/'
                        + data.node.original.url,
                    success: function (content) {
                        zoneDescription.html(content);
                    },
                    error: function (e) {
                        zoneDescription.html(e.responseText);
                    }
                });
            }
        });
</script>
