<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Archives"));
$this->Breadcrumbs->add(__("Archives consultables"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-archive', __("Archives consultables")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

echo $this->element('modal', ['idTable' => $tableId]);
$jsTable = $this->Table->getJsTableObject($tableId);

echo $this->SearchEngine->form(
    null,
    ['id' => 'archive-units-searchbar-form']
);
echo $this->Form->control(
    'all',
    [
        'type' => 'hidden',
        'value' => 'all',
    ]
);
echo $this->SearchEngine->input(
    'search',
    ['id' => 'archive-units-searchbar']
);
echo $this->Form->end();

echo $this->ModalView
    ->create('view-archive-unit-modal', ['size' => 'large'])
    ->modal(__("Visualiser une unité d'archives"))
    ->output(
        'function',
        'actionViewArchiveUnit',
        '/ArchiveUnits/view'
    )
    ->generate();

echo $this->ModalView->create('view-binary-archive-unit')
    ->modal(__("Fichiers liés à un fichier original d'une archive"))
    ->output('function', 'viewBinary', '/Archives/view-binary')
    ->generate();

echo $this->ModalView->create('description-archive-unit', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une unité d'archive"))
    ->output('function', 'viewUnitDescription', '/ArchiveUnits/description')
    ->generate();

echo $this->ModalForm
    ->create('archive-unit-edit', ['size' => 'large'])
    ->modal(__("Modification d'une unité d'archives"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "ArchiveUnits")')
    ->output('function', 'actionEditArchiveUnit', '/ArchiveUnits/edit')
    ->generate();

echo $this->Filter->create('archive-filter')
    ->filter(
        'id',
        [
            'hidden' => true,
        ]
    )
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'archival_agency_identifier',
        [
            'label' => __("Cote"),
            'wildcard',
        ]
    )
    ->filter(
        'originating_agency_id',
        [
            'label' => __("Service producteur"),
            'options' => $originating_agencies,
            'empty' => __("-- Sélectionner un service producteur --"),
        ]
    )
    ->filter(
        'transferring_agency_id',
        [
            'label' => __("Service versant"),
            'options' => $transferring_agencies,
            'empty' => __("-- Sélectionner un service versant --"),
        ]
    )
    ->filter(
        'profile_id',
        [
            'label' => __("Profil d'archives"),
            'options' => $profiles,
            'empty' => __("-- Sélectionner un profil d'archives --"),
        ]
    )
    ->filter(
        'agreement_id',
        [
            'label' => __("Accord de versement"),
            'options' => $agreements,
            'empty' => __("-- Sélectionner un accord de versement --"),
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'original_total_size[value]',
        [
            'label' => __("Taille totale des fichiers"),
            'prepend' => $this->Input->operator(
                'original_total_size[operator]',
                '>=',
                ['id' => 'original-total-size-operator']
            ),
            'append' => $this->Input->mult(
                'original_total_size[mult]',
                'mo',
                ['id' => 'original-total-size-mult']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'original_total_count[value]',
        [
            'label' => __("Nombre de fichiers"),
            'prepend' => $this->Input->operator(
                'original_total_count[operator]',
                '>=',
                ['id' => 'original-total-count-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'keyword',
        [
            'label' => __("Mot clé"),
            'data-callback' => 'searchKeywordPopup',
        ]
    )
    ->filter(
        'filename',
        [
            'label' => __("Nom de fichier"),
            'wildcard',
        ]
    )
    ->filter(
        'mime',
        [
            'label' => __("Mime de fichier"),
            'wildcard',
        ]
    )
    ->filter(
        'format',
        [
            'label' => __("Format de fichier"),
            'wildcard',
        ]
    )
    ->filter(
        'history',
        [
            'label' => __("Historique"),
            'wildcard',
        ]
    )
    ->filter(
        'description',
        [
            'label' => __("Description"),
            'wildcard',
        ]
    )
    ->filter(
        'transferring_agency_identifier',
        [
            'label' => __("Identifiant d'archive fourni par le service versant"),
            'wildcard',
        ]
    )
    ->filter(
        'originating_agency_identifier',
        [
            'label' => __("Identifiant d'archive fourni par le service producteur"),
            'wildcard',
        ]
    )
    ->filter(
        'oldest_date',
        [
            'label' => __("Date la plus ancienne"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'latest_date',
        [
            'label' => __("Date la plus récente"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'keyword_reference',
        [
            'label' => __("Identifiant du mot clé dans un référentiel donné"),
            'wildcard',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->saves($savedFilters)
    ->generateSection();

require 'ajax_index.php';

echo $this->SearchEngine->script(
    'archive-units-searchbar',
    $this->Url->build('/archive-units/index'),
    $this->Url->build('/archive-units/full-search-bar'),
    [
        __("Cote") => 'archival_agency_identifier',
        __("Nom") => 'name',
        __("Description") => 'archive_description.description',
        __("Historique") => 'archive_description.history',
        __("Mots-clés") => 'archive_keywords.{n}.content',
    ]
);
?>
<script>
    AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');

    function getNavTree(id) {
        var table = <?=$jsTable?>;
        var tr = table.table.find('tr[data-id="' + id + '"]');
        var popup = new AsalaePopup(table.table.parent());
        popup.element()
            .append(AsalaeModal.loading)
            .css({
                top: tr.offset().top - table.table.parent().offset().top,
                "max-width": 600,
                "max-height": 400,
                overflow: "auto"
            });
        popup.show();
        AsalaeLoading.ajax(
            {
                url: '<?=$this->Url->build('/archive-units/get-nav-tree')?>/' + id,
                success: function (content) {
                    popup.element().append(content);
                }
            },
            function () {
                return null;
            },
            function () {
                return popup.element().find('.loading-container').remove();
            }
        );
    }

    function archiveUnitFileDownload(id) {
        if (event.ctrlKey) {
            id += '?renew=true';
        }
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/ArchiveUnits/make-zip')?>/' + id,
            headers: {
                Accept: 'application/zip'
            },
            success: function (content, textStatus, jqXHR) {
                switch (jqXHR.getResponseHeader('X-Asalae-ZipFileDownload')) {
                    case 'email':
                    case 'fail':
                        alert(content);
                        break;
                    case 'download':
                        var disposition = jqXHR.getResponseHeader('content-disposition');
                        var matches = /"([^"]*)"/.exec(disposition);
                        var filename = (matches != null && matches[1] ? matches[1] : 'archive_unit.zip');

                        var link = document.createElement('a');
                        document.body.appendChild(link);

                        link.href = '<?=$this->Url->build('/ArchiveUnits/download-files/')?>' + id;
                        link.download = filename;
                        link.dataset.mode = 'no-ajax';
                        link.click();
                        document.body.removeChild(link);
                        break;
                }
            }
        });
    }
</script>
