<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create(null, ['id' => 'popup-search-form']);
echo $this->Form->control(
    'search',
    [
        'id' => 'popup-search-bar',
        'label' => __("Rechercher"),
        'append' => $this->Fa->i('fa-search')
            . $this->Html->tag(
                'span.sr-only',
                __("Rechercher")
            ),
        'templates' => [
            'inputGroupAddons' => '<span class="input-group-addon btn btn-primary" '
                . 'id="popup-search-bar-btn" role="button" tabindex="0" title="'
                . __("Rechercher") . '">{{content}}</span>',
        ],
    ]
);
echo $this->Form->button(
    'submit',
    [
        'style' => 'display: none',
    ]
);
echo $this->Form->end();
?>
<div id="nav-jstree" style="padding-right: 10px"></div>
<!--suppress JSPotentiallyInvalidConstructorUsage -->
<script>
    $('.jstree[role=tree]:not(:visible)').empty();
    var tree = $('#nav-jstree');
    var initialId = <?=$id?>;
    tree.jstree({
        core: {
            data: <?=json_encode($tree)?>
        },
        search: {
            caseSensitive: false,
            show_only_matches: true,
            show_only_matches_children: true,
            search_callback: function (search, node) {
                search = search.toLowerCase().unaccents();
                return node.original.search.identifier.indexOf(search) >= 0
                    || node.original.search.name.indexOf(search) >= 0;
            }
        },
        plugins: [
            "search", // Recherche par nom de noeud
            "wholerow" // sélection sur la ligne (clic facilité et affichage + moderne)
        ]
    })
        .off('ready.jstree set_state.jstree') // affiche les pointillés (supprimés dans cet event par wholerow)
        .on('select_node.jstree', function (event, data) {
            var archive_unit_id = data.node.original.url;
            if (archive_unit_id !== initialId) {
                window.location = '<?= $this->Url->build('/archive-units/index') ?>?search='
                    + data.node.original.identifier + '&id=' + archive_unit_id;
            }
        })
        .on('changed.jstree after_open.jstree after-pagination search.jstree clear_search.jstree', function () {
            // au survol, affiche le text entier d'un noeud
            tree.find('span.child-value.trimmed:not(.mouseover)')
                .off('.jstree')
                .on('mouseover.jstree', function () {
                    var trimmed = $(this).text();
                    var name = $(this).attr('title');
                    $(this).addClass('mouseover')
                        .attr('data-text', trimmed)
                        .text(name);
                })
                .on('mouseout.jstree', function () {
                    var trimmed = $(this).attr('data-text');
                    $(this).removeClass('mouseover')
                        .attr('data-text', '')
                        .text(trimmed);
                });
        });
    var searchTimeout;
    $('#popup-search-bar-btn').on('click.jstree', function () {
        if (searchTimeout) {
            clearTimeout(searchTimeout);
        }
        var value = $('#popup-search-bar').val();
        searchTimeout = setTimeout(
            function () {
                tree.jstree(true).search(value);
                searchTimeout = null;
            },
            250
        );
    });
    $('#popup-search-form').submit(function (e) {
        e.preventDefault();
        $('#popup-search-bar-btn').trigger('click.jstree');
    });
</script>
