<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Cake\Utility\Hash;

echo $this->Flash->render();

$jsTable = $this->Table->getJsTableObject($tableId = 'edit-archive-unit-keywords');
$keywords = '';
if ($this->Acl->check('/ArchiveKeywords/add')) {
    $keywords = $this->Html->tag(
        'div.alert.alert-warning',
        __(
            "Attention, les modifications sur les mots-clés sont immédiates"
                . " et ne seront pas annulées si vous cliquez sur le bouton \"Annuler\" du formulaire."
        )
    )
        . $this->ModalForm->create('add-archive-unit-keyword', ['size' => 'large'])
            ->modal(__("Ajout d'un mot clé"))
            ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "ArchiveKeywords")')
            ->output(
                'button',
                $this->Fa->charte('Ajouter', __("Ajouter un mot clé")),
                '/ArchiveKeywords/add/' . $entity->get('id')
            )
            ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
echo $this->ModalForm
    ->create('edit-archive-unit-keyword', ['size' => 'large'])
    ->modal(__("Modification d'un mot clé"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "ArchiveKeywords")')
    ->output('function', 'loadEditModalArchiveKeyword', '/ArchiveKeywords/edit')
    ->generate();

$keywords .= $this->Table->create($tableId)
    ->fields(
        [
            'content' => ['label' => 'KeywordContent'],
            'reference' => ['label' => 'KeywordReference'],
            'type' => ['label' => 'KeywordType'],
        ]
    )
    ->data(Hash::get($entity, 'archive_keywords', []))
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].invalid ? "danger" : ""',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "loadEditModalArchiveKeyword({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-keywords/edit'),
                'params' => ['ArchiveKeywords.id', 'ArchiveKeywords.content'],
            ],
            [
                'type' => "button",
                'class' => "btn-link",
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/archive-keywords/delete')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer ce mot clé"),
                'aria-label' => $title,
                'confirm' => __("Êtes-vous sûr de vouloir supprimer ce mot clé ?"),
                'display' => $this->Acl->check('/archive-keywords/delete'),
                'params' => ['ArchiveKeywords.id'],
            ],
        ]
    )
    ->generate();

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => 'edit-archive-unit',
        'id' => 'edit-archive-unit-form',
        'class' => 'accordion',
    ]
);

if ($entity->get('parent_id') === null) {
    echo $this->Html->tag('h3', __("Archive"));
    echo $this->Html->tag(
        'div',
        $this->Form->control(
            'archive.name',
            [
                'label' => __("Nom"),
            ]
        )
        . $this->Form->control(
            'archive.agreement_id',
            [
                'label' => __("Accord de versement"),
                'options' => $agreements,
                'empty' => __("-- Sélectionner un accord de versement --"),
            ]
        )
        . $this->Form->control(
            'archive.profile_id',
            [
                'label' => __("Profil d'archive"),
                'options' => $profiles,
                'empty' => __("-- Sélectionner un profil d'archive --"),
            ]
        )
    );
} else {
    echo $this->Html->tag('h3', __("Unité d'archives"));
    echo $this->Html->tag(
        'div',
        $this->Form->control(
            'name',
            [
                'label' => __("Nom"),
            ]
        )
    );
}

if ($entity->get('archive_description')) {
    if ($entity->get('parent_id') === null) {
        $useParentAccess = false;
        $checkboxAccess = '';
    } else {
        $useParentAccess = Hash::get($entity, 'parent_description.access_rule_id')
            === Hash::get($entity, 'archive_description.access_rule_id');
        $checkboxAccess = $this->Form->control(
            'use_parent_description_access_rule',
            [
                'label' => __("Utilise les restrictions d'accès de l'unité d'archives parente"),
                'type' => 'checkbox',
                'val' => $useParentAccess,
                'class' => 'use-parent-checkbox',
                'data-target' => 'description-access-rule',
            ]
        );
    }
    echo $this->Html->tag('h3', __("Description"));
    echo $this->Html->tag('div')
        . $this->Form->control(
            'archive_description.description',
            [
                'label' => __("Description"),
            ]
        )
        . $this->Form->control(
            'archive_description.oldest_date',
            [
                'label' => __("Date la plus ancienne"),
                'type' => 'text',
                'append' => $this->Date->picker('#edit-archive-unit-archive-description-oldest-date'),
                'class' => 'datepicker ',
            ]
        )
        . $this->Form->control(
            'archive_description.latest_date',
            [
                'label' => __("Date la plus récente"),
                'type' => 'text',
                'append' => $this->Date->picker('#edit-archive-unit-archive-description-latest-date'),
                'class' => 'datepicker',
            ]
        )
        . $this->Form->control(
            'archive_description.history',
            [
                'label' => __("Historique"),
            ]
        );
    if ($entity->get('parent_id') === null) {
        echo $this->Form->control(
            'archive.originating_agency_id',
            [
                'label' => __("Service producteur"),
                'options' => $originating_agencies,
                'empty' => __("-- Sélectionner un service producteur --"),
            ]
        );
    }
    echo $this->Form->fieldset(
        $keywords,
        ['legend' => __("Indexation")]
    );

    $sedaVersion = Hash::get($entity, 'archive.transfers.0.message_version');
    if ($sedaVersion === 'seda1.0' || $sedaVersion === 'seda0.2') {
        echo $this->Form->fieldset(
            $checkboxAccess
            . $this->Form->control(
                'archive_description.access_rule.access_rule_code_id',
                [
                    'label' => __("Délai de communicabilité"),
                    'options' => $codes,
                    'required' => true,
                    'disabled' => $useParentAccess,
                    'empty' => __("-- Sélectionner un délai --"),
                    'class' => 'description-access-rule',
                ]
            )
            . $this->Form->control(
                'archive_description.access_rule.start_date',
                [
                    'label' => __("Date de départ du calcul (communicabilité)"),
                    'disabled' => $useParentAccess,
                    'type' => 'text',
                    'append' => $this->Date->picker(
                        '#edit-archive-unit-archive-description-access-rule-start-date'
                    ),
                    'class' => 'datepicker description-access-rule',
                    'placeholder' => __("jj/mm/aaaa"),
                ]
            ),
            ['legend' => __("Restriction d'accès") . ' (' . __("de la description") . ')']
        );
    } else {
        echo $this->Form->control(
            'archive_description.access_rule.access_rule_code_id',
            [
                'type' => 'hidden',
            ]
        );
        echo $this->Form->control(
            'archive_description.access_rule.start_date',
            [
                'type' => 'hidden',
            ]
        );
    }
    echo $this->Html->tag('/div');
}

if ($entity->get('parent_id') !== null) {
    $useParentAccess = $entity->get('access_rule_id')
        === Hash::get($entity, 'parent_archive_unit.access_rule_id');
    $useParentAppraisal = $entity->get('appraisal_rule_id')
        === Hash::get($entity, 'parent_archive_unit.appraisal_rule_id');
    $checkboxAccess = $this->Form->control(
        'use_parent_access_rule',
        [
            'label' => __("Utilise les restrictions d'accès de l'unité d'archives parente"),
            'type' => 'checkbox',
            'val' => $useParentAccess,
            'class' => 'use-parent-checkbox',
            'data-target' => 'access-rule',
        ]
    );
    $checkboxAppraisal = $this->Form->control(
        'use_parent_appraisal_rule',
        [
            'label' => __("Utilise le sort final de l'unité d'archives parente"),
            'type' => 'checkbox',
            'val' => $useParentAppraisal,
            'class' => 'use-parent-checkbox',
            'data-target' => 'appraisal-rule',
        ]
    );
} else {
    $useParentAccess = false;
    $useParentAppraisal = false;
    $checkboxAccess = '';
    $checkboxAppraisal = '';
}

echo $this->Html->tag('h3', __("Règles de gestion"));
echo $this->Html->tag(
    'div',
    $this->Form->fieldset(
        $checkboxAccess
        . $this->Form->control(
            'access_rule.access_rule_code_id',
            [
                'label' => __("Délai de communicabilité"),
                'options' => $codes,
                'required' => true,
                'disabled' => $useParentAccess,
                'class' => 'access-rule',
                'empty' => __("-- Sélectionner un délai --"),
            ]
        )
        . $this->Form->control(
            'access_rule.start_date',
            [
                'label' => __("Date de départ du calcul (communicabilité)"),
                'type' => 'text',
                'append' => $this->Date->picker('#edit-archive-unit-access-rule-start-date'),
                'class' => 'datepicker access-rule',
                'disabled' => $useParentAccess,
                'placeholder' => __("jj/mm/aaaa"),
            ]
        ),
        ['legend' => __("Restriction d'accès")]
    )
    . $this->Form->fieldset(
        $checkboxAppraisal
        . $this->Form->control(
            'appraisal_rule.final_action_code',
            [
                'label' => __("Sort final à appliquer"),
                'disabled' => $useParentAppraisal,
                'class' => 'appraisal-rule',
                'options' => $finals,
                'empty' => __("-- Choisir un sort final --"),
            ]
        )
        . $this->Form->control(
            'appraisal_rule.appraisal_rule_code_id',
            [
                'label' => __("Durée d'utilité administrative"),
                'disabled' => $useParentAppraisal,
                'class' => 'appraisal-rule',
                'options' => $durations,
                'empty' => __("-- Sélectionner une DUA -- "),
            ]
        )
        . $this->Form->control(
            'appraisal_rule.start_date',
            [
                'label' => __("Date de départ du calcul (sort final)"),
                'disabled' => $useParentAppraisal,
                'type' => 'text',
                'append' => $this->Date->picker('#edit-archive-unit-appraisal-rule-start-date'),
                'class' => 'datepicker appraisal-rule',
                'placeholder' => __("jj/mm/aaaa"),
            ]
        ),
        ['legend' => __("Sort final")]
    )
);

echo $this->Form->end();
?>
<script>
    $('#edit-archive-unit-form').accordion({header: "h3", heightStyle: "fill"});

    $('.use-parent-checkbox').change(function () {
        var target = '.' + $(this).attr('data-target');
        $(this).closest('form').find(target).prop('disabled', $(this).is(':checked'));
    });
</script>
