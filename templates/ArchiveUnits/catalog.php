<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Communications"));
$this->Breadcrumbs->add($title = __("Catalogue"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-book', $title))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$targetUrl = '/archive-units/catalog';
require __DIR__ . '/catalog-common.php';
