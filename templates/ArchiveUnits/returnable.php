<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Restitutions"));
$this->Breadcrumbs->add(__("Archives restituables"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-archive', __("Archives restituables")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$addable = true;
$alert = '';
if ($validation_chains_delivery->count() === 0) {
    $alert .= $this->Html->tag('li', __("Circuits de validation des demandes de restitution"));
    $addable = false;
}

$url = $this->getRequest()->getRequestTarget();
$pos = strpos($url, '?');
$buttons = [];
if ($this->Acl->check('/restitution-requests/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-restitution-request', ['size' => 'modal-xxl'])
        ->modal(__("Ajouter une demande de restitution"))
        ->javascriptCallback('afterAddRestitutionRequest')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter une demande de restitution")),
            '/restitution-requests/add' . substr($url, $pos)
        )
        ->generate(
            [
                'type' => 'button',
                'class' => 'btn btn-success',
                'escape' => false,
                'title' => __(
                    "Vous devez sélectionner un service producteur dans "
                    . "les filtres et avoir au moins une unité d'archive pour activer ce bouton"
                ),
                'disabled' => !$this->getRequest()->getQuery('originating_agency_id.0')
                    || $count === 0
                    || $addable === false,
            ]
        );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

if ($alert) {
    echo $this->Html->tag('section.container');
    echo $this->Html->tag('div.alert.alert-warning');
    echo $this->Html->tag('h4', __("Paramétrage manquant"));
    echo $this->Html->tag('ul', $alert);
    echo $this->Html->tag('/div');
    echo $this->Html->tag('/section');
}

/** @var string $tableId */
$jsTableArchives = $this->Table->getJsTableObject($tableId);

$view = $this;
$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalView->create('description-archiveunit', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une archive"))
    ->output('function', 'viewUnitDescription', '/ArchiveUnits/description')
    ->generate();

echo $this->ModalView
    ->create('view-archive-unit-modal', ['size' => 'large'])
    ->modal(__("Visualiser une unité d'archives"))
    ->output(
        'function',
        'actionViewArchiveUnit',
        '/ArchiveUnits/view'
    )
    ->generate();

echo $this->Filter->create('archive-filter')
    ->saves($savedFilters)
    ->permanentFilter(
        'originating_agency_id',
        [
            'label' => __("Service producteur"),
            'options' => $originating_agencies,
            'empty' => __("-- Sélectionner un service producteur --"),
            'templates' => ['inputContainer' => '<div class="form-group {{type}} required">{{content}}</div>'],
        ]
    )
    ->permanentFilter(
        'profile_id',
        [
            'label' => __("Profil d'archives"),
            'options' => $profiles,
            'empty' => __("-- Sélectionner un profil d'archives --"),
        ]
    )
    ->permanentFilter(
        'agreement_id',
        [
            'label' => __("Accords de versement"),
            'options' => $agreements,
            'empty' => __("-- Sélectionner un accord de versement --"),
        ]
    )
    ->filter(
        'archival_agency_identifier',
        [
            'label' => __("Cote"),
            'wildcard',
        ]
    )
    ->filter(
        'transferring_agency_id',
        [
            'label' => __("Service versant"),
            'options' => $transferring_agencies,
            'empty' => __("-- Sélectionner un service versant --"),
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'original_total_size[value]',
        [
            'label' => __("Taille totale des fichiers"),
            'prepend' => $this->Input->operator(
                'original_total_size[operator]',
                '>=',
                ['id' => 'original-total-size-operator']
            ),
            'append' => $this->Input->mult(
                'original_total_size[mult]',
                'mo',
                ['id' => 'original-total-size-mult']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'original_total_count[value]',
        [
            'label' => __("Nombre de fichiers"),
            'prepend' => $this->Input->operator(
                'original_total_count[operator]',
                '>=',
                ['id' => 'original-total-count-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->generateSection();

require 'ajax_returnable.php';
?>
<script>
    AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');

    if (/[?&]choose_originating_agency=/.test(AsalaeGlobal.currentUrl)) {
        $('#originating-agency-id-0').focus().parent().addClass('alert-warning');
    }

    function afterAddRestitutionRequest(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var url = '<?=$this->Url->build('/restitution-requests/index-preparating')?>';
            AsalaeGlobal.interceptedLinkToAjax(url);
        }
    }
</script>
