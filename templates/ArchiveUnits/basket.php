<?php

/**
 * @var Asalae\View\AppView $this
 */
$catalogBasketCount = count($selected_archive_units ?? []);
$title = $this->Html->tag('h2', __("Panier"), ['class' => 'basket-title']);
$toggleBar = $this->Html->tag(
    'div',
    $this->Fa->button(
        ($catalogBasketCount ? 'fa-angle-double-up' : 'fa-angle-double-down')
        . ' text-muted fa-2x',
        __("Déplier les filtres"),
        ['onclick' => 'toggleBasket(this)', 'class' => 'toggle-basket btn-link']
    ),
    ['class' => 'text-center toggle-button']
);

echo $this->ModalForm
    ->create(
        'add-delivery-request',
        [
            'size' => 'large',
            'buttons' => [
                $this->ModalForm->getDefaultCancelButton(),
                $this->ModalForm->getDefaultAcceptButton(),
                $this->Form->button(
                    $this->Fa->i('fa-paper-plane', __("Enregistrer et envoyer")),
                    ['id' => 'btn-send-and-submit-delivery', 'bootstrap-type' => 'primary', 'class' => 'send-submit']
                ),
            ],
        ]
    )
    ->modal(__("Ajouter une demande de communication"))
    ->javascriptCallback('afterAddCommunicationRequest')
    ->output('function', 'addCommunicationRequest', '/delivery-requests/add')
    ->generate();

$buttons = $this->Html->tag(
    'div',
    $this->Html->tag(
        'button',
        $this->Fa->charte(
            'Ajouter',
            __("Ajouter une demande de communication")
        ),
        [
            'id' => 'add-communication-button',
            'class' => 'btn btn-success',
            'type' => 'button',
            'disabled' => !$catalogBasketCount,
            'onclick' => 'addCommunicationRequest()',
        ]
    )
    . $this->Html->tag(
        'button',
        $this->Fa->i(
            'fa-filter',
            __("Filtrer par contenu du panier")
        ),
        [
            'id' => 'basket-filter-button',
            'class' => 'btn btn-default',
            'type' => 'button',
            'disabled' => !$catalogBasketCount,
            'onclick' => 'viewBasket()',
        ]
    ),
    ['class' => 'btn-group']
);

$optionArchiveUnits = [];
foreach ($selected_archive_units ?? [] as $id => $text) {
    $optionArchiveUnits[] = [
        'text' => mb_strlen($text) > 80
            ? mb_substr($text, 0, 80) . '...'
            : $text,
        'title' => $text,
        'value' => $id,
    ];
}
$inputs = $this->Form->control(
    'basket',
    [
        'id' => 'select-basket',
        'label' => __("Liste des unités d'archives à communiquer"),
        'multiple' => true,
        'empty' => __(
            "Panier vide, séléctionner les éléments dans la liste des unités d'archives"
        ),
        'options' => $optionArchiveUnits,
        'val' => array_keys($selected_archive_units ?? []),
    ]
);
$countDiv = $this->Html->tag(
    'p',
    __(
        "Nombre d'unités d'archives dans le panier : {0}",
        $this->Html->tag('span.count', $catalogBasketCount ?: 0)
    )
);
$hiddenDiv = $this->Html->tag(
    'div#catalog-basket',
    $inputs . $countDiv . $buttons,
    ['class' => 'basket hidden-section']
);

$section = $this->Html->tag(
    'section',
    $title
    . $toggleBar
    . $hiddenDiv,
    ['class' => 'bg-white container basket']
);
echo $this->Html->tag(
    'form',
    $section,
    [
        'novalidate',
        'method' => 'get',
        'id' => 'basket-form',
        'class' => 'basket-section-form',
    ]
);
?>
<script>
    function toggleBasket(button) {
        var div = $('#catalog-basket');
        var icon = $(button).find('i');
        if (div.is(':visible')) {
            div.show().slideUp();
            icon.removeClass('fa-angle-double-up')
                .addClass('fa-angle-double-down');
        } else {
            div.hide().slideDown();
            icon.removeClass('fa-angle-double-down')
                .addClass('fa-angle-double-up');
        }
    }

    var selectBasket = $('#select-basket');
    AsalaeGlobal.select2(selectBasket);
    $('#catalog-basket .select2-search__field').css('width', '100%');

    selectBasket.on('change', function () {
        // Suppression d'un élément par désélection dans le select
        var unselected = $(this).find('option:not(:selected):not([value=""])');
        if (unselected.length) {
            unselected.each(function () {
                let archive_unit_id = parseInt($(this).attr('value'));
                let tableUid = $('#<?=$tableId?>').attr('data-table-uid');
                removeFromBasket(TableGenerator.instance[tableUid], archive_unit_id);
            });
        }
    });

    var archive_units = <?=json_encode($optionArchiveUnits)?>;
    if (archive_units.length === 0) {
        $('#catalog-basket').hide();
    }

    function inBasket(archive_unit_id) {
        for (var i = 0; i < archive_units.length; i++) {
            if (archive_units[i].value === archive_unit_id) {
                return true;
            }
        }
        return false;
    }

    function updateBasket() {
        // désactivation des boutons du panier si vide
        $('#add-communication-button, #basket-filter-button').enable(archive_units.length);

        var empty = selectBasket.find('option[value=""]').first().clone();
        var value = selectBasket.val();
        selectBasket.empty().append(empty);
        var val = [];
        archive_units.sort(
            function (a, b) {
                if (a < b) {
                    return -1;
                } else if (a > b) {
                    return 1;
                }
                return 0;
            }
        );
        for (var i = 0; i < archive_units.length; i++) {
            let text = archive_units[i].text;
            let name = text.length > 80 ? text.substring(0, 80) + '...' : text;
            selectBasket.append(
                $('<option>')
                    .attr('value', archive_units[i].value)
                    .attr('title', text)
                    .text(name)
            );
            val.push(archive_units[i].value);
        }
        selectBasket.val(val).trigger('change');
        $('.basket span.count').html(val.length);
        AsalaeCookies.set('catalog_basket', val.join(','));
        setTimeout(() => selectBasket.select2('close'), 10);
        if ((val.length && !selectBasket.is(':visible'))
            || (val.length === 0 && selectBasket.is(':visible'))
        ) {
            $('#basket-form button.toggle-basket').click();
        }
    }

    function addToBasket(table, archive_unit_id) {
        if (!inBasket(archive_unit_id)) {
            var data = table.getDataId(archive_unit_id);
            archive_units.push(
                {
                    value: archive_unit_id,
                    text: data.ArchiveUnits.archival_agency_identifier + ' - ' + data.ArchiveUnits.name
                }
            );
            updateBasket();
            table.generateAll();
        }
    }

    function removeFromBasket(table, archive_unit_id) {
        if (inBasket(archive_unit_id)) {
            for (var i = 0; i < archive_units.length; i++) {
                if (archive_units[i].value === archive_unit_id) {
                    archive_units.splice(i, 1);
                    break;
                }
            }
            updateBasket();
            table.generateAll();
        }
    }

    function viewBasket() {
        var ids = archive_units.map((v) => v.value);
        var url = '<?=$this->Url->build('/archive-units/catalog')?>';
        if (archive_units.length) {
            url += '?all=all&id[0][]=' + ids.join('&id[0][]=');
        }
        AsalaeGlobal.interceptedLinkToAjax(url);
    }

    $('#btn-send-and-submit-delivery').on('click', function (event) {
        event.preventDefault();
        var modal = $(this).closest('.modal');
        var form = modal.find('form');
        var input = $('<input name="send" type="hidden" value="1">');
        form.append(input).trigger('submit');
        setTimeout(() => input.remove(), 10);
    });

    function afterAddCommunicationRequest(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var url = '<?=$this->Url->build('/delivery-requests/index-preparating')?>';
            if (jqXHR.getResponseHeader('X-Asalae-Callback-Send') === 'true') {
                $(window).one(
                    'history.change',
                    function () {
                        var table = $('#delivery-requests-index-preparating-table');
                        table = TableGenerator.instance[table.attr('data-table-uid')];
                        sendDeliveryRequest(table, content.id, true);
                    }
                );
            }
            AsalaeGlobal.interceptedLinkToAjax(url);
        }
    }
</script>
