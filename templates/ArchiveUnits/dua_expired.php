<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Archives"));
$this->Breadcrumbs->add(__("DUA échue"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-calendar-times-o', __("DUA échue")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);
$url = $this->getRequest()->getRequestTarget();
$pos = strpos($url, '?');
$buttons = [];
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

/** @var string $tableId */
$jsTableArchives = $this->Table->getJsTableObject($tableId);

$view = $this;
$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalView->create('description-archiveunit', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une archive"))
    ->output('function', 'viewUnitDescription', '/ArchiveUnits/description')
    ->generate();

echo $this->Filter->create('archive-filter')
    ->saves($savedFilters)
    ->permanentFilter(
        'originating_agency_id',
        [
            'label' => __("Service producteur"),
            'options' => $originating_agencies,
            'empty' => __("-- Sélectionner un service producteur --"),
        ]
    )
    ->permanentFilter(
        'final_action_code',
        [
            'label' => __("Sort final"),
            'options' => $final_action_codes,
            'empty' => __("-- Sélectionner un sort final --"),
        ]
    )
    ->permanentFilter(
        'profile_id',
        [
            'label' => __("Profil d'archives"),
            'options' => $profiles,
            'empty' => __("-- Sélectionner un profil d'archives --"),
        ]
    )
    ->permanentFilter(
        'agreement_id',
        [
            'label' => __("Accords de versement"),
            'options' => $agreements,
            'empty' => __("-- Sélectionner un accord de versement --"),
        ]
    )
    ->filter(
        'final_action_code',
        [
            'label' => __("Sort final"),
            'options' => $final_action_codes,
            'empty' => __("-- Veuillez sélectionner une DUA --"),
        ]
    )
    ->filter(
        'archival_agency_identifier',
        [
            'label' => __("Cote"),
            'wildcard',
        ]
    )
    ->filter(
        'transferring_agency_id',
        [
            'label' => __("Service versant"),
            'options' => $transferring_agencies,
            'empty' => __("-- Sélectionner un service versant --"),
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'original_total_size[value]',
        [
            'label' => __("Taille totale des fichiers"),
            'prepend' => $this->Input->operator(
                'original_total_size[operator]',
                '>=',
                ['id' => 'original-total-size-operator']
            ),
            'append' => $this->Input->mult(
                'original_total_size[mult]',
                'mo',
                ['id' => 'original-total-size-mult']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'original_total_count[value]',
        [
            'label' => __("Nombre de fichiers"),
            'prepend' => $this->Input->operator(
                'original_total_count[operator]',
                '>=',
                ['id' => 'original-total-count-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'transferring_agency_identifier',
        [
            'label' => __("Identifiant d'archive fourni par le service versant"),
            'wildcard',
        ]
    )
    ->filter(
        'originating_agency_identifier',
        [
            'label' => __("Identifiant d'archive fourni par le service producteur"),
            'wildcard',
        ]
    )
    ->filter(
        'oldest_date',
        [
            'label' => __("Date la plus ancienne"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'latest_date',
        [
            'label' => __("Date la plus récente"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->generateSection();

require 'ajax_dua_expired.php';
?>
<script>
    AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    if (/[?&]choose_originating_agency=/.test(AsalaeGlobal.currentUrl)) {
        $('#originating-agency-id-0').focus().parent().addClass('alert-warning');
    }
</script>
