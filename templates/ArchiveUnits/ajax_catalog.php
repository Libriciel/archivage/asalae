<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

$view = $this;
if (!isset($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}

$displayXmlPublicUrl = $displayXmlPublicUrl ?? '/archive-units/display-xml-public';
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'ArchiveUnits.archival_agency_identifier' => [
                'label' => __("Cote"),
                'order' => 'archival_agency_identifier',
                'callback' => 'TableHelper.wordBreak()',
                'filter' => [
                    'archival_agency_identifier[0]' => [
                        'id' => 'filter-archival_agency_identifier-0',
                        'label' => false,
                        'aria-label' => __("Cote"),
                    ],
                ],
            ],
            'ArchiveUnits.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'callback' => 'TableHelper.wordBreak()',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'ArchiveUnits.description' => [
                'label' => __("Description"),
                'display' => false,
            ],
            'ArchiveUnits.archive.transferring_agency.name' => [
                'label' => __("Service versant"),
                'filter' => [
                    'transferring_agency_id[0]' => [
                        'id' => 'filter-transferring_agency_id-0',
                        'label' => false,
                        'aria-label' => __("Service versant"),
                        'options' => $transferring_agencies,
                    ],
                ],
            ],
            'ArchiveUnits.archive.originating_agency.name' => [
                'label' => __("Service producteur"),
                'filter' => [
                    'originating_agency_id[0]' => [
                        'id' => 'filter-originating_agency_id-0',
                        'label' => false,
                        'aria-label' => __("Service producteur"),
                        'options' => $originating_agencies,
                    ],
                ],
            ],
            'ArchiveUnits.archive.profile.name' => [
                'label' => __("Profil d'archive"),
                'filter' => [
                    'profile_id[0]' => [
                        'id' => 'filter-profile_id-0',
                        'label' => false,
                        'aria-label' => __("Profil d'archive"),
                        'options' => $profiles,
                    ],
                ],
            ],
            'ArchiveUnits.archive.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'ArchiveUnits.archive.agreement.name' => [
                'label' => __("Accord de versement"),
                'display' => false,
                'filter' => [
                    'agreement_id[0]' => [
                        'id' => 'filter-agreement_id-0',
                        'label' => false,
                        'aria-label' => __("Accord de versement"),
                        'options' => $agreements,
                    ],
                ],
            ],
            'ArchiveUnits.dates' => [
                'label' => __("Dates extrêmes"),
                'display' => false,
            ],
            'ArchiveUnits.original_total_size' => [
                'label' => __("Taille totale des fichiers"),
                'order' => 'original_total_size',
                'callback' => 'TableHelper.readableBytes',
                'filter' => [
                    'original_total_size[0][value]' => [
                        'id' => 'filter-original_total_size-0',
                        'label' => __("Taille totale des fichiers"),
                        'prepend' => $this->Input->operator(
                            'original_total_size[0][operator]',
                            '>=',
                            ['id' => 'filter-original_total_size-0-operator']
                        ),
                        'append' => $this->Input->mult(
                            'original_total_size[0][mult]',
                            'mo',
                            ['id' => 'filter-original_total_size-0-mult']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'original_total_size[1][value]' => [
                        'id' => 'filter-original_total_size-1',
                        'label' => false,
                        'aria-label' => __("Taille 2"),
                        'prepend' => $this->Input->operator(
                            'original_total_size[1][operator]',
                            '<=',
                            ['id' => 'filter-original_total_size-1-operator']
                        ),
                        'append' => $this->Input->mult(
                            'original_total_size[1][mult]',
                            'mo',
                            ['id' => 'filter-original_total_size-1-mult']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
            'ArchiveUnits.original_total_count' => [
                'label' => __("Nombre de fichiers"),
                'order' => 'original_total_count',
                'display' => false,
                'filter' => [
                    'original_total_count[0][value]' => [
                        'id' => 'filter-original_total_count-0',
                        'label' => __("Nombre de fichiers"),
                        'prepend' => $this->Input->operator(
                            'original_total_count[0][operator]',
                            '>=',
                            ['id' => 'filter-original_total_count-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'original_total_count[1][value]' => [
                        'id' => 'filter-original_total_count-1',
                        'label' => false,
                        'aria-label' => __("Nombre 2"),
                        'prepend' => $this->Input->operator(
                            'original_total_count[1][operator]',
                            '<=',
                            ['id' => 'filter-original_total_count-1-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
            'ArchiveUnits.archive.transfers.0.message_versiontrad' => [
                'label' => __("Version SEDA"),
                'display' => false,
            ],
            'ArchiveUnits.access_rule' => [
                'label' => __("Accessibilité"),
                'display' => false,
                'thead' => [
                    'start_date' => [
                        'label' => __("Date de départ du calcul"),
                        'type' => 'date',
                    ],
                    'end_date' => [
                        'label' => __("Date de fin du calcul"),
                        'type' => 'date',
                    ],
                    'access_rule_code.code' => [
                        'label' => __("Code"),
                    ],
                ],
            ],
            'ArchiveUnits.transferring_agency_identifier' => [
                'label' => __("Identifiant fourni par le service versant"),
                'order' => 'transferring_agency_identifier',
                'display' => false,
            ],
            'ArchiveUnits.max_aggregated_access_end_date' => [
                'label' => __("Date de fin de dérogation"),
                'type' => 'date',
                'display' => false,
                'order' => 'max_aggregated_access_end_date',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'max_aggregated_access_end_date[0]' => [
                        'id' => 'filter-max_aggregated_access_end_date-0',
                        'label' => __("Date de fin de dérogation"),
                        'prepend' => $this->Input->operator('dateoperator_max_aggregated_access_end_date[0]', '>='),
                        'append' => $this->Date->picker('#filter-max_aggregated_access_end_date-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'max_aggregated_access_end_date[1]' => [
                        'id' => 'filter-max_aggregated_access_end_date-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_max_aggregated_access_end_date[1]', '<='),
                        'append' => $this->Date->picker('#filter-max_aggregated_access_end_date-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'ArchiveUnits.id',
            'favorites' => true,
            'sortable' => true,
            'classEval' => 'inBasket(data[{index}].ArchiveUnits.id) ? "success": "default"',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "addToBasket({$jsTable}, {0})",
                'type' => 'button',
                'class' => 'btn-link add-to-basket',
                'label' => $view->Fa->i('fa-square-o'),
                'title' => $title = __("Ajouter au panier"),
                'aria-label' => $title,
                'display' => $this->Acl->check('/delivery-requests/add'),
                'displayEval' => 'disableBasket === false && !inBasket(data[{index}].ArchiveUnits.id)',
                'params' => ['id'],
            ],
            [
                'onclick' => "removeFromBasket({$jsTable}, {0})",
                'type' => 'button',
                'class' => 'btn-link remove-from-basket',
                'label' => $view->Fa->i('fa-check-square-o'),
                'title' => $title = __("Retirer du panier"),
                'aria-label' => $title,
                'display' => $this->Acl->check('/delivery-requests/add'),
                'displayEval' => 'disableBasket === false && inBasket(data[{index}].ArchiveUnits.id)',
                'params' => ['id'],
            ],
            [
                'onclick' => "publicDescriptionArchive({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/description-public'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].ArchiveUnits.archive.description_xml_archive_file.is_large',
                'params' => ['ArchiveUnits.id', 'ArchiveUnits.name'],
            ],
            [
                'href' => $this->Url->build($displayXmlPublicUrl) . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'data-renewable' => true,
                'display' => $this->Acl->check($displayXmlPublicUrl),
                'displayEval' => '!data[{index}].ArchiveUnits.archive.description_xml_archive_file.is_large',
                'params' => ['ArchiveUnits.id', 'ArchiveUnits.name'],
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des unités d'archives"),
        'table' => $table,
    ]
);
