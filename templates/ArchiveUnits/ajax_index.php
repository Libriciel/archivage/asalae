<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

$view = $this;
if (!isset($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'ArchiveUnits.archival_agency_identifier' => [
                'label' => __("Cote"),
                'order' => 'archival_agency_identifier',
                'callback' => 'TableHelper.wordBreak()',
                'filter' => [
                    'archival_agency_identifier[0]' => [
                        'id' => 'filter-archival_agency_identifier-0',
                        'label' => false,
                        'aria-label' => __("Cote"),
                    ],
                ],
            ],
            'ArchiveUnits.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'callback' => 'TableHelper.wordBreak()',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'ArchiveUnits.archive.transferring_agency.name' => [
                'label' => __("Service versant"),
                'filter' => [
                    'transferring_agency_id[0]' => [
                        'id' => 'filter-transferring_agency_id-0',
                        'label' => false,
                        'aria-label' => __("Service versant"),
                        'options' => $transferring_agencies,
                    ],
                ],
            ],
            'ArchiveUnits.archive.originating_agency.name' => [
                'label' => __("Service producteur"),
                'display' => false,
                'filter' => [
                    'originating_agency_id[0]' => [
                        'id' => 'filter-originating_agency_id-0',
                        'label' => false,
                        'aria-label' => __("Service producteur"),
                        'options' => $originating_agencies,
                    ],
                ],
            ],
            'ArchiveUnits.archive.profile.name' => [
                'label' => __("Profil d'archive"),
                'filter' => [
                    'profile_id[0]' => [
                        'id' => 'filter-profile_id-0',
                        'label' => false,
                        'aria-label' => __("Profil d'archive"),
                        'options' => $profiles,
                    ],
                ],
            ],
            'ArchiveUnits.archive.agreement.name' => [
                'label' => __("Accord de versement"),
                'display' => false,
                'filter' => [
                    'agreement_id[0]' => [
                        'id' => 'filter-agreement_id-0',
                        'label' => false,
                        'aria-label' => __("Accord de versement"),
                        'options' => $agreements,
                    ],
                ],
            ],
            'ArchiveUnits.archive.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'ArchiveUnits.dates' => [
                'label' => __("Dates extrêmes"),
                'display' => false,
            ],
            'ArchiveUnits.original_total_size' => [
                'label' => __("Taille totale des fichiers"),
                'order' => 'original_total_size',
                'callback' => 'TableHelper.readableBytes',
                'filter' => [
                    'original_total_size[0][value]' => [
                        'id' => 'filter-original_total_size-0',
                        'label' => __("Taille totale des fichiers"),
                        'prepend' => $this->Input->operator(
                            'original_total_size[0][operator]',
                            '>=',
                            ['id' => 'filter-original_total_size-0-operator']
                        ),
                        'append' => $this->Input->mult(
                            'original_total_size[0][mult]',
                            'mo',
                            ['id' => 'filter-original_total_size-0-mult']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'original_total_size[1][value]' => [
                        'id' => 'filter-original_total_size-1',
                        'label' => false,
                        'aria-label' => __("Taille 2"),
                        'prepend' => $this->Input->operator(
                            'original_total_size[1][operator]',
                            '<=',
                            ['id' => 'filter-original_total_size-1-operator']
                        ),
                        'append' => $this->Input->mult(
                            'original_total_size[1][mult]',
                            'mo',
                            ['id' => 'filter-original_total_size-1-mult']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
            'ArchiveUnits.original_total_count' => [
                'label' => __("Nombre de fichiers"),
                'order' => 'original_total_count',
                'display' => false,
                'filter' => [
                    'original_total_count[0][value]' => [
                        'id' => 'filter-original_total_count-0',
                        'label' => __("Nombre de fichiers"),
                        'prepend' => $this->Input->operator(
                            'original_total_count[0][operator]',
                            '>=',
                            ['id' => 'filter-original_total_count-0-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'original_total_count[1][value]' => [
                        'id' => 'filter-original_total_count-1',
                        'label' => false,
                        'aria-label' => __("Nombre 2"),
                        'prepend' => $this->Input->operator(
                            'original_total_count[1][operator]',
                            '<=',
                            ['id' => 'filter-original_total_count-1-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
            'ArchiveUnits.archive.transfers.0.message_versiontrad' => [
                'label' => __("Version SEDA"),
                'display' => false,
            ],
            'ArchiveUnits.archive_description.history' => [
                'label' => __("Historique"),
                'display' => false,
            ],
            'ArchiveUnits.archive_description.description' => [
                'label' => __("Description"),
                'display' => false,
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'ArchiveUnits.id',
            'favorites' => true,
            'sortable' => true,
            'classEval' => 'catalog_basket.indexOf(""+data[{index}].ArchiveUnits.id) !== -1 ? "success": "default"',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "getNavTree({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fas fa-network-wired'),
                'title' => $title = __("Naviguer dans {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/get-nav-tree'),
                'params' => ['ArchiveUnits.id', 'ArchiveUnits.name'],
            ],
            [
                'onclick' => "actionViewArchiveUnit({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Voir {0}", '{1}'),
                'aria-label' => __("Voir {0}", '{1}'),
                'display' => $this->Acl->check('/archive-units/view'),
                'params' => ['id', 'name'],
            ],
            [
                'onclick' => "viewUnitDescription({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/description'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'href' => $this->Url->build('/archive-units/display-xml') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/display-xml'),
                'displayEval' => '!data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'onclick' => "actionEditArchiveUnit({0})",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Modifier"),
                'display' => $this->Acl->check('/archive-units/edit'),
                'displayEval' => "data[{index}].ArchiveUnits.editable",
                'params' => ['ArchiveUnits.id', 'ArchiveUnits.name'],
            ],
            [
                'type' => 'button',
                'data-callback' => 'archiveUnitFileDownload({0})',
                'confirm' => __("Voulez-vous télécharger les fichiers de cette unité d'archives ?"),
                'label' => $view->Fa->i('fa-file-archive-o'),
                'class' => 'btn-link',
                'title' => $title = __("Télécharger les fichiers de {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Télécharger le zip"),
                'display' => $this->Acl->check('/archives/downloadFiles'),
                'displayEval' => "data[{index}].ArchiveUnits.state === 'available' "
                    . "|| data[{index}].ArchiveUnits.state === 'freezed'",
                'params' => ['ArchiveUnits.id', 'ArchiveUnits.name'],
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des unités d'archives"),
        'table' => $table,
    ]
);
