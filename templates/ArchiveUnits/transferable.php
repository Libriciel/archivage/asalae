<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Transferts sortants"));
$this->Breadcrumbs->add($title = __("Archives transférables"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-archive', $title))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$addable = true;
$alert = '';
if ($validation_chains_outgoing_transfer->count() === 0) {
    $alert .= $this->Html->tag('li', __("Circuits de validation des demandes de transferts sortants"));
    $addable = false;
}
if ($archivingSystemsCount === 0) {
    $alert .= $this->Html->tag('li', __("Système d'archivage électronique distant"));
    $addable = false;
}

$url = $this->getRequest()->getRequestTarget();
$pos = strpos($url, '?');
$buttons = [];
if ($this->Acl->check('/outgoing-transfer-requests/add1')) {
    $filters = ($pos ? substr($url, $pos) : '');
    $buttons[] = $this->ModalForm
        ->create('add-outgoing-transfer-request', ['size' => 'modal-xxl'])
        ->modal(__("Ajouter une demande de transfert sortant"))
        ->step('/outgoing-transfer-requests/add1' . $filters, 'addOutgoingTransferRequestStep1')
        ->step('/outgoing-transfer-requests/add2' . $filters, 'addOutgoingTransferRequestStep2')
        ->step('/outgoing-transfer-requests/add3' . $filters, 'addOutgoingTransferRequestStep3')
        ->javascriptCallback('afterAddOutgoingTransferRequest')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter une demande de transfert sortant"))
        )
        ->generate(
            [
                'type' => 'button',
                'class' => 'btn btn-success',
                'escape' => false,
                'disabled' => $count === 0
                    || $addable === false,
            ]
        );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

if ($alert) {
    echo $this->Html->tag('section.container');
    echo $this->Html->tag('div.alert.alert-warning');
    echo $this->Html->tag('h4', __("Paramétrage manquant"));
    echo $this->Html->tag('ul', $alert);
    echo $this->Html->tag('/div');
    echo $this->Html->tag('/section');
}

/** @var string $tableId */
$jsTableArchives = $this->Table->getJsTableObject($tableId);

$view = $this;
$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalView->create('description-archiveunit', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une archive"))
    ->output('function', 'viewUnitDescription', '/ArchiveUnits/description')
    ->generate();

echo $this->ModalView
    ->create('view-archive-unit-modal', ['size' => 'large'])
    ->modal(__("Visualiser une unité d'archives"))
    ->output(
        'function',
        'actionViewArchiveUnit',
        '/ArchiveUnits/view'
    )
    ->generate();

echo $this->Filter->create('archive-filter')
    ->saves($savedFilters)
    ->permanentFilter(
        'final_action_code',
        [
            'label' => __("Sort final"),
            'options' => $final_action_codes,
            'empty' => __("-- Veuillez sélectionner une DUA --"),
            'data-group' => __("Sort Final"),
        ]
    )
    ->permanentFilter(
        'appraisal_rule_end',
        [
            'label' => __("Statut de la DUA"),
            'options' => [
                '<' => __("Terminé"),
                '>=' => __("En cours"),
            ],
            'empty' => __("-- Veuillez sélectionner une DUA --"),
            'data-group' => __("DUA"),
        ]
    )
    ->permanentFilter(
        'agreement_id',
        [
            'label' => __("Accords de versement"),
            'options' => $agreements,
            'empty' => __("-- Sélectionner un accord de versement --"),
        ]
    )
    ->permanentFilter(
        'originating_agency_id',
        [
            'label' => __("Service producteur"),
            'options' => $originating_agencies,
            'empty' => __("-- Sélectionner un service producteur --"),
        ]
    )
    ->filter(
        'profile_id',
        [
            'label' => __("Profil d'archives"),
            'options' => $profiles,
            'empty' => __("-- Sélectionner un profil d'archives --"),
        ]
    )
    ->filter(
        'archival_agency_identifier',
        [
            'label' => __("Cote"),
            'wildcard',
        ]
    )
    ->filter(
        'transferring_agency_id',
        [
            'label' => __("Service versant"),
            'options' => $transferring_agencies,
            'empty' => __("-- Sélectionner un service versant --"),
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'original_total_size[value]',
        [
            'label' => __("Taille totale des fichiers"),
            'prepend' => $this->Input->operator(
                'original_total_size[operator]',
                '>=',
                ['id' => 'original-total-size-operator']
            ),
            'append' => $this->Input->mult(
                'original_total_size[mult]',
                'mo',
                ['id' => 'original-total-size-mult']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'original_total_count[value]',
        [
            'label' => __("Nombre de fichiers"),
            'prepend' => $this->Input->operator(
                'original_total_count[operator]',
                '>=',
                ['id' => 'original-total-count-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->generateSection();

require 'ajax_transferable.php';
?>
<script>
    AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');

    function afterAddOutgoingTransferRequest(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Step')) {
            AsalaeModal.stepCallback(content, textStatus, jqXHR);
        } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var url = '<?=$this->Url->build('/outgoing-transfer-requests/index-preparating')?>';
            AsalaeGlobal.interceptedLinkToAjax(url);
        }
    }
</script>
