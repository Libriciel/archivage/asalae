<?php

/**
 * @var Asalae\View\AppView $this
 */

/** @var string $tableId */

use Cake\Core\Configure;

$view = $this;
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'ArchiveUnits.archival_agency_identifier' => [
                'label' => __("Cote"),
                'order' => 'archival_agency_identifier',
                'callback' => 'TableHelper.wordBreak()',
                'filter' => [
                    'archival_agency_identifier[0]' => [
                        'id' => 'filter-archival_agency_identifier-0',
                        'label' => false,
                        'aria-label' => __("Cote"),
                    ],
                ],
            ],
            'ArchiveUnits.name' => [
                'label' => __("Nom"),
                'callback' => 'TableHelper.wordBreak()',
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'ArchiveUnits.archive_description.description' => [
                'label' => __("Description"),
                'display' => false,
            ],
            'ArchiveUnits.archive.transferring_agency.name' => [
                'label' => __("Service versant"),
                'filter' => [
                    'transferring_agency_id[0]' => [
                        'id' => 'filter-transferring_agency_id-0',
                        'label' => false,
                        'aria-label' => __("Service versant"),
                        'options' => $transferring_agencies,
                    ],
                ],
            ],
            'ArchiveUnits.archive.originating_agency.name' => [
                'label' => __("Service producteur"),
                'display' => false,
                'filter' => [
                    'originating_agency_id[0]' => [
                        'id' => 'filter-originating_agency_id-0',
                        'label' => false,
                        'aria-label' => __("Service producteur"),
                        'options' => $originating_agencies,
                    ],
                ],
            ],
            'ArchiveUnits.archive.profile.name' => [
                'label' => __("Profil d'archive"),
                'filter' => [
                    'profile_id[0]' => [
                        'id' => 'filter-profile_id-0',
                        'label' => false,
                        'aria-label' => __("Profil d'archives"),
                        'options' => $profiles,
                    ],
                ],
            ],
            'ArchiveUnits.archive.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'ArchiveUnits.archive.agreement.name' => [
                'label' => __("Accord de versement"),
                'filter' => [
                    'agreement_id[0]' => [
                        'id' => 'filter-agreement_id-0',
                        'label' => false,
                        'aria-label' => __("Accords de versement"),
                        'options' => $agreements,
                    ],
                ],
            ],
            'ArchiveUnits.dates' => [
                'label' => __("Dates extrêmes"),
                'display' => false,
            ],
            'ArchiveUnits.original_total_size' => [
                'label' => __("Taille totale des fichiers"),
                'order' => 'original_total_size',
                'callback' => 'TableHelper.readableBytes',
                'filter' => [
                    'original_total_size[0][value]' => [
                        'id' => 'filter-original_total_size-0',
                        'label' => __("Taille totale des fichiers"),
                        'prepend' => $this->Input->operator(
                            'original_total_size[0][operator]',
                            '>=',
                            ['id' => 'filter-original_total_size-0-operator']
                        ),
                        'append' => $this->Input->mult(
                            'original_total_size[0][mult]',
                            'mo',
                            ['id' => 'filter-original_total_size-0-mult']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'original_total_size[1][value]' => [
                        'id' => 'filter-original_total_size-1',
                        'label' => false,
                        'aria-label' => __("Taille 2"),
                        'prepend' => $this->Input->operator(
                            'original_total_size[1][operator]',
                            '<=',
                            ['id' => 'filter-original_total_size-1-operator']
                        ),
                        'append' => $this->Input->mult(
                            'original_total_size[1][mult]',
                            'mo',
                            ['id' => 'filter-original_total_size-1-mult']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
            'ArchiveUnits.original_total_count' => [
                'label' => __("Nombre de fichiers"),
                'order' => 'original_total_count',
                'display' => false,
                'filter' => [
                    'original_total_count[0][value]' => [
                        'id' => 'filter-original_total_count-0',
                        'label' => __("Nombre de fichiers"),
                        'prepend' => $this->Input->operator(
                            'original_total_count[0][operator]',
                            '>=',
                            ['id' => 'filter-original_total_count-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'original_total_count[1][value]' => [
                        'id' => 'filter-original_total_count-1',
                        'label' => false,
                        'aria-label' => __("Nombre 2"),
                        'prepend' => $this->Input->operator(
                            'original_total_count[1][operator]',
                            '<=',
                            ['id' => 'filter-original_total_count-1-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
            'ArchiveUnits.appraisal_rule.end_date' => [
                'label' => __("Date de fin de DUA"),
                'type' => 'date',
                'order' => 'dua_end',
                'display' => false,
            ],
            'ArchiveUnits.appraisal_rule.final_action_codetrad' => [
                'label' => __("Sort final"),
                'display' => false,
                'filter' => [
                    'final_action_code[0]' => [
                        'id' => 'filter-final-action-code-0',
                        'label' => false,
                        'aria-label' => __("Sort final"),
                        'options' => $final_action_codes,
                    ],
                ],
            ],
            'ArchiveUnits.access_rule.access_rule_code.code' => [
                'label' => __("Code de restriction d'accès"),
                'display' => false,
            ],
            'ArchiveUnits.archive.transfers.0.message_versiontrad' => [
                'label' => __("Version SEDA"),
                'display' => false,
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'ArchiveUnits.id',
            'favorites' => true,
            'sortable' => true,
            'classEval' => 'data[{index}].state === "destroying" ? "danger" : "available"',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewUnitDescription({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/dua-expired'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].ArchiveUnits.archive.description_xml_archive_file.is_large',
                'params' => ['ArchiveUnits.id', 'ArchiveUnits.name'],
            ],
            [
                'href' => $this->Url->build('/archive-units/display-xml') . '/{0}',
                'target' => '_blank',
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/display-xml'),
                'displayEval' => '!data[{index}].ArchiveUnits.archive.description_xml_archive_file.is_large',
                'params' => ['ArchiveUnits.id', 'ArchiveUnits.name'],
            ],
        ]
    );

$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => $titleTable,
        'table' => $table,
    ]
);
