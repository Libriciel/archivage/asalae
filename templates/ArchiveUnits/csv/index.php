<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Csv->resultTable(
    [
        'archival_agency_identifier' => __("Cote"),
        'name' => __("Nom"),
        'archive.transferring_agency.name' => __("Service versant"),
        'archive.originating_agency.name' => __("Service producteur"),
        'archive.profile.name' => __("Profil d'archive"),
        'archive.agreement.name' => __("Accord de versement"),
        'archive.created' => __("Date de création"),
        'dates' => __("Dates extrêmes"),
        'original_total_size_readable' => __("Taille totale des fichiers"),
        'original_total_size' => __("Taille totale des fichiers (octets)"),
        'original_total_count' => __("Nombre de fichiers"),
        'archive.transfers.0.message_versiontrad' => __("Version SEDA"),
        'archive_description.history' => __("Historique"),
        'archive_description.description' => __("Description"),
    ],
    $results
);
