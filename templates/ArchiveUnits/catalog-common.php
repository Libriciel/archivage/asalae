<?php

/**
 * @var Asalae\View\AppView $this
 */

$url = $this->getRequest()->getRequestTarget();
$pos = strpos($url, '?');
$params = $pos ? substr($url, $pos) : '';

$alert = '';
if ($validation_delivery_request->count() === 0) {
    $alert .= $this->Html->tag('li', __("Circuits de validation des demandes de communication"));
    $addable = false;
}
if ($alert) {
    echo $this->Html->tag('section.container');
    echo $this->Html->tag('div.alert.alert-warning');
    echo $this->Html->tag('h4', __("Paramétrage manquant"));
    echo $this->Html->tag('ul', $alert);
    echo $this->Html->tag('/div');
    echo $this->Html->tag('/section');
}
echo '<script> var disableBasket = ' . ($alert ? 'true' : 'false') . '; </script>';

$buttons = [];
if ($this->Acl->check('/delivery-requests/add')) {
    $buttons[] = $this->Html->tag(
        'button',
        $this->Fa->charte(
            'Ajouter',
            __(
                "Ajouter une demande de communication ({0} unités d'archives)",
                $resultCount
            )
        ),
        [
            'class' => 'btn btn-success' . ($params ? '' : ' disabled'),
            'type' => 'button',
            'onclick' => $params
                ? 'addCommunicationRequestByFilters()'
                : 'return false',
            'title' => __("Pour activer ce bouton, ajouter un filtre de recherche"),
        ]
    );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->SearchEngine->form(
    null,
    ['id' => 'catalog-searchbar-form']
);
echo $this->Form->control(
    'all',
    [
        'type' => 'hidden',
        'value' => 'all',
    ]
);
echo $this->SearchEngine->input(
    'search',
    ['id' => 'catalog-searchbar']
);
echo $this->Form->end();

$jsTable = $this->Table->getJsTableObject($tableId);

$view = $this;
$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalView->create('search-public-description-archive', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une unité d'archive"))
    ->output('function', 'publicDescriptionArchive', '/ArchiveUnits/description-public')
    ->generate();

if ($this->Acl->check('/delivery-requests/add')) {
    include 'basket.php';
}

echo $this->Filter->create('catalog-filters')
    ->permanentFilter(
        'all',
        [
            'id' => 'display-all-archive-unit',
            'label' => __("Affichage"),
            'options' => [
                'all' => __("Afficher toutes les unités d'archives"),
            ],
            'empty' => __("Afficher uniquement les archives"),
        ]
    )
    ->permanentFilter(
        'profile_id',
        [
            'label' => __("Profil d'archives"),
            'options' => $profiles,
            'empty' => __("-- Sélectionner un profil d'archives --"),
            'val' => $this->getRequest()->getQuery('profile_id'),
        ]
    )
    ->permanentFilter(
        'agreement_id',
        [
            'label' => __("Accord de versement"),
            'options' => $agreements,
            'empty' => __("-- Sélectionner un accord de versement --"),
            'val' => $this->getRequest()->getQuery('agreement_id'),
        ]
    )
    ->permanentFilter(
        'transferring_agency_id',
        [
            'label' => __("Service versant"),
            'options' => $transferring_agencies,
            'empty' => __("-- Sélectionner un service versant --"),
            'val' => $this->getRequest()->getQuery('transferring_agency_id'),
        ]
    )
    ->permanentFilter(
        'originating_agency_id',
        [
            'label' => __("Service producteur"),
            'options' => $originating_agencies,
            'empty' => __("-- Sélectionner un service producteur --"),
            'val' => $this->getRequest()->getQuery('originating_agency_id'),
        ]
    )
    ->filter(
        'id',
        [
            'hidden' => true,
        ]
    )
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'archival_agency_identifier',
        [
            'label' => __("Cote"),
            'wildcard',
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'original_total_size[value]',
        [
            'label' => __("Taille totale des fichiers"),
            'prepend' => $this->Input->operator(
                'original_total_size[operator]',
                '>=',
                ['id' => 'original-total-size-operator']
            ),
            'append' => $this->Input->mult(
                'original_total_size[mult]',
                'mo',
                ['id' => 'original-total-size-mult']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'original_total_count[value]',
        [
            'label' => __("Nombre de fichiers"),
            'prepend' => $this->Input->operator(
                'original_total_count[operator]',
                '>=',
                ['id' => 'original-total-count-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'keyword',
        [
            'label' => __("Mot clé"),
            'data-callback' => 'searchKeywordPopup',
        ]
    )
    ->filter(
        'filename',
        [
            'label' => __("Nom de fichier"),
            'wildcard',
        ]
    )
    ->filter(
        'transferring_agency_identifier',
        [
            'label' => __("Identifiant d'archive fourni par le service versant"),
            'wildcard',
        ]
    )
    ->filter(
        'originating_agency_identifier',
        [
            'label' => __("Identifiant d'archive fourni par le service producteur"),
            'wildcard',
        ]
    )
    ->filter(
        'oldest_date',
        [
            'label' => __("Date la plus ancienne"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'latest_date',
        [
            'label' => __("Date la plus récente"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'keyword_reference',
        [
            'label' => __("Identifiant du mot clé dans un référentiel donné"),
            'wildcard',
        ]
    )
    ->filter(
        'max_aggregated_access_end_date',
        [
            'label' => __("Date de fin de dérogation"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->saves($savedFilters)
    ->generateSection();

require 'ajax_catalog.php';

echo $this->SearchEngine->script(
    'catalog-searchbar',
    $this->Url->build($targetUrl),
    $this->Url->build('/archive-units/search-bar'),
    [
        __("Cote") => 'archival_agency_identifier',
        __("Nom") => 'name',
        __("Description") => 'archive_description.description',
        __("Historique") => 'archive_description.history',
        __("Mots-clés") => 'archive_keywords.{n}.content',
    ]
);
?>
<script>
    AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');

    /**
     * Affichage des filtres supplémentaires
     */
    function toggleFilters(button) {
        var div = $('#catalog-filters');
        var icon = $(button).find('i');
        if (div.is(':visible')) {
            div.show().slideUp();
            icon.removeClass('fa-angle-double-up')
                .addClass('fa-angle-double-down');
        } else {
            div.hide().slideDown();
            icon.removeClass('fa-angle-double-down')
                .addClass('fa-angle-double-up');
        }
    }

    function addCommunicationRequestByFilters() {
        addCommunicationRequest('<?=$params?>');
    }
</script>
