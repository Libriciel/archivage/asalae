<?php

/**
 * @var Asalae\View\AppView $this
 * @var                     $entity Asalae\Model\Entity\Role
 */
echo $this->Form->create($entity, ['idPrefix' => 'edit-role']);
require 'addedit-common.php';
echo $this->Form->end();
?>
<script>
    AsalaeGlobal.select2($('#edit-role-type-entities-ids'), __("Rechercher"));
</script>
