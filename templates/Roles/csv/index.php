<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Csv->resultTable(
    [
        'path' => __("Path permission"),
        'traduction' => __("Traduction"),
        'controller' => __("Controlleur"),
        'action' => __("Action"),
        'type' => __("Type"),
        'group' => __("Groupe"),
        // permission pour chaque roles globaux
        'Archiviste' => 'Archiviste',
        'Contrôleur' => 'Contrôleur',
        'Demandeur' => 'Demandeur',
        "Opérateur d'archivage" => "Opérateur d'archivage",
        'Producteur' => 'Producteur',
        'Référent Archives' => 'Référent Archives',
        'Versant' => 'Versant',
        'Super archiviste' => 'Super archiviste',
        'Référent technique' => 'Référent technique',
        'ws administrateur' => 'ws administrateur',
        'ws requêteur' => 'ws requêteur',
        'ws versant' => 'ws versant',
    ],
    $results
);
