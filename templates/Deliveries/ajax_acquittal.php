<?php

/**
 * @var Asalae\View\AppView $this
 */

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Deliveries.identifier' => [
                'label' => __("Identifiant"),
            ],
            'Deliveries.comment' => [
                'label' => __("Commentaire"),
            ],
            'Deliveries.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
            ],
            'Deliveries.delivery_request.archive_units' => [
                'label' => __("Unités d'archives"),
                'callback' => 'TableHelper.ul("name")',
                'escape' => false,
            ],
            'Deliveries.statetrad' => [
                'label' => __("Etat"),
            ],
            'Deliveries.archive_units_count' => [
                'label' => __("Nombre d'unités d'archives"),
                'display' => false,
            ],
            'Deliveries.original_size' => [
                'label' => __("Taille total des fichiers"),
                'display' => false,
                'callback' => 'TableHelper.readableBytes',
            ],
            'Deliveries.delivery_request.identifier' => [
                'label' => __("Demande de comm."),
                'display' => false,
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'Deliveries.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewDelivery({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/deliveries/view'),
                'params' => ['Deliveries.id', 'Deliveries.identifier'],
            ],
            [
                'data-callback' => "acquitDelivery({0})",
                'confirm' => __(
                    "En cliquant sur OK, vous déclarez avoir récupéré la communication et déclenchez sa suppression."
                ),
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-thumbs-up'),
                'title' => $title = __("Acquitter {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/deliveries/acquit'),
                'params' => ['Deliveries.id', 'Deliveries.identifier'],
            ],
        ]
    );

$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des communications"),
        'table' => $table,
    ]
);
