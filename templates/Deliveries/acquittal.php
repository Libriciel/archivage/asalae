<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Communications"));
$this->Breadcrumbs->add($h1 = __("Acquittements des communications"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-thumbs-up', $h1))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

echo $this->element('modal', ['idTable' => $tableId]);

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}

echo $this->ModalView->create('acquittal-view-delivery', ['size' => 'large'])
    ->modal(__("Visualisation d'une communication"))
    ->output('function', 'viewDelivery', '/deliveries/view')
    ->generate();

require 'ajax_acquittal.php';
?>
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    function acquitDelivery(id) {
        var table = {};
        table = <?=$jsTable?>;
        var tr = $(table.table).find('tr[data-id="' + id + '"]');
        tr.find('button').addClass('deleting').addClass('gray').addClass('disabled').disable();
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/deliveries/acquit')?>/' + id,
            method: 'DELETE',
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                if (content.report === 'done') {
                    table.removeDataId(id);
                    tr.fadeOut(400, function () {
                        $(this).remove();
                        table.decrementPaginator();
                    });
                } else {
                    alert(content.report);
                    this.error();
                }
            },
            error: function (e) {
                console.error(e);
                tr.addClass('danger');
            }
        })
    }
</script>
