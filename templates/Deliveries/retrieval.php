<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Communications"));
$this->Breadcrumbs->add($h1 = __("Récupération des communications"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-mail-reply', $h1))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

echo $this->element('modal', ['idTable' => $tableId]);

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}

echo $this->ModalView->create('retrieval-view-delivery', ['size' => 'large'])
    ->modal(__("Visualisation d'une communication"))
    ->output('function', 'viewDelivery', '/deliveries/view')
    ->generate();

echo $this->ModalView->create('search-public-description-archive', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une unité d'archive"))
    ->output('function', 'publicDescriptionArchive', '/ArchiveUnits/description-public')
    ->generate();

require 'ajax_retrieval.php';
?>
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    function changeDeliveryState(id) {
        setTimeout(function () {
            AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
        }, 100);
    }
</script>
