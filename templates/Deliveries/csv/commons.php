<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$map = function (EntityInterface $entity): EntityInterface {
    $archiveUnits = [];
    /** @var EntityInterface $deliveryRequest */
    $deliveryRequest = Hash::get($entity, 'delivery_request');
    foreach (Hash::get($deliveryRequest, 'archive_units', []) as $archiveUnit) {
        $archiveUnits[] = Hash::get($archiveUnit, 'name');
    }
    $deliveryRequest->set(
        'archive_units',
        $archiveUnits ? '- ' . implode("\n- ", $archiveUnits) : ''
    );
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'identifier' => __("Identifiant"),
        'comment' => __("Commentaire"),
        'created' => __("Date de création"),
        'delivery_request.archive_units' => __("Unités d'archives"),
        'statetrad' => __("Etat"),
    ],
    $results,
    $map
);
