<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $entity
 */

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($entity->get('identifier')));
$infos .= $this->Html->tag('p', h($entity->get('comment')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Communication"));
$infos .= $this->ViewTable->generate(
    $entity,
    [
        __("Identifiant") => 'identifier',
        __("Commentaire") => 'comment|nl2br',
        __("Etat") => 'statetrad',
        __("Nombre total d'unités d'archives") => 'archive_units_count',
        __("Nombre total de fichiers") => 'original_count',
        __("Taille totale des fichiers") => 'original_size|toReadableSize',
    ]
);
$infos .= $this->Html->tag('/article');

$tabs = $this->Tabs->create('tabs-view-delivery', ['class' => 'row no-padding']);
$tabs->add(
    'tab-view-delivery-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);

$tableUnits = $this->Table
    ->create('view-delivery-archive-units', ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'deliveries',
            'action' => 'paginate-archive-units',
            $entity->id,
            '?' => [
                'sort' => 'name',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'archival_agency_identifier' => [
                'label' => __("Identifiant"),
            ],
            'name' => [
                'label' => __("Nom"),
            ],
            'description' => [
                'label' => __("Description"),
            ],
            'dates' => [
                'label' => __("Dates extrêmes"),
            ],
            'original_total_count' => [
                'label' => __("Nombre de fichiers"),
            ],
            'original_total_size' => [
                'label' => __("Taille des fichiers"),
                'callback' => 'TableHelper.readableBytes',
            ],
        ]
    )
    ->data(Hash::get($entity, 'delivery_request.archive_units', []) ?: [])
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "publicDescriptionArchive({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/description-public'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'href' => $this->Url->build('/archive-units/display-xml-public') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'data-renewable' => true,
                'display' => $this->Acl->check('/archive-units/display-xml-public'),
                'displayEval' => '!data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
        ]
    );
$tabs->add(
    'tab-view-delivery-archive-units',
    $this->Fa->i('fa-file-code-o', __("Unités d'archives")),
    $this->AjaxPaginator->create('pagination-view-delivery-archive-units')
        ->url($url)
        ->table($tableUnits)
        ->count($unitsCount)
        ->generateTable()
);
echo $tabs;
