<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>

<ArchiveDeliveryRequestReply xmlns="fr:gouv:culture:archivesdefrance:seda:v2.1">
    <?= $comment ? "    <Comment>$comment</Comment>" . PHP_EOL : '' ?>
    <Date><?= $date ?></Date>
    <MessageIdentifier><?= $identifier ?></MessageIdentifier>
    <CodeListVersions></CodeListVersions>
    <MessageRequestIdentifier><?= $request_identifier ?></MessageRequestIdentifier>
    <?= '    <UnitIdentifier>' . implode(
        "</UnitIdentifier>\n    <UnitIdentifier>",
        $archiveUnits
    ) . '</UnitIdentifier>' . PHP_EOL ?>
    <ArchivalAgency>
        <Identifier><?= $archivalAgency ?></Identifier>
    </ArchivalAgency>
    <Requester>
        <Identifier><?= $requester ?></Identifier>
    </Requester>
</ArchiveDeliveryRequestReply>
