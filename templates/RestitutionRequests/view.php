<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $restitutionRequest
 */

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

?>
<script>
    function validationActionToIcon(value) {
        switch (value) {
            case 'validate':
                return '<?=$this->Fa->i('fa-check text-success')?>';
            case 'invalidate':
                return '<?=$this->Fa->i('fa-times text-danger')?>';
            case 'stepback':
                return '<?=$this->Fa->i('fa-reply')?>';
            default:
                return value;
        }
    }
</script>
<?php
$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($restitutionRequest->get('identifier')));
$infos .= $this->Html->tag('p', h($restitutionRequest->get('comment')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Demande de restitution"));
$infos .= $this->ViewTable->generate(
    $restitutionRequest,
    [
        __("Identifiant") => 'identifier',
        __("Etat") => 'statetrad',
        __("Commentaire") => 'comment|nl2br',
        __("Entité producteur") => 'originating_agency.name',
        __("Agent demandeur") => 'created_user.username',
        __("Nombre d'unités d'archives") => 'archive_units_count',
        __("Fichiers d'unités d'archives") => 'original_count',
        __("Taille des unités d'archives") => '{original_size|toReadableSize} ({original_size} Octets)',
        __("Attestation de restitution") => function ($rest) {
            return $rest->get('restitution_certified') ? __("générée") : __("non générée");
        },
        __("Attestation d'élimination") => function ($rest) {
            $certified = $rest->get('destruction_certified');
            if ($certified) {
                return __("générée");
            } elseif ($certified === false) {
                return __("intérmédiaire générée");
            }
            return __("non générée");
        },
        __("Date de création") => 'created',
        __("Date de modification") => 'modified',
    ]
);
$infos .= $this->Html->tag('/article');

$tabs = $this->Tabs->create('tabs-view-restitution-request', ['class' => 'row no-padding']);
$tabs->add(
    'tab-view-restitution-request-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);

$tableUnits = $this->Table
    ->create('view-restitution-request-archive-units', ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'restitution-requests',
            'action' => 'paginate-archive-units',
            $restitutionRequest->id,
            '?' => [
                'sort' => 'name',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'archival_agency_identifier' => [
                'label' => __("Identifiant"),
            ],
            'name' => [
                'label' => __("Nom"),
            ],
            'description' => [
                'label' => __("Description"),
            ],
            'dates' => [
                'label' => __("Dates extrêmes"),
            ],
            'original_total_count' => [
                'label' => __("Nombre de fichiers"),
            ],
            'original_total_size' => [
                'label' => __("Taille des fichiers"),
                'callback' => 'TableHelper.readableBytes',
            ],
        ]
    )
    ->data($restitutionRequest->get('archive_units') ?: [])
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewUnitDescription({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/description'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'href' => $this->Url->build('/archive-units/display-xml') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/display-xml'),
                'displayEval' => '!data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
        ]
    );
$tabs->add(
    'tab-view-restitution-request-archive-units',
    $this->Fa->i('fa-file-code-o', __("Unités d'archives")),
    $this->AjaxPaginator->create('pagination-restitution-request-archive-units')
        ->url($url)
        ->table($tableUnits)
        ->count($unitsCount)
        ->generateTable()
);

/** @var EntityInterface $process */
$process = Hash::get($restitutionRequest, 'validation_processes.0');
if ($process) {
    $histories = $this->Html->tag('h4', __("Historique de validation"));
    /** @var EntityInterface $history */
    foreach (Hash::get($process, 'validation_histories') as $history) {
        $history->setVirtual(['actor_info']);
    }
    $histories .= $this->Table
        ->create('validation-histories', ['class' => 'table table-striped table-hover smart-td-size'])
        ->fields(
            [
                'action' => [
                    'label' => __("Décision"),
                    'callback' => 'validationActionToIcon',
                    'titleEval' => 'actiontrad',
                ],
                'validation_actor.validation_stage.name' => [
                    'label' => __("Etape de validation"),
                ],
                'actor_info' => [
                    'label' => __("Auteur de la validation"),
                    'escape' => false,
                ],
                'created' => [
                    'label' => __("Date"),
                    'type' => 'datetime',
                ],
                'comment' => [
                    'label' => __("Commentaire"),
                ],
            ]
        )
        ->data(Hash::get($process, 'validation_histories'))
        ->params(
            [
                'identifier' => 'id',
            ]
        );

    $info = [
        __("Etat de la validation") => 'processedtrad',
        __("Décision finale") => 'validatedtrad',
        __("Circuit de validation") => 'validation_chain.name',
    ];
    if (!$process->get('processed')) {
        $info[__("Etape de validation")] = 'steptrad';
        $info[__("Doit être validé par")] = 'current_stage.validation_actors.{n}.actor_info|raw';
    }

    $tabs->add(
        'tab-restitution-request-validation',
        $this->Fa->i('fa-check-square-o', __("Validation")),
        // ---------------------------------------------------------------------
        $this->Html->tag('h3', __("Validation de {0}", h($restitutionRequest->get('identifier'))))
        . $this->ViewTable->generate($process, $info)
        . $histories
    );
}

echo $tabs;
