<?php

/**
 * @var Asalae\View\AppView $this
 */

$idPrefix = 'restitution-request-add';
$jsTableId = 'add-restitution-request-archive-units';
$paginatorId = 'pagination-add-restitution-request-archive-units';
$modalId = 'add-restitution-request';

require 'addedit-common.php'; // Form dans le common
?>
<script>
    // suppression de la demande si on appuis sur annuler
    $('#<?=$modalId?>').find('.modal-footer button.cancel')
        .off('.delete-add')
        .on('click.delete-add', function () {
            $.ajax({
                url: '<?=$this->Url->build('/restitution-requests/delete/' . $entity->id)?>',
                method: 'DELETE'
            });
        });
</script>
