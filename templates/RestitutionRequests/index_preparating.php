<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Restitutions"));
$this->Breadcrumbs->add($h1 = __("Mes demandes de restitution en préparation"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-pencil-square-o', $h1))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

echo $this->Html->tag('section.container');
echo $this->Html->tag(
    'div.alert.alert-info',
    __(
        "Les demandes de restitution sont ajoutées dans le menu {0} 
        en sélectionnant un service producteur.",
        $this->Acl->check('/archive-units/returnable')
            ? $this->Html->link(
                __("Archives restituables"),
                '/archive-units/returnable?choose_originating_agency=true&show-filters=true'
            ) : __("Archives restituables")
    )
);
echo $this->Html->tag('/section');

require 'index-common.php';
