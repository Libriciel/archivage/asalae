<?php

/**
 * @var Asalae\View\AppView $this
 */

$idPrefix = 'restitution-request-edit';
$jsTableId = 'edit-restitution-request-archive-units';
$paginatorId = 'pagination-edit-restitution-request-archive-units';
$modalId = 'edit-restitution-request';

require 'addedit-common.php'; // Form dans le common
