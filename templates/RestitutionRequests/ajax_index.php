<?php

/**
 * @var Asalae\View\AppView $this
 */

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'RestitutionRequests.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'RestitutionRequests.comment' => [
                'label' => __("Commentaire"),
            ],
            'RestitutionRequests.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'RestitutionRequests.archive_units' => [
                'label' => __("Unités d'archives"),
                'callback' => 'TableHelper.ul("name")',
                'escape' => false,
                'filter' => [
                    'archive_unit_identifier[0]' => [
                        'id' => 'filter-archive_unit_identifier-0',
                        'label' => __("Identifiant"),
                    ],
                    'archive_unit_name[0]' => [
                        'id' => 'filter-archive_unit_name-0',
                        'label' => __("Nom"),
                    ],
                ],
            ],
            'RestitutionRequests.statetrad' => [
                'label' => __("Etat"),
                'filter' => [
                    'state[0]' => [
                        'id' => 'filter-state-0',
                        'label' => false,
                        'aria-label' => __("Etat"),
                        'options' => $states,
                    ],
                ],
            ],
        ]
        + (in_array($this->getRequest()->getParam('action'), ['indexPreparating', 'indexMy', 'indexAcquitted'])
            ? []
            : [
                'RestitutionRequests.created_user.username' => [
                    'label' => __("Créée par"),
                    'escape' => false,
                    'order' => 'created_user.username',
                    'filterCallback' => 'AsalaeFilter.datepickerCallback',
                    'filter' => [
                        'created_user[0]' => [
                            'id' => 'filter-created_user-0',
                            'label' => __("Créée par"),
                            'options' => $created_users,
                        ],
                    ],
                ],
            ]
        )
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'RestitutionRequests.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'data-callback' => "scheduleDestruction({0})",
                'confirm' => __(
                    "Les archives seront détruites par la tâche planifiée \"{0}\". Voulez-vous continuer ?",
                    __("Restitutions : élimination des archives restituées")
                ),
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-calendar-o'),
                'title' => $title = __("Programmer la destruction des archives liées à {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/RestitutionRequests/scheduleArchiveDestruction'),
                'displayEval' => 'data[{index}].RestitutionRequests.state === "restitution_acquitted"',
                'params' => ['RestitutionRequests.id', 'RestitutionRequests.identifier'],
            ],
            [
                'onclick' => "cancelDestruction({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-calendar-times-o'),
                'title' => $title = __("Annuler la destruction de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/RestitutionRequests/cancelArchiveDestruction'),
                'displayEval' => 'data[{index}].RestitutionRequests.state === "archive_destruction_scheduled"',
                'params' => ['RestitutionRequests.id', 'RestitutionRequests.identifier'],
            ],
            [
                'onclick' => "actionViewRestitutionRequest({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Visualiser"),
                'display' => $this->Acl->check('/RestitutionRequests/view'),
                'params' => ['RestitutionRequests.id', 'RestitutionRequests.identifier'],
            ],
            [
                'onclick' => "actionEditRestitutionRequest({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/RestitutionRequests/edit'),
                'displayEval' => 'data[{index}].RestitutionRequests.editable '
                    . '&& data[{index}].RestitutionRequests.created_user_id === PHP.user_id',
                'params' => ['RestitutionRequests.id', 'RestitutionRequests.identifier'],
            ],
            [
                'data-callback' => "sendRestitutionRequest($jsTable, {0})",
                'type' => 'button',
                'class' => 'btn-link send',
                'display' => $this->Acl->check('/RestitutionRequests/send'),
                'displayEval' => 'data[{index}].RestitutionRequests.sendable '
                    . '&& data[{index}].RestitutionRequests.created_user_id === PHP.user_id',
                'label' => $this->Fa->charte('Envoyer', '', 'text-success'),
                'title' => __("Envoyer {0}", '{1}'),
                'aria-label' => __("Envoyer {0}", '{1}'),
                'confirm' => __(
                    "Une fois la demande de restitution envoyée, il ne sera "
                    . "plus possible de la modifier. Voulez-vous continuer ?"
                ),
                'params' => ['RestitutionRequests.id', 'RestitutionRequests.identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/RestitutionRequests/delete')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Supprimer"),
                'displayEval' => 'data[{index}].RestitutionRequests.deletable '
                    . '&& data[{index}].RestitutionRequests.created_user_id === PHP.user_id',
                'confirm' => __("Êtes-vous sûr de vouloir supprimer cette demande de restitution ?"),
                'params' => ['RestitutionRequests.id', 'RestitutionRequests.identifier'],
            ],
            [
                'href' => $this->Url->build('/restitution-requests/restitution-certificate') . '/{0}',
                'download' => '{1}_restitution_certificate.pdf',
                'target' => '_blank',
                'type' => 'button',
                'class' => 'btn-link',
                'data-action' => __("Attestation de restitution"),
                'label' => $this->Fa->i('fas fa-award'),
                'title' => $title = __("Attestation de restitution de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/RestitutionRequests/restitution-certificate'),
                'displayEval' => 'data[{index}].restitution_certified',
                'params' => ['RestitutionRequests.id', 'RestitutionRequests.identifier'],
            ],
            [
                'href' => $this->Url->build('/restitution-requests/destruction-certificate') . '/{0}',
                'download' => '{1}_restitution_certificate.pdf',
                'target' => '_blank',
                'type' => 'button',
                'class' => 'btn-link',
                'data-action' => __("Attestation d'élimination"),
                'label' => $this->Fa->i('fas fa-award'),
                'title' => $title = __("Attestation d'élimination de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/RestitutionRequests/restitution-certificate'),
                'displayEval' => 'data[{index}].destruction_certified !== null',
                'params' => ['RestitutionRequests.id', 'RestitutionRequests.identifier'],
            ],
        ]
    );

$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des demandes de restitution"),
        'table' => $table,
    ]
);
