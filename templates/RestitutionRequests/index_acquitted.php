<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Restitutions"));
$this->Breadcrumbs->add($h1 = __("Archives restituées et acquittées à éliminer"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-eraser', $h1))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

require 'index-common.php';
