<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Csv->resultTable(
    [
        'code' => __("Code"),
        'name' => __("Nom"),
        'created' => __("Date de création"),
        'modified' => __("Date de modification"),
    ],
    $results
);
