<?php

/**
 * @var Asalae\View\AppView $this
 */
$output = [
    'date_du_depot' => (new \DateTime())->format(DATE_RFC3339),
    'file_upload_status' => sprintf(
        'téléchargement %s du fichier : %s',
        $completed ? 'complet' : 'incomplet',
        $this->getRequest()->getData('file_name')
    ),
    'all_files_uploaded' => $all_files_uploaded,
];
echo json_encode($output);
