<?php

/**
 * @var Asalae\View\AppView $this
 */
$output = [
    'date_du_depot' => (new \DateTime())->format(DATE_RFC3339),
    'file_upload_status' => sprintf(
        'téléchargement %s du fichier : %s',
        $completed ? 'complet' : 'incomplet',
        h($this->getRequest()->getData('file_name'))
    ),
    'all_files_uploaded' => $all_files_uploaded,
];
foreach ($output as $key => $value) {
    echo sprintf("%s : %s<br>\n", $key, $value);
}
