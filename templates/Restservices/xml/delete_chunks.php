<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Utility\Xml;

$output = ['date_de_suppression' => (new \DateTime())->format(DATE_RFC3339)];
echo Xml::fromArray($output, ['pretty' => true])->saveXML();
