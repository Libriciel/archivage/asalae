<?php

use Cake\Utility\Xml;

$output['sedaMessages']['date_du_depot'] = (new \DateTime())->format(DATE_RFC3339);
if (isset($chunk_session_identifier)) {
    $output['sedaMessages']['chunk_session_identifier'] = $chunk_session_identifier;
}
if (isset($chunk_security_identifier)) {
    $output['sedaMessages']['chunk_security_identifier'] = $chunk_security_identifier;
}
echo Xml::fromArray($output, ['pretty' => true])->saveXML();
