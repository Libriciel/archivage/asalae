<?php

$output = ['date_du_depot' => (new \DateTime())->format(DATE_RFC3339)];
if (isset($chunk_session_identifier)) {
    $output['chunk_session_identifier'] = $chunk_session_identifier;
}
if (isset($chunk_security_identifier)) {
    $output['chunk_security_identifier'] = $chunk_security_identifier;
}
foreach ($output as $key => $value) {
    echo sprintf("%s : %s<br>\n", $key, $value);
}
