<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Html->tag('article');
echo $this->Html->tag('header.bottom-space');
echo $this->Html->tag('h3', h($entity->get('code')));
echo $this->Html->tag('p', h($entity->get('name')));
echo $this->Html->tag('/header');

echo $this->Html->tag('h4', __("Code de restriction d'accès"));
echo $this->ViewTable->generate(
    $entity,
    [
        __("Code") => 'code',
        __("Nom") => 'name',
        __("Description") => 'description',
        __("Durée") => 'duration',
    ]
);
echo $this->Html->tag('/article');
