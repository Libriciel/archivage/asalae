<?php

/**
 * @var Asalae\View\AppView $this
 */
$view = $this;
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'AccessRuleCodes.code' => [
                'label' => __("Code"),
                'order' => 'code',
                'filter' => [
                    'code[0]' => [
                        'id' => 'filter-code-0',
                        'label' => false,
                        'aria-label' => __("Code"),
                    ],
                ],
            ],
            'AccessRuleCodes.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'AccessRuleCodes.duration' => [
                'label' => __("Durée"),
                'order' => 'duration',
                'filter' => [
                    'duration[0]' => [
                        'id' => 'filter-duration-0',
                        'label' => false,
                        'aria-label' => __("Durée"),
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'AccessRuleCodes.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewAccessRuleCode({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Visualiser {0}", '{1}'),
                'aria-label' => __("Visualiser {0}", '{1}'),
                'display' => $this->Acl->check('access-rule-codes/view'),
                'params' => ['AccessRuleCodes.id', 'AccessRuleCodes.code'],
            ],
            [
                'onclick' => "actionEditAccessRuleCode({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'display' => $this->Acl->check('access-rule-codes/edit'),
                'params' => ['AccessRuleCodes.id', 'AccessRuleCodes.code'],
            ],
            function ($table) {
                /** @var Asalae\View\AppView $this */
                $deleteUrl = $this->Url->build('/AccessRuleCodes/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction({$table->tableObject}, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'display' => $this->Acl->check('access-rule-codes/delete'),
                    'displayEval' => "data[{index}].AccessRuleCodes.deletable",
                    'params' => ['AccessRuleCodes.id', 'AccessRuleCodes.code'],
                ];
            },
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => "$tableId-section",
        'title' => __("Liste des codes"),
        'table' => $table,
        'downloadCsv' => false,
        'class' => 'container',
    ]
);
