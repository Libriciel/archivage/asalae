<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Codes des restrictions d'accès seda v2"));

echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-lock', __("Codes des restrictions d'accès seda v2")))
    . $this->Breadcrumbs->render()
);

$jsTable = $this->Table->getJsTableObject($tableId);
$buttons = [];
if ($this->Acl->check('/access-rule-codes/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-access-rule-code')
        ->modal(__("Ajout d'un code de restriction d'accès"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "AccessRuleCodes")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un code de restriction d'accès")),
            '/AccessRuleCodes/add'
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->element('modal', ['idTable' => $tableId]);
echo $this->element('modal', ['idTable' => $tableIdGlobals, 'paginate' => false]);

echo $this->ModalForm
    ->create('edit-access-rule-code', ['size' => 'large'])
    ->modal(__("Modifier un code de restriction d'accès"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "AccessRuleCodes")')
    ->output(
        'function',
        'actionEditAccessRuleCode',
        '/AccessRuleCodes/edit'
    )
    ->generate();

echo $this->ModalView
    ->create('view-access-rule-code')
    ->modal(__("Visualiser un code de restriction d'accès"))
    ->output(
        'function',
        'actionViewAccessRuleCode',
        '/AccessRuleCodes/view'
    )
    ->generate();

echo $this->Filter->create('access-rule-codes-filter')
    ->saves($savedFilters)
    ->filter(
        'code',
        [
            'label' => __("Code"),
            'wildcard',
        ]
    )
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'duration',
        [
            'label' => __("Durée"),
            'wildcard',
        ]
    )
    ->generateSection();

require 'ajax_index.php';

$table = $this->Table
    ->create($tableIdGlobals, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'AccessRuleCodes.code' => [
                'label' => __("Code"),
            ],
            'AccessRuleCodes.name' => [
                'label' => __("Nom"),
            ],
            'AccessRuleCodes.duration' => [
                'label' => __("Durée"),
            ],
        ]
    )
    ->data($dataGlobals)
    ->params(
        [
            'identifier' => 'AccessRuleCodes.id',
            'favorites' => false,
            'sortable' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewAccessRuleCode({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Visualiser {0}", '{1}'),
                'aria-label' => __("Visualiser {0}", '{1}'),
                'display' => $this->Acl->check('access-rule-codes/view'),
                'params' => ['AccessRuleCodes.id', 'AccessRuleCodes.code'],
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => "$tableIdGlobals-section",
        'title' => __("Liste des codes globaux"),
        'table' => $table,
        'paginate' => false,
        'class' => 'container',
    ]
);

?>
<!--suppress JSUnresolvedFunction -->
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });
</script>
