<?php

/**
 * @var Asalae\View\AppView $this
 */
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Eaccpfs.record_id' => ['label' => __("Identifiant unique"), 'order' => 'record_id'],
            'Eaccpfs.name' => ['label' => __("Nom"), 'order' => 'name'],
            'Eaccpfs.entity_id' => ['label' => __("Identifiant ISNI"), 'order' => 'entity_id'],
            'Eaccpfs.entity_type' => ['label' => 'Type', 'options' => $entity_type, 'order' => 'entity_type'],
            'Eaccpfs.from_date' => ['label' => __("Date début"), 'type' => 'date', 'order' => 'from_date'],
            'Eaccpfs.to_date' => ['label' => __("Date fin"), 'type' => 'date', 'order' => 'to_date'],
            'Eaccpfs.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'created',
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'Eaccpfs.id',
            'favorites' => true,
            'sortable' => true,
            'checkbox' => true,
        ]
    )
    ->actions(
        [
            [
                'href' => "/Eaccpfs/download/{0}",
                'download' => '{2}',
                'target' => '_blank',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => __("Télécharger {0}", '{1}'),
                'aria-label' => __("Télécharger {0}", '{1}'),
                'params' => ['Eaccpfs.id', 'Eaccpfs.name', 'Eaccpfs.record_id'],
            ],
            [
                'onclick' => "loadEditModal({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'params' => ['Eaccpfs.id', 'Eaccpfs.name'],
            ],
            [
                'onclick' => "actionDelete({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => __("Supprimer {0}", '{1}'),
                'aria-label' => __("Supprimer {0}", '{1}'),
                'params' => ['Eaccpfs.id', 'Eaccpfs.name'],
            ],
        ]
    );

// @see src/Template/Eaccpf/index.php
$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => 'eaccpfs-section',
        'title' => __("Liste des notices d'autorité"),
        'aria-label' => __("Liste des notices d'autorité"),
        'table' => $table,
        'pagination' => [
            'options' => [
                'actions' => [
                    '0' => __("-- action groupée --"),
                    '1' => __("Télécharger en ZIP"),
                    '2' => __("Supprimer"),
                ],
            ],
            'callback' => 'updatePaginationAjaxCallback',
        ],
    ]
);
