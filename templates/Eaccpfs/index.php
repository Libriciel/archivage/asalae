<?php

/**
 * @var Asalae\View\AppView $this
 */
$view = $this;
$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

echo $this->element('modal', ['idTable' => $tableId]);
echo $this->element('modal', ['idTable' => $this->Upload->getTableId($uploadId), 'paginate' => false]);

echo $this->Modal->create(['id' => 'info-file'])
    . $this->Modal->header(__("Informations complémentaires test"))
    . $this->Modal->body($loading)
    . $this->Modal->end()
    . $this->ModalForm->modalScript('info-file');

$uploads = $this->Upload
    ->create($uploadId, ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'prev' => ['label' => __("Prévisualisation")],
            'filename' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
            'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
            'mime' => ['label' => __("Type MIME")],
            'statetrad' => ['label' => __("statetrad")],
            'message' => ['label' => __("Message"), 'style' => 'min-width: 250px', 'class' => 'message'],
        ]
    )
    ->data([])
    ->params(
        [
            'identifier' => 'id',
            'checkbox' => true,
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    )
    ->actions(
        [
            function ($table) use ($view) {
                $url = $view->Url->build('/Transfers/add');
                return [
                    'onclick'
                    => "GenericUploader.fileSelect({$table->tableObject}, '$url', "
                        . "{0}, AsalaeGlobal.updatePaginationAjax, '#eaccpfs-section')",
                    'type' => 'button',
                    'class' => 'btn-link fileSelect',
                    'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                    'label' => $view->Fa->i('fa-level-down text-success'),
                    'title' => __("Ajouter {0}", '{1}'),
                    'aria-label' => __("Ajouter {0}", '{1}'),
                    'params' => ['id', 'name'],
                ];
            },
            [
                'href' => "/download/file/{0}",
                'download' => '{1}',
                'target' => '_blank',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => __("Télécharger {0}", '{1}'),
                'aria-label' => __("Télécharger {0}", '{1}'),
                'params' => ['id', 'name'],
            ],
            [
                'onclick' => "GenericUploader.loadDataModal({0})",
                'type' => "button",
                'class' => 'btn-link',
                'data-toggle' => 'modal',
                'data-target' => '#info-file',
                'label' => $this->Fa->i('fa-list-alt'),
                'title' => __("Visualiser informations techniques"),
                'aria-label' => __("Visualiser informations techniques"),
                'params' => ['id'],
            ],
            [
                'href' => $this->Url->build(['controller' => 'Download', 'action' => 'open']) . "/{0}",
                'label' => $this->Fa->i('fa-folder-open-o'),
                'title' => __("Ouvrir {0} dans le navigateur", '{1}'),
                'aria-label' => __("Ouvrir {0} dans le navigateur", '{1}'),
                'params' => ['id', 'name'],
                'target' => '_blank',
                'displayEval' => 'data[{index}].openable',
            ],
            function ($table) use ($view) {
                $url = $view->Url->build('/Upload/delete');
                return [
                    'onclick' => "GenericUploader.fileDelete({$table->tableObject}, '$url', {0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                    'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name'],
                ];
            },
        ]
    );

echo $this->ModalForm
    ->create('eac-cpf-edit', ['size' => 'modal-xxl'])
    ->modal(__("Modifier une notice d'autorité"))
    ->javascriptCallback('afterAdd')
    ->output('function', 'loadEditModal', '/Eaccpfs/edit')
    ->generate();

echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-id-card-o', __("Notices d'autorité")))
);

echo $this->element(
    'section_uploads',
    [
        'title' => __("Formulaire d'upload"),
        'uploads' => $uploads->generate(
            [
                'attributes' => ['accept' => '.xml, application/xml'],
                'target' => $this->Url->build('Upload/index/eaccpf?replace=true'),
            ]
        ),
    ]
);

echo $this->Filter->create('eaccpfs-filter')
    ->saves($savedFilters)
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'date',
        [
            'label' => __("Date (entre début et fin)"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
            'prepend' => '',
        ]
    )
    ->filter(
        'record_id',
        [
            'label' => __("Identifiant unique"),
            'placeholder' => 'ident?fi*',
            'type' => 'text',
        ]
    )
    ->filter(
        'entity_type',
        [
            'label' => __("Type"),
            'options' => $entity_type,
            'multiple' => true,
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->generateSection();

require 'ajax_index.php';
?>
<section class="container bg-white">
    <h2 class="h4"><?= __("Actions") ?></h2>

    <div>
        <?= $this->ModalForm
            ->create('add-eaccpf-modal')
            ->modal(__("Ajouter une notice d'autorité"))
            ->javascriptCallback('afterAdd')
            ->output(
                'button',
                $this->Fa->charte('Ajouter', __("Ajouter une notice d'autorité")),
                '/Eaccpfs/add'
            )
            ->generate(['class' => 'btn btn-success', 'type' => 'button']) ?>
    </div>
</section>

<?= $this->Html->tag('div', $this->element('cpf-doc', [], ['cache' => true]), ['class' => 'hidden']) ?>

<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#eaccpfs-section');
    });

    /**
     * Action delete
     *
     * @param {number} id
     */
    function actionDelete(id) {
        var useConfirm = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
        var update = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
        if (useConfirm && !confirm("<?=__("Êtes-vous sûr de vouloir supprimer cette notice d'autorité ?")?>")) {
            return;
        }
        $.ajax({
            url: '<?=$this->Url->build('/Eaccpfs/delete')?>/' + id,
            success: function (message) {
                if (message.report === 'done') {
                    var table;
                    table = <?=$jsTable?>;
                    table.removeDataId(id);
                    $(table.table)
                        .find('tr[data-id="' + id + '"]')
                        .fadeOut(400, function () {
                            if (update) {
                                AsalaeGlobal.updatePaginationAjax($(this).closest('section'));
                            }
                        });
                } else {
                    throw message.report;
                }
            },
            error: function () {
                $('#table-1-tr-' + id)
                    .addClass('danger')
                    .find('td.Transfers-commentaire')
                    .text("<?=__("Une erreur a eu lieu lors de la suppression")?>");
            }
        });
    }

    /**
     * Callback de l'action ajout
     *
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterAdd(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            AsalaeGlobal.updatePaginationAjax('#eaccpfs-section');
        }
    }

    /**
     * Action de groupe sur le tableau
     */
    function updatePaginationAjaxCallback() {
        $('#btn-grouped-actions').off('click').click(function () {
            var visibleCheckedCheckboxes = $('.table-1-checkbox:checked');
            $(this).disable();
            switch ($('#select-grouped-actions').val()) {
                case '2':
                    if (confirm("Êtes-vous sûr de vouloir supprimer les fichiers sélectionnés ?")) {
                        visibleCheckedCheckboxes.each(function () {
                            actionDelete($(this).val(), false);
                        });
                    }
                    break;
                case '1':
                    var ids = [];
                    visibleCheckedCheckboxes.each(function () {
                        ids.push($(this).val());
                    });
                    window.location = '<?= $this->Url->build('/Eaccpfs/zip') ?>?id=' + ids.join();

            }
            $(this).enable();
            $('#table-1-checkall').prop('checked', false);
        });
    }

    updatePaginationAjaxCallback();
    $(window).off('history.change.pagination').on('history.change.pagination', function () {
        updatePaginationAjaxCallback();
    });
</script>
