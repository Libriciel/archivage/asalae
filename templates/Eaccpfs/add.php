<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->template($tmpl ?? 'MultiStepForm/add_org_entity')->step(2) . '<hr>';

$upload = $this->Upload
    ->create($uploadId, ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'filename' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
            'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
            'message' => ['label' => __("Message"), 'style' => 'min-width: 250px', 'class' => 'message'],
        ]
    )
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    );

$jsUpload = $upload->jsObject;

echo $this->Form->create($entity, ['id' => 'form-eaccpf', 'class' => 'row no-padding', 'idPrefix' => 'add-eaccpf'])

    // Tabulations
    . $this->Html->tag(
        'ul',
        $this->Html->tag(
            'li',
            $this->Html->tag(
                'a',
                $this->Fa->i('fa-plus-circle', __("Informations")),
                ['href' => '#create-eaccpf']
            )
        )
        . $this->Html->tag(
            'li',
            $this->Html->tag(
                'a',
                $this->Fa->i('fa-upload', __x('tab', "Téléversement")),
                ['href' => '#upload-eaccpf']
            )
        )
    )
    // Tab Informations
    . $this->Html->tag(
        'div',
        $this->Form->control(
            'agency_name',
            [
                'label' => __("Agence de maintenance"),
                'required' => true,
            ]
        )
        . $this->Form->control(
            'entity_id',
            [
                'label' => __("Identifiant ISNI"),
                'placeholder' => __("Optionnel"),
                'type' => 'text', // evite le type => select causé par _id
            ]
        )
        . $this->Form->control(
            'entity_type',
            [
                'label' => __("Type"),
                'options' => $entity_type,
                'required' => true,
                'default' => 'corporateBody',
                'empty' => __("-- Veuillez sélectionner une valeur --"),
            ]
        )
        . $this->Form->control(
            'from_date',
            [
                'label' => __("Date début"),
                'append' => $this->Date->picker('#add-eaccpf-from-date'),
                'class' => 'datepicker',
                'placeholder' => empty($prevEntity) ? __("Optionnel") : '',
                'required' => !empty($prevEntity),
                'type' => 'text', // evite le type => date
            ]
        )
        . $this->Form->control(
            'to_date',
            [
                'label' => __("Date fin"),
                'append' => $this->Date->picker('#add-eaccpf-to-date'),
                'class' => 'datepicker',
                'placeholder' => __("Optionnel"),
                'type' => 'text', // evite le type => date
            ] + (empty($prevEntity) ? [] : ['val' => ''])
        ),
        ['id' => 'create-eaccpf']
    )

    // Tab Upload
    . $this->Html->tag(
        'div',
        $this->Html->tag(
            'div',
            $upload->generate(
                [
                    'attributes' => ['accept' => '.xml, application/xml'],
                    'singleFile' => true,
                    'allowDuplicateUploads' => true,
                    'target' => $this->Url->build('Upload/index/eaccpf?replace=true'),
                ]
            ),
            ['class' => 'row']
        ),
        ['id' => 'upload-eaccpf']
    )

    . $this->Form->end();
?>
<script>
    $('#form-eaccpf').tabs();
    var uploadTable = {table: ''};
    uploadTable.table = '#<?=$uploadId?>-table';
    var uploader;
    uploader = <?=$jsUpload?>;

    /**
     * Colore en rouge le onglets en erreur, et selectionne le premier
     */
    AsalaeGlobal.colorTabsInError();

    $(uploadTable.table).change(function () {
        if ($(this).find('tbody tr').length === 0) {
            $(this).addClass('hide')
                .closest('section')
                .find('button.btn-primary')
                .parent()
                .addClass('hide');
        }
    });

    $('#' + uploader.config.id + '.dropbox').off().on('fileAdded', function () {
        $(uploadTable.table).removeClass('hide')
            .closest('section').find('button.btn-primary').parent().removeClass('hide');
    }).on('fileSuccess', function (event, file, message) {
        if (!AsalaeGlobal.is_numeric(message.report.id)) {
            return;
        }
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/Eaccpfs/addByUpload/' . $orgEntityId)?>/' + message.report.id,
            method: 'POST',
            success: function (content, textStatus, jqXHR) {
                switch (jqXHR.getResponseHeader('X-Asalae-Success')) {
                    case 'true':
                        $(uploadTable.table).closest('.modal').modal('hide');
                        if (typeof afterAddEaccpf === 'function') {
                            afterAddEaccpf(content, textStatus, jqXHR);
                        } else {
                            AsalaeGlobal.interceptedLinkToAjax('' + location);
                        }
                        break;
                    case 'false':
                        $(uploadTable.table).find('tr[data-id="' + message.report.id + '"]')
                            .addClass('danger').removeClass('warning')
                            .find('td.message > div')
                            .html(content.report.message);
                        break;
                    default:
                        return this.error(content);
                }
            },
            error: function () {
                $(uploadTable.table).find('tr[data-id="' + message.report.id + '"]')
                    .addClass('danger')
                    .removeClass('warning');
            }
        });
    });
</script>
