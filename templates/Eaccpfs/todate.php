<div class="alert alert-info">
    <?= __(
        "La date de fin de la précédente notice d'autorité n'a pas été saisie. "
        . "Merci de saisir une date avant de créer une nouvelle notice d'autorité."
    ) ?>
</div>
<?php
/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create($prevEntity, ['id' => 'form-eaccpf-todate', 'idPrefix' => 'todate-missing'])
    . $this->Form->control(
        'to_date',
        [
            'label' => __("Date fin"),
            'append' => $this->Date->picker('#todate-missing-to-date'),
            'class' => 'datepicker',
            'required' => true,
            'default' => $this->Date->today(),
            'type' => 'text', // evite le type => date
        ]
    )
    . $this->Form->end();
