<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $entity
 */

use Cake\Datasource\EntityInterface;

echo $this->element('jsFormTemplates');

echo $this->Form->create($entity)
    . $this->Form->control('data', ['type' => 'hidden'])
    . $this->Form->end();

foreach ($errors as $error) {
    echo '<div class="alert alert-danger alert-dismissable fade in">'
        . '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
        . $error
        . '</div>';
}
?>

<div class="col-md-4 jstree_container" id="jstreeZone"></div>
<div class="col-md-8 jstree_data" id="FormDiv"></div>
<div class="col-md-12 jstree_doc" id="DocZone"></div>

<?= $this->Html->tag('div', $this->element('cpf-doc', [], ['cache' => true]), ['class' => 'hidden']) ?>

<script>
    XsdForm.texts = PHP.messages.XsdForm;
    var XsdFormObject = new XsdForm(<?=json_encode($xsd)?>, '#jstreeZone', '#FormDiv', '#DocZone', [
        '#comment',
        '@xsi:schemaLocation',
        '@xmlns',
        '@xmlns:xsi',
        '@xmlns:xs',
        '@xmlns:xlink',
        'maintenanceHistory'
    ]);
    XsdFormObject.update(<?=$entity->get('data')?>);
    $(XsdFormObject).on('data_changed', function () {
        $('#data').val(JSON.stringify(this.data));
    });
    $('.jstree_tree').on('select_node.jstree', function () {
        $('#eac-cpf-edit').find('.modal-content').trigger('resize').css('height', '');
    });
</script>