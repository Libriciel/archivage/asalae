<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>

<ArchiveTransfer xmlns="fr:gouv:culture:archivesdefrance:seda:v2.2">
    <?= $transfer_comment ? '    <Comment>' . $transfer_comment . '</Comment>' . PHP_EOL : '' ?>
    <Date><?= $transfer_date ?></Date>
    <MessageIdentifier><?= $transfer_identifier ?></MessageIdentifier>
    <?= $agreement ? '        <ArchivalAgreement>' . $agreement . '</ArchivalAgreement>' . PHP_EOL : '' ?>
    <CodeListVersions></CodeListVersions>
    <DataObjectPackage>
        <?php
        /** @var DOMElement $archive */
        foreach ($archive->childNodes as $node) {
            $content = trim($archive->ownerDocument->saveXML($node));
            if ($content) {
                echo '        ' . $content . PHP_EOL;
            }
        }
        ?>
    </DataObjectPackage>
    <ArchivalAgency>
        <Identifier><?= $archival_agency_identifier ?></Identifier>
        <OrganizationDescriptiveMetadata>
            <Name xmlns="fr:gouv:culture:archivesdefrance:seda:v1.0"><?= $archival_agency_name ?></Name>
        </OrganizationDescriptiveMetadata>
    </ArchivalAgency>
    <TransferringAgency>
        <Identifier><?= $transferring_agency_identifier ?></Identifier>
        <OrganizationDescriptiveMetadata>
            <Name xmlns="fr:gouv:culture:archivesdefrance:seda:v1.0"><?= $transferring_agency_name ?></Name>
        </OrganizationDescriptiveMetadata>
    </TransferringAgency>
</ArchiveTransfer>
