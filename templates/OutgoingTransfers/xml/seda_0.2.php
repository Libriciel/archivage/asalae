<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>

<ArchiveTransfer xmlns="fr:gouv:ae:archive:draft:standard_echange_v0.2">
    <?= $transfer_comment ? '    <Comment languageID="fr">' . $transfer_comment . '</Comment>' . PHP_EOL : '' ?>
    <Date><?= $transfer_date ?></Date>
    <TransferIdentifier><?= $transfer_identifier ?></TransferIdentifier>
    <TransferringAgency>
        <Identification><?= $transferring_agency_identifier ?></Identification>
        <Name><?= $transferring_agency_name ?></Name>
    </TransferringAgency>
    <ArchivalAgency>
        <Identification><?= $archival_agency_identifier ?></Identification>
        <Name><?= $archival_agency_name ?></Name>
    </ArchivalAgency>
    <Contains>
        <?php
        /** @var DOMElement $archive */
        foreach ($archive->childNodes as $node) {
            $content = trim($archive->ownerDocument->saveXML($node));
            if ($content) {
                echo '        ' . $content . PHP_EOL;
            }
        }
        ?>
    </Contains>
</ArchiveTransfer>
