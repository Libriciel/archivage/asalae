<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Transferts sortants"));
$this->Breadcrumbs->add($h1 = __("Transferts sortants en cours"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-caret-square-o-right', $h1))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

require 'index-common.php';
