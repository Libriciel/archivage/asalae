<?php

/**
 * @var Asalae\View\AppView $this
 */

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'OutgoingTransfers.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'OutgoingTransfers.comment' => [
                'label' => __("Commentaire"),
            ],
            'OutgoingTransfers.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'OutgoingTransfers.outgoing_transfer_request.identifier' => [
                'label' => __("Identifiant de la demande"),
            ],
            'OutgoingTransfers.statetrad' => [
                'label' => __("Etat"),
                'filter' => [
                    'state[0]' => [
                        'id' => 'filter-state-0',
                        'label' => false,
                        'aria-label' => __("Etat"),
                        'options' => $states,
                    ],
                ],
            ],
            'OutgoingTransfers.error_msg' => [
                'label' => __("Message d'erreur"),
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'OutgoingTransfers.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewOutgoingTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/OutgoingTransfers/view'),
                'params' => ['OutgoingTransfers.id', 'OutgoingTransfers.identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/OutgoingTransfers/delete')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Supprimer"),
                'display' => $this->Acl->check('/outgoing-transfers/delete'),
                'displayEval' => 'data[{index}].OutgoingTransfers.deletable '
                    . '&& data[{index}].OutgoingTransfers.outgoing_transfer_request.created_user_id === PHP.user_id',
                'confirm' => __(
                    "Êtes-vous sûr de vouloir supprimer ce transfert sortants ? "
                    . "sa suppression retirera l'archive de la demande, voulez-vous continuer ?"
                ),
                'params' => ['OutgoingTransfers.id', 'OutgoingTransfers.identifier'],
            ],
        ]
    );

$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des transferts sortants"),
        'table' => $table,
    ]
);
