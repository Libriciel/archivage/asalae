<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $outgoingTransfers
 */

use Cake\Datasource\EntityInterface;

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($outgoingTransfers->get('identifier')));
$infos .= $this->Html->tag('p', h($outgoingTransfers->get('comment')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Transfert sortant"));
$infos .= $this->ViewTable->generate(
    $outgoingTransfers,
    [
        __("Identifiant") => 'identifier',
        __("Commentaire") => 'comment|nl2br',
        __("Etat") => 'statetrad',
        __("Identifiant de la demande") => 'outgoing_transfer_request.identifier',
        __("Nombre d'unités d'archives") => 'au_count',
        __("Nombre total de fichiers") => 'archive_unit.original_total_count',
        __("Taille totale des fichiers") => 'archive_unit.original_total_size|toReadableSize',
        __("Système d'archivage électronique")
        => '{outgoing_transfer_request.archiving_system.name} ({outgoing_transfer_request.archiving_system.url})',
        __("Date de création") => 'created',
        __("Date de modification") => 'modified',
    ]
);
$infos .= $this->Html->tag('/article');

echo $infos;
