<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$map = function (EntityInterface $entity): EntityInterface {
    $archiveUnits = [];
    foreach (Hash::get($entity, 'archive_units', []) as $archiveUnit) {
        $archiveUnits[] = Hash::get($archiveUnit, 'name');
    }
    $entity->set(
        'archive_units',
        $archiveUnits ? '- ' . implode("\n- ", $archiveUnits) : ''
    );
    $user = Hash::get($entity, 'outgoing_transfer_request.created_user.username');
    if ($name = Hash::get($entity, 'created_user.name')) {
        $user .= " ($name)";
    }
    $orgEntityName = Hash::get($entity, 'outgoing_transfer_request.created_user.org_entity.name');
    $user .= "\n- $orgEntityName";
    $entity->set('created_user', $user);
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'identifier' => __("Identifiant"),
        'comment' => __("Commentaire"),
        'created' => __("Date de création"),
        'archive_units' => __("Unités d'archives"),
        'statetrad' => __("Etat"),
        'created_user' => __("Demandeur"),
    ],
    $results,
    $map
);
