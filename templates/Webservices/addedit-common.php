<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 * @var array $entities
 */

use Cake\Utility\Hash;

echo '<label class="hide">autocomplete obliterator'
    . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';
echo $this->Html->tag(
    'div',
    $this->Form->control(
        'name',
        [
            'label' => __("Nom du webservice"),
            'required' => true,
        ]
    )
    . $this->Form->control(
        'email',
        [
            'label' => __("Email de contact"),
            'help' => __("Optionnel"),
        ]
    )
    . $this->Form->control(
        'role_id',
        [
            'label' => __("Rôle"),
            'required' => true,
            'options' => $roles,
            'empty' => __("-- Veuillez sélectionner un rôle --"),
        ]
    )
    . $this->Form->control(
        'active',
        [
            'label' => __("Actif"),
            'default' => true,
        ]
    )
);
echo $this->Form->fieldset(
    $this->Form->control(
        'username',
        [
            'label' => __("Identifiant de connexion"),
            'required' => true,
        ]
    )
    . $this->Form->control(
        'password',
        [
            'label' => __("Mot de passe de l'utilisateur"),
            'type' => 'password',
            'required' => $passwordRequired,
        ]
    )
    . $this->Form->control(
        'confirm-password',
        [
            'label' => __("Confirmer le mot de passe"),
            'type' => 'password',
            'required' => $passwordRequired,
        ]
    )
    . $this->Form->control(
        'sha256',
        [
            'label' => __("Hasher le mot de passe en sha256 (connecteur v1)"),
            'type' => 'checkbox',
        ]
    ),
    ['legend' => __("Identification (basic)")]
);
?>
<script>
    var idSelectEntity = $('#<?=$idSelectEntity?>');

    function updateRoles() {
        var value = idSelectEntity.val();
        var selectRoles = $('#<?=$idSelectRoles?>');
        selectRoles.find('option[value!=""]').remove();
        if (!value) {
            return;
        }
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/users/getWsRolesOptions')?>/' + value,
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                for (var i in content) {
                    var option = $('<option></option>').attr('value', i).text(content[i]);
                    selectRoles.append(option);
                }
            }
        });
    }

    idSelectEntity.change(updateRoles);

    <?php if (count(Hash::flatten($entities ?? [])) === 1 && empty($entity->id)) : ?>
    updateRoles();
    <?php endif; ?>
</script>
