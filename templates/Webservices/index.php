<?php

/**
 * @var Asalae\View\AppView $this
 * @var Webservice          $webservice
 */

use Asalae\Model\Entity\Webservice;

echo $this->element('modal', ['idTable' => $tableId, 'paginate' => false]);

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Users.name' => ['label' => __("Nom du webservice")],
            'Users.username' => ['label' => __("Identifiant de connexion")],
            'Users.org_entity.name' => ['label' => __("Entité")],
            'Users.org_entity.archival_agency.name' => ['label' => __("Service d'archives")],
            'Users.active' => ['label' => __("Actif"), 'type' => 'boolean'],
            'Users.email' => ['label' => __("E-mail")],
            'Users.created' => ['label' => __("Date de création"), 'type' => 'date'],
            'Users.modified' => ['label' => __("Date de Modification"), 'type' => 'date', 'display' => false],
        ]
    )
    ->data($webservices)
    ->params(
        [
            'identifier' => 'Users.id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewWebservice({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Voir {0}", '{1}'),
                'aria-label' => __("Voir {0}", '{1}'),
                'params' => ['Users.id', 'Users.name'],
            ],
            [
                'onclick' => "actionEditWebservice({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'params' => ['Users.id', 'Users.name'],
            ],
            function ($table) {
                $view = $this;
                $deleteUrl = $view->Url->build('/Webservices/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction({$table->tableObject}, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'displayEval' => 'data[{index}].Users.deletable',
                    'params' => ['Users.id', 'Users.name'],
                ];
            },
        ]
    );

$jsTable = $table->tableObject;

echo $this->Html->tag(
    'section#webservice-section.bg-white',
    $this->Html->tag(
        'div.separator',
        $this->ModalForm
            ->create('add-webservice-modal')
            ->modal(__("Ajout d'un accès de webservice"))
            ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "Users")')
            ->output(
                'button',
                $this->Fa->charte('Ajouter', __("Ajouter un accès de webservice")),
                '/Webservices/add'
            )
            ->generate(['class' => 'btn btn-success'])
        . $this->ModalView
            ->create('view-webservice-modal')
            ->modal(__("Visualiser un accès de webservice"))
            ->output(
                'function',
                'actionViewWebservice',
                '/Webservices/view'
            )
            ->generate()
        . $this->ModalForm
            ->create('edit-webservice-modal')
            ->modal(__("Modification d'un accès de webservice"))
            ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "Users")')
            ->output(
                'function',
                'actionEditWebservice',
                '/Webservices/edit'
            )
            ->generate()
    )
    . $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Liste des webservices"))
        . $this->Html->tag('div.r-actions.h4', $table->getConfigureLink())
    )
    . $table->generate()
);
