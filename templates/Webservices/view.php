<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($user->get('name')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Webservice"));
$infos .= $this->ViewTable->generate(
    $user,
    [
        __("Nom du webservice") => 'name',
        __("Email de contact") => 'email',
        __("Rôle") => 'role.name',
        __("Actif") => 'active',
        __("Identifiant de connexion") => 'username',
        __("Date de création") => 'created',
        __("Date de modification") => 'modified',
    ]
);
$infos .= $this->Html->tag('/article');

echo $infos;
