<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Cake\Utility\Hash;

echo $this->Flash->render();
echo $this->Form->create($entity, ['idPrefix' => 'edit-webservice']);
$passwordRequired = false;
$idSelectEntity = 'edit-webservice-org-entity-id';
$idSelectRoles = 'edit-webservice-role-id';

echo $this->Form->control(
    'org_entity_id',
    [
        'label' => __("Entité"),
        'options' => [$entity->get('org_entity_id') => Hash::get($entity, 'org_entity.name')],
        'readonly',
    ]
);

require 'addedit-common.php';
echo $this->Form->end();
