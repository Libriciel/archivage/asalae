<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Cake\Utility\Hash;

echo $this->Flash->render();
echo $this->Form->create($entity, ['idPrefix' => 'add-webservice']);
$passwordRequired = true;
$idSelectEntity = 'add-webservice-org-entity-id';
$idSelectRoles = 'add-webservice-role-id';

if (empty($org_entity_id)) {
    echo $this->Form->control(
        'org_entity_id',
        [
            'label' => __("Entité de l'accès webservice"),
            'options' => $entities,
            'empty' => __("-- Sélectionner une entité --"),
            'required' => true,
            'default' => count(Hash::flatten($entities)) === 1 ? current(array_keys(current($entities))) : null,
        ]
    );
} else {
    echo $this->Form->control(
        'orgentity',
        [
            'label' => __("Entité"),
            'options' => [$entity->get('org_entity')->get('id') => $entity->get('org_entity')->get('name')],
            'readonly',
        ]
    );
}

require 'addedit-common.php';
echo $this->Form->end();
