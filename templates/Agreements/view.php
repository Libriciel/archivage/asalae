<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $entity
 */

use Cake\Datasource\EntityInterface;

echo $this->Html->tag('article');
echo $this->Html->tag('header.bottom-space');
echo $this->Html->tag('h3', h($entity->get('name')));
echo $this->Html->tag('p', h($entity->get('description')));
echo $this->Html->tag('/header');
$tables = [
    __("Informations principales") => [
        [
            __("Nom") => 'name',
            __("Validation automatique des transferts") => 'auto_validate',
            __("Circuit de validation des transferts conformes") => 'proper_chain.name',
            __("Circuit de validation des transferts non conformes") => 'improper_chain.name',
            __("Date de début de validité") => 'date_begin',
            __("Date de fin de validité") => 'date_end',
            __("Actif") => 'active',
            __("Accords de versement par défaut") => 'default_agreement',
            __("Date de création") => 'created',
            __("Date de dernière modification") => 'modified',
        ],
    ],

    __("Autorisations") => [
        [
            __("Autoriser tous les services versants") => 'allow_all_transferring_agencies',
            __("Services versants") => 'transferring_agencies.{n}.name',
        ],
        [
            __("Autoriser tous les services producteurs") => 'allow_all_originating_agencies',
            __("Services Producteurs") => 'originating_agencies.{n}.name',
        ],
        [
            __("Autoriser tous les profils d'archives") => 'allow_all_profiles',
            __("Profils d'archives") => 'profiles.{n}.name',
        ],
        [
            __("Autoriser tous les niveaux de service") => 'allow_all_service_levels',
            __("Niveaux de service") => 'service_levels.{n}.name',
        ],
    ],

    __("Contrôles") => [
        [
            __("Formats autorisés") => 'allowed_formats|raw|json_decode',
            __("Nombre de transferts maximum pour une période") => 'max_transfers',
            __("Période du nombre de transferts maximum") => 'transfer_periodtrad',
            __("Volume maximum des fichiers des pièces jointes")
            => $entity->get('max_size_per_transfer') ? 'max_size_per_transfer|toReadableSize' : '',
        ],
    ],
];
foreach ($tables as $th => $tbodies) {
    $tableMessage = $this->Html->tag('table.table.table-striped.table-hover.fixed-left-th');
    $tableMessage .= $this->Html->tag('thead');
    $tableMessage .= $this->Html->tag('tr');
    $tableMessage .= $this->Html->tag('th', $th, ['colspan' => 2]);
    $tableMessage .= $this->Html->tag('/tr');
    $tableMessage .= $this->Html->tag('/thead');
    foreach ($tbodies as $trs) {
        $tableMessage .= $this->Html->tag('tbody');
        $tableMessage .= $this->ViewTable->generate($entity, $trs, ['tbody' => true]);
        $tableMessage .= $this->Html->tag('/tbody');
    }
    $tableMessage .= $this->Html->tag('/table');
    echo $tableMessage;
}
echo $this->Html->tag('/article');
