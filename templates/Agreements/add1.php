<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->template('MultiStepForm/add_agreement')->step(1) . '<hr>';

echo $this->Form->create($entity, ['idPrefix' => 'add1-agreement'])
    . $this->Form->control(
        'name',
        [
            'label' => __("Nom"),
            'required',
        ]
    )
    . $this->Form->control(
        'identifier',
        [
            'label' => __("Identifiant unique"),
            'required',
        ]
    )
    . $this->Form->control(
        'description',
        [
            'label' => __("Description"),
            'type' => 'textarea',
        ]
    )
    . $this->Form->control(
        'date_begin',
        [
            'label' => __("Date de début de validité"),
            'append' => $this->Date->picker('#add1-agreement-date-begin'),
            'class' => 'datepicker',
            'placeholder' => __("Optionnel"),
            'type' => 'text',
        ]
    )
    . $this->Form->control(
        'date_end',
        [
            'label' => __("Date de fin de validité"),
            'append' => $this->Date->picker('#add1-agreement-date-end'),
            'class' => 'datepicker',
            'placeholder' => __("Optionnel"),
            'type' => 'text',
        ]
    )
    . $this->Form->control(
        'default_agreement',
        [
            'label' => __("Accord de versement par défaut"),
        ]
    )
    . $this->Form->control(
        'active',
        [
            'label' => __("Actif (autorise son utilisation pour les transferts entrants)"),
        ]
    )
    . $this->Form->fieldset(
        $this->Form->control(
            'auto_validate',
            [
                'label' => __("Validation automatique des transferts"),
            ]
        )
        . $this->Form->control(
            'proper_chain_id',
            [
                'label' => __("Circuit de validation des transferts conformes"),
                'options' => $validation_chains_validate,
                'empty' => __("-- Sélectionner un circuit de validation --"),
            ]
        )
        . $this->Form->control(
            'improper_chain_id',
            [
                'label' => __("Circuit de validation des transferts non conformes"),
                'options' => $validation_chains_invalidate,
                'empty' => __("-- Sélectionner un circuit de validation --"),
            ]
        ),
        ['legend' => __("Validation")]
    )
    . $this->Form->end();

?>
<script>
    $('#add1-agreement-auto-validate').change(function () {
        var select = $('#add1-agreement-proper-chain-id');
        if ($(this).prop('checked')) {
            select.disable().trigger("chosen:updated");
        } else {
            select.enable().trigger("chosen:updated");
        }
    }).change();

    // empêcher d'avoir un accord de versement par défaut désactivé
    var activeCheckbox = $('#add1-agreement-active');
    var isActive;
    $('#add1-agreement-default-agreement').on('change', function () {
        if ($(this).prop('checked')) {
            activeCheckbox
                .on('click', function () {
                    return false;
                })
                .prop('checked', true)
                .addClass('disabled');
        } else {
            activeCheckbox
                .off('click')
                .prop('checked', isActive)
                .removeClass('disabled');
        }
    }).change();
    activeCheckbox.on('change', function () {
        isActive = $(this).prop('checked');
    });
</script>
