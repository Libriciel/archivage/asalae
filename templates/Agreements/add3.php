<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;

echo $this->MultiStepForm->template('MultiStepForm/add_agreement')->step(3) . '<hr>';

$form = $this->Form;
$mult = function (string $name) use ($form) {
    return $form->control(
        $name,
        [
            'options' => [
                1 => __("o"),
                1024 => __("Ko"),
                pow(1024, 2) => __("Mo"), // default est défini dans add1
                pow(1024, 3) => __("Go"),
                pow(1024, 4) => __("To"),
                pow(1024, 5) => __("Po"),
            ],
            'class' => '',
            'templates' => [
                'inputContainer' => '{{content}}',
                'formGroup' => '{{input}}',
                'select' => '<select name="{{name}}">{{content}}</select>',
            ],
        ]
    );
};

echo $this->Form->create($entity, ['idPrefix' => 'add3-agreement'])
    . $this->Form->control('name', ['type' => 'hidden'])
    . $this->Form->control('identifier', ['type' => 'hidden'])
    . $this->Form->control('description', ['type' => 'hidden'])
    . $this->Form->control('default_agreement', ['type' => 'hidden'])
    . $this->Form->control('active', ['type' => 'hidden'])
    . $this->Form->control('auto_validate', ['type' => 'hidden'])
    . $this->Form->control('proper_chain_id', ['type' => 'hidden'])
    . $this->Form->control('improper_chain_id', ['type' => 'hidden'])
    . $this->Form->control('date_begin', ['type' => 'hidden'])
    . $this->Form->control('date_end', ['type' => 'hidden'])
    . $this->Form->control('allow_all_transferring_agencies', ['type' => 'hidden'])
    . $this->Form->control('allow_all_originating_agencies', ['type' => 'hidden'])
    . $this->Form->control('allow_all_service_levels', ['type' => 'hidden'])
    . $this->Form->control('allow_all_profiles', ['type' => 'hidden']);

foreach (['transferring_agencies', 'originating_agencies', 'service_levels', 'profiles'] as $field) {
    foreach ((array)$entity->get($field) as $e) {
        if ($e instanceof EntityInterface) {
            echo '<input type="hidden" name="' . $field . '[_ids][]" value="' . $e->get('id') . '">' . PHP_EOL;
        }
    }
}

require 'addedit-common.php';
echo $this->Form->end();
?>
<script>
    var allowedFormatsSelect = $('#add3-agreement-allowed-formats-select');
    AsalaeGlobal.select2(allowedFormatsSelect);
    setTimeout(
        () => allowedFormatsSelect.siblings().find('input').css({width: '100%'})
    );

    var val = $('#add3-agreement-allowed-formats').val();
    allowedFormatsSelect.change(function () {
        var value = $(this).val();
        var json = JSON.stringify(value);
        if (json.indexOf('""') !== -1) {
            value = value.filter(v => v);
            $(this).val(value);
            json = JSON.stringify(value);
        }
        $('#add3-agreement-allowed-formats').val(json);
    });
    $.each(JSON.parse(val ? val : '[]'), function (i, e) {
        allowedFormatsSelect.find("option[value='" + e + "']").prop("selected", true);
    });
    allowedFormatsSelect.trigger("chosen:updated");
</script>
