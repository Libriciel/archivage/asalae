<?php

/**
 * @var Asalae\View\AppView $this
 */

$form = $this->Form;
$mult = function (string $name) use ($form) {
    return $form->control(
        $name,
        [
            'options' => [
                1 => __("o"),
                1024 => __("Ko"),
                pow(1024, 2) => __("Mo"),
                pow(1024, 3) => __("Go"),
                pow(1024, 4) => __("To"),
                pow(1024, 5) => __("Po"),
            ],
            'class' => '',
            'templates' => [
                'inputContainer' => '{{content}}',
                'formGroup' => '{{input}}',
                'select' => '<select name="{{name}}">{{content}}</select>',
            ],
        ]
    );
};

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => 'edit-agreement',
        'class' => 'row no-padding',
        'id' => 'form-edit-agreements',
    ]
);

// Tabulations
echo $this->Html->tag(
    'ul',
    $this->Html->tag(
        'li',
        $this->Html->tag(
            'a',
            __("Informations"),
            ['href' => '#edit-agreement-tab1']
        )
    )
    . $this->Html->tag(
        'li',
        $this->Html->tag(
            'a',
            __("Autorisations"),
            ['href' => '#edit-agreement-tab2']
        )
    )
    . $this->Html->tag(
        'li',
        $this->Html->tag(
            'a',
            __("Contrôles"),
            ['href' => '#edit-agreement-tab3']
        )
    )
    . $this->Html->tag(
        'li',
        $this->Html->tag(
            'a',
            __("Notifications"),
            ['href' => '#edit-agreement-tab4']
        )
    )
);

// Tab Informations
echo $this->Html->tag('div#edit-agreement-tab1');
echo $this->Form->control(
    'name',
    [
        'label' => __("Nom"),
        'required',
    ]
)
    . $this->Form->control(
        'identifier',
        [
            'label' => __("Identifiant unique"),
            'required',
        ]
        + ($entity->get('editable_identifier')
            ? []
            : [
                'readonly' => true,
                'title' => __("L'accord est utilisé par un ou des transferts non rejetés et ne peut donc être modifié"),
            ]
        )
    )
    . $this->Form->control(
        'description',
        [
            'label' => __("Description"),
            'type' => 'textarea',
        ]
    )
    . $this->Form->control(
        'date_begin',
        [
            'label' => __("Date de début de validité"),
            'append' => $this->Date->picker('#edit-agreement-date-begin'),
            'class' => 'datepicker',
            'placeholder' => __("Optionnel"),
            'type' => 'text',
        ]
    )
    . $this->Form->control(
        'date_end',
        [
            'label' => __("Date de fin de validité"),
            'append' => $this->Date->picker('#edit-agreement-date-end'),
            'class' => 'datepicker',
            'placeholder' => __("Optionnel"),
            'type' => 'text',
        ]
    )
    . $this->Form->control(
        'default_agreement',
        [
            'label' => __("Accord de versement par défaut"),
        ]
    )
    . $this->Form->control(
        'active',
        [
            'label' => __("Actif (autorise son utilisation pour les transferts entrants)"),
        ]
    )
    . $this->Form->fieldset(
        $this->Form->control(
            'auto_validate',
            [
                'label' => __("Validation automatique des transferts"),
            ]
        )
        . $this->Form->control(
            'proper_chain_id',
            [
                'label' => __("Circuit de validation des transferts conformes"),
                'options' => $validation_chains_validate,
                'empty' => __("-- Sélectionner un circuit de validation --"),
            ]
        )
        . $this->Form->control(
            'improper_chain_id',
            [
                'label' => __("Circuit de validation des transferts non conformes"),
                'options' => $validation_chains_invalidate,
                'empty' => __("-- Sélectionner un circuit de validation --"),
            ]
        ),
        ['legend' => __("Validation")]
    );

echo $this->Html->tag('/div');

// Tab Permissions
echo $this->Html->tag('div#edit-agreement-tab2');
echo $this->Form->control(
    'allow_all_transferring_agencies',
    [
        'label' => __("Autoriser tous les services versants"),
    ]
)
    . $this->Form->control(
        'transferring_agencies._ids',
        [
            'label' => __("Services versants"),
            'options' => $transferring_agencies,
            'data-placeholder' => __("-- Sélectionner un/plusieurs service(s) versant(s) --"),
            'multiple',
        ]
    )
    . $this->Form->control(
        'allow_all_originating_agencies',
        [
            'label' => __("Autoriser tous les services producteurs"),
        ]
    )
    . $this->Form->control(
        'originating_agencies._ids',
        [
            'label' => __("Services producteurs"),
            'options' => $originating_agencies,
            'data-placeholder' => __("-- Sélectionner un/plusieurs service(s) producteur(s) --"),
            'multiple',
        ]
    )
    . $this->Form->control(
        'allow_all_profiles',
        [
            'label' => __("Autoriser tous les profils d'archives"),
        ]
    )
    . $this->Form->control(
        'profiles._ids',
        [
            'label' => __("profils d'archives"),
            'options' => $profiles,
            'data-placeholder' => __("-- Sélectionner un/plusieurs profil(s) d'archives --"),
            'multiple',
        ]
    )
    . $this->Form->control(
        'allow_all_service_levels',
        [
            'label' => __("Autoriser tous les niveaux de service"),
        ]
    )
    . $this->Form->control(
        'service_levels._ids',
        [
            'label' => __("niveaux de service"),
            'options' => $service_levels,
            'data-placeholder' => __("-- Sélectionner un/plusieurs niveau(x) de service(s) --"),
            'multiple',
        ]
    );

echo $this->Html->tag('/div');

// Tab contrôles
echo $this->Html->tag('div#edit-agreement-tab3');
require 'addedit-common.php';
echo $this->Html->tag('/div');
echo $this->Html->tag('div#edit-agreement-tab4');
echo $this->Form->control(
    'notify_delay',
    [
        'label' => __("Délai sans transfert entrant"),
        'type' => 'number',
        'placeholder' => __("Optionnel"),
        'min' => 0,
        'help' => __(
            "Envoi d'une notification par mail si aucun transfert n'est reçu "
            . "pendant le nombre de jours saisi (0 = pas de notification)"
        ),
    ]
);
echo $this->Html->tag('/div');
echo $this->Form->end();
?>
<script>
    AsalaeGlobal.chosen($('#edit-agreement-transferring-agencies-ids'), __("Services versants disponibles"));
    AsalaeGlobal.chosen($('#edit-agreement-originating-agencies-ids'), __("Services producteurs disponibles"));
    AsalaeGlobal.chosen($('#edit-agreement-service-levels-ids'), __("niveaux de service disponibles"));
    AsalaeGlobal.chosen($('#edit-agreement-profiles-ids'), __("Profils d'archives disponibles"));
    var allowedFormatsSelect = $('#edit-agreement-allowed-formats-select');
    AsalaeGlobal.select2(
        allowedFormatsSelect,
        {width: '100%'}
    );
    setTimeout(
        () => allowedFormatsSelect.siblings().find('input').css({width: '100%'})
    );

    $('#form-edit-agreements').tabs();

    var val = $('#edit-agreement-allowed-formats').val();
    allowedFormatsSelect.change(function () {
        var value = $(this).val();
        var json = JSON.stringify(value);
        if (json.indexOf('""') !== -1) {
            value = value.filter(v => v);
            $(this).val(value);
            json = JSON.stringify(value);
        }
        $('#edit-agreement-allowed-formats').val(json);
    });
    $.each(JSON.parse(val ? val : '[]'), function (i, e) {
        allowedFormatsSelect.find("option[value='" + e + "']").prop("selected", true);
    });
    allowedFormatsSelect.trigger("change");

    $('#edit-agreement-allow-all-transferring-agencies').change(function () {
        var select = $('#edit-agreement-transferring-agencies-ids');
        if ($(this).prop('checked')) {
            select.disable().trigger("chosen:updated");
        } else {
            select.enable().trigger("chosen:updated");
        }
    }).change();
    $('#edit-agreement-allow-all-originating-agencies').change(function () {
        var select = $('#edit-agreement-originating-agencies-ids');
        if ($(this).prop('checked')) {
            select.disable().trigger("chosen:updated");
        } else {
            select.enable().trigger("chosen:updated");
        }
    }).change();
    $('#edit-agreement-allow-all-service-levels').change(function () {
        var select = $('#edit-agreement-service-levels-ids');
        if ($(this).prop('checked')) {
            select.disable().trigger("chosen:updated");
        } else {
            select.enable().trigger("chosen:updated");
        }
    }).change();

    $('#edit-agreement-auto-validate').change(function () {
        var select = $('#edit-agreement-proper-chain-id');
        if ($(this).prop('checked')) {
            select.disable().trigger("chosen:updated");
        } else {
            select.enable().trigger("chosen:updated");
        }
    }).change();
    $('#edit-agreement-allow-all-profiles').change(function () {
        var select = $('#edit-agreement-profiles-ids');
        if ($(this).prop('checked')) {
            select.disable().trigger("chosen:updated");
        } else {
            select.enable().trigger("chosen:updated");
        }
    }).change();

    // empêcher d'avoir un accord de versement par défaut désactivé
    var activeCheckbox = $('#edit-agreement-active');
    var isActive;
    $('#edit-agreement-default-agreement').on('change', function () {
        if ($(this).prop('checked')) {
            activeCheckbox
                .on('click', function () {
                    return false;
                })
                .prop('checked', true)
                .addClass('disabled');
        } else {
            activeCheckbox
                .off('click')
                .prop('checked', isActive)
                .removeClass('disabled');
        }
    }).change();
    activeCheckbox.on('change', function () {
        isActive = $(this).prop('checked');
    }).change();

    AsalaeGlobal.colorTabsInError();
</script>
