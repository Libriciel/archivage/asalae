<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Accords de versement"));
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-file-text', __("Accords de versement")))
    . $this->Breadcrumbs->render()
);

$alert = '';
$optionnel = '';
$addable = true;
if ($validation_chains_validate->count() === 0) {
    $optionnel .= $this->Html->tag('li', __("Circuits de validation des transferts conformes"));
}
if ($validation_chains_invalidate->count() === 0) {
    $alert .= $this->Html->tag('li', __("Circuits de validation des transferts non conformes"));
    $addable = false;
}
if ($service_levels->count() === 0) {
    $optionnel .= $this->Html->tag('li', __("Niveaux de service"));
}
if ($profiles->count() === 0) {
    $optionnel .= $this->Html->tag('li', __("Profils d'archives"));
}

$jsTable = $this->Table->getJsTableObject($tableId);
$buttons = [];
if ($this->Acl->check('/agreements/add1')) {
    $buttons[] = $this->ModalForm
        ->create('add-agreement')
        ->modal(__("Ajouter un accord de versement"))
        ->step('/agreements/add1', 'addAgreementStep1')
        ->step('/agreements/add2', 'addAgreementStep2')
        ->step('/agreements/add3', 'addAgreementStep3')
        ->step('/agreements/add4', 'addAgreementStep4')
        ->javascriptCallback('afterAddAgreement(' . $jsTable . ', "Agreements")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un accord de versement"))
        )
        ->generate(
            [
                'class' => 'btn btn-success' . ($addable ? '' : ' disabled'),
                'type' => 'button',
                'disabled' => $addable === false,
            ]
        );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}


if ($alert) {
    echo $this->Html->tag('section.container');
    echo $this->Html->tag('div.alert.alert-warning');
    echo $this->Html->tag('h4', __("Paramétrage manquant"));
    echo $this->Html->tag('ul', $alert);
    if ($optionnel) {
        echo $this->Html->tag('h4', __("Paramétrage optionnel"));
        echo $this->Html->tag('ul', $optionnel);
    }
    echo $this->Html->tag('/div');
    echo $this->Html->tag('/section');
}

echo $this->element('modal', ['idTable' => $tableId]);


echo $this->ModalForm
    ->create('edit-agreement')
    ->modal(__("Modifier un accord de versement"))
    ->javascriptCallback('afterEditAgreement(' . $jsTable . ', "Agreements")')
    ->output(
        'function',
        'actionEditAgreement',
        '/agreements/edit'
    )
    ->generate();

echo $this->ModalView
    ->create('view-agreement')
    ->modal(__("Visualiser un accord de versement"))
    ->output(
        'function',
        'actionViewAgreement',
        '/agreements/view'
    )
    ->generate();

echo $this->Filter->create('service-levels-filter')
    ->saves($savedFilters)
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'identifier',
        [
            'label' => __("Identifiant"),
            'wildcard',
        ]
    )
    ->filter(
        'active',
        [
            'label' => __("Actifs"),
            'options' => [
                '1' => __("Oui"),
                '0' => __("Non"),
            ],
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->filter(
        'default_agreement',
        [
            'label' => __("Defaut"),
            'options' => [
                '1' => __("Oui"),
                '0' => __("Non"),
            ],
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'modified',
        [
            'label' => __("Date de modification"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->generateSection();

require 'ajax_index.php';

?>
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#agreements-section');
    });

    function afterAddAgreement(table, model) {
        return function (content, textStatus, jqXHR) {
            TableGenericAction.afterAdd(table, model)(content, textStatus, jqXHR);
            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true' && content.default_agreement) {
                for (var i = 0; i < table.data.length; i++) {
                    if (table.data[i][model].default_agreement && table.data[i][model].id !== content.id) {
                        table.data[i][model].default_agreement = false;
                        table.generateAll();
                        break;
                    }
                }
            }
        }
    }

    function afterEditAgreement(table, model) {
        return function (content, textStatus, jqXHR) {
            TableGenericAction.afterEdit(table, model)(content, textStatus, jqXHR);
            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true' && content.default_agreement) {
                for (var i = 0; i < table.data.length; i++) {
                    if (table.data[i][model].default_agreement && table.data[i][model].id !== content.id) {
                        table.data[i][model].default_agreement = false;
                        TableGenerator.appendActions(table.data, table.actions);
                        table.generateAll();
                        break;
                    }
                }
            }
        }
    }

    var table = {};
    table = <?=$jsTable?>;

    function actionSetDefaultAgreement(id) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/agreements/set-default')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            success: function () {
                var targetData = table.getDataId(id);
                targetData.default_agreement = true;
                targetData.Agreements.default_agreement = true;
                for (var i = 0; i < table.data.length; i++) {
                    if (table.data[i].id !== id && table.data[i].default_agreement) {
                        table.data[i].default_agreement = false;
                        table.data[i].Agreements.default_agreement = false;
                    }
                }
                table.generateAll();
            }
        })
    }
</script>
