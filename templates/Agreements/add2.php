<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->template('MultiStepForm/add_agreement')->step(2) . '<hr>';

echo $this->Form->create($entity, ['idPrefix' => 'add2-agreement'])
    . $this->Form->control('name', ['type' => 'hidden'])
    . $this->Form->control('identifier', ['type' => 'hidden'])
    . $this->Form->control('description', ['type' => 'hidden'])
    . $this->Form->control('default_agreement', ['type' => 'hidden'])
    . $this->Form->control('active', ['type' => 'hidden'])
    . $this->Form->control('auto_validate', ['type' => 'hidden'])
    . $this->Form->control('proper_chain_id', ['type' => 'hidden'])
    . $this->Form->control('improper_chain_id', ['type' => 'hidden'])
    . $this->Form->control('date_begin', ['type' => 'hidden'])
    . $this->Form->control('date_end', ['type' => 'hidden'])
    . $this->Form->control(
        'allow_all_transferring_agencies',
        [
            'label' => __("Autoriser tous les services versants"),
        ]
    )
    . $this->Form->control(
        'transferring_agencies._ids',
        [
            'label' => __("Services versants"),
            'options' => $transferring_agencies,
            'data-placeholder' => __("-- Sélectionner un/plusieurs service(s) versant(s) --"),
            'multiple',
        ]
    )
    . $this->Form->control(
        'allow_all_originating_agencies',
        [
            'label' => __("Autoriser tous les services producteurs"),
        ]
    )
    . $this->Form->control(
        'originating_agencies._ids',
        [
            'label' => __("Services producteurs"),
            'options' => $originating_agencies,
            'data-placeholder' => __("-- Sélectionner un/plusieurs service(s) producteur(s) --"),
            'multiple',
        ]
    )
    . $this->Form->control(
        'allow_all_profiles',
        [
            'label' => __("Autoriser tous les profils d'archives"),
        ]
    )
    . $this->Form->control(
        'profiles._ids',
        [
            'label' => __("profils d'archives"),
            'options' => $profiles,
            'data-placeholder' => __("-- Sélectionner un/plusieurs profil(s) d'archives --"),
            'multiple',
        ]
    )
    . $this->Form->control(
        'allow_all_service_levels',
        [
            'label' => __("Autoriser tous les niveaux de service"),
        ]
    )
    . $this->Form->control(
        'service_levels._ids',
        [
            'label' => __("niveaux de service"),
            'options' => $service_levels,
            'data-placeholder' => __("-- Sélectionner un/plusieurs niveau(x) de service(s) --"),
            'multiple',
        ]
    )
    . $this->Form->end();

?>
<script>
    AsalaeGlobal.chosen($('#add2-agreement-transferring-agencies-ids'), __("Services versants disponibles"));
    AsalaeGlobal.chosen($('#add2-agreement-originating-agencies-ids'), __("Services producteurs disponibles"));
    AsalaeGlobal.chosen($('#add2-agreement-service-levels-ids'), __("niveaux de service disponibles"));
    AsalaeGlobal.chosen($('#add2-agreement-profiles-ids'), __("Profils d'archives disponibles"));

    $('#add2-agreement-allow-all-transferring-agencies').change(function () {
        var select = $('#add2-agreement-transferring-agencies-ids');
        if ($(this).prop('checked')) {
            select.disable().trigger("chosen:updated");
        } else {
            select.enable().trigger("chosen:updated");
        }
    }).change();
    $('#add2-agreement-allow-all-originating-agencies').change(function () {
        var select = $('#add2-agreement-originating-agencies-ids');
        if ($(this).prop('checked')) {
            select.disable().trigger("chosen:updated");
        } else {
            select.enable().trigger("chosen:updated");
        }
    }).change();
    $('#add2-agreement-allow-all-service-levels').change(function () {
        var select = $('#add2-agreement-service-levels-ids');
        if ($(this).prop('checked')) {
            select.disable().trigger("chosen:updated");
        } else {
            select.enable().trigger("chosen:updated");
        }
    }).change();
    $('#add2-agreement-allow-all-profiles').change(function () {
        var select = $('#add2-agreement-profiles-ids');
        if ($(this).prop('checked')) {
            select.disable().trigger("chosen:updated");
        } else {
            select.enable().trigger("chosen:updated");
        }
    }).change();
</script>
