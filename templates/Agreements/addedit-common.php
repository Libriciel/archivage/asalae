<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->control(
    'allowed_formats_select',
    [
        'label' => __("Formats de fichiers"),
        'data-placeholder' => __("-- Sélectionner un/des format(s) de fichier(s) --"),
        'help' => __("formats autorisés des fichiers des pièces jointes"),
        'options' => $allowed_formats,
        'empty' => true,
        'multiple' => true,
    ]
)
    . $this->Form->control(
        'allowed_formats',
        [
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'max_transfers',
        [
            'label' => __("Nombre de transferts maximum pour une période"),
            'type' => 'number',
            'placeholder' => __("Optionnel"),
            'min' => 1,
        ]
    )
    . $this->Form->control(
        'transfer_period',
        [
            'label' => __("Période du nombre de transferts maximum"),
            'options' => $transfer_periods,
            'help' => __("Optionnel"),
            'empty' => __("-- Sélectionner une période --"),
        ]
    )
    . $this->Html->tag('h4.fieldset', __("Volume maximum des fichiers des pièces jointes"))
    . $this->Form->control(
        'max_size_per_transfer_conv',
        [
            'label' => __("Volume maximum pour un transfert"),
            'type' => 'number',
            'class' => 'with-select',
            'append' => $mult('mult_max_size_per_transfer'),
            'placeholder' => __("Optionnel"),
            'min' => 1,
        ]
    );
