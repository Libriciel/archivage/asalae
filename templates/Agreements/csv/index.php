<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $entity
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$map = function (EntityInterface $entity): EntityInterface {
    $names = [
        'transferring_agencies',
        'originating_agencies',
        'service_levels',
        'profiles',
    ];
    foreach ($names as $name) {
        $n = Hash::extract($entity, "$name.{n}.name");
        $entity->set(
            $name,
            $n ? '- ' . implode("\n- ", $n) : ''
        );
    }

    $formats = array_filter(json_decode($entity->get('allowed_formats')) ?: []);
    if (!$formats && $entity->get('allowed_formats')) {
        $formats = explode(',', $entity->get('allowed_formats'));
    }
    $exts = [];
    $mimes = [];
    $pronoms = [];
    foreach ($formats as $format) {
        if (substr($format, 0, 3) === 'ext') {
            $exts[] = substr($format, 4);
        } elseif (substr($format, 0, 4) === 'mime') {
            $mimes[] = substr($format, 5);
        } elseif (substr($format, 0, 6) === 'pronom') {
            $pronoms[] = substr($format, 7);
        }
    }
    $entity->set('allowed_formats_pronom', $pronoms ? '- ' . implode("\n- ", $pronoms) : '');
    $entity->set('allowed_formats_mime', $mimes ? '- ' . implode("\n- ", $mimes) : '');
    $entity->set('allowed_formats_ext', $exts ? '- ' . implode("\n- ", $exts) : '');

    $entity->setVirtual(['transfer_periodtrad']);
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'name' => __("Nom"),
        'identifier' => __("Identifiant"),
        'active' => __("Actif"),
        'default_agreement' => __("Defaut"),
        'allow_all_transferring_agencies' => __("Autoriser tous les services versants"),
        'transferring_agencies' => __("Services versants autorisés"),
        'allow_all_originating_agencies' => __("Autoriser tous les services producteurs"),
        'originating_agencies' => __("Services producteurs autorisés"),
        'allow_all_service_levels' => __("Autoriser tous les niveaux de service"),
        'service_levels' => __("Niveaux de service autorisés"),
        'allow_all_profiles' => __("Autoriser tous les profils d'archives"),
        'profiles' => __("Profils d'archives autorisés"),
        'auto_validate' => __("Validation auto des transferts conformes"),
        'proper_chain.name' => __("Circuit de validation des transferts conformes"),
        'improper_chain.name' => __("Circuit de validation des transferts non conformes"),
        'date_begin' => __("Date de début de validité"),
        'date_end' => __("Date de fin de validité"),
        'allowed_formats_pronom' => __("Formats autorisés (pronoms)"),
        'allowed_formats_mime' => __("Formats autorisés (mime)"),
        'allowed_formats_ext' => __("Formats autorisés (extensions)"),
        'max_transfers' => __("Transferts maximum / période"),
        'max_size_per_transfer' => __("Taille max d'un transfert"),
        'transfer_periodtrad' => __("Période de limitation des transferts"),
        'created' => __("Date de création"),
        'modified' => __("Date de modification"),
    ],
    $results,
    $map
);
