<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->MultiStepForm->template('MultiStepForm/add_agreement')->step(4) . '<hr>';

echo $this->Form->create($entity, ['idPrefix' => 'add4-agreement']);
echo $this->Form->control(
    'notify_delay',
    [
        'label' => __("Délai sans transfert entrant"),
        'type' => 'number',
        'placeholder' => __("Optionnel"),
        'min' => 0,
        'help' => __(
            "Envoi d'une notification par mail si aucun transfert n'est reçu "
            . "pendant le nombre de jours saisi (0 = pas de notification)"
        ),
    ]
);
echo $this->Form->end();
