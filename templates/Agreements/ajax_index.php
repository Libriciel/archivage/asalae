<?php

/**
 * @var Asalae\View\AppView $this
 */
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Agreements.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'Agreements.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'Agreements.active' => [
                'label' => __("Actif"),
                'type' => 'boolean',
                'callback' => 'TableHelper.boolean',
                'filter' => [
                    'active[0]' => [
                        'id' => 'filter-active-0',
                        'label' => false,
                        'aria-label' => __("Actif"),
                        'options' => [
                            '1' => __("Oui"),
                            '0' => __("Non"),
                        ],
                    ],
                ],
            ],
            'Agreements.default_agreement' => [
                'label' => __("Defaut"),
                'type' => 'boolean',
                'filter' => [
                    'default_agreement[0]' => [
                        'id' => 'filter-default_agreement-0',
                        'label' => false,
                        'aria-label' => __("Defaut"),
                        'options' => [
                            '1' => __("Oui"),
                            '0' => __("Non"),
                        ],
                    ],
                ],
            ],
            'Agreements.allow_all' => [
                'label' => __("Autoriser tous les"),
                'target' => 'Agreements',
                'display' => false,
                'thead' => [
                    'allow_all_transferring_agencies' => ['label' => 'Services versants', 'type' => 'boolean'],
                    'allow_all_originating_agencies' => ['label' => 'Services producteurs', 'type' => 'boolean'],
                    'allow_all_service_levels' => ['label' => 'niveaux de service', 'type' => 'boolean'],
                ],
            ],
            'Agreements.transferring_agencies' => [
                'label' => __("Services versants"),
                'display' => false,
                'thead' => [
                    'name' => ['label' => __("Nom")],
                ],
            ],
            'Agreements.originating_agencies' => [
                'label' => __("Services producteurs"),
                'display' => false,
                'thead' => [
                    'name' => ['label' => __("Nom")],
                ],
            ],
            'Agreements.service_levels' => [
                'label' => __("niveaux de service"),
                'display' => false,
                'thead' => [
                    'name' => ['label' => __("Nom")],
                    'identifier' => ['label' => __("Identifiant")],
                ],
            ],
            'Agreements.chains' => [
                'label' => __("Circuits de validation"),
                'display' => false,
                'target' => 'Agreements',
                'thead' => [
                    'proper_chain.name' => ['label' => __("Conformes")],
                    'improper_chain.name' => ['label' => __("Non conformes")],
                ],
            ],
            'Agreements.auto_validate' => [
                'label' => __("Validation auto"),
                'type' => 'boolean',
                'display' => false,
            ],
            'Agreements.date_begin' => [
                'label' => __("Date de début de validité"),
                'type' => 'date',
                'display' => false,
                'order' => 'date_begin',
            ],
            'Agreements.date_end' => [
                'label' => __("Date de fin de validité"),
                'type' => 'date',
                'display' => false,
                'order' => 'date_end',
            ],
            'Agreements.allowed_formats' => [
                'label' => __("Formats autorisés"),
                'display' => false,
                'callback' => 'TableHelper.array',
            ],
            'Agreements.max_transfers' => [
                'label' => __("Transferts maximum / période"),
                'display' => false,
                'order' => 'max_transfers',
            ],
            'Agreements.max_size_per_transfer' => [
                'label' => __("Taille max d'un transfert"),
                'display' => false,
                'order' => 'max_size_per_transfer',
                'callback' => 'TableHelper.readableBytes',
            ],
            'Agreements.transfer_periodtrad' => [
                'label' => __("Période de limitation des transferts"),
                'display' => false,
            ],
            'Agreements.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'Agreements.modified' => [
                'label' => __("Date de modification"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'modified',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'modified[0]' => [
                        'id' => 'filter-modified-0',
                        'label' => __("Date de modification"),
                        'prepend' => $this->Input->operator('dateoperator_modified[0]', '>='),
                        'append' => $this->Date->picker('#filter-modified-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'modified[1]' => [
                        'id' => 'filter-modified-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_modified[1]', '<='),
                        'append' => $this->Date->picker('#filter-modified-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'Agreements.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionSetDefaultAgreement({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-bookmark'),
                'title' => $title = __("Définir ''{0}'' comme accord de versement par défaut", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/agreements/set-default'),
                'displayEval' => '!data[{index}].Agreements.default_agreement && data[{index}].Agreements.active',
                'params' => ['Agreements.id', 'Agreements.name'],
            ],
            [
                'onclick' => "actionViewAgreement({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Visualiser {0}", '{1}'),
                'aria-label' => __("Visualiser {0}", '{1}'),
                'display' => $this->Acl->check('/agreements/view'),
                'params' => ['Agreements.id', 'Agreements.name'],
            ],
            [
                'onclick' => "actionEditAgreement({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'display' => $this->Acl->check('/agreements/edit'),
                'params' => ['Agreements.id', 'Agreements.name'],
            ],
            function ($table) {
                $view = $this;
                $deleteUrl = $view->Url->build('/Agreements/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction({$table->tableObject}, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'display' => $this->Acl->check('/agreements/delete'),
                    'displayEval'
                    => 'data[{index}].Agreements.deletable && !data[{index}].Agreements.default_agreement',
                    'params' => ['Agreements.id', 'Agreements.name'],
                ];
            },
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => 'agreements-section',
        'title' => __("Liste des accords de versement"),
        'table' => $table,
    ]
);
