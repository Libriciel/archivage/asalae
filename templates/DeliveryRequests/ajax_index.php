<?php

/**
 * @var Asalae\View\AppView $this
 */

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'DeliveryRequests.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'DeliveryRequests.comment' => [
                'label' => __("Commentaire"),
            ],
            'DeliveryRequests.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'DeliveryRequests.archive_units' => [
                'label' => __("Unités d'archives"),
                'callback' => 'TableHelper.ul("name")',
                'escape' => false,
                'filter' => [
                    'archive_unit_identifier[0]' => [
                        'id' => 'filter-archive_unit_identifier-0',
                        'label' => __("Identifiant"),
                    ],
                    'archive_unit_name[0]' => [
                        'id' => 'filter-archive_unit_name-0',
                        'label' => __("Nom"),
                    ],
                ],
            ],
            'DeliveryRequests.statetrad' => [
                'label' => __("État"),
                'filter' => [
                    'state[0]' => [
                        'id' => 'filter-state-0',
                        'label' => false,
                        'aria-label' => __("Etat"),
                        'options' => $states,
                    ],
                ],
            ],
        ]
        + (in_array($this->getRequest()->getParam('action'), ['indexPreparating', 'indexMy'])
            ? []
            : [
                'DeliveryRequests.created_user.username' => [
                    'label' => __("Créée par"),
                    'escape' => false,
                    'order' => 'created_user.username',
                    'filterCallback' => 'AsalaeFilter.datepickerCallback',
                    'filter' => [
                        'created_user[0]' => [
                            'id' => 'filter-created_user-0',
                            'label' => __("Créée par"),
                            'options' => $created_users,
                        ],
                    ],
                ],
            ])
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'DeliveryRequests.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewDeliveryRequest({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Visualiser"),
                'display' => $this->Acl->check('/DeliveryRequests/view'),
                'params' => ['DeliveryRequests.id', 'DeliveryRequests.identifier'],
            ],
            [
                'href' => "/DeliveryRequests/download-pdf/{0}",
                'download' => '{2}',
                'target' => '_blank',
                'data-action' => __("Télécharger le PDF"),
                'label' => $this->Fa->i('fa-file-pdf'),
                'title' => $title = __("Télécharger le PDF de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/DeliveryRequests/download-pdf'),
                'displayEval' => 'data[{index}].DeliveryRequests.state !== "creating"',
                'params' => ['DeliveryRequests.id', 'DeliveryRequests.identifier', 'DeliveryRequests.pdf_basename'],
            ],
            [
                'onclick' => "actionEditDeliveryRequest({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/DeliveryRequests/edit'),
                'displayEval' => 'data[{index}].DeliveryRequests.editable '
                    . '&& data[{index}].DeliveryRequests.created_user_id === PHP.user_id',
                'params' => ['DeliveryRequests.id', 'DeliveryRequests.identifier'],
            ],
            [
                'data-callback' => "sendDeliveryRequest($jsTable, {0})",
                'type' => 'button',
                'class' => 'btn-link send',
                'display' => $this->Acl->check('/DeliveryRequests/send'),
                'displayEval' => 'data[{index}].DeliveryRequests.sendable '
                    . '&& data[{index}].DeliveryRequests.created_user_id === PHP.user_id',
                'label' => $this->Fa->charte('Envoyer', '', 'text-success'),
                'title' => __("Envoyer {0}", '{1}'),
                'aria-label' => __("Envoyer {0}", '{1}'),
                'confirm' => __(
                    "Une fois la demande de communication envoyée, il ne "
                    . "sera plus possible de la modifier. Voulez-vous continuer ?"
                ),
                'params' => ['DeliveryRequests.id', 'DeliveryRequests.identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/DeliveryRequests/delete')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Supprimer"),
                'displayEval' => 'data[{index}].DeliveryRequests.deletable '
                    . '&& data[{index}].DeliveryRequests.created_user_id === PHP.user_id',
                'confirm' => __("Êtes-vous sûr de vouloir supprimer cette demande de communication ?"),
                'params' => ['DeliveryRequests.id', 'DeliveryRequests.identifier'],
            ],
        ]
    );

$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des demandes de communication"),
        'table' => $table,
    ]
);
