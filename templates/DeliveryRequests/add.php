<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Flash->render();
if (!empty($fatalError)) {
    return;
}

echo $this->Form->create($entity, ['idPrefix' => 'add-delivery-request']);
$selectId = 'add-delivery-request-archive-units-ids';
$idDivDerogation = 'add-derogation-alert';
$idDivValidationChains = 'add-validation-chains';

require 'addedit-common.php';
echo $this->Form->end();
