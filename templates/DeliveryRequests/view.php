<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $entity
 */

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

?>
<script>
    function validationActionToIcon(value) {
        switch (value) {
            case 'validate':
                return '<?=$this->Fa->i('fa-check text-success')?>';
            case 'invalidate':
                return '<?=$this->Fa->i('fa-times text-danger')?>';
            case 'stepback':
                return '<?=$this->Fa->i('fa-reply')?>';
            default:
                return value;
        }
    }
</script>
<?php
$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($entity->get('identifier')));
$infos .= $this->Html->tag('p', h($entity->get('comment')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Demande de communication"));
$infos .= $this->ViewTable->generate(
    $entity,
    [
        __("Identifiant") => 'identifier',
        __("Etat") => 'statetrad',
        __("Commentaire") => 'comment|nl2br',
        __("Demande de dérogation") => 'derogation',
        __("Entité demandeur") => 'requester.name',
        __("Agent demandeur") => 'created_user.username',
        __("Date de création") => 'created',
        __("Date de modification") => 'modified',
    ]
);
$infos .= $this->Html->tag('/article');

$tabs = $this->Tabs->create('tabs-view-delivery-request', ['class' => 'row no-padding']);
$tabs->add(
    'tab-view-delivery-request-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);

$tableUnits = $this->Table
    ->create('view-delivery-request-archive-units', ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'delivery-requests',
            'action' => 'paginate-archive-units',
            $entity->id,
            '?' => [
                'sort' => 'name',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'archival_agency_identifier' => [
                'label' => __("Identifiant"),
            ],
            'name' => [
                'label' => __("Nom"),
            ],
            'description' => [
                'label' => __("Description"),
            ],
            'dates' => [
                'label' => __("Dates extrêmes"),
            ],
            'original_total_count' => [
                'label' => __("Nombre de fichiers"),
            ],
            'original_total_size' => [
                'label' => __("Taille des fichiers"),
                'callback' => 'TableHelper.readableBytes',
            ],
        ]
    )
    ->data($entity->get('archive_units') ?: [])
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "publicDescriptionArchive({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/description-public'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'href' => $this->Url->build('/archive-units/display-xml-public') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'data-renewable' => true,
                'display' => $this->Acl->check('/archive-units/display-xml-public'),
                'displayEval' => '!data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
        ]
    );
$tabs->add(
    'tab-view-delivery-request-archive-units',
    $this->Fa->i('fa-file-code-o', __("Unités d'archives")),
    $this->AjaxPaginator->create('pagination-delivery-request-archive-units')
        ->url($url)
        ->table($tableUnits)
        ->count($unitsCount)
        ->generateTable()
);

/** @var EntityInterface $process */
$process = Hash::get($entity, 'validation_processes.0');
if ($process) {
    $histories = $this->Html->tag('h4', __("Historique de validation"));
    /** @var EntityInterface $history */
    foreach (Hash::get($process, 'validation_histories') as $history) {
        $history->setVirtual(['actor_info']);
    }
    $histories .= $this->Table
        ->create('validation-histories', ['class' => 'table table-striped table-hover smart-td-size'])
        ->fields(
            [
                'action' => [
                    'label' => __("Décision"),
                    'callback' => 'validationActionToIcon',
                    'titleEval' => 'actiontrad',
                ],
                'validation_actor.validation_stage.name' => [
                    'label' => __("Etape de validation"),
                ],
                'actor_info' => [
                    'label' => __("Auteur de la validation"),
                    'escape' => false,
                ],
                'created' => [
                    'label' => __("Date"),
                    'type' => 'datetime',
                ],
                'comment' => [
                    'label' => __("Commentaire"),
                ],
            ]
        )
        ->data(Hash::get($process, 'validation_histories'))
        ->params(
            [
                'identifier' => 'id',
            ]
        );

    $info = [
        __("Etat de la validation") => 'processedtrad',
        __("Décision finale") => 'validatedtrad',
        __("Circuit de validation") => 'validation_chain.name',
    ];
    if (!$process->get('processed')) {
        $info[__("Etape de validation")] = 'steptrad';
        $info[__("Doit être validé par")] = 'current_stage.validation_actors.{n}.actor_info|raw';
    }

    $tabs->add(
        'tab-delivery-request-validation',
        $this->Fa->i('fa-check-square-o', __("Validation")),
        // ---------------------------------------------------------------------
        $this->Html->tag('h3', __("Validation de {0}", h($entity->get('identifier'))))
        . $this->ViewTable->generate($process, $info)
        . $histories
    );
}

echo $tabs;
