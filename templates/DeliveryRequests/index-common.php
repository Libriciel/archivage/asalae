<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->element('modal', ['idTable' => $tableId]);

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
echo $this->ModalForm
    ->create('edit-delivery-request', ['size' => 'large'])
    ->modal(__("Edition d'une demande de communication"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "DeliveryRequests")')
    ->output('function', 'actionEditDeliveryRequest', '/delivery-requests/edit')
    ->generate();

echo $this->ModalView
    ->create('view-delivery-request', ['size' => 'large'])
    ->modal(__("Visualisation d'une demande de communication"))
    ->output('function', 'actionViewDeliveryRequest', '/delivery-requests/view')
    ->generate();

echo $this->ModalView->create('search-public-description-archive', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une unité d'archive"))
    ->output('function', 'publicDescriptionArchive', '/ArchiveUnits/description-public')
    ->generate();


$filters = $this->Filter->create('delivery-request-filter')
    ->saves($savedFilters)
    ->filter(
        'archive_unit_identifier',
        [
            'label' => __("Identifiant d'une unité d'archives"),
            'wildcard',
        ]
    )
    ->filter(
        'archive_unit_name',
        [
            'label' => __("Nom d'une unité d'archives"),
            'wildcard',
        ]
    )
    ->filter(
        'created_user',
        [
            'label' => __("Créée par"),
            'options' => $created_users,
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->filter(
        'identifier',
        [
            'label' => __("Identifiant"),
            'wildcard',
        ]
    )
    ->filter(
        'state',
        [
            'label' => __("Etats"),
            'options' => $states,
            'multiple' => true,
        ]
    );

if (!in_array($this->getRequest()->getParam('action'), ['indexPreparating', 'indexMy'])) {
    $filters
        ->filter(
            'created_user',
            [
                'label' => __("Créée par"),
                'options' => $created_users,
            ]
        );
}

echo $filters->generateSection();

require 'ajax_index.php';
?>
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    function sendDeliveryRequest(table, id, goToMy = false) {
        var tr = $(table.table).find('tr[data-id="' + id + '"]');
        tr.find('button').addClass('deleting').addClass('gray').addClass('disabled').disable();
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/delivery-requests/send')?>/' + id,
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                table.removeDataId(id);
                $(table.table)
                    .find('tr[data-id="' + id + '"]')
                    .fadeOut(400, function () {
                        $(this).remove();
                        table.decrementPaginator();
                    });
                if (goToMy) {
                    AsalaeGlobal.interceptedLinkToAjax('<?=$this->Url->build('/delivery-requests/index-my')?>');
                }
            }
        }, function () {
            $('html').addClass('ajax-loading');
        }, function () {
            $('html').removeClass('ajax-loading');
        });
    }
</script>
