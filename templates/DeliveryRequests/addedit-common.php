<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->control(
    'comment',
    [
        'label' => __("Commentaire"),
    ]
);
echo $this->Form->control(
    'archive_units._ids',
    [
        'label' => __("Unités d'archives"),
        'multiple' => true,
        'options' => $archiveUnits,
        'required' => true,
    ]
);

$msg = $derogation
    ? $this->Html->tag(
        'div.alert.alert-warning',
        __("Demande de dérogation: {0}", $this->Html->tag('b', __("Oui")))
    )
    : $this->Html->tag(
        'div.alert.alert-success',
        __("Demande de dérogation: {0}", $this->Html->tag('b', __("Non")))
    );
echo $this->Html->tag('div#' . $idDivDerogation, $msg);

if ($chooseValidationChain) {
    echo $this->Form->control(
        'requester_id',
        [
            'label' => __("Demandeur"),
            'options' => $requesters,
            'default' => $requester->id,
        ]
    );
    echo $this->Html->tag(
        'div#' . $idDivValidationChains,
        $this->Form->control(
            'validation_chain_id',
            [
                'label' => __("Circuit de validation"),
                'options' => $chains,
                'default' => $defaultChain,
            ]
        ),
        ['style' => $derogation ? '' : 'display: none']
    );
}
?>
<script>
    var selectIds = $('#<?=$selectId?>');
    AsalaeGlobal.select2(selectIds);
    selectIds.on('change', function () {
        var value = $(this).val();
        var alert = $('#<?=$idDivDerogation?>');
        var div = $('#<?=$idDivValidationChains?>');
        var values = selectIds.val().join(',');
        if (values.length === 0) {
            $('.modal:visible form').get(0).reportValidity();
            return false;
        }
        AsalaeLoading.ajax(
            {
                url: '<?=$this->Url->build('/DeliveryRequests/derogationNeeded')?>',
                method: 'post',
                data: {archive_units_ids: values},
                headers: {Accept: 'application/json'},
                success: function (content) {
                    if (content.derogation) {
                        alert.find('.alert')
                            .removeClass('alert-success')
                            .addClass('alert-warning')
                            .find('b')
                            .text(__("Oui"));
                        div.show();
                    } else {
                        alert.find('.alert')
                            .removeClass('alert-warning')
                            .addClass('alert-success')
                            .find('b')
                            .text(__("Non"));
                        div.hide();
                    }
                }
            },
            function () {
                alert.find('.alert b')
                    .html('<?=$this->Fa->i('fa-spinner faa-spin animated')?>');
            },
            function () {
                alert.find('.alert b i.fa-spinner').remove();
            }
        )
    });
</script>
