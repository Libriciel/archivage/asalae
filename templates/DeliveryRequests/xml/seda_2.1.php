<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>

<ArchiveDeliveryRequest xmlns="fr:gouv:culture:archivesdefrance:seda:v2.1">
    <?= $comment ? "    <Comment>$comment</Comment>" . PHP_EOL : '' ?>
    <?php
    // insertion des metadonnées de volumétrie dans un commentaire
    echo '    <Comment>' . __("Volumétrie de la demande de communication") . PHP_EOL;
    $metadata = [
        'archive_units_count',
        'original_count',
        'original_size',
    ];
    foreach ($metadata as $meta) {
        echo "        $meta: {$deliveryRequest->get($meta)}\n";
    }
    echo '    </Comment>' . PHP_EOL;

    // insertion des metadonnées des archive_units
    foreach ($queryArchiveUnits as $au) {
        echo '    <Comment>' . PHP_EOL;
        $metadata = [
            'archival_agency_identifier',
            'name',
            'original_total_count',
            'original_total_size',
        ];
        foreach ($metadata as $meta) {
            $value = htmlspecialchars((string)$au[$meta], ENT_XML1);
            echo "        $meta: $value\n";
        }
        echo '    </Comment>' . PHP_EOL;
    }
    ?>
    <Date><?= $date ?></Date>
    <MessageIdentifier><?= $identifier ?></MessageIdentifier>
    <CodeListVersions></CodeListVersions>
    <Derogation><?= $derogation ? 'true' : 'false' ?></Derogation>
    <?php
    foreach ($queryArchiveUnits as $au) {
        $identifier = htmlspecialchars($au['archival_agency_identifier'], ENT_XML1, 'UTF-8');
        echo '        <UnitIdentifier>' . $identifier . '</UnitIdentifier>' . PHP_EOL;
    }
    ?>
    <ArchivalAgency>
        <Identifier><?= $archivalAgency ?></Identifier>
    </ArchivalAgency>
    <Requester>
        <Identifier><?= $requester ?></Identifier>
    </Requester>
</ArchiveDeliveryRequest>