<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create($entity, ['idPrefix' => 'edit-delivery-request']);
$selectId = 'edit-delivery-request-archive-units-ids';
$idDivDerogation = 'edit-derogation-alert';
$idDivValidationChains = 'add-validation-chains';

require 'addedit-common.php';
echo $this->Form->end();
