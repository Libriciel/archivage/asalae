<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->MultiStepForm->template('MultiStepForm/add_outgoing_transfer')->step(1) . '<hr>';

$form = $this->Html->tag('article');
$form .= $this->Html->tag('header.bottom-space');
$form .= $this->Html->tag('h3', __("Nouvelle demande de transfert sortant"));
$form .= $this->Html->tag('/header');

$form .= $this->Html->tag('h4', __("Volumétrie suite à l'application des filtres de recherche"));
$form .= $this->ViewTable->generate(
    $sums,
    [
        __("Nombre d'archives") => 'au_count',
        __("Nombre de fichiers") => 'documents_count',
        __("Volumétrie des données") => 'documents_size|toReadableSize',
    ]
);

$form .= $this->Form->create($entity, ['idPrefix' => 'add1-outgoing-transfer']);
$form .= $this->Form->control(
    'archiving_system_id',
    [
        'label' => __("Système d'archivage électronique"),
        'options' => $archivingSystemsList,
        'empty' => __("-- Sélectionnez un système d'archivage électronique"),
        'required' => true,
    ]
);
$form .= $this->Form->control(
    'filters',
    [
        'type' => 'hidden',
    ]
);

foreach ($archivingSystems as $archivingSystem) {
    $form .= $this->Html->tag(
        'div',
        $this->ViewTable->generate(
            $archivingSystem,
            [
                __("Nom du Système d'archivage électronique") => 'name',
                __("Description") => 'description',
                __("Url") => 'url|htmlEntities',
                __("Identifiant de connexion") => 'username',
            ]
        ),
        [
            'id' => 'table-add1-archiving-system-' . $archivingSystem->id,
            'style' => 'display: none',
            'class' => 'table-archiving-system',
        ]
    );
}

echo $form;
echo $this->Form->end();
echo $this->Html->tag('/article');
?>
<script>
    $('#add1-outgoing-transfer-archiving-system-id').on('change', function () {
        $('.table-archiving-system').hide();
        var value = $(this).val();
        if (value) {
            $('#table-add1-archiving-system-' + value).show();
        }
    });
</script>
