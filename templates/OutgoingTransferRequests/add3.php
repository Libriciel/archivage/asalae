<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $outgoingTransferRequest
 */

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;

echo $this->MultiStepForm->template('MultiStepForm/add_outgoing_transfer')->step(3) . '<hr>';

$jsTable = $this->Table->getJsTableObject($jsTableId);

$tableUnits = $this->Table
    ->create($jsTableId, ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'outgoing-transfer-requests',
            'action' => 'paginate-archive-units',
            $outgoingTransferRequest->id,
            '?' => [
                'sort' => 'archival_agency_identifier',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'archival_agency_identifier' => [
                'label' => __("Identifiant"),
            ],
            'name' => [
                'label' => __("Nom"),
            ],
            'description' => [
                'label' => __("Description"),
            ],
            'dates' => [
                'label' => __("Dates extrêmes"),
            ],
            'original_total_count' => [
                'label' => __("Nombre de fichiers"),
            ],
            'original_total_size' => [
                'label' => __("Taille des fichiers"),
                'callback' => 'TableHelper.readableBytes',
            ],
        ]
    )
    ->data($outgoingTransferRequest->get('archive_units'))
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewUnitDescription({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/description'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'href' => $this->Url->build('/archive-units/display-xml') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/display-xml'),
                'displayEval' => '!data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'data-callback' => sprintf(
                    "actionRemoveArchiveUnit(%s, '%s')('{0}', false)",
                    $jsTable,
                    $this->Url->build(
                        '/outgoing-transfer-requests/removeArchiveUnit'
                    ) . '/' . $outgoingTransferRequest->id
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Retirer cette unité d'archives de la demande de transfert sortant ?"),
                'displayEval' => 'data.length > 1',
                'params' => ['id', 'archival_agency_identifier'],
            ],
        ]
    );

echo $this->AjaxPaginator->create($paginatorId)
    ->url($url)
    ->table($tableUnits)
    ->count($unitsCount)
    ->generateTable();

echo $this->Form->create($outgoingTransferRequest, ['idPrefix' => 'add3-outgoing-transfer-request']);
echo $this->Form->control('id', ['type' => 'hidden']);
echo $this->Form->end();
?>
<script>
    function actionRemoveArchiveUnit(table, url) {
        return function (id) {
            $(table.table).off('.jstable').one('deleted.tablejs', function () {
                table.generateAll();
            });
            TableGenericAction.deleteAction(table, url)(id, false);
        }
    }

    // suppression de la demande si on appuis sur annuler
    $('#add-outgoing-transfer-request-3').find('.modal-footer button.cancel')
        .off('.delete-add')
        .on('click.delete-add', function () {
            $.ajax({
                url: '<?=$this->Url->build('/outgoing-transfer-requests/delete/' . $outgoingTransferRequest->id)?>',
                method: 'DELETE'
            });
        });
</script>
