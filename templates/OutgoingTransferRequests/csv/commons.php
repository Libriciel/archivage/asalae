<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

$map = function (EntityInterface $entity): EntityInterface {
    $archiveUnits = [];
    $aus = TableRegistry::getTableLocator()->get('ArchiveUnits')
        ->find()
        ->innerJoinWith('ArchiveUnitsOutgoingTransferRequests')
        ->where(['otr_id' => $entity->id])
        ->orderBy(['archival_agency_identifier'])
        ->limit(10)
        ->toArray();
    $entity->set('archive_units', $aus);

    foreach (Hash::get($entity, 'archive_units', []) as $archiveUnit) {
        $archiveUnits[] = Hash::get($archiveUnit, 'name');
    }
    $entity->set(
        'archive_units',
        $archiveUnits ? '- ' . implode("\n- ", $archiveUnits) : ''
    );
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'identifier' => __("Identifiant"),
        'comment' => __("Commentaire"),
        'created' => __("Date de création"),
        'archive_units' => __("Unités d'archives"),
        'statetrad' => __("Etat"),
    ],
    $results,
    $map
);
