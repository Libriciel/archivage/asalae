<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->element('modal', ['idTable' => $tableId]);

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
echo $this->ModalForm
    ->create('edit-outgoing-transfer-request', ['size' => 'modal-xxl'])
    ->modal(__("Edition d'une demande de transferts sortants"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "OutgoingTransferRequests")')
    ->output('function', 'actionEditOutgoingTransferRequest', '/outgoing-transfer-requests/edit')
    ->generate();

echo $this->ModalView
    ->create('view-outgoing-transfer-request', ['size' => 'modal-xxl'])
    ->modal(__("Visualisation d'une demande de transferts sortants"))
    ->output('function', 'actionViewOutgoingTransferRequest', '/outgoing-transfer-requests/view')
    ->generate();

echo $this->ModalView->create('search-public-description-archive', ['size' => 'modal-xxl'])
    ->modal(__("Description d'une unité d'archive"))
    ->output('function', 'viewUnitDescription', '/ArchiveUnits/description')
    ->generate();

$filters = $this->Filter->create('outgoing-transfer-request-filter')
    ->saves($savedFilters)
    ->filter(
        'archive_unit_identifier',
        [
            'label' => __("Identifiant d'une unité d'archives"),
            'wildcard',
        ]
    )
    ->filter(
        'archive_unit_name',
        [
            'label' => __("Nom d'une unité d'archives"),
            'wildcard',
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->filter(
        'identifier',
        [
            'label' => __("Identifiant"),
            'wildcard',
        ]
    )
    ->filter(
        'state',
        [
            'label' => __("Etats"),
            'options' => $states,
            'multiple' => true,
        ]
    )
    ->filter(
        'originating_agency_id',
        [
            'label' => __("Service producteur"),
            'options' => $originating_agencies,
            'empty' => __("-- Sélectionner un service producteur --"),
        ]
    )
    ->filter(
        'transferring_agency_id',
        [
            'label' => __("Service versant"),
            'options' => $transferring_agencies,
            'empty' => __("-- Sélectionner un service versant --"),
        ]
    )
    ->filter(
        'profile_id',
        [
            'label' => __("Profil d'archives"),
            'options' => $profiles,
            'empty' => __("-- Sélectionner un profil d'archives --"),
        ]
    )
    ->filter(
        'agreement_id',
        [
            'label' => __("Accords de versement"),
            'options' => $agreements,
            'empty' => __("-- Sélectionner un accord de versement --"),
        ]
    );

if (!in_array($this->getRequest()->getParam('action'), ['indexPreparating', 'indexMy', 'indexProcessed'])) {
    $filters
        ->filter(
            'created_user',
            [
                'label' => __("Créée par"),
                'options' => $created_users,
            ]
        );
}

echo $filters->generateSection();

require 'ajax_index.php';
?>
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    function sendOutgoingTransferRequest(table, id) {
        var tr = $(table.table).find('tr[data-id="' + id + '"]');
        tr.find('button').addClass('deleting').addClass('gray').addClass('disabled').disable();
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/outgoing-transfer-requests/send')?>/' + id,
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                table.removeDataId(id);
                $(table.table)
                    .find('tr[data-id="' + id + '"]')
                    .fadeOut(400, function () {
                        $(this).remove();
                        table.decrementPaginator();
                    });
            }
        }, function () {
            $('html').addClass('ajax-loading');
        }, function () {
            $('html').removeClass('ajax-loading');
        });
    }

    function actionResend(table, id) {
        var tr = $(table.table).find('tr[data-id="' + id + '"]');
        tr.find('button').addClass('deleting').addClass('gray').addClass('disabled').disable();
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/outgoing-transfer-requests/resend')?>/' + id,
            headers: {
                Accept: 'application/json'
            },
            success: function (data) {
                var targetData = table.getDataId(id);
                targetData.state = data.state;
                targetData.statetrad = data.statetrad;
                targetData.resendable = data.resendable;
                targetData.OutgoingTransferRequests.state = data.state;
                targetData.OutgoingTransferRequests.statetrad = data.statetrad;
                targetData.OutgoingTransferRequests.resendable = data.resendable;
                table.generateAll();
            }
        }, function () {
            $('html').addClass('ajax-loading');
        }, function () {
            $('html').removeClass('ajax-loading');
        });
    }

    function scheduleDestruction(id) {
        var table = {};
        table = <?=$jsTable?>;
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/OutgoingTransferRequests/scheduleArchiveDestruction')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            success: function (data) {
                var targetData = table.getDataId(id);
                targetData.state = data.state;
                targetData.statetrad = data.statetrad;
                targetData.OutgoingTransferRequests.state = data.state;
                targetData.OutgoingTransferRequests.statetrad = data.statetrad;
                table.generateAll();
            }
        })
    }

    function cancelDestruction(id) {
        var table = {};
        table = <?=$jsTable?>;
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/OutgoingTransferRequests/cancelArchiveDestruction')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            success: function (data) {
                var targetData = table.getDataId(id);
                targetData.state = data.state;
                targetData.statetrad = data.statetrad;
                targetData.OutgoingTransferRequests.state = data.state;
                targetData.OutgoingTransferRequests.statetrad = data.statetrad;
                table.generateAll();
            }
        })
    }
</script>
