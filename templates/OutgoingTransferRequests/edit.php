<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

$form = $this->Html->tag('article');
$form .= $this->Html->tag('header.bottom-space');
$form .= $this->Html->tag('h3', __("Nouvelle demande de transfert sortant"));
$form .= $this->Html->tag('/header');

if (isset($exception)) {
    $message = $exception->getMessage();
    if ($exception->getCode() === 403) {
        $message = __(
            "L'utilisateur {0} n'a pas les permissions d'accès nécessaire"
            . " pour envoyer un transfert sortant vers le SAE cible",
            h($archivingSystem->get('username'))
        );
    }
    echo $this->Html->tag(
        'div.alert.alert-danger',
        __("Erreur {0}: {1}", $exception->getCode(), $message)
    );
    return;
}

$form .= $this->Html->tag(
    'div',
    $this->ViewTable->generate(
        $archivingSystem,
        [
            __("Nom du Système d'archivage électronique") => 'name',
            __("Description") => 'description',
            __("Url") => 'url|htmlEntities',
            __("Identifiant de connexion") => 'username',
        ]
    ),
    [
        'id' => 'table-edit-archiving-system-' . $archivingSystem->id,
    ]
);

$form .= $this->Form->create($entity, ['idPrefix' => 'edit-outgoing-transfer']);
$form .= $this->Form->control('archiving_system_id', ['type' => 'hidden']);
$form .= $this->Form->control('filters', ['type' => 'hidden']);
$form .= $this->Form->control('archival_agency_identifier', ['type' => 'hidden']);
$form .= $this->Form->control('archival_agency_name', ['type' => 'hidden']);
$form .= $this->Form->control('transferring_agency_identifier', ['type' => 'hidden']);
$form .= $this->Form->control('transferring_agency_name', ['type' => 'hidden']);
$form .= $this->Form->control('originating_agency_identifier', ['type' => 'hidden']);
$form .= $this->Form->control('originating_agency_name', ['type' => 'hidden']);
$form .= $this->Form->control(
    'comment',
    [
        'label' => __("Commentaire"),
    ]
);
$form .= $this->Form->control(
    'archival_agency',
    [
        'label' => __("Service d'archives"),
        'options' => [
            'fake' => $entity->get('archival_agency_identifier')
                . ' - ' . $entity->get('archival_agency_name'),
        ],
        'readonly',
    ]
);
$form .= $this->Form->control(
    'transferring_agency',
    [
        'label' => __("Service versant"),
        'options' => $transferring_agencies,
        'empty' => __("-- Sélectionner un service versant --"),
        'help' => __(
            "Laisser vide si vous souhaitez utiliser le service versant indiqué dans les transferts originaux"
        ),
        'default' => $entity->get('transferring_agency_identifier'),
    ]
);
$form .= $this->Form->control(
    'originating_agency',
    [
        'label' => __("Service producteur"),
        'options' => $originating_agencies,
        'empty' => __("-- Sélectionner un service producteur --"),
        'help' => __(
            "Laisser vide si vous souhaitez utiliser le service producteur indiqué dans les transferts originaux"
        ),
        'default' => $entity->get('originating_agency_identifier'),
    ]
);
$form .= $this->Form->control(
    'agreement_identifier',
    [
        'label' => __("Accord de versement"),
        'options' => $agreements,
        'empty' => __("-- Sélectionner un accord de versement --"),
        'help' => __(
            "Laisser vide si vous souhaitez utiliser l'accord de versement indiqué dans les transferts originaux"
        ),
    ]
);
$form .= $this->Form->control(
    'profile_identifier',
    [
        'label' => __("Profil d'archives"),
        'options' => $profiles,
        'empty' => __("-- Sélectionner un profil d'archives --"),
        'help' => __(
            "Laisser vide si vous souhaitez utiliser le profil d'archives indiqué dans les transferts originaux"
        ),
    ]
);
$form .= $this->Form->end();
$form .= $this->Html->tag('/article');

$tabs = $this->Tabs->create('tabs-edit-outgoing-transfer-request', ['class' => 'row no-padding']);
$tabs->add(
    'tab-form-edit-outgoing-transfer',
    $this->Fa->charte('Modifier', __("Edition du transfert sortant")),
    $form
);

$jsTable = $this->Table->getJsTableObject($jsTableId);
$tableUnits = $this->Table
    ->create($jsTableId, ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'outgoing-transfer-requests',
            'action' => 'paginate-archive-units',
            $entity->id,
            '?' => [
                'sort' => 'name',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'archival_agency_identifier' => [
                'label' => __("Identifiant"),
            ],
            'name' => [
                'label' => __("Nom"),
            ],
            'description' => [
                'label' => __("Description"),
            ],
            'dates' => [
                'label' => __("Dates extrêmes"),
            ],
            'original_total_count' => [
                'label' => __("Nombre de fichiers"),
            ],
            'original_total_size' => [
                'label' => __("Taille des fichiers"),
                'callback' => 'TableHelper.readableBytes',
            ],
        ]
    )
    ->data($entity->get('archive_units') ?: [])
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewUnitDescription({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/description'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'href' => $this->Url->build('/archive-units/display-xml') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/display-xml'),
                'displayEval' => '!data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'data-callback' => sprintf(
                    "actionRemoveArchiveUnit(%s, '%s')('{0}', false)",
                    $jsTable,
                    $this->Url->build('/outgoing-transfer-requests/removeArchiveUnit') . '/' . $entity->id
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Retirer cette unité d'archives de la demande de transfert sortant ?"),
                'displayEval' => 'data.length > 1',
                'params' => ['id', 'archival_agency_identifier'],
            ],
        ]
    );
$tabs->add(
    'tab-archive-units-edit-outgoing-transfer',
    $this->Fa->i('fa-file-code-o', __("Unités d'archives")),
    $this->AjaxPaginator->create('pagination-outgoing-transfer-request-archive-units')
        ->url($url)
        ->table($tableUnits)
        ->count($unitsCount)
        ->generateTable()
);

echo $tabs;
?>
<script>
    $('#edit-outgoing-transfer-transferring-agency').on('change', function () {
        var identifier = $(this).val();
        var name = '';
        if (identifier) {
            name = $(this).find('option[value="' + identifier + '"]')
                .first()
                .text()
                .substr(identifier.length + 3);
        }
        $('#edit-outgoing-transfer-transferring-agency-identifier').val(identifier);
        $('#edit-outgoing-transfer-transferring-agency-name').val(name);
    });
    $('#edit-outgoing-transfer-originating-agency').on('change', function () {
        var identifier = $(this).val();
        var name = '';
        if (identifier) {
            name = $(this).find('option[value="' + identifier + '"]')
                .first()
                .text()
                .substr(identifier.length + 3);
        }
        $('#edit-outgoing-transfer-originating-agency-identifier').val(identifier);
        $('#edit-outgoing-transfer-originating-agency-name').val(name);
    });

    function actionRemoveArchiveUnit(table, url) {
        return function (id) {
            $(table.table).off('.jstable').one('deleted.tablejs', function () {
                table.generateAll();
            });
            TableGenericAction.deleteAction(table, url)(id, false);
        }
    }
</script>
