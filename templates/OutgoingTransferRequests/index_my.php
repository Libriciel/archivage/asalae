<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Transferts sortants"));
$this->Breadcrumbs->add($h1 = __("Toutes mes demandes de transferts sortants"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-folder-open', $h1))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

require 'index-common.php';
