<?php

/**
 * @var Asalae\View\AppView $this
 */

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'OutgoingTransferRequests.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'OutgoingTransferRequests.comment' => [
                'label' => __("Commentaire"),
            ],
            'OutgoingTransferRequests.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'OutgoingTransferRequests.archive_units' => [
                'label' => __("Unités d'archives"),
                'callback' => 'TableHelper.ul("name")',
                'escape' => false,
            ],
            'OutgoingTransferRequests.statetrad' => [
                'label' => __("Etat"),
                'filter' => [
                    'state[0]' => [
                        'id' => 'filter-state-0',
                        'label' => false,
                        'aria-label' => __("Etat"),
                        'options' => $states,
                    ],
                ],
            ],
        ]
        + (in_array($this->getRequest()->getParam('action'), ['indexPreparating', 'indexMy', 'indexProcessed'])
            ? []
            : [
                'OutgoingTransferRequests.created_user.username' => [
                    'label' => __("Créée par"),
                    'escape' => false,
                    'order' => 'created_user.username',
                    'filterCallback' => 'AsalaeFilter.datepickerCallback',
                    'filter' => [
                        'created_user[0]' => [
                            'id' => 'filter-created_user-0',
                            'label' => __("Créée par"),
                            'options' => $created_users,
                        ],
                    ],
                ],
            ]
        )
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'OutgoingTransferRequests.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'data-callback' => "scheduleDestruction({0})",
                'confirm' => __(
                    "Les archives seront détruites par la tâche planifiée \"{0}\". Voulez-vous continuer ?",
                    __("Transferts sortants : élimination des archives transférées")
                ),
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-calendar-o'),
                'title' => $title = __("Programmer la destruction des archives liées à {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/OutgoingTransferRequests/scheduleArchiveDestruction'),
                'displayEval' => 'data[{index}].OutgoingTransferRequests.state === "transfers_processed"',
                'params' => ['OutgoingTransferRequests.id', 'OutgoingTransferRequests.identifier'],
            ],
            [
                'onclick' => "cancelDestruction({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-calendar-times-o'),
                'title' => $title = __("Annuler la destruction de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/OutgoingTransferRequests/cancelArchiveDestruction'),
                'displayEval' => 'data[{index}].OutgoingTransferRequests.state === "archive_destruction_scheduled"',
                'params' => ['OutgoingTransferRequests.id', 'OutgoingTransferRequests.identifier'],
            ],
            [
                'onclick' => "actionViewOutgoingTransferRequest({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Visualiser"),
                'display' => $this->Acl->check('/OutgoingTransferRequests/view'),
                'params' => ['OutgoingTransferRequests.id', 'OutgoingTransferRequests.identifier'],
            ],
            [
                'onclick' => "actionEditOutgoingTransferRequest({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/OutgoingTransferRequests/edit'),
                'displayEval' => 'data[{index}].OutgoingTransferRequests.editable '
                    . '&& data[{index}].OutgoingTransferRequests.created_user_id === PHP.user_id',
                'params' => ['OutgoingTransferRequests.id', 'OutgoingTransferRequests.identifier'],
            ],
            [
                'data-callback' => "sendOutgoingTransferRequest($jsTable, {0})",
                'type' => 'button',
                'class' => 'btn-link send',
                'display' => $this->Acl->check('/OutgoingTransferRequests/send'),
                'displayEval' => 'data[{index}].OutgoingTransferRequests.sendable '
                    . '&& data[{index}].OutgoingTransferRequests.created_user_id === PHP.user_id',
                'label' => $this->Fa->charte('Envoyer', '', 'text-success'),
                'title' => __("Envoyer {0}", '{1}'),
                'aria-label' => __("Envoyer {0}", '{1}'),
                'confirm' => __(
                    "Une fois la demande de transferts sortants envoyée, il ne sera "
                    . "plus possible de la modifier. Voulez-vous continuer ?"
                ),
                'params' => ['OutgoingTransferRequests.id', 'OutgoingTransferRequests.identifier'],
            ],
            [
                'onclick' => "actionResend($jsTable, {0})",
                'type' => 'button',
                'class' => 'btn-link resend',
                'display' => $this->Acl->check('/OutgoingTransferRequests/resend'),
                'displayEval' => 'data[{index}].OutgoingTransferRequests.resendable '
                    . '&& data[{index}].OutgoingTransferRequests.created_user_id === PHP.user_id',
                'label' => $this->Fa->charte('Envoyer', '', 'text-success'),
                'title' => $title = __("Renvoyer {0}", '{1}'),
                'aria-label' => $title,
                'params' => ['OutgoingTransferRequests.id', 'OutgoingTransferRequests.identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/OutgoingTransferRequests/delete')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Supprimer"),
                'displayEval' => 'data[{index}].OutgoingTransferRequests.deletable '
                    . '&& data[{index}].OutgoingTransferRequests.created_user_id === PHP.user_id',
                'confirm' => __("Êtes-vous sûr de vouloir supprimer cette demande de transferts sortants ?"),
                'params' => ['OutgoingTransferRequests.id', 'OutgoingTransferRequests.identifier'],
            ],
        ]
    );

$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des demandes de transferts sortants"),
        'table' => $table,
    ]
);
