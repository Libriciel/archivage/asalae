<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->MultiStepForm->template('MultiStepForm/add_outgoing_transfer')->step(2) . '<hr>';

$form = $this->Html->tag('article');
$form .= $this->Html->tag('header.bottom-space');
$form .= $this->Html->tag('h3', __("Nouvelle demande de transfert sortant"));
$form .= $this->Html->tag('/header');

if (isset($exception)) {
    $message = $exception->getMessage();
    if ($exception->getCode() === 403) {
        $message = __(
            "L'utilisateur {0} n'a pas les permissions d'accès nécessaire"
            . " pour envoyer un transfert sortant vers le SAE cible",
            h($archivingSystem->get('username'))
        );
    }
    if ($exception->getCode() === 404) {
        $message = __("Le SAE cible ne répond pas.");
    }
    echo $this->Html->tag(
        'div.alert.alert-danger',
        __("Erreur {0}: {1}", $exception->getCode(), $message)
    );
    return;
}

$form .= $this->Html->tag(
    'div',
    $this->ViewTable->generate(
        $archivingSystem,
        [
            __("Nom du Système d'archivage électronique") => 'name',
            __("Description") => 'description',
            __("Url") => 'url|htmlEntities',
            __("Identifiant de connexion") => 'username',
        ]
    ),
    [
        'id' => 'table-add2-archiving-system-' . $archivingSystem->id,
    ]
);

$form .= $this->Form->create($entity, ['idPrefix' => 'add2-outgoing-transfer']);
$form .= $this->Form->control('archiving_system_id', ['type' => 'hidden']);
$form .= $this->Form->control('filters', ['type' => 'hidden']);
$form .= $this->Form->control('archival_agency_identifier', ['type' => 'hidden']);
$form .= $this->Form->control('archival_agency_name', ['type' => 'hidden']);
$form .= $this->Form->control('transferring_agency_identifier', ['type' => 'hidden']);
$form .= $this->Form->control('transferring_agency_name', ['type' => 'hidden']);
$form .= $this->Form->control('originating_agency_identifier', ['type' => 'hidden']);
$form .= $this->Form->control('originating_agency_name', ['type' => 'hidden']);
$form .= $this->Form->control(
    'comment',
    [
        'label' => __("Commentaire"),
    ]
);
$form .= $this->Form->control(
    'archival_agency',
    [
        'label' => __("Service d'archives"),
        'options' => [
            'fake' => $entity->get('archival_agency_identifier')
                . ' - ' . $entity->get('archival_agency_name'),
        ],
        'readonly',
    ]
);
$form .= $this->Form->control(
    'transferring_agency',
    [
        'label' => __("Service versant"),
        'options' => $transferring_agencies,
        'empty' => __("-- Sélectionner un service versant --"),
        'help' => __(
            "Laisser vide si vous souhaitez utiliser le service versant indiqué dans les transferts originaux"
        ),
    ]
);
$form .= $this->Form->control(
    'originating_agency',
    [
        'label' => __("Service producteur"),
        'options' => $originating_agencies,
        'empty' => __("-- Sélectionner un service producteur --"),
        'help' => __(
            "Laisser vide si vous souhaitez utiliser le service producteur indiqué dans les transferts originaux"
        ),
    ]
);
$form .= $this->Form->control(
    'agreement_identifier',
    [
        'label' => __("Accord de versement"),
        'options' => $agreements,
        'empty' => __("-- Sélectionner un accord de versement --"),
        'help' => __(
            "Laisser vide si vous souhaitez utiliser l'accord de versement indiqué dans les transferts originaux"
        ),
    ]
);
$form .= $this->Form->control(
    'profile_identifier',
    [
        'label' => __("Profil d'archives"),
        'options' => $profiles,
        'empty' => __("-- Sélectionner un profil d'archives --"),
        'help' => __(
            "Laisser vide si vous souhaitez utiliser le profil d'archives indiqué dans les transferts originaux"
        ),
    ]
);
$form .= $this->Form->control(
    'service_level_identifier',
    [
        'label' => __("Niveau de service"),
        'options' => $services,
        'empty' => __("-- Sélectionner un niveau de service --"),
        'help' => __(
            "Laisser vide si vous souhaitez utiliser le niveau de service indiqué dans les transferts originaux"
        ),
    ]
);

echo $form;
echo $this->Form->end();
echo $this->Html->tag('/article');
?>
<script>
    $('#add2-outgoing-transfer-transferring-agency').on('change', function () {
        var identifier = $(this).val();
        var name = '';
        if (identifier) {
            name = $(this).find('option[value="' + identifier + '"]')
                .first()
                .text()
                .substr(identifier.length + 3);
        }
        $('#add2-outgoing-transfer-transferring-agency-identifier').val(identifier);
        $('#add2-outgoing-transfer-transferring-agency-name').val(name);
    });
    $('#add2-outgoing-transfer-originating-agency').on('change', function () {
        var identifier = $(this).val();
        var name = '';
        if (identifier) {
            name = $(this).find('option[value="' + identifier + '"]')
                .first()
                .text()
                .substr(identifier.length + 3);
        }
        $('#add2-outgoing-transfer-originating-agency-identifier').val(identifier);
        $('#add2-outgoing-transfer-originating-agency-name').val(name);
    });
</script>
