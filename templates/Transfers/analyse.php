<?php

/**
 * @var Asalae\View\AppView             $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Cake\Utility\Hash;

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($entity->get('transfer_identifier')));
$infos .= $this->Html->tag(
    'p',
    __(
        "L'analyse du transfert a été effectuée, celui-ci est {0}",
        '<b>' . ($entity->get('is_conform') ? __("conforme") : __("non conforme")) . '</b>'
    )
);
$infos .= $this->Html->tag('/header');

$errors = Hash::get($entity, 'transfer_errors');
if (!empty($errors)) {
    $tableErrors = $tableAttachments = $this->Table
        ->create('analyse-transfer-errors', ['class' => 'table table-striped table-hover'])
        ->fields(
            [
                'level' => ['label' => __("Niveau")],
                'code' => ['label' => __("Code")],
                'message' => ['label' => __("Message")],
            ]
        )
        ->data($errors)
        ->params(
            [
                'identifier' => 'id',
            ]
        );
    $paginator = $this->AjaxPaginator->create('pagination-transfer-view-errors')
        ->url(
            [
                'controller' => 'transfers',
                'action' => 'view-errors',
                $id,
            ]
        )
        ->table($tableErrors)
        ->count($countErrors);

    $infos .= $paginator->generateTable();
}

$infos .= $this->Html->tag('/article');
echo $infos;

if ($tableId = $this->getRequest()->getQuery('refresh')) {
    $jsTable = $this->Table->getJsTableObject($tableId);
    ?>
    <script>
        if (typeof <?= $jsTable ?> !== 'undefined') {
            var table = <?=$jsTable?>;
            for (var i = 0; i < table.data?.length; i++) {
                if (table.data[i].transfer?.id === <?= $entity->get('id') ?>) {
                    table.data[i].transfer.is_conform = <?= $entity->get('is_conform') ? 'true' : 'false' ?>;
                    table.generateAll();
                    break;
                }
            }
        }
    </script>
    <?php
}

