<?php

/**
 * @var Asalae\View\AppView $this
 */

$view = $this;
$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

// On affiche l'aide uniquement sur l'url racine: /Transfers/edit/<id>
if (preg_match('/^\/\w+\/\w+\/\d+$/', $this->getRequest()->getPath())) {
    /**
     * Aide: Recherche
     */
    $tuto = $this->Html->tag('div.alert.alert-info.alert-soft', null, ['style' => 'position: relative']);
    $tuto .= $this->Html->tag('h4', $this->Fa->i('fa-info-circle', __("Barre de recherche")));
    $tuto .= $this->Html->tag(
        'button.close',
        $this->Html->tag('span', '×'),
        [
            'type' => 'button',
            'aria-label' => __("Fermer cette alerte"),
            'onclick' => '$(this).parent().parent().find(".alert-soft").slideUp()',
            'style' => 'position: absolute; top: 8px; right: 14px;',
        ]
    );
    $tuto .= $this->Html->tag('ul');
    $tuto .= $this->Html->tag(
        'li',
        $this->Fa->i(
            'fa-i-cursor',
            __("Le terme saisi est recherché dans l'arbre courant")
        )
    );
    $tuto .= $this->Html->tag(
        'li',
        $this->Fa->i(
            'fa-search',
            __(
                "Appuyer sur le bouton recherche permet de lancer une recherche "
                . "en profondeur sur le contenu des éléments (sensible à la casse, minimum 3 caractères)"
            )
        )
    );
    $tuto .= $this->Html->tag('/ul');
    $tuto .= $this->Html->tag('/div');

    /**
     * Aide: Navigation
     */
    $tuto .= $this->Html->tag('div.alert.alert-info.alert-soft', null, ['style' => 'position: relative']);
    $tuto .= $this->Html->tag('h4', $this->Fa->i('fa-info-circle', __("Navigation")));
    $tuto .= $this->Html->tag(
        'button.close',
        $this->Html->tag('span', '×'),
        [
            'type' => 'button',
            'aria-label' => __("Fermer cette alerte"),
            'onclick' => '$(this).parent().parent().find(".alert-soft").slideUp()',
            'style' => 'position: absolute; top: 8px; right: 14px;',
        ]
    );
    $tuto .= $this->Html->tag('ul');
    $tuto .= $this->Html->tag(
        'li',
        $this->Html->tag(
            'i',
            '',
            [
                'style' => 'background-image: url(/css/jstree/32px.png); background-position: -132px -4px;'
                    . ' background-repeat: no-repeat; background-color: transparent; '
                    . 'height: 17px; width: 30px; display: inline-block',
            ]
        )
        . __("Élément contenant d'autres éléments (ouverts)")
    );
    $tuto .= $this->Html->tag(
        'li',
        $this->Html->tag(
            'i',
            '',
            [
                'style' => 'background-image: url(/css/jstree/32px.png); background-position: -100px -4px;'
                    . ' background-repeat: no-repeat; background-color: transparent; height: 17px;'
                    . ' width: 30px; display: inline-block',
            ]
        )
        . __("Élément contenant d'autres éléments (fermés) - Cliquer pour ouvrir")
    );
    $tuto .= $this->Html->tag('li', $this->Fa->i('fa-building', __("Entité - service")));
    $tuto .= $this->Html->tag('li', $this->Fa->i('fa-archive', __("Unité d'archives")));
    $tuto .= $this->Html->tag('li', $this->Fa->i('fa-file-text-o', __("Description d'archives")));
    $tuto .= $this->Html->tag('li', $this->Fa->i('fa-paperclip', __("Document")));
    $tuto .= $this->Html->tag('li', $this->Fa->i('fa-key', __("Mot clé")));
    $tuto .= $this->Html->tag('/ul');
    $tuto .= $this->Html->tag('/div');

    /**
     * Aide: Actions
     */
    $tuto .= $this->Html->tag('div.alert.alert-info.alert-soft', null, ['style' => 'position: relative']);
    $tuto .= $this->Html->tag(
        'button.close',
        $this->Html->tag('span', '×'),
        [
            'type' => 'button',
            'aria-label' => __("Fermer cette alerte"),
            'onclick' => '$(this).parent().parent().find(".alert-soft").slideUp()',
            'style' => 'position: absolute; top: 8px; right: 14px;',
        ]
    );
    $tuto .= $this->Html->tag('h4', $this->Fa->i('fa-info-circle', __("Actions")));
    $tuto .= $this->Html->tag('ul');
    $tuto .= $this->Html->tag(
        'li',
        $this->Fa->i(
            'fa-mouse-pointer',
            __("Clic gauche pour ouvrir un element en édition")
        )
    );
    $tuto .= $this->Html->tag(
        'li',
        $this->Fa->i(
            'fa-mouse-pointer',
            __("Clic droit pour ouvrir un menu contextuel listant les actions possibles")
        )
    );
    $tuto .= $this->Html->tag(
        'li',
        $this->Fa->i(
            'fa-mouse-pointer',
            __(
                "Glisser-déposer - permet de changer l'ordre pour"
                . " les séries d'éléments du même nom (ex: les mots-clés)"
            )
        )
    );
    $tuto .= $this->Html->tag(
        'li',
        $this->Fa->i(
            'fa-plus-square text-success',
            __("Clic gauche sur le \"+\" vert pour ajouter l'élément")
        )
    );
    $tuto .= $this->Html->tag('/ul');
    $tuto .= $this->Html->tag('/div');
} else {
    $tuto = '';
}

echo $this->Html->tag('div#' . $jstreeId . '.col-md-4.transfer-jstree', $loading)
    . $this->Html->tag(
        'div#' . $dataId . '.col-md-8.form-div.jstree-div-form',
        $this->Form->create()
        . $this->Form->control('action', ['type' => 'hidden', 'default' => 'cancel'])
        . $tuto
        . $this->Form->end()
    );

$idWithLastPath = $urlId . ($path ? ('/' . implode('/', (array)$path)) : '');
?>
<!--suppress JSDeprecatedSymbols -->
<script>
    $('.jstree[role=tree]:not(:visible)').empty();
    var tree = $('#<?=$jstreeId?>');
    $('.transfer-jstree').not(tree).empty(); // évite les collisions d'ids des noeuds du jstree
    var modal = tree.closest('.modal');
    var footer = modal.find('.modal-footer');
    var zoneDescription = $('#<?=$dataId?>');
    var ajaxData = {
        url: '<?=$this->Url->build('/transfers/get-tree/' . $idWithLastPath)?>/?tempfile=true&addable=true',
        data: function (node) {
            return {
                id: node.id
            };
        }
    };
    var contextmenuExists = false;

    function partialAddTransfer(node) {
        var div = $('#<?=$dataId?>');
        var parent = tree.jstree(true).get_node(node.parent);
        tree.disable();
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/transfers/partialAdd/' . $urlId)?>/'
                + node.original.element + '/' + parent.original.url,
            method: 'GET',
            success: function (content) {
                div.html(content);
                div.find('input:visible, select:visible, textarea:visible, select.chosenified')
                    .not('.hasDatepicker')
                    .first()
                    .focus()
                    .trigger('chosen:activate');
            },
            error: function () {
                div.html('');
            }
        });
    }

    function partialEditTransfer(dataPath) {
        var div = $('#<?=$dataId?>');
        tree.disable();
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/transfers/partialEdit/' . $urlId)?>/' + dataPath,
            success: function (content) {
                div.html(content);
                window.onbeforeunload = function (e) {
                    e = e || window.event;
                    var msg = "<?=__("CONFIRM_EXIT_PAGE")?>";
                    if (e) {
                        e.returnValue = msg;
                    }
                    return msg;
                };
                // scroll top
                tree.closest('.modal-body').animate({
                    scrollTop: 0
                }, 500);
                div.find('input:visible, select:visible, textarea:visible, select.chosenified')
                    .not('.hasDatepicker')
                    .first()
                    .focus()
                    .trigger('chosen:activate');
                tree.trigger('partial_edit_loaded.jstree');
            },
            error: function () {
                div.html('');
            }
        });
    }

    $(document).off('.vakata.custom').off('.jstree.custom');
    tree.jstree({
        core: {
            data: ajaxData,
            check_callback: function (op, node, par, pos, more) {
                if (op === 'move_node') {
                    return this.settings.dnd.is_droppable(node);
                }
                return true;
            }
        },
        conditionalselect: function (node, event) {
            return !$('#<?=$jstreeId?>').hasClass('disabled');
        },
        search: {
            case_insensitive: true,
            show_only_matches: false,
            search_callback: function (search, node) {
                search = search.toLowerCase().unaccents();
                if (typeof node.original.searchtext === 'undefined') {
                    return node.original.text.toLowerCase().indexOf(search) >= 0;
                }
                return node.original.searchtext.toLowerCase().indexOf(search) >= 0;
            }
        },
        plugins: [
            "conditionalsel", // permet de désactiver la sélection lorsque le formulaire est apparent
            "contextmenu", // menu clic droit
            "search", // Recherche par nom de noeud
            "wholerow", // sélection sur la ligne (clic facilité et affichage + moderne)
            "dnd" // Drag & drop - Permet de déplacer les noeuds
        ],
        contextmenu: {
            select_node: false,
            show_at_node: true,
            items: function (node) {
                if (tree.hasClass('disabled')
                    || typeof node.original.url === 'undefined'
                    || (node.original.navigation && typeof node.original.last_page === 'undefined')
                ) {
                    contextmenuExists = false;
                    return {};
                }
                var items = {};
                var treeInstance = tree.jstree(true);
                var index = parseInt(node.original.url.substr(node.original.url.lastIndexOf('[') + 1), 10);
                var nodeAmount = node.original.count;

                /**
                 * Action ajouter
                 */
                if (typeof node.original.addable !== 'undefined' && node.original.addable.length) {
                    var submenu = {};
                    var addable;
                    for (var i = 0; i < node.original.addable.length; i++) {
                        addable = node.original.addable[i];
                        submenu[addable.element] = (function (addable) {
                            return {
                                "separator_before": false,
                                "separator_after": false,
                                "label": addable.element + ' (' + addable.min + '..' + addable.max + ')',
                                "icon": "fa " + (addable.count < addable.min ? 'fa-warning' : 'fa-angle-right'),
                                "action": function (obj) {
                                    var div = $('#<?=$dataId?>');
                                    tree.disable();
                                    AsalaeLoading.ajax({
                                        url: '<?=$this->Url->build('/transfers/partialAdd/' . $urlId)?>/'
                                            + addable.element + '/' + node.original.url,
                                        method: 'GET',
                                        success: function (content) {
                                            div.html(content);
                                            div.find(
                                                'input:visible, select:visible, textarea:visible, select.chosenified'
                                            )
                                                .not('.hasDatepicker')
                                                .first()
                                                .focus()
                                                .trigger('chosen:activate');
                                        },
                                        error: function () {
                                            div.html('');
                                        }
                                    });
                                }
                            };
                        })(addable);
                    }
                    items.Add = {
                        "separator_before": false,
                        "separator_after": false,
                        "label": __("Ajouter"),
                        "icon": "fa fa-plus text-success",
                        "submenu": submenu
                    };
                }

                /**
                 * Action spéciale (ajouter un ensemble de mots-clés)
                 */
                if (node.original.action === 'add-multiple-keyword') {
                    items.AddKeywords = {
                        "separator_before": false,
                        "separator_after": false,
                        "label": __("Ajouter un ensemble de mots-clés"),
                        "icon": "fa fa-tags text-success",
                        "action": function (obj) {
                            var div = $('#<?=$dataId?>');
                            tree.disable();
                            AsalaeLoading.ajax({
                                url: '<?=$this->Url->build('/transfers/addMultipleKeywords/' . $urlId)?>/'
                                    + node.original.url,
                                method: 'GET',
                                success: function (content) {
                                    div.html(content);
                                    div.find('input:visible, select:visible, textarea:visible, select.chosenified')
                                        .not('.hasDatepicker')
                                        .first()
                                        .focus()
                                        .trigger('chosen:activate');
                                },
                                error: function () {
                                    div.html('');
                                }
                            });
                        }
                    };
                }

                /**
                 * Actions de déplacement
                 */
                var showMoveMenu = false;
                var submenuMove = {};
                /**
                 * Action déplacer complètement vers le haut
                 */
                if (nodeAmount > 2 && index > 2) {
                    showMoveMenu = true;
                    submenuMove.MoveTop = {
                        "separator_before": true,
                        "separator_after": false,
                        "label": __("Déplacer tout en haut"),
                        "icon": "fa fa-angle-double-up",
                        "action": function (obj) {
                            moveNode(
                                node.original.url,
                                'before',
                                node.original.url.substr(0, node.original.url.lastIndexOf('[')) + '[1]'
                            );
                        }
                    };
                }
                /**
                 * Action déplacer vers le haut
                 */
                if (nodeAmount > 1 && index > 1) {
                    showMoveMenu = true;
                    submenuMove.MoveUp = {
                        "separator_before": true,
                        "separator_after": false,
                        "label": __("Déplacer vers le haut"),
                        "icon": "fa fa-angle-up",
                        "action": function (obj) {
                            moveNode(
                                node.original.url,
                                'before',
                                node.original.url.substr(
                                    0,
                                    node.original.url.lastIndexOf('[')
                                ) + '[' + (index - 1) + ']'
                            );
                        }
                    };
                }
                /**
                 * Action déplacer vers le bas
                 */
                if (nodeAmount > 1 && index < nodeAmount) {
                    showMoveMenu = true;
                    submenuMove.MoveDown = {
                        "separator_before": true,
                        "separator_after": false,
                        "label": __("Déplacer vers le bas"),
                        "icon": "fa fa-angle-down",
                        "action": function (obj) {
                            moveNode(
                                node.original.url,
                                'after',
                                node.original.url.substr(
                                    0,
                                    node.original.url.lastIndexOf('[')
                                ) + '[' + (index + 1) + ']'
                            );
                        }
                    };
                }
                /**
                 * Action déplacer complètement vers le bas
                 */
                if (nodeAmount > 2 && index < (nodeAmount - 1)) {
                    showMoveMenu = true;
                    submenuMove.MoveBottom = {
                        "separator_before": true,
                        "separator_after": false,
                        "label": __("Déplacer tout en bas"),
                        "icon": "fa fa-angle-double-down",
                        "action": function (obj) {
                            moveNode(
                                node.original.url,
                                'after',
                                node.original.url.substr(0, node.original.url.lastIndexOf('[')) + '[' + nodeAmount + ']'
                            );
                        }
                    };
                }
                /**
                 * Menu de déplacement
                 */
                if (showMoveMenu) {
                    items.Move = {
                        "separator_before": false,
                        "separator_after": false,
                        "label": __("Déplacer"),
                        "icon": "fa fa-angle-double-right",
                        "submenu": submenuMove
                    };
                }

                /**
                 * Action delete
                 */
                if (node.original.min === '0' || nodeAmount > parseInt(node.original.min, 10)) {
                    items.Remove = {
                        "separator_before": true,
                        "separator_after": false,
                        "label": __("Supprimer"),
                        "icon": "fa fa-trash text-danger",
                        "action": function (obj) {
                            if (confirm(
                                __(
                                    "Vous êtes sur le point de supprimer un nœud et tout ce qu'il contient. "
                                    + "Cette action ne pourra pas être annulée. Voulez-vous continuer ?"
                                )
                            )
                            ) {
                                var div = $('#<?=$dataId?>');
                                AsalaeLoading.ajax({
                                    url: '<?=$this->Url->build('/transfers/partialDelete/' . $urlId)?>/'
                                        + node.original.url,
                                    method: 'DELETE',
                                    success: function (content) {
                                        if (content.report === 'done') {
                                            treeInstance.delete_node(node);
                                            updateNodesAfterDelete(node);
                                            transferIsDirty = true;
                                            if (!content.files || content.is_repair || content.files.length === 0) {
                                                <?=$refreshJsBase?>(content.parentId);
                                                return;
                                            }
                                            var msg;
                                            if (content.files.length === 1) {
                                                msg = __(
                                                    "L'élément a bien été supprimé.\nIl est lié au fichier \"{0}\","
                                                    + " si vous souhaitez également le supprimer,"
                                                    + "\nveuillez écrire \"delete\" puis cliquer sur OK.",
                                                    content.files[0].filename
                                                );
                                            } else {
                                                var names = '';
                                                for (var i = 0, l = Math.min(content.files.length, 10); i < l; i++) {
                                                    names += content.files[i].filename + "\n";
                                                }
                                                if (content.files.length > 10) {
                                                    names += "...\n";
                                                }
                                                msg = __(
                                                    "Il y a {0} fichiers liés à ce noeud:\n{1}\npour les supprimer,"
                                                    + " veuillez saisir le mot \"delete\"",
                                                    content.files.length,
                                                    names
                                                );
                                            }
                                            var deleteFiles = prompt(msg);
                                            if (deleteFiles === 'delete') {
                                                var ids = [];
                                                for (i = 0; i < content.files.length; i++) {
                                                    ids.push(content.files[i].id);
                                                }
                                                deleteAttachments(ids);
                                                tree.on('deleted_files.jstree', function () {
                                                    <?=$refreshJsBase?>(content.parentId);
                                                });
                                            } else {
                                                <?=$refreshJsBase?>(content.parentId);
                                            }
                                        } else {
                                            this.error();
                                        }
                                    },
                                    complete: function () {
                                        div.html('');
                                    }
                                });
                            }
                        }
                    };
                }

                /**
                 * Action select page
                 */
                if (node.original.navigation && node.original.last_page) {
                    items.Page = {
                        "separator_before": true,
                        "separator_after": true,
                        "label": __("Sélectionner une page"),
                        "icon": "fa fa-file-o",
                        "action": function (obj) {
                            var page = parseInt(
                                prompt(__("Veuillez sélectionner un numéro de page")),
                                10
                            );
                            if (isNaN(page) || page <= 0) {
                                alert(__("Numero de page invalide."));
                                return;
                            }
                            if (page > node.original.last_page) {
                                alert(
                                    __(
                                        "Numéro de page ({0}) supérieur au nombre total de pages ({1})",
                                        page,
                                        node.original.last_page
                                    )
                                );
                                return;
                            }
                            if (page) {
                                var title = node.id.split('_');
                                title.shift(); // nav
                                title.shift(); // page
                                var id = title.join('_');
                                var newnode = {
                                    id: 'nav_' + page + '_' + id,
                                    parent: node.id,
                                    original: {
                                        navigation: true
                                    }
                                };
                                tree.trigger('select_node.jstree', {node: newnode});
                            }
                        }
                    };
                }
                if (Object.keys(items).length >= 1) {
                    contextmenuExists = true;
                    setTimeout(function () {
                        contextmenuExists = false;
                    }, 10);
                }
                return items;
            }
        },
        dnd: {
            copy: false,
            is_draggable: function (selected) {
                var node = selected[0];
                var parent = tree.jstree(true).get_node(node.parent);
                var child;
                for (var i = 0; i < parent.children.length; i++) {
                    child = tree.jstree(true).get_node(parent.children[i]);
                    if (child.id !== node.id && child.original.element === node.original.element) {
                        return true;
                    }
                }
                return false;
            },
            is_droppable: function (node) {
                var parent = tree.jstree(true).get_node(node.parent);
                var hovered = $('.jstree-wholerow-hovered').closest('li').attr('id');
                return parent.children.indexOf(hovered) >= 0
                    && tree.jstree(true).get_node(hovered).original.element === node.original.element;
            }
        }
    })
        .off('ready.jstree set_state.jstree') // affiche les pointillés (supprimés dans cet event par wholerow)
        .on('select_node.jstree', function (event, data) {
            if ($(this).hasClass('disabled') || data.node.original.disabled) {
                event.stopPropagation();
                event.preventDefault();
                return;
            }
            if (data.node.original.search) {
                var search = data.node.original.search;
                if (typeof search === 'boolean' || search === 'true') {
                    search = '';
                }
                if (search === '') {
                    <?=$refreshJsCallback?>;
                    return;
                }
                AsalaeLoading.ajax({
                    url: '<?=$this->Url->build('/transfers/get-tree/' . $urlId)?>'
                        + search + '?tempfile=true&addable=true',
                    method: 'GET',
                    headers: {
                        Accept: 'application/json'
                    },
                    success: function (content) {
                        var value = $('#<?=$searchbarId?>').val();
                        var jstreeDiv = $('#<?=$jstreeId?>');
                        var jstreeObj = tree.jstree(true);
                        var root = jstreeObj.get_node('#');
                        jstreeObj.delete_node(root.children);

                        function recursiveCreateNode(parent, node) {
                            var cNode = $.extend(true, {}, node);
                            if (!cNode.id) {
                                return;
                            }
                            cNode.children = typeof cNode.children === 'boolean' ? cNode.children : false;
                            parent = jstreeObj.create_node(parent, cNode);
                            if (typeof node.children === 'undefined') {
                                return;
                            }
                            for (var key in node.children) {
                                recursiveCreateNode(parent, node.children[key]);
                                if (Array.isArray(node.children[key].children)) {
                                    jstreeObj.open_node(node.children[key]);
                                }
                            }
                        }

                        recursiveCreateNode(root, content);
                        prependSearchbar(value);
                        if (typeof data.node.original.search === 'string') {// boolean = retour en arrière
                            var selectTarget = data.node.original.search.replace(/^\//, '').split('/');
                            var nodeId = '1_' + selectTarget.join('_').replace(/\[/g, '-').replace(']', '');
                            var node = jstreeObj.get_node(nodeId);
                            // si le noeud n'existe pas dans le jstree,
                            // c'est qu'il faut appeler le noeud parent (ex: Name, Title...)
                            if (!node) {
                                selectTarget.pop();
                                nodeId = '1_' + selectTarget.join('_').replace(/\[/g, '-').replace(']', '');
                                node = jstreeObj.get_node(nodeId);
                            }
                            jstreeObj.select_node(node.id);
                        }
                    },
                    complete: function () {
                        tree.trigger('search_completed.jstree');
                    }
                })
            } else if (data.node.original.navigation) {
                $('#' + data.node.id).html(
                    "<li class='jstree-initial-node jstree-loading jstree-leaf jstree-last'>"
                    + "<i class='jstree-icon jstree-ocl'></i>"
                    + "<a class='jstree-anchor' href='#'>"
                    + "<i class='jstree-icon jstree-themeicon-hidden'></i>" + "Loading ..." + "</a></li>"
                );
                AsalaeLoading.ajax({
                    url: '<?=$this->Url->build('/transfers/get-tree/' . $idWithLastPath)?>/?id='
                        + data.node.id + '&tempfile=true&addable=true',
                    success: function (content) {
                        var jstreeObj = tree.jstree(true);
                        var parentId = data.node.parent;
                        var parent = jstreeObj.get_node(parentId);
                        if (parent.original.navigation) { // si le parent est une liste, on doit remonter d'un cran
                            parentId = parent.parent;
                            parent = jstreeObj.get_node(parentId);
                        }

                        if (typeof content === 'string') {
                            return this.error({responseText: content});
                        }

                        jstreeObj.deselect_all(true);

                        // delete_node custom pour eviter un bug
                        var obj, pos, par, tmp;
                        var children = parent.children.slice();
                        par = parent;
                        for (var i = 0; i < children.length; i++) {
                            obj = jstreeObj.get_node(children[i]);
                            pos = $.inArray(obj.id, par.children);
                            par.children = $.vakata.array_remove(par.children, pos);
                            tmp = obj.children_d.concat([]);
                            tmp.push(obj.id);
                            for (var k = 0, l = tmp.length; k < l; k++) {
                                for (var m = 0, n = obj.parents.length; m < n; m++) {
                                    pos = $.inArray(tmp[k], jstreeObj._model.data[obj.parents[m]].children_d);
                                    if (pos !== -1) {
                                        jstreeObj._model.data[obj.parents[m]].children_d
                                            = $.vakata.array_remove(
                                            jstreeObj._model.data[obj.parents[m]].children_d,
                                            pos
                                        );
                                    }
                                }
                            }
                            jstreeObj.trigger('delete_node', {"node": obj, "parent": par.id});
                            for (k = 0, l = tmp.length; k < l; k++) {
                                delete jstreeObj._model.data[tmp[k]];
                            }
                            jstreeObj.redraw_node(par, true);
                        }
                        // fin du delete_node custom

                        for (i = 0; i < content.length; i++) {
                            jstreeObj.create_node(parentId, content[i]);
                        }
                        var title = data.node.id.split('_');
                        title.shift();
                        var page = title.shift();

                        zoneDescription.html($('<h4 class="h2"></h4>').text(title.pop() + ' - Page ' + page));

                        // scroll top
                        tree.closest('.modal-body').animate({
                            scrollTop: 0
                        }, 500);

                        tree.trigger('after-pagination');
                    },
                    error: function (e) {
                        zoneDescription.html(e.responseText);
                    }
                });
            } else if (typeof data.node.original.is_button !== 'undefined') {
                partialAddTransfer(data.node);
            } else {
                partialEditTransfer(data.node.original.url);
            }

        }).on('loaded.jstree', function () {
        accessibleContextMenu();
        prependSearchbar('');
        var treeJstree = tree.jstree(true);
        treeJstree.move_node = function (obj, par, pos, callback, is_loaded) {
            var target = $('.jstree-wholerow-hovered').closest('li');
            moveNode(
                obj[0].original.url,
                target.hasClass('hover-top') ? 'before' : 'after',
                tree.jstree(true).get_node(target.attr('id')).original.url
            );
            return true;
        };
        tree.jstree(true).open_node('1_ArchiveTransfer_Contains'); // seda0.2
        tree.jstree(true).open_node('1_ArchiveTransfer_Archive'); // seda1.0

    })
        .on('after_open.jstree', accessibleContextMenu)
        .on('changed.jstree after_open.jstree after-pagination ready.jstree', function () {
            // au survol, affiche le text entier d'un noeud
            tree.find('span.child-value.trimmed:not(.mouseover)')
                .off('.jstree')
                .on('mouseover.jstree', function () {
                    var trimmed = $(this).text();
                    var name = $(this).attr('title');
                    $(this).addClass('mouseover')
                        .attr('data-text', trimmed)
                        .text(name);
                })
                .on('mouseout.jstree', function () {
                    var trimmed = $(this).attr('data-text');
                    $(this).removeClass('mouseover')
                        .attr('data-text', '')
                        .text(trimmed);
                });
        });

    function prependSearchbar(value) {
        tree.jstree("search", value);
        tree.prepend(
            $('<form class="form-group text"></form>').append(
                $('<label></label>')
                    .attr('for', '<?=$searchbarId?>')
                    .text(__("Rechercher"))
            ).append(
                $('<div class="input-group"></div>').append(
                    $('<input class="form-control">')
                        .attr('id', '<?=$searchbarId?>')
                        .val(value)
                        .on('keyup', function () {
                            tree.jstree("search", $(this).val());
                        })
                        .one('keydown', function () {
                            $(this).closest('.modal').find('.alert-soft button.close').click();
                        })
                ).append(
                    $('<span class="input-group-addon btn btn-primary" role="button" tabindex="0"></span>').append(
                        $('<i class="fa fa-search" aria-hidden="true"></i>')
                    ).append($('<span class="sr-only"></span>').text(__("Rechercher")))
                        .attr('title', __("Rechercher"))
                        .on('click', doSearch)
                )
            ).on('submit', function (event) {
                event.preventDefault();
                $(this).find('.btn[role="button"]').click();
            })
        );
    }

    $(document).on('keydown.jstree.custom', function (e) {
        // [Esc]
        if ([27].indexOf(e.keyCode) >= 0) {
            e.preventDefault();
            e.stopPropagation();
            $.vakata.context.hide();
            $('.jstree-hovered').focus();
        }
    }).on('dnd_stop.vakata.custom', function (e, data) {
        $(data.element).closest('li').removeClass('dragging');
        tree.find('li').off('.dragging').removeClass('hover-bottom').removeClass('hover-top');
    }).on('dnd_start.vakata.custom', function (e, data) {
        $(data.element).closest('li').addClass('dragging');
        var selected = tree.jstree(true).get_node(data.data.nodes[0]);
        var parent = tree.jstree(true).get_node(selected.parent);
        var child;
        var isBeforeNode = true;
        var adjacentNodes = {before: null, after: null};
        var nodes = [];
        for (var i = 0; i < parent.children.length; i++) {
            child = tree.jstree(true).get_node(parent.children[i]);
            if (child.id === selected.id) {
                isBeforeNode = false;
                continue;
            }
            if (isBeforeNode) {
                adjacentNodes.before = child;
            } else if (adjacentNodes.after === null) {
                adjacentNodes.after = child;
            }
            if (child.original.element === selected.original.element) {
                nodes.push(child);
            }
        }
        var fn;
        for (i = 0; i < nodes.length; i++) {
            if (nodes[i].id === adjacentNodes.before.id) {
                fn = function () {
                    $(this).find('.jstree-wholerow-hovered')
                        .closest('li')
                        .addClass('hover-top');
                };
            } else if (nodes[i].id === adjacentNodes.after.id) {
                fn = function () {
                    $(this).find('.jstree-wholerow-hovered')
                        .closest('li')
                        .addClass('hover-bottom');
                };
            } else {
                fn = function (event) {
                    var offset = $(this).offset(),
                        height = $(this).height();
                    if (event.pageY < offset.top + (height / 2)) {
                        $(this).find('.jstree-wholerow-hovered')
                            .closest('li')
                            .addClass('hover-top')
                            .removeClass('hover-bottom');
                    } else {
                        $(this).find('.jstree-wholerow-hovered')
                            .closest('li')
                            .addClass('hover-bottom')
                            .removeClass('hover-top');
                    }
                };
            }
            $('#' + nodes[i].id).closest('li')
                .on('mousemove.dragging', fn)
                .on('mouseleave.dragging', function () {
                    $(this).removeClass('hover-bottom').removeClass('hover-top');
                });
        }
    });

    /**
     * Accessibilité du contextmenu
     */
    function accessibleContextMenu() {
        tree.find('.jstree-anchor').off('.accessibility').on('keydown.accessibility', function (e) {
            /// [Ctrl], [Alt], [+], [²]
            if ([17, 18, 107, 192].indexOf(e.keyCode) >= 0) {
                e.preventDefault();
                e.stopPropagation();
                $(this).trigger('contextmenu.jstree');
            }
        }).on('contextmenu.jstree.accessibility', function (event) {
            // Rend le context-menu "focusable" car il n'est plus derriere la modale
            setTimeout(function () {
                var contextMenu = $('ul.vakata-context');
                var modal = $('#<?=$modalId?>');
                if (contextMenu.length === 0) {
                    if (contextmenuExists) {
                        alert(
                            __(
                                "Échec lors de l'affichage du menu contextuel. Il est nécessaire"
                                + " de recharger la page pour rétablir son fonctionnement."
                                + " Veuillez enregistrer votre travail et appuyer sur F5."
                            )
                        );
                    }
                    return;
                }
                // Repositionne le menu par rapport à la modale
                contextMenu.css('top', contextMenu.position().top - modal.offset().top);
                modal.append(contextMenu);
                // Focus le 1er element du menu
                contextMenu.find('> li.vakata-context-separator > a').attr('tabindex', '-1');
                contextMenu.find('> li:not(.vakata-context-separator) > a').first().focus();
            }, 0);
        });
    }

    function setAttachmentFilename(filename) {
        return $('<input>').val(filename);
    }

    /**
     * Déplace un noeud selon data
     */
    function moveNode(move, direction, target) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/transfers/moveNode/' . $urlId)?>',
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            data: {
                move: move,
                direction: direction,
                target: target
            },
            success: function (content, textStatus, jqXHR) {
                if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                    <?=$refreshJsCallback?>;
                } else {
                    this.error();
                }
            }
        });
    }

    function removeNode(domId) {
        $('#' + domId).closest('.form-group').hide('blind', function () {
            $(this).find('input, select, textarea')
                .each(function () {
                    $(this).val($(this).attr('data-default') || '').trigger("chosen:updated");
                    if ($(this).prop('required')) {
                        $(this).data('required', 'required');
                    }
                    $(this).prop('required', false);
                });
        });
    }

    function appendNew(domId) {
        var domElement = $('#' + domId);
        if (!domElement.val()) {
            return;
        }
        var isSelect = domElement.prop('tagName') === 'SELECT';
        var match = domId.match(/^([\w\-]+)-(\d+)$/);
        var current;
        var parentDiv;
        for (var i = 0; (current = $('#' + match[1] + '-' + i)).length === 1; i++) {
            if (!current.val()) {
                parentDiv = current.closest('.form-group');
                if (!parentDiv.is('visible')) {
                    parentDiv.show('blind').find('input, select, textarea').each(function () {
                        if ($(this).data('required')) {
                            $(this).prop('required', true);
                        }
                    });
                }
                if (isSelect) {
                    current.trigger('chosen:activate');
                } else {
                    current.focus();
                }
                return;
            }
        }
        if (isSelect) {
            domElement.chosen('destroy');
        }
        var newElement = domElement.closest('.form-group').clone();
        newElement.removeClass('has-error').find('.error-message').remove();
        newElement.find('*').each(function () {
            var value, m,
                attrs = ['id', 'for'];
            for (var key in attrs) {
                value = $(this).attr(attrs[key]);
                m = value ? value.match(/^([\w\-]+)-(\d+)(.*)$/) : null;
                if (m) {
                    $(this).attr(attrs[key], m[1] + '-' + i + m[3]).val($(this).attr('data-default') || '');
                }
            }
            var name = $(this).attr('name');
            m = name ? name.match(/^(.*)\[(\d+)](.*)$/) : null;
            if (m) {
                $(this).attr('name', m[1] + '[' + i + ']' + m[3]);
            }
        });
        newElement.find('button').each(function () {
            var onclick = $(this).attr('onclick');
            if (onclick) {
                onclick = onclick.replace(/(#?[\w\-]+)-(\d+)/, '$1-' + i)
                onclick = onclick.replace('appendNew', 'removeNode');
                $(this).attr('onclick', onclick).find('.fa-plus').addClass('fa-minus').removeClass('fa-plus');
            }
        });
        newElement.insertAfter($('#' + match[1] + '-' + (i - 1)).closest('.form-group'));
        if (isSelect) {
            AsalaeGlobal.chosen(domElement, __("Rechercher"));
            AsalaeGlobal.chosen(newElement.find('select'), __("Rechercher"));
        }
    }

    function loadAttachment(element) {
        var attributes = $(element).closest('.form-group').find('.attributesList')
        attributes.find('select, input, textarea')
            .each(function () {
                $(this).val('');
            })
            .trigger('chosen:updated');
        var value = $(element).val();
        if (!value) {
            return;
        }
        var baseName = $(element).attr('name');
        baseName = baseName.substr(0, baseName.length - ('[@]'.length));
        AsalaeLoading.start();
        var buttons = footer.find('button.cancel, button.accept').disable();
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/TransferAttachments/getFileInfo/' . $urlId)?>',
            method: 'POST',
            data: {
                filename: value
            },
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                if (content.format) {
                    attributes.find('[name="' + baseName + '[format]"]')
                        .val(content.format)
                        .trigger('chosen:updated');
                }
                if (content.mime) {
                    attributes.find('[name="' + baseName + '[mimeCode]"]')
                        .val(content.mime)
                        .trigger('chosen:updated');
                }
                attributes.find('[name="' + baseName + '[filename]"]').val(value);
            },
            complete: function () {
                buttons.enable();
            }
        })
    }

    function initializeAttachment(element) {
        var filename = $(element).closest('.form-group').find('input.filename').val();
        if (filename) {
            $(element).append(
                $('<option></option>')
                    .attr('value', filename)
                    .text(filename)
                    .prop('selected', true)
            )
        }
        $(element).addClass('initialized-attachment')
            .select2({
                ajax: {
                    url: '<?=$this->Url->build('/TransferAttachments/populateSelect/' . $urlId)?>?tempfile=true',
                    method: 'POST',
                    delay: 250
                },
                escapeMarkup: function (v) {
                    return $('<span></span>').html(v).text();
                },
                dropdownParent: $(element).closest('.modal'),
                allowClear: false
            });
    }

    function ajaxSearchKeyword(element) {
        $(element).select2({
            ajax: {
                url: '<?=$this->Url->build('/Keywords/populateSelect')?>',
                method: 'POST',
                delay: 250
            },
            escapeMarkup: function (v) {
                return $('<span></span>').html(v).text();
            },
            dropdownParent: $(element).closest('.modal'),
            allowClear: true
        });
        var button = $(element).siblings('.help-block').find('button').hide();
        $(element).on('change', function () {
            var actualValue = $('.keyword-name').val();
            if (!actualValue) {
                button.click();
            } else {
                button.show();
            }
        });
    }

    function loadKeywordData(button) {
        var select = $(button).closest('.form-group').find('select');
        var id = select.val();
        if (!id) {
            return;
        }
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/Keywords/getData')?>/' + id,
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                $('.keyword-name').val(content.name);
                $('.keyword-code').val(content.code);
                $('.keyword-list-id').val(content.keyword_list.identifier);
                $('.keyword-list-name').val(content.keyword_list.name);
                $('.keyword-list-version').val(content.keyword_list.version);
            }
        });
    }

    function updateNodesAfterDelete(node) {
        if (node.original.max === '1') {
            return;
        }
        var parent = tree.jstree(true).get_node(node.parent);
        var found = false;
        var match = node.original.url.match(/\[(\d+)]$/);
        if (!match) {
            return;
        }
        var index = parseInt(match[1], 10);
        var siblingNode;
        var siblingIndex;
        var siblingNodeName;
        var siblingNewNodeName;
        for (var i = 0; i < parent.children.length; i++) {
            siblingNode = tree.jstree(true).get_node(parent.children[i]);
            if (siblingNode.original.element === node.original.element && !siblingNode.original.is_button) {
                found = true;
                match = siblingNode.original.url.match(/\[(\d+)]$/);
                siblingNode.original.count = siblingNode.original.count - 1;
                if (!match) { // prev / next
                    continue;
                }
                siblingIndex = parseInt(match[1], 10);
                if (siblingIndex > index) {
                    siblingNodeName = siblingNode.original.element + '[' + siblingNode.original.index + ']';
                    siblingNewNodeName = siblingNode.original.element + '[' + (siblingNode.original.index - 1) + ']';
                    siblingNode.original.index = siblingNode.original.index - 1;
                    siblingNode.original.url
                        = siblingNode.original.url.substr(0, siblingNode.original.url.lastIndexOf(match[0]))
                        + '[' + (siblingIndex - 1) + ']';

                    tree.jstree(true).rename_node(
                        siblingNode,
                        siblingNode.text.replace(siblingNodeName, siblingNewNodeName)
                    );
                }
            } else if (found) {
                break;
            }
        }
    }

    function doSearch() {
        var searchStr = $('#<?=$searchbarId?>').val();
        if (searchStr.length > 2) {
            var button = $(this).disable();
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build('/transfers/search-in-edit/' . $urlId)?>?tempfile=true',
                method: 'POST',
                headers: {
                    Accept: 'application/json'
                },
                data: {
                    search: searchStr
                },
                success: function (content) {
                    var value = $('#<?=$searchbarId?>').val();
                    var jstreeDiv = $('#<?=$jstreeId?>');
                    var jstree = tree.jstree(true);
                    var rootNode = jstree.get_node('#');
                    for (var i = 0; i < rootNode.children.length; i++) {
                        jstree.delete_node(rootNode.children[i]);
                    }
                    var searchNode = jstree.get_node(
                        jstree.create_node(rootNode, {
                            id: 'search-in-edit-results',
                            icon: 'fa fa-search',
                            text: __("Résultats de la recherche (cliquer ici pour revenir)"),
                            search: true,
                            state: {
                                opened: true
                            }
                        })
                    );
                    var text;
                    for (i = 0; i < content.length; i++) {
                        text = content[i].path.replace(/\//g, ' / ').replace('@', ' @ ');

                        if (content[i].value.length > 30) {
                            text += ' <span class="child-value" title="'
                                + content[i].value.replace(/"/g, '&quot;') + '">'
                                + AsalaeGlobal.shortString(
                                    content[i].value,
                                    content[i].path.indexOf('@filename')
                                    !== -1
                                )
                                + '</span>';
                        } else {
                            text += ' <span class="child-value">' + content[i].value + '</span>';
                        }
                        jstree.create_node(searchNode, {
                            icon: 'fa fa-arrow-right',
                            text: text,
                            search: content[i].path.replace(/@.*/, '')
                        });
                    }
                    prependSearchbar(value);
                },
                complete: function () {
                    button.enable();
                }
            });
        }
    }

    function openJstree(id) {
        let formVisible = $('.modal .form-div form.transfer-form');
        if (formVisible.length !== 0) {
            let footerBtns = $('.modal-footer:visible button');
            footerBtns.stop(true, true).shake({duration: 1000, distance: 2});
            setTimeout(() => AsalaeGlobal.highlight(footerBtns), 100);
            return;
        }

        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/transfers/get-xpath/' . $urlId)?>/' + id + '?tempfile=true',
            method: 'GET',
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                if (content === 'not found') {
                    alert(__("Fichier non trouvé dans le XML"));
                    return;
                }
                $('a.ui-tabs-anchor[href="#edit-transfer-editor-tab"]').click();
                setTimeout(function () {
                    var value = content;
                    var buttons = $('.modal-footer:visible button').disable();
                    var loaded = 0;
                    var fn = function () {
                        loaded++;
                        if (loaded === 2) {
                            $('.modal-footer:visible button').enable();
                        } else {
                            $('.modal-footer:visible button').disable();
                        }
                        tree.disable();
                    };
                    tree.trigger('select_node.jstree', {node: {original: {search: value}}});
                    tree.trigger('select_node.jstree', {node: {original: {url: value}}});
                    tree.one('search_completed.jstree', fn);
                    tree.one('partial_edit_loaded.jstree', fn);
                }, 0);
            }
        });
    }

    function loadBinaryData(element) {
        var select = $(element).closest('.form-group').find('select');
        var uuid = select.val();
        if (!uuid) {
            return;
        }
        $('#dataobjectgroupreferenceid-0').val('');
        $('#dataobjectreferenceid-0').val(uuid);
        $('#reference-group-0').val('').trigger('change');
    }

    function loadGroupData(element) {
        var select = $(element).closest('.form-group').find('select');
        var uuid = select.val();
        if (!uuid) {
            return;
        }
        $('#dataobjectgroupreferenceid-0').val(uuid);
        $('#dataobjectreferenceid-0').val('');
        $('#reference-0').val('').trigger('change');
    }

    function ajaxSearchBinary(element) {
        $(element).select2({
            ajax: {
                url: '<?=$this->Url->build('/Transfers/populateSelect/' . ($id ?? $base64Path))?>',
                method: 'POST',
                delay: 250
            },
            escapeMarkup: function (v) {
                return $('<span></span>').html(v).text();
            },
            dropdownParent: $(element).closest('.modal'),
            allowClear: true
        });
        var button = $(element).siblings('.help-block').find('button').hide();
        $(element).on('change', function () {
            var actualValue = $('.keyword-name').val();
            if (!actualValue) {
                button.click();
            } else {
                button.show();
            }
        });
        var ref = $('#dataobjectreferenceid-0').val();
        if ($(element).val() === '' && ref !== '') {
            $(element).select2().select2('val', ref);
        }
    }

    function ajaxSearchGroup(element) {
        $(element).select2({
            ajax: {
                url: '<?=$this->Url->build('/Transfers/populateGroupSelect/' . ($id ?? $base64Path))?>',
                method: 'POST',
                delay: 250
            },
            escapeMarkup: function (v) {
                return $('<span></span>').html(v).text();
            },
            dropdownParent: $(element).closest('.modal'),
            allowClear: true
        });
        var button = $(element).siblings('.help-block').find('button').hide();
        $(element).on('change', function () {
            var actualValue = $('.keyword-name').val();
            if (!actualValue) {
                button.click();
            } else {
                button.show();
            }
        });
    }

    /**
     * Désactive les elements de choix
     * @param element
     */
    function elementsChoice(element) {
        var choices = JSON.parse($(element).attr('data-choice'));
        var el;
        for (var i = 0; i < choices.length; i++) {
            for (var j = 0; j < choices[i].length; j++) {
                el = $('[name="' + choices[i][j] + '[0][@]"]');
                el.addClass('choice')
                    .attr('data-disabled', el.is(':disabled') ? 'true' : 'false')
                    .on('change', fnChoice(i, choices));
            }
        }
        $('.choice').each(function () {
            if ($(this).val()) {
                $(this).change();
                return false;
            }
        });
    }

    function fnChoice(i, choices) {
        return function () {
            var el;
            var oneValue = false;

            hasvalue:
                for (var k = 0; k < choices.length; k++) {
                    for (var l = 0; l < choices[k].length; l++) {
                        el = $('[name="' + choices[k][l] + '[0][@]"]');
                        if (el.val()) {
                            oneValue = true;
                            break hasvalue;
                        }
                    }
                }
            for (k = 0; k < choices.length; k++) {
                for (l = 0; l < choices[k].length; l++) {
                    el = $('[name="' + choices[k][l] + '[0][@]"]');
                    if (oneValue) {
                        el.enable(k === i && el.attr('data-disabled') === 'false');
                    } else {
                        el.enable(el.attr('data-disabled') === 'false');
                    }
                }
            }
        }
    }

    function ajaxSearchAccessRuleCode(element) {
        var selectedValue = $(element).val();
        $(element).empty().val("");

        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/access-rule-codes/populateSelect')?>',
            method: 'GET',
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                var newOption;
                for (var key in content) {
                    if (AsalaeGlobal.is_numeric(key)) {
                        insertOption($(element), content[key]);
                    } else {
                        insertOptgroup($(element), key, content[key]);
                    }
                }
                $(element).val(selectedValue);
                AsalaeGlobal.select2(element);
            }
        });
    }

    function ajaxSearchAppraisalRuleCode(element) {
        var selectedValue = $(element).val();
        $(element).empty().val("");

        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/appraisal-rule-codes/populateSelect')?>',
            method: 'GET',
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                var newOption;
                for (var key in content) {
                    if (AsalaeGlobal.is_numeric(key)) {
                        insertOption($(element), content[key]);
                    } else {
                        insertOptgroup($(element), key, content[key]);
                    }
                }
                $(element).val(selectedValue);
                AsalaeGlobal.select2(element);
            }
        });
    }

    function insertOption(element, content) {
        newOption = $('<option>')
            .attr('value', content.value)
            .attr('title', content.title)
            .text(content.text);
        $(element).append(newOption);
    }

    function insertOptgroup(element, groupName, content) {
        var newGroup = $('<optgroup>').attr('label', groupName);
        for (var i = 0; i < content.length; i++) {
            insertOption(newGroup, content[i]);
        }
        $(element).append(newGroup);
    }
</script>
