<?php

/**
 * @var Asalae\View\AppView $this
 */

$modalId = 'repair-uploaded-transfer';
$baseUrl = $this->Url->build('/transfers/repair/' . $base64Path);
$urlId = $base64Path;
$searchbarId = 'transfer-repair-searchbar';
$dataId = 'data-repair-transfert';
$jstreeId = 'jstree-repair-transfert';

echo $this->Html->tag('script', "$('#repair-transferLabel').html(\"" . __("Réparer un transfert") . "\")");

require 'editrepair-common.php';
?>
<script>
    function restoreButtons() {
        var footer = modal.find('.modal-footer');
        var div = $('#<?=$dataId?>');
        var cancel = $('<button data-dismiss="modal" class="cancel btn btn-default" type="submit"></button>')
            .append('<i class="fa fa-times-circle-o" aria-hidden="true"></i>')
            .append($('<span></span>').text(' ' + __("Annuler")))
            .on('click.closeModal', function () {
                modal.modal('hide');
            });
        var createTransfer = $('<button class="create btn btn-success" type="submit"></button>')
            .append('<i class="fa fa-plus-circle" aria-hidden="true"></i>')
            .append($('<span></span>').text(' ' + __("Créer un transfert à partir des modifications")))
            .on('click.closeModal', function () {
                footer.find('button').disable();
                AsalaeLoading.ajax({
                    url: '<?=$this->Url->build('/transfers/create-from-existing/' . $base64Path)?>',
                    method: 'POST',
                    headers: {
                        Accept: 'application/json'
                    },
                    data: {
                        createTransfer: true
                    },
                    success: function (content, textStatus, jqXHR) {
                        var step = jqXHR.getResponseHeader('X-Asalae-Step');
                        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true'
                            && step
                            && step.match(/^[\w\-.'" (),\[\]]+$/)
                        ) {
                            $('.modal:visible').modal('hide');
                            eval(step);
                        } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'false') {
                            var errorUl = $('<ul class="alert alert-danger"></ul>');
                            for (var i = 0; i < content.length; i++) {
                                errorUl.append($('<li></li>').append(content[i]));
                            }
                            div.html('<h4 class="h2"><?=__("Des erreurs ont été détectées")?></h4><hr>')
                                .append(errorUl);
                            document.getElementById("<?=$dataId?>").scrollIntoView();
                        }
                    },
                    complete: function () {
                        footer.find('button').enable();
                    }
                });
            });
        var download = $('<a class="download btn btn-info"></a>')
            .attr('download', '<?=uniqid('transfer-' . (new \DateTime())->format('Ymd') . '-')?>.xml')
            .attr('href', '<?=$this->Url->build('/transfers/download/' . $base64Path)?>')
            .attr('target', '_blank')
            .html('<i class="fa fa-download" aria-hidden="true"></i> ' + __("Télécharger le transfert"))
            .on('click.closeModal', function () {
                footer.find('button').disable();
                AsalaeLoading.ajax({
                    url: '<?=$baseUrl?>',
                    method: 'POST',
                    headers: {
                        Accept: 'application/json'
                    },
                    data: {
                        download: true
                    },
                    success: function (content, textStatus, jqXHR) {
                        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                            modal.modal('hide');
                            var table = TableGenerator.instance[$('#transfers-prepare-table').attr('data-table-uid')];
                            var data = table.getDataId(content.id);
                            data.Transfers = content;
                            TableGenerator.appendActions(table.data, table.actions);
                            table.generateAll();
                        } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'false') {
                            var errorUl = $('<ul class="alert alert-danger"></ul>');
                            for (var i = 0; i < content.length; i++) {
                                errorUl.append($('<li></li>').append(content[i]));
                            }
                            div.html('<h4 class="h2"><?=__("Des erreurs ont été détectées")?></h4><hr>')
                                .append(errorUl);
                            document.getElementById("<?=$dataId?>").scrollIntoView();
                        }
                    },
                    complete: function () {
                        footer.find('button').enable();
                    }
                });
            });
        var isValid = $('<button class="precontrol btn btn-warning" type="submit"></button>')
            .append('<i class="fa fa-calendar-check-o" aria-hidden="true"></i>')
            .append($('<span></span>').text(' ' + __("Précontrôle")))
            .on('click.closeModal', function () {
                footer.find('button').disable();
                AsalaeLoading.ajax({
                    url: '<?=$baseUrl?>',
                    method: 'POST',
                    headers: {
                        Accept: 'application/json'
                    },
                    data: {
                        precontrol: true
                    },
                    success: function (content, textStatus, jqXHR) {
                        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                            alert(__("Ce transfert est prêt à être importé"));
                        } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'false') {
                            if (Array.isArray(content)) {
                                var alertMsg = '';
                                for (var i = 0; i < content.length; i++) {
                                    alertMsg += '• ' + content[i].replace(/<\/?b>/g, '')
                                        .replace(/<br\/?>/g, "\n") + "\n";
                                }
                                alert(alertMsg);
                            } else {
                                alert(content);
                            }
                        }
                    },
                    complete: function () {
                        footer.find('button').enable();
                    }
                });
            });


        footer.empty()
            .append(cancel)
            .append(isValid)
            .append(download)
            .append(createTransfer);
        $('.jstree-clicked').focus();
    }

    restoreButtons();
</script>
