<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\ORM\Entity;

$this->extend('add2-common');
$this->assign('uploadDomId', $uploadDomId = 'transfer-add-by-upload2');
$this->assign('paginatorId', 'add-by-upload2-pagination-transfer-view-attachments');
$this->assign('buttonAddZip', $buttonAddZip = 'add-by-upload2-transfer-by-zip');
$this->assign('buttonAddManual', $buttonAddManual = 'add-by-upload2-transfer-manual');
$this->assign('idPrefix', 'transfer-add-by-upload2');
$this->assign('btnGroupId', 'add-by-upload2-transfer-buttons');
$this->assign('addTransferZipDiv', $addTransferZipDiv = 'add-by-upload2-transfer-zip');
$this->assign('addTransferManualDiv', $addTransferManualDiv = 'add-by-upload2-transfer-files');
$this->assign(
    'urlZip',
    $urlZip = sprintf('/upload/add-transfer-attachment/%d/uncompress?without_tree=true', $entity->get('id'))
);

$uploadFilesTableId = $this->Upload->getTableId('transfer-add-by-upload2-zip');
$uploadFilesTableJs = $this->Table->getJsTableObject($uploadFilesTableId);

/**
 * title-buttons
 */
$this->start('title-buttons');
echo $this->MultiStepForm->template('MultiStepForm/add_transfer_upload')->step(2) . '<hr>';

$showTable = [
    __("Fichiers mentionnés dans le bordereau") => 'documents',
    __("Fichiers présents dans le transfert") => '<span id="add2-transfer-attachments-count">{attachments}</span>',
    __("Nombre de fichiers correspondants") => '<span id="add2-transfer-matching-count">{matching}</span>',
];
if ($missingAttachments) {
    $th = count($missingAttachments) < 5
        ? __("Fichiers manquants")
        : __("Les {0} premiers fichiers manquants", count($missingAttachments));
    $showTable[$th] = '<span id="add2-transfer-missing-files">{exemples|implode("<br>")}</span>';
}

echo $this->ViewTable->generate(
    new Entity(
        [
            'documents' => $countDocuments,
            'attachments' => $countAttachments,
            'matching' => $countUploadsConform,
            'exemples' => $missingAttachments,
        ]
    ),
    $showTable
);

$this->end();

/**
 * after
 */
$this->start('after');
?>
    <script>
        var missingFilenames = <?=json_encode(array_map('h', array_keys($filenames)));?>

        $('#transfer-upload-files').filter('.dropbox').off('fileSuccess').on('fileSuccess', function (event, file) {
            var table;
            table = <?=$uploadFilesTableJs?>;

            var data = table.getDataId(file.uniqueIdentifier);

            var attachmentCount = $('#add2-transfer-attachments-count');
            var matchingCount = $('#add2-transfer-matching-count');
            var missingFilesSpan = $('#add2-transfer-missing-files');

            attachmentCount.html(parseInt(attachmentCount.html(), 10) + 1);

            var index = missingFilenames.indexOf(data.filename);
            if (index !== -1) {
                missingFilenames.splice(index, 1);
                matchingCount.html(parseInt(matchingCount.html(), 10) + 1);
                var str = [];
                var limit = Math.min(missingFilenames.length, 5);
                for (var i = 0; i < limit; i++) {
                    str.push(missingFilenames[i])
                }
                missingFilesSpan.html(str.join('<br>'));
            }
        });

        $('#<?=$uploadFilesTableId?>').one('added-zip', function () {
            addTransferUploadStep2(<?=$entity->get('id')?>);
        });

        $('#<?=$addTransferManualDiv?>').hide();
        $('#<?=$addTransferZipDiv?>').show();
        <?=$entity->get('transfer_attachments') ? "$('#$uploadFilesTableId').removeClass('hide');" : '' ?>
        $('#<?=$addTransferManualDiv?> .choose-upload-type, #<?=$addTransferZipDiv?> .choose-upload-type').remove();
    </script>
<?php
$this->end();
