<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->extend('add2-common');
$this->assign('uploadDomId', 'transfer-add2');
$this->assign('paginatorId', 'add2-pagination-transfer-view-attachments');
$this->assign('buttonAddZip', $buttonAddZip = 'add2-transfer-by-zip');
$this->assign('buttonAddManual', $buttonAddManual = 'add2-transfer-manual');
$this->assign('idPrefix', 'transfer-add2');
$this->assign('btnGroupId', 'add2-transfer-buttons');
$this->assign('addTransferZipDiv', 'add2-transfer-zip');
$this->assign('addTransferManualDiv', 'add2-transfer-files');

/**
 * title-buttons
 */
$this->start('title-buttons');
echo $this->MultiStepForm->template('MultiStepForm/' . ($tmpl ?? 'add_transfer'))->step(2) . '<hr>';

$buttonsClasses = '';
if ($countAttachments && (empty($lock) || $this->getRequest()->getQuery('type'))) {
    $buttonsClasses = '.hide';
}

echo $this->Html->tag('div#add2-transfer-buttons.text-center' . $buttonsClasses);
echo $this->Html->tag(
    "button#$buttonAddZip.btn.btn-default",
    $this->Fa->i(
        'fa-file-archive-o fa-4x',
        $this->Html->tag(
            'div',
            __("Créer l'arborescence de transfert à partir d'un fichier zip/tar.gz"),
            ['style' => 'padding-top: 5px']
        )
    ),
    ['type' => 'button', 'style' => 'white-space: normal; width: 180px; min-height: 155px;']
);
echo $this->Html->tag(
    "button#$buttonAddManual.btn.btn-default",
    $this->Fa->i(
        'fa-file-o fa-4x',
        $this->Html->tag(
            'div',
            __("Ajouter les fichiers, la structuration du transfert sera faite manuellement"),
            ['style' => 'padding-top: 5px']
        )
    ),
    ['type' => 'button', 'style' => 'white-space: normal; width: 180px; min-height: 155px;']
);

echo $this->Html->tag('/div');
$this->end();
