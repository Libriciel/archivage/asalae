<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

?>
<script>
    var attachmentsToDelete = <?=json_encode($attachmentsToDelete)?>;
    var transferIsDirty = <?=$isDirty ? 'true' : 'false'?>;

    function initializeAttachmentMessage(value, context) {
        if (!value && attachmentsToDelete.indexOf(context.id) !== -1) {
            return $('<span class="text-danger"></span>').text(
                __("ce fichier sera supprimé lors de l'enregistrement")
            );
        }
        return value;
    }

    function getAttachmentClass(data) {
        if (attachmentsToDelete.indexOf(data.id) !== -1) {
            return 'warning';
        }
        if (data.valid === false) {
            return 'danger';
        }
        return '';
    }
</script>
<?php
$modalId = 'edit-transfer';
$dataId = 'data-edit-transfert';
$jstreeId = 'jstree-edit-transfert';
$searchbarId = 'transfer-edit-searchbar';
$urlId = $id;

$lock = $entity->get('transfer_lock');
if ($entity->get('has_expired_lock') && $entity->get('created_user_id') === ($user_id ?? null)) { ?>
<script>
    $('#<?=$modalId?>').modal('hide');
    // Si la fonction addTransferStep2() n'existe pas, on redirige sur les transferts en préparations
    if (!window.addTransferStep2) {
        AsalaeGlobal.interceptedLinkToAjax('<?=$this->Url->build('/transfers/index-preparating')?>');
    }
    setTimeout(() => addTransferStep2(<?=$entity->id?>), 400);
</script>
    <?php
    return;
} elseif ($lock && $lock->get('uploading')) {
    echo $this->Html->tag(
        'div.alert.alert-info',
        __(
            "Le transfert est en cours d'upload. Veuillez fermer "
            . "cette fenêtre et tenter de l'éditer plus tard."
        )
    );
    return;
} elseif ($lock) {
    echo $this->Html->tag(
        'div.alert.alert-info',
        __(
            "Le transfert est en cours de préparation sur le serveur. Veuillez fermer "
            . "cette fenêtre et tenter de l'éditer plus tard."
        )
    );
    return;
}

if ($this->getRequest()->getParam('?.add') !== false) {
    echo $this->MultiStepForm->template('MultiStepForm/add_transfer')->step(3) . '<hr>';
    $baseUrl = $this->Url->build('/transfers/edit/' . $id . '?add');
} else {
    $baseUrl = $this->Url->build('/transfers/edit/' . $id);
}

echo $this->Html->tag(
    'script',
    "$('#edit-transferLabel').html(\"" . __("Edition d'un transfert")
    . ' - ' . h($entity->get('transfer_identifier'))
    . ' - ' . $entity->get('seda_versiontrad') . "\")"
);

/**
 * Editeur XML
 */
ob_start();

require 'editrepair-common.php';
$tabs1 = ob_get_clean();

/**
 * Fichiers des pièces jointes
 */
$uploads = $this->Upload
    ->create($uploadId, ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'info' => [
                'label' => __("Fichier"),
                'target' => '',
                'thead' => [
                    'filename' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
                    'mime' => ['label' => __("Type MIME")],
                    'hash' => ['label' => __("Empreinte du fichier")],
                    'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
                    'xpath' => ['label' => __("Emplacement XPath")],
                ],
                'filter' => [
                    'filter-filename[0]' => [
                        'id' => 'filter-transfer-attachment-filename-0',
                        'name' => 'filename',
                        'label' => __("Nom de fichier"),
                    ],
                ],
            ],
            'message' => [
                'label' => __("Message"),
                'style' => 'width: 400px',
                'class' => 'message',
                'callback' => 'initializeAttachmentMessage',
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'getAttachmentClass(data[{index}])',
            'checkbox' => true,
        ]
    )
    ->actions(
        [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'displayEval' => "!data[{index}].deletable && attachmentsToDelete.indexOf(data[{index}].id) === -1",
                'label' => $view->Fa->i('fa-location-arrow'),
                'title' => $title = __("Ouvrir l'éditeur XML sur le document {0}", '{1}'),
                'aria-label' => $title,
                'onclick' => "openJstree({0})",
                'params' => ['id', 'filename'],
            ],
            function ($upload) use ($view) {
                return [
                    'data-callback' => "uncompressFile({$upload->tableObject}, {0})",
                    'type' => "button",
                    'class' => "btn-link",
                    'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id) '
                        . '&& /\.(zip|tar(\.gz)?|t?gz|rar)$/i.test(data[{index}].filename) '
                        . '&& data[{index}].deletable',
                    'label' => $view->Fa->i('fa-file-archive-o'),
                    'title' => __("Décompresser {0}", '{1}'),
                    'aria-label' => __("Décompresser {0}", '{1}'),
                    'confirm' => __(
                        "Décompresser un fichier peut prendre plusieurs minutes "
                        . "(voir plusieurs heures pour les plus gros).\nPendant la décompression,"
                        . " vous pouvez fermer la fenêtre (clic sur annuler), l'action continuera en arrière-plan."
                        . " Une notification vous sera envoyée lorsque cette action sera terminée "
                        . "si celle-ci met plus de 5 minutes à s'exécuter.\n\nVoulez-vous continuer ?"
                    ),
                    'params' => ['id', 'filename'],
                ];
            },
            [
                'href' => "/TransferAttachments/download/{0}",
                'download' => '{1}',
                'target' => '_blank',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => __("Télécharger {0}", '{1}'),
                'aria-label' => __("Télécharger {0}", '{1}'),
                'params' => ['id', 'filename'],
            ],
            function ($table) use ($view, $urlId) {
                $deleteUrl = $view->Url->build('/Transfers/deleteAttachment/' . $urlId);
                return [
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'displayEval' => "data[{index}].deletable && attachmentsToDelete.indexOf(data[{index}].id) === -1",
                    'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'data-callback' => "deleteAttachmentAction({$table->tableObject}, '$deleteUrl', {0}, false)",
                    'confirm' => __(
                        "Cette action ne pourra pas être annulée. Êtes-vous sûr de vouloir supprimer ce fichier ?"
                    ),
                    'params' => ['id', 'filename'],
                ];
            },
        ]
    );

$paginator = $this->AjaxPaginator->create('pagination-transfer-view-attachments')
    ->url(
        [
            'controller' => 'transfers',
            'action' => 'view-attachments',
            $urlId,
            '?' => ['tempfile' => true],
        ]
    )
    ->table($uploads->table)
    ->count($countAttachments);

$tabs2 = $this->Form->create($entity, ['idPrefix' => 'transfer-add-edit']);

$tabs2 .= $this->Html->tag(
    'div',
    $paginator->scrollBottom(['style' => 'position: absolute; right:0; z-index:100']),
    ['style' => 'position: relative']
);
$tabs2 .= $this->Form->control('id', ['type' => 'hidden']);
$tabs2 .= $this->Html->tag(
    'div.form-group.fake-input',
    $this->Html->tag('span.fake-label', __("Téléversement de fichiers"))
    . $uploads->generate(
        [
            'singleFile' => false,
            'allowDuplicateUploads' => true,
            'target' => $this->Url->build('/upload/add-transfer-attachment/' . $urlId),
        ]
    )
);

// Action groupés type "pagination"
$tabs2 .= '<hr>';
$tabs2 .= $paginator->generate(
    [
        'actions' => [
            '0' => __("-- Action groupée --"),
            'delete' => __("Supprimer"),
        ],
    ]
);
$tabs2 .= $paginator->scrollTop();
$tabs2 .= $this->Form->end();

$tabs = $this->Tabs->create('edit-transfer-tabs-content', ['class' => 'row no-padding']);
$tabs->add(
    'edit-transfer-editor-tab',
    $this->Fa->i('fa-file-code-o', __("Editeur XML")),
    $tabs1
);
$tabs->add(
    'edit-transfer-files-tab',
    $this->Fa->i('fa-paperclip', __("Fichiers des pièces jointes")),
    $tabs2
);
echo $tabs;
?>

<script>
    var uploadTable = $('#transfer-edit-upload-table-table');

    function executeGroupActionAddTransferAttachments(select) {
        var checked = uploadTable.find('tbody .td-checkbox input:checked');
        if ($(select).val() === 'delete') {
            if (checked.length) {
                checked = checked.filter(function () {
                    return $(this).closest('tr').find('td.action button.delete:enabled').length > 0;
                });
            }
            if (checked.length === 0 || !confirm(__("Êtes-vous sûr de vouloir supprimer ces enregistrements ?"))) {
                return;
            }
            checked.each(function () {
                var tr = $(this).closest('tr');
                eval(tr.find('td.action button.delete').attr('data-callback'));
            });
        }
    }

    function deleteByFilename(filename) {
        var table = {};
        table = <?=$uploads->table->tableObject?>;
        var line = table.data.filter(function (v) {
            return v.filename === filename;
        })[0];
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/transfers/deleteAttachment/' . $urlId)?>',
            method: 'DELETE',
            headers: {
                Accept: 'application/json'
            },
            data: {
                filename: filename
            },
            success: function (content) {
                if (content.report === 'done') {
                    if (line) {
                        table.removeDataId(line.id);
                        table.generateAll();
                        table.decrementPaginator();
                    }
                } else if (content.report === 'scheduled') {
                    attachmentsToDelete = content.toDelete;
                    table.generateAll();
                } else {
                    alert(message.report);
                    this.error();
                }
            }
        });
    }

    function deleteAttachments(ids) {
        var table = {};
        table = <?=$uploads->table->tableObject?>;
        var lines = table.data.filter(function (v) {
            return ids.indexOf(v.id) !== -1;
        });
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/transfers/deleteAttachment/' . $urlId)?>',
            method: 'DELETE',
            headers: {
                Accept: 'application/json'
            },
            data: {
                ids: ids
            },
            success: function (content) {
                for (var i = 0; i < content.deleted.length; i++) {
                    table.removeDataId(content.deleted[i].id);
                    table.decrementPaginator();
                }
                attachmentsToDelete = content.toDelete;
                table.generateAll();
                tree.trigger('deleted_files.jstree');
            }
        });
    }

    function deleteAttachmentAction(table, url, id) {
        var tr = $(table.table).find('tr[data-id="' + id + '"]');
        tr.find('button').addClass('deleting').addClass('gray').addClass('disabled').disable();
        var line = table.getDataId(id);
        AsalaeLoading.ajax({
            url: url,
            method: 'DELETE',
            headers: {
                Accept: 'application/json'
            },
            data: {
                filename: line.filename
            },
            success: function (content) {
                if (content.report === 'done') {
                    table.removeDataId(id);
                    tr.fadeOut(400, function () {
                        $(this).remove();
                        table.decrementPaginator();
                    });
                } else if (content.report === 'scheduled') {
                    attachmentsToDelete = content.toDelete;
                    table.generateAll();
                } else {
                    alert(message.report);
                    this.error();
                }
            },
            error: function (e) {
                console.error(e);
                tr.addClass('danger');
            },
            complete: function () {
                transferIsDirty = true;
                restoreButtons();
            }
        });
    }

    /**
     * @param {TableGenerator} uploadTable
     * @param {string|int} id
     * @return {Function}
     */
    function uncompressFile(uploadTable, id) {
        var tr = uploadTable.table.find('tr[data-id=' + id + ']');
        tr.find('td.action a, td.action button').disable();
        var msgId = 'transfer-upload-' + id + '-' + Math.floor(Math.random() * (16777215 - 1048576 + 1) + 1048576);
        var msg = $('<span></span>').attr('id', msgId);
        var td = tr.addClass('warning')
            .find('td.message')
            .append(
                $('<div class="text-center"></div>')
                    .append('<i aria-hidden="true" class="fa fa-2x fa-spinner faa-spin animated"></i>')
            )
            .append(msg);
        var first = true;
        var tableData = uploadTable.getDataId(id);
        AsalaeWebsocket.connect(
            '<?=Configure::read('Ratchet.connect')?>',
            function (session) {
                session.subscribe('addcompressed_' + id, function (topic, data) {
                    var nmsg = $('#' + msgId);
                    if (nmsg.get(0) !== msg.get(0)) {
                        msg = nmsg;
                        td = nmsg.closest('td');
                    }
                    msg.text(data.message);
                    tableData.message = td.html();
                    if (first) {
                        first = false;
                        AsalaeLoading.stop();
                    }
                });
            },
            function () {
            },
            {'skipSubprotocolCheck': true}
        );

        AsalaeLoading.ajax({
            url: '<?=$this->Url->build("/transfers/add-compressed")?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            timeout: 0,
            success: function (content, textStatus, jqXHR) {
                if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                    uploadTable.removeDataId(id);
                    tr.fadeOut(400, function () {
                        $(this.remove());
                        $(uploadTable.table).trigger('change');
                    });
                    for (var i = 0; i < content.length; i++) {
                        uploadTable.data.push(content[i]);
                    }
                    TableGenerator.appendActions(uploadTable.data, uploadTable.actions);
                    uploadTable.generateAll();
                } else {
                    this.error();
                }
            },
            error: function (error, message) {
                console.error(error);
                tr.find('td.action a, td.action button').enable();
                tr.find('td.message').text(message);
                tr.addClass('danger').removeClass('warning');
            }
        });
    }

    function executeGroupActionEditTransferAttachments(select) {
        if ($(select).val() === '1') {
            if (!confirm(__("Êtes-vous sûr de vouloir supprimer cet enregistrement ?"))) {
                return;
            }
            $('#transfer-edit-files-table').find('tbody .td-checkbox input:checked').each(function () {
                var tr = $(this).closest('tr');
                var id = tr.attr('data-id');
                var action = tr.find('td.action button.delete')
                    .attr('onclick')
                    .match(/TableGenericAction\.deleteAction\((\w+), '([\w-\/]+)'\)\((\d+)\)/);
                if (action && action.length) {
                    TableGenericAction.deleteAction(eval(action[1]), action[2])(action[3], false);
                } else {
                    alert(PHP.messages.genericError);
                }
            });
        }
    }

    var detectedError = false;

    function restoreButtons() {
        var footer = modal.find('.modal-footer');
        var cancel = $('<button data-dismiss="modal" class="cancel btn btn-default" type="submit"></button>')
            .append('<i class="fa fa-times-circle-o" aria-hidden="true"></i>')
            .append($('<span></span>').text(' ' + __("Annuler")))
            .on('click.closeModal', function () {
                modal.modal('hide');
                AsalaeLoading.ajax({
                    url: '<?=$baseUrl?>',
                    method: 'DELETE',
                    headers: {
                        Accept: 'application/json'
                    }
                });
            });
        var accept = $('<button class="create btn btn-primary accept" type="submit"></button>')
            .append('<i class="fa fa-floppy-o" aria-hidden="true"></i>')
            .append($('<span></span>').text(' ' + __("Enregistrer")))
            .on('click.closeModal', function () {
                var url = '<?=$baseUrl?>';
                if (detectedError) {
                    var msg = "Attention, il manque des informations obligatoires au regard du SEDA."
                        + " Voulez-vous enregistrer le transfert dans l'état ?";
                    if (confirm(__(msg))) {
                        url += (url.indexOf('?') >= 0 ? '&' : '?') + 'force=true';
                    } else {
                        return;
                    }
                }
                if (transferIsDirty) {
                    var div = $('#<?=$dataId?>');
                    tree.disable();
                    AsalaeLoading.ajax({
                        url: url,
                        method: 'POST',
                        headers: {
                            Accept: 'application/json'
                        },
                        success: function (content, textStatus, jqXHR) {
                            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                                modal.modal('hide');
                                var tableUid = $('table[data-table-uid]:visible').attr('data-table-uid');
                                if (tableUid) {
                                    var table = TableGenerator.instance[tableUid];
                                    var data = table.getDataId(content.id);
                                    if (!data) { // cas indexes validation-processes
                                        data = table.data.find(x => x.transfer.id === content.id);
                                        if (data) {
                                            data.transfer = content;
                                        }
                                    } else {
                                        data.Transfers = content;
                                    }
                                    TableGenerator.appendActions(table.data, table.actions);
                                    table.generateAll();
                                }
                            } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'false') {
                                var errorUl = $('<ul class="alert alert-danger"></ul>');
                                for (var i = 0; i < content.length; i++) {
                                    errorUl.append($('<li></li>').append(content[i]));
                                }
                                div.html('<h4 class="h2"><?=__("Des erreurs ont été détectées")?></h4><hr>')
                                    .append(errorUl);
                                footer.find('button.accept')
                                    .removeClass('btn-primary')
                                    .addClass('btn-warning')
                                    .find('span')
                                    .text(' ' + __("Enregistrer dans l'état"));
                                document.getElementById("<?=$dataId?>").scrollIntoView();
                                detectedError = true;
                            } else {
                                div.html(content);
                            }
                        },
                        error: function (error) {
                            div.html('');
                        },
                        complete: function () {
                            tree.enable();
                        }
                    });
                } else {
                    modal.modal('hide');
                    AsalaeLoading.ajax({
                        url: '<?=$baseUrl?>',
                        method: 'DELETE',
                        headers: {
                            Accept: 'application/json'
                        }
                    });
                }
            });
        footer.empty()
            .append(cancel)
            .append(accept);
        $('.jstree-clicked').focus();
    }

    restoreButtons();

    $('#btn-pagination-transfer-view-attachments').click(function () {
        executeGroupActionAddTransferAttachments('#select-pagination-transfer-view-attachments');
    });

    uploadTable.on('change', function () {
        transferIsDirty = true;
        restoreButtons();
    });
</script>
