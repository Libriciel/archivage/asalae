<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

$jsTable = $this->Table->getJsTableObject($tableId);
$deleteUrl = $this->Url->build('/Transfers/delete');
$sendUrl = $this->Url->build('/Transfers/send');
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Transfers.transfer_date' => [
                'label' => __("Date du transfert"),
                'type' => 'datetime',
                'order' => 'transfer_date',
            ],
            'Transfers.transfer_identifier' => [
                'label' => __("Identifiant"),
                'order' => 'transfer_identifier',
                'filter' => [
                    'transfer_identifier[0]' => [
                        'id' => 'filter-transfer-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
                'callback' => 'TableHelper.wordBreak()',
            ],
            'Transfers.message_versiontrad' => [
                'label' => __("Version SEDA"),
                'display' => false,
                'order' => 'message_version',
            ],
            'Transfers.statetrad' => [
                'label' => __("Etat"),
                'titleEval' => 'Transfers.statetrad_title',
                'style' => 'cursor: help',
                'filter' => [
                    'state[0]' => [
                        'id' => 'filter-state-0',
                        'label' => false,
                        'aria-label' => __("Etat"),
                        'options' => $states,
                    ],
                ],
            ],
            'Transfers.archival_agency.name' => [
                'label' => __("Service d'Archives"),
                'display' => false,
            ],
            'Transfers.transferring_agency.name' => [
                'label' => __("Service versant"),
                'filter' => [
                    'transferring_agency_id[0]' => [
                        'id' => 'filter-transferring_agency_id-0',
                        'label' => false,
                        'aria-label' => __("Service versant"),
                        'options' => $transferring_agencies,
                    ],
                ],
            ],
            'Transfers.originating_agency.name' => [
                'label' => __("Service producteur"),
                'filter' => [
                    'originating_agency_id[0]' => [
                        'id' => 'filter-originating_agency_id-0',
                        'label' => false,
                        'aria-label' => __("Service producteur"),
                        'options' => $originating_agencies,
                    ],
                ],
                'display' => false,
            ],
            'Transfers.transfer_comment' => [
                'label' => __("Commentaire"),
            ],
            'Transfers.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'created',
            ],
            'Transfers.data_size' => [
                'label' => __("Taille PJs"),
                'display' => false,
                'callback' => 'TableHelper.readableBytes',
            ],
            'Transfers.created_user.username' => [
                'label' => __("Créé par"),
                'display' => false,
                'filter' => [
                    'created_user_id[0]' => [
                        'id' => 'filter-created_user_id-0',
                        'label' => false,
                        'aria-label' => __("Créé par"),
                        'options' => $createdUsers,
                    ],
                ],
            ],
            'Transfers.files_deleted' => [
                'label' => __("Fichiers supprimés"),
                'display' => false,
                'type' => 'boolean',
                'callback' => 'TableHelper.boolean',
                'filter' => [
                    'files_deleted[0]' => [
                        'id' => 'filter-files_deleted-0',
                        'label' => __("Fichiers supprimés"),
                        'options' => [
                            '0' => __("Non"),
                            '1' => __("Oui"),
                        ],
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'checkbox' => true,
            'identifier' => 'Transfers.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'data-action' => __("Visualiser"),
                'title' => __("Voir {0}", '{1}'),
                'aria-label' => __("Voir {0}", '{1}'),
                'display' => $this->Acl->check('/transfers/view'),
                'params' => ['Transfers.id', 'Transfers.transfer_identifier'],
            ],
            [
                'href' => "/Transfers/download/{0}",
                'download' => '{1}',
                'target' => '_blank',
                'data-action' => __("Télécharger"),
                'label' => $this->Fa->charte('Télécharger'),
                'title' => $title = __("Télécharger {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/transfers/download'),
                'params' => ['Transfers.id', 'Transfers.filename'],
            ],
            [
                'data-callback' => 'transferFileDownload({0})',
                'confirm' => __("Voulez-vous télécharger les fichiers de ce transfert ?"),
                'label' => $this->Fa->i('fa-file-archive-o'),
                'class' => 'btn-link',
                'title' => $title = __("Télécharger les fichiers de {0}", '{1}'),
                'data-action' => __("Télécharger le zip"),
                'display' => $this->Acl->check('/transfers/downloadFiles'),
                'aria-label' => $title,
                'displayEval' => "!data[{index}].Transfers.files_deleted && data[{index}].Transfers.data_size > 0",
                'params' => ['Transfers.id', 'Transfers.name'],
            ],
            [
                'href' => "/Transfers/download-pdf/{0}",
                'download' => '{2}',
                'target' => '_blank',
                'data-action' => __("Télécharger le PDF"),
                'label' => $this->Fa->i('fa-file-pdf'),
                'title' => $title = __("Télécharger le PDF de {0}", '{1}'),
                'aria-label' => $title,
                'data-renewable' => true,
                'display' => $this->Acl->check('/transfers/download-pdf'),
                'displayEval' => '!data[{index}].Transfers.is_large',
                'params' => ['Transfers.id', 'Transfers.transfer_identifier', 'Transfers.pdf_basename'],
            ],
            [
                'onclick' => 'transferPdfDownload({0})',
                'type' => 'button',
                'class' => 'btn-link',
                'data-action' => __("Télécharger le PDF (large)"),
                'label' => $this->Fa->i('fa-file-pdf'),
                'title' => $title = __("Télécharger le PDF de {0}", '{1}'),
                'aria-label' => $title,
                'data-renewable' => true,
                'display' => $this->Acl->check('/transfers/download-pdf'),
                'displayEval' => Configure::read('Transfers.force_mail')
                    ? 'true'
                    : 'data[{index}].Transfers.is_large',
                'params' => ['Transfers.id', 'Transfers.transfer_identifier'],
            ],
            [
                'onclick' => "wrapAnalyseTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-stethoscope'),
                'title' => __("Analyser {0}", '{1}'),
                'aria-label' => __("Analyser {0}", '{1}'),
                'display' => $this->Acl->check('/transfers/analyse'),
                'displayEval' => 'data[{index}].Transfers.state === "'
                    . (isset($transferEntrantIndex) ? 'validating' : 'preparating') . '"', // @temporary, cf. #304
                'params' => ['Transfers.id', 'Transfers.transfer_identifier'],
            ],
            [
                'href' => $this->Url->build('/transfers/display-xml') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'data-action' => __("Afficher"),
                'title' => __("Afficher le bordereau {0}", '{1}'),
                'aria-label' => __("Voir {0}", '{1}'),
                'display' => $this->Acl->check('/transfers/display-xml'),
                'displayEval' => '!data[{index}].Transfers.is_large',
                'params' => ['Transfers.id', 'Transfers.transfer_identifier'],
            ],
            [
                'onclick' => "exploreTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'data-action' => __("Afficher (large)"),
                'data-toggle' => 'modal',
                'data-target' => '#' . $modalXmlToHtml,
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => __("Afficher le bordereau {0}", '{1}'),
                'aria-label' => __("Voir {0}", '{1}'),
                'display' => $this->Acl->check('/transfers/explore'),
                'displayEval' => Configure::read('Transfers.force_explore')
                    ? 'true'
                    : 'data[{index}].Transfers.is_large',
                'params' => ['Transfers.id', 'Transfers.transfer_identifier'],
            ],
            [
                'onclick' => "duplicateTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'data-action' => __("Dupliquer"),
                'label' => $this->Fa->i('fa-files-o'),
                'title' => __("Dupliquer le bordereau {0}", '{1}'),
                'aria-label' => __("Dupliquer {0}", '{1}'),
                'display' => $this->Acl->check('/transfers/duplicate'),
                'params' => ['Transfers.id', 'Transfers.transfer_identifier'],
            ],
            [
                'onclick' => "wrapEditTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'display' => $this->Acl->check('/transfers/edit'),
                'displayEval' => 'data[{index}].Transfers.editable',
                'params' => ['Transfers.id', 'Transfers.transfer_identifier'],
            ],
            [
                'data-callback' => "reEditTransfer({0})",
                'confirm' => __("Voulez-vous rendre ce transfert éditable à nouveau ?"),
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-edit'),
                'data-action' => __("Ré-éditer"),
                'title' => __("Ré-éditer {0}", '{1}'),
                'aria-label' => __("Ré-éditer {0}", '{1}'),
                'display' => $this->Acl->check('/transfers/reEdit'),
                'displayEval' => 'data[{index}].Transfers.state === "rejected" '
                    . '&& data[{index}].Transfers.created_user_id === PHP.user_id',
                'params' => ['Transfers.id'],
            ],
            [
                'data-callback' => "wrapSendTransfer({$jsTable}, '$sendUrl', {0})",
                'type' => 'button',
                'class' => 'btn-link send',
                'display' => $this->Acl->check('/transfers/send'),
                'displayEval' => 'data[{index}].Transfers.state === "preparating" '
                    . '&& data[{index}].Transfers.created_user_id === PHP.user_id',
                'label' => $this->Fa->charte('Envoyer', '', 'text-success'),
                'title' => __("Envoyer {0}", '{1}'),
                'aria-label' => __("Envoyer {0}", '{1}'),
                'confirm' => __(
                    "Une fois le transfert envoyé, il ne sera plus "
                    . "possible de le modifier. Voulez-vous continuer ?"
                ),
                'params' => ['Transfers.id', 'Transfers.transfer_identifier'],
            ],
            [
                'data-callback'
                => "TableGenericAction.deleteAction({$jsTable}, '$deleteUrl')({0}, false)",
                'type' => 'button',
                'class' => 'btn-link delete',
                'data-action' => __("Supprimer"),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => __("Supprimer {0}", '{1}'),
                'aria-label' => __("Supprimer {0}", '{1}'),
                'confirm' => __("Êtes-vous sûr de vouloir supprimer ce bordereau ?"),
                'display' => $this->Acl->check('/transfers/delete'),
                'displayEval' => 'data[{index}].Transfers.created_user_id === PHP.user_id '
                    . '&& data[{index}].Transfers.deletable',
                'params' => ['Transfers.id', 'Transfers.transfer_identifier'],
            ],
            [
                'onclick' => "forceDeleteTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link delete',
                'data-action' => __("Supprimer"),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => __("Supprimer {0}", '{1}'),
                'aria-label' => __("Supprimer {0}", '{1}'),
                'display' => $this->Acl->check('/transfers/delete'),
                'displayEval' => 'data[{index}].Transfers.created_user_id !== PHP.user_id '
                    . '&& data[{index}].Transfers.force_deletable '
                    . '&& data[{index}].Transfers.deletable',
                'params' => ['Transfers.id', 'Transfers.transfer_identifier'],
            ],
            function ($table) {
                /** @var Asalae\View\AppView $this */
                $deleteUrl = $this->Url->build('/Transfers/hide');
                return [
                    'data-callback'
                    => "TableGenericAction.deleteAction({$table->tableObject}, '$deleteUrl')({0}, false)",
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'data-action' => __("Cacher"),
                    'label' => $this->Fa->i('fa-eye-slash text-danger'),
                    'title' => __("Cacher {0}", '{1}'),
                    'aria-label' => __("Cacher {0}", '{1}'),
                    'confirm' => __(
                        "Cette action est équivalente à une suppression mais garde une trace du transfert. "
                        . "Êtes-vous sûr de vouloir cacher ce transfert ?"
                    ),
                    'display' => $this->Acl->check('/transfers/hide'),
                    'displayEval' => 'data[{index}].Transfers.hiddable',
                    'params' => ['Transfers.id', 'Transfers.transfer_identifier'],
                ];
            },
        ]
    );

$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des transferts"),
        'table' => $table,
        'pagination' => [
            'options' => !empty($groupedActions) ? ['actions' => $groupedActions] : [],
            'callback' => 'updatePaginationAjaxCallback',
        ],
    ]
);
?>
<script>
    function forceDeleteTransfer(id) {
        var deleteWord = prompt(
            __(
                "Le transfert est peut-être encore en cours d'envoi.\n"
                + "Si vous souhaitez supprimer ce transfert, taper le mot 'delete' et cliquez sur le bouton OK"
            )
        );
        if (deleteWord === 'delete') {
            TableGenericAction.deleteAction(<?=$jsTable?>, '<?=$deleteUrl?>')(id, false);
        }
    }
</script>
