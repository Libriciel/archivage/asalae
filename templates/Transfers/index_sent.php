<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Transferts"));
$this->Breadcrumbs->add(__("Mes transferts envoyés"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-list', __("Mes transferts envoyés")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

require 'index-common.php';
?>
<script>
    $(function() {
        AsalaeWebsocket.connect(
            '<?=Configure::read('Ratchet.connect')?>',
            function (session) {
                session.subscribe('transfer_sent', function (topic, data) {
                    if (data.message.user_id === PHP.user_id) {
                        AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
                    }
                });
            },
            function () {
            },
            {'skipSubprotocolCheck': true}
        );
    });
</script>

