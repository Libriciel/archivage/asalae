<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>

<ArchiveTransfer xmlns="fr:gouv:culture:archivesdefrance:seda:v2.1">
    <?= $transfer_comment ? '    <Comment>' . $transfer_comment . '</Comment>' . PHP_EOL : '' ?>
    <Date><?= $transfer_date ?></Date>
    <MessageIdentifier><?= $transfer_identifier ?></MessageIdentifier>
    <?= $agreement ? '        <ArchivalAgreement>' . $agreement . '</ArchivalAgreement>' . PHP_EOL : '' ?>
    <CodeListVersions></CodeListVersions>
    <DataObjectPackage>
        <DescriptiveMetadata>
            <ArchiveUnit id="UUID-<?= $uuid ?>">
                <?= $profile ? '                <ArchiveUnitProfile>' . $profile . '</ArchiveUnitProfile>' . PHP_EOL
                    : '' ?>
                <Management>
                    <?php if (!empty($final)) : ?>
                        <AppraisalRule>
                            <Rule><?= $duration ?></Rule>
                            <StartDate><?= $final_start_date ?></StartDate>
                            <FinalAction><?= $final === 'detruire' ? 'Destroy' : 'Keep' ?></FinalAction>
                        </AppraisalRule>
                    <?php endif; ?>
                    <AccessRule>
                        <Rule><?= $code ?></Rule>
                        <StartDate><?= $start_date ?></StartDate>
                    </AccessRule>
                </Management>
                <Content>
                    <DescriptionLevel><?= $description_level ?></DescriptionLevel>
                    <Title><?= $name ?></Title>
                    <Language>fra</Language>
                    <DescriptionLanguage>fra</DescriptionLanguage>
                    <?php if (!empty($originating_agency)) : ?>
                        <OriginatingAgency>
                            <Identifier><?= h($originating_agency->get('identifier')) ?></Identifier>
                            <OrganizationDescriptiveMetadata>
                                <Description xmlns="fr:gouv:culture:archivesdefrance:seda:v1.0"><?=
                                    h($originating_agency->get('description'))
                                ?></Description>
                                <Name xmlns="fr:gouv:culture:archivesdefrance:seda:v1.0"><?=
                                    h($originating_agency->get('name'))
                                ?></Name>
                            </OrganizationDescriptiveMetadata>
                        </OriginatingAgency>
                    <?php endif; ?>
                </Content>
            </ArchiveUnit>
        </DescriptiveMetadata>
        <ManagementMetadata>
            <?= $profile ? '            <ArchivalProfile>' . $profile . '</ArchivalProfile>' . PHP_EOL : '' ?>
            <?= $service_level ? '            <ServiceLevel>' . $service_level . '</ServiceLevel>' . PHP_EOL : '' ?>
        </ManagementMetadata>
    </DataObjectPackage>
    <ArchivalAgency>
        <Identifier><?= h($archival_agency->get('identifier')) ?></Identifier>
        <OrganizationDescriptiveMetadata>
            <Description xmlns="fr:gouv:culture:archivesdefrance:seda:v1.0"><?= h(
                $archival_agency->get('description')
            ) ?></Description>
            <Name xmlns="fr:gouv:culture:archivesdefrance:seda:v1.0"><?= h($archival_agency->get('name')) ?></Name>
        </OrganizationDescriptiveMetadata>
    </ArchivalAgency>
    <TransferringAgency>
        <Identifier><?= h($transferring_agency->get('identifier')) ?></Identifier>
        <OrganizationDescriptiveMetadata>
            <Description xmlns="fr:gouv:culture:archivesdefrance:seda:v1.0"><?= h(
                $transferring_agency->get('description')
            ) ?></Description>
            <Name xmlns="fr:gouv:culture:archivesdefrance:seda:v1.0"><?= h($transferring_agency->get('name')) ?></Name>
        </OrganizationDescriptiveMetadata>
    </TransferringAgency>
</ArchiveTransfer>
