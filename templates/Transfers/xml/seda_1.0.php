<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>

<ArchiveTransfer xmlns="fr:gouv:culture:archivesdefrance:seda:v1.0">
    <?= $transfer_comment ? '    <Comment languageID="fr">' . $transfer_comment . '</Comment>' . PHP_EOL : '' ?>
    <Date><?= $transfer_date ?></Date>
    <TransferIdentifier><?= $transfer_identifier ?></TransferIdentifier>
    <ArchivalAgency>
        <Description><?= h($archival_agency->get('description')) ?></Description>
        <Identification><?= h($archival_agency->get('identifier')) ?></Identification>
        <Name><?= h($archival_agency->get('name')) ?></Name>
    </ArchivalAgency>
    <TransferringAgency>
        <Description><?= h($transferring_agency->get('description')) ?></Description>
        <Identification><?= h($transferring_agency->get('identifier')) ?></Identification>
        <Name><?= h($transferring_agency->get('name')) ?></Name>
    </TransferringAgency>
    <Archive>
        <?= $agreement ? '        <ArchivalAgreement>' . $agreement . '</ArchivalAgreement>' . PHP_EOL : '' ?>
        <?= $profile ? '        <ArchivalProfile>' . $profile . '</ArchivalProfile>' . PHP_EOL : '' ?>
        <DescriptionLanguage>fra</DescriptionLanguage>
        <Name><?= $name ?></Name>
        <?= $service_level ? '        <ServiceLevel>' . $service_level . '</ServiceLevel>' . PHP_EOL : '' ?>
        <ContentDescription>
            <DescriptionLevel><?= $description_level ?></DescriptionLevel>
            <Language>fra</Language>
            <?php if ($originating_agency) : ?>
                <OriginatingAgency>
                    <Description><?= h($originating_agency->get('description')) ?></Description>
                    <Identification><?= h($originating_agency->get('identifier')) ?></Identification>
                    <Name><?= h($originating_agency->get('name')) ?></Name>
                </OriginatingAgency>
            <?php endif; ?>
        </ContentDescription>
        <AccessRestrictionRule>
            <Code listVersionID="edition 2009"><?= $code ?></Code>
            <StartDate><?= $start_date ?></StartDate>
        </AccessRestrictionRule>
        <?php if (!empty($final)) : ?>
            <AppraisalRule>
                <Code><?= $final ?></Code>
                <Duration><?= $duration ?></Duration>
                <StartDate><?= $final_start_date ?></StartDate>
            </AppraisalRule>
        <?php endif; ?>
    </Archive>
</ArchiveTransfer>