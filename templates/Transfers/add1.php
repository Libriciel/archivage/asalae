<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;
use Cake\I18n\DateTime as CakeDateTime;

$view = $this;

echo $this->MultiStepForm->template('MultiStepForm/add_transfer')->step(1) . '<hr>';

echo $this->Form->create(
    $entity,
    ['id' => 'transfer-add1-form', 'idPrefix' => 'transfer-add1', 'autocomplete' => 'off']
)
    . $this->Form->control(
        'message_version',
        [
            'label' => __("Version du SEDA"),
            'options' => $message_versions,
            'default' => Configure::read('Transfers.default.message_version', 'seda1.0'),
            'empty' => false,
        ]
    )
    . $this->Form->control(
        'transfer_identifier',
        [
            'label' => "TransferIdentifier : " . __("Identifiant du transfert"),
            'placeholder' => __("Laisser vide pour une définition automatique"),
            'required' => false,
        ]
    )
    . $this->Form->control(
        'transfer_date',
        [
            'label' => "Date : " . __("Date de l'émission du message"),
            'type' => 'text',
            'append' => $this->Date->datetimePicker('#transfer-add1-transfer-date'),
            'class' => 'datepicker',
            'required' => true,
            'default' => (new CakeDateTime())->i18nFormat(),
        ]
    )
    . $this->Form->control(
        'transfer_comment',
        [
            'label' => 'Comment : ' . __("Commentaires"),
        ]
    )
    . $this->Form->control(
        'transferring_agency_id',
        [
            'label' => 'TransferringAgency : ' . __("Service versant"),
            'options' => $transferringAgencies,
            'empty' => __("-- Sélectionner un service versant --"),
            'default' => Configure::read('Transfers.default.transferring_agency_id'),
        ]
    )
    . $this->Form->control(
        'originating_agency_id',
        [
            'label' => 'OriginatingAgency : ' . __("Service producteur"),
            'options' => $originating_agencies,
            'empty' => __("-- Sélectionner un service producteur --"),
        ]
    )
    . $this->Form->control(
        'agreement_id',
        [
            'label' => 'ArchivalAgreement : ' . __("Accord de versement"),
            'options' => $agreements,
            'empty' => __("-- Sélectionner un accord de versement --"),
        ]
    )
    . $this->Form->control(
        'profile_id',
        [
            'label' => 'ArchivalProfile : ' . __("Profil d'archive"),
            'options' => $profiles,
            'empty' => __("-- Sélectionner un profil d'archive --"),
        ]
    )
    . $this->Form->control(
        'service_level_id',
        [
            'label' => "ServiceLevel : " . __("Niveau de service demandé (disponibilité, sécurité...)."),
            'options' => $service_levels,
            'empty' => __("-- Sélectionner un niveau de service --"),
        ]
    )
    . '<hr>'
    . $this->Form->control(
        'name',
        [
            'label' => "Name : " . __("Intitulé de l'<{0}>.", 'Archive'),
            'required' => true,
            'default' => Configure::read('Transfers.default.name'),
        ]
    )
    . $this->Form->control(
        'description_level',
        [
            'label' => 'DescriptionLevel : ' . __("Niveau de description de l'archive"),
            'required' => true,
            'options' => $description_levels,
            'empty' => __("-- Sélectionner un niveau de description --"),
            'default' => Configure::read('Transfers.default.description_level'),
        ]
    )
    . $this->Form->fieldset(
        $this->Form->control(
            'code',
            [
                'label' => 'Code : ' . __("Règle du délai de communicabilité"),
                'options' => $seda1AccessRuleCodes,
                'required' => true,
                'empty' => __("-- Sélectionner un code --"),
                'default' => Configure::read('Transfers.default.code'),
            ]
        )
        . $this->Form->control(
            'start_date',
            [
                'label' => 'StartDate : ' . __("Date de départ du calcul (communicabilité)"),
                'append' => $this->Date->picker('#transfer-add1-start-date'),
                'class' => 'datepicker',
                'required' => true,
                'placeholder' => __("jj/mm/aaaa"),
                'default' => Configure::read('Transfers.default.start_date'),
            ]
        ),
        ['legend' => 'AccessRestrictionRule : ' . __("Communicabilité")]
    )
    . $this->Form->fieldset(
        $this->Form->control(
            'final',
            [
                'label' => 'Code : ' . __("Sort final à appliquer"),
                'options' => $finals,
                'empty' => __("-- Choisir un sort final --"),
            ]
        )
        . $this->Form->control(
            'duration',
            [
                'label' => 'Duration : ' . __("Durée d'utilité administrative"),
                'options' => $seda1AppraisalRuleCodes,
                'empty' => __("-- Sélectionner une DUA -- "),
            ]
        )
        . $this->Form->control(
            'final_start_date',
            [
                'label' => 'StartDate : ' . __("Date de départ du calcul (sort final)"),
                'append' => $this->Date->picker('#transfer-add1-final-start-date'),
                'class' => 'datepicker',
                'placeholder' => __("jj/mm/aaaa"),
            ]
        ),
        ['legend' => 'AppraisalRule : ' . __("Sort final")]
    )
    . $this->Form->end();
?>
<script>
    var inputs = $('#transfer-add1-final, #transfer-add1-duration, #transfer-add1-final-start-date');
    inputs.change(function () {
        var required = false;
        inputs.each(function () {
            if ($(this).val()) {
                required = true;
                return false;
            }
        });
        if (required) {
            inputs.prop('required', true);
            inputs.closest('.form-group').addClass('required');
        } else {
            inputs.prop('required', false);
            inputs.closest('.form-group').removeClass('required');
        }
    });

    $('#transfer-add1-form').find('select').each(function () {
        AsalaeGlobal.select2($(this));
    });

    $('#transfer-add1-message-version').change(function () {
        var accessRuleCodes;
        var appraisalRuleCodes;
        if ($(this).val() === 'seda1.0') {
            accessRuleCodes = <?=json_encode($seda1AccessRuleCodes)?>;
            appraisalRuleCodes = <?=json_encode($seda1AppraisalRuleCodes)?>;
        } else {
            accessRuleCodes = <?=json_encode($seda2AccessRuleCodes)?>;
            appraisalRuleCodes = <?=json_encode($seda2AppraisalRuleCodes)?>;
        }
        setRulesOptions($('#transfer-add1-code'), accessRuleCodes);
        setRulesOptions($('#transfer-add1-duration'), appraisalRuleCodes);
    }).change();

    function setRulesOptions(selectNode, options) {
        var firstOption = selectNode.find('option:first');
        var currentValue = selectNode.val();
        selectNode.empty();
        if (!firstOption.attr('value')) {
            selectNode.append(firstOption);
        }
        if (AsalaeGlobal.is_numeric(Object.keys(options)[0])) {
            // pas de groupe d'options
            $.each(options, function (key, option) {
                insertOption(selectNode, option);
            });
        } else {
            // groupes d'options
            $.each(options, function (groupName, groupOptions) {
                var optGroup = $('<optgroup>').attr('label', groupName);
                $.each(groupOptions, function (key, option) {
                    insertOption(optGroup, option)
                });
                selectNode.append(optGroup);
            });
        }
        selectNode.val(currentValue);
    }

    function insertOption(element, content) {
        newOption = $('<option>')
            .attr('value', content.value)
            .attr('title', content.title)
            .text(content.text);
        $(element).append(newOption);
    }
</script>
