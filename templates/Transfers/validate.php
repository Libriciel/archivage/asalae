<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->Form->create()
    . $this->Form->control(
        'decision',
        [
            'label' => __("Décision"),
            'options' => [
                'deny' => __("Refuser"),
                'grant' => __("Accepter"),
            ],
            'empty' => __("-- Prendre une décision --"),
            'required',
        ]
    )
    . $this->Html->tag('div#sign-validate-transfer')
    . $this->LiberSign->insertSignForm('#sign-validate-transfer', "b5ad8d43d8d5d3660b236ea0bbc8f8d48516d7a8")
    . $this->Html->tag('/div')
    . $this->Form->end();
