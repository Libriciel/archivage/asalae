<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Transferts entrants"));
$this->Breadcrumbs->add(__("Tous les transferts"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-folder-open', __("Tous les transferts")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

require 'index-common.php';
?>
<script>
    $(function() {
        AsalaeWebsocket.connect(
            '<?=Configure::read('Ratchet.connect')?>',
            function (session) {
                session.subscribe('new_transfer', function (topic, data) {
                    if (data.message.sa_id === PHP.sa_id) {
                        AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
                    }
                });
                session.subscribe('transfer_sent', function (topic, data) {
                    if (data.message.sa_id === PHP.sa_id) {
                        AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
                    }
                });
            },
            function () {
            },
            {'skipSubprotocolCheck': true}
        );
    });
</script>
