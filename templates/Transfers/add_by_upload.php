<?php

/**
 * @var Asalae\View\AppView $this
 */

use AsalaeCore\Utility\PreControlMessages;

?>
<script>
    function selectableUploadRadio(value, context) {
        if (!AsalaeGlobal.is_numeric(context.id)) {
            return '';
        }
        var input = $('<input type="radio" name="fileuploads[id]">').val(context.id);
        setTimeout(function () {
            $('#transfer-upload-xml-table')
                .find('input[name="fileuploads[id]"]')
                .prop('checked', false)
                .first()
                .prop('checked', true);
        }, 0);
        return input;
    }

    function changeIdentifierTransfer(id, tdId) {
        AsalaeLoading.ajax(
            {
                url: '<?=$this->Url->build('/transfers/generate-identifier')?>/' + id,
                method: 'POST',
                headers: {
                    Accept: 'application/json'
                },
                success: function (content, textStatus, jqXHR) {
                    if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                        var uid = $('#transfer-upload-xml-table').attr('data-table-uid');
                        var table = TableGenerator.instance[uid];
                        var oldMessage = $(table.getDataId(tdId).message);
                        var message = $('<div></div>');
                        for (var i = 0; i < oldMessage.length; i++) {
                            message.append($(oldMessage[i]));
                        }
                        message.find('.progress.uploading .progress-bar')
                            .removeClass('progress-bar-danger')
                            .removeClass('progress-bar-warning')
                            .removeClass('progress-bar-info')
                            .addClass('progress-bar-success');
                        message.find('.error-span')
                            .removeClass('text-danger')
                            .text(__("Nouvel identifiant: {0}", content.transfer_identifier));
                        message.find('.error-message').remove();
                        content.message = message;
                        table.replaceDataId(tdId, content);
                        table.generateAll();
                    } else {
                        console.error(content);
                    }
                },
                disable_error_callback: true,
                error: function (error) {
                    var uid = $('#transfer-upload-xml-table').attr('data-table-uid');
                    var table = TableGenerator.instance[uid];
                    var tdata = table.getDataId(tdId);
                    var messageTd = $($('<div></div>').append(table.getDataId(tdId).message).html());
                    messageTd.find('.error-span')
                        .text(error.responseJSON.message);
                    tdata.message = messageTd;
                    tdata.need_identifier = false;
                    tdata.need_repair = error.responseJSON.code !== 3;
                    table.generateAll();
                }
            }
        )
    }
</script>
<?php
$view = $this;

echo $this->MultiStepForm->template('MultiStepForm/add_transfer_upload')->step(1) . '<hr>';

echo $this->ModalForm
    ->create('repair-uploaded-transfer', ['size' => 'modal-xxl'])
    ->modal(__("Réparer un transfert"))
    ->output(
        'function',
        'repairTransfer',
        '/transfers/repair'
    )
    ->generate();

echo $this->Form->create($entity, ['idPrefix' => 'transfer-add-by-upload']);

echo $this->Upload
    ->create(
        'transfer-upload-xml',
        [
            'class' => 'table table-striped table-hover hide',
            'pText' => __("Glissez-déposez votre bordereau ici"),
        ]
    )
    ->fields(
        [
            'select' => [
                'label' => __("Sélectionner"),
                'class' => 'radio-td',
                'callback' => 'selectableUploadRadio',
            ],
            'filename' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
            'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
            'message' => ['label' => __("Message"), 'style' => 'min-width: 250px', 'class' => 'message'],
        ]
    )
    ->data([])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "repairTransfer('{0}')",
                'type' => 'button',
                'class' => 'btn-link repair',
                'displayEval' => 'data[{index}].need_repair',
                'label' => $view->Fa->i('fa-wrench'),
                'title' => $title = __("Réparer le transfert"),
                'aria-label' => $title,
                'params' => ['file'],
            ],
            [
                'onclick' => "changeIdentifierTransfer('{0}', '{1}')",
                'type' => 'button',
                'class' => 'btn-link change-identifier',
                'displayEval' => 'data[{index}].need_identifier',
                'label' => $view->Fa->i('fa-commenting-o'),
                'title' => $title = __("Générer un nouvel identifiant"),
                'aria-label' => $title,
                'params' => ['file', 'uid'],
            ],
            function ($table) use ($view) {
                $url = $view->Url->build('/Upload/delete');
                return [
                    'onclick' => "GenericUploader.fileDelete({$table->tableObject}, '$url', {0})",
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                    'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name'],
                ];
            },
        ]
    )
    ->generate(
        [
            'attributes' => ['accept' => '.xml, application/xml'],
            'allowDuplicateUploads' => true,
            'target' => $this->Url->build('Upload/index/seda?replace=true'),
        ]
    );

echo '<input type="radio" name="fileuploads[id]" '
    . 'style="opacity: 0; position: absolute; z-index: -1;" required="required">';
echo $this->Form->end();
?>
<script>
    $('#transfer-upload-xml-table').on('upload.failed', function (e, data) {
        var table = TableGenerator.instance[$(this).attr('data-table-uid')];
        var tdata = table.getDataId(data.file.uniqueIdentifier);
        tdata.file = data.message.report.file;
        tdata.uid = data.file.uniqueIdentifier;
        tdata.need_repair = data.message.report.code !== 3;
        if (data.message.report.code === <?=PreControlMessages::ID_NOT_UNIQUE?>) {
            tdata.need_identifier = true;
        }
        table.generateAll();
    });
</script>
