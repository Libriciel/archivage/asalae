<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 * @var int $countUploads
 * @var int $countErrors
 * @var array $errorCodes
 */

use Asalae\Model\Table\TransfersTable;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

?>
    <script>
        var downloadable = <?=$this->Acl->check($url = '/transfer-attachments/download') ? 'true' : 'false'?>;

        function filenameToLink(value, context) {
            if (downloadable) {
                var link = $('<a target="_blank"></a>')
                    .html(TableHelper.wordBreak("/")(value))
                    .attr('download', context.basename);
                link.attr('href', '<?=$this->Url->build($url)?>/' + context.id + '/' + context.url_basename);
                return link;
            } else {
                return value;
            }
        }

        function validationActionToIcon(value) {
            switch (value) {
                case 'validate':
                    return '<?=$this->Fa->i('fa-check text-success')?>';
                case 'invalidate':
                    return '<?=$this->Fa->i('fa-times text-danger')?>';
                case 'stepback':
                    return '<?=$this->Fa->i('fa-reply')?>';
                default:
                    return value;
            }
        }
    </script>
<?php
$notifications = [];
if ($entity->get('state') !== 'preparating') {
    $notifications[__("Accusé de réception")] = function (EntityInterface $entity) {
        /** @var Asalae\View\AppView $this */
        $filename = $entity->get('acknowledgement_filename');
        if ($filename && $this->Acl->check('/transfer-attachments/download')) {
            return $this->Html->link(
                $filename,
                '/transfers/message/' . $entity->id . '/acknowledgement/' . $filename,
                ['download' => $filename]
            );
        }
        return $filename ?: null;
    };
}
if ($entity->get('state') === TransfersTable::S_REJECTED) {
    $notifications[__("Notification de rejet")] = function (EntityInterface $entity) {
        /** @var Asalae\View\AppView $this */
        $filename = $entity->get('notify_filename');
        return $this->Acl->check('/transfer-attachments/download')
            ? $this->Html->link(
                $filename,
                '/transfers/message/' . $entity->id . '/reply/' . $filename,
                ['download' => $filename]
            )
            : $filename;
    };
} elseif ($entity->get('state') === TransfersTable::S_ACCEPTED) {
    $notifications[__("Notification d'acceptation")] = function (EntityInterface $entity) {
        /** @var Asalae\View\AppView $this */
        $filename = $entity->get('notify_filename');
        return $this->Acl->check('/transfer-attachments/download')
            ? $this->Html->link(
                $filename,
                '/transfers/message/' . $entity->id . '/reply/' . $filename,
                ['download' => $filename]
            )
            : $filename;
    };
}

$tables = [
    __("Informations principales") => [
        [
            __("État") => 'statetrad',
            __("Date du dernier changement d'état") => 'last_state_update',
            __("Date de création") => 'created',
            __("Version du message") => 'message_versiontrad',
            __("Est conforme") => 'is_conform',
            __("Est modifié") => 'is_modified',
            __("Créé par") => 'created_user.name',
        ],
    ],
    __("Éléments du transferts") => [
        [
            __("Date du transfert") => 'transfer_date',
            __("Service d'archives") => 'archival_agency.name',
            __("Service versant") => 'transferring_agency.name',
            __("Accord de versement") => 'agreement.name',
            __("Profils d'archives") => 'profile.name',
            __("Niveau de service") => 'service_level.name',
            __("Nombre de pièces jointes") => 'data_count',
            __("Taille des pièces jointes") => '{data_size|default(0)|toReadableSize} ({data_size} Octets)',
            __("Pièces jointes supprimées après rétention") => 'files_deleted',
        ],
    ],
    __("Accusé de réception et notification") => [$notifications],
];

$tabs = $this->Tabs->create('view-transfer', ['class' => 'row no-padding']);
$tabs->add(
    'tab-transfer-message',
    $this->Fa->i('fa-file-code-o', __("Détails")),
    $this->Html->tag('article')
    . $this->Html->tag('header.bottom-space')
    . $this->Html->tag('h3', h($entity->get('transfer_identifier')))
    . $this->Html->tag('p', h($entity->get('transfer_comment')))
    . $this->Html->tag('/header')
    . $this->ViewTable->multiple($entity, $tables)
    . $this->Html->tag('/article')
);

/** @var EntityInterface $process */
$process = Hash::get($entity, 'validation_processes.0');
if ($process) {
    $histories = $this->Html->tag('h4', __("Historique de validation"));
    /** @var EntityInterface $history */
    foreach (Hash::get($process, 'validation_histories') as $history) {
        /** @var EntityInterface $history */
        $history->setVirtual(['actor_info']);
    }
    $histories .= $this->Table
        ->create('validation-histories', ['class' => 'table table-striped table-hover smart-td-size'])
        ->fields(
            [
                'action' => [
                    'label' => __("Décision"),
                    'callback' => 'validationActionToIcon',
                    'titleEval' => 'actiontrad',
                ],
                'validation_actor.validation_stage.name' => [
                    'label' => __("Etape de validation"),
                ],
                'actor_info' => [
                    'label' => __("Auteur de la validation"),
                    'escape' => false,
                ],
                'created' => [
                    'label' => __("Date"),
                    'type' => 'datetime',
                ],
                'comment' => [
                    'label' => __("Commentaire"),
                ],
            ]
        )
        ->data(Hash::get($process, 'validation_histories'))
        ->params(
            [
                'identifier' => 'id',
            ]
        );

    $info = [
        __("Etat de la validation") => 'processedtrad',
        __("Décision finale") => 'validatedtrad',
        __("Circuit de validation") => 'validation_chain.name',
    ];
    if (!$process->get('processed')) {
        $info[__("Etape de validation")] = 'steptrad';
        $info[__("Doit être validé par")] = 'current_stage.validation_actors.{n}.actor_info|raw';
    }

    $tabs->add(
        'tab-transfer-validation',
        $this->Fa->i('fa-check-square-o', __("Validation")),
        // ---------------------------------------------------------------------
        $this->Html->tag('h3', __("Validation de {0}", h($entity->get('transfer_identifier'))))
        . $this->ViewTable->generate($process, $info)
        . $histories
    );
}

if ($entity->get('files_deleted')) {
    $searchLink = $this->Acl->check('/archives/view')
        ? ' ' . __(
            "Vous pouvez les retrouver dans le registre des entrées {0}.",
            $this->Html->link(
                __("en suivant ce lien"),
                [
                    'controller' => 'archives',
                    'action' => 'index',
                    '?' => [
                        'transfer_id' => $entity->get('id'),
                    ],
                ]
            )
        )
        : '';
    $transferAttachmentTabContent = $this->Html->tag(
        'p',
        __("Les fichiers d'un transfert accepté ne sont plus disponibles.") . $searchLink,
        ['class' => 'alert alert-warning']
    );
} else {
    $tableAttachments = $this->Table
        ->create('view-transfer-attachments', ['class' => 'table table-striped table-hover'])
        ->url(
            $url = [
                'controller' => 'transfers',
                'action' => 'view-attachments',
                $id,
                '?' => [
                    'sort' => 'filename',
                    'direction' => 'asc',
                ],
            ]
        )
        ->fields(
            [
                'filename' => [
                    'label' => __("Nom de fichier"),
                    'callback' => 'filenameToLink',
                    'order' => 'filename',
                    'filter' => [
                        'filename[0]' => [
                            'id' => 'filter-transfer-attachment-filename-0',
                            'label' => false,
                            'aria-label' => __("Nom de fichier"),
                        ],
                    ],
                ],
                'size' => [
                    'label' => __("Taille"),
                    'callback' => 'TableHelper.readableBytes',
                    'order' => 'size',
                    'filter' => [
                        'size[0][value]' => [
                            'id' => 'filter-size-0',
                            'label' => false,
                            'aria-label' => __("Taille 1"),
                            'prepend' => $this->Input->operator('size[0][operator]', '>='),
                            'append' => $this->Input->mult('size[0][mult]'),
                            'class' => 'with-select',
                            'type' => 'number',
                            'min' => 1,
                        ],
                        'size[1][value]' => [
                            'id' => 'filter-size-1',
                            'label' => false,
                            'aria-label' => __("Taille 2"),
                            'prepend' => $this->Input->operator('size[1][operator]', '<='),
                            'append' => $this->Input->mult('size[1][mult]'),
                            'class' => 'with-select',
                            'type' => 'number',
                            'min' => 1,
                        ],
                    ],
                ],
                'meta' => [
                    'label' => __("Meta-donnés"),
                    'target' => '',
                    'thead' => [
                        'hash' => [
                            'label' => __("Empreinte du fichier"),
                            'callback' => 'TableHelper.wordBreak("_", 32)',
                        ],
                        'hash_algo' => ['label' => __("Algorithme de hashage")],
                        'mime' => ['label' => __("Type MIME")],
                        'format' => ['label' => __("Format")],
                        'virus_name' => ['label' => __("VIRUS")],
                    ],
                ],
            ]
        )
        ->data(Hash::get($entity, 'transfer_attachments', []))
        ->params(
            [
                'identifier' => 'id',
            ]
        )
        ->actions(
            [
                [
                    'href' => "/transfer-attachments/download/{0}/{3}",
                    'download' => '{2}',
                    'target' => '_blank',
                    'label' => $this->Fa->charte('Télécharger'),
                    'title' => $title = __("Télécharger {0}", '{1}'),
                    'aria-label' => $title,
                    'display' => $this->Acl->check('/transfer-attachments/download'),
                    'params' => ['id', 'filename', 'basename', 'url_basename'],
                ],
                [
                    'href' => '/transfer-attachments/open/{0}/{3}',
                    'target' => '_blank',
                    'label' => $this->Fa->i('fas fa-external-link-alt'),
                    'title' => $title = __("Ouvrir dans le navigateur {0}", '{1}'),
                    'aria-label' => $title,
                    'display' => $this->Acl->check('/transfer-attachments/open'),
                    'displayEval' => 'data[{index}].openable',
                    'params' => ['id', 'filename', 'basename', 'url_basename'],
                ],
            ]
        );
    $paginator = $this->AjaxPaginator->create('pagination-transfer-view-attachments')
        ->url($url)
        ->table($tableAttachments)
        ->count($countUploads);

    $transferAttachmentTabContent = $paginator->generateTable();
}

$tabs->add(
    'tab-transfer-attachments',
    $this->Fa->i('fa-paperclip', __("Fichiers des pièces jointes")),
    $transferAttachmentTabContent
);

$errors = Hash::get($entity, 'transfer_errors');
if (!empty($errors)) {
    $tableErrors = $tableAttachments = $this->Table
        ->create('view-transfer-errors', ['class' => 'table table-striped table-hover'])
        ->fields(
            [
                'level' => [
                    'label' => __("Niveau"),
                    'filter' => [
                        'filter-error-level[0]' => [
                            'id' => 'filter-transfer-errors-level-0',
                            'name' => 'level',
                            'label' => __("Niveau d'erreur"),
                            'options' => ['error' => 'error', 'warning' => 'warning'],
                        ],
                    ],
                ],
                'code' => [
                    'label' => __("Code"),
                    'filter' => [
                        'filter-error-code[0]' => [
                            'id' => 'filter-transfer-errors-code-0',
                            'name' => 'code',
                            'label' => __("Code d'erreur"),
                            'options' => $errorCodes,
                        ],
                    ],
                ],
                'message' => ['label' => __("Message")],
            ]
        )
        ->data($errors)
        ->params(
            [
                'identifier' => 'id',
            ]
        );
    $paginator = $this->AjaxPaginator->create('pagination-transfer-view-errors')
        ->url(
            [
                'controller' => 'transfers',
                'action' => 'view-errors',
                $id,
            ]
        )
        ->table($tableErrors)
        ->count($countErrors);
    $tabs->add(
        'tab-transfer-errors',
        $this->Fa->i('fa-warning', __("Erreurs")),
        $paginator->generateTable()
    );
}

echo $tabs;
