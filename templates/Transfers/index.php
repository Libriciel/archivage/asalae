<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-exchange', __("Transferts")))
);

$buttons = [];
if ($this->Acl->check('/transfers/add1')) {
    $buttons[] = $this->Html->link(
        $this->Fa->charte('Ajouter', __("Créer un transfert")),
        '/transfers/index-preparating?add=form',
        ['class' => 'btn btn-success', 'escape' => false]
    );
    $buttons[] = $this->Html->link(
        $this->Fa->charte('Ajouter', __("Créer un transfert par upload de bordereau")),
        '/transfers/index-preparating?add=upload',
        ['class' => 'btn btn-success', 'escape' => false]
    );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->Html->tag('ul.container.dashboard');

$links = [
    [
        'url' => '/transfers/indexPreparating',
        'count' => $this->Html->tag('span#transfers-prep-count', $prepCount),
        'label' => $this->Fa->i('fa-code', __("Mes transferts en préparation")),
    ],
    [
        'url' => '/transfers/indexSent',
        'count' => $this->Html->tag('span#transfers-sent-count', $sentCount),
        'label' => $this->Fa->i('fa-list', __("Mes transferts envoyés")),
    ],
    [
        'url' => '/transfers/indexAccepted',
        'count' => $this->Html->tag('span#transfers-sent-count', $acceptedCount),
        'label' => $this->Fa->i('fa-check', __("Mes transferts acceptés")),
    ],
    [
        'url' => '/transfers/indexRejected',
        'count' => $this->Html->tag('span#transfers-sent-count', $rejectedCount),
        'label' => $this->Fa->i('fa-thumbs-down', __("Mes transferts rejetés")),
    ],
    [
        'url' => '/transfers/indexMy',
        'count' => $this->Html->tag('span#transfers-my-count', $myCount),
        'label' => $this->Fa->i('fa-folder-open', __("Tous mes transferts")),
    ],
];
foreach ($links as $link) {
    if ($link['url'] === '#' || $this->Acl->check($link['url'])) {
        echo $this->Html->tag(
            'li',
            $this->Html->link(
                $this->Html->tag('span.message', $link['label'])
                . $this->Html->tag('span.count', $link['count']),
                $link['url'],
                ['escape' => false, 'class' => 'navbar-link']
            )
        );
    }
}

echo $this->Html->tag('/ul');
?>
<script>
    $(function () {
        AsalaeWebsocket.connect(
            '<?=Configure::read('Ratchet.connect')?>',
            function (session) {
                session.subscribe('new_transfer', function (topic, data) {
                    if (data.message.user_id === PHP.user_id) {
                        var span = $('#transfers-prep-count');
                        span.text(parseInt(span.text(), 10) + data.message.count);
                        span = $('#transfers-my-count')
                        span.text(parseInt(span.text(), 10) + data.message.count);
                    }
                });
                session.subscribe('transfer_sent', function (topic, data) {
                    if (data.message.user_id === PHP.user_id) {
                        var span = $('#transfers-sent-count');
                        span.text(parseInt(span.text(), 10) + data.message.count);
                        span = $('#transfers-my-count')
                        span.text(parseInt(span.text(), 10) + data.message.count);
                    }
                });
                session.subscribe('transfer_accepted', function (topic, data) {
                    if (data.message.user_id === PHP.user_id) {
                        var span = $('#transfers-accepted-count');
                        span.text(parseInt(span.text(), 10) + data.message.count);
                        span = $('#transfers-my-count')
                        span.text(parseInt(span.text(), 10) + data.message.count);
                    }
                });
                session.subscribe('transfer_rejected', function (topic, data) {
                    if (data.message.user_id === PHP.user_id) {
                        var span = $('#transfers-rejected-count');
                        span.text(parseInt(span.text(), 10) + data.message.count);
                        span = $('#transfers-my-count')
                        span.text(parseInt(span.text(), 10) + data.message.count);
                    }
                });
            },
            function () {
            },
            {'skipSubprotocolCheck': true}
        );
    });
</script>
