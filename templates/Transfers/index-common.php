<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

$view = $this;
$loading = $this->Html->tag(
    'div',
    $this->Fa->i('fa-4x fa-spinner faa-spin animated'),
    ['class' => 'text-center loading-container']
);

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalForm
    ->create('edit-transfer', ['size' => 'modal-xxl'])
    ->modal(__("Edition d'un transfert"))
    ->output('function', 'actionEditTransfer', '/Transfers/edit')
    ->generate();

echo $this->ModalView->create('explore-transfer-common', ['size' => 'modal-xxl'])
    ->modal(__("Afficher le bordereau"))
    ->output('function', 'exploreTransfer', '/Transfers/explore')
    ->generate();

echo $this->ModalView->create('view-transfer-common', ['size' => 'large'])
    ->modal(__("Visualisation d'un transfert"))
    ->output('function', 'viewTransfer', '/Transfers/view')
    ->generate();

echo $this->ModalView->create('analyse-transfer-common', ['size' => 'large'])
    ->modal(
        __("Analyse d'un transfert"),
        $this->Html->tag(
            'div.alert.alert-info',
            __("Analyse en cours... Veuillez patienter, cette action peut durer plusieurs minutes.")
        )
        . $loading
        . $this->Html->tag('p#feedback-analyse', '')
    )
    ->output('function', 'analyseTransfer', '/Transfers/analyse')
    ->javascriptCallback('unsubscribeAnalyse')
    ->generate();

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}

echo $this->Filter->create('transfer-filter')
    ->saves($savedFilters)
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => 'jj/mm/aaaa',
            'type' => 'date',
        ]
    )
    ->filter(
        'transfer_date',
        [
            'label' => __("Date du transfert"),
            'placeholder' => 'jj/mm/aaaa',
            'type' => 'date',
        ]
    )
    ->filter(
        'transfer_identifier',
        [
            'label' => __("Identifiant"),
            'placeholder' => 'ident?fi*',
            'wildcard',
        ]
    )
    ->filter(
        'transferring_agency_id',
        [
            'label' => __("Service versant"),
            'options' => $transferring_agencies,
            'empty' => __("-- Sélectionner un service versant --"),
        ]
    )
    ->filter(
        'originating_agency_id',
        [
            'label' => __("Service producteur"),
            'options' => $originating_agencies,
            'empty' => __("-- Sélectionner un service producteur --"),
        ]
    )
    ->filter(
        'state',
        [
            'label' => __("Etat"),
            'type' => 'select',
            'multiple' => true,
            'options' => $states,
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->filter(
        'created_user_id',
        [
            'label' => __("Créé par"),
            'type' => 'select',
            'options' => $createdUsers,
        ]
    )
    ->filter(
        'files_deleted',
        [
            'label' => __("Fichiers supprimés"),
            'options' => [
                '1' => __("Oui"),
                '0' => __("Non"),
            ],
        ]
    )
    ->generateSection();
?>

    <script>
        $(function () {
            AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
        });

        /**
         * Charge la modal de visualisation d'un SEDA
         * @param id
         */
        function loadDisplayXmlModal(id) {
            var body = $('#<?=$modalXmlToHtml?>').find('.modal-body');
            body.html($('<iframe></iframe>')
                .attr('height', '600')
                .attr('width', '100%')
                .attr('src', '<?=$this->Url->build('/Transfers/display-xml/')?>' + id));
        }

        function actionSendTransfer(table, url, id) {
            AsalaeLoading.ajax({
                url: url.replace(/\/$/, '') + '/' + id,
                method: 'POST',
                success: function (message) {
                    if (message.report === 'done') {
                        table.removeDataId(id);
                        $(table.table)
                            .find('tr[data-id="' + id + '"]')
                            .fadeOut(400, function () {
                                $(this).remove();
                                table.decrementPaginator();
                            });
                    } else {
                        throw message.report;
                    }
                },
                error: function () {
                    $(table.table).find('tr[data-id="' + id + '"]').addClass('danger');
                }
            });
        }

        function updatePaginationAjaxCallback() {
            var table = $('#<?=$tableId?>');
            $('#btn-grouped-actions').off().click(function () {
                var visibleCheckedCheckboxes = table.find('.td-checkbox > input:checked');
                $(this).disable();
                switch ($('#select-grouped-actions').val()) {
                    case 'delete':
                        if (confirm(__("Êtes-vous sûr de vouloir supprimer les transferts sélectionnés ?"))) {
                            visibleCheckedCheckboxes.each(function () {
                                eval($(this).closest('tr').find('button.delete:enabled').attr('data-callback'));
                            });
                        }
                        break;
                    case 'submit':
                        if (confirm(__("Êtes-vous sûr de vouloir envoyer les transferts sélectionnés ?"))) {
                            visibleCheckedCheckboxes.each(function () {
                                eval($(this).closest('tr').find('button.send:enabled').attr('data-callback'));
                            });
                        }
                        break;
                }
                $(this).enable();
                table.find('> thead th input.checkall').prop('checked', false);
            });
        }

        function duplicateTransfer(id) {
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build('/transfers/duplicate')?>/' + id,
                method: 'POST',
                headers: {
                    Accept: 'application/json'
                },
                success: function (content, textStatus, jqXHR) {
                    if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                        var msg = `<?=
                        __(
                            "Copie réussie : transfert ''{0}'' créé, il est accessible "
                            . "dans le menu 'Mes transferts en préparation'",
                            '${content.transfer_identifier}'
                        )?>`;
                        alert(msg);
                    } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'false') {
                        alert(AsalaeGlobal.entityErrors(content));
                    } else {
                        alert(__("Une erreur inattendue a eu lieu"));
                    }
                }
            })
        }

        function unsubscribeAnalyse() {
            $('#analyse-transfer-common').off('.killable');
            if (typeof prevsession === 'object') {
                prevsession.unsubscribe('analyse_' + previd + '_' + PHP.user_id + '_' + PHP.token);
            }
            previd = null;
        }

        if (typeof prevsession === 'object' && typeof previd === 'string') {
            unsubscribeAnalyse();
        } else {
            var previd;
            var prevsession;
        }

        function wrapAnalyseTransfer(id) {
            AsalaeWebsocket.connect(
                '<?=Configure::read('Ratchet.connect')?>',
                function (session) {
                    session.subscribe('analyse_' + id + '_' + PHP.user_id + '_' + PHP.token, function (topic, data) {
                        $('#feedback-analyse').text(data.message);
                    });
                    previd = '' + id;
                    prevsession = session;
                },
                function () {
                },
                {'skipSubprotocolCheck': true}
            );
            $('#analyse-transfer-common').one('hide.bs.modal.killable', function () {
                unsubscribeAnalyse();
                AsalaeLoading.ajax({url: '<?=$this->Url->build('/Transfers/abortAnalyse')?>'});
            });
            return analyseTransfer(id);
        }

        function wrapEditTransfer(id) {
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build('/transfers/get-edit-meta')?>/' + id,
                headers: {
                    Accept: 'application/json'
                },
                success: function (content) {
                    if (!content || !content.isDirty) {
                        actionEditTransfer(id);
                        return;
                    }
                    if (content.session === PHP.token) {
                        actionEditTransfer(id);
                        alert(
                            __(
                                "Attention, vous reprenez l'édition de ce transfert qui avait commencé le {0}",
                                moment(content.created).format('LLLL')
                            )
                        );
                    } else if (confirm(
                        __(
                            "L'édition de ce transfert ne s'est pas correctement terminée. "
                            + "Voulez-vous reprendre l'édition? Attention si vous cliquez sur Annuler,"
                            + " les éventuelles modifications seront perdues."
                        )
                    )
                    ) {
                        actionEditTransfer(id);
                    } else {
                        actionEditTransfer(id + '?renew=true');
                    }
                }
            });
        }

        function wrapSendTransfer(table, url, id) {
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build('/transfers/get-edit-meta')?>/' + id + '?validate=true',
                headers: {
                    Accept: 'application/json'
                },
                success: function (content) {
                    if (content.validate) {
                        actionSendTransfer(table, url, id);
                    } else if (content.isDirty) {
                        alert(
                            __(
                                "L'édition de ce transfert ne s'est pas correctement "
                                + "terminée ou est encore en cours. Merci d'éditer ce "
                                + "transfert avant de pouvoir l'envoyer."
                            )
                        );
                    } else {
                        alert(
                            __(
                                "Des erreurs on été détectées et bloquent l'envoi. "
                                + "Merci de corriger ces erreurs en éditant le transferts."
                            )
                        );
                    }
                }
            });
        }

        function transferFileDownload(id) {
            if (event.ctrlKey) {
                id += '?renew=true';
            }
            // ajax :
            // soit DL
            // soit message pour dire que mail ou erreur
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build('/Transfers/make-zip')?>/' + id,
                headers: {
                    Accept: 'application/zip'
                },
                success: function (content, textStatus, jqXHR) {
                    switch (jqXHR.getResponseHeader('X-Asalae-ZipFileDownload')) {
                        case 'email':
                        case 'fail':
                            alert(content);
                            break;
                        case 'download':
                            var disposition = jqXHR.getResponseHeader('content-disposition');
                            var matches = /"([^"]*)"/.exec(disposition);
                            var filename = (matches != null && matches[1] ? matches[1] : 'transfer.zip');

                            var link = document.createElement('a');
                            document.body.appendChild(link);

                            link.href = '<?=$this->Url->build('/Transfers/download-files/')?>' + id;
                            link.download = filename;
                            link.dataset.mode = 'no-ajax';
                            link.click();
                            document.body.removeChild(link);
                            break;
                    }
                }
            });
        }

        function transferPdfDownload(id) {
            if (event.ctrlKey) {
                id += '?renew=true';
            }
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build('/Transfers/make-pdf')?>/' + id,
                headers: {
                    Accept: 'application/zip'
                },
                success: function (content, textStatus, jqXHR) {
                    switch (jqXHR.getResponseHeader('X-Asalae-PdfFileDownload')) {
                        case 'email':
                        case 'fail':
                            alert(content);
                            break;
                        case 'download':
                            var disposition = jqXHR.getResponseHeader('content-disposition');
                            var matches = /"([^"]*)"/.exec(disposition);
                            var filename = (matches != null && matches[1] ? matches[1] : 'tranfer.pdf');

                            var link = document.createElement('a');
                            document.body.appendChild(link);

                            link.href = '<?=$this->Url->build('/Transfers/download-pdf/')?>' + id;
                            link.download = filename;
                            link.dataset.mode = 'no-ajax';
                            link.click();
                            document.body.removeChild(link);
                            break;
                    }
                }
            });
        }

        function reEditTransfer(id) {
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build('/Transfers/re-edit')?>/' + id,
                headers: {
                    Accept: 'application/json'
                },
                success: function (content, textStatus, jqXHR) {
                    AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
                }
            });
        }
    </script>
<?php
require 'ajax_index.php';
