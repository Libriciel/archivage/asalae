<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Transferts"));
$this->Breadcrumbs->add(__("Mes transferts en préparation"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-code', __("Mes transferts en préparation")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$buttons = [];
if ($this->Acl->check('/transfers/add1')) {
    $buttons[] = $this->Form->button(
        $this->Fa->charte('Ajouter', __("Créer un transfert")),
        [
            'type' => 'button',
            'onclick' => 'addTransferStep1()',
            'class' => 'btn btn-success',
            'secure' => 'skip',
        ]
    );
    $buttons[] = $this->Form->button(
        $this->Fa->charte('Ajouter', __("Créer un transfert par upload de bordereau")),
        [
            'type' => 'button',
            'onclick' => 'uploadTransferStep1()',
            'class' => 'btn btn-success',
        ]
    );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->ModalForm->create(
    'add-transfer',
    [
        'size' => 'modal-xxl',
        'acceptButton' => $this->Form->button(
            $this->Fa->charte('Enregistrer', __("Passer à l'étape suivante")),
            ['bootstrap-type' => 'primary', 'class' => 'accept']
        ),
    ]
)
    ->modal(__("Ajout d'un transfert"))
    ->step('/transfers/add1', 'addTransferStep1')
    ->step('/transfers/add2', 'addTransferStep2')
    ->javascriptCallback('AsalaeModal.stepCallback')
    ->output('function')
    ->generate();

echo $this->ModalForm->create(
    'add-transfer-upload',
    [
        'size' => 'modal-xxl',
        'acceptButton' => $this->Form->button(
            $this->Fa->charte('Enregistrer', __("Passer à l'étape suivante")),
            ['bootstrap-type' => 'primary', 'class' => 'accept']
        ),
    ]
)
    ->modal(__("Ajout d'un transfert par upload de bordereau"))
    ->step('/transfers/addByUpload', 'uploadTransferStep1')
    ->step('/transfers/addByUpload2', 'addTransferUploadStep2')
    ->javascriptCallback('AsalaeModal.stepCallback')
    ->output('function')
    ->generate();

require 'index-common.php';
?>
<script>
    $(function() {
        AsalaeWebsocket.connect(
            '<?=Configure::read('Ratchet.connect')?>',
            function (session) {
                session.subscribe('new_transfer', function (topic, data) {
                    if (data.message.user_id === PHP.user_id) {
                        AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
                    }
                });
            },
            function () {
            },
            {'skipSubprotocolCheck': true}
        );
    });
    $('#add-transfer-1, #add-transfer-upload-1').on('hidden.bs.modal', function () {
        var url = location.href;
        var match = url.match(/([&?])(add=[^&]+)(&|$)/);
        if (match) {
            var newUrl;
            if (match[3] === "") {
                newUrl = url.replace(match[0], '');
            } else {
                newUrl = url.replace(match[2] + match[3], '');
            }
            window.history.pushState({}, '', newUrl);
        }
    });
    $('#add-transfer-2, #add-transfer-upload-2').on('hidden.bs.modal', function () {
        AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
    });
    $(function () {
        setTimeout(function () {
            var url = location.href;
            var match = url.match(/[&?]add=([^&]+)(?:&|$)/);
            if (match && match[1] === 'form') {
                addTransferStep1();
            } else if (match && match[1] === 'upload') {
                uploadTransferStep1();
            }
        }, 400);
    });
</script>
