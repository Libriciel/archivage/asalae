<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

$uploadDomId = $this->fetch('uploadDomId');
$paginatorId = $this->fetch('paginatorId');
$buttonAddZip = $this->fetch('buttonAddZip');
$buttonAddManual = $this->fetch('buttonAddManual');
$idPrefix = $this->fetch('idPrefix');
$btnGroupId = $this->fetch('btnGroupId');
$addTransferZipDiv = $this->fetch('addTransferZipDiv');
$addTransferManualDiv = $this->fetch('addTransferManualDiv');
$urlZip = $this->fetch('urlZip');

$view = $this;
$uploads = $this->Upload
    ->create(
        $uploadDomId . '-manual',
        ['class' => 'table table-striped table-hover' . ($countAttachments ? '' : ' hide')]
    )
    ->fields(
        [
            'info' => [
                'label' => __("Fichier"),
                'target' => '',
                'thead' => [
                    'filename' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
                    'mime' => ['label' => __("Type MIME")],
                    'hash' => ['label' => __("Empreinte du fichier")],
                    'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
                ],
                'filter' => [
                    'filter-filename[0]' => [
                        'id' => 'filter-transfer-attachment-filename-0',
                        'name' => 'filename',
                        'label' => __("Nom de fichier"),
                    ],
                ],
            ],
            'message' => ['label' => __("Message"), 'style' => 'min-width: 250px', 'class' => 'message'],
        ]
    )
    ->data($entity->get('transfer_attachments') ?: [])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
            'checkbox' => true,
        ]
    )
    ->actions(
        $actions = [
            function ($upload) use ($view) {
                return [
                    'data-callback' => "uncompressFile({$upload->tableObject}, {0})",
                    'type' => "button",
                    'class' => "btn-link",
                    'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id) '
                        . '&& /\.(zip|tar(\.gz)?|t?gz|rar)$/i.test(data[{index}].filename)',
                    'label' => $view->Fa->i('fa-file-archive-o'),
                    'title' => __("Décompresser {0}", '{1}'),
                    'aria-label' => __("Décompresser {0}", '{1}'),
                    'confirm' => __(
                        "Décompresser un fichier peut prendre plusieurs minutes "
                        . "(voir plusieurs heures pour les plus gros).\nPendant la décompression, "
                        . "vous pouvez fermer la fenêtre (clic sur annuler), "
                        . "l'action continuera en arrière-plan. Une notification vous sera envoyée"
                        . " lorsque cette action sera terminée si celle-ci met plus de 5 minutes "
                        . "à s'exécuter.\n\nVoulez-vous continuer ?"
                    ),
                    'params' => ['id', 'filename'],
                ];
            },
            [
                'href' => "/TransferAttachments/download/{0}",
                'download' => '{1}',
                'target' => '_blank',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => __("Télécharger {0}", '{1}'),
                'aria-label' => __("Télécharger {0}", '{1}'),
                'params' => ['id', 'filename'],
            ],
            function ($table) use ($view) {
                $deleteUrl = $view->Url->build('/TransferAttachments/delete');
                return [
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'displayEval' => "data[{index}].deletable",
                    'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'data-callback'
                    => "wrapDelete({$table->tableObject}, '$deleteUrl', {0})",
                    'confirm' => __(
                        "Cette action ne pourra pas être annulée. "
                        . "Êtes-vous sûr de vouloir supprimer ce fichier ?"
                    ),
                    'params' => ['id', 'filename'],
                ];
            },
        ]
    );

$paginator = $this->AjaxPaginator->create($paginatorId . '-manual')
    ->url(
        [
            'controller' => 'transfers',
            'action' => 'view-attachments',
            $id,
            '?' => ['count' => true],
        ]
    )
    ->table($uploads->table)
    ->count($countAttachments);

$uploadsZip = $this->Upload
    ->create(
        $uploadDomId . '-zip',
        [
            'class' => 'table table-striped table-hover hide',
            'pText' => __("Glissez-déposer votre fichier zip (ou tar.gz)"),
        ]
    )
    ->fields(
        [
            'info' => [
                'label' => __("Fichier"),
                'target' => '',
                'thead' => [
                    'filename' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
                    'mime' => ['label' => __("Type MIME")],
                    'hash' => ['label' => __("Empreinte du fichier")],
                    'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
                ],
                'filter' => [
                    'filter-filename[0]' => [
                        'id' => 'filter-transfer-attachment-filename-0',
                        'name' => 'filename',
                        'label' => __("Nom de fichier"),
                    ],
                ],
            ],
            'message' => ['label' => __("Message"), 'style' => 'width: 400px', 'class' => 'message'],
        ]
    )
    ->data($entity->get('transfer_attachments') ?: [])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
            'checkbox' => true,
        ]
    )
    ->actions($actions);

$paginatorZip = $this->AjaxPaginator->create($paginatorId . '-zip')
    ->url(
        [
            'controller' => 'transfers',
            'action' => 'view-attachments',
            $id,
            '?' => ['count' => true],
        ]
    )
    ->table($uploadsZip->table)
    ->count($countAttachments);


echo $this->Form->create($entity, ['idPrefix' => $idPrefix, 'id' => $idPrefix . '-form']);

echo $this->Form->control('id', ['type' => 'hidden']);
echo $this->Form->control('generate_tree', ['type' => 'hidden']);

echo $this->Html->tag(
    'div',
    $paginator->scrollBottom(['style' => 'position: absolute; right:0; z-index:100']),
    ['style' => 'position: relative']
);

echo $this->fetch('title-buttons');

/**
 * div Ajout des fichiers
 */
$buttonBack = $this->Html->tag(
    'button',
    $this->Fa->i('fa-arrow-left', __("Retour aux choix")),
    ['type' => 'button', 'onclick' => 'chooseUploadType()', 'class' => 'choose-upload-type']
);

$manualDivClasses = '';
if ($this->getRequest()->getQuery('type') !== 'manual' || $countAttachments === 0) {
    $manualDivClasses = '.hidden';
}
$zipDivClasses = '';
if ($this->getRequest()->getQuery('type') !== 'zip' || $countAttachments === 0) {
    $zipDivClasses = '.hidden';
}

echo $this->Html->tag("div#$addTransferManualDiv{$manualDivClasses}");

echo $this->Html->tag(
    'div.form-group.fake-input',
    $this->Html->tag('span.fake-label', __("Téléversement de fichiers"))
    . $uploads->generate(
        [
            'singleFile' => false,
            'allowDuplicateUploads' => true,
            'target' => $this->Url->build('/upload/add-transfer-attachment/' . $entity->get('id')),
        ]
    )
    . $buttonBack
);
?>
    <script>
        $('#btn-<?=$paginatorId?>-manual').click(function () {
            executeGroupActionAddTransferAttachments('#select-<?=$paginatorId?>-manual', 'manual');
        });

        $('#btn-<?=$paginatorId?>-zip').click(function () {
            executeGroupActionAddTransferAttachments('#select-<?=$paginatorId?>-zip', 'zip');
        });

        /**
         * @param {TableGenerator} uploadTable
         * @param {string|int} id
         * @return {Function}
         */
        function uncompressFile(uploadTable, id) {
            var tr = uploadTable.table.find('tr[data-id=' + id + ']');
            tr.find('td.action a, td.action button').disable();
            var msgId = 'transfer-upload-' + id + '-' + Math.floor(Math.random() * (16777215 - 1048576 + 1) + 1048576);
            var msg = $('<span></span>').attr('id', msgId);
            var td = tr.addClass('warning')
                .find('td.message')
                .append(
                    $('<div class="text-center"></div>').append(
                        '<i aria-hidden="true" class="fa fa-2x fa-spinner faa-spin animated"></i>'
                    )
                )
                .append(msg);
            var first = true;
            var tableData = uploadTable.getDataId(id);
            AsalaeWebsocket.connect(
                '<?=Configure::read('Ratchet.connect')?>',
                function (session) {
                    session.subscribe('addcompressed_' + id, function (topic, data) {
                        var nmsg = $('#' + msgId);
                        if (nmsg.get(0) !== msg.get(0)) {
                            msg = nmsg;
                            td = nmsg.closest('td');
                        }
                        msg.text(data.message);
                        tableData.message = td.html();
                        if (first) {
                            first = false;
                            AsalaeLoading.stop();
                        }
                    });
                },
                function () {
                },
                {'skipSubprotocolCheck': true}
            );
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build("/transfers/add-compressed")?>/' + id,
                method: 'POST',
                headers: {
                    Accept: 'application/json'
                },
                timeout: 0,
                success: function (content, textStatus, jqXHR) {
                    if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                        uploadTable.removeDataId(id);
                        tr.fadeOut(400, function () {
                            $(this.remove());
                            $(uploadTable.table).trigger('change');
                        });
                        for (var i = 0; i < Math.min(content.length, 100); i++) {
                            uploadTable.data.push(content[i]);
                        }
                        TableGenerator.appendActions(uploadTable.data, uploadTable.actions);
                        uploadTable.generateAll();

                        var instanceIndex = $('#<?=$paginatorId?>-zip').attr('data-paginator');
                        var paginator = AsalaePaginator.instances[instanceIndex];
                        paginator.applyXHR(jqXHR);
                        $(uploadTable.table).trigger('added-zip');
                    } else {
                        this.error();
                    }
                },
                error: function (error, message) {
                    console.error(error);
                    tr.find('td.action a, td.action button').enable();
                    tr.find('td.message').text(message);
                    tr.addClass('danger').removeClass('warning');
                }
            });
        }

        function executeGroupActionAddTransferAttachments(select, suffix) {
            var checked = $('#<?=$uploadDomId?>-' + suffix + '-table').find('tbody .td-checkbox input:checked');
            if ($(select).val() === 'delete') {
                if (checked.length) {
                    checked = checked.filter(function () {
                        return $(this).closest('tr').find('td.action button.delete:enabled').length > 0;
                    });
                }
                if (checked.length === 0 || !confirm(__("Êtes-vous sûr de vouloir supprimer ces enregistrements ?"))) {
                    return;
                }
                checked.each(function () {
                    var tr = $(this).closest('tr');
                    eval(tr.find('td.action button.delete').attr('data-callback'));
                });
            }
        }

        $('#<?=$uploadDomId?>-manual, #<?=$uploadDomId?>-zip')
            .filter('.dropbox')
            .off('complete.uploader.paginator')
            .on('complete.uploader.paginator', function (data) {
                var instanceIndex = $('#<?=$paginatorId?>-manual').attr('data-paginator');
                var paginator = AsalaePaginator.instances[instanceIndex];
                paginator.params.count = paginator.table.data.length;
                if (paginator.params.count > paginator.params.perPage) {
                    paginator.ajax(1, true);
                } else {
                    paginator.rebuildPagination();
                }
            });

        var modalBody = $('#<?=$idPrefix?>-form').closest('.modal-body');
        var suppressed = false;
        AsalaeWebsocket.connect(
            '<?=Configure::read('Ratchet.connect')?>',
            function (session) {
                session.subscribe('hashcheck_<?=$id?>', function (topic, data) {
                    modalBody.html(data.message);
                    // retire la transparence de la modale pendant le chargement
                    if (!suppressed) {
                        modalBody.closest('.modal').stop(true, true).fadeTo(400, 1);
                        suppressed = true;
                    }
                });
            },
            function () {
            },
            {'skipSubprotocolCheck': true}
        );
    </script>
<?php

echo '<hr>';
echo $paginator->generate(
    [
        'actions' => [
            '0' => __("-- Action groupée --"),
            'delete' => __("Supprimer"),
        ],
    ]
);
echo $this->Html->tag('/div');
/**
 * div Ajout d'une arbo
 */
echo $this->Html->tag("div#$addTransferZipDiv{$zipDivClasses}");

echo $this->Html->tag(
    'div.form-group.fake-input',
    $this->Html->tag('span.fake-label', __("Téléversement de fichiers"))
    . $uploadsZip->generate(
        [
            'attributes' => ['accept' => '.zip, application/zip, .tar.gz, application/gzip, .tar; application/tar'],
            'allowDuplicateUploads' => true,
            'singleFile' => true,
            'target' => $this->Url->build(
                $urlZip ?: sprintf('/upload/add-transfer-attachment/%d/uncompress', $entity->get('id'))
            ),
        ]
    )
    . $buttonBack
);
?>
    <script>
        var uploadTable = <?=$uploadsZip->table->tableObject?>;
        var currentUploads = {};
        uploadTable.table.off('upload.failed').on('upload.failed', function (event, data) {
            var td = uploadTable.table.find('[data-id="%s"] td.message'.format(data.file.uniqueIdentifier));
            td.parent().find('button.cancel, button.retry').enable();
            if (currentUploads[data.file.uniqueIdentifier].saved) {
                var tableData = uploadTable.getDataId(data.file.uniqueIdentifier);
                var message = $('<div>').append(tableData.message);
                message.find('.faa-spin').parent().remove();
                message.find('.websocket-message').remove();
                tableData.id = currentUploads[data.file.uniqueIdentifier].id;
                tableData.message = message;
                tableData.deletable = true;
                td.empty().append(tableData.message);
                td.find('.faa-spin').parent().remove();
                td.find('.websocket-message').remove();
                uploadTable.generateAll();
                if (currentUploads[data.file.uniqueIdentifier].state === 'generate_tree') {
                    setTimeout(
                        function () {
                            var firstAction = uploadTable.table.find('.action .td-group-sub-sub').first();
                            var newRetryDiv = firstAction.clone();
                            var newRetryButton = $('<button class="btn-link refresh" type="button">');
                            newRetryButton.attr('title', __("Actualiser"));
                            newRetryButton.append('<i class="fa fa-refresh cursor-pointer" aria-hidden="true"></i>');
                            newRetryButton.append($('<span class="sr-only">').text(__("Actualiser")));
                            newRetryDiv.find('button').remove();
                            newRetryDiv.find('.td-group-fieldvalue').append(newRetryButton);
                            newRetryDiv.insertBefore(firstAction);
                            newRetryButton.attr('onclick', "addTransferStep2('<?=$id?>?type=zip')");
                            uploadTable.table.find('.action .td-group-sub-sub').not(newRetryDiv).remove();
                        },
                        10
                    );
                }
            }
        });
        $('#<?=$uploadDomId?>-zip').filter('.dropbox')
            .off('fileSuccess')
            .on('fileSuccess', function (event, file) {
                modalTransferAdd2.modal('hide');
                actionEditTransfer('<?=$id?>?add');
            });

        var modalTransferAdd2 = modalBody.closest('.modal');
        var uploaderZip = <?=$uploadsZip->jsObject?>;
        uploaderZip.uploader.on('fileAdded', function (data) {
            currentUploads[data.uniqueIdentifier] = {
                id: null,
                saved: false,
                state: 'uploading' // uncompress | generate_tree
            };
            uploaderZip.dropbox.hide();
        });

        function wrapDelete(table, url, id) {
            uploaderZip.dropbox.show();
            TableGenericAction.deleteAction(table, url)(id, false);
        }

        var currentSubscriptions = [
            'hashcheck_<?=$id?>',
            'transfer_<?=$id?>_attachment_saved',
            'transfer_<?=$id?>_create_tree',
        ];
        AsalaeWebsocket.connect(
            '<?=Configure::read('Ratchet.connect')?>',
            function (session) {
                session.subscribe(
                    'transfer_<?=$id?>_attachment_saved',
                    function (topic, data) {
                        var uploadTable = <?=$uploadsZip->table->tableObject?>;
                        let fid;
                        let attachement_id;
                        [fid, attachement_id] = data.message.split(':');
                        var tableData = uploadTable.getDataId(fid);
                        if (!tableData) {
                            return;
                        }
                        uploaderZip.autoretry = false;
                        var msgId = 'transfer-upload-%d-%d'.format(
                            attachement_id,
                            Math.floor(Math.random() * (16777215 - 1048576 + 1) + 1048576)
                        );
                        if (!currentUploads[fid]) {
                            currentUploads[fid] = {
                                id: null,
                                saved: false,
                                state: 'uploading' // uncompress | generate_tree
                            };
                            uploaderZip.dropbox.hide();
                        }
                        currentUploads[fid].id = attachement_id;
                        currentUploads[fid].saved = true;
                        currentUploads[fid].state = 'uncompress';
                        var tr = uploadTable.table.find('tr[data-id=' + fid + ']');
                        if (tr.length === 0) { // cas ou l'upload est déjà finalisé
                            tr = uploadTable.table.find('tr[data-id=' + attachement_id + ']');
                            tableData = uploadTable.getDataId(attachement_id);
                        }
                        tr.find('td.action a, td.action button').disable();
                        var msg = $('<span class="websocket-message"></span>').attr('id', msgId);
                        var td = tr.addClass('warning')
                            .find('td.message')
                            .append(
                                $('<div class="text-center"></div>').append(
                                    '<i aria-hidden="true" class="fa fa-2x fa-spinner faa-spin animated"></i>'
                                )
                            )
                            .append(msg);
                        currentSubscriptions.push('addcompressed_' + attachement_id);
                        AsalaeWebsocket.subscribe('addcompressed_' + attachement_id, function (topic, data) {
                            var nmsg = $('#' + msgId);
                            if (nmsg.get(0) !== msg.get(0)) {
                                msg = nmsg;
                                td = nmsg.closest('td');
                            }
                            msg.text(data.message);
                            tableData.message = td.html();
                            if (currentUploads[fid]) {
                                currentUploads[fid].state = 'generate_tree';
                            }
                        });
                        session.subscribe(
                            'transfer_<?=$id?>_create_tree',
                            function (topic, data) {
                                var nmsg = $('#' + msgId);
                                if (nmsg.get(0) !== msg.get(0)) {
                                    msg = nmsg;
                                    td = nmsg.closest('td');
                                }
                                msg.text(data.message);
                                tableData.message = td.html();
                            }
                        );
                    }
                );
            }
        );
        modalTransferAdd2.one(
            'hidden.bs.modal',
            () => AsalaeWebsocket.unsubscribe(currentSubscriptions)
        );
    </script>
<?php

echo '<hr>';
echo $paginatorZip->generate(
    [
        'actions' => [
            '0' => __("-- Action groupée --"),
            'delete' => __("Supprimer"),
        ],
    ]
);
echo $this->Html->tag('/div');

echo $this->Form->end();
echo $paginator->scrollTop();
?>
    <script>
        var zipDiv = $('#<?=$addTransferZipDiv?>');
        if (zipDiv.hasClass('hidden')) {
            zipDiv.hide().removeClass('hidden');
        }
        var fileDiv = $('#<?=$addTransferManualDiv?>');
        if (fileDiv.hasClass('hidden')) {
            fileDiv.hide().removeClass('hidden');
        }
        var btnGroup = $('#<?=$btnGroupId?>');
        var inputGenerateTree = $('#<?=$idPrefix?>-generate-tree');
        $('#<?=$buttonAddZip?>').click(function () {
            inputGenerateTree.val(1);
            btnGroup.slideUp();
            zipDiv.slideDown();
        });
        $('#<?=$buttonAddManual?>').click(function () {
            inputGenerateTree.val(0);
            btnGroup.slideUp();
            fileDiv.slideDown();
        });

        function chooseUploadType() {
            btnGroup.slideDown();
            if (fileDiv.is(':visible')) {
                fileDiv.slideUp();
            }
            if (zipDiv.is(':visible')) {
                zipDiv.slideUp();
            }
        }

        $('#<?=$uploadDomId?>-manual, #<?=$uploadDomId?>-zip').one('fileSuccess', function () {
            $('#<?=$addTransferManualDiv?> .choose-upload-type, #<?=$addTransferZipDiv?> .choose-upload-type').remove();
        });
    </script>
<?php
echo $this->fetch('after');
