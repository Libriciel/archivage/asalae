<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Transferts entrants"));
$this->Breadcrumbs->add(__("Transferts rejetés"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-times', __("Transferts rejetés")))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

require 'index-common.php';
?>
<script>
    $(function() {
        AsalaeWebsocket.connect(
            '<?=Configure::read('Ratchet.connect')?>',
            function (session) {
                session.subscribe('transfer_rejected', function (topic, data) {
                    if (data.message.user_id === PHP.user_id) {
                        AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
                    }
                });
            },
            function () {
            },
            {'skipSubprotocolCheck': true}
        );
    });
</script>

