<?php

/**
 * @var Asalae\View\AppView $this
 */
$loading = $this->Html->tag(
    'div.text-center.loading-container',
    $this->Fa->i('fa-4x fa-spinner faa-spin animated')
);
echo $this->Form->create(null, ['id' => 'add-multiple-keywords-form']);

echo $this->Form->control(
    'load-keywords',
    [
        'label' => __("Mots-clés issue de listes"),
        'id' => 'select-load-keywords',
        'options' => [],
        'empty' => true,
        'multiple' => true,
        'data-placeholder' => __("-- Sélectionner un mot clé --"),
    ]
);

echo $this->Html->tag('h4.fake-label', __("Saisie libre"));
echo $this->Html->tag('table', null, ['class' => 'table table table-striped table-hover']);

echo $this->Html->tag('thead');
echo $this->Html->tableHeaders(
    [
        __("Nom"),
        __("Code"),
    ]
);
echo $this->Html->tag('/thead');

echo $this->Html->tag('tbody');
echo $this->Html->tableCells(
    [
        $this->Form->control(
            'name',
            [
                'label' => false,
                'class' => 'name multiple-input-gen',
                'required' => false,
                'id' => 'add-multiple-keyword-0-name',
                'name' => 'customKeyword[0][name]',
            ]
        ),
        $this->Form->control(
            'code',
            [
                'label' => false,
                'class' => 'code multiple-input-gen',
                'required' => false,
                'id' => 'add-multiple-keyword-0-code',
                'name' => 'customKeyword[0][code]',
            ]
        ),
    ]
);
echo $this->Html->tag('/tbody');

echo $this->Html->tag('/table');

echo $this->Form->end();
?>
<script>
    var form = $('#add-multiple-keywords-form');
    var modal = form.closest('.modal');
    var footer = modal.find('.modal-footer');
    footer.find('button.cancel')
        .removeAttr('data-dismiss')
        .off()
        .on('click.partialEdit', function (event) {
            event.preventDefault();
            $('#data-edit-transfert').slideUp(400, function () {
                $(this).html('').show();
                $('#jstree-edit-transfert').enable().removeClass('disabled');
                restoreButtons();
            });
        });

    modal.find('button.accept')
        .removeAttr('data-dismiss')
        .off()
        .on('click.partialEdit', function (event) {
            event.preventDefault();
            var div = $('#data-edit-transfert');
            detectedError = false;
            AsalaeModal.submit(form, {
                success: function (content, textStatus, jqXHR) {
                    if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                        div.empty();
                        footer.find('button.cancel, button.accept').off();
                        modal.one('ajax.complete', function () {
                            restoreButtons();
                            tree.on('loaded.jstree', function () {
                                var instance = tree.jstree(true);
                                var objs = instance.get_json('#', {no_state: true, flat: true});
                                var path;
                                for (var i = 0; i < objs.length; i++) {
                                    path = instance.get_node(objs[i].id).original.path;
                                    if (path === content) {
                                        instance._open_to(objs[i].id)
                                            .children('.jstree-wholerow')
                                            .addClass('jstree-wholerow-clicked');
                                        break;
                                    }
                                }
                            });
                        });
                        actionEditTransfer(<?=$id?>);
                    } else {
                        div.html(content);
                        setTimeout(function () {
                            div.find(':invalid')
                                .filter('input:visible, textarea:visible, select:visible, select.chosenified')
                                .first()
                                .focus()
                                .trigger('chosen:activate');
                        }, 400);
                    }
                }
            });
        });

    var selectedKeywords = $('#select-load-keywords');
    selectedKeywords.select2({
        multiple: true,
        ajax: {
            url: '<?=$this->Url->build('/Keywords/populateSelect')?>',
            method: 'POST',
            delay: 250
        },
        escapeMarkup: function (v) {
            return $('<span></span>').html(v).text();
        },
        dropdownParent: selectedKeywords.closest('.modal'),
        allowClear: true
    });

    function multipleInputHandler() {
        var idMatch = /add-multiple-keyword-(\d+)-(\w+)/;
        var value = $(this).val(),
            cssClass = $(this).hasClass('code') ? 'code' : 'name',
            empty = false,
            trs,
            i,
            j,
            tbody = $(this).closest('tbody');
        var emptyTrs = tbody.find('tr').filter(function () {
            var empty = false;
            $(this).find('input.name').each(function () {
                if (!$(this).val()) {
                    empty = true;
                    return false;
                }
            })
            return empty;
        });

        // On ne garde que le 1er lot d'input vide et on le place en dernier
        if (!value && !$(this).closest('tr').find('input').not('.' + cssClass).val()) {
            if (emptyTrs.length > 1) {
                for (i = 1; i < emptyTrs.length; i++) {
                    emptyTrs[i].remove();
                }
            }
            tbody.append(emptyTrs[0]);
        }

        // On restaure les ids/names avec les numéros dans le bon ordre
        trs = tbody.find('tr');
        for (i = 0; i < trs.length; i++) {
            $(trs[i]).find('input').each(function () {
                var match = $(this).attr('id').match(idMatch);
                if (match && parseInt(match[1], 10) !== i) {
                    $(this).attr('name', 'customKeyword[' + i + '][' + match[2] + ']')
                        .attr('id', 'add-multiple-keyword-' + i + '-' + match[2])
                }
            });
        }

        // Si il n'y a plus de lots vide Disponible, on en créer un nouveau
        if (emptyTrs.length === 0) {
            var tr = $(trs[trs.length - 1]).clone();
            tr.find('input').each(function () {
                var match = $(this).attr('id').match(idMatch);
                if (match) {
                    $(this).attr('name', 'customKeyword[' + trs.length + '][' + match[2] + ']')
                        .attr('id', 'add-multiple-keyword-' + trs.length + '-' + match[2])
                        .val('')
                        .change(multipleInputHandler);
                }
            });
            tbody.append(tr);
        }
    }

    $('.multiple-input-gen').change(multipleInputHandler);
</script>
