<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Csv->resultTable(
    [
        'transfer_date' => __("Date du transfert"),
        'transfer_identifier' => __("Identifiant"),
        'message_versiontrad' => __("Version SEDA"),
        'statetrad' => __("Etat"),
        'archival_agency.name' => __("Service d'Archives"),
        'transferring_agency.name' => __("Service versant"),
        'originating_agency.name' => __("Service producteur"),
        'transfer_comment' => __("Commentaire"),
        'created' => __("Date de création"),
        'data_size_readable' => __("Taille PJs"),
        'data_size' => __("Taille PJs (octets)"),
        'created_user.username' => __("Créé par"),
    ],
    $results
);
