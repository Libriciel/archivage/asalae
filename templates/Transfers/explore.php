<?php

/**
 * @var Asalae\View\AppView $this
 */

$modalId = 'explore-transfer';
$modalLabelId = 'explore-transferLabel';
$tabsId = 'explore-transfer-tabs-content';
$editorTabId = 'explore-transfer-editor-tab';
$filesTabId = 'explore-transfer-files-tab';
$jstreeId = 'jstree-explore-transfert';
$dataId = 'data-explore-transfert';
$groupActionId = 'group-action-explore-transfer-attachments';
$filesTableId = 'transfer-explore-files-table';
$searchbarId = 'transfer-explore-searchbar';
$baseUrl = $this->Url->build('/transfers/explore/' . $id);

echo $this->Html->tag(
    'div.alert.alert-info',
    __(
        "La taille du transfert (> 2,5Mo) ne permet pas d'ouvrir le "
        . "transfert via la visionneuse. Vous pouvez accéder aux informations du "
        . "bordereau en cliquant sur les éléments de l'arborescence."
    )
);

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);
echo $this->Html->tag(
    'script',
    "$('#$modalLabelId').html(\"" . __("Afficher le bordereau")
    . ' - ' . h($entity->get('transfer_identifier')) . ' - ' . $entity->get('seda_versiontrad') . "\")"
);

/**
 * Editeur XML
 */
echo $this->Html->tag('div#' . $jstreeId . '.col-md-4', $loading)
    . $this->Html->tag(
        'div#' . $dataId . '.col-md-8.jstree-div-form',
        $this->Form->create()
        . $this->Form->control('action', ['type' => 'hidden', 'default' => 'cancel'])
        . $this->Form->end()
    );
?>
<script>
    $('.jstree[role=tree]:not(:visible)').empty();
    var tree = $('#<?=$jstreeId?>');
    var modal = tree.closest('.modal');
    var zoneDescription = $('#<?=$dataId?>');
    var loadingAnim = $('<?=$loading?>');
    var ajaxData = {
        url: '<?=$this->Url->build('/transfers/get-tree/' . $id)?>/',
        data: function (node) {
            return {
                id: node.id
            };
        }
    };

    function partialExploreTransfer(dataPath) {
        var div = $('#<?=$dataId?>').html('<?=$loading?>');
        $.ajax({
            url: '<?=$this->Url->build('/transfers/partial-explore/' . $entity->get('id'))?>/' + dataPath,
            success: function (content) {
                div.html(content);
            },
            error: function () {
                alert(PHP.messages.genericError);
                div.html('');
            }
        });
    }

    function nodeCount(node) {
        var parent = tree.jstree(true).get_node(node.parent);
        var count = 0;
        var element;
        for (var i = 0; i < parent.children.length; i++) {
            element = tree.jstree(true).get_node(parent.children[i]).original.element;
            if (element === node.original.element) {
                count++;
            }
        }
        return count;
    }

    $(document).off('.vakata.custom').off('.jstree.custom');
    tree.jstree({
        core: {
            data: ajaxData,
            check_callback: function (op, node, par, pos, more) {
                return true;
            }
        },
        search: {
            case_insensitive: true,
            show_only_matches: false,
            search_callback: function (search, node) {
                search = search.toLowerCase().unaccents();
                if (typeof node.original.searchtext === 'undefined') {
                    return node.original.text.indexOf(search) >= 0;
                }
                return node.original.searchtext.indexOf(search) >= 0;
            }
        },
        plugins: [
            "search", // Recherche par nom de noeud
            "wholerow", // sélection sur la ligne (clic facilité et affichage + moderne)
        ]
    })
        .off('ready.jstree set_state.jstree') // affiche les pointillés (supprimés dans cet event par wholerow)
        .on('select_node.jstree', function (event, data) {
            if (data.node.original.search) {
                var search = data.node.original.search;
                if (typeof search === 'boolean' || search === 'true') {
                    search = '';
                }
                if (search === '') {
                    return;
                }
                AsalaeLoading.ajax({
                    url: '<?=$this->Url->build('/transfers/get-tree/' . $id)?>'
                        + search,
                    method: 'POST',
                    headers: {
                        Accept: 'application/json'
                    },
                    success: function (content) {
                        var value = $('#<?=$searchbarId?>').val();
                        var jstreeDiv = $('#<?=$jstreeId?>');
                        var jstreeObj = tree.jstree(true);
                        var root = jstreeObj.get_node('#');
                        jstreeObj.delete_node(root.children);

                        function recursiveCreateNode(parent, node) {
                            var cNode = $.extend(true, {}, node);
                            if (!cNode.id) {
                                return;
                            }
                            cNode.children = typeof cNode.children === 'boolean' ? cNode.children : false;
                            parent = jstreeObj.create_node(parent, cNode);
                            if (typeof node.children === 'undefined') {
                                return;
                            }
                            for (var key in node.children) {
                                recursiveCreateNode(parent, node.children[key]);
                                if (Array.isArray(node.children[key].children)) {
                                    jstreeObj.open_node(node.children[key]);
                                }
                            }
                        }

                        recursiveCreateNode(root, content);
                        prependSearchbar(value);
                        if (typeof data.node.original.search === 'string') {// boolean = retour en arrière
                            var selectTarget = data.node.original.search.replace(/^\//, '').split('/');
                            var nodeId = '1_' + selectTarget.join('_').replace(/\[/g, '-').replace(']', '');
                            var node = jstreeObj.get_node(nodeId);
                            // si le noeud n'existe pas dans le jstree,
                            // c'est qu'il faut appeler le noeud parent (ex: Name, Title...)
                            if (!node) {
                                selectTarget.pop();
                                nodeId = '1_' + selectTarget.join('_').replace(/\[/g, '-').replace(']', '');
                                node = jstreeObj.get_node(nodeId);
                            }
                            jstreeObj.select_node(node.id);
                        }
                    },
                    complete: function () {
                        tree.trigger('search_completed.jstree');
                    }
                })
            } else if (data.node.original.navigation) {
                $('#' + data.node.id).html(
                    "<li class='jstree-initial-node jstree-loading jstree-leaf jstree-last'>"
                    + "<i class='jstree-icon jstree-ocl'></i><" + "a class='jstree-anchor' href='#'>"
                    + "<i class='jstree-icon jstree-themeicon-hidden'></i>" + "Loading ..." + "</a></li>"
                );
                $.ajax({
                    url: '<?=$this->Url->build('/transfers/get-tree/' . $id)?>/?id=' + data.node.id,
                    success: function (content) {
                        var jstreeObj = tree.jstree(true);
                        var parentId = data.node.parent;
                        var parent = jstreeObj.get_node(parentId);
                        if (parent.original.navigation) { // si le parent est une liste, on doit remonter d'un cran
                            parentId = parent.parent;
                            parent = jstreeObj.get_node(parentId);
                        }

                        if (typeof content === 'string') {
                            return this.error({responseText: content});
                        }

                        jstreeObj.delete_node(parent.children);
                        for (var i = 0; i < content.length; i++) {
                            jstreeObj.create_node(parentId, content[i]);
                        }
                        var title = data.node.id.split('_');
                        title.shift();
                        var page = title.shift();

                        zoneDescription.html($('<h4 class="h2"></h4>').text(title.pop() + ' - Page ' + page));
                    },
                    error: function (e) {
                        zoneDescription.html(e.responseText);
                    }
                });
            } else {
                partialExploreTransfer(data.node.original.url);
            }
            // scroll top
            tree.closest('.modal-body').animate({
                scrollTop: 0
            }, 500);

        }).on('loaded.jstree', function () {
        prependSearchbar('');
        var treeJstree = tree.jstree(true);
        tree.jstree(true).open_node('1_ArchiveTransfer_Contains'); // seda0.2
        tree.jstree(true).open_node('1_ArchiveTransfer_Archive'); // seda1.0

    }).on('changed.jstree after_open.jstree after-pagination search.jstree clear_search.jstree', function () {
        // au survol, affiche le text entier d'un noeud
        tree.find('span.child-value.trimmed:not(.mouseover)')
            .off('.jstree')
            .on('mouseover.jstree', function () {
                var trimmed = $(this).text();
                var name = $(this).attr('title');
                $(this).addClass('mouseover')
                    .attr('data-text', trimmed)
                    .text(name);
            })
            .on('mouseout.jstree', function () {
                var trimmed = $(this).attr('data-text');
                $(this).removeClass('mouseover')
                    .attr('data-text', '')
                    .text(trimmed);
            });
    });

    function prependSearchbar(value) {
        tree.jstree("search", value);
        tree.prepend(
            $('<form class="form-group text"></form>').append(
                $('<label></label>')
                    .attr('for', '<?=$searchbarId?>')
                    .text(__("Rechercher"))
            ).append(
                $('<div class="input-group"></div>').append(
                    $('<input class="form-control">')
                        .attr('id', '<?=$searchbarId?>')
                        .val(value)
                        .on('keyup', function () {
                            tree.jstree("search", $(this).val());
                        })
                ).append(
                    $('<span class="input-group-addon btn btn-primary" role="button" tabindex="0"></span>').append(
                        $('<i class="fa fa-search" aria-hidden="true"></i>')
                    ).append($('<span class="sr-only"></span>').text(__("Rechercher")))
                        .attr('title', __("Rechercher"))
                        .on('click', doSearch)
                )
            ).on('submit', function (event) {
                event.preventDefault();
                $(this).find('.btn[role="button"]').click();
            })
        );
    }

    function ajaxSearchKeyword(element) {
        $(element).select2({
            ajax: {
                url: '<?=$this->Url->build('/Keywords/populateSelect')?>',
                method: 'POST',
                delay: 250
            },
            escapeMarkup: function (v) {
                return $('<span></span>').html(v).text();
            },
            dropdownParent: $(element).closest('.modal'),
            allowClear: true
        });
        var button = $(element).siblings('.help-block').find('button').hide();
        $(element).on('change', function () {
            var actualValue = $('.keyword-name').val();
            if (!actualValue) {
                button.click();
            } else {
                button.show();
            }
        });
    }

    function doSearch() {
        var searchStr = $('#<?=$searchbarId?>').val();
        if (searchStr.length > 2) {
            var button = $(this).disable();
            AsalaeLoading.ajax({
                url: '<?=$this->Url->build('/transfers/search-in-edit/' . $id)?>',
                method: 'POST',
                headers: {
                    Accept: 'application/json'
                },
                data: {
                    search: searchStr
                },
                success: function (content) {
                    var value = $('#<?=$searchbarId?>').val();
                    var jstreeDiv = $('#jstree-edit-transfert');
                    var jstree = tree.jstree(true);
                    var rootNode = jstree.get_node('#');
                    for (var i = 0; i < rootNode.children.length; i++) {
                        jstree.delete_node(rootNode.children[i]);
                    }
                    var searchNode = jstree.get_node(
                        jstree.create_node(rootNode, {
                            id: 'search-in-edit-results',
                            icon: 'fa fa-search',
                            text: __("Résultats de la recherche (cliquer ici pour revenir)"),
                            search: true,
                            state: {
                                opened: true
                            }
                        })
                    );
                    var text;
                    for (i = 0; i < content.length; i++) {
                        text = content[i].path.replace(/\//g, ' / ').replace('@', ' @ ');

                        if (content[i].value.length > 30) {
                            text += ' <span class="child-value" title="'
                                + content[i].value.replace(/"/g, '&quot;') + '">'
                                + AsalaeGlobal.shortString(
                                    content[i].value,
                                    content[i].path.indexOf('@filename') !== -1
                                )
                                + '</span>';
                        } else {
                            text += ' <span class="child-value">' + content[i].value + '</span>';
                        }
                        jstree.create_node(searchNode, {
                            icon: 'fa fa-arrow-right',
                            text: text,
                            search: content[i].path
                        });
                    }
                    prependSearchbar(value);
                },
                complete: function () {
                    button.enable();
                }
            });
        }
    }
</script>
