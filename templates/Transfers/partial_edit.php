<?php

/**
 * @var Asalae\View\AppView $this
 * @var array               $path
 */

use Cake\I18n\Date as CakeDate;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\Utility\Text;

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

echo $this->Html->tag('h4.h2', h(end($path))) . '<hr>';
foreach ($path as $title) {
    $this->Breadcrumbs->add($title);
}
echo $this->Breadcrumbs->render();

echo $this->Form->create($form, ['id' => $formId = uniqid('partial-edit-form-'), 'class' => 'transfer-form']);
$schema = $form->schema['children'] ?? $form->schema;

$datepickerFn = function (array &$params) {
    $view = $this;
    $isDate = false;
    if (($params['data-type'] ?? false) === 'datetime') {
        $params['append'] = $view->Date->datetimePicker('#' . $params['id']);
        try {
            $params['default'] = empty($params['default']) ? null : CakeDateTime::parse($params['default']);
        } catch (\Exception) {
        }
        $isDate = true;
    } elseif (($params['data-type'] ?? false) === 'date') {
        $params['append'] = $view->Date->picker('#' . $params['id']);
        try {
            $params['default'] = empty($params['default']) ? null : CakeDate::parse($params['default']);
        } catch (\Exception) {
        }
        $isDate = true;
    }
    if ($isDate) {
        $params['type'] = 'text';
        $params['class'] = 'datepicker';
        if ($params['default'] instanceof DateTimeInterface) {
            $params['default']->setTimezone(new DateTimeZone(date_default_timezone_get()));
        }
    }
};

// Range par champ requis
$dataRequired = [];
$dataNotRequired = [];
foreach ($data as $field => $values) {
    $schemaField = $form->schema['children'][$field];
    $card = $schemaField['cardinality'] ?? '0..1';
    if (substr($card, 0, strpos($card, '..')) !== '1') {
        $dataNotRequired[$field] = $values;
    } else {
        $dataRequired[$field] = $values;
    }
}
$data = $dataRequired + $dataNotRequired;

if (!empty($attributes)) {
    echo $this->Html->tag('div.attributesList');
    foreach ($attributes as $attr => $value) {
        echo $this->Form->control(
            '@' . $attr,
            [
                'label' => $attr,
                'val' => $value,
                'disabled' => true,
            ]
        );
    }
    echo $this->Html->tag('/div') . '<hr>';
}

if (empty($data)) {
    echo $this->Html->tag(
        'div.alert.alert-info',
        __("Aucun élément n'est directement sur ce noeud. Veuillez appuyer sur enregistrer.")
    );
}

$isAttr = false;
foreach ($data as $field => $values) {
    $schemaField = $form->schema['children'][$field];
    if (isset($schemaField['display']) && !$schemaField['display']) {
        continue;
    }
    unset($form->schema['children']['Contains']);
    $domId = mb_strtolower(Text::slug($field, '-'));

    foreach ($values as $i => $value) {
        $params = $schemaField;
        $params['id'] = $domId . '-' . $i;
        $params['name'] = $field . '[' . $i . '][@]';
        $params['data-default'] = ($params['default'] ?? '');
        $params['default'] = $value['@'] ?: $params['data-default'];
        $params['empty'] = true;
        unset($value['@']);
        [$required, $max] = explode('..', $params['cardinality'] ?? '0..1');
        $params['required'] = (bool)$required;
        $prepend = '';
        $append = '';

        // Bouton d'ajout (champs multiples ex: 1..n)
        if ($max === 'n') {
            if ($i === 0) {
                $onclick = "appendNew('{$params['id']}')";
                $message = $this->Fa->i('fa-plus')
                    . $this->Html->tag('span.sr-only', __("Ajouter un {0}", $field));
            } else {
                $onclick = "removeNode('{$params['id']}')";
                $message = $this->Fa->i('fa-minus')
                    . $this->Html->tag('span.sr-only', __("Retirer un {0}", $field));
            }
            $params['append'] = $this->Html->tag(
                'button.btn.btn-default',
                $message,
                ['type' => 'button', 'onclick' => $onclick]
            );
        }

        // Attributs
        if (count($value) > 0) {
            $append = $this->Html->tag('div.attributesList', null, ['style' => 'display: none']);
            foreach ($value as $attr => $attrValue) {
                $schemaAttr = $schemaField['attributes'][$attr];
                $arg = [
                    'id' => $field . '-' . $i . '-' . $attr,
                    'name' => $field . '[' . $i . '][' . $attr . ']',
                    'default' => $attrValue ?: ($schemaAttr['default'] ?? ''),
                    'data-default' => ($schemaAttrg['default'] ?? ''),
                    'label' => $schemaAttr['label'] ?? $attr,
                ] + $schemaAttr;
                $datepickerFn($arg);
                unset($arg['validations'], $arg['allowEmpty']);
                $append .= $this->Form->control($field . '_#' . $i . '_@' . $attr, $arg);
                if (isset($arg['options']) && (empty($arg['type']) || $arg['type'] !== 'select')) {
                    $append .= $this->Html->tag(
                        'script',
                        'AsalaeGlobal.chosen($(\'#' . $field . '-' . $i . '-' . $attr . '\'), __("Rechercher"));'
                    );
                }
            }
            $append .= $this->Html->tag('/div');
        }

        // Bouton afficher les attributs dans label
        $params['label'] = $params['label'] ?? $field;
        if (!empty($params['attributes'])) {
            $params['templates']['label'] = "<label{{attrs}}>{{text}}</label> "
                . $this->Html->tag(
                    'button.btn-link',
                    $this->Fa->i('fa-tags')
                    . $this->Html->tag('span.sr-only', __("Afficher les attributs")),
                    [
                        'type' => 'button',
                        'title' => __("Afficher les attributs"),
                        'onclick'
                        => "$('#{$params['id']}').closest('.form-group').find('.attributesList').toggle('blind')",
                    ]
                );
        }

        // Aide
        if (isset($params['info'])) {
            $prepend = $this->Html->tag(
                'button.btn-link',
                $this->Fa->i('fa-info-circle')
                    . $this->Html->tag('span.sr-only', __("Aide: {0}", $params['info'])),
                [
                    'type' => 'button',
                    'title' => $params['info'],
                    'style' => 'cursor: help',
                ]
            ) . ' ';
            $params['label'] = [
                'text' => $params['label'],
                'title' => $params['info'],
                'style' => 'cursor: help',
            ];
            unset($params['info']);
        }

        // Cas d'un datetime (conversion en champs avec datepicker)
        $datepickerFn($params);

        $templates = [
            'inputContainer' => '<div class="form-group {{type}}{{required}}">'
                . $prepend . '{{content}}' . $append . '</div>',
            'inputContainerError' => '<div class="form-group has-error {{type}}{{required}}">'
                . $prepend . '{{content}}{{error}}' . $append . '</div>',
        ];
        $params['templates'] = isset($params['templates'])
            ? $params['templates'] + $templates
            : $templates;

        $callback = $params['afterload'] ?? null;
        $chosen = $params['chosen'] ?? true;
        unset(
            $params['afterload'],
            $params['chosen'],
            $params['attributes'],
            $params['cardinality'],
            $params['validations'],
            $params['allowEmpty']
        );
        echo $this->Form->control($field . '_#' . $i, $params);
        $scripts = [];
        if ($chosen && isset($params['options']) && (empty($params['type']) || $params['type'] !== 'select')) {
            $scripts[] = 'AsalaeGlobal.chosen($(\'#' . $params['id'] . '\'), __("Rechercher"));';
        }
        if ($callback) {
            $scripts[] = $callback . '(\'#' . $params['id'] . '\');';
        }
        echo $this->Html->tag('script', implode(PHP_EOL, $scripts));
    }
}

echo $this->Form->button(__("Enregistrer"), ['class' => 'hide accept']);

echo $this->Form->end();
?>
<!--suppress JSUnnecessarySemicolon, JSDeprecatedSymbols, RequiredAttributes -->
<script>
    var form = $('#<?=$formId?>');
    var modal = form.closest('.modal');
    var footer = modal.find('.modal-footer');
    var formDiv = modal.find('.form-div');
    var title = formDiv.find('h4').text();
    var cancel = $('<button type="button" class="btn btn-default cancel"></button>')
        .append('<i class="fa fa-times-circle" aria-hidden="true"></i>')
        .append($('<span></span>').text(' ' + __("Annuler les modifications sur {0}", title)))
        .on('click.partialEdit', function (event) {
            event.preventDefault();
            detectedError = false;
            formDiv.slideUp(400, function () {
                $(this).html('').show();
                $('#jstree-edit-transfert').enable().removeClass('disabled');
                restoreButtons();
            });
            $('#data-repair-transfert').slideUp(400, function () {
                $(this).html('').show();
                $('#jstree-repair-transfert').enable().removeClass('disabled');
                restoreButtons();
            });
        });
    var accept = $('<button type="button" class="btn btn-primary accept"></button>')
        .append('<i class="fa fa-floppy-o" aria-hidden="true"></i>')
        .append($('<span></span>').text(' ' + __("Enregistrer les modifications sur {0}", title)));
    footer.empty()
        .append(cancel)
        .append(accept);

    modal.find('button.accept') // inclue le boutton caché dans le formulaire
        .on('click.partialEdit', function (event) {
            event.preventDefault();
            if (form.get(0).checkValidity()) {
                var div = formDiv.html('<?=$loading?>');
                var submit = $(this).disable();
                detectedError = false;
                window.onbeforeunload = function (e) {
                    e = e || window.event;
                    var msg = "<?=__("CONFIRM_EXIT_PAGE")?>";
                    if (e) {
                        e.returnValue = msg;
                    }
                    return msg;
                };
                $.ajax({
                    url: form.attr('action'),
                    method: 'POST',
                    data: form.serialize(),
                    success: function (content, textStatus, jqXHR) {
                        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                            div.empty();
                            footer.find('button.cancel, button.accept').off();
                            modal.one('ajax.complete', function () {
                                restoreButtons();
                                tree.on('loaded.jstree', function () {
                                    var instance = tree.jstree(true);
                                    var objs = instance.get_json('#', {no_state: true, flat: true});
                                    var path;
                                    for (var i = 0; i < objs.length; i++) {
                                        path = instance.get_node(objs[i].id).original.path;
                                        if (path === content) {
                                            instance._open_to(objs[i].id)
                                                .children('.jstree-wholerow')
                                                .addClass('jstree-wholerow-clicked');
                                            break;
                                        }
                                    }
                                });
                            });
                            <?=$refreshJsCallback?>;
                        } else {
                            div.html(content);
                            setTimeout(function () {
                                div.find(':invalid')
                                    .filter('input:visible, textarea:visible, select:visible')
                                    .first()
                                    .focus();
                            }, 400);
                        }
                    },
                    error: function () {
                        alert(PHP.messages.genericError);
                    },
                    complete: function () {
                        submit.enable();
                    }
                });
            } else {
                form.get(0).reportValidity();
                AsalaeGlobal.colorTabsInError();
            }
        });

    <?php if (!empty($selectedAttachment)) : ?>
    $('.initialized-attachment').append(
        $('<optgroup></optgroup>')
            .attr('label', '<?=addcslashes($selectedAttachment['group'], "'")?>')
            .append(
                $('<option></option>')
                    .attr('value', '<?=$v = addcslashes($selectedAttachment['value'], "'")?>')
                    .text('<?=$v?>')
                    .prop('selected', true)
            )
    ).select2({
        ajax: {
            url: '<?=$this->Url->build('/TransferAttachments/populateSelect/' . $id)?>?tempfile=true',
            method: 'POST',
            delay: 250
        },
        escapeMarkup: function (v) {
            return $('<span></span>').html(v).text();
        },
        dropdownParent: modal,
        allowClear: false
    }).trigger('change');
    <?php endif; ?>

    $('.form-group .attributesList').each(function () {
        (function (list) {
            list.find('input, select, textarea').on('change', function () {
                var hasAttribute = false;
                list.find('input, select, textarea').each(function () {
                    if ($(this).val()) {
                        hasAttribute = true;
                        return false;
                    }
                });
                list.closest('.form-group')
                    .toggleClass('has-attribute', hasAttribute);
            }).first().trigger('change');
        })($(this));
    });

    $('input[data-validationtype]').off('.oldestdate').on('change.oldestdate', function () {
        var latestdateElem = $('input[data-validationtype="latestdate"]');
        var oldestdateElem = $('input[data-validationtype="oldestdate"]');
        var latestdate = latestdateElem.val();
        var oldestdate = oldestdateElem.val();
        if (!latestdate || !oldestdate) {
            latestdateElem.get(0).setCustomValidity('');
            oldestdateElem.get(0).setCustomValidity('');
        }
        if (latestdate) {
            latestdate = new Date(
                moment(latestdate, moment().creationData().locale._longDateFormat.L).format('Y-MM-DD')
            );
        }
        if (oldestdate) {
            oldestdate = new Date(
                moment(oldestdate, moment().creationData().locale._longDateFormat.L).format('Y-MM-DD')
            );
        }
        if (latestdate < oldestdate) {
            latestdateElem.get(0).setCustomValidity(
                __("LatestDate ne peut pas être plus ancienne que OldestDate")
            );
        } else {
            latestdateElem.get(0).setCustomValidity('');
            oldestdateElem.get(0).setCustomValidity('');
        }
    });
</script>
