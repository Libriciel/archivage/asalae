<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Csv->resultTable(
    [
        'name' => __("Nom"),
        'identifier' => __("Identifiant"),
        'active' => __("Actif"),
        'version' => __("Version"),
        'count' => __("Nombre mots-clés"),
        'created' => __("Date de création"),
        'modified' => __("Date de modification"),
    ],
    $results
);
