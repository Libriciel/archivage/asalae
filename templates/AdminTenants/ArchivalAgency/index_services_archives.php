<?php

/**
 * @var Asalae\View\AppView $this
 */
$view = $this;

echo $this->ModalForm->create('add-notice-sa')
    ->modal(__("Ajout d'une notice d'autorité au service d'archives"))
    ->javascriptCallback('afterAddNoticeSA')
    ->output('function', 'addNoticeSA', '/admin-tenants/addEaccpf')
    ->generate();

echo $this->ModalForm->create('add-user-sa')
    ->modal(__("Ajout d'un utilisateur au service d'archives"))
    ->javascriptCallback('afterAddUserSA')
    ->output('function', 'addUserSA', '/admin-tenants/addUser')
    ->generate();


echo $this->element('modal', ['idTable' => $tableIdServiceArchive, 'paginate' => false]);
$tableService = $this->Table
    ->create($tableIdServiceArchive, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'OrgEntities.name' => ['label' => __("Nom")],
            'OrgEntities.identifier' => ['label' => __("Identifiant")],
            'OrgEntities.is_main_archival_agency' => ['label' => __("Gestionnaire"), 'type' => 'boolean'],
            'OrgEntities.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
            ],
            'OrgEntities.modified' => [
                'label' => __("Date de modification"),
                'type' => 'datetime',
                'display' => false,
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'OrgEntities.id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "loadEditModalServiceArchive({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'params' => ['OrgEntities.id', 'OrgEntities.name'],
            ],
        ]
    );

$jsTableServiceArchive = $tableService->tableObject;

echo $this->ModalForm
    ->create('service-archive-edit', ['size' => 'modal-xxl'])
    ->modal(__("Modification d'un service d'Archives"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTableServiceArchive . ', "OrgEntities")')
    ->output('function', 'loadEditModalServiceArchive', '/admin-tenants/editServiceArchive')
    ->generate();

echo $this->Html->tag(
    'section',
    $this->Html->tag('header')
    . $this->Html->tag('h2.h4', __("Liste des services d'Archives"))
    . $this->Html->tag('div.r-actions.h4', $tableService->getConfigureLink())
    . $this->Html->tag('/header')
    . $tableService->generate(),
    ['id' => 'service-archive-section', 'class' => "bg-white"]
);

echo $this->ModalForm->create('index-sa-add-notice-sa')
    ->modal(__("Ajout d'une notice d'autorité au service d'Archives"))
    ->javascriptCallback('afterAddNoticeSA')
    ->output('function', 'addNoticeSA', '/admin-tenants/addEaccpf')
    ->generate();

echo $this->ModalForm->create('index-sa-add-user-sa')
    ->modal(__("Ajout d'un utilisateur au service d'Archives"))
    ->javascriptCallback('afterAddUserSA')
    ->output('function', 'addUserSA', '/admin-tenants/addUser')
    ->generate();

?>

<script>
    function reloadThisModal() {
        $('#service-archive.modal:visible').find('button.refresh').click();
    }

    /**
     * Callback d'après ajout d'une notice sur le nouveau service d'archives
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterAddNoticeSA(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            addUserSA(content.org_entity_id);
        }
    }

    /**
     * Callback d'après ajout d'un utilisateur sur le nouveau service d'archives
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterAddUserSA(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            loadEditModalServiceArchive(content.org_entity_id);
            if (typeof content.code === 'string') {
                setTimeout(function () {
                    window.open('<?=$this->Url->build('/auth-urls/activate')?>/' + content.code, '_blank');
                }, 100);// laisse le temps à la db d'enregistrer l'url
            }
        }
    }
</script>
