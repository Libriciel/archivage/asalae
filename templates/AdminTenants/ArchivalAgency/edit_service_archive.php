<?php
/**
 * @var Asalae\View\AppView $this
 */

echo $this->element('modal', ['idTable' => $tableIdEaccpf, 'paginate' => false]);
echo $this->element('modal', ['idTable' => $tableIdUser, 'paginate' => false]);

$tableEaccpf = $this->Table
    ->create($tableIdEaccpf, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Eaccpfs.record_id' => ['label' => __("Identifiant unique")],
            'Eaccpfs.name' => ['label' => __("Nom")],
            'Eaccpfs.entity_id' => ['label' => __("Identifiant ISNI")],
            'Eaccpfs.entity_typetrad' => ['label' => __("Type")],
            'Eaccpfs.from_date' => ['label' => __("Date début"), 'type' => 'date'],
            'Eaccpfs.to_date' => ['label' => __("Date fin"), 'type' => 'date'],
            'Eaccpfs.created' => ['label' => __("Date de création"), 'type' => 'datetime', 'display' => false],
        ]
    )
    ->data($entity->get('eaccpfs'))
    ->params(
        [
            'identifier' => 'Eaccpfs.id',
        ]
    )
    ->actions(
        [
            [
                'href' => "/Eaccpfs/download/{0}",
                'download' => '{2}',
                'target' => '_blank',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => __("Télécharger {0}", '{1}'),
                'aria-label' => __("Télécharger {0}", '{1}'),
                'params' => ['Eaccpfs.id', 'Eaccpfs.name', 'Eaccpfs.record_id'],
            ],
        ]
    );
$jsTableEaccpf = $tableEaccpf->tableObject;

$tableUser = $this->Table
    ->create($tableIdUser, ['class' => 'table table-striped table-hover smart-td-size'])
    ->url(
        $url = [
            'controller' => 'admins',
            'action' => 'paginate-users',
            $entity->id,
            '?' => [
                'sort' => 'username',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'username' => ['label' => __("Identifiant")],
            'name' => ['label' => __("Nom d'utilisateur")],
            'role.name' => ['label' => __("Rôle")],
            'active' => ['label' => __("Actif"), 'type' => 'boolean'],
            'is_linked_to_ldap' => ['label' => __("Lié à un LDAP"), 'type' => 'boolean'],
            'created' => ['label' => __("Date de création"), 'type' => 'datetime', 'display' => false],
            'modified' => ['label' => __("Date de modification"), 'type' => 'datetime', 'display' => false],
        ]
    )
    ->data($entity->get('users'))
    ->params(
        [
            'identifier' => 'id',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewWebservice({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Voir {0}", '{1}'),
                'aria-label' => __("Voir {0}", '{1}'),
                'displayEval' => 'data[{index}].agent_type === "software"',
                'params' => ['id', 'name'],
            ],
            [
                'onclick' => "actionEditWebservice({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'displayEval' => 'data[{index}].agent_type === "software"',
                'params' => ['id', 'name'],
            ],
            [
                'onclick' => "loadEditModalUser({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'displayEval' => 'data[{index}].agent_type === "person"',
                'params' => ['id', 'name'],
            ],
            function ($table) {
                $view = $this;
                $deleteUrl = $view->Url->build('/users/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction({$table->tableObject}, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'displayEval' => 'data[{index}].deletable',
                    'params' => ['id', 'name'],
                ];
            },
        ]
    );
$jsTableUser = $tableUser->tableObject;
$paginator = $this->AjaxPaginator->create('pagination-org-entity-users')
    ->url($url)
    ->table($tableUser)
    ->count($countUsers);
?>
<section class="row no-padding" id="tab-entity-sa-container">
    <?php
    $addBtn = '';
    if ($rolesUsers) {
        $addBtn .= $this->ModalForm
            ->create('edit-entity-add-user-modal')
            ->modal(__("Ajouter un utilisateur"))
            ->javascriptCallback('afterAddUserSA')
            ->output(
                'button',
                $this->Fa->charte('Ajouter', __("Ajouter un utilisateur")),
                '/Users/add/' . $entity->get('id')
            )
            ->generate(['class' => 'btn btn-success', 'type' => 'button']);
    }
    if ($rolesWs) {
        $addBtn .= $this->ModalForm
            ->create('edit-sa-add-webservice-modal')
            ->modal(__("Ajout d'un accès de webservice"))
            ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTableUser . ', "Users")')
            ->output(
                'button',
                $this->Fa->charte('Ajouter', __("Ajouter un accès de webservice")),
                '/Webservices/add/' . $entity->get('id')
            )
            ->generate(['class' => 'btn btn-success']);
    }

    if (count($ldaps)) {
        $addBtn .= $this->ModalForm
            ->create('import-ldap-users')
            ->modal(__("Importer un utilisateur LDAP"))
            ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTableUser . ', "Users")')
            ->output(
                'button',
                $this->Fa->charte('Ajouter', __("Importer un utilisateur LDAP")),
                '/admin-tenants/importLdapUser/' . $entity->get('id')
            )
            ->generate(['class' => 'btn btn-success']);
    }

    echo // Tabulations
        $this->Html->tag(
            'ul',
            $this->Html->tag(
                'li',
                $this->Html->tag(
                    'a',
                    $this->Fa->charte('Modifier')
                    . ' ' . __("Edition du service d'archives"),
                    ['href' => '#entity-sa-section']
                )
            )
            . $this->Html->tag(
                'li',
                $this->Html->tag(
                    'a',
                    $this->Html->tag('span', '', ['class' => 'fa fa-id-card'])
                    . ' ' . __("Notice d'autorité"),
                    ['href' => '#eaccpf-sa-section', 'class' => 'save-on-change']
                )
            )
            . $this->Html->tag(
                'li',
                $this->Html->tag(
                    'a',
                    $this->Fa->charte('Utilisateurs')
                    . ' ' . __("Utilisateurs"),
                    ['href' => '#users-sa-section', 'class' => 'save-on-change']
                )
            )
        )

        // Tab d'édition
        . $this->Html->tag(
            'div',
            $this->Form->create($entity, ['id' => 'edit-org-entity-sa-form', 'idPrefix' => 'edit-org-entity-sa'])
            . $this->Form->control('name', ['label' => __("Nom"), 'required'])
            . $this->Form->control(
                'identifier',
                [
                    'label' => __("Identifiant unique"),
                    'disabled' => !$editableIdentifier,
                    'required',
                ]
            )
            . $this->Form->control(
                'secure_data_spaces._ids',
                [
                    'label' => __("Espaces de stockage sécurisés Disponibles"),
                    'disabled' => true,
                    'options' => $secure_data_spaces,
                    'multiple' => true,
                    'data-placeholder' => __("-- Sélectionner les espaces de stockage sécurisés disponibles --"),
                ]
            )
            . $this->Form->control(
                'default_secure_data_space_id',
                [
                    'label' => __("Espace de stockage sécurisé par défaut"),
                    'required' => true,
                    'options' => $secure_data_spaces,
                    'empty' => __("-- Sélectionner l'espace de stockage sécurisé par défaut--"),
                ]
            )
            . $this->Form->control(
                'timestampers._ids',
                [
                    'label' => __("Services d'horodatage disponibles"),
                    'disabled' => true,
                    'options' => $timestampers,
                    'multiple' => true,
                    'data-placeholder' => __("-- Sélectionner les services d'horodatage disponibles --"),
                ]
            )
            . (
            $entity->get('is_main_archival_agency')
                ? $this->Form->control(
                    'timestamper_id',
                    [
                        'label' => __("Service d'horodatage pour le journal des événements"),
                        'disabled' => true,
                        'options' => $timestampers,
                        'empty' => __("-- Sélectionner le service d'horodatage pour le journal des événements --"),
                    ]
                )
                : ''
            )
            . $this->Form->control(
                'ldaps._ids',
                [
                    'label' => __("Ldaps disponibles"),
                    'options' => $ldaps,
                    'disabled' => true,
                    'multiple' => true,
                    'data-placeholder' => __("-- Sélectionner des Ldaps disponibles --"),
                ]
            )
            . $this->Form->control(
                'archiving_systems._ids',
                [
                    'label' => __("Systèmes d'Archivage Electronique pour les transferts sortants"),
                    'options' => $archivingSystems,
                    'multiple' => true,
                    'disabled' => true,
                    'data-placeholder' => __("-- Sélectionner parmis les SAE disponibles --"),
                ]
            )
            . $this->Html->tag(
                'div',
                $this->Form->button(
                    $this->Fa->charte('Enregistrer', __("Enregistrer")),
                    ['bootstrap-type' => 'primary']
                ),
                ['class' => 'text-right']
            )
            . $this->Form->end(),
            ['id' => 'entity-sa-section']
        )

        // Tab Notice d'autorité
        . $this->Html->tag(
            'section',
            $this->Html->tag('div', '', ['class' => 'separator']) . '
    <header>
        <h2 class="h4">' . __("Liste des notices d'autorité") . '</h2>
        <div class="r-actions h4">
            ' . $tableEaccpf->getConfigureLink() . '
        </div>
    </header>
    ' . $tableEaccpf->generate(),
            ['id' => 'eaccpf-sa-section', 'class' => "bg-white"]
        )

        // Tab Utilisateurs
        . $this->Html->tag(
            'section',
            $this->Html->tag(
                'div',
                $paginator->scrollBottom(['style' => 'position: absolute; right: 20px; z-index:100'])
                . $addBtn
                . $this->ModalForm
                    ->create('user-edit')
                    ->modal(__("Edition d'un utilisateur"))
                    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTableUser . ', "Users")')
                    ->output('function', 'loadEditModalUser', '/Users/edit-by-admin')
                    ->generate()
                . $this->ModalView
                    ->create('view-webservice-modal')
                    ->modal(__("Visualiser un accès de webservice"))
                    ->output(
                        'function',
                        'actionViewWebservice',
                        '/Webservices/view'
                    )
                    ->generate()
                . $this->ModalForm
                    ->create('edit-webservice-modal')
                    ->modal(__("Modification d'un accès de webservice"))
                    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTableUser . ', "Users")')
                    ->output(
                        'function',
                        'actionEditWebservice',
                        '/Webservices/edit'
                    )
                    ->generate(),
                ['class' => 'separator']
            )
            . $this->Html->tag(
                'header',
                $this->Html->tag('h2.h4', __("Liste des utilisateurs de l'entité"))
                . $this->Html->tag('div.r-actions.h4', $tableUser->getConfigureLink())
            )
            . $tableUser . $paginator . $paginator->scrollTop(),
            ['id' => 'users-sa-section', 'class' => "bg-white"]
        )

        . $this->Form->end();
    ?>
</section>

<!--suppress JSJQueryEfficiency -->
<script>
    $('#tab-entity-sa-container').tabs();
    var entitySaForm = $('#edit-org-entity-sa-form');

    var dataspacesSelect = $('#edit-org-entity-sa-secure-data-spaces-ids');
    AsalaeGlobal.chosen($('#edit-org-entity-type-entity-id'), __("Rechercher"));
    AsalaeGlobal.chosen($('#edit-org-entity-parent-id'), __("Rechercher"));
    AsalaeGlobal.select2(dataspacesSelect);
    AsalaeGlobal.select2($('#edit-org-entity-sa-timestampers-ids'));
    AsalaeGlobal.select2($('#edit-org-entity-sa-ldaps-ids'));
    AsalaeGlobal.select2($('#edit-org-entity-sa-archiving-systems-ids'));

    var initialFormSAValue = '';
    setTimeout(function () {
        initialFormSAValue = entitySaForm.serialize();
    }, 0);
    $('.save-on-change').click(function () {
        var form = $('#edit-org-entity-sa-form');
        var formValue = entitySaForm.serialize();
        if (formValue !== initialFormSAValue
            && confirm(__("Vous avez apporté des modifications au formulaire. Souhaitez-vous les sauvegarder ?"))
        ) {
            entitySaForm.trigger('submit');
        }
        initialFormSAValue = formValue;
    });

    function disableUnselectedDataSpaces() {
        var values = dataspacesSelect.val();
        var defaultSpace = $('#edit-org-entity-sa-default-secure-data-space-id');
        defaultSpace.find('option').each(function () {
            $(this).prop('disabled', values.indexOf($(this).attr('value')) === -1);
        });
        if (values.indexOf(defaultSpace.val()) === -1) {
            defaultSpace.val('');
        }
    }

    dataspacesSelect.change(disableUnselectedDataSpaces);
    disableUnselectedDataSpaces();
</script>
