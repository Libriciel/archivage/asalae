<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

$debug = Configure::read('debug') ? ' checked="checked"' : '';

echo $this->element('jsFormTemplates');

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);
$genericVignette = [
    'class' => 'cursor-pointer vignette bg-white',
    'role' => 'button',
    'tabindex' => '0',
    'onkeydown' => 'handleModalKeypress(event)',
];
$actualiser = $this->Fa->charteBtn(
    'Actualiser',
    $title = __("Actualiser"),
    ['icon' => 'cursor-pointer', 'class' => 'btn-link refresh', 'title' => $title]
);

echo $this->Html->tag(
    'div.container.sr-only',
    $this->Html->tag('h1', __d('admins', "Administration technique"))
);
echo $this->Html->tag('div#probe-div.container', '', ['style' => 'position: relative']);

/**
 * Liste des vignettes
 */
echo $this->Html->tag('div.container.container-flex.check-container');
echo $this->Html->tag('div.row');

/**
 * System check
 */
echo $this->ModalView
    ->create('check-app', ['size' => 'large'])
    ->modal(
        'System check' . ' ' . $actualiser,
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i($system ? 'fa-check' : 'fa-times'))
        . $this->Html->tag('div.text', __d('admins', "System check")),
        '/admin-tenants/ajax-check'
    )
    ->generate(
        ['class' => 'cursor-pointer vignette ' . ($system ? 'success' : 'danger')]
        + $genericVignette
    );

/**
 * Sessions
 */
echo $this->ModalView
    ->create('sessions', ['size' => 'large'])
    ->modal(
        __d('admins', "Sessions actives") . ' ' . $actualiser,
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-link'))
        . $this->Html->tag('div.text', __d('admins', "Sessions actives")),
        '/admin-tenants/index-sessions'
    )
    ->generate($genericVignette);

/**
 * Envoyer une notification
 */
echo $this->ModalForm
    ->create(
        'notify-all',
        [
            'size' => 'large',
            'acceptButton' => $this->Form->button(
                $this->Fa->charte('Envoyer', __("Envoyer")),
                ['bootstrap-type' => 'primary', 'class' => 'accept']
            ),
        ]
    )
    ->modal(__("Envoyer une notification à tous les utilisateurs"))
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-bell'))
        . $this->Html->tag('div.text', __d('admins', "Envoyer une Notification")),
        '/admin-tenants/notify-all'
    )
    ->javascriptCallback('afterNotification')
    ->generate($genericVignette);

/**
 * Etat disques
 */
echo $this->ModalView
    ->create('check-disk', ['size' => 'large'])
    ->modal(
        __("Etat des disques") . ' ' . $actualiser,
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-database'))
        . $this->Html->tag('div.text', __d('admins', "Etat disques")),
        '/admin-tenants/ajax-check-disk'
    )
    ->generate($genericVignette);

/**
 * Volumes
 */
echo $this->ModalView
    ->create('volumes', ['size' => 'large'])
    ->modal(
        __d('admins', "Volumes et espaces de conservation sécurisés"),
        $loading
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-database'))
        . $this->Html->tag('div.text', __d('admins', "Stockages")),
        '/admin-tenants/index-volumes'
    )
    ->generate($genericVignette);

/**
 * Service d'archives
 */
echo $this->ModalView
    ->create('service-archive', ['size' => 'large'])
    ->modal(
        __d('admins', "Services d'Archives") . ' ' . $actualiser,
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-sitemap'))
        . $this->Html->tag('div.text', __d('admins', "Services d'Archives")),
        '/admin-tenants/index-services-archives'
    )
    ->generate($genericVignette);

/**
 * Accès webservices
 */
echo $this->ModalView
    ->create('webservice', ['size' => 'large'])
    ->modal(
        __d('admins', "Accès webservices"),
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-plug'))
        . $this->Html->tag('div.text', __d('admins', "Accès webservices")),
        '/webservices/index'
    )
    ->generate($genericVignette);

/**
 * Jobs
 */
echo $this->ModalView
    ->create('tasks-monitor', ['size' => 'large'])
    ->modal(
        __("Jobs et workers") . ' ' . $actualiser,
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-tasks'))
        . $this->Html->tag('div.text', __d('admins', "Jobs")),
        '/admin-tenants/ajax-tasks'
    )
    ->generate($genericVignette);

/**
 * Logs
 */
echo $this->ModalView
    ->create('view-logs', ['size' => 'large'])
    ->modal('Logs' . ' ' . $actualiser, $loading)
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-file-text'))
        . $this->Html->tag('div.text', __d('admins', "Logs")),
        '/admin-tenants/ajax-logs'
    )
    ->generate($genericVignette);

/**
 * Annuaires LDAP
 */
echo $this->ModalView
    ->create('ldaps', ['size' => 'large'])
    ->modal(
        __d('admins', "Annuaires LDAP/AD"),
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-address-book-o'))
        . $this->Html->tag('div.text', __d('admins', "Annuaires LDAP/AD")),
        '/admin-tenants/index-ldaps'
    )
    ->generate($genericVignette);

/**
 * Systèmes d'Archivage Electronique
 */
echo $this->ModalView
    ->create('archiving-systems', ['size' => 'large'])
    ->modal(
        __d('admins', "Systèmes d'Archivage Electronique"),
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-external-link'))
        . $this->Html->tag('div.text', __d('admins', "Systèmes d'Archivage Electronique")),
        '/archiving-systems/index'
    )
    ->generate($genericVignette);

echo $this->Html->tag('/div');
echo $this->Html->tag('/div');
?>
<script>

    /**
     * les divs se comporte comme un bouton (bouton enter = click)
     */
    function handleModalKeypress(event) {
        if ([13, 32].indexOf(event.keyCode) === -1) {
            return;
        }
        $(event.target).click();
    }

    var checkedVerbs = [];

    function checkVerbs() {
        var checkVerbs = ['put', 'delete', 'patch', 'head'];
        for (var i = 0; i < checkVerbs.length; i++) {
            (function (i) {
                $.ajax({
                    url: '<?=$this->Url->build('/admin-tenants/ping')?>',
                    method: checkVerbs[i],
                    success: function () {
                        checkedVerbs.push(checkVerbs[i]);
                    },
                    error: function () {
                        $('#div-check-app').removeClass('success').addClass('danger')
                            .find('.icon .fa').removeClass('fa-check').addClass('fa-times');
                    }
                });
            })(i);
        }
    }

    checkVerbs();
    $.ajax(
        {
            url: '<?=$this->Url->build('/admin-tenants/ajax-check-volumes')?>',
            headers: {Accept: 'application/json'},
            success: function (content) {
                if (!content.volume_ok) {
                    this.error();
                }
            },
            error: function () {
                $('#div-check-app').removeClass('success').addClass('danger')
                    .find('.icon .fa').removeClass('fa-check').addClass('fa-times');
            }
        }
    );
    $.ajax(
        {
            url: '<?=$this->Url->build('/admin-tenants/ajax-check-tree-sanity')?>',
            headers: {Accept: 'application/json'},
            success: function (content) {
                if (!content.tree_ok) {
                    this.error();
                }
            },
            error: function () {
                $('#div-check-app').removeClass('success').addClass('danger')
                    .find('.icon .fa').removeClass('fa-check').addClass('fa-times');
            }
        }
    );

    /**
     * Utilisé dans index_crons.php
     */
    function parseJsonData(value) {
        if (!value) {
            return;
        }
        var parsed = JSON.parse(value);
        var table = $('<table class="parsed-json-data"></table>');
        var tbody = $('<tbody></tbody>');
        table.append(tbody);
        var tr;
        for (var key in parsed) {
            tbody.append(
                $('<tr></tr>')
                    .append($('<th></th>').append(key + ':'))
                    .append($('<td></td>').append(parsed[key]))
            );
        }
        return table;
    }

    /**
     * Utilisé dans index_volume.php
     */
    function parseArrayData(value) {
        if (!value) {
            return;
        }
        var table = $('<table class="parsed-json-data"></table>');
        var tbody = $('<tbody></tbody>');
        table.append(tbody);
        var tr;
        for (var key in value) {
            tbody.append(
                $('<tr></tr>')
                    .append($('<th></th>').append(key + ':'))
                    .append($('<td></td>').append(value[key]))
            );
        }
        return table;
    }

    /**
     * Donne le nombre de message envoyés
     */
    function afterNotification(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            if (content.count > 1) {
                alert(__("{0} messages ont été envoyés", content.count));
            } else if (content.count === 1) {
                alert(__("1 message a été envoyé"));
            } else {
                alert(__("aucun message n'a été envoyé"));
            }
        } else {
            alert(PHP.messages.genericError);
        }
    }

    /**
     * Calcule la taille que prend un texte (word) dans un contexte (element)
     * @var {string} word
     * @var {HTMLElement|jQuery|string} element
     */
    function wordSizeCalculator(word, element) {
        var div = $('<div></div>').css({
            "position": 'absolute',
            "visibility": 'hidden',
            "height": 'auto',
            "width": 'auto',
            "white-space": 'nowrap'
        }).append(word);
        $(element).append(div);
        var size = {
            width: div.outerWidth(),
            height: div.outerHeight()
        };
        div.remove();
        return size;
    }

    /**
     * Formate les textes des vignettes pour une meilleure apparence
     */
    $('.vignette .text:not(.no-reformat)').each(function () {
        var words = $(this).text().trim().split(' ');
        var max = 0;
        var current = 0;
        var widths = [];
        var currentWidth;
        var small = $('<small></small>');
        var width;
        var br = false;
        var space = false;

        if (words.length === 1) {
            if (wordSizeCalculator(words[0], this).width > $(this).width()) {
                $(this).html(small.append(words[0]));
            }
            return;
        }
        for (var i = 0; i < words.length; i++) {
            currentWidth = wordSizeCalculator(words[i], this).width;
            widths.push(currentWidth);
            max = Math.max(max, currentWidth);
        }
        for (i = 0; i < words.length; i++) {
            if (br) {
                br = false;
                small.append('<br>');
                current = 0;
            } else if (i > 0) {
                small.append(' ');
            }
            width = widths.shift();
            current += width;
            if (current >= max) {
                if (i > 0) {
                    small.append('<br>');
                    current = 0;
                } else {
                    br = true;
                }
            }
            small.append(words[i]);
        }
        if (max > $(this).width()) {
            $(this).html(small);
        } else {
            $(this).html(small.html());
        }
    });

    function probe() {
        if (probePaused) {
            return;
        }
        $.ajax({
            url: '<?=$this->Url->build('/admin-tenants/probe')?>',
            success: function (content) {
                $('#probe-div').html(content);
                refreshProbeIcon = $('#refresh-probe').find('i.fa');
            }
        });
    }

    var probePaused = false;
    probe();
    probePaused = <?=Configure::read('Admins.probePaused') ? 'true' : 'false'?>;
    var probeInterval = setInterval(probe, 5000);
    var refreshProbeIcon = $('#refresh-probe').find('i.fa');
    setTimeout(
        function () {
            refreshProbeIcon
                .toggleClass('fa-pause', !probePaused)
                .toggleClass('fa-play', probePaused)
        },
        0
    );

    function toogleProbeRefresh() {
        refreshProbeIcon
            .toggleClass('fa-pause', probePaused)
            .toggleClass('fa-play', !probePaused);
        probePaused = !probePaused;
    }

    var ctrlCount = 0;
    $(document).on('keydown', function (event) {
        if (event.keyCode === 17) { // Ctrl
            ctrlCount++;
        } else {
            ctrlCount = 0;
            return;
        }
        if (ctrlCount === 3) {
            toogleProbeRefresh();
        }
    });

    var ratchet = [];
    $('.modal').on('hide.bs.modal', function () {
        var keys = [];
        var i, j;
        for (i = 0; i < ratchet.length; i++) {
            keys = Object.keys(ratchet[i]._subscriptions);
            for (j = 0; j < keys.length; j++) {
                try {
                    ratchet[i].unsubscribe(keys[j]);
                } catch (e) {
                }
            }
        }
        ratchet = [];
    });

    function notifyMe(btn) {
        var inputs = $(btn).closest('form').serializeObject();
        notifications.session.publish(
            'user_' + notifications.user_id + '_' + notifications.token,
            {
                category: 'user_' + notifications.user_id + '_' + notifications.token,
                message: {
                    id: 0,
                    user_id: 'admin',
                    text: '<h4>' + __("Message de l'administrateur") + '</h4>' + inputs.msg,
                    css_class: inputs.color,
                    created: Date.now()
                }
            }
        );
    }
</script>
