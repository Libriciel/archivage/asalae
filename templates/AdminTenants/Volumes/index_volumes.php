<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->element('modal', ['idTable' => $tableIdVolumes, 'paginate' => false]);
echo $this->element('modal', ['idTable' => $tableIdSpaces, 'paginate' => false]);

$view = $this;
$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

echo $this->ModalView
    ->create('view-volume', ['size' => 'large'])
    ->modal(__d('admins', "Volume"), $loading)
    ->output('function', 'viewVolume', '/admin-tenants/viewVolume')
    ->generate();

echo $this->ModalView
    ->create('browse-volume', ['size' => 'modal-xxl'])
    ->modal(__d('admins', "Parcourir"), $loading)
    ->output('function', 'browseVolume', '/admin-tenants/volumeClient')
    ->generate();

$tableVolumes = $this->Table
    ->create($tableIdVolumes, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Volumes.id' => [
                'label' => __("ID"),
                'display' => false,
            ],
            'Volumes.name' => ['label' => __("Nom")],
            'Volumes.drivertrad' => ['label' => __("Driver")],
            'Volumes.fields_object' => [
                'label' => __("Champs"),
                'callback' => 'parseArrayData',
            ],
            'Volumes.description' => [
                'label' => __("Description"),
                'display' => false,
            ],
            'Volumes.active' => [
                'label' => __("Actif"),
                'type' => 'boolean',
            ],
            'Volumes.date_begin' => [
                'label' => __("Date de début"),
                'display' => false,
                'type' => 'date',
            ],
            'Volumes.date_end' => [
                'label' => __("Date de fin"),
                'display' => false,
                'type' => 'date',
            ],
            'Volumes.alert_rate' => [
                'label' => __("Alerte de taux d'occupation"),
                'display' => false,
            ],
            'Volumes.disk_usage' => [
                'label' => __("Occupation disque"),
                'display' => false,
                'callback' => 'TableHelper.readableBytes',
            ],
            'Volumes.max_disk_usage' => [
                'label' => __("Occupation disque max"),
                'display' => false,
                'callback' => 'TableHelper.readableBytes',
            ],
            'Volumes.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
            ],
            'Volumes.modified' => [
                'label' => __("Date de modification"),
                'type' => 'datetime',
                'display' => false,
            ],
        ]
    )
    ->data($volumes)
    ->params(
        [
            'identifier' => 'Volumes.id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewVolume({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Voir {0}", '{1}'),
                'aria-label' => __("Voir {0}", '{1}'),
                'params' => ['Volumes.id', 'Volumes.name'],
            ],
            [
                'onclick' => "actionTestVolume({0}, $(this), {})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-calendar-check-o'),
                'title' => __("Tester {0}", '{1}'),
                'aria-label' => __("Tester {0}", '{1}'),
                'params' => ['Volumes.id', 'Volumes.name'],
            ],
            [
                'onclick' => "browseVolume({0})",
                'type' => "button",
                'class' => 'btn-link',
                'data-action' => __("Parcourir"),
                'label' => $this->Fa->i('fa-list'),
                'title' => __("Parcourir {0}", '{1}'),
                'aria-label' => __("Parcourir {0}", '{1}'),
                'params' => ['Volumes.id', 'Volumes.name'],
            ],
            function ($table) {
                return [
                    'data-callback' => sprintf(
                        "TableGenericAction.deleteAction(%s, '%s')('{0}', false)",
                        $table->tableObject,
                        $this->Url->build('/admin-tenants/deleteVolume')
                    ),
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'data-action' => __("Supprimer"),
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'confirm' => __("Êtes-vous sûr de vouloir supprimer ce volume ?"),
                    'displayEval' => "data[{index}].Volumes.deletable",
                    'params' => ['Volumes.id', 'Volumes.name'],
                ];
            },
        ]
    );

$tableSpaces = $this->Table
    ->create($tableIdSpaces, ['class' => 'table table-striped table-hover'])
    ->fields(
        [
            'SecureDataSpaces.id' => [
                'label' => __("ID"),
            ],
            'SecureDataSpaces.name' => [
                'label' => __("Nom"),
            ],
            'SecureDataSpaces.volumes' => [
                'label' => __("Volumes"),
                'thead' => [
                    'name' => ['label' => __("Nom")],
                    'drivertrad' => ['label' => __("driver")],
                ],
            ],
            'SecureDataSpaces.org_entity.name' => [
                'label' => __("Service d'archives"),
            ],

            'SecureDataSpaces.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
            ],
            'SecureDataSpaces.modified' => [
                'label' => __("Date de modification"),
                'type' => 'datetime',
                'display' => false,
            ],
        ]
    )
    ->data($spaces)
    ->params(
        [
            'identifier' => 'SecureDataSpaces.id',
        ]
    );

echo $this->Html->tag('section#tab-volumes-container.row.no-padding');

echo // Tabulations
    $this->Html->tag(
        'ul',
        $this->Html->tag(
            'li',
            $this->Html->tag(
                'a',
                $this->Fa->i('fa-database')
                . ' ' . __("Volumes"),
                ['href' => '#volumes-section']
            )
        )
        . $this->Html->tag(
            'li',
            $this->Html->tag(
                'a',
                $this->Fa->i('fa-cube')
                . ' ' . __("espaces de conservation sécurisés"),
                ['href' => '#spaces-section']
            )
        )
    )

    // Tab volumes
    . $this->Html->tag(
        'section#volumes-section.bg-white',
        $this->Html->tag(
            'header',
            $this->Html->tag('h2.h4', __("Liste des volumes"))
            . $this->Html->tag('div.r-actions.h4', $tableVolumes->getConfigureLink())
        )
        . $tableVolumes->generate()
    )

    // Tab espaces de conservation sécurisés
    . $this->Html->tag(
        'section#spaces-section.bg-white',
        $this->Html->tag(
            'header',
            $this->Html->tag('h2.h4', __("Liste des espaces de conservation sécurisés"))
            . $this->Html->tag('div.r-actions.h4', $tableSpaces->getConfigureLink())
        )
        . $tableSpaces->generate()
    );

echo $this->Html->tag('/section');

echo $this->Html->tag('script', "$('#tab-volumes-container').tabs();");
?>
<script>
    function actionTestVolume(id, btn, data) {
        var form = $(btn).closest('form');
        if (form.length && !form.get(0).checkValidity()) {
            form.get(0).reportValidity();
            return;
        }
        if (!confirm(
            __(
                "Cette action va tenter d'écrire un fichier sur le volume, "
                + "le lire, puis le supprimer. Voulez-vous continuer ?"
            )
        )
        ) {
            return;
        }
        btn.disable();

        AsalaeLoading.start();
        $.ajax({
            url: '<?=$this->Url->build('/admin-tenants/testVolume')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            data: data,
            success: function (content) {
                AsalaeLoading.stop();
                btn.enable();
                if (content.success) {
                    alert(__("OK - Le volume a été vérifié avec succès"));
                } else {
                    var errorMsgs = [];
                    if (content.exception) {
                        errorMsgs.push('Exception: ' + content.exception);
                    }
                    if (content.errors && Array.isArray(content.errors)) {
                        for (var i = 0; i < content.errors.length; i++) {
                            errorMsgs.push(content.errors[i].description);
                        }
                    }
                    alert(__("Une erreur a eu lieu: {0}", errorMsgs.join(",\n")));
                }
            },
            error: function () {
                AsalaeLoading.stop();
                btn.enable();
                alert(PHP.messages.genericError);
            }
        });
    }

    function alertCron(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            alert(__("Un cron a été ajouté, vous pouvez le visualiser dans la vignette Tâches planifiées."));
        }
    }

    function afterAddSpace(content, textStatus, jqXHR) {
        $('.container-flash > .alert-danger').remove();
        TableGenericAction.afterAdd(<?=$tableSpaces->tableObject?>, "SecureDataSpaces")(content, textStatus, jqXHR);
    }

    var deleteVolumeFilesEnabled = false;

    function enableDeleteVolumeFiles() {
        deleteVolumeFilesEnabled = true;
    }
</script>
