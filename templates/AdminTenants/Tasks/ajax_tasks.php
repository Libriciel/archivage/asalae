<script>
    // evite l'appel multiple de reloadThisModal durant la même seconde
    var lastReload = Math.floor(Date.now() / 1000); // timestamp (seconds)
    function reloadThisModal() {
        var time = Math.floor(Date.now() / 1000);
        if (time > lastReload) {
            lastReload = time;
            $('#tasks-monitor.modal:visible').find('button.refresh').click();
        }
    }

    function indexJobsTube(value) {
        return $('<button type="button" class="btn-link"></button>')
            .text(value)
            .attr('title', __("Liste des jobs du tube {0}", value))
            .on('click', function () {
                viewJobsTube(value);
            });
    }

    function indexJobsState(state) {
        return function (value, context) {
            return $('<button type="button" class="btn-link"></button>')
                .text(value)
                .attr('title', __("Liste des jobs {0} du tube {1}", state, context.tube))
                .on('click', function () {
                    viewJobsTube(context.tube + '?job_state[]=' + state);
                });
        }
    }

    function workerWithWatchers(value, context) {
        var watchers = parseInt(context['current-waiting'], 10);
        return value === watchers
            ? value
            : value + ' <span class="text-danger" title="' + __("{0} en BDD ; {1} en attente", value, watchers)
            + '">(' + watchers + ')</span>';
    }
</script>
<?php
/**
 * @var Asalae\View\AppView $this
 * @var Cake\ORM\ResultSet  $workers
 */

$view = $this;
$actualiser = $this->Fa->charteBtn(
    'Actualiser',
    $title = __("Actualiser"),
    ['icon' => 'cursor-pointer', 'class' => 'btn-link refresh', 'title' => $title]
);
$jsTable = $this->Table->getJsTableObject($tableId = 'jobs-tube');

echo $this->ModalView
    ->create('view-worker-log', ['size' => 'large'])
    ->modal(__("Affichage des logs d'un worker"))
    ->output(
        'function',
        'viewWorkerLog',
        '/admin-tenants/worker-log'
    )
    ->generate();

echo $this->ModalView
    ->create('view-jobs-tube', ['size' => 'modal-xxl'])
    ->modal(__("Affichage des jobs d'un tube") . ' ' . $actualiser)
    ->output(
        'function',
        'viewJobsTube',
        '/admin-tenants/index-jobs'
    )
    ->generate();

$tabs = $this->Tabs->create('tabs-tasks', ['class' => 'no-padding']);

/**
 * Jobs tab
 */
$tabs->add(
    'tab-tasks-jobs',
    $this->Fa->i('fa-tasks', __("Jobs")),
    require 'jobs_tab.php',
    ['div' => ['class' => 'no-padding']]
);

/**
 * Workers tab
 */
$tabs->add(
    'tab-tasks-workers',
    $this->Fa->i('fa-wrench', __("Workers")),
    require 'workers_tab.php',
    ['div' => ['class' => 'no-padding']]
);

echo $tabs;
?>
<!--suppress JSUnusedAssignment -->
<script>
    /**
     *
     * @param {string} url
     * @param {string} tube
     * @param {TableGenerator} tableGenerator
     */
    function ajaxAction(url, tube, tableGenerator) {
        var tr = tableGenerator.table.find('tr[data-id="' + tube + '"]').first();
        $('html').addClass('ajax-loading');
        tr.addClass('warning').removeClass('error').removeClass('success');
        $.ajax({
            url: url,
            headers: {Accept: 'application/json'},
            success: function (content, textStatus, jqXHR) {
                if (typeof content === 'object' && typeof content.success === 'boolean') {
                    tr.removeClass('warning');
                    if (content.success) {
                        if (typeof content.tube === 'string') {
                            tr = tr.parent().find('tr').filter(function () {
                                return $(this).find('td.tube').text().trim() === content.tube;
                            });
                        }
                        tr.addClass('success');
                    } else {
                        tr.addClass('error');
                        alert(content.error);
                    }
                }
                switch (jqXHR.getResponseHeader('X-Asalae-Success')) {
                    case 'true':
                        reloadThisModal();
                        break;
                    case 'false':
                        tr.addClass('danger').removeClass('warning');
                        break;
                }
            },
            error: function () {
                tr.addClass('danger').removeClass('warning');
                alert(PHP.messages.genericError);
            },
            complete: function () {
                $('html').removeClass('ajax-loading');
            }
        });
    }

</script>
