<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\ORM\ResultSet  $workers
 */

$workerTable = $this->Table
    ->create('workers')
    ->fields(
        [
            'name' => ['label' => __d('tasks', "Name")],
            'tube' => ['label' => __d('tasks', "Tube"), 'class' => 'tube'],
            'last_launch' => ['label' => __d('tasks', "Dernier lancement"), 'type' => 'datetime'],
            'hostname' => ['label' => 'Hostname'],
            'pid' => ['label' => 'Pid'],
        ]
    )
    ->data($workers->toArray())
    ->params(
        [
            'identifier' => 'id',
            'classEval' => "!data[{index}].running ? 'danger' : ''",
        ]
    )
    ->actions(
        [
            [
                'type' => "button",
                'onclick' => 'viewWorkerLog({0})',
                'label' => $view->Fa->i('fa-file-text'),
                'class' => 'btn-link',
                'title' => $title = __d('tasks', "Afficher les logs"),
                'aria-label' => $title,
                'params' => ['id', 'name'],
            ],
        ]
    )
    ->generate();
return $workerTable;
