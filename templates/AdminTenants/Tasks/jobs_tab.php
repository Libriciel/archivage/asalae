<?php

/**
 * @var Asalae\View\AppView $this
 */

use AsalaeCore\View\Helper\Object\Table;

$jsTable = $this->Table->getJsTableObject('all-tube');

$jobTable = $this->Html->tag('div.modal-header');
$jobTable .= $this->Html->tag('h3.h4', __d('tasks', "Gestion des jobs"));
$jobTable .= $this->ModalForm
    ->create('tasks-add-form')
    ->modal(__("Nouveau Job"))
    ->javascriptCallback('reloadThisModal')
    ->output('button', $this->Fa->charte('Ajouter', __("Ajouter un job")), '/Tasks/add')
    ->generate(['class' => 'btn btn-success', 'type' => 'button']);
$jobTable .= $this->Html->tag('/div');

$table = $this->Table
    ->create('all-tube', ['class' => 'table table-striped table-hover',])
    ->fields(
        [
            'tube' => [
                'label' => __d('tasks', "Tube"),
                'callback' => 'indexJobsTube',
            ],
            'workers' => [
                'label' => __d('tasks', "Workers"),
                'callback' => 'workerWithWatchers',
            ],
            'current-jobs-urgent' => ['display' => false],
            'current-jobs-ready' => [
                'label' => __d('tasks', "Prêts"),
                'callback' => 'indexJobsState("ready")',
                'class' => 'job-ready',
            ],
            'current-jobs-reserved' => [
                'label' => __d('tasks', "En cours"),
                'callback' => 'indexJobsState("reserved")',
                'class' => 'job-reserved',
            ],
            'current-jobs-delayed' => [
                'label' => __d('tasks', "Prévu"),
                'callback' => 'indexJobsState("delayed")',
            ],
            'current-jobs-buried' => [
                'label' => __d('tasks', "Echecs"),
                'callback' => 'indexJobsState("buried")',
                'class' => 'job-buried',
            ],
            'total-jobs' => ['display' => false],
            'current-using' => ['display' => false],
            'current-watching' => ['display' => false],
            'current-waiting' => ['display' => false],
            'cmd-delete' => ['display' => false],
            'cmd-pause-tube' => ['display' => false],
            'pause' => ['display' => false],
            'pause-time-left' => ['display' => false],
        ]
    )
    ->data($tubes)
    ->params(
        [
            'identifier' => 'tube',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            function (Table $table) use ($view) {
                return [
                    'type' => "button",
                    'class' => "btn-link",
                    'data-callback' => sprintf(
                        "ajaxAction('%s/{0}', '{0}', %s)",
                        $view->Url->build('/admin-tenants/resumeAllTube'),
                        $table->tableObject
                    ),
                    'confirm' => __d(
                        'tasks',
                        "Tous les jobs en échecs seront relancés. Voulez-vous continuer ?"
                    ),
                    'label' => $view->Fa->i('fas fa-fast-forward text-success'),
                    'title' => $title = __d('tasks', "Relancer tous les jobs"),
                    'displayEval' => 'data[{index}]["current-jobs-buried"] > 0',
                    'aria-label' => $title,
                    'params' => ['tube'],
                ];
            },
        ]
    );

$jobTable .= $table;
return $jobTable;
