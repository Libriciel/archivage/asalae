<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

?>
    <div style="color: #4c575f">
        <h4 style="margin: 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5">
            <?= __("Des jobs liés à votre activité sont en erreurs") ?>
        </h4>

        <?php

        foreach ($jobErrors as $jobError) {
            echo $this->Html->tag('div');
            echo $this->Html->tag(
                'h5',
                __("Job sur worker {0} ({1}) :", h($jobError['worker']), h($jobError['object'])),
                ['style' => 'margin: 20px 0 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5']
            );
            echo $this->Html->tag('p', __("Erreur : {0}", nl2br(h($jobError['error'] ?? ''))));
            echo $this->Html->tag('/div');
        }

        ?>
    </div>
<?php

/** @noinspection HtmlDeprecatedAttribute */
$this->assign(
    'after-signature',
    '<tr><td style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td>'
    . '<td style="color: #4c575f; font-family: arial, sans-serif; padding-top: 5px;' .
    ' border-top: 1px solid #d9d9d9;" bgcolor="#f1f1f1" align="center">'
    . __(
        "Vous recevez ce mail car votre profil utilisateur indique que vous "
        . "pouvez recevoir des notifications pour les jobs en erreur."
    )
    . '<br/>'
    . $this->Html->link(
        __("Se désabonner"),
        trim(Configure::read('App.fullBaseUrl'), '/ ') . '/auth-urls/activate/' . $code->get('code')
    )
    . '</td><td style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td></tr>'
);
