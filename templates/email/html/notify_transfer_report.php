<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;
use Cake\I18n\Number;

?>
    <div style="color: #4c575f">
        <h4 style="margin: 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5">
            <?= __(
                "Récapitulatif des transferts entrants pour les {0} jours précédents :",
                $user->get('notify_transfer_report_frequency')
            ) ?>
        </h4>

        <ul>
            <li><?= __(
                "transferts acceptés : {0} transferts pour {1}",
                $report['accepted']['count'],
                Number::toReadableSize($report['accepted']['size'])
            ) ?></li>
            <li><?= __(
                "transferts rejetés : {0} transferts pour {1}",
                $report['rejected']['count'],
                Number::toReadableSize($report['rejected']['size'])
            ) ?></li>
            <li><?= __(
                "transferts en cours de validation : {0} transferts pour {1}",
                $report['validating']['count'],
                Number::toReadableSize($report['validating']['size'])
            ) ?></li>
        </ul>
    </div>
<?php

/** @noinspection HtmlDeprecatedAttribute */
$this->assign(
    'after-signature',
    '<tr><td style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td>'
    . '<td style="color: #4c575f; font-family: arial, sans-serif; padding-top: 5px;' .
    ' border-top: 1px solid #d9d9d9;" bgcolor="#f1f1f1" align="center">'
    . __(
        "Vous recevez ce mail car votre profil utilisateur indique que vous "
        . "pouvez recevoir des notifications pour le récapitulatif des transferts entrants."
    )
    . '<br/>'
    . $this->Html->link(
        __("Se désabonner"),
        trim(Configure::read('App.fullBaseUrl'), '/ ') . '/auth-urls/activate/' . $code->get('code')
    )
    . '</td><td style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td></tr>'
);
