<?php

/**
 * @var Asalae\View\AppView $this
 * @var string              $filename
 * @var string              $link
 */

use Cake\Core\Configure;

?>
<div style="color: #4c575f">
    <h4 style="margin: 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5">
        <?= __("Le fichier {0} est prêt à être téléchargé", $filename) ?>
    </h4>
    <p><?= __(
        "Vous avez émis une demande de téléchargement de fichier {0}, en voici le lien de téléchargement :",
        'PDF'
    ) ?></p>

    <p><?= $this->Html->link(
        __("Télécharger le pdf"),
        trim(Configure::read('App.fullBaseUrl'), '/ ') . $link
    ) ?></p>
</div>
