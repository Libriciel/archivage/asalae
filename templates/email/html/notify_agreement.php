<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

?>
    <div style="color: #4c575f">
        <h4 style="margin: 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5">
            <?= __("Aucun transfert entrant") ?>
        </h4>

        <p>
            <?= __(
                "Aucun transfert pour l'accord de versement ''{0}'' ({1}) n'a été effectué "
                . "pendant les {2} jours précédents.",
                $agreement->get('name'),
                $agreement->get('identifier'),
                $agreement->get('notify_delay'),
            ) ?>
        </p>
    </div>
<?php

/** @noinspection HtmlDeprecatedAttribute */
$this->assign(
    'after-signature',
    '<tr><td style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td>'
    . '<td style="color: #4c575f; font-family: arial, sans-serif; padding-top: 5px;' .
    ' border-top: 1px solid #d9d9d9;" bgcolor="#f1f1f1" align="center">'
    . __(
        "Vous recevez ce mail car votre profil utilisateur indique que vous "
        . "pouvez recevoir des notifications pour les accords de versement."
    )
    . '<br/>'
    . $this->Html->link(
        __("Se désabonner"),
        trim(Configure::read('App.fullBaseUrl'), '/ ') . '/auth-urls/activate/' . $code->get('code')
    )
    . '</td><td style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td></tr>'
);
