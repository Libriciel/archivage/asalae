<?php

/**
 * @var Asalae\View\AppView             $this
 * @var Cake\Datasource\EntityInterface $code
 */

use Cake\Core\Configure;

?>
<div style="color: #4c575f">
    <h4 style="margin: 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5">
        <?= __("Vous êtes invité à prendre une décision sur asalae") ?>
    </h4>
    <p><?= __("Une demande de transfert sortant nécessite une prise de décision") ?></p>

    <ul>
        <li><b><?= __("Date") ?>:</b> <?= $outgoingTransferRequest->get('created') ?></li>
        <li><b><?= __("Identifiant") ?>:</b> <?= $outgoingTransferRequest->get('identifier') ?></li>
        <li><b><?= __("Etape de validation") ?>:</b> <?= $stage->get('name') ?></li>
    </ul>
    <p><?= __("Afin de vous connecter à l'application, il vous faudra cliquer sur le lien ci-dessous.") ?></p>

    <p><?= $this->Html->link(
        __("Lien de prise de décision"),
        trim(Configure::read('App.fullBaseUrl'), '/ ') . '/auth-urls/activate/' . $code->get('code')
    ) ?></p>

    <p><?= $this->Html->link(
        __("Accéder à la plateforme"),
        Configure::read('App.fullBaseUrl')
    ) ?></p>
</div>
