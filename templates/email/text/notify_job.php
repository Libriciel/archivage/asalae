<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

?>
<?= __("Des jobs liés à votre activité sont en erreurs") ?>

<?php

foreach ($jobErrors as $jobError) {
    echo __("Job sur worker {0} ({1}) :", h($jobError['worker']), h($jobError['object'])) . "\n";
    echo __("Erreur : {0}", h($jobError['error'])) . "\n\n";
}

?>

<?php
/** @noinspection HtmlDeprecatedAttribute */
$this->assign(
    'after-signature',
    __(
        "Vous recevez ce mail car votre profil utilisateur indique que vous "
        . "pouvez recevoir des notifications pour les jobs en erreur."
    )
    . "\n"
    . __("Se désabonner") . ' : '
    . $this->Html->link(
        __("Se désabonner"),
        trim(Configure::read('App.fullBaseUrl'), '/ ')
        . '/auth-urls/activate/' . $code->get('code')
    )
);
