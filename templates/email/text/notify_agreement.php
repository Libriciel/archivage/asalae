<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

?>
<?= __("Aucun transfert entrant") ?>

<?= __(
    "Aucun transfert pour l'accord de versement ''{0}'' ({1}) n'a été effectué "
    . "pendant les {2} jours précédents.",
    $agreement->get('name'),
    $agreement->get('identifier'),
    $agreement->get('notify_delay'),
) ?>

<?php
/** @noinspection HtmlDeprecatedAttribute */
$this->assign(
    'after-signature',
    __(
        "Vous recevez ce mail car votre profil utilisateur indique que vous "
        . "pouvez recevoir des notifications pour les accords de versement."
    )
    . "\n"
    . __("Se désabonner") . ' : '
    . trim(Configure::read('App.fullBaseUrl'), '/ ')
    . '/auth-urls/activate/' . $code->get('code')
);
