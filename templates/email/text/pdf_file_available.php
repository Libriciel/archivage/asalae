<?php

/**
 * @var Asalae\View\AppView $this
 * @var string              $filename
 * @var string              $link
 */

use Cake\Core\Configure;

?>
<?= __("Le fichier {0} est prêt à être téléchargé", $filename) ?>

<?= __(
    "Vous avez émis une demande de téléchargement de fichier {0}, en voici le lien de téléchargement :",
    'PDF'
) ?>

<?= trim(Configure::read('App.fullBaseUrl'), '/ ') . $link;
