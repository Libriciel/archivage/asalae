<?php

/**
 * @var Asalae\View\AppView             $this
 * @var Cake\Datasource\EntityInterface $code
 */

use Cake\Core\Configure;

?>
<?= __("Vous êtes invité à prendre une décision sur asalae") ?>
<?= __("Une demande de restitution nécessite une prise de décision") ?>

<?= __("Date") ?>: <?= $restitutionRequest->get('created') ?>
<?= __("Identifiant") ?>: <?= $restitutionRequest->get('identifier') ?>
<?= __("Etape de validation") ?>: <?= $stage->get('name') ?>

<?= __("Afin de vous connecter à l'application, il vous faudra cliquer sur le lien ci-dessous.") ?>

<?= trim(Configure::read('App.fullBaseUrl'), '/ ') . '/auth-urls/activate/' . $code->get('code') ?>

<?= Configure::read('App.fullBaseUrl') ?>
