<?php

/**
 * @var Asalae\View\AppView             $this
 * @var Cake\Datasource\EntityInterface $code
 */

use Cake\Core\Configure;

?>
<?= __("Vous êtes invité à prendre une décision sur asalae") ?>
<?= __("Un transfert nécessite une prise de décision") ?>

<?= __("Date") ?>: <?= $transfer->get('transfer_date') ?>
<?= __("Identifiant") ?>: <?= $transfer->get('transfer_identifier') ?>
<?= __("Est conforme ?") ?>: <?= $transfer->get('is_conform') ? __("Oui") : __("Non") ?>
<?= __("Etape de validation") ?>: <?= $stage->get('name') ?>

<?= __("Afin de vous connecter à l'application, il vous faudra cliquer sur le lien ci-dessous.") ?>

<?= trim(Configure::read('App.fullBaseUrl'), '/ ') . '/auth-urls/activate/' . $code->get('code') ?>

<?= Configure::read('App.fullBaseUrl') ?>
