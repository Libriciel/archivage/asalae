<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;
use Cake\I18n\Number;

?>
<?= __(
    "Récapitulatif des transferts entrants pour les {0} jours précédents :",
    $user->get('notify_transfer_report_frequency')
) ?>

<?=
__(
    "transferts acceptés : {0} transferts pour {1}",
    $report['accepted']['count'],
    Number::toReadableSize($report['accepted']['size'])
)
?>
<?=
__(
    "transferts rejetés : {0} transferts pour {1}",
    $report['rejected']['count'],
    Number::toReadableSize($report['rejected']['size'])
)
?>
<?=
__(
    "transferts en cours de validation : {0} transferts pour {1}",
    $report['validating']['count'],
    Number::toReadableSize($report['validating']['size'])
)
?>

<?php
/** @noinspection HtmlDeprecatedAttribute */
$this->assign(
    'after-signature',
    __(
        "Vous recevez ce mail car votre profil utilisateur indique que vous "
        . "pouvez recevoir des notifications pour les jobs en erreur."
    )
    . "\n"
    . __("Se désabonner") . ' : '
    . $this->Html->link(
        __("Se désabonner"),
        trim(Configure::read('App.fullBaseUrl'), '/ ')
        . '/auth-urls/activate/' . $code->get('code')
    )
);
