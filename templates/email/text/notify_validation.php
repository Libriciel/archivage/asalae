<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Core\Configure;

?>
<?= __("Vous êtes invité à vous connecter à Asalae pour valider des {0}.", $text) ?>

<?= __("Retrouverez la liste des éléments à valider en cliquant sur le lien suivant :") ?>

<?= __("Liste des éléments à valider") ?> : <?= $this->Url->build($url, ['fullBaseUrl' => true]) ?>
<?php
/** @noinspection HtmlDeprecatedAttribute */
$this->assign(
    'after-signature',
    __(
        "Vous recevez ce mail car votre profil utilisateur indique que vous "
        . "pouvez recevoir des notifications pour les validations."
    )
    . "\n"
    . __("Se désabonner") . ' : '
    . $this->Html->link(
        __("Se désabonner"),
        trim(Configure::read('App.fullBaseUrl'), '/ ')
        . '/auth-urls/activate/' . $code->get('code')
    )
);
