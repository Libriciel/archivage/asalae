<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Restitutions"));
$this->Breadcrumbs->add($h1 = __("Récupération des restitutions"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-mail-reply', $h1))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

echo $this->element('modal', ['idTable' => $tableId]);

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}

echo $this->ModalView
    ->create('view-restitution', ['size' => 'modal-xxl'])
    ->modal(__("Visualisation d'une demande de restitution"))
    ->output('function', 'actionViewRestitution', '/restitutions/view')
    ->generate();

echo $this->Filter->create($tableId . '-filter')
    ->saves($savedFilters)
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->filter(
        'identifier',
        [
            'label' => __("Identifiant"),
            'wildcard',
        ]
    )
    ->filter(
        'state',
        [
            'label' => __("Etats"),
            'options' => $states,
            'multiple' => true,
        ]
    )
    ->filter(
        'original_size[value]',
        [
            'label' => __("Taille totale des fichiers"),
            'prepend' => $this->Input->operator(
                'original_size[operator]',
                '>=',
                ['id' => 'original-total-size-operator']
            ),
            'append' => $this->Input->mult(
                'original_size[mult]',
                'mo',
                ['id' => 'original-total-size-mult']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'original_count[value]',
        [
            'label' => __("Nombre de fichiers"),
            'prepend' => $this->Input->operator(
                'original_count[operator]',
                '>=',
                ['id' => 'original-total-count-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->generateSection();

require 'ajax_retrieval.php';
?>
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    function changeRestitutionState(id, table) {
        setTimeout(
            function () {
                AsalaeLoading.ajax(
                    {
                        url: '<?=$this->Url->build('/restitutions/get-info')?>/' + id,
                        headers: {
                            Accept: 'application/json'
                        },
                        success: function (content) {
                            content.Restitutions = content;
                            table.replaceDataId(id, content);
                            table.generateAll();
                        }
                    }
                );
            },
            100
        );
    }
</script>
