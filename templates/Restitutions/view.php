<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $restitution
 */

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($restitution->get('identifier')));
$infos .= $this->Html->tag('p', h($restitution->get('comment')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Demande de restitution"));
$infos .= $this->ViewTable->generate(
    $restitution,
    [
        __("Identifiant") => 'identifier',
        __("Etat") => 'statetrad',
        __("Commentaire") => 'comment|nl2br',
        __("Entité producteur") => 'restitution_request.originating_agency.name',
        __("Agent demandeur") => 'restitution_request.created_user.username',
        __("Nombre d'unités d'archives") => 'restitution_request.archive_units_count',
        __("Fichiers d'unités d'archives") => 'restitution_request.original_count',
        __("Taille des unités d'archives")
        => '{restitution_request.original_size|toReadableSize} ({restitution_request.original_size} Octets)',
        __("Date de création") => 'created',
        __("Date de modification") => 'modified',
    ]
);
$infos .= $this->Html->tag('/article');

$tabs = $this->Tabs->create('tabs-view-restitution', ['class' => 'row no-padding']);
$tabs->add(
    'tab-view-restitution-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);

$tableUnits = $this->Table
    ->create('view-restitution-archive-units', ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'restitution-requests',
            'action' => 'paginate-archive-units',
            $restitution->get('restitution_request_id'),
            '?' => [
                'sort' => 'name',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'archival_agency_identifier' => [
                'label' => __("Identifiant"),
            ],
            'name' => [
                'label' => __("Nom"),
            ],
            'description' => [
                'label' => __("Description"),
            ],
            'dates' => [
                'label' => __("Dates extrêmes"),
            ],
            'original_total_count' => [
                'label' => __("Nombre de fichiers"),
            ],
            'original_total_size' => [
                'label' => __("Taille des fichiers"),
                'callback' => 'TableHelper.readableBytes',
            ],
        ]
    )
    ->data(Hash::get($restitution, 'restitution_request.archive_units', []))
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewUnitDescription({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/description'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'href' => $this->Url->build('/archive-units/display-xml') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/display-xml'),
                'displayEval' => '!data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
        ]
    );
$tabs->add(
    'tab-view-restitution-archive-units',
    $this->Fa->i('fa-file-code-o', __("Unités d'archives")),
    $this->AjaxPaginator->create('pagination-restitution-archive-units')
        ->url($url)
        ->table($tableUnits)
        ->count($unitsCount)
        ->generateTable()
);

echo $tabs;
