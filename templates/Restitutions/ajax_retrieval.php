<?php

/**
 * @var Asalae\View\AppView $this
 */

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Restitutions.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'Restitutions.comment' => [
                'label' => __("Commentaire"),
            ],
            'Restitutions.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'Restitutions.restitution_request.archive_units' => [
                'label' => __("Unités d'archives"),
                'callback' => 'TableHelper.ul("name")',
                'escape' => false,
                'filter' => [
                    'archive_unit_identifier[0]' => [
                        'id' => 'filter-archive_unit_identifier-0',
                        'label' => __("Identifiant"),
                    ],
                    'archive_unit_name[0]' => [
                        'id' => 'filter-archive_unit_name-0',
                        'label' => __("Nom"),
                    ],
                ],
            ],
            'Restitutions.statetrad' => [
                'label' => __("Etat"),
                'filter' => [
                    'state[0]' => [
                        'id' => 'filter-state-0',
                        'label' => false,
                        'aria-label' => __("Etat"),
                        'options' => $states,
                    ],
                ],
            ],
            'Restitutions.original_count' => [
                'label' => __("Nombre de fichiers"),
                'display' => false,
                'order' => 'original_count',
                'filter' => [
                    'original_count[0][value]' => [
                        'id' => 'filter-original_count-0',
                        'label' => __("Nombre de fichiers"),
                        'prepend' => $this->Input->operator(
                            'original_count[0][operator]',
                            '>=',
                            ['id' => 'filter-original_count-0-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'original_count[1][value]' => [
                        'id' => 'filter-original_count-1',
                        'label' => false,
                        'aria-label' => __("Quantité 2"),
                        'prepend' => $this->Input->operator(
                            'original_count[1][operator]',
                            '<=',
                            ['id' => 'filter-original_count-1-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
            'Restitutions.original_size' => [
                'label' => __("Taille totale des fichiers"),
                'callback' => 'TableHelper.readableBytes',
                'filter' => [
                    'original_size[0][value]' => [
                        'id' => 'filter-original_size-0',
                        'label' => __("Taille totale des fichiers"),
                        'prepend' => $this->Input->operator(
                            'original_size[0][operator]',
                            '>=',
                            ['id' => 'filter-original_size-0-operator']
                        ),
                        'append' => $this->Input->mult(
                            'original_size[0][mult]',
                            'mo',
                            ['id' => 'filter-original_size-0-mult']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'original_size[1][value]' => [
                        'id' => 'filter-original_size-1',
                        'label' => false,
                        'aria-label' => __("Taille 2"),
                        'prepend' => $this->Input->operator(
                            'original_size[1][operator]',
                            '<=',
                            ['id' => 'filter-original_size-1-operator']
                        ),
                        'append' => $this->Input->mult(
                            'original_size[1][mult]',
                            'mo',
                            ['id' => 'filter-original_size-1-mult']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'Restitutions.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewRestitution({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/Restitutions/view'),
                'params' => ['id', 'identifier'],
            ],
            [
                'href' => "/restitutions/download/{0}",
                'download' => '{1}',
                'target' => '_blank',
                'onclick' => sprintf(
                    "changeRestitutionState('{0}', %s)",
                    $jsTable
                ),
                'label' => $this->Fa->charte('Télécharger'),
                'title' => $title = __("Télécharger {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/restitutions/download'),
                'params' => ['id', 'basename'],
            ],
        ]
    );

$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des restitutions"),
        'table' => $table,
    ]
);
