<?php

/**
 * @var Asalae\View\AppView $this
 */
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Restitutions"));
$this->Breadcrumbs->add($h1 = __("Acquittement des restitutions"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-thumbs-up', $h1))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

echo $this->element('modal', ['idTable' => $tableId]);

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}

echo $this->ModalView
    ->create('view-restitution', ['size' => 'modal-xxl'])
    ->modal(__("Visualisation d'une demande de restitution"))
    ->output('function', 'actionViewRestitution', '/restitutions/view')
    ->generate();

echo $this->Filter->create($tableId . '-filter')
    ->saves($savedFilters)
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->filter(
        'identifier',
        [
            'label' => __("Identifiant"),
            'wildcard',
        ]
    )
    ->filter(
        'state',
        [
            'label' => __("Etats"),
            'options' => $states,
            'multiple' => true,
        ]
    )
    ->filter(
        'original_size[value]',
        [
            'label' => __("Taille totale des fichiers"),
            'prepend' => $this->Input->operator(
                'original_size[operator]',
                '>=',
                ['id' => 'original-total-size-operator']
            ),
            'append' => $this->Input->mult(
                'original_size[mult]',
                'mo',
                ['id' => 'original-total-size-mult']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'original_count[value]',
        [
            'label' => __("Nombre de fichiers"),
            'prepend' => $this->Input->operator(
                'original_count[operator]',
                '>=',
                ['id' => 'original-total-count-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->generateSection();

require 'ajax_acquittal.php';
?>
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    function acquitRestitution(id) {
        var table = {};
        table = <?=$jsTable?>;
        var tr = $(table.table).find('tr[data-id="' + id + '"]');
        tr.find('button').addClass('deleting').addClass('gray').addClass('disabled').disable();
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/restitutions/acquit')?>/' + id,
            method: 'DELETE',
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                if (content.report === 'done') {
                    table.removeDataId(id);
                    tr.fadeOut(400, function () {
                        $(this).remove();
                        table.decrementPaginator();
                    });
                } else {
                    alert(content.report);
                    this.error();
                }
            },
            error: function (e) {
                console.error(e);
                tr.addClass('danger');
            }
        });
    }
</script>
