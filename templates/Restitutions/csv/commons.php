<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$map = function (EntityInterface $entity): EntityInterface {
    $archiveUnits = [];
    /** @var EntityInterface $restitutionRequest */
    $restitutionRequest = Hash::get($entity, 'restitution_request');
    foreach (Hash::get($restitutionRequest, 'archive_units', []) as $archiveUnit) {
        $archiveUnits[] = Hash::get($archiveUnit, 'name');
    }
    $restitutionRequest->set(
        'archive_units',
        $archiveUnits ? '- ' . implode("\n- ", $archiveUnits) : ''
    );
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'identifier' => __("Identifiant"),
        'comment' => __("Commentaire"),
        'created' => __("Date de création"),
        'restitution_request.archive_units' => __("Unités d'archives"),
        'statetrad' => __("Etat"),
        'original_count' => __("Nombre de fichiers"),
        'original_size_readable' => __("Taille totale des fichiers"),
        'original_size' => __("Taille totale des fichiers (octet)"),
    ],
    $results,
    $map
);
