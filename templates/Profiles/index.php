<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Profils d'archives"));
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-file-code-o', __("Profils d'archives")))
    . $this->Breadcrumbs->render()
);

$jsTable = $this->Table->getJsTableObject($tableId);
$buttons = [];
if ($this->Acl->check('/profiles/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-profile-modal', ['size' => 'large'])
        ->modal(__("Ajout d'un profil d'archives"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "Profiles")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un profil d'archives")),
            '/Profiles/add'
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalForm
    ->create('edit-profile', ['size' => 'large'])
    ->modal(__("Modifier un profil d'archives"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "Profiles")')
    ->output(
        'function',
        'editProfile',
        '/Profiles/edit'
    )
    ->generate();

echo $this->ModalView
    ->create('view-profile')
    ->modal(__("Visualiser un profil d'archives"))
    ->output(
        'function',
        'actionViewProfile',
        '/Profiles/view'
    )
    ->generate();

echo $this->Filter->create('profiles-filter')
    ->saves($savedFilters)
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'identifier',
        [
            'label' => __("Identifiant"),
            'wildcard',
        ]
    )
    ->filter(
        'active',
        [
            'label' => __("Actifs"),
            'options' => [
                '1' => __("Oui"),
                '0' => __("Non"),
            ],
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'modified',
        [
            'label' => __("Date de modification"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->generateSection();

require 'ajax_index.php';
?>
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#profiles-section');
    });
</script>
