<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $entity
 */

use Cake\Datasource\EntityInterface;

$map = function (EntityInterface $entity): EntityInterface {
    $filenames = [];
    foreach ($entity->get('fileuploads') as $fileupload) {
        $filenames[] = $fileupload->get('name');
    }
    $entity->set('fileuploads', $filenames ? '- ' . implode("\n- ", $filenames) : '');
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'name' => __("Nom"),
        'identifier' => __("Identifiant"),
        'fileuploads' => __("Schemas"),
        'active' => __("Actif"),
        'created' => __("Date de création"),
        'modified' => __("Date de modification"),
    ],
    $results,
    $map
);
