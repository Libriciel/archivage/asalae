<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Cake\Utility\Hash;

echo $this->Html->tag('article');
echo $this->Html->tag('header.bottom-space');
echo $this->Html->tag('h3', h($entity->get('name')));
echo $this->Html->tag('p', h($entity->get('description')));
echo $this->Html->tag('/header');

echo $this->Html->tag('table.table.table-striped.table-hover.fixed-left-th');
echo $this->Html->tag('tbody');
echo $this->Html->tag(
    'tr',
    $this->Html->tag('th', __("Nom"))
        . $this->Html->tag('td', h($entity->get('name')))
)
    . $this->Html->tag(
        'tr',
        $this->Html->tag('th', __("Identifiant"))
        . $this->Html->tag('td', h($entity->get('identifier')))
    )
    . $this->Html->tag(
        'tr',
        $this->Html->tag('th', __("Description"))
        . $this->Html->tag('td', h($entity->get('description')))
    )
    . $this->Html->tag(
        'tr',
        $this->Html->tag('th', __("Activé"))
        . $this->Html->tag('td', $entity->get('active') ? __("Oui") : __("Non"))
    )
    . $this->Html->tag(
        'tr',
        $this->Html->tag('th', __("Date de création"))
        . $this->Html->tag('td', h($entity->get('created')))
    )
    . $this->Html->tag(
        'tr',
        $this->Html->tag('th', __("Date de modification"))
        . $this->Html->tag('td', h($entity->get('modified')))
    );
echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');
foreach ((array)$entity->get('fileuploads') as $key => $file) {
    $link = $this->Html->link(
        $filename = h(Hash::get($entity, "fileuploads.$key.name")),
        $this->Url->build('/download/file/' . Hash::get($entity, "fileuploads.$key.id")),
        ['target' => '_blank', 'download']
    );
    echo $this->Html->tag('table.table.table-striped.table-hover.fixed-left-th');
    echo $this->Html->tag('thead');
    echo $this->Html->tag(
        'tr',
        $this->Html->tag('th', __("Schéma du profil d'archivage"), ['colspan' => 2])
    );
    echo $this->Html->tag('/thead');
    echo $this->Html->tag('tbody')
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Nom"))
            . $this->Html->tag('td', $link)
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Taille"))
            . $this->Html->tag('td', $this->Number->toReadableSize(h(Hash::get($entity, "fileuploads.$key.size"))))
        );
    echo $this->Html->tag('/tbody');
    echo $this->Html->tag('/table');
}

echo $this->Html->tag('/article');
