<?php

/**
 * @var Asalae\View\AppView $this
 */
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Profiles.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'Profiles.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'Profiles.fileuploads' => [
                'label' => __("Schemas"),
                'callback' => 'TableHelper.ul("name")',
            ],
            'Profiles.active' => [
                'label' => __("Actif"),
                'type' => 'boolean',
                'callback' => 'TableHelper.boolean',
                'filter' => [
                    'active[0]' => [
                        'id' => 'filter-active-0',
                        'label' => false,
                        'aria-label' => __("Actif"),
                        'options' => [
                            '1' => __("Oui"),
                            '0' => __("Non"),
                        ],
                    ],
                ],
            ],
            'Profiles.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'Profiles.modified' => [
                'label' => __("Date de modification"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'modified',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'modified[0]' => [
                        'id' => 'filter-modified-0',
                        'label' => __("Date de modification"),
                        'prepend' => $this->Input->operator('dateoperator_modified[0]', '>='),
                        'append' => $this->Date->picker('#filter-modified-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'modified[1]' => [
                        'id' => 'filter-modified-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_modified[1]', '<='),
                        'append' => $this->Date->picker('#filter-modified-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'Profiles.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewProfile({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/profiles/view'),
                'params' => ['Profiles.id', 'Profiles.name'],
            ],
            [
                'onclick' => "editProfile({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/profiles/edit'),
                'params' => ['Profiles.id', 'Profiles.name'],
            ],
            function ($table) {
                $view = $this;
                $deleteUrl = $view->Url->build('/Profiles/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction({$table->tableObject}, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => $title = __("Supprimer {0}", '{1}'),
                    'aria-label' => $title,
                    'display' => $this->Acl->check('/profiles/delete'),
                    'displayEval' => 'data[{index}].deletable',
                    'params' => ['Profiles.id', 'Profiles.name'],
                ];
            },
        ]
    );

// @see src/Template/Profiles/index.php
$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => 'profiles-section',
        'title' => __("Liste des profils d'archives"),
        'table' => $table,
    ]
);
