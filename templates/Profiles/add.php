<?php

/**
 * @var Asalae\View\AppView $this
 */
$formId = 'form-add-profile';
echo $this->Form->create($entity, ['id' => $formId, 'idPrefix' => 'add-profile']);
$uploadBoxId = 'add-upload-profile-box';
$uploadId = 'add-upload-profile';
require 'addedit-common.php';
echo $this->Form->end();
