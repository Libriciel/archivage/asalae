<?php
/**
 * @var Asalae\View\AppView $this
 */
?>
<script>
    function renderExt(value, context) {
        if (typeof context.name === 'undefined') {
            return null;
        }
        var ext = context.name.split('.').pop().toLowerCase();
        return '<div class="prev-ext"><i class="fa fa-file-o" aria-hidden="true"></i><span>' + ext + '</span></div>';
    }

    function selectableUpload(value, context) {
        return '<input type="checkbox" name="fileuploads[_ids][]" value="' + context.id + '" checked="checked">';
    }
</script>
<?php
$uploads = $this->Upload
    ->create($uploadId, ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'select' => ['label' => __("Sélectionner"), 'class' => 'radio-td', 'callback' => 'selectableUpload'],
            'ext' => ['label' => false, 'callback' => 'renderExt'],
            'name' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
            'message' => ['label' => __("Message"), 'class' => 'message', 'style' => 'min-width: 250px'],
            'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
        ]
    )
    ->data($uploadData ?? [])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    );
$jsUpload = $uploads->jsObject;
$jsTableUpload = $this->Upload->getTableId($uploadId);

echo $this->Form->control('name', ['label' => __("Nom")])
    . $this->Form->control(
        'identifier',
        ['label' => __("Identifiant")]
        + ($entity->get('editable_identifier')
            ? []
            : [
                'readonly' => true,
                'title' => __(
                    "Le profil est utilisé par un ou des transferts " .
                    "non rejetés et ne peut donc être modifié"
                ),
            ]
        )
    )
    . $this->Form->control('description', ['label' => __("Description")])
    . $this->Form->control('active', ['label' => __("Actif (autorise son utilisation pour les transferts entrants)")])
    . '<hr>'
    . $this->Html->tag('span', __("Schéma du profil d'archivage"), ['class' => 'fake-label'])
    . $uploads->generate(
        [
            'attributes' => ['accept' => '.xsd, .rng, application/xml'],
            'target' => $this->Url->build('/Upload/index/profile?replace=true'),
            'autoretry' => false,
        ]
    );
?>

<script>
    var uploadTable = {table: ''};
    uploadTable.table = '#<?=$jsTableUpload?>';
    var uploader = {config: {id: ''}};
    uploader = <?=$jsUpload?>;

    $(uploadTable.table).change(function () {
        if ($(this).find('tbody tr').length === 0) {
            $(this).addClass('hide')
                .closest('section')
                .find('button.btn-primary')
                .parent()
                .addClass('hide');
        }
    });

    setTimeout(function () {
        $('div.dropbox').off('.draganimation').on('dragover.draganimation', function () {
            $(this).addClass('dragover');
        }).on('dragleave.draganimation dragend.draganimation drop.draganimation', function () {
            $(this).removeClass('dragover');
        });
    }, 0);
    uploader.uploader.on('fileAdded', function () {
        $(uploadTable.table).removeClass('hide').get(0).scrollIntoView();
    });
    // Note: uploader.uploader est de type Flow pas jQuery, on ne chaine pas les on()
    uploader.uploader.on('fileSuccess', function (file, message) {
        if (typeof message === 'string') {
            try {
                message = JSON.parse(message);
            } catch (e) {
                console.error("can't JSON.parse message: " + message);
                throw e;
            }
        }
        var table = eval(uploader.config.table);
        if (message.report.error) {
            table.data.splice(table.getDataId(file.uniqueIdentifier), 1);
            return;
        }
        if (!AsalaeGlobal.is_numeric(message.report.id)) {
            return;
        }
        setTimeout(function () {
            table.table.find('tr[data-id="' + message.report.id + '"] input[type="checkbox"]').prop('checked', true);
            fileuploadsCheckboxes = $('#<?=$formId?>').find('input[name="fileuploads[_ids][]"]');
        }, 0);

    });
    if ($(uploadTable.table).find('tbody tr').length >= 1) {
        $(uploadTable.table).removeClass('hide');
    }

    var fileuploadsCheckboxes = $('#<?=$formId?>').find('input[name="fileuploads[_ids][]"]');
</script>
