<?php

/**
 * @var Asalae\View\AppView $this
 */
$formId = 'form-edit-profile';
echo $this->Form->create($entity, ['id' => $formId, 'idPrefix' => 'edit-profile']);
$uploadBoxId = 'edit-upload-profile-box';
$uploadId = 'edit-upload-profile';
require 'addedit-common.php';
echo $this->Form->end();
