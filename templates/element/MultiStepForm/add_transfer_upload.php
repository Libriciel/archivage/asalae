<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->create(
    [
        __("Upload du bordereau"),
        __("Ajout des fichiers"),
        __("Editeur de bordereau"),
    ],
    $step
);
