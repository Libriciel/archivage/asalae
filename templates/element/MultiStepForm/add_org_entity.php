<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->create(
    [
        __("Création d'une entité"),
        __("Notice d'autorité"),
    ],
    $step
);
