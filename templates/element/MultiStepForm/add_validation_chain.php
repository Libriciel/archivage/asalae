<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->create(
    [
        __("Création du circuit de validation"),
        __("Ajout d'une première étape"),
        __("Ajout d'un premier acteur de validation"),
    ],
    $step
);
