<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->create(
    [
        __("Informations"),
        __("Autorisations"),
        __("Contrôles"),
        __("Notifications"),
    ],
    $step
);
