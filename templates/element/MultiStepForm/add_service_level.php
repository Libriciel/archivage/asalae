<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->create(
    [
        __("Informations"),
        __("Politique d'horodatage"),
        __("Politique de conversion"),
    ],
    $step
);
