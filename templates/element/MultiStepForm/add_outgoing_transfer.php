<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->create(
    [
        __("Sélection du SAE"),
        __("Formulaire"),
        __("Unités d'archives"),
    ],
    $step
);
