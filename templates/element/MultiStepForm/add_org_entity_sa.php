<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->create(
    [
        __("Création du service d'archives"),
        __("Notice d'autorité"),
        __("Premier utilisateur"),
    ],
    $step
);
