<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->create(
    [
        __("Sélection du traitement"),
        __("Unités d'archives"),
        __("Récapitulatif"),
    ],
    $step
);
