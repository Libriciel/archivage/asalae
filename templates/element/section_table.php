<?php

/**
 * @var Asalae\View\AppView $this
 * @var string $id identifiant de la section
 * @var string $title Contenu du h2
 * @var AsalaeCore\View\Helper\Object\Table $table
 * @var array $downloadCsv
 * @var bool $paginate affiche ou non la pagination (true par défaut)
 */

use Cake\Utility\Inflector;

echo $this->Html->tag(
    'div',
    null,
    ['id' => $id, 'class' => $class ?? null]
);
echo $this->Html->tag(
    'section',
    null,
    ['class' => 'container bg-white paginable-section']
);

/**
 * header
 */
echo $this->Html->tag('header');
echo $this->Html->tag('h4', $title);
echo $this->Html->tag('div', null, ['class' => 'l-actions h4']);
echo $this->Fa->charteBtn(
    'Filtrer',
    __("Réinitialiser les filtres de recherche"),
    [
        'class' => 'btn-link display-active-filter',
        'text' => $this->Fa->i('fa-times fa-small-append fa-hidden-append text-danger'),
    ]
);
echo $this->Fa->button(
    'fa-sort-amount-asc',
    __("Retirer le tri"),
    [
        'class' => 'btn-link display-active-sort',
        'text' => $this->Fa->i('fa-times fa-small-append fa-hidden-append text-danger'),
    ]
);
echo $this->Html->tag('/div');
$options = $pagination['options'] ?? [];
$url = $options['url'] ?? [];
unset($options['url']);
if (empty($options['model'])) {
    if (is_array($url) && !isset($url['controller'])) {
        $url['controller'] = $this->getRequest()->getParam('controller');
    }
    $model = is_array($url)
        ? $url['controller']
        : substr(trim($url, '/'), 0, strpos(trim($url, '/'), '/'));
    $model = Inflector::camelize($model, '-');
} else {
    $model = $options['model'];
}
if ($paginate ?? true) {
    $paginationUp = $this->Html->tag(
        'ul',
        $this->Paginator->prev(
            '<i class="fa fa-chevron-left" aria-hidden="true"></i><span class="sr-only">'
            . __("Previous")
            . '</span>',
            ['escape' => false, 'url' => $url]
        )
        . $this->Paginator->numbers(
            [
                'url' => $url,
                'first' => 1,
                'last' => 1,
                'modulus' => 2,
            ]
        )
        . $this->Paginator->next(
            '<i class="fa fa-chevron-right" aria-hidden="true"></i><span class="sr-only">' . __("Next") . '</span>',
            ['escape' => false, 'url' => $url]
        ),
        ['class' => 'pagination pagination-sm m-0']
    );
    echo $this->Html->tag(
        'div.results',
        $this->Html->tag(
            'small',
            $this->Paginator->counter(
                __("{0} résultats", '<i class="count">{{count}}</i>'),
            ),
            ['class' => 'text-muted pagination-counters']
        )
    );
} else {
    $paginationUp = null;
}
$configure = $this->Html->tag('div.configure-link', $table->getConfigureLink(['class' => 'btn-link']));

echo $this->Html->tag('div', $paginationUp . $configure, ['class' => 'r-actions h4']);

echo $this->Html->tag('/header');

/**
 * Body
 */
echo $table->generate();

if ($paginate ?? true) {
    echo $this->Html->tag('div.row');
    $this->Paginator->setConfig(
        'numbers',
        [
            'first' => 1,
            'last' => 1,
            'modulus' => 2,
        ]
    );
    echo $this->Paginator->blocPagination(
        $pagination['base-id'] ?? 'grouped-actions',
        $pagination['options'] ?? []
    );
    echo $this->Html->tag('/div');
}

echo $this->Html->tag('/section');

if (!empty($downloadEad) || !empty($downloadCsv)) {
    echo $this->Html->tag('section.container.text-right');
    if (!empty($downloadEad)) {
        echo $this->Html->tag(
            'a.btn.btn-primary',
            $this->Fa->charte('Télécharger', __("Télécharger au format EAD 2002")),
            [
                'download' => $downloadEad['download'],
                'href' => $downloadEad['href'],
                'target' => '_blank',
            ]
        );
    }
    if (!empty($downloadCsv)) {
        echo $this->Html->tag(
            'a.btn.btn-primary',
            $this->Fa->charte('Télécharger', __("Télécharger au format CSV")),
            [
                'download' => $downloadCsv['download'],
                'href' => $downloadCsv['href'],
                'target' => '_blank',
            ]
        );
    }
    echo $this->Html->tag('/section');
}

if (!empty($pagination['callback'])) {
    echo $this->Html->tag('script', '$(function() { ' . $pagination['callback'] . '(); });');
}

echo $this->Html->tag('/div');
