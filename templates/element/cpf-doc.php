<?php
/**
 * Export de la documentation depuis http://eac.staatsbibliothek-berlin.de/fileadmin/user_upload/schema/cpfTagLibrary.html
 * phpcs:ignoreFile
 */
?>
<div data-id="abbreviation">
    <h3>abbreviation</h3>
    <h4><?= __d("cpf-doc", "Abbreviation") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The &lt;abbreviation&gt; element contains an abbreviation or code for
 identifying a thesaurus, controlled vocabulary, or other standard used in formulating and
 compiling the EAC-CPF description."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;abbreviation&gt; element contains an abbreviation or code for identifying a
 thesaurus, controlled vocabulary, or other standard used in formulating and compiling the
 EAC-CPF description. It is recommended that the value be selected from an authorized list of
 codes. An example of such a list may be the MARC Code List (http://www.loc.gov/marc/sourcelist/)."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[token]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "conventionDeclaration, localTypeDeclaration") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;conventionDeclaration&gt;
                    &lt;abbreviation&gt;RICA&lt;/abbreviation&gt;

                    &lt;citation&gt;RICA (Regole italiane di catalogazione per autore)&lt;/citation&gt;
                &lt;/conventionDeclaration&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="abstract">
    <h3>abstract</h3>
    <h4><?= __d("cpf-doc", "Abstract") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element that contains a brief summary of the information contained within
 the &lt;biogHist&gt; as a whole."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;abstract&gt; is a brief synopsis of the identity’s biography or history
 that is often based on the longer descriptions found in &lt;biogHist&gt;. Its
 purpose is to help readers quickly identify the identity described in the EAC-CPF instance.
 The content within this element may also be harvested by other systems to provide
 explanatory context for the &lt;nameEntry&gt; data when it appears in a set of search
 results."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text], span") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "biogHist") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;biogHist&gt;
                    &lt;abstract&gt;Hubert H. Humphrey was born in Wallace, South Dakota (1911). He  was elected Mayor of Minneapolis in 1945 and served until
                        1948.  In November of 1948, he was elected to the United
                        States Senate and he also served as the Senate Democratic Whip from 1961 to 1964 and in 1968, Humphrey was
                        the Democratic
                        Party&#039;s candidate for President, but he was defeated by Richard M. Nixon. &lt;/abstract&gt;

                    &lt;p&gt;Hubert H. Humphrey was born in Wallace, South Dakota, on May 27,
                        1911. He left South Dakota to attend the University of Minnesota but
                        returned to South Dakota to help manage his father’s drug store early
                        in the depression. He attended the Capitol College of Pharmacy in
                        Denver, Colorado, and became a register pharmacist in 1933. On
                        September 3, 1936, Humphrey married Muriel Fay Buck. He returned
                        to the University of Minnesota and earned a B.A. degree in 1939. In
                        1940 he earned an M.A. in political science from Louisiana State
                        University and returned to Minneapolis to teach and pursue further
                        graduate study, he began working for the W.P.A. (Works Progress
                        Administration). He moved on from there to a series of positions with
                        wartime agencies. In 1943, he ran unsuccessfully for Mayor of
                        Minneapolis and returned to teaching as a visiting professor at
                        Macalester College in St. Paul. Between 1943 and 1945 Humphrey
                        worked at a variety of jobs. In 1945, he was elected Mayor of
                        Minneapolis and served until 1948. In 1948, at the Democratic
                        National Convention, he gained national attention when he delivered a
                        stirring speech in favor of a strong civil rights plank in the party’s
                        platform. In November of 1948, Humphrey was elected to the United
                        States Senate. He served as the Senate Democratic Whip from 1961 to
                        1964.&lt;/p&gt;

                    &lt;p&gt;In 1964, at the Democratic National Convention, President Lyndon B.
                        Johnson asked the convention to select Humphrey as the Vice
                        Presidential nominee. The ticket was elected in November in a
                        Democratic landslide. In 1968, Humphrey was the Democratic
                        Party’s candidate for President, but he was defeated narrowly by
                        Richard M. Nixon. After the defeat, Humphrey returned to Minnesota
                        to teach at the University of Minnesota and Macalester College. He
                        returned to the U.S. Senate in 1971, and he won re-election in 1976.
                        He died January 13, 1978 of cancer.&lt;/p&gt;
                &lt;/biogHist&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="address">
    <h3>address</h3>
    <h4><?= __d("cpf-doc", "Address") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A postal or other address."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "&lt;address&gt; is a wrapper element within &lt;place&gt;. It contains one or more
 &lt;addressLine&gt; elements that together comprise full or sufficient information
 identifying a postal or other address related to the entity being described."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "addressLine") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "place") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.3</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;place&gt;
                    &lt;placeRole&gt;siege social&lt;/placeRole&gt;

                    &lt;address&gt;
                        &lt;addressLine localType=&quot;voie&quot;&gt;2 rue Corneille&lt;/addressLine&gt;

                        &lt;addressLine localType=&quot;city&quot;&gt;Paris&lt;/addressLine&gt;

                        &lt;addressLine localType=&quot;postalcode&quot;&gt;75006&lt;/addressLine&gt;

                        &lt;addressLine&gt;France&lt;/addressLine&gt;
                    &lt;/address&gt;
                &lt;/place&gt;

                &lt;place&gt;
                    &lt;placeRole&gt;headquarters&lt;/placeRole&gt;

                    &lt;address&gt;
                        &lt;addressLine&gt;221 Kifissias Avenue&lt;/addressLine&gt;

                        &lt;addressLine&gt;Marousi&lt;/addressLine&gt;

                        &lt;addressLine&gt;15124&lt;/addressLine&gt;

                        &lt;addressLine&gt;Greece&lt;/addressLine&gt;
                    &lt;/address&gt;
                &lt;/place&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="addressLine">
    <h3>addressLine</h3>
    <h4><?= __d("cpf-doc", "Address Line") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 One line of a postal or other address"
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A required element in &lt;address&gt;, the &lt;addressLine&gt; element is used to encode
 one line of a postal or other address. &lt;addressLine&gt; may be repeated for each line of the address."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "address") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.3</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;place&gt;
                    &lt;placeRole&gt;siege social&lt;/placeRole&gt;

                    &lt;address&gt;
                        &lt;addressLine localType=&quot;voie&quot;&gt;2 rue Corneille&lt;/addressLine&gt;

                        &lt;addressLine localType=&quot;city&quot;&gt;Paris&lt;/addressLine&gt;

                        &lt;addressLine localType=&quot;postalcode&quot;&gt;75006&lt;/addressLine&gt;

                        &lt;addressLine&gt;France&lt;/addressLine&gt;
                    &lt;/address&gt;
                &lt;/place&gt;


                &lt;place&gt;
                    &lt;placeRole&gt;headquarters&lt;/placeRole&gt;

                    &lt;address&gt;
                        &lt;addressLine&gt;221 Kifissias Avenue&lt;/addressLine&gt;

                        &lt;addressLine&gt;Marousi&lt;/addressLine&gt;

                        &lt;addressLine&gt;15124&lt;/addressLine&gt;

                        &lt;addressLine&gt;Greece&lt;/addressLine&gt;
                    &lt;/address&gt;
                &lt;/place&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="agencyCode">
    <h3>agencyCode</h3>
    <h4><?= __d("cpf-doc", "Agency Code") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The code that represents the institution or service responsible for the
 creation, maintenance and/or dissemination of the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An element of &lt;maintenanceAgency&gt; within &lt;control&gt; providing a code
 representing the institution or service responsible for the creation, maintenance and/or
 dissemination of the EAC-CPF instance. The name of the agency is given in
 &lt;agencyName&gt;. The code is used in combination with the content of the required
 &lt;recordId&gt; to provide a globally unique identifier for the instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The format of the code is constrained to that of the International Standard Identifier for
 Libraries and Related Organizations (ISIL: ISO 15511): a prefix, a dash, and an identifier.
 The code is alphanumeric (A-Z, 0-9, solidus, hyphen-minus, and colon only) with a maximum of
 16 characters. If appropriate to local or national convention insert a valid ISIL for an
 institution, whether provided by a national authority (usually the national library) or a
 service (such as OCLC). If this is not the case then local institution codes may be given
 with the ISO 3166-1 alpha-2 country code as the prefix to ensure international uniqueness in
 the &lt;agencyCode&gt; element."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "maintenanceAgency") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.2</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;maintenanceAgency&gt;
                    &lt;agencyCode&gt;AU-ANL:PEAU&lt;/agencyCode&gt;

                    &lt;agencyName&gt;National Library of Australia&lt;/agencyName&gt;
                &lt;/maintenanceAgency&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="agencyName">
    <h3>agencyName</h3>
    <h4><?= __d("cpf-doc", "Agency Name") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The name of the institution or service responsible for the creation,
 maintenance, and/or dissemination of the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A mandatory element of &lt;maintenanceAgency&gt; within &lt;control&gt; that
 provides the name of the institution or service responsible for the creation, maintenance
 and/or dissemination of the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "It is recommended that one uses the form of the agency name that is authorized by an
 appropriate national or international agency or service."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "maintenanceAgency") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.2</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;maintenanceAgency&gt;
                    &lt;agencyCode&gt;AU-ANL:PEAU&lt;/agencyCode&gt;

                    &lt;agencyName&gt;National Library of Australia&lt;/agencyName&gt;
                &lt;/maintenanceAgency&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="agent">
    <h3>agent</h3>
    <h4><?= __d("cpf-doc", "Agent") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The agent (human or machine) responsible for an event in the maintenance of
 the EAC instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "For each maintenance event described in a &lt;maintenanceEvent&gt; element, the name of
 the agent responsible for the maintenance event must be given. This might be a person or
 institution in which case the &lt;agentType&gt; should be set as &quot;human,&quot; or the name of
 a system, in which case set the &lt;agentType&gt; to &quot;machine.&quot;"
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "maintenanceEvent") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.9</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;created&lt;/eventType&gt;

                    &lt;eventDateTime&gt;20/06/2000 12:00&lt;/eventDateTime&gt;

                    &lt;agentType&gt;human&lt;/agentType&gt;

                    &lt;agent&gt;Bountouri, Lina&lt;/agent&gt;
                &lt;/maintenanceEvent&gt;

                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;updated&lt;/eventType&gt;
June 1, 2012
                    &lt;agentType&gt;human&lt;/agentType&gt;

                    &lt;agent&gt;Ionian University Library, Catalog Division&lt;/agent&gt;
                &lt;/maintenanceEvent&gt;

                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;revised&lt;/eventType&gt;

                    &lt;eventDateTime&gt;2012-06-11 10:00AM&lt;/eventDateTime&gt;

                    &lt;agentType&gt;machine&lt;/agentType&gt;

                    &lt;agent&gt;ConvertUtility&lt;/agent&gt;
                &lt;/maintenanceEvent&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="agentType">
    <h3>agentType</h3>
    <h4><?= __d("cpf-doc", "Agent Type") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The type of agent responsible for a maintenance event of the EAC-CPF
 instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "For each maintenance event described in a &lt;maintenanceEvent&gt; element, the type of
 agent given in the &lt;agent&gt; element must be given as either &quot;human&quot; or
 &quot;machine.&quot;"
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "&quot;human&quot; or &quot;machine&quot;") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "maintenanceEvent") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.9</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;created&lt;/eventType&gt;

                    &lt;eventDateTime&gt;20/06/2000 12:00&lt;/eventDateTime&gt;

                    &lt;agentType&gt;human&lt;/agentType&gt;

                    &lt;agent&gt;Bountouri, Lina&lt;/agent&gt;
                &lt;/maintenanceEvent&gt;

                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;updated&lt;/eventType&gt;
June 1, 2012
                    &lt;agentType&gt;human&lt;/agentType&gt;

                    &lt;agent&gt;Ionian University Library, Catalog Division&lt;/agent&gt;
                &lt;/maintenanceEvent&gt;

                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;revised&lt;/eventType&gt;

                    &lt;eventDateTime&gt;2012-06-11 10:00AM&lt;/eventDateTime&gt;

                    &lt;agentType&gt;machine&lt;/agentType&gt;

                    &lt;agent&gt;ConvertUtility&lt;/agent&gt;
                &lt;/maintenanceEvent&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="alternativeForm">
    <h3>alternativeForm</h3>
    <h4><?= __d("cpf-doc", "Alternative Form") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Qualifies the name contained in &lt;nameEntry&gt; or set of names
 contained in &lt;nameEntryParallel&gt; as alternative or variant forms."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The name of an EAC-CPF entity, as formed within the &lt;nameEntry&gt; or
 &lt;nameEntryParallel&gt; elements, may be the authorized form of the name according to
 a particular set of rules or conventions, or it may be an alternative or unauthorized form
 according to a different set of rules. This optional element provides the opportunity to
 indicate one or more sets of rules or conventions under which the form of the name expressed
 in &lt;nameEntry&gt; or &lt;nameEntryParallel&gt; would be regarded as an alternative
 or unauthorized form."
        ) ?></p>
    <p><?= __d("cpf-doc", "The eac-cpf schema offers two possibilities:") ?></p>
    <p><?= __d(
            "cpf-doc",
            "1. &lt;alternativeForm&gt; is used within &lt;nameEntry&gt; only when
 &lt;nameEntry&gt; is not included within &lt;nameEntryParallel&gt;. In this case, it
 qualifies the form of the name recorded within the precise &lt;nameEntry&gt; element as a
 variant form of the name, as compared to other &lt;nameEntry&gt; elements which are deemed as
 authorized ones."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "2. &lt;alternativeForm&gt; may be used within &lt;nameEntryParallel&gt; to indicate
 that the set of parallel names recorded in separate &lt;nameEntry&gt; elements within
 &lt;nameEntryParallel&gt; are deemed as variant forms of the name."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The content of the &lt;alternativeForm&gt; is an abbreviation selected from a constrained set of values,
 each of which represents a set of national, international or other rules that govern the
 construction of EAC-CPF names in those environments. The abbreviations expressed in
 &lt;alternativeForm&gt; must be declared within the &lt;conventionDeclaration&gt;
 element in &lt;control&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The parallel element &lt;authorizedForm&gt; permits one to indicate rules or
 conventions according to which the name is the authorized form. The element
 &lt;preferredForm&gt; permits one to indicate that the name as expressed is the
 preferred form in the encoder&#039;s local context, regardless of its authorized status in any
 other name authority environment."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "&lt;alternativeForm&gt; may be repeated in case a single or a set of
 &lt;nameEntry&gt; element(s) may conform to more than one rule."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "NMTOKEN") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "nameEntry, nameEntryParallel") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.1.5</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;nameEntry&gt;
                    &lt;part&gt;Brown, Bob, 1886-1959&lt;/part&gt;

                    &lt;authorizedForm&gt;AACR2&lt;/authorizedForm&gt;

                    &lt;alternativeForm&gt;ncafnor&lt;/alternativeForm&gt;
                &lt;/nameEntry&gt;

                &lt;nameEntry&gt;
                    &lt;part&gt;Brown, Robert Carlton (1886-1959)&lt;/part&gt;

                    &lt;authorizedForm&gt;ncafnor&lt;/authorizedForm&gt;

                    &lt;alternativeForm&gt;AACR2&lt;/alternativeForm&gt;
                &lt;/nameEntry&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="alternativeSet">
    <h3>alternativeSet</h3>
    <h4><?= __d("cpf-doc", "Alternative Set") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A container element for two or more authority records derived from two or
 more authority systems, expressed within a single EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Alternative Set is a container element for two or more authority records derived from two
 or more authority systems. Alternative authority records are contained with
 &lt;alternativeSet&gt; in &lt;setComponent&gt; elements. This approach allows different
 descriptions of the same CPF entity to be contained within a single EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Authority record aggregation may be used in cooperative or consortial contexts that gather
 together records describing the same CPF entity in different languages, or from different rules
 when it is desirable to provide users with alternative descriptions of the same entity. For
 example, in the context of the European Union, an international cooperative project may want
 to provide users the option of storing descriptions in Italian, French, German, English, Spanish,
 and in other European languages."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "&lt;alternativeSet&gt; should not be confused with &lt;sources&gt;, wherein authority
 records referenced are not intended to be displayed as alternative versions."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "setComponent") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "cpfDescription") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:base") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;alternativeSet&gt;
                    &lt;setComponent href=&quot;http://authorities.loc.gov/&quot; type=&quot;simple&quot; lastDateTimeVerified=&quot;2009-08-02&quot;&gt;
                        &lt;componentEntry&gt;Bright Sparcs Record&lt;/componentEntry&gt;
                    &lt;/setComponent&gt;

                    &lt;setComponent href=&quot;http://nla.gov.au/anbd.aut-an35335937&quot; type=&quot;simple&quot; lastDateTimeVerified=&quot;2012-06-25&quot;&gt;
                        &lt;componentEntry&gt;NLA record.&lt;/componentEntry&gt;
                    &lt;/setComponent&gt;

                    &lt;setComponent&gt;
                        &lt;objectXMLWrap&gt;
                            &lt;eac-cpf xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xsi:schemaLocation=&quot;urn:isbn:1-931666-33-4 http://eac.staatsbibliothek-berlin.de/schema/cpf.xsd&quot;&gt;
                                &lt;control&gt;[...]&lt;/control&gt;

                                &lt;cpfDescription&gt;[...]&lt;/cpfDescription&gt;
&lt;/eac-cpf&gt;
                        &lt;/objectXMLWrap&gt;
                    &lt;/setComponent&gt;
                &lt;/alternativeSet&gt;


                &lt;alternativeSet&gt;
                    &lt;setComponent href=&quot;http://authorities.loc.gov/&quot; type=&quot;simple&quot;&gt;
                        &lt;componentEntry&gt;Bright Sparcs Record&lt;/componentEntry&gt;
                    &lt;/setComponent&gt;

                    &lt;setComponent href=&quot;http://nla.gov.au/anbd.aut-an35335937&quot; type=&quot;simple&quot;&gt;
                        &lt;componentEntry&gt;NLA record.&lt;/componentEntry&gt;
                    &lt;/setComponent&gt;
                &lt;/alternativeSet&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="authorizedForm">
    <h3>authorizedForm</h3>
    <h4><?= __d("cpf-doc", "Authorized Form") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Qualifies the name contained in &lt;nameEntry&gt; or the set of names
 contained in &lt;nameEntryParallel&gt; as authorized access points."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The name of an EAC-CPF entity, as formed within the &lt;nameEntry&gt; or
 &lt;nameEntryParallel&gt; elements, may be the authorized form of the name according to
 a particular set of rules or conventions, or it may be an alternative or unauthorized form
 according to a different set of rules. This optional element provides the opportunity to
 indicate one or more sets of rules or conventions under which the form of the name expressed
 in &lt;nameEntry&gt; or &lt;nameEntryParallel&gt; would be regarded as an authorized
 form."
        ) ?></p>
    <p><?= __d("cpf-doc", "The eac-cpf schema offers two possibilities:") ?></p>
    <p><?= __d(
            "cpf-doc",
            "1. &lt;authorizedForm&gt; is used within &lt;nameEntry&gt; only when
 &lt;nameEntry&gt; is not included within &lt;nameEntryParallel&gt;. In this case, it
 qualifies the form of the name recorded within the precise &lt;nameEntry&gt; element as
 an authorized access point."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "2. &lt;authorizedForm&gt; may be used within &lt;nameEntryParallel&gt; to indicate
 that the set of parallel names recorded in separate &lt;nameEntry&gt; elements within
 &lt;nameEntryParallel&gt; are deemed as authorized access points."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The content of the element is an abbreviation selected from a constrained set of values,
 each of which represents a set of national, international or other rules that govern the
 construction of EAC-CPF names in those environments. The abbreviations expressed in
 &lt;authorizedForm&gt; must be declared within the &lt;conventionDeclaration&gt;
 element in &lt;control&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The parallel element &lt;alternativeForm&gt; permits one to indicate rules or
 conventions according to which the name is the authorized form. The element
 &lt;preferredForm&gt; permits one to indicate that the name as expressed is the
 preferred form in the encoder&#039;s local context, regardless of its authorized status in any
 other name authority environment."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "&lt;authorizedForm&gt; may be repeated in case a single or a set of
 &lt;nameEntry&gt; element(s) may conform to more than one rule."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "NMTOKEN") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "nameEntry, nameEntryParallel") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;nameEntry&gt;
                    &lt;part&gt;Brown, Bob, 1886-1959&lt;/part&gt;

                    &lt;authorizedForm&gt;AACR2&lt;/authorizedForm&gt;

                    &lt;alternativeForm&gt;ncafnor&lt;/alternativeForm&gt;
                &lt;/nameEntry&gt;

                &lt;nameEntry&gt;
                    &lt;part&gt;Brown, Robert Carlton (1886-1959)&lt;/part&gt;

                    &lt;authorizedForm&gt;ncafnor&lt;/authorizedForm&gt;

                    &lt;alternativeForm&gt;AACR2&lt;/alternativeForm&gt;
                &lt;/nameEntry&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="biogHist">
    <h3>biogHist</h3>
    <h4><?= __d("cpf-doc", "Biography or History") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A concise essay and/or chronology that provides biographical or historical
 information about the EAC-CPF entity."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;biogHist&gt; includes significant details about the life of an individual or
 family, or the administrative history of a corporate body. The &lt;biogHist&gt; may
 contain just text in a series of Paragraphs &lt;p&gt;, and/or a Chronology List
 &lt;chronList&gt; that matches dates and date ranges with associated events and/or
 places. The &lt;abstract&gt; element is intended to provide a very brief synopsis of the
 full &lt;biogHist&gt; content that could be extracted for inclusion in a remote source,
 such as a MARC record."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The optional &lt;citation&gt;, &lt;list&gt;, and &lt;outline&gt; elements are
 used to accommodate greater complexity in expressing or representing the biography or
 history of the CPF entity being described. A simpler discursive expression of the information may be encoded
 as one or more &lt;p&gt; elements."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "abstract, chronList, citation, list, outline, p") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR 5.2.2</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;biogHist&gt;
                    &lt;abstract&gt;Established in 1961, the United States Peace Corps administered and
                        coordinated Federal international volunteer and related domestic volunteer programs in
                        areas of agricultural assistance, community development, education, environmental
                        protection, and nation assistance.&lt;/abstract&gt;

                    &lt;p&gt;The Peace Corps was established as an operating agency in the Department of
                        State Delegation of Authority 85-11, effective March 3, 1961, pursuant to Executive Order
                        (E.O.) 10924, March 1, 1961. It was recognized legislatively by the Peace Corps Act (75
                        Stat. 612), approved September 22, 1961. The Peace Corps was reassigned to the newly
                        established ACTION by Reorganization Plan No. 1 of 1971, effective July 1, 1971. It was
                        made autonomous within ACTION by E.O. 12137, May 16, 1979, and was made an independent
                        agency by Title VI of the International Security and Development Corporation Act of 1981
                        (95 Stat. 1540), February 21, 1982. The Peace Corps administered and coordinated Federal
                        international volunteer and related domestic volunteer programs including the areas of
                        agricultural assistance, community development, education, environmental protection, and
                        nation assistance.&lt;/p&gt;
                &lt;/biogHist&gt;

                &lt;biogHist&gt;
                    &lt;p&gt;Ilma Mary Brewer, nee Pidgeon, was Lecturer in Botany/Biology, University of
                        Sydney 1963-70 and Senior Lecturer in Biological Sciences 1970-78. She developed new
                        methods of teaching based on the recognition that a student learnt more by working at
                        his/her own place and instruction him/her self. Her findings were published as a book,
                        &quot;Learning More and Teaching Less.&quot;&lt;/p&gt;

                    &lt;chronList&gt;
                        &lt;chronItem&gt;
                            &lt;date standardDate=&quot;1936&quot;&gt;1936&lt;/date&gt;

                            &lt;event&gt;Bachelor of Science (BSc) completed at the University of
                                Sydney&lt;/event&gt;
                        &lt;/chronItem&gt;

                        &lt;chronItem&gt;
                            &lt;date standardDate=&quot;1937&quot;&gt;1937&lt;/date&gt;

                            &lt;event&gt;Master of Science (MSc) completed at the University of
                                Sydney&lt;/event&gt;
                        &lt;/chronItem&gt;

                        &lt;chronItem&gt;
                            &lt;dateRange&gt;
                                &lt;fromDate standardDate=&quot;1937&quot;&gt;1937&lt;/fromDate&gt;

                                &lt;toDate standardDate=&quot;1941&quot;&gt;1941&lt;/toDate&gt;
                            &lt;/dateRange&gt;

                            &lt;event&gt;Linnean Macleay Fellow&lt;/event&gt;
                        &lt;/chronItem&gt;
                    &lt;/chronList&gt;
                &lt;/biogHist&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="chronItem">
    <h3>chronItem</h3>
    <h4><?= __d("cpf-doc", "Chronology List Item") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A container element that keeps a date or a date range paired with an
 associated event and an optional place within a Chronology List &lt;chronList&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Each &lt;chronItem&gt; contains a &lt;date&gt; (a single date) or a
 &lt;dateRange&gt; (an inclusive date span) coupled with an &lt;event&gt;. A
 &lt;placeEntry&gt; element is optionally available to ground the event in a particular
 location."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "date, dateRange, event, placeEntry") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "chronList") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.2</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;chronItem&gt;
                    &lt;date standardDate=&quot;1957&quot;&gt;1957&lt;/date&gt;

                    &lt;event&gt;Left Mer and moved to the mainland. Worked at various jobs including
                        canecutter and railway labourer.&lt;/event&gt;
                &lt;/chronItem&gt;

                &lt;chronItem&gt;
                    &lt;dateRange&gt;
                        &lt;fromDate standardDate=&quot;1960&quot;&gt;1960&lt;/fromDate&gt;

                        &lt;toDate standardDate=&quot;1961&quot;&gt;1961&lt;/toDate&gt;
                    &lt;/dateRange&gt;

                    &lt;event&gt;Union representative, Townsville-Mount Isa rail construction
                        project.&lt;/event&gt;
                &lt;/chronItem&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="chronList">
    <h3>chronList</h3>
    <h4><?= __d("cpf-doc", "Chronology List") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A structured chronological list of events, dates, and (optionally) places
 that may be used within the &lt;biogHist&gt; element."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Chronology List comprises a sequentially structured list of significant events in the life
 of the person or during the existence of the family or corporate body described in the
 EAC-CPF instance. Such events are associated with the date of occurrence and optionally with
 the name of a place. Each &lt;chronList&gt; contains a series of &lt;chronItem&gt;
 elements, each associating a &lt;date&gt; or &lt;dateRange&gt; with an
 &lt;event&gt; and an optional &lt;placeEntry&gt;."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "chronItem") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "biogHist") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.2</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;chronList&gt;
                    &lt;chronItem&gt;
                        &lt;date standardDate=&quot;1936&quot;&gt;1936&lt;/date&gt;

                        &lt;event&gt;Bachelor of Science (BSc) completed at the University of
                            Sydney&lt;/event&gt;
                    &lt;/chronItem&gt;

                    &lt;chronItem&gt;
                        &lt;date standardDate=&quot;1937&quot;&gt;1937&lt;/date&gt;

                        &lt;event&gt;Master of Science (MSc) completed at the University of
                            Sydney&lt;/event&gt;
                    &lt;/chronItem&gt;

                    &lt;chronItem&gt;
                        &lt;dateRange&gt;
                            &lt;fromDate standardDate=&quot;1937&quot;&gt;1937&lt;/fromDate&gt;

                            &lt;toDate standardDate=&quot;1941&quot;&gt;1941&lt;/toDate&gt;
                        &lt;/dateRange&gt;

                        &lt;event&gt;Linnean Macleay Fellow&lt;/event&gt;
                    &lt;/chronItem&gt;

                    &lt;chronItem&gt;
                        &lt;dateRange&gt;
                            &lt;fromDate standardDate=&quot;1939&quot;&gt;c. 1939&lt;/fromDate&gt;

                            &lt;toDate standardDate=&quot;1945&quot;&gt;c. 1945&lt;/toDate&gt;
                        &lt;/dateRange&gt;

                        &lt;event&gt;Worked with Army Intelligence to map the vegetation (trees and undergrowth)
                            in the coastal regions of New South Wales&lt;/event&gt;
                    &lt;/chronItem&gt;
                &lt;/chronList&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="citation">
    <h3>citation</h3>
    <h4><?= __d("cpf-doc", "Citation") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element that cites an external resource."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "&lt;citation&gt; is a generic element available within a number of descriptive elements that cites an external resource in machine and / or
 human readable form.
 The purpose of the &lt;citation&gt; element is to point to a resource that provides
 descriptive data which is not otherwise given in the EAC-CPF instance, such as an original
 document that sets out the mandate for a corporate body; it should not be confused with the
 &lt;source&gt; element which is used to declare a particular resource used in the
 composition of the EAC-CPF instance. In most contexts it is optional, but a
 &lt;citation&gt; element must be given within &lt;conventionDeclaration&gt; and
 &lt;localTypeDeclaration&gt; elements in &lt;control&gt;. Provide the formal title or
 name of the resource, using the &lt;span&gt; element to indicate any formatting (such as
 italic or bold etc) thought necessary. The user experience can be controlled by use of the
 available XML linking Language (Xlink) attributes for which consult the specification at
 http://www.w3.org/TR/xlink/."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text], span") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "biogHist, conventionDeclaration, function, functions, legalStatus, legalStatuses, localDescription, localDescriptions, localTypeDeclaration, mandate, mandates, occupation, occupations, place, places"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "lastDateTimeVerified") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:actuate") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:arcrole") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:href") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:role") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:show") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:title") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:type") ?></td>
            <td><?= __d("cpf-doc", "Required (if any XLINK attributes used)") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "Within conventionDeclaration:: Mandatory, RepeatableOther elements:: Optional, Repeatable"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;conventionDeclaration&gt;
                    &lt;abbreviation&gt;AFNOR&lt;/abbreviation&gt;

                    &lt;citation&gt;Indexation conforme à la norme: AFNOR. &quot; AFNOR NF Z44-060 Documentation - Catalogage d&#039;auteurs
                        et d&#039;anonymes: formes et structures des vedettes de collectivités - auteurs.&quot; Décembre 1996.
                        &lt;/citation&gt;
                &lt;/conventionDeclaration&gt;

                &lt;conventionDeclaration&gt;
                    &lt;abbreviation&gt;USNARA-LDRG&lt;/abbreviation&gt;

                    &lt;citation&gt;U.S. National Archives and Records Administration. &quot;Lifecycle Data
                        Requirements Guide&quot;. March 2012. (for creating the authorized form of the
                        name).&lt;/citation&gt;
                &lt;/conventionDeclaration&gt;

                &lt;mandate&gt;
                    &lt;citation&gt;FR ANOM COL C11D 1 Fo179-179 vo: concession de pêche en Acadie par le
                        Roi à Bergier, Gaultier, Boucher et autres daté du 3 mars 1684.&lt;/citation&gt;
                &lt;/mandate&gt;

                &lt;biogHist&gt;
                    &lt;citation&gt;The full biography is presented in &quot;Vasari, Giorgio. &quot;Part 3, Paolo Uccello.&quot; In
                        &lt;span style=&quot;italics&quot;&gt;The lives of the artists&lt;/span&gt;
, by Giorgio Vasari, translated by P. Bondanella and J.C. Bondanella. Oxford: Oxford University Press, 1991.&quot;&lt;/citation&gt;
                &lt;/biogHist&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="componentEntry">
    <h3>componentEntry</h3>
    <h4><?= __d("cpf-doc", "Component Entry") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A caption that can be used to provide identification and access to a linked
 resource."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A &lt;componentEntry&gt; occurs within &lt;setComponent&gt; to provide a
 textual note about the EAC-CPF instance that is being bundled together with other authority
 records for the same entity within an &lt;alternativeSet&gt; wrapper. The bundled
 alternate records for a given EAC-CPF entity may be in different languages or may come from
 different authority systems. The bundling allows them to be transmitted or stored together.
 The &lt;componentEntry&gt; element provides a place where a particular alternate record
 can be described or explained in relation to the other authority records."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "setComponent") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "scriptCode") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "transliteration") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;alternativeSet&gt;
                    &lt;setComponent href=&quot;http://nla.gov.au/anbd.aut-an35335937&quot; type=&quot;simple&quot;&gt;
                        &lt;componentEntry&gt;Bright Sparcs Record&lt;/componentEntry&gt;
                    &lt;/setComponent&gt;

                    &lt;setComponent lastDateTimeVerified=&quot;2009-08-02&quot; href=&quot;mawsonBS.xml&quot; type=&quot;simple&quot;&gt;
                        &lt;objectXMLWrap&gt;
                            &lt;eac-cpf xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xsi:schemaLocation=&quot;urn:isbn:1-931666-33-4 http://eac.staatsbibliothek-berlin.de/schema/cpf.xsd&quot;&gt; [...] &lt;/eac-cpf&gt;
                        &lt;/objectXMLWrap&gt;
                    &lt;/setComponent&gt;
                &lt;/alternativeSet&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="control">
    <h3>control</h3>
    <h4><?= __d("cpf-doc", "Control") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The element of the instance that contains control information about its
 identity, creation, maintenance, status, and the rules and authorities used in the
 composition of the description."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "This required wrapper element within &lt;eac-cpf&gt; contains the information necessary
 to manage the instance. This includes information about its identity, creation,
 maintenance, and status as well the languages, rules and authorities used in the composition
 of the description."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "It must contain a unique identifier for the instance within the &lt;recordId&gt;
 element. Other associated identifiers may be given in &lt;otherRecordId&gt;. There
 must be a description of the agency responsible for its creation and maintenance in
 &lt;maintenanceAgency&gt; as well as statements about its current drafting status in
 &lt;maintenanceStatus&gt; and the creation, maintenance, and disposition of the instance
 in &lt;maintenanceHistory&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Three elements are available to declare languages, rules and conventions used in the
 EAC-CPF instance. The &lt;languageDeclaration&gt; element provides information on the
 language and script used in the description. The &lt;conventionDeclaration&gt; element
 provides information on the authorities or controlled vocabularies used in the instance.
 &lt;localTypeDeclaration&gt; declares the local conventions and controlled
 vocabularies used within the localType."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Other optional elements available in &lt;control&gt; include a &lt;sources&gt;
 element to provide information about the documentary sources used in the composition of the
 description and a &lt;publicationStatus&gt; element to indicate the editorial status of
 the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Other control elements considered necessary but not otherwise included may be given in the
 &lt;localControl&gt; element."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "conventionDeclaration, languageDeclaration, localControl, localTypeDeclaration, maintenanceAgency, maintenanceHistory, maintenanceStatus, otherRecordId, publicationStatus, recordId, sources"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "eac-cpf") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:base") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;control&gt;
                    &lt;recordId&gt;nla.party-521122&lt;/recordId&gt;

                    &lt;maintenanceStatus&gt;revised&lt;/maintenanceStatus&gt;

                    &lt;publicationStatus&gt;approved&lt;/publicationStatus&gt;

                    &lt;maintenanceAgency&gt;
                        &lt;agencyCode&gt;GR-TEI:ATHENS&lt;/agencyCode&gt;

                        &lt;agencyName&gt;National Library of Australia&lt;/agencyName&gt;
                    &lt;/maintenanceAgency&gt;

                    &lt;languageDeclaration&gt;
                        &lt;language languageCode=&quot;eng&quot;&gt;&lt;/language&gt;

                        &lt;script scriptCode=&quot;Latn&quot;&gt;&lt;/script&gt;
                    &lt;/languageDeclaration&gt;

                    &lt;conventionDeclaration&gt;
                        &lt;abbreviation&gt;AACR2&lt;/abbreviation&gt;

                        &lt;citation&gt;Anglo-American Cataloging Rules, Revised&lt;/citation&gt;
                    &lt;/conventionDeclaration&gt;

                    &lt;maintenanceHistory&gt;
                        &lt;maintenanceEvent&gt;
                            &lt;eventType&gt;created&lt;/eventType&gt;

                            &lt;eventDateTime standardDateTime=&quot;2009-06-26T05:33:41Z&quot;&gt;2009-06-26T05:33:41Z&lt;/eventDateTime&gt;

                            &lt;agentType&gt;human&lt;/agentType&gt;

                            &lt;agent&gt;bdewhurs&lt;/agent&gt;
                        &lt;/maintenanceEvent&gt;
                    &lt;/maintenanceHistory&gt;

                    &lt;sources&gt;
                        &lt;source href=&quot;http://www.icacds.org.uk/eng/ISAAAR(CPF)2ed.pdf&quot; type=&quot;simple&quot;&gt;
                            &lt;sourceEntry&gt;ISAAR(CPF)&lt;/sourceEntry&gt;

                            &lt;descriptiveNote&gt;
                                &lt;p&gt;Record created based on ISAAR(CPF) 2nd ed Example 5 - Person
                                    description&lt;/p&gt;
                            &lt;/descriptiveNote&gt;
                        &lt;/source&gt;
                    &lt;/sources&gt;
                &lt;/control&gt;


                &lt;control&gt;
                    &lt;recordId&gt;254pap_XML&lt;/recordId&gt;

                    &lt;maintenanceStatus&gt;new&lt;/maintenanceStatus&gt;

                    &lt;maintenanceAgency&gt;
                        &lt;agencyCode&gt;GR-ΤΕΙΑ&lt;/agencyCode&gt;

                        &lt;agencyName&gt;Technological Educational Institute of Athens&lt;/agencyName&gt;
                    &lt;/maintenanceAgency&gt;

                    &lt;languageDeclaration&gt;
                        &lt;language languageCode=&quot;gre&quot;&gt;Greek language, Modern.&lt;/language&gt;

                        &lt;script scriptCode=&quot;Grek&quot;&gt;&lt;/script&gt;
                    &lt;/languageDeclaration&gt;

                    &lt;conventionDeclaration&gt;
                        &lt;abbreviation&gt;ISAAR (CPF)&lt;/abbreviation&gt;

                        &lt;citation&gt;Committee on Descriptive Standards Canberra. International Standard
                            Archival Authority Description for Corporate Bodies, Persons and Families (ISAAR
                            CPF). Australia, Second Edition, October (2003).&lt;/citation&gt;
                    &lt;/conventionDeclaration&gt;

                    &lt;conventionDeclaration&gt;
                        &lt;abbreviation&gt;NLG Authorities&lt;/abbreviation&gt;

                        &lt;citation&gt;Maniati, Ioanna (ed.) (1991). National Library of Greece: Greek Subject Headings Catalog. Athens.&lt;/citation&gt;
                    &lt;/conventionDeclaration&gt;

                    &lt;maintenanceHistory&gt;
                        &lt;maintenanceEvent&gt;
                            &lt;eventType&gt;created&lt;/eventType&gt;

                            &lt;eventDateTime&gt;2011-05-20&lt;/eventDateTime&gt;

                            &lt;agentType&gt;human&lt;/agentType&gt;

                            &lt;agent&gt;Department of Library Science and Information Systems, Technological Educational Institute of Athens&lt;/agent&gt;
                        &lt;/maintenanceEvent&gt;
                    &lt;/maintenanceHistory&gt;

                    &lt;sources&gt;
                        &lt;source&gt;
                            &lt;sourceEntry&gt;Archives of the Secondary Education Division(Ministry of National Education and Religion).&lt;/sourceEntry&gt;
                        &lt;/source&gt;
                    &lt;/sources&gt;
                &lt;/control&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="conventionDeclaration">
    <h3>conventionDeclaration</h3>
    <h4><?= __d("cpf-doc", "Convention Declaration") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A declaration of the rules or conventions, including authorized controlled
 vocabularies and thesauri, applied in creating the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An optional element of &lt;control&gt;, used for declaring references in the
 &lt;citation&gt; element to any rules and conventions, including authorized controlled
 vocabularies or thesauri, applied in the construction of the description. For example,
 &lt;conventionDeclaration&gt; should be used to identify any controlled vocabularies
 the terms of which appear as values of the attribute vocabularySource for &lt;term&gt;,
 &lt;placeEntry&gt;, and &lt;placeRole&gt; elements. Any notes relating to how these
 rules or conventions have been used may be given within a &lt;descriptiveNote&gt;
 element. The &lt;abbreviation&gt; element may be used to identify the standard or
 controlled vocabulary in a coded structure. Each new rule / set of rules should be contained in a separate &lt;conventionDeclaration&gt; element."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "abbreviation, citation, descriptiveNote") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "control") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.3</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;conventionDeclaration&gt;
                    &lt;abbreviation&gt;AFNOR&lt;/abbreviation&gt;

                    &lt;citation&gt;AFNOR NFZ 44-060 (decembre 1986)&lt;/citation&gt;

                    &lt;descriptiveNote&gt;
                        &lt;p&gt;Indexation conforme à la norme AFNOR NFZ 44-060 (Décembre
                            1986) Catalogage: forme et structure des vedettes de collectivités-auteurs
                            Notice encodee conformement à la norme internationale de description
                            archivistique contextuelle informatisée EAC 2004&lt;/p&gt;
                    &lt;/descriptiveNote&gt;
                &lt;/conventionDeclaration&gt;

                &lt;conventionDeclaration&gt;
                    &lt;citation&gt;International Standards Organization. &quot;ISO 8601 - Data elements and
                        interchange formats - Information interchange - Representation of dates and times.&quot;
                        Geneva: International Standards Organization, 2000.&lt;/citation&gt;
                &lt;/conventionDeclaration&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="cpfDescription">
    <h3>cpfDescription</h3>
    <h4><?= __d("cpf-doc", "Corporate Body, Person, or Family Description") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The &lt;cpfDescription&gt; contains the description of one identity.
 Commonly one CPF entity has one identity, but when it has more than one, use either
 &lt;multipleIdentities&gt; or multiple interrelated EAC-CPF instances."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;cpfDescription&gt; contains the description of one identity. Each description
 contains the name or names by which the identity is known, and optionally may contain
 a description of historical context to facilitate understanding of the identity. The
 &lt;cpfDescription&gt; includes a required &lt;identity&gt; element containing
 authorized or parallel name entries and optional &lt;description&gt; and
 &lt;relations&gt; to provide contextual information for the CPF entity being described,
 including the relations to other corporate bodies, persons, families, resources, and functions."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An optional &lt;alternativeSet&gt; element allows the incorporation of two or more
 authority records derived from two or more authority systems. The xml:id
 attribute allows individual &lt;cpfDescription&gt; elements to be individually identified
 when using the &lt;multipleIdentities&gt; structure."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "alternativeSet, description, identity, relations") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "eac-cpf, multipleIdentities") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.1, 5.2, 5.3</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:base") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "Within eac-cpf:: Mandatory, Non-repeatableWithin multipleIdentities:: Mandatory, Repeatable"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;cpfDescription&gt;
                    &lt;identity&gt;[...] &lt;/identity&gt;

                    &lt;description&gt;[...] &lt;/description&gt;

                    &lt;relations&gt;[...]&lt;/relations&gt;
                &lt;/cpfDescription&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="cpfRelation">
    <h3>cpfRelation</h3>
    <h4><?= __d("cpf-doc", "Corporate Body, Person, or Family Relation") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element designed to encode a relationship between a corporate body, person, or family and the CPF entity described in the
 EAC-CPF instance"
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;cpfRelation&gt; element contains the description of a corporate body, person,
 or family related to the described CPF entity. Such related entities are typically described in
 another EAC-CPF instance or other encoding language such as MARC. Use the
 &lt;objectXMLWrap&gt; to incorporate XML elements from any XML namespace or
 &lt;objectBinWrap&gt; for base64-encoded binary data. A &lt;relationEntry&gt; element
 is provided for textual identification of the related entity."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Use the &lt;date&gt;, &lt;dateRange&gt;, or &lt;dateSet&gt; elements for
 specifying the time period of the relationship and the
 &lt;placeEntry&gt; element for recording relevant location information. A
 &lt;descriptiveNote&gt; element may be included for a more detailed explanation of the relationship."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The cpfRelationType attribute may be used to specify the nature of the
 &lt;cpfRelation&gt; entity’s relationship to the entity described in the
 EAC-CPF instance. Values are chosen from a closed list."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "date, dateRange, dateSet, descriptiveNote, objectBinWrap, objectXMLWrap, placeEntry, relationEntry"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "relations") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.3</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "cpfRelationType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "lastDateTimeVerified") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:actuate") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:arcrole") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:href") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:role") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:show") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:title") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:type") ?></td>
            <td><?= __d("cpf-doc", "Required (if any XLINK attributes used)") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;cpfRelation cpfRelationType=&quot;hierarchical-parent&quot; xlink:href=&quot;FRDAFANCH00MC_NAETUDE_110&quot; xlink:type=&quot;simple&quot;&gt;
                    &lt;relationEntry&gt;Étude notariale CX&lt;/relationEntry&gt;

                    &lt;dateRange&gt;
                        &lt;fromDate standardDate=&quot;1609-07-04&quot;&gt;4 juillet 1609&lt;/fromDate&gt;

                        &lt;toDate standardDate=&quot;1640-07-07&quot;&gt;7 juillet 1640&lt;/toDate&gt;
                    &lt;/dateRange&gt;
                &lt;/cpfRelation&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="date">
    <h3>date</h3>
    <h4><?= __d("cpf-doc", "Date") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The single date of an event in the history of, or a relationship with, the
 person, family, or corporate body being described in the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A generic element expressing the single date of an event in the history of, or a
 relationship with, the person, family, or corporate body being described in the EAC-CPF
 instance. If the event or relationship has inclusive dates use the &lt;dateRange&gt;
 element, while more complex dates (combining singles dates and date ranges) can be expressed
 in &lt;dateSet&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The content of the element is intended to be a human-readable natural language date with a
 machine-readable date provided as the value of the standardDate attribute,
 formulated according to ISO 8601. Other attributes include notBefore and
 notAfter for dates of uncertainty. The localType attribute can be
 used to supply a more specific characterization of the date."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Dates of existence for the entity being described in the EAC-CPF instance are encoded with
 the &lt;existDates&gt; element, while the dates of use of a particular name of an entity
 are encoded in &lt;useDates&gt;. The date and time of a maintenance event in the history
 of the EAC-CPF instance are given in the &lt;eventDateTime&gt; element."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "chronItem, cpfRelation, dateSet, existDates, function, functionRelation, legalStatus, localControl, localDescription, mandate, occupation, place, resourceRelation, useDates"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "notAfter") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "notBefore") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "standardDate") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;date standardDate=&quot;1765-09-18&quot;&gt;September 18, 1765&lt;/date&gt;


                &lt;date localType=&quot;WeddingDay&quot; standardDate=&quot;2000-08-12&quot;&gt;September 12, 2000&lt;/date&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="dateRange">
    <h3>dateRange</h3>
    <h4><?= __d("cpf-doc", "Date Range") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The date range of an event in the history of, or a relationship with, the
 person, family, or corporate body being described in the EAC-CPF instance. It contains &lt;fromDate&gt; and &lt;toDate&gt; child elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A generic element that expresses inclusive dates of an event in the history of, or a
 relationship with, the person, family, or corporate body being described in the EAC-CPF
 instance. &lt;dateRange&gt; contains mandatory &lt;fromDate&gt; and &lt;toDate&gt;
 child elements. If the event or relationship has a single date use the &lt;date&gt;
 element, while more complex dates (combining single dates and date ranges) can be expressed
 in &lt;dateSet&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The localType attribute can be used to supply a more specific characterization
 of the date."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Dates of existence for the identity being described in the EAC-CPF instance are encoded
 with the &lt;existDates&gt; element, while the dates of use of a particular name of an
 identity are encoded in &lt;useDates&gt;. The date and time of a maintenance event in the
 history of the EAC-CPF instance are given in the &lt;eventDateTime&gt; element."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "fromDate, toDate") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "chronItem, cpfRelation, dateSet, existDates, function, functionRelation, legalStatus, localControl, localDescription, mandate, occupation, place, resourceRelation, useDates"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;dateRange&gt;
                    &lt;fromDate standardDate=&quot;1765-09-18&quot;&gt;September 18, 1765&lt;/fromDate&gt;

                    &lt;toDate standardDate=&quot;1846-06-01&quot;&gt;June 1, 1846&lt;/toDate&gt;
                &lt;/dateRange&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="dateSet">
    <h3>dateSet</h3>
    <h4><?= __d("cpf-doc", "Date Set") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A grouping element that facilitates complex date expressions by being able to combine single dates and date ranges, multiple
 single dates, or multiple date ranges."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A grouping element used for combining single dates and date ranges, multiple single dates, or multiple date ranges. The &lt;dateSet&gt; element is used in situations where complex
 date information needs to be conveyed and requires at least two child elements. These can be
 a combination of &lt;date&gt; and &lt;dateRange&gt; elements."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "date, dateRange") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "cpfRelation, existDates, function, functionRelation, legalStatus, localDescription, mandate, occupation, place, resourceRelation, useDates"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;dateSet&gt;
                    &lt;dateRange&gt;
                        &lt;fromDate standardDate=&quot;1928-09&quot;&gt;1928 settembre&lt;/fromDate&gt;

                        &lt;toDate standardDate=&quot;1930-08&quot;&gt;1930 autunno&lt;/toDate&gt;
                    &lt;/dateRange&gt;

                    &lt;dateRange&gt;
                        &lt;fromDate standardDate=&quot;1947&quot;&gt;1947&lt;/fromDate&gt;

                        &lt;toDate standardDate=&quot;1949&quot;&gt;1949&lt;/toDate&gt;
                    &lt;/dateRange&gt;

                    &lt;date&gt;1950&lt;/date&gt;

                    &lt;date standardDate=&quot;1951-10-27&quot;&gt;27 of October 1951&lt;/date&gt;
                &lt;/dateSet&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="description">
    <h3>description</h3>
    <h4><?= __d("cpf-doc", "Description") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A wrapper for all of the content elements comprising description of the CPF entity described in the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The elements that constitute &lt;description&gt; together permit descriptive
 information to be encoded in either structured or unstructured fashions, or in a combined
 approach. &lt;description&gt; accommodates the encoding of all the data elements that
 comprise the Description Area of ISAAR (CPF) including historical, biographical, and
 genealogical information; legal status and mandates; functions, occupations, and activities,
 and the dates and places that further constrain those elements."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "biogHist, existDates, function, functions, generalContext, languageUsed, languagesUsed, legalStatus, legalStatuses, localDescription, localDescriptions, mandate, mandates, occupation, occupations, place, places, structureOrGenealogy"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "cpfDescription") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:base") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;description&gt;
                    &lt;existDates&gt;
                        &lt;dateRange&gt;
                            &lt;fromDate standardDate=&quot;1765-09-18&quot;&gt;September 18, 1765&lt;/fromDate&gt;

                            &lt;toDate standardDate=&quot;1846-06-01&quot;&gt;June 1, 1846&lt;/toDate&gt;
                        &lt;/dateRange&gt;
                    &lt;/existDates&gt;

                    &lt;place&gt;
                        &lt;placeEntry altitude=&quot;389&quot; latitude=&quot;46.140833&quot; longitude=&quot;12.215556&quot;&gt;Belluno&lt;/placeEntry&gt;

                        &lt;placeRole&gt;Birthplace&lt;/placeRole&gt;

                        &lt;date standardDate=&quot;1765-09-18&quot;&gt;September 18, 1765&lt;/date&gt;
                    &lt;/place&gt;

                    &lt;biogHist&gt;
                        &lt;p&gt;Cappellari was born at Belluno on 18 September 1765 to a noble family. At an
                            early age he joined the order of the Camaldolese (part of the Benedictine monastic
                            family) and entered the Monastery of San Michele di Murano, near Venice. As a Camaldolese
                            monk, Cappellari rapidly gained distinction for his theological and linguistic skills.
                            His first appearance before a wider public was in 1799, when he published against the
                            Italian Jansenists a controversial work entitled
                            &lt;span localType=&quot;title&quot; style=&quot;font-style:italic&quot;&gt;II Trionfo della Santa Sede&lt;/span&gt;
, which besides
                            passing through several editions in Italy, has been translated into several European
                            languages. In 1800, he became a member of the Academy of the Catholic Religion, founded
                            by Pope Pius VII (1800-1823), to which he contributed a number of memoirs on theological
                            and philosophical questions, and in 1805 was made abbot of San Gregorio on the Caelian
                            Hill.&lt;/p&gt;
                    &lt;/biogHist&gt;
                &lt;/description&gt;


                &lt;description&gt;
                    &lt;existDates&gt;
                        &lt;dateRange&gt;
                            &lt;fromDate standardDate=&quot;1969&quot;&gt;1969&lt;/fromDate&gt;

                            &lt;toDate standardDate=&quot;1981&quot;&gt;1981&lt;/toDate&gt;
                        &lt;/dateRange&gt;
                    &lt;/existDates&gt;

                    &lt;place&gt;
                        &lt;placeRole&gt;headquarters&lt;/placeRole&gt;

                        &lt;placeEntry&gt;Athens&lt;/placeEntry&gt;
                    &lt;/place&gt;

                    &lt;legalStatus&gt;
                        &lt;term&gt;Public services, organizations and enterprises&lt;/term&gt;
                    &lt;/legalStatus&gt;

                    &lt;functions&gt;
                        &lt;function&gt;
                            &lt;term&gt;School education&lt;/term&gt;
                        &lt;/function&gt;

                        &lt;function&gt;
                            &lt;term&gt;Tertiary education&lt;/term&gt;
                        &lt;/function&gt;
                    &lt;/functions&gt;
                &lt;/description&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="descriptiveNote">
    <h3>descriptiveNote</h3>
    <h4><?= __d("cpf-doc", "Descriptive Note") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A generic element that provides additional information and specifications,
 in textual form, concerning the descriptive element in which it is contained."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A generic element available in a number of descriptive elements that can be used to
 provide any additional descriptive notes that might be appropriate. Notes must be contained
 in paragraphs (&lt;p&gt;) and any necessary formatting may be indicated by use of the
 &lt;span&gt; element within &lt;p&gt;."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "p") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "conventionDeclaration, cpfRelation, existDates, function, functionRelation, functions, identity, languageDeclaration, languageUsed, languagesUsed, legalStatus, legalStatuses, localDescription, localDescriptions, localTypeDeclaration, maintenanceAgency, mandate, mandates, occupation, occupations, place, places, resourceRelation, setComponent, source"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;source xlink:href=&quot;http://www.icacds.org.uk/eng/ISAAAR(CPF)2ed.pdf&quot; xlink:type=&quot;simple&quot;&gt;
                    &lt;sourceEntry&gt;ISAAAR(CPF)&lt;/sourceEntry&gt;

                    &lt;descriptiveNote&gt;
                        &lt;p&gt;Record created based on ISAAR(CPF) 2nd ed Example 5 - Person
                            description&lt;/p&gt;
                    &lt;/descriptiveNote&gt;
                &lt;/source&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="eac-cpf">
    <h3>eac-cpf</h3>
    <h4><?= __d("cpf-doc", "Encoded Archival Context - Corporate Bodies, Persons, and Families") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The &lt;eac-cpf&gt; is the root element and as such contains contains the
 entire EAC-CPF description of the person, corporate body, or family. It contains a required
 &lt;control&gt; followed by either a &lt;cpfDescription&gt; or a
 &lt;multipleIdentities&gt; element."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The outermost wrapper element that defines a particular instance of an archival authority
 record encoded with the EAC-CPF XML Schema. It contains a required &lt;control&gt; and
 either a &lt;cpfDescription&gt; or a &lt;multipleIndentities&gt; element."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "control, cpfDescription, multipleIdentities") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:base") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;eac-cpf xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xsi:schemaLocation=&quot;urn:isbn:1-931666-33-4 http://eac.staatsbibliothek-berlin.de/schema/cpf.xsd&quot;&gt;
                    &lt;control&gt;[...]&lt;/control&gt;

                    &lt;cpfDescription&gt;[...]&lt;/cpfDescription&gt;
&lt;/eac-cpf&gt;


                &lt;eac-cpf&gt;
                    &lt;control&gt;[...]&lt;/control&gt;

                    &lt;multipleIdentities&gt;
                        &lt;cpfDescription&gt;[...]&lt;/cpfDescription&gt;

                        &lt;cpfDescription&gt;[...]&lt;/cpfDescription&gt;
                    &lt;/multipleIdentities&gt;
&lt;/eac-cpf&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="entityId">
    <h3>entityId</h3>
    <h4><?= __d("cpf-doc", "Entity Identifier") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Any formal identifier used to designate the entity being described."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An optional element of &lt;identity&gt; that may be used to record any identifier
 associated with the CPF entity being described in the EAC-CPF instance. Identifiers such as
 legal identifiers, typically assigned by an authoritative agency, may be recorded in this
 element."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Do not confuse with &lt;recordId&gt; within &lt;control&gt;, which refers to an
 identifier for the EAC-CPF instance rather than the entity it describes."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "identity") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.1.6</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;identity&gt;
                    &lt;entityId&gt;CLU-SC-000008&lt;/entityId&gt;

                    &lt;entityType&gt;person&lt;/entityType&gt;

                    &lt;nameEntry&gt;
                        &lt;part&gt;Brown, Bob, 1886-1959&lt;/part&gt;

                        &lt;authorizedForm&gt;AACR2&lt;/authorizedForm&gt;

                        &lt;alternativeForm&gt;ncafnor&lt;/alternativeForm&gt;
                    &lt;/nameEntry&gt;

                    &lt;nameEntry&gt;
                        &lt;part&gt;Brown, Robert Carlton (1886-1959)&lt;/part&gt;

                        &lt;authorizedForm&gt;ncafnor&lt;/authorizedForm&gt;

                        &lt;alternativeForm&gt;AACR2&lt;/alternativeForm&gt;
                    &lt;/nameEntry&gt;
                &lt;/identity&gt;

 CLU-SC-000008  person  Brown, Bob, 1886-1959  AACR2  ncafnor  Brown, Robert Carlton (1886-1959)  ncafnor  AACR2
</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="entityType">
    <h3>entityType</h3>
    <h4><?= __d("cpf-doc", "Entity Type") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The type of CPF entity being described. Values available are: person,
 corporateBody, or family."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Within &lt;identity&gt; this mandatory element specifies the type of CPF entity being
 described in the EAC-CPF instance. Values available are: person, corporateBody, or
 family."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "&quot;person&quot; or &quot;corporateBody&quot; or &quot;family&quot;"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "identity") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.1.1</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;identity&gt;
                    &lt;entityType&gt;corporateBody&lt;/entityType&gt;

                    &lt;nameEntry&gt;
                        &lt;part&gt;British Broadcasting Corporation&lt;/part&gt;

                        &lt;useDates&gt;
                            &lt;dateRange&gt;
                                &lt;fromDate standardDate=&quot;1922-10-18&quot;&gt;Oct. 18, 1922&lt;/fromDate&gt;

                                &lt;toDate&gt;&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/useDates&gt;
                    &lt;/nameEntry&gt;
                &lt;/identity&gt;


                &lt;identity&gt;
                    &lt;entityType&gt;person&lt;/entityType&gt;

                    &lt;nameEntry&gt;
                        &lt;part localType=&quot;surname&quot;&gt;Elytes&lt;/part&gt;

                        &lt;part localType=&quot;name&quot;&gt;Odysseas&lt;/part&gt;

                        &lt;useDates&gt;
                            &lt;dateRange&gt;
                                &lt;fromDate standardDate=&quot;1911&quot;&gt;1911&lt;/fromDate&gt;

                                &lt;toDate standardDate=&quot;1996&quot;&gt;1996&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/useDates&gt;
                    &lt;/nameEntry&gt;
                &lt;/identity&gt;


                &lt;identity&gt;
                    &lt;entityType&gt;family&lt;/entityType&gt;

                    &lt;nameEntry&gt;
                        &lt;part&gt;Rockefeller family&lt;/part&gt;

                        &lt;useDates&gt;
                            &lt;dateRange&gt;
                                &lt;fromDate standardDate=&quot;1839&quot;&gt;1839&lt;/fromDate&gt;

                                &lt;toDate&gt;&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/useDates&gt;
                    &lt;/nameEntry&gt;
                &lt;/identity&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="event">
    <h3>event</h3>
    <h4><?= __d("cpf-doc", "Event") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element used to encode an event associated with a date and, optionally, a
 place within a structured chronology."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The element contains discursive text identifying the event described by the
 &lt;chronItem&gt;. Every &lt;event&gt; must have an associated &lt;date&gt;
 element, and it may also have an optional &lt;placeEntry&gt; element."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "chronItem") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;chronList&gt;
                    &lt;chronItem&gt;
                        &lt;dateRange&gt;
                            &lt;fromDate standardDate=&quot;1944&quot;&gt;1944&lt;/fromDate&gt;

                            &lt;toDate standardDate=&quot;1947&quot;&gt;1947&lt;/toDate&gt;
                        &lt;/dateRange&gt;

                        &lt;event&gt;Summer employment at Minnesota Valley Canning Co.&lt;/event&gt;

                        &lt;placeEntry&gt;Blue Earth (Minn.)&lt;/placeEntry&gt;
                    &lt;/chronItem&gt;

                    &lt;chronItem&gt;
                        &lt;dateRange&gt;
                            &lt;fromDate standardDate=&quot;1946&quot;&gt;1946&lt;/fromDate&gt;

                            &lt;toDate standardDate=&quot;1948&quot;&gt;1948&lt;/toDate&gt;
                        &lt;/dateRange&gt;

                        &lt;event&gt;Macalester College&lt;/event&gt;

                        &lt;placeEntry&gt;St. Paul (Minn.)&lt;/placeEntry&gt;
                    &lt;/chronItem&gt;

                    &lt;chronItem&gt;
                        &lt;date standardDate=&quot;1948&quot;&gt;1948&lt;/date&gt;

                        &lt;event&gt;Campaign Manager, 2nd District, State Democratic-Farmer-Labor Central
                            Committee. Report to Orville Freeman.&lt;/event&gt;
                    &lt;/chronItem&gt;
                &lt;/chronList&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="eventDateTime">
    <h3>eventDateTime</h3>
    <h4><?= __d("cpf-doc", "Maintenance Event Date and Time") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The date and time of a maintenance event for the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A required element within the &lt;maintenanceEvent&gt;, &lt;eventDateTime&gt; gives
 the date and time of a maintenance event for the EAC-CPF instance. The date and time may be
 recorded manually or machine generated in natural language as well as in machine-readable
 format by use of the standardDateTime attribute."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "maintenanceEvent") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.6</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "standardDateTime") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;created&lt;/eventType&gt;

                    &lt;eventDateTime standardDateTime=&quot;2009-11-30T12:00:00+01:00&quot;&gt;&lt;/eventDateTime&gt;

                    &lt;agentType&gt;human&lt;/agentType&gt;

                    &lt;agent&gt;Bill Stockting&lt;/agent&gt;

                    &lt;eventDescription&gt;Created from original in ISAAR (CPF), 2nd edition: example
                        10&lt;/eventDescription&gt;
                &lt;/maintenanceEvent&gt;

                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;updated&lt;/eventType&gt;

                    &lt;eventDateTime&gt;11th of June 2012 at 9AM&lt;/eventDateTime&gt;

                    &lt;agentType&gt;human&lt;/agentType&gt;

                    &lt;agent&gt;Karin Bredenberg&lt;/agent&gt;

                    &lt;eventDescription&gt;Updated example 10&lt;/eventDescription&gt;
                &lt;/maintenanceEvent&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="eventDescription">
    <h3>eventDescription</h3>
    <h4><?= __d("cpf-doc", "Maintenance Event Description") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The description of a maintenance event in the life of the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An element of &lt;maintenanceEvent&gt; used for describing the maintenance event. The element allows a full description of the maintenance event to be given alongside
 the basic definition of the event in the
 &lt;eventType&gt; element."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "maintenanceEvent") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.9</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;created&lt;/eventType&gt;

                    &lt;eventDateTime standardDateTime=&quot;2009-11-30T12:00:00+01:00&quot;&gt;&lt;/eventDateTime&gt;

                    &lt;agentType&gt;human&lt;/agentType&gt;

                    &lt;agent&gt;Bill Stockting&lt;/agent&gt;

                    &lt;eventDescription&gt;Created from original in ISAAR (CPF), 2nd edition: example 10&lt;/eventDescription&gt;
                &lt;/maintenanceEvent&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="eventType">
    <h3>eventType</h3>
    <h4><?= __d("cpf-doc", "Maintenance Event Type") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The type of maintenance event for the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A required element within &lt;maintenanceEvent&gt; that identifies the type of maintenance event.
 The available values are: &quot;created&quot;, &quot;revised&quot;, &quot;updated&quot;, &quot;deleted&quot;, &quot;derived&quot;, or
 &quot;cancelled&quot;. A discursive description of the event may be given in the optional
 &lt;eventDescription&gt; element."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "On first creation, the event type would be &quot;created&quot;. A &quot;derived&quot; event type is available
 to indicate that the record was derived from another descriptive system. If revisions were
 made to an existing EAC-CPF instance, the event type would be &quot;revised&quot;. Updating a record,
 as a specific type of revision, may be indicated with the value &quot;updated&quot;. Because it is
 important to be clear what has happened to records particularly when sharing and making
 links between them, other event types include &quot;deleted&quot; for records that are deleted from a
 system or &quot;cancelled&quot; for records that are marked as not current (obsolete or rejected) but
 kept for reference."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "&quot;cancelled&quot; or &quot;created&quot; or &quot;deleted&quot; or &quot;derived&quot; or &quot;revised&quot; or &quot;updated&quot;"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "maintenanceEvent") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.9</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;created&lt;/eventType&gt;

                    &lt;eventDateTime standardDateTime=&quot;2009-11-30T12:00:00+01:00&quot;&gt;&lt;/eventDateTime&gt;

                    &lt;agentType&gt;human&lt;/agentType&gt;

                    &lt;agent&gt;Bill Stockting&lt;/agent&gt;

                    &lt;eventDescription&gt;Created from original in ISAAR (CPF), 2nd edition: example
                        10&lt;/eventDescription&gt;
                &lt;/maintenanceEvent&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="existDates">
    <h3>existDates</h3>
    <h4><?= __d("cpf-doc", "Dates of Existence") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The dates of existence of the entity being described, such as dates of
 establishment and dissolution for corporate bodies and dates of birth and death or floruit
 for persons."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The dates of existence of the entity being described, such as dates of establishment and
 dissolution for corporate bodies and dates of birth and death or floruit for persons.
 &lt;existDates&gt; may contain actual or approximate dates expressed through its
 elements &lt;date&gt;, &lt;dateRange&gt;, or &lt;dateSet&gt;. A
 &lt;descriptiveNote&gt; may be included if a fuller explanation of the dates of
 existence is needed."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Use the &lt;date&gt; element to record the date of a single event, such as a date of
 birth or date of incorporation. Use &lt;dateRange&gt; to encode a pair of inclusive
 dates. Use &lt;dateSet&gt; to encode more complex date expressions that intermix
 &lt;date&gt; and &lt;dateRange&gt; elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Do not confuse with &lt;useDates&gt;, which is a child element of &lt;nameEntry&gt;
 or &lt;nameEntryParallel&gt; and represents the dates of use for a particular name or set
 of names."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "date, dateRange, dateSet, descriptiveNote") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.1</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;existDates&gt;
                    &lt;dateRange&gt;
                        &lt;fromDate standardDate=&quot;1868&quot;&gt;1868&lt;/fromDate&gt;

                        &lt;toDate standardDate=&quot;1936&quot;&gt;1936&lt;/toDate&gt;
                    &lt;/dateRange&gt;

                    &lt;descriptiveNote&gt;
                        &lt;p&gt;The company was in business these years&lt;/p&gt;
                    &lt;/descriptiveNote&gt;
                &lt;/existDates&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="fromDate">
    <h3>fromDate</h3>
    <h4><?= __d("cpf-doc", "From Date") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The beginning date in a date range."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The beginning date in a date range, &lt;fromDate&gt; may contain actual or approximate
 dates expressed as a month, day, or year in any format. A standard numerical form of the
 date (YYYYMMDD, etc.) may be specified with the standardDate attribute. The
 notBefore and notAfter attributes may be used to indicate
 uncertainty."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "dateRange") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "notAfter") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "notBefore") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "standardDate") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;existDates&gt;
                    &lt;dateRange&gt;
                        &lt;fromDate standardDate=&quot;1868&quot;&gt;1868&lt;/fromDate&gt;

                        &lt;toDate standardDate=&quot;1936&quot;&gt;1936&lt;/toDate&gt;
                    &lt;/dateRange&gt;
                &lt;/existDates&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="function">
    <h3>function</h3>
    <h4><?= __d("cpf-doc", "Function") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element that provides information about a function, activity, role, or
 purpose performed or manifested by the CPF entity being described."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A &lt;function&gt; element is a wrapper element used to encode an index term, using the
 child element &lt;term&gt;. Terms are used to identify the functions, processes,
 activities, tasks, or transactions performed by the CPF entity described in the EAC-CPF instance. They may be drawn
 from
 controlled vocabularies or may be natural language terms. Associated date or date range
 (&lt;date&gt;, &lt;dateRange&gt; or &lt;dateSet&gt;) and place(s)
 (&lt;placeEntry&gt;) may be included to further constrain the term’s meaning. A
 &lt;descriptiveNote&gt; may be included if a textual explanation is needed."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A single &lt;function&gt; element may be encoded directly within
 &lt;description&gt;. Alternatively, multiple &lt;function&gt; elements may be grouped
 within a &lt;functions&gt; element that facilitates manipulating them as a group."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Functions may alternatively be described in discursive form in &lt;biogHist&gt;. The &lt;function&gt; element should be used whenever separate
 semantic processing of information about functions is required."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "As a further alternative, descriptions of functions may form discrete components in an
 archival descriptive system. Such descriptions should be compiled in accordance with the
 International Standard for Describing Functions (ISDF) and will typically be described in
 another encoding language. In such a system, use &lt;functionRelation&gt; to point from
 the EAC-CPF entity to the related function description."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "citation, date, dateRange, dateSet, descriptiveNote, placeEntry, term"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description, functions") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.5</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;function&gt;
                    &lt;term&gt;Estate ownership&lt;/term&gt;

                    &lt;descriptiveNote&gt;Social, political, and cultural role typical of landed aristocracy
                        in England. The first Viscount Campden amassed a large fortune in trade in London and
                        purchased extensive estates, including Exton (Rutland), and Chipping Campden
                        (Gloucestershire). The Barham Court (Kent) estate was the acquisition of the first Baron
                        Barham, a successful admiral and naval administrator (First Lord of the Admiralty
                        1805).&lt;/descriptiveNote&gt;
                &lt;/function&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="functionRelation">
    <h3>functionRelation</h3>
    <h4><?= __d("cpf-doc", "Function Relation") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element designed to encode a relationship between a function and the CPF entity described in the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;functionRelation&gt; element contains the description of a function related to
 the described CPF entity. Such related functions are typically described in another encoding
 language. Use &lt;objectXMLWrap&gt; to incorporate XML elements from any XML namespace or
 &lt;objectBinWrap&gt; for base64-encoded binary data. A &lt;relationEntry&gt; element
 is provided for textual identification of the related function."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Use the &lt;date&gt;, &lt;dateRange&gt;, or &lt;dateSet&gt; elements for
 specifying when the relation applied and the &lt;placeEntry&gt; element for relevant
 location information. A &lt;descriptiveNote&gt; element may be included for a more detailed explanation of the relationship."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The functionRelationType attribute is used to specify the nature of the
 relationship that exists between the &lt;function&gt; and the CPF entity described in the EAC-CPF instance.
 Values are chosen from a closed list."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "date, dateRange, dateSet, descriptiveNote, objectBinWrap, objectXMLWrap, placeEntry, relationEntry"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "relations") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "ISDF Chapter 6") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "functionRelationType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "lastDateTimeVerified") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:actuate") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:arcrole") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:href") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:role") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:show") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:title") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:type") ?></td>
            <td><?= __d("cpf-doc", "Required (if any XLINK attributes used)") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;functionRelation functionRelationType=&quot;performs&quot;&gt;
                    &lt;relationEntry&gt;Alumni communication management, University of
                        Glasgow&lt;/relationEntry&gt;

                    &lt;descriptiveNote&gt;
                        &lt;p&gt;The management of the University&#039;s communication with its
                            alumni.&lt;/p&gt;
                    &lt;/descriptiveNote&gt;
                &lt;/functionRelation&gt;

                &lt;functionRelation functionRelationType=&quot;controls&quot;&gt;
                    &lt;relationEntry&gt;Εstablishment and abolishment of schools&lt;/relationEntry&gt;

                    &lt;descriptiveNote&gt;
                        &lt;p&gt;The second responsibility of the Department is to control the establishment and abolishment of schools.&lt;/p&gt;
                    &lt;/descriptiveNote&gt;
                &lt;/functionRelation&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="functions">
    <h3>functions</h3>
    <h4><?= __d("cpf-doc", "Functions") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A grouping element used to bundle together individual &lt;function&gt;
 elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Use the optional &lt;functions&gt; element to group together one or more occurrences of
 &lt;function&gt; so that they can be manipulated as a package. A single
 &lt;function&gt; element may stand alone or may be wrapped within &lt;functions&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The optional &lt;citation&gt;, &lt;list&gt;, and &lt;outline&gt; elements are
 used to accommodate greater complexity in expressing or representing the functions,
 processes, activities, tasks, or transactions being described. A simpler discursive
 expression of the functions may be encoded in one or more &lt;p&gt; elements."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "citation, descriptiveNote, function, list, outline, p") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.5</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;functions&gt;
                    &lt;function&gt;
                        &lt;term&gt;Indigenous land rights plaintiff&lt;/term&gt;
                    &lt;/function&gt;

                    &lt;function&gt;
                        &lt;term&gt;Indigenous arts administrator&lt;/term&gt;
                    &lt;/function&gt;
                &lt;/functions&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="generalContext">
    <h3>generalContext</h3>
    <h4><?= __d("cpf-doc", "General Context") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element that encodes information about the general social and cultural
 context of the CPF entity being described."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The element &lt;generalContext&gt; may contain a &lt;list&gt;, &lt;outline&gt;,
 or &lt;p&gt; element to format information about the social, cultural, economic,
 political, and/or historical milieu in which the CPF entity being described existed. The general context provides
 wide latitude to record contextual information not specifically accommodated by other elements container in &lt;description&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The optional &lt;citation&gt;, &lt;list&gt;, and &lt;outline&gt; elements are
 used to accommodate greater complexity in expressing or representing the general context
 being described. A simpler discursive expression of the general context may be encoded as
 one or more &lt;p&gt; elements."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "citation, list, outline, p") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.8</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;generalContext&gt;
                    &lt;p&gt;Edward Koiko Mabo was born in 1936 on the island of Mer, one of the Murray
                        Islands, which are located at the eastern extremity of Torres Strait. In June 1992, six
                        months after his death, Mabo achieved national prominence as the successful principal
                        plaintiff in the landmark High Court ruling on native land title. The High Court ruling,
                        for the first time, gave legal recognition to the fact that indigenous land ownership
                        existed in Australia before European settlement and that, in some cases, this land tenure
                        was not subsequently extinguished by the Crown.&lt;/p&gt;
                &lt;/generalContext&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="identity">
    <h3>identity</h3>
    <h4><?= __d("cpf-doc", "Identity") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A wrapper element for the name or names related to the identity being described within the &lt;cpfDescription&gt; element in the EAC-CPF instance. In case of multiple identities, a separate &lt;identity&gt; element is contained in each of the &lt;cpfDescription&gt; elements of instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "&lt;identity&gt; is a container element used to group the elements necessary to encode the name or names related to the identity of the CPF
 entity within the &lt;cpfDescription&gt; element. Within this element the &lt;entityType&gt; element is required and specifies the type of entity (i.e., corporateBody, family, or person). One or more &lt;nameEntry&gt; elements and / or one or more &lt;nameEntryParallel&gt; elements specifying names by which the identity is known is also required. An optional &lt;entityId&gt; is available for any identifiers associated with the CPF entity."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "All names by which the identity, being described within one &lt;cpfDescription&gt; element in the EAC-CPF instance is known are provided within this element. Within &lt;identity&gt;, each of the names, whether authorized or alternatives, should be recorded in a separate &lt;nameEntry&gt; element."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "In addition to needing to accommodate one or more names used for or by the CPF entity,
 &lt;identity&gt; may accommodate two or more parallel names in different languages or
 scripts. In countries where there is more than one official language, such as Canada or
 Switzerland, names of CPF entities are frequently provided in more than one language. Within
 &lt;identity&gt;, a &lt;nameEntryParallel&gt; element should be used to group two or
 more &lt;nameEntry&gt; elements that represent parallel forms of the name of the CPF entity
 being described."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Within &lt;identity&gt;, a &lt;descriptiveNote&gt; element may be used to record
 other information in a textual form that assists in the identification of the CPF entity."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "In case of multiple identities of the same entity in one EAC-CPF instance, a separate &lt;identity&gt; element is contained in each of the &lt;cpfDescription&gt; elements of the EAC-CPF instance."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "descriptiveNote, entityId, entityType, nameEntry, nameEntryParallel"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "cpfDescription") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.1</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "identityType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:base") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;identity&gt;
                    &lt;entityId&gt;CLU-SC-000008&lt;/entityId&gt;

                    &lt;entityType&gt;person&lt;/entityType&gt;

                    &lt;nameEntry&gt;
                        &lt;part&gt;Brown, Bob&lt;/part&gt;

                        &lt;useDates&gt;
                            &lt;dateRange&gt;
                                &lt;fromDate standardDate=&quot;1886&quot;&gt;1886&lt;/fromDate&gt;

                                &lt;toDate standardDate=&quot;1959&quot;&gt;1959&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/useDates&gt;

                        &lt;authorizedForm&gt;AACR2&lt;/authorizedForm&gt;
                    &lt;/nameEntry&gt;

                    &lt;nameEntry&gt;
                        &lt;part&gt;Brown, Robert Carlton&lt;/part&gt;

                        &lt;useDates&gt;
                            &lt;dateRange&gt;
                                &lt;fromDate standardDate=&quot;1886&quot;&gt;1886&lt;/fromDate&gt;

                                &lt;toDate standardDate=&quot;1959&quot;&gt;1959&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/useDates&gt;

                        &lt;alternativeForm&gt;AACR2&lt;/alternativeForm&gt;
                    &lt;/nameEntry&gt;
                &lt;/identity&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="item">
    <h3>item</h3>
    <h4><?= __d("cpf-doc", "Item") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A formatting element that encodes the individual entries in a &lt;list&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;item&gt; element is used for general lists within descriptive elements and
 within levels in an outline. Do not confuse with &lt;chronItem&gt;, which encodes entries
 within a structured chronology, &lt;chronList&gt;."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text], span") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "level, list") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "Within level:: Mandatory, Non-repeatableWithin list:: Mandatory, Repeatable"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;list&gt;
                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;1450-1950&lt;/span&gt;

                        (1929)&lt;/item&gt;

                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;Globe Gliding&lt;/span&gt;

                        (1930)&lt;/item&gt;

                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;Gems&lt;/span&gt;
 (1931) &lt;/item&gt;

                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;Words&lt;/span&gt;

                        (1931)&lt;/item&gt;

                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;Demonics&lt;/span&gt;
 (1931)&lt;/item&gt;

                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;Can We Co-operate&lt;/span&gt;

                        (1942)&lt;/item&gt;

                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;Amazing Amazon&lt;/span&gt;
 (1942)
                        &lt;/item&gt;
                &lt;/list&gt;


                &lt;outline&gt;
                    &lt;level&gt;
                        &lt;item&gt;I.&lt;/item&gt;
                    &lt;/level&gt;

                    &lt;level&gt;
                        &lt;item&gt;II.&lt;/item&gt;

                        &lt;level&gt;
                            &lt;item&gt;A.&lt;/item&gt;
                    &lt;/level&gt;

                        &lt;level&gt;
                            &lt;item&gt;B.&lt;/item&gt;

                            &lt;level&gt;
                                &lt;item&gt;1.&lt;/item&gt;
                        &lt;/level&gt;

                            &lt;level&gt;
                                &lt;item&gt;2.&lt;/item&gt;
                        &lt;/level&gt;
                            &lt;/level&gt;
                            &lt;/level&gt;
                &lt;/outline&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="language">
    <h3>language</h3>
    <h4><?= __d("cpf-doc", "Language") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The specification of a particular language used in the EAC-CPF instance or
 in the creative work of the CPF entity being described."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A required element within &lt;languageDeclaration&gt; that gives the main language in
 which the EAC-CPF instance is written."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A required element within &lt;languageUsed&gt; that gives the language or languages
 used by the CPF entity being described."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The content of the languageCode attribute must be given in the form of valid
 code from ISO 639-2b."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "languageDeclaration, languageUsed") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.7</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "languageCode") ?></td>
            <td><?= __d("cpf-doc", "Required") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;languageDeclaration&gt;
                    &lt;language languageCode=&quot;eng&quot;&gt;English&lt;/language&gt;

                    &lt;script scriptCode=&quot;Latn&quot;&gt;Latin&lt;/script&gt;
                &lt;/languageDeclaration&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="languageDeclaration">
    <h3>languageDeclaration</h3>
    <h4><?= __d("cpf-doc", "Language Declaration") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The declaration of the predominant language and script used in the EAC-CPF
 instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A wrapper element within &lt;control&gt; that declares the primary language and script
 in which an EAC-CPF instance is written in the required &lt;language&gt; and
 &lt;script&gt; elements. Any comments about the languages and scripts in which the
 EAC-CPF instance is written may be included in the optional &lt;descriptiveNote&gt;
 element."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "descriptiveNote, language, script") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "control") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.7</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;languageDeclaration&gt;
                    &lt;language languageCode=&quot;eng&quot;&gt;English&lt;/language&gt;

                    &lt;script scriptCode=&quot;Latn&quot;&gt;Latin&lt;/script&gt;
                &lt;/languageDeclaration&gt;


                &lt;languageDeclaration&gt;
                    &lt;language languageCode=&quot;gre&quot;&gt;Greek, Modern (1453-)&lt;/language&gt;

                    &lt;script scriptCode=&quot;Grek&quot;&gt;Greek&lt;/script&gt;
                &lt;/languageDeclaration&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="languagesUsed">
    <h3>languagesUsed</h3>
    <h4><?= __d("cpf-doc", "Languages Used") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A grouping element used to bundle together individual
 &lt;languageUsed&gt; elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Use the optional &lt;languagesUsed&gt; element to group together one or more
 occurrences of &lt;languageUsed&gt; so that they can be manipulated as a package. A
 single &lt;languageUsed&gt; may be alone or may be wrapped within
 &lt;languagesUsed&gt;."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "descriptiveNote, languageUsed") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;languagesUsed&gt;
                    &lt;languageUsed&gt;
                        &lt;language languageCode=&quot;eng&quot;&gt;English&lt;/language&gt;

                        &lt;script scriptCode=&quot;Latn&quot;&gt;Latin&lt;/script&gt;
                    &lt;/languageUsed&gt;

                    &lt;languageUsed&gt;
                        &lt;language languageCode=&quot;spa&quot;&gt;Spanish&lt;/language&gt;

                        &lt;script scriptCode=&quot;Latn&quot;&gt;Latin&lt;/script&gt;
                    &lt;/languageUsed&gt;

                    &lt;descriptiveNote&gt;
                        &lt;p&gt;Published works in English and Spanish.&lt;/p&gt;
                    &lt;/descriptiveNote&gt;
                &lt;/languagesUsed&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="languageUsed">
    <h3>languageUsed</h3>
    <h4><?= __d("cpf-doc", "Language Used") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The language and script used by the CPF entity being described."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "&lt;languageUsed&gt; is an element within &lt;description&gt; used to indicate the
 language and script in which the CPF entity being described was creative or productive. Use the
 &lt;language&gt; element to specify the language and a corresponding
 &lt;script&gt; element for the script."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Do not confuse with &lt;languageDeclaration&gt; which refers to the language and script
 of the EAC-CPF instance."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "descriptiveNote, language, script") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description, languagesUsed") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;languageUsed&gt;
                    &lt;language languageCode=&quot;eng&quot;&gt;English&lt;/language&gt;

                    &lt;script scriptCode=&quot;Latn&quot;&gt;Latin&lt;/script&gt;
                &lt;/languageUsed&gt;


                &lt;languageUsed&gt;
                    &lt;language languageCode=&quot;gre&quot;&gt;Greek, Modern (1453-)&lt;/language&gt;

                    &lt;script scriptCode=&quot;Grek&quot;&gt;Greek&lt;/script&gt;
                &lt;/languageUsed&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="legalStatus">
    <h3>legalStatus</h3>
    <h4><?= __d("cpf-doc", "Legal Status") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element used to encode information about the legal status of a corporate
 body."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A &lt;legalStatus&gt; element is a wrapper element used to encode an index term, using the element &lt;term&gt;. The legal status of a corporate body is typically defined
 and granted by authorities or through authorized agencies. Enter terms in accordance with
 provisions of the controlling legislation. Terms may be drawn from controlled vocabularies
 or may be natural language terms."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Associated date or date range (&lt;date&gt;, &lt;dateRange&gt; or
 &lt;dateSet&gt;) and place(s) (&lt;placeEntry&gt;) may be included to further
 constrain the term&#039;s meaning. A &lt;descriptiveNote&gt; element may be included if fuller
 textual explanation is needed."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A single &lt;legalStatus&gt; may be encoded directly within &lt;description&gt;.
 Alternatively, multiple &lt;legalStatus&gt; elements may be grouped within a
 &lt;legalStatuses&gt; element that facilitates manipulating them as a group."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Legal statuses may alternatively be described in discursive form in the &lt;biogHist&gt;. The &lt;legalStatus&gt; element should be used whenever separate semantic processing of information about legal statuses is required."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "citation, date, dateRange, dateSet, descriptiveNote, placeEntry, term"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description, legalStatuses") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.4</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;legalStatus&gt;
                    &lt;term&gt;Department of State&lt;/term&gt;
                &lt;/legalStatus&gt;

                &lt;legalStatus&gt;
                    &lt;term&gt;Organismo de la Administracion Central del Estado&lt;/term&gt;

                    &lt;date standardDate=&quot;1769&quot;&gt;1769&lt;/date&gt;
                &lt;/legalStatus&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="legalStatuses">
    <h3>legalStatuses</h3>
    <h4><?= __d("cpf-doc", "Legal Statuses") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A grouping element used to bundle together individual &lt;legalStatus&gt;
 elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Use the optional &lt;legalStatuses&gt; to group together one or more occurrences of
 &lt;legalStatus&gt; so that they can be manipulated as a package. A single
 &lt;legalStatus&gt; may stand alone or may be wrapped within
 &lt;legalStatuses&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The optional &lt;citation&gt;, &lt;list&gt;, and &lt;outline&gt; elements are
 used to accommodate greater complexity in expressing or representing the legal statuses
 being described. A simpler discursive expression of the legal statuses may be encoded as one
 or more &lt;p&gt; elements."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "citation, descriptiveNote, legalStatus, list, outline, p") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.4</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;legalStatuses&gt;
                    &lt;legalStatus&gt;
                        &lt;term&gt;Private limited liability company&lt;/term&gt;

                        &lt;dateRange&gt;
                            &lt;fromDate standardDate=&quot;1941&quot;&gt;1941&lt;/fromDate&gt;

                            &lt;toDate standardDate=&quot;1948&quot;&gt;1948&lt;/toDate&gt;
                        &lt;/dateRange&gt;
                    &lt;/legalStatus&gt;

                    &lt;legalStatus&gt;
                        &lt;term&gt;Public limited liability company&lt;/term&gt;

                        &lt;dateRange&gt;
                            &lt;fromDate standardDate=&quot;1948&quot;&gt;1948&lt;/fromDate&gt;

                            &lt;toDate standardDate=&quot;2006&quot;&gt;2006&lt;/toDate&gt;
                        &lt;/dateRange&gt;
                    &lt;/legalStatus&gt;

                    &lt;legalStatus&gt;
                        &lt;term&gt;Private limited liability company&lt;/term&gt;

                        &lt;dateRange&gt;
                            &lt;fromDate standardDate=&quot;2006&quot;&gt;2006&lt;/fromDate&gt;

                            &lt;toDate standardDate=&quot;2008&quot;&gt;2008&lt;/toDate&gt;
                        &lt;/dateRange&gt;
                    &lt;/legalStatus&gt;
                &lt;/legalStatuses&gt;


                &lt;legalStatuses&gt;
                    &lt;legalStatus&gt;
                        &lt;term&gt;EPIC&lt;/term&gt;

                        &lt;dateRange&gt;
                            &lt;fromDate notBefore=&quot;1946-04&quot;&gt;avril 1946&lt;/fromDate&gt;

                            &lt;toDate notAfter=&quot;2004-11&quot;&gt;novembre 2004&lt;/toDate&gt;
                        &lt;/dateRange&gt;

                        &lt;descriptiveNote&gt;
                            &lt;p&gt;Établissement public à caractère industriel et commercial&lt;/p&gt;
                        &lt;/descriptiveNote&gt;
                    &lt;/legalStatus&gt;

                    &lt;legalStatus&gt;
                        &lt;term&gt;SA&lt;/term&gt;

                        &lt;dateRange&gt;
                            &lt;fromDate notBefore=&quot;2004-11&quot;&gt;novembre 2004&lt;/fromDate&gt;

                            &lt;toDate&gt;&lt;/toDate&gt;
                        &lt;/dateRange&gt;

                        &lt;descriptiveNote&gt;
                            &lt;p&gt;Société anonyme à capitaux publics&lt;/p&gt;
                        &lt;/descriptiveNote&gt;
                    &lt;/legalStatus&gt;
                &lt;/legalStatuses&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="level">
    <h3>level</h3>
    <h4><?= __d("cpf-doc", "Level") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A required element within &lt;outline&gt; that delineates the outline
 format."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Within an &lt;outline&gt;, multiple &lt;level&gt; elements are used to indicate the
 hierarchical levels of information that comprise the outline. When more than one
 hierarchical level exists, successive &lt;level&gt; exists, successive &lt;level&gt;
 elements are nested recursively to identify the layers of content. Each &lt;level&gt;, in
 turn, contains one &lt;item&gt; element that convey the information content of
 the outline."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "item, level") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "level, outline") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;outline&gt;
                    &lt;level&gt;
                        &lt;item&gt;I.&lt;/item&gt;
                    &lt;/level&gt;

                    &lt;level&gt;
                        &lt;item&gt;II.&lt;/item&gt;

                        &lt;level&gt;
                            &lt;item&gt;A.&lt;/item&gt;
                    &lt;/level&gt;

                        &lt;level&gt;
                            &lt;item&gt;B.&lt;/item&gt;

                            &lt;level&gt;
                                &lt;item&gt;1.&lt;/item&gt;
                        &lt;/level&gt;

                            &lt;level&gt;
                                &lt;item&gt;2.&lt;/item&gt;
                        &lt;/level&gt;
                            &lt;/level&gt;
                            &lt;/level&gt;
                &lt;/outline&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="list">
    <h3>list</h3>
    <h4><?= __d("cpf-doc", "List") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A wrapper element to encode a simple list consisting of one or more
 &lt;item&gt; elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;list&gt; element is used for general lists that can be embedded with a large
 number of descriptive elements. Lists are comprised of one or more &lt;item&gt; elements.
 Do not confuse with &lt;chronList&gt;, which encodes a structured chronology."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "item") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "biogHist, functions, generalContext, legalStatuses, localDescriptions, mandates, occupations, places, structureOrGenealogy"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;list&gt;
                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;1450-1950&lt;/span&gt;

                        (1929)&lt;/item&gt;

                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;Globe Gliding&lt;/span&gt;

                        (1930)&lt;/item&gt;

                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;Gems&lt;/span&gt;
 (1931) &lt;/item&gt;

                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;Words&lt;/span&gt;
 (1931)&lt;/item&gt;

                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;Demonics&lt;/span&gt;
 (1931)&lt;/item&gt;

                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;Can We Co-operate&lt;/span&gt;

                        (1942)&lt;/item&gt;

                    &lt;item&gt;
                        &lt;span style=&quot;font-style:italic&quot;&gt;Amazing Amazon&lt;/span&gt;
 (1942)
                        &lt;/item&gt;
                &lt;/list&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="localControl">
    <h3>localControl</h3>
    <h4><?= __d("cpf-doc", "Local Control") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Any additional control entry necessary to accommodate local practice."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An element used to record and define any control information necessary to accommodate
 local practice, in addition to the elements otherwise made available within
 &lt;control&gt;. The value of the entry should be given in a &lt;term&gt; element and an
 associated date can be given either as a single date (&lt;date&gt;) or a date range
 (&lt;dateRange&gt;)."
        ) ?></p>
    <p><?= __d("cpf-doc", "The type of entry may be defined using the localType attribute.") ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "date, dateRange, term") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "control") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;localControl localType=&quot;detailLevel&quot;&gt;
                    &lt;term&gt;minimal&lt;/term&gt;
                &lt;/localControl&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="localDescription">
    <h3>localDescription</h3>
    <h4><?= __d("cpf-doc", "Local Description") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Used to extend the descriptive categories to others available
 in a local system. Its meaning will depend on the context in which it occurs."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "&lt;localDescription&gt; provides a means to extend the list of description elements
 defined by ISAAR (CPF) and specified in the EAC-CPF schema. It should be used to record
 structured index terms rather than discursive text. It contains a child element
 &lt;term&gt;, which may be drawn from controlled vocabularies or may be natural language
 terms. Associate date or date range (&lt;date&gt;, &lt;dateRange&gt; or
 &lt;dateSet&gt;) and place(s) (&lt;placeEntry&gt;) may be included to further
 constrain the term&#039;s meaning. A &lt;descriptiveNote&gt; may be included if a fuller
 textual explanation is needed."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;localDescription&gt; element should be used whenever in a local system a separate semantic process
 of the descriptive information is required that cannot be accommodated by the existing categories available in EAC-CPF."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "citation, date, dateRange, dateSet, descriptiveNote, placeEntry, term"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description, localDescriptions") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Required") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;localDescription localType=&quot;http://....fr/eac-cpf/localType/nationalité&quot;&gt;
                    &lt;term vocabularySource=&quot;http://....fr/eac-cpf/localType/nationalité#French&quot;&gt;French&lt;/term&gt;

                    &lt;placeEntry countryCode=&quot;FR&quot; vocabularySource=&quot;http://....fr/registerOfFrenchPlaceNames#France&quot;&gt;France&lt;/placeEntry&gt;
                &lt;/localDescription&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="localDescriptions">
    <h3>localDescriptions</h3>
    <h4><?= __d("cpf-doc", "Local Descriptions") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A grouping element used to bundle together individual
 &lt;localDescription&gt; elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Use the optional &lt;localDescriptions&gt; element to group together one or more
 occurrences of &lt;localDescription&gt; so that they can be manipulated as a package. A
 single &lt;localDescription&gt; may stand alone or may be wrapped within
 &lt;localDescriptions&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The optional &lt;citation&gt;, &lt;list&gt;, and &lt;outline&gt; elements are
 used to accommodate greater complexity in expressing or representing the information being
 described. A simpler discursive expression may be encoded as one or more &lt;p&gt; elements."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "citation, descriptiveNote, list, localDescription, outline, p") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Required") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;localDescriptions localType=&quot;http://....fr/eac-cpf/localType/&quot;&gt;
                    &lt;localDescription localType=&quot;http://....fr/eac-cpf/localType/nationalité&quot;&gt;
                        &lt;term vocabularySource=&quot;http://....fr/eac-cpf/localType/nationalité#French&quot;&gt;French&lt;/term&gt;

                        &lt;placeEntry countryCode=&quot;FR&quot; vocabularySource=&quot;http://....fr/registerOfFrenchPlaceNames#France&quot;&gt;France&lt;/placeEntry&gt;
                    &lt;/localDescription&gt;

                    &lt;localDescription localType=&quot;http://....fr/eac-cpf/localType/citoyenneté&quot;&gt;
                        &lt;term vocabularySource=&quot;http://....fr/eac-cpf/localType/citoyenneté#French&quot;&gt;French&lt;/term&gt;

                        &lt;placeEntry countryCode=&quot;FR&quot; vocabularySource=&quot;http://....fr/registerOfFrenchPlaceNames#France&quot;&gt;France&lt;/placeEntry&gt;
                    &lt;/localDescription&gt;

                    &lt;localDescription localType=&quot;http://....fr/eac-cpf/localType/citoyenneté&quot;&gt;
                        &lt;term vocabularySource=&quot;http://....fr/eac-cpf/localType/citoyenneté#Brazil&quot;&gt;Brazil&lt;/term&gt;

                        &lt;placeEntry countryCode=&quot;BR&quot; vocabularySource=&quot;http://....fr/registerOfFrenchPlaceNames#France&quot;&gt;Brazil&lt;/placeEntry&gt;
                    &lt;/localDescription&gt;
                &lt;/localDescriptions&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="localTypeDeclaration">
    <h3>localTypeDeclaration</h3>
    <h4><?= __d("cpf-doc", "Local Type Declaration") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 &lt;localTypeDeclaration&gt; is used to declare any local conventions used in
 localType in the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;localTypeDeclaration&gt; is available to declare the local conventions and
 controlled vocabularies used in localType in the EAC-CPF instance. The
 &lt;citation&gt; element must be used to cite the resource that lists the used values
 (semantic scope and intention). Any notes relating to how rules or conventions that have been
 used may be given within a &lt;descriptiveNote&gt; element. The &lt;abbreviation&gt;
 element may be used to identify the standard or controlled vocabulary in a coded
 structure."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "abbreviation, citation, descriptiveNote") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "control") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;localTypeDeclaration&gt;
                    &lt;abbreviation&gt;Categorycodes&lt;/abbreviation&gt;

                    &lt;citation xlink:href=&quot;http://nad.ra.se/static/termlistor/Kategorikoder.htm&quot; xlink:type=&quot;simple&quot;&gt;The categorycodes used in Swedish NAD (http://nad.ra.se). To be used in
                        element function&lt;/citation&gt;

                    &lt;descriptiveNote&gt;
                        &lt;p&gt;Codes for categorizing different types of authority records through
                            organizational form, operation, function, archivalorganization etcetera. &lt;/p&gt;
                    &lt;/descriptiveNote&gt;
                &lt;/localTypeDeclaration&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="maintenanceAgency">
    <h3>maintenanceAgency</h3>
    <h4><?= __d("cpf-doc", "Maintenance Agency") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The institution or service responsible for the creation, maintenance, and/or
 dissemination of the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A required element within &lt;control&gt; for wrapping information about the
 institution or service responsible for the creation, maintenance, and/or dissemination of
 the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "This must include the &lt;agencyName&gt; element and it is recommended to include the
 optional &lt;agencyCode&gt; and / or &lt;otherAgencyCode&gt; elements as well to
 unambiguously identify the institution or service. Additional local institutional codes are
 given in &lt;otherAgencyCode&gt;. Any general information about the institution in
 relation to the EAC-CPF instance may also be given in a &lt;descriptiveNote&gt;
 element."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "agencyCode, agencyName, descriptiveNote, otherAgencyCode") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "control") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.2</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;maintenanceAgency&gt;
                    &lt;agencyCode&gt;FR-DAF&lt;/agencyCode&gt;

                    &lt;agencyName&gt;Archives nationales (Paris)&lt;/agencyName&gt;
                &lt;/maintenanceAgency&gt;


                &lt;maintenanceAgency&gt;
                    &lt;otherAgencyCode&gt;GB-058&lt;/otherAgencyCode&gt;

                    &lt;agencyName&gt;The British Library: Manuscript Collections&lt;/agencyName&gt;
                &lt;/maintenanceAgency&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="maintenanceEvent">
    <h3>maintenanceEvent</h3>
    <h4><?= __d("cpf-doc", "Maintenance Event") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A maintenance event in the life of the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An element within &lt;maintenanceHistory&gt; to record information about
 particular maintenance events in the history of the EAC-CPF instance. There will always be
 at least one maintenance event for each instance, usually its creation, and the type of each event must
 be defined in the &lt;eventType&gt; element. Information must also be given about who or
 what carried out, or was otherwise responsible for, the work on the EAC-CPF instance in the
 &lt;agent&gt; and &lt;agentType&gt; elements and when the event took place in the
 &lt;eventDateTime&gt; element. The event may also be described in the
 &lt;eventDescription&gt; element."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "agent, agentType, eventDateTime, eventDescription, eventType") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "maintenanceHistory") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.6 and 5.4.9</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;derived&lt;/eventType&gt;

                    &lt;eventDateTime standardDateTime=&quot;2009-08-30T09:37:17.029-04:00&quot;&gt;&lt;/eventDateTime&gt;

                    &lt;agentType&gt;machine&lt;/agentType&gt;

                    &lt;agent&gt;XSLT ead2cpf.xsl/Saxon B9&lt;/agent&gt;

                    &lt;eventDescription&gt;Derived from EAD instance.&lt;/eventDescription&gt;
                &lt;/maintenanceEvent&gt;

                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;revised&lt;/eventType&gt;

                    &lt;eventDateTime standardDateTime=&quot;2009-07-08T10:45:00-01:00&quot;&gt;2009-07-08 10:45&lt;/eventDateTime&gt;

                    &lt;agentType&gt;human&lt;/agentType&gt;

                    &lt;agent&gt;Claire Sibille&lt;/agent&gt;

                    &lt;eventDescription&gt;Notice convertie en EAC-CPF avec l&#039; éditeur oXygen&lt;/eventDescription&gt;
                &lt;/maintenanceEvent&gt;

                &lt;maintenanceEvent&gt;
                    &lt;eventType&gt;created&lt;/eventType&gt;

                    &lt;eventDateTime standardDateTime=&quot;2001-11-03T12:00:00+01:00&quot;&gt;2001-11/03 12:00&lt;/eventDateTime&gt;

                    &lt;agentType&gt;human&lt;/agentType&gt;

                    &lt;agent&gt;Lina Bountouri&lt;/agent&gt;
                &lt;/maintenanceEvent&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="maintenanceHistory">
    <h3>maintenanceHistory</h3>
    <h4><?= __d("cpf-doc", "Maintenance History") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The history of the creation and maintenance of the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A required wrapper element within &lt;control&gt; to record the history of the creation and maintenance of
 the EAC-CPF instance. There must be at least one &lt;maintenanceEvent&gt; element,
 usually recording the creation of the instance, but there may be many other
 &lt;maintenanceEvent&gt; elements documenting the milestone events or activities in the
 maintenance of the instance."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "maintenanceEvent") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "control") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.6, 5.4.9</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;maintenanceHistory&gt;
                    &lt;maintenanceEvent&gt;
                        &lt;eventType&gt;created&lt;/eventType&gt;

                        &lt;eventDateTime standardDateTime=&quot;2009-06-29T00:20:00.000-00:00&quot;&gt;29 giugno 2009&lt;/eventDateTime&gt;

                        &lt;agentType&gt;human&lt;/agentType&gt;

                        &lt;agent&gt;Salvatore Vassallo&lt;/agent&gt;

                        &lt;eventDescription&gt;Prima codifica dell&#039;espempio in italiano presente nelle ISAAR(CPF)&lt;/eventDescription&gt;
                    &lt;/maintenanceEvent&gt;

                    &lt;maintenanceEvent&gt;
                        &lt;eventType&gt;revised&lt;/eventType&gt;

                        &lt;eventDateTime standardDateTime=&quot;2009-06-29T17:16:00.000-00:00&quot;&gt;29 giugno 2009&lt;/eventDateTime&gt;

                        &lt;agentType&gt;human&lt;/agentType&gt;

                        &lt;agent&gt;Salvatore Vassallo&lt;/agent&gt;

                        &lt;eventDescription&gt;Aggiunte fonti, regole usate e campi di controllo&lt;/eventDescription&gt;
                    &lt;/maintenanceEvent&gt;
                &lt;/maintenanceHistory&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="maintenanceStatus">
    <h3>maintenanceStatus</h3>
    <h4><?= __d("cpf-doc", "Maintenance Status") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The current drafting status of the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A required element within &lt;control&gt; that records the current drafting status of
 an EAC-CPF instance: as an EAC-CPF instance is modified or other events happen to it (as
 recorded in the &lt;maintenanceHistory&gt; element), the maintenance status should also
 be updated to reflect the current drafting status."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "On first creation the status would be &quot;new&quot;, which on revision can be changed to
 &quot;revised&quot;. Because it is important to be clear about what has happened to records particularly
 when sharing and making links between them, a number of status values are available for
 records that are no longer current. A record that is simply deleted from a system can be
 given the status &quot;deleted&quot;, but in cases where a record is marked as not current (obsolete
 or rejected) but kept for reference then it should be given the status &quot;cancelled&quot;. If a
 record is deleted because it has become superseded by two or more records then its
 status should be given as &quot;deletedSplit&quot;, while if it has simply been replaced by a new
 record then &quot;deletedReplaced&quot; is the appropriate status value. A &quot;derived&quot; status value is
 available to indicate that the record was derived from another descriptive system."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "&quot;cancelled&quot; or &quot;deleted&quot; or &quot;deletedReplaced&quot; or &quot;deletedSplit&quot; or &quot;derived&quot; or &quot;new&quot; or &quot;revised&quot;"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "control") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.4</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;maintenanceStatus&gt;new&lt;/maintenanceStatus&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="mandate">
    <h3>mandate</h3>
    <h4><?= __d("cpf-doc", "Mandate") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The source of authority or mandate for the corporate body in terms of its
 powers, functions, responsibilities or sphere of activities, such as a law, directive, or
 charter."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A &lt;mandate&gt; element is a wrapper element used to encode an index term, using the
 element, &lt;term&gt;. Terms are used to identify the source of authority or
 mandate for the corporate body in terms of its powers, functions, responsibilities or sphere
 of activities, such as a law, directive or charter. Terms may be drawn from controlled
 vocabularies or may be natural language terms. Associated date or date range
 (&lt;date&gt;, &lt;dateRange&gt; or &lt;dateSet&gt;) and place(s)
 (&lt;placeEntry&gt;) may be included to further constrain the term&#039;s meaning. A
 &lt;descriptiveNote&gt; element may be included if a fuller explanation is
 needed."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A single &lt;mandate&gt; element may be encoded directly within &lt;description&gt;.
 Alternatively, multiple &lt;mandate&gt; elements may be grouped within
 &lt;mandates&gt;."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "citation, date, dateRange, dateSet, descriptiveNote, placeEntry, term"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description, mandates") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.6</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;mandate&gt;
                    &lt;dateRange&gt;
                        &lt;fromDate standardDate=&quot;1919&quot;&gt;1919&lt;/fromDate&gt;

                        &lt;toDate standardDate=&quot;1925&quot;&gt;1925&lt;/toDate&gt;
                    &lt;/dateRange&gt;

                    &lt;citation&gt;Minnesota. Executive Session Laws 1919 c49&lt;/citation&gt;

                    &lt;descriptiveNote&gt;
                        &lt;p&gt;Board created in 1919 to receive and examine applications for
                            bonuses from Minnesota soldiers.&lt;/p&gt;
                    &lt;/descriptiveNote&gt;
                &lt;/mandate&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="mandates">
    <h3>mandates</h3>
    <h4><?= __d("cpf-doc", "Mandates") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A grouping element used to bundle together individual &lt;mandate&gt;
 elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Use the optional &lt;mandates&gt; element to group together one or more occurrences of
 &lt;mandate&gt; so that they can be manipulated as a package. A single
 &lt;mandate&gt; may stand alone or may be wrapped within &lt;mandates&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The optional &lt;citation&gt;, &lt;list&gt;, and &lt;outline&gt; elements are
 used to accommodate greater complexity in expressing or representing the mandates being
 described. A simpler discursive expression of the mandates may be encoded as one or more
 &lt;p&gt; elements."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "citation, descriptiveNote, list, mandate, outline, p") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.6</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;mandates&gt;
                    &lt;mandate&gt;
                        &lt;term&gt;Instrucciones de 13-VI-1586 por las que se crean y definen las
                            secretarias de Tierra y Mar.&lt;/term&gt;
                    &lt;/mandate&gt;

                    &lt;mandate&gt;
                        &lt;term&gt;Real Decreto de Nueva Planta para el consejo de Guerra de
                            23-IV-1714.&lt;/term&gt;
                    &lt;/mandate&gt;

                    &lt;mandate&gt;
                        &lt;term&gt;Real Decreto de Nueva Planta para el consejo de Guerra de
                            23-VIII-1715.&lt;/term&gt;
                    &lt;/mandate&gt;
                &lt;/mandates&gt;


                &lt;mandates&gt;
                    &lt;mandate&gt;
                        &lt;term&gt;Law 380/1914, &quot;The Establishment of the Greek State Archiving Service&quot;&lt;/term&gt;

                        &lt;date standardDate=&quot;1914&quot;&gt;1914&lt;/date&gt;

                        &lt;descriptiveNote&gt;
                            &lt;p&gt;The General State Archives of Greece were established thanks the efforts
                                of the Professor Spyridon Lambrou and the historian-researcher Yiannis Vlachogiannis with the purpose
                                of &quot;collecting and monitoring documents included in all public archives dating 50 years back&quot;.
                                Their efforts conluded to the production of a law by the Prime Minister Eleftherios Venizelos.&lt;/p&gt;
                        &lt;/descriptiveNote&gt;
                    &lt;/mandate&gt;

                    &lt;mandate&gt;
                        &lt;term&gt;Law 2027/1939&lt;/term&gt;

                        &lt;date standardDate=&quot;1939&quot;&gt;1939&lt;/date&gt;

                        &lt;descriptiveNote&gt;
                            &lt;p&gt;Law 2027/1939 determines the &quot;Reorganization of the General State Archives&quot;.&lt;/p&gt;
                        &lt;/descriptiveNote&gt;
                    &lt;/mandate&gt;

                    &lt;mandate&gt;
                        &lt;term&gt;Law 1946/1991&lt;/term&gt;

                        &lt;date standardDate=&quot;1991&quot;&gt;1991&lt;/date&gt;

                        &lt;descriptiveNote&gt;
                            &lt;p&gt;Law 1946/1991 determines a new legislative frame, which regulates the operation of the General State Archives to this day.
                                The Central Service is structured into departments and Archives are established in prefectures which did not exist till then.
                                &lt;/p&gt;
                        &lt;/descriptiveNote&gt;
                    &lt;/mandate&gt;
                &lt;/mandates&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="multipleIdentities">
    <h3>multipleIdentities</h3>
    <h4><?= __d("cpf-doc", "Multiple Identities") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A wrapper element used to group together more than one
 &lt;cpfDescription&gt; within a single EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A grouping element used to encode more than one &lt;cpfDescription&gt; in a single
 EAC-CPF instance. The use of the &lt;multipleIdentities&gt; element can resolve two
 identity circumstances. First, it can be used to represent more than one identity (including
 official identities) of the same CPF entity each with a separate &lt;cpfDescription&gt;.
 Second, it can be used to represent a collaborative identity that includes multiple
 individuals operating under a shared identity (such as a shared pseudonym)."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "cpfDescription") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "eac-cpf") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:base") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;multipleIdentities&gt;
                    &lt;cpfDescription&gt;
                        &lt;identity identityType=&quot;acquired&quot; localType=&quot;pseudonyme&quot;&gt;
                            &lt;entityType&gt;person&lt;/entityType&gt;

                            &lt;nameEntry lang=&quot;ru&quot; scriptCode=&quot;Latn&quot; transliteration=&quot;ISO9:1995&quot;&gt;
                                &lt;part localType=&quot;élément d&#039;entrée&quot;&gt;Gorki&lt;/part&gt;

                                &lt;part localType=&quot;autre élément&quot;&gt;Maksim&lt;/part&gt;

                                &lt;useDates&gt;
                                    &lt;dateRange&gt;
                                        &lt;fromDate standardDate=&quot;1892&quot;&gt;1892&lt;/fromDate&gt;

                                        &lt;toDate standardDate=&quot;1936&quot;&gt;1936&lt;/toDate&gt;
                                    &lt;/dateRange&gt;
                                &lt;/useDates&gt;
                            &lt;/nameEntry&gt;
                        &lt;/identity&gt;

                        &lt;description&gt;
                            &lt;existDates&gt;
                                &lt;dateRange&gt;
                                    &lt;fromDate standardDate=&quot;1868&quot;&gt;1868&lt;/fromDate&gt;

                                    &lt;toDate standardDate=&quot;1936&quot;&gt;1936&lt;/toDate&gt;
                                &lt;/dateRange&gt;
                            &lt;/existDates&gt;

                            &lt;function&gt;
                                &lt;term&gt;Romancier&lt;/term&gt;
                            &lt;/function&gt;

                            &lt;languageUsed&gt;
                                &lt;language languageCode=&quot;rus&quot;&gt;russe&lt;/language&gt;

                                &lt;script scriptCode=&quot;Cyrl&quot;&gt;&lt;/script&gt;
                            &lt;/languageUsed&gt;
                        &lt;/description&gt;
                    &lt;/cpfDescription&gt;

                    &lt;cpfDescription&gt;
                        &lt;identity identityType=&quot;given&quot; localType=&quot;état civil&quot;&gt;
                            &lt;entityType&gt;person&lt;/entityType&gt;

                            &lt;nameEntry lang=&quot;rus&quot; scriptCode=&quot;Latn&quot; transliteration=&quot;ISO9:1995&quot;&gt;
                                &lt;part localType=&quot;élément d&#039;entrée&quot;&gt;Peškov&lt;/part&gt;

                                &lt;part localType=&quot;autre élément&quot;&gt;Aleksej Maksimovič&lt;/part&gt;
                            &lt;/nameEntry&gt;
                        &lt;/identity&gt;

                        &lt;description&gt;
                            &lt;existDates&gt;
                                &lt;dateRange&gt;
                                    &lt;fromDate standardDate=&quot;1868-03-28&quot;&gt;28 mars 1868&lt;/fromDate&gt;

                                    &lt;toDate standardDate=&quot;1936-06-18&quot;&gt;18 juin 1936&lt;/toDate&gt;
                                &lt;/dateRange&gt;
                            &lt;/existDates&gt;

                            &lt;places&gt;
                                &lt;place&gt;
                                    &lt;placeRole&gt;naissance&lt;/placeRole&gt;

                                    &lt;placeEntry&gt;Nijni-Novgorod (Russie)&lt;/placeEntry&gt;
                                &lt;/place&gt;

                                &lt;place&gt;
                                    &lt;placeRole&gt;décès&lt;/placeRole&gt;

                                    &lt;placeEntry&gt;Gorki (Russie)&lt;/placeEntry&gt;
                                &lt;/place&gt;

                                &lt;place&gt;
                                    &lt;placeRole&gt;nationalité&lt;/placeRole&gt;

                                    &lt;placeEntry countryCode=&quot;SU&quot;&gt;Union Soviétique&lt;/placeEntry&gt;
                                &lt;/place&gt;
                            &lt;/places&gt;

                            &lt;biogHist&gt;
                                &lt;p&gt;Élevé par son oncle maternel à Nijni-Novgorod. S&#039;installe à Kazan en 1884.
                                    Autodidacte. Premiers contacts avec les milieux marxistes et populistes. Retour
                                    à Nijni-Novgorod en 1889 et première arrestation. Entame un premier voyage dans
                                    le sud de la Russie en 1891 et s&#039;installe à Tiflis (1891-1892), avant de revenir
                                    à Nijni-Novgorod (1893-1895 puis 1898). Arrêté un deuixème fois à Tiflis en
                                    1898. Il se rend pour la première fois à Saint-Pétersbourg en 1899. Arrêté une
                                    3e fois à Nijni-Novgorod en 1901, ce qui provoque une campagne de protestations.
                                    Entretient des liens d&#039;amitié avec Cehov et Tol&#039;stoj. Il apporte son soutien
                                    financier au Parti social-démocrate et se rapproche des Bolcheviks après 1905.
                                    Il s&#039;exile à Capri de 1906 à 1913. Rentré en Russie en 1913, il s&#039;exile de
                                    nouveau en 1921 en Allemagne puis en Italie en 1923. Il retourne définitivement
                                    en URSS en 1932.&lt;/p&gt;
                            &lt;/biogHist&gt;
                        &lt;/description&gt;
                    &lt;/cpfDescription&gt;
                &lt;/multipleIdentities&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="nameEntry">
    <h3>nameEntry</h3>
    <h4><?= __d("cpf-doc", "Name Entry") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element containing a name entry for a corporate body, person, or family.
 &lt;nameEntry&gt; is made up of one or more &lt;part&gt; elements so that the CPF entity
 can be identified with certainty and distinguished from others bearing the same or similar
 names."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Within &lt;identity&gt;, the element &lt;nameEntry&gt; is used to record a name by
 which the corporate body, the person, or the family described in the EAC-CPF instance is
 known."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "When &lt;nameEntry&gt; occurs within &lt;nameEntryParallel&gt; it is used to record
 two or more parallel forms (e.g., official forms of the name in different languages and/or
 scripts or transliterated forms of the name). When &lt;nameEntry&gt; is not included within
 &lt;nameEntryParallel&gt; it is used to record the authorized or alternative forms,
 whether standardized or not."
        ) ?></p>
    <p><?= __d("cpf-doc", "Each form of the name is recorded in a separate &lt;nameEntry&gt; element.") ?></p>
    <p><?= __d(
            "cpf-doc",
            "Each &lt;nameEntry&gt; should contain at least one &lt;part&gt; element. Within
 &lt;nameEntry&gt; each of the component parts of a name may be recorded in a separate
 &lt;part&gt; element."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "When &lt;nameEntry&gt; does not occur within &lt;nameEntryParallel&gt;, it may
 include two optional elements, &lt;authorizedForm&gt; and &lt;alternativeForm&gt;, to
 account more precisely for the status of the form of the name contained in the
 &lt;nameEntry&gt; element, as compared to other possible forms of the name contained in
 other &lt;nameEntry&gt; elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;nameEntry&gt; element may also contain a &lt;useDates&gt; element to
 indicate the dates the name was used but only when &lt;nameEntry&gt; is not included within
 &lt;nameEntryParallel&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The attributes scriptCode and xml:lang are used to specify the
 script and the language of each of the names recorded in &lt;nameEntry&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "If the form of the name recorded in &lt;nameEntry&gt; is a transliterated one, the
 attribute transliteration is used to record the conventions or rules applied to
 transliterate this form of the name."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "alternativeForm, authorizedForm, part, preferredForm, useDates") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "identity, nameEntryParallel") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.1.2, 5.1.3, 5.1.4, 5.1.5</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "scriptCode") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "transliteration") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;nameEntry&gt;
                    &lt;part localType=&quot;surname&quot;&gt;Lemoyne&lt;/part&gt;

                    &lt;part localType=&quot;forename&quot;&gt;Francois&lt;/part&gt;

                    &lt;authorizedForm&gt;AFNOR&lt;/authorizedForm&gt;
                &lt;/nameEntry&gt;

                &lt;nameEntry&gt;
                    &lt;part localType=&quot;surname&quot;&gt;Lemoine&lt;/part&gt;

                    &lt;part localType=&quot;forename&quot;&gt;Francois&lt;/part&gt;

                    &lt;alternativeForm&gt;AFNOR&lt;/alternativeForm&gt;
                &lt;/nameEntry&gt;

                &lt;nameEntry&gt;
                    &lt;part&gt;Brown, Bob&lt;/part&gt;

                    &lt;useDates&gt;
                        &lt;dateRange&gt;
                            &lt;fromDate standardDate=&quot;1886&quot;&gt;1886&lt;/fromDate&gt;

                            &lt;toDate standardDate=&quot;1959&quot;&gt;1959&lt;/toDate&gt;
                        &lt;/dateRange&gt;
                    &lt;/useDates&gt;

                    &lt;authorizedForm&gt;AACR2&lt;/authorizedForm&gt;
                &lt;/nameEntry&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="nameEntryParallel">
    <h3>nameEntryParallel</h3>
    <h4><?= __d("cpf-doc", "Name Entry Parallel") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A wrapper element for two or more &lt;nameEntry&gt; elements that
 represent parallel forms of the name."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A wrapper element used to group two or more &lt;nameEntry&gt; elements representing
 parallel forms of the name for the same CPF entity (e.g., official forms of the name in
 different languages and/or scripts, transliterated forms of the name). All those forms
 should have the same status, either authorized or alternative. Two optional elements,
 &lt;authorizedForm&gt; or &lt;alternativeForm&gt; may be used to account more
 precisely for the status of the set of parallel name forms contained in
 &lt;nameEntryParallel&gt;. Do not use for pairing authorized and unauthorized forms of
 the same name (e.g., an authorized form with see references)."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;nameEntryParallel&gt; element may contain a &lt;useDates&gt; element to
 indicate the dates the set of parallel name forms was used."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "alternativeForm, authorizedForm, nameEntry, useDates") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "identity") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.1.3</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;nameEntryParallel&gt;
                    &lt;nameEntry lang=&quot;fr&quot; scriptCode=&quot;Latn&quot;&gt;
                        &lt;part&gt;Institut international des droits de l&#039;homme&lt;/part&gt;

                        &lt;preferredForm&gt;AFNOR_Z44-060&lt;/preferredForm&gt;
                    &lt;/nameEntry&gt;

                    &lt;nameEntry lang=&quot;en&quot; scriptCode=&quot;Latn&quot;&gt;
                        &lt;part&gt;International institute of human rights&lt;/part&gt;
                    &lt;/nameEntry&gt;

                    &lt;nameEntry lang=&quot;sp&quot; scriptCode=&quot;Latn&quot;&gt;
                        &lt;part&gt;Instituto internacional de derechos humanos&lt;/part&gt;
                    &lt;/nameEntry&gt;

                    &lt;authorizedForm&gt;AFNOR_Z44-060&lt;/authorizedForm&gt;
                &lt;/nameEntryParallel&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="objectBinWrap">
    <h3>objectBinWrap</h3>
    <h4><?= __d("cpf-doc", "Object Bin Wrap") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 This element provides a place for a base64-encoded binary representation of
 a resource."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "This element provides a place for a base64-encoded binary representation of a resource.
 The datatype of base64-encoded binary is based on the W3C Schema Part 2: Datatypes. (for which consult the specification
 at http://www.w3.org/TR/xmlschema-2/)"
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "base64Binary") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "cpfRelation, functionRelation, resourceRelation, setComponent, source"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;objectBinWrap&gt; [Base64 Binary code] &lt;/objectBinWrap&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="objectXMLWrap">
    <h3>objectXMLWrap</h3>
    <h4><?= __d("cpf-doc", "Object XML Wrap") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A place for incorporating XML elements from any XML namespace."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "This element provides a place to express data in another XML encoding language. While the
 element is not restricted with respect to namespace, to facilitate interoperability, the XML
 should conform to an open, standard XML schema and a namespace attribute should be present
 on the root element referencing the namespace of the standard."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "any element from any namespace") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "cpfRelation, functionRelation, resourceRelation, setComponent, source"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;objectXMLWrap&gt;
                    &lt;mods xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xsi:schemaLocation=&quot;http://www.loc.gov/mods/v3 http://www.loc.gov/mods/v3/mods-3-3.xsd&quot;&gt;
                        &lt;titleInfo&gt;
                            &lt;title&gt;Artisti trentini tra le due guerre&lt;/title&gt;
                        &lt;/titleInfo&gt;

                        &lt;name&gt;
                            &lt;namePart type=&quot;given&quot;&gt;Nicoletta&lt;/namePart&gt;

                            &lt;namePart type=&quot;family&quot;&gt;Boschiero&lt;/namePart&gt;

                            &lt;role&gt;
                                &lt;roleTerm type=&quot;text&quot;&gt;autore&lt;/roleTerm&gt;
                            &lt;/role&gt;
                        &lt;/name&gt;
                    &lt;/mods&gt;
                &lt;/objectXMLWrap&gt;


                &lt;objectXMLWrap&gt;
                    &lt;bibl xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xsi:schemaLocation=&quot;http://www.tei.org/ns/1.0 tei.xsd&quot; default=&quot;false&quot;&gt;
                        &lt;title&gt;
                            &lt;emph rend=&quot;italic&quot;&gt;Paris d&#039;hier et d&#039;aujourd&#039;hui&lt;/emph&gt;
                        &lt;/title&gt;

                        &lt;respStmt&gt;
                            &lt;resp&gt;photographes&lt;/resp&gt;

                            &lt;name&gt;Roger Henrard&lt;/name&gt;

                            &lt;name&gt;Yann Arthus-Bertrand&lt;/name&gt;
                        &lt;/respStmt&gt;
                    &lt;/bibl&gt;
                &lt;/objectXMLWrap&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="occupation">
    <h3>occupation</h3>
    <h4><?= __d("cpf-doc", "Occupation") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element that provides information about the occupation of the CPF entity
 being described."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An &lt;occupation&gt; element is a wrapper element used to encode an index term, using
 the element &lt;term&gt;. Terms are used to identify an occupation held by the
 CPF entity. Terms may be drawn from controlled vocabularies or may be natural language
 terms. Associated date or date range (&lt;date&gt;, &lt;dateRange&gt; or
 &lt;dateSet&gt;) and place(s) (&lt;placeEntry&gt;) may be included to further
 constrain the term&#039;s meaning. A &lt;descriptiveNote&gt; element may be included if a
 textual explanation needed."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A single &lt;occupation&gt; element may be encoded directly within
 &lt;description&gt;. Alternatively, multiple &lt;occupation&gt; elements may be
 grouped within a &lt;occupations&gt; wrapper that facilitates manipulating them as a
 group."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Occupations may alternatively be described in discursive form in &lt;biogHist&gt;. The &lt;occupation&gt; element should be used whenever
 separate semantic processing of information about occupations is required."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "citation, date, dateRange, dateSet, descriptiveNote, placeEntry, term"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description, occupations") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.5</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;occupations&gt;
                    &lt;occupation&gt;
                        &lt;term&gt;Teacher&lt;/term&gt;
                    &lt;/occupation&gt;

                    &lt;occupation&gt;
                        &lt;term&gt;Railway labourer&lt;/term&gt;
                    &lt;/occupation&gt;
                &lt;/occupations&gt;


                &lt;description&gt;
                    &lt;occupations&gt;
                        &lt;occupation&gt;
                            &lt;term&gt;Writer/Poet &lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1931&lt;/fromDate&gt;

                                &lt;toDate&gt;1971&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Diplomat&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1926&lt;/fromDate&gt;

                                &lt;toDate&gt;1962&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Vice Consul at the General Consulate of the Hellenic Republic in
                                London&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1931&lt;/fromDate&gt;

                                &lt;toDate&gt;1934&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Consul in Koritsa&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1936&lt;/fromDate&gt;

                                &lt;toDate&gt;1938&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Head of the Foreign Press Departement (Sub ministry of Press and Information) in Athens&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1938&lt;/fromDate&gt;

                                &lt;toDate&gt;1941&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Diplomat in the Greek Embassy of Pretoria&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1941&lt;/fromDate&gt;

                                &lt;toDate&gt;1942&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Director of the political office of Viceroy Archbishop Damaskinos and
                                director of the National Theatre&lt;/term&gt;

                            &lt;date&gt;1945&lt;/date&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Embassy Counsellor in Ancara &lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1948&lt;/fromDate&gt;

                                &lt;toDate&gt;1950&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Embassy Counsellor in London&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1951&lt;/fromDate&gt;

                                &lt;toDate&gt;1953&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Appointed as minister to Lebanon (with responsibilities for Libanon, Syria, Jordan, and Iraq&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1952&lt;/fromDate&gt;

                                &lt;toDate&gt;1956&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Head of the Second Department of Politics (Ministry of Foreign Affairs) in Athens&lt;/term&gt;

                            &lt;date&gt;1956&lt;/date&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Royal Greek Ambassador in London&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1957&lt;/fromDate&gt;

                                &lt;toDate&gt;1962&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;
                    &lt;/occupations&gt;

                    &lt;biogHist&gt;
                        &lt;citation&gt;The complete biography of George Seferis can be found in
                            &quot;Beaton, Roderick .
                            &lt;span style=&quot;italics&quot;&gt;George Seferis: Waiting for the Angel – A Biography.&lt;/span&gt;
 New Haven: Yale University Press, 2003.&quot;
                            &lt;/citation&gt;
                    &lt;/biogHist&gt;
                &lt;/description&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="occupations">
    <h3>occupations</h3>
    <h4><?= __d("cpf-doc", "Occupations") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A grouping element used to bundle together individual &lt;occupation&gt;
 elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Use the optional &lt;occupations&gt; element to group together one or more occurrences
 of &lt;occupation&gt; so that they can be manipulated as a package. A single
 &lt;occupation&gt; may stand alone or may be wrapped within &lt;occupations&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The optional &lt;citation&gt;, &lt;list&gt;, and &lt;outline&gt; elements are
 used to accommodate greater complexity in expressing or representing the occupations being
 described. A simpler discursive expression of the occupations may be encoded as one or more
 &lt;p&gt; elements."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "citation, descriptiveNote, list, occupation, outline, p") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.5</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;occupations&gt;
                    &lt;occupation&gt;
                        &lt;term&gt;Teacher&lt;/term&gt;
                    &lt;/occupation&gt;

                    &lt;occupation&gt;
                        &lt;term&gt;Railway labourer&lt;/term&gt;
                    &lt;/occupation&gt;
                &lt;/occupations&gt;


                &lt;description&gt;
                    &lt;occupations&gt;
                        &lt;occupation&gt;
                            &lt;term&gt;Writer/Poet &lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1931&lt;/fromDate&gt;

                                &lt;toDate&gt;1971&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Diplomat&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1926&lt;/fromDate&gt;

                                &lt;toDate&gt;1962&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Vice Consul at the General Consulate of the Hellenic Republic in
                                London&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1931&lt;/fromDate&gt;

                                &lt;toDate&gt;1934&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Consul in Koritsa&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1936&lt;/fromDate&gt;

                                &lt;toDate&gt;1938&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Head of the Foreign Press Departement (Sub ministry of Press and Information) in Athens&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1938&lt;/fromDate&gt;

                                &lt;toDate&gt;1941&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Diplomat in the Greek Embassy of Pretoria&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1941&lt;/fromDate&gt;

                                &lt;toDate&gt;1942&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Director of the political office of Viceroy Archbishop Damaskinos and
                                director of the National Theatre&lt;/term&gt;

                            &lt;date&gt;1945&lt;/date&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Embassy Counsellor in Ancara &lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1948&lt;/fromDate&gt;

                                &lt;toDate&gt;1950&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Embassy Counsellor in London&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1951&lt;/fromDate&gt;

                                &lt;toDate&gt;1953&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Appointed as minister to Lebanon (with responsibilities for Libanon, Syria, Jordan, and Iraq&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1952&lt;/fromDate&gt;

                                &lt;toDate&gt;1956&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Head of the Second Department of Politics (Ministry of Foreign Affairs) in Athens&lt;/term&gt;

                            &lt;date&gt;1956&lt;/date&gt;
                        &lt;/occupation&gt;

                        &lt;occupation&gt;
                            &lt;term&gt;Royal Greek Ambassador in London&lt;/term&gt;

                            &lt;dateRange&gt;
                                &lt;fromDate&gt;1957&lt;/fromDate&gt;

                                &lt;toDate&gt;1962&lt;/toDate&gt;
                            &lt;/dateRange&gt;
                        &lt;/occupation&gt;
                    &lt;/occupations&gt;

                    &lt;biogHist&gt;
                        &lt;citation&gt;The complete biography of George Seferis can be found in
                            &quot;Beaton, Roderick .
                            &lt;span style=&quot;italics&quot;&gt;George Seferis: Waiting for the Angel – A Biography.&lt;/span&gt;
 New Haven: Yale University Press, 2003.&quot;
                            &lt;/citation&gt;
                    &lt;/biogHist&gt;
                &lt;/description&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="otherAgencyCode">
    <h3>otherAgencyCode</h3>
    <h4><?= __d("cpf-doc", "Other Agency Code") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Alternate code representing the institution or service responsible for the
 creation, maintenance, and/or dissemination of the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An element of &lt;maintenanceAgency&gt; within &lt;control&gt; providing an
 alternative and/or local institution code representing the institution or service
 responsible for the creation, maintenance, and/or dissemination of the EAC-CPF instance to
 that given in the &lt;agencyCode&gt; element in the format of the International Standard
 Identifier for Libraries and Related Organizations (ISIL: ISO 15511). The name of the agency
 is given in &lt;agencyName&gt;. The addition of an ISO 3166-1 alpha-2 country code as the
 prefix is recommended to ensure international uniqueness."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "NMTOKEN") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "maintenanceAgency") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;maintenanceAgency&gt;
                    &lt;otherAgencyCode&gt;GB-058&lt;/otherAgencyCode&gt;

                    &lt;agencyName&gt;The British Library: Manuscript Collections&lt;/agencyName&gt;
                &lt;/maintenanceAgency&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="otherRecordId">
    <h3>otherRecordId</h3>
    <h4><?= __d("cpf-doc", "Other Record Identifier") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Alternative record identifiers that may be associated with the EAC-CPF
 instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An element in &lt;control&gt; used to encode record identifiers that are alternatives to the mandatory identifier in &lt;recordId&gt;. These might include the identifiers of merged EAC-CPF instances representing the
 same CPF entity or those of records that are no longer current but had some part in the history
 and maintenance of the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The localType attribute can be used to identify the kind of institution or
 service responsible for each associated record identifier if not the same as that given in
 the &lt;maintenanceAgency&gt; element for this EAC-CPF instance."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "control") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;otherRecordId&gt;ARC-ID-976172&lt;/otherRecordId&gt;

                &lt;otherRecordId localType=&quot;NAD_Code&quot;&gt;SE/RA/10018&lt;/otherRecordId&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="outline">
    <h3>outline</h3>
    <h4><?= __d("cpf-doc", "Outline") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element used within other elements of &lt;description&gt; to encode information in an
 outline format."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "&lt;outline&gt; contains one or more &lt;level&gt; elements, which contain an &lt;item&gt; element or further &lt;level&gt; elements in a hierarchical
 fashion."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "level") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "biogHist, functions, generalContext, legalStatuses, localDescriptions, mandates, occupations, places, structureOrGenealogy"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;outline&gt;
                    &lt;level&gt;
                        &lt;item&gt;I.&lt;/item&gt;
                    &lt;/level&gt;

                    &lt;level&gt;
                        &lt;item&gt;II.&lt;/item&gt;

                        &lt;level&gt;
                            &lt;item&gt;A.&lt;/item&gt;
                    &lt;/level&gt;

                        &lt;level&gt;
                            &lt;item&gt;B.&lt;/item&gt;

                            &lt;level&gt;
                                &lt;item&gt;1.&lt;/item&gt;
                        &lt;/level&gt;

                            &lt;level&gt;
                                &lt;item&gt;2.&lt;/item&gt;
                        &lt;/level&gt;
                            &lt;/level&gt;
                            &lt;/level&gt;
                &lt;/outline&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="p">
    <h3>p</h3>
    <h4><?= __d("cpf-doc", "Paragraph") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A generic element used within other elements of &lt;description&gt; that marks one or more sentences that form a logical prose passage."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A paragraph may be a subdivision of a larger composition, or it may exist alone. It is
 usually typographically distinct. A line space is often left blank before it; the text
 begins on a new line; and the first letter of the first word is often indented, enlarged, or
 both."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;p&gt; element is an important textual feature, which must be used as part of
 any general description within a &lt;descriptiveNote&gt; element and may be used inside
 many of the descriptive elements within &lt;description&gt;. While it generally contains
 discursive text, it may also contain a &lt;span&gt; element to further stress or style
 specific character strings by particular formatting (such as italic or bold, etc.)."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text], span") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "biogHist, descriptiveNote, functions, generalContext, legalStatuses, localDescriptions, mandates, occupations, places, structureOrGenealogy"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;biogHist&gt;
                    &lt;p&gt;Robert Carlton Brown (1886-1959) was a writer, editor, publisher, and traveler.
                        From 1908 to 1917, he wrote poetry and prose for numerous magazines and newspapers in New
                        York City, publishing two pulp novels,
                        &lt;span style=&quot;font-style:italic&quot;&gt;What
                            Happened to Mary&lt;/span&gt;
 and
                        &lt;span style=&quot;font-style:italic&quot;&gt;The
                            Remarkable Adventures of Christopher Poe&lt;/span&gt;
 (1913), and one volume of poetry,

                        &lt;span style=&quot;font-style:italic&quot;&gt;My Marjonary&lt;/span&gt;
 (1916).&lt;/p&gt;

                    &lt;p&gt;During 1918, he traveled extensively in Mexico and Central America, writing for
                        the U.S. Committee of Public Information in Santiago de Chile. In 1919, he moved with his
                        wife, Rose Brown, to Rio de Janeiro, where they founded
                        &lt;span style=&quot;font-style:italic&quot;&gt;Brazilian American&lt;/span&gt;
, a weekly magazine that ran
                        until 1929. With Brown&#039;s mother, Cora, the Browns also established magazines in Mexico
                        City and London:
                        &lt;span style=&quot;font-style:italic&quot;&gt;Mexican American&lt;/span&gt;

                        (1924-1929) and
                        &lt;span style=&quot;font-style:italic&quot;&gt;British American&lt;/span&gt;

                        (1926-1929).&lt;/p&gt;
                &lt;/biogHist&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="part">
    <h3>part</h3>
    <h4><?= __d("cpf-doc", "Part") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 &lt;part&gt; is used to distinguish components of the name of the CPF entity’s name within &lt;nameEntry&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Within &lt;nameEntry&gt; each of the component parts of a name, such as forename,
 surname or honorific title, may be recorded in a separate &lt;part&gt; element.
 &lt;part&gt; may also contain the full name of the entity when it is not possible to
 distinguish the different component parts of the name. The &lt;useDates&gt; element
 should be used for any date information related to the use of the name."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The designation of the information contained in the &lt;part&gt; can be specified by
 the attribute localType."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "nameEntry") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;nameEntry&gt;
                    &lt;part localType=&quot;surname&quot;&gt;Lemoyne&lt;/part&gt;

                    &lt;part localType=&quot;forename&quot;&gt;Francois&lt;/part&gt;

                    &lt;authorizedForm&gt;AFNOR&lt;/authorizedForm&gt;
                &lt;/nameEntry&gt;


                &lt;nameEntry&gt;
                    &lt;part&gt;Elytes, Odysseas&lt;/part&gt;

                    &lt;useDates&gt;
                        &lt;dateRange&gt;
                            &lt;fromDate&gt;1911&lt;/fromDate&gt;

                            &lt;toDate&gt;1996&lt;/toDate&gt;
                        &lt;/dateRange&gt;
                    &lt;/useDates&gt;
                &lt;/nameEntry&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="place">
    <h3>place</h3>
    <h4><?= __d("cpf-doc", "Place") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element that provides information about a place or jurisdiction where the
 CPF entity was based, lived, or with which it had some other significant
 connection."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A &lt;place&gt; element is a wrapper element used to encode an index term placed within the element &lt;placeEntry&gt;. Terms are used to identify the places or jurisdictions
 where the CPF entity was based, lived, or with which it had some other significant
 connection. The &lt;placeRole&gt; element is available to specify the nature of the
 connection of the place with the CPF entity being described, and its use is strongly
 recommended. Terms in &lt;placeEntry&gt; and &lt;placeRole&gt; may be drawn from
 controlled vocabularies or may be natural language terms. These controlled vocabularies can
 be identified with the vocabularySource attribute. Associated date or date range
 (&lt;date&gt;, &lt;dateRange&gt; or &lt;dateSet&gt;) information may be included
 to further constrain the term&#039;s meaning. A &lt;descriptiveNote&gt; may be included if a fuller explanation of the relation between the values is needed. An &lt;address&gt; element is also available for
 specifying a postal or other address."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A single &lt;place&gt; element may be encoded directly within &lt;description&gt;.
 Alternatively, multiple &lt;place&gt; elements may be grouped within a &lt;places&gt;
 wrapper that facilitates manipulating them as a group."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Places may alternatively be described in discursive form in &lt;biogHist&gt;. The &lt;place&gt; element should be used whenever separate semantic
 processing of information about places is required."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "address, citation, date, dateRange, dateSet, descriptiveNote, placeEntry, placeRole"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description, places") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.3</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;place&gt;
                    &lt;placeEntry&gt;Paris, France&lt;/placeEntry&gt;

                    &lt;placeRole&gt;Residence&lt;/placeRole&gt;
                &lt;/place&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="placeEntry">
    <h3>placeEntry</h3>
    <h4><?= __d("cpf-doc", "Place Entry") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element used to encode information about the place or jurisdiction where the CPF entity described in the EAC-CPF instance
 was based, lived, or with which it had some other significant connection."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Places should be identified by the proper noun that commonly designates the place, natural
 feature, or political jurisdiction. It is recommended that place names be taken from
 authorized vocabularies. Within &lt;place&gt;, a companion &lt;placeRole&gt; is
 strongly recommended to describe the nature of the association of the place to the
 entity."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "latitude, longitude, and altitude attributes are
 available for specific geographical data. The accuracy attribute may be used to
 indicate uncertainty. The vocabularySource attribute may be used to indicate the
 controlled vocabulary from which the &lt;placeEntry&gt; term is derived."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;placeEntry&gt; element is repeatable. It is strongly recommended that within
 &lt;place&gt;, the &lt;placeEntry&gt; element be repeated only to represent the same
 place name in various languages, with an accompanying xml:lang attribute."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "chronItem, cpfRelation, function, functionRelation, legalStatus, localDescription, mandate, occupation, place, resourceRelation"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "accuracy") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "altitude") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "countryCode") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "latitude") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "longitude") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "scriptCode") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "transliteration") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "vocabularySource") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;place&gt;
                    &lt;placeRole&gt;Residence&lt;/placeRole&gt;

                    &lt;placeEntry localType=&quot;address&quot;&gt;31 rue Cambon&lt;/placeEntry&gt;

                    &lt;placeEntry localType=&quot;address&quot;&gt;Paris&lt;/placeEntry&gt;

                    &lt;placeEntry localType=&quot;address&quot; countryCode=&quot;FR&quot;&gt;France&lt;/placeEntry&gt;
                &lt;/place&gt;

                &lt;place&gt;
                    &lt;placeRole&gt;Birthplace&lt;/placeRole&gt;

                    &lt;placeEntry latitude=&quot;59.37541&quot; longitude=&quot;17.03371&quot;&gt;Strängnäs&lt;/placeEntry&gt;
                &lt;/place&gt;

                &lt;place&gt;
                    &lt;placeRole&gt;Family seat&lt;/placeRole&gt;

                    &lt;placeEntry vocabularySource=&quot;lcsh&quot; latitude=&quot;55.4667&quot; longitude=&quot;4.3000&quot;&gt;Auchinleck
                        (Scotland)&lt;/placeEntry&gt;

                    &lt;dateRange&gt;
                        &lt;fromDate standardDate=&quot;1504&quot;&gt;1504&lt;/fromDate&gt;

                        &lt;toDate standardDate=&quot;1986&quot;&gt;1986&lt;/toDate&gt;
                    &lt;/dateRange&gt;
                &lt;/place&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="placeRole">
    <h3>placeRole</h3>
    <h4><?= __d("cpf-doc", "Place Role") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element to identify the contextual role a place or jurisdiction encoded in &lt;place&gt; elements has in relation to the CPF entity."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "&lt;placeRole&gt; provides a contextual role for a &lt;placeEntry&gt; within
 &lt;place&gt;. Places should be identified in &lt;placeEntry&gt; by the proper noun that commonly designates
 the place, natural feature, or political jurisdiction. It is strongly recommended that each
 place name is accompanied by a &lt;placeRole&gt; element in order to describe the nature
 of the association of the place to the CPF entity."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The vocabularySource attribute may be used to indicate the controlled
 vocabulary form which the &lt;placeRole&gt; term is derived."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "place") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "lastDateTimeVerified") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "scriptCode") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "transliteration") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "vocabularySource") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;place&gt;
                    &lt;placeRole&gt;Birth&lt;/placeRole&gt;

                    &lt;placeEntry&gt;Brussels, Belgium&lt;/placeEntry&gt;
                &lt;/place&gt;

                &lt;place&gt;
                    &lt;placeRole&gt;Citizenship&lt;/placeRole&gt;

                    &lt;placeEntry&gt;Greece&lt;/placeEntry&gt;
                &lt;/place&gt;

                &lt;place&gt;
                    &lt;placeRole&gt;Residence&lt;/placeRole&gt;

                    &lt;placeEntry countryCode=&quot;GR&quot; vocabularySource=&quot;ISO3166-2&quot;&gt;Greece&lt;/placeEntry&gt;
                &lt;/place&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="places">
    <h3>places</h3>
    <h4><?= __d("cpf-doc", "Places") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A grouping element used to bundle together individual &lt;place&gt;
 elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Use the optional &lt;places&gt; element to group together one or more occurrences of
 &lt;place&gt; so that they can be manipulated as a package. A single &lt;place&gt;
 may stand alone or may be wrapped within &lt;places&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The optional &lt;citation&gt;, &lt;list&gt;, and &lt;outline&gt; elements are
 used to accommodate greater complexity in expressing or representing the places and dates
 being described. A simpler discursive expression of the places may be encoded as one or more
 &lt;p&gt; elements."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "citation, descriptiveNote, list, outline, p, place") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.3</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;places&gt;
                    &lt;place&gt;
                        &lt;placeRole&gt;Residence&lt;/placeRole&gt;

                        &lt;placeEntry&gt;Paris, France&lt;/placeEntry&gt;
                    &lt;/place&gt;

                    &lt;place&gt;
                        &lt;placeRole&gt;Residence&lt;/placeRole&gt;

                        &lt;placeEntry&gt;New York, N.Y.&lt;/placeEntry&gt;
                    &lt;/place&gt;

                    &lt;place&gt;
                        &lt;placeRole&gt;Residence&lt;/placeRole&gt;

                        &lt;placeEntry&gt;Riode Janeiro, Brazil&lt;/placeEntry&gt;
                    &lt;/place&gt;

                    &lt;place&gt;
                        &lt;placeRole&gt;Recidence&lt;/placeRole&gt;

                        &lt;placeEntry countryCode=&quot;SWE&quot;&gt;Eskilstuna&lt;/placeEntry&gt;
                    &lt;/place&gt;
                &lt;/places&gt;

                &lt;places&gt;
                    &lt;place&gt;
                        &lt;placeRole&gt;naissance&lt;/placeRole&gt;

                        &lt;placeEntry&gt;Nijni-Novgorod (Russie)&lt;/placeEntry&gt;
                    &lt;/place&gt;

                    &lt;place&gt;
                        &lt;placeRole&gt;déces&lt;/placeRole&gt;

                        &lt;placeEntry&gt;Gorki (Russie)&lt;/placeEntry&gt;
                    &lt;/place&gt;

                    &lt;place&gt;
                        &lt;placeRole&gt;nationalité&lt;/placeRole&gt;

                        &lt;placeEntry countryCode=&quot;SU&quot;&gt;Union Soviétique&lt;/placeEntry&gt;
                    &lt;/place&gt;
                &lt;/places&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="preferredForm">
    <h3>preferredForm</h3>
    <h4><?= __d("cpf-doc", "Preferred Form of Name") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element used to indicate which of the parallel names recorded within
 &lt;nameEntryParallel&gt;, is the preferred one for display purposes in a given
 context."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "In cases where parallel names are encoded as multiple &lt;nameEntry&gt; elements within
 &lt;nameEntryParallel&gt;, a &lt;nameEntry&gt; may be chosen as preferred in a given
 context. The &lt;preferredForm&gt; element is used only when &lt;nameEntry&gt; occurs
 within &lt;nameEntryParallel&gt;. It is used only to distinguish the preferred form of
 the name to be displayed, as compared to the other authorized parallel names recorded in
 other &lt;nameEntry&gt; elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;preferredForm&gt; element contains an abbreviation of the relevant national,
 international or other convention or rule applied by a given agency and according to which
 the name thus qualified is deemed as preferred to the others. The abbreviations expressed in
 &lt;preferredForm&gt; must be declared within the &lt;conventionDeclaration&gt;
 element in &lt;control&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Within &lt;nameEntryParallel&gt;, &lt;preferredForm&gt; may occur simultaneously
 with &lt;authorizedForm&gt; elements. While the &lt;authorizedForm&gt;, when used
 within &lt;nameEntryParallel&gt;, qualifies collectively the set of the parallel forms
 recorded in the separate &lt;nameEntry&gt; elements, the &lt;preferredForm&gt; element
 is used specifically within the &lt;nameEntry&gt; elements to qualify the preferred
 form(s) of the name that an agency chooses to display."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "NMTOKEN") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "nameEntry (only when it occurs within nameEntryParallel)") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.1.3</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;nameEntryParallel&gt;Institut international des droits de l&#039;hommeAFNOR_Z44-060International institute of human
                    rightsInstituto internacional de derechos humanosAFNOR_Z44-060&lt;/nameEntryParallel&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="publicationStatus">
    <h3>publicationStatus</h3>
    <h4><?= __d("cpf-doc", "Publication Status") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The current publication status of the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The current publication status of the EAC-CPF instance may be specified in the
 &lt;publicationStatus&gt; element. If according to local practice the instance is still
 in the process of being approved for publication, the value should be &quot;inProcess,&quot; while if
 approved for publication, the value should be &quot;approved.&quot;"
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "&quot;inProcess&quot; or &quot;approved&quot;") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "control") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.4</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;publicationStatus&gt;inProcess&lt;/publicationStatus&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="recordId">
    <h3>recordId</h3>
    <h4><?= __d("cpf-doc", "Record Identifier") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The unique identifier for a particular instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A required element within &lt;control&gt; that designates a unique identifier for the
 EAC-CPF instance. The assigning owner ensures the uniqueness of the &lt;recordId&gt; within the EAC-CPF
 descriptive system under its control. The &lt;recordId&gt;, when used in combination with
 the content of the required &lt;agencyCode&gt; element within
 &lt;maintenanceAgency&gt;, will provide a globally unique identifier."
        ) ?></p>
    <p><?= __d("cpf-doc", "Record alternate record identifiers if desired in &lt;otherRecordId&gt;.") ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "NMTOKEN") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "control") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.1</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;recordId&gt;F10219&lt;/recordId&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="relationEntry">
    <h3>relationEntry</h3>
    <h4><?= __d("cpf-doc", "Relation Entry") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A descriptive element for relations that identifies the relationship in a textual form."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A descriptive element for relations that provides discursive text identifying a related CPF entity, a resource created by
 or otherwise related to the named CPF entity (e.g., archival records), or the name of a related function."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "cpfRelation, functionRelation, resourceRelation") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "scriptCode") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "transliteration") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;functionRelation functionRelationType=&quot;performs&quot;&gt;
                    &lt;relationEntry&gt;Alumni communication management, University of
                        Glasgow&lt;/relationEntry&gt;

                    &lt;descriptiveNote&gt;
                        &lt;p&gt;The management of the University&#039;s communication with its alumni.&lt;/p&gt;
                    &lt;/descriptiveNote&gt;
                &lt;/functionRelation&gt;

                &lt;cpfRelation cpfRelationType=&quot;hierarchical-child&quot;&gt;
                    &lt;relationEntry&gt;Supreme Education Council, Ministry of Education&lt;/relationEntry&gt;

                    &lt;dateRange&gt;
                        &lt;fromDate standardDate=&quot;1967&quot;&gt;1967&lt;/fromDate&gt;

                        &lt;toDate standardDate=&quot;1981&quot;&gt;1981&lt;/toDate&gt;
                    &lt;/dateRange&gt;

                    &lt;descriptiveNote&gt;
                        &lt;p&gt;Together with the Minister of Education, has the task of management and
                            supervision over the General Education.&lt;/p&gt;
                    &lt;/descriptiveNote&gt;
                &lt;/cpfRelation&gt;

                &lt;resourceRelation resourceRelationType=&quot;creatorOf&quot; id=&quot;UA013&quot;&gt;
                    &lt;relationEntry&gt;Department of Romance Languages records&lt;/relationEntry&gt;

                    &lt;objectXMLWrap&gt;
                        &lt;ead&gt;
                            &lt;archdesc level=&quot;collection&quot;&gt;
                                &lt;did&gt;
                                    &lt;unittitle&gt;Department of Romance Languages records&lt;/unittitle&gt;

                                    &lt;unitid&gt;UA013&lt;/unitid&gt;
                                &lt;/did&gt;
                            &lt;/archdesc&gt;
                        &lt;/ead&gt;
                    &lt;/objectXMLWrap&gt;
                &lt;/resourceRelation&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="relations">
    <h3>relations</h3>
    <h4><?= __d("cpf-doc", "Relations") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A wrapper element for grouping one or more specific relations, each of them expressed by &lt;cpfRelation&gt;, &lt;resourceRelation&gt;, or &lt;functionRelation&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A wrapper element that groups together one or more relation elements, each of which
 encodes a specific relationship. CPF entities may have relationships with other corporate
 bodies, persons or families, which may be expressed using &lt;cpfRelation&gt;; functions,
 which may be expressed using &lt;functionRelation&gt;; or resources such as archival
 collections, bibliographic resources, or artifacts, which may be expressed using
 &lt;resourceRelation&gt;."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "cpfRelation, functionRelation, resourceRelation") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "cpfDescription") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "The general concept of providing connections from archival authority
 records to other related resources is stated in ISAAR(CPF) 6.0: &quot;Archival authority records
 are created primarily to document the context of records creation. To make this documentation
 useful it is necessary to link the authority records to descriptions of records. Archival
 authority records can also be linked to other relevant information resources.&quot;"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:base") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;relations&gt;
                    &lt;cpfRelation&gt;[...]&lt;/cpfRelation&gt;

                    &lt;functionRelation&gt;[...]&lt;/functionRelation&gt;

                    &lt;resourceRelation&gt;[...]&lt;/resourceRelation&gt;
                &lt;/relations&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="resourceRelation">
    <h3>resourceRelation</h3>
    <h4><?= __d("cpf-doc", "Resource Relation") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 An element for encoding a relation between a resource and the CPF entity."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;resourceRelation&gt; element contains the description of a resource related to
 the described entity. Use &lt;objectXMLWrap&gt; to incorporate XML elements from any XML
 namespaces or &lt;objectBinWrap&gt; for base64-encoded binary data. A
 &lt;relationEntry&gt; element is provided for a textual description of the related resource."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Use the &lt;date&gt;, &lt;dateRange&gt;, or &lt;dateSet&gt; elements for
 specifying the time period of the relationship and the &lt;placeEntry&gt; element for relevant
 location information. A &lt;descriptiveNote&gt; element may be included for a more
 detailed specifications or explanations of the relationship."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The resourceRelationType attribute is used to specify the nature of the
 relationship between the resource and the entity described in the EAC-CPF instance."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "date, dateRange, dateSet, descriptiveNote, objectBinWrap, objectXMLWrap, placeEntry, relationEntry"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "relations") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 6</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "lastDateTimeVerified") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "resourceRelationType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:actuate") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:arcrole") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:href") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:role") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:show") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:title") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:type") ?></td>
            <td><?= __d("cpf-doc", "Required (if any XLINK attributes used)") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;resourceRelation resourceRelationType=&quot;creatorOf&quot;&gt;
                    &lt;objectXMLWrap&gt;
                        &lt;ead xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xsi:schemaLocation=&quot;urn:isbn:1-931666-22-9 http://www.loc.gov/ead/ead.xsd&quot;&gt;
                            &lt;eadheader&gt;
                                &lt;eadid identifier=&quot;e4012531&quot; mainagencycode=&quot;GB-0066&quot;&gt;&lt;/eadid&gt;

                                &lt;filedesc&gt;
                                    &lt;titlestmt&gt;
                                        &lt;titleproper&gt;&lt;/titleproper&gt;
                                    &lt;/titlestmt&gt;
                                &lt;/filedesc&gt;
                            &lt;/eadheader&gt;

                            &lt;archdesc level=&quot;file&quot;&gt;
                                &lt;did&gt;
                                    &lt;unitid repositorycode=&quot;GB-0066&quot;&gt;E 40/12531&lt;/unitid&gt;

                                    &lt;unittitle&gt;Attached seal, Andrew Noel&lt;/unittitle&gt;

                                    &lt;unitdate&gt;1551-1552&lt;/unitdate&gt;

                                    &lt;physdesc&gt;
                                        &lt;genreform type=&quot;materialType&quot;&gt;Seals&lt;/genreform&gt;
                                    &lt;/physdesc&gt;
                                &lt;/did&gt;
                            &lt;/archdesc&gt;
                        &lt;/ead&gt;
                    &lt;/objectXMLWrap&gt;
                &lt;/resourceRelation&gt;

                &lt;resourceRelation resourceRelationType=&quot;creatorOf&quot; id=&quot;UA013&quot;&gt;
                    &lt;relationEntry&gt;Department of Romance Languages records&lt;/relationEntry&gt;

                    &lt;objectXMLWrap&gt;
                        &lt;ead xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xsi:schemaLocation=&quot;urn:isbn:1-931666-22-9 http://www.loc.gov/ead/ead.xsd&quot;&gt;
                            &lt;archdesc level=&quot;collection&quot;&gt;
                                &lt;did&gt;
                                    &lt;unittitle&gt;Department of Romance Languages records&lt;/unittitle&gt;

                                    &lt;unitid&gt;UA013&lt;/unitid&gt;
                                &lt;/did&gt;
                            &lt;/archdesc&gt;
                        &lt;/ead&gt;
                    &lt;/objectXMLWrap&gt;
                &lt;/resourceRelation&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="script">
    <h3>script</h3>
    <h4><?= __d("cpf-doc", "Script") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The specification of a particular script used in the EAC-CPF instance or in
 the creative work of the CPF entity being described."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A required element within &lt;languageDeclaration&gt; that gives the main script in
 which the EAC-CPF instance is written."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A required element within &lt;languageUsed&gt; that gives the main script used by the
 CPF entity being described in his/her creative or productive work."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The content of the scriptCode attribute must be given in the form of a valid
 code from ISO 15924."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "languageDeclaration, languageUsed") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.7</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "scriptCode") ?></td>
            <td><?= __d("cpf-doc", "Required") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;languageDeclaration&gt;
                    &lt;language languageCode=&quot;eng&quot;&gt;English&lt;/language&gt;

                    &lt;script scriptCode=&quot;Latn&quot;&gt;Latin&lt;/script&gt;
                &lt;/languageDeclaration&gt;


                &lt;languagesUsed&gt;
                    &lt;languageUsed&gt;
                        &lt;language languageCode=&quot;gre&quot;&gt;Greek, Modern (1453-)&lt;/language&gt;

                        &lt;script scriptCode=&quot;Grek&quot;&gt;Greek&lt;/script&gt;
                    &lt;/languageUsed&gt;

                    &lt;languageUsed&gt;
                        &lt;language languageCode=&quot;gre&quot;&gt;Greek, Modern (1453-)&lt;/language&gt;

                        &lt;script scriptCode=&quot;Grek&quot;&gt;Greek&lt;/script&gt;
                    &lt;/languageUsed&gt;
                &lt;/languagesUsed&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="setComponent">
    <h3>setComponent</h3>
    <h4><?= __d("cpf-doc", "Set Component") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A wrapper element within &lt;alternativeSet&gt; that contains the EAC-CPF
 encoding for one entire authority record, thereby permitting the bundling of authority
 records from multiple authority systems within a single &lt;cpfDescription&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "&lt;setComponent&gt; provides a wrapper to contain an entire authority record,
 so that multiple records for the same identity from separate authority systems or in
 different languages, may be combined together within a single EAC-CPF instance. The
 mandatory &lt;componentEntry&gt; element encodes the link to the authority record in the
 external authority system. An optional &lt;descriptiveNote&gt; may be used for a textual
 note providing further information about the record referenced in
 &lt;setComponent&gt;."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "componentEntry, descriptiveNote, objectBinWrap, objectXMLWrap") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "alternativeSet") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "lastDateTimeVerified") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:actuate") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:arcrole") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:href") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:role") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:show") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:title") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:type") ?></td>
            <td><?= __d("cpf-doc", "Required (if any XLINK attributes used)") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;alternativeSet&gt;
                    &lt;setComponent xlink:href=&quot;http://authorities.loc.gov&quot; xlink:type=&quot;simple&quot;&gt;
                        &lt;componentEntry&gt;Bright Sparcs Record&lt;/componentEntry&gt;
                    &lt;/setComponent&gt;

                    &lt;setComponent xlink:href=&quot;http://nla.gov.au/anbd.aut-an35335937&quot; xlink:type=&quot;simple&quot;&gt;
                        &lt;componentEntry&gt;NLA record.&lt;/componentEntry&gt;
                    &lt;/setComponent&gt;
                &lt;/alternativeSet&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="source">
    <h3>source</h3>
    <h4><?= __d("cpf-doc", "Source") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A source used for the establishement of the description of the CPF entity in the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An element for identifying a particular source of evidence used in describing the CPF entity. A record for the source must
 be included in either &lt;objectXMLWrap&gt; or
 &lt;objectBinWrap&gt;; or as a textual description in the &lt;sourceEntry&gt;
 element. Use the optional &lt;descriptiveNote&gt; for any additional notes about the
 source. A &lt;source&gt; in this context should not be confused with the
 &lt;citation&gt; element which is used in a number of descriptive elements to point to a
 resource that provides descriptive data which is not otherwise given in the EAC-CPF
 instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A link to the source may be made using the XML Linking Language (Xlink) attributes (consult the specification at http://www.w3.org/TR/xlink/)
 and the last date and time that the source was verified
 can be given in the lastDateTimeVerified attribute."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "descriptiveNote, objectBinWrap, objectXMLWrap, sourceEntry") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "sources") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.8</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "lastDateTimeVerified") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:actuate") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:arcrole") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:href") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:role") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:show") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:title") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xlink:type") ?></td>
            <td><?= __d("cpf-doc", "Required (if any XLINK attributes used)") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;sources&gt;
                    &lt;source&gt;
                        &lt;sourceEntry&gt;HMC, Principal Family and Estate Collections: Family Names L-W,
                            1999&lt;/sourceEntry&gt;
                    &lt;/source&gt;

                    &lt;source&gt;
                        &lt;sourceEntry&gt;HMC, Complete Peerage, 1936&lt;/sourceEntry&gt;
                    &lt;/source&gt;
                &lt;/sources&gt;


                &lt;sources&gt;
                    &lt;source&gt;
                        &lt;sourceEntry&gt;Union Lists of Artist Names, The Getty Research
                            Institute&lt;/sourceEntry&gt;
                    &lt;/source&gt;

                    &lt;source&gt;
                        &lt;sourceEntry&gt;Cultural Objects Name Authority Online, The Getty Research
                            Institute&lt;/sourceEntry&gt;
                    &lt;/source&gt;
                &lt;/sources&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="sourceEntry">
    <h3>sourceEntry</h3>
    <h4><?= __d("cpf-doc", "Source Entry") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A textual identification, such as a title, for a particular source of evidence used to establish the description of the CPF
 entity in the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The &lt;sourceEntry&gt; element may be used within the &lt;source&gt; element to
 identify a source used in the construction of the EAC-CPF instance directly rather than
 using other content in &lt;objectBinWrap&gt; or &lt;objectXMLWrap&gt;. The
 &lt;sourceEntry&gt; element generally contains free text."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "source") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.8</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "scriptCode") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "transliteration") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;sources&gt;
                    &lt;source&gt;
                        &lt;sourceEntry&gt;HMC, Principal Family and Estate Collections: Family Names L-W,
                            1999&lt;/sourceEntry&gt;
                    &lt;/source&gt;

                    &lt;source&gt;
                        &lt;sourceEntry&gt;HMC, Complete Peerage, 1936&lt;/sourceEntry&gt;
                    &lt;/source&gt;
                &lt;/sources&gt;


                &lt;sources&gt;
                    &lt;source&gt;
                        &lt;sourceEntry&gt;Union Lists of Artist Names, The Getty Research
                            Institute&lt;/sourceEntry&gt;
                    &lt;/source&gt;

                    &lt;source&gt;
                        &lt;sourceEntry&gt;Cultural Objects Name Authority Online, The Getty Research
                            Institute&lt;/sourceEntry&gt;
                    &lt;/source&gt;
                &lt;/sources&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="sources">
    <h3>sources</h3>
    <h4><?= __d("cpf-doc", "Sources") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A grouping element to record of the sources used for the description of the CPF entity in the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A grouping element within &lt;control&gt; used to groupone or more sources consulted in creating the description of the CPF entityin the EAC-CPF instance."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "source") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "control") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.4.8</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:base") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;sources&gt;
                    &lt;source&gt;
                        &lt;sourceEntry&gt;HMC, Principal Family and Estate Collections: Family Names L-W,
                            1999&lt;/sourceEntry&gt;
                    &lt;/source&gt;

                    &lt;source&gt;
                        &lt;sourceEntry&gt;HMC, Complete Peerage, 1936&lt;/sourceEntry&gt;
                    &lt;/source&gt;
                &lt;/sources&gt;


                &lt;sources&gt;
                    &lt;source&gt;
                        &lt;sourceEntry&gt;Union Lists of Artist Names, The Getty Research
                            Institute&lt;/sourceEntry&gt;
                    &lt;/source&gt;

                    &lt;source&gt;
                        &lt;sourceEntry&gt;Cultural Objects Name Authority Online, The Getty Research
                            Institute&lt;/sourceEntry&gt;
                    &lt;/source&gt;
                &lt;/sources&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="span">
    <h3>span</h3>
    <h4><?= __d("cpf-doc", "Span") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Specifies the beginning and the end of a span of text."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A formatting element for distinguishingwords or phrases that are intentionally stressed or emphasized for linguistic effect
 or identifying some qualities of the words or phrases. Use
 the style attribute to affect an arbitrary stylistic difference. Use the
 localType attribute to assign other characteristics."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "abstract, citation, componentEntry, item, p, sourceEntry") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "style") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;biogHist&gt;
                    &lt;p&gt;Robert Carlton Brown (1886-1959) was a writer, editor, publisher, and traveler.
                        From 1908 to 1917, he wrote poetry and prose for numerous magazines and newspapers in New
                        York City, publishing two pulp novels,
                        &lt;span style=&quot;font-style:italic&quot;&gt;What
                            Happened to Mary&lt;/span&gt;
 and
                        &lt;span style=&quot;font-style:italic&quot;&gt;The
                            Remarkable Adventures of Christopher Poe&lt;/span&gt;
 (1913), and one volume of poetry,

                        &lt;span style=&quot;font-style:italic&quot;&gt;My Marjonary&lt;/span&gt;
 (1916).&lt;/p&gt;

                    &lt;p&gt;During 1918, he traveled extensively in Mexico and Central America, writing for
                        the U.S. Committee of Public Information in Santiago de Chile. In 1919, he moved with his
                        wife, Rose Brown, to Rio de Janeiro, where they founded
                        &lt;span style=&quot;font-style:italic&quot;&gt;Brazilian American&lt;/span&gt;
, a weekly magazine that ran
                        until 1929. With Brown&#039;s mother, Cora, the Browns also established magazines in Mexico
                        City and London:
                        &lt;span style=&quot;font-style:italic&quot;&gt;Mexican American&lt;/span&gt;

                        (1924-1929) and
                        &lt;span style=&quot;font-style:italic&quot;&gt;British American&lt;/span&gt;

                        (1926-1929).&lt;/p&gt;
                &lt;/biogHist&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="structureOrGenealogy">
    <h3>structureOrGenealogy</h3>
    <h4><?= __d("cpf-doc", "Structure or Genealogy") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A description of the internal administrative structure(s) of a corporate
 body or the genealogy of a family."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "&lt;structureOrGenealogy&gt; encodes information within the description area, information expressing
 the internal administrative structure(s) of a corporate body and the dates of any changes to
 that structure that are significant to understanding the way that corporate body conducted
 affairs (such as dated organization charts), or the genealogy of a family (such as a family
 tree) in a way that demonstrates the interrelationships of its members with relevant
 dates."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The optional &lt;list&gt; and &lt;outline&gt; elements are used to accommodate
 greater complexity in expressing or representing the structure(s) or genealogy being described.
 &lt;citation&gt; may be used to provide a link to external documents like organizational
 charts and family trees. A simpler discursive expression of the structure(s) or genealogy may be encoded as
 one or more &lt;p&gt; elements."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Genealogical and administrative structure information may alternatively be described in
 discursive form in the &lt;biogHist&gt; element (ISAAR (CPF) 5.2.2 History) and/or with
 &lt;cpfRelation&gt; elements (ISAAR (CPF) 5.3). The &lt;structureOrGenealogy&gt;
 element should be used whenever separate semantic processing of information about structures
 or genealogies is required."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "citation, list, outline, p") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "description") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.2.7</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "localType") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;structureOrGenealogy&gt;
                    &lt;p&gt;Sir Edward Noel (d 1643) married Julian, daughter and co-heir of Baptists Hicks
                        (d 1629), Viscount Campden, and succeeded to the viscouty of Campden and a portion of
                        his father-in-law&#039;s estates. The third Viscount Campden (1612-82) married Hester Wotton,
                        daughter of the second Baron Wotton. The fourth Viscount Campden (1641-1689, created
                        Earl of Gainsborough, 1682) married Elizabeth Wriothesley, elder daughter of the fourth
                        Earl of Southampton. Jane Noel (d 1811), sister of the fifth and sixth Earls of
                        Gainsborough, married Gerard Anne Edwards of Welham Grove (Leicetershire) and had issue
                        Gerard Noel Edwards (1759-1838). He married in 1780 Diana Middleton (1762-1823 suo jure
                        Baroness Barham), daughter of Charles Middleton (1726-1813), created first Baronet of
                        Barham Court (Kent) in 1781 and first Baron Barham in 1805. GN Edwards assumed the
                        surname Noel in 1798 on inheriting the sixth Earl of Gainborough&#039;s Rutland and
                        Gloucestershire estates (though not the Earl&#039;s honours, which were extinguished); and he
                        later inherited his father-in-law&#039;s baronetcy. His eldest son John Noel (1781-1866)
                        succeeded to the estates of his mother and his father, to his mother&#039;s barony and his
                        father&#039;s baronetcy, and was created Viscount Campden and Earl of Gainsborough in
                        1841.&lt;/p&gt;
                &lt;/structureOrGenealogy&gt;


                &lt;structureOrGenealogy&gt;
                    &lt;p&gt;The organogram of the
                        &lt;span style=&quot;font-style:italic&quot;&gt;Ministry of Culture and Tourism&lt;/span&gt;
 before its incorporation with the Ministry of Education and Religious Affairs, was the following:&lt;/p&gt;

                    &lt;outline&gt;
                        &lt;level localType=&quot;first&quot;&gt;
                            &lt;item&gt;Minister of Culture and Tourism&lt;/item&gt;
                        &lt;/level&gt;

                        &lt;level localType=&quot;second&quot;&gt;
                            &lt;item&gt;Deputy Minister of Culture and Tourism&lt;/item&gt;
                        &lt;/level&gt;

                        &lt;level localType=&quot;third&quot;&gt;
                            &lt;item&gt;General Secretary of Tourism&lt;/item&gt;
                        &lt;/level&gt;

                        &lt;level localType=&quot;third&quot;&gt;
                            &lt;item&gt;General Secretary of Sports&lt;/item&gt;
                        &lt;/level&gt;

                        &lt;level localType=&quot;third&quot;&gt;
                            &lt;item&gt;General Secretary of Culture&lt;/item&gt;
                        &lt;/level&gt;

                        &lt;level localType=&quot;third&quot;&gt;
                            &lt;item&gt;General Secretary for Culture and Tourism Infrastructure&lt;/item&gt;
                        &lt;/level&gt;
                    &lt;/outline&gt;
                &lt;/structureOrGenealogy&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="term">
    <h3>term</h3>
    <h4><?= __d("cpf-doc", "Term") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A generic element used to encode a descriptive term in accordance with local
 descriptive rules."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A generic element used to encode a descriptive term in accordance with local descriptive
 rules. The local authority – thesaurus or local controlled vocabulary – should be declared
 in the &lt;localTypeDeclaration&gt; element within the &lt;control&gt; section."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d(
                    "cpf-doc",
                    "function, legalStatus, localControl, localDescription, mandate, occupation, place"
                ) ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "lastDateTimeVerified") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "scriptCode") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "transliteration") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "vocabularySource") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Examples:") ?></th>
            <td colspan="2"><pre>
                &lt;function&gt;
                    &lt;term&gt;Estate ownership&lt;/term&gt;

                    &lt;descriptiveNote&gt;
                        &lt;p&gt;Social, political, and cultural role typical of landed
                            aristocracy in England. The first Viscount Campden amassed a large
                            fortune in trade in London and purchased extensive estates, including
                            Exton (Rutland), and Chipping Campden (Gloucestershire). The Barham
                            Court (Kent) estate was the acquisition of the first Baron Barham, a
                            successful admiral and naval administrator (First Lord of the Admiralty
                            1805).&lt;/p&gt;
                    &lt;/descriptiveNote&gt;
                &lt;/function&gt;


                &lt;function&gt;
                    &lt;term vocabularySource=&quot;AGIFT&quot;&gt;Education and training&lt;/term&gt;
                &lt;/function&gt;


                &lt;localControl localType=&quot;detailLevel&quot;&gt;
                    &lt;term&gt;minimal&lt;/term&gt;
                &lt;/localControl&gt;


                &lt;legalStatus&gt;
                    &lt;term scriptCode=&quot;Latn&quot;&gt;Organismo de la Administracion Central del Estado&lt;/term&gt;

                    &lt;date standardDate=&quot;1769&quot;&gt;1769&lt;/date&gt;
                &lt;/legalStatus&gt;


                &lt;mandate&gt;
                    &lt;term lastDateTimeVerified=&quot;2012-07-12T12:13:25&quot;&gt;Minnesota. Executive Session Laws 1919 c49&lt;/term&gt;

                    &lt;dateRange&gt;
                        &lt;fromDate standardDate=&quot;1919&quot;&gt;1919&lt;/fromDate&gt;

                        &lt;toDate standardDate=&quot;1925&quot;&gt;1925&lt;/toDate&gt;
                    &lt;/dateRange&gt;

                    &lt;descriptiveNote&gt;
                        &lt;p&gt;Board created in 1919 to receive and examine applications for
                            bonuses from Minnesota soldiers.&lt;/p&gt;
                    &lt;/descriptiveNote&gt;
                &lt;/mandate&gt;


                &lt;occupation&gt;
                    &lt;term&gt;Teacher&lt;/term&gt;
                &lt;/occupation&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="toDate">
    <h3>toDate</h3>
    <h4><?= __d("cpf-doc", "To Date") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The ending date in a date range."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "May contain actual or approximate dates expressed as a month, day, or year in any format.
 A standard numerical form of the date (YYYYMMDD, etc.) can be specified with the
 standardDate attribute. The notBefore and notAfter
 attributes may be used to indicate uncertainty."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "[text]") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "dateRange") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "notAfter") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "notBefore") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "standardDate") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Mandatory, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;existDates&gt;
                    &lt;dateRange&gt;
                        &lt;fromDate standardDate=&quot;1868&quot;&gt;1868&lt;/fromDate&gt;

                        &lt;toDate standardDate=&quot;1936&quot;&gt;1936&lt;/toDate&gt;
                    &lt;/dateRange&gt;
                &lt;/existDates&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="useDates">
    <h3>useDates</h3>
    <h4><?= __d("cpf-doc", "Date of Use") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The dates when the name or names were used for or by the CPF entity being described."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Within &lt;nameEntry&gt;, &lt;useDates&gt; provides the dates during which the name was used for or by the CPF entity. For parallel names, &lt;useDates&gt; may occur in &lt;nameEntryParallel&gt;
 rather than in the individual &lt;nameEntry&gt; elements contained in
 &lt;nameEntryParallel&gt;."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "May contain:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "date, dateRange, dateSet") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "May occur within:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "nameEntry, nameEntryParallel") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "References:") ?></th>
            <td colspan="2">ISAAR (CPF) 5.1.2 and 5.1.3</td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Attributes:") ?></th>
            <td><?= __d("cpf-doc", "xml:id") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><span class="sr-only"><?= __("Espace d'indentation") ?></span></th>
            <td><?= __d("cpf-doc", "xml:lang") ?></td>
            <td><?= __d("cpf-doc", "Optional") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Availability:") ?></th>
            <td colspan="2"><?= __d("cpf-doc", "Optional, Non-repeatable") ?></td>
        </tr>
        <tr>
            <th><?= __d("cpf-doc", "Example:") ?></th>
            <td colspan="2"><pre>
                &lt;nameEntry&gt;
                    &lt;part&gt;Brown, Bob&lt;/part&gt;

                    &lt;useDates&gt;
                        &lt;dateRange&gt;
                            &lt;fromDate standardDate=&quot;1886&quot;&gt;1886&lt;/fromDate&gt;

                            &lt;toDate standardDate=&quot;1959&quot;&gt;1959&lt;/toDate&gt;
                        &lt;/dateRange&gt;
                    &lt;/useDates&gt;

                    &lt;authorizedForm&gt;AACR2&lt;/authorizedForm&gt;
                &lt;/nameEntry&gt;

</pre>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@accuracy">
    <h3>@accuracy</h3>
    <h4><?= __d("cpf-doc", "Accuracy") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The accuracy specification for a place statement."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Within the &lt;placeEntry&gt; element, this attribute allows for an
 accuracy specification."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "string") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@altitude">
    <h3>@altitude</h3>
    <h4><?= __d("cpf-doc", "Altitude") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The altitude or elevation of the geographic place."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The altitude may occur on &lt;place&gt;. The value of the attribute is the altitude or elevation of the geographic place named. altitude should be used in conjunction with longitude and
 latitude."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "NMTOKEN") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@countryCode">
    <h3>@countryCode</h3>
    <h4><?= __d("cpf-doc", "Country Code") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Two letter ISO 3166-1 standard code representing a country."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The countryCode may occur on &lt;placeEntry&gt; or
 &lt;address&gt;. The countryCode attribute may be used to provide the ISO
 3166-1 standard code designating a country. The values are to be taken from the list of ISO 3166-1 Codes for the Representation
 of Names of Countries, which can be downloaded from the website of ISO 3166 Maintenance
 Agency http://www.iso.org/iso/country_codes/iso_3166_code_lists.htm"
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d(
                    "cpf-doc",
                    "ISO 3166-1 Codes for the Representation of Names of Countries, column
 A2."
                ) ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@cpfRelationType">
    <h3>@cpfRelationType</h3>
    <h4><?= __d("cpf-doc", "Corporate Body, Person, or Family Relation Type") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The type of relation that the corporate body, person, or family has to the
 entity being described."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The cpfRelationType may occur on &lt;cpfRelation&gt;. The
 value designates the type of relation that a corporate body, person, or family has to the
 entity being described in the EAC-CPF instance. If the nature of the relation is more
 specific than one of the values given below, the Xlink attributes can be used in addition to
 cpfRelationType."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d(
                    "cpf-doc",
                    "&quot;identity&quot; or &quot;hierarchical&quot; or &quot;hierarchical-parent&quot; or
 &quot;hierarchical-child&quot; or &quot;temporal&quot; or &quot;temporal-earlier&quot; or &quot;temporal-later&quot; or &quot;family&quot; or
 &quot;associative&quot;"
                ) ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@functionRelationType">
    <h3>@functionRelationType</h3>
    <h4><?= __d("cpf-doc", "Function Relation Type") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The type of relation that the entity being described has to the related
 function."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The functionRelationType may occur on
 &lt;functionRelation&gt;. The value designates the type of relation that the entity being
 described has to the related function. If the type of relation is more specific than one of
 the values given below, Xlink attributes may be used in addition to
 functionRelationType."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "&quot;controls&quot; or &quot;owns&quot; or &quot;performs&quot;") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@identityType">
    <h3>@identityType</h3>
    <h4><?= __d("cpf-doc", "Identity Type") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Indicates whether the identity is given or acquired. May be useful for
 processing when multiple identities are described in the same instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The identityType may occur on &lt;identity&gt;. Though
 optional, it is recommended that it be used when multiple identities are described in the
 same EAC-CPF instance using &lt;multipleIdentities&gt;. It will enable processors to
 distinguish between the description of a person and one or more personae."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "&quot;given&quot; or &quot;acquired&quot;") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@languageCode">
    <h3>@languageCode</h3>
    <h4><?= __d("cpf-doc", "Language Code") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Three-letter ISO 639-2 language code. Must occur on
 &lt;language&gt;."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The languageCode must occur on &lt;language&gt;. The
 Language Code is used to provide the ISO 639-2 standard code representing the language of the
 EAC-CPF instance. The values are to be taken from the list of ISO 639-2 Codes for the
 representation of Names of Languages, which can be downloaded from the website of the
 Library of Congress which is the registration agency of the standard (http://www.loc.gov/standards/iso639-2/langhome.html)."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "ISO 639-2 Codes for the Representation of Names of Languages") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@lastDateTimeVerified">
    <h3>@lastDateTimeVerified</h3>
    <h4><?= __d("cpf-doc", "Last Date and Time Verified") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Last date or date and time the linked resource was verified."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The lastDateTimeVerified may occur on &lt;citation&gt;,
 &lt;cpfRelation&gt;, &lt;functionRelation&gt;, &lt;resourceRelation&gt;,
 &lt;setComponent&gt;, or &lt;term&gt;. The value of the attribute provides the last
 date or last date and time when a related (or linked) object was verified. Verification may
 include link resolution as well as verification of the version of the linked object."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d(
                    "cpf-doc",
                    "Union of the XML Schema Datatypes date, gYear, gYearMonth, and dateTime.
 The following are all valid patterns: 2009-12-31, 2009, 2009-12, 2009-12-31T23:59:59."
                ) ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@latitude">
    <h3>@latitude</h3>
    <h4><?= __d("cpf-doc", "Latitude") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The geographic latitude of the place."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The latitude may occur on &lt;placeEntry&gt;. The value of the
 attribute is the geographic latitude of the place named. latitude should be used
 in conjunction with longitude and altitude."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "NMTOKEN") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@localType">
    <h3>@localType</h3>
    <h4><?= __d("cpf-doc", "Local Type") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Used to make the semantics of the element more specific or to provide
 semantic specificity to elements that are semantically weak. Value should be an absolute
 URI."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The localType is broadly available on many descriptive and a
 few control elements. It is intended to provide a means to narrow the semantics of some
 elements or provide semantics for elements that are primarily structural or that are
 semantically weak. The value of the localType must conform to W3C Schema Part 2:
 Datatypes, anyURI. To facilitate exchange of EAC-CPF instances, it is highly recommend that
 the URI be absolute and resolvable to a local resource that describes the semantic scope and
 use of the value."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "anyURI") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@longitude">
    <h3>@longitude</h3>
    <h4><?= __d("cpf-doc", "Longitude") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The longitude of the place."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The longitude may occur on &lt;place&gt;. The value of the
 attribute is the geographic longitude of the place named. longitude should be used
 in conjunction with latitude and altitude."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "NMTOKEN") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@notAfter">
    <h3>@notAfter</h3>
    <h4><?= __d("cpf-doc", "Not After") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The latest date possible for an uncertain date."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The notAfter may occur on &lt;date&gt;,
 &lt;fromDate&gt;, and &lt;toDate&gt;. For uncertain dates, used in conjunction with
 standardDate, the value of notAfter is the latest date possible. See
 also notBefore."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d(
                    "cpf-doc",
                    "Union of the XML Schema Datatypes date, gYear, and gYearMonth. The
 following are all valid patterns: 2009-12-31, 2009, 2009-12."
                ) ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@notBefore">
    <h3>@notBefore</h3>
    <h4><?= __d("cpf-doc", "Not Before") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The earliest date possible for an uncertain date."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The notBefore may occur on &lt;date&gt;,
 &lt;fromDate&gt;, and &lt;toDate&gt;. For uncertain dates, used in conjunction with
 standardDate, the value of notBefore is the earliest date possible.
 See also notAfter."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d(
                    "cpf-doc",
                    "Union of the XML Schema Datatypes date, gYear, and gYearMonth. The
 following are all valid patterns: 2009-12-31, 2009, 2009-12."
                ) ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@resourceRelationType">
    <h3>@resourceRelationType</h3>
    <h4><?= __d("cpf-doc", "Resource Relation Type") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The type of relation of entity being described to the resource."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The resourceRelationType may occur on
 &lt;resourceRelation&gt;. The value designates the type of relation that the entity
 described in the EAC-CPF instance has to the resource. If the nature of the relation is more
 specific than one of the available values, the Xlink attributes may be used in addition to
 resourceRelationType."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "&quot;creatorOf&quot; or &quot;subjectOf&quot; or &quot;other&quot;") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@scriptCode">
    <h3>@scriptCode</h3>
    <h4><?= __d("cpf-doc", "Script Code") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The ISO 15924 four-letter code for the writing script used."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A standard four-letter code for the writing script used with a given
 language. The scriptCode attribute is required for the &lt;script&gt; element,
 and is available on other elements where language designations may be used."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "ISO 15924 Code for the Representation of Names and Scripts") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@standardDate">
    <h3>@standardDate</h3>
    <h4><?= __d("cpf-doc", "Standard Date") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The date represented in a standard form for computer processing."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The standardDate may occur on &lt;date&gt;,
 &lt;fromDate&gt;, and &lt;toDate&gt;. The value of standardDate provides a
 standard form of the date expressed in &lt;date&gt;, &lt;fromDate&gt;, or
 &lt;toDate&gt; that can be used in computer processing, such as searching."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d(
                    "cpf-doc",
                    "Union of the XML Schema Datatypes date, gYear, and gYearMonth. The
 following are all valid patterns: 2009-12-31, 2009, 2009-12."
                ) ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@standardDateTime">
    <h3>@standardDateTime</h3>
    <h4><?= __d("cpf-doc", "Standard Date and Time") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 The date or date and time represented in a standard form for computer
 processing."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The standardDateTime may occur on &lt;eventDateTime&gt;.
 The value of standardDateTime provides a standard form of the date or date and
 time expressed in the &lt;eventDateTime&gt; that can be used in computer processing. For
 example, using both the value in &lt;eventType&gt; and standardDateTime, all
 EAC-CPF instances &quot;revised&quot; on a particular date can be identified."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d(
                    "cpf-doc",
                    "Union of the XML Schema Datatypes date, gYear, gYearMonth, and dateTime.
 The following are all valid patterns: 2009-12-31, 2009, 2009-12, 2009-12-31T23:59:59."
                ) ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@style">
    <h3>@style</h3>
    <h4><?= __d("cpf-doc", "Style") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Used to specify a rendering style for string. It is recommended that the
 value conforms to W3C CSS."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The style may occur on &lt;span&gt;. In a limited number
 of contexts that accommodate discursive description, &lt;span&gt; with style
 may be used to identify an arbitrary string that is intended to be rendered in a specific
 style. It is highly recommended that the value of style be expressed as a W3C CSS
 style to facilitate interoperability."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "string") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@transliteration">
    <h3>@transliteration</h3>
    <h4><?= __d("cpf-doc", "Transliteration") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A value designating the transliteration scheme used in representing
 converting one script into another script."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The conventions or rules that prescribe a method for converting one
 script into another script."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "NMTOKEN") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@vocabularySource">
    <h3>@vocabularySource</h3>
    <h4><?= __d("cpf-doc", "Source of Vocabulary") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A value designating the source of the vocabulary from which terms are
 derived."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The controlled vocabulary used to derive terms for the &lt;term&gt;,
 &lt;placeEntry&gt;, and &lt;placeRole&gt; elements. Controlled vocabularies should be
 declared in the &lt;conventionDeclaration&gt; element within &lt;control&gt;."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "anyURI") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@xlink:actuate">
    <h3>@xlink:actuate</h3>
    <h4><?= __d("cpf-doc", "Xlink: Actuate") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Used to signal intended behavior with respect to whether the remote resource
 link is resolved when the containing resource is rendered, or when the user requests the
 resource."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The xlink:actuate should be used with the
 xlink:show to indicate intended behavior. The xlink:actuate is used to
 signal intended behavior with respect to when the remote resource is to appear. The values
 indicate whether the resource is to appear automatically when the containing resource is
 rendered (&quot;onLoad&quot;), or only after the user requests the resource (&quot;onRequest&quot;). &quot;None&quot; or
 &quot;other&quot; are also valid values."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d(
                    "cpf-doc",
                    "&quot;onLoad&quot; or &quot;onRequest&quot; or &quot;other&quot; or &quot;none&quot;"
                ) ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@xlink:arcrole">
    <h3>@xlink:arcrole</h3>
    <h4><?= __d("cpf-doc", "Xlink: Arc role") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 URI defining the purpose of the link."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An absolute URI that describes the nature of the relation between the
 entity being described in the EAC-CPF instance and a remote resource (&lt;cpfRelation&gt;,
 &lt;resourceRelation&gt;, or &lt;functionRelation&gt;) or the nature of the relation
 between the EAC-CPF instance description (or component of description) and the remote
 resource (&lt;citation&gt;, &lt;setComponent&gt;, and&lt;source&gt;). The value of
 the attribute should indicate the direction of the relation. It is recommended that the
 direction of the relation be from local resource to remote resource."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "string") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@xlink:href">
    <h3>@xlink:href</h3>
    <h4><?= __d("cpf-doc", "Xlink: HREF") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Contains a URI, possibly relative, pointing to the related resource"
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The address for a remote resource. The xlink:href takes the
 form of a Uniform Resource Identifier (URI). While it is permissible to use a relative URI,
 and an absolute URI is recommended."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "anyURI") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@xlink:role">
    <h3>@xlink:role</h3>
    <h4><?= __d("cpf-doc", "Xlink: Role") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Identifies the type or nature of the remote resource with an absolute
 URI."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Identifies the type or nature of the remote resource with an absolute
 URI."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "string") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@xlink:show">
    <h3>@xlink:show</h3>
    <h4><?= __d("cpf-doc", "Xlink: Show") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Used to signal intended behavior with respect to where the remote resource
 is to appear when the link is resolved."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The xlink:show should be used with the
 xlink:actuate to indicate intended behavior. The xlink:show is used to
 signal intended behavior with respect to where the remote resource is to appear when the link
 is resolved. The values indicate whether the resource is to appear embedded at the point of
 the link (&quot;embed&quot;), replace the resource in which it appears (&quot;replace&quot;), or in a new window
 (&quot;new&quot;). &quot;None&quot; or &quot;other&quot; are also valid values."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d(
                    "cpf-doc",
                    "&quot;embed&quot; or &quot;new&quot; or &quot;replace&quot; or &quot;none&quot; or &quot;other&quot;"
                ) ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@xlink:title">
    <h3>@xlink:title</h3>
    <h4><?= __d("cpf-doc", "Xlink: Title") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Information that may be used as a viewable caption for the remote
 resource."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "Information that serves as a viewable caption that indicates the name or
 type of the linked remote source."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "string") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@xlink:type">
    <h3>@xlink:type</h3>
    <h4><?= __d("cpf-doc", "Xlink: Type") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A fixed value that identifies an XLINK compliant element of a particular
 type."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "A fixed value that identifies an XLINK compliant element of a particular
 type. Only one XLINK type is used in EAC-CPF, &quot;simple.&quot; While the attribute is optional in
 instances validated against the W3C Schema version of EAC-CPF, the Relax NG version of
 EAC-CPF requires it. To support exchange of EAC-CPF instances, it is recommended that the
 xlink:type be present in the instance whenever any of the other XLINK attributes
 are used."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "Fixed value: &quot;simple&quot;") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@xml:base">
    <h3>@xml:base</h3>
    <h4><?= __d("cpf-doc", "XML Base") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Used to specify a base URI other than the base URI of the EAC-CPF
 instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "xml:base is used to specify the base URI other than the base
 URI of the EAC-CPF instance, for the purpose of resolving any relative URIs used with
 elements that contain one or more descendants that use an attribute of type anyURI.
 xml:base makes it possible to declare an absolute base URI to facilitate the use
 of relative URIs on the descendants."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "NMTOKEN") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@xml:id">
    <h3>@xml:id</h3>
    <h4><?= __d("cpf-doc", "XML Identifier") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 A unique identifier available on most elements that can be used to name
 specific elements in the EAC-CPF instance."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "An identifier used to name the element so that it can be referred to, or
 referenced from, somewhere else. Each xml:id within a document must have a unique
 value. The xml:id attribute regularizes the naming of the element and thus
 facilitates building links between it and other resources. For example, the xml:id
 may be used to uniquely identify two or more &lt;cpfDescription&gt; within
 &lt;multipleIdentities&gt;. Uniquely identifying or distinguishing two or more
 &lt;cpfDescription&gt; may be essential or useful in maintenance environments when
 relating resources, functions, or corporate bodies, persons, or families to one among two or
 more identities represented in one EAC-CPF instance."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "NMTOKEN") ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div data-id="@xml:lang">
    <h3>@xml:lang</h3>
    <h4><?= __d("cpf-doc", "XML Language") ?></h4>
    <p><?= __d(
            "cpf-doc",
            "Summary:
 Two-letter language code from the IANA registry as dictated by the W3C specification."
        ) ?></p>
    <p><?= __d(
            "cpf-doc",
            "The xml:lang may occur on any element intended to contain
 natural language content whenever information about the language of the content of this
 element and its children are needed. xml:lang should be used when the language of
 the element differs from the Language Code declared in the languageCode attribute
 on the element &lt;language&gt; within the &lt;control&gt; element. The values in the list are taken
 from the IANA Registry (http://www.iana.org/assignments/language-subtag-registry). The use of the IANA Registry
 code for languages in this context is outlined in the W3C specification. The syntax is specified at: http://www.w3.org/International/articles/language-tags/."
        ) ?></p>
    <table class="table smart-td-size">
        <tbody>
        <tr>
            <th><?= __d("cpf-doc", "Datatype:") ?></th>
            <td><?= __d("cpf-doc", "IANA Registry for language codes.") ?></td>
        </tr>
        </tbody>
    </table>
</div>
