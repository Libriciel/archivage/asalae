<?php

/**
 * @var Asalae\View\AppView $this
 */

use Authorization\Identity;
use Cake\Database\Exception\DatabaseException;
use Cake\Utility\Hash;
use Cake\View\View;

echo $this->ModalForm
    ->create('archival-agency-conf', ['size' => 'modal-xxl'])
    ->modal(__("Configuration du service d'archives"))
    ->output('function', 'editArchivalAgencyConfig', '/org-entities/edit-config')
    ->generate();

$separator = $this->Html->tag(
    'li',
    '',
    [
        'role' => 'separator',
        'class' => 'divider',
    ]
);

echo $this->Modal->create(
    [
        'id' => 'modal-rgpd',
        'class' => 'ui-draggable-handle',
    ]
)
    . $this->Modal->header(
        __("Informations relatives au règlement européen sur la protection des données personnelles (RGPD)")
    )
    . $this->Modal->body($this->element('rgpd'))
    . $this->Modal->end();

try {
    /** @var Identity $identity */
    $identity = $this->getRequest()->getAttribute('identity');
    $user = $identity ? $identity->getOriginalData() : [];
    $saName = h(Hash::get($user, 'org_entity.archival_agency.name'));
    foreach (Hash::get($user, 'org_entity.archival_agency.configurations', []) as $configuration) {
        if ($configuration['name'] === 'header-title') {
            $saName = h($configuration['setting']);
            break;
        }
    }
    $entityName = h(Hash::get($user, 'org_entity.name'));
    if ($saName === $entityName) {
        $entityName = '';
    } else {
        $entityName = $this->Html->tag('li.navbar-brand.hidden-xs', $entityName);
    }
    $saName = $this->Html->tag('li.navbar-brand.hidden-xs', $saName);
    $nom = h(trim(Hash::get($user, 'name')));
    $name = $nom
        ? $this->Html->tag('li.navbar-brand.hidden-xs', $nom)
        : '';

    $adminTech = $this->getRequest()->getSession()->read('Admin.tech');
} catch (DatabaseException $e) {
    $saName = null;
    $entityName = null;
    $nom = null;
    $name = null;
    $adminTech = false;
}

$userMenu = $userMenu ?? [];
$customMenu = [];
foreach ($userMenu as $parsedInput) {
    $content = $parsedInput['@'];
    $close = isset($parsedInput['@close']) ? $this->Fa->charte('Fermer', '', 'close') : '';
    unset($parsedInput['@'], $parsedInput['@close']);

    if (
        preg_match('/^javascript:|#/', $parsedInput['href'])
        || $this->Acl->check($parsedInput['href'])
    ) {
        $customMenu[] = $this->Html->tag(
            'li',
            $this->Html->tag('a', $content, $parsedInput) . $close
        );
    }
}

$menuDefinition = [
    [
        'icon' => $this->Fa->i('fa-exchange'),
        'label' => __("Transferts"),
        'dropdown' => [
            [
                'icon' => $this->Fa->i('fa-plus'),
                'label' => __("Créer un transfert"),
                'url' => '/transfers/index-preparating?add=form',
            ],
            [
                'icon' => $this->Fa->i('fa-plus'),
                'label' => __("Créer un transfert par upload de bordereau"),
                'url' => '/transfers/index-preparating?add=upload',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-code'),
                'label' => __("Mes transferts en préparation"),
                'url' => '/transfers/index-preparating',
            ],
            [
                'icon' => $this->Fa->i('fa-list-ul'),
                'label' => __("Mes transferts envoyés"),
                'url' => '/transfers/index-sent',
            ],
            [
                'icon' => $this->Fa->i('fa-check'),
                'label' => __("Mes transferts acceptés"),
                'url' => '/transfers/index-my-accepted',
            ],
            [
                'icon' => $this->Fa->i('fa-times'),
                'label' => __("Mes transferts rejetés"),
                'url' => '/transfers/index-my-rejected',
            ],
            [
                'icon' => $this->Fa->i('fa-folder-open'),
                'label' => __("Tous mes transferts"),
                'url' => '/transfers/index-my',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-folder-open'),
                'label' => __("Tous les transferts de mon service"),
                'url' => '/transfers/index-my-entity',
            ],
        ],
    ],
    [
        'icon' => $this->Fa->i('fa-exchange'),
        'label' => __("Transferts entrants"),
        'url' => '#',
        'dropdown' => [
            [
                'icon' => $this->Fa->i('fa-check-square-o'),
                'label' => __("Transferts conformes à valider"),
                'url' => '/validation-processes/my-transfers/validate',
            ],
            [
                'icon' => $this->Fa->i('fa-thumbs-down'),
                'label' => __("Transferts non conformes à valider"),
                'url' => '/validation-processes/my-transfers/invalidate',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-check'),
                'label' => __("Transferts acceptés"),
                'url' => '/transfers/index-accepted',
            ],
            [
                'icon' => $this->Fa->i('fa-times'),
                'label' => __("Transferts rejetés"),
                'url' => '/transfers/index-rejected',
            ],
            [
                'icon' => $this->Fa->i('fa-clock-o'),
                'label' => __("Transferts en cours de validation"),
                'url' => '/validation-processes/transfers',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-folder-open'),
                'label' => __("Tous les transferts"),
                'url' => '/transfers/index-all',
            ],
        ],
    ],
    [
        'icon' => $this->Fa->i('fa-archive'),
        'label' => __("Archives"),
        'dropdown' => [
            [
                'icon' => $this->Fa->i('fa-archive'),
                'label' => __("Archives consultables"),
                'url' => '/archive-units/index',
            ],
            [
                'icon' => $this->Fa->i('fa-cogs'),
                'label' => __("Traitements par lot"),
                'url' => '/archives/treatments',
            ],
            [
                'icon' => $this->Fa->i('fa-calendar-times-o'),
                'label' => __("DUA échue"),
                'url' => '/archive-units/dua-expired',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-list-ul'),
                'label' => __("Registre des entrées"),
                'url' => '/archives/index',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-archive'),
                'label' => __("Archives techniques"),
                'url' => '/technical-archives/index',
            ],
        ],
    ],
    [
        'icon' => $this->Fa->i('fa-share-square-o'),
        'label' => __("Communications"),
        'dropdown' => [
            [
                'icon' => $this->Fa->i('fa-book'),
                'label' => __("Catalogue"),
                'url' => '/archive-units/catalog',
            ],
            [
                'icon' => $this->Fa->i('fa-book'),
                'label' => __("Catalogue de mon service"),
                'url' => '/archive-units/catalog-my-entity',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-pencil-square-o'),
                'label' => __("Mes demandes de communication en préparation"),
                'url' => '/delivery-requests/index-preparating',
            ],
            [
                'icon' => $this->Fa->i('fa-folder-open'),
                'label' => __("Toutes mes demandes de communication"),
                'url' => '/delivery-requests/index-my',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-check-square-o'),
                'label' => __("Demandes de communication à valider"),
                'url' => '/validation-processes/delivery-requests',
            ],
            [
                'icon' => $this->Fa->i('fa-folder-open'),
                'label' => __("Toutes les demandes de communication"),
                'url' => '/delivery-requests/index-all',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-mail-reply'),
                'label' => __("Récupération des communications"),
                'url' => '/deliveries/retrieval',
            ],
            [
                'icon' => $this->Fa->i('fa-thumbs-up'),
                'label' => __("Acquittements des communications"),
                'url' => '/deliveries/acquittal',
            ],
        ],
    ],
    [
        'icon' => $this->Fa->i('fa-eraser'),
        'label' => __("Eliminations"),
        'dropdown' => [
            [
                'icon' => $this->Fa->i('fa-archive'),
                'label' => __("Archives éliminables"),
                'url' => '/archive-units/eliminable?show-filters=true&choose_originating_agency=true',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-pencil-square-o'),
                'label' => __("Mes demandes d'élimination en préparation"),
                'url' => '/destruction-requests/index-preparating',
            ],
            [
                'icon' => $this->Fa->i('fa-folder-open'),
                'label' => __("Toutes mes demandes d'élimination"),
                'url' => '/destruction-requests/index-my',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-check-square-o'),
                'label' => __("Demandes d'élimination à valider"),
                'url' => '/validation-processes/destruction-requests',
            ],
            [
                'icon' => $this->Fa->i('fa-folder-open'),
                'label' => __("Toutes les demandes d'élimination"),
                'url' => '/destruction-requests/index-all',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-mail-reply'),
                'label' => __("Notifications d'élimination"),
                'url' => '/destruction-notifications/index',
            ],
        ],
    ],
    [
        'icon' => $this->Fa->i('fa-eject'),
        'label' => __("Restitutions"),
        'dropdown' => [
            [
                'icon' => $this->Fa->i('fa-archive'),
                'label' => __("Archives restituables"),
                'url' => '/archive-units/returnable?show-filters=true&choose_originating_agency=true',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-pencil-square-o'),
                'label' => __("Mes demandes de restitution en préparation"),
                'url' => '/restitution-requests/index-preparating',
            ],
            [
                'icon' => $this->Fa->i('fa-folder-open'),
                'label' => __("Toutes mes demandes de restitutions"),
                'url' => '/restitution-requests/index-my',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-check-square-o'),
                'label' => __("Demandes de restitution à valider"),
                'url' => '/validation-processes/restitution-requests',
            ],
            [
                'icon' => $this->Fa->i('fa-folder-open'),
                'label' => __("Toutes les demandes de restitution"),
                'url' => '/restitution-requests/index-all',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-mail-reply'),
                'label' => __("Récupération des restitutions"),
                'url' => '/restitutions/retrieval',
            ],
            [
                'icon' => $this->Fa->i('fa-thumbs-up'),
                'label' => __("Acquittement des restitutions"),
                'url' => '/restitutions/acquittal',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-eraser'),
                'label' => __("Archives restituées et acquittées à éliminer"),
                'url' => '/restitution-requests/index-acquitted',
            ],

        ],
    ],
    [
        'icon' => $this->Fa->i('fa-forward'),
        'label' => __("Transferts sortants"),
        'dropdown' => [
            [
                'icon' => $this->Fa->i('fa-archive'),
                'label' => __("Archives transférables"),
                'url' => '/archive-units/transferable?show-filters=true',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-pencil-square-o'),
                'label' => __("Mes demandes de transferts sortants en préparation"),
                'url' => '/outgoing-transfer-requests/index-preparating',
            ],
            [
                'icon' => $this->Fa->i('fa-folder-open'),
                'label' => __("Toutes mes demandes de transferts sortants"),
                'url' => '/outgoing-transfer-requests/index-my',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-check-square-o'),
                'label' => __("Demandes de transferts sortants à valider"),
                'url' => '/validation-processes/outgoing-transfer-requests',
            ],
            [
                'icon' => $this->Fa->i('fa-folder-open'),
                'label' => __("Toutes les demandes de transferts sortants"),
                'url' => '/outgoing-transfer-requests/index-all',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-caret-square-o-right'),
                'label' => __("Transferts sortants en cours"),
                'url' => '/outgoing-transfers/index-preparating',
            ],
            [
                'icon' => $this->Fa->i('fa-check'),
                'label' => __("Transferts sortants acceptés"),
                'url' => '/outgoing-transfers/index-accepted',
            ],
            [
                'icon' => $this->Fa->i('fa-times'),
                'label' => __("Transferts sortants rejetés"),
                'url' => '/outgoing-transfers/index-rejected',
            ],
            [
                'icon' => $this->Fa->i('fa-list-ul'),
                'label' => __("Tous les transferts sortants"),
                'url' => '/outgoing-transfers/index-all',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-eraser'),
                'label' => __("Archives transférées à éliminer"),
                'url' => '/outgoing-transfer-requests/index-processed',
            ],
        ],
    ],
    [
        'icon' => $this->Fa->i('fa-wrench'),
        'label' => __("Administration"),
        'dropdown' => [
            [
                'icon' => $this->Fa->i('fa-building'),
                'label' => __("Entités"),
                'url' => '/org-entities/index',
            ],
            [
                'icon' => $this->Fa->i('fa-users'),
                'label' => __("Rôles utilisateur"),
                'url' => '/roles/index',
            ],
            [
                'icon' => $this->Fa->i('fa-user'),
                'label' => __("Utilisateurs"),
                'url' => '/users/index',
            ],
            [
                'icon' => $this->Fa->i('fa-road'),
                'label' => __("Circuits de validation"),
                'url' => '/validation-chains/index',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-file-code-o'),
                'label' => __("Profils d'archives"),
                'url' => '/profiles/index',
            ],
            [
                'icon' => $this->Fa->i('fa-file-text'),
                'label' => __("Accords de versement"),
                'url' => '/agreements/index',
            ],
            [
                'icon' => $this->Fa->i('fa-key'),
                'label' => __("Mots-clés"),
                'url' => '/keyword-lists/index',
            ],
            [
                'icon' => $this->Fa->i('fa-handshake-o'),
                'label' => __("Niveaux de service"),
                'url' => '/service-levels/index',
            ],
            [
                'icon' => $this->Fa->i('fa-lock'),
                'label' => __("Codes des restrictions d'accès seda v2"),
                'url' => '/access-rule-codes/index',
            ],
            [
                'icon' => $this->Fa->i('fas fa-clock'),
                'label' => __("Codes des durées d'utilité administrative seda v2"),
                'url' => '/appraisal-rule-codes/index',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-tachometer'),
                'label' => __("Compteurs"),
                'url' => '/counters/index',
            ],
            [
                'icon' => $this->Fa->i('fa-sliders'),
                'label' => __("Configuration"),
                'url' => '/org-entities/edit',
                'type' => 'button',
                'onclick' => 'editArchivalAgencyConfig()',
            ],
            [
                'icon' => $this->Fa->i('fa-tasks'),
                'label' => __("Jobs en cours"),
                'url' => '/tasks/admin',
            ],
        ],
    ],
];

/**
 * Constructeur du menu
 * @param string|array $element
 * @param array $headerMenu
 * @param View|Asalae\View\AppView $View
 */
$menuBuilder = function ($element, &$headerMenu, Cake\View\View $View) use (&$menuBuilder, $separator) {
    if (is_string($element)) {
        if (!empty($headerMenu) && end($headerMenu) !== $separator) {
            $headerMenu[] = $separator;
        }
        return;
    }

    $element += [
        'icon' => '',
        'label' => '',
        'url' => '#',
        'dropdown' => [],
        'type' => 'link',
        'onclick' => '',
    ];
    if ($element['type'] === 'button' && ($element['url'] === '#' || $View->Acl->check($element['url']))) {
        $button = $View->Html->tag(
            'button',
            $element['icon'] . ' ' . $element['label'],
            [
                'class' => 'navbar-link btn-link',
                'escape' => false,
                'onclick' => $element['onclick'],
            ]
        );
        $headerMenu[] = $View->Html->tag('li', $button, ['class' => 'navbar-item']);
    } elseif (empty($element['dropdown'])) {
        $li = ['class' => 'navbar-item'];
        $a = ['class' => 'navbar-link', 'escape' => false];
        $link = $View->Acl->link(
            $element['icon'] . ' ' . $element['label'],
            $element['url'],
            $a,
            'li',
            $li
        );
        if ($link) {
            $headerMenu[] = $link;
        }
    } else {
        $li['class'] = 'dropdown dropdown-asalae navbar-item';
        $a = [
            'class' => 'navbar-link dropdown-toggle',
            'data-toggle' => 'dropdown',
            'aria-expanded' => 'false',
            'href' => '#',
        ];
        $submenu = [];
        foreach ($element['dropdown'] as $subelement) {
            $menuBuilder($subelement, $submenu, $View);
        }

        // Retire les séparateurs à la fin du menu
        /** @noinspection PhpArrayIsAlwaysEmptyInspection $menuBuilder() modifie $submenu par ref */
        $c = count($submenu);
        for ($i = $c - 1; $i > 0; $i--) {
            /** @noinspection PhpArrayIsAlwaysEmptyInspection */
            if ($submenu[$i] === $separator) {
                unset($submenu[$i]);
            } else {
                break;
            }
        }

        /** @noinspection PhpArrayIsAlwaysEmptyInspection */
        if (count($submenu) > 0) {
            $span = $View->Html->tag('span', '', ['class' => 'caret']);
            $link = $View->Html->tag('a', $element['icon'] . ' ' . $element['label'] . ' ' . $span, $a);
            $ul = $View->Html->tag(
                'ul',
                implode(PHP_EOL, $submenu),
                ['class' => 'dropdown-menu flex-column']
            );
            $headerMenu[] = $View->Html->tag('li', $link . PHP_EOL . $ul, $li);
        }
    }
};
$headerMenu = [];
foreach ($menuDefinition as $element) {
    $menuBuilder($element, $headerMenu, $this);
}

$catalogUrl = $this->Url->build('/archive-units/catalog');
?>
<!-- Menu du haut -->
<header class="navbar-fixed-top">
    <nav class="navbar navbar-asalae bg-ls-dark">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button"
                        class="navbar-toggle collapsed"
                        data-toggle="collapse"
                        data-target=".header-menu-collapsable"
                        aria-expanded="false">
                    <span class="sr-only"><?= __("Toggle navigation") ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="main-logo"
                   class="navbar-brand logo"
                   href="<?= $this->Url->build('/') ?>"><?= $this->fetch('logo'); ?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse header-menu-collapsable">
                <ul class="nav navbar-nav navbar-left">
                    <?= $saName ?>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <?= $entityName ?>
                    <?= $name ?>
                    <li class="dropdown navbar-item" title="Filtrer" id="menu-li-filter" style="display: none">
                        <button data-toggle="dropdown" type="button" class="btn-link">
                            <?= $this->Fa->charte('Filtrer') ?>
                            <span class="resp resp-hide-large"
                                  aria-hidden="true"> <?= __("Filtres") ?> </span>
                            <span class="sr-only"><?= __("Filtres de recherche") ?></span>
                        </button>
                        <ul class="dropdown-menu flex-column filtersSelectWindow">
                            <li>
                                <button data-toggle="modal"
                                        data-target="#modal-filters"
                                        class="navbar-link btn-link"
                                        type="button">
                                    <?= $this->Fa->charte(
                                        'Modifier',
                                        __("Ajouter / Modifier les filtres")
                                    ) ?>
                                </button>
                            </li>
                            <?php if ($this->getRequest()->getParam('?.SaveFilterSelect') !== false) : ?>
                                <li class="savedFilter">
                                    <button class="navbar-link btn-link"
                                            onclick="AsalaeGlobal.confirmRemoveFilters()"
                                            type="button">
                                        <?= $this->Fa->charte(
                                            'Réinitialiser',
                                            __("Réinitialiser les filtres")
                                        ) ?>
                                    </button>
                                </li>
                            <?php endif; ?>
                            <li role="separator" class="divider"></li>
                            <?php
                            if (!empty($savedFilters) && is_array($savedFilters)) {
                                echo $this->Filter->selectSaved($savedFilters);
                            }
                            ?>
                        </ul>
                    </li>
                    <li id="container-number-notification" class="dropdown navbar-item">
                        <button type="button" class="btn-link" data-toggle="dropdown" aria-expanded="false">
                            <?= $this->Fa->charte('Notification', '', 'animated') ?>
                            <span class="resp resp-hide-large"
                                  aria-hidden="true"> <?= __("Notifications") ?> </span>
                            <span class="sr-only"><?= __("Notifications") ?></span>
                            <span id="number-notification" class="animated">0</span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu flex-column" id="notifications-ul-container">
                            <li>
                                <button class="navbar-link btn-link"
                                        onclick="AsalaeGlobal.deleteAllNotifications(new Event('click'))"
                                        type="button"><?= __("Supprimer toutes les notifications") ?></button>
                            </li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </li>
                    <li class="dropdown link" id="user-menu">
                        <button class="dropdown-toggle btn-link"
                                data-toggle="dropdown"
                                aria-expanded="false"
                                type="button">
                            <i aria-hidden="true" class="fa fa-cogs"></i>
                            <span class="resp resp-hide-large"> <?= __("Paramétrage") ?> </span>
                            <span class="resp resp-hide-small sr-only"> <?= __("Paramétrage") ?> </span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu flex-column">
                            <?php
                            foreach ($archivalAgenciesOpts ?? [] as $archivalAgencyId => $archivalAgencyName) {
                                if ($archivalAgencyId === $saId) {
                                    echo $this->Html->tag(
                                        'li.active.select-archival-agency',
                                        $this->Html->tag(
                                            'button',
                                            $archivalAgencyName,
                                            [
                                                'class' => 'btn-link navbar-link disable-drag',
                                                'onclick' => "changeArchivalAgency($archivalAgencyId)",
                                                'data-id' => $archivalAgencyId,
                                                'disabled' => true,
                                            ]
                                        )
                                    );
                                } else {
                                    echo $this->Html->tag(
                                        'li.select-archival-agency',
                                        $this->Html->tag(
                                            'button',
                                            $archivalAgencyName,
                                            [
                                                'class' => 'btn-link navbar-link disable-drag',
                                                'onclick' => "changeArchivalAgency($archivalAgencyId)",
                                                'data-id' => $archivalAgencyId,
                                            ]
                                        )
                                    );
                                }
                            }
                            echo $separator;

                            if ($this->Acl->check(['controller' => 'Users', 'action' => 'edit'])) {
                                echo $this->Html->tag(
                                    'li',
                                    $this->ModalForm
                                        ->create('user-edit-form')
                                        ->modal(__("Modifier mon compte utilisateur"))
                                        ->output(
                                            'button',
                                            '<i aria-hidden="true" class="fa fa-address-card-o"></i> '
                                            . __("Modifier mon compte utilisateur"),
                                            '/users/edit'
                                        )
                                        ->generate(
                                            [
                                                'class' => 'navbar-link disable-drag btn-link',
                                                'type' => 'button',
                                            ]
                                        )
                                    . $this->Html->tag(
                                        'script',
                                        "$('#body-content').append($('#user-edit-form'));"
                                    )
                                );
                                echo $this->Html->tag(
                                    'li',
                                    $this->Html->tag(
                                        'button',
                                        $this->Fa->charte('Informations', __("Informations RGPD")),
                                        [
                                            'data-toggle' => 'modal',
                                            'data-target' => '#modal-rgpd',
                                            'class' => 'navbar-link btn-link disable-drag',
                                            'type' => 'button',
                                        ]
                                    )
                                );
                            }
                            echo $separator;

                            if ($customMenu) {
                                echo implode(PHP_EOL, $customMenu);
                            } else {
                                echo $this->Html->tag('li.navbar-link.disable-drag');
                                echo $this->Html->tag(
                                    'div#drop-zone-custom-menu',
                                    __("Glissez-déposez ici<br>vos raccourcis préférés")
                                );
                                echo $this->Html->tag('/li');
                            }
                            echo '<li role="separator" class="divider"></li>';
                            if ($userCanSeeIndicators ?? false) {
                                echo $this->Html->tag(
                                    'li',
                                    $this->Html->link(
                                        $this->Fa->i('fa-tachometer', __("Indicateurs par service versant")),
                                        '/indicators/index-transferring-agency',
                                        ['class' => 'navbar-link disable-drag', 'escape' => false]
                                    )
                                );
                                echo $this->Html->tag(
                                    'li',
                                    $this->Html->link(
                                        $this->Fa->i('fa-tachometer', __("Indicateurs par service producteur")),
                                        '/indicators/index-originating-agency',
                                        ['class' => 'navbar-link disable-drag', 'escape' => false]
                                    )
                                );
                            }
                            echo $this->Acl->link(
                                $this->Fa->i('fa-tasks', __("Mes jobs en attente")),
                                '/tasks/index',
                                ['class' => 'navbar-link disable-drag', 'escape' => false],
                                'li'
                            );

                            if ($adminTech) {
                                $links = [
                                    'Admins/index' => __("Administration technique"),
                                    '----------',
                                    'Admins/logout' => $this->Fa->charte(
                                        'Se déconnecter',
                                        __("Déconnexion administrateur technique")
                                    ),
                                ];
                            } else {
                                $links = [
                                    '----------',
                                    'Users/logout' => $this->Fa->charte(
                                        'Se déconnecter',
                                        __("Se déconnecter")
                                    ),
                                ];
                            }
                            foreach ($links as $url => $text) {
                                if (is_numeric($url)) {
                                    echo $separator;
                                    continue;
                                }
                                echo $this->Html->tag('li');
                                echo $this->Html->link(
                                    $text,
                                    $url,
                                    [
                                        'data-mode' => 'no-ajax',
                                        'class' => 'navbar-link disable-drag',
                                        'escape' => false,
                                    ]
                                );
                                echo $this->Html->tag('/li');
                            }
                            ?>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
        <div class="container-fluid" id="navigation-menu">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse header-menu-collapsable" id="header-menu-collapsable">
                <ul class="nav navbar-nav">
                    <?= implode(PHP_EOL, $headerMenu) ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>
<script>
    // Maintien une distance idéale sur le haut du body pour éviter que le menu passe par dessu le contenu
    function headerSizeToBodyTop() {
        $('body').css({
            'padding-top': $('.navbar-fixed-top').height()
        });
    }

    $(window).resize(headerSizeToBodyTop);
    headerSizeToBodyTop();

    $('#main-logo').click(function (event) {
        if (event.ctrlKey && event.shiftKey) {
            event.preventDefault();
            window.open('<?=$this->Url->build('/admins')?>', '_blank');
        }
    });

    /**
     * Filtres de recherche par mots-clés
     */
    function searchKeywordPopup() {
        var element = $(this);
        if (element.closest('.form-group').find('select').length === 0) {
            var select = $('<select class="form-control">').attr('id', element.attr('id') + '-select');
            select.append($('<option>').attr('value', element.val()).text(element.val()));
            element.hide().parent().append(select).find('label').attr('for', element.attr('id') + '-select');
            select.select2(
                {
                    ajax: {
                        url: '<?=$this->Url->build('/keywords/populate-select')?>?include_search=true&key=content',
                        method: 'POST',
                        delay: 250
                    },
                    escapeMarkup: function (v) {
                        return $('<span></span>').html(v).text();
                    },
                    width: '100%',
                    dropdownPosition: 'below'
                }
            );
            select.on('change', function () {
                element.val($(this).val());
            });
        }
    }

    function changeArchivalAgency(archivalAgencyId) {
        AsalaeLoading.ajax(
            {
                url: '<?=$this->Url->build('/users/change-logged-archival-agency')?>/' + archivalAgencyId,
                method: 'POST',
                success: function () {
                    location.assign(location.origin);
                },
                error: function () {
                    AsalaeLoading.stop();
                },
                disable_complete_callback: true // maintien l'animation de chargement
            }
        );
    }
</script>
