<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Html->tag(
    'p',
    __(
        "La création de votre compte utilisateur sur asalae demande la "
        . "saisie de données personnelles vous concernant. Vous trouverez ci-dessous"
        . " le détail des informations collectées ainsi que les traitements effectués."
        . " Ces données sont collectées uniquement dans le cadre de votre activité "
        . "professionnelle et pour l'utilisation exclusive de l'application asalae. "
        . "Le fonctionnement dans les conditions nominales et recommandées de l'application, "
        . "ne permet pas de partager ou de diffuser à un tiers vos informations personnelles."
        . " Vous pouvez demander à tout moment à l'administrateur de la plateforme de vous"
        . " communiquer l'ensemble des informations collectées pour la création de votre compte."
    )
);
echo '<hr>';

// -----------------------------------------------------------------------------
echo $this->Html->tag('h3', __("Collecte"));
// -----------------------------------------------------------------------------

echo $this->Html->tag('p');

echo __("Lors de la création de votre compte utilisateur, les informations suivantes sont collectées :");

echo $this->Html->tag(
    'ul',
    $this->Html->tag('li', __("Identifiant de connexion"))
    . $this->Html->tag('li', __("Nom d'utilisateur"))
    . $this->Html->tag('li', __("Adresse de messagerie électronique professionnelle (courriel)"))
);

echo $this->Html->tag('/p');
echo '<hr>';

// -----------------------------------------------------------------------------
echo $this->Html->tag('h3', __("Traitements"));
// -----------------------------------------------------------------------------

echo $this->Html->tag('p');

echo __(
    "Vos informations personnelles sont utilisées pour le bon "
    . "fonctionnement de asalae dans les cas listés ci-après :"
);

echo $this->Html->tag(
    'ul',
    $this->Html->tag(
        'li',
        __("Affichage de vos données personnelles :")
        . $this->Html->tag(
            'ul',
            $this->Html->tag(
                'li',
                __(
                    "liste des utilisateurs :
                votre identifiant de connexion, votre nom d'utilisateur et"
                    . " votre courriel apparaissent dans la liste"
                )
            )
            . $this->Html->tag(
                'li',
                __(
                    "vue détaillée de votre compte utilisateur :
                votre identifiant de connexion, nom d'utilisateur et courriel "
                    . "apparaissent dans la vue détaillée de votre compte utilisateur"
                )
            )
            . $this->Html->tag(
                'li',
                __(
                    "circuits de validation :
                votre identifiant de connexion et votre nom d'utilisateur peuvent"
                    . " apparaître dans la composition des étapes des circuits de validation"
                )
            )
        )
    )
    . $this->Html->tag(
        'li',
        __("Suite à une action de votre part dans asalae :")
        . $this->Html->tag(
            'ul',
            $this->Html->tag(
                'li',
                __(
                    "historique des traitements :
                votre identifiant de connexion et votre nom d'utilisateur apparaissent"
                    . " dans l'historique des traitements des transferts d'archives,
                des demandes de communications, d'éliminations et de restitutions"
                )
            )
            . $this->Html->tag(
                'li',
                __(
                    "cycle de vie des archives :
                votre identifiant de connexion et votre nom d'utilisateur "
                    . "apparaissent dans le cycle de vie des archives"
                )
            )
            . $this->Html->tag(
                'li',
                __(
                    "journal des événements :
                votre identifiant de connexion et votre nom d'utilisateur "
                    . "figurent dans les événements du journal des événements"
                )
            )
        )
    )
    . $this->Html->tag(
        'li',
        __("Suite à l'envoi de courriels par asalae :")
        . $this->Html->tag(
            'ul',
            $this->Html->tag(
                'li',
                __(
                    "votre identifiant de connexion, votre nom d'utilisateur et votre courriel apparaissent 
                dans de courriel de notification de création de votre compte utilisateur"
                )
            )
            . $this->Html->tag(
                'li',
                __(
                    "votre identifiant de connexion et votre courriel apparaissent dans le courriel
                de notification de modification de votre mot de passe"
                )
            )
        )
    )
);

echo $this->Html->tag('/p');
echo '<hr>';


// -----------------------------------------------------------------------------
echo $this->Html->tag('h3', __("Cookies"));
// -----------------------------------------------------------------------------

echo $this->Html->tag('p');

echo __(
    "L'application utilise des cookies afin de sauvegarder temporairement,"
    . " le temps de la session, les préférences utilisateurs."
);

echo $this->Html->tag('/p');
echo '<hr>';

// -----------------------------------------------------------------------------
echo $this->Html->tag('h3', __("Droit à l'oubli"));
// -----------------------------------------------------------------------------

echo $this->Html->tag(
    'p',
    __(
        "Dans le cadre normatif du fonctionnement de asalae, "
        . "votre compte utilisateur ne peut être supprimé définitivement.
    Par contre il peut être désactivé et vous pouvez demander à cette occasion" .
        " d’anonymiser vos identifiant de connexion, nom d'utilisateur et courriel."
    )
);

echo $this->Html->tag(
    'p',
    __(
        "Sachez également qu'il est impossible, pour des raisons d'intégrité et de probité des archives,
    de supprimer les traces de votre activité dans les journaux des événements."
    )
);
