<?php
/**
 * @var Asalae\View\AppView $this
 */

$this->extend('/layout/html');
$this->assign(
    'logo',
    $this->Html->image(
        'asalae-color-white.svg',
        [
            'alt' => 'LOGO',
            'title' => 'asalae',
            'style' => 'height: 40px; padding-top: 5px;',
        ]
    )
);
$this->start('main-header');
?>
    <header class="navbar-fixed-top">
        <nav class="navbar navbar-asalae bg-ls-dark">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand logo"
                       href="#"
                       data-mode="no-ajax"><?= $this->fetch('logo') ?></a>
                </div>
            </div><!-- /.container-fluid -->
        </nav>
    </header>
<?php
$this->end();

$this->start('main-content');
?>
    <!-- Contenu de la page -->

    <div class="clearfix main-content">
        <div id="zone-debug"></div>
        <?= $this->Flash->render() ?>

        <?= $this->fetch('content') ?>
    </div>
<?php
$this->end();
