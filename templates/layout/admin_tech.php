<?php
/**
 * @var Asalae\View\AppView $this
 */

$this->extend('/layout/html');
$this->assign('body-class', 'admin');
$this->assign(
    'logo',
    $this->Html->image(
        'asalae-color-white.svg',
        [
            'alt' => 'LOGO',
            'title' => 'asalae',
            'style' => 'height: 40px; padding-top: 5px;',
        ]
    )
);
$this->start('main-header');
?>
    <header class="navbar-fixed-top">
        <nav class="navbar navbar-asalae bg-ls-dark">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button"
                            class="navbar-toggle collapsed"
                            data-toggle="collapse"
                            data-target=".header-menu-collapsable"
                            aria-expanded="false">
                        <span class="sr-only"><?= __("Toggle navigation") ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logo"
                       href="#"
                       data-mode="no-ajax"><?= $this->fetch('logo') ?></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse header-menu-collapsable">
                    <ul class="nav navbar-nav navbar-left">
                        <li class="navbar-brand hidden-xs"><?= __("Administration technique") ?></li>
                    </ul>

                    <ul class="nav navbar-nav">
                    </ul>

                    <ul class="nav navbar-nav navbar-right" id="user-menu">
                        <li id="container-number-notification" class="dropdown navbar-item">
                            <button type="button" class="btn-link" data-toggle="dropdown" aria-expanded="false">
                                <?= $this->Fa->charte('Notification', '', 'animated') ?>
                                <span class="resp resp-hide-large"
                                      aria-hidden="true"> <?= __("Notifications") ?> </span>
                                <span class="sr-only"><?= __("Notifications") ?></span>
                                <span id="number-notification" class="animated">0</span>
                            </button>
                            <ul class="dropdown-menu flex-column" id="notifications-ul-container">
                                <li>
                                    <button class="navbar-link btn-link"
                                            onclick="AsalaeGlobal.deleteAllNotifications(new Event('click'))"
                                            type="button"><?= __("Supprimer toutes les notifications") ?></button>
                                </li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </li>
                        <li class="dropdown link">
                            <a href="#" class="dropdown-toggle"
                               data-toggle="dropdown"
                               aria-haspopup="true"
                               aria-expanded="false">
                                <i aria-hidden="true" class="fa fa-gear"></i>
                                <span class="resp resp-hide-large"> <?= __("Paramétrage") ?> </span>
                                <span class="resp resp-hide-small sr-only"> <?= __("Paramétrage") ?> </span>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu flex-column">
                                <li class="active">
                                    <a href="javascript:" class="navbar-link" role="button">
                                        &gt; <?= __("Administration technique") ?>
                                    </a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <?php
                                echo $this->Html->tag(
                                    'li',
                                    $this->ModalForm
                                        ->create('user-admin-edit-form')
                                        ->modal(__("Modifier mon compte administrateur"))
                                        ->output(
                                            'button',
                                            '<i aria-hidden="true" class="fa fa-address-card-o"></i> '
                                            . __("Modifier mon compte administrateur"),
                                            '/admins/edit-admin'
                                        )
                                        ->generate(
                                            [
                                                'class' => 'navbar-link disable-drag btn-link',
                                                'type' => 'button',
                                            ]
                                        )
                                    . $this->Html->tag(
                                        'script',
                                        "$('#body-content').append($('#user-admin-edit-form'));"
                                    )
                                );
                                ?>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?= $this->Url->build('Admins/logout') ?>"
                                       data-mode="no-ajax"
                                       class="navbar-link">
                                        <?= $this->Fa->charte(
                                            'Se déconnecter',
                                            __("Déconnexion administrateur technique")
                                        ) ?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </header>
<?php
$this->end();


$this->start('main-content');
?>
    <!-- Contenu de la page -->

    <div class="clearfix main-content">
        <div id="zone-debug"></div>
        <?= $this->Flash->render() ?>

        <?= $this->fetch('content') ?>
    </div>
<?php
$this->end();