<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Database\Exception\DatabaseException;

$this->extend('/layout/html');
$this->assign(
    'logo',
    $this->Html->image(
        'asalae-color-white.svg',
        [
            'alt' => 'Logo asalae',
            'title' => 'asalae',
            'style' => 'height: 40px; padding-top: 5px;',
        ]
    )
);
$this->assign('main-header', $this->element('header'));


$this->start('main-content');
?>
    <!-- Contenu de la page -->

    <div class="clearfix main-content">
        <div id="zone-debug"></div>
        <?php
        try {
            echo $this->Flash->render();
        } catch (DatabaseException $e) {
        }
        ?>

        <?= $this->fetch('content') ?>
    </div>
<?php
$this->end();
