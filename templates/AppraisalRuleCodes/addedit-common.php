<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->Form->control(
    'code',
    [
        'label' => __("Code"),
        'help' => __("Codes de APP0Y à APP150Y non autorisés."),
        'readonly' => !$entity->get('deletable'),
    ]
)
    . $this->Form->control('name', ['label' => __("Nom")])
    . $this->Form->control('description', ['label' => __("Description")])
    . $this->Form->control(
        'duration',
        [
            'label' => __("Durée"),
            'help' => __("Sous la forme PnYnMnD (nY = n années, nM = n mois, nD = n jours)"),
            'readonly' => !$entity->get('deletable'),
        ]
    );

echo $this->Html->tag('div.alert.alert-info');
echo $this->Html->tag('h4', __("Exemples de durées"), ['class' => 'fake-label']);
echo $this->Html->tag('p', __("P10Y: dix ans, P6M: 6 mois, P15D: 15 jours"));
echo $this->Html->tag('p', __("P1Y6M: un an et 6 mois, P1Y6M7D: un an, 6 mois et 7 jours"));
echo $this->Html->tag('p', __("P0Y: 0 an, la règle prend fin dès la date de départ de caclul"));
echo $this->Html->tag('/div');
