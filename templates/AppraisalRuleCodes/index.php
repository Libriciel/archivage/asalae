<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Codes des durées d'utilité administrative seda v2"));

echo $this->Html->tag(
    'div.container',
    $this->Html->tag(
        'h1',
        $this->Fa->i(
            'fa-lock',
            __("Codes des durées d'utilité administrative seda v2")
        )
    )
    . $this->Breadcrumbs->render()
);

$jsTable = $this->Table->getJsTableObject($tableId);
$buttons = [];
if ($this->Acl->check('/appraisal-rule-codes/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-appraisal-rule-code')
        ->modal(__("Ajout d'un code de durée d'utilité administrative"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "AppraisalRuleCodes")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un code de durée d'utilité administrative")),
            '/AppraisalRuleCodes/add'
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->element('modal', ['idTable' => $tableId]);
echo $this->element('modal', ['idTable' => $tableIdGlobals, 'paginate' => false]);

echo $this->ModalForm
    ->create('edit-appraisal-rule-code', ['size' => 'large'])
    ->modal(__("Modifier un code de durée d'utilité administrative"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "AppraisalRuleCodes")')
    ->output(
        'function',
        'actionEditAppraisalRuleCode',
        '/AppraisalRuleCodes/edit'
    )
    ->generate();

echo $this->ModalView
    ->create('view-appraisal-rule-code')
    ->modal(__("Visualiser un code de durée d'utilité administrative"))
    ->output(
        'function',
        'actionViewAppraisalRuleCode',
        '/AppraisalRuleCodes/view'
    )
    ->generate();

echo $this->Filter->create('appraisal-rule-codes-filter')
    ->saves($savedFilters)
    ->filter(
        'code',
        [
            'label' => __("Code"),
            'wildcard',
        ]
    )
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'duration',
        [
            'label' => __("Durée"),
            'wildcard',
        ]
    )
    ->generateSection();

require 'ajax_index.php';

$table = $this->Table
    ->create($tableIdGlobals, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'AppraisalRuleCodes.code' => [
                'label' => __("Code"),
            ],
            'AppraisalRuleCodes.name' => [
                'label' => __("Nom"),
            ],
            'AppraisalRuleCodes.duration' => [
                'label' => __("Durée"),
            ],
        ]
    )
    ->data($dataGlobals)
    ->params(
        [
            'identifier' => 'AppraisalRuleCodes.id',
            'favorites' => false,
            'sortable' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewAppraisalRuleCode({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Visualiser {0}", '{1}'),
                'aria-label' => __("Visualiser {0}", '{1}'),
                'display' => $this->Acl->check('appraisal-rule-codes/view'),
                'params' => ['AppraisalRuleCodes.id', 'AppraisalRuleCodes.code'],
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => "$tableIdGlobals-section",
        'title' => __("Liste des codes globaux"),
        'table' => $table,
        'paginate' => false,
    ]
);

?>
<!--suppress JSUnresolvedFunction -->
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });
</script>
