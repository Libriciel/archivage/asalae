<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Csv->resultTable(
    [
        'code' => __("Code"),
        'name' => __("Nom"),
        'duration' => __("Durée"),
    ],
    $results
);
