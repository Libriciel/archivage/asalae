<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Csv->resultTable(
    [
        'name' => __("Nom"),
        'identifier' => __("Identifiant"),
        'definition_mask' => __("Définition"),
        'created' => __("Date de création"),
        'modified' => __("Date de modification"),
    ],
    $results
);
