<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->Form->create($entity, ['idPrefix' => 'add-validation-actor']);
require 'addedit-common.php';
echo $this->Form->end();
?>
<script>
    $('#add-validation-actor-app-type').change(function () {
        var value = $(this).val();
        $('.actor-type-users-div').toggle(value === 'USER').removeClass('hidden');
        $('#add-validation-actor-app-foreign-key').prop('required', value === 'USER');

        $('.actor-type-mails-div').toggle(value === 'MAIL').removeClass('hidden');
        $('#add-validation-actor-app-key').prop('required', value === 'MAIL');
        $('#add-validation-actor-actor-name').prop('required', value === 'MAIL');
    }).change();
    AsalaeGlobal.chosen($('#add-validation-actor-app-foreign-key'), __("Rechercher"));
    $('input[name=type_validation][value=V]').prop('checked', true);
</script>
