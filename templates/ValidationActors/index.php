<script>
    function compositionCallback(value, context) {
        switch (context.ValidationActors.app_type) {
            case 'USER':
                var meta = context.ValidationActors.meta;
                var name = context.ValidationActors.user.name;
                if (!name) {
                    name = context.ValidationActors.user.username;
                }
                var trads = {};
                trads = <?=json_encode($type_validations)?>;
                return __("Utilisateur : {0}, validation par {1}", name, trads[meta.type_validation]);
            case 'MAIL':
                return __("Email: {0}", context.ValidationActors.app_key);
            case 'SERVICE_ARCHIVES':
                return __("Service d'Archives");
            case 'SERVICE_DEMANDEUR':
                return __("Service demandeur");
            case 'SERVICE_PRODUCTEUR':
                return __("Service producteur");
        }
        return 'error';
    }
</script>
<?php
/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Circuits de validation"));
$this->Breadcrumbs->add(__("Circuit {0}", h($chain->get('name'))), '/validation-chains');
$this->Breadcrumbs->add(__("Etape"));
$this->Breadcrumbs->add(__("Etape {0}", h($stage->get('name'))), '/validation-stages/index/' . $chain->get('id'));
$this->Breadcrumbs->add(__("Acteurs"));
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-users', __("Acteurs de l'étape de validation")))
    . $this->Breadcrumbs->render()
);

$jsTable = $this->Table->getJsTableObject($tableId);

echo $this->element('modal', ['idTable' => $tableId, 'paginate' => false]);

$buttons = [];
if ($this->Acl->check('/ValidationActors/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-validation-actor-modal')
        ->modal(__("Ajouter un acteur à l'étape de circuit de validation"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "ValidationActors")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un acteur")),
            '/ValidationActors/add/' . $stage->get('id')
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}
echo $this->ModalForm
    ->create('edit-validation-actor', ['size' => 'large'])
    ->modal(__("Modifier un acteur de l'étape de circuit de validation"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "ValidationActors")')
    ->output(
        'function',
        'editValidationActor',
        '/ValidationActors/edit'
    )
    ->generate();

require 'ajax_index.php';

echo $this->Html->tag('div.container');
echo $this->Html->link(
    $this->Fa->i('fa-arrow-left', __("Retour aux étapes de validation du circuit {0}", h($chain->get('name')))),
    $this->Url->build('/validation-stages/index/' . $chain->get('id')),
    ['escape' => false]
);
echo $this->Html->tag('/div');
