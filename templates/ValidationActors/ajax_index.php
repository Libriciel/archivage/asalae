<?php

/**
 * @var Asalae\View\AppView $this
 */
if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover'])
    ->fields(
        [
            'ValidationActors.app_typetrad' => ['label' => __("Type")],
            'ValidationActors.composition' => [
                'label' => __("Paramètres"),
                'callback' => 'compositionCallback',
            ],
            'ValidationActors.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
            ],
            'ValidationActors.modified' => [
                'label' => __("Date de modification"),
                'type' => 'datetime',
                'display' => false,
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'ValidationActors.id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "editValidationActor({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier cet acteur"),
                'aria-label' => __("Modifier cet acteur"),
                'display' => $this->Acl->check('/validation-actors/edit'),
                'params' => ['ValidationActors.id'],
            ],
            [
                'type' => "button",
                'class' => "btn-link",
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/validation-actors/delete')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer cet acteur"),
                'aria-label' => $title,
                'confirm' => __("Êtes-vous sûr de vouloir supprimer cet acteur ?"),
                'display' => $this->Acl->check('/validation-actors/delete'),
                'params' => ['ValidationActors.id'],
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des acteurs de l'étape de validation"),
        'table' => $table,
    ]
);
