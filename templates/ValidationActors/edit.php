<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->Form->create($entity, ['idPrefix' => 'edit-validation-actor']);
require 'addedit-common.php';
echo $this->Form->end();
?>
<script>
    var appType = $('#edit-validation-actor-app-type');
    var value = appType.val();

    $('.actor-type-users-div').toggle(value === 'USER').removeClass('hidden');
    var appForeignKey = $('#edit-validation-actor-app-foreign-key').prop('required', value === 'USER');

    $('.actor-type-mails-div').toggle(value === 'MAIL').removeClass('hidden');
    $('#edit-validation-actor-app-key').prop('required', value === 'MAIL');
    $('#edit-validation-actor-actor-name').prop('required', value === 'MAIL');

    appType.disable();
    AsalaeGlobal.chosen(appForeignKey, __("Rechercher"));
</script>
