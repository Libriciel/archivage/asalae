<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->Form->control(
    'app_type',
    [
        'label' => __("Type d'acteur d'étape de circuit de validation"),
        'required' => true,
        'options' => $app_types,
        'empty' => __("-- Sélectionner un type d'acteur --"),
    ]
)

    // user
    . $this->Html->tag('div.hidden.actor-type-users-div')
    . '<hr>';

if (!$userCount) {
    echo $this->Html->tag(
        'p',
        __(
            "Aucun utilisateur disponible pour cette étape (un même utilisateur "
            . "ne peut apparaître deux fois dans la même étape)."
        ),
        ['class' => 'alert alert-warning']
    );
}

echo $this->Form->control(
    'app_foreign_key',
    [
        'label' => __("Utilisateur"),
        'options' => $users,
        'empty' => __("-- Sélectionner un utilisateur --"),
    ]
)
    . $this->Form->control(
        'type_validation',
        [
            'label' => __("Type Validation"),
            'options' => $type_validations,
            'type' => 'radio',
        ]
    )
    . $this->Html->tag('/div')

    // mail
    . $this->Html->tag('div.hidden.actor-type-mails-div')
    . '<hr>'
    . $this->Form->control(
        'app_key',
        [
            'label' => __("Email"),
            'type' => 'email',
            'placeholder' => 'email@asalae.fr',
        ]
    )
    . $this->Form->control(
        'actor_name',
        [
            'label' => __("Nom"),
            'placeholder' => 'M DUPUIT Martin',
        ]
    )
    . $this->Html->tag('/div');
