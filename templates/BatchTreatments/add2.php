<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $batchTreatment
 */

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;

echo $this->MultiStepForm->template('MultiStepForm/add_batch_treatment')->step(2) . '<hr>';

$jsTable = $this->Table->getJsTableObject($jsTableId);

$tableUnits = $this->Table
    ->create($jsTableId, ['class' => 'table table-striped table-hover'])
    ->url(
        $url = [
            'controller' => 'batch-treatments',
            'action' => 'paginate-archive-units',
            $batchTreatment->id,
            '?' => [
                'sort' => 'archival_agency_identifier',
                'direction' => 'asc',
            ],
        ]
    )
    ->fields(
        [
            'archival_agency_identifier' => [
                'label' => __("Identifiant"),
            ],
            'name' => [
                'label' => __("Nom"),
            ],
            'description' => [
                'label' => __("Description"),
            ],
            'dates' => [
                'label' => __("Dates extrêmes"),
            ],
            'original_total_count' => [
                'label' => __("Nombre de fichiers"),
            ],
            'original_total_size' => [
                'label' => __("Taille des fichiers"),
                'callback' => 'TableHelper.readableBytes',
            ],
        ]
    )
    ->data($batchTreatment->get('archive_units'))
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewUnitDescription({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0} (large)", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/description'),
                'displayEval' => Configure::read('ArchiveUnits.force_explore')
                    ? 'true'
                    : 'data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'href' => $this->Url->build('/archive-units/display-xml') . '/{0}',
                'target' => '_blank',
                'label' => $this->Fa->i('fa-file-text-o'),
                'title' => $title = __("Afficher la description de {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archive-units/display-xml'),
                'displayEval' => '!data[{index}].archive.description_xml_archive_file.is_large',
                'params' => ['id', 'name'],
            ],
            [
                'data-callback' => sprintf(
                    "actionRemoveArchiveUnit(%s, '%s')('{0}', false)",
                    $jsTable,
                    $this->Url->build('/batch-treatments/removeArchiveUnit') . '/' . $batchTreatment->id
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Retirer cette unité d'archives du traitement ?"),
                'displayEval' => 'data.length > 1',
                'params' => ['id', 'archival_agency_identifier'],
            ],
        ]
    );

echo $this->AjaxPaginator->create($paginatorId)
    ->url($url)
    ->table($tableUnits)
    ->count($unitsCount)
    ->generateTable();

echo $this->Form->create($batchTreatment, ['id' => 'add2-batch-treatment-form', 'idPrefix' => 'add2-batch-treatment']);
echo $this->Form->control('id', ['type' => 'hidden']);
echo $this->Form->end();
?>
<script>
    var form = $('#add2-batch-treatment-form');
    var modal = form.closest('.modal');
    var footer = modal.find('.modal-footer');
    footer.find('button.cancel')
        .removeAttr('data-dismiss')
        .off()
        .on('click.add2BatchTreatment', function (event) {
            event.preventDefault();
            $.ajax({
                url: '<?=$this->getRequest()->getUri()?>',
                method: 'DELETE',
                headers: {
                    Accept: 'application/json'
                },
                success: function (message) {
                    if (typeof message === 'object' && message.report !== 'done') {
                        throw message.report;
                    }
                    $('.modal:visible').modal('hide');
                },
                error: function (e) {
                    console.error(e);
                }
            });
        });

    function actionRemoveArchiveUnit(table, url) {
        return function (id) {
            $(table.table).off('.jstable').one('deleted.tablejs', function () {
                table.generateAll();
            });
            TableGenericAction.deleteAction(table, url)(id, false);
        }
    }
</script>
