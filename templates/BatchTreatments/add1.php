<?php

/**
 * @var Asalae\View\AppView $this
 * @var Asalae\Form\BatchTreatmentForm $formEntity
 */

echo $this->MultiStepForm->template('MultiStepForm/add_batch_treatment')->step(1) . '<hr>';

$form = $this->Html->tag('article');
$form .= $this->Html->tag('header.bottom-space');
$form .= $this->Html->tag('h3', __("Sélection du traitement"));
$form .= $this->Html->tag('/header');

$all = $this->getRequest()->getQuery('all');
if (!$all || $all[0] !== 'all') {
    $form .= $this->Html->tag(
        'div.alert.alert-info',
        __(
            "Attention, seules les archives (unités d'archives de plus haut niveau)"
            . " sont sélectionnées dans le filtre. Les sous unités d'archives ainsi"
            . " que leurs fichiers ne sont pas concernés par ce traitement (en dehors des règles d'héritage)."
        )
    );
}

$form .= $this->Html->tag('h4', __("Volumétrie suite à l'application des filtres de recherche"));
$form .= $this->ViewTable->generate(
    $sums,
    [
        __("Nombre d'unités d'archives") => 'au_count',
        __("Nombre de fichiers") => 'documents_count',
        __("Volumétrie des données") => 'documents_size|toReadableSize',
    ]
);

$form .= $this->Form->create($formEntity, ['idPrefix' => 'add1-batch-treatments']);
$form .= $this->Form->control(
    'type',
    [
        'label' => __("Sélection du traitement"),
        'options' => $formEntity->optionsTypes(),
        'empty' => __("-- Sélectionnez un traitement à effectuer --"),
    ]
);

// Si pas uniquement du seda V2 et qu'on a des codes spécifiques, avertissement
$codeV2Alert = $this->Html->tag(
    'div.alert.alert-info',
    __("Vous ne pouvez pas sélectionner les codes SEDA V2 car au moins une archive présente n'est pas en SEDA V2.")
);
$codesSpecifiques = __("Codes spécifiques");

/**
 * DUA, sort final
 */
$codeSpecificAppraisal = $archivesUnitsSedaNotV2 && isset($durations[$codesSpecifiques]);
$appraisal = ($codeSpecificAppraisal ? $codeV2Alert : '')
    . $this->Form->control(
        'appraisal__final',
        [
            'label' => 'Code : ' . __("Sort final à appliquer"),
            'options' => $finals,
            'empty' => __("-- Choisir un sort final --"),
        ]
    )
    . $this->Form->control(
        'appraisal__duration',
        [
            'label' => 'Duration : ' . __("Durée d'utilité administrative"),
            'options' => $codeSpecificAppraisal
                ? $durations[__("Codes globaux")]
                : $durations,
            'empty' => __("-- Sélectionner une DUA -- "),
        ]
    )
    . $this->Form->control(
        'appraisal__final_start_date',
        [
            'label' => 'StartDate : ' . __("Date de départ du calcul (sort final)"),
            'append' => $this->Date->picker('#add1-batch-treatments-appraisal-final-start-date'),
            'type' => 'text',
            'class' => 'datepicker',
            'placeholder' => __("jj/mm/aaaa"),
        ]
    );
$form .= $this->Html->tag(
    'div',
    $this->Form->fieldset(
        $appraisal,
        ['legend' => 'AppraisalRule : ' . __("Sort final")]
    ),
    ['style' => 'display: none', 'class' => 'batch-treatment-fieldset-add1 appraisal']
);

/**
 * Communicabilité
 */
$codesSpecificRestriction = $archivesUnitsSedaNotV2 && isset($codes[$codesSpecifiques]);
$restriction = ($codesSpecificRestriction ? $codeV2Alert : '')
    . $this->Form->control(
        'restriction__code',
        [
            'label' => 'Code : ' . __("Règle du délai de communicabilité"),
            'options' => $codesSpecificRestriction
                ? $codes[__("Codes globaux")]
                : $codes,
            'empty' => __("-- Sélectionner un délai --"),
        ]
    )
    . $this->Form->control(
        'restriction__start_date',
        [
            'label' => 'StartDate : ' . __("Date de départ du calcul (communicabilité)"),
            'append' => $this->Date->picker('#add1-batch-treatments-restriction-start-date'),
            'type' => 'text',
            'class' => 'datepicker',
            'placeholder' => __("jj/mm/aaaa"),
        ]
    );
$form .= $this->Html->tag(
    'div',
    $this->Form->fieldset(
        $restriction,
        ['legend' => 'AccessRestrictionRule : ' . __("Communicabilité")]
    ),
    ['style' => 'display: none', 'class' => 'batch-treatment-fieldset-add1 restriction']
);

/**
 * Nom
 */
$name = $this->Form->control(
    'name__research',
    [
        'label' => 'Name : ' . __("Rechercher le terme"),
        'help' => __("Est sensible à la casse et aux accents"),
    ]
)
    . $this->Form->control(
        'name__replace',
        [
            'label' => 'Name : ' . __("Remplacer par le terme"),
        ]
    );
$form .= $this->Html->tag(
    'div',
    $this->Form->fieldset(
        $name,
        ['legend' => 'Name : ' . __("Nom")]
    ),
    ['style' => 'display: none', 'class' => 'batch-treatment-fieldset-add1 name']
);

/**
 * Description
 */
$description = $this->Form->control(
    'description__research',
    [
        'label' => 'Description : ' . __("Rechercher le terme"),
        'help' => __("Est sensible à la casse et aux accents"),
    ]
)
    . $this->Form->control(
        'description__replace',
        [
            'label' => 'Description : ' . __("Remplacer par le terme"),
        ]
    );
$form .= $this->Html->tag(
    'div',
    $this->Form->fieldset(
        $description,
        ['legend' => 'Description : ' . __("Description")]
    ),
    ['style' => 'display: none', 'class' => 'batch-treatment-fieldset-add1 description']
);


/**
 * Mots-clés
 */
$keywords = $this->Form->control(
    'keyword__type',
    [
        'label' => __("Type de traitement sur les mots-clés"),
        'empty' => __("-- Choisir un type de traitement sur les mots-clés --"),
        'options' => $formEntity->optionsKeywordTypes(),
    ]
);
$keywords .= $this->Html->tag(
    'div',
    $this->Form->control(
        'keyword__add',
        [
            'label' => __("Mot clé qui sera ajouté"),
        ]
    ),
    ['class' => 'batch-treatment-keyword-add1 add', 'style' => 'display: none']
);
$keywords .= $this->Html->tag(
    'div',
    $this->Form->control(
        'keyword__delete',
        [
            'label' => __("Mot clé qui sera supprimé"),
        ]
    ),
    ['class' => 'batch-treatment-keyword-add1 delete', 'style' => 'display: none']
);
$keywords .= $this->Html->tag(
    'div',
    $this->Form->control(
        'keyword__research',
        [
            'label' => 'Keyword : ' . __("Rechercher le terme"),
        ]
    )
    . $this->Form->control(
        'keyword__replace',
        [
            'label' => 'Keyword : ' . __("Remplacer par le terme"),
        ]
    ),
    ['class' => 'batch-treatment-keyword-add1 replace', 'style' => 'display: none']
);
$form .= $this->Html->tag(
    'div',
    $this->Form->fieldset(
        $keywords,
        ['legend' => 'Keyword : ' . __("Mots-clés")]
    ),
    ['style' => 'display: none', 'class' => 'batch-treatment-fieldset-add1 keyword']
);

/**
 * Conversions
 */
$convert = $this->Form->control(
    'convert',
    [
        'label' => __("Conversions"),
        'empty' => __("-- Sélectionnez un traitement sur les conversions --"),
        'options' => $formEntity->optionsConvert(),
    ]
);
$form .= $this->Html->tag(
    'div',
    $this->Form->fieldset(
        $convert,
        ['legend' => __("Conversions")]
    ),
    ['style' => 'display: none', 'class' => 'batch-treatment-fieldset-add1 convert']
);

echo $form;
echo $this->Form->end();
echo $this->Html->tag('/article');
?>
<script>
    $('#add1-batch-treatments-archiving-system-id').on('change', function () {
        $('.table-archiving-system').hide();
        var value = $(this).val();
        if (value) {
            $('#table-add1-archiving-system-' + value).show();
        }
    });

    $('#add1-batch-treatments-type').on('change', function () {
        var value = $(this).val();
        $('.batch-treatment-fieldset-add1').hide();
        if (value) {
            $('.batch-treatment-fieldset-add1.' + value).show();
        }
    }).trigger('change');

    $('#add1-batch-treatments-keyword-type').on('change', function () {
        var value = $(this).val();
        $('.batch-treatment-keyword-add1').hide();
        if (value) {
            $('.batch-treatment-keyword-add1.' + value).show();
        }
    }).trigger('change');
</script>
