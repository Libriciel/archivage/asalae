<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface $batchTreatment
 * @var Asalae\Form\BatchTreatmentForm $formEntity
 */

use Cake\Datasource\EntityInterface;
use Cake\ORM\Entity;
use Cake\Utility\Hash;

echo $this->MultiStepForm->template('MultiStepForm/add_batch_treatment')->step(3) . '<hr>';

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($batchTreatment->get('identifier')));
$infos .= $this->Html->tag('p', h($batchTreatment->get('comment')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Volumétrie finale"));
$infos .= $this->ViewTable->generate(
    $sums,
    [
        __("Nombre d'unités d'archives") => 'au_count',
        __("Nombre de fichiers") => 'documents_count',
        __("Volumétrie des données") => 'documents_size|toReadableSize',
    ]
);

$infos .= $this->Html->tag('h4', __("Traitement à appliquer"));

$trad = Hash::get($formEntity->optionsTypes(), $formEntity->getData('type'));
$vEntity = new Entity($formEntity->getData() + ['typetrad' => $trad]);
switch ($formEntity->getData('type')) {
    case 'appraisal':
        $infos .= $this->ViewTable->generate(
            $vEntity,
            [
                __("Type") => 'typetrad',
                'Code : ' . __("Sort final à appliquer") => 'appraisal__final',
                'Duration : ' . __("Durée d'utilité administrative") => 'appraisal__duration',
                'StartDate : ' . __("Date de départ du calcul (sort final)") => 'appraisal__final_start_date',
            ]
        );
        break;
    case 'restriction':
        $infos .= $this->ViewTable->generate(
            $vEntity,
            [
                __("Type") => 'typetrad',
                'Code : ' . __("Règle du délai de communicabilité") => 'restriction__code',
                'StartDate : ' . __("Date de départ du calcul (communicabilité)") => 'restriction__start_date',
            ]
        );
        break;
    case 'name':
        $infos .= $this->ViewTable->generate(
            $vEntity,
            [
                __("Type") => 'typetrad',
                'Name : ' . __("Rechercher le terme") => 'name__research',
                'Name : ' . __("Remplacer par le terme") => 'name__replace',
            ]
        );
        break;
    case 'description':
        $infos .= $this->ViewTable->generate(
            $vEntity,
            [
                __("Type") => 'typetrad',
                'Description : ' . __("Rechercher le terme") => 'description__research',
                'Description : ' . __("Remplacer par le terme") => 'description__replace',
            ]
        );
        break;
    case 'keyword':
        $trad = Hash::get($formEntity->optionsKeywordTypes(), $formEntity->getData('keyword__type'));
        $vEntity->set('keyword_typetrad', $trad);
        switch ($formEntity->getData('keyword__type')) {
            case 'add':
                $infos .= $this->ViewTable->generate(
                    $vEntity,
                    [
                        __("Type") => 'typetrad',
                        __("Sous-type") => 'keyword_typetrad',
                        __("Mot clé qui sera ajouté") => 'keyword__add',
                    ]
                );
                break;
            case 'delete':
                $infos .= $this->ViewTable->generate(
                    $vEntity,
                    [
                        __("Type") => 'typetrad',
                        __("Sous-type") => 'keyword_typetrad',
                        __("Mot clé qui sera supprimé") => 'keyword__delete',
                    ]
                );
                break;
            case 'replace':
                $infos .= $this->ViewTable->generate(
                    $vEntity,
                    [
                        __("Type") => 'typetrad',
                        __("Sous-type") => 'keyword_typetrad',
                        'Keyword : ' . __("Rechercher le terme") => 'keyword__research',
                        'Keyword : ' . __("Remplacer par le terme") => 'keyword__replace',
                    ]
                );
                break;
        }
        break;
    case 'convert':
        $trad = Hash::get($formEntity->optionsConvert(), $formEntity->getData('convert'));
        $vEntity->set('converttrad', $trad);
        $infos .= $this->ViewTable->generate(
            $vEntity,
            [
                __("Type") => 'typetrad',
                __("Conversions") => 'converttrad',
            ]
        );
        break;
}

$infos .= $this->Html->tag('/article');

echo $infos;

echo $this->Form->create($batchTreatment, ['id' => 'add3-batch-treatment-form', 'idPrefix' => 'add3-batch-treatment']);
echo $this->Form->control('id', ['type' => 'hidden']);
echo $this->Form->end();
?>
<script>
    var form = $('#add3-batch-treatment-form');
    var modal = form.closest('.modal');
    var footer = modal.find('.modal-footer');
    footer.find('button.cancel')
        .removeAttr('data-dismiss')
        .off()
        .on('click.add2BatchTreatment', function (event) {
            event.preventDefault();
            $.ajax({
                url: '<?=$this->getRequest()->getUri()?>',
                method: 'DELETE',
                headers: {
                    Accept: 'application/json'
                },
                success: function (message) {
                    if (typeof message === 'object' && message.report !== 'done') {
                        throw message.report;
                    }
                    $('.modal:visible').modal('hide');
                },
                error: function (e) {
                    console.error(e);
                }
            });
        });
    footer.find('button.accept')
        .html('<?=$this->Fa->i('fa-cogs', __("Appliquer le traitement"))?>');
</script>
