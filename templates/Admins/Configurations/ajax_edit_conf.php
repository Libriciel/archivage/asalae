<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface $se
 * @var AsalaeCore\Form\ConfigurationForm $form
 */

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

?>
<!--suppress RequiredAttributes -->
<script>
    function urlToImg(value) {
        if (value && value.substr(0, 4) !== '<img') {
            return $('<img alt="prev">')
                .attr('src', value)
                .css({
                    "max-width": "160px",
                    "max-height": "160px"
                });
        }
        return value;
    }

    function selectableUploadRadio(value, context) {
        if (!AsalaeGlobal.is_numeric(context.id) && context.id !== 'local') {
            return '';
        }
        var input = $('<input type="radio" name="fileuploads[id]">').val(context.id);
        setTimeout(function () {
            $('#config-upload-logo-table')
                .find('input[name="fileuploads[id]"]')
                .prop('checked', false)
                .first()
                .prop('checked', true);
        }, 0);
        return input;
    }
</script>
<?php
$configArray = json_decode($config, true);

// Permet d'afficher "valeur custom" si la valeur n'est pas dans la liste
$passwordComplexityDefault = Hash::get($configArray, 'Password.complexity');
if ($passwordComplexityDefault && !in_array($passwordComplexityDefault, array_keys($password_complexity))) {
    $passwordComplexityDefault = -1;
}
$passwordAdminComplexityDefault = Hash::get($configArray, 'Password.admin.complexity');
if ($passwordAdminComplexityDefault && !in_array($passwordAdminComplexityDefault, array_keys($password_complexity))) {
    $passwordAdminComplexityDefault = -1;
}

$defaultEncodeFilename = Hash::get($default, 'DataCompressorUtility.encodeFilename');
if ($defaultEncodeFilename === false) {
    $defaultEncodeFilename = $datacompressor_encodefilename['0'];
}
$defaultUseShred = Hash::get($default, 'FilesystemUtility.useShred');
if ($defaultUseShred !== null) {
    $defaultUseShred = $filesystem_useshred[(int)Hash::get($default, 'FilesystemUtility.useShred')];
}

$view = $this;

$tabs = $this->Tabs->create('tabs-edit-configuration', ['class' => 'row no-padding']);

echo $this->Form->create($form, ['id' => 'config-editor', 'autocomplete' => 'new-password']);

echo '<label class="hide">autocomplete obliterator'
    . '<input type="text" value="">'
    . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';

// Pour la validation uniquement
echo $this->Form->control(
    'client_ip',
    ['type' => 'hidden', 'val' => $myip = $this->getRequest()->clientIp()]
);

$tabs->add(
    'tab-edit-configuration-utility',
    $this->Fa->i('fa-cogs', __x('tab', "Utilitaires")),
    // -----------------------------------------------------------------------------
    $this->Form->control(
        'app_fullbaseurl',
        [
            'label' => __("Url de base de l'application"),
            'default' => Hash::get($configArray, 'App.fullBaseUrl'),
            'placeholder' => 'http://asalae2.fr',
            'required',
        ]
    )

    . $this->Form->control(
        'ignore_invalid_fullbaseurl',
        [
            'label' => __("Ignore les erreurs sur l'url de base de l'application"),
            'default' => Hash::get($configArray, 'App.ignore_invalid_fullbaseurl'),
            'type' => 'checkbox',
        ]
    )

    . $this->Form->control(
        'hash_algo',
        [
            'label' => __("algorithme de hashage"),
            'default' => Hash::get($configArray, 'hash_algo'),
            'options' => $hash_algo,
            'empty' => __("-- Utiliser la valeur par défaut ({0}) --", Hash::get($default, 'hash_algo')),
        ]
    )

    . $this->Form->control(
        'password_complexity_select',
        [
            'label' => __("Complexité du mot de passe"),
            'default' => $passwordComplexityDefault,
            'options' => $password_complexity,
            'empty' => __(
                "-- Utiliser la valeur par défaut ({0}) --",
                Hash::get($default, 'Password.complexity')
            ),
        ]
    )

    . $this->Form->control(
        'password_complexity',
        [
            'label' => false,
            'default' => Hash::get($configArray, 'Password.complexity'),
            'type' => 'number',
            'min' => 0,
            'step' => 1,
        ]
    )

    . $this->Form->control(
        'password_admin_complexity_select',
        [
            'label' => __("Complexité du mot de passe pour l'administration technique"),
            'default' => $passwordAdminComplexityDefault,
            'options' => $password_complexity,
            'empty' => __(
                "-- Utiliser la valeur par défaut ({0}) --",
                Hash::get($default, 'Password.admin.complexity')
            ),
        ]
    )

    . $this->Form->control(
        'password_admin_complexity',
        [
            'label' => false,
            'default' => Hash::get($configArray, 'Password.admin.complexity'),
            'type' => 'number',
            'min' => 0,
            'step' => 1,
        ]
    )

    . $this->Form->control(
        'ratchet_connect',
        [
            'label' => __("Web socket Ratchet (serveur de notifications)"),
            'default' => Hash::get($configArray, 'Ratchet.connect'),
            'placeholder' => 'wss://' . Hash::get($configArray, 'Domain') . '/wss',
            'required',
        ]
    )

    . $this->Form->control(
        'datacompressor_encodefilename',
        [
            'label' => __("Encodage du nom des fichiers contenus dans les fichiers ZIP"),
            'default' => Hash::get($configArray, 'DataCompressorUtility.encodeFilename'),
            'options' => $datacompressor_encodefilename,
            'empty' => __("-- Utiliser la valeur par défaut ({0}) --", $defaultEncodeFilename),
        ]
    )

    . $this->Form->control(
        'filesystem_useshred',
        [
            'label' => __("Utiliser shred pour la destruction des fichiers"),
            'default' => Hash::get($configArray, 'FilesystemUtility.useShred'),
            'options' => $filesystem_useshred,
            'empty' => __(
                "-- Utiliser la valeur par défaut ({0}) --",
                $defaultUseShred ? $filesystem_useshred[1] : $filesystem_useshred[0]
            ),
        ]
    )

    . $this->Form->control(
        'paginator_limit',
        [
            'label' => __("Nombre de résultats par pages par défaut"),
            'default' => Hash::get($configArray, 'Pagination.limit', 20),
            'type' => 'number',
            'step' => 1,
            'min' => 1,
            'max' => 200,
            'help' => __("Une valeur élevée a un impacte négatif sur les performances"),
        ]
    )

    . $this->Form->control(
        'ajaxpaginator_limit',
        [
            'label' => __("Nombre de résultats par pages dans les modales"),
            'default' => Hash::get($configArray, 'AjaxPaginator.limit', 100),
            'type' => 'number',
            'step' => 1,
            'min' => 1,
            'max' => 1000,
        ]
    )

    . $this->Form->control(
        'downloads_asyncfilecreationtimelimit',
        [
            'label' => __(
                "Durée maximale de temps de création de zip avant de "
                . "proposer un téléchargement asynchrone (secondes)"
            ),
            'default' => Hash::get($configArray, 'Downloads.asyncFileCreationTimeLimit'),
            'placeholder' => 30,
            'min' => 1,
            'max' => 240,
        ]
    )

    . $this->Form->control(
        'downloads_tempfileconservationtimelimit',
        [
            'label' => __(
                "Temps de conservation des fichiers temporaires disponibles au téléchargement (heures)"
            ),
            'default' => Hash::get($configArray, 'Downloads.tempFileConservationTimeLimit'),
            'placeholder' => 48,
            'min' => 1,
            'max' => 24 * 30,
        ]
    )

    . '<hr>'

    . $this->Form->control(
        'proxy_host',
        [
            'label' => __("Host du proxy"),
            'default' => Hash::get($configArray, 'Proxy.host'),
            'placeholder' => 'proxy.exemple.com',
            'min' => 1,
            'max' => 180,
        ]
    )
    . $this->Form->control(
        'proxy_port',
        [
            'label' => __("Port du proxy"),
            'default' => Hash::get($configArray, 'Proxy.port'),
            'placeholder' => 8080,
            'min' => 1,
            'max' => 65535,
        ]
    )
    . $this->Form->control(
        'proxy_username',
        [
            'label' => __("Utilisateur de connexion du proxy"),
            'default' => Hash::get($configArray, 'Proxy.username'),
        ]
    )
    . $this->Form->control(
        'proxy_password',
        [
            'label' => __("Mot de passe de connexion du proxy"),
            'default' => Hash::get($configArray, 'Proxy.password'),
            'placeholder' => '******************',
            'type' => 'password',
        ]
    )

    . '<hr>'

    . $this->Form->control(
        'attestations_destruction_delay',
        [
            'label' => __(
                "Attestations d'élimination : délai de destruction des "
                . "sauvegardes des fichiers des archives (en nombre de jours)"
            ),
            'help' => __(
                "L'attestation d'élimination sera produite une fois "
                . "le délai passé à partir de l'élimination des archives. "
                . "Si le délai est fixé à 0 alors l'attestation sera "
                . "immédiatement produite. Si le délai est supérieur à 0, "
                . "il est possible d'activer la production d'attestations "
                . "intermédiaires."
            ),
            'default' => Hash::get($configArray, 'Attestations.destruction_delay'),
            'min' => 0,
        ]
    )
);

$wildz255 = '[1-2]?\*[0-9]?|1[0-9]\*|2[0-5]\*';
$z255 = "($wildz255|0?[0-9]{1,2}|1[0-9]{2}|2[0-4][0-9]|25[0-5])";
$ipv6 = '[:0-9a-fA-F]+';
$tabs->add(
    'tab-edit-configuration-accesses',
    $this->Fa->i('fa-key', __x('tab', "Accès")),
    // -----------------------------------------------------------------------------
    $this->Form->control(
        'ip_enable_whitelist',
        [
            'label' => __("Filtre les accès par IP"),
            'default' => Hash::get($configArray, 'Ip.enable_whitelist'),
        ]
    )
    . $this->Html->tag(
        'p',
        __("Filtre les ips qui peuvent accéder à l'interface d'administration technique")
        . '<br>' . __("Votre adresse IP est ''{0}''", $myip)
    )
    . $this->Form->control(
        'ip_whitelist',
        [
            'name' => 'ip_whitelist[]',
            'label' => __("Adresses IPs authorisées"),
            'val' => Hash::get($configArray, 'Ip.whitelist.0'),
            'required' => false,
            'help' => __("Les adresses locales 127.0.0.1 et ::1 sont activés par défaut")
                . '<br>' . __("Accepte les wildcards. Exemple: 10.0.*.*"),
            'pattern' => "(($z255\.){3}$z255$|^$ipv6$)",
        ]
    )
);

$tabs->add(
    'tab-edit-configuration-style',
    $this->Fa->i('fa-picture-o', __x('tab', "Apparence")),
    // -----------------------------------------------------------------------------
    $this->Html->tag(
        'div.form-group.fake-input',
        $this->Html->tag('span.fake-label', __("Logo sur la page de login"))
        . $this->Upload
            ->create('config-upload-logo', ['class' => 'table table-striped table-hover hide'])
            ->fields(
                [
                    'select' => [
                        'label' => __("Sélectionner"),
                        'class' => 'radio-td',
                        'callback' => 'selectableUploadRadio',
                    ],
                    'prev' => ['label' => __("Prévisualiser"), "callback" => "urlToImg", 'escape' => false],
                    'name' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
                    'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
                    'message' => ['label' => __("Message"), 'style' => 'min-width: 100px', 'class' => 'message'],
                ]
            )
            ->data($uploadFiles)
            ->params(
                [
                    'identifier' => 'id',
                    'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
                ]
            )
            ->actions(
                [
                    function ($table) use ($view) {
                        $url = $view->Url->build('/Upload/delete');
                        return [
                            'onclick' => "GenericUploader.fileDelete({$table->tableObject}, '$url', {0})",
                            'type' => 'button',
                            'class' => 'btn-link delete',
                            'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                            'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                            'title' => __("Supprimer {0}", '{1}'),
                            'aria-label' => __("Supprimer {0}", '{1}'),
                            'params' => ['id', 'name'],
                        ];
                    },
                    function ($table) use ($view, $se) {
                        $url = $view->Url->build('/Admins/deleteLogo');
                        $id = $se ? $se->get('id') : null;
                        return [
                            'onclick' => "deleteLogoClient({$table->tableObject}, '$url', {$id})",
                            'type' => 'button',
                            'class' => 'btn-link delete',
                            'displayEval' => 'data[{index}].webroot',
                            'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                            'title' => __("Supprimer {0}", '{1}'),
                            'aria-label' => __("Supprimer {0}", '{1}'),
                            'params' => ['id', 'name'],
                        ];
                    },
                ]
            )
            ->generate(
                [
                    'attributes' => ['accept' => 'image/*'],
                    'allowDuplicateUploads' => true,
                    'target' => $this->Url->build('Upload/index/?replace=true'),
                ]
            )
    )

    . $this->Form->control(
        'libriciel_login_background',
        [
            'label' => __("Couleur de fond du logo pour la page de login"),
            'default' => ($style = Hash::get($configArray, 'Libriciel.login.logo-client.style'))
                ? substr($style, strpos($style, ': ') + 2)
                : '#337ab7',
            'type' => 'color',
        ]
    )
);

$tabs->add(
    'tab-edit-configuration-email',
    $this->Fa->i('fa-envelope', __x('tab', "E-mails")),
    // -----------------------------------------------------------------------------

    $this->Form->control(
        'emails_from',
        [
            'label' => __("Champ from"),
            'default' => Hash::get($configArray, 'Email.default.from'),
            'placeholder' => 'contact@asalae2.fr',
        ]
    )

    . '<hr>'

    . $this->Form->control(
        'emails_class',
        [
            'label' => __("Méthode d'envoi"),
            'data-placeholder' => __("-- Choisir une méthode --"),
            'options' => [
                [
                    'text' => 'Mail',
                    'value' => 'Mail',
                    'title' => __("Send using PHP mail function"),
                ],
                [
                    'text' => 'Smtp',
                    'value' => 'Smtp',
                    'title' => __("Send using SMTP"),
                ],
            ],
            'default' => Hash::get($configArray, 'EmailTransport.default.className'),
        ]
    )

    . $this->Form->control(
        'emails_host',
        [
            'label' => __("Hôte"),
            'default' => Hash::get($configArray, 'EmailTransport.default.host'),
            'placeholder' => 'localhost',
        ]
    )

    . $this->Form->control(
        'emails_port',
        [
            'label' => __("Port"),
            'default' => Hash::get($configArray, 'EmailTransport.default.port'),
            'placeholder' => '25',
            'min' => 1,
            'step' => 1,
            'max' => 65535,
        ]
    )

    . $this->Form->control(
        'emails_username',
        [
            'label' => __("Identifiant de connexion"),
            'default' => Hash::get($configArray, 'EmailTransport.default.username'),
            'placeholder' => 'contact',
        ]
    )

    . $this->Form->control(
        'emails_password',
        [
            'label' => __("Password"),
            'default' => Hash::get($configArray, 'EmailTransport.default.password'),
            'placeholder' => '*************',
            'type' => 'password',
        ]
    )

    . $this->Html->tag(
        'button.btn.btn-warning',
        $this->Fa->i('fa-envelope-o', __("Envoyer un mail de test")),
        ['onclick' => 'sendTestMail()', 'type' => 'button']
    )
);

$datasourceTabs = $this->Tabs->create('tabs-edit-configuration-datasources-tabs', ['class' => 'row no-padding']);
$datasourceTabs->add(
    'tab_datasources1',
    __x('tab_datasources', "default"),
    //--------------------------------------------------------------------------
    $this->Form->control(
        'datasources_default_driver',
        [
            'label' => __x('datasource', "Driver"),
            'default' => Hash::get($configArray, 'Datasources.default.driver'),
        ]
    )

    . $this->Form->control(
        'datasources_default_host',
        [
            'label' => __x('datasource', "Hôte"),
            'default' => Hash::get($configArray, 'Datasources.default.host'),
            'placeholder' => 'localhost',
        ]
    )

    . $this->Form->control(
        'datasources_default_username',
        [
            'label' => __x('datasource', "Username"),
            'default' => Hash::get($configArray, 'Datasources.default.username'),
            'placeholder' => 'admin',
        ]
    )

    . $this->Form->control(
        'datasources_default_password',
        [
            'label' => __x('datasource', "Password"),
            'default' => Hash::get($configArray, 'Datasources.default.password'),
            'placeholder' => '************',
            'type' => 'password',
        ]
    )

    . $this->Form->control(
        'datasources_default_database',
        [
            'label' => __x('datasource', "Database"),
            'default' => Hash::get($configArray, 'Datasources.default.database'),
            'placeholder' => 'asalae2',
        ]
    )

    . $this->Form->control(
        'datasources_default_log',
        [
            'label' => __x('datasource', "Journaliser les requêtes"),
            'default' => Hash::get($configArray, 'Datasources.default.log'),
            'type' => 'checkbox',
        ]
    )

    . $this->Form->control(
        'datasources_default_ignore_invalid',
        [
            'label' => __("Ignore les erreurs sur la base de données"),
            'default' => Hash::get($configArray, 'Datasources.default.ignore_invalid'),
            'type' => 'checkbox',
        ]
    )
);
$datasourceTabs->add(
    'tab_datasources2',
    __x('tab_datasources', "debug_kit"),
    //--------------------------------------------------------------------------

    $this->Form->control(
        'datasources_debug_kit_driver',
        [
            'label' => __x('datasource', "Driver"),
            'default' => Hash::get($configArray, 'Datasources.debug_kit.driver'),
        ]
    )

    . $this->Form->control(
        'datasources_debug_kit_host',
        [
            'label' => __x('datasource', "Hôte"),
            'default' => Hash::get($configArray, 'Datasources.debug_kit.host'),
            'placeholder' => 'localhost',
        ]
    )

    . $this->Form->control(
        'datasources_debug_kit_username',
        [
            'label' => __x('datasource', "Username"),
            'default' => Hash::get($configArray, 'Datasources.debug_kit.username'),
            'placeholder' => 'admin',
        ]
    )

    . $this->Form->control(
        'datasources_debug_kit_password',
        [
            'label' => __x('datasource', "Password"),
            'default' => Hash::get($configArray, 'Datasources.debug_kit.password'),
            'placeholder' => '************',
            'type' => 'password',
        ]
    )

    . $this->Form->control(
        'datasources_debug_kit_database',
        [
            'label' => __x('datasource', "Database"),
            'default' => Hash::get($configArray, 'Datasources.debug_kit.database'),
            'placeholder' => 'asalae2',
        ]
    )
);
$datasourceTabs->add(
    'tab_datasources3',
    __x('tab_datasources', "test"),
    //--------------------------------------------------------------------------
    $this->Form->control(
        'datasources_test_driver',
        [
            'label' => __x('datasource', "Driver"),
            'default' => Hash::get($configArray, 'Datasources.test.driver'),
        ]
    )

    . $this->Form->control(
        'datasources_test_host',
        [
            'label' => __x('datasource', "Hôte"),
            'default' => Hash::get($configArray, 'Datasources.test.host'),
            'placeholder' => 'localhost',
        ]
    )

    . $this->Form->control(
        'datasources_test_username',
        [
            'label' => __x('datasource', "Username"),
            'default' => Hash::get($configArray, 'Datasources.test.username'),
            'placeholder' => 'admin',
        ]
    )

    . $this->Form->control(
        'datasources_test_password',
        [
            'label' => __x('datasource', "Password"),
            'default' => Hash::get($configArray, 'Datasources.test.password'),
            'placeholder' => '************',
            'type' => 'password',
        ]
    )

    . $this->Form->control(
        'datasources_test_database',
        [
            'label' => __x('datasource', "Database"),
            'default' => Hash::get($configArray, 'Datasources.test.database'),
            'placeholder' => 'asalae2',
        ]
    )
);

$tabs->add(
    'tab-edit-configuration-datasources',
    $this->Fa->i('fa-database', __x('tab', "Datasources")),
    // -----------------------------------------------------------------------------
    $datasourceTabs
);

$workers = array_keys(Configure::read('Beanstalk.workers'));
$workerTab = [];
foreach ($workers as $name) {
    $workerTab[] = $this->Html->tag('h4', $name)
        . $this->Form->control(
            'workers_' . $name . '_run',
            [
                'label' => __x('worker', "Commande de lancement"),
                'default' => Hash::get($configArray, 'Beanstalk.workers.' . $name . '.run'),
                'placeholder' => 'bin/cake worker ' . $name,
                'title' => __(
                    "\"bin/cake worker <nom_du_worker>\" est le fonctionnement par défaut
\"docker run -d <nom_de_l_image_docker> <commande>\" lancé par docker
\"ssh w02.asalae2.fr \"/var/www/asalae2/bin worker <nom_du_worker>\"\" pour du ssh"
                ),
            ]
        )
        . $this->Form->control(
            'workers_' . $name . '_kill',
            [
                'label' => __x('worker', "Commande d'arrêt"),
                'default' => Hash::get($configArray, 'Beanstalk.workers.' . $name . '.kill'),
                'placeholder' => 'kill {pid}',
                'title' => __(
                    "\"kill {pid}\" fonctionnement par défaut
\"docker kill {{hostname}}\" pour du docker
\"ssh {{hostname}} \"kill {{pid}}\"\" pour du ssh"
                ),
            ]
        )
        . $this->Form->control(
            'workers_' . $name . '_quantity',
            [
                'label' => __x('worker', "Nombre de workers qui seront lancés avec cette configuration"),
                'default' => Hash::get($configArray, 'Beanstalk.workers.' . $name . '.quantity'),
                'placeholder' => '1',
                'type' => 'number',
                'min' => 0,
                'max' => 255,
                'step' => 1,
            ]
        )
        . $this->Form->control(
            'workers_' . $name . '_active',
            [
                'label' => __x('worker', "Ce worker est actif ?"),
                'default' => Hash::get($configArray, 'Beanstalk.workers.' . $name . '.active'),
                'options' => ['0' => __("Non")],
                'empty' => __("Oui"),
            ]
        );
}

$tabs->add(
    'tab-edit-configuration-workers',
    $this->Fa->i('fa-wrench', __x('tab', "Workers")),
    // -----------------------------------------------------------------------------
    implode('<hr>', $workerTab)
);

$tabs->add(
    'tab-edit-configuration-editor',
    $this->Fa->i('fa-file-text', __x('tab', "Editeur de texte")),
    // -----------------------------------------------------------------------------
    $this->Html->tag(
        'p',
        __("Les valeurs du json seront écrasées par les champs saisis dans les autres onglets"),
        ['class' => 'alert alert-warning']
    )

    . $this->Html->tag(
        'p',
        __("Fichier de configuration : {0}", $appLocalConfigFileUri),
        ['class' => 'alert alert-warning']
    )

    . $this->Form->control(
        'config',
        [
            'label' => __("Configuration"),
            'rows' => 50,
            'default' => $config,
            'style' => 'font-family: monospace',
            'spellcheck' => 'false',
        ]
    )
);

echo $tabs . $this->Form->end();
?>
<script>
    var ipWhiteList = <?=json_encode(Hash::get($configArray, 'Ip.whitelist'))?> ?? [];

    /**
     * Colore en rouge le onglets en erreur, et selectionne le premier
     */
    AsalaeGlobal.colorTabsInError();

    $('#password-complexity-select').change(function () {
        var value = $(this).val(),
            passwordComplexity = $('#password-complexity');
        if (value === '-1') {
            value = '';
            passwordComplexity.focus();
        }
        passwordComplexity.val(value);
    });
    $('#password-admin-complexity-select').change(function () {
        var value = $(this).val(),
            passwordComplexity = $('#password-admin-complexity');
        if (value === '-1') {
            value = '';
            passwordComplexity.focus();
        }
        passwordComplexity.val(value);
    });


    /**
     * Suppression d'un background-image local (!= TableGenericAction.deleteAction)
     * @param table
     * @param url
     * @param id
     */
    function deleteLogoClient(table, url, id) {
        var useConfirm = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
        if (useConfirm && !confirm(__("Êtes-vous sûr de vouloir supprimer cet enregistrement ?"))) {
            return;
        }
        $.ajax({
            url: url.replace(/\/$/, '') + '/' + id,
            method: 'DELETE',
            success: function (message) {
                if (message.report === 'done') {
                    table.removeDataId('local');
                    $(table.table)
                        .find('tr[data-id="local"]')
                        .fadeOut(400, function () {
                            $(this).remove();
                        });
                } else {
                    throw message.report;
                }
            },
            error: function () {
                $(table.table).find('tr[data-id="' + id + '"]').addClass('danger');
            }
        });
    }

    function sendTestMail() {
        var email = prompt(
            __("Veuillez entrer le mail de destination")
        <?=$defaultEmail ? ', "' . $defaultEmail . '"' : ''?>
    )
        ;
        if (email) {
            if (!$('<input type="email">').val(email).is(':valid')) {
                alert(__("Email invalide"));
                return;
            }
            $.ajax({
                url: '<?=$this->Url->build('/admins/sendTestMail')?>',
                method: 'POST',
                data: {
                    email: email
                },
                headers: {
                    Accept: 'application/json'
                },
                success: function (content, textStatus, jqXHR) {
                    if (jqXHR.getResponseHeader('X-Email-Debug') === 'true') {
                        window.open(content, '_blank');
                        return;
                    }
                    if (content.success) {
                        alert(__("L'email de test a bien été envoyé"));
                    } else {
                        return this.error();
                    }
                },
                error: function () {
                    alert(PHP.messages.genericError);
                }
            })
        }
    }

    var whiteListInput = $('#ip-whitelist');

    function ipWhitelistInputChange() {
        var existsEmpty = false;
        whiteListInput.parent().find('> input').each(
            function () {
                if ($(this).val() === '') {
                    if (existsEmpty) {
                        $(this).remove();
                    }
                    existsEmpty = true;
                }
            }
        );
        if ($(this).val() && !existsEmpty) {
            var input = whiteListInput.clone();
            input.removeAttr('id');
            input.val('');
            input.insertAfter(whiteListInput.parent().find('> input').last());
            input.on('change keyup', ipWhitelistInputChange);
        }
    }

    for (var i = 0; i < ipWhiteList.length; i++) {
        var input = whiteListInput.clone();
        input.removeAttr('id');
        input.val(ipWhiteList[i]);
        input.insertBefore(whiteListInput);
        input.change(ipWhitelistInputChange);
    }
    whiteListInput.val('');
    whiteListInput.on('change keyup', ipWhitelistInputChange);
</script>
