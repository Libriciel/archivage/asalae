<?php

/**
 * @var Asalae\View\AppView $this
 */

$hours = [];
for ($h = 0; $h < 24; $h++) {
    for ($m = 0; $m < 60; $m += 5) {
        $hours[$s = sprintf('%02d:%02d:00', $h, $m)] = $s;
    }
}

echo $this->Form->create($form, ['idPrefix' => 'interrupt']);

echo $this->Form->control(
    'enabled',
    [
        'label' => __("Activer immédiatement l'interruption"),
    ]
);
echo $this->Form->control(
    'scheduled__begin',
    [
        'label' => __("Programmer la date de début de l'interruption"),
        'type' => 'text',
        'append' => $this->Date->datetimePicker('#interrupt-scheduled-begin'),
        'class' => 'datepicker',
    ]
);
echo $this->Form->control(
    'scheduled__end',
    [
        'label' => __("Programmer la date de fin de l'interruption"),
        'type' => 'text',
        'append' => $this->Date->datetimePicker('#interrupt-scheduled-end'),
        'class' => 'datepicker',
    ]
);
echo $this->Form->control(
    'message',
    [
        'label' => __("Message à afficher aux utilisateurs"),
        'type' => 'textarea',
        'class' => 'mce mce-small',
    ]
);

echo $this->Form->fieldset(
    $this->Form->control(
        'periodic__begin',
        [
            'label' => __("Date de début de l'interruption journalière"),
            'options' => $hours,
            'empty' => __("-- Sélectionner une heure --"),
        ]
    )
    . $this->Form->control(
        'periodic__end',
        [
            'label' => __("Date de fin de l'interruption journalière"),
            'options' => $hours,
            'empty' => __("-- Sélectionner une heure --"),
        ]
    )
    . $this->Form->control(
        'message_periodic',
        [
            'label' => __("Message lors de l'interruption journalière"),
            'type' => 'textarea',
            'class' => 'mce mce-small',
        ]
    ),
    ['legend' => __("Interruption journalière")]
);

echo $this->Form->fieldset(
    $this->Form->control(
        'enable_whitelist',
        [
            'label' => __("Permettre aux webservices de continuer à accéder à l'application"),
        ]
    )
    . $this->Form->control(
        'whitelist_headers_inline',
        [
            'label' => __("Entêtes identifiant les webservices autorisés (séparés par une virgule(,))"),
        ]
    ),
    ['legend' => __("Autorisation des webservices")]
);

echo $this->Form->fieldset(
    $this->Form->control(
        'enable_workers',
        [
            'label' => __("Permettre aux workers de continuer à travailler"),
        ]
    ),
    ['legend' => __("Autorisation des workers")]
);

echo $this->Form->submit();
echo $this->Form->end();
?>

<script>
    var interruptModal = $('#service-interruption'),
        interruptForm = interruptModal.find('form');
    interruptForm.submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: interruptForm.attr('action'),
            method: 'POST',
            data: interruptForm.serialize(),
            success: function (content, textStatus, jqXHR) {
                switch (jqXHR.getResponseHeader('X-Asalae-Success')) {
                    case 'true':
                        interruptModal.modal("hide");
                        break;
                    case 'false':
                        interruptModal.find('div.modal-body').html(content);
                        break;
                }
            },
            error: function () {
                alert(PHP.messages.genericError);
            }
        });
    });

    setTimeout(function () {
        tinymce.init({
            selector: 'textarea.mce-small',
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste help wordcount'
            ],
            // @see https://www.tiny.cloud/docs/advanced/editor-control-identifiers
            toolbar: 'undo redo | bold italic underline forecolor backcolor | fontsizeselect | bullist numlist | code',
            contextmenu: 'link | cut copy paste | formats removeformat | align | hr',
            branding: false,
            language: tinymceDefaultLanguage,
            setup: function (editor) {
                editor.on('keyup', function (e) {
                    editor.save();
                });
                editor.on('change', function (e) {
                    editor.save();
                    $(editor.getElement()).trigger('change');
                });
                setTimeout(function () {
                    $(editor.getElement()).css({display: 'block', position: 'absolute', "z-index": -1});
                }, 500);
            }
        });
        var modal = $('.modal:visible').last();
        modal.one('hidden.bs.modal', function () {
            tinymce.remove('textarea.mce-small');
        });
        $(document).off('.tinymodal').on('focusin.tinymodal', function (e) {
            var dialog = $(e.target).closest(".tox-dialog");
            if (dialog.length && modal.find(dialog).length === 0) {
                var wrapper = $('.tox-tinymce-aux');
                modal.append(wrapper);
            }
        });
    }, 0);

    var interruptFields = $('#interrupt-enabled, #interrupt-scheduled-begin, #interrupt-scheduled-end');
    interruptFields.on('change', function () {
        var interrupt = interruptFields.filter(function () {
            if ($(this).attr('type') === 'checkbox') {
                return $(this).prop('checked');
            }
            return $(this).val();
        });
        $('#interrupt-message').prop('required', interrupt.length > 0)
            .closest('div.form-group')
            .toggleClass('required', interrupt.length > 0);
    }).change();
    var dailyFields = $('#interrupt-periodic-begin, #interrupt-periodic-end');
    dailyFields.on('change', function () {
        var daily = dailyFields.filter(function () {
            return $(this).val();
        });
        $('#interrupt-message-periodic').prop('required', daily.length > 0)
            .closest('div.form-group')
            .toggleClass('required', daily.length > 0);
    }).change();
</script>