<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create(
    $entity,
    [
        'id' => $idForm = 'edit-timestamper-form',
        'autocomplete' => 'new-password',
        'idPrefix' => $idPrefix = 'edit-timestamper',
    ]
);

require 'addedit-common-timestamper.php';
echo $this->Form->end();
?>
<script>
    selectedDriver($('#edit-timestamper-driver'));
    AsalaeGlobal.select2($('#edit-timestamper-org-entities-ids'));
</script>
