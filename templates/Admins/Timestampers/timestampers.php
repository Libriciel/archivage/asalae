<?php

/**
 * @var Asalae\View\AppView $this
 */

$jsTable = $this->Table->getJsTableObject($tableId);

echo $this->element('modal', ['idTable' => $tableId, 'paginate' => false]);
$tableTimestampers = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover'])
    ->fields(
        [
            'Timestampers.name' => ['label' => __("Nom du service d'horodatage")],
            'Timestampers.driver' => ['label' => __("Driver")],
        ]
    )
    ->data($timestampers)
    ->params(
        [
            'identifier' => 'Timestampers.id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionEditTimestamper({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'params' => ['Timestampers.id', 'Timestampers.name'],
            ],
            [
                'onclick' => "testTimestamper(null, {0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-calendar-check-o text-warning'),
                'title' => $title = __("Tester {0}", '{1}'),
                'aria-label' => $title,
                'params' => ['Timestampers.id', 'Timestampers.name'],
            ],
            [
                'type' => "button",
                'class' => "btn-link",
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/Admins/deleteTimestamper')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'displayEval' => 'data[{index}].Timestampers.deletable',
                'confirm' => __("Êtes-vous sûr de vouloir supprimer ce service d'horodatage ?"),
                'params' => ['Timestampers.id', 'Timestampers.name'],
            ],
        ]
    );

echo $this->Html->tag(
    'section#timestampers-section.bg-white',
    $this->Html->tag(
        'div.separator',
        $this->ModalForm
            ->create('edit-timestamper')
            ->modal(__("Modifier un service d'horodatage"))
            ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "Timestampers")')
            ->output(
                'function',
                'actionEditTimestamper',
                '/Admins/editTimestamper'
            )
            ->generate()
        . $this->ModalForm
            ->create('add-timestamper')
            ->modal(__("Ajouter un service d'horodatage"))
            ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "Timestampers")')
            ->output(
                'button',
                $this->Fa->charte('Ajouter', __("Ajouter un service d'horodatage")),
                '/Admins/addTimestamper'
            )
            ->generate(['class' => 'btn btn-success', 'type' => 'button'])
        . $this->ModalForm
            ->create('add-cert', ['size' => 'large'])
            ->modal(__("Créer un certificat TSA"))
            ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "Timestampers")')
            ->output(
                'button',
                $this->Fa->i('fa-file-text', __("Créer un certificat TSA")),
                '/Admins/addTimestamperCert'
            )
            ->generate(['class' => 'btn btn-success', 'type' => 'button'])
    )
    . $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Liste des services d'horodatage"))
        . $this->Html->tag('div.r-actions.h4', $tableTimestampers->getConfigureLink())
    )
    . $tableTimestampers->generate()
);
?>
<script>
    function testTimestamper(form) {
        var id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
        if (!confirm(
            __(
                "Cette action peut avoir pour conséquence de consommer un jeton, "
                + "ce qui peut entraîner un coût supplémentaire. Voulez-vous continuer ?"
            )
        )
        ) {
            return;
        }
        $(form).find('.btn').disable();
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/admins/testTimestamper')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            data: id ? undefined : $(form).serialize(),
            success: function (content) {
                if (content.success) {
                    alert(__("OK - Le service d'horodatage a été vérifié avec succès"));
                } else {
                    var errorMsg = '';
                    if (content.exception) {
                        errorMsg += 'Exception: ' + content.exception;
                    }
                    if (content.errors) {
                        errorMsg += content.errors.join(', ');
                    }
                    alert(__("Une erreur a eu lieu: {0}", errorMsg));
                }
            },
            error: function (e) {
                console.error(e);
                alert(e.statusText);
            },
            complete: function () {
                $(form).find('.btn').enable();
            }
        });
    }
</script>
