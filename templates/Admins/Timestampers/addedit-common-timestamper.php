<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Cake\Core\Configure;
use Cake\Utility\Hash;
use Cake\Utility\Text;

$drivers = Configure::read('Timestamping.drivers', []);
$keys = array_keys($drivers);
echo $this->Form->control(
    'name',
    [
        'label' => __("Nom du service d'horodatage"),
        'required' => true,
    ]
);
if (!empty($sas)) {
    echo $this->Form->control(
        'org_entities._ids',
        [
            'label' => __("Service d'archives"),
            'options' => $sas,
            'data-placeholder' => __("-- Sélectionner les services d'archives autorisés --"),
            'multiple' => true,
            'help' => __(
                "Optionnel - Vous pourrez sélectionner cet horodateur "
                . "depuis l'interface des services d'archives"
            ),
            'default' => !$entity->get('id') && count($sas) === 1 ? current(array_keys($sas)) : null,
        ]
    );
}
echo $this->Form->control(
    'driver',
    [
        'label' => __("Driver"),
        'required' => true,
        'options' => array_combine($keys, Hash::extract($drivers, '{s}.name')),
        'empty' => __("-- Sélectionner un driver --"),
        'onchange' => 'selectedDriver(this)',
    ]
);
echo '<label class="hide">autocomplete obliterator'
    . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';
foreach ($drivers as $driver => $params) {
    $unsetValue = !$entity->isNew() && $driver !== $entity->get('driver');
    echo $this->Html->tag(
        'fieldset',
        null,
        [
            'id' => $idPrefix . '-fieldset-' . $driver,
            'class' => 'hide timestamper-hidden-fieldset',
        ]
    );
    foreach ($params['fields'] ?? [] as $field => $paramsField) {
        if (isset($paramsField['display']) && !$paramsField['display']) {
            continue;
        }
        $paramsField['id'] = $driver . '-' . mb_strtolower(Text::slug($field, '-'));
        if (isset($paramsField['read_config'])) {
            $paramsField['value'] = Configure::read($paramsField['read_config']);
            unset($paramsField['read_config']);
        }
        if ($unsetValue) {
            $paramsField['value'] = false;
        }
        echo $this->Form->control($field, $paramsField);
    }
    echo $this->Html->tag('/fieldset');
}

echo $this->Html->tag(
    'button.btn.btn-warning',
    $this->Fa->i('fa-calendar-check-o', __("Tester le service d'horodatage")),
    ['type' => 'button', 'onclick' => 'testTimestamper("#' . $idForm . '")']
);
?>
<script>
    function selectedDriver(element) {
        var value = $(element).val();
        $('.timestamper-hidden-fieldset').addClass('hide').find('input, select, textarea').disable();
        if (value) {
            $('#<?=$idPrefix?>-fieldset-' + value).removeClass('hide').find('input, select, textarea').enable();
        }
    }
</script>
