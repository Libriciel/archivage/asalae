<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create(
    $entity,
    [
        'id' => $idForm = 'add-timestamper-form',
        'autocomplete' => 'off',
        'idPrefix' => $idPrefix = 'add-timestamper',
    ]
);

require 'addedit-common-timestamper.php';
echo $this->Form->end();
?>
<script>
    selectedDriver($('#add-timestamper-driver'));
    AsalaeGlobal.select2($('#add-timestamper-org-entities-ids'));
</script>
