<?php

/**
 * @var Asalae\View\AppView $this
 * @var AsalaeCore\Form\TSACertForm $form
 */

use AsalaeCore\Utility\Config;

$pathToLocal = Config::getPathToLocal();

echo $this->Form->create($form, ['id' => 'add-timestamper-tsa']);

echo $this->Form->control(
    'countryName',
    [
        'label' => 'countryName',
        'default' => 'FR',
    ]
);
echo $this->Form->control(
    'stateOrProvinceName',
    [
        'label' => 'stateOrProvinceName',
        'default' => 'France',
    ]
);
echo $this->Form->control(
    'localityName',
    [
        'label' => 'localityName',
        'default' => 'Paris',
    ]
);
echo $this->Form->control(
    'organizationName',
    [
        'label' => 'organizationName',
        'default' => __("Ma collectivité"),
    ]
);
echo $this->Form->control(
    'organizationalUnitName',
    [
        'label' => 'organizationalUnitName',
        'default' => __("Mon service"),
    ]
);
echo $this->Form->control(
    'commonName',
    [
        'label' => 'commonName',
        'default' => __("Mon nom"),
    ]
);
echo $this->Form->control(
    'emailAddress',
    [
        'label' => 'emailAddress',
        'default' => __("Mon email"),
    ]
);
echo $this->Form->control(
    'extfile',
    [
        'label' => 'extfile',
        'default' => $form::DEFAULT_TIMESTAMPER_TSA_EXTFILE,
    ]
);

$dir = dirname($pathToLocal) . DS . 'timestamping';
echo $this->Form->control(
    'directory',
    [
        'label' => __("Dossier pour les certificats"),
        'default' => $dir,
    ]
);
echo $this->Form->control(
    'cnf',
    [
        'label' => 'cnf',
        'default' => file_get_contents(RESOURCES . 'openssl.cnf'),
    ]
);
echo $this->Form->control(
    'name',
    [
        'label' => __("Nom de l'horodateur"),
        'default' => "Openssl local",
    ]
);
echo $this->Html->tag(
    'div.btn-group',
    $this->Html->tag(
        'button.btn.btn-warning',
        $this->Fa->i('fa-clock-o', __("Tester l'horodatage")),
        ['onclick' => 'testTimestamper()', 'type' => 'button']
    )
);
echo $this->Form->end();
?>
<script>
    function testTimestamper() {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/admins/testTimestamperTSA')?>',
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            data: $('#add-timestamper-tsa').serialize(),
            success: function (content) {
                if (content.success) {
                    alert(__("OK - Le service d'horodatage a été vérifié avec succès"));
                } else {
                    var errorMsg = '';
                    if (content.exception) {
                        errorMsg += 'Exception: ' + content.exception;
                    }
                    if (content.errors) {
                        errorMsg += content.errors.join(', ');
                    }
                    alert(__("Une erreur a eu lieu: {0}", errorMsg));
                }
            },
            error: function (e) {
                console.error(e);
                alert(e.statusText);
            }
        });
    }
</script>
