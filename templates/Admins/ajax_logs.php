<?php

/**
 * @var Asalae\View\AppView $this
 */
$keys = array_keys($logs);
echo $this->Form->control(
    'selectlogfile',
    [
        'label' => __("Fichiers de log"),
        'options' => array_combine($keys, $keys),
        'default' => 'error.log',
    ]
);
foreach ($logs as $logname => $content) {
    $id = preg_replace('/\W+/', '_', basename($logname));
    $button = $this->Fa->button(
        'fa-play',
        $title = __("Mise à jour en temp réel"),
        [
            'title' => $title,
            'data-play' => $title,
            'data-pause' => __("Arrêter la mise à jour en temps réel"),
            'class' => 'btn btn-link tail',
            'onclick' => "tailF('" . $this->Url->build(
                '/no-session-renew/admins/tail/' . $logname
            ) . "', '" . $id . "', this)",
        ]
    );
    echo $this->Html->tag(
        'div',
        $this->Html->tag('label', $logname)
        . ' ' . $button
        . $this->Html->tag(
            'pre',
            h($content),
            [
                'id' => $id,
                'class' => 'form-control console log',
                'contenteditable' => true,
            ]
        )
        . '<hr>'
        . $this->Html->link(
            $this->Fa->charte('Télécharger', __("Télécharger {0}", $logname)),
            sprintf(
                '/admins/downloadLog/%s',
                $logname
            ),
            [
                'class' => 'btn btn-primary',
                'download' => $logname,
                'escape' => false,
            ]
        ),
        ['class' => 'log-area form-group', 'style' => 'display: none', 'data-logfile' => $logname]
    );
}
?>
<script>
    $('textarea').each(function () {
        var e = $(this).get(0);
        e.scrollTop = e.scrollHeight - e.getBoundingClientRect().height;
    });

    var tailXhr;

    function tailF(url, id, button) {
        var textarea = $('#' + id),
            btn = $(button),
            modal = btn.closest('.modal');
        if (tailXhr) {
            tailXhr.abort();
        }
        textarea.off('.activearea').one('focus.activearea', function () {
            tailXhr.abort();
            btn.removeClass('active')
                .attr('title', btn.attr('data-play'))
                .disable()
                .find('i.fa')
                .addClass('fa-play')
                .removeClass('fa-pause');
        }).one('blur.activearea', function (e) {
            setTimeout(function () {
                if (btn.is(':enabled')) {
                    return;
                }
                tailF(url, id, button);
                btn.addClass('active')
                    .attr('title', btn.attr('data-pause'))
                    .enable()
                    .find('i.fa')
                    .removeClass('fa-play')
                    .addClass('fa-pause');
            }, 200);// nécessaire si on click sur le bouton (100=insuffisants)
        });

        btn.blur()
            .attr('onclick', '')
            .off('click.abort')
            .one('click.abort', function (e) {
                $('button.tail:not(.active)').enable()
                    .find('i.fa').removeClass('fa-lock').addClass('fa-play');
                $(this).one('click.abort', function () {
                    $(this).data('abortAjax', false);
                    tailF(url, id, this);
                });
                $(this).data('abortAjax', true)
                    .removeClass('active')
                    .attr('title', btn.attr('data-play'))
                    .find('i.fa')
                    .addClass('fa-play')
                    .removeClass('fa-pause');
                modal.off('hide.bs.modal');
                tailXhr.abort();
            })
            .addClass('active')
            .attr('title', btn.attr('data-pause'))
            .find('i.fa')
            .removeClass('fa-play')
            .addClass('fa-pause');
        modal.one('hide.bs.modal', function () {
            tailXhr.abort();
        });
        if (typeof btn.data('abortAjax') === 'undefined') {
            btn.data('abortAjax', false);
        }
        $('button.tail:not(.active)').disable()
            .find('i.fa').addClass('fa-lock').removeClass('fa-play')
            .closest('.log-area').hide();
        tailXhr = $.ajax({
            url: url,
            xhr: function () {
                var xhr = new XMLHttpRequest();
                var loaded = false;
                xhr.addEventListener("readystatechange", function () {
                    if (this.readyState === 3 && !btn.data('abortAjax')) {// === LOADING
                        if (loaded) {
                            var actualValue = textarea.html();
                            var newValue = this.response.trim();
                            if (actualValue !== newValue) {
                                textarea.html((new Filter).toHtml(newValue));
                                textarea.scrollTop(textarea.get(0).scrollHeight);
                            }
                        }
                        loaded = true;
                    }
                }, false);
                return xhr;
            },
            success: function (content) {
                var actualValue = textarea.html();
                if (actualValue !== content) {
                    textarea.html((new Filter).toHtml(content.trim()));
                    textarea.scrollTop(textarea.get(0).scrollHeight);
                }
                if (!btn.data('abortAjax')) {
                    tailF(url, id);
                }
            }
        });
    }

    var selectlogfile = $('#selectlogfile').change(function () {
        if (tailXhr) {
            $('button.tail.active').trigger('click.abort');
        }
        $('.log-area').hide();
        var div = $('[data-logfile="' + $(this).val() + '"]').show();
        div.find('button.tail').click();
        var textarea = div.find('pre.console');
        textarea.scrollTop(textarea.get(0).scrollHeight);
    }).change();
    AsalaeGlobal.select2(selectlogfile);
</script>