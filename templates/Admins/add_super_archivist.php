<?php

/**
 * @var Asalae\View\AppView $this
 * @var Asalae\Model\Entity\User $user
 */

echo $this->Form->create($user, ['idPrefix' => 'add-user-se']);

if ($user->get('ldap_id')) {
    echo $this->Html->tag(
        'div.alert.alert-info',
        __(
            "Ce compte étant importé depuis un annuaire LDAP/AD,"
            . " il n'est pas possible d'en modifier les informations utilisateur."
        )
    );
}

echo $this->Form->control(
    'username',
    [
        'label' => __("Identifiant"),
        'required' => true,
    ] + (
    $user->get('ldap_id')
        ? ['disabled' => true]
        : []
    )
);

echo $this->Form->control(
    'name',
    [
        'label' => __("Nom d'utilisateur"),
        'placeholder' => __("optionnel"),
    ] + (
        $user->get('ldap_id')
            ? ['disabled' => true]
            : []
        )
)
    . $this->Form->control(
        'email',
        [
            'label' => __("Email"),
            'type' => 'email',
        ] + (
        $user->get('ldap_id')
            ? ['disabled' => true]
            : []
        )
    )
    . $this->Form->control(
        'archival_agencies._ids',
        [
            'label' => __("Services d'archives accessibles"),
            'multiple' => true,
            'data-placeholder' => __("-- Sélectionner un ou plusieurs service d'archives --"),
            'options' => $archivalAgencies,
            'required' => true,
        ]
    )
    . $this->Form->control(
        'is_validator',
        [
            'label' => [
                'text' => __("Est validateur pour ses entités ?"),
                'title' => __(
                    "L'utilisateur effectue des validations pour ses services d'archives de rattachement.
Si désactivé, il pourra toujours être validateur si l'utilisateur est
explicitement sélectionné comme validateur."
                ),
            ],
        ]
    )
    . $this->Form->control(
        'use_cert',
        [
            'label' => [
                'text' => __("Validation par signature électronique pour ses entités ?"),
                'title' => __("Les validations doivent être effectuées avec un certificat"),
            ],
        ]
    )
    . $this->Form->control(
        'high_contrast',
        [
            'label' => [
                'text' => $this->Fa->i('fa-blind') . $this->Fa->i('fa-wheelchair')
                    . __("Utiliser un contraste élevé ?"),
                'title' => __("Modifie le visuel pour faciliter la lecture aux mal-voyants"),
            ],
            'escape' => false,
        ]
    )
    . $this->Form->control(
        'active',
        [
            'label' => __("Actif"),
        ]
    );

echo $this->Form->end();
?>
<script>
    AsalaeGlobal.select2($('#add-user-se-archival-agencies-ids'));

    var isValidator = $('#add-user-se-is-validator');
    var useCert =  $('#add-user-se-use-cert');
    isValidator
        .on('change', () => useCert.enable(isValidator.prop('checked'))
            .prop('checked', false))
        .change();
</script>
