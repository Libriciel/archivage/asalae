<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create($form, ['idPrefix' => 'add-user-se']);

echo $this->Form->control(
    'user_id',
    [
        'label' => __("Utilisateur à promouvoir"),
        'options' => $users,
        'empty' => __("-- Sélectionner un utilisateur --"),
        'required' => true,
    ]
)
    . $this->Form->control(
        'archival_agencies',
        [
            'label' => __("Services d'archives accessibles"),
            'multiple' => true,
            'data-placeholder' => __("-- Sélectionner un ou plusieurs service d'archives --"),
            'options' => $archivalAgencies,
            'required' => true,
        ]
    );

echo $this->Form->end();
?>
<script>
    AsalaeGlobal.select2($('#add-user-se-user-id'));
    AsalaeGlobal.select2($('#add-user-se-archival-agencies'));
</script>
