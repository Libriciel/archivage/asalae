<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->control('name', ['label' => __("Nom de l'espace de conservation")])
    . $this->Form->control('description', ['label' => __("Description")]);

if (!empty($sas)) {
    echo $this->Form->control(
        'org_entity_id',
        [
            'label' => __("Service d'archives"),
            'options' => $sas,
            'empty' => $entity->get('org_entity_id') ? false : __("-- Sélectionner un service d'archives --"),
            'help' => __(
                "Optionnel - Vous pourrez sélectionner cet espace de "
                . "conservation depuis l'interface des services d'archives"
            ),
            'readonly' => $entity->get('org_entity_id') !== null,
        ]
    );
}
