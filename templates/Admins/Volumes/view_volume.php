<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;

echo $this->Html->tag('article');
echo $this->Html->tag('header.bottom-space');
echo $this->Html->tag('h3', h($volume->get('name')));
echo $this->Html->tag('/header');

echo $this->Html->tag('h4', __("Volume"));
echo $this->ViewTable->generate(
    $volume,
    [
        __("Identifiant en BDD") => 'id',
        __("Nom du volume") => 'name',
        __("Description") => 'description',
        __("Actif") => 'active',
        __("Alerte taux d'occupation") => 'alert_rate',
        __("Occupation actuelle") => 'disk_usage|default(0)|toReadableSize',
        __("Limite logicielle de taille du disque") => 'max_disk_usage|toReadableSize',
        __("Driver") => 'driver',
        __("Champs") => 'fields|raw|json_decode',
        __("Vitesse d'accès") => function (EntityInterface $entity) {
            $duration = $entity->get('read_duration');
            return $duration !== null ? round($duration * 1000, 2) . ' ms' : __("Non mesuré");
        },
        __("Date de création") => 'created',
        __("Date de modification") => 'modified',
    ],
    ['id' => 'view-volume-table']
);
echo $this->Html->tag('/article');
