<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create($entity, ['idPrefix' => 'edit-volume']);
$selectBtnId = 'edit-volume-select-btn';
$testBtnId = 'edit-volume-test-btn';
$driverId = 'edit-volume-driver';
require 'addedit-common-volume.php';
echo $this->Form->end();
