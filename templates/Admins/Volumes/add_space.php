<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create($entity, ['idPrefix' => 'add-space']);
require 'addedit-common-space.php';
echo $this->Form->control(
    'volumes._ids',
    [
        'label' => __("Volumes"),
        'options' => $volumes,
        'multiple' => true,
        'required' => true,
        'data-placeholder' => __("-- Sélectionner les volumes à utiliser --"),
        'default' => count($volumes) === 1 ? current(array_keys($volumes)) : null,
    ]
);
echo $this->Form->end();
?>
<script>
    AsalaeGlobal.chosen($('#add-space-volumes-ids'), __("Volumes utilisées"));
</script>
