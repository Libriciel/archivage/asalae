<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create($entity, ['idPrefix' => 'edit-space']);
require 'addedit-common-space.php';
echo $this->Form->end();
?>
<script>
    AsalaeGlobal.select2($('#edit-space-volumes-ids'));
</script>
