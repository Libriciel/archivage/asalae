<?php

/**
 * @var Asalae\View\AppView $this
 */

echo '<label class="hide" aria-hidden="true">autocomplete obliterator'
    . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';

$mult = $this->Input->mult('mult', 'go');

echo $this->Form->control(
    'name',
    [
        'label' => __("Nom du volume"),
    ]
)
    . $this->Form->control(
        'description',
        [
            'label' => __("Description"),
            'type' => 'textarea',
            'info' => __("optionnel"),
        ]
    )
    . $this->Form->control(
        'location',
        [
            'label' => __(
                "Site, localisation de conservation : utilisé "
                . "à la place du nom du volume dans les attestations si renseigné"
            ),
            'type' => 'textarea',
            'info' => __("optionnel"),
        ]
    )
    . $this->Form->control(
        'active',
        [
            'label' => __("Actif"),
            'type' => 'checkbox',
        ]
    )
    . $this->Form->control(
        'alert_rate',
        [
            'label' => __("Alerte taux d'occupation"),
            'type' => 'number',
            'min' => 1,
            'max' => 100,
            'prepend' => '%',
            'placeholder' => __("optionnel"),
            'info' => __("Affichage d'une alerte lorsque >= x% d'occupation disque."),
        ]
    )
    . $this->Form->control(
        'max_disk_usage_conv',
        [
            'label' => __("Limite logicielle de taille du disque"),
            'append' => $mult,
            'class' => 'with-select',
            'type' => 'number',
            'min' => 1,
            'required' => true,
        ]
    );


echo $this->Form->control('driver', ['type' => 'hidden', 'id' => false]);
echo $this->Form->control(
    'driver',
    [
        'label' => __("Driver du volume"),
        'options' => $drivers,
        'empty' => __("-- Sélectionner un driver --"),
        'required' => true,
        'disabled' => $entity->get('is_used'),
    ]
) . '<hr>';

$urlDelete = $this->Url->build('/');
$uploads = $this->Upload
    ->create('upload-minio-cert', ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'info' => [
                'label' => __("Fichier"),
                'target' => '',
                'thead' => [
                    'filename' => [
                        'label' => __("Nom de fichier"),
                        'callback' => 'TableHelper.wordBreak()',
                    ],
                    'hash' => [
                        'label' => __("Empreinte du fichier"),
                        'callback' => 'TableHelper.wordBreak("_", 32)',
                    ],
                    'size' => [
                        'label' => __("Taille"),
                        'callback' => 'TableHelper.readableBytes',
                    ],
                ],
            ],
            'message' => [
                'label' => __("Message"),
                'style' => 'width: 400px',
                'class' => 'message',
            ],
        ]
    )
    ->data($uploadData ?? [])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "$('.verify-minio-cert').val('{0}')",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-check-square'),
                'title' => $title = __("Sélectionner {0} comme certificat", '{1}'),
                'aria-label' => $title,
                'params' => ['path', 'name'],
            ],
            function ($table) {
                $deleteUrl = $this->Url->build('/upload/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction({$table->tableObject}, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name'],
                ];
            },
        ]
    );
$jsUpload = $uploads->jsObject;
$jsTableUpload = $this->Upload->getTableId('upload-minio-cert');

$i = 0;
foreach (array_keys($drivers) as $driver) {
    echo $this->Html->tag('div.hide.drivers-div', null, ['data-driver' => $driver, 'data-index' => $i]);

    foreach ($driversFields[$driver] as $field => $params) {
        if (isset($params['display']) && $params['display'] === false) {
            $params['type'] = 'hidden';
            unset($params['display']);
        }
        if (!empty($params['read_config'])) {
            continue;
        }

        // Ajout d'un champ upload pour le certificat minio
        if ($driver === 'MINIO' && $field === 'verify') {
            echo $this->Html->tag(
                'div.form-group.fake-input',
                $this->Html->tag('span.fake-label', __("Certificats"))
                . $uploads->generate(
                    [
                        'singleFile' => false,
                        'allowDuplicateUploads' => true,
                        'target' => $this->Url->build('/upload/minio-cert'),
                    ]
                )
            );
        }

        echo $this->Form->control("fields.$i.$field", $params);
    }

    echo $this->Html->tag('/div');
    $i++;
}
echo $this->Html->tag(
    "button#$testBtnId.btn.btn-warning.hide",
    $this->Fa->i('fa-calendar-check-o') . ' '
    . __("Tester le volume"),
    ['type' => 'button']
);
?>
<script>
    $('#<?=$driverId?>').change(function () {
        var value = $(this).val();
        var btn = $('button#<?=$testBtnId?>');
        if (!value) {
            $('.drivers-div').addClass('hide');
            btn.addClass('hide');
            return;
        }
        btn.removeClass('hide');
        var divs = btn.closest('form').find('.drivers-div');
        divs.find('input').disable();
        divs.find('input[required]').each(function () {
            $(this).attr('data-required', $(this).attr('required'))
                .removeAttr('required');
        });
        divs.addClass('hide');
        var divActive = $('[data-driver=' + value + ']');
        divActive.find('input').enable()
            .each(function () {
                $(this).prop('readonly', <?= $entity->get('is_used') ? 'true' : 'false' ?>)
                    .removeAttr('data-readonly')
            });
        divActive.removeClass('hide')
            .find('input[data-required]')
            .each(function () {
                $(this).attr('required', $(this).attr('data-required'))
                    .removeAttr('data-required');
            });
    }).trigger('change');
    $('button#<?=$testBtnId?>').click(function () {
        var form = $(this).closest('form');
        actionTestVolume('', $(this), form.serialize());
    });

    $('#upload-minio-cert').on('fileSuccess', function (event, file, data) {
        $('.verify-minio-cert').val(data.report.path);
    });
</script>
