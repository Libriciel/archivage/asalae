<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create($entity, ['idPrefix' => 'add-volume']);
$selectBtnId = 'add-volume-select-btn';
$testBtnId = 'add-volume-test-btn';
$driverId = 'add-volume-driver';
require 'addedit-common-volume.php';
echo $this->Form->end();
