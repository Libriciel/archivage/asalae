<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\I18n\DateTime as CakeDateTime;

echo $this->Form->create($form, ['idPrefix' => 'change-volumes']);

echo $this->Form->control(
    'add',
    [
        'label' => __("Volumes à ajouter"),
        'multiple' => true,
        'options' => $availablesVolumes,
        'empty' => true,
        'data-placeholder' => __("-- Sélectionner des volumes --"),
        'required' => false,
    ]
);

echo $this->Form->control(
    'remove',
    [
        'label' => __("Volumes à retirer"),
        'multiple' => true,
        'options' => $currentVolumes,
        'empty' => true,
        'data-placeholder' => __("-- Sélectionner des volumes --"),
    ]
);

echo '<hr>';

echo $this->Form->control(
    'cron',
    [
        'label' => __("Date et heure de la prochaine exécution"),
        'type' => 'text',
        'append' => $this->Date->datetimePicker('#change-volumes-cron'),
        'class' => 'datepicker',
        'default' => (new CakeDateTime())->i18nFormat(),
    ]
);

echo $this->Form->end();
?>
<script>
    var volumeAdd = $('#change-volumes-add');
    var volumeRemove = $('#change-volumes-remove');

    AsalaeGlobal.select2(volumeAdd, {allowClear: false});
    AsalaeGlobal.select2(volumeRemove, {allowClear: false});

    volumeAdd.on('change', function () {
        volumeRemove.disable(volumeAdd.val().length);
    });
    volumeRemove.on('change', function () {
        volumeAdd.disable(volumeRemove.val().length);
    });
</script>
