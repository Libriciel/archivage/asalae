<?php
/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $volume
 * @var array $files
 */
?>
<script>
    function evalClassVolumeClient(data) {
        if (data.directory) {
            return "warning";
        }
        if (data.is_integrity_ok === false) {
            return "danger";
        }
    }
</script>
<?php

echo $this->Html->tag('article');
echo $this->Html->tag('header.bottom-space');
echo $this->Html->tag('h3', h($volume->get('name')) . ' (id=' . $volume->id . ')');
echo $this->Html->tag('/header');

if (!empty($exception)) {
    echo $this->Html->tag('p.alert.alert-danger', $exception);
    return;
}

$path = '/';
$fields = $volume->get('fields_object');
$basePath = $fields->bucket ?? $fields->path;
$this->Breadcrumbs->add($basePath, '#', ['onclick' => "volumeClient('/')"]);
foreach (explode('/', $storagePath) as $spath) {
    $path .= "$spath/";
    $this->Breadcrumbs->add($spath, '#', ['onclick' => "volumeClient('$path')"]);
}
echo $this->Breadcrumbs->render();

echo $this->Form->create(
    null,
    [
        'id' => 'client-searchbar-form',
        'method' => 'get',
        'type' => 'get',
    ]
);
echo $this->Form->control(
    'search',
    [
        'id' => 'client-searchbar',
        'label' => __("Se rendre à l'emplacement"),
        'append' => $this->Fa->i('fa-search')
            . $this->Html->tag(
                'span.sr-only',
                __("Rechercher")
            ),
        'templates' => [
            'inputGroupAddons' => '<span class="input-group-addon btn btn-primary" '
                . 'onclick="$(\'#client-searchbar-form\').submit()" role="button" tabindex="0" title="'
                . __("Rechercher") . '">{{content}}</span>',
        ],
        'aria-autocomplete' => 'both',
        'aria-haspopup' => false,
        'autocapitalize' => 'off',
        'autocomplete' => 'off',
        'autocorrect' => 'off',
        'role' => 'combobox',
        'spellcheck' => false,
        'val' => $storagePath,
    ]
);
echo $this->Form->end();

$deleteUrl = $this->Url->build('/admins/deleteVolumeFile/' . $volume->id);
$jsTable = $this->Table->getJsTableObject('volume_client');
$table = $this->Table
    ->create('volume_client')
    ->fields(
        [
            'icon' => [
                'label' => 'Type',
                'callback' => "TableHelper.filenameIcon('filename', 'directory')",
            ],
            'filename' => [
                'label' => 'Filename',
                'callback' => "TableHelper.wordBreak('_', 80)",
            ],
            'count' => [
                'label' => __("Nombre de fichiers (bdd)"),
            ],
            'size' => [
                'label' => __("Taille des fichiers (bdd)"),
                'callback' => 'TableHelper.readableBytes',
            ],
            'hash' => [
                'label' => __("Hash (bdd)"),
                'callback' => 'TableHelper.wordBreak()',
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'fullpath',
            'classEval' => 'evalClassVolumeClient(data[{index}])',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "integrityCheck('{0}', this)",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-check-square-o'),
                'title' => $title = __("Vérifier l'intégrité de {0}", '{1}'),
                'aria-label' => $title,
                'displayEval' => '(!data[{index}].directory) && data[{index}].hash !== undefined',
                'params' => ['fullpath', 'filename'],
            ],
            [
                'onclick' => "volumeClient('{0}')",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-folder-open'),
                'title' => $title = __("Ouvrir {0}", '{1}'),
                'aria-label' => $title,
                'displayEval' => 'data[{index}].directory',
                'params' => ['fullpath', 'filename'],
            ],
            [
                'data-callback' => "TableGenericAction.deleteAction($jsTable, '$deleteUrl')('{0}', false)",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer un dossier/fichier"),
                'aria-label' => $title,
                'displayEval' => 'deleteVolumeFilesEnabled',
                'confirm' => __(
                    "Voulez-vous vraiment supprimer ce fichier/dossier ? Cette opération ne pourra pas être annulée."
                ),
                'params' => ['fullpath'],
            ],
        ]
    );

echo $table;

$prevDir = dirname($storagePath);
if ($prevDir) {
    echo $this->Form->button(
        $this->Fa->i('fa-arrow-left', __("Dossier précédent")),
        [
            'class' => 'cancel',
            'type' => 'button',
            'onclick' => "volumeClient('$prevDir')",
        ]
    );
}

echo $this->Html->tag('/article');
?>
<script>
    function integrityCheck(path, button) {
        var npath = AsalaeGlobal.safeUrlEncode(path.replace(/&amp;/g, '&')).replace(/~2F/g, '/');
        var btn = $(button);
        AsalaeLoading.ajax(
            {
                url: '<?=$this->Url->build('/admins/volume-check-hash')?>/<?=$volume->id?>/' + npath,
                headers: {
                    Accept: 'application/json'
                },
                success: function (content) {
                    var tr = btn.closest('tr');
                    if (content.match) {
                        tr.addClass('success').removeClass('danger');
                    } else {
                        tr.addClass('danger').removeClass('success');
                    }
                }
            }
        );
    }

    function volumeClient(path) {
        path = path.replace(/%2F/g, '%252F');
        var table = TableGenerator.instance[$('#volume_client').attr('data-table-uid')];
        $.ajax(
            {
                url: '<?=$this->Url->build('/admins/volume-client')?>/<?=$volume->id?>/' + path,
                success: function (content) {
                    var modalBody = $('#volume_client').closest('.modal-body');
                    modalBody
                        .find('*').off().remove().end()
                        .html(content);
                }
            }
        )
    }

    $('#client-searchbar-form').on('submit', function (event) {
        event.preventDefault();
        volumeClient($('#client-searchbar').val());
        return false;
    });
</script>
