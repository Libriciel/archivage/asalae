<?php

/**
 * @var Asalae\View\AppView      $this
 * @var Asalae\Model\Entity\User $user
 */
echo $this->MultiStepForm->template('MultiStepForm/add_org_entity_sa')->step(3) . '<hr>';
echo $this->Form->create($user, ['autocomplete' => 'new-password', 'idPrefix' => 'add-user']);
$labelPassword = __("Password");
$idJauge = 'add-user-jauge-password';
$displayRole = false;
$displayPassword = false;
require dirname(__DIR__, 2) . '/Users/addedit-common.php';
echo $this->Form->end();
