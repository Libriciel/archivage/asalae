<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->ModalForm
    ->create('set-main-service-archives')
    ->modal(__("Définition du service d'archives gestionnaire"))
    ->javascriptCallback('setMainArchivalAgencyCallback')
    ->output(
        'function',
        'actionSetMainArchivalAgency',
        '/Admins/setMainArchivalAgency'
    )
    ->generate();

echo $this->ModalForm->create('add-notice-sa')
    ->modal(__("Ajout d'une notice d'autorité au service d'archives"))
    ->javascriptCallback('afterAddNoticeSA')
    ->output('function', 'addNoticeSA', '/Admins/addEaccpf')
    ->generate();

echo $this->ModalForm->create('add-user-sa')
    ->modal(__("Ajout d'un utilisateur au service d'archives"))
    ->javascriptCallback('afterAddUserSA')
    ->output('function', 'addUserSA', '/Admins/addUser')
    ->generate();


echo $this->element('modal', ['idTable' => $tableIdServiceArchive, 'paginate' => false]);
$tableService = $this->Table
    ->create($tableIdServiceArchive, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'OrgEntities.name' => ['label' => __("Nom")],
            'OrgEntities.identifier' => ['label' => __("Identifiant")],
            'OrgEntities.is_main_archival_agency' => ['label' => __("Gestionnaire"), 'type' => 'boolean'],
            'OrgEntities.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
            ],
            'OrgEntities.modified' => [
                'label' => __("Date de modification"),
                'type' => 'datetime',
                'display' => false,
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'OrgEntities.id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionSetMainArchivalAgency({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-bookmark'),
                'title' => $title = __("Définir ''{0}'' comme service d'archives gestionnaire", '{1}'),
                'aria-label' => $title,
                'displayEval' => '!data[{index}].OrgEntities.is_main_archival_agency',
                'params' => ['OrgEntities.id', 'OrgEntities.name'],
            ],
            [
                'onclick' => "loadEditModalServiceArchive({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'params' => ['OrgEntities.id', 'OrgEntities.name'],
            ],
            function ($table) {
                $view = $this;
                $deleteUrl = $view->Url->build('/OrgEntities/delete');
                return [
                    'onclick' => "deleteEntity({$table->tableObject}, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'displayEval' => 'data[{index}].OrgEntities.deletable',
                    'params' => ['OrgEntities.id', 'OrgEntities.name'],
                ];
            },
        ]
    );

$jsTableServiceArchive = $tableService->tableObject;

$disabled = $availableDataspaces ? '' : ' disabled';

echo $this->Html->tag(
    'section',
    $this->Html->tag(
        'div',
        $this->ModalForm
            ->create('service-archive-add')
            ->modal(__("Ajout d'un service d'Archives"))
            ->javascriptCallback('afterAddSA(' . $jsTableServiceArchive . ', "OrgEntities")')
            ->output(
                'button',
                $this->Fa->charte('Ajouter', __("Ajouter un service d'Archives")),
                '/Admins/addServiceArchive'
            )
            ->generate(
                [
                    'class' => 'btn btn-success' . $disabled,
                    'type' => 'button',
                ] + ($availableDataspaces ? [] : ['disabled' => true])
            )
        . $this->ModalForm
            ->create('service-archive-edit', ['size' => 'modal-xxl'])
            ->modal(__("Modification d'un service d'Archives"))
            ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTableServiceArchive . ', "OrgEntities")')
            ->output('function', 'loadEditModalServiceArchive', '/Admins/editServiceArchive')
            ->generate(),
        ['class' => 'separator']
    )
    . ($availableDataspaces
        ? ''
        : $this->Html->tag(
            'p.alert.alert-warning',
            __("Ajout d'un nouveau service d'Archives impossible car aucun espace de stockage disponible")
        )
    )
    . $this->Html->tag('header')
    . $this->Html->tag('h2.h4', __("Liste des services d'Archives"))
    . $this->Html->tag('div.r-actions.h4', $tableService->getConfigureLink())
    . $this->Html->tag('/header')
    . $tableService->generate(),
    ['id' => 'service-archive-section', 'class' => "bg-white"]
);

echo $this->ModalForm->create('index-sa-add-notice-sa')
    ->modal(__("Ajout d'une notice d'autorité au service d'Archives"))
    ->javascriptCallback('afterAddNoticeSA')
    ->output('function', 'addNoticeSA', '/Admins/addEaccpf')
    ->generate();

echo $this->ModalForm->create('index-sa-add-user-sa')
    ->modal(__("Ajout d'un utilisateur au service d'Archives"))
    ->javascriptCallback('afterAddUserSA')
    ->output('function', 'addUserSA', '/Admins/addUser')
    ->generate();

?>

<script>
    function reloadThisModal() {
        $('#service-archive.modal:visible').find('button.refresh').click();
    }

    /**
     * Supprime une entité puis recharge la modale
     * @return {Function}
     */
    function deleteEntity(table, url) {
        $(table.table).one('deleted_data.table', reloadThisModal);
        return TableGenericAction.deleteAction(table, url);
    }

    /**
     * Callback d'après ajout d'un service d'archives
     * @param table
     * @param model
     * @return {Function}
     */
    function afterAddSA(table, model) {
        return function (content, textStatus, jqXHR) {
            TableGenericAction.afterAdd(table, model)(content, textStatus, jqXHR);
            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                addNoticeSA(content.id);
            }
        }
    }

    /**
     * Callback d'après ajout d'une notice sur le nouveau service d'archives
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterAddNoticeSA(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            addUserSA(content.org_entity_id);
        }
    }

    /**
     * Callback d'après ajout d'un utilisateur sur le nouveau service d'archives
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterAddUserSA(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            loadEditModalServiceArchive(content.org_entity_id);
            if (typeof content.code === 'string') {
                setTimeout(function () {
                    window.open('<?=$this->Url->build('/auth-urls/activate')?>/' + content.code, '_blank');
                }, 100);// laisse le temps à la db d'enregistrer l'url
            }
        }
    }

    function setMainArchivalAgencyCallback(content) {
        var table;
        table = <?=$jsTableServiceArchive?>;
        for (var i = 0; i < table.data.length; i++) {
            table.data[i].is_main_archival_agency = table.data[i].id === content.id;
            table.data[i].OrgEntities.is_main_archival_agency = table.data[i].id === content.id;
        }
        table.generateAll();
    }
</script>
