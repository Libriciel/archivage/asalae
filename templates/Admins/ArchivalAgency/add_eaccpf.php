<?php

/**
 * @var Asalae\View\AppView $this
 */
$tmpl = 'MultiStepForm/add_org_entity_sa';
require dirname(__DIR__, 2) . '/Eaccpfs/add.php';
?>
<script>
    /** global addUserSA */
    /**
     * Callback de l'action ajout (eaccpf)
     *
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterAddEaccpf(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            addUserSA(content[0].org_entity_id);
        }
    }
</script>
