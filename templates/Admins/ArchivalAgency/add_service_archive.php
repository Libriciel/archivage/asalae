<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->template('MultiStepForm/add_org_entity_sa')->step(1) . '<hr>';

echo $this->Form->create($entity, ['idPrefix' => 'add-service-archive'])
    . $this->Form->control('name', ['label' => __("Nom")])
    . $this->Form->control('identifier', ['label' => __("Identifiant")])
    . $this->Form->control(
        'timestampers._ids',
        [
            'label' => __("Services d'horodatage disponibles"),
            'required' => true,
            'options' => $timestampers,
            'multiple' => true,
            'data-placeholder' => __("-- Sélectionner les services d'horodatage disponibles --"),
            'default' => count($timestampers) === 1 ? current(array_keys($timestampers)) : null,
        ]
    );
if (!$mainExists) {
    echo $this->Form->control(
        'timestamper_id',
        [
            'label' => __("Service d'horodatage pour le journal des événements"),
            'required' => true,
            'options' => $timestampers,
            'empty' => __("-- Sélectionner un service d'horodatage --"),
            'default' => count($timestampers) === 1 ? current(array_keys($timestampers)) : null,
        ]
    );
}
echo $this->Form->control(
    'default_secure_data_space_id',
    [
        'label' => __("Espace de stockage par défaut"),
        'required' => true,
        'options' => $secure_data_spaces,
        'empty' => __("-- Sélectionner un espace de stockage par défaut --"),
        'default' => count($secure_data_spaces) === 1 ? current(array_keys($secure_data_spaces)) : null,
    ]
)
    . $this->Form->control(
        'ldaps._ids',
        [
            'label' => __("Ldaps disponibles"),
            'options' => $ldaps,
            'multiple' => true,
            'data-placeholder' => __("-- Sélectionner des Ldaps disponibles --"),
        ]
    )
    . $this->Form->control(
        'archiving_systems._ids',
        [
            'label' => __("Systèmes d'Archivage Electronique pour les transferts sortants"),
            'options' => $archivingSystems,
            'multiple' => true,
            'data-placeholder' => __("-- Sélectionner parmis les SAE disponibles --"),
        ]
    )
    . $this->Form->end();
?>
<script>
    AsalaeGlobal.chosen($('#add-service-archive-timestampers-ids'), __("Services d'horodatage disponibles"));
    AsalaeGlobal.chosen($('#add-service-archive-ldaps-ids'), __("Ldaps disponibles"));
    AsalaeGlobal.select2($('#add-service-archive-archiving-systems-ids'));
</script>
