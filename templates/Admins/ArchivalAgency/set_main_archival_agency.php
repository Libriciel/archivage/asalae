<?php

/**
 * @var Asalae\View\AppView $this
 */

echo $this->Form->create($entity, ['idPrefix' => 'set-timestamper-service-archive']);
echo $this->Form->control(
    'timestamper_id',
    [
        'label' => __("Service d'horodatage pour le journal des événements"),
        'required' => true,
        'options' => $timestampers,
        'empty' => __("-- Sélectionner un service d'horodatage --"),
    ]
);
echo $this->Form->end();
