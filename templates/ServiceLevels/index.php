<?php

/**
 * @var Asalae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Niveaux de service"));

echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-handshake-o', __("Niveaux de service")))
    . $this->Breadcrumbs->render()
);

$jsTable = $this->Table->getJsTableObject($tableId);
$buttons = [];
if ($this->Acl->check('/agreements/add1')) {
    $buttons[] = $this->ModalForm
        ->create('add-service-levels')
        ->modal(__("Ajout d'un niveau de service"))
        ->step('/service-levels/add1', 'addServiceLevelStep1')
        ->step('/service-levels/add2', 'addServiceLevelStep2')
        ->step('/service-levels/add3', 'addServiceLevelStep3')
        ->javascriptCallback('AsalaeModal.stepCallback')
        ->javascriptCallback('afterAddServiceLevel(' . $jsTable . ', "ServiceLevels")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un niveau de service")),
            '/ServiceLevels/add1'
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalForm
    ->create('add2-service-level-list')
    ->modal(__("Ajouter un niveau de service"))
    ->javascriptCallback('afterAddServiceLevel2(' . $jsTable . ', "ServiceLevels")')
    ->output(
        'function',
        'actionAddServiceLevel2',
        '/ServiceLevels/add2'
    )
    ->generate();

echo $this->ModalForm
    ->create('edit-service-level-list', ['size' => 'large'])
    ->modal(__("Modifier un niveau de service"))
    ->javascriptCallback('afterEditServiceLevel(' . $jsTable . ', "ServiceLevels")')
    ->output(
        'function',
        'actionEditServiceLevel',
        '/ServiceLevels/edit'
    )
    ->generate();

echo $this->ModalView
    ->create('view-service-level-list')
    ->modal(__("Visualiser un niveau de service"))
    ->output(
        'function',
        'actionViewServiceLevel',
        '/ServiceLevels/view'
    )
    ->generate();

echo $this->Filter->create('service-levels-filter')
    ->saves($savedFilters)
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'identifier',
        [
            'label' => __("Identifiant"),
            'wildcard',
        ]
    )
    ->filter(
        'active',
        [
            'label' => __("Actifs"),
            'options' => [
                '1' => __("Oui"),
                '0' => __("Non"),
            ],
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    )
    ->filter(
        'default_level',
        [
            'label' => __("Defaut"),
            'options' => [
                '1' => __("Oui"),
                '0' => __("Non"),
            ],
        ]
    )
    ->filter(
        'secure_data_space_id',
        [
            'label' => __("Espace de stockage sécurisé"),
            'options' => $secureDataSpaces,
        ]
    )
    ->filter(
        'modified',
        [
            'label' => __("Date de modification"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->generateSection();

require 'ajax_index.php';
?>
<!--suppress JSUnresolvedFunction -->
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    function afterEditServiceLevel(table, model) {
        return function (content, textStatus, jqXHR) {
            TableGenericAction.afterEdit(table, model)(content, textStatus, jqXHR);
            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true' && content.default_level) {
                for (var i = 0; i < table.data.length; i++) {
                    if (table.data[i][model].default_level && table.data[i][model].id !== content.id) {
                        table.data[i][model].default_level = false;
                        table.data[i][model].deletable = true;
                        TableGenerator.appendActions(table.data, table.actions);
                        table.generateAll();
                        break;
                    }
                }
            }
        }
    }

    function afterAddServiceLevel(table, model) {
        return function (content, textStatus, jqXHR) {
            var fn;
            if (jqXHR.getResponseHeader('X-Asalae-Step')) {
                fn = AsalaeModal.stepCallback;
            } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                fn = TableGenericAction.afterAdd(table, model);
            }
            fn(content, textStatus, jqXHR);
        }
    }
</script>
