<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->template('MultiStepForm/add_service_level')->step(2) . '<hr>';

echo $this->Form->create($entity, ['idPrefix' => 'add-service-level'])
    . $this->Form->control('name', ['type' => 'hidden'])
    . $this->Form->control('identifier', ['type' => 'hidden'])
    . $this->Form->control('description', ['type' => 'hidden'])
    . $this->Form->control('default_level', ['type' => 'hidden'])
    . $this->Form->control('secure_data_space_id', ['type' => 'hidden'])
    . $this->Form->control(
        'ts_msg_id',
        [
            'label' => __("Service d'horodatage pour les messages SEDA"),
            'options' => $timestampers,
            'empty' => __("-- Choisir un service d'horodatage --"),
        ]
    )
    . $this->Form->control(
        'ts_pjs_id',
        [
            'label' => __("Service d'horodatage pour les pièces jointes"),
            'options' => $timestampers,
            'empty' => __("-- Choisir un service d'horodatage --"),
        ]
    )
    . $this->Form->control(
        'ts_conv_id',
        [
            'label' => __("Service d'horodatage pour les conversions de fichier"),
            'options' => $timestampers,
            'empty' => __("-- Choisir un service d'horodatage --"),
        ]
    )
    . $this->Form->end();
