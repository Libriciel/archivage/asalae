<?php

/**
 * @var Asalae\View\AppView $this
 */
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'ServiceLevels.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'ServiceLevels.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'ServiceLevels.active' => [
                'label' => __("Actif"),
                'type' => 'boolean',
                'callback' => 'TableHelper.boolean',
                'filter' => [
                    'active[0]' => [
                        'id' => 'filter-active-0',
                        'label' => false,
                        'aria-label' => __("Actif"),
                        'options' => [
                            '1' => __("Oui"),
                            '0' => __("Non"),
                        ],
                    ],
                ],
            ],
            'ServiceLevels.default_level' => [
                'label' => __("Defaut"),
                'type' => 'boolean',
                'callback' => 'TableHelper.boolean',
                'filter' => [
                    'default_level[0]' => [
                        'id' => 'filter-default_level-0',
                        'label' => false,
                        'aria-label' => __("Defaut"),
                        'options' => [
                            '1' => __("Oui"),
                            '0' => __("Non"),
                        ],
                    ],
                ],
            ],
            'ServiceLevels.secure_data_space.name' => [
                'label' => __("Espace de stockage sécurisé"),
                'filter' => [
                    'secure_data_space_id[0]' => [
                        'id' => 'filter-secure_data_space_id-0',
                        'label' => false,
                        'aria-label' => __("Espace de stockage sécurisé"),
                        'options' => $secureDataSpaces,
                    ],
                ],
            ],
            'ServiceLevels.timestampers' => [
                'label' => __("Politique d'horodatage"),
                'target' => 'ServiceLevels',
                'thead' => [
                    'ts_msg.timestamper.name' => ['label' => __("Messages")],
                    'ts_pjs.timestamper.name' => ['label' => __("Pièces jointes")],
                    'ts_conv.timestamper.name' => ['label' => __("Politique de conversion")],
                ],
            ],
            'ServiceLevels.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'ServiceLevels.modified' => [
                'label' => __("Date de modification"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'modified',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'modified[0]' => [
                        'id' => 'filter-modified-0',
                        'label' => __("Date de modification"),
                        'prepend' => $this->Input->operator('dateoperator_modified[0]', '>='),
                        'append' => $this->Date->picker('#filter-modified-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'modified[1]' => [
                        'id' => 'filter-modified-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_modified[1]', '<='),
                        'append' => $this->Date->picker('#filter-modified-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'ServiceLevels.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewServiceLevel({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Visualiser {0}", '{1}'),
                'aria-label' => __("Visualiser {0}", '{1}'),
                'display' => $this->Acl->check('service-levels/view'),
                'params' => ['ServiceLevels.id', 'ServiceLevels.name'],
            ],
            [
                'onclick' => "actionEditServiceLevel({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'display' => $this->Acl->check('service-levels/edit'),
                'params' => ['ServiceLevels.id', 'ServiceLevels.name'],
            ],
            function ($table) {
                $view = $this;
                $deleteUrl = $view->Url->build('/ServiceLevels/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction({$table->tableObject}, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'display' => $view->Acl->check('service-levels/delete'),
                    'displayEval' => "data[{index}].ServiceLevels.deletable",
                    'params' => ['ServiceLevels.id', 'ServiceLevels.name'],
                ];
            },
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => "$tableId-section",
        'title' => __("Liste des niveaux de service"),
        'table' => $table,
    ]
);
