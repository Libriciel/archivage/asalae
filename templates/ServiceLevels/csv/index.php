<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $entity
 */

use Cake\Datasource\EntityInterface;

echo $this->Csv->resultTable(
    [
        'name' => __("Nom"),
        'identifier' => __("Identifiant"),
        'active' => __("Actif"),
        'default_level' => __("Defaut"),
        'secure_data_space.name' => __("Espace de stockage sécurisé"),
        'ts_msg.timestamper.name' => __("Horodatage messages"),
        'ts_pjs.timestamper.name' => __("Horodatage pièces jointes"),
        'ts_conv.timestamper.name' => __("Horodatage conversions"),
        'convert_preservationtrad' => __("Conversion des formats de conservation"),
        'convert_disseminationtrad' => __("Conversion des formats de diffusion"),
        'created' => __("Date de création"),
        'modified' => __("Date de modification"),
    ],
    $results
);
