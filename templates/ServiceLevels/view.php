<?php

/**
 * @var Asalae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

echo $this->Html->tag('article');
echo $this->Html->tag('header.bottom-space');
echo $this->Html->tag('h3', h($entity->get('name')));
echo $this->Html->tag('p', h($entity->get('description')));
echo $this->Html->tag('/header');

echo $this->Html->tag('h4', __("Niveau de service"));
echo $this->ViewTable->generate(
    $entity,
    [
        __("Nom") => 'name',
        __("Identifiant unique") => 'identifier',
        __("Commentaire") => 'description',
        __("Espace de stockage sécurisé") => 'secure_data_space.name',
        __("Niveau de service par défaut") => 'default_level',
        __("Actif") => 'active',
    ],
    ['id' => 'view-service-level-edit']
);

echo $this->Html->tag('h4', __("Politique d'horodatage"));
echo $this->ViewTable->generate(
    $entity,
    [
        __("Service d'horodatage pour les messages SEDA") => 'ts_msg.timestamper.name',
        __("Service d'horodatage pour les pièces jointes") => 'ts_pjs.timestamper.name',
        __("Service d'horodatage pour les conversions de fichier") => 'ts_conv.timestamper.name',
    ],
    ['id' => 'view-service-level-timestamp']
);

echo $this->Html->tag('h4', __("Politique de conversion"));
echo $this->ViewTable->generate(
    $entity,
    [
        __("Conversion des formats de conservation") => 'convert_preservationtrad|raw|flatten',
        __("Conversion des formats de diffusion") => 'convert_disseminationtrad|raw|flatten',
    ],
    ['id' => 'view-service-level-convert']
);

echo $this->Html->tag('/article');
