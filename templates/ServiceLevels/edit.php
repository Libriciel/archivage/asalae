<?php

/**
 * @var Asalae\View\AppView $this
 */

$tabs = $this->Tabs->create('tabs-service-level', ['class' => 'row no-padding']);

echo $this->Form->create($entity, ['idPrefix' => 'edit-service-level']);

/**
 * Edition
 */
$tabs->add(
    'tab-service-level-info',
    $this->Fa->charte('Modifier', __("Edition du niveau de service")),
    // -----------------------------------------------------------------------------
    $this->Form->control('name', ['label' => __("Nom"), 'required'])
    . $this->Form->control(
        'identifier',
        [
            'label' => __("Identifiant unique"),
            'required',
        ]
        + ($entity->get('editable_identifier')
            ? []
            : [
                'readonly' => true,
                'title' => __(
                    "Le niveau de service est utilisé par un ou des transferts non rejetés "
                    . "et ne peut donc être modifié"
                ),
            ]
        )
    )
    . $this->Form->control(
        'description',
        [
            'label' => __("Description"),
            'type' => 'textarea',
        ]
    )
    . $this->Form->control(
        'commitment',
        [
            'label' => __(
                "Engagement : ajouté à la liste des engagements"
                . " du service d'Archives dans les attestations si renseigné"
            ),
            'type' => 'textarea',
        ]
    )
    . $this->Form->control(
        'secure_data_space_id',
        [
            'label' => __("Espace de stockage sécurisé"),
            'options' => $secure_data_spaces,
            'empty' => __("-- Sélectionner un espace de stockage sécurisé --"),
        ]
    )
    . $this->Form->control('default_level', ['label' => __("Niveau de service par défaut")])
    . $this->Form->control(
        'active',
        [
            'label' => __("Actif (autorise son utilisation pour les transferts entrants)"),
        ]
    )
);

/**
 * Politique d'horodatage
 */
$tabs->add(
    'tab-service-level-timestampers',
    $this->Fa->i('fa-clock-o', __("Politique d'horodatage")),
    // -----------------------------------------------------------------------------
    $this->Form->control(
        'ts_msg_id',
        [
            'label' => __("Service d'horodatage pour les messages SEDA"),
            'options' => $timestampers,
            'empty' => __("-- Choisir un service d'horodatage --"),
        ]
    )
    . $this->Form->control(
        'ts_pjs_id',
        [
            'label' => __("Service d'horodatage pour les pièces jointes"),
            'options' => $timestampers,
            'empty' => __("-- Choisir un service d'horodatage --"),
        ]
    )
    . $this->Form->control(
        'ts_conv_id',
        [
            'label' => __("Service d'horodatage pour les conversions de fichier"),
            'options' => $timestampers,
            'empty' => __("-- Choisir un service d'horodatage --"),
        ]
    )
);

/**
 * Politique de conversion
 */
$tabs->add(
    'tab-service-level-convert',
    $this->Fa->i('fa-retweet', __("Politique de conversion")),
    // -----------------------------------------------------------------------------
    $this->Form->control(
        'convert_preservation',
        [
            'label' => __("Activer la conversion des formats de conservation"),
            'multiple' => true,
            'options' => $formats,
            'data-placeholder' => __("-- Sélectionner les conversions à effectuer --"),
        ]
    )
    . $this->Form->control(
        'convert_dissemination',
        [
            'label' => __("Activer la conversion des formats de diffusion"),
            'multiple' => true,
            'options' => $formats,
            'data-placeholder' => __("-- Sélectionner les conversions à effectuer --"),
        ]
    )
);

echo $tabs;

echo $this->Form->end();
?>
<script>
    AsalaeGlobal.chosen($('#edit-service-level-convert-preservation'));
    AsalaeGlobal.chosen($('#edit-service-level-convert-dissemination'));

    // empêcher d'avoir un niveau de service par défaut désactivé
    var activeCheckbox = $('#edit-service-level-active');
    var isActive;
    $('#edit-service-level-default-level').on('change', function () {
        if ($(this).prop('checked')) {
            activeCheckbox
                .on('click', function () {
                    return false;
                })
                .prop('checked', true)
                .addClass('disabled');
        } else {
            activeCheckbox
                .off('click')
                .prop('checked', isActive)
                .removeClass('disabled');
        }
    }).change();
    activeCheckbox.on('change', function () {
        isActive = $(this).prop('checked');
    }).change();
</script>
