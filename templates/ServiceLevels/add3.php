<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->template('MultiStepForm/add_service_level')->step(3) . '<hr>';

echo $this->Html->tag(
    'p.alert.alert-info',
    __(
        "Les conversions activées ci-dessous seront effectuées de façon "
        . "automatique après la création des archives."
    )
);

echo $this->Form->create($entity, ['idPrefix' => 'add-service-level'])
    . $this->Form->control('name', ['type' => 'hidden'])
    . $this->Form->control('identifier', ['type' => 'hidden'])
    . $this->Form->control('description', ['type' => 'hidden'])
    . $this->Form->control('default_level', ['type' => 'hidden'])
    . $this->Form->control('secure_data_space_id', ['type' => 'hidden'])
    . $this->Form->control('ts_msg_id', ['type' => 'hidden'])
    . $this->Form->control('ts_pjs_id', ['type' => 'hidden'])
    . $this->Form->control('ts_conv_id', ['type' => 'hidden'])
    . $this->Form->control(
        'convert_preservation',
        [
            'label' => __("Activer la conversion des formats de conservation"),
            'multiple' => true,
            'options' => $formats,
            'data-placeholder' => __("-- Sélectionner les conversions à effectuer --"),
        ]
    )
    . $this->Form->control(
        'convert_dissemination',
        [
            'label' => __("Activer la conversion des formats de diffusion"),
            'multiple' => true,
            'options' => $formats,
            'data-placeholder' => __("-- Sélectionner les conversions à effectuer --"),
        ]
    )
    . $this->Form->end();
?>
<script>
    AsalaeGlobal.chosen($('#add-service-level-convert-preservation'));
    AsalaeGlobal.chosen($('#add-service-level-convert-dissemination'));
</script>
