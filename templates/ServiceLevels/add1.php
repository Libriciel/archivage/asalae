<?php

/**
 * @var Asalae\View\AppView $this
 */
echo $this->MultiStepForm->template('MultiStepForm/add_service_level')->step(1) . '<hr>';

echo $this->Form->create($entity, ['idPrefix' => 'add-service-level'])
    . $this->Form->control('name', ['label' => __("Nom"), 'required'])
    . $this->Form->control('identifier', ['label' => __("Identifiant unique"), 'required'])
    . $this->Form->control(
        'description',
        [
            'label' => __("Description"),
            'type' => 'textarea',
        ]
    )
    . $this->Form->control(
        'commitment',
        [
            'label' => __(
                "Engagement : ajouté à la liste des engagements"
                . " du service d'Archives dans les attestations si renseigné"
            ),
            'type' => 'textarea',
        ]
    )
    . $this->Form->control('default_level', ['label' => __("Niveau de service par défaut")])
    . $this->Form->control(
        'secure_data_space_id',
        [
            'label' => __("Espace de stockage sécurisé"),
            'options' => $secure_data_spaces,
            'empty' => __("-- Sélectionner un espace de stockage sécurisé --"),
        ]
    )
    . $this->Form->end();
