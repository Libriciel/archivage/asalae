<div class="container">
    <h1><i aria-hidden="true" class="fa fa-calendar-o"></i> <?= __("Référentiels externes") ?></h1>
    <br/>
    <h3>Liste des référentiels externes</h3>
    <br/>
    <p>Les référentiels externes sont accessibles aux utilisateurs ayant le rôle de :</p>
    <ul>
        <li>Archiviste du Service d'Archives des archives</li>
    </ul>
    <br/>
    <p>Les actions possibles par profil sont : </p>
    <ul>
        <li>afficher la vue détaillée</li>
        <li>modifier</li>
        <li>supprimer</li>
    </ul>
    <br/>
    <p>Les filtres possibles sont : </p>
    <ul>
        <li>nom</li>
        <li>identifiant</li>
        <li>statut actif</li>
    </ul>
</div>

