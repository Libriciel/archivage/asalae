<?php

/**
 * @var Asalae\View\AppView $this
 * @var EntityInterface     $archivalAgency
 * @var EntityInterface     $originatingAgency
 * @var EntityInterface     $destructionRequest
 * @var EntityInterface[]   $secureDataSpaces
 * @var array               $archiveUnits
 * @var FrozenTime          $validDate
 * @var FrozenTime          $destroyedDate
 */

use Cake\Datasource\EntityInterface;
use Cake\I18n\FrozenTime;
use Cake\I18n\Number;

$identifier = $destructionRequest->get('identifier');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr-FR" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= h($identifier) ?></title>
    <!--suppress CssNonIntegerLengthInPixels -->
    <style type="text/css">
        <?php require dirname(__DIR__, 2) . '/vendor/libriciel/seda2pdf/templates/style.css'; ?>

        table.list-files {
            font-family: monospace;
        }

        table.list-files tr th {
            min-width: 0;
        }

        table.list-files tr td {
            width: auto;
        }

        p.h3 {
            font-size: 18.72px;
            margin-bottom: 20px;
        }

        h4.h3 {
            margin: 5px 0;
        }

        p {
            margin: 5px 0;
        }

        .info {
            background-color: #e1f1f7;
            padding: 10px;
        }

        ul {
            margin: 0;
        }

        li {
            list-style-position: inside;
            margin: 2px 0;
        }
    </style>
</head>
<body>
<?php

echo $this->Html->tag(
    'h1.h1',
    $definitive
        ? __("Attestation d’élimination")
        : __("Attestation d’élimination intermédiaire")
);
if (!$definitive) {
    echo $this->Html->tag(
        'p.h3',
        __(
            "Une attestation définitive sera produite après destruction "
            . "des sauvegardes {0} jour(s) après la date d'opération",
            $destructionDelay
        )
    );
}
echo $this->Html2pdf->table(
    [
        __("Date de l'édition de l'attestation") => new FrozenTime(),
        __("Identité du service d'Archives")
        => $archivalAgency->get('identifier') . ' ' . $archivalAgency->get('name'),
        __("Identité du service producteur")
        => $originatingAgency->get('identifier') . ' ' . $originatingAgency->get('name'),
    ]
);

echo '<hr>';

// Bloc opération
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Opération"));
echo $this->Html2pdf->table(
    [
        __("Nature de l'opération") => 'ÉLIMINATION',
        __("Date d'opération") => $destroyedDate,
    ]
);
echo $this->Html->tag('/div'); // div.section

// Bloc ordre ou accord
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Demande d'élimination"));
echo $this->Html2pdf->table(
    [
        __("Identifiant de la demande") => $identifier,
        __("Demande validée le") => $validDate,
    ]
);
echo $this->Html->tag('/div'); // div.section

// Bloc notification
if (!empty($notification)) {
    echo $this->Html->tag('div.section');
    echo $this->Html->tag('h3.h2', __("Notification d'élimination"));
    echo $this->Html2pdf->table(
        [
            __("Identifiant de la notification") => $notification->get('identifier'),
            __("Commentaire de la notification") => $notification->get('comment'),
            __("Date de la notification") => $notification->get('created'),
        ]
    );
    echo $this->Html->tag('/div'); // div.section
}

// Bloc Engagement du SA
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Le Service d'Archives s'engage"));
// Le Service d'Archives s'engage
echo $this->Html->tag('ul.info');
echo $this->Html->tag(
    'li',
    __("en l'exactitude des informations portées dans la présente attestation ;")
);
echo $this->Html->tag(
    'li',
    __("à avoir appliqué un processus d'élimination conforme à la norme ;")
);
echo $this->Html->tag(
    'li',
    __(
        "à avoir éliminé les archives selon le processus décrit dans le" .
        " Mémento Asalae : destruction des fichiers stockés, suppression des" .
        " méta données des archives consultables ;"
    )
);
$volumeList = $this->Html->tag('ul');
$allVolumes = [];
foreach ($secureDataSpaces as $secureDataSpace) {
    $volumes = $secureDataSpace->get('volumes');
    /** @var EntityInterface $volume */
    foreach ($volumes as $volume) {
        $allVolumes[] = $volume->get('location') ?: $volume->get('name');
    }
}
sort($allVolumes);
foreach ($allVolumes as $text) {
    $volumeList .= $this->Html->tag('li', $text, ['escape' => true]);
}
$volumeList .= $this->Html->tag('/ul');
echo $this->Html->tag(
    'li',
    __(
        "à avoir dûment éliminé les archives sur "
        . "l’ensemble des sites de conservation qui sont : {0}",
        $volumeList
    ),
    ['escape' => false]
);
echo $this->Html->tag('/ul');
echo $this->Html->tag('/div'); // div.section

// Conseil pour la conservation de la présente attestation
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Conseil pour la conservation de la présente attestation"));
echo $this->Html->tag('ul.info');
echo $this->Html->tag(
    'li',
    __(
        "Il est conseillé au service producteur de conserver les "
        . "attestations d’élimination, sans limite de durée."
    )
);
echo $this->Html->tag(
    'li',
    __(
        "Il est conseillé au service d'Archives de conserver ces "
        . "attestations a minima jusqu’à prescription des effets contractuels "
        . "entre le service d'Archives et le service Producteur"
    )
);
echo $this->Html->tag('/ul');
echo $this->Html->tag('/div');

// Durée de validité
echo $this->Html->tag('div.section');
echo $this->Html->tag('h3.h2', __("Durée de validité"));
echo $this->Html->tag('ul.info');
echo $this->Html->tag(
    'li',
    __("L’attestation est valable sans limite de temps")
);
echo $this->Html->tag('/ul');
echo $this->Html->tag('/div');

echo $this->Html->tag('/div'); // div.section

// Bloc Fichiers
echo $this->Html->tag('h2', __("Liste des objets numériques"));
echo $this->Html2pdf->table(
    [
        __("Nombre d'objets numériques") => $destructionRequest->get('original_count'),
        __("Poids total des objets numériques")
        => Number::toReadableSize($destructionRequest->get('original_size')),
    ]
);

foreach ($archiveUnits as $archiveUnit) {
    echo $this->Html->tag('div.section');
    echo $this->Html->tag('h3', __("Unité d'archives : {0}", $archiveUnit['identifier']));
    echo $this->Html2pdf->table(
        [
            __("Nom de l'unité d'archives") => $archiveUnit['name'],
            __("Identité du service versant") => $archiveUnit['transferring_agency'],
            __("Algorithme empreintes") => $archiveUnit['hash_algo'],
            __("Date de versement") => $archiveUnit['date'],
        ]
    );

    echo $this->Html->tag('table.list-files');
    echo $this->Html->tag('thead');
    echo $this->Html->tag('th', __("Identifiant archive"));
    echo $this->Html->tag('th', __("Nom fichier"));
    echo $this->Html->tag('th', __("Taille"));
    echo $this->Html->tag('th', __("Empreinte"));
    echo $this->Html->tag('/thead');
    echo $this->Html->tag('tbody');
    foreach ($archiveUnit['files'] as $file) {
        $hash = implode(
            '<wbr>',
            str_split(
                h($file['hash']),
                20
            )
        );
        $filename = str_replace(
            '/',
            '/<wbr>',
            implode(
                '<wbr>',
                str_split(
                    h($file['filename']),
                    20
                )
            )
        );
        $size = is_numeric($file['size']) ? Number::toReadableSize($file['size']) : $file['size'];
        $size = str_replace(' ', '&nbsp;', $size);
        echo $this->Html->tag('tr');
        echo $this->Html->tag('td', $file['archival_agency_identifier']);
        echo $this->Html->tag('td', $filename);
        echo $this->Html->tag('td', $size);
        echo $this->Html->tag('td', $hash);
        echo $this->Html->tag('/tr');
    }
    echo $this->Html->tag('/tbody');
    echo $this->Html->tag('/table');
    echo $this->Html->tag('/div'); // div.section
}
?>
</body>
</html>
