<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>

<ArchiveDestructionNotification xmlns="fr:gouv:culture:archivesdefrance:seda:v2.1">
    <?= $comment ? "    <Comment>$comment</Comment>" . PHP_EOL : '' ?>
    <Date><?= $date ?></Date>
    <MessageIdentifier><?= $identifier ?></MessageIdentifier>
    <CodeListVersions></CodeListVersions>
    <AuthorizationRequestReplyIdentifier><?= $requestIdentifier ?></AuthorizationRequestReplyIdentifier>
    <?= '    <UnitIdentifier>' . implode(
        "</UnitIdentifier>\n    <UnitIdentifier>",
        $archiveUnits
    ) . '</UnitIdentifier>' . PHP_EOL ?>
    <ArchivalAgency>
        <Identifier><?= $archivalAgency ?></Identifier>
    </ArchivalAgency>
    <OriginatingAgency>
        <Identifier><?= $originatingAgency ?></Identifier>
    </OriginatingAgency>
</ArchiveDestructionNotification>