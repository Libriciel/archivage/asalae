<?php

/**
 * @var Asalae\View\AppView $this
 */

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'DestructionNotifications.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'DestructionNotifications.comment' => [
                'label' => __("Commentaire"),
            ],
            'DestructionNotifications.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'DestructionNotifications.destruction_notification_xpaths' => [
                'label' => __("Unités d'archives"),
                'callback' => 'TableHelper.ul("archive_unit_name")',
                'escape' => false,
                'filter' => [
                    'archive_unit_identifier[0]' => [
                        'id' => 'filter-archive_unit_identifier-0',
                        'label' => __("Identifiant"),
                    ],
                    'archive_unit_name[0]' => [
                        'id' => 'filter-archive_unit_name-0',
                        'label' => __("Nom"),
                    ],
                ],
            ],
            'DestructionNotifications.destruction_request.original_count' => [
                'label' => __("Nombre de fichiers éliminés"),
                'order' => 'original_count',
                'filter' => [
                    'original_count[0][value]' => [
                        'id' => 'filter-original_count-0',
                        'label' => __("Nombre de fichiers éliminés"),
                        'prepend' => $this->Input->operator(
                            'original_count[0][operator]',
                            '>=',
                            ['id' => 'filter-original_count-0-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'original_count[1][value]' => [
                        'id' => 'filter-original_count-1',
                        'label' => false,
                        'aria-label' => __("Nombre 2"),
                        'prepend' => $this->Input->operator(
                            'original_count[1][operator]',
                            '<=',
                            ['id' => 'filter-original_count-1-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
            'DestructionNotifications.destruction_request.original_size' => [
                'label' => __("Taille des fichiers éliminés"),
                'callback' => 'TableHelper.readableBytes',
                'order' => 'original_size',
                'filter' => [
                    'original_size[0][value]' => [
                        'id' => 'filter-original_size-0',
                        'label' => __("Taille des fichiers éliminés"),
                        'prepend' => $this->Input->operator(
                            'original_size[0][operator]',
                            '>=',
                            ['id' => 'filter-original_size-0-operator']
                        ),
                        'append' => $this->Input->mult(
                            'original_size[0][mult]',
                            'mo',
                            ['id' => 'filter-original_size-0-mult']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'original_size[1][value]' => [
                        'id' => 'filter-original_size-1',
                        'label' => false,
                        'aria-label' => __("Taille 2"),
                        'prepend' => $this->Input->operator(
                            'original_size[1][operator]',
                            '<=',
                            ['id' => 'filter-original_size-1-operator']
                        ),
                        'append' => $this->Input->mult(
                            'original_size[1][mult]',
                            'mo',
                            ['id' => 'filter-original_size-1-mult']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'DestructionNotifications.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'href' => $this->Url->build('/destruction-notifications/download') . '/{0}',
                'download' => '{1}_destruction_notification.xml',
                'target' => '_blank',
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Télécharger'),
                'title' => $title = __("Télécharger {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/DestructionNotifications/download'),
                'params' => ['DestructionNotifications.id', 'DestructionNotifications.identifier'],
            ],
            [
                'href' => $this->Url->build('/destruction-notifications/destruction-certificate') . '/{0}',
                'download' => '{1}_destruction_certificate.pdf',
                'target' => '_blank',
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fas fa-award'),
                'title' => $title = __("Attestation d'élimination de {0}", '{1}'),
                'data-action' => __("Attestation d'élimination"),
                'aria-label' => $title,
                'display' => $this->Acl->check('/DestructionNotifications/destruction-certificate'),
                'displayEval' => 'data[{index}].destruction_request.certified !== null',
                'params' => ['DestructionNotifications.id', 'DestructionNotifications.destruction_request.identifier'],
            ],
        ]
    );

$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des notifications d'élimination"),
        'table' => $table,
    ]
);
