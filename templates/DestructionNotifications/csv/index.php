<?php

/**
 * @var Asalae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$map = function (EntityInterface $entity): EntityInterface {
    $archiveUnits = [];
    $destructionRequest = $entity->get('destruction_request');
    foreach (Hash::get($destructionRequest, 'archive_units', []) as $archiveUnit) {
        $archiveUnits[] = Hash::get($archiveUnit, 'name');
    }
    $destructionRequest->set(
        'archive_units',
        $archiveUnits ? '- ' . implode("\n- ", $archiveUnits) : ''
    );
    return $entity;
};

echo $this->Csv->resultTable(
    [
        'identifier' => __("Identifiant"),
        'comment' => __("Commentaire"),
        'created' => __("Date de création"),
        'destruction_request.archive_units' => __("Unités d'archives"),
        'destruction_request.original_count' => __("Nombre de fichiers éliminés"),
        'destruction_request.original_size_readable' => __("Taille des fichiers éliminés (octets)"),
        'destruction_request.original_size' => __("Taille des fichiers éliminés"),
    ],
    $results,
    $map
);
