<?php

/**
 * @var Asalae\View\AppView $this
 */

foreach ($icons as $css => $ics) {
    echo $this->Html->tag('h2.h3', $css);
    foreach ($ics as $class => $str) {
        echo $this->Html->tag('table.table.table-striped.table-hover.fixed-left-th');
        echo $this->Html->tag('tbody');

        echo $this->Html->tag('tr');
        echo $this->Html->tag('th', 'fa ' . $class);
        echo $this->Html->tag('td', $this->Html->tag('span.fa.fa-2x.' . $class, ''));
        echo $this->Html->tag('/tr');

        echo $this->Html->tag('tr');
        echo $this->Html->tag('th', 'far ' . $class);
        echo $this->Html->tag('td', $this->Html->tag('span.far.fa-2x.' . $class, ''));
        echo $this->Html->tag('/tr');

        echo $this->Html->tag('tr');
        echo $this->Html->tag('th', 'fas ' . $class);
        echo $this->Html->tag('td', $this->Html->tag('span.fas.fa-2x.' . $class, ''));
        echo $this->Html->tag('/tr');

        echo $this->Html->tag('/tbody');
        echo $this->Html->tag('/table');
    }
}
