<?php

namespace Asalae\TestSuite\Fixture;

use AsalaeCore\Utility\DatabaseUtility;
use Cake\TestSuite\Fixture\Extension\PHPUnitExtension as CakePHPUnitExtension;
use PHPUnit\Runner\Extension\Facade;
use PHPUnit\Runner\Extension\ParameterCollection;
use PHPUnit\TextUI\Configuration\Configuration;
use ReflectionException;

class PHPUnitExtension extends CakePHPUnitExtension
{
    /**
     * Initializes before any tests are run.
     *
     * @param Configuration       $configuration
     * @param Facade              $facade
     * @param ParameterCollection $parameters
     * @return void
     * @throws ReflectionException
     */
    public function bootstrap(Configuration $configuration, Facade $facade, ParameterCollection $parameters): void
    {
        parent::bootstrap($configuration, $facade, $parameters);

        // note: $pdo remplacé durant les migrations, il faut définir regexp après
        if (DatabaseUtility::driverName() === 'sqlite') {
            $pdo = DatabaseUtility::getPdo();
            $pdo->sqliteCreateFunction(
                'regexp',
                function ($pattern, $data) {
                    return $pattern && $data ? preg_match(
                        "~$pattern~isuS",
                        $data
                    ) : null;
                }
            );
        }
    }
}
