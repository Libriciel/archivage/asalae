<?php

/**
 * Asalae\TestSuite\VolumeSample
 */

namespace Asalae\TestSuite;

use Asalae\Exception\GenericException;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Driver\Volume\VolumeInterface;
use AsalaeCore\TestSuite\VolumeSample as CoreVolumeSample;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Entité de la table conversion_policys
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class VolumeSample extends CoreVolumeSample
{
    /**
     * @var VolumeFilesystem
     */
    public static $volume3;
    /**
     * @var VolumeFilesystem
     */
    public static $volume4;

    /**
     * Initialise les dossiers et les volumes pour les tests
     * @throws VolumeException
     */
    public static function init()
    {
        parent::init();
        if (is_dir(TMP_VOL3)) {
            Filesystem::remove(TMP_VOL3);
        }
        Filesystem::mkdir(TMP_VOL3);
        self::$volume3 = new VolumeFilesystem(['path' => TMP_VOL3]);
        Configure::write('App.paths.data', TMP_TESTDIR);
    }

    /**
     * Rempli les dossiers des volumes
     * @throws Exception
     */
    public static function fillVolumes()
    {
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }
        if (is_dir(TMP_VOL3)) {
            Filesystem::remove(TMP_VOL3);
        }
        exec(
            sprintf(
                'cp -r %s %s',
                escapeshellarg(static::getSampleDir() . 'asalae_data/volume01'),
                escapeshellarg(TMP_VOL1)
            )
        );
        exec(
            sprintf(
                'cp -r %s %s',
                escapeshellarg(static::getSampleDir() . 'asalae_data/volume02'),
                escapeshellarg(TMP_VOL2)
            )
        );
    }

    /**
     * Rempli les dossiers des transfers
     * @throws Exception
     */
    public static function fillTransfersData()
    {

        if (is_dir(TMP_TESTDIR . '/transfers')) {
            Filesystem::remove(TMP_TESTDIR . '/transfers');
        }
        exec(
            sprintf(
                'cp -r %s %s',
                escapeshellarg(
                    static::getSampleDir() . 'asalae_data/transfers'
                ),
                TMP_TESTDIR . '/transfers'
            )
        );
    }

    /**
     * Détruit les données des volumes
     * @throws Exception
     */
    public static function destroy()
    {
        parent::destroy();
        if (is_dir(TMP_VOL3)) {
            Filesystem::remove(TMP_VOL3);
        }
    }

    /**
     * Insert les fichiers d'une archive
     * @param int $archive_id
     * @throws VolumeException
     */
    public static function insertArchiveFiles(int $archive_id)
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $StoredFiles = $loc->get('StoredFiles');

        $archive = $Archives->get($archive_id);
        $manager = new VolumeManager($archive->get('secure_data_space_id'));
        $volumes = $manager->getVolumes();

        $archiveFiles = $StoredFiles->find()
            ->innerJoinWith('ArchiveFiles')
            ->where(['archive_id' => $archive_id]);
        /** @var EntityInterface $archiveFile */
        foreach ($archiveFiles as $archiveFile) {
            self::insertStoredFile($archiveFile, $volumes);
        }
        $archiveBinaries = $StoredFiles->find()
            ->innerJoinWith('ArchiveBinaries')
            ->innerJoinWith('ArchiveBinaries.ArchiveUnits')
            ->where(['archive_id' => $archive_id]);
        /** @var EntityInterface $archiveBinary */
        foreach ($archiveBinaries as $archiveBinary) {
            self::insertStoredFile($archiveBinary, $volumes);
        }
    }

    /**
     * Insert un stored_file dans les volumes
     * @param EntityInterface   $storedFile
     * @param VolumeInterface[] $volumes
     * @throws VolumeException
     */
    private static function insertStoredFile(
        EntityInterface $storedFile,
        array $volumes
    ) {
        $name = basename($storedFile->get('name'));
        if (!is_file(static::getSampleDir() . $name)) {
            throw new GenericException("sample file '$name' was not found");
        }
        /** @var VolumeFilesystem $volume */
        foreach ($volumes as $volume) {
            $volume->fileUpload(
                static::getSampleDir() . $name,
                $storedFile->get('name')
            );
        }
    }
}
