<?php

/**
 * Asalae\Exception\DumpShellException
 */

namespace Asalae\Exception;

use Exception;

/**
 * Exception levée lors d'un problème de prérequit pour l'import du DumpShell
 *
 * @category Shell
 *
 * @copyright (c) 2020 Libriciel
 * @license   http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 */
class DumpShellException extends Exception
{
}
