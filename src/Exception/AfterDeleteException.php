<?php

/**
 * Asalae\Exception\AfterDeleteException
 */

namespace Asalae\Exception;

use Exception;

/**
 * Formulaire de modification de la configuration
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, 2018 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AfterDeleteException extends Exception
{
}
