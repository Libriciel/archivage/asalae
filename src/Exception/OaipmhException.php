<?php

/**
 * Asalae\Exception\OaipmhException
 */

namespace Asalae\Exception;

use Exception;

/**
 * @copyright (c) 2017, 2018 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OaipmhException extends Exception
{
    public const array BAD_VERB = [
        'code' => 'badVerb',
        'value' => 'Value of the verb argument is not a legal OAI-PMH verb.',
    ];
    public const array TOKEN_INVALID = [
        'code' => 'badResumptionToken',
        'value' => 'The value of the resumptionToken argument is invalid or expired.',
    ];
    public const array VERB_NOT_SUPPORT_TOKEN = [
        'code' => 'badArgument',
        'value' => 'This verb does not support resumption token.',
    ];
    public const array INCLUDE_ILLEGAL_ARGUMENTS = [
        'code' => 'badArgument',
        'value' => 'The request includes illegal arguments.',
    ];
    public const array MISSING_ARGUMENT = [
        'code' => 'badArgument',
        'value' => 'The request is missing required arguments.',
    ];
    public const array MISSING_IDENTIFIER = [
        'code' => 'badArgument',
        'value' => 'The request is missing the "identifier" argument.',
    ];
    public const array MISSING_METADATAPREFIX = [
        'code' => 'badArgument',
        'value' => 'The request is missing the "metadataPrefix" argument.',
    ];
    public const array ID_DOES_NOT_EXIST = [
        'code' => 'idDoesNotExist',
        'value' => 'The value of the identifier argument is unknown or illegal in this repository.',
    ];
    public const array CANNOT_DISSEMINATE_FORMAT = [
        'code' => 'cannotDisseminateFormat',
        'value' => 'The metadata format identified by the value given for the metadataPrefix
         argument is not supported by the item or by the repository.',
    ];
    public const array ILLEGAL_FROM = [
        'code' => 'badArgument',
        'value' => 'The the value of the "from" argument have an illegal syntax.',
    ];
    public const array ILLEGAL_UNTIL = [
        'code' => 'badArgument',
        'value' => 'The the value of the "until" argument have an illegal syntax.',
    ];
    public const array ILLEGAL_SET = [
        'code' => 'badArgument',
        'value' => 'The the value of the "set" argument have an illegal syntax.',
    ];
    public const array NO_MATCH = [
        'code' => 'noRecordsMatch',
        'value' => 'The combination of the values of the from, until, set and metadataPrefix
         arguments results in an empty list.',
    ];

    /**
     * @var string
     */
    protected $_code;

    /**
     * @var string
     */
    protected $_value;

    /**
     * AfterDeleteException constructor.
     * @param array $error
     */
    public function __construct(array $error)
    {
        $this->_code = $error['code'];
        $this->_value = $error['value'];
        parent::__construct();
    }

    /**
     * @return string
     */
    public function getCodeAttribute(): string
    {
        return $this->_code;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->_value;
    }
}
