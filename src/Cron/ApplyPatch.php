<?php

/**
 * Asalae\Cron\ApplyPatch
 */

namespace Asalae\Cron;

use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Factory\Utility;
use Cake\Console\ConsoleOutput;
use Cake\Datasource\EntityInterface;
use Exception;

/**
 * Cron pour lancer un patch
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2024, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ApplyPatch implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;

    /**
     * @var array
     */
    public $params;

    /**
     * Constructeur de la classe du cron
     * @param array              $params paramètres additionnels
     *                                   du cron
     * @param ConsoleOutput|null $out
     * @param ConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->params = $params;
        $this->initializeOutput($out, $err);
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [
            'patch' => [
                'label' => __("Commande de patch (bin/cake)"),
            ],
        ];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        $Exec = Utility::get('Exec');
        $Exec->setDefaultStdout(LOGS . $this->params['patch'] . '.log');
        $result = $Exec->command(CAKE_SHELL, $this->params['patch']);
        $this->out($result->stdout);
        if ($result->stderr) {
            $this->err($result->stderr);
        }
        return $result->code === 0 ? 'success' : 'error';
    }
}
