<?php

/**
 * Asalae\Cron\RestitutionRequestArchiveDestruction
 */

namespace Asalae\Cron;

use Asalae\Model\Entity\StoredFile;
use Asalae\Model\Table\ArchiveBinariesArchiveUnitsTable;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveDescriptionsTable;
use Asalae\Model\Table\ArchiveIndicatorsTable;
use Asalae\Model\Table\ArchiveKeywordsTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsDeliveryRequestsTable;
use Asalae\Model\Table\ArchiveUnitsDestructionRequestsTable;
use Asalae\Model\Table\ArchiveUnitsOutgoingTransferRequestsTable;
use Asalae\Model\Table\ArchiveUnitsRestitutionRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\OutgoingTransfersTable;
use Asalae\Model\Table\PublicDescriptionArchiveFilesTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\ColoredCronTrait;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Datasource\EntityInterface;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInterval;
use DateInvalidTimeZoneException;
use DateTime;
use DOMException;
use Exception;

/**
 * Crée des jobs pour résoudre les anomalies
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RestitutionRequestArchiveDestruction implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;
    use ColoredCronTrait;

    /**
     * @var ArchiveBinariesTable
     */
    private $ArchiveBinaries;
    /**
     * @var ArchiveBinariesArchiveUnitsTable
     */
    private $ArchiveBinariesArchiveUnits;
    /**
     * @var ArchiveDescriptionsTable
     */
    private $ArchiveDescriptions;
    /**
     * @var ArchiveIndicatorsTable
     */
    private $ArchiveIndicators;
    /**
     * @var ArchiveKeywordsTable
     */
    private $ArchiveKeywords;
    /**
     * @var ArchiveUnitsTable
     */
    private $ArchiveUnits;
    /**
     * @var ArchiveUnitsDeliveryRequestsTable
     */
    private $ArchiveUnitsDeliveryRequests;
    /**
     * @var ArchiveUnitsDestructionRequestsTable
     */
    private $ArchiveUnitsDestructionRequests;
    /**
     * @var ArchiveUnitsOutgoingTransferRequestsTable
     */
    private $ArchiveUnitsOutgoingTransferRequests;
    /**
     * @var ArchiveUnitsRestitutionRequestsTable
     */
    private $ArchiveUnitsRestitutionRequests;
    /**
     * @var ArchivesTable
     */
    private $Archives;
    /**
     * @var EventLogsTable
     */
    private $EventLogs;
    /**
     * @var OrgEntitiesTable
     */
    private $OrgEntities;
    /**
     * @var OutgoingTransfersTable
     */
    private $OutgoingTransfers;
    /**
     * @var PublicDescriptionArchiveFilesTable
     */
    private $PublicDescriptionArchiveFiles;
    /**
     * @var RestitutionRequestsTable
     */
    private $RestitutionRequests;
    /**
     * @var EntityInterface
     */
    private $cron;
    /**
     * @var VolumeManager[]
     */
    private $dataSpaces = [];

    /**
     * Constructeur de la classe du cron
     * @param array                  $params paramètres
     *                                       additionnels
     *                                       du cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     * @throws Exception
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
        $loc = TableRegistry::getTableLocator();
        $this->ArchiveBinaries = $loc->get('ArchiveBinaries');
        $this->ArchiveBinariesArchiveUnits = $loc->get(
            'ArchiveBinariesArchiveUnits'
        );
        $this->ArchiveDescriptions = $loc->get('ArchiveDescriptions');
        $this->ArchiveIndicators = $loc->get('ArchiveIndicators');
        $this->ArchiveKeywords = $loc->get('ArchiveKeywords');
        $this->ArchiveUnits = $loc->get('ArchiveUnits');
        $this->ArchiveUnitsDeliveryRequests = $loc->get(
            'ArchiveUnitsDeliveryRequests'
        );
        $this->ArchiveUnitsDestructionRequests = $loc->get(
            'ArchiveUnitsDestructionRequests'
        );
        $this->ArchiveUnitsOutgoingTransferRequests = $loc->get(
            'ArchiveUnitsOutgoingTransferRequests'
        );
        $this->ArchiveUnitsRestitutionRequests = $loc->get(
            'ArchiveUnitsRestitutionRequests'
        );
        $this->Archives = $loc->get('Archives');
        $this->EventLogs = $loc->get('EventLogs');
        $this->OrgEntities = $loc->get('OrgEntities');
        $this->OutgoingTransfers = $loc->get('OutgoingTransfers');
        $this->PublicDescriptionArchiveFiles = $loc->get(
            'PublicDescriptionArchiveFiles'
        );
        $this->RestitutionRequests = $loc->get('RestitutionRequests');
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        $this->cron = $cron;
        $this->updateRestitutionStates();

        $restitutionRequests = $this->RestitutionRequests->find()
            ->where(
                [
                    'RestitutionRequests.state IN' => [
                        RestitutionRequestsTable::S_ARCHIVE_DESTRUCTION_SCHEDULED,
                        RestitutionRequestsTable::S_ARCHIVE_FILES_DESTROYING,
                        RestitutionRequestsTable::S_ARCHIVE_DESCRIPTION_DELETING,
                    ],
                ]
            );
        $conn = $this->RestitutionRequests->getConnection();

        /** @var EntityInterface $restitutionRequest */
        foreach ($restitutionRequests as $restitutionRequest) {
            $conn->begin();
            try {
                $this->saveIndicators($restitutionRequest);
                $this->destructRestitutionFiles($restitutionRequest);
                $this->deleteArchiveUnits($restitutionRequest);

                $this->RestitutionRequests->transition(
                    $restitutionRequest,
                    RestitutionRequestsTable::T_DELETE_DESCRIPTION
                );
                // génère l'attestation d'élimination des archives restitués
                $this->out(__("Création de l'attestation d'élimination..."));
                $restitutionRequest->get('destruction_certification');
                $this->RestitutionRequests->saveOrFail($restitutionRequest);
                $conn->commit();
            } catch (Exception $e) {
                $this->err(__("Echec lors de l'élimination de la restitution"));
                $this->err($e->getMessage());
                Log::error((string)$e);
                $conn->rollback();
                return 'failed';
            }
        }

        return 'success';
    }

    /**
     * Met à jour le status de la demande de Restitution par rapport à la config
     * du service d'archives (destuction après x jours)
     * @throws Exception
     */
    private function updateRestitutionStates()
    {
        $sas = $this->OrgEntities->find()
            ->select(
                [
                    'OrgEntities.id',
                ]
            )
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES,
                ]
            )
            ->contain(
                [
                    'Configurations' => fn(Query $q) => $q->where(
                        ['Configurations.name' => ConfigurationsTable::CONF_RR_DESTR_DELAY]
                    ),
                ]
            );
        /** @var EntityInterface $sa */
        foreach ($sas as $sa) {
            $period = new DateTime();
            $period->sub(
                new DateInterval(
                    sprintf('P%dD', Hash::get($sa, 'configurations.0.setting') ?: 15)
                )
            );

            $count = $this->RestitutionRequests->transitionAll(
                RestitutionRequestsTable::T_SCHEDULE_ARCHIVE_DESTRUCTION,
                [
                    'state' => RestitutionRequestsTable::S_RESTITUTION_ACQUITTED,
                    'last_state_update <=' => $period,
                    'archival_agency_id' => $sa->id,
                ]
            );
            if ($count) {
                $this->out(
                    __n(
                        "Changement d'état pour {0} archive du service d'archives n°{1}",
                        "Changement d'état pour {0} archives du service d'archives n°{1}",
                        $count,
                        $count,
                        $sa->id
                    )
                );
            }
        }
    }

    /**
     * Ajoute les indicateurs
     * @param EntityInterface $restitutionRequest
     */
    private function saveIndicators(EntityInterface $restitutionRequest)
    {
        $archives = $this->Archives->find()
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.RestitutionRequests')
            ->where(['RestitutionRequests.id' => $restitutionRequest->id]);
        foreach ($archives as $archive) {
            $entity = $this->ArchiveIndicators->newEntity(
                [
                    'archive_date' => new DateTime(),
                    'archival_agency_id' => $archive->get('archival_agency_id'),
                    'transferring_agency_id' => $archive->get('transferring_agency_id'),
                    'originating_agency_id' => $archive->get('originating_agency_id'),
                    'archive_count' => -1,
                    'units_count' => 0 - $archive->get('units_count'),
                    'original_count' => 0 - $archive->get('original_count'),
                    'original_size' => 0 - $archive->get('original_size'),
                    'timestamp_count' => 0 - $archive->get('timestamp_count'),
                    'timestamp_size' => 0 - $archive->get('timestamp_size'),
                    'preservation_count' => 0 - $archive->get('preservation_count'),
                    'preservation_size' => 0 - $archive->get('preservation_size'),
                    'dissemination_count' => 0 - $archive->get('dissemination_count'),
                    'dissemination_size' => 0 - $archive->get('dissemination_size'),
                    'agreement_id' => $archive->get('agreement_id'),
                    'profile_id' => $archive->get('profile_id'),
                ]
            );
            $this->ArchiveIndicators->saveOrFail($entity);
        }
    }

    /**
     * Elimination des fichiers des archives liés à une restitution
     * @param EntityInterface $restitutionRequest
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    private function destructRestitutionFiles(
        EntityInterface $restitutionRequest
    ) {
        $this->out(
            __(
                "Elimination des fichiers liés à la demande de restitution n°{0}",
                $restitutionRequest->id
            )
        );

        $this->RestitutionRequests->transition(
            $restitutionRequest,
            RestitutionRequestsTable::T_DESTROY_ARCHIVE_FILES
        );
        $this->RestitutionRequests->saveOrFail($restitutionRequest);

        $archives = $this->Archives->find()
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.RestitutionRequests')
            ->where(['RestitutionRequests.id' => $restitutionRequest->id])
            ->contain(['PublicDescriptionArchiveFiles' => ['StoredFiles']]);

        $conn = $this->ArchiveUnits->getConnection();

        /** @var EntityInterface $archive */
        foreach ($archives as $archive) {
            $manager = $this->getVolumeManager($archive);
            $binaries = $this->ArchiveBinaries->find()
                ->innerJoinWith('ArchiveUnits')
                ->where(['ArchiveUnits.archive_id' => $archive->id])
                ->contain(['StoredFiles']);
            /** @var EntityInterface $binary */
            foreach ($binaries as $binary) {
                $conn->begin();
                $this->ArchiveBinaries->updateAll(
                    ['stored_file_id' => null],
                    ['id' => $binary->id]
                );
                /** @var StoredFile $storedFile */
                $storedFile = $binary->get('stored_file');
                if (
                    $storedFile && $manager->fileExists(
                        $storedFile->get('name')
                    )
                ) {
                    $manager->fileDelete($storedFile->get('name'));
                }
                $conn->commit();
            }

            /** @var EntityInterface $desc */
            $desc = $archive->get('public_description_archive_file');
            if ($desc) {
                $this->PublicDescriptionArchiveFiles->deleteOrFail($desc);
                $manager->fileDelete(Hash::get($desc, 'stored_file.name'));
            }

            $this->Archives->patchEntity(
                $archive,
                [
                    'original_size' => 0,
                    'original_count' => 0,
                    'timestamp_size' => 0,
                    'timestamp_count' => 0,
                    'preservation_size' => 0,
                    'preservation_count' => 0,
                    'dissemination_size' => 0,
                    'dissemination_count' => 0,
                    'units_count' => 0,
                    'next_pubdesc' => null,
                ]
            );
            $this->Archives->transition($archive, ArchivesTable::T_RESTITUTE);
            $this->Archives->saveOrFail($archive);
        }
    }

    /**
     * Donne le VolumeManager lié à une archive
     * @param EntityInterface $archive
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(EntityInterface $archive): VolumeManager
    {
        if (!isset($this->dataSpaces[$archive->get('secure_data_space_id')])) {
            $this->dataSpaces[$archive->get('secure_data_space_id')]
                = new VolumeManager($archive->get('secure_data_space_id'));
        }
        return $this->dataSpaces[$archive->get('secure_data_space_id')];
    }

    /**
     * Supprime les unités d'archives liés à une demande de restitution avec
     * toutes les jointures vers cette unité d'archives
     * @param EntityInterface $restitutionRequest
     * @throws DOMException
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    private function deleteArchiveUnits(EntityInterface $restitutionRequest)
    {
        $this->out(__("Suppression des unités d'archives en BDD"));

        $this->RestitutionRequests->transition(
            $restitutionRequest,
            RestitutionRequestsTable::T_DELETE_ARCHIVE_DESCRIPTION
        );
        $this->RestitutionRequests->saveOrFail($restitutionRequest);

        $rootArchiveUnits = $this->ArchiveUnits->find()
            ->innerJoinWith('ArchiveUnitsRestitutionRequests')
            ->where(
                ['ArchiveUnitsRestitutionRequests.restitution_request_id' => $restitutionRequest->id]
            )
            ->contain(
                [
                    'Archives' => [
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                    ],
                ]
            );
        $conn = $this->ArchiveUnits->getConnection();
        /** @var EntityInterface $rootArchiveUnit */
        foreach ($rootArchiveUnits as $rootArchiveUnit) {
            $conn->begin();
            $this->ArchiveUnits->cascadeDelete($rootArchiveUnit);
            $this->logEvent(
                $rootArchiveUnit->get('archive'),
                $restitutionRequest
            );

            $conn->commit();
        }
    }

    /**
     * Mise à jour du journal des evenements et du cycle de vie de l'archive
     * @param EntityInterface $archive
     * @param EntityInterface $restitutionRequest
     * @throws VolumeException
     * @throws DOMException
     */
    private function logEvent(
        EntityInterface $archive,
        EntityInterface $restitutionRequest
    ) {
        $entry = $this->EventLogs->newEntry(
            'restitution_destroy',
            'success',
            __(
                "Destruction de l'archive {0} suite à la demande de restitution {1}",
                $archive->get('archival_agency_identifier'),
                $restitutionRequest->get('identifier')
            ),
            $this->cron,
            $archive
        );
        $this->EventLogs->saveOrFail($entry);
        $storedFile = Hash::get(
            $archive,
            'lifecycle_xml_archive_file.stored_file',
            []
        );
        // obligatoirement asynchrone car dans une transaction
        $this->Archives->updateLifecycleXml($storedFile, $entry, true);
    }
}
