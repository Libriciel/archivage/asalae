<?php

/**
 * Asalae\Cron\MailNotifications
 */

namespace Asalae\Cron;

use Asalae\Model\Table\AuthUrlsTable;
use Asalae\Model\Table\BeanstalkJobsTable;
use Asalae\Model\Table\MailsTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\ValidationChainsTable;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Console\ConsoleOutput;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Factory\Utility;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Date as CakeDate;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInterval;
use DateTime;
use Exception;

/**
 * Envoi des notifications par email
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MailNotifications implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;

    /**
     * Liste des Users.notify_*_frequency du jour
     * @var array|int[]
     */
    private array $intervals;
    /**
     * @var int mails de validation envoyés
     */
    private int $sentValidationMails = 0;
    /**
     * @var int mails de jobs en erreur envoyés
     */
    private int $sentJobMails = 0;
    /**
     * @var int mails pour les accords de versements sans transferts
     */
    private int $sentAgreementMails = 0;
    /**
     * @var int mails pour les rapports sur les transferts
     */
    private int $sentTransferReportMails = 0;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params paramètres additionnels du
     *                                       cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     * @throws Exception
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
        $this->out = $out ?: new ConsoleOutput('php://stdout');
        $this->err = $err ?: new ConsoleOutput('php://stderr');
        $this->out->setOutputAs(ConsoleOutput::COLOR);
        $this->err->setOutputAs(ConsoleOutput::COLOR);
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        $this->intervals = $this->listAvailableIntervals(time());
        $this->sendValidations();
        $this->sendJobs();
        $this->sendAgreements();
        $this->sendTransferReport();
        return 'success';
    }

    /**
     * Donne la liste de Users.notify_*_frequency pour un timestamp donné
     * @param int $timestamp
     * @return int[]
     * @throws Exception
     */
    private function listAvailableIntervals(int $timestamp): array
    {
        $intervals = [1];
        if (date('w', $timestamp) === '1') { // lundi
            $intervals[] = 7;
        }
        if (date('d', $timestamp) === '01') { // 1er jour du mois
            $intervals[] = 30;
        }
        if (date('m', $timestamp) === '01') { // 1er jour de l'année
            $intervals[] = 365;
        }
        $now = new DateTime('@' . $timestamp);
        $beginOfTheYear = new DateTime(date('Y-01-01', $timestamp));
        $diff = $beginOfTheYear->diff($now);
        if ($diff->days) {
            // Ajoute les divisions ex: 30 est divisible par 2, 3, 5, 6, 10 et 15
            for ($i = 2; $i < $diff->days; $i++) {
                if ($i !== 7 && $i !== 30 && $diff->days % $i === 0) {
                    $intervals[] = $i;
                }
            }
        }

        return $intervals;
    }

    /**
     * Envoi les mails de validation
     * @return void
     * @throws Exception
     */
    private function sendValidations()
    {
        $Users = TableRegistry::getTableLocator()->get('Users');
        $ValidationActors = TableRegistry::getTableLocator()->get('ValidationActors');
        $ValidationProcesses = TableRegistry::getTableLocator()->get('ValidationProcesses');
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $subquery = $ValidationActors->find()
            ->select(['Users.id'])
            ->innerJoinWith('Users')
            ->innerJoinWith('ValidationStages.ValidationChains')
            ->where(['ValidationChains.app_meta LIKE' => '%"active":true%']);
        $users = $Users->find()
            ->where(
                [
                    'Users.active' => true,
                    'Users.notify_validation_frequency IN' => $this->intervals,
                    'Users.id IN' => $subquery,
                ]
            )
            ->contain(
                [
                    'OrgEntities' => ['ArchivalAgencies' => ['Configurations']],
                    'ValidationActors' => ['ValidationStages' => ['ValidationChains']],
                ]
            );
        foreach ($users as $user) {
            $types = $ValidationProcesses->find()
                ->select(['type' => 'ValidationChains.app_type'])
                ->innerJoinWith('CurrentStages.ValidationActors.Users')
                ->innerJoinWith('CurrentStages.ValidationChains')
                ->where(
                    [
                        'Users.id' => $user->id,
                        'ValidationProcesses.processed IS' => false,
                    ]
                )
                ->distinct(['ValidationChains.app_type'])
                ->disableHydration()
                ->all()
                ->map(fn($row) => $row['type'])
                ->toArray();
            $authUrl = empty($types)
                ? null
                : $AuthUrls->saveOrFail(
                    $AuthUrls->newEntity(
                        [
                            'url' => '/users/unsubscribe/' . $user->id . '/validation',
                            'expire' => $this->getExpireDate($user->get('notify_validation_frequency')),
                        ]
                    )
                );
            foreach ($types as $type) {
                $this->sendValidationMail($type, $user, $authUrl);
                $this->sentValidationMails++;
            }
        }
        $this->out(__("Notifications de validation envoyés: {0}", $this->sentValidationMails));
    }

    /**
     * Donne le timestamp de la date d'expiration selon la fréquence
     * @param string $frequency
     * @return string timestamp
     */
    private function getExpireDate(string $frequency): string
    {
        switch ($frequency) {
            case '1':
                $duration = strtotime('+1 day');
                break;
            case '7':
                $duration = strtotime('+1 week');
                break;
            case '30':
                $duration = strtotime('+1 month');
                break;
            case '365':
                $duration = strtotime('+1 year');
                break;
            default:
                $duration = strtotime("+$frequency days");
                break;
        }
        return date('Y-m-d H:i:s', $duration);
    }

    /**
     * Envoi un mail de validation à l'utilisateur
     * @param string          $type    de validation
     * @param EntityInterface $user
     * @param EntityInterface $authUrl url pour se désabonner
     * @return bool
     * @throws Exception
     */
    private function sendValidationMail(
        string $type,
        EntityInterface $user,
        EntityInterface $authUrl
    ) {
        $prefix = $this->getConfigurationValue($user, 'mail-title-prefix');
        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $vars = [
            'htmlSignature' => $this->getConfigurationValue($user, 'mail-html-signature'),
            'textSignature' => $this->getConfigurationValue($user, 'mail-text-signature'),
            'code' => $authUrl,
        ];
        switch ($type) {
            case ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM:
                $vars['text'] = __("transferts conformes");
                $vars['url'] = '/validation-processes/my-transfers/validate';
                break;
            case ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM:
                $vars['text'] = __("transferts non conformes");
                $vars['url'] = '/validation-processes/my-transfers/invalidate';
                break;
            case ValidationChainsTable::ARCHIVE_DELIVERY_REQUEST:
                $vars['text'] = __("demandes de communication");
                $vars['url'] = '/validation-processes/delivery-requests';
                break;
            case ValidationChainsTable::ARCHIVE_DESTRUCTION_REQUEST:
                $vars['text'] = __("demandes d'élimination");
                $vars['url'] = '/validation-processes/destruction-requests';
                break;
            case ValidationChainsTable::ARCHIVE_OUTGOING_TRANSFER_REQUEST:
                $vars['text'] = __("demandes de transferts sortants");
                $vars['url'] = '/validation-processes/outgoing-transfer-requests';
                break;
            case ValidationChainsTable::ARCHIVE_RESTITUTION_REQUEST:
                $vars['text'] = __("demandes de restitution");
                $vars['url'] = '/validation-processes/restitution-requests';
                break;
        }
        $email->setViewVars($vars);
        $email->viewBuilder()
            ->setTemplate('notify_validation')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);
        $subject = $prefix . ' ' . __("validation de {0}", $vars['text']);
        $email->setEmailFormat('both')
            ->setSubject($subject)
            ->setTo($user->get('email'));

        /** @var MailsTable $Mails */
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        return $Mails->asyncMail($email, $user->id);
    }

    /**
     * Donne la config d'un service d'archive à partir d'un utilisateur
     * @param EntityInterface $user
     * @param string          $configName
     * @return mixed|null
     */
    private function getConfigurationValue(EntityInterface $user, string $configName)
    {
        $configurations = Hash::get($user, 'org_entity.archival_agency.configurations') ?: [];
        /** @var EntityInterface $config */
        foreach ($configurations as $config) {
            if ($config->get('name') === $configName) {
                return $config->get('setting');
            }
        }
        return null;
    }

    /**
     * Envoi les mails de jobs en erreur
     * @return void
     * @throws Exception
     */
    private function sendJobs()
    {
        $Users = TableRegistry::getTableLocator()->get('Users');
        $BeanstalkJobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $subquery = $BeanstalkJobs->find()
            ->select(['Users.id'])
            ->innerJoinWith('Users')
            ->where(['BeanstalkJobs.job_state' => BeanstalkJobsTable::S_FAILED]);
        $users = $Users->find()
            ->where(
                [
                    'Users.active' => true,
                    'Users.notify_job_frequency IN' => $this->intervals,
                    'Users.id IN' => $subquery,
                ]
            )
            ->contain(
                ['OrgEntities' => ['ArchivalAgencies' => ['Configurations']]]
            );
        foreach ($users as $user) {
            $jobErrors = $BeanstalkJobs->find()
                ->innerJoinWith('Users')
                ->where(
                    [
                        'Users.id' => $user->id,
                        'BeanstalkJobs.job_state' => BeanstalkJobsTable::S_FAILED,
                    ]
                )
                ->distinct(
                    [
                        'BeanstalkJobs.tube',
                        'BeanstalkJobs.object_model',
                        'BeanstalkJobs.object_foreign_key',
                    ]
                )
                ->contain(['Archives', 'Transfers'])
                ->disableHydration()
                ->all()
                ->map(
                    function ($row) {
                        if (isset($row['transfer'])) {
                            $object = sprintf(
                                'transfer id=%d : %s',
                                $row['transfer']['id'],
                                $row['transfer']['transfer_identifier']
                            );
                        } elseif (isset($row['archive'])) {
                            $object = sprintf(
                                'archive id=%d : %s',
                                $row['archive']['id'],
                                $row['archive']['archival_agency_identifier']
                            );
                        } elseif (isset($row['object_model']) && isset($row['object_foreign_key'])) {
                            $object = sprintf(
                                '%s id=%d',
                                $row['object_model'],
                                $row['object_foreign_key']
                            );
                        }
                        return [
                            'worker' => $row['tube'],
                            'error' => $row['errors'],
                            'object' => $object ?? '',
                        ];
                    }
                )
                ->toArray();
            $authUrl = $AuthUrls->saveOrFail(
                $AuthUrls->newEntity(
                    [
                        'url' => '/users/unsubscribe/' . $user->id . '/job',
                        'expire' => $this->getExpireDate($user->get('notify_job_frequency')),
                    ]
                )
            );
            $this->sendJobMail($jobErrors, $user, $authUrl);
            $this->sentJobMails++;
        }
        $this->out(__("Notifications de job envoyés: {0}", $this->sentJobMails));
    }

    /**
     * Envoi un mail de jobs en erreur à l'utilisateur
     * @param array           $jobErrors de validation
     * @param EntityInterface $user
     * @param EntityInterface $authUrl   url pour se
     *                                   désabonner
     * @return bool
     * @throws Exception
     */
    private function sendJobMail(
        array $jobErrors,
        EntityInterface $user,
        EntityInterface $authUrl
    ) {
        $prefix = $this->getConfigurationValue($user, 'mail-title-prefix');
        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $vars = [
            'htmlSignature' => $this->getConfigurationValue($user, 'mail-html-signature'),
            'textSignature' => $this->getConfigurationValue($user, 'mail-text-signature'),
            'code' => $authUrl,
            'jobErrors' => $jobErrors,
        ];
        $email->setViewVars($vars);
        $email->viewBuilder()
            ->setTemplate('notify_job')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);
        $subject = $prefix . ' ' . __("des jobs sont erreur");
        $email->setEmailFormat('both')
            ->setSubject($subject)
            ->setTo($user->get('email'));

        /** @var MailsTable $Mails */
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        return $Mails->asyncMail($email, $user->id);
    }

    /**
     * Mails liés aux transferts absent depuis un certain temps selon un delai
     * défini dans l'accord de versement
     * @return void
     * @throws Exception
     */
    private function sendAgreements()
    {
        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');

        $agreements = $Agreements->find()
            ->where(['Agreements.notify_delay >' => 0]);
        $now = new CakeDate();
        foreach ($agreements as $agreement) {
            $delay = $agreement->get('notify_delay');
            $interval = new DateInterval('P' . $delay . 'D');
            $limit = $now->add($interval);
            $hasTransfer = $Transfers->exists(
                [
                    'agreement_id' => $agreement->id,
                    'created >=' => $limit,
                    'state NOT IN' => [
                        TransfersTable::S_CREATING,
                        TransfersTable::S_PREPARATING,
                    ],
                ]
            );
            if (!$hasTransfer) {
                $this->sendAgreementsMail($agreement);
            }
        }
        $this->out(
            __(
                "Notifications des accords de versements envoyés: {0}",
                $this->sentAgreementMails
            )
        );
    }

    /**
     * Envoi les mails liés à un accord de versement aux utilisateurs abonnées
     * @param EntityInterface $agreement
     * @return void
     * @throws Exception
     */
    private function sendAgreementsMail(EntityInterface $agreement)
    {
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $Users = TableRegistry::getTableLocator()->get('Users');

        $users = $Users->find()
            ->where(
                [
                    'Users.active' => true,
                    'Users.notify_agreement_frequency IN' => $this->intervals,
                    'ArchivalAgencies.id' => $agreement->get('org_entity_id'),
                ]
            )
            ->contain(
                [
                    'OrgEntities' => ['ArchivalAgencies' => ['Configurations']],
                    'ValidationActors' => ['ValidationStages' => ['ValidationChains']],
                ]
            );
        /** @var EntityInterface $user */
        foreach ($users as $user) {
            $authUrl = $AuthUrls->saveOrFail(
                $AuthUrls->newEntity(
                    [
                        'url' => '/users/unsubscribe/' . $user->id . '/agreement',
                        'expire' => $this->getExpireDate($user->get('notify_agreement_frequency')),
                    ]
                )
            );
            $this->sendAgreementMail($agreement, $user, $authUrl);
            $this->sentAgreementMails++;
        }
    }

    /**
     * Envoi le mail lié à un accord de versement à un utilisateur
     * @param EntityInterface $agreement
     * @param EntityInterface $user
     * @param EntityInterface $authUrl
     * @return bool
     * @throws Exception
     */
    private function sendAgreementMail(
        EntityInterface $agreement,
        EntityInterface $user,
        EntityInterface $authUrl
    ) {
        $prefix = $this->getConfigurationValue($user, 'mail-title-prefix');
        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $vars = [
            'htmlSignature' => $this->getConfigurationValue($user, 'mail-html-signature'),
            'textSignature' => $this->getConfigurationValue($user, 'mail-text-signature'),
            'code' => $authUrl,
            'agreement' => $agreement,
        ];
        $email->setViewVars($vars);
        $email->viewBuilder()
            ->setTemplate('notify_agreement')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);
        $subject = $prefix . ' '
            . __("aucun transfert entrant sur ''{0}''", $agreement->get('identifier'));
        $email->setEmailFormat('both')
            ->setSubject($subject)
            ->setTo($user->get('email'));

        /** @var MailsTable $Mails */
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        return $Mails->asyncMail($email, $user->id);
    }

    /**
     * Envoi un rapport sur les transferts entrants aux abonnées
     * @return void
     * @throws Exception
     */
    private function sendTransferReport()
    {
        $Users = TableRegistry::getTableLocator()->get('Users');

        $users = $Users->find()
            ->where(
                [
                    'Users.active' => true,
                    'Users.notify_transfer_report_frequency IN' => $this->intervals,
                ]
            )
            ->contain(
                [
                    'SuperArchivistsArchivalAgencies' => ['ArchivalAgencies'],
                    'OrgEntities' => ['ArchivalAgencies' => ['Configurations']],
                    'ValidationActors' => ['ValidationStages' => ['ValidationChains']],
                ]
            );
        $counts = [];
        /** @var EntityInterface $user */
        foreach ($users as $user) {
            // Les super archivistes ont plusieurs SA
            $archivalAgencies = Hash::extract($user, 'super_archivists_archival_agencies.{n}.archival_agency');
            if (empty($archivalAgencies)) {
                $archivalAgencies = [Hash::get($user, 'org_entity.archival_agency')];
            }
            foreach ($archivalAgencies as $archivalAgency) {
                $this->doReportTransferForUser($user, Hash::get($archivalAgency, 'id'), $counts);
            }
        }
        $this->out(
            __(
                "Notifications pour le récapitulatif des transferts entrants: {0}",
                $this->sentTransferReportMails
            )
        );
    }

    /**
     * Envoi une notification de rapport de transfert pour un utilisateur et un
     * service d'archives donné en alimentant $counts
     * @param EntityInterface $user
     * @param int             $archival_agency_id
     * @param array           $counts
     * @return void
     * @throws Exception
     */
    private function doReportTransferForUser(
        EntityInterface $user,
        int $archival_agency_id,
        array &$counts
    ) {
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $freq = $user->get('notify_transfer_report_frequency');
        if (!isset($counts[$archival_agency_id . ':' . $freq])) {
            $counts[$archival_agency_id . ':' . $freq] = $this->reportTransfer($archival_agency_id, $freq);
        }
        if ($counts[$archival_agency_id . ':' . $freq]) {
            $authUrl = $AuthUrls->saveOrFail(
                $AuthUrls->newEntity(
                    [
                        'url' => '/users/unsubscribe/' . $user->id . '/transfer_report',
                        'expire' => $this->getExpireDate($freq),
                    ]
                )
            );
            $this->sendTransferReportMail($counts[$archival_agency_id . ':' . $freq], $user, $authUrl);
            $this->sentTransferReportMails++;
        }
    }

    /**
     * Donne le rapport de transferts d'un service d'archive sur une période
     * donnée
     * @param int    $archival_agency_id
     * @param string $frequency
     * @return array|false false si pas de transferts
     */
    private function reportTransfer(int $archival_agency_id, string $frequency)
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $report = [];

        $q = $Transfers->query();
        $baseQuery = $Transfers->find()
            ->select(
                [
                    'count' => $q->func()->count('*'),
                    'size' => $q->func()->sum('data_size'),
                ]
            )
            ->where(
                [
                    'archival_agency_id' => $archival_agency_id,
                    'created >=' => $this->getFromDate($frequency),
                ]
            )
            ->disableHydration();
        $report['accepted'] = (clone $baseQuery)
            ->andWhere(['state' => TransfersTable::S_ACCEPTED])
            ->all()
            ->map(fn($row) => array_map('intval', $row))
            ->toArray()[0];
        $report['rejected'] = (clone $baseQuery)
            ->andWhere(['state' => TransfersTable::S_REJECTED])
            ->all()
            ->map(fn($row) => array_map('intval', $row))
            ->toArray()[0];
        $report['validating'] = (clone $baseQuery)
            ->andWhere(['state' => TransfersTable::S_VALIDATING])
            ->all()
            ->map(fn($row) => array_map('intval', $row))
            ->toArray()[0];
        $totalCount = Hash::reduce($report, '{s}.count', fn($total, $v) => $total + $v);
        return $totalCount ? $report : false;
    }

    /**
     * Donne le timestamp de la date d'expiration selon la fréquence
     * @param string $frequency
     * @return string timestamp
     */
    private function getFromDate(string $frequency): string
    {
        switch ($frequency) {
            case '1':
                $duration = strtotime('-1 day');
                break;
            case '7':
                $duration = strtotime('-1 week');
                break;
            case '30':
                $duration = strtotime('-1 month');
                break;
            case '365':
                $duration = strtotime('-1 year');
                break;
            default:
                $duration = strtotime("-$frequency days");
                break;
        }
        return date('Y-m-d H:i:s', $duration);
    }

    /**
     * Envoi le mail récapitulatif des transferts entrants
     * @param array           $report
     * @param EntityInterface $user
     * @param EntityInterface $authUrl
     * @return bool
     * @throws Exception
     */
    private function sendTransferReportMail(
        array $report,
        EntityInterface $user,
        EntityInterface $authUrl
    ) {
        $prefix = $this->getConfigurationValue($user, 'mail-title-prefix');
        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $vars = [
            'htmlSignature' => $this->getConfigurationValue($user, 'mail-html-signature'),
            'textSignature' => $this->getConfigurationValue($user, 'mail-text-signature'),
            'code' => $authUrl,
            'report' => $report,
            'user' => $user,
        ];
        $email->setViewVars($vars);
        $email->viewBuilder()
            ->setTemplate('notify_transfer_report')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);
        $subject = $prefix . ' '
            . __("récapitulatif des transferts entrants");
        $email->setEmailFormat('both')
            ->setSubject($subject)
            ->setTo($user->get('email'));

        /** @var MailsTable $Mails */
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        return $Mails->asyncMail($email, $user->id);
    }
}
