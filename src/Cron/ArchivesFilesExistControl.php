<?php

/**
 * Asalae\Cron\ArchivesFilesExistControl
 */

namespace Asalae\Cron;

use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\TechnicalArchivesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\ColoredCronTrait;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeInterface;
use AsalaeCore\Cron\CronInterface;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Datasource\EntityInterface;
use Cake\Log\Log;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use DateInterval;
use DateTime;
use Exception;

/**
 * Interface des crons
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchivesFilesExistControl implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;
    use TouchableTrait;
    use ColoredCronTrait;

    /**
     * @var array params
     */
    private $params;

    /**
     * @var Exception state = error si != empty
     */
    private $fail;

    /**
     * @var int nombre d'evenements exportés
     */
    private $index = 0;

    /**
     * @var int nombre d'evenements à exporter
     */
    private $total;

    /**
     * @var VolumeInterface[] volumes de stockage
     */
    private $volumes;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params paramètres additionnels
     *                                       du cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->params = $params + [
            'max_execution_time' => 1, // en heures
            'min_delay' => 1, // en jours
            'update_time' => 60, // en secondes
            'context' => 'shell',
        ];
        $this->initializeOutput($out, $err);
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [
            'max_execution_time' => [
                'type' => 'number',
                'label' => 'max_execution_time',
                'default' => 1,
                'min' => 1,
                'help' => __(
                    "Temps maximum d'exécution de la tâche (en heures)"
                ),
            ],
            'min_delay' => [
                'type' => 'number',
                'label' => 'min_delay',
                'default' => 1,
                'min' => 1,
                'help' => __(
                    "Délai minimum entre deux vérifications (en jours)"
                ),
            ],
        ];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws VolumeException
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        $this->initCounter($exec);
        $beginTime = time();
        $maxTime = $beginTime + (3600 * $this->params['max_execution_time']);
        $threshold = (new DateTime())->sub(
            new DateInterval('P' . ($this->params['min_delay']) . 'D')
        );

        $loc = TableRegistry::getTableLocator();

        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');
        $archivesConditions = $this->params['context'] === 'user-action'
            ? []
            : [
                'Archives.is_integrity_ok IS' => true,
                'OR' => [
                    'Archives.filesexist_date IS' => null,
                    'Archives.filesexist_date <' => $threshold,
                ],
            ];
        $archivesQuery = $Archives->findByIntegrity()
            ->where($archivesConditions)
            ->andWhere(
                [
                    'ArchiveBinaries.is_integrity_ok IS' => true,
                    'StoredFilesVolumes.is_integrity_ok IS' => true,
                ]
            )
            ->orderBy(
                [
                    'Archives.filesexist_date IS NULL' => 'desc',
                    // nouveaux en 1er
                    'Archives.filesexist_date' => 'asc',
                    // par dernière vérif
                    'Archives.created' => 'asc',
                    // par date de création
                    'StoredFilesVolumes.volume_id' => 'asc',
                    'StoredFilesVolumes.storage_ref' => 'asc',
                ],
                true
            );
        /** @var TechnicalArchivesTable $TechnicalArchives */
        $TechnicalArchives = $loc->get('TechnicalArchives');
        $technicalArchivesConditions = $this->params['context'] === 'user-action'
            ? []
            : [
                'TechnicalArchives.is_integrity_ok IS' => true,
                'OR' => [
                    'TechnicalArchives.filesexist_date IS' => null,
                    'TechnicalArchives.filesexist_date <' => $threshold,
                ],
            ];
        $technicalArchivesQuery = $TechnicalArchives->findByIntegrity()
            ->where($technicalArchivesConditions)
            ->andWhere(
                [
                    'ArchiveBinaries.is_integrity_ok IS' => true,
                    'StoredFilesVolumes.is_integrity_ok IS' => true,
                ]
            )
            ->orderBy(
                [
                    'TechnicalArchives.filesexist_date IS NULL' => 'desc',
                    // nouveaux en 1er
                    'TechnicalArchives.filesexist_date' => 'asc',
                    // par dernière vérif
                    'TechnicalArchives.created' => 'asc',
                    // par date de création
                    'StoredFilesVolumes.volume_id' => 'asc',
                    'StoredFilesVolumes.storage_ref' => 'asc',
                ],
                true
            );

        $count = $archivesQuery->count() + $technicalArchivesQuery->count();
        if ($count === 0) {
            $this->out(
                __(
                    "Aucune vérification de présence des fichiers n'est nécessaire"
                )
            );
            return 'success';
        } else {
            $this->out(
                __n(
                    "Il y a {0} fichier à vérifier",
                    "Il y a {0} fichiers à vérifier",
                    $count,
                    $count
                )
            );
        }
        $this->total = $count;
        $failed = false;
        $skipped = false;

        $archiveSuccesses = 0;
        $archivesQuery->select(['Archives.id'], true)
            ->disableHydration();
        $alreadyDone = [];
        foreach ($archivesQuery as $archive) {
            if (isset($alreadyDone[$archive['id']])) {
                continue; // distinct sans modifier le order by
            }
            $alreadyDone[$archive['id']] = true;
            // Si le process à durée trop longtemps, on s'arrète là
            if (time() > $maxTime) {
                $this->out(__("Limite de durée d'exécution atteinte."));
                break;
            }
            $success = $this->checkArchive($archive['id']);
            if ($success === true) {
                $archiveSuccesses++;
            } elseif ($success === false) {
                $failed = true;
            } else {
                $skipped = true;
            }
        }
        if ($archiveSuccesses) {
            $this->out(
                __n(
                    "Archive testée avec succès: {0}",
                    "Archives testées avec succès: {0}",
                    $archiveSuccesses,
                    $archiveSuccesses
                )
            );
        }

        $technicalArchiveSuccesses = 0;
        $technicalArchivesQuery->select(['TechnicalArchives.id'], true)
            ->disableHydration();
        $alreadyDone = [];
        foreach ($technicalArchivesQuery as $technicalArchive) {
            if (isset($alreadyDone[$technicalArchive['id']])) {
                continue; // distinct sans modifier le order by
            }
            $alreadyDone[$technicalArchive['id']] = true;
            // Si le process à durée trop longtemps, on s'arrète là
            if (time() > $maxTime) {
                $this->out(__("Limite de durée d'exécution atteinte."));
                break;
            }
            $success = $this->checkTechnicalArchive($technicalArchive['id']);
            if ($success === true) {
                $technicalArchiveSuccesses++;
            } elseif ($success === false) {
                $failed = true;
            } else {
                $skipped = true;
            }
        }
        if ($technicalArchiveSuccesses) {
            $this->out(
                __n(
                    "Archive technique testée avec succès: {0}",
                    "Archives techniques testées avec succès: {0}",
                    $technicalArchiveSuccesses,
                    $technicalArchiveSuccesses
                )
            );
        }

        $success = ($archiveSuccesses || $technicalArchiveSuccesses) && $failed === false && $skipped === false;
        $outcome = $success ? 'success' : ($failed ? 'error' : 'warning');

        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->newEntry(
            'check_archives_files_exist',
            $outcome,
            $this->getOutput(),
            $cron,
            $cron
        );
        $EventLogs->saveOrFail($entry);

        return $outcome;
    }

    /**
     * Vérifie la présence des fichiers d'une archive
     * @param int $archive_id
     * @return bool|null
     * @throws VolumeException
     */
    private function checkArchive($archive_id)
    {
        return $this->checkArchiveCommons(
            $archive_id,
            'Archives',
            __("Archive")
        );
    }

    /**
     * Vérifie la présence des fichiers d'une archive/archive technique
     * @param int    $id
     * @param string $model
     * @param string $name
     * @return bool|null
     * @throws VolumeException
     */
    private function checkArchiveCommons($id, string $model, string $name)
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivesTable|TechnicalArchivesTable $Archives */
        $Archives = $loc->get($model);
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');

        $query = $Archives->findByIntegrity()
            ->where([$model . '.id' => $id])
            ->andWhere(
                [
                    $model . '.is_integrity_ok IS' => true,
                    'ArchiveBinaries.is_integrity_ok IS' => true,
                    'StoredFilesVolumes.is_integrity_ok IS' => true,
                ]
            );

        $archiveIntegrity = true;
        $conn = $Archives->getConnection();
        $conn->begin();

        /** @var EntityInterface $fetch */
        foreach ($query as $fetch) {
            $this->touchAndReport();
            $archiveBinaryIntegrity = true;
            if ($model === 'Archives') {
                $storedFile = new Entity(
                    [
                        'id' => $fetch->get('stored_file_id'),
                        'name' => $fetch->get('stored_file_name'),
                    ]
                );
                $fetch->set(
                    'lifecycle_xml_archive_file',
                    ['stored_file' => $storedFile]
                );
            }
            $archive = $fetch->toArray();

            // si le volume ne fonctionne pas, on passe
            if (!$volume = $this->getVolume($archive['volume_id'])) {
                $conn->rollback();
                $this->out("$name n°$id <warning>Skipped</warning>");
                return null;
            }

            // on récupère le fichier
            try {
                $exists = $volume->fileExists($archive['storage_ref']);
                if (!$exists) {
                    Log::error(
                        $err = __(
                            "{0} - Le fichier {1} n'a pas été trouvé sur le volume {2} (id: {3})",
                            $archive['archival_agency_identifier'],
                            $archive['storage_ref'],
                            $archive['volume_name'],
                            $archive['volume_id']
                        )
                    );
                    $this->out($err);
                    $archiveIntegrity = false;
                    $archiveBinaryIntegrity = false;
                }
            } catch (VolumeException $e) {
                /** @noinspection PhpIfWithCommonPartsInspection $volume->ping() doit avoir lieu avant le rollback */
                if (
                    $e->volumeErrorCode === VolumeException::FILE_NOT_FOUND
                    && $volume->ping() === false
                ) {
                    // interruption du volume, on passe l'archive sans la marquer non intègre
                    $conn->rollback();
                    return null;
                } else {
                    $conn->rollback();
                    throw $e;
                }
            }
            $ArchiveBinaries->updateAll(
                ['is_integrity_ok' => $archiveBinaryIntegrity],
                ['id' => $archive['archive_binary_id']]
            );
            $StoredFilesVolumes->updateAll(
                ['is_integrity_ok' => $archiveBinaryIntegrity],
                [
                    'storage_ref' => $archive['storage_ref'],
                    'volume_id' => $archive['volume_id'],
                    'is_integrity_ok IS NOT' => false,
                ]
            );
        }
        if ($model === 'Archives') {
            $ArchiveFiles = $loc->get('ArchiveFiles');
            $query = $Archives->findByArchiveFilesIntegrity()
                ->where([$model . '.id' => $id])
                ->andWhere([$model . '.is_integrity_ok IS' => true]);
            /** @var EntityInterface $fetch */
            foreach ($query as $fetch) {
                $this->touchAndReport();
                $archiveFileIntegrity = true;
                $storedFile = new Entity(
                    [
                        'id' => $fetch->get('stored_file_id'),
                        'name' => $fetch->get('stored_file_name'),
                    ]
                );
                $fetch->set(
                    'lifecycle_xml_archive_file',
                    ['stored_file' => $storedFile]
                );
                $archive = $fetch->toArray();

                // si le volume ne fonctionne pas, on passe
                if (!$volume = $this->getVolume($archive['volume_id'])) {
                    $conn->rollback();
                    $this->out("$name n°$id <warning>Skipped</warning>");
                    return null;
                }

                // on récupère le fichier
                try {
                    $exists = $volume->fileExists($archive['storage_ref']);
                    if (!$exists) {
                        Log::error(
                            $err = __(
                                "{0} - Le fichier {1} n'a pas été trouvé sur le volume {2} (id: {3})",
                                $archive['archival_agency_identifier'],
                                $archive['storage_ref'],
                                $archive['volume_name'],
                                $archive['volume_id']
                            )
                        );
                        $this->out($err);
                        $archiveIntegrity = false;
                        $archiveFileIntegrity = false;
                    }
                } catch (VolumeException $e) {
                    /** @noinspection PhpIfWithCommonPartsInspection $volume->ping() doit avoir lieu avant le rollback */
                    if (
                        $e->volumeErrorCode === VolumeException::FILE_NOT_FOUND
                        && $volume->ping() === false
                    ) {
                        // interruption du volume, on passe l'archive sans la marquer non intègre
                        $conn->rollback();
                        return null;
                    } else {
                        $conn->rollback();
                        throw $e;
                    }
                }
                $ArchiveFiles->updateAll(
                    ['is_integrity_ok' => $archiveFileIntegrity],
                    ['id' => $archive['archive_file_id']]
                );
                $StoredFilesVolumes->updateAll(
                    ['is_integrity_ok' => $archiveFileIntegrity],
                    [
                        'storage_ref' => $archive['storage_ref'],
                        'volume_id' => $archive['volume_id'],
                        'is_integrity_ok IS NOT' => false,
                    ]
                );
            }
        }
        $Archives->updateAll(
            [
                'is_integrity_ok' => $archiveIntegrity,
                'filesexist_date' => new DateTime(),
            ],
            ['id' => $id]
        );
        $conn->commit();
        // On n'affiche que les archives en erreur
        if (!$archiveIntegrity) {
            $this->o(false, "$name n°$id");
        }
        return $archiveIntegrity;
    }

    /**
     * Affirme son existance et affiche un rapport
     */
    public function touchAndReport()
    {
        $ntimer = microtime(true);
        if ($this->exec && $ntimer - $this->timer > $this->minDelayBeforeSave) {
            $this->timer = $ntimer;
            $this->out(
                __("Fichiers contrôlées: {0} / {1}", $this->index, $this->total)
            );
            $this->exec->set('last_update', new DateTime());
            $this->CronExecutions->save($this->exec);
        }
        $this->index++;
    }

    /**
     * Donne le volume par son id
     * @param int $volume_id
     * @return VolumeInterface|false
     */
    private function getVolume(int $volume_id)
    {
        if (!isset($this->volumes[$volume_id])) {
            try {
                $this->volumes[$volume_id] = VolumeManager::getDriverById(
                    $volume_id
                );
            } catch (VolumeException) {
                $this->volumes[$volume_id] = false;
            }
        }
        return $this->volumes[$volume_id];
    }

    /**
     * Vérifie la présence des fichiers d'une archive technique
     * @param int $archive_id
     * @return bool|null
     * @throws VolumeException
     */
    private function checkTechnicalArchive($archive_id)
    {
        return $this->checkArchiveCommons(
            $archive_id,
            'TechnicalArchives',
            __("Archive technique")
        );
    }
}
