<?php

/**
 * Asalae\Cron\ArchiveUnitCalcSearch
 */

namespace Asalae\Cron;

use Asalae\Model\Table\ArchiveUnitsTable;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\CronInterface;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use DateTime;
use Exception;

/**
 * Calcule le champ ArchiveUnits.search
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveUnitCalcSearch implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;
    use TouchableTrait;

    /**
     * @var int nombre d'archives traités
     */
    private $index = 0;

    /**
     * @var int nombre d'archives a traiter
     */
    private $total;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params paramètres additionnels du
     *                                       cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        $this->initCounter($exec);
        $loc = TableRegistry::getTableLocator();
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $Archives = $loc->get('Archives');
        $ArchiveDescriptions = $loc->get('ArchiveDescriptions');
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $conn = $Archives->getConnection();
        $conn->begin();
        $query = $Archives->find()
            ->where(
                ['Archives.next_search <=' => (new DateTime())->format('Y-m-d')]
            );
        $count = $query->count();
        $this->total = $count;
        $this->out(__("Il y a {0} archive(s) à mettre à jours", $count));
        $success = true;
        /** @var EntityInterface $archive */
        foreach ($query as $archive) {
            $this->touchAndReport();
            $ArchiveUnits->updateSearchField(
                ['ArchiveUnits.archive_id' => $archive->id]
            );
            $nextDescription = $ArchiveDescriptions->find()
                ->select(['end_date' => 'DescriptionAccessRules.end_date'])
                ->innerJoinWith('DescriptionAccessRules')
                ->innerJoinWith('ArchiveUnits')
                ->where(['ArchiveUnits.archive_id' => $archive->id])
                ->andWhere(
                    [
                        'DescriptionAccessRules.end_date >' => (new DateTime())->format(
                            'Y-m-d'
                        ),
                    ]
                )
                ->orderBy(['DescriptionAccessRules.end_date' => 'asc'])
                ->first();
            $nextKeyword = $ArchiveKeywords->find()
                ->select(['end_date' => 'KeywordAccessRules.end_date'])
                ->innerJoinWith('KeywordAccessRules')
                ->innerJoinWith('ArchiveUnits')
                ->where(['ArchiveUnits.archive_id' => $archive->id])
                ->andWhere(
                    [
                        'KeywordAccessRules.end_date >' => (new DateTime())->format(
                            'Y-m-d'
                        ),
                    ]
                )
                ->orderBy(['KeywordAccessRules.end_date' => 'asc'])
                ->first();
            $nextDescription = $nextDescription ? $nextDescription->get(
                'end_date'
            ) : null;
            $nextKeyword = $nextKeyword ? $nextKeyword->get('end_date') : null;
            $next = $nextDescription && $nextKeyword
                ? min($nextDescription, $nextKeyword)
                : ($nextDescription ?: $nextKeyword);
            $archive->set('next_search', $next);
            if (!$Archives->save($archive)) {
                $this->out('save failed');
                $success = false;
                break;
            }
        }
        if ($success) {
            $conn->commit();
            return 'success';
        } else {
            $conn->rollback();
            return 'error';
        }
    }

    /**
     * Affirme son existance et affiche un rapport
     */
    public function touchAndReport()
    {
        $ntimer = microtime(true);
        if ($this->exec && $ntimer - $this->timer > $this->minDelayBeforeSave) {
            $this->timer = $ntimer;
            $this->out(
                __("Archives traités: {0} / {1}", $this->index, $this->total)
            );
            $this->exec->set('last_update', new DateTime());
            $this->CronExecutions->save($this->exec);
        }
        $this->index++;
    }
}
