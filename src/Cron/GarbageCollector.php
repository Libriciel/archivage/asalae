<?php

/**
 * Asalae\Cron\GarbageCollector
 */

namespace Asalae\Cron;

use Asalae\Model\Entity\Archive;
use Asalae\Model\Entity\ArchiveUnit;
use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\DeliveryRequestsTable;
use Asalae\Model\Table\OaipmhTokensTable;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Console\ConsoleOutput;
use AsalaeCore\Cron\CronInterface;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInterval;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Suppression :
 *      - des OaipmhTokens expirés, avec les OaipmhListsets et OaipmhArchives ne cascade
 *      - des fichiers zip expirés (Archives et Transferts)
 *
 * @category Shell\Cron
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class GarbageCollector implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;

    /**
     * @var array params
     */
    private $params;

    /**
     * @var Exception state = error si != empty
     */
    private $fail;

    /**
     * @var int date pour le calcul de la péremption d'un fichier
     */
    private $limitDate;

    /**
     * @var int décompte des fichiers supprimés
     */
    private $deletedFiles = 0;

    /**
     * Constructeur de la classe du cron
     * @param array              $params paramètres additionnels du
     *                                   cron
     * @param ConsoleOutput|null $out
     * @param ConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        $this->limitDate = time() - (Configure::read('Uploads.tempFileConservationTimeLimit', 24 * 7) * 3600);

        if (
            $this->cleanAccessTokens()
            && $this->cleanAuthUrls()
            && $this->cleanOaipmhTokens()
            && $this->removeArchiveZipFiles()
            && $this->removeArchiveUnitZipFiles()
            && $this->removeTransferFiles()
            && $this->removeUploadedFiles()
            && $this->removeDeliveryFiles()
            && $this->cleanDataDir(Configure::read('App.paths.data') . DS . 'upload')
        ) {
            return 'success';
        }
        return 'error';
    }

    /**
     * Suppression des access tokens
     * @return bool
     */
    private function cleanAccessTokens(): bool
    {
        $AccessTokens = TableRegistry::getTableLocator()->get('AccessTokens');

        $query = $AccessTokens->find()
            ->where(['expiration_date <' => time()]);

        $count = 0;
        foreach ($query as $entity) {
            $AccessTokens->deleteOrFail($entity);
            $count++;
        }

        $this->out(
            __n(
                "{0} access token supprimé",
                "{0} access tokens supprimés",
                $count,
                $count
            )
        );
        return true;
    }

    /**
     * Suppression des auth urls
     * @return bool
     */
    private function cleanAuthUrls(): bool
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');

        $query = $AuthUrls->find()
            ->where(['expire <' => time()]);

        $count = 0;
        foreach ($query as $entity) {
            $AuthUrls->deleteOrFail($entity);
            $count++;
        }

        $this->out(
            __n(
                "{0} auth urls supprimé",
                "{0} auth url supprimés",
                $count,
                $count
            )
        );
        return true;
    }

    /**
     * Suppression des oaipmh tokens
     * @return bool
     * @throws Exception
     */
    private function cleanOaipmhTokens(): bool
    {
        /** @var OaipmhTokensTable $OaipmhTokens */
        $OaipmhTokens = TableRegistry::getTableLocator()->get('OaipmhTokens');

        $query = $OaipmhTokens->find()
            ->where(['expiration_date <' => new CakeDateTime()])
            ->orderBy(['id' => 'desc']);

        $count = $query->count();
        foreach ($query as $entity) {
            $OaipmhTokens->deleteOrFail($entity);
        }

        $this->out(
            __n(
                "{0} oaipmh token supprimé",
                "{0} oaipmh tokens supprimés",
                $count,
                $count
            )
        );
        return true;
    }

    /**
     * Suppression des fichiers zip d'archive dont la date de création dépasse la limite configurée
     * @return bool
     * @throws Exception
     */
    private function removeArchiveZipFiles(): bool
    {
        $folder = Archive::getZipDownloadFolder();
        $limitDate = time() - Configure::read(
            'Downloads.tempFileConservationTimeLimit'
        ) * 3600;
        $count = 0;
        if (is_dir($folder)) {
            $handle = opendir($folder);
            while ($entry = readdir($handle)) {
                if (
                    is_file($folder . $entry)
                    && filemtime($folder . $entry) <= $limitDate
                ) {
                    Filesystem::remove($folder . $entry);
                    $count++;
                }
            }
            closedir($handle);
        }
        $this->out(
            __n(
                "{0} zip d'archive supprimé",
                "{0} zip d'archive supprimés",
                $count,
                $count
            )
        );
        return true;
    }

    /**
     * Suppression des fichiers zip d'unité d'archives dont la date de création
     * dépasse la limite configurée
     * @return bool
     * @throws Exception
     */
    protected function removeArchiveUnitZipFiles(): bool
    {
        $folder = ArchiveUnit::getZipDownloadFolder();
        $limitDate = time() - Configure::read(
            'Downloads.tempFileConservationTimeLimit'
        ) * 3600;
        $count = 0;
        if (is_dir($folder)) {
            $handle = opendir($folder);
            while ($entry = readdir($handle)) {
                if (
                    is_file($folder . $entry)
                    && filemtime($folder . $entry) <= $limitDate
                ) {
                    Filesystem::remove($folder . $entry);
                    $count++;
                }
            }
            closedir($handle);
        }
        $this->out(
            __n(
                "{0} zip d'unité d'archives supprimé",
                "{0} zip d'unité d'archives supprimés",
                $count,
                $count
            )
        );
        return true;
    }

    /**
     * Suppression des fichiers des transferts dont la date de création dépasse la limite configurée
     * @return bool
     * @throws Exception
     */
    private function removeTransferFiles(): bool
    {
        $folder = Transfer::getZipDownloadFolder();
        $limitDate = time() - Configure::read(
            'Downloads.tempFileConservationTimeLimit'
        ) * 3600;
        $count = 0;
        if (is_dir($folder)) {
            $count = self::removeOldFilesAndEmptySubFolders($folder, $limitDate);
        }
        $this->out(
            __n(
                "{0} fichier de transfert supprimé",
                "{0} fichiers de transfert supprimés",
                $count,
                $count
            )
        );
        return true;
    }

    /**
     * suppression des fichiers et des sous répertoires vides en fonction d'une date limite
     * @param string $folder    dossier racine des fichiers
     *                          à supprimer
     * @param int    $olderThan date limite des fichiers à supprimer
     * @return float nombre de fichiers supprimés
     * @throws Exception
     */
    private function removeOldFilesAndEmptySubFolders($folder, $olderThan): float
    {
        $count = 0;
        $handle = opendir($folder);
        while ($entry = readdir($handle)) {
            if ($entry == '.' || $entry == '..') {
                continue;
            }
            if (
                is_file($folder . $entry)
                && filemtime($folder . $entry) <= $olderThan
            ) {
                Filesystem::remove($folder . $entry);
                $count++;
            }
            if (is_dir($folder . $entry)) {
                $subFolder = $folder . $entry . DS;
                $count += self::removeOldFilesAndEmptySubFolders($subFolder, $olderThan);
                Filesystem::removeEmptySubFolders($subFolder);
            }
        }
        closedir($handle);
        return $count;
    }

    /**
     * Supprime les fichiers uploadés non utilisés
     * @return bool
     */
    private function removeUploadedFiles(): bool
    {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $fileuploads = $Fileuploads->find()
            ->where(
                [
                    'locked' => false,
                    'created <' => $this->limitDate,
                ]
            );
        $successCount = 0;
        $errorCount = 0;
        foreach ($fileuploads as $fileupload) {
            if ($Fileuploads->delete($fileupload, ['unlink' => true])) {
                $successCount++;
            } else {
                $errorCount++;
                $this->err(
                    __(
                        "Le fichier {0} ({1}) n'a pas pu être supprimé.",
                        $fileupload->get('name'),
                        $fileupload->get('path')
                    )
                );
            }
        }
        $this->out(
            __n(
                "{0} fichier d'upload supprimé",
                "{0} fichiers d'upload supprimés",
                $successCount,
                $successCount
            )
        );
        if ($errorCount) {
            $this->out(
                __n(
                    "{0} fichier d'upload en erreur",
                    "{0} fichiers d'upload en erreur",
                    $errorCount,
                    $errorCount
                )
            );
        }
        return true;
    }

    /**
     * Supprime les communications
     * @return bool
     * @throws Exception
     */
    private function removeDeliveryFiles(): bool
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $Deliveries = TableRegistry::getTableLocator()->get('Deliveries');
        $query = $OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'TypeEntities.code' => 'SA',
                ]
            )
            ->contain(
                [
                    'Configurations' => function (Query $q) {
                        return $q->where(
                            [
                                'Configurations.name' => ConfigurationsTable::CONF_DELIVERY_RETENTION_PERIOD,
                            ]
                        );
                    },
                ]
            );
        $count = 0;
        /** @var EntityInterface $archivalAgency */
        foreach ($query as $archivalAgency) {
            $retention = (int)Hash::get($archivalAgency, 'configurations.0.setting', 7);
            $limit = (new CakeDateTime())->sub(new DateInterval("P{$retention}D"));
            $q = $Deliveries->find()
                ->innerJoinWith('DeliveryRequests')
                ->where(
                    [
                        'DeliveryRequests.state IN' => [
                            DeliveryRequestsTable::S_DELIVERY_AVAILABLE,
                            DeliveryRequestsTable::S_DELIVERY_DOWNLOADED,
                        ],
                        'DeliveryRequests.last_state_update <' => $limit,
                    ]
                );
            foreach ($q as $delivery) {
                if ($Deliveries->delete($delivery)) { // transition delete dans le callback afterDelete
                    $count++;
                }
            }
        }
        $this->out(
            __n(
                "{0} communication supprimé",
                "{0} communications supprimés",
                $count,
                $count
            )
        );
        return true;
    }

    /**
     * Nettoie un dossier en supprimant les fichier plus anciens que Uploads.tempFileConservationTimeLimit
     *      et en supprimant les dossiers vides qui en résultent
     *      de manière récursive
     * @param string $dirname
     * @param bool   $initial première récursion : on affiche le décompte et on conserve le dossier
     * @return bool
     * @throws Exception
     */
    private function cleanDataDir(string $dirname, bool $initial = true): bool
    {
        if ($initial) {
            $this->deletedFiles = 0;
        }

        foreach (glob($dirname . '/*') as $file) {
            if (!is_writable($file)) {
                throw new Exception(__("{0} n'est pas inscriptible", $file));
            }
            if (is_dir($file)) {
                $this->cleanDataDir($file, false);
            } elseif (filemtime($file) < $this->limitDate) {
                Filesystem::remove($file);
                $this->deletedFiles++;
            }
        }
        if (!$initial && count(glob($dirname . '/*')) === 0) {
            Filesystem::remove($dirname);
        }

        if ($initial) {
            $this->out(
                __n(
                    "{0} fichiers supprimé dans {1}",
                    "{0} fichiers supprimés dans {1}",
                    $this->deletedFiles,
                    $this->deletedFiles,
                    $dirname
                )
            );
        }
        return true;
    }
}
