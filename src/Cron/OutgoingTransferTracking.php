<?php

/**
 * Asalae\Cron\OutgoingTransferTracking
 */

namespace Asalae\Cron;

use Asalae\Exception\GenericException;
use Asalae\Model\Entity\ArchivingSystem;
use Asalae\Model\Table\OutgoingTransferRequestsTable;
use Asalae\Model\Table\OutgoingTransfersTable;
use Asalae\Utility\SedaValidator;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\ColoredCronTrait;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Utility\DOMUtility;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Datasource\EntityInterface;
use Cake\Http\Client;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DOMDocument;
use Exception;

/**
 * Lecture des accusés de réception et des réponses aux
 * transferts des transferts sortants en cours
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OutgoingTransferTracking implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;
    use ColoredCronTrait;

    /**
     * @var OutgoingTransferRequestsTable
     */
    private $OutgoingTransferRequests;
    /**
     * @var OutgoingTransfersTable
     */
    private $OutgoingTransfers;
    /**
     * @var EntityInterface
     */
    private $cron;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params paramètres additionnels du
     *                                       cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     * @throws Exception
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
        $loc = TableRegistry::getTableLocator();
        $this->OutgoingTransfers = $loc->get('OutgoingTransfers');
        $this->OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        $query = $this->OutgoingTransferRequests->find()
            ->where(
                ['state' => OutgoingTransferRequestsTable::S_TRANSFERS_SENT]
            )
            ->contain(['ArchivingSystems']);
        $this->out(
            __(
                "Il y a {0} demande(s) de transfert (état: {1}) à traiter",
                $query->count(),
                $this->OutgoingTransferRequests->options(
                    'state'
                )['transfers_sent']
            )
        );
        $conn = $this->OutgoingTransferRequests->getConnection();
        $output = 'success';
        /** @var EntityInterface $outgoingTransferRequest */
        foreach ($query as $outgoingTransferRequest) {
            $conn->begin();
            /** @var ArchivingSystem $archivingSystem */
            $archivingSystem = Hash::get(
                $outgoingTransferRequest,
                'archiving_system'
            );
            $outgoingTransfers = $this->OutgoingTransfers->find()
                ->where(
                    [
                        'state' => OutgoingTransfersTable::S_SENT,
                        'outgoing_transfer_request_id' => $outgoingTransferRequest->id,
                    ]
                );
            /** @var EntityInterface $outgoingTransfer */
            foreach ($outgoingTransfers as $outgoingTransfer) {
                $response = $archivingSystem->getAcknowledgement(
                    $outgoingTransfer
                );
                if (
                    $this->checkResponse(
                        $response,
                        $outgoingTransfer
                    ) === 'warning'
                ) {
                    $output = 'warning';
                    continue;
                }
                if (
                    $this->OutgoingTransfers->transition(
                        $outgoingTransfer,
                        OutgoingTransfersTable::T_ACKNOWLEDGE
                    )
                ) {
                    $this->OutgoingTransfers->save($outgoingTransfer);
                }
            }
            if (
                $outgoingTransfers->count() === 0
                && $this->OutgoingTransferRequests->transition(
                    $outgoingTransferRequest,
                    OutgoingTransferRequestsTable::T_ACKNOWLEDGE_TRANSFERS
                )
            ) {
                $this->OutgoingTransferRequests->save($outgoingTransferRequest);
            }
            $conn->commit();
        }

        $query = $this->OutgoingTransferRequests->find()
            ->where(
                ['state' => OutgoingTransferRequestsTable::S_TRANSFERS_RECEIVED]
            )
            ->contain(['ArchivingSystems']);
        $this->out(
            __(
                "Il y a {0} demande(s) de transfert (état: {1}) à traiter",
                $query->count(),
                $this->OutgoingTransferRequests->options(
                    'state'
                )['transfers_received']
            )
        );
        /** @var EntityInterface $outgoingTransferRequest */
        foreach ($query as $outgoingTransferRequest) {
            $conn->begin();
            /** @var ArchivingSystem $archivingSystem */
            $archivingSystem = Hash::get(
                $outgoingTransferRequest,
                'archiving_system'
            );
            $outgoingTransfers = $this->OutgoingTransfers->find()
                ->where(
                    [
                        'state' => OutgoingTransfersTable::S_RECEIVED,
                        'outgoing_transfer_request_id' => $outgoingTransferRequest->id,
                    ]
                );
            /** @var EntityInterface $outgoingTransfer */
            foreach ($outgoingTransfers as $outgoingTransfer) {
                $response = $archivingSystem->getReply($outgoingTransfer);
                $dom = $this->checkResponse($response, $outgoingTransfer);
                if (!$dom instanceof DOMDocument) {
                    $output = 'warning';
                    continue;
                }
                $util = new DOMUtility($dom);
                $node = $util->node('//ns:ReplyCode');
                if (!$node) {
                    throw new GenericException('Reply code not found');
                }
                $code = trim($node->nodeValue);
                if ($code === '000') {
                    if (
                        $this->OutgoingTransfers->transition(
                            $outgoingTransfer,
                            OutgoingTransfersTable::T_ACCEPT
                        )
                    ) {
                        $this->OutgoingTransfers->save($outgoingTransfer);
                    }
                } else {
                    if (
                        $this->OutgoingTransfers->transition(
                            $outgoingTransfer,
                            OutgoingTransfersTable::T_REFUSE
                        )
                    ) {
                        $this->OutgoingTransfers->save($outgoingTransfer);
                    }
                }
            }

            $query = $this->OutgoingTransfers->find(
                'list',
                keyField: 'state',
                valueField: 'state_count'
            );
            $states = $query->select(
                [
                    'state',
                    'state_count' => $query->func()->count('*'),
                ]
            )
                ->where(
                    ['outgoing_transfer_request_id' => $outgoingTransferRequest->id]
                )
                ->groupBy(['state'])
                ->disableHydration()
                ->toArray();

            $allAccepted = count($states) === 1 && isset($states['accepted']);
            $allRejected = count($states) === 1 && isset($states['rejected']);
            $mixed = count(
                $states
            ) === 2 && isset($states['accepted']) && isset($states['rejected']);
            if ($allRejected || !$states) {
                if (
                    $this->OutgoingTransferRequests->transition(
                        $outgoingTransferRequest,
                        OutgoingTransferRequestsTable::T_REJECT_ALL_TRANSFERS
                    )
                ) {
                    $this->OutgoingTransferRequests->save(
                        $outgoingTransferRequest
                    );
                }
            } elseif ($allAccepted || $mixed) {
                if (
                    $this->OutgoingTransferRequests->transition(
                        $outgoingTransferRequest,
                        OutgoingTransferRequestsTable::T_PROCESS_TRANSFERS
                    )
                ) {
                    $this->OutgoingTransferRequests->save(
                        $outgoingTransferRequest
                    );
                }
            }

            $conn->commit();
        }

        return $output;
    }

    /**
     * Vérifi le code HTTP et la validité SEDA
     * @param Client\Response $response
     * @param EntityInterface $outgoingTransfer
     * @return DOMDocument|string
     */
    private function checkResponse(
        Client\Response $response,
        EntityInterface $outgoingTransfer
    ) {
        $append = ' <error>Failed</error>';
        if ($response->getStatusCode() < 400) {
            $append = ' <warning>Skip</warning>';
        }
        if ($response->getStatusCode() >= 400) {
            $this->out(
                __(
                    "Erreur code HTTP {0} sur le transfert {1}: {2}",
                    $response->getStatusCode(),
                    $outgoingTransfer->get('identifier'),
                    $response->getStringBody()
                )
                . $append
            );
            return 'warning';
        }
        $dom = new DOMDocument();
        if (!$dom->loadXML($response->getStringBody())) {
            $this->o(
                false,
                __(
                    "La réponse pour le transfert {0} n'est pas un XML valide",
                    $response->getStatusCode(),
                    $outgoingTransfer->get('identifier')
                )
            );
            return 'warning';
        }
        if ($response->getStatusCode() !== 200) {
            $this->out(
                __(
                    "Réponse du SAE : ''{0}'' sur le transfert {1}",
                    $dom->documentElement->nodeValue,
                    $outgoingTransfer->get('identifier')
                )
                . $append
            );
            return 'warning';
        }
        if (!SedaValidator::validate($dom)) {
            $this->o(
                false,
                __(
                    "La réponse pour le transfert {0} n'est pas un SEDA valide",
                    $response->getStatusCode(),
                    $outgoingTransfer->get('identifier')
                )
            );
            return 'warning';
        }
        return $dom;
    }
}
