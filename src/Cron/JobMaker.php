<?php

/**
 * Asalae\Cron\JobMaker
 */

namespace Asalae\Cron;

use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\BatchTreatmentsTable;
use Asalae\Model\Table\BeanstalkJobsTable;
use Asalae\Model\Table\DeliveryRequestsTable;
use Asalae\Model\Table\DestructionRequestsTable;
use Asalae\Model\Table\OutgoingTransferRequestsTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Table\ServiceLevelsTable;
use Asalae\Model\Table\TransfersTable;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Console\ConsoleOutput;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Factory\Utility;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInvalidTimeZoneException;
use Exception;
use ZMQSocketException;

/**
 * Crée des jobs pour résoudre les anomalies
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobMaker implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;

    /**
     * @var Beanstalk
     */
    private $Beanstalk;

    /**
     * @var TransfersTable
     */
    private $Transfers;
    /**
     * @var ServiceLevelsTable
     */
    private $ServiceLevels;
    /**
     * @var BeanstalkJobsTable
     */
    private $BeanstalkJobs;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params paramètres additionnels du
     *                                       cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     * @throws Exception
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
        $this->out = $out ?: new ConsoleOutput('php://stdout');
        $this->err = $err ?: new ConsoleOutput('php://stderr');
        $this->out->setOutputAs(ConsoleOutput::COLOR);
        $this->err->setOutputAs(ConsoleOutput::COLOR);
        $this->Beanstalk = Utility::get('Beanstalk');
        $loc = TableRegistry::getTableLocator();
        $this->Transfers = $loc->get('Transfers');
        $this->ServiceLevels = $loc->get('ServiceLevels');
        $this->BeanstalkJobs = $loc->get('BeanstalkJobs');
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        if (!$this->Beanstalk->isConnected()) {
            $this->out(__("Le serveur Beanstalkd ne fonctionne pas"));
            return 'error';
        }

        $success = $this->checkTransfers();
        $success = $this->checkArchiveUnits() && $success;
        $success = $this->checkArchiveBinaries() && $success;
        $success = $this->checkArchiveManagements() && $success;
        $success = $this->checkBatchTreatments() && $success;
        $success = $this->checkConversions() && $success;
        $success = $this->checkDeliveries() && $success;
        $success = $this->checkDestruction() && $success;
        $success = $this->checkOutgoingTransfers() && $success;
        $success = $this->checkRestitutions() && $success;
        if ($success) {
            $this->out(__("Aucun job n'a besoin d'être lancé"));
        }

        return $success ? 'success' : 'warning';
    }

    /**
     * Vérifi si il y a des transfers bloqués
     * workers :
     * - AnalyseWorker.php
     * - ArchiveWorker.php
     * - ControlWorker.php
     * - TimestampWorker.php
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkTransfers(): bool
    {
        $timestamping = $this->checkTimestamping();
        $analysing = $this->checkAnalysing();
        $controlling = $this->checkControlling();
        $archiving = $this->checkArchiving();
        return $timestamping && $analysing && $controlling && $archiving;
    }

    /**
     * Vérifi les transfers en état timestamping
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkTimestamping(): bool
    {
        $query = $this->Transfers->find()
            ->where(
                [
                    'state' => TransfersTable::S_TIMESTAMPING,
                ]
            )
            ->contain(
                [
                    'ServiceLevels' => ['TsMsgs', 'TsPjss', 'TsConvs'],
                ]
            )
            ->orderBy(['Transfers.id']);
        $jobs = 0;
        /** @var EntityInterface $transfer */
        foreach ($query as $transfer) {
            $serviceLevel = $transfer->get('service_level');
            if (!$serviceLevel) {
                $serviceLevel = $this->ServiceLevels->find()
                    ->where(
                        [
                            'ServiceLevels.org_entity_id' => $transfer->get(
                                'archival_agency_id'
                            ),
                            'ServiceLevels.default_level' => true,
                        ]
                    )
                    ->contain(['TsMsgs', 'TsPjss', 'TsConvs'])
                    ->first();
            }

            if (
                $serviceLevel
                && ($serviceLevel->get('ts_msg_id')
                || $serviceLevel->get('ts_pjs_id'))
            ) {
                $job = [
                    'transfer_id' => $transfer->get('id'),
                    'user_id' => $transfer->get('created_user_id'),
                    'ts_msg_id' => Hash::get(
                        $serviceLevel,
                        'ts_msg.timestamper_id'
                    ),
                    'ts_pjs_id' => Hash::get(
                        $serviceLevel,
                        'ts_pjs.timestamper_id'
                    ),
                    'ts_conv_id' => Hash::get(
                        $serviceLevel,
                        'ts_conv.timestamper_id'
                    ),
                ];
                if ($this->emit('timestamping', $job, 'Transfers', $transfer->get('id'))) {
                    $jobs++;
                }
            } else {
                // état timestamping sans niveau de service ou niveau de service sans horodatage
                $this->out(
                    __(
                        "Le transfert ''{0}'' (id={1}) a un état qui ne lui correspond pas",
                        $transfer->get('transfer_identifier'),
                        $transfer->get('id')
                    )
                );
            }
        }
        if ($jobs > 0) {
            $this->out(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    $jobs,
                    $jobs,
                    'timestamp'
                )
            );
            return false;
        }
        return true;
    }

    /**
     * Envoi un job, en prenant soin de ne pas créer un doublon
     * @param string $tube
     * @param array  $data
     * @param string $model
     * @param int    $fk
     * @return bool
     * @throws ZMQSocketException
     * @throws DateInvalidTimeZoneException
     */
    private function emit(string $tube, array $data, string $model, int $fk): bool
    {
        $job = $this->BeanstalkJobs
            ->find()
            ->where(
                [
                    'tube' => $tube,
                    'object_model' => $model,
                    'object_foreign_key' => $fk,
                ]
            )
            ->first();
        // si le job existe, on le relance ou on ne fait rien
        if ($job) {
            if ($job->get('job_state') === BeanstalkJobsTable::S_FAILED) {
                $this->out(__("Job {0} relancé pour {1}:{2}", $tube, $model, $fk));
                $this->BeanstalkJobs->transitionOrFail($job, BeanstalkJobsTable::T_RETRY);
                return (bool)$this->BeanstalkJobs->save($job);
            }
        } else {
            $this->out(__("Job {0} créé pour {1}:{2}", $tube, $model, $fk));
            $this->Beanstalk->setTube($tube)->emit($data);
            return true;
        }
        return false;
    }

    /**
     * Vérifi les transfers en état analysing
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkAnalysing()
    {
        return $this->genericCheckTransfer(
            TransfersTable::S_ANALYSING,
            'analyse'
        );
    }

    /**
     * Code commun de vérification d'un transfert
     * @param string $state
     * @param string $tube
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function genericCheckTransfer(string $state, string $tube): bool
    {
        $query = $this->Transfers->find()
            ->where(
                [
                    'state' => $state,
                ]
            )
            ->contain(['Archives'])
            ->orderBy(['Transfers.id']);
        $jobs = 0;
        /** @var EntityInterface $transfer */
        foreach ($query as $transfer) {
            if (count($transfer->get('archives') ?: []) > 0) {
                continue; // transfert composite en état archiving
            }
            $job = [
                'transfer_id' => $transfer->get('id'),
                'user_id' => $transfer->get('created_user_id'),
            ];
            if ($this->emit($tube, $job, 'Transfers', $transfer->get('id'))) {
                $jobs++;
            }
        }
        if ($jobs > 0) {
            $this->out(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    $jobs,
                    $jobs,
                    $tube
                )
            );
            return false;
        }
        return true;
    }

    /**
     * Vérifi les transfers en état controlling
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkControlling()
    {
        return $this->genericCheckTransfer(
            TransfersTable::S_CONTROLLING,
            'control'
        );
    }

    /**
     * Vérifi les transfers en état archiving
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkArchiving()
    {
        return $this->genericCheckTransfer(
            TransfersTable::S_ARCHIVING,
            'archive'
        );
    }

    /**
     * Vérification des ArchiveUnits
     * Workers :
     * - ArchiveUnitWorker.php
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkArchiveUnits(): bool
    {
        return $this->genericCheckArchive(
            [
                ArchivesTable::S_CREATING,
                ArchivesTable::S_UPDATE_CREATING,
            ],
            'archive-unit'
        );
    }

    /**
     * Code commun de vérification d'un transfert
     * @param array  $states
     * @param string $tube
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function genericCheckArchive(array $states, string $tube): bool
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $query = $Archives->find()
            ->where(
                [
                    'Archives.state IN' => $states,
                ]
            )
            ->contain(
                [
                    'Transfers' => function (Query $q) {
                        return $q->where(['Transfers.state' => TransfersTable::S_ARCHIVING]);
                    },
                ]
            )
            ->orderBy(['Archives.id']);
        $jobs = 0;
        /** @var EntityInterface $archive */
        foreach ($query as $archive) {
            if (!preg_match('/\d+/', $archive->get('xml_node_tagname'), $m)) {
                throw new Exception('xml path does not match');
            }
            $inCompositeState = in_array(
                $archive->get('state'),
                [
                    ArchivesTable::S_UPDATE_CREATING,
                    ArchivesTable::S_UPDATE_DESCRIBING,
                    ArchivesTable::S_UPDATE_MANAGING,
                    ArchivesTable::S_UPDATE_STORING,
                ]
            );
            /** @var EntityInterface $transfer */
            foreach (Hash::get($archive, 'transfers') as $transfer) {
                $job = [
                    'transfer_id' => $transfer->id,
                    'user_id' => $transfer->get('created_user_id'),
                    'archive_id' => $archive->get('id'),
                    'index' => (int)$m[0],
                ];
                if ($inCompositeState) {
                    $job['composite_id'] = $archive->id;
                }
                if ($this->emit($tube, $job, 'Transfers', $transfer->id)) {
                    $jobs++;
                }
            }
        }
        $success = true;
        if ($jobs > 0) {
            $this->out(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    $jobs,
                    $jobs,
                    $tube
                )
            );
            $success = false;
        }
        return $success;
    }

    /**
     * Vérification des ArchiveBinaries
     * Workers :
     * - ArchiveBinaryWorker.php
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkArchiveBinaries(): bool
    {
        return $this->genericCheckArchive(
            [
                ArchivesTable::S_DESCRIBING,
                ArchivesTable::S_UPDATE_DESCRIBING,
            ],
            'archive-binary'
        );
    }

    /**
     * Vérification des ArchiveManagements
     * Workers :
     * - ArchiveManagementWorker.php
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkArchiveManagements(): bool
    {
        return $this->genericCheckArchive(
            [
                ArchivesTable::S_STORING,
                ArchivesTable::S_UPDATE_STORING,
                ArchivesTable::S_MANAGING,
                ArchivesTable::S_UPDATE_MANAGING,
            ],
            'archive-management'
        );
    }

    /**
     * Vérifi si il y a des traitements bloqués
     * workers :
     * - BatchTreatmentWorker.php
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkBatchTreatments(): bool
    {
        $tube = 'batch-treatment';
        $loc = TableRegistry::getTableLocator();
        $BatchTreatments = $loc->get('BatchTreatments');
        $query = $BatchTreatments->find()
            ->where(
                [
                    'BatchTreatments.state IN' => [
                        BatchTreatmentsTable::S_ABORTED,
                        BatchTreatmentsTable::S_PROCESSING,
                        BatchTreatmentsTable::S_READY,
                    ],
                ]
            )
            ->orderBy(['BatchTreatments.id']);
        $jobs = 0;
        /** @var EntityInterface $batchTreatment */
        foreach ($query as $batchTreatment) {
            $job = [
                'batch_treatment_id' => $batchTreatment->id,
            ];
            if ($this->emit($tube, $job, 'BatchTreatments', $batchTreatment->id)) {
                $jobs++;
            }
        }
        $success = true;
        if ($jobs > 0) {
            $this->out(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    $jobs,
                    $jobs,
                    $tube
                )
            );
            $success = false;
        }
        return $success;
    }

    /**
     * Vérifi si il y a des conversions bloqués
     * workers :
     * - ConversionWorker.php
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkConversions(): bool
    {
        $tube = 'conversion';

        // on se contente de relancer les jobs en échec
        $query = $this->BeanstalkJobs->find()
            ->where(
                [
                    'tube' => $tube,
                    'job_state' => BeanstalkJobsTable::S_FAILED,
                ]
            );
        $jobs = 0;
        /** @var EntityInterface $beanstalkJob */
        foreach ($query as $beanstalkJob) {
            $job = [
                'batch_treatment_id' => $beanstalkJob->id,
            ];
            $emission = $this->emit(
                $tube,
                $job,
                $beanstalkJob->get('object_model'),
                $beanstalkJob->get('object_foreign_key')
            );
            if ($emission) {
                $jobs++;
            }
        }
        $success = true;
        if ($jobs > 0) {
            $this->out(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    $jobs,
                    $jobs,
                    $tube
                )
            );
            $success = false;
        }
        return $success;
    }

    /**
     * Vérifi si il y a des communications bloqués
     * workers :
     * - DeliveryWorker.php
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkDeliveries()
    {
        $tube = 'delivery';

        // on se contente de relancer les jobs en échec
        $loc = TableRegistry::getTableLocator();
        $DeliveryRequests = $loc->get('DeliveryRequests');
        $query = $DeliveryRequests->find()
            ->where(
                [
                    'state' => DeliveryRequestsTable::S_ACCEPTED,
                ]
            )
            ->orderBy(['DeliveryRequests.id']);
        $jobs = 0;
        /** @var EntityInterface $deliveryRequest */
        foreach ($query as $deliveryRequest) {
            $job = [
                'delivery_request_id' => $deliveryRequest->id,
            ];
            $emission = $this->emit(
                $tube,
                $job,
                'DeliveryRequests',
                $deliveryRequest->id
            );
            if ($emission) {
                $jobs++;
            }
        }
        $success = true;
        if ($jobs > 0) {
            $this->out(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    $jobs,
                    $jobs,
                    $tube
                )
            );
            $success = false;
        }
        return $success;
    }

    /**
     * Vérifi si il y a des éliminations bloqués
     * workers :
     * - DestructionWorker.php
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkDestruction()
    {
        $tube = 'destruction';

        // on se contente de relancer les jobs en échec
        $loc = TableRegistry::getTableLocator();
        $DestructionRequests = $loc->get('DestructionRequests');
        $query = $DestructionRequests->find()
            ->where(
                [
                    'state IN' => [
                        DestructionRequestsTable::S_ACCEPTED,
                        DestructionRequestsTable::S_FILES_DESTROYING,
                        DestructionRequestsTable::S_DESCRIPTION_DELETING,
                    ],
                ]
            )
            ->orderBy(['DestructionRequests.id']);
        $jobs = 0;
        /** @var EntityInterface $destructionRequest */
        foreach ($query as $destructionRequest) {
            $job = [
                'destruction_request_id' => $destructionRequest->id,
            ];
            $emission = $this->emit(
                $tube,
                $job,
                'DestructionRequests',
                $destructionRequest->id
            );
            if ($emission) {
                $jobs++;
            }
        }
        $success = true;
        if ($jobs > 0) {
            $this->out(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    $jobs,
                    $jobs,
                    $tube
                )
            );
            $success = false;
        }
        return $success;
    }

    /**
     * Vérifi si il y a des éliminations bloqués
     * workers :
     * - OutgoingTransferWorker.php
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkOutgoingTransfers()
    {
        $tube = 'outgoing-transfer';

        // on se contente de relancer les jobs en échec
        $loc = TableRegistry::getTableLocator();
        $OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
        $query = $OutgoingTransferRequests->find()
            ->where(
                [
                    'state IN' => OutgoingTransferRequestsTable::S_ACCEPTED,
                ]
            )
            ->orderBy(['OutgoingTransferRequests.id']);
        $jobs = 0;
        /** @var EntityInterface $outgoingTransferRequest */
        foreach ($query as $outgoingTransferRequest) {
            $job = [
                'outgoing_transfer_request_id' => $outgoingTransferRequest->id,
            ];
            $emission = $this->emit(
                $tube,
                $job,
                'OutgoingTransferRequests',
                $outgoingTransferRequest->id
            );
            if ($emission) {
                $jobs++;
            }
        }
        $success = true;
        if ($jobs > 0) {
            $this->out(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    $jobs,
                    $jobs,
                    $tube
                )
            );
            $success = false;
        }
        return $success;
    }

    /**
     * Vérifi si il y a des éliminations bloqués
     * workers :
     * - RestitutionBuildWorker.php
     * @return bool
     * @throws DateInvalidTimeZoneException
     * @throws ZMQSocketException
     */
    private function checkRestitutions()
    {
        $tube = 'restitution-build';

        // on se contente de relancer les jobs en échec
        $loc = TableRegistry::getTableLocator();
        $RestitutionRequests = $loc->get('RestitutionRequests');
        $query = $RestitutionRequests->find()
            ->where(
                [
                    'state IN' => RestitutionRequestsTable::S_ACCEPTED,
                ]
            )
            ->orderBy(['RestitutionRequests.id']);
        $jobs = 0;
        /** @var EntityInterface $outgoingTransferRequest */
        foreach ($query as $outgoingTransferRequest) {
            $job = [
                'restitution_request_id' => $outgoingTransferRequest->id,
            ];
            $emission = $this->emit(
                $tube,
                $job,
                'RestitutionRequests',
                $outgoingTransferRequest->id
            );
            if ($emission) {
                $jobs++;
            }
        }
        $success = true;
        if ($jobs > 0) {
            $this->out(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    $jobs,
                    $jobs,
                    $tube
                )
            );
            $success = false;
        }
        return $success;
    }
}
