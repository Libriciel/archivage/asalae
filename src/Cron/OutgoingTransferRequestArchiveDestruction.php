<?php

/**
 * Asalae\Cron\OutgoingTransferRequestArchiveDestruction
 */

namespace Asalae\Cron;

use Asalae\Model\Entity\StoredFile;
use Asalae\Model\Table\ArchiveBinariesArchiveUnitsTable;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveDescriptionsTable;
use Asalae\Model\Table\ArchiveIndicatorsTable;
use Asalae\Model\Table\ArchiveKeywordsTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsDeliveryRequestsTable;
use Asalae\Model\Table\ArchiveUnitsDestructionRequestsTable;
use Asalae\Model\Table\ArchiveUnitsOutgoingTransferRequestsTable;
use Asalae\Model\Table\ArchiveUnitsRestitutionRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\OutgoingTransferRequestsTable;
use Asalae\Model\Table\OutgoingTransfersTable;
use Asalae\Model\Table\PublicDescriptionArchiveFilesTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\ColoredCronTrait;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Datasource\EntityInterface;
use Cake\Log\Log;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInterval;
use DateInvalidTimeZoneException;
use DateTime;
use DOMException;
use Exception;

/**
 * Lecture des accusés de réception et des réponses aux
 * transferts des transferts sortants en cours
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property Table $RestitutionRequests
 */
class OutgoingTransferRequestArchiveDestruction implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;
    use ColoredCronTrait;

    /**
     * @var ArchiveBinariesTable
     */
    private $ArchiveBinaries;
    /**
     * @var ArchiveBinariesArchiveUnitsTable
     */
    private $ArchiveBinariesArchiveUnits;
    /**
     * @var ArchiveDescriptionsTable
     */
    private $ArchiveDescriptions;
    /**
     * @var ArchiveIndicatorsTable
     */
    private $ArchiveIndicators;
    /**
     * @var ArchiveKeywordsTable
     */
    private $ArchiveKeywords;
    /**
     * @var ArchiveUnitsTable
     */
    private $ArchiveUnits;
    /**
     * @var ArchiveUnitsDeliveryRequestsTable
     */
    private $ArchiveUnitsDeliveryRequests;
    /**
     * @var ArchiveUnitsDestructionRequestsTable
     */
    private $ArchiveUnitsDestructionRequests;
    /**
     * @var ArchiveUnitsOutgoingTransferRequestsTable
     */
    private $ArchiveUnitsOutgoingTransferRequests;
    /**
     * @var ArchiveUnitsRestitutionRequestsTable
     */
    private $ArchiveUnitsRestitutionRequests;
    /**
     * @var ArchivesTable
     */
    private $Archives;
    /**
     * @var EventLogsTable
     */
    private $EventLogs;
    /**
     * @var OrgEntitiesTable
     */
    private $OrgEntities;
    /**
     * @var OutgoingTransferRequestsTable
     */
    private $OutgoingTransferRequests;
    /**
     * @var OutgoingTransfersTable
     */
    private $OutgoingTransfers;
    /**
     * @var PublicDescriptionArchiveFilesTable
     */
    private $PublicDescriptionArchiveFiles;
    /**
     * @var RestitutionRequestsTable
     */
    private $RestitutionRequests;
    /**
     * @var EntityInterface
     */
    private $cron;
    /**
     * @var VolumeManager[]
     */
    private $dataSpaces = [];

    /**
     * Constructeur de la classe du cron
     * @param array                  $params paramètres
     *                                       additionnels
     *                                       du cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     * @throws Exception
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
        $loc = TableRegistry::getTableLocator();
        $this->ArchiveBinaries = $loc->get('ArchiveBinaries');
        $this->ArchiveBinariesArchiveUnits = $loc->get(
            'ArchiveBinariesArchiveUnits'
        );
        $this->ArchiveDescriptions = $loc->get('ArchiveDescriptions');
        $this->ArchiveIndicators = $loc->get('ArchiveIndicators');
        $this->ArchiveKeywords = $loc->get('ArchiveKeywords');
        $this->ArchiveUnits = $loc->get('ArchiveUnits');
        $this->ArchiveUnitsDeliveryRequests = $loc->get('ArchiveUnitsDeliveryRequests');
        $this->ArchiveUnitsDestructionRequests = $loc->get('ArchiveUnitsDestructionRequests');
        $this->ArchiveUnitsOutgoingTransferRequests = $loc->get('ArchiveUnitsOutgoingTransferRequests');
        $this->ArchiveUnitsRestitutionRequests = $loc->get('ArchiveUnitsRestitutionRequests');
        $this->Archives = $loc->get('Archives');
        $this->EventLogs = $loc->get('EventLogs');
        $this->OrgEntities = $loc->get('OrgEntities');
        $this->OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
        $this->OutgoingTransfers = $loc->get('OutgoingTransfers');
        $this->PublicDescriptionArchiveFiles = $loc->get('PublicDescriptionArchiveFiles');
        $this->RestitutionRequests = $loc->get('RestitutionRequests');
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        $this->cron = $cron;
        $this->updateOutgoingTransferRequestsStates();

        $outgoingTransferRequests = $this->OutgoingTransferRequests->find()
            ->where(
                [
                    'OutgoingTransferRequests.state IN' => [
                        OutgoingTransferRequestsTable::S_ARCHIVE_DESTRUCTION_SCHEDULED,
                        OutgoingTransferRequestsTable::S_ARCHIVE_FILES_DESTROYING,
                        OutgoingTransferRequestsTable::S_ARCHIVE_DESCRIPTION_DELETING,
                    ],
                ]
            );

        /** @var EntityInterface $outgoingTransferRequest */
        foreach ($outgoingTransferRequests as $outgoingTransferRequest) {
            try {
                $this->destructOutgoingTransferRequestFiles($outgoingTransferRequest);
                $this->deleteArchiveUnits($outgoingTransferRequest);

                $this->OutgoingTransferRequests->transition(
                    $outgoingTransferRequest,
                    OutgoingTransferRequestsTable::T_TERMINATE
                );
                $this->OutgoingTransferRequests->saveOrFail(
                    $outgoingTransferRequest
                );
            } catch (Exception $e) {
                $this->err(
                    __("Echec lors de l'élimination du transfert sortant")
                );
                $this->err($e->getMessage());
                Log::error((string)$e);
                return 'failed';
            }
        }

        return 'success';
    }


    /**
     * Met à jour le status de la demande de Transfert sortant par rapport à la config
     * du service d'archives (destuction après x jours)
     * @throws Exception
     */
    private function updateOutgoingTransferRequestsStates()
    {
        $sas = $this->OrgEntities->find()
            ->select(
                [
                    'OrgEntities.id',
                    'period' => 'Configurations.setting',
                ]
            )
            ->innerJoinWith('TypeEntities')
            ->leftJoinWith('Configurations')
            ->where(
                [
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES,
                    'OR' => [
                        'Configurations.id IS' => null,
                        'Configurations.name IS' => ConfigurationsTable::CONF_OTR_DESTR_DELAY,
                    ],
                ]
            );
        /** @var EntityInterface $sa */
        foreach ($sas as $sa) {
            $period = new DateTime();
            $period->sub(
                new DateInterval(
                    sprintf('P%dD', $sa->get('period') ?: 15)
                )
            );

            $count = $this->OutgoingTransferRequests->transitionAll(
                OutgoingTransferRequestsTable::T_SCHEDULE_ARCHIVE_DESTRUCTION,
                [
                    'state' => OutgoingTransferRequestsTable::S_TRANSFERS_PROCESSED,
                    'last_state_update <=' => $period,
                    'archival_agency_id' => $sa->id,
                ]
            );
            if ($count) {
                $this->out(
                    __n(
                        "Changement d'état pour {0} archive du service d'archives n°{1}",
                        "Changement d'état pour {0} archives du service d'archives n°{1}",
                        $count,
                        $count,
                        $sa->id
                    )
                );
            }
        }
    }

    /**
     * Elimination des fichiers des archives liés à un transfert sortant
     * @param EntityInterface $outgoingTransferRequest
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    private function destructOutgoingTransferRequestFiles(
        EntityInterface $outgoingTransferRequest
    ) {
        $this->out(
            __(
                "Elimination des fichiers liés à la demande de transfert sortant n°{0}",
                $outgoingTransferRequest->id
            )
        );

        $this->OutgoingTransferRequests->transition(
            $outgoingTransferRequest,
            OutgoingTransferRequestsTable::T_DESTROY_ARCHIVE_FILES
        );
        $this->OutgoingTransferRequests->saveOrFail($outgoingTransferRequest);

        $archiveIdsSubquery = $this->OutgoingTransfers->find()
            ->select(['ArchiveUnits.archive_id'])
            ->innerJoinWith('ArchiveUnits')
            ->where(
                [
                    'OutgoingTransfers.outgoing_transfer_request_id' => $outgoingTransferRequest->id,
                    'OutgoingTransfers.state' => OutgoingTransfersTable::S_ACCEPTED,
                ]
            );

        $archives = $this->Archives->find()
            ->where(['Archives.id IN' => $archiveIdsSubquery])
            ->contain(['PublicDescriptionArchiveFiles' => ['StoredFiles']]);

        $conn = $this->ArchiveUnits->getConnection();

        /** @var EntityInterface $archive */
        foreach ($archives as $archive) {
            $manager = $this->getVolumeManager($archive);
            $binaries = $this->ArchiveBinaries->find()
                ->innerJoinWith('ArchiveUnits')
                ->where(['ArchiveUnits.archive_id' => $archive->id])
                ->contain(['StoredFiles']);
            /** @var EntityInterface $binary */
            foreach ($binaries as $binary) {
                $conn->begin();
                $this->ArchiveBinaries->updateAll(
                    ['stored_file_id' => null],
                    ['id' => $binary->id]
                );
                /** @var StoredFile $storedFile */
                $storedFile = $binary->get('stored_file');
                if (
                    $storedFile && $manager->fileExists(
                        $storedFile->get('name')
                    )
                ) {
                    $manager->fileDelete($storedFile->get('name'));
                }
                $conn->commit();
            }

            /** @var EntityInterface $desc */
            $desc = $archive->get('public_description_archive_file');
            if ($desc) {
                $this->PublicDescriptionArchiveFiles->deleteOrFail($desc);
                $manager->fileDelete(Hash::get($desc, 'stored_file.name'));
            }

            $this->Archives->patchEntity(
                $archive,
                [
                    'original_size' => 0,
                    'original_count' => 0,
                    'timestamp_size' => 0,
                    'timestamp_count' => 0,
                    'preservation_size' => 0,
                    'preservation_count' => 0,
                    'dissemination_size' => 0,
                    'dissemination_count' => 0,
                    'units_count' => 0,
                    'next_pubdesc' => null,
                ]
            );
            $this->Archives->transition($archive, ArchivesTable::T_TRANSFER);
            $this->Archives->saveOrFail($archive);
        }
    }

    /**
     * Donne le VolumeManager lié à une archive
     * @param EntityInterface $archive
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(EntityInterface $archive): VolumeManager
    {
        if (!isset($this->dataSpaces[$archive->get('secure_data_space_id')])) {
            $this->dataSpaces[$archive->get('secure_data_space_id')]
                = new VolumeManager($archive->get('secure_data_space_id'));
        }
        return $this->dataSpaces[$archive->get('secure_data_space_id')];
    }

    /**
     * Supprime les unités d'archives liés à une demande de transfert sortant avec
     * toutes les jointures vers cette unité d'archives
     * @param EntityInterface $outgoingTransferRequest
     * @throws DOMException
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    private function deleteArchiveUnits(EntityInterface $outgoingTransferRequest)
    {
        $this->out(__("Suppression des unités d'archives en BDD"));

        $this->OutgoingTransferRequests->transition(
            $outgoingTransferRequest,
            OutgoingTransferRequestsTable::T_DELETE_ARCHIVE_DESCRIPTION
        );
        $this->OutgoingTransferRequests->saveOrFail($outgoingTransferRequest);

        $rootArchiveUnits = $this->ArchiveUnits->find()
            ->innerJoinWith('ArchiveUnitsOutgoingTransferRequests')
            ->innerJoinWith('OutgoingTransfers')
            ->where(
                [
                    'ArchiveUnitsOutgoingTransferRequests.otr_id' => $outgoingTransferRequest->id,
                    'OutgoingTransfers.state' => OutgoingTransfersTable::S_ACCEPTED,
                ]
            )
            ->contain(
                [
                    'Archives' => [
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                    ],
                ]
            );
        $conn = $this->ArchiveUnits->getConnection();
        /** @var EntityInterface $rootArchiveUnit */
        foreach ($rootArchiveUnits as $rootArchiveUnit) {
            /** @var EntityInterface $archive */
            $archive = $rootArchiveUnit->get('archive');
            $oldCounters = $archive->toArray();
            $conn->begin();
            $this->ArchiveUnits->cascadeDelete($rootArchiveUnit);
            $this->saveIndicators(
                $this->Archives->get($archive->id),
                $oldCounters
            );
            $conn->commit();
            $this->logEvent($archive, $outgoingTransferRequest);
        }
    }

    /**
     * Ajoute les indicateurs de conversions
     * @param EntityInterface $archive
     * @param array           $oldCounters
     */
    private function saveIndicators(
        EntityInterface $archive,
        array $oldCounters
    ) {
        $loc = TableRegistry::getTableLocator();
        $ArchiveIndicators = $loc->get('ArchiveIndicators');

        $entity = $ArchiveIndicators->newEntity(
            [
                'archive_date' => new DateTime(),
                'archival_agency_id' => $archive->get('archival_agency_id'),
                'transferring_agency_id' => $archive->get('transferring_agency_id'),
                'originating_agency_id' => $archive->get('originating_agency_id'),
                'archive_count' => -1,
                'units_count' => 0 - $oldCounters['units_count'],
                'original_count' => 0 - $oldCounters['original_count'],
                'original_size' => 0 - $oldCounters['original_size'],
                'timestamp_count' => 0 - $oldCounters['timestamp_count'],
                'timestamp_size' => 0 - $oldCounters['timestamp_size'],
                'preservation_count' => 0 - $oldCounters['preservation_count'],
                'preservation_size' => 0 - $oldCounters['preservation_size'],
                'dissemination_count' => 0 - $oldCounters['dissemination_count'],
                'dissemination_size' => 0 - $oldCounters['dissemination_size'],
                'agreement_id' => $archive->get('agreement_id'),
                'profile_id' => $archive->get('profile_id'),
            ]
        );
        $ArchiveIndicators->saveOrFail($entity);
    }

    /**
     * Mise à jour du journal des evenements et du cycle de vie de l'archive
     * @param EntityInterface $archive
     * @param EntityInterface $outgoingTransferRequest
     * @throws VolumeException
     * @throws DOMException
     */
    private function logEvent(
        EntityInterface $archive,
        EntityInterface $outgoingTransferRequest
    ) {
        $entry = $this->EventLogs->newEntry(
            'outgoing_transfer_destroy',
            'success',
            __(
                "Destruction de l'archive {0} suite à la demande de transfert sortant {1}",
                $archive->get('archival_agency_identifier'),
                $outgoingTransferRequest->get('identifier')
            ),
            $this->cron,
            $archive
        );
        $this->EventLogs->saveOrFail($entry);
        $storedFile = Hash::get(
            $archive,
            'lifecycle_xml_archive_file.stored_file',
            []
        );
        $this->Archives->updateLifecycleXml($storedFile, $entry);
    }
}
