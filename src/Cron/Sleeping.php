<?php

/**
 * Asalae\Cron\Sleeping
 */

namespace Asalae\Cron;

use AsalaeCore\Cron\CronInterface;
use Cake\Console\ConsoleOutput;
use Cake\Datasource\EntityInterface;
use DateInterval;
use DateTime;
use Exception;

/**
 * Cron de test (à ajouter manuellement en base)
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Sleeping implements CronInterface
{
    /**
     * @var int durée en seconde de sleep
     */
    public $sleepTime;

    /**
     * @var bool attente en mode busy
     */
    public $busyWait;

    /**
     * Constructeur de la classe du cron
     * @param array              $params paramètres additionnels
     *                                   du cron
     * @param ConsoleOutput|null $out
     * @param ConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->sleepTime = $params['sleepTime'] ?? 36000; // sleep 10h
        $this->busyWait = $params['busyWait'] ?? false; // sleep 10h
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        if ($this->busyWait) {
            $begin = new DateTime();
            $end = $begin->add(new DateInterval('PT' . $this->sleepTime . 'S'));
            do {
                $now = new DateTime();
            } while ($now < $end);
        } else {
            exec('sleep ' . $this->sleepTime); // fainte sonarqube
        }
        return 'success';
    }

    /**
     * Permet d'obtenir les messages de sorti
     * @return string
     */
    public function getOutput(): string
    {
        return "sleeping";
    }

    /**
     * Outputs a single or multiple error messages to stderr. If no parameters
     * are passed outputs just a newline.
     * @param string|array|null $message  A string or an array of strings to output
     * @param int               $newlines Number of newlines to append
     * @return false
     */
    public function err($message = null, int $newlines = 1)
    {
        return false;
    }
}
