<?php

/**
 * Asalae\Cron\AutoTreeRepair
 */

namespace Asalae\Cron;

use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\ColoredCronTrait;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\DatabaseUtility;
use AsalaeCore\Utility\Exec;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\TableRegistry;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use PDO;

/**
 * Corrige les problèmes de tree automatiquement
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2024, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AutoTreeRepair implements CronInterface
{
    use ConsoleLogTrait;
    use ColoredCronTrait;

    public const int DEFAULT_ASYNC_MAX_WAIT = 60;

    /**
     * @var array liste des models
     */
    private array $tables;
    /**
     * @var bool
     */
    private bool $failed = false;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params Paramètres additionnels du cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->tables = $params['tables'] ?? [];
        $this->initializeOutput($out, $err);
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        /** @var TableLocator $loc */
        $loc = TableRegistry::getTableLocator();
        $paths = App::classPath('Model/Table');
        $models = glob($paths[0] . '*Table.php');
        $trees = [];
        foreach ($models as $file) {
            $name = basename($file, 'Table.php');
            $model = $loc->get($name);
            if ($model->hasBehavior('Tree')) {
                $alias = $model->getAlias();
                $trees[$alias] = $alias;
            }
        }
        return [
            'tables' => [
                'label' => __("Tables à vérifier/corriger automatiquement"),
                'type' => 'select',
                'multiple' => true,
                'options' => $trees,
            ],
        ];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        if (!$this->checkRunning()) {
            return 'warning';
        }
        $loc = TableRegistry::getTableLocator();
        $conn = DatabaseUtility::getPdo();
        foreach ($this->tables as $table) {
            $model = $loc->get($table);
            $tree = $model->getBehavior('Tree');
            $table = $model->getTable();
            $lft = $tree->getConfig('left');
            $rght = $tree->getConfig('right');
            $count = $model->find()->count();
            if ($count === 0) {
                $this->o(true, $table);
                continue;
            }
            $stmt = $conn->prepare("SELECT $lft, $rght FROM $table ORDER BY $lft");
            $stmt->execute();

            $lftsRghts = [];
            while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                if (isset($lftsRghts[$data[$lft]])) {
                    $this->failure($table, __("lft en double ({0})", $data[$lft]));
                    continue 2;
                }
                if (isset($lftsRghts[$data[$rght]])) {
                    $this->failure($table, __("rght en double ({0})", $data[$rght]));
                    continue 2;
                }
                if ($data[$lft] >= $data[$rght]) {
                    $this->failure($table, __("lft / rght inversés, ({0}, {1})", $data[$lft], $data[$rght]));
                    continue 2;
                }
                $lftsRghts[$data[$lft]] = true;
                $lftsRghts[$data[$rght]] = true;
            }

            $count = count($lftsRghts) / 2;
            $nums = array_keys($lftsRghts);
            $min = min($nums);
            $expectedMin = 1;
            $max = max($nums);
            $expectedMax = $count * 2;
            $sum = array_reduce(
                $nums,
                function ($a, $b) {
                    return $a + $b;
                }
            );
            $expectedSum = $count * ($count * 2 + 1);
            if ($min !== $expectedMin) {
                $this->failure($table, __("min(lft) n'est pas égal à 1"));
                continue;
            }
            if ($max !== $expectedMax) {
                $this->failure($table, __("max(rght) n'est pas égal à {0}", $expectedMax));
                continue;
            }
            if ($sum !== $expectedSum) {
                $this->failure(
                    $table,
                    __(
                        "somme des lft / rght ne correspond pas au chiffre attendu ({0})",
                        $expectedSum
                    )
                );
            }
        }
        if ($this->failed) {
            $beginTime = microtime(true);
            $waitLimit = $beginTime + Configure::read('AutoTreeRepair.async_max_wait', self::DEFAULT_ASYNC_MAX_WAIT);
            do {
                sleep(1);
            } while (Exec::runningAsync(0) && microtime(true) < $waitLimit);
            if (Exec::runningAsync(0)) {
                $this->out(
                    __(
                        "Une réparation est toujours en cours avec le PID {0} et continuera en arrière-plan.",
                        current(Exec::$pids)
                    )
                );
            } else {
                Filesystem::remove(TMP . 'auto_tree_repair');
            }
        }
        return $this->failed ? 'warning' : 'success';
    }

    /**
     * @param string $table
     * @param string $message
     * @return void
     * @throws Exception
     */
    private function failure(string $table, string $message): void
    {
        $this->failed = true;
        $this->o(false, sprintf("%s: %s", $table, $message));
        /** @var Exec $Exec */
        $Exec = Utility::get('Exec');
        $Exec->defaultStdout = LOGS . 'auto_repair_table.log';
        $pid = $Exec->async(CAKE_SHELL, 'tree', 'repair', $table);
        $this->out(__("Réparation de la table lancée avec le pid={0}", $pid));
        file_put_contents(TMP . 'auto_tree_repair/' . $table . '.lock', $pid);
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function checkRunning(): bool
    {
        $dir = TMP . 'auto_tree_repair';
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
            return true;
        }
        /** @var Exec $Exec */
        $Exec = Utility::get('Exec');
        $files = glob($dir . '/*.lock');
        foreach ($files as $file) {
            $name = basename($file, '.lock');
            $pid = (int)file_get_contents($file);
            $ps = $Exec->rawCommand(sprintf('ps -p %d | grep php', $pid)) ?: '';
            $running = preg_match('/^\s*(\d+)/', $ps, $m)
                && (int)$m[1] === $pid;
            if ($running) {
                $this->o(false, __("Un repair semble être déjà en cours (PID={0}) table: ", $pid, $name));
                return false;
            } else {
                unlink($file);
            }
        }
        return true;
    }
}
