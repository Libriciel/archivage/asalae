<?php

/**
 * Asalae\Cron\Check
 */

namespace Asalae\Cron;

use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\ColoredCronTrait;
use AsalaeCore\Cron\CronInterface;
use Cake\Cache\Cache;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Datasource\EntityInterface;
use DateTime;
use Exception;

/**
 * Supprime le cache expiré
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CacheClear implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;
    use ColoredCronTrait;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params Paramètres additionnels du cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        $ltrim = strlen(CACHE);
        foreach (Cache::configured() as $cache) {
            $config = Cache::getConfig($cache);
            if (
                empty($config['duration'])
                || empty($config['path'])
                || !is_dir($config['path'])
            ) {
                continue;
            }
            $now = new DateTime();
            $interval = $now->diff(new DateTime($config['duration']));
            $limit = $now->sub($interval);
            foreach (glob($config['path'] . '*') as $file) {
                $lastModified = new DateTime();
                $lastModified->setTimestamp(filemtime($file));
                if ($lastModified < $limit) {
                    $expiredStr = $lastModified->diff($limit);
                    $expiredStr = $expiredStr->d
                        ? __n(
                            "{0} jour",
                            "{0} jours",
                            $expiredStr->d,
                            $expiredStr->d
                        )
                        : (new DateTime('00:00:00'))
                            ->add($expiredStr)
                            ->format('H:i:s');
                    $this->out(
                        __(
                            "Cache expiré depuis {0} : {1}",
                            $expiredStr,
                            substr($file, $ltrim)
                        )
                    );
                    unlink($file);
                }
            }
        }
        return 'success';
    }
}
