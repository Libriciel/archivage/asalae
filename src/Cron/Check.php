<?php

/**
 * Asalae\Cron\Check
 */

namespace Asalae\Cron;

use Asalae\Controller\AdminsController;
use Asalae\Model\Table\BeanstalkJobsTable;
use Asalae\Utility\Check as CheckUtility;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\ColoredCronTrait;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Error\ErrorTrap;
use AsalaeCore\Factory\Utility;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Driver\Mysql;
use Cake\Database\Driver\Postgres;
use Cake\Database\Driver\Sqlite;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use Exception;
use ReflectionClass;
use stdClass;

/**
 * Vérifie l'application
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Check implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;
    use ColoredCronTrait;

    public const string PHP_MIN_VERSION = '7.0.0';
    public const string POSTGRES_MIN_VERSION = '8.4.0';
    public const string SQLITE_MIN_VERSION = '3.0.0';
    public const string MYSQL_MIN_VERSION = '5.7.0';
    public const int DISK_MIN_SPACE = 1000000000;

    /**
     * @var Exception state = error si != empty
     */
    private $fail;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params Paramètres additionnels du cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        $php = $this->checkPhp();
        $database = $this->checkDatabase();
        $softwares = $this->checkSoftwares();
        /** @var CheckUtility $CheckUtility */
        $CheckUtility = Utility::get(CheckUtility::class);
        $beanstalkd = $CheckUtility->beanstalkd();
        $this->o($beanstalkd, __("Serveur Beanstalkd"));
        $clamav = $CheckUtility->clamav();
        $ratchet = $CheckUtility->ratchet();
        $this->o($ratchet, __("Serveur Ratchet"));
        $this->o($clamav, __("Antivirus"));
        $writables = $CheckUtility->writableOk();
        $this->o($writables, __("Dossiers inscriptibles"));
        $timestampers = $this->checkTimestampers();
        $volumes = $this->checkVolumes();
        $jobs = $this->checkJobs();
        $diskSpace = $this->checkDiskSpace();
        $treeSanity = $this->checkTreeSanity();
        $checkCrons = $this->checkCrons();
        if ($this->fail) {
            return 'error';
        }
        $success = $php && $database && $softwares && $beanstalkd && $ratchet
            && $timestampers && $volumes && $clamav && $jobs
            && $diskSpace && $treeSanity && $checkCrons;
        return $success ? 'success' : 'warning';
    }

    /**
     * Vérifie php et ses modules
     * @return bool
     * @throws Exception
     */
    private function checkPhp(): bool
    {
        $phpVersion = version_compare(PHP_VERSION, self::PHP_MIN_VERSION, '>=');
        $this->o($phpVersion, __("Version php: {0}", PHP_VERSION));

        $composerJson = json_decode(
            file_get_contents(ROOT . DS . 'composer.json'),
            true
        );
        $requiredExts = array_filter(
            array_keys($composerJson['require']),
            function ($v) {
                return substr($v, 0, 4) === 'ext-';
            }
        );
        $modulesPhp = array_map(
            function ($v) {
                return substr($v, 4);
            },
            $requiredExts
        );
        sort($modulesPhp);
        $success = $phpVersion;
        /** @var CheckUtility $Check */
        $Check = Utility::get(CheckUtility::class);
        foreach ($Check->extPhp() as $module) {
            $s = extension_loaded($module);
            $success = $success && $s;
            $this->o($s, __("Extension {0}", $module));
        }
        foreach ($Check->php() as $parametragePhp => $ok) {
            $success = $success && $ok;
            $this->o($ok, __("Paramétrage de {0}", $parametragePhp));
        }

        return $success;
    }

    /**
     * Vérifi la connexion et la version minimale de la base de données
     * @return bool
     */
    private function checkDatabase(): bool
    {
        $configPath = include Configure::read('App.paths.path_to_local_config');
        $config = json_decode(file_get_contents($configPath));
        $success = true;
        foreach ($config->Datasources as $datasource => $params) {
            try {
                /** @var Connection $conn */
                $conn = ConnectionManager::get($datasource);
                $conn->execute('select 1');
                $version = 'unknown';
                switch ($params->driver ?? Postgres::class) {
                    case Postgres::class:
                        $version = current(
                            $conn->execute('select version()')->fetch()
                        );
                        $ok = $version
                            && preg_match(
                                '/\w+\s+([\d.]+)/',
                                $version,
                                $match
                            )
                            && version_compare(
                                $version = $match[1],
                                self::POSTGRES_MIN_VERSION,
                                '>='
                            );
                        $type = 'postgres';
                        break;
                    case Sqlite::class:
                        /** @noinspection PhpFullyQualifiedNameUsageInspection il ne faut pas importer SQLite3 */
                        $ok = class_exists('\SQLite3')
                            && ($version = \SQLite3::version())
                            && isset($version['versionString'])
                            && ($version = $version['versionString'])
                            && version_compare(
                                $version,
                                self::SQLITE_MIN_VERSION,
                                '>='
                            );
                        $type = 'sqlite';
                        break;
                    case Mysql::class:
                        $version = current(
                            $conn->execute('select version()')->fetch()
                        );
                        $ok = $version
                            && version_compare(
                                $version,
                                self::MYSQL_MIN_VERSION,
                                '>='
                            );
                        $type = 'mysql';
                        break;
                    default:
                        throw new Exception(
                            __("Driver non compatible: {0}", $params->driver)
                        );
                }
                $this->o(
                    $ok,
                    __(
                        "Datasource {0}, type {1}: {2}",
                        $datasource,
                        $type,
                        $version
                    )
                );
            } catch (Exception $e) {
                $this->err('<error>' . ((string)$e) . '</error>');
                $success = false;
                $ok = false;
                $this->fail = $e;
            }
            $success = $success && $ok;
        }
        $Phinxlogs = TableRegistry::getTableLocator()->get('Phinxlog');
        $migrations = glob(CONFIG . 'Migrations/*.php');
        $schema = !empty($migrations);
        foreach ($migrations as $path) {
            $filename = basename($path, '.php');
            [$version] = explode('_', $filename, 2);
            $conditions = ['version' => $version];
            if (!$Phinxlogs->exists($conditions)) {
                $success = false;
                $schema = false;
                break;
            }
        }
        $this->o($schema, __("Schema de base de donnée"));

        return $success;
    }

    /**
     * Vérifie que les logiciels nécessaire sont installés
     * @return bool
     * @throws Exception
     */
    private function checkSoftwares(): bool
    {
        /** @var CheckUtility $CheckUtility */
        $CheckUtility = Utility::get(CheckUtility::class);
        $success = true;
        $prerequis = array_keys(
            Hash::normalize(
                Configure::read('Check.prerequis', CheckUtility::$prerequis)
            )
        );
        $missings = $CheckUtility->missings();
        foreach ($prerequis as $key) {
            $success = $success && !in_array($key, $missings);
            $this->o(!in_array($key, $missings), __("Executable {0}", $key));
        }
        return $success;
    }

    /**
     * Effectue un (new $driver($url))->ping() sur un TimestampingInterface
     * @return bool
     */
    private function checkTimestampers(): bool
    {
        $success = true;
        $Timestampers = TableRegistry::getTableLocator()->get('Timestampers');
        foreach ($Timestampers->find() as $entity) {
            $data = json_decode($entity->get('fields'));
            $driver = $data->driver;
            $config = Configure::read('Timestamping.drivers.' . $driver);
            $args = $this->getDriverArgs($config['fields'], $data);
            $ok = false;
            try {
                $refl = new ReflectionClass($config['class']);
                $instance = $refl->newInstanceArgs($args);
                if (!empty($instance)) {
                    $errHandler = ErrorTrap::captureErrors(
                        [$instance, 'ping']
                    );
                    $success = $errHandler['result'];
                    $ok = $errHandler['result'];
                    if (!empty($errHandler['errors'])) {
                        $this->o(
                            false,
                            implode(
                                PHP_EOL,
                                Hash::flatten($errHandler['errors'])
                            )
                        );
                    }
                }
            } catch (Exception $e) {
                $success = false;
                $this->err('<error>' . ((string)$e) . '</error>');
                $this->fail = $e;
            }
            $this->o($ok, __("Horodateur {0}", $entity->get('name')));
        }
        return $success;
    }

    /**
     * Extrait les arguments de l'instance de l'horodateur entre la config et
     * les données saisies
     * @param array    $fields
     * @param stdClass $data
     * @return array
     */
    private function getDriverArgs(array $fields, $data): array
    {
        $decryptKey = hash('sha256', AdminsController::DECRYPT_KEY);
        $args = [];
        foreach ($fields as $field => $params) {
            $value = null;
            if (!empty($params['read_config'])) {
                $value = Configure::read($params['read_config']);
            } elseif (isset($data->$field)) {
                $value = $data->$field;
            } elseif (!empty($params['default'])) {
                $value = $params['default'];
            }
            if ($value && isset($params['type']) && $params['type'] === 'password') {
                $value = Security::decrypt(
                    base64_decode($value),
                    $decryptKey
                );
            }
            $args[$field] = $value ?: '';
        }
        return $args;
    }

    /**
     * Effectue un (new $driver($url))->ping() sur un TimestampingInterface
     * @return bool
     * @throws Exception
     */
    private function checkVolumes()
    {
        $success = true;
        /** @var CheckUtility $CheckUtility */
        $CheckUtility = Utility::get(CheckUtility::class);
        foreach ($CheckUtility->volumes() as $name => $result) {
            $this->o($result['success'], __("Volume {0}", $name));
            $success = $success && $result['success'];
        }
        return $success;
    }

    /**
     * Faux si un job est en erreur
     * @return bool
     */
    private function checkJobs(): bool
    {
        $workers = array_keys(Configure::read('Beanstalk.workers', []));
        sort($workers);

        $BeanstalkJobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $success = true;
        foreach ($workers as $name) {
            $count = $BeanstalkJobs->find()
                ->where(
                    [
                        'tube' => $name,
                        'job_state' => BeanstalkJobsTable::S_FAILED,
                    ]
                )
                ->count();
            if ($count === 0) {
                $this->o(true, __("Job {0}", $name));
            } else {
                $this->out(
                    __(
                        "Jobs {0} {1} ({2} job(s) en erreur)",
                        $name,
                        '<error>Fail</error>',
                        $count
                    )
                );
            }
            $success = $success && $count === 0;
        }
        return $success;
    }

    /**
     * Vérifi si une alerte espace disk faible est présente
     * @return bool
     */
    private function checkDiskSpace(): bool
    {
        $dirs = [
            ROOT,
            Configure::read('App.paths.data'),
            sys_get_temp_dir(),
        ];
        $success = true;
        foreach ($dirs as $dir) {
            $cond = disk_free_space($dir) > self::DISK_MIN_SPACE;
            $this->o($cond, __("Espace disque serveur sur {0}", $dir));
            $success = $success && $cond;
        }
        return $success;
    }

    /**
     * Vérifi les TreeBehavior
     * @return bool
     * @throws Exception
     */
    private function checkTreeSanity()
    {
        $success = true;
        /** @var CheckUtility $CheckUtility */
        $CheckUtility = Utility::get(CheckUtility::class);
        foreach ($CheckUtility->treeSanity() as $name => $data) {
            $result = $data['success'];
            $this->o($result, __("Table {0}: {1}", $name, $data['message']));
            $success = $success && $result;
        }
        return $success;
    }

    /**
     * Vérifi les crons
     * @return bool
     * @throws Exception
     */
    private function checkCrons()
    {
        /** @var CheckUtility $CheckUtility */
        $CheckUtility = Utility::get(CheckUtility::class);
        $ok = $CheckUtility->cronOk();
        $this->o($ok, __("Activité récente des tâches planifiées"));
        return $ok;
    }
}
