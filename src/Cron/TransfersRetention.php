<?php

/**
 * Asalae\Cron\TransfersRetention
 */

namespace Asalae\Cron;

use Asalae\Model\Entity\Archive;
use Asalae\Model\Entity\Transfer;
use Asalae\Model\Entity\TransferAttachment;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\TransferAttachmentsTable;
use Asalae\Model\Table\TransfersTable;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Console\ConsoleOutput;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Form\AbstractGenericMessageForm;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Exception\PersistenceFailedException;
use Cake\ORM\TableRegistry;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Suppression des documents de transferts dont la date de rétention est dépassée
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TransfersRetention implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;

    /**
     * @var array params
     */
    private $params;

    /**
     * @var Exception state = error si != empty
     */
    private $fail;

    /**
     * Constructeur de la classe du cron
     * @param array              $params paramètres additionnels du cron
     * @param ConsoleOutput|null $out
     * @param ConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->params = $params + [
            'retention_delay' => 1, // en jours
        ];
        $this->initializeOutput($out, $err);
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [
            'retention_delay' => [
                'type' => 'number',
                'label' => 'retention_delay',
                'min' => 0,
                'default' => 1,
                'help' => __("Délai de rétention (en jours)"),
            ],
        ];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        /** @var TransfersTable $Transfers */
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = TableRegistry::getTableLocator()->get(
            'TransferAttachments'
        );
        $Archives = TableRegistry::getTableLocator()->get('Archives');

        $query = $Transfers->find()
            ->where(
                [
                    'Transfers.state' => TransfersTable::S_ACCEPTED,
                    'Transfers.files_deleted IS NOT' => true,
                    // '... IS' => false ne fonctionne pas
                ]
            )
            ->all();

        $transfersToUpdate = [];
        $transferAttachmentCount = 0;
        $conn = $Transfers->getConnection();
        $conn->begin();
        try {
            /** @var Transfer $transfer */
            foreach ($query as $transfer) {
                $archives = $Archives->find()
                    ->innerJoinWith('Transfers')
                    ->where(['Transfers.id' => $transfer->id]);
                /** @var Archive $archive */
                foreach ($archives as $archive) {
                    if (!$this->isArchiveOk($archive)) {
                        continue 2;
                    }
                }
                $attachmentsQuery = $TransferAttachments->find()
                    ->where(['transfer_id' => $transfer->id]);
                /** @var TransferAttachment $attachment */
                foreach ($attachmentsQuery as $attachment) {
                    $attachment->set('force_delete', true);
                    $TransferAttachments->deleteOrFail($attachment);
                    $transferAttachmentCount++;
                }
                $transfertFolder = AbstractGenericMessageForm::getDataDirectory($transfer->get('id'));
                if (is_dir($transfertFolder)) {
                    Filesystem::remove($transfertFolder);
                }
                $transfersToUpdate[] = $transfer->get('id');
            }
            $conn->commit();
        } catch (PersistenceFailedException $e) {
            $conn->rollback();
            $this->out($e->getMessage());
            return 'error';
        }

        if ($count = count($transfersToUpdate)) {
            foreach (array_chunk($transfersToUpdate, 50000) as $ids_chunk) {
                $Transfers->updateAll(
                    ['files_deleted' => true],
                    ['id IN' => $ids_chunk]
                );
            }
        }
        $this->out(
            __n(
                "{0} fichier supprimé",
                "{0} fichiers supprimés",
                $transferAttachmentCount,
                $transferAttachmentCount
            )
        );
        $this->out(
            __n(
                "{0} transfert traité",
                "{0} transferts traités",
                $count,
                $count
            )
        );
        return 'success';
    }

    /**
     * Ok si
     *  - dans l'état détruite, restituée ou transférée OU l'intégrité est true
     * ET
     *  - a été vérifiée au moins retention_delay jours après sa création
     * @param Archive $archive
     * @return bool
     */
    protected function isArchiveOk(Archive $archive): bool
    {
        $states = [
            ArchivesTable::S_DESTROYED,
            ArchivesTable::S_RESTITUTED,
            ArchivesTable::S_TRANSFERRED,
        ];

        return (in_array($archive->get('state'), $states)
                || $archive->get('is_integrity_ok') === true)
            && (time() - $archive->get('created')->timestamp)
            > $this->params['retention_delay'] * 24 * 3600;
    }
}
