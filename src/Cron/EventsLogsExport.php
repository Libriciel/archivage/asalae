<?php

/**
 * Asalae\Cron\EventsLogsExport
 */

namespace Asalae\Cron;

use Asalae\Model\Entity\Cron;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\SecureDataSpacesTable;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Table\StoredFilesVolumesTable;
use Asalae\Model\Table\TechnicalArchivesTable;
use Asalae\Model\Table\TechnicalArchiveUnitsTable;
use Asalae\Model\Table\VolumesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Driver\Timestamping\TimestampingInterface;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Utility\Premis;
use Cake\Console\ConsoleOutput;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\NotFoundException;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\Stub\ConsoleOutput as TestConsoleOutput;
use Cake\Utility\Hash;
use DateInterval;
use DateInvalidTimeZoneException;
use DateTimeInterface;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use ReflectionClass;
use ReflectionException;

/**
 * Cron d'export du journal des événements
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveBinariesTable $ArchiveBinaries
 */
class EventsLogsExport implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;
    use TouchableTrait;

    /**
     * @var EntityInterface Service Archives gestionnaire
     */
    private $sa;

    /**
     * @var EntityInterface Archive technique
     */
    private $archive;

    /**
     * @var EventLogsTable
     */
    private $EventLogs;

    /**
     * @var TechnicalArchivesTable
     */
    private $TechnicalArchives;

    /**
     * @var TechnicalArchiveUnitsTable
     */
    private $TechnicalArchiveUnits;

    /**
     * @var SecureDataSpacesTable
     */
    private $SecureDataSpaces;

    /**
     * @var VolumesTable
     */
    private $Volumes;

    /**
     * @var OrgEntitiesTable
     */
    private $OrgEntities;

    /**
     * @var StoredFilesTable
     */
    private $StoredFiles;

    /**
     * @var StoredFilesVolumesTable
     */
    private $StoredFilesVolumes;

    /**
     * @var ArchiveBinariesTable
     */
    private $ArchiveBinaries;

    /**
     * @var bool
     */
    private $break = false;

    /**
     * @var array params additionnels
     */
    private $params;

    /**
     * @var string Dossier temporaire
     */
    private $tmpDir;

    /**
     * @var int nombre d'événements exportés
     */
    private $index = 0;

    /**
     * @var int nombre d'événements à exporter
     */
    private $total;

    /**
     * @var CakeDateTime
     */
    private $eventDay;

    /**
     * @var CakeDateTime
     */
    private $eventYear;

    /**
     * @var CakeDateTime
     */
    private $nextYear;

    /**
     * @var array
     */
    private $files;

    /**
     * Constructeur de la classe du cron
     * @param array                                $params paramètres
     *                                                     additionnels du cron
     * @param ConsoleOutput|TestConsoleOutput|null $out
     * @param ConsoleOutput|TestConsoleOutput|null $err
     * @throws Exception
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->params = $params;
        $this->initializeOutput($out, $err);
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $this->EventLogs = $loc->get('EventLogs');
        $this->TechnicalArchives = $loc->get('TechnicalArchives');
        $this->TechnicalArchiveUnits = $loc->get('TechnicalArchiveUnits');
        $this->SecureDataSpaces = $loc->get('SecureDataSpaces');
        $this->Volumes = $loc->get('Volumes');
        $this->OrgEntities = $loc->get('OrgEntities');
        $this->ArchiveBinaries = $loc->get('ArchiveBinaries');
        $this->StoredFiles = $loc->get('StoredFiles');
        $this->StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $this->sa = $this->OrgEntities->find()
            ->where(['is_main_archival_agency' => true])
            ->first();
        if (isset($this->params['dry-run'])) {
            mkdir(
                $this->tmpDir = sys_get_temp_dir() . DS . uniqid(
                    'event_logs_export-'
                )
            );
        }
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Destructeur
     */
    public function __destruct()
    {
        if ($this->tmpDir && is_dir($this->tmpDir)) {
            Filesystem::remove($this->tmpDir);
        }
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws ReflectionException
     * @throws VolumeException
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        if (!$this->sa) {
            $this->out(
                __("Aucun service d'archives gestionnaire n'a été trouvé")
            );
            return 'success';
        }
        $this->initCounter($exec);
        $now = new CakeDateTime('today');

        // recherche la date du plus ancien évènement non exporté.
        $event = $this->EventLogs->find()
            ->select(['created'])
            ->where(
                [
                    'exported IS' => false,
                    'created <' => $now,
                ]
            )
            ->orderBy(['created' => 'asc'])
            ->first();
        if (!$event) {
            $this->out(__("Aucun événements à exporter"));
            return 'success';
        }

        /** @var CakeDateTime $nextDate */
        $nextDate = $event->get('created');
        do {
            $result = $this->exportDate($nextDate);
            $nextDate = $nextDate->add(new DateInterval('P1D'));
        } while ($nextDate < $now && $result === 'success' && !$this->break);

        $this->TechnicalArchiveUnits->updateCountSize($this->TechnicalArchiveUnits->find());
        $this->TechnicalArchives->updateCountSize($this->TechnicalArchives->find());
        return $result;
    }

    /**
     * Export pour un jour donné (eventDay)
     * @param DateTimeInterface $date
     * @return string
     * @throws ReflectionException
     * @throws VolumeException
     * @throws Exception
     */
    private function exportDate(DateTimeInterface $date): string
    {
        $this->eventDay = new CakeDateTime($date->format('Y-m-d') . ' 00:00:00Z');
        $this->eventYear = new CakeDateTime($this->eventDay->format('Y-01-01'));
        $this->nextYear = (clone $this->eventYear)->add(new DateInterval('P1Y'));
        $this->index = 0;
        $this->touch();
        $this->out('==========================');
        $this->out($this->eventDay->format('Y-m-d'));
        $this->out('==========================');

        $archivalAgencies = $this->OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES])
            ->orderBy(['OrgEntities.id']);
        $conn = $this->EventLogs->getConnection();
        $this->files = [];
        $exportArchivalAgencyCount = 0;
        /** @var EntityInterface $archivalAgency */
        foreach ($archivalAgencies as $archivalAgency) {
            $conn->begin();
            try {
                $this->exportArchivalAgency($archivalAgency);
                $exportArchivalAgencyCount += $this->total;
            } catch (Exception $e) {
                $this->rollbackFiles();
                $conn->rollback();
                throw $e;
            }
            $conn->commit();
        }
        $this->exportOperatingEvents();
        $exportOperatingEventsCount = $this->total;
        if ($exportArchivalAgencyCount > 0 || $exportOperatingEventsCount > 0) {
            $this->sealArchives();
            $this->out(__("Terminé avec succès"));
        } else {
            $this->out(__("Aucuns événements à exporter"));
        }

        return 'success';
    }

    /**
     * Export du journal pour un jour et un tenant donné
     * @param EntityInterface $archivalAgency
     * @return string|void
     * @throws DateInvalidTimeZoneException
     * @throws VolumeException
     */
    private function exportArchivalAgency(EntityInterface $archivalAgency)
    {
        $this->archive = null;
        if (!empty($this->params['date'])) {
            $conditions = [
                'date(EventLogs.created) =' => $this->eventDay->format('Y-m-d'),
            ];
        } else {
            $conditions = [
                'EventLogs.exported IS' => false,
                'date(EventLogs.created) <=' => $this->eventDay->format('Y-m-d'),
            ];
        }
        $conditions['archival_agency_id'] = $archivalAgency->id;

        $count = $this->EventLogs->find()->where($conditions)->count();
        $this->out(
            __(
                "Export de {0} événements du service d'archives {1}...",
                $count,
                $archivalAgency->get('name')
            )
        );
        $this->total = $count;
        if (empty($count)) {
            return 'success';
        }

        $this->exportByConditions($conditions, $archivalAgency);
    }

    /**
     * Export d'un lot d'événements selon $conditions
     * @param array                $conditions
     * @param EntityInterface|null $archivalAgency null=événements d'exploitation
     * @return string|void
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    private function exportByConditions(
        array $conditions,
        EntityInterface $archivalAgency = null
    ) {
        /**
         * Recherche/création des technical_archive/technical_archive_units
         */
        $this->out(__("Recherche d'archive existante"));
        $query = $this->TechnicalArchives->find();
        $conditionsArchive = [
            'type' => TechnicalArchivesTable::TYPE_OPERATING_EVENTS_LOGS,
            'created >=' => $this->eventYear,
            'created <' => $this->nextYear,
        ];
        if ($archivalAgency) {
            $conditionsArchive['archival_agency_id'] = $archivalAgency->id;
            $conditionsArchive['type'] = TechnicalArchivesTable::TYPE_ARCHIVAL_AGENCY_EVENTS_LOGS;
        }
        $this->archive = $query
            ->where($conditionsArchive)
            ->contain(
                [
                    'FirstTechnicalArchiveUnits' => [
                        'LastChildTechnicalArchiveUnits',
                    ],
                ]
            )
            ->first();
        if (!$this->archive) {
            $this->out(__("Initialisation d'une nouvelle archive"));
            $this->archive = $this->initializeArchive($archivalAgency);
        }
        $archiveUnit = Hash::get(
            $this->archive,
            'first_technical_archive_unit.last_child_technical_archive_unit',
            []
        );
        /** @var CakeDateTime $auDate */
        $auDate = Hash::get($archiveUnit, 'oldest_date');
        if (!$auDate || $auDate->format('m') !== $this->eventDay->format('m')) {
            $this->out(
                __(
                    "Initialisation d'une nouvelle unité d'archive pour le mois en cours"
                )
            );
            $archiveUnit = $this->initializeArchiveUnit($archivalAgency);
        }

        /**
         * Vérifications
         */
        if ($archivalAgency) {
            $filename = sprintf(
                "%s/asalae_%s_%s.xml",
                $this->archive->get('storage_path'),
                $archivalAgency->get('identifier'),
                $this->eventDay->format('Ymd')
            );
        } else {
            $filename = sprintf(
                "%s/asalae_operating_events_log_%s.xml",
                $this->archive->get('storage_path'),
                $this->eventDay->format('Ymd')
            );
        }
        $manager = $this->getVolumeManager();
        if ($manager->fileExists($filename)) {
            $this->out(__("Le fichier {0} existe déjà.", $filename));
            return 'success';
        }

        /**
         * Création du premis
         */
        $this->out(__("Génération du fichier PREMIS"));
        $previous = $this->StoredFiles->find()
            ->innerJoinWith('ArchiveBinaries')
            ->innerJoinWith('ArchiveBinaries.TechnicalArchiveUnits')
            ->where(
                [
                    'TechnicalArchiveUnits.technical_archive_id' => $this->archive->get('id'),
                    'TechnicalArchiveUnits.parent_id IS NOT' => null,
                    'StoredFiles.name NOT LIKE' => '%.tsr',
                ]
            )
            ->orderBy(
                [
                    'TechnicalArchiveUnits.oldest_date' => 'desc',
                    'TechnicalArchiveUnits.id' => 'desc',
                    'StoredFiles.created' => 'desc',
                    'StoredFiles.id' => 'desc',
                ]
            )
            ->first();
        $premis = $this->EventLogs->export(
            $conditions,
            $previous,
            [$this, 'touchAndReport']
        );
        if (!$premis->schemaValidate(PREMIS_V3)) {
            $this->out(__("Echec de l'export du journal"));
            $this->break = true;
            return 'fail';
        }

        /**
         * Stockage du premis
         */
        $this->out(__("Ecriture des fichiers sur l'espace de stockage"));
        $this->out($filename);
        if (!isset($this->params['dry-run'])) {
            $this->override($manager, $filename);
            $this->TechnicalArchives->transition($this->archive, TechnicalArchivesTable::T_STORE);
            $this->TechnicalArchives->saveOrFail($this->archive);
            $storedFile1 = $manager->filePutContent($filename, $premis->saveXML());
            $this->files[] = $storedFile1;
            $this->createArchiveBinaries(
                $archiveUnit,
                $storedFile1
            );
            $this->TechnicalArchives->transition($this->archive, TechnicalArchivesTable::T_FINISH);
            $this->TechnicalArchives->saveOrFail($this->archive);
        } else {
            $this->out($premis->saveXML());
            return 'success';
        }

        // la condition sur created est mal interprété dans updateAll
        // -> contournement avec une sous-requête
        $subQuery = $this->EventLogs->find()
            ->select(['EventLogs.id'])
            ->where($conditions);
        $this->EventLogs->updateAll(['exported' => true], ['id IN' => $subQuery]);
    }

    /**
     * Créer l'archive annuelle
     * @param EntityInterface|null $archivalAgency
     * @return EntityInterface
     * @throws DateInvalidTimeZoneException
     */
    private function initializeArchive(EntityInterface $archivalAgency = null): EntityInterface
    {
        if ($archivalAgency) {
            $name = __(
                "Journaux des événements : {0}",
                $this->eventDay->format('Y')
            );
            $identifier = sprintf(
                '%d_archival_agency_events_logs',
                $this->eventDay->format('Y')
            );
            $dataArchive = [
                'name' => $name,
                'type' => TechnicalArchivesTable::TYPE_ARCHIVAL_AGENCY_EVENTS_LOGS,
                'archival_agency_id' => $archivalAgency->id,
                'archival_agency_identifier' => $identifier,
                'secure_data_space_id' => $archivalAgency->get('default_secure_data_space_id'),
                'storage_path' => sprintf(
                    '%s/technical_archives/%s',
                    $archivalAgency->get('identifier'),
                    $identifier
                ),
            ];
            $dataArchiveUnit = [
                'name' => $name,
                'archival_agency_identifier' => sprintf("%s_%04d", $identifier, 1),
                'description' => __(
                    "Archive technique annuelle des journaux des évènements"
                    . " d'exploitation et des sceaux des journaux. Les journaux "
                    . "des événements d'exploitation sont des fichiers XML PREMIS"
                    . " qui contiennent les événements d'exploitation d'une même "
                    . "journée. Les sceaux des journaux sont des fichiers PREMIS"
                    . " qui font le lien avec les journaux des événements et les"
                    . " journaux d'exploitation d'une même journée. Les fichiers"
                    . " des sceaux des journaux sont horodatés."
                ),
            ];
        } else {
            $name = __(
                "Journaux des événements d'exploitation : {0}",
                $this->eventDay->format('Y')
            );
            $identifier = sprintf(
                '%d_operating_events_logs',
                $this->eventDay->format('Y')
            );
            $dataArchive = [
                'name' => $name,
                'type' => TechnicalArchivesTable::TYPE_OPERATING_EVENTS_LOGS,
                'archival_agency_id' => $this->sa->id,
                'archival_agency_identifier' => $identifier,
                'secure_data_space_id' => $this->sa->get('default_secure_data_space_id'),
                'storage_path' => sprintf(
                    '%s/technical_archives/%s',
                    $this->sa->get('identifier'),
                    $identifier
                ),
            ];
            $dataArchiveUnit = [
                'name' => $name,
                'archival_agency_identifier' => sprintf("%s_%04d", $identifier, 1),
                'description' => __(
                    "Archive technique annuelle des journaux des "
                    . "évènements d'exploitation et des sceaux des journaux. Les "
                    . "journaux des événements d'exploitation sont des fichiers "
                    . "XML PREMIS qui contiennent les événements d'exploitation "
                    . "d'une même journée. Les sceaux des journaux sont des "
                    . "fichiers PREMIS qui font le lien avec les journaux des "
                    . "événements et les journaux d'exploitation d'une même "
                    . "journée. Les fichiers des sceaux des journaux sont horodatés."
                ),
            ];
        }
        $archive = $this->TechnicalArchives->newEntity(
            $dataArchive + [
                'state' => $this->TechnicalArchives->initialState,
                'original_size' => 0,
                'original_count' => 0,
                'timestamp_size' => 0,
                'timestamp_count' => 0,
                'management_size' => 0,
                'management_count' => 0,
                'created' => $this->eventDay,
            ]
        );
        $this->TechnicalArchives->saveOrFail($archive);

        $archiveUnit = $this->TechnicalArchiveUnits->newEntity(
            $dataArchiveUnit + [
                'state' => $this->TechnicalArchiveUnits->initialState,
                'technical_archive_id' => $archive->get('id'),
                'parent_id' => null,
                'oldest_date' => $this->eventDay,
                'latest_date' => $this->eventDay,
                'original_local_size' => 0,
                'original_local_count' => 0,
                'original_total_size' => 0,
                'original_total_count' => 0,
                'xml_node_tagname' => '',
                'created' => $this->eventDay,
            ],
            ['setter' => false]
        );
        $this->TechnicalArchiveUnits->saveOrFail($archiveUnit);

        $archive->set('first_technical_archive_unit', $archiveUnit);
        $this->TechnicalArchives->transitionOrFail($archive, TechnicalArchivesTable::T_DESCRIBE);
        $this->archive = $archive;
        $au = $this->initializeArchiveUnit($archivalAgency);
        $archiveUnit->set('last_child_technical_archive_unit', $au);

        return $archive;
    }

    /**
     * Génère l'archive_unit du mois en cours
     * @param EntityInterface|null $archivalAgency
     * @return EntityInterface
     */
    private function initializeArchiveUnit(EntityInterface $archivalAgency = null): EntityInterface
    {
        if ($archivalAgency) {
            $name = __(
                "Journaux des événements : {0}",
                $this->eventDay->format('Y-m')
            );
            $description = __(
                "Archive technique mensuelle des journaux des évènements.",
                $this->eventDay->format('Y-m')
            );
        } else {
            $name = __(
                "Journaux des événements d'exploitation : {0}",
                $this->eventDay->format('Y-m')
            );
            $description = __(
                "Archive technique mensuelle des journaux des évènements d'exploitation : {0}",
                $this->eventDay->format('Y-m')
            );
        }
        $uuid = Hash::get($this->archive, 'archival_agency_identifier');
        $archiveUnit = $this->TechnicalArchiveUnits->newEntity(
            [
                'state' => $this->TechnicalArchiveUnits->initialState,
                'technical_archive_id' => $this->archive->get('id'),
                'parent_id' => Hash::get(
                    $this->archive,
                    'first_technical_archive_unit.id'
                ),
                'archival_agency_identifier' => sprintf(
                    "%s_%04d",
                    $uuid,
                    $this->countArchiveUnits($this->archive) + 1
                ),
                'name' => $name,
                'description' => $description,
                'oldest_date' => $this->eventDay,
                'latest_date' => $this->eventDay,
                'original_local_size' => 0,
                'original_local_count' => 0,
                'original_total_size' => 0,
                'original_total_count' => 0,
                'xml_node_tagname' => '',
                'created' => $this->eventDay,
            ]
        );
        return $this->TechnicalArchiveUnits->saveOrFail($archiveUnit);
    }

    /**
     * Donne le nombre d'archive units d'une archive
     * @param EntityInterface $archive
     * @return int
     */
    private function countArchiveUnits(EntityInterface $archive): int
    {
        return $this->TechnicalArchiveUnits->find()
            ->where(['technical_archive_id' => $archive->get('id')])
            ->count();
    }

    /**
     * Donne le VolumeManager selon dry-run ou l'archive technique
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(): VolumeManager
    {
        if (isset($this->params['dry-run'])) {
            $secureDataSpace = $this->SecureDataSpaces->findOrCreate(
                [
                    'name' => 'temporary_' . $this->archive->get('archival_agency_id'),
                    'org_entity_id' => $this->archive->get('archival_agency_id'),
                ]
            );
            $secureDataSpaceId = $secureDataSpace->id;
            $this->Volumes->findOrCreate(
                [
                    'name' => 'temporary_' . $this->archive->get('archival_agency_id'),
                    'fields' => json_encode(['path' => $this->tmpDir], JSON_UNESCAPED_SLASHES),
                    'driver' => 'FILESYSTEM',
                    'active' => true,
                    'secure_data_space_id' => $secureDataSpaceId,
                    'max_disk_usage' => 1000000000,
                ]
            );
        } else {
            $secureDataSpaceId = $this->archive->get('secure_data_space_id');
        }
        return new VolumeManager($secureDataSpaceId);
    }

    /**
     * Supprime les fichiers existants sans références
     * @param VolumeManager $manager
     * @param string        $filename
     * @return void
     * @throws VolumeException
     */
    private function override(VolumeManager $manager, string $filename)
    {
        if (empty($this->params['override'])) {
            return;
        }
        foreach ($manager->getVolumes() as $driver) {
            if ($driver->fileExists($filename)) {
                $driver->fileDelete($filename);
            }
        }
    }

    /**
     * Créé les archive_binaries liés aux stored_files
     * @param EntityInterface      $archiveUnit
     * @param EntityInterface      $originalFile
     * @param EntityInterface|null $tokenFile
     */
    private function createArchiveBinaries(
        EntityInterface $archiveUnit,
        EntityInterface $originalFile,
        EntityInterface $tokenFile = null
    ) {
        $original = $this->ArchiveBinaries->newEntity(
            [
                'type' => 'original_data',
                'filename' => basename($originalFile->get('name')),
                'format' => 'fmt/101',
                'mime' => 'application/xml',
                'extension' => 'xml',
                'stored_file_id' => $originalFile->get('id'),
                'in_rgi' => false,
                'app_meta' => [],
            ]
        );
        $original->set('technical_archive_units', [$archiveUnit]);
        $this->ArchiveBinaries->saveOrFail($original);
        if ($tokenFile) {
            $token = $this->ArchiveBinaries->newEntity(
                [
                    'type' => 'original_timestamp',
                    'filename' => basename($tokenFile->get('name')),
                    'mime' => 'application/octet-stream',
                    'extension' => 'tsr',
                    'stored_file_id' => $tokenFile->get('id'),
                    'original_data_id' => $original->get('id'),
                    'in_rgi' => false,
                    'app_meta' => [],
                ]
            );
            $token->set('technical_archive_units', [$archiveUnit]);
            $this->ArchiveBinaries->saveOrFail($token);
        }
    }

    /**
     * Supprime les fichiers enregistrés durant la transaction
     * @return void
     * @throws VolumeException
     */
    private function rollbackFiles()
    {
        if (!$this->archive) {
            return;
        }
        $manager = $this->getVolumeManager();
        foreach ($this->files as $storedFile) {
            foreach ($manager->getVolumes() as $driver) {
                if ($driver->fileExists($storedFile->get('name'))) {
                    $driver->fileDelete($storedFile->get('name'));
                }
            }
        }
    }

    /**
     * Export du journal pour un jour et un tenant donné
     * @return string|void
     * @throws DateInvalidTimeZoneException
     * @throws VolumeException
     */
    private function exportOperatingEvents()
    {
        $this->archive = null;
        if (!empty($this->params['date'])) {
            $conditions = [
                'date(EventLogs.created) =' => $this->eventDay->format('Y-m-d'),
            ];
        } else {
            $conditions = [
                'EventLogs.exported IS' => false,
                'date(EventLogs.created) <=' => $this->eventDay->format('Y-m-d'),
            ];
        }
        $conditions['archival_agency_id IS'] = null;

        $count = $this->EventLogs->find()->where($conditions)->count();
        $this->out(__("Export de {0} événements d'exploitation...", $count));
        $this->total = $count;
        if (empty($count)) {
            return 'success';
        }

        $this->exportByConditions($conditions);
    }

    /**
     * Créé l'archive unit de scellement des journaux avec horodatage
     * @return string|void
     * @throws DateInvalidTimeZoneException
     * @throws ReflectionException
     * @throws VolumeException
     */
    private function sealArchives()
    {
        $loc = TableRegistry::getTableLocator();
        $Crons = $loc->get('Crons');
        /** @var Cron $cron */
        $cron = $Crons->find()
            ->where(['classname' => self::class])
            ->firstOrFail();
        $ArchiveBinaries = TableRegistry::getTableLocator()->get('ArchiveBinaries');
        $premis = new Premis();
        $this->addPreviousFileToSeal($premis);
        $event = new Premis\Event('seal_events_logs');
        $event->detail = __("Création des journaux des événements");
        $agent = $cron->toPremisAgent();
        $agent->name = __("Tâche planifiée de création des journaux des événements");
        foreach ($this->files as $storedFile) {
            $archiveBinary = $ArchiveBinaries->find()
                ->where(['stored_file_id' => $storedFile->id])
                ->contain(['TechnicalArchiveUnits'])
                ->firstOrFail();
            /** @var EntityInterface $technicalArchiveUnit */
            $technicalArchiveUnit = Hash::get($archiveBinary, 'technical_archive_units.0');
            $file = new Premis\File(
                $archiveBinary->get('format') ?: 'unknown',
                'local',
                'ArchiveBinaries:' . $archiveBinary->id
            );
            $file->originalName = $storedFile->get('name');
            $file->messageDigest = $storedFile->get('hash');
            if ($archiveBinary->get('format')) {
                $file->formatRegistryName = 'pronom';
                $file->formatRegistryKey = $archiveBinary->get('format');
            } else {
                $file->formatName = 'UNKNOWN';
            }
            if ($storedFile->get('hash_algo') !== 'sha256') {
                $file->messageDigestAlgorithm = [
                    '@' => $storedFile->get('hash_algo'),
                ];
            }
            $file->size = $storedFile->get('size');

            $intel = new Premis\IntellectualEntity(
                'local',
                'TechnicalArchiveUnits:' . $technicalArchiveUnit->id
            );
            $intel->originalName = $technicalArchiveUnit->get('name');
            $file->relationship = [
                'relatedObjectIdentifierType' => $intel->identifierType,
                'relatedObjectIdentifierValue' => $intel->identifierValue,
            ];
            $event->addObject($intel);
            $event->addObject($file);
            if (empty($event->agents)) {
                $event->addAgent($agent);
            }
        }
        $premis->add($event);

        $query = $this->TechnicalArchives->find();
        $conditionsArchive = [
            'type' => TechnicalArchivesTable::TYPE_OPERATING_EVENTS_LOGS,
            'created >=' => $this->eventYear,
            'created <' => $this->nextYear,
        ];
        $this->archive = $query
            ->where($conditionsArchive)
            ->contain(
                [
                    'FirstTechnicalArchiveUnits' => [
                        'LastChildTechnicalArchiveUnits',
                    ],
                ]
            )
            ->first();
        if (!$this->archive) {
            $this->out(__("Initialisation d'une nouvelle archive"));
            $this->archive = $this->initializeArchive();
        }
        $archiveUnit = Hash::get(
            $this->archive,
            'first_technical_archive_unit.last_child_technical_archive_unit',
            []
        );
        /** @var CakeDateTime $auDate */
        $auDate = Hash::get($archiveUnit, 'oldest_date');
        if (!$auDate || $auDate->format('m') !== $this->eventDay->format('m')) {
            $this->out(
                __(
                    "Initialisation d'une nouvelle unité d'archive pour le mois en cours"
                )
            );
            $archiveUnit = $this->initializeArchiveUnit();
        }
        $filename = sprintf(
            "%s/asalae_seal_events_logs_%s.xml",
            $this->archive->get('storage_path'),
            $this->eventDay->format('Ymd')
        );
        $file = null;
        $premisXml = (string)$premis;
        try {
            if (!isset($this->params['dry-run'])) {
                /**
                 * Horodatage
                 */
                $this->out(__("Horodatage"));
                $file = tmpfile();
                fwrite($file, $premisXml);
                fseek($file, 0);
                $driver = $this->getTimestamperDriver($this->sa->get('timestamper_id'));
                $token = $driver->generateToken($file);
                fseek($file, 0);
                $manager = $this->getVolumeManager();
                $this->out($filename);
                $this->override($manager, $filename);
                $storedFile1 = $manager->filePutContent($filename, $premisXml);
                $this->files[] = $storedFile1;
                $this->out($filename . '.tsr');
                $this->override($manager, $filename . '.tsr');
                $storedFile2 = $manager->filePutContent($filename . '.tsr', $token);
                $this->files[] = $storedFile2;
                $this->createArchiveBinaries(
                    $archiveUnit,
                    $storedFile1,
                    $storedFile2
                );
            } else {
                $this->out($premisXml);
                return 'success';
            }
        } finally {
            if (isset($file)) {
                fclose($file);
            }
        }
    }

    /**
     * Ajoute le chainage du seal précédent
     * @param Premis $premis
     * @return void
     */
    private function addPreviousFileToSeal(Premis $premis)
    {
        $previous = $this->StoredFiles->find()
            ->innerJoinWith('ArchiveBinaries')
            ->innerJoinWith('ArchiveBinaries.TechnicalArchiveUnits')
            ->innerJoinWith('ArchiveBinaries.TechnicalArchiveUnits.TechnicalArchives')
            ->where(
                [
                    'TechnicalArchives.type' => TechnicalArchivesTable::TYPE_OPERATING_EVENTS_LOGS,
                    'StoredFiles.name LIKE' => '%/asalae_seal_events_logs_%.xml',
                ]
            )
            ->orderBy(
                [
                    'TechnicalArchiveUnits.oldest_date' => 'desc',
                    'TechnicalArchiveUnits.id' => 'desc',
                    'StoredFiles.created' => 'desc',
                    'StoredFiles.id' => 'desc',
                ]
            )
            ->first();
        if (!$previous) {
            return;
        }

        $object = new Premis\IntellectualEntity('local', 'EventsLogsChain');
        $object->originalName = 'previous file';
        $object->significantProperties['filename'] = basename(
            $previous->get('name')
        );
        $object->significantProperties['size'] = $previous->get('size');
        $object->significantProperties['hash_algo'] = $previous->get(
            'hash_algo'
        );
        $object->significantProperties['hash'] = $previous->get('hash');

        $premis->addObject($object);
    }

    /**
     * Permet d'obtenir une instance du driver d'un horodateur à partir de son id
     * @param string|int $timestamper_id
     * @return TimestampingInterface
     * @throws ReflectionException
     * @throws Exception
     */
    private function getTimestamperDriver($timestamper_id): TimestampingInterface
    {
        $Timestampers = TableRegistry::getTableLocator()->get('Timestampers');
        $timestamper = $Timestampers->find()
            ->where(['id' => $timestamper_id])
            ->firstOrFail();
        $config = Configure::read(
            'Timestamping.drivers.' . $timestamper->get('driver')
        );
        if (!$config) {
            throw new NotFoundException(
                sprintf(
                    "The driver %s was not found",
                    $timestamper->get('driver')
                )
            );
        }

        $args = [];
        foreach ((array)Hash::get($config, 'fields', []) as $field => $params) {
            $value = null;
            if (!empty($params['read_config'])) {
                $value = Configure::read($params['read_config']);
            } elseif (
                isset($params['type'])
                && $params['type'] === 'password'
                && $password = $timestamper->get($field . '_decrypted')
            ) {
                $value = $password;
            } elseif ($timestamper->get($field)) {
                $value = $timestamper->get($field);
            } elseif (!empty($params['default'])) {
                $value = $params['default'];
            }
            if ($value) {
                $args[] = $value;
            } else {
                $args[] = '';
            }
        }

        $refl = new ReflectionClass(Hash::get($config, 'class', ''));
        $instance = $refl->newInstanceArgs($args);
        if (!$instance instanceof TimestampingInterface) {
            throw new Exception(
                sprintf(
                    "The driver %s class does not implement the TimestampingInterface",
                    $timestamper->get('driver')
                )
            );
        }
        return $instance;
    }

    /**
     * Affirme son existance et affiche un rapport
     */
    public function touchAndReport()
    {
        $this->index++;
        $ntimer = microtime(true);
        if ($ntimer - $this->timer > $this->minDelayBeforeSave) {
            $this->timer = $ntimer;
            $this->out(
                __("Evénements exportés: {0} / {1}", $this->index, $this->total)
            );
            if ($this->exec) {
                $this->exec->set('last_update', new CakeDateTime());
                $this->CronExecutions->save($this->exec);
            }
        }
    }
}
