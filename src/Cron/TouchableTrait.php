<?php

/**
 * Asalae\Cron\Touchable
 */

namespace Asalae\Cron;

use Asalae\Model\Table\CronExecutionsTable;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use DateTime;
use Exception;

/**
 * mise à jour du champ cron_executions.last_update
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait TouchableTrait
{
    /**
     * @var EntityInterface CronExecution
     */
    public $exec;

    /**
     * @var float timestamp de la dernière mise à jour
     */
    private $timer = 0.0;

    /**
     * @var int durée en secondes entre les sauvegarde du CronExecution
     */
    private $minDelayBeforeSave = 60;

    /**
     * @var CronExecutionsTable table
     */
    private $CronExecutions;

    /**
     * Initialise le compteur
     * @param EntityInterface $exec
     */
    public function initCounter($exec)
    {
        $this->exec = $exec;
        $this->timer = microtime(true);
        $this->minDelayBeforeSave = Configure::read(
            'Crons.touch_min_delay',
            60
        );
        $this->CronExecutions = TableRegistry::getTableLocator()->get(
            'CronExecutions'
        );
    }

    /**
     * Mise à jour du champ last_update
     * @throws Exception
     */
    public function touch()
    {
        $ntimer = microtime(true);
        if ($ntimer - $this->timer > $this->minDelayBeforeSave) {
            $this->timer = $ntimer;
            if ($this->exec) {
                $this->exec->set('last_update', new DateTime());
                $this->CronExecutions->save($this->exec);
            }
        }
    }
}
