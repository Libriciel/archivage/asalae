<?php

/**
 * Asalae\Cron\ArchivesIntegrityControl
 */

namespace Asalae\Cron;

use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\StoredFilesVolumesTable;
use Asalae\Model\Table\TechnicalArchivesTable;
use Asalae\Model\Table\VolumesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\ColoredCronTrait;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Driver\Volume\VolumeInterface;
use AsalaeCore\Driver\Volume\VolumeS3;
use AsalaeCore\Utility\HashUtility;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Database\Connection;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Error\Debugger;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInterval;
use DateTime;
use Exception;
use Throwable;

/**
 * Interface des crons
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchivesIntegrityControl implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;
    use TouchableTrait;
    use ColoredCronTrait;

    /**
     * @var array params
     */
    private $params;

    /**
     * @var Exception state = error si != empty
     */
    private $fail;

    /**
     * @var int nombre d'evenements exportés
     */
    private $index = 0;

    /**
     * @var int nombre d'evenements à exporter
     */
    private $total;

    /**
     * @var string result
     */
    private $outputValue;

    /**
     * @var array
     */
    private $controlParams;

    /**
     * @var int
     */
    private $prevArchiveId;

    /**
     * @var bool
     */
    private $success;

    /**
     * @var Table
     */
    private $Model;

    /**
     * @var int[]
     */
    private $successes;

    /**
     * @var EntityInterface
     */
    private $prevArchive;

    /**
     * @var VolumeInterface[]
     */
    private $volumes;

    /**
     * @var Connection
     */
    private $conn;

    /**
     * @var string
     */
    private $name;

    /**
     * @var array fetch->toArray()
     */
    private $archive;

    /**
     * @var EntityInterface
     */
    private $cron;
    /**
     * @var int
     */
    private $maxTime;
    private DateTime $threshold;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params paramètres additionnels du
     *                                       cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->params = $params + [
            'max_execution_time' => 1, // en heures
            'min_delay' => 7, // en jours
            'update_time' => 60, // en secondes
            'context' => 'shell',
        ];
        $this->initializeOutput($out, $err);
        $this->initializeOutput($out, $err);
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return ArchivesFilesExistControl::getVirtualFields();
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        $this->cron = $cron;
        $this->initCounter($exec);
        $beginTime = time();
        $this->maxTime = $beginTime + (3600 * $this->params['max_execution_time']);
        $this->threshold = (new DateTime())->sub(
            new DateInterval('P' . ($this->params['min_delay']) . 'D')
        );

        $loc = TableRegistry::getTableLocator();

        /** @var StoredFilesVolumesTable $StoredFiles */
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $conditions = $this->params['context'] === 'user-action'
            ? []
            : [
                'OR' => [
                    ['StoredFilesVolumes.is_integrity_ok IS' => false],
                    ['StoredFilesVolumes.is_integrity_ok IS' => null],
                    'StoredFilesVolumes.integrity_date IS' => null,
                    'StoredFilesVolumes.integrity_date <' => $this->threshold,
                ],
            ];
        $storedFilesVolumesQuery = $StoredFilesVolumes->find()
            ->where($conditions)
            ->orderBy(
                [
                    'StoredFilesVolumes.is_integrity_ok IS NULL' => 'desc',
                    'StoredFilesVolumes.is_integrity_ok' => 'asc',
                    'StoredFilesVolumes.integrity_date' => 'asc',
                ]
            );
        $count = $storedFilesVolumesQuery->count();
        if ($count === 0) {
            $this->out(__("Aucune vérification d'intégrité n'est nécessaire"));
            return 'success';
        } else {
            $this->out(
                __n(
                    "Il y a {0} fichier à vérifier",
                    "Il y a {0} fichiers à vérifier",
                    $count,
                    $count
                )
            );
        }
        $this->total = $count;

        $allVolumesOk = $this->checkSecureDataSpaces($storedFilesVolumesQuery);

        $queryArchiveFiles = clone $storedFilesVolumesQuery;
        $queryArchiveFiles
            ->select(['StoredFilesVolumes.id'])
            ->innerJoinWith('StoredFiles.ArchiveFiles');
        $queryArchiveBinaries = clone $storedFilesVolumesQuery;
        $queryArchiveBinaries
            ->select(['StoredFilesVolumes.id'])
            ->innerJoinWith('StoredFiles.ArchiveBinaries.ArchiveUnits');
        $queryTechnicalArchiveBinaries = clone $storedFilesVolumesQuery;
        $queryTechnicalArchiveBinaries
            ->select(['StoredFilesVolumes.id'])
            ->innerJoinWith('StoredFiles.ArchiveBinaries.TechnicalArchiveUnits');

        $archiveSuccess = $this->checkArchiveIntegrity($storedFilesVolumesQuery);
        $technicalArchiveSuccess = $this->checkTechnicalArchiveIntegrity($storedFilesVolumesQuery);

        return $allVolumesOk && $archiveSuccess && $technicalArchiveSuccess ? 'success' : 'error';
    }

    /**
     * Ajoute un contrôle préalable sur les volumes, pour éviter de rendre des
     * milliers d'archives non conformes pour un volume hors réseau
     * @param Query $storedFilesVolumesQuery
     * @return bool
     */
    private function checkSecureDataSpaces(Query $storedFilesVolumesQuery): bool
    {
        $loc = TableRegistry::getTableLocator();
        /** @var VolumesTable $StoredFiles */
        $Volumes = $loc->get('Volumes');
        $volumes = $Volumes->find()
            ->where(['Volumes.id IN' => (clone $storedFilesVolumesQuery)->select(['volume_id'], true)])
            ->orderBy(['Volumes.id']);
        $notWorkingSecureDataSpaces = [];
        /** @var EntityInterface $volume */
        foreach ($volumes as $volume) {
            try {
                /** @var VolumeS3|VolumeFilesystem $driver */
                $driver = VolumeManager::getDriver($volume);
                $test = $driver->test();
            } catch (VolumeException $e) {
                $test['success'] = false;
                $test['exception'] = $e->getMessage();
            }
            if ($test['success']) {
                $this->out(sprintf('volume %d: OK', $volume->id));
            } else {
                $notWorkingSecureDataSpaces[] = $volume->get('secure_data_space_id');
                $this->err(
                    sprintf(
                        'volume %d: %s',
                        $volume->id,
                        Debugger::exportVar($test['errors'] ?? $test['exception'])
                    )
                );
            }
        }
        if ($notWorkingSecureDataSpaces) {
            $storedFilesVolumesQuery
                ->innerJoinWith('Volumes')
                ->where(['Volumes.secure_data_space_id NOT IN' => $notWorkingSecureDataSpaces]);
            return false;
        }
        return true;
    }

    /**
     * Effectue le test d'intégrité sur les archives
     * @param Query $storedFilesVolumesQuery
     * @return bool
     * @throws VolumeException
     */
    private function checkArchiveIntegrity(Query $storedFilesVolumesQuery): bool
    {
        $failed = false;
        $archivesConditions = $this->params['context'] === 'user-action'
            ? []
            : [
                'OR' => [
                    ['Archives.is_integrity_ok IS' => false],
                    ['Archives.is_integrity_ok IS' => null],
                    'Archives.integrity_date IS' => null,
                    'Archives.integrity_date <' => $this->threshold,
                ],
            ];
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');
        $archivesQuery = $Archives->find()
            ->where($archivesConditions)
            ->orderBy(
                [
                    'Archives.is_integrity_ok IS NULL' => 'desc',
                    'Archives.is_integrity_ok' => 'asc',
                    'Archives.integrity_date' => 'asc',
                    'Archives.created' => 'asc',
                ]
            );

        $this->filterEmptyArchives($archivesQuery, $storedFilesVolumesQuery);

        $count = $archivesQuery->count();
        if ($count === 0) {
            $this->out(__("Aucune vérification d'intégrité n'est nécessaire sur les archives"));
        } else {
            $this->out(
                __n(
                    "Il y a {0} archive à vérifier",
                    "Il y a {0} archives à vérifier",
                    $count,
                    $count
                )
            );
        }
        $archiveSuccesses = 0;
        foreach ($archivesQuery as $archive) {
            // Si le process à durée trop longtemps, on s'arrète là
            if (time() > $this->maxTime) {
                $this->out(__("Limite de durée d'exécution atteinte."));
                break;
            }
            if ($this->checkArchive($archive, $storedFilesVolumesQuery)) {
                $archiveSuccesses++;
            } else {
                $failed = true;
            }
        }
        if ($archiveSuccesses) {
            $this->out(
                __n(
                    "Archive testée avec succès: {0}",
                    "Archives testées avec succès: {0}",
                    $archiveSuccesses,
                    $archiveSuccesses
                )
            );
        }
        return !$failed;
    }

    /**
     * On ne garde que les archives qui sont liés à $storedFilesVolumesQuery
     * @param Query $archivesQuery
     * @param Query $storedFilesVolumesQuery
     * @return void
     */
    private function filterEmptyArchives(Query $archivesQuery, Query $storedFilesVolumesQuery)
    {
        $archivesConditions = clone $archivesQuery->clause('where');
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');

        $subquery = (clone $storedFilesVolumesQuery)->select(['StoredFilesVolumes.id'], true);
        $archiveFiles = $Archives->find()
            ->select(['Archives.id'])
            ->innerJoinWith('ArchiveFiles.StoredFiles.StoredFilesVolumes')
            ->where($archivesConditions)
            ->andWhere(['StoredFilesVolumes.id IN' => $subquery]);
        $archiveBinaries = $Archives->find()
            ->select(['Archives.id'])
            ->innerJoinWith('ArchiveUnits.ArchiveBinaries.StoredFiles.StoredFilesVolumes')
            ->where($archivesConditions)
            ->andWhere(['StoredFilesVolumes.id IN' => $subquery]);
        $archivesWithStoredFilesVolumesQuery = $archiveFiles->union($archiveBinaries);
        $archivesQuery->where(['Archives.id IN' => $archivesWithStoredFilesVolumesQuery]);
    }

    /**
     * Vérifie la présence des fichiers d'une archive
     * @param EntityInterface $archive
     * @param Query           $storedFilesVolumes
     * @return bool|null
     * @throws VolumeException
     */
    private function checkArchive(EntityInterface $archive, Query $storedFilesVolumes)
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');
        $conn = $Archives->getConnection();
        $conn->begin();

        $queryArchiveFiles = clone $storedFilesVolumes;
        $queryArchiveFiles->innerJoinWith('StoredFiles.ArchiveFiles')
            ->where(['ArchiveFiles.archive_id' => $archive->id])
            ->contain(['StoredFiles']);
        foreach ($queryArchiveFiles as $storedFileVolume) {
            $this->touchAndReport();
            $this->checkStoredFileVolume($storedFileVolume);
        }
        $archiveIntegrityOk = $this->updateArchiveFilesIntegrityOk($archive);

        $queryArchiveBinaries = clone $storedFilesVolumes;
        $queryArchiveBinaries->innerJoinWith('StoredFiles.ArchiveBinaries.ArchiveUnits')
            ->where(['ArchiveUnits.archive_id' => $archive->id])
            ->contain(['StoredFiles']);
        $count = $queryArchiveBinaries->count();
        $queryArchiveBinaries->limit(65000);
        for ($i = 0; $i <= $count; $i += 65000) {
            $queryArchiveBinaries->offset($i);
            foreach ($queryArchiveBinaries as $storedFileVolume) {
                $this->touchAndReport();
                $this->checkStoredFileVolume($storedFileVolume);
            }
        }
        $archiveIntegrityOk = $this->updateArchiveBinariesIntegrityOk($archive)
            && $archiveIntegrityOk;

        $archive = $Archives->find()
            ->where(['Archives.id' => $archive->id])
            ->contain(['LifecycleXmlArchiveFiles' => ['StoredFiles']])
            ->firstOrFail();
        $archive->set('is_integrity_ok', $archiveIntegrityOk);
        $archive->set('integrity_date', new DateTime());
        $Archives->saveOrFail($archive);
        $conn->commit();
        self::createLifecycleEvent(
            $archiveIntegrityOk,
            $archive,
            $this->cron
        );

        return $archiveIntegrityOk;
    }

    /**
     * Affirme son existance et affiche un rapport
     */
    public function touchAndReport()
    {
        $ntimer = microtime(true);
        if ($this->exec && $ntimer - $this->timer > $this->minDelayBeforeSave) {
            $this->timer = $ntimer;
            $this->out(
                __(
                    "Intégrités contrôlées: {0} / {1}",
                    $this->index,
                    $this->total
                )
            );
            $this->exec->set('last_update', new DateTime());
            $this->CronExecutions->save($this->exec);
        }
        $this->index++;
    }

    /**
     * Vérifi un unique stored_file_volume
     * @param EntityInterface $storedFileVolume
     * @return void
     * @throws VolumeException
     */
    private function checkStoredFileVolume(EntityInterface $storedFileVolume): bool
    {
        $now = new DateTime();
        $notFoundErrors = [
            VolumeException::FILE_NOT_FOUND,
            VolumeException::FILE_READ_ERROR,
        ];
        $bddHash = Hash::get($storedFileVolume, 'stored_file.hash');
        $hashAlgo = Hash::get($storedFileVolume, 'stored_file.hash_algo');
        $storedFileVolume->set('is_integrity_ok');
        $ref = $storedFileVolume->get('storage_ref');
        try {
            /** @var VolumeS3|VolumeFilesystem $driver */
            $volume = $this->getVolume($storedFileVolume->get('volume_id'));
            $hash = $volume->hash($ref, $hashAlgo);
            $hashOk = HashUtility::hashMatch($hash, $bddHash);
            $storedFileVolume->set('is_integrity_ok', $hashOk);
            if (!$hashOk) {
                $this->err(
                    __(
                        "Le fichier {0} (volume id={1}) n'a pas le bon hash "
                        . "(volume: {2} / bdd: {3}) (stored_file_id = {4})",
                        $ref,
                        $storedFileVolume->get('volume_id'),
                        $hash,
                        $bddHash,
                        $storedFileVolume->get('stored_file_id')
                    )
                );
            }
        } catch (VolumeException $e) {
            if (in_array($e->volumeErrorCode, $notFoundErrors)) {
                $storedFileVolume->set('is_integrity_ok', false);
                $this->err(
                    __(
                        "Le fichier {0} (volume id={1}) n'a pas été trouvé (stored_file_id = {2})",
                        $ref,
                        $storedFileVolume->get('volume_id'),
                        $storedFileVolume->get('stored_file_id')
                    )
                );
            } else {
                throw $e;
            }
        }
        $storedFileVolume->set('integrity_date', $now);
        $loc = TableRegistry::getTableLocator();
        /** @var StoredFilesVolumesTable $StoredFiles */
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $StoredFilesVolumes->saveOrFail($storedFileVolume);
        return (bool)$storedFileVolume->get('is_integrity_ok');
    }

    /**
     * Donne le volume par son id
     * @param int $volume_id
     * @return VolumeInterface|false
     * @throws VolumeException
     */
    private function getVolume(int $volume_id)
    {
        if (!isset($this->volumes[$volume_id])) {
            $this->volumes[$volume_id] = VolumeManager::getDriverById($volume_id);
        }
        return $this->volumes[$volume_id];
    }

    /**
     * Remonte l'info is_integrity_ok des stored_files_volumes aux archive_files
     * @param EntityInterface $archive
     * @return bool
     */
    private function updateArchiveFilesIntegrityOk(EntityInterface $archive)
    {
        $archiveIntegrityOk = true;
        /** @var ArchiveFilesTable $ArchiveFiles */
        $ArchiveFiles = TableRegistry::getTableLocator()->get('ArchiveFiles');
        $archiveFilesQuery = $ArchiveFiles->find()
            ->select(['ArchiveFiles.id', 'sfv_id' => 'sfv.id'])
            ->innerJoin(
                ['sf' => 'stored_files'],
                ['sf.id' => new IdentifierExpression('ArchiveFiles.stored_file_id')]
            )
            ->leftJoin(
                ['sfv' => 'stored_files_volumes'],
                [
                    'sfv.stored_file_id' => new IdentifierExpression('sf.id'),
                    'sfv.is_integrity_ok IS false',
                ]
            )
            ->where(['ArchiveFiles.archive_id' => $archive->id])
            ->disableHydration();
        foreach ($archiveFilesQuery as $archiveFile) {
            $integrity = $archiveFile['sfv_id'] === null;
            $archiveIntegrityOk = $archiveIntegrityOk && $integrity;
            $ArchiveFiles->updateAll(
                ['is_integrity_ok' => $integrity],
                ['id' => $archiveFile['id']]
            );
        }
        return $archiveIntegrityOk;
    }

    /**
     * Remonte l'info is_integrity_ok des stored_files_volumes aux archive_binaries
     * @param EntityInterface $archive
     * @param bool            $isTechnicalArchive
     * @return bool
     */
    private function updateArchiveBinariesIntegrityOk(
        EntityInterface $archive,
        bool $isTechnicalArchive = false
    ) {
        $archiveIntegrityOk = true;
        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = TableRegistry::getTableLocator()->get('ArchiveBinaries');
        if ($isTechnicalArchive) {
            $join = 'TechnicalArchiveUnits';
            $joinFk = 'technical_archive_id';
        } else {
            $join = 'ArchiveUnits';
            $joinFk = 'archive_id';
        }
        $archiveBinariesQuery = $ArchiveBinaries->find()
            ->select(['ArchiveBinaries.id', 'sfv_id' => 'sfv.id'])
            ->innerJoinWith($join)
            ->innerJoin(
                ['sf' => 'stored_files'],
                ['sf.id' => new IdentifierExpression('ArchiveBinaries.stored_file_id')]
            )
            ->leftJoin(
                ['sfv' => 'stored_files_volumes'],
                [
                    'sfv.stored_file_id' => new IdentifierExpression('sf.id'),
                    'sfv.is_integrity_ok IS false',
                ]
            )
            ->where(["$join.$joinFk" => $archive->id])
            ->disableHydration();
        foreach ($archiveBinariesQuery as $archiveFile) {
            $integrity = $archiveFile['sfv_id'] === null;
            $archiveIntegrityOk = $archiveIntegrityOk && $integrity;
            $ArchiveBinaries->updateAll(
                ['is_integrity_ok' => $integrity],
                ['id' => $archiveFile['id']]
            );
        }
        return $archiveIntegrityOk;
    }

    /**
     * Ajoute un evenement et l'intègre au cycle de vie de l'archive
     * @param bool            $success
     * @param EntityInterface $archive
     * @param EntityInterface $agent
     * @return bool
     */
    public static function createLifecycleEvent(
        bool $success,
        EntityInterface $archive,
        EntityInterface $agent
    ) {
        if ($success) {
            $msg = __(
                "Intégrité de l'archive ''{0}'' vérifiée avec succès",
                $archive->get('archival_agency_identifier')
            );
        } else {
            $msg = __(
                "Echec lors de la vérification de l'intégrité de l'archive ''{0}''",
                $archive->get('archival_agency_identifier')
            );
        }
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->newEntry(
            'check_archive_integrity',
            $success ? 'success' : 'fail',
            $msg,
            $agent
        );
        $entry->set('in_lifecycle', false);
        $entry->set('object_model', $archive->getSource());
        $entry->set('object_foreign_key', $archive->get('id'));
        $EventLogs->saveOrFail($entry);
        $storedFile = Hash::get(
            $archive,
            'lifecycle_xml_archive_file.stored_file',
            []
        );
        try {
            return $Archives->updateLifecycleXml($storedFile, $entry);
        } catch (Throwable $e) {
            Log::error(
                __(
                    "Mise à jour du cycle de vie de l'archive {0} (id={1}) impossible:\n    {2}",
                    $archive->get('archival_agency_identifier'),
                    $archive->get('id'),
                    $e->getMessage()
                )
            );
            return false;
        }
    }

    /**
     * Effectue le test d'intégrité sur les archives
     * @param Query $storedFilesVolumesQuery
     * @return bool
     * @throws VolumeException
     */
    private function checkTechnicalArchiveIntegrity(Query $storedFilesVolumesQuery): bool
    {
        $failed = false;
        $technicalArchivesConditions = $this->params['context'] === 'user-action'
            ? []
            : [
                'OR' => [
                    ['TechnicalArchives.is_integrity_ok IS' => false],
                    ['TechnicalArchives.is_integrity_ok IS' => null],
                    'TechnicalArchives.integrity_date IS' => null,
                    'TechnicalArchives.integrity_date <' => $this->threshold,
                ],
            ];
        $loc = TableRegistry::getTableLocator();
        /** @var TechnicalArchivesTable $TechnicalArchives */
        $TechnicalArchives = $loc->get('TechnicalArchives');
        $technicalArchivesQuery = $TechnicalArchives->find()
            ->where($technicalArchivesConditions)
            ->orderBy(
                [
                    'TechnicalArchives.is_integrity_ok IS NULL' => 'desc',
                    'TechnicalArchives.is_integrity_ok' => 'asc',
                    'TechnicalArchives.integrity_date' => 'asc',
                    'TechnicalArchives.created' => 'asc',
                ]
            );
        $count = $technicalArchivesQuery->count();
        if ($count === 0) {
            $this->out(__("Aucune vérification d'intégrité n'est nécessaire sur les archives techniques"));
        } else {
            $this->out(
                __n(
                    "Il y a {0} archive technique à vérifier",
                    "Il y a {0} archives techniques à vérifier",
                    $count,
                    $count
                )
            );
        }
        $technicalArchiveSuccesses = 0;
        foreach ($technicalArchivesQuery as $technicalArchive) {
            // Si le process à durée trop longtemps, on s'arrète là
            if (time() > $this->maxTime) {
                $this->out(__("Limite de durée d'exécution atteinte."));
                break;
            }
            if ($this->checkTechnicalArchive($technicalArchive, $storedFilesVolumesQuery)) {
                $technicalArchiveSuccesses++;
            } else {
                $failed = true;
            }
        }
        if ($technicalArchiveSuccesses) {
            $this->out(
                __n(
                    "TechnicalArchive testée avec succès: {0}",
                    "TechnicalArchives testées avec succès: {0}",
                    $technicalArchiveSuccesses,
                    $technicalArchiveSuccesses
                )
            );
        }
        return !$failed;
    }

    /**
     * Vérifie la présence des fichiers d'une archive technique
     * @param EntityInterface $technicalArchive
     * @param Query           $storedFilesVolumes
     * @return bool|null
     * @throws VolumeException
     */
    private function checkTechnicalArchive(EntityInterface $technicalArchive, Query $storedFilesVolumes)
    {
        $loc = TableRegistry::getTableLocator();
        /** @var TechnicalArchivesTable $TechnicalArchives */
        $TechnicalArchives = $loc->get('TechnicalArchives');
        $conn = $TechnicalArchives->getConnection();
        $conn->begin();

        $queryArchiveBinaries = clone $storedFilesVolumes;
        $queryArchiveBinaries
            ->innerJoinWith('StoredFiles.ArchiveBinaries.TechnicalArchiveUnits')
            ->where(['TechnicalArchiveUnits.technical_archive_id' => $technicalArchive->id])
            ->contain(['StoredFiles']);
        $count = $queryArchiveBinaries->count();
        $queryArchiveBinaries->limit(65000);
        for ($i = 0; $i <= $count; $i += 65000) {
            $queryArchiveBinaries->offset($i);
            foreach ($queryArchiveBinaries as $storedFileVolume) {
                $this->touchAndReport();
                $this->checkStoredFileVolume($storedFileVolume);
            }
        }
        $archiveIntegrityOk = $this->updateArchiveBinariesIntegrityOk($technicalArchive, true);

        $technicalArchive->set('is_integrity_ok', $archiveIntegrityOk);
        $technicalArchive->set('integrity_date', new DateTime());
        $TechnicalArchives->saveOrFail($technicalArchive);
        self::createTechnicalLifecycleEvent(
            $archiveIntegrityOk,
            $technicalArchive,
            $this->cron
        );
        $conn->commit();

        return $archiveIntegrityOk;
    }

    /**
     * Ajoute un evenement d'archive technique
     * @param bool            $success
     * @param EntityInterface $archive
     * @param EntityInterface $agent
     */
    public static function createTechnicalLifecycleEvent(
        bool $success,
        EntityInterface $archive,
        EntityInterface $agent
    ) {
        if ($success) {
            $msg = __(
                "Intégrité de l'archive technique ''{0}'' vérifiée avec succès",
                $archive->get('archival_agency_identifier')
            );
        } else {
            $msg = __(
                "Echec lors de la vérification de l'intégrité de l'archive technique ''{0}''",
                $archive->get('archival_agency_identifier')
            );
        }
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->newEntry(
            'check_technical_archive_integrity',
            $success ? 'success' : 'fail',
            $msg,
            $agent
        );
        $entry->set('in_lifecycle', false);
        $entry->set('object_model', $archive->getSource());
        $entry->set('object_foreign_key', $archive->get('id'));
        $EventLogs->saveOrFail($entry);
    }
}
