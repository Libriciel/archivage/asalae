<?php

/**
 * Asalae\Cron\ChangeVolumesInSpace
 */

namespace Asalae\Cron;

use Asalae\Model\Entity\Volume;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Driver\Volume\VolumeInterface;
use Cake\Console\ConsoleOutput;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Cron de modification des volumes d'un espace de stockage
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ChangeVolumesInSpace implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;

    /**
     * @var int durée d'attente entre 2 essais
     */
    public static int $sleepTime = 1;
    /**
     * @var float nombre de secondes entre 2 affichages de l'avancement des uploads (ajout d'un volume)
     */
    public static float $feedBackCooldown = 30.0;
    /**
     * @var array
     */
    public $params;
    /**
     * @var VolumeManager
     */
    private VolumeManager $VolumeManager;
    /**
     * @var string[]
     */
    private array $changeVolumeLogFile;

    /**
     * Constructeur de la classe du cron
     * @param array              $params paramètres additionnels
     *                                   du cron
     * @param ConsoleOutput|null $out
     * @param ConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->params = $params;
        $this->initializeOutput($out, $err);
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [
            'ignore_upload_errors' => [
                'type' => 'checkbox',
                'label' => __("Ignorer les erreurs lors de l'upload d'un fichier (non recommandé)"),
            ],
            'ignore_delete_errors' => [
                'type' => 'checkbox',
                'label' => __("Ignorer les erreurs lors de la suppression d'un fichier (non recommandé)"),
            ],
        ];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws VolumeException
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        $loc = TableRegistry::getTableLocator();
        $Spaces = $loc->get('SecureDataSpaces');
        $space = $Spaces->get($this->params['space_id']);
        $this->out(
            __(
                "Modification des volumes de l'espace {0} (uid={1})",
                h($space->get('name')),
                $this->params['uid']
            )
        );
        $success = true;
        if (!empty($this->params['add'])) {
            $success = $this->addVolumes($space, $this->params['add']);
        }
        if (!empty($this->params['remove']) && $success) {
            return $this->removeVolumes($space, $this->params['remove']);
        }
        return $success ? 'success' : 'error';
    }

    /**
     * Ajoute un ou plusieurs volumes à l'espace de stockage
     * @param EntityInterface $space
     * @param array           $add
     * @return bool
     * @throws VolumeException
     */
    private function addVolumes(EntityInterface $space, array $add): bool
    {
        $loc = TableRegistry::getTableLocator();
        $StoredFiles = $loc->get('StoredFiles');
        $Volumes = $loc->get('Volumes');
        $volumes = $Volumes->find()->where(
            [
                'id IN' => $add,
                'secure_data_space_id IS' => null,
            ]
        );

        $current = $StoredFiles->find()
            ->select(
                [
                    'count' => $StoredFiles->query()->func()->count('*'),
                    'used' => $StoredFiles->query()->func()->sum('size'),
                ]
            )
            ->where(['secure_data_space_id' => $space->get('id')])
            ->all()
            ->first();
        $currentSpaceSize = (int)$current->get('used');
        $currentCount = $current->get('count');
        $drivers = [];

        /** @var EntityInterface $volume */
        foreach ($volumes as $volume) {
            $driver = VolumeManager::getDriver($volume);
            $test = $driver->test();
            if (empty($test['success'])) {
                $this->out(
                    __(
                        "Le test d'écriture sur le volume {0} a échoué",
                        h($volume->get('name'))
                    )
                );
                return false;
            }
            if ($volume->get('disk_usage') > 0) {
                $this->out(
                    __(
                        "Des données existent déjà dans le volume {0}",
                        h($volume->get('name'))
                    )
                );
                return false;
            }
            if ($volume->get('max_disk_usage') < $currentSpaceSize) {
                $this->out(
                    __(
                        "La taille du volume {0} est insuffisante",
                        h($volume->get('name'))
                    )
                );
                return false;
            }
            $drivers[] = [
                'volume' => $volume,
                'driver' => $driver,
            ];
        }

        $logFile = $this->getLogFilename($space->get('id'));
        if (is_file($logFile)) {
            unlink($logFile);
        }
        // si il n'y a pas de volume dans l'espace...
        if (
            !$Volumes->exists(
                ['secure_data_space_id' => $this->params['space_id']]
            )
        ) {
            foreach ($drivers as $data) {
                /** @var EntityInterface $volume */
                $volume = $data['volume'];
                $name = h($volume->get('name'));
                $Volumes->patchEntity(
                    $volume,
                    [
                        'secure_data_space_id' => $space->get('id'),
                        'disk_usage' => 0,
                    ]
                );
                if (!$Volumes->save($volume)) {
                    $this->out(
                        __("L'attribution du volume {0} a échoué", $name)
                    );
                    return false;
                }
            }
            return true;
        }
        $this->VolumeManager = new VolumeManager($space->get('id'));
        $success = true;
        foreach ($drivers as $data) {
            if (!$this->copyFilesTo($data, $currentCount, $currentSpaceSize, $space)) {
                $success = false;
            }
        }
        return $success;
    }

    /**
     * Chemin du fichier de log
     * @param int $secureDataSpace
     * @return string
     */
    private function getLogFilename(int $secureDataSpace): string
    {
        if (empty($this->changeVolumeLogFile[$secureDataSpace])) {
            $this->changeVolumeLogFile[$secureDataSpace] = sprintf(
                '%schange_volume_%d.log',
                Configure::read('App.paths.logs', LOGS),
                $secureDataSpace
            );
        }
        return $this->changeVolumeLogFile[$secureDataSpace];
    }

    /**
     * Ajoute les fichiers sur le driver $data
     * @param array           $data
     * @param int             $currentCount
     * @param int             $currentSpaceSize
     * @param EntityInterface $space
     * @return bool
     */
    private function copyFilesTo(
        array $data,
        int $currentCount,
        int $currentSpaceSize,
        EntityInterface $space
    ): bool {
        $loc = TableRegistry::getTableLocator();
        $StoredFiles = $loc->get('StoredFiles');
        $Volumes = $loc->get('Volumes');
        /** @var EntityInterface $volume */
        $volume = $data['volume'];
        /** @var VolumeInterface $driver */
        $driver = $data['driver'];
        $name = h($volume->get('name'));
        $this->out(
            __(
                "Copie de {0,number,#,###} fichiers vers {1}",
                $currentCount,
                $name
            )
        );
        $query = $StoredFiles->find()
            ->select(['id', 'name'])
            ->where(['secure_data_space_id' => $space->get('id')])
            ->disableHydration();
        $count = $query->count();
        $success = true;
        $conn = $StoredFiles->getConnection();
        try {
            $mtime = microtime(true);
            foreach ($query as $i => $storedFile) {
                if (microtime(true) - $mtime > self::$feedBackCooldown) {
                    $mtime = microtime(true);
                    $this->out(sprintf('%d / %d', $i + 1, $count));
                }
                try {
                    $conn->begin();
                    $this->uploadToVolume($storedFile['name'], $driver, $volume->id, $space->get('id'));
                    $conn->commit();
                } catch (Exception $e) {
                    $conn->rollback();
                    if (empty($this->params['ignore_upload_errors'])) {
                        throw $e;
                    } else {
                        $this->err($e->getMessage());
                        $this->out(
                            __(
                                "Echec lors de la copie de {0} vers {1}",
                                h($storedFile['name'] ?? '""'),
                                $name
                            )
                        );
                    }
                }
            }
        } catch (Exception $e) {
            $this->err($e->getMessage());
            $this->out(
                __(
                    "Echec lors de la copie de {0} vers {1}",
                    h($storedFile['name'] ?? '""'),
                    $name
                )
            );
            $success = false;
        }
        $Volumes->patchEntity(
            $volume,
            [
                'secure_data_space_id' => $space->get('id'),
                'disk_usage' => $currentSpaceSize,
            ]
        );
        if (!$Volumes->save($volume) && $success) {
            $this->out(
                __(
                    "Les fichiers ont bien été copiés dans {0} mais l'attribution du volume a échoué",
                    $name
                )
            );
            return false;
        }
        return $success;
    }

    /**
     * Effectu l'upload
     * @param string          $name
     * @param VolumeInterface $driver
     * @param int             $volume_id
     * @param int             $space_id
     * @param int             $i
     * @return void
     * @throws VolumeException
     */
    private function uploadToVolume(
        string $name,
        VolumeInterface $driver,
        int $volume_id,
        int $space_id,
        int $i = 0
    ) {
        try {
            $this->VolumeManager->uploadToVolume(
                $name,
                $driver,
                $volume_id
            );
            file_put_contents(
                $this->getLogFilename($space_id),
                sprintf(
                    "%s - added %s to volume %s\n",
                    time(),
                    h($name),
                    $name
                ),
                FILE_APPEND
            );
        } catch (VolumeException $e) {
            if ($i >= 9 || $e->volumeErrorCode === VolumeException::FILE_NOT_FOUND) {
                throw $e;
            }
            $this->out($e->getMessage());
            $this->out(
                __(
                    "Nouvelle tentative d'upload de {0} vers le volume {1} dans {2} seconde...",
                    $name,
                    $volume_id,
                    self::$sleepTime
                )
            );
            sleep(self::$sleepTime); // NOSONAR
            $this->uploadToVolume($name, $driver, $volume_id, $space_id, $i + 1);
        }
    }

    /**
     * Retire un volume d'un espace de stockage et supprime les fichiers qu'il y
     * a dessus
     * @param EntityInterface $space
     * @param array           $remove
     * @return string success|warning|error
     * @throws VolumeException
     */
    private function removeVolumes(
        EntityInterface $space,
        array $remove
    ): string {
        $loc = TableRegistry::getTableLocator();
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $StoredFiles = $loc->get('StoredFiles');
        $Volumes = $loc->get('Volumes');
        $volumes = $Volumes->find()
            ->where(['id IN' => $remove]);
        $current = $StoredFiles->find()
            ->select(
                [
                    'count' => $StoredFiles->query()->func()->count('*'),
                    'used' => $StoredFiles->query()->func()->sum('size'),
                ]
            )
            ->where(['secure_data_space_id' => $space->get('id')])
            ->all()
            ->first();
        $currentCount = $current->get('count');
        $drivers = [];
        $outputCode = 'success';

        /** @var EntityInterface $volume */
        foreach ($volumes as $volume) {
            try {
                $driver = VolumeManager::getDriver($volume);
            } catch (VolumeException $e) {
                if ($e->volumeErrorCode === VolumeException::VOLUME_NOT_ACCESSIBLE) {
                    $Volumes->patchEntity(
                        $volume,
                        ['secure_data_space_id' => null]
                    );
                    $StoredFilesVolumes->deleteAll(
                        ['volume_id' => $volume->id]
                    );
                    if ($Volumes->save($volume)) {
                        $this->out(
                            __(
                                "Le volume {0} a été retiré sans supprimer"
                                . " ses fichiers car il n'étais pas accessible",
                                $volume->get('name')
                            )
                        );
                        $outputCode = 'warning';
                    } else {
                        $this->err(
                            __(
                                "Le retrait du volume {0} a échoué",
                                $volume->get('name')
                            )
                        );
                        return 'error';
                    }
                    continue;
                }
                throw $e;
            }
            $test = $driver->test();
            if (empty($test['success'])) {
                $this->err(
                    __(
                        "Le test d'écriture sur le volume {0} a échoué",
                        h($volume->get('name'))
                    )
                );
                $outputCode = 'warning';
            } else {
                $drivers[] = [
                    'volume' => $volume,
                    'driver' => $driver,
                ];
            }
        }

        $logFile = Configure::read('ChangeVolumesInSpace.log_dir', TMP)
            . 'change_volume_' . $space->get('id') . '.log';
        foreach ($drivers as $data) {
            /** @var EntityInterface $volume */
            $volume = $data['volume'];
            /** @var VolumeInterface|VolumeFilesystem $driver */
            $driver = $data['driver'];

            // véfication s'il existe au moins 1 stored_file avec is_integrity = false
            $integrityError = $StoredFiles->find()
                ->leftJoin(
                    ['StoredFilesVolumes' => 'stored_files_volumes'],
                    [
                        'StoredFilesVolumes.stored_file_id' => new IdentifierExpression('StoredFiles.id'),
                        'StoredFilesVolumes.is_integrity_ok IS false',
                    ]
                )
                ->where(
                    [
                        'StoredFiles.secure_data_space_id' => $space->get('id'),
                        'StoredFilesVolumes.volume_id !=' => $volume->get('id'),
                    ]
                )
                ->count();
            if ($integrityError) {
                $this->out(
                    __("Il y a {0} fichier(s) à l'intégrité en échec sur les volumes restants", $integrityError)
                );
                return 'error';
            }

            $name = h($volume->get('name'));
            $this->out(
                __(
                    "Suppression de {0,number,#,###} fichiers du volume {1}",
                    $currentCount,
                    $name
                )
            );

            $otherVolumes = $Volumes->find()
                ->where(
                    [
                        'secure_data_space_id' => $space->get('id'),
                        'id IS NOT' => $volume->get('id'),
                    ]
                )
                ->orderBy(['read_duration' => 'ASC'])
                ->all();
            $volsCount = count($otherVolumes);

            $query = $StoredFilesVolumes->find()
                ->select(['id', 'storage_ref', 'stored_file_id'])
                ->where(['volume_id' => $volume->get('id')])
                ->disableHydration();
            // vérification de la présence des fichiers sur les autres disques
            foreach ($query as $storedFile) {
                $onOtherVolumes = $StoredFilesVolumes->find()
                    ->where(
                        [
                            'volume_id !=' => $volume->get('id'),
                            'stored_file_id' => $storedFile['stored_file_id'],
                        ]
                    )
                    ->count();
                if (!$onOtherVolumes) {
                    $this->out(
                        __(
                            "Le fichier ''{0}'' (id: {1}) est présent sur le volume ''{2}'' (id : {3})"
                            . " mais pas sur les autres volumes de l'ECS",
                            $storedFile['storage_ref'],
                            $storedFile['id'],
                            $volume->get('name'),
                            $volume->get('id')
                        )
                    );
                    return 'error';
                } elseif ($onOtherVolumes < ($volsCount - 1)) {
                    $this->out(
                        __(
                            "Le fichier '{0}' (id: {1})  est présent sur le volume '{2}' (id : {3})"
                            . " mais pas sur tous les autres volumes de l'ECS,"
                            . " veuillez lancer la commande 'volume_manager repair'",
                            $storedFile['storage_ref'],
                            $storedFile['id'],
                        )
                    );
                    $outputCode = 'warning';
                }

                // vérification de l'existence des fichiers sur au moins un autre volume
                /** @var Volume $otherVol */
                $exists = false;
                foreach ($otherVolumes as $otherVol) {
                    $otherDriver = VolumeManager::getDriver($otherVol);
                    if ($otherDriver->fileExists($storedFile['storage_ref'])) {
                        $exists = true;
                        break;
                    }
                }
                if (!$exists) {
                    $this->out(
                        __("Le fichier ''{0}'' n'existe pas sur les autres volumes", $storedFile['storage_ref'])
                    );
                    return 'error';
                }
            }

            $conn = $StoredFilesVolumes->getConnection();
            try {
                foreach ($query as $storedFile) {
                    try {
                        $conn->begin();
                        $driver->fileDelete($storedFile['storage_ref']);
                        $StoredFilesVolumes->deleteAll(['id' => $storedFile['id']]);
                        file_put_contents(
                            $logFile,
                            sprintf(
                                "%s - removed %s from volume %s\n",
                                time(),
                                h($storedFile['storage_ref']),
                                $name
                            ),
                            FILE_APPEND
                        );
                        $conn->commit();
                    } catch (Exception $e) {
                        $conn->rollback();
                        if (empty($this->params['ignore_delete_errors'])) {
                            throw $e;
                        } else {
                            $this->err($e->getMessage());
                            $this->out(
                                __(
                                    "Echec lors de la suppression de {0} sur {1}",
                                    h($storedFile['storage_ref'] ?? '""'),
                                    $name
                                )
                            );
                        }
                    }
                }
            } catch (Exception) {
                $this->out(
                    __(
                        "Echec lors de la suppression de {0} sur {1}",
                        h($storedFile['storage_ref'] ?? '""'),
                        $name
                    )
                );
                $outputCode = 'warning';
            }
            $Volumes->patchEntity(
                $volume,
                [
                    'secure_data_space_id' => null,
                    'disk_usage' => 0,
                ]
            );
            if (!$Volumes->save($volume)) {
                $this->out(
                    __(
                        "Les fichiers ont bien été supprimés dans {0} mais le retrait du volume a échoué",
                        $name
                    )
                );
                return 'error';
            }
        }
        return $outputCode;
    }
}
