<?php

/**
 * Asalae\Cron\DestructionCertificateCreation
 */

namespace Asalae\Cron;

use Asalae\Model\Entity\DestructionRequest;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Entity\RestitutionRequest;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\DestructionRequestsTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Console\ConsoleOutput;
use AsalaeCore\Cron\CronInterface;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\TableRegistry;

/**
 * Génération des attestations d'élimination après le délai de destruction des sauvegardes.
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DestructionCertificateCreation implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;

    /**
     * Constructeur de la classe du cron
     * @param array              $params paramètres additionnels du cron
     * @param ConsoleOutput|null $out
     * @param ConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     */
    public function work(EntityInterface $exec = null, EntityInterface $cron = null): string
    {
        $this->destructionRequests();
        $this->restitutionRequests();

        $this->out('<success>done</success>');
        return 'success';
    }

    /**
     * Génère l'attestation d'élimination d'une demande d'élimination
     * @return void
     */
    private function destructionRequests()
    {
        $loc = TableRegistry::getTableLocator();
        $DestructionRequests = $loc->get('DestructionRequests');

        $query = $DestructionRequests->find()
            ->where(
                [
                    'DestructionRequests.state' => DestructionRequestsTable::S_DESTROYED,
                    'OR' => [
                        'DestructionRequests.certified IS NOT' => true,
                        'DestructionRequests.certified IS' => null,
                    ],
                ]
            )
            ->contain(['ArchivalAgencies']);

        $destructionDelay = Configure::read('Attestations.destruction_delay', 0);
        $today = new CakeDateTime('00:00:00');

        /** @var DestructionRequest $destructionRequest */
        foreach ($query as $destructionRequest) {
            /** @var OrgEntity $archivalAgency */
            $archivalAgency = $destructionRequest->get('archival_agency');
            $intermediate = $archivalAgency->getConfig(
                ConfigurationsTable::CONF_ATTEST_DESTR_CERT_INTERMEDIATE,
                false
            );
            /** @var CakeDateTime $createdDate */
            $createdDate = $destructionRequest->get('destroyed_date')
                ?: $destructionRequest->get('created');
            $createdDate = new CakeDateTime($createdDate->format('Y-m-d'));
            $diffInDays = $createdDate->diffInDays($today);
            if (!$destructionDelay || $destructionDelay <= $diffInDays) {
                $this->out(
                    __(
                        "Création de l'attestation définitive pour l'élimination {0}",
                        $destructionRequest->get('identifier')
                    )
                );
                $destructionRequest->get('certification');
                $DestructionRequests->save($destructionRequest);
            } elseif ($intermediate && $destructionRequest->get('certified') === null) {
                $this->out(
                    __(
                        "Création de l'attestation intermédiaire pour l'élimination {0}",
                        $destructionRequest->get('identifier')
                    )
                );
                $destructionRequest->get('certification');
                $DestructionRequests->save($destructionRequest);
            }
        }
    }

    /**
     * Génère l'attestation d'élimination suite à une demande de restitution
     * @return void
     */
    private function restitutionRequests()
    {
        $loc = TableRegistry::getTableLocator();
        $RestitutionRequests = $loc->get('RestitutionRequests');

        $query = $RestitutionRequests->find()
            ->where(
                [
                    'RestitutionRequests.state' => RestitutionRequestsTable::S_RESTITUTED,
                    'OR' => [
                        'RestitutionRequests.destruction_certified IS NOT' => true,
                        'RestitutionRequests.destruction_certified IS' => null,
                    ],
                ]
            )
            ->contain(['ArchivalAgencies']);

        $restitutionDelay = Configure::read('Attestations.destruction_delay', 0);
        $today = new CakeDateTime('00:00:00');

        /** @var RestitutionRequest $restitutionRequest */
        foreach ($query as $restitutionRequest) {
            /** @var OrgEntity $archivalAgency */
            $archivalAgency = $restitutionRequest->get('archival_agency');
            $intermediate = $archivalAgency->getConfig(
                ConfigurationsTable::CONF_ATTEST_DESTR_CERT_INTERMEDIATE,
                false
            );
            /** @var CakeDateTime $createdDate */
            $createdDate = $restitutionRequest->get('restituted_date')
                ?: $restitutionRequest->get('created');
            $createdDate = new CakeDateTime($createdDate->format('Y-m-d'));
            $diffInDays = $createdDate->diffInDays($today);
            if (!$restitutionDelay || $restitutionDelay <= $diffInDays) {
                $this->out(
                    __(
                        "Création de l'attestation définitive pour la restitution {0}",
                        $restitutionRequest->get('identifier')
                    )
                );
                $restitutionRequest->get('destruction_certification');
                $RestitutionRequests->save($restitutionRequest);
            } elseif ($intermediate && $restitutionRequest->get('destruction_certified') === null) {
                $this->out(
                    __(
                        "Création de l'attestation intermédiaire pour la restitution {0}",
                        $restitutionRequest->get('identifier')
                    )
                );
                $restitutionRequest->get('destruction_certification');
                $RestitutionRequests->save($restitutionRequest);
            }
        }
    }
}
