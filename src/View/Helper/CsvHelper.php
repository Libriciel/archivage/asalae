<?php

/**
 * Asalae\View\Helper\CsvHelper
 */

namespace Asalae\View\Helper;

use AsalaeCore\View\Helper\FormHelper;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\Number;
use Cake\ORM\Query;
use Cake\ORM\ResultSet;
use Cake\Utility\Hash;
use Cake\View\Helper;
use DateTimeInterface;
use Exception;

/**
 * Génère un CSV pour un tableau de résultat classique
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FormHelper Form
 */
class CsvHelper extends Helper
{
    /**
     * Runtime config
     *
     * @var array<string, mixed>
     */
    protected array $_defaultConfig = [
        'separator' => ',',
        'escapeChar' => '"',
        'newLine' => "\r\n",
    ];

    /**
     * Génère un CSV de tableau de résultats
     * @param array                                     $cols
     * @param Query|ResultSet|EntityInterface[]|array[] $results
     * @param callable|null                             $map
     * @return string
     */
    public function resultTable(array $cols, $results, callable $map = null): string
    {
        if ($results instanceof Query) {
            try {
                $results = $this->queryToArray($results, $map);
            } catch (Exception) {
            }
        } elseif ($map) {
            $results = array_map($map, $results);
        }
        $esc = $this->getConfig('escapeChar', '"');
        $nl = $this->getConfig('newLine', "\r\n");
        $impl = $this->getConfig('separator', ',');

        foreach ($cols as $key => $value) {
            $cols[$key] = str_replace('"', '""', $value);
        }

        // LIGNE 1 : Noms de colonnes
        $text = $esc . implode($esc . $impl . $esc, $cols) . $esc . $nl;

        // LIGNE 2+ : résultats
        foreach ($results as $entity) {
            $row = [];
            foreach (array_keys($cols) as $path) {
                $value = Hash::get($entity, $path);
                if ($value instanceof CakeDate || $value instanceof DateTimeInterface) {
                    $value = (string)$value;
                } elseif (is_object($value)) {
                    $value = $esc . str_replace('"', '""', $value) . $esc;
                } elseif (is_bool($value)) {
                    $value = $value ? __("Oui") : __("Non");
                } elseif (is_string($value) && $value) {
                    $value = $esc . str_replace('"', '""', $value) . $esc;
                } elseif (is_float($value)) {
                    $value = $esc . Number::format(
                        $value,
                        ['precision' => 13, 'pattern' => '0.#############']
                    ) . $esc;
                }
                $row[] = $value;
            }
            $text .= implode($impl, $row) . $nl;
        }
        return $text;
    }

    /**
     * Evite la barrière des 65k resultats
     * @param Query         $query
     * @param callable|null $map
     * @return array
     */
    private function queryToArray(Query $query, callable $map = null): array
    {
        $limit = $query->clause('limit');
        $count = $query->count();
        if ($limit > 65535 && $count > 65535) {
            $loader = $query->getEagerLoader();
            $prevLoader = clone $loader;
            $results = [];
            foreach (range(0, intdiv($count, 65535)) as $iteration) {
                $cloneQuery = clone $query;
                $cloneQuery = $cloneQuery->limit(65535)
                    ->offset(65535 * $iteration);
                $cloneQuery->setEagerLoader($prevLoader);
                $cloneQuery = $cloneQuery->all();
                if ($map) {
                    $cloneQuery = $cloneQuery->map($map);
                }
                $results = array_merge($results, $cloneQuery->toArray());
            }
            return $results;
        } else {
            $query = $query->all();
            if ($map) {
                $query = $query->map($map);
            }
            return $query->toArray();
        }
    }
}
