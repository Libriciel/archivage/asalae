<?php

/**
 * Asalae\View\Helper\TranslateHelper
 */

namespace Asalae\View\Helper;

use AsalaeCore\View\Helper\TranslateHelper as CoreTranslateHelper;
use Cake\I18n\I18n;
use Cake\View\Helper\HtmlHelper;

/**
 * Centralise certaines traductions
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property HtmlHelper $Html
 */
class TranslateHelper extends CoreTranslateHelper
{
    /**
     * Permet d'obtenir la traduction pour un controller / action dans un
     * contexte de droits
     *
     * @param string      $controller
     * @param string|null $action
     * @return string
     */
    public function permission(
        string $controller,
        string $action = null
    ): string {
        if ($action === null) {
            return $this->translateController($controller);
        }
        switch ($controller) {
            case 'Archives':
                $translation = $this->translateArchives($action);
                break;
            case 'ArchiveBinaries':
                $translation = $this->translateArchiveBinaries($action);
                break;
            case 'ArchiveUnits':
                $translation = $this->translateArchiveUnits($action);
                break;
            case 'Deliveries':
                $translation = $this->translateDeliveries($action);
                break;
            case 'DeliveryRequests':
                $translation = $this->translateDeliveryRequests($action);
                break;
            case 'DestructionNotifications':
                $translation = $this->translateDestructionNotifications($action);
                break;
            case 'DestructionRequests':
                $translation = $this->translateDestructionRequests($action);
                break;
            case 'KeywordLists':
                $translation = $this->translateKeywordLists($action);
                break;
            case 'Ldaps':
                $translation = $this->translateLdaps($action);
                break;
            case 'OutgoingTransferRequests':
                $translation = $this->translateOutgoingTransferRequests($action);
                break;
            case 'OutgoingTransfers':
                $translation = $this->translateOutgoingTransfers($action);
                break;
            case 'RestitutionRequests':
                $translation = $this->translateRestitutionRequests($action);
                break;
            case 'Restitutions':
                $translation = $this->translateRestitutions($action);
                break;
            case 'Restservices':
                $translation = $this->translateRestservices($action);
                break;
            case 'Tasks':
                $translation = $this->translateTasks($action);
                break;
            case 'Transfers':
                $translation = $this->translateTransfers($action);
                break;
            case 'Users':
                $translation = $this->translateUsers($action);
                break;
            case 'ValidationProcesses':
                $translation = $this->translateValidationProcesses($action);
                break;
            default:
                $translation = null;
        }
        return empty($translation) ? $this->translateAction($controller, $action) : $translation;
    }

    /**
     * Donne la traduction d'un controlleur, sans action
     * @param string $controller
     * @return string
     */
    private function translateController(string $controller): string
    {
        $translation = [
            'AccessRuleCodes' => __d('permission', "Codes des restrictions d'accès seda v2"),
            'Agreements' => __d('permission', "Accords de versement"),
            'AppraisalRuleCodes' => __d('permission', "Codes des durées d'utilité administrative seda v2"),
            'Archives' => __d('permission', "Registre des entrées"),
            'ArchiveBinaries' => __d('permission', "Fichiers d'archives"),
            'ArchiveKeywords' => __d('permission', "Mots-clés d'archives"),
            'ArchiveUnits' => __d('permission', "Unités d'archives"),
            'BatchTreatments' => __d('permission', "Traitements par lot"),
            'Counters' => __d('permission', "Compteurs"),
            'DeliveryRequests' => __d('permission', "Demandes de communication"),
            'Deliveries' => __d('permission', "Communications"),
            'DestructionNotifications' => __d('permission', "DestructionNotifications"),
            'DestructionRequests' => __d('permission', "DestructionRequests"),
            'KeywordLists' => __d('permission', "Mots-clés"),
            'OrgEntities' => __d('permission', "Entités"),
            'OutgoingTransferRequests' => __d('permission', "Demandes de transferts sortants"),
            'OutgoingTransfers' => __d('permission', "Transferts sortants"),
            'RestitutionRequests' => __d('permission', "Demandes de restitutions"),
            'Restitutions' => __d('permission', "Restitutions"),
            'Users' => __d('permission', "Utilisateurs"),
            'Roles' => __d('permission', "Rôles utilisateur"),
            'Profiles' => __d('permission', "Profils d'archives"),
            'ServiceLevels' => __d('permission', "Niveaux de service"),
            'Restservices' => __d('permission', "Webservice asalae v1"),
            'Transfers' => __d('permission', "Transferts"),
            'Tasks' => __d('permission', "Jobs en cours"),
            'TechnicalArchives' => __d('permission', "Archives techniques"),
            'ValidationChains' => __d('permission', "Circuits de validation"),
            'ValidationProcesses' => __d('permission', "Validation"),
            'Webservices' => __d('permission', "Webservices"),
        ];
        return $translation[$controller] ?? I18n::getTranslator('permission')->translate($controller);
    }

    /**
     * Traductions du controlleur Archives
     * @param string $action
     * @return string|void
     */
    private function translateArchives(string $action)
    {
        switch ($action) {
            case 'description':
                return __dx('permission', 'Archives', 'description');
            case 'search':
                return __dx('permission', 'Archives', 'search');
            case 'checkIntegrity':
                return __dx('permission', 'Archives', 'checkIntegrity');
            case 'lifecycle':
                return __dx('permission', 'Archives', 'lifecycle');
            case 'treatments':
                return __dx('permission', 'Archives', 'treatments');
            case 'freeze':
                return __dx('permission', 'Archives', "Geler/dégeler");
            case 'archivingCertificate':
                return __dx('permission', 'Archives', "Téléchargement des attestations d'archivage");
        }
    }

    /**
     * Traductions du controlleur ArchiveBinaries
     * @param string $action
     * @return string|void
     */
    private function translateArchiveBinaries(string $action)
    {
        if ($action === 'download') {
            return __dx('permission', 'Archives', "Téléchargement");
        }
    }

    /**
     * Traductions du controlleur ArchiveUnits
     * @param string $action
     * @return string|void
     */
    private function translateArchiveUnits(string $action)
    {
        switch ($action) {
            case 'catalog':
                return __dx('permission', 'ArchiveUnits', 'catalog');
            case 'catalogMyEntity':
                return __dx('permission', 'ArchiveUnits', 'catalogMyEntity');
            case 'duaExpired':
                return __dx('permission', 'ArchiveUnits', 'duaExpired');
            case 'returnable':
                return __dx('permission', 'ArchiveUnits', 'returnable');
            case 'transferable':
                return __dx('permission', 'ArchiveUnits', 'transferable');
            case 'index':
                return __dx(
                    'permission',
                    'ArchiveUnits',
                    "Archives consultables"
                );
            case 'eliminable':
                return __dx(
                    'permission',
                    'ArchiveUnits',
                    "Archives éliminables"
                );
        }
    }

    /**
     * Traductions du controlleur Deliveries
     * @param string $action
     * @return string|void
     */
    private function translateDeliveries(string $action)
    {
        switch ($action) {
            case 'acquittal':
                return __dx('permission', 'Deliveries', 'acquittal');
            case 'retrieval':
                return __dx('permission', 'Deliveries', 'retrieval');
        }
    }

    /**
     * Traductions du controlleur DeliveryRequests
     * @param string $action
     * @return string|void
     */
    private function translateDeliveryRequests(string $action)
    {
        switch ($action) {
            case 'indexPreparating':
                return __dx(
                    'permission',
                    'DeliveryRequests',
                    'indexPreparating'
                );
            case 'indexMy':
                return __dx('permission', 'DeliveryRequests', 'indexMy');
            case 'indexAll':
                return __dx('permission', 'DeliveryRequests', 'indexAll');
        }
    }

    /**
     * Traductions du controlleur DestructionNotifications
     * @param string $action
     * @return string|void
     */
    private function translateDestructionNotifications(string $action)
    {
        if ($action === 'destructionCertificate') {
            return __dx(
                'permission',
                'DestructionRequests',
                "Téléchargement de l'attestation d'élimination"
            );
        }
    }

    /**
     * Traductions du controlleur DestructionRequests
     * @param string $action
     * @return string|void
     */
    private function translateDestructionRequests(string $action)
    {
        switch ($action) {
            case 'indexMy':
                return __dx('permission', 'DestructionRequests', 'indexMy');
            case 'indexAll':
                return __dx(
                    'permission',
                    'DestructionRequests',
                    'indexAll'
                );
            case 'indexPreparating':
                return __dx(
                    'permission',
                    'DestructionRequests',
                    'indexPreparating'
                );
        }
    }

    /**
     * Traductions du controlleur KeywordLists
     * @param string $action
     * @return string|void
     */
    private function translateKeywordLists(string $action)
    {
        if ($action == 'newVersion') {
            return __dx('permission', 'KeywordLists', 'newVersion');
        }
    }

    /**
     * Traductions du controlleur Ldaps
     * @param string $action
     * @return string|void
     */
    private function translateLdaps(string $action)
    {
        if ($action == 'importUsers') {
            return __dx('permission', 'Ldaps', 'importUsers');
        }
    }

    /**
     * Traductions du controlleur OutgoingTransferRequests
     * @param string $action
     * @return string|void
     */
    private function translateOutgoingTransferRequests(string $action)
    {
        switch ($action) {
            case 'indexAll':
                return __dx(
                    'permission',
                    'OutgoingTransferRequests',
                    'indexAll'
                );
            case 'indexMy':
                return __dx(
                    'permission',
                    'OutgoingTransferRequests',
                    'indexMy'
                );
            case 'indexPreparating':
                return __dx(
                    'permission',
                    'OutgoingTransferRequests',
                    'indexPreparating'
                );
            case 'indexProcessed':
                return __dx(
                    'permission',
                    'OutgoingTransferRequests',
                    'indexProcessed'
                );
        }
    }

    /**
     * Traductions du controlleur OutgoingTransfers
     * @param string $action
     * @return string|void
     */
    private function translateOutgoingTransfers(string $action)
    {
        switch ($action) {
            case 'indexAccepted':
                return __dx(
                    'permission',
                    'OutgoingTransfers',
                    'indexAccepted'
                );
            case 'indexAll':
                return __dx('permission', 'OutgoingTransfers', 'indexAll');
            case 'indexPreparating':
                return __dx(
                    'permission',
                    'OutgoingTransfers',
                    'indexPreparating'
                );
            case 'indexRejected':
                return __dx(
                    'permission',
                    'OutgoingTransfers',
                    'indexRejected'
                );
        }
    }

    /**
     * Traductions du controlleur RestitutionRequests
     * @param string $action
     * @return string|void
     */
    private function translateRestitutionRequests(string $action)
    {
        switch ($action) {
            case 'indexAcquitted':
                return __dx(
                    'permission',
                    'RestitutionRequests',
                    'indexAcquitted'
                );
            case 'indexAll':
                return __dx(
                    'permission',
                    'RestitutionRequests',
                    'indexAll'
                );
            case 'indexMy':
                return __dx('permission', 'RestitutionRequests', 'indexMy');
            case 'indexPreparating':
                return __dx(
                    'permission',
                    'RestitutionRequests',
                    'indexPreparating'
                );
            case 'restitutionCertificate':
                return __dx(
                    'permission',
                    'RestitutionRequests',
                    "Téléchargement des attestations de restitution"
                );
        }
    }

    /**
     * Traductions du controlleur Restitutions
     * @param string $action
     * @return string|void
     */
    private function translateRestitutions(string $action)
    {
        switch ($action) {
            case 'acquittal':
                return __dx('permission', 'Restitutions', 'acquittal');
            case 'retrieval':
                return __dx('permission', 'Restitutions', 'retrieval');
        }
    }

    /**
     * Traductions du controlleur Restservices
     * @param string $action
     * @return string|void
     */
    private function translateRestservices(string $action)
    {
        if ($action == 'sedaMessages') {
            return __dx('permission', 'Restservices', 'sedaMessages');
        }
    }

    /**
     * Traductions du controlleur Tasks
     * @param string $action
     * @return string|void
     */
    private function translateTasks(string $action)
    {
        switch ($action) {
            case 'admin':
                return __dx('permission', 'Tasks', 'admin');
            case 'adminJobInfo':
                return __dx('permission', 'Tasks', 'adminJobInfo');
            case 'ajaxCancel':
                return __dx('permission', 'Tasks', 'ajaxCancel');
            case 'ajaxPause':
                return __dx('permission', 'Tasks', 'ajaxPause');
            case 'ajaxResume':
                return __dx('permission', 'Tasks', 'ajaxResume');
        }
    }

    /**
     * Traductions du controlleur Transfers
     * @param string $action
     * @return string|void
     */
    private function translateTransfers(string $action)
    {
        switch ($action) {
            case 'analyse':
                return __dx('permission', 'Transfers', 'analyse');
            case 'indexMy':
                return __dx('permission', 'Transfers', 'indexMy');
            case 'indexMyAccepted':
                return __dx('permission', 'Transfers', 'indexMyAccepted');
            case 'indexMyEntity':
                return __dx('permission', 'Transfers', 'indexMyEntity');
            case 'indexMyRejected':
                return __dx('permission', 'Transfers', 'indexMyRejected');
            case 'indexPreparating':
                return __dx('permission', 'Transfers', 'indexPreparating');
            case 'indexSent':
                return __dx('permission', 'Transfers', 'indexSent');
            case 'send':
                return __dx('permission', 'Transfers', 'send');
            case 'indexAccepted':
                return __dx('permission', 'Transfers', 'indexAccepted');
            case 'indexRejected':
                return __dx('permission', 'Transfers', 'indexRejected');
            case 'indexAll':
                return __dx('permission', 'Transfers', 'indexAll');
        }
    }

    /**
     * Traductions du controlleur Users
     * @param string $action
     * @return string|void
     */
    private function translateUsers(string $action)
    {
        switch ($action) {
            case 'editByAdmin':
                return __dx('permission', 'Users', 'editByAdmin');
            case 'changeEntity':
                return __dx(
                    'permission',
                    'Users',
                    "Modifier l'entité de l'utilisateur"
                );
            case 'acceptUser':
                return __dx(
                    'permission',
                    'Users',
                    "Accepter l'utilisateur d'un autre service d'archives"
                );
            case 'refuseUser':
                return __dx(
                    'permission',
                    'Users',
                    "Refuser l'utilisateur d'un autre service d'archives"
                );
        }
    }

    /**
     * Traductions du controlleur ValidationProcesses
     * @param string $action
     * @return string|void
     */
    private function translateValidationProcesses(string $action)
    {
        switch ($action) {
            case 'deliveryRequests':
                return __dx(
                    'permission',
                    'ValidationProcesses',
                    'deliveryRequests'
                );
            case 'destructionRequests':
                return __dx(
                    'permission',
                    'ValidationProcesses',
                    'destructionRequests'
                );
            case 'myTransfers':
                return __dx(
                    'permission',
                    'ValidationProcesses',
                    'myTransfers'
                );
            case 'outgoingTransferRequests':
                return __dx(
                    'permission',
                    'ValidationProcesses',
                    'outgoingTransferRequests'
                );
            case 'restitutionRequests':
                return __dx(
                    'permission',
                    'ValidationProcesses',
                    'restitutionRequests'
                );
            case 'transfers':
                return __dx(
                    'permission',
                    'ValidationProcesses',
                    'transfers'
                );
        }
    }

    /**
     * Traduction d'une action (non spécifique au controlleur)
     * @param string $controller
     * @param string $action
     * @return string
     */
    private function translateAction(string $controller, string $action)
    {
        switch ($action) {
            case 'add':
            case 'add1':
                return __d('permission', "Ajouter");
            case 'delete':
                return __d('permission', "Supprimer");
            case 'edit':
                return __d('permission', "Modifier");
            case 'index':
                return __d('permission', "Lister");
            case 'view':
                return __d('permission', "Visualiser");
            case 'download':
                return __d('permission', "Télécharger");
            case 'send':
                return __d('permission', "Envoyer");
            case 'api.create':
                return __d('permission', "Création via Web service");
            case 'api.read':
                return __d('permission', "Lecture via Web service");
            case 'api.update':
                return __d('permission', "Mise à jour via Web service");
            case 'api.delete':
                return __d('permission', "Suppression via Web service");
        }
        return I18n::getTranslator('permission')->translate(
            $action,
            ['_context' => $controller]
        );
    }
}
