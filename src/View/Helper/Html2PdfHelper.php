<?php

/**
 * Asalae\View\Helper\Html2PdfHelper
 */

namespace Asalae\View\Helper;

use Cake\View\Helper;

/**
 * Helper pour les templates PDF
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Html2PdfHelper extends Helper
{
    /**
     * Affiche un tableau de résultats
     * @param array $table
     * @return string
     */
    public function table(array $table): string
    {
        $trs = [];
        foreach ($table as $label => $value) {
            $value = nl2br(htmlentities($value ?? ''));
            $trs[] = "<tr><th>$label</th><td>$value</td></tr>\n";
        }
        return $trs ? '<table><tbody>' . implode("\n", $trs) . '</tbody></table>' : '';
    }
}
