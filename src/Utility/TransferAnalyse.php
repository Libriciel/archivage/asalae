<?php

/**
 * Asalae\Utility\TransferAnalyse
 */

namespace Asalae\Utility;

use Asalae\Model\Table\KillablesTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Utility\Antivirus\AntivirusInterface;
use AsalaeCore\Utility\Antivirus\Clamav;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\PreControlMessages;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use DateMalformedStringException;
use Exception;
use FileValidator\Utility\FileValidator;
use ZMQSocketException;

/**
 * Réuni la logique du AnalyseWorker, du ControlWorker et de l'action Transfers::analyse
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TransferAnalyse
{
    /**
     * Traits
     */
    use ControlTrait;

    /**
     * @var KillablesTable
     */
    public $Killables;

    /**
     * TransferAnalyse constructor.
     * @param EntityInterface $transfer nécessite un contain(['TransferAttachments'])
     */
    public function __construct(EntityInterface $transfer)
    {
        $this->Transfer = $transfer;
        $this->Killables = TableRegistry::getTableLocator()->get('Killables');
    }

    /**
     * Effectu l'analyse d'un transfert
     * @param callable|null $fn
     * @throws Exception
     */
    public function analyse(?callable $fn = null)
    {
        $dir = AbstractGenericMessageForm::getDataDirectory(
            $this->Transfer->get('id')
        ) . DS . 'attachments';
        /** @var Clamav|AntivirusInterface $Antivirus */
        $Antivirus = Utility::get(Configure::read('Antivirus.classname'));
        $viruses = $Antivirus->scan($dir);

        $algo = Configure::read('hash_algo', 'sha256');
        $dataDir = AbstractGenericMessageForm::getDataDirectory($this->Transfer->get('id'));
        $basePath = $dataDir . DS . 'attachments';
        $loc = TableRegistry::getTableLocator();
        $TransferAttachments = $loc->get('TransferAttachments');
        $query = $TransferAttachments->find()
            ->where(['transfer_id' => $this->Transfer->get('id')]);
        $count = $query->count();
        $util = DOMUtility::load($this->Transfer->get('xml'));

        /** @var EntityInterface $attachment */
        foreach ($query as $i => $attachment) {
            if ($fn) {
                $fn($attachment->get('filename'), $i + 1, $count);
            }
            $path = $basePath . DS . $attachment->get('filename');
            if (!is_file($path)) {
                $dataAttachment = [
                    'extension' => null,
                    'format' => null,
                    'mime' => null,
                    'virus_name' => null,
                    'hash' => null,
                    'hash_algo' => null,
                    'valid' => false,
                ];
                $TransferAttachments->patchEntity($attachment, $dataAttachment);
                $TransferAttachments->saveOrFail($attachment);
                continue;
            }
            if ($attachment->get('format') === null) {
                $dataAttachment = $this->extractPronom($path);
            } else {
                $dataAttachment = [];
            }
            if ($attachment->get('virus_name') === null) {
                $dataAttachment['virus_name'] = $viruses[$path] ?? '';
            }
            if (!$attachment->get('hash') || !$attachment->get('hash_algo')) {
                $dataAttachment += [
                    'hash' => hash_file($algo, $path),
                    'hash_algo' => $algo,
                ];
            }
            if ($attachment->get('valid') === null) {
                $dataAttachment['valid'] = Utility::get(FileValidator::class)
                    ->isValid($path);
            }
            $TransferAttachments->patchEntity($attachment, $dataAttachment);
            $TransferAttachments->saveOrFail($attachment);
        }
        $util->dom->save($this->Transfer->get('xml'));
    }

    /**
     * Donne le pronom d'un fichier
     * @param string $filename
     * @return array
     * @throws Exception
     */
    private function extractPronom(string $filename): array
    {
        $exec = Utility::get('Exec')->command('sf -json', $filename);
        $data = json_decode($exec->stdout);
        if ($data && isset($data->files[0]->matches[0]->id)) {
            $format = $data->files[0]->matches[0]->id;
            if ($format === 'UNKNOWN') {
                $format = '';
            }
            return [
                'extension' => pathinfo($filename, PATHINFO_EXTENSION),
                'format' => $format,
                'mime' => mime_content_type($filename),
            ];
        }
        return [
            'extension' => pathinfo($filename, PATHINFO_EXTENSION),
            'format' => null,
            'mime' => mime_content_type($filename),
            'error' => __("Unable to find a pronom on {0}", $filename),
        ];
    }

    /**
     * effectu le contrôle d'un transfert
     * @param string $notifySuffix
     * @return array
     * @throws ZMQSocketException
     * @throws DateMalformedStringException
     */
    public function control(string $notifySuffix = '')
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $TransferErrors = TableRegistry::getTableLocator()->get(
            'TransferErrors'
        );
        $this->PreControlMessages = new PreControlMessages(
            $this->Transfer->get('xml')
        );

        $errors = [];
        $this->validateMessage(
            $errors,
            function () use ($notifySuffix) {
                Utility::get('Notify')->emit(
                    'analyse_' . $this->Transfer->id . $notifySuffix,
                    __("Vérification du schéma...")
                );
            }
        );
        $this->validateAttachments(
            $errors,
            function ($filename, $i, $count) use ($notifySuffix) {
                $this->Killables->checkKill(true);
                Utility::get('Notify')->emit(
                    'analyse_' . $this->Transfer->id . $notifySuffix,
                    sprintf('control %d / %d - %s', $i, $count, h($filename))
                );
            },
            function ($filename, $i, $count) use ($notifySuffix) {
                Utility::get('Notify')->emit(
                    'analyse_' . $this->Transfer->id . $notifySuffix,
                    sprintf(
                        '%s - %d / %d - %s',
                        __("Contrôle de présence du fichier dans le bordereau"),
                        $i,
                        $count,
                        h($filename)
                    )
                );
            }
        );
        Utility::get('Notify')->emit(
            'analyse_' . $this->Transfer->get('id') . $notifySuffix,
            __("Vérification des entités...")
        );
        $sa = $this->validateSa($errors);
        $sv = $this->validateSv($errors, $sa);
        $this->validateSp($errors, $sa);
        $this->validatekeywords($errors);
        $this->validateProfiles($errors, $sa);
        $this->validateServiceLevels($errors, $sa);
        $this->validateAgreements($errors, $sa, $sv);
        $this->validateRules($errors, $sa);
        $this->validateBinaries($errors);
        $this->validateDataObjectReferences($errors);
        $this->validateArchiveUnit($errors);
        $this->validateComposite($errors, $sa);
        $valid = true;
        $conn = $TransferErrors->getConnection();
        $conn->begin();
        $TransferErrors->deleteAll(
            ['transfer_id' => $this->Transfer->get('id')]
        );
        try {
            foreach ($errors as $value) {
                $TransferErrors->saveOrFail($TransferErrors->newEntity($value));
                if ($value['level'] === 'error') {
                    $valid = false;
                }
            }
            $this->Transfer->set('is_conform', $valid);
            $Transfers->saveOrFail($this->Transfer);
            $conn->commit();
        } catch (Exception $e) {
            $conn->rollback();
            throw $e;
        }
        return $errors;
    }
}
