<?php

/**
 * Asalae\Utility\SedaValidator
 */

namespace Asalae\Utility;

use Asalae\Exception\GenericException;
use DOMDocument;

/**
 * Validation des xml seda
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SedaValidator
{
    /**
     * Valide un fichier seda
     * @param DOMDocument|string $seda dom ou chemin vers le fichier seda ou xml
     * @return bool
     */
    public static function validate($seda): bool
    {
        if (is_string($seda)) {
            $dom = new DOMDocument();
            if (is_file($seda)) {
                $dom->load($seda);
            } else {
                $dom->loadXML($seda);
            }
        } else {
            $dom = $seda;
        }
        $xmlns = $dom->documentElement->getAttributeNode('xmlns');
        $namespace = $xmlns?->nodeValue;
        $isArchive = $dom->documentElement->tagName === 'Archive';
        switch ($namespace) {
            case NAMESPACE_SEDA_02:
                $xsd = $isArchive ? SEDA_ARCHIVE_V02_XSD : SEDA_V02_XSD;
                break;
            case NAMESPACE_SEDA_10:
                $xsd = $isArchive ? SEDA_ARCHIVE_V10_XSD : SEDA_V10_XSD;
                break;
            case NAMESPACE_SEDA_21:
                $xsd = $isArchive ? SEDA_ARCHIVE_V21_XSD : SEDA_V21_XSD;
                break;
            case NAMESPACE_SEDA_22:
                $xsd = $isArchive ? SEDA_ARCHIVE_V22_XSD : SEDA_V22_XSD;
                break;
            default:
                throw new GenericException(
                    'unsuported namespace: ' . $dom->namespaceURI
                );
        }
        return $dom->schemaValidate($xsd);
    }
}
