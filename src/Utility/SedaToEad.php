<?php

/**
 * Asalae\Utility\SedaToEad
 */

namespace Asalae\Utility;

use Asalae\Model\Entity\Archive;
use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Utility\DOMUtility;
use Cake\Http\Exception\NotImplementedException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DOMDocument;
use DOMElement;
use DOMException;
use DOMNode;
use Exception;

/**
 * Conversion du Xml Seda en Ead pour 1 archive
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SedaToEad
{
    /**
     * @var DOMUtility
     */
    private $util;

    /**
     * @var DOMDocument
     */
    private $dom;

    /**
     * Donne Ead pour l'archive elle même (noeud archdesc)
     * @param DOMDocument $dom
     * @param Archive     $archive
     * @return DOMElement
     * @throws DOMException
     */
    public function getEad(DOMDocument $dom, Archive $archive): DOMElement
    {
        $this->dom = $dom;

        try {
            /** @var DescriptionXmlArchiveFile $desc */
            $desc = Hash::get($archive, 'description_xml_archive_file');

            if (!$desc) {
                throw new VolumeException();
            }
            $descDom = $desc->getDom();
        } catch (VolumeException) {
            $archdesc = $this->dom->createElement('archdesc');
            $archdesc->setAttribute('xmlns', 'urn:isbn:1-931666-22-9');
            $comment = $this->dom->createComment(
                __("Fichier manquant ou non disponible pour l'archive \"{0}\"", $archive->get('name'))
            );
            $archdesc->appendChild($comment);

            return $archdesc;
        }

        $descDom->formatOutput = true;
        $descDom->preserveWhiteSpace = false;

        $this->util = new DOMUtility($descDom);
        $archdesc = $this->dom->createElement('archdesc');
        $archdesc->setAttribute('xmlns', 'urn:isbn:1-931666-22-9');
        $this->setLevelAttribute($archdesc);

        // did
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $descPath = '//ns:Archive';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $descPath = '//ns:Archive/ns:DescriptiveMetadata/ns:ArchiveUnit';
                break;
            default:
                throw new NotImplementedException(
                    sprintf('not implemented for namespace "%s"', $this->util->namespace)
                );
        }
        $this->addDid($archdesc, $this->util->node($descPath));

        $this->setAcquinfo($archdesc, $archive);
        $this->setRules($archdesc);

        // ContentDescription | Content
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $this->addContent($archdesc, $this->util->node('ns:ContentDescription'));
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $this->addContent($archdesc, $this->util->node('ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content'));
                break;
        }

        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $this->addArchiveObjects($archdesc);
                $this->addDocuments($archdesc);
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $au = $this->util->node('ns:DescriptiveMetadata/ns:ArchiveUnit');
                $this->addDataObjectReferences($archdesc, $au);
                $this->addArchiveUnits($archdesc, $au);
                break;
        }

        return $archdesc;
    }

    /**
     * Set l'attribut level de premier niveau
     * @param DOMElement $archdesc
     * @return void
     */
    private function setLevelAttribute(DOMElement $archdesc): void
    {
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
                $path = 'ns:Contains/ns:DescriptionLevel';
                break;
            case NAMESPACE_SEDA_10:
                $path = 'ns:ContentDescription/ns:DescriptionLevel';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $path = 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:DescriptionLevel';
                break;
            default:
                throw new NotImplementedException(
                    sprintf('not implemented for namespace "%s"', $this->util->namespace)
                );
        }
        $value = $this->util->nodeValue($path);
        $archdesc->setAttribute('level', strtolower($value));
    }

    /**
     * Ajout du noeud did
     * @param DOMElement $ead
     * @param DOMElement $archive
     * @return void
     * @throws DOMException
     */
    private function addDid(DOMElement $ead, DOMElement $archive): void
    {
        $did = $this->dom->createElement('did');

        $this->setUnitid($did, $archive);

        // unittitle
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $path = 'ns:Name';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $path = 'ns:Content/ns:Title';
                break;
            default:
                throw new NotImplementedException(
                    sprintf('not implemented for namespace "%s"', $this->util->namespace)
                );
        }
        $this->addEadNodes($path, 'unittitle', $did, $archive);

        // unitdate
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $startDatePath = 'ns:ContentDescription/ns:OldestDate';
                $endDatePath = 'ns:ContentDescription/ns:LatestDate';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $startDatePath = 'ns:Content/ns:StartDate';
                $endDatePath = 'ns:Content/ns:EndDate';
                break;
            default:
                throw new NotImplementedException(
                    sprintf('not implemented for namespace "%s"', $this->util->namespace)
                );
        }
        $startDate = $this->util->nodeValue($startDatePath, $archive);
        $endDate = $this->util->nodeValue($endDatePath, $archive);
        if ($startDate || $endDate) {
            $unitdate = $this->dom->createElement('unitdate');
            $unitdate->appendChild($this->dom->createTextNode(trim($startDate) . '/' . trim($endDate)));
            $did->appendChild($unitdate);
        }

        // langmaterial
        $langmaterial = $this->dom->createElement('langmaterial');
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $path = 'ns:Language';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $path = 'ns:Content/ns:Language';
                break;
        }
        $this->addEadNodes($path, 'language', $langmaterial, $archive);
        if ($langmaterial->childNodes->count()) {
            $did->appendChild($langmaterial);
        }

        // repository
        $this->addEadNodes('ns:Repository', 'p', $did, $archive);

        // origination
        $this->addOrigination($archive, $did);

        $ead->appendChild($did);
    }

    /**
     * Ajout des balises unitid
     * @param DOMElement $did
     * @param DOMElement $archive
     * @return void
     * @throws DOMException
     */
    private function setUnitid(DOMElement $did, DOMElement $archive): void
    {
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $aPath = 'ns:ArchivalAgencyArchiveIdentifier|ns:ArchivalAgencyObjectIdentifier';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $aPath = 'ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier';
                break;
            default:
                throw new NotImplementedException(
                    sprintf('not implemented for namespace "%s"', $this->util->namespace)
                );
        }
        if ($archiveAgency = $this->util->nodeValue($aPath, $archive)) {
            $unitid = $this->dom->createElement('unitid');
            $unitid->setAttribute('type', "identifiant du service d'archives");
            $unitid->nodeValue = $archiveAgency;
            $did->appendChild($unitid);
        }
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $tPath = 'ns:TransferringAgencyArchiveIdentifier|ns:TransferringAgencyObjectIdentifier';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $tPath = 'ns:Content/ns:TransferringAgencyArchiveUnitIdentifier';
                break;
            default:
                throw new NotImplementedException(
                    sprintf('not implemented for namespace "%s"', $this->util->namespace)
                );
        }
        if ($transferringAgency = $this->util->nodeValue($tPath, $archive)) {
            $unitid = $this->dom->createElement('unitid');
            $unitid->setAttribute('type', "identifiant du service versant");
            $unitid->nodeValue = $transferringAgency;
            $did->appendChild($unitid);
        }
    }

    /**
     * Cherche un ou des éléments selon $path, crée un noeud $nodeName, et l'ajoute à l'élément $ead
     * @param string|null  $path
     * @param string       $nodeName
     * @param DOMNode      $ead
     * @param DOMNode|null $context
     * @return void
     * @throws DOMException
     */
    private function addEadNodes($path, string $nodeName, DOMNode $ead, $context = null): void
    {
        if ($path === null) {
            return;
        }
        foreach ($this->util->xpath->query($path, $context) as $name) {
            $child = $this->dom->createElement($nodeName);
            $child->appendChild(
                $this->dom->createTextNode($name->nodeValue)
            );
            $ead->appendChild($child);
        }
    }

    /**
     * Ajout de la balise origination
     * @param DOMElement $archive
     * @param DOMElement $did
     * @return void
     * @throws DOMException
     */
    private function addOrigination(DOMElement $archive, DOMElement $did): void
    {
        $origination = $this->dom->createElement('origination');
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $refPath = 'ns:OriginatingAgency';
                $this->addEadNodes(
                    'ns:OriginatingAgency/ns:Description',
                    'corpname',
                    $origination,
                    $archive
                );
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $refPath = 'ns:Content/ns:OriginatingAgency/ns:Identifier';
                break;
            default:
                throw new NotImplementedException(
                    sprintf(
                        'not implemented for namespace "%s"',
                        $this->util->namespace
                    )
                );
        }
        $this->addEadNodes($refPath, 'ref', $origination, $archive);

        if ($origination->childNodes->count()) {
            $did->appendChild($origination);
        }
    }

    /**
     * Ajout de la balise Acquinfo (date)
     * @param DOMElement $archdesc
     * @param Archive    $archive
     * @return void
     * @throws DOMException
     */
    private function setAcquinfo(DOMElement $archdesc, Archive $archive): void
    {
        if ($dateValue = Hash::get($archive, 'transfers.0.transfer_date')) {
            $acqinfo = $this->dom->createElement('acqinfo');
            $p = $this->dom->createElement('p');
            $date = $this->dom->createElement('date');
            $date->appendChild($this->dom->createTextNode($dateValue));
            $p->appendChild($date);
            $acqinfo->appendChild($p);
            $archdesc->appendChild($acqinfo);
        }
    }

    /**
     * Ajout de AccessRule et AppraisalRule
     * @param DOMElement $archdesc
     * @return void
     * @throws DOMException
     */
    private function setRules(DOMElement $archdesc): void
    {
        // AccessRestrictionRule | AccessRule
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $this->addAccessRule($archdesc, $this->util->node('ns:AccessRestrictionRule'));
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $this->addAccessRule(
                    $archdesc,
                    $this->util->node('ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AccessRule')
                );
                break;
        }

        // AppraisalRule | AppraisalRule
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
                $this->addAppraisalRule($archdesc, $this->util->node('ns:Appraisal'));
                break;
            case NAMESPACE_SEDA_10:
                $this->addAppraisalRule($archdesc, $this->util->node('ns:AppraisalRule'));
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $this->addAppraisalRule(
                    $archdesc,
                    $this->util->node('ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AppraisalRule')
                );
                break;
        }
    }

    /**
     * Ajout d'une access rule
     * @param DOMElement      $ead
     * @param DOMElement|null $seda
     * @return void
     * @throws DOMException
     */
    private function addAccessRule(DOMElement $ead, ?DOMElement $seda): void
    {
        if ($seda === null) {
            return;
        }

        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $codePath = 'ns:Code';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $codePath = 'ns:Rule';
                break;
            default:
                throw new NotImplementedException(
                    sprintf('not implemented for namespace "%s"', $this->util->namespace)
                );
        }

        $code = $this->util->nodeValue($codePath, $seda);
        $start = $this->util->nodeValue('ns:StartDate', $seda);

        $accessrestrict = $this->dom->createElement('accessrestrict');
        $accessrestrict->setAttribute('audience', 'internal');

        $AccessRuleCodes = TableRegistry::getTableLocator()->get('AccessRuleCodes');
        $rule = $AccessRuleCodes->find()
            ->select(['name', 'description'])
            ->where(['code' => trim($code)])
            ->firstOrFail();

        $pCode = $this->dom->createElement('p');
        $pCode->appendChild(
            $this->dom->createTextNode(
                $rule->get('name') === '0 an' ? __("Immédiat") : $rule->get('name')
            )
        );
        $accessrestrict->appendChild($pCode);

        $pDesc = $this->dom->createElement('p');
        $pDesc->appendChild($this->dom->createTextNode($rule->get('description')));
        $accessrestrict->appendChild($pDesc);

        $pStart = $this->dom->createElement('p');
        $pStart->appendChild($this->dom->createTextNode(__("à partir de : {0}", $start)));
        $accessrestrict->appendChild($pStart);
        $ead->appendChild($accessrestrict);
    }

    /**
     * Ajout d'une appraisal rule
     * @param DOMElement      $ead
     * @param DOMElement|null $seda
     * @return void
     * @throws DOMException
     */
    private function addAppraisalRule(DOMElement $ead, ?DOMElement $seda): void
    {
        if ($seda === null) {
            return;
        }

        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $codePath = 'ns:Code';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $codePath = 'ns:Rule';
                break;
            default:
                throw new NotImplementedException(
                    sprintf('not implemented for namespace "%s"', $this->util->namespace)
                );
        }

        $code = $this->util->nodeValue($codePath, $seda);
        $duration = $this->util->nodeValue('ns:Duration', $seda);
        $start = $this->util->nodeValue('ns:StartDate', $seda);
        $appraisal = $this->dom->createElement('appraisal');

        $AppraisalRuleCodes = TableRegistry::getTableLocator()->get('AppraisalRuleCodes');
        $rule = $AppraisalRuleCodes->find()
            ->select(['name'])
            ->where(['duration' => $duration ? trim($duration) : 'P0Y'])
            ->firstOrFail();

        $pCode = $this->dom->createElement('p');
        $pCode->appendChild($this->dom->createTextNode($code));
        $appraisal->appendChild($pCode);

        $pDuration = $this->dom->createElement('p');
        $pDuration->appendChild($this->dom->createTextNode(__("durée : {0}", $rule->get('name'))));
        $appraisal->appendChild($pDuration);
        $ead->appendChild($appraisal);

        $pStart = $this->dom->createElement('p');
        $pStart->appendChild($this->dom->createTextNode(__("à partir de : {0}", $start)));
        $appraisal->appendChild($pStart);
        $ead->appendChild($appraisal);
    }

    /**
     * Ajout du ContentDescription
     * @param DOMElement      $ead
     * @param DOMElement|null $seda
     * @return void
     * @throws DOMException
     */
    private function addContent(DOMElement $ead, ?DOMElement $seda): void
    {
        if ($seda === null) {
            return;
        }

        // ContentDescription | Content
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
                $accessPath = 'ns:AccessRestriction';
                break;
            case NAMESPACE_SEDA_10:
                $accessPath = 'ns:AccessRestrictionRule';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $accessPath = 'ns:Management/ns:AccessRule';
                break;
            default:
                throw new NotImplementedException(
                    sprintf('not implemented for namespace "%s"', $this->util->namespace)
                );
        }
        $this->addAccessRestrictionRule($ead, $this->util->node($accessPath, $seda));


        // RelatedObjectReference
        $this->addEadNodes('ns:RelatedObjectReference', 'separatedmaterial', $ead);

        // CustodialHistory
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
                $custoPath = 'ns:CustodialHistory';
                break;
            case NAMESPACE_SEDA_10:
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $custoPath = 'ns:CustodialHistory/ns:CustodialHistoryItem';
                break;
            default:
                throw new NotImplementedException(
                    sprintf('not implemented for namespace "%s"', $this->util->namespace)
                );
        }
        $custodhist = $this->dom->createElement('custodhist');
        $this->addEadNodes($custoPath, 'p', $custodhist, $seda);
        if ($custodhist->childNodes->count()) {
            $ead->appendChild($custodhist);
        }

        // Description
        $scopecontent = $this->dom->createElement('scopecontent');
        $this->addEadNodes('ns:Description', 'p', $scopecontent, $seda);
        if ($scopecontent->childNodes->count()) {
            $ead->appendChild($scopecontent);
        }

        // FilePlanPosition
        $fileplan = $this->dom->createElement('fileplan');
        $this->addEadNodes('ns:FilePlanPosition', 'p', $fileplan, $seda);
        if ($fileplan->childNodes->count()) {
            $ead->appendChild($fileplan);
        }

        // Keyword
        $this->addKeywords($seda, $ead);

        // OtherMetadata
        $odd = $this->dom->createElement('odd');
        $this->addEadNodes('ns:OtherMetadata', 'p', $odd, $seda);
        if ($odd->childNodes->count()) {
            $ead->appendChild($odd);
        }

        // OriginatingAgency/Description
        $this->addOriginatingAgency($seda, $ead);
    }

    /**
     * Ajout du AccessRestrictionRule
     * @param DOMElement      $ead
     * @param DOMElement|null $seda
     * @return void
     * @throws DOMException
     */
    private function addAccessRestrictionRule(DOMElement $ead, ?DOMElement $seda): void
    {
        if ($seda === null) {
            return;
        }

        $accessrestrict = $this->dom->createElement('accessrestrict');
        $accessrestrict->setAttribute('audience', 'internal');

        $code = $this->util->nodeValue('ns:Code', $seda);
        $start = $this->util->nodeValue('ns:StartDate', $seda);

        $AccessRuleCodes = TableRegistry::getTableLocator()->get('AccessRuleCodes');
        $rule = $AccessRuleCodes->find()
            ->select(['name', 'description'])
            ->where(['code' => trim($code)])
            ->firstOrFail();

        $pCode = $this->dom->createElement('p');
        $pCode->appendChild(
            $this->dom->createTextNode(
                $rule->get('name') === '0 an' ? __("Immédiat") : $rule->get('name')
            )
        );
        $accessrestrict->appendChild($pCode);

        $pDesc = $this->dom->createElement('p');
        $pDesc->appendChild($this->dom->createTextNode($rule->get('description')));
        $accessrestrict->appendChild($pDesc);

        $pStart = $this->dom->createElement('p');
        $pStart->appendChild($this->dom->createTextNode(__("à partir de : {0}", $start)));
        $accessrestrict->appendChild($pStart);

        $ead->appendChild($accessrestrict);
    }

    /**
     * Ajout des Keywords
     * @param DOMElement $seda
     * @param DOMElement $ead
     * @return void
     * @throws DOMException
     */
    private function addKeywords(DOMElement $seda, DOMElement $ead): void
    {
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
                $keywordPath = 'ns:ContentDescriptive';
                break;
            case NAMESPACE_SEDA_10:
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $keywordPath = 'ns:Keyword';
                break;
            default:
                throw new NotImplementedException(
                    sprintf(
                        'not implemented for namespace "%s"',
                        $this->util->namespace
                    )
                );
        }
        /** @var DOMNode $keyword */
        foreach ($this->util->xpath->query($keywordPath, $seda) as $keyword) {
            $controlaccess = $this->dom->createElement('controlaccess');
            $name = $this->util->nodeValue('ns:KeywordType', $keyword) ?? 'p';
            $keywordType = $this->dom->createElement($name);
            $keywordType->appendChild(
                $this->dom->createTextNode(
                    $this->util->nodeValue('ns:KeywordContent', $keyword)
                )
            );
            $controlaccess->appendChild($keywordType);
            $ead->appendChild($controlaccess);
        }
    }

    /**
     * Ajout de l'OriginatingAgency
     * @param DOMElement $seda
     * @param DOMElement $ead
     * @return void
     * @throws DOMException
     */
    private function addOriginatingAgency(DOMElement $seda, DOMElement $ead): void
    {
        $bioghist = $this->dom->createElement('bioghist');
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
                $oaPath = 'OriginatingAgency/Identification';
                $pVal = $this->util->nodeValue($oaPath, $seda);
                if ($pVal) {
                    $p = $this->dom->createElement('p', $pVal);
                    $bioghist->appendChild($p);
                    $ead->appendChild($bioghist);
                }
                break;
            case NAMESPACE_SEDA_10:
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $p = $this->util->node('OriginatingAgency/Description', $seda);
                if ($p) {
                    $bioghist->appendChild($p);
                    $ead->appendChild($bioghist);
                }
                break;
        }
    }

    /**
     * Ajout des Archive Objects
     * @param DOMElement      $ead
     * @param DOMElement|null $seda
     * @return void
     * @throws DOMException
     */
    private function addArchiveObjects(DOMElement $ead, ?DOMElement $seda = null): void
    {
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
                $path = 'ns:Contains';
                break;
            case NAMESPACE_SEDA_10:
                $path = 'ns:ArchiveObject';
                break;
            default:
                throw new NotImplementedException(
                    sprintf('not implemented for namespace "%s"', $this->util->namespace)
                );
        }
        foreach ($this->util->xpath->query($path, $seda) as $archiveObject) {
            $this->addArchiveObject($ead, $archiveObject);
        }
    }

    /**
     * Ajout d'un Archive Object (récursif)
     * @param DOMElement $ead
     * @param DOMElement $archiveObject
     * @return void
     * @throws DOMException
     */
    private function addArchiveObject(DOMElement $ead, DOMElement $archiveObject): void
    {
        $dsc = $this->dom->createElement('dsc');
        $c = $this->dom->createElement('c');

        $level = $this->util->nodeValue('ns:ContentDescription/ns:DescriptionLevel', $archiveObject);
        if ($level) {
            $c->setAttribute('level', strtolower($level));
        }

        $this->addDid($c, $archiveObject);

        // AccessRestrictionRule
        $this->addAccessRule($c, $this->util->node('ns:AccessRestrictionRule', $archiveObject));

        // ContentDescription
        $this->addContent($c, $this->util->node('ns:ContentDescription', $archiveObject));

        // AppraisalRule
        $this->addAppraisalRule($c, $this->util->node('ns:AppraisalRule', $archiveObject));

        $bioghist = $this->dom->createElement('bioghist');
        $p = $this->util->node('OriginatingAgency/Description', $archiveObject);
        if ($p) {
            $bioghist->appendChild($p);
            $c->appendChild($bioghist);
        }

        $this->addDocuments($c, $archiveObject);
        $this->addArchiveObjects($c, $archiveObject);

        $dsc->appendChild($c);
        $ead->appendChild($dsc);
    }

    /**
     * Ajout des Documents
     * @param DOMElement      $ead
     * @param DOMElement|null $seda
     * @return void
     * @throws DOMException
     */
    private function addDocuments(DOMElement $ead, ?DOMElement $seda = null): void
    {
        foreach ($this->util->xpath->query('ns:Document', $seda) as $document) {
            $this->addDocument($ead, $document);
        }
    }

    /**
     * Ajout du Document (récursif)
     * @param DOMElement      $ead
     * @param DOMElement|null $document
     * @return void
     * @throws DOMException
     */
    private function addDocument(DOMElement $ead, ?DOMElement $document): void
    {
        $dsc = $this->dom->createElement('dsc');
        $c = $this->dom->createElement('c');

        $did = $this->dom->createElement('did');
        /** @var DOMElement $attachment */
        $attachment = $this->util->node('ns:Attachment', $document);
        $unittitle = $this->dom->createElement('unittitle', $attachment->getAttribute('filename'));
        $did->appendChild($unittitle);

        $oa = $this->util->node('ns:OriginatingAgencyDocumentIdentifier', $document);
        if ($oa) {
            $unitid = $this->dom->createElement('unitid', $oa->nodeValue);
            $unitid->setAttribute('type', __("producteur"));
            $did->appendChild($unitid);
        }

        $ta = $this->util->node('ns:TransferringAgencyDocumentIdentifier', $document);
        if ($ta) {
            $unitid = $this->dom->createElement('unitid', $ta->nodeValue);
            $unitid->setAttribute('type', __("versant"));
            $did->appendChild($unitid);
        }

        $size = $this->util->xpath->query('ns:Size', $document);
        if ($size->count()) {
            $extent = $this->dom->createElement('extent', $size->item(0)->nodeValue);
            $physdesc = $this->dom->createElement('physdesc');
            $physdesc->appendChild($extent);
            $did->appendChild($physdesc);
        }

        $c->appendChild($did);

        $uri = $attachment->getAttribute('uri');
        if ($uri) {
            $dao = $this->dom->createElement('dao');
            $dao->setAttribute('href', $uri);
            $c->appendChild($dao);
        }

        $format = $attachment->getAttribute('format');
        if ($format) {
            $formatP = $this->dom->createElement('p', $format);
            $formatPhysdesc = $this->dom->createElement('phystech');
            $formatPhysdesc->appendChild($formatP);
            $c->appendChild($formatPhysdesc);
        }

        $dsc->appendChild($c);
        $ead->appendChild($dsc);
    }

    /**
     * Ajout des Data Object References
     * @param DOMElement      $ead
     * @param DOMElement|null $seda
     * @return void
     * @throws DOMException
     */
    private function addDataObjectReferences(DOMElement $ead, ?DOMElement $seda = null): void
    {
        foreach ($this->util->xpath->query('ns:DataObjectReference', $seda) as $dataObjectReference) {
            $this->addDataObjectReference($ead, $dataObjectReference);
        }
    }

    /**
     * Ajout d'un Data Object Reference (récursif)
     * @param DOMElement      $ead
     * @param DOMElement|null $dataObjectReference
     * @return void
     * @throws DOMException
     */
    private function addDataObjectReference(DOMElement $ead, ?DOMElement $dataObjectReference): void
    {
        $dsc = $this->dom->createElement('dsc');
        $c = $this->dom->createElement('c');

        $id = $this->util->nodeValue('ns:DataObjectReferenceId', $dataObjectReference);
        if ($id) {
            $binaryDataPath = sprintf('//ns:Archive/ns:BinaryDataObject[@id=%s]', $this->util->xpathQuote($id));
            $binaryData = $this->util->xpath->query($binaryDataPath);
        }
        if (isset($binaryData) && $binaryData->count()) {
            $binaries = [$binaryData->item(0)];
        } else {
            $idGroup = $this->util->nodeValue('ns:DataObjectGroupReferenceId', $dataObjectReference);
            if ($idGroup === null) {
                throw new Exception('Missing DataObjectGroupReferenceId value');
            }
            $groupBinaryDataPath
                = sprintf('//ns:Archive/ns:DataObjectGroup[@id=%s]', $this->util->xpathQuote($idGroup));
            $groupBinaryData = $this->util->xpath->query($groupBinaryDataPath);
            $binaries = $this->util->xpath->query('ns:BinaryDataObject', $groupBinaryData->item(0));
        }

        foreach ($binaries as $binary) {
            $ead->appendChild($this->createDidNode($binary, $c, $dsc));
        }
    }

    /**
     * @param DOMNode    $binary
     * @param DOMElement $c
     * @param DOMElement $dsc
     * @return DOMElement
     * @throws DOMException
     */
    protected function createDidNode(DOMNode $binary, DOMElement $c, DOMElement $dsc): DOMElement
    {
        // did
        $did = $this->dom->createElement('did');
        $unittitle = $this->dom->createElement(
            'unittitle',
            $this->util->nodeValue('ns:FileInfo/ns:Filename', $binary)
        );
        $did->appendChild($unittitle);
        $physdesc = $this->dom->createElement('physdesc');
        $extent = $this->dom->createElement(
            'extent',
            $this->util->nodeValue('ns:Size', $binary)
        );
        $physdesc->appendChild($extent);
        $did->appendChild($physdesc);
        $c->appendChild($did);

        // dao
        $href = $this->util->nodeValue('ns:FileInfo/ns:Filename', $binary);
        if ($href) {
            $dao = $this->dom->createElement('dao');
            $dao->setAttribute('href', $href);
            $c->appendChild($dao);
        }

        // phystech
        $formatId = $this->util->nodeValue(
            'ns:FormatIdentification/ns:FormatId',
            $binary
        );
        if ($formatId) {
            $phystech = $this->dom->createElement('phystech');
            $p = $this->dom->createElement('p', $formatId);
            $phystech->appendChild($p);
            $c->appendChild($phystech);
        }
        $dsc->appendChild($c);
        return $dsc;
    }

    /**
     * Ajout des Archive Units
     * @param DOMElement      $ead
     * @param DOMElement|null $seda
     * @return void
     * @throws DOMException
     */
    private function addArchiveUnits(DOMElement $ead, ?DOMElement $seda = null): void
    {
        foreach ($this->util->xpath->query('ns:ArchiveUnit', $seda) as $archiveUnit) {
            $this->addArchiveUnit($ead, $archiveUnit);
        }
    }

    /**
     * Ajout d'un Archive Unit (récursif)
     * @param DOMElement      $ead
     * @param DOMElement|null $archive
     * @return void
     * @throws DOMException
     */
    private function addArchiveUnit(DOMElement $ead, ?DOMElement $archive): void
    {
        $dsc = $this->dom->createElement('dsc');
        $c = $this->dom->createElement('c');

        if ($level = $this->util->nodeValue('ns:Content/ns:DescriptionLevel', $archive)) {
            $c->setAttribute('level', strtolower($level));
        }

        $this->addDid($c, $archive);
        $this->addAccessRule($c, $this->util->node('ns:AccessRule', $archive));
        $this->addAppraisalRule($c, $this->util->node('ns:AppraisalRule', $archive));
        $this->addContent($c, $this->util->node('ns:Content', $archive));
        $this->addDataObjectReferences($c, $archive);
        $this->addArchiveUnits($c, $archive);

        $dsc->appendChild($c);
        $ead->appendChild($dsc);
    }
}
