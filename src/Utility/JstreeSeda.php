<?php

/**
 * Asalae\Utility\JstreeSeda
 */

namespace Asalae\Utility;

use AsalaeCore\Form\MessageForm;
use AsalaeCore\Form\MessageFormInterface;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Translit;
use Cake\Core\Configure;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\DateTime as CakeDateTime;
use DOMDocument;
use DOMElement;
use DOMNodeList;
use Exception;
use IntlDateFormatter;

/**
 * Permet de produire le jstree d'un xml seda
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property MessageFormInterface $form
 */
class JstreeSeda
{
    /**
     * @var array
     */
    public $icon = [
        'ArchiveTransfer' => 'fa fa-exchange',
        'ArchiveObject' => 'fa fa-folder',
        'ArchiveUnit' => 'fa fa-folder',
        'Archive' => 'fa fa-archive',
        'DescriptiveMetadata' => 'fa fa-archive',
        'Contains' => 'fa fa-archive',
        'Document' => 'fa fa-paperclip',
        'BinaryDataObject' => 'fa fa-paperclip',
        'Keyword' => 'fa fa-key',
        'ContentDescription' => 'fa fa-file-text-o',
        'ContentDescriptive' => 'fa fa-key',
        'ArchivalAgency' => 'fa fa-building text-info',
        'TransferringAgency' => 'fa fa-building text-info',
        'OriginatingAgency' => 'fa fa-building text-info',
        'SubmissionAgency' => 'fa fa-building text-info',
    ];
    /**
     * @var string[]
     */
    public $appendSubnodeText = [
        'name',
        'identifier',
        'identification',
        'keywordcontent',
        'repositoryarchiveunitpid',
    ];
    /**
     * @var int nombre d'éléments max par page (pagination)
     */
    public $elementsPerPage = 100;
    /**
     * @var int nombre de chars avant coupure du nom (NomDunNoeud[5] Nom dans le noeud~)
     */
    public $maxCharsPerName = 30;
    /**
     * @var MessageFormInterface
     */
    public MessageFormInterface $form;
    /**
     * @var bool
     */
    private bool $showHidden;
    /**
     * @var bool
     */
    private bool $showAddable;
    /**
     * @var DOMUtility
     */
    private DOMUtility $util;
    /**
     * @var int
     */
    private int $page = 1;

    /**
     * Constructeur
     * @param DOMDocument $dom
     * @param string|null $schemaPath ex: ArchiveTransfer.Archive.ArchiveObject
     * @param array       $options
     */
    public function __construct(DOMDocument $dom, string $schemaPath = null, array $options = [])
    {
        $options += [
            'showHidden' => false,
            'showAddable' => false,
        ];
        $this->util = new DOMUtility($dom);
        if (empty($schemaPath)) {
            $schemaPath = $dom->documentElement->nodeName;
            if ($pos = strpos($schemaPath, ':')) {
                $schemaPath = substr($schemaPath, $pos + 1);
            }
        }
        $this->form = MessageForm::buildForm(
            $this->util->namespace ?: NAMESPACE_SEDA_10,
            null,
            $schemaPath
        );
        $this->showHidden = $options['showHidden'];
        $this->showAddable = $options['showAddable'];
        $this->elementsPerPage = Configure::read(
            'Transfers.elementsPerPage',
            $this->elementsPerPage
        );
    }

    /**
     * Donne le chemin pour le schema (ex: ArchiveTransfer.Archive.ArchiveObject)
     * @param string $xpath
     * @return string
     */
    public static function xpathToSchemaPath(string $xpath): string
    {
        $path = str_replace('/ns:', '.', $xpath);
        $path = preg_replace('/\[\d+]/', '', $path);
        return ltrim($path, '.');
    }

    /**
     * Donne le type de tableau attendu par jstree
     * @param string ...$pathToOpen chemin à ouvrir (une partie seulement est chargée)
     * @return array
     * @throws Exception
     */
    public function getJsTree(...$pathToOpen): array
    {
        $activeClass = true;
        // si path est vide, on ouvre la 1ere archive
        if (empty($pathToOpen)) {
            $pathToOpen = ['ArchiveTransfer'];
            switch ($this->util->namespace) {
                case NAMESPACE_SEDA_10:
                    $xpath = ['Archive'];
                    break;
                case NAMESPACE_SEDA_02:
                    $xpath = ['Contains'];
                    break;
                case NAMESPACE_SEDA_21:
                case NAMESPACE_SEDA_22:
                    $xpath = explode(
                        '/',
                        'DataObjectPackage/DescriptiveMetadata/ArchiveUnit'
                    );
                    break;
                default:
                    $xpath = [];
            }
            $prevElement = null;
            foreach ($xpath as $elementName) {
                if ($q = $this->util->xpath->query('ns:' . $elementName, $prevElement)) {
                    $pathToOpen[] = $elementName . ($q->count() > 1 ? '[1]' : '');
                    $prevElement = $q->item(0);
                }
            }
            $activeClass = false;
        }
        return $this->setDataTree($pathToOpen, $activeClass);
    }

    /**
     * Donne un array compatible jstree, ouvre l'arbre jusqu'à $pathToOpen
     * Ouvre également les 20 premiers elements
     * @param array $pathToOpen
     * @param bool  $activeClass
     * @return array
     * @throws Exception
     */
    private function setDataTree(array $pathToOpen, bool $activeClass): array
    {
        $data = $this->loadDataNode($this->util->dom->documentElement, true);
        $data['children'] = $this->loadChildrenNodes($this->util->dom->documentElement);
        array_shift($pathToOpen);
        $refData =& $data;
        foreach ($pathToOpen as $step) {
            if (!preg_match('/\[\d+]$/', $step)) {
                $step .= '[1]';
            }
            if (isset($refData['children'][$step])) {
                $refData =& $refData['children'][$step];
                $refData['state']['opened'] = true;
                if (isset($refData['children']) && $refData['children'] === true) {
                    $refData['children'] = $this->loadChildrenNodes($refData['node']);
                }
            }
        }
        if ($activeClass) {
            $refData['li_attr'] = ['class' => 'active_node']; // color le dernier noeud
        }
        $this->loadFirstsNodes($data);
        $this->appendMetaData($data, '', $pathToOpen);
        return $data;
    }

    /**
     * Données issues d'un noeud
     * @param DOMElement $node
     * @param bool       $opened
     * @return array
     */
    private function loadDataNode(DOMElement $node, bool $opened = false): array
    {
        $tagName = $node->tagName;
        return [
            'element' => $tagName,
            'node' => $node,
            'state' => ['opened' => $opened],
            'li_attr' => ['class' => $opened ? 'opened active' : 'closed'],
            'children' => true,
        ];
    }

    /**
     * Transforme un `children => true` en array
     * @param DOMElement $parentNode
     * @param bool       $opened
     * @return array
     */
    private function loadChildrenNodes(DOMElement $parentNode, bool $opened = false): array
    {
        $children = [];
        foreach ($parentNode->childNodes as $node) {
            if (!$node instanceof DOMElement) {
                continue;
            }
            $indexes[$node->tagName] = ($indexes[$node->tagName] ?? 0) + 1;
            $refName = sprintf('%s[%d]', $node->tagName, $indexes[$node->tagName]);
            $children[$refName] = $this->loadDataNode($node, $opened);
            $children[$refName]['index'] = $indexes[$node->tagName];
            $children[$refName]['count'] =& $indexes[$node->tagName];
        }
        return $children;
    }

    /**
     * Ouvre les x premiers noeuds
     * @param array $data
     * @param int   $limit
     * @param int   $i
     * @throws Exception
     */
    private function loadFirstsNodes(
        array &$data,
        int $limit = 20,
        int &$i = 0
    ) {
        if ($i >= $limit || empty($data['children'])) {
            return;
        }
        if ($data['children'] === true) {
            $data['children'] = $this->loadChildrenNodes($data['node']);
            $i++;
        }
        foreach ($data['children'] as &$value) {
            $this->loadFirstsNodes($value, $limit, $i);
            $i++;
        }
        unset($value);
    }

    /**
     * Ajoute l'ensemble des données nécessaire au fonctionnement du jstree
     * @param array  $data
     * @param string $parentUrl
     * @param array  $pathToOpen
     * @return void
     */
    public function appendMetaData(array &$data, string $parentUrl = '', array $pathToOpen = [])
    {
        $this->appendUrl($data, $parentUrl);
        $this->appendSchemaData($data);
        $this->appendNavigationData($data, $pathToOpen);
        if ($this->showAddable) {
            $this->appendAddable($data);
        }
        $this->sanitizeJstreeData($data);
    }

    /**
     * Calcul de l'url en fonction du noeud et de son index
     * @param array  $data
     * @param string $parentUrl
     * @return void
     */
    private function appendUrl(array &$data, string $parentUrl = '')
    {
        $elementUrl = $parentUrl . $data['element'];
        if (isset($data['count']) && $data['count'] > 1 && isset($data['index'])) {
            $elementUrl .= '[' . $data['index'] . ']';
        }
        $data['id'] = str_replace(
            ['/', '[', ']'],
            ['_', '-', ''],
            1 . '/' . $elementUrl
        );
        $data['url'] = $elementUrl;
        if (!isset($data['children']) || !is_array($data['children'])) {
            return;
        }
        foreach ($data['children'] as &$value) {
            $this->appendUrl($value, $elementUrl . '/');
        }
        unset($value);
    }

    /**
     * Ajoute les informations issues du MessageFormSchemaInterface
     * @param array $data
     * @return void
     */
    private function appendSchemaData(array &$data)
    {
        $schema = $this->getNodeSchema($data['node']);
        if (!isset($schema['text'])) {
            return;
        }
        $this->mergeSchemaAttribute($schema, $data);

        // Liste les champs qu'on peux ajouter
        $addables = [];
        foreach ($schema['children'] ?? [] as $element => $eData) {
            if (isset($eData['children'])) {
                [$min, $max] = explode(
                    '..',
                    $eData['cardinality'] ?? '0..1'
                );
                $addables[$element] = [
                    'element' => $element,
                    'cardinality' => $eData['cardinality'] ?? '0..1',
                    'min' => $min,
                    'max' => $max,
                    'count' => 0,
                ];
                if ($eData['action'] ?? false) {
                    $addables[$element]['action'] = $eData['action'];
                }
            }
        }
        $data['order'] = array_keys($addables);
        $choices = $data['choice'] ?? [];

        if (empty($data['order'])) {
            $data['children'] = false;
        }

        // recursion
        if (!empty($data['children']) && is_array($data['children'])) {
            foreach ($data['children'] as $key => &$childData) {
                $this->appendSchemaData($childData);
                if (in_array($childData['element'], $choices)) {
                    foreach ($choices as $choice) {
                        if ($choice !== $childData['element']) {
                            unset($addables[$choice]);
                        }
                    }
                }

                $hideMe = $this->showHidden === false && ($childData['display'] ?? true) === false;
                // On supprime les elements qui ne sont pas des noeuds parents
                if (!isset($childData['text']) || $hideMe) {
                    unset($data['children'][$key]);
                    unset($addables[$childData['element']]);
                    // on retire les addables qui on atteind la limite max
                } elseif (isset($addables[$childData['element']])) {
                    $add =& $addables[$childData['element']];
                    $add['count'] = $childData['count'];
                    if ($add['max'] !== 'n' && $childData['count'] >= $add['max']) {
                        unset($addables[$childData['element']]);
                    }
                }
            }

            // Si les enfants ne sont pas chargés, on compte les noeuds enfants
        } elseif (isset($data['children']) && $data['children'] === true) {
            $counts = $this->countChildrenNodes(
                $data['node']->childNodes,
                $choices,
                $addables
            );
            foreach ($counts as $el => $count) {
                if (!isset($addables[$el])) {
                    continue;
                }
                if ($addables[$el]['max'] !== 'n' && $count >= $addables[$el]['max']) {
                    unset($addables[$el]);
                } else {
                    $addables[$el]['count'] = $count;
                }
            }
        }

        $data['addable'] = array_values($addables);
        // les icones issues du schéma sous soumises à conditions
        if (isset($schema['icon']) && isset($counts)) {
            $complete = true;
            foreach ($schema['icon']['fields'] as $field) {
                if (!isset($counts[$field])) {
                    $complete = false;
                    break;
                }
            }
            $data['icon'] = $complete ? $schema['icon']['complete'] : $schema['icon']['incomplete'];
        }

        if ($data['addable'] && isset($data['children']) && $data['children'] === false) {
            $data['children'] = [];
        }
    }

    /**
     * Donne les informations issues du schema seda à partir d'un noeud
     * @param DOMElement $node
     * @return array
     */
    private function getNodeSchema(DOMElement $node): array
    {
        $paths = [];
        $parent = $node;
        /** @var DOMElement $parent */
        while (($parent = $parent->parentNode) && $parent->parentNode) {
            array_unshift($paths, $parent->tagName);
        }
        $paths[] = $node->tagName;
        $ref =& $this->form->schema;
        foreach ($paths as $k) {
            if (isset($ref['children'][$k])) {
                $ref =& $ref['children'][$k];
            } elseif (isset($ref[$k])) {
                $ref =& $ref[$k];
            }
        }
        return (array)$ref;
    }

    /**
     * Ajoute les valeurs manquantes à $data
     * @param array $schema
     * @param array $data
     * @return void
     */
    private function mergeSchemaAttribute(array $schema, array &$data): void
    {
        if (isset($schema['text'])) {
            $data['text'] = $schema['text'];
        } else {
            return;
        }
        if (isset($schema['li_attr'])) {
            $data['li_attr'] = ($data['li_attr'] ?? []) + $schema['li_attr'];
        }
        if (isset($schema['display'])) {
            $data['display'] = $schema['display'];
        }
        if (isset($schema['choice'])) {
            $data['choice'] = $schema['choice'];
        }
        $this->appendGenericChildValue($data);
        $data['cardinality'] = $schema['cardinality'] ?? '0..1';
        [$data['min'], $data['max']] = explode(
            '..',
            $data['cardinality']
        );
    }

    /**
     * Ajoute les valeurs des noeuds enfants dans le titre du noeud
     * @param array $data
     * @return void
     */
    private function appendGenericChildValue(array &$data)
    {
        CakeDate::$niceFormat = IntlDateFormatter::SHORT;
        CakeDateTime::$niceFormat = [IntlDateFormatter::SHORT, IntlDateFormatter::NONE];
        $text = basename($data['url']);
        $search = '';
        foreach ($data['node']->childNodes as $subChild) {
            if ($subChild instanceof DOMElement) {
                $nodeName = strtolower($subChild->nodeName);
                if (in_array($nodeName, $this->appendSubnodeText)) {
                    $name = trim($subChild->nodeValue);
                    $nameShort = $this->getShortName($name);
                    $text .= ' ' . $nameShort;
                    $search .= Translit::asciiToLower($name);
                } elseif ($nodeName === 'content') {
                    $title = $this->util->xpath->query('ns:Title', $subChild)
                        ->item(0);
                    $title = $title ? $title->nodeValue : '';
                    $titleShort = $this->getShortName($title, true);
                    $text .= ' <span class="child-value" title="' . $title . '">' . $titleShort . '</span>';
                    $search .= Translit::asciiToLower($title);
                } elseif ($nodeName === 'attachment') {
                    $filename = $subChild->getAttribute('filename');
                    $filenameShort = $this->getShortName($filename, true);
                    $text .= ' <span class="child-value" title="' . $filename . '">' . $filenameShort . '</span>';
                    $search .= Translit::asciiToLower($filename);
                } elseif ($nodeName === 'filename') {
                    $filename = $subChild->nodeValue;
                    $filenameShort = $this->getShortName($filename, true);
                    $text .= ' <span class="child-value" title="' . $filename . '">' . $filenameShort . '</span>';
                    $search .= Translit::asciiToLower($filename);
                } elseif (in_array($nodeName, ['dataobjectreferenceid', 'dataobjectgroupreferenceid'])) {
                    $id = $subChild->nodeValue;
                    $idShort = $this->getShortName($id);
                    $text .= ' <span class="child-value" title="' . $id . '">' . $idShort . '</span>';
                    $search .= Translit::asciiToLower($id);
                }
            }
        }

        if (in_array($data['node']->nodeName, ['DataObjectGroup', 'BinaryDataObject'])) {
            $id = $data['node']->getAttribute('id');
            $idShort = $this->getShortName($id);
            $text .= ' <span class="child-value" title="' . $id . '">' . $idShort . '</span>';
            $search .= Translit::asciiToLower($id);
        }

        if (in_array($data['node']->nodeName, ['AccessRestrictionRule', 'AppraisalRule', 'AccessRule'])) {
            $code = $startDate = $final = null;
            foreach ($data['node']->childNodes as $subnode) {
                if (!$subnode instanceof DOMElement) {
                    continue;
                }
                if ($subnode->tagName === 'Code') {
                    $code = $subnode->nodeValue;
                } elseif ($subnode->tagName === 'Rule') {
                    $code = $subnode->nodeValue;
                    if (preg_match('/^APP(\d+)Y$/', $code, $m)) {
                        $code = __n("{0} an", "{0} ans", $m[1], $m[1]);
                    }
                } elseif ($subnode->tagName === 'FinalAction') {
                    $final = $subnode->nodeValue === 'Destroy'
                        ? __("détruire")
                        : __("conserver");
                } elseif ($subnode->tagName === 'StartDate') {
                    $startDate = (new CakeDate($subnode->nodeValue))->nice();
                } elseif ($subnode->tagName === 'Duration') {
                    $years = (int)substr($subnode->nodeValue, 1);
                    $code .= ' ' . __n("{0} an", "{0} ans", $years, $years);
                }
            }
            $rule = trim($final . ' ' . $code . ' ' . $startDate);
            $text .= ' <span class="child-value" title="' . $rule . '">' . $rule . '</span>';
            $search .= Translit::asciiToLower($rule);
        }

        $data['text'] = $text;
        $data['searchtext'] = $search;
    }

    /**
     * Donne un nom raccourci à 30 chars
     * @param string $name
     * @param bool   $isFilename
     * @return string
     */
    private function getShortName(
        string $name,
        bool $isFilename = false
    ): string {
        if (mb_strlen($name) <= $this->maxCharsPerName) {
            $trimmed = $name;
        } elseif ($isFilename) {
            if (mb_strpos($name, '/')) {
                $name = basename($name);
                if (mb_strlen($name) <= $this->maxCharsPerName) {
                    return $name;
                }
            }
            $ext = pathinfo($name, PATHINFO_EXTENSION);
            $endString = $ext ? '~.' . $ext : '~';
            $cutLength = $this->maxCharsPerName - mb_strlen($endString);
            $trimmed = mb_substr($name, 0, $cutLength) . $endString;
        } else {
            $trimmed = mb_substr($name, 0, $this->maxCharsPerName - 1) . '~';
        }

        if ($trimmed === $name) {
            return "<span class=\"child-value\" title=\"$name\">$name</span>";
        } else {
            return "<span class=\"child-value trimmed\" title=\"$name\">$trimmed</span>";
        }
    }

    /**
     * @param DOMNodeList $children
     * @param array       $choices
     * @param array       $addables
     * @return array
     */
    private function countChildrenNodes(
        DOMNodeList $children,
        array $choices,
        array &$addables
    ): array {
        $counts = [];
        $childsTempData = [];
        foreach ($children as $childNode) {
            if (!$childNode instanceof DOMElement) {
                continue;
            }
            // Si on recontre ce tagname pour la 1ère fois, on initialise le
            // compteur et on vérifi le "choice"
            if (!isset($counts[$childNode->tagName])) {
                $counts[$childNode->tagName] = 0;
                if (in_array($childNode->tagName, $choices)) {
                    foreach ($choices as $choice) {
                        if ($choice !== $childNode->tagName) {
                            unset($addables[$choice]);
                        }
                    }
                }
                // FIXME / TODO vérifier si le code qui suit sert à quelque chose
                $childSchema = $this->getNodeSchema($childNode);
                $cardinality = $childSchema['cardinality'] ?? '0..1';
                $tempData =& $childsTempData[$childNode->tagName];
                $tempData = [];
                [$tempData['min'], $tempData['max']] = explode(
                    '..',
                    $cardinality
                );
            }
            $counts[$childNode->tagName]++;
        }
        return $counts;
    }

    /**
     * Ajoute les elements de navigation et de pagination (avec support visuel)
     * @param array $data
     * @param array $pathToOpen
     * @return void
     */
    private function appendNavigationData(array &$data, array $pathToOpen = [])
    {
        if (empty($data['icon'])) {
            $data['icon'] = $this->icon[$data['element']] ?? 'fa fa-file-o';
        }
        if (isset($data['count']) && isset($data['min']) && $data['count'] < $data['min']) {
            $data['icon'] = 'fa fa-warning text-warning';
        }
        if (is_array($data['children'])) {
            if (isset($pathToOpen[0])) {
                preg_match('/^([^[]+)(?:\[(\d+)])?$/', $pathToOpen[0], $m);
            }
            $filterPagination = [];
            $page = $this->page;
            $shifted = false;
            $open = [];
            foreach ($data['children'] as $key => &$childData) {
                if (isset($m) && $m) {
                    $tagName = $m[1];
                    if ($tagName === $childData['element'] && $shifted === false) {
                        array_shift($pathToOpen);
                        $shifted = true;
                        $open[] = $tagName;
                    } elseif (count($pathToOpen) === 1 && $tagName === $data['element']) {
                        $open[] = $childData['element'];
                    }
                }
                if ($childData['count'] > $this->elementsPerPage) {
                    if (isset($m[2])) {
                        $page = (int)ceil($m[2] / $this->elementsPerPage);
                    }
                    $filterPagination[$childData['element']][$key] = $childData;
                    unset($data['children'][$key]);
                } else {
                    $this->appendNavigationData($childData, $pathToOpen);
                }
            }
            if ($filterPagination) {
                $this->appendPagination($data, $filterPagination, $open, $page);
            }
        }
    }

    /**
     * Noeud de type liste, avec pagination
     * @param array $data
     * @param array $filterPagination
     * @param array $open
     * @param int   $page
     * @return void
     */
    private function appendPagination(array &$data, array $filterPagination, array $open, int $page = 1)
    {
        foreach ($filterPagination as $element => &$childDatas) {
            $offset = ($page * $this->elementsPerPage) - $this->elementsPerPage;
            $count = count($childDatas);
            $childDatas = array_slice($childDatas, $offset, $this->elementsPerPage);
            foreach ($childDatas as &$childData) {
                $this->appendNavigationData($childData);
            }
            unset($childData);
            $this->appendPrevNext($childDatas, $element, $data['url'], $count, $page);
            $lastPage = (int)ceil($count / $this->elementsPerPage);
            $newOrder = [];
            $before = array_slice($data['order'], 0, array_search($element, $data['order']));
            foreach ($data['children'] as $key => $value) {
                if (in_array($value['element'], $before)) {
                    $newOrder[$key] = $value;
                    unset($data['children'][$key]);
                }
            }
            $newOrder['pagination_' . $element] = [
                'id' => 'paginate_' . str_replace(
                    ['/', '[', ']'],
                    ['_', '-', ''],
                    $page . '/' . $data['url']
                ),
                'text' => __("Liste de ''{0}'' ({1} pages)", $element, $lastPage),
                'last_page' => $lastPage,
                'element' => $element,
                'disabled' => true,
                'url' => $data['url'],
                'navigation' => true,
                'icon' => 'fa fa-object-group',
                'state' => ['opened' => in_array($element, $open)],
                'children' => $childDatas,
            ];
            $data['children'] = array_merge($newOrder, $data['children']);
        }
    }

    /**
     * Ajoute les boutons prev et next
     * @param array  $data
     * @param string $element
     * @param string $url
     * @param int    $count
     * @param int    $page
     * @return void
     */
    private function appendPrevNext(array &$data, string $element, string $url, int $count, int $page = 1)
    {
        $offset = ($page * $this->elementsPerPage) - $this->elementsPerPage;
        $navigationBegin = [];
        if ($page > 1) {
            if ($page !== 2) {
                $navigationBegin[] = $this->getNavigation(1, $url)
                    + ['element' => $element]
                    + ['icon' => 'fa fa-angle-double-left'];
            }
            $navigationBegin[] = $this->getNavigation($page - 1, $url)
                + ['element' => $element]
                + ['icon' => 'fa fa-chevron-left'];
        }

        array_unshift($data, ...$navigationBegin);
        $lastPage = (int)ceil($count / $this->elementsPerPage);
        $limit = min($offset + $this->elementsPerPage, $count);
        $nextPage = $limit < $count ? $page + 1 : false;
        $navigationEnd = [];
        if ($nextPage) {
            $navigationEnd[] = $this->getNavigation($page + 1, $url)
                + ['element' => $element]
                + ['icon' => 'fa fa-chevron-right'];
            if ($lastPage !== $nextPage) {
                $navigationEnd[] = $this->getNavigation($lastPage, $url)
                    + ['element' => $element]
                    + ['icon' => 'fa fa-angle-double-right'];
            }
        }
        array_push($data, ...$navigationEnd);
    }

    /**
     * Donne la base d'un lien de pagination
     * @param int    $page
     * @param string $url
     * @return array
     */
    private function getNavigation(int $page, string $url): array
    {
        return [
            'id' => 'nav_' . str_replace(
                ['/', '[', ']'],
                ['_', '-', ''],
                $page . '/' . $url
            ),
            'text' => __("... Page {0} ...", $page),
            'url' => $url,
            'navigation' => true,
            'children' => false,
        ];
    }

    /**
     * Ajoute les elements qu'on peut ajouter dans le jstree
     * @param array $data
     * @return void
     */
    private function appendAddable(array &$data)
    {
        if (!isset($data['children']) || !is_array($data['children'])) {
            return;
        }
        if (empty($data['addable'])) {
            foreach ($data['children'] as &$value) {
                $this->appendAddable($value);
            }
            unset($value);
            return;
        }

        // Liste des champs à ajouter
        $addable = array_map(fn($d) => $d['element'], $data['addable']);

        // On commence par lister les noeuds existants
        $existing = [];
        foreach ($data['children'] as $element => &$value) {
            if (isset($value['navigation']) && isset($value['children'])) {
                $index = array_search($value['element'], $addable);
                $value['addable'] = [$data['addable'][$index]];
                $value['order'] = [$value['element']];
            }
            if (!isset($existing[$value['element']])) {
                $existing[$value['element']] = [];
            }
            $existing[$value['element']][] = $element;
        }

        // On liste les champs requis
        $required = [];
        foreach ($data['addable'] as $add) {
            if ($add['count'] < $add['min']) {
                $required[] = $add['element'];
            }
        }
        if ($required) {
            $data['icon'] = 'fa fa-warning text-warning';
        }
        // on reforme le $data['children'] avec l'ajout des liens d'ajout
        $newChildren = [];
        foreach ($data['order'] ?? [] as $element) {
            // on commence par mettre les champs existants
            foreach ($existing[$element] ?? [] as $elementIndex) {
                $newChildren[$elementIndex] = $data['children'][$elementIndex];
                $this->appendAddable($newChildren[$elementIndex]);
            }
            if (in_array($element, $addable) && empty($data['navigation'])) {
                $cssClasses = in_array($element, $required)
                    ? 'fa fa-warning text-warning'
                    : 'fa fa-plus-square text-success';
                $newChildren['add_' . $element] = [
                    'text' => $element,
                    'element' => $element,
                    'is_button' => true,
                    'icon' => $cssClasses,
                    'children' => false,
                    'li_attr' => [
                        'class' => 'add-element text-success',
                        'title' => __("Ajouter {0}", $element),
                    ],
                ];
            }
        }
        if ($newChildren) {
            $data['children'] = $newChildren;
        }
    }

    /**
     * Retire les elements utiles uniquement pour la construction de data
     * @param array $data
     * @return void
     */
    private function sanitizeJstreeData(array &$data)
    {
        if (isset($data['node'])) {
            unset($data['node']);
        }
        if (isset($data['text']) && !isset($data['li_attr'])) {
            $data['li_attr'] = [];
        }
        // évite d'afficher le title du parent dans jstree
        if (isset($data['text']) && !isset($data['li_attr']['title'])) {
            $data['li_attr']['title'] = $data['text'];
        }
        if (isset($data['children']) && is_array($data['children'])) {
            $data['children'] = array_values($data['children']);
            foreach ($data['children'] as &$value) {
                $this->sanitizeJstreeData($value);
            }
            unset($value);
            if (!$data['children']) {
                $data['children'] = false;
            }
        }
    }

    /**
     * Donne le contenu d'un noeud spécifique
     * @param string $idNode
     * @return array
     * @throws Exception
     */
    public function getByIdNode(string $idNode): array
    {
        $path = explode(
            '/',
            preg_replace(
                '/(\[\d+)/',
                '$1]',
                str_replace(['_', '-'], ['/', '['], $idNode)
            )
        );
        $page = array_shift($path);
        if (!is_numeric($page)) { // next | prev
            $page = array_shift($path);
        }
        $this->page = $page;
        $node = $this->util->node('/ns:' . implode('/ns:', $path));
        $data = $this->loadDataNode($node, true);
        $data['children'] = $this->loadChildrenNodes($node);
        $this->loadFirstsNodes($data);
        $tagname = array_pop($path);
        if (preg_match('/^.*\[(\d+)]$/', $tagname, $m)) {
            $data['count'] = $m[1];
            $data['index'] = $m[1];
        }
        $parentUrl = implode('/', $path);
        $this->appendMetaData($data, $parentUrl ? $parentUrl . '/' : '', [$data['element']]);
        if (is_bool($data['children'])) {
            $data['children'] = [];
        }
        return $data['children'];
    }
}
