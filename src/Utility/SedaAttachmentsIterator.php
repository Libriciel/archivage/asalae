<?php

/**
 * Asalae\Utility\SedaAttachmentsIterator
 */

namespace Asalae\Utility;

use Asalae\Model\Table\ArchiveBinariesTable;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\HashUtility;
use Cake\Http\Exception\NotImplementedException;
use DOMDocument;
use DOMElement;
use DOMNodeList;
use Iterator;

/**
 * Iteration sur un dom
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SedaAttachmentsIterator implements Iterator
{
    /**
     * @var DOMNodeList
     */
    private DOMNodeList $list;
    /**
     * @var int
     */
    private int $count;
    /**
     * @var int
     */
    private int $index = 0;
    /**
     * @var string
     */
    private string $namespace;
    /**
     * @var DOMUtility
     */
    private DOMUtility $util;

    /**
     * Constructeur
     * @param DOMDocument $dom
     */
    public function __construct(DOMDocument $dom)
    {
        $this->util = new DOMUtility($dom);
        $this->namespace = $this->util->namespace;
        $this->list = $this->util->xpath->query('//ns:Document|//ns:BinaryDataObject');
        $this->count = $this->list->count();
    }

    /**
     * Return the current element
     * @link https://php.net/manual/en/iterator.current.php
     * @return array
     */
    public function current(): array
    {
        return $this->renderIndex($this->index);
    }

    /**
     * Donne un array avec les données liés à un index de l'itérateur
     * @param int $index
     * @return array
     */
    private function renderIndex(int $index): array
    {
        /** @var DOMElement $document */
        $document = $this->list->item($index);
        $filename = ArchiveBinariesTable::getBinaryFilename($this->util, $document);
        $size = $this->util->nodeValue('ns:Size', $document);
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
                $archiveObject = $document->parentNode;
                $size = 'N/A';
                /** @var DOMElement $contain */
                $contain = $this->util->node(
                    sprintf(
                        "ns:Integrity/ns:UnitIdentifier[text()=%s]/../ns:Contains",
                        $this->util->xpathQuote($filename)
                    )
                );
                if ($contain) {
                    $hash = $contain->nodeValue;
                    $hash_algo = $contain->getAttribute('algorithme');
                } else {
                    $hash = $hash_algo = null;
                }
                $ids = [
                    'ns:ArchivalAgencyArchiveIdentifier',
                    'ns:ArchivalAgencyObjectIdentifier',
                    'ns:ArchivalAgencyDocumentIdentifier',
                ];
                $identifier = $this->util
                    ->nodeValue(implode('|', $ids), $archiveObject);
                break;
            case NAMESPACE_SEDA_10:
                $archiveObject = $document->parentNode;
                /** @var DOMElement $integrity */
                $integrity = $this->util->node('ns:Integrity', $document);
                if ($integrity) {
                    $hash = $integrity->nodeValue;
                    $hash_algo = $integrity->getAttribute('algorithme');
                } else {
                    $hash = $hash_algo = null;
                }
                $ids = [
                    'ns:ArchivalAgencyArchiveIdentifier',
                    'ns:ArchivalAgencyObjectIdentifier',
                    'ns:ArchivalAgencyDocumentIdentifier',
                ];
                $identifier = $this->util
                    ->nodeValue(implode('|', $ids), $archiveObject);
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                /** @var DOMElement $dataObjectGroup */
                $dataObjectGroup = $document->parentNode;
                /** @var DOMElement $integrity */
                $integrity = $this->util->node('ns:MessageDigest', $document);
                if ($integrity) {
                    $hash = $integrity->nodeValue;
                    $hash_algo = $integrity->getAttribute('algorithm');
                } else {
                    $hash = $hash_algo = null;
                }
                $id = $document->getAttribute('id');
                $archiveUnits = $this->util->xpath
                    ->query(
                        sprintf(
                            '//ns:DataObjectReferenceId[text()=%s]/../..',
                            $this->util->xpathQuote($id)
                        )
                    );
                $identifier = $this->extractIdentifierSeda2x($archiveUnits);
                if (!$identifier && $dataObjectGroup->tagName === 'DataObjectGroup') {
                    $groupId = $dataObjectGroup->getAttribute('id');
                    $archiveUnits = $this->util->xpath
                        ->query(
                            sprintf(
                                '//ns:DataObjectGroupReferenceId[text()=%s]/../..',
                                $this->util->xpathQuote($groupId)
                            )
                        );
                    $identifier = $this->extractIdentifierSeda2x($archiveUnits);
                }
                break;
            default:
                throw new NotImplementedException();
        }

        return [
            'filename' => $filename,
            'size' => $size ?? 'N/C',
            'archival_agency_identifier' => $identifier ?? 'N/C',
            'hash' => $hash ?? 'N/C',
            'hash_algo' => !empty($hash_algo) ? HashUtility::toPhpAlgo($hash_algo) : 'N/C',
        ];
    }

    /**
     * Donne l'identifiant pour une liste archive_units (seda 2.1, 2.2)
     * @param DOMNodeList $archiveUnits
     * @return string
     */
    private function extractIdentifierSeda2x(DOMNodeList $archiveUnits): string
    {
        $identifier = '';
        /** @var DOMElement $archiveUnit */
        foreach ($archiveUnits as $archiveUnit) {
            if ($archiveUnit->tagName === 'ArchiveUnit') {
                $identifier = $this->util->nodeValue(
                    'ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier',
                    $archiveUnit
                );
                if ($identifier) {
                    break;
                }
            }
        }
        return $identifier ?: '';
    }

    /**
     * Move forward to next element
     * @link https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next(): void
    {
        ++$this->index;
    }

    /**
     * Return the key of the current element
     * @link https://php.net/manual/en/iterator.key.php
     * @return string
     */
    public function key(): string
    {
        return $this->index;
    }

    /**
     * Checks if current position is valid
     * @link https://php.net/manual/en/iterator.valid.php
     * @return bool The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid(): bool
    {
        return $this->index >= 0 && $this->index < $this->count;
    }

    /**
     * Rewind the Iterator to the first element
     * @link https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind(): void
    {
        $this->index = 0;
    }
}
