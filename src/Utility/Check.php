<?php

/**
 * Asalae\Utility\Check
 */

namespace Asalae\Utility;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Form\ConfigurationForm;
use AsalaeCore\Utility\Check as CoreCheck;
use AsalaeCore\Utility\Config;
use AsalaeCore\Utility\DatabaseUtility;
use AsalaeCore\Utility\LimitBreak;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateTime;
use Exception;
use PDO;
use ReflectionException;

/**
 * Vérification de l'application
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Check extends CoreCheck
{
    public const int DEFAULT_VOLUME_FAIL_RETRY = 5;
    public const int DEFAULT_VOLUME_FAIL_SLEEP_TIME = 2;
    public const int DEFAULT_CRON_MAX_DAYS_WITHOUT_EXECUTION = 7;

    /**
     * @var array
     */
    protected static $volumes;
    /**
     * @var array
     */
    protected static $treeSanity;

    /**
     * Faux si un des volumes a un problème
     * @return bool
     * @throws Exception
     */
    public static function volumesOK(): bool
    {
        foreach (self::volumes() as $data) {
            if (!$data['success']) {
                return false;
            }
        }
        return true;
    }

    /**
     * Vérifie la santé des volumes avec Cache
     * @return array
     * @throws Exception
     */
    public static function volumes(): array
    {
        if (!isset(static::$volumes)) {
            static::getVolumes();
        }
        return static::$volumes;
    }

    /**
     * Vérifie la santé des volumes
     * @throws Exception
     */
    private static function getVolumes(): void
    {
        try {
            $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        } catch (Exception) {
            static::$volumes = [];
            return;
        }
        static::$volumes = [];
        $query = $Volumes->find()
            ->where(['active' => true, 'secure_data_space_id IS NOT' => null])
            ->orderBy(['name']);
        $fails = [];
        $begin = microtime(true);
        foreach ($query as $volume) {
            $used = $volume->get('disk_usage') / $volume->get(
                'max_disk_usage'
            ) * 100;
            $alert = $used > min($volume->get('alert_rate') ?: 95, 95);
            static::$volumes[$volume->get('name')] = [
                'used' => round($used, 1) . '%',
                'alert' => $alert,
            ];
            $v =& static::$volumes[$volume->get('name')];
            try {
                $driver = VolumeManager::getDriver($volume);
                $test = $driver->test();
                $v['success'] = ($test['success'] ?? false) && $alert === false;
                if (!$v['success']) {
                    $fails[] = $volume;
                }
            } catch (Exception) {
                $v['success'] = false;
                $fails[] = $volume;
            }
        }
        $duration = floor(microtime(true) - $begin);

        $retry = Configure::read(
            'Check.volumes.retry',
            self::DEFAULT_VOLUME_FAIL_RETRY
        );
        $time = Configure::read(
            'Check.volumes.sleep_time',
            self::DEFAULT_VOLUME_FAIL_SLEEP_TIME
        );
        while ($fails && $retry > 0) {
            $retry--;
            if ($duration > 0) {
                sleep($time); // NOSONAR
            }
            $begin = microtime(true);
            $nfails = [];
            foreach ($fails as $volume) {
                $v =& static::$volumes[$volume->get('name')];
                try {
                    $driver = VolumeManager::getDriver($volume);
                    $test = $driver->test();
                    $v['success'] = ($test['success'] ?? false) && $v['alert'] === false;
                } catch (Exception) {
                }
                if (!$v['success']) {
                    $nfails[] = $volume;
                }
            }
            $fails = $nfails;
            $duration = floor(microtime(true) - $begin);
        }
    }

    /**
     * Liste des workers et comparaison avec la quantité en base
     * @return array
     * @throws Exception
     * @deprecated
     */
    public static function workers(): array
    {
        return [];
    }

    /**
     * Vrai si la configuration (app_local.json) est valide
     * @return array
     */
    public static function configurationErrors(): array
    {
        $form = new ConfigurationForm();
        $map = array_flip($form::CONFIG_MAP);
        $config = Hash::flatten(Config::readLocal());
        $data = ['config' => file_get_contents(Config::getPathToLocal())];
        foreach ($config as $key => $value) {
            if (isset($map[$key])) {
                $data[$map[$key]] = $value;
            }
        }
        return $form->validate($data) ? [] : $form->getErrors();
    }

    /**
     * Vérifi la cohérence des données issue d'un treeBehavior
     * @return bool
     * @throws ReflectionException
     */
    public static function treeSanityOk(): bool
    {
        foreach (self::treeSanity() as $arr) {
            if (!$arr['success']) {
                return false;
            }
        }
        return true;
    }

    /**
     * Vrai si les lft et rght sont cohérents
     * @return array
     * @throws ReflectionException
     */
    public static function treeSanity(): array
    {
        if (!isset(static::$treeSanity)) {
            static::$treeSanity = static::getTreeSanity();
        }
        return static::$treeSanity;
    }

    /**
     * Vrai si les lft et rght sont cohérents
     * @return array
     * @throws ReflectionException
     */
    private static function getTreeSanity(): array
    {
        $loc = TableRegistry::getTableLocator();
        $paths = App::classPath('Model/Table');
        $models = glob($paths[0] . '*Table.php');
        $results = [];
        foreach ($models as $file) {
            $name = basename($file, 'Table.php');
            $model = $loc->get($name);
            if ($model->hasBehavior('Tree')) {
                $tree = $model->getBehavior('Tree');
                $table = $model->getTable();
                $alias = $model->getAlias();
                $lft = $tree->getConfig('left');
                $rght = $tree->getConfig('right');
                $count = $model->find()->count();
                $results[$alias] = [
                    'success' => true,
                    'message' => __("OK"),
                ];
                if ($count === 0) {
                    continue;
                }
                $requiredMemory = $count * 300; // 261977704 pour 1.3M de lignes, soit 200 * 1.3M, 300 on est large
                LimitBreak::setMemoryLimit($requiredMemory);

                $conn = DatabaseUtility::getPdo();
                $stmt = $conn->prepare("SELECT $lft, $rght FROM $table ORDER BY $lft");
                $stmt->execute();

                $lftsRghts = [];
                while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    if (isset($lftsRghts[$data[$lft]])) {
                        $results[$alias] = [
                            'success' => false,
                            'message' => __("lft en double ({0})", $data[$lft]),
                        ];
                        continue 2;
                    }
                    if (isset($lftsRghts[$data[$rght]])) {
                        $results[$alias] = [
                            'success' => false,
                            'message' => __("rght en double ({0})", $data[$rght]),
                        ];
                        continue 2;
                    }
                    if ($data[$lft] >= $data[$rght]) {
                        $results[$alias] = [
                            'success' => false,
                            'message' => __("lft / rght inversés, ({0}, {1})", $data[$lft], $data[$rght]),
                        ];
                        continue 2;
                    }
                    $lftsRghts[$data[$lft]] = true;
                    $lftsRghts[$data[$rght]] = true;
                }

                $count = count($lftsRghts) / 2;
                $nums = array_keys($lftsRghts);
                $min = min($nums);
                $expectedMin = 1;
                $max = max($nums);
                $expectedMax = $count * 2;
                $sum = array_reduce(
                    $nums,
                    function ($a, $b) {
                        return $a + $b;
                    }
                );
                $expectedSum = $count * ($count * 2 + 1);
                if ($min !== $expectedMin) {
                    $results[$alias] = [
                        'success' => false,
                        'message' => __("min(lft) n'est pas égal à 1"),
                    ];
                    continue;
                }
                if ($max !== $expectedMax) {
                    $results[$alias] = [
                        'success' => false,
                        'message' => __("max(rght) n'est pas égal à {0}", $expectedMax),
                    ];
                    continue;
                }
                if ($sum !== $expectedSum) {
                    $results[$alias] = [
                        'success' => false,
                        'message' => __(
                            "somme des lft / rght ne correspond pas au chiffre attendu ({0})",
                            $expectedSum
                        ),
                    ];
                }
            }
        }
        return $results;
    }

    /**
     * Vide le cache statique
     * @return void
     */
    public static function clear()
    {
        self::$volumes = null;
        self::$treeSanity = null;
    }

    /**
     * Vérifi les executions de crons
     * @return bool
     */
    public static function cronOk(): bool
    {
        $CronExecutions = TableRegistry::getTableLocator()->get('CronExecutions');
        $lastOne = $CronExecutions->find()
            ->where(['date_end is not' => null])
            ->orderByDesc('date_end')
            ->first();
        if (!$lastOne) {
            return true;
        }
        $dateNow = new DateTime('now');
        $interval = $dateNow->diff($lastOne->get('date_end'));
        return $interval->days <= Configure::read(
            'Check.crons.max_days_since_last_execution',
            self::DEFAULT_CRON_MAX_DAYS_WITHOUT_EXECUTION
        );
    }
}
