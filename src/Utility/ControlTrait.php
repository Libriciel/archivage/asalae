<?php

/**
 * Asalae\Utility\TransferAnalyse
 */

namespace Asalae\Utility;

use Asalae\Model\Entity\TransferError;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\ArchiveBinariesTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Form\MessageSchema\MessageSchema;
use AsalaeCore\Form\MessageSchema\Seda02Schema;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Form\MessageSchema\Seda21Schema;
use AsalaeCore\Form\MessageSchema\Seda22Schema;
use AsalaeCore\I18n\Date as CoreDate;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\HashUtility;
use AsalaeCore\Utility\PreControlMessages;
use AsalaeCore\Utility\ValidatorXml;
use Asalae\Model\Table\KillablesTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Date as CakeDate;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInterval;
use DateMalformedStringException;
use DateTimeInterface;
use DOMElement;
use DOMNode;
use DOMXPath;
use DateTime;
use Exception;

/**
 * Pour ControlWorker et Transfers::analyse
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait ControlTrait
{
    /**
     * @var KillablesTable
     */
    public $Killables;
    /**
     * @var PreControlMessages
     */
    private $PreControlMessages;
    /**
     * @var EntityInterface
     */
    private $Transfer;
    /**
     * @var DOMUtility
     */
    private DOMUtility $util;
    /**
     * @var array
     */
    private array $errors;

    /**
     * Validation xsd du bordereau
     * @param array         $errors
     * @param callable|null $fn
     * @throws Exception
     */
    private function validateMessage(array &$errors, ?callable $fn = null)
    {
        if ($fn) {
            $fn();
        }
        $validator = $this->getValidator(
            $this->PreControlMessages->getSchema()
        );
        if (!$validator->validate($this->Transfer->get('xml'))) {
            foreach ($validator->errorsDetails as $error) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_MALFORMED_MESSAGE,
                    'message' => __(
                        "Conformité {0} : {1} en ligne {2}",
                        $this->Transfer->get('message_versiontrad'),
                        $error['message'],
                        $error['line']
                    ),
                ];
            }
        }
    }

    /**
     * Donne le validateur pour un schema donné
     * @param string $schema
     * @return ValidatorXml
     * @throws Exception
     */
    private function getValidator(string $schema): ValidatorXml
    {
        $validator = Utility::get('ValidatorXml', $schema);
        $validator->setSchema($schema);
        return $validator;
    }

    /**
     * Effectue une validation sur les pièces jointes
     * @param array         $errors
     * @param callable|null $fnNode
     * @param callable|null $fnAttachment
     */
    private function validateAttachments(
        array &$errors,
        ?callable $fnNode = null,
        ?callable $fnAttachment = null
    ) {
        $loc = TableRegistry::getTableLocator();
        $TransferAttachments = $loc->get('TransferAttachments');
        $namespace = $this->PreControlMessages->getNamespace();
        $xpath = $this->util()->xpath;
        $exists = false;
        $transferId = $this->Transfer->get('id');
        $dataDir = AbstractGenericMessageForm::getDataDirectory($transferId);
        $basePath = $dataDir . DS . 'attachments';
        $files = [];

        /** @var Seda02Schema|Seda10Schema|Seda21Schema|Seda22Schema $schemaClassname */
        $schemaClassname = MessageSchema::getSchemaClassname($namespace);
        $documentTagname = $schemaClassname::getTagname(
            'Document',
            'ArchiveTransfer'
        );
        $query = $xpath->query('//ns:' . $documentTagname);
        $count = $query->count();
        /**
         * @var DOMElement $documentNode
         * @var DOMElement $filenameNode
         * @var DOMElement $uriNode
         * @var DOMElement $attachmentNode
         */
        foreach ($query as $i => $documentNode) {
            $filename = null;
            $xmlFormat = null;
            $xmlMime = null;
            $attachmentNode = $this->util()->node('ns:Attachment', $documentNode);
            if ($attachmentNode) {
                $filename = $attachmentNode->getAttribute('filename');
                $xmlFormat = $attachmentNode->getAttribute('format');
                $xmlMime = $attachmentNode->getAttribute('mimeCode');
            }
            if (in_array($namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
                $filename = ArchiveBinariesTable::getBinaryFilename($this->util(), $documentNode);
                $xmlFormat = $this->util()
                    ->nodeValue('ns:FormatIdentification/ns:FormatId', $documentNode)
                    ?: $xmlFormat;
                $xmlMime = $this->util()
                    ->nodeValue('ns:FormatIdentification/ns:MimeType', $documentNode)
                    ?: $xmlMime;
            }

            $exists = true;
            if (isset($files[$filename])) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_ATTACHMENT_MISSING,
                    'message' => __(
                        "Pièce jointe déjà référencée dans le bordereau : {0}",
                        $filename
                    ),
                ];
                continue;
            }
            $files[$filename] = true;
            $path = $basePath . DS . $filename;
            if ($fnNode) {
                $fnNode($filename, $i + 1, $count);
            }
            if (empty($filename)) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_INVALID_HASH,
                    'message' => __(
                        "Nom du fichier de la pièce jointe non renseigné : {0}",
                        $this->getNodePath($documentNode)
                    ),
                ];
                continue;
            } elseif (is_dir($path)) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_ATTACHMENT_MISSING,
                    'message' => __(
                        "Nom du fichier de la pièce jointe est un dossier et non un fichier : {0}",
                        $filename
                    ),
                ];
                continue;
            } elseif (!file_exists($path)) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_ATTACHMENT_MISSING,
                    'message' => __(
                        "Fichier de la pièce jointe absent (non transmis) : {0}",
                        $filename
                    ),
                ];
                continue;
            }
            $transferAttachment = $TransferAttachments->find()
                ->where(
                    [
                        'transfer_id' => $this->Transfer->get('id'),
                        'filename' => $filename,
                    ]
                )
                ->first();
            if (!$transferAttachment) {
                continue;
            }
            $dbFormat = $transferAttachment->get('format');
            $dbMime = $transferAttachment->get('mime');
            $this->validateIntegrity(
                $documentNode,
                $filename,
                $transferAttachment,
                $errors
            );

            if ($transferAttachment->get('virus_name')) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_ATTACHMENT_MISSING,
                    'message' => __(
                        "Virus {0} détecté dans le fichier {1}",
                        $transferAttachment->get('virus_name'),
                        $filename
                    ),
                ];
            }
            if (!$dbFormat) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'warning',
                    'code' => TransferError::CODE_NON_COMPLIANT_FORMAT,
                    'message' => __(
                        "Le format de la pièce jointe {0} n'a pas pu être déterminé",
                        $filename
                    ),
                ];
            } elseif ($xmlFormat && $xmlFormat !== $dbFormat) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'warning',
                    'code' => TransferError::CODE_NON_COMPLIANT_FORMAT,
                    'message' => __(
                        "Format du document {0} ne correspond pas au format détecté {1} pour la pièce jointe {2}",
                        $xmlFormat,
                        $dbFormat,
                        $filename
                    ),
                ];
            }
            if ($dbFormat && !$transferAttachment->get('valid')) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'warning',
                    'code' => TransferError::CODE_NON_COMPLIANT_FORMAT,
                    'message' => __(
                        "Possible erreur sur la validité du format de la pièce jointe {0}",
                        $filename
                    ),
                ];
            }
            if ($xmlMime && $xmlMime !== $dbMime) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'warning',
                    'code' => TransferError::CODE_NON_COMPLIANT_FORMAT,
                    'message' => __(
                        "Mimetype du document {0} ne correspond pas au 
                        mimetype détecté {1} pour la pièce jointe {2}",
                        $xmlMime,
                        $dbMime,
                        $filename
                    ),
                ];
            }
        }
        if (!$exists) {
            $errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_ATTACHMENT_MISSING,
                'message' => __(
                    "Aucune pièce jointe référencée dans le bordereau"
                ),
            ];
            return;
        }
        $loc = TableRegistry::getTableLocator();
        $TransferAttachments = $loc->get('TransferAttachments');
        $query = $TransferAttachments->find()
            ->where(['transfer_id' => $this->Transfer->id]);

        /** @var EntityInterface $attachment */
        foreach ($query as $i => $attachment) {
            if ($fnAttachment) {
                $fnAttachment($attachment->get('filename'), $i + 1, $count);
            }
            if (!isset($files[$attachment->get('filename')])) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_ATTACHMENT_MISSING,
                    'message' => __(
                        "Fichier {0} non référencé dans le bordereau",
                        $attachment->get('filename')
                    ),
                ];
            }
        }

        // Vérification que chaque archives possèdent au moins 1 fichier
        /** @var DOMElement $archiveNode */
        foreach ($xpath->query('//ns:Archive|ns:Contains') as $archiveNode) {
            if ($xpath->query('.//ns:Document/ns:Attachment', $archiveNode)->count() === 0) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_ATTACHMENT_MISSING,
                    'message' => __(
                        "Aucune pièce jointe référencée dans un des noeuds archive"
                    ),
                ];
                break;
            }
        }
        foreach ($xpath->query('//ns:DescriptiveMetadata/ns:ArchiveUnit') as $archiveNode) {
            if ($xpath->query('.//ns:DataObjectReference', $archiveNode)->count() === 0) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_ATTACHMENT_MISSING,
                    'message' => __(
                        "Aucune pièce jointe référencée dans un des noeuds racine ArchiveUnit"
                    ),
                ];
                break;
            }
        }
    }

    /**
     * Donne l'instance DOMUtility lié au dom du précontrol
     * @return DOMUtility
     */
    private function util(): DOMUtility
    {
        if (empty($this->util)) {
            $dom = $this->PreControlMessages->getDom();
            $this->util = new DOMUtility($dom);
        }
        return $this->util;
    }

    /**
     * Donne le chemin xml sous la forme ArchiveTransfer/Contains/Document
     * @param DOMElement $item
     * @return string
     */
    private function getNodePath(DOMElement $item): string
    {
        $path = $this->getTagnameWithCount($item);
        if ($item->parentNode instanceof DOMElement) {
            $count = 0;
            $suffix = '';
            foreach ($item->parentNode->childNodes as $node) {
                if ($node instanceof DOMElement && $node->tagName === $path) {
                    $count++;
                    if ($node === $item) {
                        $suffix = '[' . $count . ']';
                        break;
                    }
                }
            }
            if ($count > 1) {
                $path .= $suffix;
            }
            $ancestors = $this->getNodePath($item->parentNode);
            $path = ($ancestors ? $ancestors . '/' : '') . $path;
        }
        return $path;
    }

    /**
     * Ajoute un [n] au tagName d'un item, n correspondant à l'index xpath
     * N'ajoute rien si le nom de balise est unique au sein du parent
     * @param DOMElement $item
     * @return string
     */
    private function getTagnameWithCount(DOMElement $item): string
    {
        $tagName = $item->tagName;
        if (!$item->parentNode instanceof DOMElement) {
            return $tagName;
        }
        $count = 0;
        $suffix = '';
        foreach ($item->parentNode->childNodes as $node) {
            if ($node instanceof DOMElement && $node->tagName === $tagName) {
                $count++;
                if ($node === $item) {
                    $suffix = '[' . $count . ']';
                }
            }
        }
        if ($count > 1) {
            $tagName .= $suffix;
        }
        return $tagName;
    }

    /**
     * Compare les hash du bordereau avec la valeur en base/recalcul si algo !=
     * @param DOMElement      $documentNode
     * @param string          $filename
     * @param EntityInterface $transferAttachment
     * @param array           $errors
     */
    private function validateIntegrity(
        DOMElement $documentNode,
        string $filename,
        EntityInterface $transferAttachment,
        array &$errors
    ) {
        $namespace = $this->PreControlMessages->getNamespace();
        if ($namespace === NAMESPACE_SEDA_02) {
            $path = sprintf(
                '/ns:ArchiveTransfer/ns:Integrity/ns:UnitIdentifier[normalize-space(.)=%s]/../ns:Contains',
                DOMUtility::xpathQuote($filename)
            );
            $integrity = $this->PreControlMessages->getXpath()
                ->query($path)
                ->item(0);
        } elseif ($namespace === NAMESPACE_SEDA_10) {
            $integrity = $documentNode->getElementsByTagName('Integrity')
                ->item(0);
        } elseif (in_array($namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            $integrity = $documentNode->getElementsByTagName('MessageDigest')
                ->item(0);
        } else {
            return;
        }
        if ($integrity instanceof DOMElement) {
            $value = trim($integrity->nodeValue);
            $algo = $integrity->getAttribute('algorithme');
            $algo = $algo ?: $integrity->getAttribute('algorithm');
            $algo = HashUtility::toPhpAlgo($algo);
        } else {
            $value = null;
            $algo = null;
        }

        if (empty($algo) && !empty($value)) {
            $errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_INVALID_HASH,
                'message' => __(
                    "Algorithme de hachage non transmis pour la pièce jointe {0}",
                    $filename
                ),
            ];
        }
        if (empty($value) && !empty($algo)) {
            $errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_INVALID_HASH,
                'message' => __(
                    "Empreinte non transmise pour la pièce jointe {0}",
                    $filename
                ),
            ];
            return;
        }
        if ($algo && !in_array($algo, hash_algos())) {
            $errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_INVALID_HASH,
                'message' => __(
                    "Algorithme de hachage {0} non identifié pour la pièce jointe {1}",
                    $algo,
                    $filename
                ),
            ];
            return;
        }
        $attachmentAlgo = $transferAttachment->get('hash_algo');
        if ($attachmentAlgo && !in_array($attachmentAlgo, hash_algos())) {
            $errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_INVALID_HASH,
                'message' => __(
                    "Algorithme de hachage {0} non identifié pour la pièce jointe {1}",
                    $attachmentAlgo,
                    $filename
                ),
            ];
            return;
        }

        if ($algo && $algo !== $transferAttachment->get('hash_algo')) {
            $hash = hash_file($algo, $transferAttachment->get('path'));
        } else {
            $hash = $transferAttachment->get('hash');
        }
        if ($algo && !HashUtility::hashMatch($hash, $value)) {
            $errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_ATTACHMENT_MISSING,
                'message' => __(
                    "Empreinte invalide pour la pièce jointe {0} :
                     empreinte du bordereau {1}, empreinte calculée {2}",
                    $filename,
                    $value,
                    $hash
                ),
            ];
        }
    }

    /**
     * Validation du service producteur
     * @param array                $errors
     * @param EntityInterface|null $sa     Service d'archives
     */
    private function validateSp(array &$errors, EntityInterface $sa = null)
    {
        $pre = $this->PreControlMessages;
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $namespace = $pre->getNamespace();
        $xpath = new DOMXPath($pre->getDom());
        $xpath->registerNamespace('ns', $namespace);

        $archiveObject = [
            NAMESPACE_SEDA_02 => '//ns:Contains/ns:ContentDescription/ns:OriginatingAgency/ns:Identification',
            NAMESPACE_SEDA_10 => '//ns:Archive/ns:ContentDescription/ns:OriginatingAgency/ns:Identification',
            NAMESPACE_SEDA_21 => '//ns:Content/ns:OriginatingAgency/ns:Identifier',
            NAMESPACE_SEDA_22 => '//ns:Content/ns:OriginatingAgency/ns:Identifier',
        ];
        $xpathQuery = '//ns:Archive/ns:ContentDescription/ns:OriginatingAgency/ns:Identification'
            . '|' . $archiveObject[$namespace];
        /** @var DOMElement $item */
        foreach ($xpath->query($xpathQuery) as $item) {
            $identifier = $item->nodeValue;
            $entities = $OrgEntities->find()
                ->select(
                    [
                        'OrgEntities.active',
                        'TypeEntities.code',
                        'LastEaccpfs.from_date',
                        'LastEaccpfs.to_date',
                    ]
                )
                ->contain(['LastEaccpfs', 'TypeEntities'])
                ->where(['OrgEntities.identifier' => $identifier]);
            if ($sa) {
                $entities->andWhere(
                    [
                        'OrgEntities.lft >=' => $sa->get('lft'),
                        'OrgEntities.rght <=' => $sa->get('rght'),
                    ]
                );
            }
            if ($entities->count() === 0) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_UNRECOGNIZED_ORIGINATING_AGENCY,
                    'message' => __(
                        "Entité du service producteur du bordereau non trouvée : {0}",
                        $identifier
                    ),
                ];
            } else {
                $sp = null;
                /** @var EntityInterface $entity */
                foreach ($entities as $entity) {
                    if (
                        in_array(
                            $entity->get('type_entity')->get('code'),
                            OrgEntitiesTable::CODE_ORIGINATING_AGENCIES
                        )
                    ) {
                        $sp = $entity;
                        break;
                    }
                }
                if (!$sp) {
                    $errors[] = [
                        'transfer_id' => $this->Transfer->get('id'),
                        'level' => 'error',
                        'code' => TransferError::CODE_UNRECOGNIZED_ORIGINATING_AGENCY,
                        'message' => __(
                            "Entité correpondante au Service producteur du bordereau 
                            non de type Service Producteur : {0}",
                            $identifier
                        ),
                    ];
                }
                if (!$sp->get('active')) {
                    $errors[] = [
                        'transfer_id' => $this->Transfer->get('id'),
                        'level' => 'error',
                        'code' => TransferError::CODE_UNRECOGNIZED_ORIGINATING_AGENCY,
                        'message' => __(
                            "Entité correspondante au Service producteur du bordereau non active : {0}",
                            $identifier
                        ),
                    ];
                }
            }
        }
    }

    /**
     * Validation des Mots clefs
     * @param array $errors
     * @throws Exception
     */
    private function validateKeywords(array &$errors)
    {
        $doubles = [];
        $pre = $this->PreControlMessages;
        $util = new DOMUtility($pre->getDom());
        $xpath = $util->xpath;

        /** @var DOMNode $node */
        foreach ($xpath->query('//ns:KeywordContent') as $node) {
            $content = $node->nodeValue;
            $esc = DOMUtility::xpathQuote($content);
            $parent = $node->parentNode->parentNode;
            $contentPath = $parent->getNodePath() . "/ns:*/ns:KeywordContent[text()=$esc]";

            if ($xpath->query($contentPath)->count() > 1) {
                $doubles[$this->getNodePath($parent)] = $content;
            }

            $contentLength = mb_strlen($node->nodeValue);
            if ($contentLength > 255) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_NAME,
                    'message' => __(
                        "Mot clé trop long (max 255 caractères) : {0} ({1})",
                        $node->nodeValue,
                        $util->getElementXpath($node)
                    ),
                ];
            }
        }

        /** @var DOMNode $node */
        foreach ($xpath->query('//ns:KeywordReference') as $node) {
            $contentLength = mb_strlen($node->nodeValue);
            if ($contentLength > 255) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_NAME,
                    'message' => __(
                        "Référence de mot clé trop long (max 255 caractères) : {0} ({1})",
                        $node->nodeValue,
                        $util->getElementXpath($node)
                    ),
                ];
            }
        }

        foreach ($doubles as $doublePath => $doubleContent) {
            $errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_CUSTOM_KEYWORD,
                'message' => __("Mot clé en double : {0} ({1})", $doubleContent, $doublePath),
            ];
        }
    }

    /**
     * Validation des profils d'archives
     * @param array           $errors
     * @param EntityInterface $sa
     * @throws Exception
     */
    private function validateProfiles(array &$errors, EntityInterface $sa)
    {
        $pre = $this->PreControlMessages;
        $Profiles = TableRegistry::getTableLocator()->get('Profiles');
        $namespace = $pre->getNamespace();
        $xpath = new DOMXPath($pre->getDom());
        $xpath->registerNamespace('ns', $namespace);
        /** @var DOMElement $element */
        foreach ($xpath->query('//ns:ArchivalProfile') as $element) {
            $identifier = $element->nodeValue;
            $query = $Profiles->find()
                ->contain(['Fileuploads'])
                ->where(['identifier' => $identifier])
                ->andWhere(['org_entity_id' => $sa->get('id')]);
            $profile = $query->first();
            if (!$profile) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_INVALID_PROFILE,
                    'message' => __("Profil d'archive non trouvé : {0}", $identifier),
                ];
                continue;
            }
            if (!$profile->get('active')) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_INVALID_PROFILE,
                    'message' => __("Profil d'archive non actif : {0}", $identifier),
                ];
            }
            foreach ((array)$profile->get('fileuploads') as $file) {
                $validator = $this->getValidator($file->get('path'));
                if ($validator->validate($this->Transfer->get('xml'))) {
                    continue;
                }
                foreach ($validator->errorsDetails as $error) {
                    $errors[] = [
                        'transfer_id' => $this->Transfer->get('id'),
                        'level' => 'error',
                        'code' => TransferError::CODE_INVALID_PROFILE,
                        'message' => __(
                            "Schema du profil {0} : {1} en ligne {2}",
                            $identifier,
                            $error['message'],
                            $error['line']
                        ),
                    ];
                }
            }
        }
    }

    /**
     * Validation des niveaux de service
     * @param array           $errors
     * @param EntityInterface $sa
     */
    private function validateServiceLevels(array &$errors, EntityInterface $sa)
    {
        $pre = $this->PreControlMessages;
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $namespace = $pre->getNamespace();
        $xpath = new DOMXPath($pre->getDom());
        $xpath->registerNamespace('ns', $namespace);
        /** @var DOMElement $element */
        foreach ($xpath->query('//ns:ServiceLevel') as $element) {
            $identifier = $element->nodeValue;
            $query = $ServiceLevels->find()
                ->where(['identifier' => $identifier])
                ->andWhere(['org_entity_id' => $sa->get('id')]);
            $serviceLevel = $query->first();
            if (!$serviceLevel) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_INVALID_PROFILE,
                    'message' => __("Niveau de service non trouvé : {0}", $identifier),
                ];
                continue;
            }
            if (!$serviceLevel->get('active')) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_INVALID_PROFILE,
                    'message' => __("Niveau de service non actif : {0}", $identifier),
                ];
            }
        }
    }

    /**
     * Validation des accords de versement
     * @param array                $errors
     * @param EntityInterface|null $sa
     * @param EntityInterface|null $sv
     * @return array|EntityInterface|null
     * @throws Exception
     */
    private function validateAgreements(
        array &$errors,
        EntityInterface $sa = null,
        EntityInterface $sv = null
    ) {
        $this->errors =& $errors;
        if (empty($sa)) {
            $sa = $this->validateSa($errors);
        }
        if (empty($sv)) {
            $sv = $this->validateSv($errors, $sa);
        }
        $schema = MessageSchema::getSchema($this->util()->namespace);
        /** @var Seda02Schema|Seda10Schema|Seda21Schema|Seda22Schema $schema */
        $archiveTagname = $schema::getTagname('Archive', 'ArchiveTransfer');
        $agreementTagname = $schema::getTagname(
            'ArchivalAgreement',
            'ArchiveTransfer'
        );
        $loc = TableRegistry::getTableLocator();
        $Agreements = $loc->get('Agreements');
        $defaultAgreement = $Agreements->find()
            ->where(
                ['default_agreement IS' => true, 'org_entity_id' => $sa->id]
            )
            ->contain(
                [
                    'TransferringAgencies',
                    'OriginatingAgencies',
                    'ServiceLevels',
                    'Profiles',
                ]
            )
            ->first();
        $firstAgreement = null;
        $xpath = in_array($this->util()->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])
            ? 'ns:DataObjectPackage/ns:DescriptiveMetadata/ns:' . $archiveTagname
            : 'ns:' . $archiveTagname;
        /** @var DOMElement $archiveNode */
        $archiveNodes = $this->util()->xpath->query($xpath);
        foreach ($archiveNodes as $archiveNode) {
            $this->validateArchiveAgreement(
                $archiveNode,
                $agreementTagname,
                $defaultAgreement,
                $firstAgreement,
                $sa,
                $sv
            );
        }
        return $firstAgreement;
    }

    /**
     * Validation du service d'archive
     * @param array $errors
     * @return array|EntityInterface|null
     * @throws Exception
     */
    private function validateSa(array &$errors)
    {
        $pre = $this->PreControlMessages;
        $now = new DateTime();
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $path = $pre->identifierArchivalAgencyPaths[$pre->getNamespace()];
        $namespace = $pre->getNamespace();
        $xpath = new DOMXPath($pre->getDom());
        $xpath->registerNamespace('ns', $namespace);
        $identifier = $xpath->query($path);
        if ($identifier) {
            $identifier = $identifier->item(0)->nodeValue;
        }
        $entities = $OrgEntities->find()
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.active',
                    'OrgEntities.lft',
                    'OrgEntities.rght',
                    'TypeEntities.code',
                    'LastEaccpfs.from_date',
                    'LastEaccpfs.to_date',
                ]
            )
            ->contain(['LastEaccpfs', 'TypeEntities'])
            ->where(['OrgEntities.identifier' => $identifier]);
        if ($entities->count() === 0) {
            $errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_UNRECOGNIZED_ARCHIVAL_AGENCY,
                'message' => __(
                    "Entité du service d'archives du bordereau non trouvée : {0}",
                    $identifier
                ),
            ];
            return null;
        }
        $sa = null;
        /** @var EntityInterface $entity */
        foreach ($entities as $entity) {
            if ($entity->get('type_entity')->get('code') === 'SA') {
                $sa = $entity;
                break;
            }
        }
        if (!$sa) {
            $errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_UNRECOGNIZED_ARCHIVAL_AGENCY,
                'message' => __(
                    "Entité correpondante au Service d'archives du bordereau 
                    non de type Service d'Archives : {0}",
                    $identifier
                ),
            ];
            $sa = $entities->first();
        }
        if (!$sa->get('active')) {
            $errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_UNRECOGNIZED_ARCHIVAL_AGENCY,
                'message' => __(
                    "Entité correspondante au Service d'archives du bordereau non active : {0}",
                    $identifier
                ),
            ];
        }
        $lastEaccpf = $sa->get('last_eaccpf');
        if ($lastEaccpf instanceof EntityInterface) {
            $from = $this->dateToDateTime($lastEaccpf->get('from_date'));
            $to = $this->dateToDateTime($lastEaccpf->get('to_date'));
        } else {
            $from = null;
            $to = null;
        }
        if (($from && $now < $from) || ($to && $now > $to)) {
            $errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_UNRECOGNIZED_ARCHIVAL_AGENCY,
                'message' => __(
                    "Entité correspondante au Service d'archives du bordereau 
                    en dehors des dates d'existence : {0}",
                    $identifier
                ),
            ];
        }
        return $sa;
    }

    /**
     * Assure d'avoir un DateTime pour comparaison
     * @param DateTimeInterface|CoreDate|CakeDate|null $date
     * @return DateTime
     * @throws DateMalformedStringException
     */
    private function dateToDateTime(DateTimeInterface|CoreDate|CakeDate|null $date): ?DateTime
    {
        if ($date instanceof CakeDate || $date instanceof CoreDate) {
            return new DateTime($date->format('Y-m-d') . ' 00:00:00');
        } elseif ($date instanceof DateTimeInterface) {
            $date = new DateTime($date);
        }
        return $date;
    }

    /**
     * Validation du service versant
     * @param array                $errors
     * @param EntityInterface|null $sa     Service d'archives
     * @return array|EntityInterface|null
     * @throws DateMalformedStringException
     */
    private function validateSv(array &$errors, EntityInterface $sa = null)
    {
        $pre = $this->PreControlMessages;
        $now = new DateTime();
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $path = $pre->identifierTransferringAgencyPaths[$pre->getNamespace()];
        $namespace = $pre->getNamespace();
        $xpath = new DOMXPath($pre->getDom());
        $xpath->registerNamespace('ns', $namespace);
        $identifier = $xpath->query($path);
        if ($identifier) {
            $identifier = $identifier->item(0)->nodeValue;
        }
        $entities = $OrgEntities->find()
            ->select(
                [
                    'OrgEntities.active',
                    'OrgEntities.identifier',
                    'TypeEntities.code',
                    'LastEaccpfs.from_date',
                    'LastEaccpfs.to_date',
                ]
            )
            ->contain(['LastEaccpfs', 'TypeEntities'])
            ->where(['OrgEntities.identifier' => $identifier]);
        if ($sa) {
            $entities->andWhere(
                [
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                ]
            );
        }
        $sv = null;
        if ($entities->count() === 0) {
            $errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_UNRECOGNIZED_TRANSFERRING_AGENCY,
                'message' => __(
                    "Entité du service versant du bordereau non trouvée : {0}",
                    $identifier
                ),
            ];
        } else {
            /** @var EntityInterface $entity */
            foreach ($entities as $entity) {
                if (
                    in_array(
                        $entity->get('type_entity')->get('code'),
                        OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES
                    )
                ) {
                    $sv = $entity;
                    break;
                }
            }
            if (!$sv) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_UNRECOGNIZED_TRANSFERRING_AGENCY,
                    'message' => __(
                        "Entité correpondante au Service versant du bordereau 
                        non de type Service Versant : {0}",
                        $identifier
                    ),
                ];
                $sv = $entities->first();
            }
            if (!$sv->get('active')) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_UNRECOGNIZED_TRANSFERRING_AGENCY,
                    'message' => __(
                        "Entité correspondante au Service versant du bordereau non active : {0}",
                        $identifier
                    ),
                ];
            }
            $lastEaccpf = $sv->get('last_eaccpf');
            if ($lastEaccpf instanceof EntityInterface) {
                $from = $this->dateToDateTime($lastEaccpf->get('from_date'));
                $to = $this->dateToDateTime($lastEaccpf->get('to_date'));
            } else {
                $from = null;
                $to = null;
            }
            if (($from && $now < $from) || ($to && $now > $to)) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_UNRECOGNIZED_TRANSFERRING_AGENCY,
                    'message' => __(
                        "Entité correspondante au Service versant du bordereau 
                        en dehors des dates d'existence : {0}",
                        $identifier
                    ),
                ];
            }
        }
        return $sv;
    }

    /**
     * Valide l'accord de versement lié à une archive
     * @param DOMElement           $archiveNode
     * @param string               $agreementTagname
     * @param EntityInterface|null $defaultAgreement
     * @param EntityInterface|null $firstAgreement
     * @param EntityInterface|null $sa
     * @param EntityInterface|null $sv
     * @return void
     * @throws DateMalformedStringException
     */
    private function validateArchiveAgreement(
        DOMElement $archiveNode,
        string $agreementTagname,
        ?EntityInterface $defaultAgreement,
        ?EntityInterface &$firstAgreement,
        EntityInterface $sa = null,
        EntityInterface $sv = null
    ) {
        $now = new DateTime();
        $loc = TableRegistry::getTableLocator();
        $Agreements = $loc->get('Agreements');
        $Transfers = $loc->get('Transfers');
        $agreementNode = in_array($this->util()->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])
            ? $this->util()->xpath->query('ns:' . $agreementTagname)->item(0)
            : $this->util()->xpath->query('ns:' . $agreementTagname, $archiveNode)
                ->item(0);
        if (!$agreementNode && !$defaultAgreement) {
            return;
        } elseif (!$agreementNode) {
            $agreement = $defaultAgreement;
        } else {
            $agreement = $Agreements->find()
                ->where(
                    [
                        'identifier' => $agreementNode->nodeValue,
                        'org_entity_id' => $sa->id,
                    ]
                )
                ->contain(
                    [
                        'TransferringAgencies',
                        'OriginatingAgencies',
                        'ServiceLevels',
                        'Profiles',
                    ]
                )
                ->first();
            if (!$agreement) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_INVALID_AGREEMENT,
                    'message' => __(
                        "Accord de versement non trouvé : {0}",
                        $agreementNode->nodeValue
                    ),
                ];
                return;
            }
        }
        if (!$firstAgreement) {
            $firstAgreement = $agreement;
        }
        $agreementIdentifier = $agreement->get('identifier');
        if (!$agreement->get('active')) {
            $this->errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_INVALID_AGREEMENT,
                'message' => __(
                    "Accord de versement non actif : {0}",
                    $agreementIdentifier
                ),
            ];
        }
        switch ($agreement->get('transfer_period')) {
            case 'day':
                $tdate = new DateTime('today 00:00:00');
                break;
            case 'week':
                $tdate = new DateTime('last monday 00:00:00');
                break;
            case 'month':
                $tdate = new DateTime('first day of this month 00:00:00');
                break;
            case 'year':
                $tdate = new DateTime('first day of this year 00:00:00');
        }
        if (isset($tdate) && $agreement->get('max_transfers')) {
            $count = $Transfers->find()
                ->where(
                    [
                        'agreement_id' => $agreement->id,
                        'state NOT IN' => [
                            'creating',
                            'preparating',
                            'rejected',
                        ],
                        'created >=' => $tdate,
                    ]
                )
                ->count();
            if ($count >= $agreement->get('max_transfers')) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_AGREEMENT_TRANSFER_LIMIT,
                    'message' => __(
                        "Nombre de transferts maximum pour une période dépassé"
                        . " pour l'accord de versement ({0})",
                        $agreementIdentifier
                    ),
                ];
            }
        }

        $from = $this->dateToDateTime($agreement->get('date_begin'));
        $to = $this->dateToDateTime($agreement->get('date_end'));
        if (($from && $now < $from) || ($to && $now > $to)) {
            $this->errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_AGREEMENT_EXPIRED,
                'message' => __(
                    "Accord de versement en dehors des dates de validité : {0}",
                    $agreementIdentifier
                ),
            ];
        }
        $this->validateAgreementTransferringAgencies($agreement, $sv);
        $this->validateAgreementOriginatingAgencies($agreement, $archiveNode);
        $this->validateAgreementProfiles($agreement, $archiveNode);
        $this->validateAgreementServiceLevels($agreement, $archiveNode);
        $this->validateAgreementDataFormat($agreement, $archiveNode);
        if ($agreement->get('max_size_per_transfer')) {
            $query = $loc->get('TransferAttachments')->find();
            $size = $query->select(['sum' => $query->func()->sum('size')])
                ->where(['transfer_id' => $this->Transfer->id])
                ->first();
            $size = $size ? $size->get('sum') : 0;
            if ($size > $agreement->get('max_size_per_transfer')) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_NON_COMPLIANT_FORMAT,
                    'message' => __(
                        "Volume des pièces jointes ({0} octets) dépasse le volume maximum autorisé 
                            ({1} octets) pour un transfert unitaire pour l'accord de versement : {2}",
                        $size,
                        $agreement->get('max_size_per_transfer'),
                        $agreementIdentifier
                    ),
                ];
            }
        }
    }

    /**
     * Vérifi les versants autorisés par l'accord de versement
     * @param EntityInterface      $agreement
     * @param EntityInterface|null $sv
     * @return void
     */
    private function validateAgreementTransferringAgencies(
        EntityInterface $agreement,
        EntityInterface $sv = null
    ) {
        $agreementIdentifier = $agreement->get('identifier');
        if ($sv && !$agreement->get('allow_all_transferring_agencies')) {
            $inList = false;
            /** @var EntityInterface $ta */
            foreach ((array)$agreement->get('transferring_agencies') as $ta) {
                if ($ta->get('identifier') === $sv->get('identifier')) {
                    $inList = true;
                    break;
                }
            }
            if (!$inList) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_TRANSFERRING_AGENCY_NOT_ALLOWED,
                    'message' => __(
                        "Le service versant ({0}) du transfert ne fait pas 
                            partie des services versants autorisés par l'accord de versement ({1})",
                        $sv->get('identifier'),
                        $agreementIdentifier
                    ),
                ];
            }
        }
    }

    /**
     * Vérifi les producteurs autorisés par l'accord de versement
     * @param EntityInterface $agreement
     * @param DOMElement      $archiveNode
     * @return void
     */
    private function validateAgreementOriginatingAgencies(
        EntityInterface $agreement,
        DOMElement $archiveNode
    ) {
        $agreementIdentifier = $agreement->get('identifier');
        if (!$agreement->get('allow_all_originating_agencies')) {
            if (in_array($this->util()->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
                $xpathQuery = 'ns:Content/ns:OriginatingAgency/ns:Identifier';
            } else {
                $xpathQuery = 'ns:ContentDescription/ns:OriginatingAgency/ns:Identification';
            }
            /** @var DOMElement $item */
            foreach ($this->util()->xpath->query($xpathQuery, $archiveNode) as $item) {
                $spIdentifier = $item->nodeValue;
                $inList = false;
                /** @var EntityInterface $ta */
                foreach ((array)$agreement->get('originating_agencies') as $sp) {
                    if ($sp->get('identifier') === $spIdentifier) {
                        $inList = true;
                        break;
                    }
                }
                if (!$inList) {
                    $this->errors[] = [
                        'transfer_id' => $this->Transfer->get('id'),
                        'level' => 'error',
                        'code' => TransferError::CODE_TRANSFERRING_AGENCY_NOT_ALLOWED,
                        'message' => __(
                            "Service producteur du transfert {0} non autorisé
                                 pour l'accord de versement : {1}",
                            $spIdentifier,
                            $agreementIdentifier
                        ),
                    ];
                }
            }
        }
    }

    /**
     * Vérifi les profils d'archives autorisés par l'accord de versement
     * @param EntityInterface $agreement
     * @param DOMElement      $archiveNode
     * @return void
     */
    private function validateAgreementProfiles(
        EntityInterface $agreement,
        DOMElement $archiveNode
    ) {
        $agreementIdentifier = $agreement->get('identifier');
        if (!$agreement->get('allow_all_profiles')) {
            $xpathQuery = 'ns:ArchivalProfile';
            /** @var DOMElement $item */
            foreach ($this->util()->xpath->query($xpathQuery, $archiveNode) as $item) {
                $profileIdentifier = $item->nodeValue;
                $inList = false;
                /** @var EntityInterface $ta */
                foreach ((array)$agreement->get('profiles') as $profile) {
                    if ($profile->get('identifier') === $profileIdentifier) {
                        $inList = true;
                        break;
                    }
                }
                if (!$inList) {
                    $this->errors[] = [
                        'transfer_id' => $this->Transfer->get('id'),
                        'level' => 'error',
                        'code' => TransferError::CODE_TRANSFERRING_AGENCY_NOT_ALLOWED,
                        'message' => __(
                            "Profil d'archive du transfert {0} non autorisé
                                 pour l'accord de versement : {1}",
                            $profileIdentifier,
                            $agreementIdentifier
                        ),
                    ];
                }
            }
        }
    }

    /**
     * Vérifi les niveaux de service autorisés par l'accord de versement
     * @param EntityInterface $agreement
     * @param DOMElement      $archiveNode
     * @return void
     */
    private function validateAgreementServiceLevels(
        EntityInterface $agreement,
        DOMElement $archiveNode
    ) {
        $agreementIdentifier = $agreement->get('identifier');
        if (!$agreement->get('allow_all_service_levels')) {
            $xpathQuery = 'ns:ServiceLevel';
            /** @var DOMElement $item */
            foreach ($this->util()->xpath->query($xpathQuery, $archiveNode) as $item) {
                $serviceLevelIdentifier = $item->nodeValue;
                $inList = false;
                /** @var EntityInterface $ta */
                foreach ((array)$agreement->get('service_levels') as $serviceLevel) {
                    if ($serviceLevel->get('identifier') === $serviceLevelIdentifier) {
                        $inList = true;
                        break;
                    }
                }
                if (!$inList) {
                    $this->errors[] = [
                        'transfer_id' => $this->Transfer->get('id'),
                        'level' => 'error',
                        'code' => TransferError::CODE_TRANSFERRING_AGENCY_NOT_ALLOWED,
                        'message' => __(
                            "Niveau de service du transfert {0} non autorisé 
                                pour l'accord de versement : {1}",
                            $serviceLevelIdentifier,
                            $agreementIdentifier
                        ),
                    ];
                }
            }
        }
    }

    /**
     * Vérifi que le format des fichiers est autorisé par l'accord de versement
     * @param EntityInterface $agreement
     * @param DOMElement      $archiveNode
     * @return void
     */
    private function validateAgreementDataFormat(
        EntityInterface $agreement,
        DOMElement $archiveNode
    ) {
        $agreementIdentifier = $agreement->get('identifier');
        $af = $agreement->get('allowed_formats') ?: '[]';
        $formats = Hash::filter(json_decode($af) ?: []);
        if ($formats) {
            /** @var Seda02Schema|Seda10Schema|Seda21Schema|Seda22Schema $schemaClassname */
            $schemaClassname = MessageSchema::getSchemaClassname($this->util()->namespace);
            $documentTagname = $schemaClassname::getTagname(
                'Document',
                'ArchiveTransfer'
            );
            $query = $this->util()->xpath
                ->query('//ns:' . $documentTagname, $archiveNode);
            /**
             * @var DOMElement $documentNode
             * @var DOMElement $filenameNode
             * @var DOMElement $uriNode
             * @var DOMElement $attachmentNode
             */
            foreach ($query as $documentNode) {
                $filename = null;
                $format = null;
                $mime = null;
                $attachmentNode = $this->util()->node('ns:Attachment', $documentNode);
                if ($attachmentNode) {
                    $filename = $attachmentNode->getAttribute('filename');
                    $format = $attachmentNode->getAttribute('format');
                    $mime = $attachmentNode->getAttribute('mimeCode');
                }
                if (in_array($this->util()->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
                    $filename = ArchiveBinariesTable::getBinaryFilename($this->util(), $documentNode);
                    $format = $this->util()
                        ->nodeValue('ns:FormatIdentification/ns:FormatId', $documentNode)
                        ?: $format;
                    $mime = $this->util()
                        ->nodeValue('ns:FormatIdentification/ns:MimeType', $documentNode)
                        ?: $mime;
                }
                $ext = strtolower(
                    pathinfo(
                        $filename,
                        PATHINFO_EXTENSION
                    )
                );
                if (!$format || !$mime) {
                    $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
                    $transferAttachment = $TransferAttachments->find()
                        ->where(
                            [
                                'transfer_id' => $this->Transfer->get('id'),
                                'filename' => $filename,
                            ]
                        )
                        ->first();
                    if (!$format && $transferAttachment && $transferAttachment->get('format')) {
                        $format = $transferAttachment->get('format');
                    }
                    if (!$mime && $transferAttachment && $transferAttachment->get('mime')) {
                        $mime = $transferAttachment->get('mime');
                    }
                }
                $allowed = false;
                foreach ($formats as $frmt) {
                    [$type, $value] = explode('-', $frmt, 2);
                    if (
                        ($type === 'pronom' && $format === $value)
                        || ($type === 'ext' && $ext === $value)
                        || ($type === 'mime' && (strpos($value, $mime) !== false))
                    ) {
                        $allowed = true;
                        break;
                    }
                }
                if (!$allowed) {
                    $this->errors[] = [
                        'transfer_id' => $this->Transfer->get('id'),
                        'level' => 'error',
                        'code' => TransferError::CODE_NON_COMPLIANT_FORMAT,
                        'message' => __(
                            "Format ''{0}'', mime ''{1}'', extension ''{2}'' du fichier {3} non autorisé
                                 pour l'accord de versement : {4}",
                            $format ?: '<NC>',
                            $mime ?: '<NC>',
                            $ext,
                            $filename,
                            $agreementIdentifier
                        ),
                    ];
                }
            }
        }
    }

    /**
     * Validation des AppraisalRules et AccessRules
     * @param array           $errors
     * @param EntityInterface $sa     Service d'archives
     * @return void
     * @throws Exception
     */
    private function validateRules(array &$errors, EntityInterface $sa): void
    {
        $pre = $this->PreControlMessages;
        $namespace = $pre->getNamespace();
        $util = new DOMUtility($pre->getDom());
        $this->checkAppraisalCoherence($errors, $util, $sa);

        if (!in_array($namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            return;
        }

        $appraisalRules = $util->xpath->query('//ns:AppraisalRule/ns:Rule');

        $AppraisalRuleCodes = TableRegistry::getTableLocator()->get('AppraisalRuleCodes');
        foreach ($appraisalRules ?? [] as $appraisalRule) {
            $appraisalRuleCode = $AppraisalRuleCodes->find()
                ->where(
                    [
                        'AppraisalRuleCodes.code' => $appraisalRule->nodeValue,
                        'OR' => [
                            'AppraisalRuleCodes.org_entity_id IS' => null,
                            'AppraisalRuleCodes.org_entity_id' => $sa->get('id'),
                        ],
                    ]
                )
                ->first();
            if (!$appraisalRuleCode) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_RULE,
                    'message' => __(
                        "Valeur de la règle non autorisée : ''{0}'' ({1})",
                        $appraisalRule->nodeValue,
                        $this->getNodePath($appraisalRule)
                    ),
                ];
            }
        }

        $accessRules = $util->xpath->query('//ns:AccessRule/ns:Rule');

        $AccessRuleCodes = TableRegistry::getTableLocator()->get('AccessRuleCodes');
        foreach ($accessRules ?? [] as $accessRule) {
            $accessRuleCode = $AccessRuleCodes->find()
                ->where(
                    [
                        'AccessRuleCodes.code' => $accessRule->nodeValue,
                        'OR' => [
                            'AccessRuleCodes.org_entity_id IS' => null,
                            'AccessRuleCodes.org_entity_id' => $sa->get('id'),
                        ],
                    ]
                )
                ->first();
            if (!$accessRuleCode) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_RULE,
                    'message' => __(
                        "Valeur de la règle non autorisée : ''{0}'' ({1})",
                        $accessRule->nodeValue,
                        $this->getNodePath($accessRule)
                    ),
                ];
            }
        }
    }

    /**
     * Vérifi qu'il n'y a pes d'incohérence ex: AU à conserver dans une AU à éliminer
     * @param array           $errors
     * @param DOMUtility      $util
     * @param EntityInterface $sa
     * @return void
     * @throws Exception
     */
    private function checkAppraisalCoherence(array &$errors, DOMUtility $util, EntityInterface $sa): void
    {
        switch ($util->namespace) {
            case NAMESPACE_SEDA_02:
                $seda2x = false;
                $appraisalNodename = 'Appraisal';
                $archiveUnitAppraisalPath = 'Contains';
                $actionNodename = 'Code';
                $destroyAction = 'detruire';
                $keepAction = 'conserver';
                break;
            case NAMESPACE_SEDA_10:
                $seda2x = false;
                $appraisalNodename = 'AppraisalRule';
                $archiveUnitAppraisalPath = 'ArchiveObject';
                $actionNodename = 'Code';
                $destroyAction = 'detruire';
                $keepAction = 'conserver';
                break;
            default:
                $seda2x = true;
                $appraisalNodename = 'AppraisalRule';
                $archiveUnitAppraisalPath = 'ArchiveUnit/ns:Management';
                $actionNodename = 'FinalAction';
                $destroyAction = 'Destroy';
                $keepAction = 'Keep';
        }
        $appraisalRulesDestroyQuery = sprintf(
            '//ns:%s/ns:%s[text()="%s"]/..',
            $appraisalNodename,
            $actionNodename,
            $destroyAction
        );
        $subnodesQueryDestroy = sprintf(
            './/ns:%s/ns:%s/ns:%s[text()="%s"]/..',
            $archiveUnitAppraisalPath,
            $appraisalNodename,
            $actionNodename,
            $destroyAction
        );
        $subnodesQueryKeep = sprintf(
            './/ns:%s/ns:%s/ns:%s[text()="%s"]/..',
            $archiveUnitAppraisalPath,
            $appraisalNodename,
            $actionNodename,
            $keepAction
        );

        /** @var DOMElement $appraisalRule */
        foreach ($util->xpath->query($appraisalRulesDestroyQuery) as $appraisalRule) {
            $startDate = new DateTime($util->nodeValue('ns:StartDate', $appraisalRule));

            if ($seda2x) {
                $duration = $this->getAppraisalDurationByRule($util->nodeValue('ns:Rule', $appraisalRule), $sa);
                $archiveUnit = $appraisalRule->parentNode->parentNode;
            } else {
                $duration = new DateInterval($util->nodeValue('ns:Duration', $appraisalRule));
                $archiveUnit = $appraisalRule->parentNode;
            }
            if (!$duration) {
                continue;
            }
            $destroyDate = $startDate->add($duration);
            $hasKeepSubAppraisal = $util->xpath->query($subnodesQueryKeep, $archiveUnit)->count() > 0;
            if ($hasKeepSubAppraisal) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_RULE,
                    'message' => __(
                        "Unité d'archives à conserver à l'intérieur d'une unité d'archives à détruire: ({0})",
                        $this->getNodePath($appraisalRule)
                    ),
                ];
            }
            foreach ($util->xpath->query($subnodesQueryDestroy) as $subnode) {
                $subStartDate = new DateTime($util->nodeValue('ns:StartDate', $subnode));
                if ($seda2x) {
                    $subDuration = $this->getAppraisalDurationByRule($util->nodeValue('ns:Rule', $subnode), $sa);
                } else {
                    $subDuration = new DateInterval($util->nodeValue('ns:Duration', $subnode));
                }
                if (!$subDuration) {
                    continue;
                }
                $subDestroyDate = $subStartDate->add($subDuration);
                if ($subDestroyDate > $destroyDate) {
                    $errors[] = [
                        'transfer_id' => $this->Transfer->get('id'),
                        'level' => 'error',
                        'code' => TransferError::CODE_CUSTOM_RULE,
                        'message' => __(
                            "Unité d'archives à détruire à l'intérieur d'une" .
                            " unité d'archives à détruire avec une date antérieure: ({0})",
                            $this->getNodePath($appraisalRule)
                        ),
                    ];
                }
            }
        }
    }

    /**
     * AppraisalRule to DateInterval
     * @param string          $rule
     * @param EntityInterface $sa
     * @return DateInterval|null
     * @throws Exception
     */
    private function getAppraisalDurationByRule(string $rule, EntityInterface $sa): ?DateInterval
    {
        $AppraisalRuleCodes = TableRegistry::getTableLocator()->get('AppraisalRuleCodes');
        $appraisalRuleCode = $AppraisalRuleCodes->find()
            ->where(
                [
                    'AppraisalRuleCodes.code' => $rule,
                    'OR' => [
                        'AppraisalRuleCodes.org_entity_id IS' => null,
                        'AppraisalRuleCodes.org_entity_id' => $sa->get('id'),
                    ],
                ]
            )
            ->first();
        if (!$appraisalRuleCode) {
            return null;
        }
        return new DateInterval($appraisalRuleCode->get('duration'));
    }

    /**
     * Ajoute des erreurs pour les BinaryDataObject sans liens avec ArchiveUnit
     * @param array $errors
     */
    private function validateBinaries(array &$errors)
    {
        $pre = $this->PreControlMessages;
        $namespace = $this->util->namespace;
        if (!in_array($namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            return;
        }

        $util = new DOMUtility($pre->getDom());
        /** @var DOMElement $element */
        foreach ($util->xpath->query('//ns:BinaryDataObject') as $binary) {
            $binaryIdentifier = $binary->getAttribute('id');
            $qContent = $util->xpathQuote($binaryIdentifier);
            $linked = $util->xpath->query(
                sprintf('.//ns:DataObjectReferenceId[contains(text(), %s)]', $qContent)
            )->count();
            if ($binary->parentNode->nodeName === 'DataObjectGroup') {
                $qContent = $util->xpathQuote(
                    $binary->parentNode->getAttribute('id')
                );
                $linked += $util->xpath->query(
                    sprintf('.//ns:DataObjectGroupReferenceId[contains(text(), %s)]', $qContent)
                )->count();
            }
            if ($DataObjectGroupId = $util->node('./ns:DataObjectGroupId', $binary)) {
                $qContent = $util->xpathQuote(
                    trim($DataObjectGroupId->nodeValue)
                );
                $linked += $util->xpath->query(
                    sprintf('.//ns:DataObjectGroupReferenceId[contains(text(), %s)]', $qContent)
                )->count();
            }
            if ($linked === 0) {
                $filename = ArchiveBinariesTable::getBinaryFilename($this->util(), $binary);
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_BINARY_DATA,
                    'message' => __(
                        "BinaryDataObject du fichier ''{0}'' non référencé dans les ArchiveUnits",
                        $filename
                    ),
                ];
            }
        }
    }

    /**
     * Ajoute des erreurs pour les DataObjectReferences pointants dans le vide
     * @param array $errors
     */
    private function validateDataObjectReferences(array &$errors)
    {
        $pre = $this->PreControlMessages;
        $namespace = $pre->getNamespace();
        if (!in_array($namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            return;
        }

        $util = new DOMUtility($pre->getDom());
        /** @var DOMElement $element */
        foreach ($util->xpath->query('//ns:DataObjectReference/ns:DataObjectReferenceId') as $element) {
            $binaryIdentifier = trim($element->nodeValue);
            $qContent = $util->xpathQuote($binaryIdentifier);
            $linked = $util->xpath->query(
                sprintf('.//ns:BinaryDataObject[@id=%s]', $qContent)
            )->count();
            if ($linked === 0) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_ATTACHMENT_MISSING,
                    'message' => __(
                        "Référence à un objet binaire non présent dans le transfert "
                        . "DataObjectReference.DataObjectReferenceId = {0}",
                        $binaryIdentifier
                    ),
                ];
            }
        }
        /** @var DOMElement $element */
        foreach ($util->xpath->query('//ns:DataObjectReference/ns:DataObjectGroupReferenceId') as $element) {
            $binaryIdentifier = trim($element->nodeValue);
            $qContent = $util->xpathQuote($binaryIdentifier);
            $linked = $util->xpath->query(
                sprintf('.//ns:DataObjectGroup[@id=%s]', $qContent)
            )->count();
            if ($linked === 0) {
                $errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_ATTACHMENT_MISSING,
                    'message' => __(
                        "Référence à un groupe d'objets binaires non présent dans le transfert "
                        . "DataObjectReference.DataObjectGroupReferenceId = {0}",
                        $binaryIdentifier
                    ),
                ];
            }
        }
    }

    /**
     * Ajoute une erreur s'il n'y a pas d'archive_unit
     * @param array $errors
     */
    private function validateArchiveUnit(array &$errors)
    {
        $pre = $this->PreControlMessages;
        $namespace = $pre->getNamespace();
        $util = new DOMUtility($pre->getDom());
        switch ($namespace) {
            case NAMESPACE_SEDA_02:
                $emptyContainsName = $util->node('//ns:Contains/ns:Name[not(normalize-space())]');
                if ($emptyContainsName) {
                    $errors[] = [
                        'transfer_id' => $this->Transfer->get('id'),
                        'level' => 'error',
                        'code' => TransferError::CODE_CUSTOM_NAME,
                        'message' => __(
                            "Nom vide pour l'unité d'archives {0}",
                            $util->getElementXpath($emptyContainsName, '')
                        ),
                    ];
                }
                break;
            case NAMESPACE_SEDA_10:
                $emptyArchiveName = $util->node('//ns:Archive/ns:Name[not(normalize-space())]');
                $emptyObjectName = $util->node('//ns:ArchiveObject/ns:Name[not(normalize-space())]');
                if ($emptyArchiveName || $emptyObjectName) {
                    $errors[] = [
                        'transfer_id' => $this->Transfer->get('id'),
                        'level' => 'error',
                        'code' => TransferError::CODE_CUSTOM_NAME,
                        'message' => __(
                            "Nom vide pour l'unité d'archives {0}",
                            $emptyArchiveName
                                ? $util->getElementXpath($emptyArchiveName, '')
                                : $util->getElementXpath($emptyObjectName, '')
                        ),
                    ];
                }
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $au = $util->node('ns:DataObjectPackage/ns:DescriptiveMetadata/ns:ArchiveUnit');
                if (!$au) {
                    $errors[] = [
                        'transfer_id' => $this->Transfer->get('id'),
                        'level' => 'error',
                        'code' => TransferError::CODE_ARCHIVE_MISSING,
                        'message' => __(
                            "Aucune unité d'archives présente dans le bordereau"
                        ),
                    ];
                } else {
                    $emptyArchiveUnitName = $util->node('//ns:ArchiveUnit/ns:Content/ns:Title[not(normalize-space())]');
                    $missingArchiveUnitName = $util->node('//ns:ArchiveUnit/ns:Content[not(ns:Title)]');
                    if ($emptyArchiveUnitName || $missingArchiveUnitName) {
                        $errors[] = [
                            'transfer_id' => $this->Transfer->get('id'),
                            'level' => 'error',
                            'code' => TransferError::CODE_CUSTOM_NAME,
                            'message' => __(
                                "Nom vide pour l'unité d'archives {0}",
                                $emptyArchiveUnitName
                                    ? $util->getElementXpath($emptyArchiveUnitName, '')
                                    : $util->getElementXpath($missingArchiveUnitName, '')
                            ),
                        ];
                    }
                }
                break;
        }
    }

    /**
     * Erreurs liés aux archives composites
     * @param array           $errors
     * @param EntityInterface $sa
     */
    private function validateComposite(array &$errors, EntityInterface $sa)
    {
        $pre = $this->PreControlMessages;
        $namespace = $pre->getNamespace();
        if (in_array($namespace, [NAMESPACE_SEDA_02, NAMESPACE_SEDA_10])) {
            return;
        }
        $this->util = new DOMUtility($pre->getDom());
        if ($this->util->xpath->query('//ns:RepositoryArchiveUnitPID')->count() === 0) {
            return;
        }
        $this->errors =& $errors;

        $this->checkFirstLevelComposite();
        $archive = $this->checkCompositeTargetArchive($sa);
        $this->checkCompositeBinaries($archive);
    }

    /**
     * Seul les ArchiveUnits de 1er niveau peuvent être utilisés dans les archives
     * composites, et toutes doivent avoir une référence
     * @return void
     */
    private function checkFirstLevelComposite()
    {
        $path = '//ns:ArchiveUnit/ns:ArchiveUnit/ns:Content/'
            . 'ns:RelatedObjectReference/ns:IsPartOf/ns:RepositoryArchiveUnitPID';
        $query = $this->util->xpath->query($path);
        if ($query->count()) {
            $this->errors[] = [
                'transfer_id' => $this->Transfer->get('id'),
                'level' => 'error',
                'code' => TransferError::CODE_CUSTOM_COMPOSITE,
                'message' => __(
                    "Les métadonnées {0} doivent être présentes "
                    . "dans toutes les unités d'archives de premier niveau",
                    'RelatedObjectReference.IsPartOf.RepositoryArchiveUnitPID'
                ),
            ];
        }
        $query = $this->util->xpath->query(
            'ns:DataObjectPackage/ns:DescriptiveMetadata/ns:ArchiveUnit'
        );
        /** @var DOMElement $archiveUnitNode */
        foreach ($query as $archiveUnitNode) {
            $path = 'ns:Content/ns:RelatedObjectReference/ns:IsPartOf/ns:RepositoryArchiveUnitPID';
            $q = $this->util->xpath->query($path, $archiveUnitNode);
            $count = $q->count();
            if ($count === 0) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_COMPOSITE,
                    'message' => __(
                        "les métadonnées {0} doivent être présentes dans "
                        . "toutes les unités d'archives de premier niveau",
                        'RelatedObjectReference.IsPartOf.RepositoryArchiveUnitPID'
                    ),
                ];
                break;
            } elseif ($count > 1) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_COMPOSITE,
                    'message' => __(
                        "la métadonnée {0} ne doit être présente qu'une "
                        . "seule fois par unité d'archives",
                        'RelatedObjectReference.IsPartOf.RepositoryArchiveUnitPID'
                    ),
                ];
                break;
            }
        }
    }

    /**
     * Vérifi la conformité du transfert vis à vis de l'entrée cible
     * @param EntityInterface $sa
     * @return EntityInterface|null
     */
    private function checkCompositeTargetArchive(EntityInterface $sa)
    {
        $query = $this->util->xpath->query(
            'ns:DataObjectPackage/ns:DescriptiveMetadata/ns:ArchiveUnit'
        );
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $archive = null;
        $version = null;
        /** @var DOMElement $archiveUnitNode */
        foreach ($query as $archiveUnitNode) {
            $path = 'ns:Content/ns:RelatedObjectReference/ns:IsPartOf/ns:RepositoryArchiveUnitPID';
            $identifier = trim($this->util->nodeValue($path, $archiveUnitNode));
            $archiveUnits = $ArchiveUnits->find()
                ->innerJoinWith('Archives')
                ->where($this->getConditionsForComposite($identifier))
                ->andWhere(['Archives.archival_agency_id' => $sa->id]);
            $count = $archiveUnits->count();
            if ($count === 0) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_COMPOSITE,
                    'message' => __(
                        "L'unité d'archives de rattachement {0} n'a pas été trouvée",
                        $identifier
                    ),
                ];
                continue;
            }
            if ($count > 1) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_COMPOSITE,
                    'message' => __(
                        "La référence {0} est ambiguë, plusieurs unités d'archives correspondent",
                        $identifier
                    ),
                ];
                continue;
            }
            $archiveUnit = $archiveUnits->firstOrFail();
            if ($archive && $archiveUnit->get('archive_id') !== $archive->id) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_COMPOSITE,
                    'message' => __(
                        "Les unités d'archives de rattachement ne sont "
                        . "pas toutes liées à la même entrée"
                    ),
                ];
            } elseif (empty($archive)) {
                $archive = $loc->get('Archives')->find()
                    ->where(['Archives.id' => $archiveUnit->get('archive_id')])
                    ->contain(
                        [
                            'TransferringAgencies',
                            'OriginatingAgencies',
                            'Transfers',
                        ]
                    )
                    ->firstOrFail();
                $version = Hash::get($archive, 'transfers.0.message_version');
                $availableOrUpdatingState = [
                    ArchivesTable::S_AVAILABLE,
                    ArchivesTable::S_UPDATE_CREATING,
                    ArchivesTable::S_UPDATE_DESCRIBING,
                    ArchivesTable::S_UPDATE_MANAGING,
                    ArchivesTable::S_UPDATE_STORING,
                ];
                if (!in_array($archive->get('state'), $availableOrUpdatingState)) {
                    $this->errors[] = [
                        'transfer_id' => $this->Transfer->get('id'),
                        'level' => 'error',
                        'code' => TransferError::CODE_CUSTOM_COMPOSITE,
                        'message' => __(
                            "L'état de l'archive {0} ({1}) ne permet pas le rattachement du transfert",
                            $archive->get('archival_agency_identifier'),
                            $archive->get('state')
                        ),
                    ];
                }
            }
            if ($archiveUnit->get('state') !== ArchiveUnitsTable::S_AVAILABLE) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_COMPOSITE,
                    'message' => __(
                        "L'état de l'archive {0} ({1}) ne permet pas le rattachement du transfert",
                        $archive->get('archival_agency_identifier'),
                        $archive->get('state')
                    ),
                ];
            }
            $path = 'ns:Content/ns:OriginatingAgency/ns:Identifier';
            $originatingIdentifier = trim($this->util->nodeValue($path, $archiveUnitNode));
            if (
                $originatingIdentifier
                && Hash::get($archive, 'originating_agency.identifier') !== $originatingIdentifier
            ) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_COMPOSITE,
                    'message' => __(
                        "Le service producteur du transfert {0} n'est " .
                        "pas le même que celui de l'archive de rattachement {1}",
                        $originatingIdentifier,
                        Hash::get($archive, 'originating_agency.identifier')
                    ),
                ];
            }
        }
        if ($archive) {
            $transferringAgency = Hash::get($archive, 'transferring_agency.identifier');
            $identifier = $this->util->nodeValue('ns:TransferringAgency/ns:Identifier');
            if ($version !== $this->Transfer->get('message_version')) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_COMPOSITE,
                    'message' => __(
                        "La version du SEDA du transfert {0} est différente"
                        . " de la version du SEDA de l'archive "
                        . "de rattachement {1}",
                        $this->Transfer->get('message_version'),
                        $version
                    ),
                ];
            }
            if ($transferringAgency !== $identifier) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_COMPOSITE,
                    'message' => __(
                        "Le service versant du transfert {0} n'est pas le " .
                        "même que celui de l'archive de rattachement {1}",
                        $identifier,
                        $transferringAgency
                    ),
                ];
            }
        }
        return $archive;
    }

    /**
     * Parse identifier pour en faire une condition
     * @param string $identifier
     * @return array
     */
    private function getConditionsForComposite(string $identifier): array
    {
        if (strpos($identifier, 'ArchivalAgencyIdentifier:') !== false) {
            $conditions = [
                'ArchiveUnits.archival_agency_identifier' => substr(
                    $identifier,
                    strlen('ArchivalAgencyIdentifier:')
                ),
            ];
        } elseif (strpos($identifier, 'TransferringAgencyIdentifier:') !== false) {
            $conditions = [
                'ArchiveUnits.transferring_agency_identifier' => substr(
                    $identifier,
                    strlen('TransferringAgencyIdentifier:')
                ),
            ];
        } elseif (strpos($identifier, 'OriginatingAgencyIdentifier:') !== false) {
            $conditions = [
                'ArchiveUnits.originating_agency_identifier' => substr(
                    $identifier,
                    strlen('OriginatingAgencyIdentifier:')
                ),
            ];
        } else {
            $conditions = [
                'OR' => [
                    'ArchiveUnits.archival_agency_identifier' => $identifier,
                    'ArchiveUnits.transferring_agency_identifier' => $identifier,
                    'ArchiveUnits.originating_agency_identifier' => $identifier,
                ],
            ];
        }
        return $conditions;
    }

    /**
     * Vérifie les BinaryDataObject pour une archive composite
     * @param EntityInterface|null $archive
     * @return void
     */
    private function checkCompositeBinaries(EntityInterface $archive = null)
    {
        if (!$archive) {
            return;
        }
        $query = $this->util->xpath->query('//ns:BinaryDataObject');
        $loc = TableRegistry::getTableLocator();
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        /** @var DOMElement $archiveUnitNode */
        foreach ($query as $archiveUnitNode) {
            $filename = ArchiveBinariesTable::getBinaryFilename($this->util(), $archiveUnitNode);
            $exists = (bool)count(
                $ArchiveBinaries->find()
                    ->select(['existing' => 1])
                    ->innerJoinWith('ArchiveUnits')
                    ->where(
                        [
                            'ArchiveUnits.archive_id' => $archive->id,
                            'ArchiveBinaries.filename' => $filename,
                        ]
                    )
                    ->limit(1)
                    ->disableHydration()
                    ->toArray()
            );
            if ($exists) {
                $this->errors[] = [
                    'transfer_id' => $this->Transfer->get('id'),
                    'level' => 'error',
                    'code' => TransferError::CODE_CUSTOM_COMPOSITE,
                    'message' => __(
                        "Le fichier {0} est déjà présent parmi les fichiers originaux de l'archive de rattachement",
                        $filename
                    ),
                ];
            }
        }
    }
}
