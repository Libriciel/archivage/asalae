<?php

/**
 * Asalae\Command\SessionCommand
 */

namespace Asalae\Command;

use Asalae\Model\Entity\Session;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\Exception\RecordNotFoundException;
use Exception;

/**
 * Update asynchrone des entités d'un utilisateur (stockés en session)
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SessionOrgEntitiesCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'session org_entities';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(__("Liste les sessions utilisateurs"));
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $Sessions = $this->fetchTable('Sessions');
        /** @var Session $session */
        foreach ($Sessions->find() as $session) {
            try {
                $session->updateOrgEntities();
                $Sessions->save($session);
            } catch (RecordNotFoundException) {
                $Sessions->delete($session);
            }
        }
        $this->io->success('done');
    }
}
