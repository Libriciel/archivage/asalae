<?php

/**
 * Asalae\Command\Lifecycle\RepairCommand
 */

namespace Asalae\Command\Lifecycle;

use Asalae\Exception\GenericException;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchiveKeywordsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Exception\StopException;
use Cake\Core\Configure;
use DateTime;
use Exception;

/**
 * Upload des fichiers du cycle de vie situé dans le dossier configuré
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveBinariesTable ArchiveBinaries
 * @property ArchiveFilesTable    ArchiveFiles
 * @property ArchiveKeywordsTable ArchiveKeywords
 * @property EventLogsTable       EventLogs
 * @property StoredFilesTable     StoredFiles
 */
class UploadCommand extends Command
{
    /**
     * @var string|null
     */
    private $lock;
    /**
     * @var VolumeManager[]
     */
    private $volumeManager;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'lifecycle upload';
    }

    /**
     * Supprime le fichier lock
     */
    public function __destruct()
    {
        if ($this->lock && is_file($this->lock)) {
            unlink($this->lock);
        }
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();
        $parser->setDescription(
            __("Upload des fichiers du cycle de vie situé dans le dossier configuré")
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $basedir = UploadCommand::getBasedir();
        $this->lock($basedir, $io);

        $uploadInstructionsDir = $basedir . DS . 'to_upload';
        $uploads = glob($uploadInstructionsDir . DS . '*.serialize');
        $io->out(__("Upload de {0} fichiers du cycle de vie", count($uploads)));
        $EventLogs = $this->fetchTable('EventLogs');
        foreach ($uploads as $upload) {
            $data = unserialize(file_get_contents($upload));
            if (!$data) {
                throw new GenericException(
                    __(
                        "Erreur lors du unserialize du fichier {0}",
                        $upload
                    )
                );
            }
            $io->out('file ' . $data['stored_file_id']);
            try {
                $manager = $this->getVolumeManager($data['secure_data_space_id']);
                $manager->set($data['stored_file_id'], base64_decode($data['b64']));
                unlink($upload);
                if (!empty($data['eventlog_ids'])) {
                    $EventLogs->updateAll(
                        ['in_lifecycle' => true],
                        ['id IN' => $data['eventlog_ids']]
                    );
                }
            } catch (VolumeException $e) {
                $io->error(__("Echec lors de l'envoi du cycle de vie {0}", basename($upload)));
            }
        }
        if ($this->lock && is_file($this->lock)) {
            unlink($this->lock);
        }
        if (isset($e)) {
            throw $e;
        }
        $io->success('done');
    }

    /**
     * Gestion du dossier de travail
     * @return string
     */
    private static function getBasedir(): string
    {
        $basedir = Configure::read(
            'Archives.updateLifecycle.path',
            rtrim(
                Configure::read('App.paths.data'),
                DS
            ) . DS . 'update_lifecycle'
        );
        if (strpos($basedir, '..') !== false) {
            throw new GenericException('forbidden path');
        }
        if (!is_dir($basedir)) {
            mkdir($basedir, 0770, true);
        }
        return $basedir;
    }

    /**
     * gestion du lock fichier (supprimé lors du __destruct())
     * @param string    $basedir
     * @param ConsoleIo $io
     * @throws StopException si un lock existe déjà
     */
    private function lock(string $basedir, ConsoleIo $io)
    {
        $uploadLock = $basedir . DS . '.lock';
        if (is_file($uploadLock)) {
            $pid = (int)file_get_contents($uploadLock);
            $ps = exec(sprintf('ps -p %d', $pid));
            $running = preg_match('/^\s*(\d+)/', $ps, $m)
                && (int)$m[1] === $pid;
            if ($running) {
                $io->abort(__("Un upload semble être déjà en cours (PID={0})", $pid));
            }
        }
        file_put_contents($uploadLock, getmypid());
        $this->lock = $uploadLock;
    }

    /**
     * Donne le volume manager selon le secure_data_space_id
     * @param int $secure_data_space_id
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(int $secure_data_space_id): VolumeManager
    {
        if (!isset($this->volumeManager[$secure_data_space_id])) {
            $this->volumeManager[$secure_data_space_id] = new VolumeManager($secure_data_space_id);
        }
        return $this->volumeManager[$secure_data_space_id];
    }

    /**
     * Ecrit le fichier temporaire d'instructions de mise à jour du lifecycle
     * @param string $content
     * @param int    $stored_file_id
     * @param int    $secure_data_space_id
     * @param array  $eventLogIds
     * @return false|int
     */
    public static function prepareUpload(
        string $content,
        int $stored_file_id,
        int $secure_data_space_id,
        array $eventLogIds = []
    ) {
        $basedir = UploadCommand::getBasedir();
        $uploadInstructionsDir = $basedir . DS . 'to_upload';
        $datetime = (new DateTime())->format('Ymdhisu');
        $filename = $uploadInstructionsDir . DS . $datetime . '.serialize';
        if (!is_dir(dirname($filename))) {
            mkdir(dirname($filename), 0770, true);
        }
        return file_put_contents(
            $filename,
            serialize(
                [
                    'b64' => base64_encode($content),
                    'stored_file_id' => $stored_file_id,
                    'secure_data_space_id' => $secure_data_space_id,
                    'eventlog_ids' => $eventLogIds,
                ]
            )
        );
    }
}
