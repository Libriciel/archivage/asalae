<?php

/**
 * Asalae\Command\Lifecycle\RepairCommand
 */

namespace Asalae\Command\Lifecycle;

use Asalae\Model\Entity\Archive;
use Asalae\Model\Entity\EventLog;
use Asalae\Model\Entity\SecureDataSpace;
use Asalae\Model\Entity\Transfer;
use Asalae\Model\Entity\User;
use Asalae\Model\Entity\Volume;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchiveKeywordsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Utility\Premis;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateTime;
use DOMDocument;
use Exception;
use Throwable;

/**
 * Commande de réparation d'un cycle de vie
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveBinariesTable ArchiveBinaries
 * @property ArchiveFilesTable    ArchiveFiles
 * @property ArchiveKeywordsTable ArchiveKeywords
 * @property EventLogsTable       EventLogs
 * @property StoredFilesTable     StoredFiles
 */
class RepairCommand extends Command
{
    /**
     * @var Archive
     */
    private $Archive;

    /**
     * @var int
     */
    private $lastEventId = 0;

    /**
     * @var ConsoleIo
     */
    private $io;
    private Premis\Agent $agent;
    private mixed $identifier;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'lifecycle repair';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();
        $parser->addArgument(
            'filename',
            [
                'help' => __(
                    "Fichier du fichier du cycle de vie à réparer (stored_file.name)"
                ),
                'required' => true,
            ]
        );
        $parser->addOption(
            'dryrun',
            [
                'help' => __(
                    "Affiche le contenu du fichier du cycle de vie réparé sans le remplacer"
                ),
                'boolean' => true,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $storedFile = $this->fetchTable('StoredFiles')->find()
            ->where(['StoredFiles.name' => $args->getArgument('filename')])
            ->contain(
                [
                    'ArchiveFiles' => [
                        'Archives' => [
                            'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                            'SecureDataSpaces' => ['Volumes'],
                            'Transfers' => ['Users'],
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        $VolumeManager = new VolumeManager(
            $storedFile->get('secure_data_space_id')
        );
        $io->out(__("Affichage de l'ancien fichier en rouge (s'il existe):"));
        try {
            $io->error(
                $VolumeManager->fileGetContent($storedFile->get('name'))
            );
        } catch (Throwable) {
            $io->error(__("Fichier non trouvé"));
        }
        $this->Archive = Hash::get($storedFile, 'archive_files.0.archive');
        $dom = $this->generatePremis($io);
        $this->appendManagementFiles($dom);

        $subqueryArchiveBinaries = $this->fetchTable('ArchiveBinaries')->find()
            ->select(['ArchiveBinaries.id'])
            ->innerJoin(
                ['abau' => 'archive_binaries_archive_units'],
                [
                    'abau.archive_binary_id' => new IdentifierExpression(
                        'ArchiveBinaries.id'
                    ),
                ]
            )
            ->innerJoin(
                ['abau_au' => 'archive_units'],
                [
                    'abau_au.id' => new IdentifierExpression(
                        'abau.archive_unit_id'
                    ),
                ]
            )
            ->where(['abau_au.archive_id' => $this->Archive->id]);
        $subqueryArchiveKeywords = $this->fetchTable('ArchiveKeywords')->find()
            ->select(['ArchiveKeywords.id'])
            ->innerJoin(
                ['ak_au' => 'archive_units'],
                [
                    'ak_au.id' => new IdentifierExpression(
                        'ArchiveKeywords.archive_unit_id'
                    ),
                ]
            )
            ->where(['ak_au.archive_id' => $this->Archive->id]);

        $query = $this->fetchTable('EventLogs')->find()
            ->leftJoinWith('ArchiveBinaries')
            ->leftJoinWith('ArchiveFiles')
            ->leftJoinWith('ArchiveKeywords')
            ->leftJoinWith('ArchiveUnits')
            ->leftJoinWith('Archives')
            ->where(
                [
                    'EventLogs.id >' => $this->lastEventId,
                    'EventLogs.object_model IN' => [
                        'ArchiveBinaries',
                        'ArchiveFiles',
                        'ArchiveKeywords',
                        'ArchiveUnits',
                        'Archives',
                    ],
                    'OR' => [
                        [
                            'ArchiveBinaries.id IS NOT' => null,
                            'ArchiveBinaries.id IN' => $subqueryArchiveBinaries,
                        ],
                        [
                            'ArchiveFiles.archive_id' => $this->Archive->id,
                        ],
                        [
                            'ArchiveKeywords.id IS NOT' => null,
                            'ArchiveKeywords.id IN' => $subqueryArchiveKeywords,
                        ],
                        [
                            'ArchiveUnits.archive_id' => $this->Archive->id,
                        ],
                        [
                            'Archives.id' => $this->Archive->id,
                        ],
                    ],
                ]
            )
            ->orderBy(['EventLogs.id']);
        /** @var EventLog $event */
        foreach ($query as $eventLog) {
            /** @var Premis\Event $event */
            $event = $eventLog->get('premis_event');
            Premis::appendEvent($dom, $event);
        }

        if ($args->getOption('dryrun')) {
            $io->out('============================');
            $io->out('========== DRYRUN ==========');
            $io->out('============================', 2);
            $io->out($dom->saveXML());
        } else {
            if ($VolumeManager->fileExists($storedFile->get('name'))) {
                $tmpFile = sys_get_temp_dir() . DS . uniqid(
                    'lifecycle-repair-'
                );
                $dom->save($tmpFile);
                try {
                    $VolumeManager->replace($storedFile->id, $tmpFile);
                } finally {
                    unlink($tmpFile);
                }
                $io->success(__("Fichier remplacé"));
            } else {
                $VolumeManager->filePutContent(
                    $storedFile->get('name'),
                    $dom->saveXML()
                );
                $io->success(__("Fichier uploadé"));
            }
        }
    }

    /**
     * Donne le fichier premis du cycle de vie
     * @param ConsoleIo $io
     * @return DOMDocument
     * @throws Exception
     */
    private function generatePremis(ConsoleIo $io): DOMDocument
    {
        $loc = TableRegistry::getTableLocator();
        /** @var Transfer $transferEntity */
        $transferEntity = $this->Archive->get('transfers')[0];
        $transfer_identifier = Hash::get(
            $this->Archive,
            'transfers.0.transfer_identifier'
        );
        /** @var DateTime $transfer_date */
        $transfer_date = Hash::get($this->Archive, 'transfers.0.created');
        $transfer_date = $transfer_date->format(DATE_RFC3339);
        $archive_date = $this->Archive->get('created')->format(DATE_RFC3339);
        $user_id = Hash::get($this->Archive, 'transfers.0.user_id');

        $premis = new Premis();
        $agent = $this->getUserAgent();

        /** @var Premis\IntellectualEntity $transfer */
        $transfer = $transferEntity->toPremisObject();

        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->find()
            ->where(
                [
                    'object_model' => 'Transfers',
                    'object_foreign_key' => $transferEntity->id,
                ]
            )
            ->orderBy(['id'])
            ->first();
        $this->lastEventId = max($this->lastEventId, $entry->id);
        $details = __(
            "Prise en compte du transfert ''{0}''",
            $transfer_identifier
        );
        if (!$entry) {
            $entry = $EventLogs->newEntry(
                'transfer_add',
                'success',
                $details,
                $user_id,
                $transferEntity
            );

            $data = $entry->toArray();
            unset($data['exported'], $data['outcome_detail']);
            $entry = $EventLogs->find()
                ->where($data)
                ->orderBy(['id'])
                ->first();
            if (!$entry) {
                $io->abort(
                    __(
                        "Anomalie critique ! L'événement {0} {1}:{2} "
                        . "devrait exister mais il n'a pas été trouvé en base",
                        'transfer_add',
                        'Transfers',
                        $transferEntity->id
                    )
                );
            }
            $this->lastEventId = max($this->lastEventId, $entry->id);
        }
        $this->lastEventId = max($this->lastEventId, $entry->id);
        $createTransferEvent = new Premis\Event(
            'transfer_add',
            'local',
            'EventLogs:' . $entry->id
        );
        $createTransferEvent->detail = __("création du transfert");
        $createTransferEvent->outcome = 'success';
        $createTransferEvent->outcomeDetail = $details;
        $createTransferEvent->dateTime = $transfer_date;
        $createTransferEvent->addAgent($agent);
        $createTransferEvent->addObject($transfer);
        $premis->add($createTransferEvent);

        /** @var Premis\IntellectualEntity $archive */
        $archive = $this->Archive->toPremisObject();

        $entry = $EventLogs->newEntry(
            'archive_add',
            'success',
            $details = __(
                "Création de l'archive ''{0}''",
                $this->Archive->get('archival_agency_identifier')
            ),
            $user_id,
            $this->Archive
        );
        $data = $entry->toArray();
        unset($data['exported'], $data['outcome_detail']);
        $entry = $EventLogs->find()
            ->where($data)
            ->orderBy(['id'])
            ->first();
        if (!$entry) {
            $io->abort(
                __(
                    "Anomalie critique ! L'événement {0} {1}:{2} devrait exister mais il n'a pas été trouvé en base",
                    'archive_add',
                    'Archives',
                    $this->Archive->id
                )
            );
        }
        $this->lastEventId = max($this->lastEventId, $entry->id);
        $createArchiveEvent = new Premis\Event(
            'archive_add',
            'local',
            'EventLogs:' . $entry->id
        );
        $createArchiveEvent->detail = __("création de l'archive");
        $createArchiveEvent->outcome = 'success';
        $createArchiveEvent->outcomeDetail = $details;
        $createArchiveEvent->dateTime = $archive_date;
        $createArchiveEvent->addAgent($agent);
        $createArchiveEvent->addObject($archive);

        /** @var SecureDataSpace $secureDataSpace */
        $secureDataSpace = $this->Archive->get('secure_data_space');
        $createArchiveEvent->addObject($secureDataSpace->toPremisObject());
        /** @var Volume $volume */
        foreach ($secureDataSpace->get('volumes') as $volume) {
            $createArchiveEvent->addObject($volume->toPremisObject());
        }
        $premis->add($createArchiveEvent);

        $Archives = $loc->get('Archives');
        $units = $Archives->find()
            ->select(
                [
                    'archive_unit_id' => 'ArchiveUnits.id',
                    'archival_agency_identifier' => 'ArchiveUnits.archival_agency_identifier',
                    'name' => 'ArchiveUnits.name',
                    'archive_binary_id' => 'ArchiveBinaries.id',
                    'filename' => 'ArchiveBinaries.filename',
                    'format' => 'Pronoms.name',
                    'puid' => 'Pronoms.puid',
                    'hash' => 'StoredFiles.hash',
                    'hash_algo' => 'StoredFiles.hash_algo',
                    'size' => 'StoredFiles.size',
                    'space_id' => 'SecureDataSpaces.id',
                    'space' => 'SecureDataSpaces.name',
                ]
            )
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('SecureDataSpaces')
            ->innerJoinWith('ArchiveUnits.ArchiveBinaries')
            ->leftJoinWith('ArchiveUnits.ArchiveBinaries.Pronoms')
            ->leftJoinWith('ArchiveUnits.ArchiveBinaries.StoredFiles')
            ->where(
                [
                    'Archives.id' => $this->Archive->get('id'),
                    'ArchiveBinaries.type' => 'original_data',
                ]
            )
            ->orderBy(['ArchiveUnits.lft' => 'asc']);
        $dom = $premis->generateDocument();
        $total = $units->count();
        $io->out(__("Cycle de vie: {0} unités à décrire.", $total));
        $c = 0;
        $objects = '';
        $events = '';

        /** @var EntityInterface $entity */
        foreach ($units as $entity) {
            $c++;
            if ($c % 1000 === 0) {
                $io->out(sprintf('%d / %d', $c, $total));
            }

            /**
             * Archive binary
             */
            $binary = new Premis\File(
                $entity->get('format') ?: 'unknown',
                'local',
                'ArchiveBinaries:' . $entity->get('archive_binary_id')
            );
            $binary->originalName = $entity->get('filename');
            $binary->messageDigest = $entity->get('hash');
            if ($entity->get('format')) {
                $binary->formatRegistryName = 'pronom';
                $binary->formatRegistryKey = $entity->get('puid');
            } else {
                $binary->formatName = 'UNKNOWN';
            }
            if (!$entity->get('hash_algo') === 'sha256') {
                $binary->messageDigestAlgorithm = [
                    '@' => $entity->get(
                        'hash_algo'
                    ),
                ];
            }
            $binary->messageDigest = $entity->get('hash');
            $binary->size = $entity->get('size');
            $objects .= $binary;

            $entry = $EventLogs->newEntry(
                'archivebinary_add',
                'success',
                $details = __(
                    "Stockage du fichier ''{0}'' sur l'espace de conservation ''{1}''",
                    $entity->get('filename'),
                    $entity->get('space')
                ),
                $user_id
            );
            $entry->set('object_model', 'ArchiveBinaries');
            $entry->set(
                'object_foreign_key',
                $entity->get('archive_binary_id')
            );

            $data = $entry->toArray();
            unset($data['exported'], $data['outcome_detail']);
            $entry = $EventLogs->find()
                ->where($data)
                ->orderBy(['id'])
                ->first();
            if (!$entry) {
                $io->abort(
                    __(
                        "Anomalie critique ! L'événement {0} {1}:{2} "
                        . "devrait exister mais il n'a pas été trouvé en base",
                        'archivebinary_add',
                        'ArchiveBinaries',
                        $entity->get('archive_binary_id')
                    )
                );
            }
            $this->lastEventId = max($this->lastEventId, $entry->id);

            $createArchiveBinaryEvent = new Premis\Event(
                'archivebinary_add',
                'local',
                'EventLogs:' . $entry->id
            );
            $createArchiveBinaryEvent->detail = __("stockage d'un fichier");
            $createArchiveBinaryEvent->outcome = 'success';
            $createArchiveBinaryEvent->outcomeDetail = $details;
            $createArchiveBinaryEvent->dateTime = $archive_date;
            $createArchiveBinaryEvent->addAgent($agent);
            $createArchiveBinaryEvent->addObject($binary);
            $events .= $createArchiveBinaryEvent;
        }
        $dom2 = new DOMDocument();
        $dom2->formatOutput = true;
        $dom2->preserveWhiteSpace = false;
        $xml = $dom->saveXML();
        $firstEvent = strpos($xml, '<event');
        $firstAgent = strpos($xml, '<agent');
        $oldEvents = substr($xml, $firstEvent, $firstAgent - $firstEvent);
        $oldAgents = substr($xml, $firstAgent);
        $dom2->loadXML(
            substr(
                $xml,
                0,
                $firstEvent
            ) . $objects . $oldEvents . $events . $oldAgents
        );
        if ($dom2->schemaValidate(PREMIS_V3)) {
            $io->success('Premis OK');
        } else {
            $io->error('BAD Premis');
            throw new Exception();
        }
        return $dom2;
    }

    /**
     * Donne l'Agent lié au créateur de l'archive
     * @return Premis\Agent
     */
    private function getUserAgent(): Premis\Agent
    {
        /** @var User $user */
        $user = Hash::get($this->Archive, 'transfers.0.user');
        return $user->toPremisAgent();
    }

    /**
     * Ajoute les fichiers de management dans le premis
     * @param DOMDocument $premis
     * @throws Exception
     */
    private function appendManagementFiles(DOMDocument $premis)
    {
        $this->identifier = $this->Archive->get('archival_agency_identifier');
        $this->agent = $this->getUserAgent();
        $this->saveArchiveFile($premis, ArchiveFilesTable::TYPE_LIFECYCLE);
        $this->saveArchiveFile($premis, ArchiveFilesTable::TYPE_DESCRIPTION);
        $this->saveArchiveFile($premis, ArchiveFilesTable::TYPE_TRANSFER);
    }

    /**
     * Ajoute un fichier de management en bdd, dans le premis (lifecycle)
     * et le journal d'evenements
     * @param DOMDocument $premis
     * @param string      $type
     * @return EntityInterface
     * @throws Exception
     */
    private function saveArchiveFile(DOMDocument $premis, string $type)
    {
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $user_id = Hash::get($this->Archive, 'transfers.0.user_id');
        $filename = urlencode($this->identifier) . '_' . $type . '.xml';
        $storedFile = $this->fetchTable('StoredFiles')->find()
            ->innerJoinWith('ArchiveFiles')
            ->where(
                [
                    'ArchiveFiles.archive_id' => $this->Archive->get('id'),
                    'ArchiveFiles.type' => $type,
                ]
            )
            ->contain(['ArchiveFiles'])
            ->firstOrFail();
        /** @var EntityInterface $archiveFile */
        $archiveFile = Hash::get($storedFile, 'archive_files.0');
        $binary = new Premis\File(
            'Extensible Markup Language',
            'local',
            'ArchiveFiles:' . $archiveFile->get('id')
        );
        $binary->originalName = $storedFile->get('name');
        $binary->messageDigest = $storedFile->get('hash');
        $binary->formatRegistryName = 'pronom';
        $binary->formatRegistryKey = 'fmt/101';
        $binary->relationship = [
            'relatedObjectIdentifierType' => 'local',
            'relatedObjectIdentifierValue' => 'Archives:' . $this->Archive->get(
                'id'
            ),
        ];
        $binary->storages[] = [
            'contentLocationType' => 'local',
            'contentLocationValue' => 'SecureDataSpaces:' . $storedFile->get(
                'secure_data_space_id'
            ),
            'storageMedium' => Hash::get(
                $this->Archive,
                'secure_data_spaces.name'
            ),
        ];
        $entry = $EventLogs->newEntry(
            'archivefile_add',
            'success',
            $details = __(
                "Stockage du fichier ''{0}'' sur l'espace de conservation ''{1}''",
                $filename,
                Hash::get($this->Archive, 'secure_data_space.name')
            ),
            $user_id,
            $archiveFile
        );
        $data = $entry->toArray();
        unset($data['exported'], $data['outcome_detail']);
        $entry = $EventLogs->find()
            ->where($data)
            ->orderBy(['id'])
            ->first();
        if (!$entry) {
            $this->io->abort(
                __(
                    "Anomalie critique ! L'événement {0} {1}:{2} devrait exister mais il n'a pas été trouvé en base",
                    'archivefile_add',
                    'ArchiveFiles',
                    $archiveFile->id
                )
            );
        }
        $this->lastEventId = max($this->lastEventId, $entry->id);
        $event = new Premis\Event(
            'archivefile_add',
            'local',
            'EventLogs:' . $entry->id
        );
        $event->detail = __("stockage du fichier");
        $event->outcome = 'success';
        $event->outcomeDetail = $details;
        $event->dateTime = (new DateTime())->format(DATE_RFC3339);
        $event->addAgent($this->agent);
        $event->addObject($binary);
        Premis::appendEvent($premis, $event);
        return $archiveFile;
    }
}
