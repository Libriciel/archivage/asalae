<?php

/**
 * Asalae\Command\Lifecycle\LifecycleCommand
 */

namespace Asalae\Command\Lifecycle;

use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Table\StoredFilesVolumesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use DOMDocument;
use Throwable;

/**
 * Réparation des lifecycle
 *
 * @category    Command
 *
 * @copyright   (c) 2021, Libriciel
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 * @property StoredFilesTable StoredFiles
 * @property StoredFilesVolumesTable StoredFilesVolumes
 */
class LifecycleCommand extends Command
{
    /**
     * @var VolumeManager[]
     */
    private $volumeManagers = [];

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'lifecycle';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __(
                "Vérifie que les fichiers du cycle de vie des archives sont présents sur le stockage,"
                . " qu'ils n'ont pas une taille nulle et qu'ils sont conformes au schéma PREMIS (en option)"
            )
        );
        $parser->addOption(
            'schema',
            [
                'help' => __(
                    "Vérifie la conformité du fichier du cycle de vie avec le schema PREMIS"
                ),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'hash',
            [
                'help' => __(
                    "Vérifie le hash contre celui stocké en bdd"
                ),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'volume_id',
            [
                'help' => __("Ne vérifie qu'un volume au sein d'un ECS"),
            ]
        );
        $parser->addOption(
            'offset',
            [
                'help' => __("Permet de reprendre un process interrompu"),
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $query = $this->fetchTable('StoredFiles')->find()
            ->innerJoinWith('ArchiveFiles')
            ->where(['ArchiveFiles.type' => ArchiveFilesTable::TYPE_LIFECYCLE])
            ->orderByAsc('StoredFiles.id');
        $offset = $args->getOption('offset') ?: 0;
        if ($offset) {
            $query->offset($offset);
        }
        $count = $query->count();
        $io->out(__("Vérification de {0} cycles de vie", $count));
        $success = true;

        /** @var EntityInterface $storedFile */
        foreach ($query as $i => $storedFile) {
            if ($i && $i % 100 === 0) {
                $io->out(sprintf('%d / %d', $i + $offset, $count));
            }
            if (!$storedFile->get('size')) {
                $io->err(__("Taille de fichier nulle sur {0}", $storedFile->get('name')));
                $success = false;
                continue;
            }
            $volumeManager = $this->getVolumeManager($storedFile->get('secure_data_space_id'));
            $volumes = $volumeManager->getVolumes();
            $volume_id = (int)$args->getOption('volume_id');
            if ($volume_id) {
                $onlyDriver = $volumes[$volume_id] ?? null;
                $volumes = [$volume_id => $onlyDriver];
                if (!$onlyDriver) {
                    $io->verbose(
                        __("Volume non trouvé dans l'ECS {0}", $storedFile->get('secure_data_space_id'))
                    );
                    continue;
                }
            }
            foreach ($volumes as $volume_id => $driver) {
                try {
                    // Vérification de présence
                    if (!$driver->fileExists($storedFile->get('name'))) {
                        $io->err(
                            __(
                                "Fichier {0} manquant sur le volume {1}",
                                $storedFile->get('name'),
                                $volume_id
                            )
                        );
                        $success = false;
                        continue 2;
                    }

                    // Vérification de référence
                    $ref = $this->fetchTable('StoredFilesVolumes')->exists(
                        [
                            'stored_file_id' => $storedFile->id,
                            'volume_id' => $volume_id,
                        ]
                    );
                    if (!$ref) {
                        $io->err(
                            __(
                                "Référence du fichier {0} manquant sur le volume {1}",
                                $storedFile->get('name'),
                                $volume_id
                            )
                        );
                        $success = false;
                        continue 2;
                    }

                    // schemaValidate PREMIS_V3
                    if ($args->getOption('schema')) {
                        $dom = new DOMDocument();
                        $dom->loadXML(
                            $driver->fileGetContent($storedFile->get('name'))
                        );
                        if (!$dom->schemaValidate(PREMIS_V3)) {
                            $io->err(
                                __(
                                    "Echec de validation du schema sur le fichier {0} sur le volume {1}",
                                    $storedFile->get('name'),
                                    $volume_id
                                )
                            );
                            $success = false;
                            continue 2;
                        }
                    }

                    // validation du hash
                    if ($args->getOption('hash')) {
                        $hash = $driver->hash($storedFile->get('name'), $storedFile->get('hash_algo'));
                        if ($hash !== $storedFile->get('hash')) {
                            $io->err(
                                __(
                                    "Hash du fichier {0} sur le volume {1}"
                                    . " différent de celui en bdd (volume: {2} / bdd: {3})",
                                    $storedFile->get('name'),
                                    $volume_id,
                                    $hash,
                                    $storedFile->get('hash')
                                )
                            );
                            $success = false;
                            continue 2;
                        } else {
                            $io->verbose(sprintf('%d - %s', $storedFile->id, $storedFile->get('name')));
                        }
                    }
                } catch (Throwable $e) {
                    $io->err(__("Exception lors de l'analyse de {0}", $storedFile->get('name')));
                    $io->err((string)$e);
                    $success = false;
                    continue 2;
                }
            }
        }
        if ($success) {
            $io->success(__("Tout les fichiers ont été vérifiés avec succès"));
        } else {
            $io->abort(
                __(
                    "Un ou plusieurs fichiers sont en échec. "
                    . "Utilisez la commande `bin/cake lifecycle repair <filename>`"
                )
            );
        }
    }

    /**
     * Donne l'ECS
     * @param int $secure_data_space_id
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager($secure_data_space_id): VolumeManager
    {
        if (!isset($this->volumeManagers[$secure_data_space_id])) {
            $this->volumeManagers[$secure_data_space_id] = new VolumeManager(
                $secure_data_space_id
            );
        }
        return $this->volumeManagers[$secure_data_space_id];
    }
}
