<?php

/**
 * Asalae\Command\Lifecycle\MissingCommand
 */

namespace Asalae\Command\Lifecycle;

use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchiveKeywordsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeInterface;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Exception;

/**
 * Commande de restauration d'un cycle de vie
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveBinariesTable ArchiveBinaries
 * @property ArchiveFilesTable    ArchiveFiles
 * @property ArchiveKeywordsTable ArchiveKeywords
 * @property EventLogsTable       EventLogs
 * @property StoredFilesTable     StoredFiles
 */
class MissingCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;
    /**
     * @var VolumeManager[]
     */
    private $volumeManagers = [];

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'lifecycle missing';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();

        $parser->setDescription(
            '*************************************************************' . PHP_EOL
            . __(
                "Détecte les lifecycles disparus et les répare avec le fichier .upload"
            ) . PHP_EOL
            . '*************************************************************'
        );

        $parser->addOption(
            'dryrun',
            [
                'help' => __(
                    "Affiche la liste des fichiers du cycle de vie à réparer"
                ),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'only-detected',
            [
                'help' => __(
                    "Vérifi uniquement les lifecycle avec is_integrity_ok à false"
                ),
                'boolean' => true,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;
        $query = $this->fetchTable('StoredFiles')->find()
            ->innerJoinWith('ArchiveFiles')
            ->where(['ArchiveFiles.type' => ArchiveFilesTable::TYPE_LIFECYCLE])
            ->orderBy(['ArchiveFiles.is_integrity_ok IS FALSE' => 'desc'])
            ->contain(
                [
                    'ArchiveFiles' => function (Query $q) {
                        return $q->where(['ArchiveFiles.type' => ArchiveFilesTable::TYPE_LIFECYCLE])
                            ->limit(1);
                    },
                ]
            );
        if ($args->getOption('only-detected')) {
            $query->andWhere(['ArchiveFiles.is_integrity_ok IS' => false]);
        }
        $count = $query->count();
        $io->out(__("Vérification de {0} cycles de vie", $count));
        $success = true;
        $repaired = $args->getOption('dryrun') === false;

        /** @var EntityInterface $storedFile */
        foreach ($query as $i => $storedFile) {
            if ($i && $i % 100 === 0) {
                $io->out(sprintf('%d / %d', $i, $count));
            }
            $volumeManager = $this->getVolumeManager(
                $storedFile->get('secure_data_space_id')
            );
            $existsOnAnotherVolume = null;
            $missings = [];
            // on liste les fichiers existants pour pouvoir les envoyer là où ils manquent
            foreach ($volumeManager->getVolumes() as $volume_id => $driver) {
                if ($driver->fileExists($storedFile->get('name'))) {
                    $existsOnAnotherVolume = $driver;
                } else {
                    $missings[$volume_id] = $driver;
                }
            }
            if (empty($missings)) {
                continue;
            }
            foreach ($missings as $volume_id => $driver) {
                $success = false;
                $processMissing = $this->processMissing(
                    $driver,
                    $volume_id,
                    $storedFile,
                    $existsOnAnotherVolume
                );
                if (!$processMissing) {
                    $repaired = false;
                }
            }
        }
        if ($success) {
            $io->success(__("Tout les fichiers ont été vérifiés avec succès"));
        } elseif ($args->getOption('dryrun')) {
            $io->success(
                __(
                    "Certains fichiers sont manquant, essayez sans l'option dryrun"
                )
            );
        } elseif ($repaired) {
            $io->success(
                __(
                    "Certains fichiers étais manquant mais ils ont tous été réparés"
                )
            );
        } else {
            $io->abort(
                __(
                    "Un ou plusieurs fichiers sont en échec. "
                    . "Utilisez la commande `bin/cake lifecycle repair <filename>`"
                )
            );
        }
    }

    /**
     * Donne l'ECS
     * @param int $secure_data_space_id
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager($secure_data_space_id): VolumeManager
    {
        if (!isset($this->volumeManagers[$secure_data_space_id])) {
            $this->volumeManagers[$secure_data_space_id] = new VolumeManager(
                $secure_data_space_id
            );
        }
        return $this->volumeManagers[$secure_data_space_id];
    }

    /**
     * Tente de réparer un fichier manquant
     * @param VolumeInterface      $driver
     * @param int                  $volume_id
     * @param EntityInterface      $storedFile
     * @param VolumeInterface|null $existsOnAnotherVolume
     * @return bool
     * @throws VolumeException
     */
    private function processMissing(
        VolumeInterface $driver,
        int $volume_id,
        EntityInterface $storedFile,
        ?VolumeInterface $existsOnAnotherVolume
    ) {
        $volumeManager = $this->getVolumeManager(
            $storedFile->get('secure_data_space_id')
        );
        $this->io->err(
            __(
                "Le fichier ''{0}'' est abstent du volume {1}",
                $storedFile->get('name'),
                $volume_id
            )
        );
        $currentRepair = false;
        $dir = dirname($storedFile->get('name'));

        // Si le fichier existe sur un autre volume, on fait une simple copie
        if ($existsOnAnotherVolume) {
            if ($this->args->getOption('dryrun')) {
                $this->io->out(
                    __(
                        "Le fichier ''{0}'' peut être restauré"
                        . " à partir d'un autre volume de l'ECS sur le volume {1}",
                        $storedFile->get('name'),
                        $volume_id
                    )
                );
            } else {
                $existsOnAnotherVolume->streamTo($storedFile->get('name'), $driver);
                $this->io->out(
                    __(
                        "Le fichier ''{0}'' a été restauré"
                        . " à partir d'un autre volume de l'ECS sur le volume {1}",
                        $storedFile->get('name'),
                        $volume_id
                    )
                );
            }
            return true;
        }

        // Si le fichier n'est pas restaurable depuis l'ECS, on regarde les backups en .upload
        // on commence par récupérer la liste des fichiers du dossier management (emplacement des .upload)
        try {
            $files = $driver->ls($dir);
        } catch (VolumeException $e) {
            $this->io->err(
                __(
                    "Erreur lors de la tentative de réparation "
                    . "du fichier ''{0}'' sur le volume {1} : {2}",
                    $storedFile->get('name'),
                    $volume_id,
                    $e->getMessage()
                )
            );
            return false;
        }
        unset($files[0]); // nom du dossier
        if (!$files) {
            $this->io->err(
                __(
                    "Aucuns fichiers détectés dans le dossier"
                    . " ''{0}'' sur le volume {1}",
                    dirname($storedFile->get('name')),
                    $volume_id
                )
            );
        }
        // On parcours les lifecycle.xml.*.upload et on les compare au hash connu
        foreach (array_reverse($files) as $file) {
            if (
                !preg_match(
                    '/^<comment>(.*_lifecycle\.xml.*\.upload)<\/comment>$/',
                    $file,
                    $m
                )
            ) {
                continue;
            }
            $backupHash = $driver->hash(
                $dir . '/' . $m[1],
                $storedFile->get('hash_algo')
            );
            if ($backupHash === $storedFile->get('hash')) {
                $currentRepair = true;
                if ($this->args->getOption('dryrun')) {
                    $this->io->out(
                        __(
                            "Le fichier ''{0}'' peut être restauré"
                            . " à partir du fichier ''{1}'' sur le volume {2}",
                            $storedFile->get('name'),
                            $dir . '/' . $m[1],
                            $volume_id
                        )
                    );
                } else {
                    $driver->rename(
                        $dir . '/' . $m[1],
                        $storedFile->get('name')
                    );
                    $this->io->out(
                        __(
                            "Le fichier ''{0}'' a été restauré"
                            . " à partir du fichier ''{1}'' sur le volume {2}",
                            $storedFile->get('name'),
                            $dir . '/' . $m[1],
                            $volume_id
                        )
                    );
                    $otherVolumes = $volumeManager->getVolumes();
                    unset($otherVolumes[$volume_id]);
                    if (!$otherVolumes) {
                        continue;
                    }
                    $path = sys_get_temp_dir() . DS . uniqid(
                        'lifecycle-repair-'
                    ) . '.xml';
                    $driver->fileDownload(
                        $storedFile->get('name'),
                        $path
                    );
                    foreach ($otherVolumes as $vol_id => $vol) {
                        if (
                            $vol->fileExists(
                                $storedFile->get('name')
                            )
                        ) {
                            continue;
                        }
                        try {
                            $vol->fileUpload(
                                $path,
                                $storedFile->get('name')
                            );
                            $this->io->out(
                                __(
                                    "Le fichier ''{0}'' a été uploadé"
                                    . " à partir du volume ''{1}'' sur le volume {2}",
                                    $storedFile->get('name'),
                                    $volume_id,
                                    $vol_id
                                )
                            );
                        } catch (VolumeException) {
                            $this->io->err(
                                __(
                                    "Erreur lors de l'upload de la"
                                    . " correction du fichier ''{0}'' sur le volume {1}",
                                    $storedFile->get('name'),
                                    $vol_id
                                )
                            );
                            return false;
                        }
                    }
                    unlink($path);
                }
            }
        }
        // Si on a pas réussi à réparer, on propose un repair
        if (!$currentRepair) {
            $this->io->err(
                __(
                    "Aucun fichier de backup correspondant au "
                    . "hash n'a été trouvé sur le volume {0} pour le fichier ''{1}''",
                    $volume_id,
                    $storedFile->get('name')
                )
            );
            $arg = escapeshellarg($storedFile->get('name'));
            $this->io->info(
                __(
                    "Si le fichier n'est pas restauré à partir "
                    . "d'un autre volume, vous pouvez tenter un repair "
                    . "avec la commande suivante : {0}",
                    "bin/cake lifecycle repair $arg"
                )
            );
            return false;
        }
        return true;
    }
}
