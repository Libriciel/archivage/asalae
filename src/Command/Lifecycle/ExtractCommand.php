<?php

/**
 * Asalae\Command\Lifecycle\ExtractCommand
 */

namespace Asalae\Command\Lifecycle;

use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/**
 * Permet d'extraire tous les fichiers des cycles de vie (PREMIS) des archives d'un même service Producteur
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2023, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ExtractCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'lifecycle extract';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();
        $parser->addArgument(
            'archival_agency_identifier',
            [
                'help' => __("Identifiant du Service d'Archives"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'originating_agency_identifier',
            [
                'help' => __("Identifiant du Service Producteur"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'destination_path',
            [
                'help' => __("Dossier de destination (doit être vide et inscriptible)"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return void
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        // vérifier si dossier existe, droits écriture et vide
        $path = realpath($args->getArgument('destination_path'));
        if (!$path) {
            $io->abort(__("Le dossier indiqué n'existe pas"));
        }
        if (!is_writable($path)) {
            $io->abort(__("Le dossier indiqué n'est pas inscriptible"));
        }
        if (count(scandir($path)) !== 2) {
            $io->abort(__("Le dossier indiqué n'est pas vide"));
        }

        // vérifier si sa et sp existent
        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities');

        $sa = $OrgEntities
            ->find()
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'identifier' => $args->getArgument('archival_agency_identifier'),
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES,
                ]
            )
            ->firstOrFail();

        $oa = $OrgEntities
            ->find()
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'identifier' => $args->getArgument('originating_agency_identifier'),
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES,
                ]
            )
            ->firstOrFail();

        // récupérer les archives
        $archives = $loc->get('Archives')->find()
            ->where(
                [
                    'Archives.archival_agency_id' => $sa->get('id'),
                    'Archives.originating_agency_id' => $oa->get('id'),
                    'Archives.state NOT IN' => [
                        ArchivesTable::S_CREATING,
                        ArchivesTable::S_DESCRIBING,
                        ArchivesTable::S_STORING,
                        ArchivesTable::S_MANAGING,
                    ],
                ]
            )
            ->contain(['LifecycleXmlArchiveFiles' => 'StoredFiles']);

        $count = $archives->count();
        $io->out(
            __n(
                "Sauvegarde de {0} fichier",
                "Sauvegarde de {0} fichiers",
                $count,
                $count
            )
        );

        $managers = [];
        foreach ($archives as $archive) {
            $secure_data_space_id = Hash::get($archive, 'lifecycle_xml_archive_file.stored_file.secure_data_space_id');
            if (!isset($managers[$secure_data_space_id])) {
                $managers[$secure_data_space_id] = new VolumeManager($secure_data_space_id);
            }
            $managers[$secure_data_space_id]->fileDownload(
                Hash::get($archive, 'lifecycle_xml_archive_file.stored_file.name'),
                $path . DS . Hash::get($archive, 'lifecycle_xml_archive_file.filename')
            );
        }

        $io->success('done');
    }
}
