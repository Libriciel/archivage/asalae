<?php

/**
 * Asalae\Command\Patch2112Command
 */

namespace Asalae\Command;

use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Utility\DOMUtility;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use DOMNode;
use Exception;

/**
 * mise à jour des fichiers deleted.json
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Patch2112Command extends Command
{
    /**
     * @var ArchiveUnitsTable
     */
    private $ArchiveUnits;

    /**
     * @var EntityInterface
     */
    private $archive;

    /**
     * @var VolumeManager[]
     */
    private $volumeManagers = [];

    /**
     * @var string[]
     */
    private $deletedNodes = [];

    /**
     * @var string
     */
    private $uaXpath;

    /**
     * @var string
     */
    private $uaIdentifierXPath;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'patch2112';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();

        $parser->setDescription(
            __("Mise à jour des fichiers de management des archives deleted.json")
        );

        $parser->addOption(
            'dryrun',
            [
                'help' => __(
                    "Effectue le caclul mais ne met pas à jour les fichiers deleted.json"
                ),
                'boolean' => true,
            ]
        );

        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        // chargement des tables
        $this->ArchiveUnits = $this->fetchTable('ArchiveUnits');

        $io->out(__("Mise à jour des fichiers deleted.json"));
        $queryArchives = $this->fetchTable('Archives')
            ->find()
            ->where(
                [
                    'Archives.state not in' =>
                        [
                            ArchivesTable::S_CREATING,
                            ArchivesTable::S_DESCRIBING,
                            ArchivesTable::S_STORING,
                            ArchivesTable::S_MANAGING,
                            ArchivesTable::S_DESTROYED,
                            ArchivesTable::S_RESTITUTED,
                            ArchivesTable::S_TRANSFERRED,
                        ],
                ]
            )
            ->orderByAsc('Archives.id')
            ->disableHydration();
        $count = $queryArchives->count();
        $updatedCount = 0;
        $mtime = microtime(true);
        foreach ($queryArchives as $i => $this->archive) {
            if (microtime(true) - $mtime > 10.0) {
                $mtime = microtime(true);
                $io->out(__("Archives {0}/{1}", $i + 1, $count));
            }

            // initialisation du volumemanager
            $volumeManager = $this->getVolumeManager($this->archive['secure_data_space_id']);

            // si pas de fichier deleted.json, on passe au suivant
            $deletedArchiveFile = $this->fetchTable('ArchiveFiles')->find()
                ->where(
                    [
                        'archive_id' => $this->archive['id'],
                        'type' => 'deleted',
                    ]
                )
                ->disableHydration()
                ->first();
            if (!$deletedArchiveFile) {
                continue;
            }

            // chargement du fichier de description dans un DOMUtilioty
            $descriptionArchiveFile = $this->fetchTable('ArchiveFiles')->find()
                ->where(
                    [
                        'archive_id' => $this->archive['id'],
                        'type' => 'description',
                    ]
                )
                ->disableHydration()
                ->first();
            if (!$descriptionArchiveFile) {
                $io->abort(
                    __(
                        "Fichier de description non trouvé en base de données pour l'archive id: {0}",
                        $this->archive['id']
                    )
                );
            }
            $util = DOMUtility::loadXML(
                $volumeManager->get($descriptionArchiveFile['stored_file_id'])
            );

            // initialisations pour la recherche des noeuds supprimés
            $this->deletedNodes = [];
            if ($util->namespace === NAMESPACE_SEDA_02) {
                $this->uaXpath = 'ns:Contains';
                $this->uaIdentifierXPath = 'ns:ArchivalAgencyObjectIdentifier';
                $context = null;
            } elseif ($util->namespace === NAMESPACE_SEDA_10) {
                $this->uaXpath = 'ns:ArchiveObject';
                $this->uaIdentifierXPath = 'ns:ArchivalAgencyObjectIdentifier';
                $context = null;
            } else {
                $this->uaXpath = 'ns:ArchiveUnit';
                $this->uaIdentifierXPath = 'ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier';
                $context = $util->node('/ns:Archive/ns:DescriptiveMetadata/ns:ArchiveUnit');
            }

            // recherche itérative des paths des unités d'archives non présentes en base de données
            $this->searchDeletedArchiveUnitsNodes($util, $context);

            // mise à jour du fichier deleted.json si besoin
            $currentDeletedContent = $volumeManager->get($deletedArchiveFile['stored_file_id']);
            $currentDeletedNodes = json_decode($currentDeletedContent, true);
            sort($currentDeletedNodes);
            sort($this->deletedNodes);
            if ($this->deletedNodes !== $currentDeletedNodes) {
                $updatedCount++;
                $io->out(
                    __(
                        "Différences détectées pour l'archive {0} (id:{1})",
                        $this->archive['archival_agency_identifier'],
                        $this->archive['id']
                    )
                );
                $io->out('  ' . __("contenu à remplacer") . '     : ' . $currentDeletedContent);
                $io->out(
                    '  ' . __("contenu de remplacement") . ' : '
                    . json_encode($this->deletedNodes, JSON_UNESCAPED_SLASHES)
                );
                if ($args->getOption('dryrun')) {
                    $io->out('  ' . __("fichier {0} non mis à jour (dryrun)", $deletedArchiveFile['filename']));
                } else {
                    $volumeManager->set(
                        $deletedArchiveFile['stored_file_id'],
                        json_encode($this->deletedNodes, JSON_UNESCAPED_SLASHES)
                    );
                    $io->out('  ' . __("fichier {0} deleted.json mis à jour", $deletedArchiveFile['filename']));
                }
            }
        }
        if ($args->getOption('dryrun')) {
            $io->out(__("{0} fichiers deleted.json doivent être mis à jour", $updatedCount));
        } else {
            $io->out(__("{0} fichiers deleted.json mis à jour", $updatedCount));
        }

        $io->success('done');

        return self::CODE_SUCCESS;
    }

    /**
     * Donne l'ECS avec mise en cache
     * @param int $secure_data_space_id
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(int $secure_data_space_id): VolumeManager
    {
        if (!isset($this->volumeManagers[$secure_data_space_id])) {
            $this->volumeManagers[$secure_data_space_id] = new VolumeManager($secure_data_space_id);
        }
        return $this->volumeManagers[$secure_data_space_id];
    }

    /**
     * recherche récursive des unités d'archives supprimées
     * @param DOMUtility $util
     * @param ?DOMNode   $context
     * @return void
     */
    private function searchDeletedArchiveUnitsNodes(DOMUtility &$util, ?DOMNode $context): void
    {
        $uaNodes = $util->xpath->query($this->uaXpath, $context);
        foreach ($uaNodes as $uaNode) {
            // identifiant de l'ua
            $uaIdentifier = $util->nodeValue($this->uaIdentifierXPath, $uaNode);
            // cas des références vers d'autre UA en seda 2.x (ArchiveUnit.ArchiveUnitRefId
            if (!$uaIdentifier) {
                continue;
            }
            // existe en base de données
            if (
                $this->ArchiveUnits->exists(
                    [
                        'archive_id' => $this->archive['id'],
                        'archival_agency_identifier' => $uaIdentifier,
                    ]
                )
            ) {
                $this->searchDeletedArchiveUnitsNodes($util, $uaNode);
            } else {
                $this->deletedNodes[] = $util->getElementXpath($uaNode);
            }
        }
    }
}
