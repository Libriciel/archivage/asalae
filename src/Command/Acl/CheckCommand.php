<?php

/**
 * Asalae\Command\Configuration\CheckCommand
 */

namespace Asalae\Command\Acl;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;

/**
 * Redo de la commande acl check
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CheckCommand extends AbstractAclCommand
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'acl check';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(__d('cake_acl', 'Use this command to check ACL permissions.'));
        $parser->addArgument(
            'aro',
            [
                'help' => __d('cake_acl', 'ARO to check.'),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'aco',
            [
                'help' => __d('cake_acl', 'ACO to check.'),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'action',
            [
                'help' => __d('cake_acl', 'Action to check'),
                'choices' => ['*', 'create', 'read', 'update', 'delete'],
            ]
        );
        return $parser;
    }

    /**
     * Vérifi l'accès d'un utilisateur/role (aro) à un controller/action (aco)
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     * @throws Exception
     * @see PermissionsTable::check()
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        if ($this->genericCommand('check', $args)) {
            $io->out(__d('cake_acl', '{0} is <success>allowed</success>.', [$this->getAro($args)]));
            return self::CODE_SUCCESS;
        } else {
            $io->out(__d('cake_acl', '{0} is <error>not allowed</error>.', [$this->getAro($args)]));
            return self::CODE_ERROR;
        }
    }
}
