<?php

/**
 * Asalae\Command\Configuration\InheritCommand
 */

namespace Asalae\Command\Acl;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;

/**
 * Commande ACL pour définir les permissions à inherit
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class InheritCommand extends AbstractAclCommand
{
    /**
     * Retourne le nom de la commande.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'acl inherit';
    }

    /**
     * Configure les options et le parser pour la commande.
     *
     * En surchargeant cette méthode, il est possible de configurer
     * le ConsoleOptionParser avant de le retourner.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();

        $parser->setDescription(
            __d(
                'cake_acl',
                'Use this command to force a child ARO object to inherit' .
                ' its permissions settings from its parent.'
            ),
        );
        $parser->addArgument(
            'aro',
            [
                'help' => __d('cake_acl', 'ARO to have permissions inherit.'),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'aco',
            [
                'help' => __d('cake_acl', 'ACO to inherit permissions on.'),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'action',
            [
                'help' => __d('cake_acl', 'Action to inherit'),
                'choices' => ['*', 'create', 'read', 'update', 'delete'],
            ]
        );
        return $parser;
    }

    /**
     * Définit l'accès d'un utilisateur/role (aro) sur inherit pour un controller/action (aco).
     *
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     * @throws Exception
     * @see PermissionsTable::allow()
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        if ($this->genericCommand('inherit', $args, 0)) {
            $io->out(__d('cake_acl', 'Permission inherited.'));
            return self::CODE_SUCCESS;
        } else {
            $io->out(__d('cake_acl', 'Permission was not inherited.'));
            return self::CODE_ERROR;
        }
    }
}
