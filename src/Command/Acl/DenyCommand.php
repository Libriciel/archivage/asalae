<?php

/**
 * Asalae\Command\Configuration\DenyCommand
 */

namespace Asalae\Command\Acl;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;

/**
 * Redo de la commande acl deny
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DenyCommand extends AbstractAclCommand
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'acl deny';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();

        $parser->setDescription(
            __d(
                'cake_acl',
                'Use this command to deny ACL permissions. Once executed,' .
                ' the ARO specified (and its children, if any) will have ALLOW' .
                ' access to the specified ACO action (and the ACO\'s children, if any).'
            )
        );
        $parser->addArgument(
            'aro',
            [
                'help' => __d('cake_acl', 'ARO to deny permission to.'),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'aco',
            [
                'help' => __d('cake_acl', 'ACO to deny permission to.'),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'action',
            [
                'help' => __d('cake_acl', 'Action to deny'),
                'choices' => ['*', 'create', 'read', 'update', 'delete'],
            ]
        );
        return $parser;
    }

    /**
     * Vérifi l'accès d'un utilisateur/role (aro) à un controller/action (aco)
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     * @throws Exception
     * @see PermissionsTable::allow()
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        if ($this->genericCommand('deny', $args, -1)) {
            $io->out(__d('cake_acl', 'Permission denied.'));
            return self::CODE_SUCCESS;
        } else {
            $io->out(__d('cake_acl', 'Permission was not denied.'));
            return self::CODE_ERROR;
        }
    }
}
