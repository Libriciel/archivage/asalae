<?php

/**
 * Asalae\Command\Configuration\GrantCommand
 */

namespace Asalae\Command\Acl;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;

/**
 * Redo de la commande acl grant
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class GrantCommand extends AbstractAclCommand
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'acl grant';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();

        // Mise à jour de la description
        $parser->setDescription(
            __d(
                'cake_acl',
                'Use this command to deny ACL permissions. Once executed,' .
                ' the ARO specified (and its children, if any) will have DENY' .
                ' access to the specified ACO action (and the ACO\'s children, if any).'
            )
        );
        $parser->addArgument(
            'aro',
            [
                'help' => __d('cake_acl', 'ARO to deny.'),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'aco',
            [
                'help' => __d('cake_acl', 'ACO to deny.'),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'action',
            [
                'help' => __d('cake_acl', 'Action to deny'),
                'default' => 'all',
            ]
        );

        return $parser;
    }

    /**
     * Vérifi l'accès d'un utilisateur/role (aro) à un controller/action (aco)
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     * @throws Exception
     * @see PermissionsTable::allow()
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        if ($this->genericCommand('allow', $args)) {
            $io->out(__d('cake_acl', 'Permission <success>granted</success>.'));
            return self::CODE_SUCCESS;
        } else {
            $io->out(__d('cake_acl', 'Permission was <error>not granted</error>.'));
            return self::CODE_ERROR;
        }
    }
}
