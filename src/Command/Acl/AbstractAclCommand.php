<?php

/**
 * Asalae\Command\Acl\AbstractAclCommand
 */

namespace Asalae\Command\Acl;

use AsalaeCore\Controller\Component\AclComponent;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Http\ServerRequest;
use Exception;

/**
 * Réécriture des commandes ACL (parties communes)
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class AbstractAclCommand extends Command
{
    private AclComponent $Acl;

    /**
     * Retrieves the AclComponent instance. If it has not already been instantiated,
     * initializes it using the ComponentRegistry.
     *
     * @return AclComponent The AclComponent instance.
     * @throws Exception
     */
    protected function getAclComponent(): AclComponent
    {
        if (empty($this->Acl)) {
            $registry = new ComponentRegistry(new Controller(new ServerRequest()));
            $this->Acl = new AclComponent($registry);
        }
        return $this->Acl;
    }

    /**
     * Retrieves the 'aro' argument value from the provided arguments object.
     *
     * If the 'aro' value is numeric, it will be returned as an integer. Otherwise,
     * it will be returned in its original form, which could be a string or null.
     *
     * @param Arguments $args The arguments object containing the 'aro' field.
     * @return string|int|null The value of 'aro', either as a string, integer, or null.
     */
    protected function getAro(Arguments $args): string|int|null
    {
        $aro = $args->getArgument('aro');
        return is_numeric($aro) ? intval($aro) : $aro;
    }

    /**
     * Retrieves the 'aco' argument value from the provided arguments object.
     *
     * If the 'aco' value is numeric, it will be returned as an integer. Otherwise,
     * it will be returned in its original form, which could be a string or null.
     *
     * @param Arguments $args The arguments object containing the 'aco' field.
     * @return string|int|null The value of 'aco', either as a string, integer, or null.
     */
    protected function getAco(Arguments $args): string|int|null
    {
        $aco = $args->getArgument('aco');
        return is_numeric($aco) ? intval($aco) : $aco;
    }

    /**
     * Retrieves the action argument from the given arguments or returns a default value.
     *
     * @param Arguments $args The arguments object containing the action parameter.
     * @return string|int|null The value of the action argument, or a default value if not found.
     */
    protected function getAction(Arguments $args): string|int|null
    {
        return $args->getArgument('action') ?: '*';
    }

    /**
     * Executes a generic ACL command using the provided command name and arguments.
     *
     * @param string    $command The name of the ACL command to execute.
     * @param Arguments $args    An instance of Arguments containing the necessary parameters for the command.
     * @param int|null  $value
     * @return bool Returns true if the command was executed successfully, false otherwise.
     * @throws Exception
     * @see PermissionsTable::check()
     * @see PermissionsTable::allow()
     */
    protected function genericCommand(string $command, Arguments $args, ?int $value = null): bool
    {
        $component = $this->getAclComponent();
        $arguments = [
            $this->getAro($args),
            $this->getAco($args),
            $this->getAction($args),
        ];
        if ($value !== null) {
            $arguments[] = $value;
        }
        return call_user_func_array([$component, $command], $arguments);
    }
}
