<?php

/**
 * Asalae\Command\MinkCommand
 */

namespace Asalae\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\MinkSuite\MinkCase;
use AsalaeCore\MinkSuite\MinkConsole;
use AsalaeCore\MinkSuite\MinkSuite;
use AsalaeCore\Utility\Exec;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Client as CakeHttpClient;
use Exception;
use PDOException;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use RuntimeException;
use Throwable;

/**
 * Contrôle de mink
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MinkCommand extends Command
{
    public const string MINK_BASEURL = 'https://localhost:3443/api/webservices/ping';
    public const string MINK_SERVICE = 'mink_web';
    public const string ASALAE_SERVICE = 'asalae_web';
    public const string MINK_COMPOSE_FILE = 'docker-compose-mink.yml';
    public const string MINK_BROWSER_URL = 'http://localhost:9300';

    public static MinkSuite $minkSuite;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'mink';
    }

    /**
     * Gets the option parser instance and configures it.
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Commandes de lancement des tests mink, à lancer avec bin/mink après un bin/chrome") . PHP_EOL
            . '*************************************************************'
        );
        $parser->addArgument(
            'arg1',
            [
                'help' => __("Chemin vers le test mink à executer (optionnel) ou action (up ou down)"),
            ]
        );
        $parser->addOption(
            'console',
            [
                'help' => __("Lance mink en mode console"),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'console-on-fail',
            [
                'help' => __("Lance mink en mode console en cas d'erreur"),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'no-login',
            [
                'help' => __(
                    "En mode console, ne redirige pas automatiquement sur la page de login"
                ),
                'boolean' => true,
            ]
        );
        return $parser;
    }

    /**
     * Donne un lien de changement de mot de passe utilisateur
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     * @throws Throwable
     * @throws ReflectionException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        parent::execute($args, $io);
        if (!defined('MINK_TEST_CASE')) {
            $io->abort(__("Ce script doit être lancé par bin/mink"));
        }

        if ($args->getArgument('arg1') === 'down') {
            $this->down();
            return self::CODE_SUCCESS;
        }

        $this->bootstrap($io);

        if ($args->getArgument('arg1') === 'up') {
            return self::CODE_SUCCESS;
        }

        if ($args->getOption('console')) {
            return $this->runConsole($args, $io);
        }

        if ($args->getArgument('arg1')) {
            $success = static::$minkSuite->run(realpath($args->getArgument('arg1')));
        } else {
            $success = static::$minkSuite->run();
        }

        if (!$success) {
            if ($args->getOption('console-on-fail')) {
                $Mink = static::$minkSuite->getMink();
                $mink = new MinkCase('console', $Mink, [], $io);
                $minkConsole = new MinkConsole($mink);
                $minkConsole->console();
            }
            return self::CODE_ERROR;
        }
        return self::CODE_SUCCESS;
    }

    /**
     * Mode mink --console (écriture/debug des tests)
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     * @throws ReflectionException
     */
    private function runConsole(Arguments $args, ConsoleIo $io)
    {
        $Mink = static::$minkSuite->getMink();
        $mink = new MinkCase('console', $Mink, [], $io);
        $minkConsole = new MinkConsole($mink);
        $refl = new ReflectionClass($mink);
        $methods = array_map(
            fn($v) => sprintf(
                "%s(%s)",
                $v->name,
                implode(
                    ', ',
                    array_map(
                        fn($v2) => $v2->getName(),
                        $v->getParameters()
                    )
                )
            ),
            $refl->getMethods(ReflectionMethod::IS_PROTECTED)
        );
        sort($methods);
        $io->out(
            sprintf(
                "%s\ntype 'exit' to quit\n%s\nusage exemple:\n%s\n\n\$mink method list:\n%s",
                str_repeat('=', 30),
                str_repeat('=', 30),
                '$mink->doLogin();',
                '- ' . implode("\n- ", $methods)
            )
        );
        if (!$args->getOption('no-login')) {
            $minkConsole->doLogin();
        }
        $minkConsole->console();

        return self::CODE_SUCCESS;
    }

    /**
     * Vérifi/lance la stack mink et check la connectivité avec bdd et chrome
     * @param ConsoleIo $io
     * @return void
     * @throws Exception
     */
    private function bootstrap(ConsoleIo $io)
    {
        $client = new CakeHttpClient(
            [
                'ssl_verify_peer' => false,
                'ssl_verify_host' => false,
            ]
        );
        try {
            $response = $client->get(self::MINK_BASEURL);
            $success = $response->getStatusCode() === 200;
        } catch (Exception) {
            $success = false;
        }
        if ($success) {
            $io->out(__("Connexion sur Chrome réussie"));
        }

        /** @var Exec $Exec */
        $Exec = Utility::get('Exec');

        // Si asalae_web tourne, on coupe la stack
        $asalaeWebRunning = $Exec->command('docker compose ps -q ' . self::ASALAE_SERVICE);
        if ($asalaeWebRunning->success && !empty($asalaeWebRunning->stdout)) {
            $Exec->proc('docker compose kill');
        }

        // Si mink ne tourne pas, on lance la stack mink
        $minkWebRunning = $Exec->command(
            sprintf(
                'docker compose -f %s ps -q %s',
                self::MINK_COMPOSE_FILE,
                self::MINK_SERVICE
            )
        );
        if (!$minkWebRunning['success'] || empty($minkWebRunning['stdout'])) {
            $Exec->proc(sprintf('docker compose -f %s kill', self::MINK_COMPOSE_FILE));
            $Exec->proc(sprintf('docker compose -f %s down -v --remove-orphans', self::MINK_COMPOSE_FILE));
            $Exec->proc(sprintf('docker compose -f %s up -d --wait', self::MINK_COMPOSE_FILE));
        }

        /**
         * On vérifie la connexion avec la base de données
         */
        try {
            $conn = ConnectionManager::get('default');
            $conn->selectQuery('select 1');
        } catch (PDOException | MissingConnectionException) {
            $io->abort(__("Echec de connexion à la base de données"));
        }

        $minkOptions = [
            'pause' => 0,
            'verbose' => false,
            'quiet' => false,
        ];
        static::$minkSuite = new MinkSuite(self::MINK_BROWSER_URL, $minkOptions);

        $mink = static::$minkSuite->getMink();
        if (!$mink->isSessionStarted(BROWSER_SESSION)) {
            throw new RuntimeException('Mink session is not started.');
        }
    }

    /**
     * Remet le serveur dans son état normal
     * @return void
     * @throws Exception
     */
    private function down()
    {
        /** @var Exec $Exec */
        $Exec = Utility::get('Exec');
        $Exec->proc('docker compose -f docker-compose-mink.yml kill');
        $Exec->proc('docker compose -f docker-compose-mink.yml down -v --remove-orphans');
        $Exec->proc('docker compose up -d --wait');
    }
}
