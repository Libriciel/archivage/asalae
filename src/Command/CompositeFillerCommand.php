<?php

/**
 * Asalae\Command\CalcCountSizeArchivesCommand
 */

namespace Asalae\Command;

use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\UsersTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\DOMUtility;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Command\Helper\ProgressHelper;
use Cake\Utility\Hash;
use DateTime;
use DOMException;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Pheanstalk\PheanstalkInterface;

/**
 * Recalcule les count et size liés aux archives
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CompositeFillerCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    public $io;
    /**
     * @var Arguments
     */
    public $args;
    /**
     * @var array
     */
    private $defaultTransferParams = [];

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'composite filler';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __("Permet de faire des transferts composites en masse pour des tests")
        );
        $parser->addArgument(
            'user_id',
            [
                'help' => __("Id de l'utilisateur"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'amount',
            [
                'help' => __("Quantité d'archives"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'target_identifier',
            [
                'help' => __("Identifiant de l'unité d'archives cible"),
                'required' => true,
            ]
        );
        $parser->addOption(
            'keep',
            [
                'help' => __("Appraisal rule à 'keep' plutôt que 'destroy'"),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'datasource',
            [
                'help' => __("Datasource utilisé"),
                'default' => 'default',
            ]
        );
        $parser->addOption(
            'agreement_id',
            [
                'help' => __("Id de l'accord de versement"),
            ]
        );
        $parser->addOption(
            'profile_id',
            [
                'help' => __("Id du profil d'archives"),
            ]
        );
        $parser->addOption(
            'service_level_id',
            [
                'help' => __("Id du niveau de service"),
            ]
        );
        $parser->addOption(
            'originating_agency_id',
            [
                'help' => __("Id du service producteur"),
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->args = $args;
        $this->io = $io;
        $opts = ['connectionName' => $args->getOption('datasource')];

        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives', $opts);
        $archive = $Archives->find()
            ->leftJoinWith('ArchiveUnits')
            ->where(
                [
                    'ArchiveUnits.archival_agency_identifier' => $args->getArgument('target_identifier'),
                ]
            )
            ->firstOrFail();

        if ($archive->get('state') !== ArchivesTable::S_AVAILABLE) {
            $this->io->abort(__("L'archive cible doit être disponnible"));
        }

        /** @var TransfersTable $Archives */
        $Transfers = $this->fetchTable('Transfers', $opts);
        $firstTransfer = $Transfers->find()
            ->innerJoinWith('ArchivesTransfers')
            ->where(['ArchivesTransfers.archive_id' => $archive->id])
            ->orderByAsc('Transfers.id')
            ->firstOrFail();
        if (in_array($firstTransfer->get('message_version'), ['seda0.2', 'seda1.0'])) {
            $this->io->abort(__("L'archive cible doit être en seda 2.x"));
        }
        $this->defaultTransferParams = [
            'transferring_agency_id' => $firstTransfer->get('transferring_agency_id'),
            'agreement_id' => $args->getOption('agreement_id')
                ?: $firstTransfer->get('agreement_id'),
            'profile_id' => $args->getOption('profile_id')
                ?: $firstTransfer->get('profile_id'),
            'service_level_id' => $args->getOption('service_level_id')
                ?: $firstTransfer->get('service_level_id'),
            'originating_agency_id' => $args->getOption('originating_agency_id')
                ?: $firstTransfer->get('originating_agency_id'),
        ];

        /** @var UsersTable $Archives */
        $Users = $this->fetchTable('Users', $opts);
        $user = $Users->find()
            ->where(['Users.id' => $args->getArgument('user_id')])
            ->contain(
                [
                    'SuperArchivistsArchivalAgencies' => [
                        'ArchivalAgencies',
                    ],
                    'OrgEntities' => [
                        'ArchivalAgencies',
                    ],
                ]
            )
            ->firstOrFail();
        $sas = Hash::extract($user, 'super_archivists_archival_agencies.{n}.archival_agency') ?: [];
        $sas[] = Hash::get($user, 'org_entity.archival_agency');
        $sa = null;
        /** @var EntityInterface|array $s */
        foreach ($sas as $s) {
            if (Hash::get($s, 'id') === $archive->get('archival_agency_id')) {
                $sa = $s;
                break;
            }
        }
        if (!$sa) {
            $this->io->abort(
                __("Le service d'archives ne correspond pas entre l'utilisateur et l'archive cible")
            );
        }
        if (!$sa instanceof EntityInterface) {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = $this->fetchTable('OrgEntities', $opts);
            $sa = $OrgEntities->get($sa['id']);
        }

        $messageVersion = $firstTransfer->get('message_version');

        $Beanstalk = Utility::get('Beanstalk');
        $Beanstalk->setTube('analyse');

        /** @var ProgressHelper $progress */
        $progress = $io->helper('Progress');
        $amount = (int)$args->getArgument('amount');
        $progress->init(['total' => $amount]);

        $conn = $Users->getConnection();
        for ($i = 0; $i < $amount; $i++) {
            $progress->increment();
            $progress->draw();

            $conn->begin();

            /** @var Transfer $transfer */
            $transfer = $this->createTransfer($sa, $user, $messageVersion);
            $this->appendTargetRefToTransfer($transfer);
            $attachment = $this->createAttachment($transfer);
            $transfer->generateArchiveContain([$attachment]);

            $conn->commit();

            $Beanstalk->emit(
                [
                    'user_id' => $user->id,
                    'transfer_id' => $transfer->id,
                ],
                PheanstalkInterface::DEFAULT_PRIORITY,
                1, // pour laisser le temps de finaliser la transaction
                PheanstalkInterface::DEFAULT_TTR
            );
        }
        $progress->draw();

        $io->out();
        $io->success('done');
    }


    /**
     * Crée un transfert
     * @param EntityInterface $sa
     * @param EntityInterface $user
     * @param string          $messageVersion
     * @return EntityInterface
     * @throws Exception
     */
    private function createTransfer(
        EntityInterface $sa,
        EntityInterface $user,
        string $messageVersion
    ): EntityInterface {
        $loc = TableRegistry::getTableLocator();
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        /** @var CountersTable $Counters */
        $Counters = $loc->get('Counters');
        $identifier = $Counters->next($sa->id, 'ArchiveTransfer');
        $data = [
            'transfer_comment' => 'generated transfer for tests',
            'transfer_date' => $date = new DateTime(),
            'transfer_identifier' => $identifier,
            'archival_agency_id' => $sa->id,
            'message_version' => $messageVersion,
            'state' => $Transfers->initialState,
            'state_history' => null,
            'last_state_update' => null,
            'is_conform' => true,
            'is_modified' => false,
            'is_accepted' => true,
            'filename' => $identifier . '.xml',
            'data_size' => null,
            'data_count' => null,
            'created_user_id' => $user->id,
                // pour la génération du xml
            'name' => uuid_create(UUID_TYPE_TIME),
            'description_level' => 'file',
            'code' => 'AR038',
            'start_date' => $date,
            'final' => '',
            'duration' => '',
            'final_start_date' => '',
            'files_deleted' => false,
        ] + $this->defaultTransferParams;
        /** @var Transfer $entity */
        $entity = $Transfers->newEntity($data, ['validate' => false]);
        $Transfers->transitionOrFail($entity, TransfersTable::T_PREPARE);
        $Transfers->transitionOrFail($entity, TransfersTable::T_ANALYSE);
        $Transfers->saveOrFail($entity);
        Filesystem::dumpFile($entity->get('xml'), $entity->generateXml());
        return $entity;
    }

    /**
     * Ajoute les données de référence pour le transfert composite
     * @param EntityInterface $transfer
     * @return void
     * @throws DOMException
     */
    private function appendTargetRefToTransfer(EntityInterface $transfer): void
    {
        $util = DOMUtility::load($transfer->get('xml'));
        $contentNode = $util->node('ns:DataObjectPackage/ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content');
        $relatedObjectReferenceNode = $util->createElement('RelatedObjectReference');
        $contentNode->appendChild($relatedObjectReferenceNode);
        $isPartOfNode = $util->createElement('IsPartOf');
        $relatedObjectReferenceNode->appendChild($isPartOfNode);
        $repositoryArchiveUnitPID = $util->createElement(
            'RepositoryArchiveUnitPID',
            $this->args->getArgument('target_identifier')
        );
        $isPartOfNode->appendChild($repositoryArchiveUnitPID);
        $util->dom->save($transfer->get('xml'));
    }

    /**
     * Créé une pièce jointe au transfert
     * @param EntityInterface $transfer
     * @return EntityInterface
     * @throws Exception
     */
    private function createAttachment(EntityInterface $transfer): EntityInterface
    {
        $opts = ['connectionName' => $this->args->getOption('datasource')];
        $TransferAttachments = $this->fetchTable('TransferAttachments', $opts);
        $content = microtime(true);
        $entity = $TransferAttachments->newEntity(
            [
                'transfer_id' => $transfer->id,
                'filename' => preg_replace('/\D+/', '', $content) . '.txt',
                'size' => 1,
                'hash' => hash('sha256', $content),
                'hash_algo' => 'sha256',
                'virus_name' => '',
                'valid' => true,
                'mime' => 'text/plain',
                'extension' => 'txt',
                'format' => 'x-fmt/111',
            ],
            ['validate' => false]
        );
        $TransferAttachments->saveOrFail($entity);
        Filesystem::dumpFile($entity->get('path'), $content);
        return $entity;
    }
}
