<?php

/**
 * Asalae\Command\DataCoherenceCommand
 */

namespace Asalae\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Error\Debugger;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Exception;

/**
 * Vérification de l'intégrité des données stockés en base
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DataCoherenceAllCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'data_coherence all';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(
            '*************************************************************' . PHP_EOL
            . __(
                "Cohérence des données vis-à-vis des règles de validation définie dans les modèles"
            ) . PHP_EOL
            . '*************************************************************'
        );

        $parser->addOption(
            'model',
            [
                'help' => __("Effectue la recherche sur un model unique"),
                'short' => 'm',
            ]
        );
        $parser->addOption(
            'skip-large-tables',
            [
                'help' => __(
                    "Permet de ne pas exécuter les tables qui contiennent plus de {0} lignes",
                    '<large-table-threshold>'
                ),
                'short' => 's',
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'large-table-threshold',
            [
                'help' => __(
                    "Seuil à partir duquel ont considère une table comme large"
                ),
                'default' => '10000',
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        if ($model = $this->args->getOption('model')) {
            $tables = [$model];
        } else {
            $tables = ConnectionManager::get('default')
                ->getSchemaCollection()
                ->listTables();
        }
        $loc = TableRegistry::getTableLocator();
        $success = true;
        foreach ($tables as $table) {
            $modelName = Inflector::camelize($table);
            $model = $loc->get($modelName);
            if (get_class($model) === Table::class) {
                continue;
            }
            $this->io->out(__("Vérification de {0}", $modelName));
            $query = $model->find();
            $total = $query->count();
            $this->io->out(__("Nombre de lignes: {0}", $total));
            $threshold = $this->args->getOption('large-table-threshold') ?: 10000;
            if ($total >= $threshold && $this->args->getOption('skip-large-tables')) {
                $this->io->warning("skipped");
                continue;
            }
            $c = 0;
            /** @var EntityInterface $entity */
            foreach ($query as $entity) {
                $c++;
                if ($c % $threshold === 0) {
                    $this->io->out(sprintf('%d / %d', $c, $total));
                }
                $model->patchEntity($entity, $entity->toArray());
                if ($errors = $entity->getErrors()) {
                    $this->io->err(sprintf('error: %s:%s', $modelName, $entity->id));
                    $this->io->verbose(Debugger::exportVar($errors));
                    $success = false;
                }
            }
        }
        if ($success) {
            $this->io->success('done');
        } else {
            $this->io->abort(__("Une ou plusieurs erreurs ont été détectées"));
        }
    }

    /**
     * Vérifie une entité en particulier et donne les erreurs
     * @param string $linkEntity
     */
    public function getError(string $linkEntity)
    {
        if (!strpos($linkEntity, ':')) {
            $this->io->abort(
                __("Vous devez indiquer une référence au format ModelName:id")
            );
        }
        [$modelName, $id] = explode(':', $linkEntity);
        $loc = TableRegistry::getTableLocator();
        $model = $loc->get($modelName);
        $entity = $model->get($id);
        $model->patchEntity($entity, $entity->toArray());
        if (!$entity->getErrors()) {
            $this->io->out(__("Aucune erreur détectée"));
        }
        foreach ($entity->getErrors() as $fieldname => $errors) {
            foreach ($errors as $error) {
                $this->io->err(sprintf('%s: %s', $fieldname, $error));
            }
        }
    }
}
