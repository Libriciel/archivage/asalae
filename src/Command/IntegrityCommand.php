<?php

/**
 * Asalae\Command\IntegrityCommand
 */

namespace Asalae\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;

/**
 * Visualise la liste des is_integrity_ok => false
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class IntegrityCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'integrity';
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $loc = TableRegistry::getTableLocator();
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');

        $query = $StoredFilesVolumes->find()
            ->select(
                [
                    'ecs_id' => 'SecureDataSpaces.id',
                    'volume_id' => 'Volumes.id',
                    'storage_ref' => 'StoredFilesVolumes.storage_ref',
                    'hash' => 'StoredFiles.hash',
                    'sfv_integrity' => 'StoredFilesVolumes.is_integrity_ok',
                    'afile_integrity' => 'ArchiveFiles.is_integrity_ok',
                    'binary_integrity' => 'ArchiveBinaries.is_integrity_ok',
                    'archive_integrity' => 'Archives.is_integrity_ok',
                    'tarchive_integrity' => 'TechnicalArchives.is_integrity_ok',
                    'archive_id' => 'ArchiveUnits.archive_id',
                    'technical_archive_id' => 'TechnicalArchiveUnits.technical_archive_id',
                    'archive_file_archive_id' => 'ArchiveFiles.id',
                ]
            )
            ->innerJoinWith('Volumes')
            ->innerJoinWith('Volumes.SecureDataSpaces')
            ->innerJoinWith('StoredFiles')
            ->leftJoinWith('StoredFiles.ArchiveBinaries')
            ->leftJoinWith(
                'StoredFiles.ArchiveBinaries.ArchiveBinariesArchiveUnits'
            )
            ->leftJoinWith(
                'StoredFiles.ArchiveBinaries.ArchiveBinariesArchiveUnits.ArchiveUnits'
            )
            ->leftJoinWith(
                'StoredFiles.ArchiveBinaries.ArchiveBinariesArchiveUnits.ArchiveUnits.Archives'
            )
            ->leftJoinWith(
                'StoredFiles.ArchiveBinaries.ArchiveBinariesTechnicalArchiveUnits'
            )
            ->leftJoinWith(
                'StoredFiles.ArchiveBinaries.ArchiveBinariesTechnicalArchiveUnits.TechnicalArchiveUnits'
            )
            ->leftJoinWith(
                'StoredFiles.ArchiveBinaries.ArchiveBinariesTechnicalArchiveUnits'
                . '.TechnicalArchiveUnits.TechnicalArchives'
            )
            ->leftJoinWith('StoredFiles.ArchiveFiles')
            ->where(
                [
                    [
                        'OR' => [
                            'StoredFilesVolumes.is_integrity_ok IS' => false,
                            'ArchiveBinaries.is_integrity_ok IS' => false,
                            'ArchiveFiles.is_integrity_ok IS' => false,
                            'Archives.is_integrity_ok IS' => false,
                            'TechnicalArchives.is_integrity_ok IS' => false,
                        ],
                    ],
                ]
            )
            ->orderBy(
                [
                    'Volumes.secure_data_space_id',
                    'Volumes.id',
                    'StoredFilesVolumes.storage_ref',
                ]
            );
        $data = [];
        foreach ($query as $int) {
            $flag = [];
            foreach (['sfv', 'binary', 'archive', 'afile', 'tarchive'] as $f) {
                $g = $int->get($f . '_integrity');
                $flag[] = $g === null
                    ? '_'
                    : ($g ? '<success>o</success>'
                        : '<error>x</error>');
            }
            $data[] = [
                'ecs' => (string)$int->get('ecs_id'),
                'vol' => (string)$int->get('volume_id'),
                'storage_ref' => $int->get('storage_ref'),
                'hash' => $int->get('hash'),
                'flag' => implode('', $flag),
            ];
        }

        $flagMsg = 'flag: <success>o</success>=true <error>x</error>=false _=null ;'
            . ' StoredFilesVolumes, ArchiveBinaries, Archives, ArchiveFiles, TechnicalArchives';
        $io->out($flagMsg);
        $fields = ['ecs', 'vol', 'storage_ref', 'hash', 'flag'];
        array_unshift($data, $fields);

        $io->helper('Table')->output($data);

        $io->out($flagMsg);
    }
}
