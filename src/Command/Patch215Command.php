<?php

/**
 * Asalae\Command\Patch215Command
 */

namespace Asalae\Command;

use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Utility\DOMUtility;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\Utility\Hash;
use DateInterval;
use DateTimeInterface;
use DOMElement;
use Exception;

/**
 * Récupère les métadonnées manquante en base
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Patch215Command extends Command
{
    /**
     * @var VolumeManager[]
     */
    private $volumeManager = [];
    /**
     * @var ConsoleIo
     */
    private $io = [];

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'patch215';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __("Récupère les métadonnées manquante en base")
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $memoryDir = Configure::read('App.paths.data') . DS . 'tmp' . DS . 'patch215' . DS;
        if (!is_dir($memoryDir)) {
            mkdir($memoryDir, 0770, true);
        }
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $this->fetchTable('Archives')
            ->find()
            ->innerJoinWith('Transfers')
            ->where(['Transfers.message_version !=' => 'seda0.2']) // pas de problèmes en 0.2
            ->contain(['DescriptionXmlArchiveFiles'])
            ->orderByAsc('Archives.id');
        $count = $query->count();
        $io->out(__("Vérification des métadonnées de {0} archives", $count));
        $mtime = microtime(true);

        foreach ($query as $i => $archive) {
            if (microtime(true) - $mtime > 10.0) {
                $mtime = microtime(true);
                $io->out(__("Archives {0}/{1}", $i + 1, $count));
            }
            if (is_file($memoryDir . $archive->id)) {
                continue;
            }
            $volumeManager = $this->getVolumeManager($archive);
            $storedFileId = Hash::get($archive, 'description_xml_archive_file.stored_file_id');
            if (empty($storedFileId)) {
                $io->err(__("Description de l'archive {0} non trouvée", $archive->id));
                continue;
            }
            $util = DOMUtility::loadXML(
                $volumeManager->get($storedFileId)
            );
            if ($util->namespace === NAMESPACE_SEDA_10) {
                $this->repairSeda10($archive->id, $util);
            } else {
                $this->repairSeda21($archive->id, $util);
            }
            $ArchiveUnits->updateSearchField(
                ['ArchiveUnits.archive_id' => $archive->id]
            );
            touch($memoryDir . $archive->id);
        }
        $io->success('done');
    }

    /**
     * Donne le volume manager lié à une archive
     * @param EntityInterface $archive
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(EntityInterface $archive): VolumeManager
    {
        $secureDataSpaceId = $archive->get('secure_data_space_id');
        if (!isset($this->volumeManager[$secureDataSpaceId])) {
            $this->volumeManager[$secureDataSpaceId] = new VolumeManager($secureDataSpaceId);
        }
        return $this->volumeManager[$secureDataSpaceId];
    }

    /**
     * transferring_agency_identifier et originating_agency_identifier sont récupérés
     * @param int        $archive_id
     * @param DOMUtility $util
     * @return void
     */
    public function repairSeda10(int $archive_id, DOMUtility $util)
    {
        $mtime = microtime(true);
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $util->xpath->query('//ns:OriginatingAgencyObjectIdentifier');
        $count = $query->count();
        /** @var DOMElement $oaoiNode */
        foreach ($query as $i => $oaoiNode) {
            if (microtime(true) - $mtime > 10.0) {
                $mtime = microtime(true);
                $this->io->out(
                    __(
                        "ArchiveUnits (archive_id={0}) - OriginatingAgencyObjectIdentifier {1}/{2}",
                        $archive_id,
                        $i + 1,
                        $count
                    )
                );
            }
            $archiveUnit = $ArchiveUnits->findArchiveUnitByDOMElement($archive_id, $oaoiNode->parentNode);
            if (!$archiveUnit) {
                $this->io->err(
                    __(
                        "L'archive_unit lié au noeud {0} n'a pas été trouvé"
                        . " dans la base de données. Abandon de la mise à jour de l'archive id={1}",
                        $util->getElementXpath($oaoiNode, ''),
                        $archive_id
                    )
                );
                return;
            }
            if (!$archiveUnit->get('originating_agency_identifier')) {
                $ArchiveUnits->updateAll(
                    ['originating_agency_identifier' => trim($oaoiNode->nodeValue)],
                    ['id' => $archiveUnit->id]
                );
            }
        }

        $query = $util->xpath->query('//ns:TransferringAgencyObjectIdentifier');
        $count = $query->count();
        /** @var DOMElement $taoiNode */
        foreach ($query as $i => $taoiNode) {
            if (microtime(true) - $mtime > 10.0) {
                $mtime = microtime(true);
                $this->io->out(
                    __(
                        "ArchiveUnits (archive_id={0}) - TransferringAgencyObjectIdentifier {1}/{2}",
                        $archive_id,
                        $i + 1,
                        $count
                    )
                );
            }
            $archiveUnit = $ArchiveUnits->findArchiveUnitByDOMElement($archive_id, $taoiNode->parentNode);
            if (!$archiveUnit) {
                $this->io->err(
                    __(
                        "L'archive_unit lié au noeud {0} n'a pas été trouvé"
                        . " dans la base de données. Abandon de la mise à jour de l'archive id={1}",
                        $util->getElementXpath($taoiNode, ''),
                        $archive_id
                    )
                );
                return;
            }
            if (!$archiveUnit->get('transferring_agency_identifier')) {
                $ArchiveUnits->updateAll(
                    ['transferring_agency_identifier' => trim($taoiNode->nodeValue)],
                    ['id' => $archiveUnit->id]
                );
            }
        }
    }

    /**
     * originating_agency_identifier et archive_descriptions est récupéré
     * @param int        $archive_id
     * @param DOMUtility $util
     * @return void
     * @throws Exception
     */
    private function repairSeda21(int $archive_id, DOMUtility $util)
    {
        $mtime = microtime(true);
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $ArchiveDescriptions = $this->fetchTable('ArchiveDescriptions');
        $AppraisalRuleCodes = $this->fetchTable('AppraisalRuleCodes');
        $AppraisalRules = $this->fetchTable('AppraisalRules');
        $query = $util->xpath->query('//ns:Content/ns:OriginatingAgencyArchiveUnitIdentifier[1]');
        $count = $query->count();
        /** @var DOMElement $oaaui */
        foreach ($query as $i => $oaaui) {
            if (microtime(true) - $mtime > 10.0) {
                $mtime = microtime(true);
                $this->io->out(
                    __(
                        "ArchiveUnits (archive_id={0}) - OriginatingAgencyArchiveUnitIdentifier {1}/{2}",
                        $archive_id,
                        $i + 1,
                        $count
                    )
                );
            }
            $content = $oaaui->parentNode;
            $archiveUnit = $ArchiveUnits->findArchiveUnitByDOMElement($archive_id, $content->parentNode);
            if (!$archiveUnit) {
                $this->io->err(
                    __(
                        "L'archive_unit lié au noeud {0} n'a pas été trouvé"
                        . " dans la base de données. Abandon de la mise à jour de l'archive id={1}",
                        $util->getElementXpath($oaaui, ''),
                        $archive_id
                    )
                );
                return;
            }
            if (!$archiveUnit->get('originating_agency_identifier')) {
                $ArchiveUnits->updateAll(
                    ['originating_agency_identifier' => trim($oaaui->nodeValue)],
                    ['id' => $archiveUnit->id]
                );
            }
            if ($archiveUnit->get('parent_id') === null) {
                $ArchiveUnits->Archives->updateAll(
                    ['originating_agency_identifier' => trim($oaaui->nodeValue)],
                    ['id' => $archiveUnit->get('archive_id')]
                );
            }
        }

        $query = $util->xpath->query('//ns:ArchiveUnit/ns:Content');
        $count = $query->count();
        /** @var DOMElement $content */
        foreach ($query as $i => $content) {
            if (microtime(true) - $mtime > 10.0) {
                $mtime = microtime(true);
                $this->io->out(
                    __(
                        "ArchiveUnits (archive_id={0}) - Content {1}/{2}",
                        $archive_id,
                        $i + 1,
                        $count
                    )
                );
            }
            $archiveUnit = $ArchiveUnits->findArchiveUnitByDOMElement($archive_id, $content->parentNode);
            if (!$archiveUnit) {
                $this->io->err(
                    __(
                        "L'archive_unit lié au noeud {0} n'a pas été trouvé"
                        . " dans la base de données. Abandon de la mise à jour de l'archive id={1}",
                        $util->getElementXpath($content, ''),
                        $archive_id
                    )
                );
                return;
            }
            $desc = $ArchiveDescriptions->find()
                ->where(['archive_unit_id' => $archiveUnit->id])
                ->first();
            if ($desc) {
                $descriptions = $util->xpath->query('ns:Description', $content);
                $description = [];
                /** @var DOMElement $item */
                foreach ($descriptions as $item) {
                    $description[] = trim($item->nodeValue, '. ');
                }
                $description = implode('. ', $description);
                $history = [];
                $items = $util->xpath->query(
                    'ns:CustodialHistory/ns:CustodialHistoryItem',
                    $content
                );
                /** @var DOMElement $item */
                foreach ($items as $item) {
                    $history[] = trim($item->nodeValue, '. ');
                }
                $history = implode('. ', $history);
                $file_plan_position = $util->node(
                    'ns:FilePlanPosition',
                    $content
                );
                $oldest_date = $util->node(
                    'ns:StartDate',
                    $content
                );
                $latest_date = $util->node(
                    'ns:EndDate',
                    $content
                );
                $data = array_filter(
                    [
                        'description' => $description,
                        'history' => empty($history) ? null : $history,
                        'file_plan_position' => empty($file_plan_position) ? null
                            : trim($file_plan_position->nodeValue),
                        'oldest_date' => empty($oldest_date)
                            ? null
                            : new CakeDateTime(
                                trim($oldest_date->nodeValue)
                            ),
                        'latest_date' => empty($latest_date)
                            ? null
                            : new CakeDateTime(
                                trim($latest_date->nodeValue)
                            ),
                    ]
                );
                if ($data) {
                    $ArchiveDescriptions->updateAll(
                        $data,
                        ['id' => $desc->id]
                    );
                }
            }
        }

        $query = $util->xpath->query('//ns:ArchiveUnit/ns:Management/ns:AppraisalRule');
        $count = $query->count();
        /** @var DOMElement $appraisalRule */
        foreach ($query as $i => $appraisalRule) {
            if (microtime(true) - $mtime > 10.0) {
                $mtime = microtime(true);
                $this->io->out(
                    __(
                        "ArchiveUnits (archive_id={0}) - AppraisalRule {1}/{2}",
                        $archive_id,
                        $i + 1,
                        $count
                    )
                );
            }
            $archiveUnit = $ArchiveUnits->findArchiveUnitByDOMElement(
                $archive_id,
                $appraisalRule->parentNode->parentNode
            );
            if (!$archiveUnit) {
                $this->io->err(
                    __(
                        "L'archive_unit lié au noeud {0} n'a pas été trouvé"
                        . " dans la base de données. Abandon de la mise à jour de l'archive id={1}",
                        $util->getElementXpath($appraisalRule, ''),
                        $archive_id
                    )
                );
                return;
            }
            // Seul les unités d'archives filles doivent être recalculés
            if ($archiveUnit->get('parent_id') === null) {
                continue;
            }
            $prevAppraisalRuleId = $archiveUnit->get('appraisal_rule_id');

            // On récupère le AppraisalRule de l'entité parente
            $parentAppraisalRule = $AppraisalRules->find()
                ->innerJoinWith('ArchiveUnits')
                ->where(['ArchiveUnits.id' => $archiveUnit->get('parent_id')])
                ->firstOrFail();
            $parentAppraisalRuleCodeId = $parentAppraisalRule->get('appraisal_rule_code_id');
            $parentFinalActionCode = $parentAppraisalRule->get('final_action_code');
            $parentStartDate = $parentAppraisalRule->get('start_date');
            $parentStartDate = ($parentStartDate instanceof DateTimeInterface || $parentStartDate instanceof CakeDate)
                ? $parentStartDate->format('Y-m-d')
                : $parentStartDate;

            $finalAction = $util->xpath->query('ns:FinalAction', $appraisalRule)->item(0);
            $code = empty($finalAction) ? 'keep' : trim(strtolower($finalAction->nodeValue));
            $rule = $util->xpath->query('ns:Rule', $appraisalRule)->item(0);
            $startDate = $util->xpath->query('ns:StartDate', $appraisalRule)->item(0);
            if (empty($rule)) {
                continue;
            }
            $duration = substr(trim($rule->nodeValue), 2);

            $appraisalRuleCode = $AppraisalRuleCodes->find()
                ->select(['id', 'duration'])
                ->where(['code' => trim($rule->nodeValue)])
                ->first();
            if (empty($appraisalRuleCode)) {
                $appraisalRuleCode = $AppraisalRuleCodes->find()
                    ->select(['id', 'duration'])
                    ->where(['code' => 'APP0Y'])
                    ->firstOrFail();
            }
            $startDate = new CakeDateTime(
                empty($startDate) ? date('Y-m-d') : trim($startDate->nodeValue)
            );

            if (
                $appraisalRuleCode->get('id') === $parentAppraisalRuleCodeId
                && $code === $parentFinalActionCode
                && $startDate->format('Y-m-d') === $parentStartDate
            ) {
                continue;
            }
            $appraisalRule = $AppraisalRules->newEntity(
                [
                    'appraisal_rule_code_id' => $appraisalRuleCode->get('id'),
                    'final_action_code' => $code,
                    'start_date' => clone $startDate,
                    'end_date' => $startDate->add(new DateInterval($duration)),
                ]
            );
            $AppraisalRules->saveOrFail($appraisalRule);

            $ArchiveUnits->updateAll(
                ['appraisal_rule_id' => $appraisalRule->id],
                [
                    'lft >=' => $archiveUnit->get('lft'),
                    'rght <=' => $archiveUnit->get('rght'),
                    'appraisal_rule_id' => $prevAppraisalRuleId,
                ]
            );
        }
    }
}
