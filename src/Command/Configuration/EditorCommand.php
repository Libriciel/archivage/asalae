<?php

/**
 * Asalae\Command\Configuration\EditorCommand
 */

namespace Asalae\Command\Configuration;

use AsalaeCore\Command\FileTrait;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Error\Debugger;
use Cake\Utility\Hash;
use Exception;

/**
 * Commande configuration editor
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EditorCommand extends AbstractConfigurationCommand
{
    use FileTrait;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'configuration editor';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(__("Editeur interactif de configuration"));
        $apacheUser = exec('apachectl -S 2>/dev/null | grep User | sed \'s/.*name="\([^"]*\)".*/\1/\'')
            ?: 'www-data';

        $parser->addOption(
            'user',
            [
                'short' => 'u',
                'help' => __(
                    "L'utilisateur système http ex: pour apache2: {0}",
                    $apacheUser
                ),
                'default' => $apacheUser,
            ]
        );
        return $parser;
    }

    /**
     * Editeur interactif
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return void
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $this->checkWritable();
        $config = $this->getConfig();
        $this->io->out(
            __d(
                'configuration_shell',
                "Saisissez un chemin de configuration (ex: {0}) suivi par la valeur souhaitée (ex: {1}). 
                    En cas d’ambiguïté sur le type de valeur, un choix vous sera proposé. 
                    En cas de valeur vide, la suppression de la valeur vous sera proposée.",
                "App.defaultLocale",
                "fr_FR"
            )
        );
        do {
            do {
                $path = $this->io->ask(
                    __d(
                        'configuration_shell',
                        "Veuillez indiquer un chemin de configuration à insérer, tapez {0} pour arréter.",
                        "stop"
                    )
                );
            } while (!$path);
            if ($path === 'stop') {
                break;
            }
            if ($value = Hash::get($config, $path)) {
                if (is_array($value)) {
                    $this->io->warning(
                        __d(
                            'configuration_shell',
                            "La valeur est un array, la seule action possible est la suppression"
                        )
                    );
                    $this->io->out(Debugger::exportVar($value));
                    $delete = $this->io->askChoice(
                        __d(
                            'configuration_shell',
                            "Voulez-vous retirer cette valeur ?"
                        ),
                        ['y', 'n'],
                        'n'
                    );
                    if ($delete === 'y') {
                        $config = Hash::remove($config, $path);
                    }
                    $continuer = $this->io->askChoice(
                        __d(
                            'configuration_shell',
                            "Voulez-vous saisir une autre valeur ?"
                        ),
                        ['y', 'n'],
                        'y'
                    );
                    continue;
                }
                $value = '(' . gettype($value) . ')' . $value;
                $this->io->out(
                    __d(
                        'configuration_shell',
                        "<info>Une valeur existe déjà</info> : {0}",
                        '<warning>' . $value . '</warning>'
                    )
                );
            }
            $value = $this->io->ask(
                __d('configuration_shell', "Veuillez indiquer une valeur")
            );
            if ($value === 'true' || $value === 'false') {
                $type = $this->io->askChoice(
                    __d(
                        'configuration_shell',
                        "Insérer une valeur de type {0} ?",
                        "boolean"
                    ),
                    ['boolean', 'string'],
                    'boolean'
                );
                if ($type === 'boolean') {
                    $value = $value === 'true';
                }
            } elseif ($value === 'null') {
                $type = $this->io->askChoice(
                    __d(
                        'configuration_shell',
                        "Insérer une valeur de type {0} ?",
                        "null"
                    ),
                    ['null', 'string'],
                    'null'
                );
                if ($type === 'null') {
                    $value = null;
                }
            } elseif (preg_match('/^\d+\.\d*$/', $value)) {
                $type = $this->io->askChoice(
                    __d(
                        'configuration_shell',
                        "Insérer une valeur de type {0} ?",
                        "float"
                    ),
                    ['float', 'string'],
                    'float'
                );
                if ($type === 'float') {
                    $value = (float)$value;
                }
            } elseif (is_numeric($value)) {
                $type = $this->io->askChoice(
                    __d(
                        'configuration_shell',
                        "Insérer une valeur de type {0} ?",
                        "integer"
                    ),
                    ['integer', 'string'],
                    'integer'
                );
                if ($type === 'integer') {
                    $value = (int)$value;
                }
            }
            $delete = false;
            if ($value === '' || $value === null) {
                $delete = $this->io->askChoice(
                    __d(
                        'configuration_shell',
                        "Voulez-vous retirer cette valeur ?"
                    ),
                    ['y', 'n'],
                    'n'
                );
            }
            if ($delete && $delete !== 'n') {
                $config = Hash::remove($config, $path);
            } else {
                $config = Hash::insert($config, $path, $value);
                $this->io->out(
                    json_encode(
                        Hash::insert([], $path, $value),
                        JSON_UNESCAPED_SLASHES
                    )
                );
            }

            $continuer = $this->io->askChoice(
                __d(
                    'configuration_shell',
                    "Voulez-vous saisir une autre valeur ?"
                ),
                ['y', 'n'],
                'y'
            );
        } while ($continuer === 'y');
        ksort($config);
        $this->createFileWithDefault(
            $this->getConfigPath(),
            json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES),
            'y'
        );
    }
}
