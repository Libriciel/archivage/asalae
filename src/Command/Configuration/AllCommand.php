<?php

/**
 * Asalae\Command\Configuration\AllCommand
 */

namespace Asalae\Command\Configuration;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Utility\Hash;
use Exception;

/**
 * Commande configuration all
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AllCommand extends AbstractConfigurationCommand
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'configuration all';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(__("Visualise les clés de configurations"));
        return $parser;
    }

    /**
     * Affiche toutes les clés de configuration
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return void
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $keys = array_keys(
            Hash::flatten(Hash::merge(Configure::read(), $this->getConfig()))
        );
        sort($keys);
        foreach ($keys as $key) {
            $this->io->out($key);
        }
    }
}
