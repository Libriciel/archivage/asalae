<?php

/**
 * Asalae\Command\Configuration\AbstractConfigurationCommand
 */

namespace Asalae\Command\Configuration;

use AsalaeCore\Command\PasswordTrait;
use AsalaeCore\Utility\Config;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Hash;
use Exception;

/**
 * Permet de créer le fichier de configuration
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class AbstractConfigurationCommand extends Command
{
    /**
     * Traits
     */
    use PasswordTrait;

    /**
     * @var array Datasources configurables
     */
    public static $datasources = ['default', 'test'];
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Défini un chemin inscriptible pour le fichier de configuration
     *
     * @return string
     */
    protected function configPath(): string
    {
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        $defaultPath = is_readable($pathToLocalConfig)
            ? include $pathToLocalConfig // NOSONAR include_once n'est pas pertinent dans ce cas
            : Configure::read(
                'App.paths.local_config',
                CONFIG . 'app_local.json'
            );
        if ($defaultPath === 1) {
            $defaultPath = Configure::read(
                'App.paths.local_config',
                CONFIG . 'app_local.json'
            );
        }
        do {
            $configPath = $this->io->ask(
                __d(
                    'configuration_shell',
                    "Indiquez le chemin qu'aura le fichier de configuration"
                ),
                $defaultPath
            );
            $writable = is_writable(
                file_exists($configPath) ? $configPath : dirname($configPath)
            );
            if (!$writable) {
                $this->io->err(
                    __d(
                        'configuration_shell',
                        "Le chemin indiqué n'est pas inscriptible"
                    )
                );
            }
        } while ($writable === false);
        return realpath($configPath)
            ?: realpath(dirname($configPath)) . DS . basename($configPath);
    }

    /**
     * Configuration de la base de données
     *
     * @param array $config
     */
    protected function configureDatabase(array &$config): void
    {
        $host = $username = $database = $driver = $password = null;
        extract((array)Hash::get($config, 'Datasources.default'));
        $default = include CONFIG . 'app_default.php'; // NOSONAR include_once n'est pas pertinent dans ce cas
        $connected = false;

        foreach (self::$datasources as $datasource) {
            $connected = false;
            do {
                $choice = $this->io->askChoice(
                    __d(
                        'configuration_shell',
                        "Voulez-vous configurer le datasource [{0}] ?",
                        $datasource
                    ),
                    ['y', 'n'],
                    'y'
                );
                if ($choice === 'n') {
                    break;
                }
                extract(
                    (array)Hash::get($config, 'Datasources.' . $datasource)
                );

                foreach (
                    [
                        'driver',
                        'host',
                        'username',
                        'password',
                        'database',
                    ] as $key
                ) {
                    $$key = $$key
                        ?: (string)Hash::get(
                            $default,
                            "Datasources.$datasource.$key"
                        );
                    $func = 'configureDatabase' . ucfirst($key);
                    $this->$func($datasource, $$key, $default);
                }

                ConnectionManager::drop($datasource);
                ConnectionManager::setConfig(
                    $datasource,
                    Hash::get($default, 'Datasources.' . $datasource) + [
                        'className' => 'Cake\Database\Connection',
                    ]
                );

                $connected = $this->checkConnection($datasource);
                if (!$connected) {
                    $this->io->err(
                        __d('configuration_shell', "Connection failed!")
                    );
                } else {
                    $this->io->success(
                        __d(
                            'configuration_shell',
                            "La connexion à la base de données a été effectué avec succès !"
                        ),
                        2
                    );
                }
            } while ($connected === false);

            if ($choice === 'y') {
                $config['Datasources'][$datasource]
                    = compact(
                        'driver',
                        'host',
                        'username',
                        'password',
                        'database'
                    );
            }
        }

        if ($connected || $this->checkConnection('default')) {
            $this->io->success(
                __d(
                    'configuration_shell',
                    "La connexion à la base de données a été effectué avec succès !"
                ),
                2
            );
        } else {
            $this->io->err(
                __d(
                    'configuration_shell',
                    "Impossible de se connecter à la base de données"
                )
            );
        }
    }

    /**
     * Vérifie que la base de données est accessible à partir des paramètres
     * saisis dans la configuration
     *
     * @param string $datasource
     * @return boolean
     */
    protected function checkConnection(string $datasource): bool
    {
        try {
            $conn = ConnectionManager::get($datasource);
            $conn->execute('select 1');
            return true;
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return false;
        }
    }

    /**
     * Configure la valeur "driver" du datasource défini
     *
     * @param string $datasource
     * @param string $driver
     * @param array  $config
     */
    protected function configureDatabaseDriver(
        string $datasource,
        string &$driver,
        array &$config
    ): void {
        do {
            $driver = $this->io->ask(
                __d('configuration_shell', "Database driver"),
                $driver ?: 'Cake\\Database\\Driver\\Postgres'
            );
        } while (!class_exists($driver));
        $config = Hash::insert(
            $config,
            "Datasources.$datasource.driver",
            $driver
        );
        echo PHP_EOL;
    }

    /**
     * Configure la valeur "host" du datasource défini
     *
     * @param string $datasource
     * @param string $host
     * @param array  $config
     */
    protected function configureDatabaseHost(
        string $datasource,
        string &$host,
        array &$config
    ): void {
        do {
            $host = $this->io->ask(
                __d('configuration_shell', "Database host"),
                $host ?: 'localhost'
            );
        } while (empty($host));
        $config = Hash::insert(
            $config,
            "Datasources.$datasource.host",
            $host
        );
        echo PHP_EOL;
    }

    /**
     * Configure la valeur "username" du datasource défini
     *
     * @param string $datasource
     * @param string $username
     * @param array  $config
     */
    protected function configureDatabaseUsername(
        string $datasource,
        string &$username,
        array &$config
    ): void {
        do {
            $username = $this->io->ask(
                __d('configuration_shell', "Database username"),
                $username ?: null
            );
        } while (empty($username));
        $config = Hash::insert(
            $config,
            "Datasources.$datasource.username",
            $username
        );
        echo PHP_EOL;
    }

    /**
     * Configure la valeur "password" du datasource défini
     *
     * @param string $datasource
     * @param string $password
     * @param array  $config
     * @throws Exception
     */
    protected function configureDatabasePassword(
        string $datasource,
        string &$password,
        array &$config
    ): void {
        do {
            $password = $this->enterPassword(
                __d('configuration_shell', "Database password")
            );
        } while (empty($password));
        $config = Hash::insert(
            $config,
            "Datasources.$datasource.password",
            $password
        );
        echo PHP_EOL;
    }

    /**
     * Configure la valeur "database" du datasource défini
     *
     * @param string $datasource
     * @param string $database
     * @param array  $config
     */
    protected function configureDatabaseDatabase(
        string $datasource,
        string &$database,
        array &$config
    ): void {
        do {
            $database = $this->io->ask(
                __d('configuration_shell', "Database name"),
                $database ?: null
            );
        } while (empty($database));
        $config = Hash::insert(
            $config,
            "Datasources.$datasource.database",
            $database
        );
        echo PHP_EOL;
    }

    /**
     * Configuration de la clé de sécurité
     *
     * @param array $config
     */
    protected function configureSalt(array &$config): void
    {
        if (!empty($config['Security']['salt'])) {
            $keep = $this->io->askChoice(
                __d(
                    'configuration_shell',
                    "Une clé de sécurité existe déjà, voulez-vous la conserver ?"
                ),
                ['y', 'n'],
                'y'
            );
            $confirm = 'y';
            if ($keep === 'n') {
                $confirm = $this->io->askChoice(
                    __d(
                        'configuration_shell',
                        "Il est vivement conseillé de conserver la clé, confirmez-vous sa suppression ?"
                    ),
                    ['y', 'n'],
                    'n'
                );
            }
            if ($keep === 'y' || $confirm === 'n') {
                $this->io->success(
                    __d(
                        'configuration_shell',
                        "La clé de sécurité a bien été conservée"
                    ),
                    2
                );
                return;
            }
        }

        while (empty($key) || !is_string($key)) {
            $options = $this->io->askChoice(
                __d(
                    'configuration_shell',
                    "Configuration de la clé de sécurité [(s)aisir, (g)énérer]"
                ),
                ['s', 'g'],  // (s)et or (g)enerate
                'g'
            );
            switch ($options) {
                case 's':
                    $key = $this->io->ask(
                        __d(
                            'configuration_shell',
                            "Veuillez rentrer une clé de sécurité :"
                        ),
                        Hash::get($config, 'Security.salt')
                    );
                    break;
                case 'g':
                    $key = hash('sha256', ROOT . php_uname() . microtime(true));
                    break;
            }
        }
        $config['Security']['salt'] = $key;
        $this->io->success(
            __d('configuration_shell', "La clé de sécurité a bien été définie"),
            2
        );
    }

    /**
     * Configuration du domaine
     *
     * @param array $config
     */
    protected function configureDomain(array &$config): void
    {
        stream_context_set_default(
            [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ],
            ]
        );
        do {
            $domain = $this->io->ask(
                __d(
                    'configuration_shell',
                    "Veuillez indiquer l'url complète de l'application (ex: https://asalae.fr)"
                ),
                Hash::get($config, 'App.fullBaseUrl')
            );
            $continue = false;
            $headers = @get_headers($domain);
            if (!$headers || $headers[0] === 'HTTP/1.1 404 Not Found') {
                $continue = true;
                $break = $this->io->askChoice(
                    __d(
                        'configuration_shell',
                        "L'url mentionnée n'a pas fonctionné. Voulez-vous quand même saisir cette valeur ?"
                    ),
                    ['y', 'n'],
                    'y'
                );
                if ($break === 'y') {
                    break;
                }
            }
        } while ($continue);
        $config['App']['fullBaseUrl'] = $domain;

        $this->io->success(
            __d('configuration_shell', "Le nom de domaine indiqué est valide"),
            2
        );
    }

    /**
     * Configuration de ratchet
     *
     * @param array $config
     */
    protected function configureRatchet(array &$config): void
    {
        if (!$default = Hash::get($config, 'Ratchet.connect')) {
            $p = parse_url(Hash::get($config, 'App.fullBaseUrl'));
            $default = sprintf('wss://%s/wss', $p['host'] ?? '');
        }
        $config['Ratchet']['connect'] = $this->io->ask(
            __d(
                'configuration_shell',
                "Veuillez indiquer le websocket de ratchet"
            ),
            $default
        );
    }

    /**
     * Configuration du chemin vers data (données de l'application ne
     * faisant pas partie des sources)
     *
     * @param array $config
     */
    protected function configureDataPath(array &$config): void
    {
        do {
            $configDataPath = $this->io->ask(
                __d(
                    'configuration_shell',
                    "Veuillez indiquer le chemin vers data"
                ),
                Hash::get($config, 'App.paths.data') ?: ROOT . DS . 'data'
            );
            $path = realpath($configDataPath);
            $writable = is_writable($path);
            if (!$writable) {
                $this->io->err(
                    __d(
                        'configuration_shell',
                        "Le chemin indiqué n'est pas inscriptible"
                    )
                );
            }
        } while ($writable === false);
        $config['App']['paths']['data'] = $configDataPath;
    }

    /**
     * Donne la config
     */
    protected function getConfig(): array
    {
        $pathToLocalConfig = $this->getConfigPath();
        if ($pathToLocalConfig) {
            return json_decode(file_get_contents($pathToLocalConfig), true);
        }
        return [];
    }

    /**
     * Donne le chemin vers le fichier de configuration
     * @return string
     */
    protected function getConfigPath(): string
    {
        return Config::getPathToLocal();
    }

    /**
     * Vérifi que la config est modifiable
     */
    protected function checkWritable(): void
    {
        $httpUser = $this->args->getOption('user');
        $currentUser = posix_getpwuid(posix_geteuid());
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');

        if ($currentUser['name'] !== $httpUser) {
            $this->io->abort(
                __d(
                    'configuration_shell',
                    "This script must be called by {0} but was called by {1}",
                    $httpUser,
                    $currentUser['name']
                )
            );
        }
        if (
            (!is_file($pathToLocalConfig) && !is_writable(
                dirname($pathToLocalConfig)
            ))
        ) {
            $this->io->abort(
                __(
                    "Le dossier config doit être inscriptible par {0} pour utiliser ce shell",
                    $currentUser['name']
                )
            );
        } elseif (
            is_file($pathToLocalConfig) && !is_writable(
                $pathToLocalConfig
            )
        ) {
            $this->io->abort(
                __(
                    "Le fichier config/path_to_local.php doit être inscriptible par {0} pour utiliser ce shell",
                    $currentUser['name']
                )
            );
        }
        $configPath = $this->getConfigPath();
        if (
            empty($configPath) || !is_file($configPath) || !is_writeable(
                $configPath
            )
        ) {
            $this->io->abort(
                __(
                    "Le fichier de configuration '{0}' n'existe pas ou n'est pas inscriptible par {1}",
                    $configPath,
                    $currentUser['name']
                )
            );
        }
    }
}
