<?php

/**
 * Asalae\Command\Configuration\CheckDatabaseCommand
 */

namespace Asalae\Command\Configuration;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

/**
 * Commande configuration check_database
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CheckDatabaseCommand extends AbstractConfigurationCommand
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'configuration check_database';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(__("vérifi la connection vers la base de données configuré"));
        return $parser;
    }

    /**
     * Vérifi la connection vers la base de données configuré
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int 0: success - 1: error
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        if ($this->checkConnection('default')) {
            $this->io->success('connected');
            return self::CODE_SUCCESS;
        } else {
            $this->io->err('not connected');
            return self::CODE_ERROR;
        }
    }
}
