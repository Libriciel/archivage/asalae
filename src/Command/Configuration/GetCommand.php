<?php

/**
 * Asalae\Command\Configuration\GetCommand
 */

namespace Asalae\Command\Configuration;

use AsalaeCore\Utility\Config;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Utility\Hash;
use Exception;

/**
 * Commande configuration get
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class GetCommand extends AbstractConfigurationCommand
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'configuration get';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(__("Visualise la valeur d'une clé"));
        $parser->addArgument(
            'path',
            [
                'help' => __(
                    "Chemin de la clé de configuration. ex: App.paths.data"
                ),
                'required' => true,
            ]
        );
        $parser->addOption(
            'raw',
            [
                'help' => __(
                    "La valeur n'est pas converti en json (si possible)"
                ),
                'boolean' => true,
            ]
        );
        return $parser;
    }

    /**
     * Affiche le contenu d'une clé de configuration
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return void
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $config = $this->getConfig();
        $value = Hash::get(Hash::merge(Config::readAll(), $config), $args->getArgument('path'));

        if ($this->args->getOption('raw') && (is_scalar($value) || is_null($value))) {
            $this->io->out($value);
        } else {
            $this->io->out(
                json_encode(
                    $value,
                    JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
                )
            );
        }
    }
}
