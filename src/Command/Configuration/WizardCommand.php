<?php

/**
 * Asalae\Command\Configuration\WizardCommand
 */

namespace Asalae\Command\Configuration;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\I18n\I18n;
use Exception;

/**
 * Commande configuration wizard (configuration interractive)
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class WizardCommand extends AbstractConfigurationCommand
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'configuration wizard';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $apacheUser = exec('apachectl -S 2>/dev/null | grep User | sed \'s/.*name="\([^"]*\)".*/\1/\'')
            ?: 'www-data';

        $parser->addOption(
            'user',
            [
                'short' => 'u',
                'help' => __(
                    "L'utilisateur système http ex: pour apache2: {0}",
                    $apacheUser
                ),
                'default' => $apacheUser,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $this->checkWritable();
        $local = $this->io->ask(
            __d('configuration_shell', "Please select a locale"),
            'fr_FR'
        );
        I18n::setLocale($local);
        $this->io->nl();

        $config = [];
        $configPath = $this->configPath();

        $exist = file_exists($configPath);

        if ($exist) {
            $this->io->success(
                __d(
                    'configuration_shell',
                    "Le fichier existe déjà, les valeurs par défaut seront"
                    . " définies en fonction du fichier existant "
                    . "et les valeurs non configurables ici seront conservées."
                ),
                2
            );
            $config = json_decode(file_get_contents($configPath), true);
        }

        $config['App']['defaultLocale'] = $local;

        $this->configureDataPath($config);
        $this->configureDomain($config);
        $this->configureDatabase($config);
        $this->configureSalt($config);
        $this->configureRatchet($config);

        ksort($config);
        $this->io->createFile(
            $configPath,
            json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
        );
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        $this->io->createFile($pathToLocalConfig, "<?php return '$configPath'; ?>");
    }
}
