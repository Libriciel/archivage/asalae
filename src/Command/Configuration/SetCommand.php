<?php

/**
 * Asalae\Command\Configuration\SetCommand
 */

namespace Asalae\Command\Configuration;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Commande configuration set
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SetCommand extends AbstractConfigurationCommand
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'configuration set';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(__("Lance le shell en mode injection de configuration"));
        $apacheUser = exec('apachectl -S 2>/dev/null | grep User | sed \'s/.*name="\([^"]*\)".*/\1/\'')
            ?: 'www-data';

        $parser->addArgument(
            'path',
            [
                'help' => __("Chemin de la clé de configuration. ex: App.paths.data"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'value',
            [
                'help' => __(
                    "Valeur à insérer. "
                    . "ex: 'true' = (bool)1, 'un mot' = (string)'un mot', '3.14' = (float)3.14"
                ),
                'required' => true,
            ]
        );
        $parser->addOption(
            'user',
            [
                'short' => 'u',
                'help' => __("L'utilisateur système http ex: pour apache2: {0}", $apacheUser),
                'default' => $apacheUser,
            ]
        );
        $parser->addOption(
            'type',
            [
                'help' => __("Type de la valeur à insérer (cast)"),
                'default' => 'auto',
                'choices' => [
                    'auto',
                    'string',
                    'int',
                    'integer',
                    'float',
                    'double',
                    'bool',
                    'boolean',
                    'array',
                    'null',
                ],
            ]
        );
        return $parser;
    }

    /**
     * Défini une valeur sur une clé
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return void
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $path = $args->getArgument('path');
        $value = $args->getArgument('value');
        $config = $this->getConfig();
        $this->checkWritable();
        $value = $this->castValue($value, $args->getOption('type'));

        $this->io->out(
            __(
                "Précédente valeur: \n{0}",
                json_encode(
                    Hash::insert([], $path, Hash::get($config, $path)),
                    JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
                )
            )
        );
        $config = Hash::insert($config, $path, $value);
        $this->io->out(
            __(
                "Nouvelle valeur: \n{0}",
                json_encode(Hash::insert([], $path, $value), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
            )
        );

        ksort($config);
        $configPath = $this->getConfigPath();
        $json = json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        if (!$json) {
            $this->io->abort(__("Erreur lors du parsing JSON"));
        }
        Filesystem::begin();
        Filesystem::dumpFile($configPath, $json);
        Filesystem::commit();
    }

    /**
     * Casts the given value to the specified type.
     *
     * @param string|null $value The value to be cast, which can be null or a string.
     * @param string      $type  The target type to which the value should be cast.
     *                           Supported types include 'auto', 'string', 'int', or 'integer'.
     * @return mixed The value cast to the specified type. This could be a string, integer, float, boolean, or null.
     */
    private function castValue(?string $value, string $type)
    {
        if ($type === 'auto') {
            if ($value === 'true') {
                $value = true;
            } elseif ($value === 'false') {
                $value = false;
            } elseif ($value === 'null') {
                $value = null;
            } elseif (preg_match('/^\d+\.\d*$/', $value)) {
                $value = (float)$value;
            } elseif (is_numeric($value)) {
                $value = (int)$value;
            }
        } elseif ($type === 'string') {
            $value = (string)$value;
        } elseif ($type === 'int' || $type === 'integer') {
            $value = (int)$value;
        } elseif ($type === 'float' || $type === 'double') {
            $value = (float)$value;
        } elseif ($type === 'bool' || $type === 'boolean') {
            $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        } elseif ($type === 'array') {
            if (is_string($value)) {
                $decoded = json_decode($value, true);
                $value = is_array($decoded) ? $decoded : [$value];
            } else {
                $value = [$value];
            }
        } elseif ($type === 'null') {
            $value = null;
        }
        return $value;
    }
}
