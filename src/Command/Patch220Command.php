<?php

/**
 * Asalae\Command\Patch220Command
 */

namespace Asalae\Command;

use Asalae\Model\Table\TechnicalArchivesTable;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;

/**
 * Récupère les métadonnées manquante en base
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Patch220Command extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'patch220';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __("Récupère les métadonnées manquante en base")
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $commonConditions = ['EventLogs.archival_agency_id IS' => null, 'EventLogs.user_id IS' => null];
        $secondQuery = $EventLogs->find()
            ->select(['EventLogs.id', 'archival_agency_id' => 'Archives.archival_agency_id'])
            ->innerJoinWith('Archives')
            ->where($commonConditions);
        $thirdQuery = $EventLogs->find()
            ->select(['EventLogs.id', 'archival_agency_id' => 'Archives.archival_agency_id'])
            ->innerJoinWith('ArchiveFiles.Archives')
            ->where($commonConditions);
        $fourthQuery = $EventLogs->find()
            ->select(['EventLogs.id', 'archival_agency_id' => 'Archives.archival_agency_id'])
            ->innerJoinWith('ArchiveBinaries.ArchiveUnits.Archives')
            ->where($commonConditions);
        $fifthQuery = $EventLogs->find()
            ->select(['EventLogs.id', 'archival_agency_id' => 'Transfers.archival_agency_id'])
            ->innerJoinWith('Transfers')
            ->where($commonConditions);
        $query = $EventLogs->find()
            ->select(['EventLogs.id', 'archival_agency_id' => 'ArchivalAgencies.id'])
            ->innerJoinWith('Useragents.OrgEntities.ArchivalAgencies')
            ->where(['EventLogs.archival_agency_id IS' => null])
            ->union($secondQuery)
            ->union($thirdQuery)
            ->union($fourthQuery)
            ->union($fifthQuery);
        $count = $query->count();
        if ($count) {
            $io->out(__("Mise à jour de {0} événements...", $count));
            foreach ($query as $eventLog) {
                $archival_agency_id = Hash::get($eventLog, 'archival_agency_id');
                if ($archival_agency_id) {
                    $EventLogs->updateAll(
                        ['archival_agency_id' => $archival_agency_id],
                        ['id' => $eventLog->id]
                    );
                }
            }
        }

        /** @var TechnicalArchivesTable $TechnicalArchives */
        $TechnicalArchives = TableRegistry::getTableLocator()->get('TechnicalArchives');
        $query = $TechnicalArchives->find()
            ->where(['name LIKE' => 'Journaux des événements de as_lae%']);
        $count = $query->count();
        if ($count) {
            $io->out(__("Mise à jour de {0} archives techniques...", $count));
            foreach ($query as $technicalArchive) {
                if (preg_match('/\d{4}$/', $technicalArchive->get('name'), $m)) {
                    $TechnicalArchives->updateAll(
                        ['name' => __("Journaux des événements : {0}", $m[0])],
                        ['id' => $technicalArchive->id]
                    );
                }
            }
        }

        $TechnicalArchives->updateAll(
            ['state' => TechnicalArchivesTable::S_AVAILABLE],
            []
        );

        $io->success('done');
    }
}
