<?php

/**
 * Asalae\Command\InstallConfigCommand
 */

namespace Asalae\Command;

use Asalae\Form\InstallConfigForm;
use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Utility\FormatError;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Utility\Hash;
use Exception;

/**
 * installation de Asalae 2 via fichier ini
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class InstallConfigCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'install config';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();
        $parser->addArgument(
            'ini',
            [
                'help' => __("Fichier ini de configuration"),
            ]
        );

        $parser->addOption(
            'overwrite',
            [
                'help' => __(
                    "réécriture des fichiers déjà existants (horodatage et profils d'archives)"
                ),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'headless',
            [
                'help' => __("Pas d'interactivité (pas d'overwrite des fichiers déjà existants)"),
                'boolean' => true,
            ]
        );

        $parser->addOption(
            'force',
            [
                'help' => __("Ignore la validation"),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'postinstall',
            [
                'help' => __("Permet d'ajouter des éléments suite à une installation"),
                'boolean' => true,
            ]
        );

        return $parser;
    }

    /**
     * Commande d'installation via fichier de conf
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $filename = $args->getArgument('ini');
        if (!is_file($filename)) {
            $io->abort('file not found');
        }
        $config = parse_ini_file($filename, true);
        if (!$config) {
            $io->abort('parse error');
        }
        $form = new InstallConfigForm();
        $form->stdOut = [$io, 'out'];
        $form->postInstall = $args->getOption('postinstall');
        $form->overwrite = $args->getOption('overwrite');
        $form->headless = $args->getOption('headless');
        $data = Hash::flatten($config, '__');
        $form->setData($data);
        if ($form->execute($data)) {
            $io->success(__("Installation terminée"));
        } elseif ($args->getOption('force') || $args->getOption('postinstall')) {
            $io->err(FormatError::formErrors($form));
            $form->force($data);
        } else {
            $io->err(FormatError::formErrors($form));
            $io->abort('validation error');
        }
    }
}
