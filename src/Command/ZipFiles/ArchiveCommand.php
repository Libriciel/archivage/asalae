<?php

/**
 * Asalae\Command\ZipFiles\ArchiveCommand
 */

namespace Asalae\Command\ZipFiles;

use Asalae\Model\Entity\Archive;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Datacompressor\Exception\DataCompressorException;
use ZMQSocketException;

/**
 * Génère un zip d'archive
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveCommand extends AbstractZipFilesCommand
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var string $lock
     */
    protected $lock;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'zip_files archive';
    }

    /**
     * Display help for this console.
     * @return ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(
            __("Zip les fichiers d'une archive")
        );
        $parser->addArgument(
            'archive_id',
            [
                'help' => __("Id de l'archive"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'user_id',
            [
                'help' => __(
                    "Id de l'user requérant le lien de téléchargement"
                ),
                'required' => true,
            ]
        );
        $parser->addOption(
            'no-mail',
            [
                'help' => __("N'enverra pas d'email"),
                'flag' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     * @throws DataCompressorException
     * @throws ZMQSocketException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $archive_id = $this->args->getArgument('archive_id');
        $user_id = $this->args->getArgument('user_id');
        $this->lock(
            Configure::read('App.paths.download', TMP . 'download')
            . DS . 'archive_' . $archive_id . '.lock'
        );
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        /** @var Archive $archive */
        $archive = $Archives->find()
            ->contain(
                [
                    'ArchiveFiles' => ['StoredFiles'],
                    'DescriptionXmlArchiveFiles',
                ]
            )
            ->where(['Archives.id' => $archive_id])
            ->firstOrFail();
        $readmePath = $archive->getZipTempFolder() . 'README.txt';
        if (file_exists($readmePath)) {
            $filename = $archive->getZipDownloadFolder()
                . $archive_id . '_mail_request.txt';
            $handle = fopen($filename, 'a');
            fwrite($handle, "$archive_id\n");
            fclose($handle);
            $this->io->out('success with mail');
            $this->throwStop();
        }
        $url = Configure::read('App.fullBaseUrl')
            . Router::url('/Archives/download-files/' . $archive_id);
        $success = $archive->makeZip();
        $name = __("l'archive {0}", $archive->get('archival_agency_identifier'));
        $this->notify($success, $name, $url, $user_id);
        $this->sendEmailAfterZipCreation(
            $user_id,
            $archive,
            [
                'success' => $success,
                'link' => $url,
                'subject' => __("Archive disponible au téléchargement"),
                'titleArg' => __("d'une archive"),
                'pArg' => __("d'archive"),
            ]
        );
        $this->io->out('success');
    }
}
