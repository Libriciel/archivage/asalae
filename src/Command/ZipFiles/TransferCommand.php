<?php

/**
 * Asalae\Command\ZipFiles\TransferCommand
 */

namespace Asalae\Command\ZipFiles;

use Asalae\Model\Entity\Transfer;
use AsalaeCore\Form\AbstractGenericMessageForm;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use ZMQSocketException;

/**
 *
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TransferCommand extends AbstractZipFilesCommand
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var string $lock
     */
    protected $lock;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'zip_files transfer';
    }

    /**
     * Display help for this console.
     * @return ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(
            __("Zip les fichiers d'un transfert")
        );
        $parser->addArgument(
            'transfer_id',
            [
                'help' => __("Id du transfert"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'user_id',
            [
                'help' => __(
                    "Id de l'user requérant le lien de téléchargement"
                ),
                'required' => true,
            ]
        );
        $parser->addOption(
            'no-mail',
            [
                'help' => __("N'enverra pas d'email"),
                'flag' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws ZMQSocketException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $transfer_id = $this->args->getArgument('transfer_id');
        $user_id = $this->args->getArgument('user_id');
        $this->lock(
            Configure::read('App.paths.download', TMP . 'download')
            . DS . 'transfer_' . $transfer_id . '.lock'
        );
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        /** @var Transfer $transfer */
        $transfer = $Transfers->find()
            ->where(
                [
                    'Transfers.id' => $transfer_id,
                    'Transfers.files_deleted IS NOT' => true,
                ]
            )
            ->firstOrFail();
        $readmePath = AbstractGenericMessageForm::getDataDirectory($transfer_id)
            . DS . 'attachments'
            . DS . 'README.txt';
        if (file_exists($readmePath)) {
            $filename = $transfer->getZipDownloadFolder()
                . $transfer_id . '_mail_request.txt';
            $handle = fopen($filename, 'a');
            fwrite($handle, "$transfer_id\n");
            fclose($handle);
            $this->io->out('success with mail');
            $this->throwStop();
        }

        $url = Configure::read('App.fullBaseUrl')
            . Router::url('/Transfers/download-files/' . $transfer_id);
        $success = $transfer->makeZip();
        $name = __("le transfert {0}", $transfer->get('transfer_identifier'));
        $this->notify($success, $name, $url, $user_id);
        $this->sendEmailAfterZipCreation(
            $user_id,
            $transfer,
            [
                'success' => $success,
                'link' => Configure::read('App.fullBaseUrl')
                    . '/Transfers/download-files/'
                    . $transfer_id,
                'subject' => __("Transfert disponible au téléchargement"),
                'titleArg' => __("d'un transfert"),
                'pArg' => __("d'un transfert"),
            ]
        );
        $this->io->out('success');
    }
}
