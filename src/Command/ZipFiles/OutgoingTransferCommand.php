<?php

/**
 * Asalae\Command\ZipFiles\OutgoingTransferCommand
 */

namespace Asalae\Command\ZipFiles;

use Asalae\Model\Entity\OutgoingTransfer;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Génère un zip de transfert sortant
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OutgoingTransferCommand extends AbstractZipFilesCommand
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var string $lock
     */
    protected $lock;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'zip_files outgoing_transfer';
    }

    /**
     * Display help for this console.
     * @return ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(
            __("Zip les fichiers d'un transfert sortant")
        );
        $parser->addArgument(
            'outgoing_transfer_id',
            [
                'help' => __("Id du transfert sortant"),
                'required' => true,
            ]
        );
        $parser->addOption(
            'no-mail',
            [
                'help' => __("N'enverra pas d'email"),
                'flag' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $outgoing_transfer_id = $this->args->getArgument('outgoing_transfer_id');
        $this->lock(
            Configure::read('App.paths.download', TMP . 'download')
            . DS . 'outgoing_transfer_' . $outgoing_transfer_id . '.lock'
        );
        $OutgoingTransfers = TableRegistry::getTableLocator()->get(
            'OutgoingTransfers'
        );
        /** @var OutgoingTransfer $outgoingTransfer */
        $outgoingTransfer = $OutgoingTransfers->get($outgoing_transfer_id);
        if ($outgoingTransfer->makeZip()) {
            $this->io->out('success');
        } else {
            $this->io->err('zip failed');
        }
    }
}
