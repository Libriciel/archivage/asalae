<?php

/**
 * Asalae\Command\ZipFiles\ArchiveUnitCommand
 */

namespace Asalae\Command\ZipFiles;

use Asalae\Model\Entity\ArchiveUnit;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Datacompressor\Exception\DataCompressorException;
use ZMQSocketException;

/**
 * Génère un zip d'unité d'archives
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveUnitCommand extends AbstractZipFilesCommand
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var string $lock
     */
    protected $lock;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'zip_files archive_unit';
    }

    /**
     * Display help for this console.
     * @return ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(
            __("Zip les fichiers d'une unité d'archives")
        );
        $parser->addArgument(
            'archive_unit_id',
            [
                'help' => __("Id de l'unité d'archives"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'user_id',
            [
                'help' => __(
                    "Id de l'user requérant le lien de téléchargement"
                ),
                'required' => true,
            ]
        );
        $parser->addOption(
            'no-mail',
            [
                'help' => __("N'enverra pas d'email"),
                'flag' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     * @throws DataCompressorException
     * @throws ZMQSocketException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $archive_unit_id = $this->args->getArgument('archive_unit_id');
        $user_id = $this->args->getArgument('user_id');
        $this->lock(
            Configure::read('App.paths.download', TMP . 'download')
            . DS . 'archive_unit_' . $archive_unit_id . '.lock'
        );
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        /** @var ArchiveUnit $archiveUnit */
        $archiveUnit = $ArchiveUnits->find()
            ->contain(
                [
                    'Archives' => [
                        'ArchiveFiles' => ['StoredFiles'],
                        'DescriptionXmlArchiveFiles',
                    ],
                ]
            )
            ->where(['ArchiveUnits.id' => $archive_unit_id])
            ->firstOrFail();
        $readmePath = $archiveUnit->getZipTempFolder() . 'README.txt';
        if (file_exists($readmePath)) {
            $filename = $archiveUnit->getZipDownloadFolder()
                . $archive_unit_id . '_mail_request.txt';
            $handle = fopen($filename, 'a');
            fwrite($handle, "$archive_unit_id\n");
            fclose($handle);
            $this->io->out('success with mail');
            $this->throwStop();
        }

        $url = Configure::read('App.fullBaseUrl')
            . Router::url('/ArchiveUnits/download-files/' . $archive_unit_id);
        $success = $archiveUnit->makeZip();
        $name = __("l'unité d'archive {0}", $archiveUnit->get('archival_agency_identifier'));
        $this->notify($success, $name, $url, $user_id);
        $this->sendEmailAfterZipCreation(
            $user_id,
            $archiveUnit,
            [
                'success' => $success,
                'link' => $url,
                'subject' => __(
                    "Unité d'archives disponible au téléchargement"
                ),
                'titleArg' => __("d'une unité d'archives"),
                'pArg' => __("d'unité d'archives"),
            ]
        );
        if ($success) {
            $this->io->out('success');
        } else {
            $this->io->err('failed');
        }
    }
}
