<?php

/**
 * Asalae\Command\ZipFiles\AbstractZipFilesCommand
 */

namespace Asalae\Command\ZipFiles;

use Asalae\Model\Entity\Configuration;
use Asalae\Model\Entity\User;
use Asalae\Model\Entity\ZippableEntityInterface;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\MailsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\Notify;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\Exception\StopException;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateTime;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use ZMQSocketException;

/**
 * Shell de création de zip des fichiers d'une archive ou transfert donné(e)
 * (pour téléchargement asynchrone)
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class AbstractZipFilesCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var string $lock
     */
    protected $lock;

    /**
     * Supprime le fichier lock
     */
    public function __destruct()
    {
        if ($this->lock && is_file($this->lock)) {
            unlink($this->lock);
        }
    }

    /**
     * Envoi du mail
     * @param int                                     $user_id
     * @param ZippableEntityInterface|EntityInterface $entity
     * @param array                                   $vars
     * @throws Exception
     */
    protected function sendEmailAfterZipCreation(
        $user_id,
        ZippableEntityInterface $entity,
        array $vars = []
    ) {
        if ($this->args->getOption('no-mail') !== null) {
            return;
        }
        $loc = TableRegistry::getTableLocator();
        /** @var User $user */
        $user = $loc->get('Users')->find()
            ->select(['id', 'email'])
            ->contain(['Roles', 'OrgEntities'])
            ->where(['Users.id' => $user_id])
            ->firstOrFail();

        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $sa = $OrgEntities->getUserSA($user->get('id'));
        /** @var Configuration|null $conf */
        $conf = TableRegistry::getTableLocator()->get('Configurations')
            ->find()
            ->select(['setting'])
            ->where(
                [
                    'org_entity_id' => $sa->get('id'),
                    'name' => ConfigurationsTable::CONF_MAIL_TITLE_PREFIX,
                ]
            )
            ->first();

        $prefix = $conf ? $conf->get('setting') : '[asalae]';
        $userMails = [$user->get('email')];
        $filename = $entity->getZipDownloadFolder()
            . $entity->get('id') . '_mail_request.txt';

        if (is_file($filename)) {
            $mailRequests = array_unique(
                array_merge(
                    [$user->id],
                    file(
                        $filename,
                        FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES
                    )
                )
            );
            /** @var User[] $users */
            $users = TableRegistry::getTableLocator()->get('Users')->find()
                ->select(['email'])
                ->where(
                    [
                        'Users.id IN' => $mailRequests,
                        'Users.email IS NOT' => null,
                    ]
                )
                ->disableHydration()
                ->all()
                ->toArray();
            $userMails = array_unique(
                array_merge($userMails, Hash::extract($users, '{n}.email'))
            );
            Filesystem::remove($filename);
        }

        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $email->viewBuilder()
            ->setTemplate('zip_file_available')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);

        $timeLimit = Configure::read('Downloads.tempFileConservationTimeLimit');
        $expiration = time() + ($timeLimit * 3600);

        $email->setEmailFormat('both')
            ->setSubject($prefix . ($vars['subject'] ?? ''))
            ->setViewVars(
                $vars + [
                    'expiration' => (new DateTime())
                        ->setTimestamp($expiration)
                        ->format('Y-m-d H:i:s'),
                ]
            )
            ->setTo($userMails);
        /** @var MailsTable $Mails */
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        $Mails->asyncMail($email, $user_id);
    }

    /**
     * gestion du lock fichier (supprimé lors du __destruct())
     * @param string $uploadLock
     * @throws StopException si un lock existe déjà
     */
    protected function lock(string $uploadLock)
    {
        if (!is_dir(dirname($uploadLock))) {
            mkdir(dirname($uploadLock), 0777, true);
        }
        if (is_file($uploadLock)) {
            $pid = (int)file_get_contents($uploadLock);
            $ps = exec(sprintf('ps -p %d', $pid));
            $running = preg_match('/^\s*(\d+)/', $ps, $m)
                && (int)$m[1] === $pid;
            if ($running) {
                $this->io->abort(
                    __(
                        "Un ZipFiles semble être déjà en cours (PID={0})",
                        $pid
                    )
                );
            }
        }
        file_put_contents($uploadLock, getmypid());
        $this->lock = $uploadLock;
    }

    /**
     * Envoi une notification à l'utilisateur
     * @param bool   $success
     * @param string $name
     * @param string $url
     * @param int    $user_id
     * @return void
     * @throws ZMQSocketException
     */
    protected function notify(bool $success, string $name, string $url, int $user_id)
    {
        if ($success) {
            $message = __(
                "Le zip pour {0} a été créé, il est téléchargeable depuis"
                . " le lien suivant : {1}",
                $name,
                '<a href="' . $url . '" target="_blank" download>'
                . __("Téléchargement du zip")
                . '</a>'
            );
        } else {
            $message = __(
                "Un problème a eu lieu lors de la création du zip {0}",
                $name
            );
        }
        Notify::send(
            $user_id,
            [],
            $message,
            $success ? 'alert-success' : 'alert-danger'
        );
    }

    /**
     * Remplace l'ancien _stop()
     * @return void
     */
    protected function throwStop(): void
    {
        throw new StopException('Halting error reached', self::CODE_SUCCESS);
    }
}
