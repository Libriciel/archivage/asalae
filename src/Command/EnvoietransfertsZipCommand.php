<?php

/**
 * Asalae\Command\EnvoietransfertsZipCommand
 */

namespace Asalae\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\PreControlMessages;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Utility\Text;
use DateMalformedStringException;
use DateTime;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Envoi un xml + zip déjà préparé
 *
 * @category    Command
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EnvoietransfertsZipCommand extends EnvoietransfertsCommand
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'envoietransferts zip';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserZip = new ConsoleOptionParser();
        $parserZip->setDescription(
            __d(
                'envoietransferts_shell',
                "Se base sur un xml et un zip pour envoyer un transfert"
            )
        );
        $parserZip->addArgument(
            'xml',
            [
                'help' => __d(
                    'envoietransferts_shell',
                    "Chemin vers le bordereau XML"
                ),
                'required' => true,
            ]
        );
        $parserZip->addArgument(
            'zip',
            [
                'help' => __d(
                    'envoietransferts_shell',
                    "Chemin vers le fichier zip"
                ),
                'required' => true,
            ]
        );
        $parserZip->addOption(
            'repeat',
            [
                'help' => __d(
                    'envoietransferts_shell',
                    "Repéter X fois"
                ),
                'default' => '1',
            ]
        );
        $parserZip->addOption(
            'wslogin',
            [
                'short' => 'l',
                'help' => __d(
                    'envoietransferts_shell',
                    "identifiant de connexion pour les webservices"
                ),
                'default' => 'admin',
            ]
        );
        $parserZip->addOption(
            'wspasswd',
            [
                'short' => 'p',
                'password' => true,
                'help' => __d(
                    'envoietransferts_shell',
                    "mot de passe de connexion pour les webservices"
                ),
                'default' => 'admin',
            ]
        );
        $parserZip->addOption(
            'url',
            [
                'short' => 'u',
                'help' => __d(
                    'envoietransferts_shell',
                    "url de base de asalae (ex: https://www.asalae.org)"
                ),
                'default' => Configure::read('App.fullBaseUrl'),
            ]
        );
        $parserZip->addOption(
            'target-url',
            [
                'short' => 't',
                'help' => __d(
                    'envoietransferts_shell',
                    "chemin url cible (default: {0})",
                    self::TARGET_URL
                ),
                'default' => self::TARGET_URL,
            ]
        );
        return $parserZip;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws GuzzleException
     * @throws DateMalformedStringException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $xml = $args->getArgument('xml');
        $zip = $args->getArgument('zip');

        $amount = (int)$this->args->getOption('repeat');
        /** @var Client $client */
        $client = Utility::get(
            Client::class,
            [
                'base_uri' => $this->args->getOption('url'),
                'auth' => [$this->args->getOption('wslogin'), $this->args->getOption('wspasswd')],
                'verify' => false, // certificats autosigné
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]
        );
        $util = DOMUtility::load($xml);
        $filename = basename($xml);
        $precontrol = new PreControlMessages($xml);
        $path = $precontrol->identifierIdPaths[$util->namespace];
        $identifier = $util->xpath->query($path)->item(0);
        $target = $this->args->getOption('target-url');
        for ($i = 0; $i < $amount; $i++) {
            $begin = microtime(true);
            $this->io->out(__d('envoietransferts_shell', "Transfert #{0}", $i + 1));
            $ziphandle = fopen($zip, 'r');
            try {
                $identifier->nodeValue = Text::uuid();
                $response = $client->request(
                    'POST',
                    $target,
                    [
                        'headers' => [
                            'Accept' => 'application/json',
                            'X-Requested-With' => 'asalae2',
                        ],
                        'multipart' => [
                            [
                                'name' => 'seda_message',
                                'filename' => $filename,
                                'contents' => $util->dom->saveXML(),
                            ],
                            [
                                'name' => 'attachments',
                                'contents' => $ziphandle,
                            ],
                        ],
                    ]
                );
                $body = $response->getBody();
                $body->rewind();
                $raw = $body->getContents();
                $data = json_decode($raw);
                if (isset($data->date_du_depot) && $data->date_du_depot) {
                    $result = __("oui");
                } else {
                    $result = __("non");
                    if (isset($data->errors)) {
                        $result .= ' ' . implode(', ', $data->errors);
                    }
                    if (Configure::read('debug')) {
                        debug($response->getStatusCode() . ' : ' . $raw);
                    }
                }
                $time = (new DateTime(
                    '@' . (int)(microtime(true) - $begin)
                ))->format('H:i:s');
                $this->io->out(
                    '  ' . __d(
                        'envoietransferts_shell',
                        "réussite du transfert : {0} - en {1}",
                        $result,
                        $time
                    )
                );
            } catch (ClientException $e) {
                if (is_resource($ziphandle)) {
                    fclose($ziphandle);
                }
                $this->handleClientException($e, $target);
                throw new Exception();
            } finally {
                if (is_resource($ziphandle)) {
                    fclose($ziphandle);
                }
            }
        }
        if (isset($ziphandle) && is_resource($ziphandle)) {
            fclose($ziphandle);
        }
    }
}
