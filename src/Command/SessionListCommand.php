<?php

/**
 * Asalae\Command\SessionListCommand
 */

namespace Asalae\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\I18n\Date as CakeDate;
use DateTimeInterface;

/**
 * Liste des sessions utilisateurs
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SessionListCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'session list';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(__("Liste les sessions utilisateurs"));
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $Sessions = $this->fetchTable('Sessions');
        $fields = ['id', 'created', 'modified', 'username', 'name'];
        $data = $Sessions->find()
            ->select(
                [
                    'id' => 'Sessions.id',
                    'created' => 'Sessions.created',
                    'modified' => 'Sessions.modified',
                    'username' => 'Users.username',
                    'name' => 'Users.name',
                ]
            )
            ->contain(['Users'])
            ->where(['Sessions.user_id IS NOT' => null])
            ->orderBy(['Users.username' => 'asc', 'Sessions.modified' => 'desc'])
            ->disableHydration()
            ->all()
            ->map(
                function ($data) {
                    foreach ($data as $k => $v) {
                        if (($v instanceof DateTimeInterface || $v instanceof CakeDate)) {
                            $data[$k] = $v->format('Y-m-d H:i:s');
                        }
                    }
                    return $data;
                }
            )
            ->toArray();
        array_unshift($data, $fields);

        $this->io->helper('Table')->output($data);
    }
}
