<?php

/**
 * Asalae\Command\CalcCountSizeArchivesCommand
 */

namespace Asalae\Command;

use Asalae\Model\Table\ArchivesTable;
use AsalaeCore\Exception\GenericException;
use Cake\Command\Command;
use Cake\Command\Helper\ProgressHelper;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Error\Debugger;
use Cake\ORM\Table;
use Exception;

/**
 * Recalcule les count et size liés aux archives
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CalcCountSizeArchivesCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    public $io;
    /**
     * @var Arguments
     */
    public $args;

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __("Recalcule les count et size liés aux archives")
        );
        $parser->addOption(
            'conditions-json',
            [
                'help' => __("Conditions au format json"),
            ]
        );
        $parser->addOption(
            'input-json',
            [
                'help' => __(
                    "Permet la saisie de json sans avoir à échapper les caractères"
                ),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'datasource',
            [
                'help' => __("Datasource utilisé"),
                'default' => 'default',
            ]
        );
        $parser->addOption(
            'headless',
            [
                'help' => __("Pas d'interactivité"),
                'boolean' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->args = $args;
        $this->io = $io;
        $opts = ['connectionName' => $args->getOption('datasource')];
        $json = $args->getOption('conditions-json');
        if (!$json && $args->getOption('headless')) {
            $io->abort(__("Le mode headless doit être lancé avec un --conditions-json valide"));
        }
        if (!$json && $args->getOption('input-json')) {
            $json = $io->ask(
                __("Veuillez saisir les conditions sous la forme json")
            );
        }
        if ($json) {
            $conditions = json_decode($json, true);
            $io->out(__("Conditions: {0}", Debugger::exportVar($conditions)));
            if (!$conditions) {
                throw new GenericException('Failed to parse json');
            }
        } else {
            $conditions = [];
            $continue = false;
            $io->out(
                __(
                    "Les conditions doivent êtres saisies sous la forme (Model.field <operator?>) (value)"
                )
            );
            while (!$conditions || $continue) {
                $key = $io->ask(
                    __(
                        "Veuillez saisir un champ pour la condition (ex: Archives.state)"
                    )
                );
                $value = $io->ask(
                    __(
                        "Veuillez saisir une valeur pour la condition (ex: Archives.state)"
                    )
                );
                if ($key) {
                    $this->checkConditions([$key => $value]);
                    $conditions[$key] = $value;
                }
                $io->out(
                    __("Conditions: {0}", Debugger::exportVar($conditions))
                );
                $continue = $io->askChoice(
                    __("Ajouter une autre condition ?"),
                    ['y', 'n'],
                    'n'
                ) === 'y';
            }
        }
        $io->info(
            __(
                "Rappeler cette commande :\n{0}",
                'bin/cake ' . self::defaultName() . ' --conditions-json '
                . escapeshellarg(json_encode($conditions, JSON_UNESCAPED_SLASHES))
            )
        );
        $this->checkConditions($conditions);
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives', $opts);
        $q = $Archives->find();
        $query = $q
            ->leftJoinWith('Transfers')
            ->where($conditions)
            ->contain(
                [
                    'Transfers',
                ]
            );
        $count = $query->count();
        $io->out(__("Nombre d'archives : {0}", $count));
        if ($count === 0) {
            return;
        }
        /** @var ProgressHelper $progress */
        $progress = $io->helper('Progress');
        $progress->init(['total' => $count]);

        foreach ($query as $archive) {
            $progress->increment();
            $progress->draw();
            $Archives->updateCountSize([$archive]);
        }
        $progress->draw();

        $io->out();
        $io->success('done');
    }

    /**
     * Vérifi que le model et le field des conditions existent
     * @param array $conditions
     * @return array
     */
    private function checkConditions(array $conditions): array
    {
        $models = [];
        $opts = ['connectionName' => $this->args->getOption('datasource')];
        foreach ($conditions as $key => $values) {
            if (strtoupper($key) === 'OR') {
                $this->checkConditions($values);
            } elseif (strpos($key, '.') === false) {
                $this->io->abort(
                    __("Les conditions doivent être sous la forme Model.field")
                );
            } elseif (preg_match('/^(\w+)\.(\w+)(?: \w)?$/', $key, $m)) {
                /** @var Table $Model */
                $Model = $this->fetchTable($m[1], $opts);
                if (!$Model->getSchema()->hasColumn($m[2])) {
                    throw new GenericException(
                        __(
                            "Le model {0} n'a pas de champ {1}",
                            $m[1],
                            $m[2]
                        )
                    );
                }
                $models[] = $m[1];
            }
        }
        return $models;
    }

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'calc_count_size archives';
    }
}
