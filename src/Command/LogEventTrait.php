<?php

namespace Asalae\Command;

use Asalae\Model\Table\EventLogsTable;
use AsalaeCore\Utility\Premis;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;

trait LogEventTrait
{
    /**
     * Ajoute un evenement de commande
     * @param string               $type
     * @param string               $outcome
     * @param string               $detail
     * @param EntityInterface|null $object
     */
    protected function logEvent(
        string $type,
        string $outcome,
        string $detail,
        $object = null
    ) {
        $loc = TableRegistry::getTableLocator();
        if (empty($object)) {
            $object = $loc->get('Versions')->find()
                ->where(['subject' => Configure::read('App.name')])
                ->orderBy(['id' => 'desc'])
                ->firstOrFail();
        }
        $agent = new Premis\Agent('shell', get_class($this));

        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->newEntry(
            $type,
            $outcome,
            $detail,
            $agent,
            $object
        );
        $EventLogs->saveOrFail($entry);
    }
}
