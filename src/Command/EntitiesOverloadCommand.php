<?php

/**
 * Asalae\Command\EntitiesOverloadCommand
 */

namespace Asalae\Command;

use Asalae\Model\Entity\Eaccpf;
use Asalae\Model\Table\EaccpfsTable;
use AsalaeCore\Command\PromptOptionsTrait;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Faker\Factory;
use Faker\Generator;

/**
 * Génère des entités
 *
 * @category    Command
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EntitiesOverloadCommand extends Command
{
    /**
     * Traits
     */
    use PromptOptionsTrait;

    /**
     * @var Generator instance de Faker
     */
    public static $Faker;

    /**
     * @var Table OrgEntities
     */
    public $OrgEntities;

    /**
     * @var EaccpfsTable $Eaccpfs
     */
    public $Eaccpfs;
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Constructeur de classe
     */
    public function __construct()
    {
        if (empty(self::$Faker)) {
            self::$Faker = Factory::create('fr_FR');
        }
        $this->OrgEntities = TableRegistry::getTableLocator()->get(
            'OrgEntities'
        );
        $this->Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs');
    }

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'entities_overload';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * @param ConsoleOptionParser $parser Option parser to update.
     * @return ConsoleOptionParser
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription(
            __("Permet de créer des entités en masse pour des tests")
        );
        $parser->addArgument(
            'amount',
            [
                'help' => __("Quantité d'entités à créer"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $nb = $args->getArgument('amount');
        $count = $this->OrgEntities->find()->count();
        $this->io->out(__("Il y a actuellement {0} entités", $count));

        while ($count < $nb) {
            $this->createEntityArbo($nb, $count);
        }

        $this->io->out(__("Il y en a désormais {0}", $count));
    }

    /**
     * Créer un arbre entier d'entités
     * @param int  $nb
     * @param int  $count
     * @param null $parent_id
     * @param int  $nested
     */
    private function createEntityArbo(
        int $nb,
        int &$count,
        $parent_id = null,
        int $nested = 0
    ) {
        $entity = $this->OrgEntities->newEntity(
            [
                'name' => self::$Faker->unique()->company,
                'identifier' => self::$Faker->unique()->uuid,
                'parent_id' => $parent_id,
            ]
        );
        $this->OrgEntities->save($entity);
        /** @var Eaccpf $eaccpf */
        $eaccpf = $this->Eaccpfs->newEntity(
            [
                'record_id' => $entity->get('identifier'),
                'name' => $entity->get('name'),
                'agency_name' => self::$Faker->company,
                'entity_type' => self::$Faker->randomElement(
                    ['person', 'family', 'corporateBody']
                ),
                'org_entity_id' => $entity->get('id'),
            ]
        );
        $this->Eaccpfs->initializeData(
            $eaccpf,
            ['name' => 'EntitiesOverloadCommand']
        );
        $this->Eaccpfs->save($eaccpf);

        $count++;
        if ($count >= $nb) {
            return;
        }

        $hasChilds = self::$Faker->boolean;
        if ($hasChilds && $nested < 10) {
            $nbChilds = self::$Faker->numberBetween(1, 12);
            for ($i = 0; $i < $nbChilds; $i++) {
                $this->createEntityArbo(
                    $nb,
                    $count,
                    $entity->get('id'),
                    $nested + 1
                );
                if ($count >= $nb) {
                    return;
                }
            }
        }
    }
}
