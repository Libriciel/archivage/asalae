<?php

/**
 * Asalae\Command\EventLogsRepairCommand
 */

namespace Asalae\Command;

use Asalae\Model\Table\EventLogsTable;
use AsalaeCore\Utility\Premis;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Permet de corriger les problèmes d'objets supprimés du journal qui empèche l'export
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EventLogsRepairCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'event_logs repair';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addOption(
            'name',
            [
                'help' => __(
                    "Nom qui sera inséré dans le premis (mode headless)"
                ),
                'short' => 'n',
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');

        $models = array_values($EventLogs::$premisObjectEntities);
        $query = $EventLogs->find()
            ->where(['object_serialized IS' => null])
            ->distinct(['object_model', 'object_foreign_key']);
        foreach ($models as $model) {
            $query->leftJoinWith($model)->where(["$model.id IS" => null]);
        }
        $io->out(
            __n(
                "Il y a {0} erreur à traiter",
                "Il y a {0} erreurs à traiter",
                $c = $query->count(),
                $c
            )
        );
        $default = $args->getOption('name');
        /** @var EntityInterface $eventLog */
        foreach ($query as $eventLog) {
            $objectId = $eventLog->get('object_model') . ':' . $eventLog->get(
                'object_foreign_key'
            );
            $io->out(__("Un objet manquant a été trouvé: {0}", $objectId));
            $object = new Premis\IntellectualEntity('local', $objectId);
            $object->originalName = $default
                ?: $io->ask(
                    __("Veuillez saisir un nom"),
                    'deleted_object'
                );
            $serialized = serialize($object);
            $conditions = ['object_model' => $eventLog->get('object_model')];
            if ($fk = $eventLog->get('object_foreign_key')) {
                $conditions['object_foreign_key'] = $fk;
            } else {
                $conditions['object_foreign_key IS'] = null;
            }
            $EventLogs->updateAll(
                ['object_serialized' => $serialized],
                $conditions
            );
        }
    }
}
