<?php

/**
 * Asalae\Command\MonitorCommand
 */

namespace Asalae\Command;

use Asalae\Model\Entity\BeanstalkWorker;
use Asalae\Model\Table\BeanstalkJobsTable;
use Asalae\Model\Table\BeanstalkWorkersTable;
use AsalaeCore\Command\WorkerManagerCommand;
use AsalaeCore\Factory\Utility;
use Beanstalk\Utility\Beanstalk;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use DateTime;
use Exception;
use Pheanstalk\Pheanstalk;
use Throwable;

/**
 * Permet de vérifier la santé des workers et de la base de données
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MonitorCommand extends Command
{
    public const int SLEEP_TIME = 60;

    /**
     * @var Beanstalk
     */
    public $Beanstalk;
    /**
     * @var Pheanstalk
     */
    public $pheanstalk;
    /**
     * @var BeanstalkWorkersTable
     */
    public $BeanstalkWorkers;
    /**
     * @var BeanstalkJobsTable
     */
    public $BeanstalkJobs;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'monitor';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __("Vérifi la base de données et la santé des workers en continu")
        );
        $parser->addOption(
            'oneshot',
            [
                'help' => __("Ne boucle pas"),
                'boolean' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->Beanstalk = Utility::get('Beanstalk');
        $this->Beanstalk->params['disable_check_ttr'] = true;
        // beanstalk v3
        if (!method_exists($this->Beanstalk, 'getPheanstalk')) {
            return;
        }
        $this->pheanstalk = $this->Beanstalk->getPheanstalk();
        $loc = TableRegistry::getTableLocator();
        $this->BeanstalkWorkers = $loc->get('BeanstalkWorkers');
        $this->BeanstalkJobs = $loc->get('BeanstalkJobs');

        $logfile = Configure::read(
            'MonitorCommand.logfile',
            LOGS . 'monitor.log'
        );
        $file = fopen($logfile, 'a');
        set_error_handler(
            function ($severity, $message, $filename, $line) use ($file, $io) {
                $time = (new DateTime())->format('Y-m-d H:i:s - ');
                $filename = str_replace(APP, 'APP/', $filename);
                $filename = str_replace(ROOT, 'ROOT', $filename);
                $error = "ERROR $severity - ";
                $io->out("$error$filename:$line $message");
                fwrite($file, "$time$error$filename:$line $message\n");
            }
        );
        $i = 0;
        while (true) {
            $i++;
            try {
                $time = (new DateTime())->format('Y-m-d H:i:s - ');
                if ($this->checkConnection('default') === false) {
                    $msg = __("Echec de connexion à la base de données");
                    $io->out($msg);
                    fwrite($file, $time . $msg . PHP_EOL);
                } else {
                    $this->checkWorkers($io, $file);
                }

                if ($i % 30 === 0) {
                    $msg = __("Monitor OK (30min)");
                    $io->out($msg);
                    fwrite($file, $time . $msg . PHP_EOL);
                }
            } catch (Throwable $e) {
                trigger_error($e->getMessage());
            }
            $io->verbose('done');
            if ($args->getOption('oneshot')) {
                break;
            }
            sleep(self::SLEEP_TIME);//NOSONAR
        }
    }

    /**
     * Vérifie que la base de données est accessible
     * @param string $datasource
     * @return boolean
     */
    private function checkConnection(string $datasource): bool
    {
        try {
            $conn = ConnectionManager::get($datasource);
            $conn->execute('select 1');
            return true;
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return false;
        }
    }

    /**
     * Vérifi que les worker fonctionnent
     * @param ConsoleIo $io
     * @param resource  $file
     */
    private function checkWorkers(ConsoleIo $io, $file)
    {
        try {
            $this->BeanstalkWorkers->sync();
            $this->BeanstalkWorkers->sync = true;
            $this->Beanstalk->setTube('ping');
            if (
                !method_exists($this->Beanstalk, 'count')
                || !method_exists($this->Beanstalk, 'getNext')
                || !method_exists($this->Beanstalk, 'getData')
                || !method_exists($this->Beanstalk, 'done')
            ) {
                return;
            }
            while ($this->Beanstalk->count()) { // vidange du tube avant emploi
                $this->Beanstalk->getNext(1);
                $this->Beanstalk->done();
            }
            $tubes = [];
            $workersOk = [];
            $count = $this->BeanstalkWorkers->find()->count();
            /** @var BeanstalkWorker $worker */
            foreach ($this->BeanstalkWorkers->find() as $worker) {
                if (empty($tubes[$worker->get('tube')])) {
                    $stat = $this->pheanstalk->statsTube($worker->get('tube'));
                    $tubes[$worker->get(
                        'tube'
                    )] = ((array)$stat)['current-jobs-reserved'];
                }
                if ($tubes[$worker->get('tube')]) {
                    $io->verbose(
                        __("Le tube {0} est occupé", $worker->get('tube'))
                    );
                    $workersOk[] = $worker->id;
                    // on ne vérifi pas les tubes qui on un job reserved (en cours)
                    continue;
                }
                $job = $this->Beanstalk->setTube($worker->get('tube'))->emit(
                    [
                        'command' => 'ping',
                        'beanstalk_worker_id' => $worker->get('id'),
                    ],
                    1
                );
                $io->verbose(
                    __(
                        "Envoi du job {0} sur le tube {1}",
                        $job,
                        $worker->get('tube')
                    )
                );
            }
            if (count($workersOk) !== $count) {
                sleep(1); //NOSONAR on attend 1s le temps que la réponse arrive
                $this->Beanstalk->setTube('ping');
                while ($this->Beanstalk->count()) { // vidange du tube avant emploi
                    $this->Beanstalk->getNext(1);
                    $id = $this->Beanstalk->getData();
                    $io->verbose(__("Réponse du worker {0}", $id));
                    $workersOk[] = $id;
                    $this->Beanstalk->done();
                }
            }
            if ($workersOk && count($workersOk) !== $count) {
                $query = $this->BeanstalkWorkers->find()->where(
                    ['id NOT IN' => $workersOk]
                );
                foreach ($query as $worker) {
                    $time = (new DateTime())->format('Y-m-d H:i:s - ');
                    $msg = __(
                        "Le worker id={0} du tube {1} n'a pas répondu",
                        $worker->id,
                        $worker->get('tube')
                    );
                    $io->out($msg);
                    fwrite($file, $time . $msg . PHP_EOL);
                    WorkerManagerCommand::killWorker($worker->id);
                }
            }
            $Exec = Utility::get('Exec');
            $io->verbose(__("Vérification de la liste des workers..."));

            $config = Configure::read('Beanstalk.workers');
            foreach ($config as $name => $args) {
                if (!$args['active']) {
                    continue;
                }
                $count = $this->BeanstalkWorkers->find()
                    ->where(['name' => $name])
                    ->count();
                while ($count < $args['quantity']) {
                    $time = (new DateTime())->format('Y-m-d H:i:s - ');
                    $msg = __(
                        "Worker ''{0}'' manquant. Lancement d'un nouveau worker.",
                        $name
                    );
                    $io->out($msg);
                    fwrite($file, $time . $msg . PHP_EOL);
                    $command = preg_replace(
                        '/^bin\/cake(\.bat|\.php)?/',
                        ROOT . DS . 'bin' . DS . 'cake',
                        $args['run']
                    );
                    $Exec->async($command);
                    $count++;
                }
            }
        } catch (Exception $e) {
            trigger_error(
                'Exception on ' . $e->getFile() . ':' . $e->getLine() . ' : ' . $e->getMessage()
            );
        }
    }
}
