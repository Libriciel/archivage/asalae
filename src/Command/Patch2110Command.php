<?php

/**
 * Asalae\Command\Patch2110Command
 */

namespace Asalae\Command;

use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\ArchivesTable;
use AsalaeCore\Form\AbstractGenericMessageForm;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Suppression des répertoires des transferts entrants acceptés pour lesquels
 * les fichiers ont été supprimés (transfers.files_deleted = true)
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Patch2110Command extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'patch2110';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();

        $parser->setDescription(
            __(
                "Suppression des répertoires des transferts entrants " .
                "acceptés pour lesquels les fichiers ont été supprimés"
            )
        );

        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $Archives = $this->fetchTable('Archives');
        $Transfers = $this->fetchTable('Transfers');

        // mise à jour du chanmp transfers.archive_file_id
        $io->out(__("Mise à jour du champ Transfers.archive_file_id"));
        $query = $Archives
            ->find()
            ->contain(['TransferXmlArchiveFiles'])
            ->where(
                [
                    'Archives.state not in' =>
                        [
                            ArchivesTable::S_CREATING,
                            ArchivesTable::S_DESCRIBING,
                            ArchivesTable::S_STORING,
                            ArchivesTable::S_MANAGING,
                        ],
                ]
            )
            ->orderByAsc('Archives.id');
        $count = $query->count();
        $updatedCount = 0;
        $mtime = microtime(true);
        foreach ($query as $i => $archive) {
            if (microtime(true) - $mtime > 10.0) {
                $mtime = microtime(true);
                $io->out(__("Archives {0}/{1}", $i + 1, $count));
            }
            $archiveFileId = Hash::get($archive, 'transfer_xml_archive_file.id');
            if (!$archiveFileId) {
                $io->out(
                    __(
                        "Fichier de description non trouvé en base de données pour l'archive id: {0}",
                        $archive->get('id')
                    )
                );
                continue;
            }
            $updatedCount++;
            $Transfers->updateAll(
                ['archive_file_id' => $archiveFileId],
                ['id' => $archive->get('transfer_id')]
            );
        }
        $io->out(__("  {0} transferts mis à jour", $updatedCount));

        // suppression du dossier des transferts entrants acceptés et post rétention
        $io->out(__("Suppression des dossiers des transferts entrants acceptés après rétention"));
        $query = $Transfers
            ->find()
            ->where(['Transfers.files_deleted' => true])
            ->orderByAsc('Transfers.id');
        $count = $query->count();
        $mtime = microtime(true);

        $deletedCount = 0;
        $alreadyDeletedCount = 0;
        /**
         * @var  Transfer $transfer
         */
        foreach ($query as $i => $transfer) {
            if (microtime(true) - $mtime > 10.0) {
                $mtime = microtime(true);
                $io->out(__("Transferts {0}/{1}", $i + 1, $count));
            }
            $transferFolder = AbstractGenericMessageForm::getDataDirectory($transfer->get('id'));
            if (file_exists($transferFolder)) {
                Filesystem::remove($transferFolder);
                $deletedCount++;
            } else {
                $alreadyDeletedCount++;
            }
        }

        $io->out(__("  {0} répertoires des transferts supprimés", $deletedCount));
        $io->out(__("  {0} répertoires des transferts déjà supprimés", $alreadyDeletedCount));

        $io->success('done');
    }
}
