<?php

/**
 * Asalae\Command\ArchiveFillerCommand
 */

namespace Asalae\Command;

use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Volume\VolumeManager;
use Asalae\Worker\ArchiveManagementWorker;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Command\Command;
use Cake\Command\Helper\ProgressHelper;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Text;
use DateInterval;
use DateInvalidTimeZoneException;
use DateTime;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Permet de créer des archives en masse pour des tests
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveFillerCommand extends Command
{
    /**
     * @var VolumeManager
     */
    public $VolumeManager;
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'archive filler';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * @param ConsoleOptionParser $parser Option parser to update.
     * @return ConsoleOptionParser
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription(
            __("Permet de créer des archives en masse pour des tests")
        );
        $parser->addArgument(
            'user_id',
            [
                'help' => __("Id de l'utilisateur"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'amount',
            [
                'help' => __("Quantité d'archives (default=100)"),
                'required' => true,
            ]
        );
        $parser->addOption(
            'keep',
            [
                'help' => __("Appraisal rule à 'keep' plutôt que 'destroy'"),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'v2.1',
            [
                'help' => __("SEDA v2.1 plutôt que la v1.0"),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'v2.2',
            [
                'help' => __("SEDA v2.2 plutôt que la v1.0"),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'datasource',
            [
                'help' => __("Datasource utilisé"),
                'default' => 'default',
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;
        $this->main($args->getArgument('user_id'), $args->getArgument('amount'));
    }

    /**
     * Méthode principale
     * @param int $user_id
     * @param int $amount
     * @throws Exception
     */
    public function main($user_id, $amount = 100)
    {
        if (!empty($this->args->getOption('v2.1')) && !empty($this->args->getOption('v2.2'))) {
            $this->io->abort(__("Une seule version à la fois"));
        }
        $opts = ['connectionName' => $this->args->getOption('datasource')];
        $loc = TableRegistry::getTableLocator();
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities', $opts);
        $loc->get('ArchivalAgencies', ['className' => 'OrgEntities'] + $opts);
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits', $opts);
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives', $opts);
        $Users = $loc->get('Users', $opts);
        $user = $Users->find()
            ->where(['Users.id' => $user_id])
            ->contain(['OrgEntities' => ['ArchivalAgencies']])
            ->firstOrFail();
        /** @var EntityInterface $sa */
        $sa = $OrgEntities->getArchivalAgency($user->get('org_entity_id'));
        $conn = $OrgEntities->getConnection();
        /** @var ProgressHelper $progress */
        $progress = $this->io->helper('Progress');
        $progress->init(['total' => (int)$amount]);
        $defaultSecureDataSpace = $this->fetchTable('SecureDataSpaces')
            ->find()
            ->where(
                [
                    'SecureDataSpaces.org_entity_id' => $sa->id,
                    'SecureDataSpaces.is_default IS' => true,
                ]
            )
            ->firstOrFail();
        $this->VolumeManager = new VolumeManager($defaultSecureDataSpace->id);
        for ($i = 0; $i < $amount; $i++) {
            $conn->begin();
            Filesystem::begin();
            try {
                /** @var Transfer $transfer */
                $transfer = $this->createTransfer($sa, $user);
                $transfer->set('archival_agency', $sa);
                $attachment = $this->createAttachment($transfer);
                $transfer->generateArchiveContain([$attachment]);
                $transfer->set('transfer_attachments', [$attachment]);
                $accessRule = $this->createAccessRule();
                $appraisalRule = $this->createAppraisalRule();
                $archive = $this->createArchive(
                    $transfer,
                    $accessRule,
                    $appraisalRule
                );
                $archive->set('transfers', [$transfer]);
                $this->createArchiveUnits(
                    $archive,
                    $accessRule,
                    $appraisalRule
                );
                $worker = new ArchiveManagementWorker(new Entity(), $this->io);
                $worker->work(
                    [
                        'user_id' => $user->id,
                        'transfer_id' => $transfer->id,
                        'archive_id' => $archive->id,
                    ]
                );
                $conn->commit();
                Filesystem::commit();
                $condition = ['ArchiveUnits.archive_id' => $archive->id];
                $ArchiveUnits->calcDuaExpiredRoot($condition);
                $ArchiveUnits->updateSearchField($condition);
                $ArchiveUnits->updateFullSearchField($condition);
                $Archives->updateCountSize([$archive]);
                $ArchiveUnits->updateCountSize($ArchiveUnits->find()->where($condition));
            } catch (Exception $e) {
                $this->io->err((string)$e);
                $conn->rollback();
                Filesystem::rollback();
            } finally {
                $progress->increment();
                $progress->draw();
            }
        }
        $this->io->out('done');
    }

    /**
     * Crée un transfert pour l'archive
     * @param EntityInterface $sa
     * @param EntityInterface $user
     * @return EntityInterface
     * @throws Exception
     */
    private function createTransfer(
        EntityInterface $sa,
        EntityInterface $user
    ): EntityInterface {
        $opts = ['connectionName' => $this->args->getOption('datasource')];
        $loc = TableRegistry::getTableLocator();
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers', $opts);
        /** @var CountersTable $Counters */
        $Counters = $loc->get('Counters', $opts);
        $identifier = $Counters->next($sa->id, 'ArchiveTransfer');
        if ($this->args->getOption('v2.1') ?? false) {
            $message_version = 'seda2.1';
        } elseif ($this->args->getOption('v2.2') ?? false) {
            $message_version = 'seda2.2';
        } else {
            $message_version = 'seda1.0';
        }
        $data = [
            'transfer_comment' => 'generated archive for tests',
            'transfer_date' => $date = new DateTime(),
            'transfer_identifier' => $identifier,
            'archival_agency_id' => $sa->id,
            'transferring_agency_id' => $sa->id,
            'originating_agency_id' => $sa->id,
            'message_version' => $message_version,
            'state' => $Transfers->initialState,
            'state_history' => null,
            'last_state_update' => null,
            'is_conform' => true,
            'is_modified' => false,
            'is_accepted' => true,
            'agreement_id' => null,
            'profile_id' => null,
            'filename' => $identifier . '.xml',
            'data_size' => null,
            'data_count' => null,
            'created_user_id' => $user->id,
            'service_level_id' => null,
            // pour la génération du xml
            'name' => Text::uuid(),
            'description_level' => 'file',
            'code' => 'AR038',
            'start_date' => $date,
            'final' => $this->args->getOption('keep') ? 'conserver' : 'detruire',
            'duration' => 'APP0Y',
            'final_start_date' => new DateTime(),
            'files_deleted' => false,
        ];
        /** @var Transfer $entity */
        $entity = $Transfers->newEntity($data, ['validate' => false]);
        $Transfers->transitionOrFail($entity, TransfersTable::T_PREPARE);
        $Transfers->saveOrFail($entity);
        Filesystem::dumpFile($entity->get('xml'), $entity->generateXml());
        $Transfers->transitionOrFail($entity, TransfersTable::T_ANALYSE);
        $Transfers->transitionOrFail($entity, TransfersTable::T_CONTROL);
        $Transfers->transitionOrFail($entity, TransfersTable::T_ARCHIVE);
        $Transfers->saveOrFail($entity);
        return $entity;
    }

    /**
     * Créé une pièce jointe au transfert
     * @param EntityInterface $transfer
     * @return EntityInterface
     * @throws Exception
     */
    private function createAttachment(EntityInterface $transfer): EntityInterface
    {
        $opts = ['connectionName' => $this->args->getOption('datasource')];
        $loc = TableRegistry::getTableLocator();
        $TransferAttachments = $loc->get('TransferAttachments', $opts);
        $entity = $TransferAttachments->newEntity(
            [
                'transfer_id' => $transfer->id,
                'filename' => 'testfile.txt',
                'size' => 1,
                'hash' => '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b',
                'hash_algo' => 'sha256',
                'virus_name' => '',
                'valid' => true,
                'mime' => 'text/plain',
                'extension' => 'txt',
                'format' => 'x-fmt/111',
            ],
            ['validate' => false]
        );
        $TransferAttachments->saveOrFail($entity);
        Filesystem::dumpFile($entity->get('path'), '1');
        return $entity;
    }

    /**
     * Donne un access rule
     * @param string $code
     * @return EntityInterface
     * @throws Exception
     */
    private function createAccessRule($code = 'AR062'): EntityInterface
    {
        $opts = ['connectionName' => $this->args->getOption('datasource')];
        $loc = TableRegistry::getTableLocator();
        $AccessRules = $loc->get('AccessRules', $opts);
        $AccessRuleCodes = $loc->get('AccessRuleCodes', $opts);
        $rule = $AccessRuleCodes->find()
            ->select(['id', 'duration'])
            ->where(['code' => $code])
            ->firstOrFail();
        $accessRule = $AccessRules->newEntity(
            [
                'access_rule_code_id' => $rule->get('id'),
                'start_date' => new DateTime(),
                'end_date' => (new DateTime())->add(
                    new DateInterval($rule->get('duration'))
                ),
            ]
        );
        return $AccessRules->saveOrFail($accessRule);
    }

    /**
     * Donne un appraisal rule
     * @return EntityInterface
     * @throws Exception
     */
    private function createAppraisalRule(): EntityInterface
    {
        $opts = ['connectionName' => $this->args->getOption('datasource')];
        $loc = TableRegistry::getTableLocator();
        $AppraisalRules = $loc->get('AppraisalRules', $opts);
        $AppraisalRuleCodes = $loc->get('AppraisalRuleCodes', $opts);
        $rule = $AppraisalRuleCodes->find()
            ->select(['id', 'duration'])
            ->where(['code' => 'APP0Y'])
            ->firstOrFail();
        $appraisalRule = $AppraisalRules->newEntity(
            [
                'appraisal_rule_code_id' => $rule->get('id'),
                'final_action_code' => $this->args->getOption('keep') ? 'keep' : 'destroy',
                'start_date' => new DateTime(),
                'end_date' => (new DateTime())->add(new DateInterval($rule->get('duration'))),
            ]
        );
        return $AppraisalRules->saveOrFail($appraisalRule);
    }

    /**
     * Création de l'archive
     * @param EntityInterface $transfer
     * @param EntityInterface $accessRule
     * @param EntityInterface $appraisalRule
     * @return EntityInterface
     * @throws Exception
     */
    private function createArchive(
        EntityInterface $transfer,
        EntityInterface $accessRule,
        EntityInterface $appraisalRule
    ) {
        $opts = ['connectionName' => $this->args->getOption('datasource')];
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives', $opts);
        $saId = $transfer->get('archival_agency_id');
        $secure_data_space_id = Hash::get(
            $transfer,
            'archival_agency.default_secure_data_space_id'
        );
        /** @var CountersTable $Counters */
        $Counters = $loc->get('Counters', $opts);
        $archiveIdentifier = $Counters->next(
            $saId,
            'ArchivalAgencyArchiveIdentifier'
        );
        if (($this->args->getOption('v2.1') ?? false) || ($this->args->getOption('v2.2') ?? false)) {
            $xml_node_tagname = 'ArchiveUnit[1]';
        } else {
            $xml_node_tagname = 'Archive[1]';
        }
        $archive = $Archives->newEntity(
            [
                'state' => $Archives->initialState,
                'is_integrity_ok' => null,
                'archival_agency_id' => $saId,
                'transferring_agency_id' => $saId,
                'originating_agency_id' => $saId,
                'transferring_operator_id' => null,
                'agreement_id' => null,
                'profile_id' => null,
                'transfer_id' => $transfer->get('id'),
                'service_level_id' => null,
                'access_rule_id' => $accessRule->id,
                'appraisal_rule_id' => $appraisalRule->id,
                'archival_agency_identifier' => $archiveIdentifier,
                'transferring_agency_identifier' => null,
                'originating_agency_identifier' => null,
                'secure_data_space_id' => $secure_data_space_id,
                'storage_path' => implode(
                    '/',
                    [
                        'no_profile',
                        date('Y-m'),
                        urlencode($archiveIdentifier),
                    ]
                ),
                'name' => $transfer->get('name'),
                'original_size' => 0,
                'description' => null,
                'oldest_date' => null,
                'latest_date' => null,
                'transferred_size' => 1,
                'management_size' => 0,
                'preservation_size' => 0,
                'dissemination_size' => 0,
                'timestamp_size' => 0,
                'transferred_count' => 1,
                'management_count' => 0,
                'original_count' => 1,
                'preservation_count' => 0,
                'dissemination_count' => 0,
                'timestamp_count' => 0,
                'xml_node_tagname' => $xml_node_tagname,
                'last_state_update' => null,
                'integrity_date' => null,
                'units_count' => 0,
                'filesexist_date' => null,
                'next_pubdesc' => null,
                'next_search' => null,
            ]
        );
        $Archives->transitionOrFail($archive, ArchivesTable::T_DESCRIBE);
        $Archives->transitionOrFail($archive, ArchivesTable::T_STORE);
        return $Archives->saveOrFail($archive);
    }

    /**
     * Créé les archive_units et archive_binaries
     * (provoque la création du archive_description)
     * @param EntityInterface $archive
     * @param EntityInterface $accessRule
     * @param EntityInterface $appraisalRule
     * @return array
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    private function createArchiveUnits(
        EntityInterface $archive,
        EntityInterface $accessRule,
        EntityInterface $appraisalRule
    ): array {
        $opts = ['connectionName' => $this->args->getOption('datasource')];
        $loc = TableRegistry::getTableLocator();
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits', $opts);

        if (($this->args->getOption('v2.1') ?? false) || ($this->args->getOption('v2.2') ?? false)) {
            $original_local_size = 1;
            $original_local_count = 1;
            $auXml_node_tagname = 'ArchiveUnit[1]';
            $documentXml_node_tagname = 'ArchiveUnit[1]';
        } else {
            $original_local_size = 0;
            $original_local_count = 0;
            $auXml_node_tagname = 'Archive[1]';
            $documentXml_node_tagname = 'Document[1]';
        }

        $auArchive = $ArchiveUnits->newEntity(
            [
                'state' => $ArchiveUnits->initialState,
                'archive_id' => $archive->get('id'),
                'access_rule_id' => $accessRule->get('id'),
                'appraisal_rule_id' => $appraisalRule->get('id'),
                'archival_agency_identifier' => $archive->get(
                    'archival_agency_identifier'
                ),
                'transferring_agency_identifier' => null,
                'originating_agency_identifier' => null,
                'name' => $archive->get('name'),
                'original_local_size' => $original_local_size,
                'original_total_size' => 1,
                'parent_id' => null,
                'lft' => null,
                'rght' => null,
                'xml_node_tagname' => $auXml_node_tagname,
                'original_total_count' => 1,
                'original_local_count' => $original_local_count,
                'search' => null,
                'expired_dua_root' => true,
                'full_search' => null,
            ]
        );
        $ArchiveUnits->transitionOrFail(
            $auArchive,
            ArchiveUnitsTable::T_FINISH
        );
        $ArchiveUnits->saveOrFail($auArchive);
        $this->createArchiveDescription($auArchive);

        $auDocument = $ArchiveUnits->newEntity(
            [
                'state' => $ArchiveUnits->initialState,
                'archive_id' => $archive->get('id'),
                'access_rule_id' => $accessRule->get('id'),
                'appraisal_rule_id' => $appraisalRule->get('id'),
                'archival_agency_identifier' => $archive->get(
                    'archival_agency_identifier'
                ) . '_0001',
                'transferring_agency_identifier' => null,
                'originating_agency_identifier' => null,
                'name' => 'testfile.txt',
                'original_local_size' => 1,
                'original_total_size' => 1,
                'parent_id' => $auArchive->id,
                'lft' => null,
                'rght' => null,
                'xml_node_tagname' => $documentXml_node_tagname,
                'original_total_count' => 1,
                'original_local_count' => 1,
            ]
        );
        $ArchiveUnits->transitionOrFail(
            $auDocument,
            ArchiveUnitsTable::T_FINISH
        );
        $ArchiveUnits->saveOrFail($auDocument);

        $ArchiveBinaries = $loc->get('ArchiveBinaries', $opts);
        $path = $archive->get('storage_path') . '/original_data/testfile.txt';
        $storedFile = $this->VolumeManager->fileUpload(
            Hash::get($archive, 'transfers.0.transfer_attachments.0.path'),
            $path
        );
        $archiveBinary = $ArchiveBinaries->newEntity(
            [
                'is_integrity_ok' => null,
                'type' => 'original_data',
                'filename' => 'testfile.txt',
                'format' => 'x-fmt/111',
                'mime' => 'text/plain',
                'extension' => 'txt',
                'stored_file_id' => $storedFile->get('id'),
                'in_rgi' => true,
                'app_meta' => ['category' => 'document_text'],
            ]
        );
        $archiveBinary->set('archive_units', [$auDocument]);
        $ArchiveBinaries->saveOrFail($archiveBinary);

        return [$auArchive, $archiveBinary];
    }

    /**
     * Création de archive_description
     * @param EntityInterface $archiveUnit
     * @return EntityInterface
     * @throws Exception
     */
    private function createArchiveDescription(EntityInterface $archiveUnit): EntityInterface
    {
        $opts = ['connectionName' => $this->args->getOption('datasource')];
        $loc = TableRegistry::getTableLocator();
        $ArchiveDescriptions = $loc->get('ArchiveDescriptions', $opts);
        $accessRule = $this->createAccessRule('AR038');
        $description = $ArchiveDescriptions->newEntity(
            [
                'archive_unit_id' => $archiveUnit->get('id'),
                'access_rule_id' => $accessRule->get('id'),
                'description' => 'generated archive for tests',
                'history' => null,
                'file_plan_position' => null,
                'oldest_date' => null,
                'latest_date' => null,
            ]
        );
        $description->set('access_rule', $accessRule);
        $ArchiveDescriptions->saveOrFail($description);
        $archiveUnit->set('archive_description', $description);
        return $description;
    }
}
