<?php

/**
 * Asalae\Command\IntegrityCheckCommand
 */

namespace Asalae\Command;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;

/**
 * Vérifi l'intégrité d'un fichier ou d'une archive
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class IntegrityCheckCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    private $io;
    /**
     * @var VolumeManager[]
     */
    private $volumeManager;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'integrity check';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __(
                "Vérifie l'intégrité d'un fichier ou d'une archive"
            )
        );
        $parser->addArgument(
            'arg1',
            [
                'help' => __("archives.id ou stored_files.name à vérifier"),
            ]
        );
        return $parser;
    }

    /**
     * Main
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $StoredFiles = $this->fetchTable('StoredFiles');
        if (is_numeric($args->getArgument('arg1'))) {
            $archiveFiles = $StoredFiles->find()
                ->innerJoinWith('ArchiveFiles')
                ->where(['ArchiveFiles.archive_id' => $args->getArgument('arg1')])
                ->orderByAsc('ArchiveFiles.id');
            $count = $archiveFiles->count();
            if ($count === 0) {
                $io->abort(
                    __(
                        "Aucuns fichiers de management trouvés pour l'archive id={0}",
                        $args->getArgument('arg1')
                    )
                );
            }

            $io->out(__("Vérification de l'intégrité de {0} fichiers de management", $count));
            $success = true;
            foreach ($archiveFiles as $archiveFile) {
                $success = $this->checkIntegrity($archiveFile) && $success;
            }

            $archiveBinaries = $StoredFiles->find()
                ->innerJoinWith('ArchiveBinaries')
                ->innerJoinWith('ArchiveBinaries.ArchiveBinariesArchiveUnits')
                ->innerJoinWith('ArchiveBinaries.ArchiveBinariesArchiveUnits.ArchiveUnits')
                ->where(['ArchiveUnits.archive_id' => $args->getArgument('arg1')])
                ->orderByAsc('ArchiveBinaries.id');
            $io->out(__("Vérification de l'intégrité de {0} fichiers binaires", $archiveBinaries->count()));
            foreach ($archiveBinaries as $archiveBinary) {
                $success = $this->checkIntegrity($archiveBinary) && $success;
            }
        } else {
            $storedFile = $StoredFiles->find()
                ->where(['name LIKE' => '%' . $args->getArgument('arg1')])
                ->firstOrFail();
            $success = $this->checkIntegrity($storedFile);
        }

        if ($success) {
            $io->out('done');
            return;
        }
        $io->abort('failed');
    }

    /**
     * Compare un fichier à ce qui est stocké en base
     * @param EntityInterface $storedFile
     * @return void
     * @throws VolumeException
     */
    private function checkIntegrity(EntityInterface $storedFile): bool
    {
        $volumeManager = $this->getVolumeManager($storedFile->get('secure_data_space_id'));
        $success = true;
        foreach ($volumeManager->getVolumes() as $volume_id => $driver) {
            if (!$driver->fileExists($storedFile->get('name'))) {
                $this->io->err(
                    __(
                        "Fichier {0} absent sur le volume {1}"
                        . " (stored_file_id = {2})",
                        $storedFile->get('name'),
                        $volume_id,
                        $storedFile->id
                    )
                );
                $success = false;
                continue;
            }
            $hash = $driver->hash($storedFile->get('name'), $storedFile->get('hash_algo'));
            if ($hash !== $storedFile->get('hash')) {
                $this->io->err(
                    __(
                        "Hash du fichier {0} sur le volume {1}"
                        . " différent de celui en bdd (volume: {2} / bdd: {3}) (stored_file_id = {4})",
                        $storedFile->get('name'),
                        $volume_id,
                        $hash,
                        $storedFile->get('hash'),
                        $storedFile->id
                    )
                );
                $success = false;
            }
        }
        if ($success) {
            $this->io->out(sprintf('%d - %s', $storedFile->id, $storedFile->get('name')));
        }
        return $success;
    }

    /**
     * Donne le volume manager selon le secure_data_space_id
     * @param int $secure_data_space_id
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(int $secure_data_space_id): VolumeManager
    {
        if (!isset($this->volumeManager[$secure_data_space_id])) {
            $this->volumeManager[$secure_data_space_id] = new VolumeManager($secure_data_space_id);
        }
        return $this->volumeManager[$secure_data_space_id];
    }
}
