<?php

/**
 * Asalae\Command\TransferFillerCommand
 */

namespace Asalae\Command;

use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\Antivirus\AntivirusInterface;
use AsalaeCore\Utility\Antivirus\Clamav;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Command\Helper\ProgressHelper;
use Cake\Utility\Hash;
use Datacompressor\Utility\DataCompressor;
use DateTime;
use Exception;
use FileValidator\Utility\FileValidator;
use Libriciel\Filesystem\Utility\Filesystem;
use Pheanstalk\PheanstalkInterface;

/**
 * Permet de créer des transferts en masse pour des tests
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TransferFillerCommand extends Command
{
    /**
     * @var VolumeManager
     */
    public $VolumeManager;
    /**
     * @var Arguments
     */
    public $args;
    /**
     * @var string
     */
    public static string $defaultTmpDir = '';

    private $tmpDir;
    private $attachments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tmpDir = self::$defaultTmpDir
            ?: sys_get_temp_dir() . DS . 'transfer_filler' . DS . uuid_create(UUID_TYPE_TIME);
        if (!is_dir($this->tmpDir)) {
            mkdir($this->tmpDir, 0777, true);
        }
    }

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'transfer filler';
    }

    /**
     * Destructor
     * @throws Exception
     */
    public function __destruct()
    {
        if (is_dir($this->tmpDir)) {
            Filesystem::remove($this->tmpDir);
        }
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * @param ConsoleOptionParser $parser Option parser to update.
     * @return ConsoleOptionParser
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription(
            __("Permet de créer des transferts en masse pour des tests")
        );
        $parser->addArgument(
            'user_id',
            [
                'help' => __("Id de l'utilisateur"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'amount',
            [
                'help' => __("Quantité de transferts (default=100)"),
            ]
        );
        $parser->addOption(
            'attachment',
            [
                'short' => 'a',
                'help' => __(
                    "Fichier zip/tar.gz à joindre aux transferts (multiple)"
                ),
            ]
        );
        $parser->addOption(
            'agreement_id',
            [
                'help' => __("Id de l'accord de versement"),
            ]
        );
        $parser->addOption(
            'profile_id',
            [
                'help' => __("Id du profil d'archives"),
            ]
        );
        $parser->addOption(
            'service_level_id',
            [
                'help' => __("Id du niveau de service"),
            ]
        );
        $parser->addOption(
            'originating_agency_id',
            [
                'help' => __("Id du service producteur"),
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io): ?int
    {
        $this->args = $args;
        $user_id = $args->getArgument('user_id');
        $amount = $args->getArgument('amount') ?: 100;
        $attachment = $args->getOption('attachment');
        if ($attachment) {
            if (!is_file($attachment)) {
                $io->abort(sprintf('"%s" file not found', $attachment));
            }
            $this->uncompressAttachments($attachment, $io);
        }

        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities');
        $Users = $loc->get('Users');
        $sa = $OrgEntities->find()
            ->where(['is_main_archival_agency IS' => true])
            ->firstOrFail();
        $user = $Users->get($user_id);
        $conn = $OrgEntities->getConnection();
        /** @var ProgressHelper $progress */
        $progress = $io->helper('Progress');
        $progress->init(['total' => (int)$amount]);
        $this->VolumeManager = new VolumeManager(
            $sa->get('default_secure_data_space_id')
        );
        $Beanstalk = Utility::get('Beanstalk');
        $Beanstalk->setTube('analyse');
        $io->out(__("Création de {0} transferts...", $amount));
        for ($i = 0; $i < $amount; $i++) {
            $conn->begin();
            Filesystem::begin();
            try {
                /** @var Transfer $transfer */
                $transfer = $this->createTransfer($sa, $user);
                $transfer->set('archival_agency', $sa);
                $attachments = $this->createAttachment($transfer);
                $transfer->generateArchiveContain($attachments);
                $transfer->set('transfer_attachments', [$attachment]);
                $Beanstalk->emit(
                    [
                        'user_id' => $user->id,
                        'transfer_id' => $transfer->id,
                    ],
                    PheanstalkInterface::DEFAULT_PRIORITY,
                    2, // pour laisser le temps de finaliser la transaction
                    PheanstalkInterface::DEFAULT_TTR
                );
                $conn->commit();
                Filesystem::commit();
            } catch (Exception $e) {
                $io->err((string)$e);
                $conn->rollback();
                Filesystem::rollback();
            } finally {
                $progress->increment();
                $progress->draw();
            }
        }
        $io->out('done');

        return self::CODE_SUCCESS;
    }

    /**
     * Prémache la création des attachments
     * @param string    $attachment
     * @param ConsoleIo $io
     * @throws Exception
     */
    private function uncompressAttachments(string $attachment, ConsoleIo $io)
    {
        if (preg_match('/\.zip$/i', $attachment)) {
            $nAttachment = $this->tmpDir . DS . 'attachments.zip';
            copy($attachment, $nAttachment);
            $io->out('sanitizeZip...');
            DataCompressor::sanitizeZip($nAttachment);
            $attachment = $nAttachment;
        }
        $targetUri = $this->tmpDir . DS . 'attachments';
        $io->out('uncompress...');
        $files = DataCompressor::uncompress($attachment, $targetUri);
        try {
            /** @var Clamav|AntivirusInterface $Antivirus */
            $Antivirus = Utility::get(Configure::read('Antivirus.classname'));
            $io->out('antivirus scan...');
            $viruses = $Antivirus->scan($targetUri);
        } catch (Exception $e) {
            $this->log($e->getMessage());
            $viruses = null;
        }

        $io->out('siegfried analyse...');
        $exec = Utility::get('Exec')->command('sf -json', $targetUri);
        $json = json_decode($exec->stdout, true);
        if (!$json) {
            Filesystem::rollback();
            throw new Exception('siegfried failed: ' . $exec->stdout . $exec->stderr);
        }
        $sf = [];
        foreach (Hash::get($json, 'files', []) as $file) {
            $format = Hash::get($file, 'matches.0.id') ?: 'UNKNOWN';
            $sf[$file['filename']] = [
                'format' => $format === 'UNKNOWN' ? '' : $format,
                'mime' => mime_content_type($file['filename']),
                'extension' => pathinfo($file['filename'], PATHINFO_EXTENSION),
            ];
        }
        $algo = Configure::read('hash_algo', 'sha256');
        $len = mb_strlen($this->tmpDir . DS);
        foreach ($files as $file) {
            if ($viruses === null) {
                $virus = null;
            } elseif (isset($viruses[$file])) {
                $virus = $viruses[$file];
            } else {
                $virus = '';
            }
            $this->attachments[$file] = [
                'filename' => mb_substr($file, $len),
                'size' => filesize($file),
                'hash' => hash_file($algo, $file),
                'hash_algo' => $algo,
                'deletable' => true,
                'mime' => mime_content_type($file),
                'extension' => $sf[$file]['extension'],
                'format' => $sf[$file]['format'],
                'virus_name' => $virus,
                'valid' => Utility::get(FileValidator::class)->isValid($file),
            ];
        }
    }

    /**
     * Crée un transfert
     * @param EntityInterface $sa
     * @param EntityInterface $user
     * @return EntityInterface
     * @throws Exception
     */
    private function createTransfer(
        EntityInterface $sa,
        EntityInterface $user
    ): EntityInterface {
        $loc = TableRegistry::getTableLocator();
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        /** @var CountersTable $Counters */
        $Counters = $loc->get('Counters');
        $identifier = $Counters->next($sa->id, 'ArchiveTransfer');
        $data = [
            'transfer_comment' => 'generated transfer for tests',
            'transfer_date' => $date = new DateTime(),
            'transfer_identifier' => $identifier,
            'archival_agency_id' => $sa->id,
            'transferring_agency_id' => $sa->id,
            'message_version' => 'seda1.0',
            'state' => $Transfers->initialState,
            'state_history' => null,
            'last_state_update' => null,
            'is_conform' => true,
            'is_modified' => false,
            'is_accepted' => true,
            'agreement_id' => $this->args->getOption('agreement_id'),
            'profile_id' => $this->args->getOption('profile_id'),
            'filename' => $identifier . '.xml',
            'data_size' => null,
            'data_count' => null,
            'created_user_id' => $user->id,
            'service_level_id' => $this->args->getOption('service_level_id'),
            // pour la génération du xml
            'originating_agency_id' => $this->args->getOption('originating_agency_id') ?: $sa->id,
            'name' => uuid_create(UUID_TYPE_TIME),
            'description_level' => 'file',
            'code' => 'AR038',
            'start_date' => $date,
            'final' => '',
            'duration' => '',
            'final_start_date' => '',
            'files_deleted' => false,
        ];
        /** @var Transfer $entity */
        $entity = $Transfers->newEntity($data, ['validate' => false]);
        $Transfers->transitionOrFail($entity, TransfersTable::T_PREPARE);
        $Transfers->transitionOrFail($entity, TransfersTable::T_ANALYSE);
        $Transfers->saveOrFail($entity);
        Filesystem::dumpFile($entity->get('xml'), $entity->generateXml());
        return $entity;
    }

    /**
     * Créé une pièce jointe au transfert
     * @param EntityInterface $transfer
     * @return EntityInterface[]
     * @throws Exception
     */
    private function createAttachment(EntityInterface $transfer): array
    {
        $loc = TableRegistry::getTableLocator();
        $TransferAttachments = $loc->get('TransferAttachments');
        $entities = [];
        if (!$this->attachments) {
            $entity = $TransferAttachments->newEntity(
                [
                    'transfer_id' => $transfer->id,
                    'filename' => 'testfile.txt',
                    'size' => 1,
                    'hash' => '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b',
                    'hash_algo' => 'sha256',
                    'virus_name' => '',
                    'valid' => true,
                    'mime' => 'text/plain',
                    'extension' => 'txt',
                    'format' => 'x-fmt/111',
                ],
                ['validate' => false]
            );
            $TransferAttachments->saveOrFail($entity);
            Filesystem::dumpFile($entity->get('path'), '1');
            $entities[] = $entity;
        } else {
            foreach ($this->attachments as $file => $params) {
                $entity = $TransferAttachments->newEntity(
                    [
                        'transfer_id' => $transfer->id,
                    ] + $params,
                    ['validate' => false]
                );
                $TransferAttachments->saveOrFail($entity);
                Filesystem::copy($file, $entity->get('path'));
                $entities[] = $entity;
            }
        }

        return $entities;
    }
}
