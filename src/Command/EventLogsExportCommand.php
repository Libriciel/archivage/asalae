<?php

/**
 * Asalae\Command\EventLogsExportCommand
 */

namespace Asalae\Command;

use Asalae\Cron\EventsLogsExport;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchiveKeywordsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Table\TechnicalArchivesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Console\ConsoleIo;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Driver\Volume\VolumeS3;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo as CakeConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Date as CakeDate;
use Cake\ORM\Query;
use DateInterval;
use DateTime;
use DateTimeInterface;
use ReflectionException;
use Throwable;

/**
 * Commande de reconstruction de l'export du journal
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveBinariesTable ArchiveBinaries
 * @property ArchiveFilesTable    ArchiveFiles
 * @property ArchiveKeywordsTable ArchiveKeywords
 * @property EventLogsTable       EventLogs
 * @property StoredFilesTable     StoredFiles
 */
class EventLogsExportCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    private $io;
    /**
     * @var array|mixed
     */
    private mixed $managers;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'event_logs export';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();
        $parser->setDescription(
            __(
                "Cette commande interactive permet de reconstruire les "
                . "archives d'export du journal des événements. Elle ne doit "
                . "être lancée que si il y a eu un problème identifié. Il est "
                . "possible de perdre des informations."
            )
        );
        $parser->addArgument(
            'begin_date',
            [
                'help' => __(
                    "Date de départ au format Y-m-d, si vide, tout les "
                    . "événements seront à nouveau exportés"
                ),
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Throwable
     */
    public function execute(Arguments $args, CakeConsoleIo $io)
    {
        $this->io = $io;
        $in = $io->askChoice(
            __("Cette commande peut entrainer une perte de données, souhaitez-vous continuer ?"),
            ['y', 'n']
        );
        if ($in === 'n') {
            return;
        }
        $io->out(__("Vérification des objets d'événements manquants..."));
        $repairEventLogsCommand = new EventLogsRepairCommand();
        $repairEventLogsCommand->execute(
            new Arguments([], [], []),
            $io
        );
        $date = $args->getArgument('begin_date');
        while ($date && !preg_match('/^\d{4}-\d{2}-\d{2}$/', $date)) {
            $date = $io->ask(
                __(
                    "Veuillez rentrer une date au format yyyy-mm-dd ou "
                    . "laisser vide pour traiter tous les événements"
                )
            );
        }
        if ($date) {
            $date = new CakeDate($date);
        }
        $conn = ConnectionManager::get('default');
        $conn->begin();

        try {
            $this->deleteExistingArchives($date);
            if ($date) {
                $query = $this->fetchTable('EventLogs')->find()
                    ->select(['id'])
                    ->where(['date(created) >=' => $date->format('Y-m-d')]);
                $conditions = ['id IN' => $query];
            } else {
                $conditions = [];
            }
            $this->fetchTable('EventLogs')->updateAll(
                ['exported' => false],
                $conditions
            );
            $this->exportEventLogs();
            $conn->commit();
        } catch (Throwable $e) {
            while ($conn->inTransaction()) {
                $conn->rollback();
            }
            throw $e;
        }
    }

    /**
     * Supprime les archives techniques d'événements selon la $date
     * @param DateTimeInterface|null $date si null, on supprime tout
     * @return void
     * @throws VolumeException
     */
    private function deleteExistingArchives(DateTimeInterface $date = null)
    {
        $Archives = $this->fetchTable('TechnicalArchives');
        $archives = $Archives->find()
            ->where(
                [
                    'type IN' => [
                        TechnicalArchivesTable::TYPE_ARCHIVAL_AGENCY_EVENTS_LOGS,
                        TechnicalArchivesTable::TYPE_OPERATING_EVENTS_LOGS,
                        TechnicalArchivesTable::TYPE_EVENTS_LOGS, // legacy
                    ],
                ]
            );
        if ($date) {
            $archives->where(['date(created) >=' => $date->format('Y-01-01')]);
        }
        $ids = $this->testVolumeManagers($archives);
        $this->deleteFiles($ids, $date);
        if ($ids) {
            $TechnicalArchiveUnits = $this->fetchTable('TechnicalArchiveUnits');
            $query = $TechnicalArchiveUnits->find()
                ->select(['id'])
                ->where(['technical_archive_id IN' => $ids]);
            if ($date) {
                $query->where(['date(oldest_date) >=' => $date->format('Y-m-d')]);
            }
            $TechnicalArchiveUnits->deleteAll(['id IN' => $query]);
            $thisYear = new DateTime('first day of january this year');
            $nextDateYear = $date?->add(new DateInterval('P1Y'));
            if ($date && $nextDateYear < $thisYear) {
                $query = $Archives->find()
                    ->select(['id'])
                    ->where(
                        [
                            'in IN' => $ids,
                            'date(created) >=' => $nextDateYear->format('Y-01-01'),
                        ]
                    );
                $Archives->deleteAll(['id IN' => $query]);
            } elseif (!$date) {
                $Archives->deleteAll(['id IN' => $ids]);
            }
        }
    }

    /**
     * Test les volumes manager des archives
     * @param Query $archives
     * @return array liste des ids des archives
     * @throws VolumeException
     */
    private function testVolumeManagers(Query $archives)
    {
        $this->managers = [];
        $ids = [];
        // on test tous les dataspaces avant d'aller plus loin
        foreach ($archives as $archive) {
            $sdsId = $archive->get('secure_data_space_id');
            if (!isset($this->managers[$sdsId])) {
                $this->io->out(__("Test de l'ECS id={0}", $sdsId), 0);
                $this->managers[$sdsId] = new VolumeManager($sdsId);
                /** @var VolumeS3|VolumeFilesystem $driver */
                foreach ($this->managers[$sdsId]->getVolumes() as $driver) {
                    if ($driver->test()['success'] === false) {
                        throw new VolumeException(VolumeException::VOLUME_NOT_ACCESSIBLE);
                    }
                }
                $this->io->success(' ' . __("OK", $sdsId));
            }
            $ids[] = $archive->id;
        }
        return $ids;
    }

    /**
     * Supprime les fichiers liés aux archives dans $ids
     * @param array                  $ids
     * @param DateTimeInterface|null $date
     * @return void
     */
    private function deleteFiles(array $ids, DateTimeInterface $date = null)
    {
        $StoredFiles = $this->fetchTable('StoredFiles');
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        if ($ids) {
            $query = $StoredFiles->find()
                ->innerJoinWith('ArchiveBinaries.TechnicalArchiveUnits')
                ->where(['TechnicalArchiveUnits.technical_archive_id IN' => $ids])
                ->orderByDesc('ArchiveBinaries.original_data_id IS NOT NULL');
            if ($date) {
                $query->where(['date(TechnicalArchiveUnits.oldest_date) >=' => $date->format('Y-m-d')]);
            }
            foreach ($query as $storedFile) {
                $ArchiveBinaries->deleteAll(['stored_file_id' => $storedFile->id]);
                try {
                    $this->io->out(__("Suppression de {0}", $storedFile->get('name')));
                    $this->managers[$storedFile->get('secure_data_space_id')]
                        ->fileDelete($storedFile->get('name'));
                } catch (VolumeException $e) {
                    $this->io->err($e->getMessage());
                }
            }
        }
        $orphans = $StoredFiles->find()
            ->leftJoinWith('ArchiveBinaries')
            ->leftJoinWith('ArchiveFiles')
            ->leftJoinWith('ArchiveBinaries.ArchiveUnits')
            ->leftJoinWith('ArchiveBinaries.TechnicalArchiveUnits')
            ->where(
                [
                    'OR' => [
                        [ // stored_files orphelins
                            'ArchiveBinaries.id IS' => null,
                            'ArchiveFiles.id IS' => null,
                        ],
                        [ // archive_binaries orphelins
                            'ArchiveFiles.id IS' => null,
                            'ArchiveUnits.id IS' => null,
                            'TechnicalArchiveUnits.id IS' => null,
                        ],
                    ],
                ]
            )
            ->orderBy(['ArchiveBinaries.original_data_id is null']);
        foreach ($orphans as $storedFile) {
            $ArchiveBinaries->deleteAll(['stored_file_id' => $storedFile->id]);
            try {
                $this->io->out(__("Suppression d'un orphelin {0}", $storedFile->get('name')));
                $secure_data_space_id = $storedFile->get('secure_data_space_id');
                if (!isset($this->managers[$secure_data_space_id])) {
                    $this->managers[$secure_data_space_id] = new VolumeManager($secure_data_space_id);
                }
                $this->managers[$secure_data_space_id]
                    ->fileDelete($storedFile->get('name'));
            } catch (VolumeException $e) {
                $this->io->err($e->getMessage());
            }
        }
    }

    /**
     * Lance le cron d'export du journal
     * @return void
     * @throws VolumeException
     * @throws ReflectionException
     */
    private function exportEventLogs()
    {
        $cron = new EventsLogsExport(
            ['override' => true],
            $this->io->getOut(),
            $this->io->getErr()
        );
        $cron->work();
    }
}
