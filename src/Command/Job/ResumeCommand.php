<?php

/**
 * Asalae\Command\Job\ResumeCommand
 */

namespace Asalae\Command\Job;

use Asalae\Command\LogEventTrait;
use AsalaeCore\Command\JobResumeCommand as CoreJobResumeCommand;
use Beanstalk\Model\Table\BeanstalkJobsTable;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Permet de relancer un job
 * ex: bin/cake job resume 4531
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ResumeCommand extends CoreJobResumeCommand
{
    use LogEventTrait;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job resume';
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        /** @var BeanstalkJobsTable $Jobs */
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $jobid = $args->getArgument('jobid');
        $entity = $Jobs->find()
            ->where(['BeanstalkJobs.id' => $jobid])
            ->first();
        if (!$entity) {
            $io->abort(
                __(
                    "Le job (id={0}) n'a pas été trouvé en base de données",
                    $args->getArgument('jobid')
                )
            );
        }
        $Jobs->transitionOrFail($entity, BeanstalkJobsTable::T_RESUME);
        $Jobs->saveOrFail($entity);
        $this->logEvent(
            'shell_resume_job',
            'success',
            __("Reprise du id={0} du tube ''{1}''", $entity->get('id'), $entity->get('tube'))
        );
        $io->out('done');
    }
}
