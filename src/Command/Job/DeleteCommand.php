<?php

/**
 * Asalae\Command\Job\DeleteCommand
 */

namespace Asalae\Command\Job;

use Asalae\Command\LogEventTrait;
use AsalaeCore\Command\JobDeleteCommand as CoreJobDeleteCommand;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Permet de supprimer un job
 * ex: bin/cake job delete 4531
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DeleteCommand extends CoreJobDeleteCommand
{
    use LogEventTrait;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job delete';
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $entity = $Jobs->find()
            ->where(['BeanstalkJobs.id' => $args->getArgument('jobid')])
            ->first();
        if (!$entity) {
            $io->abort(
                __(
                    "Le job (id={0}) n'a pas été trouvé en base de données",
                    $args->getArgument('jobid')
                )
            );
        }
        $Jobs->deleteOrFail($entity);
        $this->logEvent(
            'shell_delete_job',
            'success',
            __("Suppression du job id={0} du tube ''{1}''", $entity->get('id'), $entity->get('tube'))
        );
        $io->out('done');
    }
}
