<?php

/**
 * Asalae\Command\Job\EditCommand
 */

namespace Asalae\Command\Job;

use Asalae\Command\LogEventTrait;
use AsalaeCore\Command\JobEditCommand as CoreJobEditCommand;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Permet de modifier un job
 * ex: bin/cake job edit 4531 '{"user_id":5,"foo":"bar"}'
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EditCommand extends CoreJobEditCommand
{
    use LogEventTrait;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job edit';
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $jobid = $args->getArgument('jobid');
        $entity = $Jobs->find()
            ->where(['BeanstalkJobs.id' => $jobid])
            ->first();
        if (!$entity) {
            $io->abort(
                __(
                    "Le job (id={0}) n'a pas été trouvé en base de données",
                    $args->getArgument('jobid')
                )
            );
        }
        if (!$args->getArgument('data')) {
            $io->out(
                escapeshellarg(
                    json_encode($entity->get('object_data'), JSON_UNESCAPED_SLASHES)
                )
            );
            return;
        } elseif (!($data = json_decode($args->getArgument('data'), true))) {
            $io->abort(__("Erreur lors du parsing json"));
        } elseif (!is_array($data)) {
            $io->abort(__("Les données doivent être sous la forme d'un tableau"));
        }

        $Jobs->patchEntity($entity, ['data' => $data]);
        $Jobs->saveOrFail($entity);
        $this->logEvent(
            'shell_edit_job',
            'success',
            __("Modification du job id={0} du tube ''{1}''", $jobid, $entity->get('tube'))
        );
        $io->success(__("{0} modifié avec succès", $jobid));
    }
}
