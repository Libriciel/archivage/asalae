<?php

/**
 * Asalae\Command\Job\ResumeAllCommand
 */

namespace Asalae\Command\Job;

use Asalae\Command\LogEventTrait;
use AsalaeCore\Command\JobResumeAllCommand as CoreJobResumeAllCommand;
use Beanstalk\Model\Table\BeanstalkJobsTable;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Permet de relancer tous les jobs d'un tube
 * ex: bin/cake job resume_all test
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ResumeAllCommand extends CoreJobResumeAllCommand
{
    use LogEventTrait;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job resume_all';
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $tube = $args->getArgument('tube');
        /** @var BeanstalkJobsTable $Jobs */
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $query = $Jobs->find()
            ->where(['tube' => $tube, 'job_state' => BeanstalkJobsTable::S_FAILED])
            ->orderBy(['priority']);
        $kicked = $query->count();
        foreach ($query as $job) {
            $Jobs->transition($job, BeanstalkJobsTable::T_RETRY);
            $Jobs->save($job);
        }
        if ($kicked === 0) {
            $io->warning(__("Aucun job n'a été relancé"));
            return;
        }
        $this->logEvent(
            'shell_resume_all_jobs',
            'success',
            __("Reprise de {0} jobs du tube ''{0}''", $kicked, $tube)
        );
        $io->success(__("{0} jobs ont été relancés", $kicked));
    }
}
