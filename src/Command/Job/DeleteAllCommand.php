<?php

/**
 * Asalae\Command\Job\DeleteAllCommand
 */

namespace Asalae\Command\Job;

use Asalae\Command\LogEventTrait;
use AsalaeCore\Command\JobDeleteAllCommand as CoreJobDeleteAllCommand;
use Beanstalk\Model\Table\BeanstalkJobsTable;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Exception;

/**
 * Permet de supprimer tous les jobs en erreur (buried) d'un tube
 * ex: bin/cake job delete_all test --state buried
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DeleteAllCommand extends CoreJobDeleteAllCommand
{
    use LogEventTrait;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job delete_all';
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $tube = $args->getArgument('tube');
        $deleted = $this->fetchTable('BeanstalkJobs')
            ->deleteAll(
                [
                    'tube' => $tube,
                    'job_state !=' => BeanstalkJobsTable::S_WORKING,
                ]
            );
        if ($deleted === 0) {
            $io->warning(__("Aucun job n'a été supprimé"));
            return;
        }
        $this->logEvent(
            'shell_delete_all_jobs',
            'success',
            __("Suppression de {0} jobs du tube ''{0}''", $deleted, $tube)
        );
        $io->success(__("{0} jobs ont été supprimés", $deleted));
    }
}
