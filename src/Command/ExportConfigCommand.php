<?php

/**
 * Asalae\Command\ExportConfigCommand
 */

namespace Asalae\Command;

use Asalae\Model\Entity\Aco;
use Asalae\Model\Entity\ArosAco;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Entity\Role;
use Asalae\Model\Table\OrgEntitiesTable;
use AsalaeCore\Application as CoreApplication;
use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\Controller;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Config;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Query;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;

/**
 * export de la configuration de Asalae 2 dans fichier ini
 * (pour utilisation ensuite via './bin/cake install config export.ini --postinstall'
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ExportConfigCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * @var OrgEntitiesTable
     */
    private $OrgEntities;

    /**
     * id des org_entities filles du tenant si on limite l'export à un seul SA
     * @var int[]
     */
    private $tenantChilds;

    /**
     * id du tenant si on limite l'export à un seul SA
     * @var int
     */
    private $tenantId;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var array
     */
    private $files = [];

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'export config';
    }

    /**
     * Gets the option parser instance and Configs it.
     *
     * By overriding this method you can Config the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();
        $parser->addArgument(
            'scope',
            [
                'help' => __("Scope : all, technical, functional (défaut)"),
                'choices' => ['all', 'technical', 'functional'],
            ]
        );
        $parser->addOption(
            'tenant',
            [
                'help' => __(
                    "Identifiant du tenant, permet de limiter l'export à un seul tenant (SA)"
                    . " pour un export fonctionnel (functional)"
                ),
                'short' => 't',
            ]
        );

        $parser->addOption(
            'output-file',
            [
                'help' => __(
                    "Chemin et nom du fichier ini de destination\n"
                    . " (valeurs dynamiques : {version} pour la version de l'application,\n{time} pour le timestamp)"
                ),
                'short' => 'f',
                'default' => sys_get_temp_dir() . DS . 'asalae2_export_{version}_{time}.ini',
            ]
        );
        $parser->addOption(
            'no_date',
            [
                'help' => __(
                    "Ne pas ajouter la date dans le fichier afin de faciliter la comparaison par hash du fichier."
                ),
                'short' => 'n',
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'datasource',
            [
                'help' => __("Datasource utilisé"),
                'default' => 'default',
            ]
        );

        return $parser;
    }

    /**
     * Commande d'installation via fichier de conf
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io): int
    {
        if ($args->getOption('datasource') !== 'default') {
            ConnectionManager::drop('default');
            ConnectionManager::setConfig(
                'default',
                ConnectionManager::getConfig($args->getOption('datasource'))
            );
        }
        $this->io = $io;
        $this->OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');

        /** @var string $scope */
        $scope = $args->getArgument('scope') ?: 'functional';

        $tenant = $args->getOption('tenant');
        if ($tenant !== null) {
            /** @var EntityInterface $tenant */
            $tenant = $this->OrgEntities->find()
                ->innerJoinWith('TypeEntities')
                ->where(['identifier' => $tenant, 'code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES])
                ->first();
            if (!$tenant) {
                throw new Exception(__("Le tenant spécifié n'existe pas"));
            }
            $this->tenantChilds = $this->OrgEntities->find()
                ->where(
                    [
                        'lft >=' => $tenant->get('lft'),
                        'rght <=' => $tenant->get('rght'),
                    ]
                )
                ->all()
                ->extract('id')
                ->toArray();
            $this->tenantId = $tenant->get('id');
        }

        $versionFile = file(ROOT . DS . 'VERSION.txt');
        $version = trim(array_pop($versionFile));
        /** @var string $file */
        $file = str_replace(['{version}', '{time}'], [$version, time()], $args->getOption('output-file'));
        if (!is_writable($dir = dirname($file))) {
            throw new Exception(__("Le chemin indiqué {0} n'est pas inscriptible", $dir));
        }

        $this->data['migration_version'] = $this->getMigrationVersion();

        if (!$args->getOption('no_date')) {
            $this->data['date'] = time();
        }

        if ($scope === 'all' || $scope === 'technical') {
            $this->exportTechnical();
        }
        if ($scope === 'all' || $scope === 'functional') {
            $this->exportFunctional();
        }

        $this->data['files'] = $this->files;
        $this->write($file, $this->data);

        $this->io->verbose(__("Fichier d'export sauvegardé :"));
        $this->io->out($file);

        return self::CODE_SUCCESS;
    }

    /**
     * La version de la migration
     * @return int
     */
    public function getMigrationVersion(): int
    {
        /** @var Entity|null $plog */
        $plog = TableRegistry::getTableLocator()->get('Phinxlog')->find()
            ->orderBy(['version' => 'desc'])
            ->first();

        return $plog === null ? 0 : $plog->get('version');
    }

    /**
     * Partie Technique de l'export
     * @return void
     * @throws Exception
     */
    private function exportTechnical()
    {
        $loc = TableRegistry::getTableLocator();

        // config
        $this->data['config'] = [
            'config_file' => include Config::read('App.paths.path_to_local_config'),
            'debug' => Config::read('debug'),
            'data_dir' => Config::read('App.paths.data'),
            'admin_file' => Config::read('App.paths.administrators_json'),
        ];

        $this->data['app'] = [
            'locale' => Config::read('App.defaultLocale'),
            'timezone' => Config::read('App.timezone'),
            'url' => Config::read('App.fullBaseUrl'),
        ];

        $this->data['ratchet'] = [
            'url_ws' => Config::read('Ratchet.connect'),
        ];

        $this->data['database'] = [
            'host' => Config::readAll('Datasources.default.host'),
            'username' => Config::readAll('Datasources.default.username'),
            'password' => Config::readAll('Datasources.default.password'),
            'database' => Config::readAll('Datasources.default.database'),
        ];

        $this->data['email'] = [
            'from' => Config::readAll('Email.default.from'),
            'method' => Config::readAll('EmailTransport.default.className'),
            'host' => Config::readAll('EmailTransport.default.host'),
            'port' => Config::readAll('EmailTransport.default.port'),
            'username' => Config::readAll('Email.default.username'),
            'password' => Config::readAll('Email.default.password'),
        ];

        $this->data['proxy'] = [
            'host' => Config::readAll('Proxy.host'),
            'port' => Config::readAll('Proxy.port'),
            'username' => Config::readAll('Proxy.username'),
            'password' => Config::readAll('Proxy.password'),
        ];

        $this->data['security'] = [
            'type' => 'set',
            'value' => Config::readAll('Security.salt'),
        ];

        // admins
        $admins = json_decode(file_get_contents(Config::read('App.paths.administrators_json')), true) ?: [];
        $extractAdmin = fn(array $a) => [
            'username' => $a['username'],
            'email' => $a['email'],
            'hashed_password' => $a['password'],
        ];
        $this->data['admins'] = array_map($extractAdmin, $admins);

        // org_entities (SE)
        /** @var OrgEntity $se */
        $se = $this->OrgEntities->find()
            ->select(['name', 'identifier'])
            ->where(['code IN' => OrgEntitiesTable::CODE_OPERATING_DEPARTMENT])
            ->contain(['TypeEntities'])
            ->firstOrFail()
            ->toArray();

        $eaccpf = $loc->get('Eaccpfs')->find()
            ->where(['record_id' => $se['identifier']])
            ->firstOrFail()
            ->get('data');

        $se['installer_name'] = Hash::get(
            json_decode($eaccpf, true),
            'eac-cpf.control.maintenanceHistory.maintenanceEvent.0.agent'
        );
        $this->data['exploitation_service'] = $se;

        // timestampers
        $timestampers = $loc->get('Timestampers')->find()
            ->orderBy('id')
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return array_filter(
                        [
                            '_id' => $entity->get('id'),
                            'name' => $entity->get('name'),
                            'driver' => $entity->get('driver'),
                            'url' => $entity->get('url'),
                            'useProxy' => $entity->get('useProxy'),
                            'proxyHost' => $entity->get('proxyHost'),
                            'proxyPort' => $entity->get('proxyPort'),
                            'proxyLogin' => $entity->get('proxyLogin'),
                            'proxyPassword' => $entity->get('proxyPassword'),
                            'timestamp_format' => $entity->get('timestamp_format'),
                            'hash_algo' => $entity->get('hash_algo'),
                            'login' => $entity->get('login'),
                            'password' => $entity->get('password'),
                            'ca' => $entity->get('ca'),
                            'crt' => $entity->get('crt'),
                            'pem' => $entity->get('pem'),
                            'cnf' => $entity->get('cnf'),
                        ],
                        fn($e) => $e !== null
                    );
                }
            )
            ->toArray();
        $this->data['timestampers'] = array_reduce(
            $timestampers,
            fn($c, $i) => $c + ['timestamper_' . $i['_id'] => $i],
            []
        );

        foreach ($timestampers as $timestamper) {
            if ($timestamper['driver'] === 'OPENSSL') {
                foreach (['ca', 'crt', 'pem', 'cnf'] as $type) {
                    $path = $timestamper[$type];
                    if (!is_file($path)) {
                        throw new Exception(__("Le fichier d'horodatage {0} n'existe pas", $path));
                    }
                    $this->files[] = [
                        'path' => $path,
                        'content' => base64_encode(file_get_contents($path)),
                    ];
                }
            }
        }

        // volumes
        $volumes = $loc->get('Volumes')->find()
            ->where(
                $this->tenantChilds
                    ? ['SecureDataSpaces.org_entity_id IN' => $this->tenantChilds]
                    : []
            )
            ->contain('SecureDataSpaces')
            ->orderBy('Volumes.id')
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'name' => $entity->get('name'),
                        'fields' => $entity->get('fields'),
                        'driver' => $entity->get('driver'),
                        'description' => $entity->get('description'),
                        'active' => (int)$entity->get('active'),
                        'alert_rate' => $entity->get('alert_rate'),
                        'disk_usage' => $entity->get('disk_usage'),
                        'max_disk_usage' => $entity->get('max_disk_usage'),
                        'location' => $entity->get('location'),
                    ];
                }
            )
            ->toArray();
        $this->data['volumes'] = array_reduce(
            $volumes,
            fn($c, $i) => $c + ['volume_' . $i['_id'] => $i],
            []
        );

        // ecs
        $ecs = $loc->get('SecureDataSpaces')->find()
            ->where(
                $this->tenantChilds
                    ? ['SecureDataSpaces.org_entity_id IN' => $this->tenantChilds]
                    : []
            )
            ->contain(['Volumes', 'OrgEntities'])
            ->orderBy('SecureDataSpaces.id')
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'name' => $entity->get('name'),
                        'description' => $entity->get('description'),
                        'org_entity' => Hash::get($entity, 'org_entity.identifier'),
                        'is_default' => (int)$entity->get('is_default'),
                        'volumes' => array_map(
                            fn($i) => 'volume_' . $i,
                            Hash::extract($entity, 'volumes.{n}.id')
                        ),
                    ];
                }
            )
            ->toArray();
        $this->data['secure_data_spaces'] = array_reduce(
            $ecs,
            fn($c, $i) => $c + ['secure_data_space_' . $i['_id'] => $i],
            []
        );

        // org_entities (SA)
        $archivalAgencies = $this->OrgEntities->find()
            ->where(['code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES])
            ->andWhere(
                $this->tenantChilds
                    ? ['OrgEntities.id IN' => $this->tenantChilds]
                    : []
            )
            ->contain(
                [
                    'SecureDataSpaces',
                    'Timestampers',
                    'TypeEntities',
                ]
            )
            ->orderBy('OrgEntities.id')
            ->all()
            ->map(
                function (EntityInterface $entity) use ($loc): array {
                    $timestamper = $entity->get('timestamper_id')
                        ? $loc->get('Timestampers')->find()
                            ->where(['id' => $entity->get('timestamper_id')])
                            ->first()
                        : null;
                    return [
                        '_id' => $entity->get('id'),
                        'name' => $entity->get('name'),
                        'identifier' => $entity->get('identifier'),
                        'short_name' => $entity->get('short_name'),
                        'is_main_archival_agency' => (int)$entity->get('is_main_archival_agency'),
                        'active' => (int)$entity->get('active'),
                        'timestamper' => $timestamper ? $timestamper->get('name') : null,
                        'timestampers' => array_map(
                            fn($i) => 'timestamper_' . $i,
                            Hash::extract($entity, 'timestampers.{n}.id')
                        ),
                        'secure_data_spaces' => array_map(
                            fn($i) => 'secure_data_space_' . $i,
                            Hash::extract($entity, 'secure_data_spaces.{n}.id')
                        ),
                    ];
                }
            )
            ->toArray();
        $this->data['archival_agencies'] = array_reduce(
            $archivalAgencies,
            fn($c, $i) => $c + ['org_entity_' . $i['_id'] => $i],
            []
        );

        // ldaps
        $ldaps = $loc->get('Ldaps')->find()
            ->where(
                $this->tenantChilds
                    ? ['Ldaps.org_entity_id IN' => $this->tenantChilds]
                    : []
            )
            ->contain(['OrgEntities'])
            ->orderBy('Ldaps.id')
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'name' => $entity->get('name'),
                        'host' => $entity->get('host'),
                        'port' => $entity->get('port'),
                        'user_query_login' => $entity->get('user_query_login'),
                        'user_query_password' => $entity->get('user_query_password'),
                        'ldap_root_search' => $entity->get('ldap_root_search'),
                        'user_login_attribute' => $entity->get('user_login_attribute'),
                        'ldap_users_filter' => $entity->get('ldap_users_filter'),
                        'account_prefix' => $entity->get('account_prefix'),
                        'account_suffix' => $entity->get('account_suffix'),
                        'description' => $entity->get('description'),
                        'use_proxy' => (int)$entity->get('use_proxy'),
                        'use_ssl' => (int)$entity->get('use_ssl'),
                        'use_tls' => (int)$entity->get('use_tls'),
                        'user_name_attribute' => $entity->get('user_name_attribute'),
                        'user_mail_attribute' => $entity->get('user_mail_attribute'),
                        'schema' => $entity->get('schema'),
                        'follow_referrals' => (int)$entity->get('follow_referrals'),
                        'version' => $entity->get('version'),
                        'timeout' => $entity->get('timeout'),
                        'custom_options' => $entity->get('custom_options'),
                        'user_username_attribute' => $entity->get('user_username_attribute'),
                        'org_entity' => Hash::get($entity, 'org_entity.identifier'),
                    ];
                }
            )
            ->toArray();
        $this->data['ldaps'] = array_reduce(
            $ldaps,
            fn($c, $i) => $c + ['ldap_' . $i['_id'] => $i],
            []
        );

        // archiving_systems
        $archivingSystems = $loc->get('ArchivingSystems')->find()
            ->where(
                $this->tenantChilds
                    ? ['ArchivingSystems.org_entity_id IN' => $this->tenantChilds]
                    : []
            )
            ->orderBy('ArchivingSystems.id')
            ->contain(['OrgEntities'])
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'name' => $entity->get('name'),
                        'description' => $entity->get('description'),
                        'url' => $entity->get('url'),
                        'username' => $entity->get('username'),
                        'use_proxy' => (int)$entity->get('use_proxy'),
                        'active' => (int)$entity->get('active'),
                        'password' => $entity->get('password')->toFormData(),
                        'ssl_verify_peer' => (int)$entity->get('ssl_verify_peer'),
                        'ssl_verify_peer_name' => (int)$entity->get('ssl_verify_peer_name'),
                        'ssl_verify_depth' => $entity->get('ssl_verify_depth'),
                        'ssl_verify_host' => (int)$entity->get('ssl_verify_host'),
                        'ssl_cafile' => $entity->get('ssl_cafile'),
                        'chunk_size' => $entity->get('chunk_size'),
                        'org_entity' => Hash::get($entity, 'org_entity.identifier'),
                    ];
                }
            )
            ->toArray();
        $this->data['archiving_systems'] = array_reduce(
            $archivingSystems,
            fn($c, $i) => $c + ['archiving_system_' . $i['_id'] => $i],
            []
        );
    }

    /**
     * Partie Fonctionnelle de l'export
     * @return void
     * @throws Exception
     */
    private function exportFunctional()
    {
        $loc = TableRegistry::getTableLocator();

        // org_entities (autres)
        $orgEntities = $this->OrgEntities->find()
            ->where(['code NOT IN' => ['SE', 'SA']])
            ->andWhere(
                $this->tenantChilds
                    ? ['OrgEntities.id IN' => $this->tenantChilds]
                    : []
            )
            ->orderBy('OrgEntities.lft')
            ->contain(['ParentOrgEntities', 'TypeEntities', 'ArchivalAgencies'])
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'name' => $entity->get('name'),
                        'identifier' => $entity->get('identifier'),
                        'short_name' => $entity->get('short_name'),
                        'parent' => Hash::get($entity, 'parent_org_entity.identifier'),
                        'archival_agency' => Hash::get($entity, 'archival_agency.identifier'),
                        'type_entity' => Hash::get($entity, 'type_entity.code'),
                    ];
                }
            )
            ->toArray();
        $this->data['org_entities'] = array_reduce(
            $orgEntities,
            fn($c, $i) => $c + ['org_entity_' . $i['_id'] => $i],
            []
        );

        // roles
        $roles = $loc->get('Roles')->find()
            ->where(
                $this->tenantChilds
                    ? ['Roles.org_entity_id IN' => $this->tenantChilds]
                    : ['Roles.org_entity_id IS NOT' => null]
            )
            ->orderBy('Roles.lft')
            ->contain(['OrgEntities', 'ParentRoles', 'TypeEntities'])
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'name' => $entity->get('name'),
                        'active' => (int)$entity->get('active'),
                        'hierarchical_view' => (int)$entity->get('hierarchical_view'),
                        'description' => $entity->get('description'),
                        'agent_type' => $entity->get('agent_type'),
                        'type_entities' => Hash::extract($entity, 'type_entities.{n}.code'),
                        'parent_role' => Hash::get($entity, 'parent_role.name'),
                        'org_entity' => Hash::get($entity, 'org_entity.identifier'),
                    ];
                }
            )
            ->toArray();
        $this->data['roles'] = array_reduce(
            $roles,
            fn($c, $i) => $c + ['role_' . $i['_id'] => $i],
            []
        );

        // users
        $users = $loc->get('Users')->find()
            ->leftJoinWith('SuperArchivistsArchivalAgencies')
            ->where(
                $this->tenantChilds
                    ? [
                        'OR' => [
                            ['Users.org_entity_id IN' => $this->tenantChilds],
                            ['SuperArchivistsArchivalAgencies.archival_agency_id IN' => $this->tenantChilds]
                        ]
                    ]
                    : []
            )
            ->orderBy('Users.id')
            ->contain(['ArchivalAgencies', 'Ldaps', 'OrgEntities', 'Roles'])
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'username' => $entity->get('username'),
                        'hashed_password' => $entity->get('password'),
                        'menu' => $entity->get('menu'),
                        'high_contrast' => (int)$entity->get('high_contrast'),
                        'active' => (int)$entity->get('active'),
                        'email' => $entity->get('email'),
                        'name' => $entity->get('name'),
                        'agent_type' => $entity->get('agent_type'),
                        'is_validator' => (int)$entity->get('is_validator'),
                        'use_cert' => (int)$entity->get('use_cert'),
                        'ldap_login' => $entity->get('ldap_login'),
                        'ldap' => Hash::get($entity, 'ldap.name'),
                        'org_entity' => Hash::get($entity, 'org_entity.identifier'),
                        'archival_agency' => Hash::get($entity, 'archival_agency.identifier'),
                        'role' => Hash::get($entity, 'role.code') ?? ('role_' . Hash::get($entity, 'role.id')),
                        'super_archivist_of' => array_map(
                            fn ($i) => 'org_entity_' . $i,
                            Hash::extract($entity, 'archival_agencies.{n}.identifier')
                        ),
                    ];
                }
            )
            ->toArray();
        $this->data['users'] = array_reduce(
            $users,
            fn($c, $i) => $c + ['user_' . $i['_id'] => $i],
            []
        );

        // validation_actors
        $validationActors = $loc->get('ValidationActors')->find()
            ->where(
                $this->tenantChilds
                    ? ['ValidationChains.org_entity_id IN' => $this->tenantChilds]
                    : []
            )
            ->contain(
                [
                    'Users',
                    'ValidationStages.ValidationChains',
                ]
            )
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    $data = [
                        '_id' => $entity->get('id'),
                        'type' => $entity->get('app_type'),
                    ];
                    switch ($entity->get('app_type')) {
                        case 'USER':
                            $data['user_username'] = Hash::get($entity, 'user.username');
                            $data['user_type_validation'] = $entity->get('type_validation');
                            break;
                        case 'MAIL':
                            $data['mail_mail'] = $entity->get('app_key');
                            $data['mail_actor_name'] = $entity->get('actor_name');
                            break;
                        case 'SERVICE_ARCHIVES':
                        case 'SERVICE_DEMANDEUR':
                        case 'SERVICE_PRODUCTEUR':
                            break;
                        default:
                            throw new Exception("Unknown validation_actor type : " . $entity->get('app_type'));
                    }
                    return $data;
                }
            )
            ->toArray();
        $this->data['validation_actors'] = array_reduce(
            $validationActors,
            fn($c, $i) => $c + ['validation_actor_' . $i['_id'] => $i],
            []
        );

        // validation_stages
        $validationStages = $loc->get('ValidationStages')->find()
            ->where(
                $this->tenantChilds
                    ? ['ValidationChains.org_entity_id IN' => $this->tenantChilds]
                    : []
            )
            ->contain(
                [
                    'ValidationChains',
                    'ValidationActors',
                ]
            )
            ->orderBy('ValidationStages.id')
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'name' => $entity->get('name'),
                        'description' => $entity->get('description'),
                        'all_actors_to_complete' => (int)$entity->get('all_actors_to_complete'),
                        'ord' => $entity->get('ord'),
                        'app_type' => $entity->get('app_type'),
                        'actors' => array_map(
                            fn($i) => 'validation_actor_' . $i,
                            Hash::extract($entity, 'validation_actors.{n}.id')
                        ),
                    ];
                }
            )
            ->toArray();
        $this->data['validation_stages'] = array_reduce(
            $validationStages,
            fn($c, $i) => $c + ['validation_stage_' . $i['_id'] => $i],
            []
        );

        // validation_chains
        $validationChains = $loc->get('ValidationChains')->find()
            ->where(
                $this->tenantChilds
                    ? ['ValidationChains.org_entity_id IN' => $this->tenantChilds]
                    : []
            )
            ->orderBy('ValidationChains.id')
            ->contain(
                [
                    'OrgEntities',
                    'ValidationStages' => fn(Query $q) => $q->orderBy(['ord']),
                ]
            )
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'name' => $entity->get('name'),
                        'description' => $entity->get('description'),
                        'app_type' => $entity->get('app_type'),
                        'archival_agency' => Hash::get($entity, 'org_entity.identifier'),
                        'stages' => array_map(
                            fn($i) => 'validation_stage_' . $i,
                            Hash::extract($entity, 'validation_stages.{n}.id')
                        ),
                        'is_default' => (int)$entity->get('default'),
                        'is_active' => (int)$entity->get('active'),
                    ];
                }
            )
            ->toArray();
        $this->data['validations'] = array_reduce(
            $validationChains,
            fn($c, $i) => $c + ['validation_' . $i['_id'] => $i],
            []
        );

        // profiles
        $profiles = $loc->get('Profiles')->find()
            ->where(
                $this->tenantChilds
                    ? ['Profiles.org_entity_id IN' => $this->tenantChilds]
                    : []
            )
            ->orderBy('Profiles.id')
            ->contain(['OrgEntities'])
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'identifier' => $entity->get('identifier'),
                        'name' => $entity->get('name'),
                        'description' => $entity->get('description'),
                        'active' => (int)$entity->get('active'),
                        'archival_agency' => Hash::get($entity, 'org_entity.identifier'),
                    ];
                }
            )
            ->toArray();
        $this->data['profiles'] = array_reduce(
            $profiles,
            fn($c, $i) => $c + ['profile_' . $i['_id'] => $i],
            []
        );

        // service levels
        $serviceLevels = $loc->get('ServiceLevels')->find()
            ->where(
                $this->tenantChilds
                    ? ['ServiceLevels.org_entity_id IN' => $this->tenantChilds]
                    : []
            )
            ->orderBy('ServiceLevels.id')
            ->contain(
                [
                    'OrgEntities',
                    'SecureDataSpaces',
                    'TsMsgs' => ['OrgEntities', 'Timestampers'],
                    'TsPjss' => ['OrgEntities', 'Timestampers'],
                    'TsConvs' => ['OrgEntities', 'Timestampers'],
                ]
            )
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'identifier' => $entity->get('identifier'),
                        'name' => $entity->get('name'),
                        'description' => $entity->get('description'),
                        'secure_data_space' => 'secure_data_space_' . $entity->get('secure_data_space_id'),
                        'archival_agency' => Hash::get($entity, 'org_entity.identifier'),
                        'default' => (int)$entity->get('default_level'),
                        'active' => (int)$entity->get('active'),
                        'timestamp_messages' => Hash::get($entity, 'ts_msg')
                            ? [
                                'archival_agency' => Hash::get($entity, 'ts_msg.org_entity.identifier'),
                                'timestamper' => Hash::get($entity, 'ts_msg.timestamper.name'),
                            ]
                            : null,
                        'timestamp_attachments' => Hash::get($entity, 'ts_pjs')
                            ? [
                                'archival_agency' => Hash::get($entity, 'ts_pjs.org_entity.identifier'),
                                'timestamper' => Hash::get($entity, 'ts_pjs.timestamper.name'),
                            ]
                            : null,
                        'timestamp_conversions' => Hash::get($entity, 'ts_conv')
                            ? [
                                'archival_agency' => Hash::get($entity, 'ts_conv.org_entity.identifier'),
                                'timestamper' => Hash::get($entity, 'ts_conv.timestamper.name'),
                            ]
                            : null,
                        'convert_preservation' => $entity->get('convert_preservation'),
                        'convert_diffusion' => $entity->get('convert_diffusion'),
                        'commitment' => $entity->get('commitment'),
                    ];
                }
            )
            ->toArray();
        $this->data['service_levels'] = array_reduce(
            $serviceLevels,
            fn($c, $i) => $c + ['service_level_' . $i['_id'] => $i],
            []
        );

        // agreements
        $agreements = $loc->get('Agreements')->find()
            ->where(
                $this->tenantChilds
                    ? ['Agreements.org_entity_id IN' => $this->tenantChilds]
                    : []
            )
            ->orderBy(
                [
                    'Agreements.default_agreement' => 'desc', // pour la validation custom.default_agreement
                    'Agreements.id' => 'asc',
                ]
            )
            ->contain(
                [
                    'OrgEntities',
                    'ImproperChains',
                    'Profiles',
                    'ProperChains',
                    'OriginatingAgencies',
                    'ServiceLevels',
                    'TransferringAgencies',
                ]
            )
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'identifier' => $entity->get('identifier'),
                        'name' => $entity->get('name'),
                        'description' => $entity->get('description'),
                        'active' => (int)$entity->get('active'),
                        'allow_all_transferring_agencies' => (int)$entity->get('allow_all_transferring_agencies'),
                        'allow_all_originating_agencies' => (int)$entity->get('allow_all_originating_agencies'),
                        'allow_all_service_levels' => (int)$entity->get('allow_all_service_levels'),
                        'auto_validate' => (int)$entity->get('auto_validate'),
                        'archival_agency' => Hash::get($entity, 'org_entity.identifier'),
                        'date_begin' => $entity->get('date_begin'),
                        'date_end' => $entity->get('date_end'),
                        'default' => (int)$entity->get('default_agreement'),
                        'validation_conform' => Hash::get($entity, 'proper_chain.id')
                            ? 'validation_' . Hash::get($entity, 'proper_chain.id')
                            : null,
                        'validation_not_conform' => Hash::get($entity, 'improper_chain.id')
                            ? 'validation_' . Hash::get($entity, 'improper_chain.id')
                            : null,
                        'allowed_transferring_agencies' => array_map(
                            fn($i) => 'org_entity_' . $i,
                            Hash::extract($entity, 'transferring_agencies.{n}.id')
                        ),
                        'allowed_originating_agencies' => array_map(
                            fn($i) => 'org_entity_' . $i,
                            Hash::extract($entity, 'originating_agencies.{n}.id')
                        ),
                        'allowed_service_levels' =>
                            array_map(
                                fn($i) => 'service_level_' . $i,
                                Hash::extract($entity, 'service_levels.{n}.id')
                            ),
                        'allowed_profiles' => array_map(
                            fn($i) => 'profile_' . $i,
                            Hash::extract($entity, 'profiles.{n}.id')
                        ),
                        'allowed_formats' => $entity->get('allowed_formats'),
                        'max_tranfers' => $entity->get('max_tranfers'),
                        'transfer_period' => $entity->get('transfer_period'),
                        'max_size_per_transfer' => $entity->get('max_size_per_transfer'),
                        'allow_all_profiles' => (int)$entity->get('allow_all_profiles'),
                    ];
                }
            )
            ->toArray();
        $this->data['agreements'] = array_reduce(
            $agreements,
            fn($c, $i) => $c + ['agreement_' . $i['_id'] => $i],
            []
        );

        // configurations
        $configurations = $loc->get('Configurations')->find()
            ->innerJoinWith('OrgEntities')
            ->innerJoinWith('OrgEntities.TypeEntities')
            ->where(
                $this->tenantId
                    ? ['Configurations.org_entity_id' => $this->tenantId]
                    : ['TypeEntities.code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES]
            )
            ->orderBy('Configurations.id')
            ->contain(['OrgEntities'])
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'name' => $entity->get('name'),
                        'setting' => $entity->get('setting'),
                        'org_entity' => Hash::get($entity, 'org_entity.identifier'),
                    ];
                }
            )
            ->toArray();
        $this->data['configurations'] = array_reduce(
            $configurations,
            fn($c, $i) => $c + ['configuration_' . $i['_id'] => $i],
            []
        );

        // keywords
        $keywordLists = $loc->get('KeywordLists')->find()
            ->where(
                $this->tenantChilds
                    ? ['KeywordLists.org_entity_id IN' => $this->tenantChilds]
                    : []
            )
            ->orderBy('KeywordLists.id')
            ->contain(['OrgEntities'])
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'identifier' => $entity->get('identifier'),
                        'name' => $entity->get('name'),
                        'description' => $entity->get('description'),
                        'active' => (int)$entity->get('active'),
                        'version' => $entity->get('version'),
                        'org_entity' => Hash::get($entity, 'org_entity.identifier'),
                    ];
                }
            )
            ->toArray();
        $this->data['keyword_lists'] = array_reduce(
            $keywordLists,
            fn($c, $i) => $c + ['keyword_list_' . $i['_id'] => $i],
            []
        );

        $keywords = $loc->get('Keywords')->find()
            ->where(
                $this->tenantChilds
                    ? ['KeywordLists.org_entity_id IN' => $this->tenantChilds]
                    : []
            )
            ->orderBy('Keywords.id')
            ->contain(['KeywordLists', 'ParentKeywords'])
            ->all()
            ->map(
                function (EntityInterface $entity): array {
                    return [
                        '_id' => $entity->get('id'),
                        'version' => $entity->get('version'),
                        'code' => $entity->get('code'),
                        'name' => $entity->get('name'),
                        'exact_match' => $entity->get('exact_match'),
                        'change_note' => $entity->get('change_note'),
                        'keyword_list' => 'keyword_list_' . Hash::get($entity, 'keyword_list.id'),
                        'parent_keyword_name' => Hash::get($entity, 'parent_keyword.name'),
                        'parent_keyword_version' => Hash::get($entity, 'parent_keyword.version'),
                    ] + ($entity->get('imported_from')
                            ? ['imported_from' => $entity->get('imported_from')]
                            : []);
                }
            )
            ->toArray();
        $this->data['keywords'] = array_reduce(
            $keywords,
            fn($c, $i) => $c + ['keyword_' . $i['_id'] => $i],
            []
        );

        // permissions (à la fin, juste avant les files, pour rendre le fichier un peu plus lisible)
        $this->data['permissions'] = $this->getPerms();
    }

    /**
     * Récupération des permissions, rangées par nom de rôle
     * @return array
     * @throws Exception
     */
    private function getPerms(): array
    {
        $Roles = TableRegistry::getTableLocator()->get('Roles');
        $Acos = TableRegistry::getTableLocator()->get('Acos');

        $query = $Roles->find()
            ->where(
                $this->tenantChilds
                    ? ['Roles.org_entity_id IN' => $this->tenantChilds]
                    : ['Roles.org_entity_id IS NOT' => null]
            )
            ->contain(['Aros' => ['ArosAcos' => ['Acos']]])
            ->orderBy(['Roles.lft']);
        $export = [];
        /** @var Role $role */
        foreach ($query as $role) {
            $export['role_' . $role->get('id')] = [];
            /** @var ArosAco $perm */
            foreach (Hash::get($role, 'aro.aros_acos', []) as $perm) {
                $access = [];
                foreach (['_create', '_read', '_update', '_delete'] as $crud) {
                    $access[$crud] = $perm->get($crud);
                }
                $q = $Acos->find()
                    ->select(['parents.alias'])
                    ->where(['Acos.id' => $perm->get('aco_id')])
                    ->innerJoin(
                        ['parents' => 'acos'],
                        [
                            'parents.lft <=' => new IdentifierExpression('Acos.lft'),
                            'parents.rght >=' => new IdentifierExpression('Acos.rght'),
                        ]
                    )
                    ->orderByAsc('parents.lft')
                    ->all()
                    ->map(fn(Aco $entity) => $entity->get('parents')['alias']);
                $path = implode('/', $q->toArray());
                $export['role_' . $role->get('id')][$path] = $access;
            }
        }
        $controllers = [];
        $apis = [];
        $basePath = Configure::read('App.namespace') . '\\Controller\\';
        $appControllerMethods = get_class_methods($basePath . "AppController");
        foreach (CoreApplication::getControllers() as $controllerClass) {
            /** @var string|Controller $controllerClass */
            $methods = get_class_methods($controllerClass);
            $arrayName = explode('\\', $controllerClass);
            $name = end($arrayName);
            $controllerName = substr(
                $name,
                0,
                strlen($name) - strlen('Controller')
            );

            // get_class_methods() envoi les methodes protégés dans ce contexte
            if ($controllerName === 'Devs') {
                $methods = ['index'];
            }

            $controllers[$controllerName] = array_diff(
                $methods,
                $appControllerMethods
            );
            sort($controllers[$controllerName]);
            if (in_array(ApiInterface::class, class_implements($controllerClass))) {
                $apis[$controllerName] = $controllerClass::getApiActions();
            }
        }
        foreach ($export as $role => $actions) {
            ksort($export[$role]);
            $export[$role] = array_filter(
                $export[$role],
                function ($key) use ($controllers, $apis) {
                    if (preg_match('#^root/api/([^/]+)(?:/([^/]+))?$#', $key, $m)) {
                        $controller = $m[1];
                        $action = $m[2] ?? 'default';
                        if ($action === 'default') {
                            return isset($apis[$controller]);
                        }
                        return isset($apis[$controller]) && in_array(
                            $action,
                            $apis[$controller]
                        );
                    }
                    if (preg_match('#^root/controllers/([^/]+)/([^/]+)$#', $key, $m)) {
                        return isset($controllers[$m[1]])
                            && in_array($m[2], $controllers[$m[1]]);
                    }
                    return true;
                },
                ARRAY_FILTER_USE_KEY
            );
        }
        return $export;
    }

    /**
     * Sauvegarde du fichier
     * @param string $file
     * @param array  $data
     * @return bool
     * @throws Exception
     */
    public static function write(string $file, array $data = []): bool
    {
        $ini = [];
        foreach ($data as $key => $val) {
            if (is_array($val)) {
                $ini[] = "[$key]";
                foreach ($val as $skey => $sval) {
                    self::formatSubLevelArrayIni($sval, $skey, $ini);
                }
            } else {
                $ini[] = $key . ' = ' . self::formatIniValue($val);
            }
            $ini[] = null; // retour à la ligne
        }
        if (!is_dir(dirname($file))) {
            mkdir(dirname($file), 0777, true);
        }
        $fp = fopen($file, 'w');

        return fwrite($fp, implode(PHP_EOL, $ini) . PHP_EOL) && fclose($fp);
    }

    /**
     * Formatage d'un sous tableau pour le .ini
     * @param mixed  $sval
     * @param string $skey
     * @param array  $ini
     * @return void
     */
    protected static function formatSubLevelArrayIni($sval, string $skey, array &$ini): void
    {
        if (is_array($sval)) {
            foreach ($sval as $_skey => $_sval) {
                if (is_numeric($_skey)) {
                    $ini[] = $skey . '[] = ' . self::formatIniValue($_sval);
                } else {
                    $ini[] = $skey . "[$_skey] = " . self::formatIniValue($_sval);
                }
            }
        } else {
            $ini[] = $skey . ' = ' . self::formatIniValue($sval);
        }
    }

    /**
     * Formate les données pour l'export en .ini
     * @param mixed $value
     * @return float|int|string
     */
    protected static function formatIniValue($value)
    {
        return is_numeric($value)
            ? $value
            : (is_array($value)
                ? implode(',', $value)
                : '"' . addcslashes($value ?: '', '"') . '"'
            );
    }
}
