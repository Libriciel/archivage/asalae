<?php

/**
 * Asalae\Command\Patch222Command
 */

namespace Asalae\Command;

use Asalae\Model\Table\ArchivesTable;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Utility\Hash;
use Exception;

/**
 * Patch 2.1.10 compatible 2.2.x
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Patch222Command extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'patch222';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();

        $parser->setDescription(__("Calcul du archive_file_id des transfers"));

        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $Archives = $this->fetchTable('Archives');
        $Transfers = $this->fetchTable('Transfers');

        // mise à jour du chanmp transfers.archive_file_id
        $io->out(__("Mise à jour du champ Transfers.archive_file_id"));
        $query = $Archives
            ->find()
            ->select(['Archives.id'])
            ->where(
                [
                    'Archives.state not in' =>
                        [
                            ArchivesTable::S_CREATING,
                            ArchivesTable::S_DESCRIBING,
                            ArchivesTable::S_STORING,
                            ArchivesTable::S_MANAGING,
                        ],
                ]
            )
            ->disableHydration()
            ->orderByAsc('Archives.id');
        $count = $query->count();
        $updatedCount = 0;
        $mtime = microtime(true);
        foreach ($query as $i => $archiveCursor) {
            $archive = $Archives
                ->find()
                ->contain(
                    [
                        'TransferXmlArchiveFiles' => ['sort' => 'TransferXmlArchiveFiles.id'],
                        'Transfers' => ['sort' => 'Transfers.id'],
                    ]
                )
                ->where(
                    ['Archives.id' => $archiveCursor['id']]
                )
                ->firstOrFail();
            if (microtime(true) - $mtime > 10.0) {
                $mtime = microtime(true);
                $io->out(__("Archives {0}/{1}", $i + 1, $count));
            }
            foreach ($archive->get('transfers') as $key => $transfer) {
                $archiveFileId = Hash::get($archive, "transfer_xml_archive_files.$key.id");
                if (!$archiveFileId) {
                    $io->out(
                        __(
                            "Fichier de description non trouvé en base de données pour l'archive id: {0}",
                            $archive->get('id')
                        )
                    );
                    continue;
                }
                $updatedCount += $Transfers->updateAll(
                    ['archive_file_id' => $archiveFileId],
                    ['id' => $transfer->get('id'), 'archive_file_id IS' => null]
                );
            }
        }
        $io->out(__("Archives {0}/{1}", $count, $count));
        $io->out(__("  {0} transferts mis à jour", $updatedCount));

        $io->success('done');
    }
}
