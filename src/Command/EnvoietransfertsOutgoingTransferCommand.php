<?php

/**
 * Asalae\Command\EnvoietransfertsOutgoingTransferCommand
 */

namespace Asalae\Command;

use Asalae\Model\Entity\ArchivingSystem;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;

/**
 * Envoi par API du transfert sortant
 *
 * @category    Command
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EnvoietransfertsOutgoingTransferCommand extends EnvoietransfertsCommand
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'envoietransferts outgoing_transfer';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserOutgoingTransfer = new ConsoleOptionParser();
        $parserOutgoingTransfer->setDescription(
            __d(
                'envoietransferts_shell',
                "Permet l'envoi d'un transfert sortant"
            )
        );
        $parserOutgoingTransfer->addArgument(
            'outgoing_transfer_id',
            [
                'help' => __d(
                    'envoietransferts_shell',
                    "Id du outgoing_transfer à envoyer"
                ),
                'required' => true,
            ]
        );
        return $parserOutgoingTransfer;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $outgoing_transfer_id = $args->getArgument('outgoing_transfer_id');

        $OutgoingTransfers = TableRegistry::getTableLocator()->get(
            'OutgoingTransfers'
        );
        $outgoingTransfer = $OutgoingTransfers->find()
            ->where(['OutgoingTransfers.id' => $outgoing_transfer_id])
            ->contain(
                [
                    'OutgoingTransferRequests' => [
                        'ArchivingSystems',
                    ],
                ]
            )
            ->firstOrFail();
        $zip = $outgoingTransfer->get('zip');
        if (!is_file($zip)) {
            throw new NotFoundException($zip . ' not found');
        }
        /** @var ArchivingSystem $archivingSystem */
        $archivingSystem = Hash::get(
            $outgoingTransfer,
            'outgoing_transfer_request.archiving_system'
        );
        $response = $archivingSystem->postOutgoingTransfer($outgoingTransfer);
        if (is_array($response)) {
            if (isset($response['success']) && $response['success']) {
                $this->io->out(
                    'response: transfer.id = ' . ($response['id'] ?? null)
                );
                if (!empty($response['uploaded_response'])) {
                    $this->io->out(
                        'uploaded_response: ' . $response['uploaded_response']
                    );
                }
                $this->io->out('done');
            } else {
                if (isset($response['error'])) {
                    $msg = is_array($response['error'])
                        ? var_export($response['error'], true)
                        : $response['error'];
                    $this->io->err($msg);
                } elseif (isset($response['code'])) {
                    $this->io->err('Error code ' . $response['code']);
                }
                if (!empty($response['message'])) {
                    $this->io->err($response['message']);
                }
                $this->abort();
            }
        } else {
            file_put_contents(
                Configure::read(
                    'Envoietransferts.debuglog',
                    LOGS . 'envoietransferts_debug.log'
                ),
                $response
            );
            $this->io->abort('failed');
        }
    }
}
