<?php

/**
 * Asalae\Command\OpenIdConnectCommand
 */

namespace Asalae\Command;

use AsalaeCore\Command\PasswordTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\Utility\Config;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Error\Debugger;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Facilite la connexion OpenID
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OpenIdConnectCommand extends Command
{
    use PasswordTrait;

    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;
    /**
     * @var string
     */
    private string $token_endpoint;
    /**
     * @var string
     */
    private string $userinfo_endpoint;
    /**
     * @var string
     */
    private string $adminToken = '';
    /**
     * @var array
     */
    private array $dataLogApi;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'open_id_connect';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __(
                "Facilite la connexion OpenID, les exemples sont donnés"
                . " pour une connexion via keycloak"
            )
        );
        $parser->addOption(
            'admin_host',
            [
                'help' => __("Hôte pour l'API admin"),
            ]
        );
        $parser->addOption(
            'admin_port',
            [
                'help' => __("Port pour l'API admin"),
            ]
        );
        $parser->addOption(
            'admin_base_url',
            [
                'help' => __("Url de base pour l'API admin"),
            ]
        );
        $parser->addOption(
            'admin_username',
            [
                'short' => 'u',
                'help' => __("Username pour l'accès API admin"),
            ]
        );
        $parser->addOption(
            'admin_password',
            [
                'short' => 'p',
                'help' => __("Password pour l'accès API admin"),
            ]
        );
        $parser->addOption(
            'admin_client_id',
            [
                'help' => __("Client_id pour l'accès API admin (admin-cli)"),
            ]
        );
        /**
         * @see https://www.keycloak.org/docs-api/9.0/rest-api/index.html#_getusers
         */
        $parser->addOption(
            'admin_list_users_endpoint',
            [
                'help' => __("Liste les utilisateurs"),
                'default' => 'users',
            ]
        );
        /**
         * @see https://www.keycloak.org/docs-api/9.0/rest-api/index.html#_getclients
         */
        $parser->addOption(
            'admin_list_clients_endpoint',
            [
                'help' => __("Liste les clients"),
                'default' => 'clients',
            ]
        );
        /**
         * @see https://www.keycloak.org/docs-api/9.0/rest-api/index.html#_getclient
         */
        $parser->addOption(
            'admin_get_client_endpoint',
            [
                'help' => __("Informations sur un client"),
                'default' => 'clients',
            ]
        );
        $parser->addOption(
            'user_username',
            [
                'help' => __("Username pour le test de connexion d'un utilisateur"),
            ]
        );
        $parser->addOption(
            'user_password',
            [
                'help' => __("Password pour le test de connexion d'un utilisateur"),
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;
        $config = (array)Configure::read('OpenIDConnect');

        $this->askBaseUrl($config);
        $this->askAdmin($config);
        $this->askClientId($config);
        $this->askUsername($config);
        $io->out('done');
    }

    /**
     * Demande base_url
     * @param array $config
     * @return void
     * @throws Exception
     */
    private function askBaseUrl(array &$config)
    {
        $client = Utility::get(Client::class);
        $this->io->out("exemple: http://localhost:8080/auth/realms/master");
        do {
            $config['base_url'] = $this->io->ask(
                __("Veuillez saisir l'url de base"),
                $config['base_url'] ?? null
            );
            $config['base_url'] = rtrim($config['base_url'], '/');
            /** @var Client $client */
            $response = $client->get(
                sprintf('%s/.well-known/openid-configuration', $config['base_url'])
            );
            $body = $response->getStringBody();
            $json = json_decode($body, true);
            $this->io->out(Debugger::exportVar($json));
            $success = isset($json['authorization_endpoint'])
                && isset($json['userinfo_endpoint']);
            if ($success && !isset($json['end_session_endpoint'])) {
                $this->io->warning(__("end_session n'a pas été trouvé"));
            }
            if ($success && !isset($json['token_endpoint'])) {
                $this->io->warning(__("token_endpoint n'a pas été trouvé"));
            }
            if (!$success) {
                $this->io->error(
                    sprintf(
                        'ERROR code %d : %s',
                        $response->getStatusCode(),
                        $body ?: '<empty>'
                    )
                );
            }
        } while (!$success);
        $this->token_endpoint = $json['token_endpoint'] ?? '';
        $this->userinfo_endpoint = $json['userinfo_endpoint'] ?? '';
        $this->saveConfig($config);
        $this->io->success(__("base_url défini avec succès."));
    }

    /**
     * Enregistre les modifications dans le app_local.json
     * @param array $configOpenId
     * @return void
     * @throws Exception
     */
    private function saveConfig(array $configOpenId)
    {
        $config = $this->getConfig();
        foreach (Hash::flatten($configOpenId) as $key => $value) {
            if ($value === 'true') {
                $value = true;
            } elseif ($value === 'false') {
                $value = false;
            } elseif ($value === 'null') {
                $value = null;
            } elseif (preg_match('/^\d+\.\d*$/', $value)) {
                $value = (float)$value;
            } elseif (is_numeric($value)) {
                $value = (int)$value;
            }
            $config = Hash::insert($config, "OpenIDConnect.$key", $value);
        }
        ksort($config);
        $configPath = $this->getConfigPath();
        $json = json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        if (!$json) {
            $this->io->abort(__("Erreur lors du parsing JSON"));
        }
        Filesystem::begin();
        Filesystem::dumpFile($configPath, $json);
        Filesystem::commit();
    }

    /**
     * Donne la config
     */
    private function getConfig(): array
    {
        $pathToLocalConfig = $this->getConfigPath();
        if ($pathToLocalConfig) {
            return json_decode(file_get_contents($pathToLocalConfig), true);
        }
        return [];
    }

    /**
     * Donne le chemin vers le fichier de configuration
     * @return string
     */
    private function getConfigPath()
    {
        return Config::getPathToLocal();
    }

    /**
     * Demande la connexion de l'admin keycloak
     * @param array $config
     * @return void
     * @throws Exception
     */
    private function askAdmin(array $config)
    {
        $username = $this->args->getOption('admin_username') ?: $this->io->ask(
            __(
                "Username de l'admin (ne sera pas conservé, permet de "
                . "faciliter la configuration, laisser vide pour saisie manuelle)"
            )
        );
        if ($username) {
            /** @var Client $client */
            $client = Utility::get(Client::class);
            do {
                // Ni ' ni ` ne sont acceptés par enterPassword, même échapé ou doublement échapé
                $prompt = str_replace("'", "´", __("Saisir le mot de passe de l'administrateur {0} :", $username));
                $password = $this->args->getOption('admin_password')
                    ?: $this->enterPassword($prompt);
                $clientId = $this->args->getOption('admin_client_id')
                    ?: $this->io->ask(
                        __(
                            "</question><critical>client_id</critical><question>"
                            . " de l'API administrateur (ne pas mettre le client asalae) (defaut: admin-cli)"
                        ),
                        $clientId ?? 'admin-cli'
                    );
                $response = $client->post(
                    $this->token_endpoint,
                    [
                        'grant_type' => 'password',
                        'username' => $username,
                        'password' => $password,
                        'client_id' => $clientId,
                    ]
                );
                $body = $response->getStringBody();
                $json = json_decode($body, true);
                if (!isset($json['access_token']) && isset($json['error_description'])) {
                    $this->io->error($json['error_description']);
                }
            } while (!isset($json['access_token']));
            $this->adminToken = $json['access_token'];

            preg_match('/(https?:\/\/[^\/:]+):(\d+)\/.*/', $config['base_url'], $m);

            $this->dataLogApi = [
                'grant_type' => 'password',
                'username' => $username,
                'password' => $password,
                'client_id' => $clientId,
                'admin_host' => $this->args->getOption('admin_host')
                    ?: ($m[1] ?? 'https://localhost'),
                'admin_port' => $this->args->getOption('admin_port')
                    ?: ($m[2] ?? '8080'),
                'admin_base_url' => $this->args->getOption('admin_base_url')
                    ?: 'admin/realms/master',
            ];
            $this->io->warning(__("Connecté sur l'API en temps que ''{0}''", $username));
        }
    }

    /**
     * Demande base_url
     * @param array $config
     * @return void
     * @throws Exception
     */
    private function askClientId(array &$config)
    {
        $clients = [];
        if ($this->adminToken) {
            $json = $this->apiAdmin('admin_list_clients_endpoint');
            if (isset($json['error'])) {
                $this->io->error($json['error']);
            }
            foreach ($json as $client) {
                if (isset($client['clientId'])) {
                    $clients[$client['clientId']] = $client;
                }
            }
        }
        // si on est connecté à l'API admin
        if ($clients) {
            $config['client_id'] = $this->io->askChoice(
                __("Veuillez saisir le </question><critical>client_id</critical><question>"),
                array_keys($clients),
                $config['client_id'] ?? null
            );

            // Récupère le secret du client choisi
            $clientSecret = $this->apiAdmin(
                'admin_get_client_endpoint',
                $clients[$config['client_id']]['id'] . '/client-secret'
            );
            if ($clientSecret && isset($clientSecret['value'])) {
                $secret = $clientSecret['value'];
                $config['client_secret'] = $secret;
            }
        } else {
            $config['client_id'] = $this->io->ask(
                __("Veuillez saisir le </question><critical>client_id</critical><question>"),
                $config['client_id'] ?? null
            );
        }
        if (!isset($secret)) {
            $config['client_secret'] = $this->io->ask(
                __(
                    "Veuillez saisir le </question><critical>client_secret"
                    . "</critical><question> (optionnel: taper 'null' pour le retirer)"
                ),
                $config['client_secret'] ?? null
            );
            if ($config['client_secret'] === 'null') {
                $config['client_secret'] = null;
            }
        }
        $config['redirect_uri'] = $this->io->ask(
            __(
                "Veuillez saisir le </question><critical>redirect_uri</critical><question> (optionnel: taper"
                . " 'null' pour le retirer) (valeur par défaut: 'auto')"
            ),
            $config['redirect_uri'] ?? null
        );
        $config['scope'] = $this->io->ask(
            __(
                "Veuillez saisir le </question><critical>scope</critical><question> (optionnel: taper"
                . " 'null' pour le retirer) (valeur par défaut: 'openid')"
            ),
            $config['scope'] ?? null
        );
        $this->saveConfig($config);
    }

    /**
     * Appel d'api admin
     * @param string $endpoint
     * @param string $arg
     * @return array
     * @throws Exception
     */
    private function apiAdmin(string $endpoint, string $arg = ''): array
    {
        $this->apiAdminLogAgain();
        /** @var Client $client */
        $client = Utility::get(Client::class);
        $response = $client->get(
            $url = sprintf(
                '%s:%d/%s/%s%s',
                $this->dataLogApi['admin_host'],
                $this->dataLogApi['admin_port'],
                $this->dataLogApi['admin_base_url'],
                $this->args->getOption($endpoint),
                $arg ? '/' . $arg : ''
            ),
            [],
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->adminToken,
                ],
            ]
        );
        $this->io->out($response->getStatusCode() . ' ' . $url);
        return json_decode($response->getStringBody(), true) ?: [];
    }

    /**
     * Redemande un jeton de connexion
     * @return void
     * @throws Exception
     */
    private function apiAdminLogAgain()
    {
        /** @var Client $client */
        $client = Utility::get(Client::class);
        $response = $client->post($this->token_endpoint, $this->dataLogApi);
        $body = $response->getStringBody();
        $json = json_decode($body, true);
        $this->adminToken = $json['access_token'];
    }

    /**
     * Tentative de connexion et définition du username
     * @param array $config
     * @return void
     * @throws Exception
     */
    private function askUsername(array &$config)
    {
        /** @var Client $client */
        $client = Utility::get(Client::class);
        $this->io->out(__("Tentative de connexion d'un utilisateur"));
        do {
            $username = $this->args->getOption('user_username')
                ?: $this->io->ask('Username', $username ?? '');
            $password = $this->args->getOption('user_password')
                ?: $this->enterPassword('Password');
            $response = $client->post(
                $this->token_endpoint,
                [
                    'grant_type' => 'password',
                    'username' => $username,
                    'password' => $password,
                ]
                + $config
            );
            $body = $response->getStringBody();
            $json = json_decode($body, true);
            $failed = !$json || !isset($json['access_token']);
            if ($failed) {
                $this->io->err(
                    sprintf("error code %d:\n%s", $response->getStatusCode(), $body)
                );
            }
        } while ($failed);
        $this->io->warning(__("Connecté en temps que ''{0}''", $username));

        $response = $client->get(
            $this->userinfo_endpoint,
            [],
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $json['access_token'],
                ],
            ]
        );
        $this->io->out($response->getStatusCode() . ' ' . $this->userinfo_endpoint);
        $userinfo = array_filter(
            json_decode($response->getStringBody(), true) ?: [],
            'is_string'
        );
        $this->io->out(Debugger::exportVar($userinfo));
        $config['username'] = $this->io->askChoice(
            __("Choisir le champ qui sera utilisé pour se logger sur l'application"),
            array_keys($userinfo),
            $config['username'] ?? 'sub'
        );
        $this->saveConfig($config);
    }
}
