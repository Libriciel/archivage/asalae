<?php

/**
 * Asalae\Command\InstallCommand
 */

namespace Asalae\Command;

use Asalae\Model\Entity\Eaccpf;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Table\AcosTable;
use Asalae\Model\Table\ArosAcosTable;
use Asalae\Model\Table\ArosTable;
use Asalae\Model\Table\EaccpfsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\TypeEntitiesTable;
use Asalae\Model\Table\UsersTable;
use AsalaeCore\Command\CreateAcosTrait;
use AsalaeCore\Command\PromptOptionsTrait;
use AsalaeCore\Console\AppConsoleOptionParser;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Exception\DatabaseException;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Error\Debugger;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\Table;
use Exception;
use PDOException;

/**
 * Facilite l'installation initale de Asalae 2
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property UsersTable        $Users
 * @property OrgEntitiesTable  $OrgEntities
 * @property TypeEntitiesTable $TypeEntities
 * @property EaccpfsTable      $Eaccpfs
 * @property ArosAcosTable     $Permissions
 * @property ArosTable         $Aros
 * @property AcosTable         $Acos
 */
class InstallCommand extends Command
{
    /**
     * Traits
     */
    use PromptOptionsTrait;
    use CreateAcosTrait;

    /**
     * @var string ApiInterface::class
     */
    public const string API_INTERFACE = '\\AsalaeCore\\Controller\\ApiInterface';
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;
    private Table|ArosAcosTable $Permissions;
    private Table|ArosTable $Aros;
    private Table|AcosTable $Acos;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'install';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();
        $parser->addOption(
            'se-name',
            [
                'help' => __d('install_shell', "Nom du service d'exploitation"),
            ]
        );
        $parser->addOption(
            'se-id',
            [
                'help' => __d(
                    'install_shell',
                    "Identifiant du service d'exploitation"
                ),
            ]
        );
        $parser->addOption(
            'cpf-agent',
            [
                'help' => __d(
                    'install_shell',
                    "Nom du créateur de la notice d'autorité (vous-même)"
                ),
            ]
        );
        return $parser;
    }

    /**
     * Vérifie que la base de donnée est accessible à partir des paramètres saisis
     * dans la configuration
     *
     * @return boolean
     */
    private function checkConnection(): bool
    {
        try {
            $conn = ConnectionManager::get('default');
            $conn->execute('select 1');
            return true;
        } catch (PDOException | MissingConnectionException) {
            return false;
        }
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        if (!is_dir(TMP)) {
            mkdir(TMP);
        }
        if (!is_dir(Configure::read('App.paths.logs', LOGS))) {
            mkdir(Configure::read('App.paths.logs', LOGS));
        }
        $this->checkConfig();
        $this->checkDatabase();

        /** @var Connection $conn */
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $this->createServiceExploitation();
        $this->initializePermissions();
        $conn->commit();
    }

    /**
     * Vérification du fichier de configuration
     */
    private function checkConfig()
    {
        $this->io->out(
            __d('install', "Vérification du fichier de configuration...")
        );
        $path = Configure::read('App.paths.path_to_local_config');
        if (is_file($path)) {
            $configFile = include $path;
            if (is_file($configFile)) {
                $this->io->out(__d('install', "Fichier de configuration trouvé."));
            } else {
                $this->io->abort(
                    __d(
                        'install',
                        "Erreur, impossible de trouver le fichier de configuration !"
                    )
                );
            }
        } else {
            $this->io->abort(
                __d(
                    'install',
                    "Il semble que l'application n'a pas été configurée. "
                    . "Merci de lancer le shell `Configuration` avant de lancer l'installation."
                )
            );
        }
    }

    /**
     * Vérification de l'état de la base de données (connexion et migrations)
     */
    private function checkDatabase()
    {
        $this->io->out(
            __d('install', "Tentative de connexion à la base de donnée...")
        );
        if (!$this->checkConnection()) {
            $this->io->out(
                __d(
                    'install',
                    "échec de connexion. merci de vérifier le fichier {0}",
                    "config/app_local.json"
                )
            );
            exit;
        }
        $this->io->success(__d('install', "Connexion réussie."));
        $this->io->out(
            __d('install', "Vérifications de l'existance de la migration...")
        );
        while (
            !is_file(
                CONFIG . 'Migrations' . DS . 'schema-dump-default.lock'
            )
        ) {
            $this->io->out(
                __d(
                    'install',
                    "Vous devez effectuer la migration pour continuer."
                )
            );
            $this->io->out("bin/cake migrations migrate");
            $this->io->ask(__d('install', "OK..."));
        }
        do {
            $error = null;
            try {
                $Phinxlog = $this->fetchTable('Phinxlog');
                $Phinxlog->find()->first();
            } catch (DatabaseException $e) {
                $error = $e->getMessage();
                $this->io->err($error);
                $this->io->out(
                    __d(
                        'install',
                        "Vous devez effectuer la migration pour continuer."
                    )
                );
                $this->io->out("bin/cake migrations migrate");
                $this->io->ask(__d('install', "OK..."));
            }
        } while ($error);
        $this->io->success(__d('install', "Migration ok"));
    }

    /**
     * Initialise le service d'exploitation
     * @return OrgEntity Service d'exploitation
     * @throws Exception
     */
    private function createServiceExploitation(): OrgEntity
    {
        $OrgEntities = $this->fetchTable('OrgEntities');
        $TypeEntities = $this->fetchTable('TypeEntities');
        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = $this->fetchTable('Eaccpfs');
        /** @var OrgEntity $se */
        $se = $OrgEntities->find()->where(['code' => 'SE'])->contain(
            ['TypeEntities']
        )->first();
        if ($se) {
            $this->io->out(__d('install', "Service d'exploitation ok"));
            return $se;
        }
        $type_entity_id = $TypeEntities->find()
            ->select(['id'])
            ->where(['code' => 'SE'])
            ->firstOrFail()
            ->get('id');
        /** @var OrgEntity $se */
        $se = $this->createEntity(
            $OrgEntities,
            [
                'name' => [
                    'message' => __d(
                        'install',
                        "Veuillez saisir un nom pour le service d'exploitation"
                    ),
                    'option' => 'se-name',
                ],
                'identifier' => [
                    'message' => __d(
                        'install',
                        "Veuillez saisir son identifiant unique"
                    ),
                    'option' => 'se-id',
                ],
            ],
            compact('type_entity_id')
        );
        $this->io->success(__d('install', "Entité créée avec succès"));

        $from_date = new CakeDateTime();
        $from_date->setTimezone(Configure::read('App.timezone', 'UTC'));
        $initialData = [
            'name' => $se->get('name'),
            'record_id' => $se->get('identifier'),
            'agency_name' => $se->get('name'),
            'org_entity_id' => $se->get('id'),
            'entity_type' => 'corporateBody',
            'from_date' => $from_date,
        ];
        $data = $initialData;
        $username = $this->args->getOption('cpf-agent');
        do {
            /** @var Eaccpf $entity */
            $entity = $Eaccpfs->newEntity($data);
            if (empty($username)) {
                $username = $this->io->ask(
                    __d(
                        'install',
                        "Votre nom (case 'agent' du 'maintenanceHistory' pour l'évenement de création)"
                    )
                );
            }
            $Eaccpfs->initializeData($entity, ['name' => $username]);
            if ($err = $entity->getErrors()) {
                foreach ($err as $field => $error) {
                    $this->io->err(
                        __d(
                            'install',
                            "Erreur sur le champ ''{0}'': {1}",
                            $field,
                            current($error)
                        )
                    );
                }
                $username = null;
            }
            $success = $Eaccpfs->save($entity);
            if (!$success) {
                $this->io->err(Debugger::exportVar($entity->getErrors()));
            }
        } while (!$success);
        $this->io->success(__d('install', "Notice d'autorité créée avec succès"));

        return $se;
    }

    /**
     * Crée une entité de façon générique
     *
     * ex:
     * $model = $this->OrgEntities
     * $fields = [
     *      'name' => ['message' => 'Saisir un nom', 'option' => 'username']
     * ]
     * @param Table $model
     * @param array $fields
     * @param array $initialData
     * @return EntityInterface
     * @throws Exception
     */
    private function createEntity(
        Table $model,
        array $fields,
        array $initialData = []
    ): EntityInterface {
        $data = $initialData;
        foreach ($fields as $field => $params) {
            if (!empty($params['option'])) {
                $data[$field] = $this->args->getOption($params['option']);
            }
        }
        do {
            foreach ($fields as $field => $params) {
                if (!empty($data[$field])) {
                    continue;
                }
                $value = !empty($params['type']) && $params['type'] === 'password'
                    ? $this->enterPassword($params['message'])
                    : $this->io->ask($params['message']);
                $data[$field] = $value;
                if (empty($params['copy'])) {
                    continue;
                }
                foreach ($params['copy'] as $field) {
                    $data[$field] = $value;
                }
            }
            $entity = $model->newEntity($data);
            if ($err = $entity->getErrors()) {
                foreach ($err as $field => $error) {
                    $this->io->err(
                        __d(
                            'install',
                            "Erreur sur le champ ''{0}'': {1}",
                            $field,
                            current($error)
                        )
                    );
                }
                $data = $initialData;
            }
        } while (!$model->save($entity));
        return $entity;
    }

    /**
     * Créé les acos et assure l'accès à l'application de l'administrateur
     * @throws Exception
     */
    private function initializePermissions()
    {
        $this->Aros = $this->fetchTable('Aros');
        $this->Acos = $this->fetchTable('Acos');
        $this->Permissions = $this->fetchTable('ArosAcos');
        $this->io->out(__d('install', "Initialisation en cours..."));
        $this->createControllerAcos();
        $this->io->success(__d('install', "L'application a été installée."));
        $this->io->out(__d('install', "Initialisation de l'api REST..."));
        $this->createApiAcos();
        $this->io->success(__d('install', "Api REST installée."));
    }
}
