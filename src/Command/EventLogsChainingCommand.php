<?php

/**
 * Asalae\Command\EventLogsChainingCommand
 */

namespace Asalae\Command;

use Asalae\Model\Entity\StoredFile;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\TechnicalArchivesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Utility\DOMUtility;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use DOMDocument;

/**
 * Vérification du chaînage des cycles de vie
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2023, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EventLogsChainingCommand extends Command
{
    /**
     * @var VolumeManager[]
     */
    private $volumeManagers = [];

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'event_logs chaining';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();
        $parser->setDescription(
            __("Vérification du chaînage des journaux des événements")
        );
        $parser->addArgument(
            'archival_agency_identifier',
            [
                'help' => __("Identifiant du Service d'Archives"),
                'required' => false,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io): int
    {
        $loc = TableRegistry::getTableLocator();

        $archivalAgencyIdentifier = $args->getArgument('archival_agency_identifier');

        $success = true;

        if ($archivalAgencyIdentifier) {
            $sa = $loc->get('OrgEntities')
                ->find()
                ->innerJoinWith('TypeEntities')
                ->where(
                    [
                        'identifier' => $archivalAgencyIdentifier,
                        'TypeEntities.code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES,
                    ]
                )
                ->firstOrFail();

            $success = $this->checkOneArchivingSystem($sa, $io);
        } else {
            $querySa = $loc->get('OrgEntities')
                ->find()
                ->innerJoinWith('TypeEntities')
                ->where(
                    [
                        'TypeEntities.code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES,
                    ]
                )
                ->all();

            $loc->get('OrgEntities')
                ->find()
                ->innerJoinWith('ArchivalAgencies')
                ->where(['OrgEntities.id' => 5, 'ArchivalAgencies.id' => 2])
                ->first();

            foreach ($querySa as $sa) {
                $success = $this->checkOneArchivingSystem($sa, $io) && $success;
            }
        }

        if (!$success) {
            $io->error('fail');
            return static::CODE_ERROR;
        }
        $io->success(__("Tous les fichiers ont été vérifiés avec succès"));

        return static::CODE_SUCCESS;
    }

    /**
     * @param EntityInterface $sa
     * @param ConsoleIo       $io
     * @return bool
     * @throws VolumeException
     */
    public function checkOneArchivingSystem(EntityInterface $sa, ConsoleIo $io): bool
    {
        $io->out(
            __(
                "Vérification des cycles de vie pour le Service d'Archives {0}",
                $sa->get('identifier')
            )
        );

        if ($errors = $this->chainingCheck($sa)) {
            foreach ($errors as $filename => $errorTypes) {
                foreach ($errorTypes as $errorType => $error) {
                    $io->out(
                        __(
                            "Erreur <error>{0}</error> sur le fichier <error>{1}</error> : "
                            . "réelle = <error>{2}</error>, valeur du chaînage = <error>{3}</error>",
                            $errorType,
                            $filename,
                            $error['prev'],
                            $error['current']
                        )
                    );
                }
            }
        } else {
            $io->out(__("Pas d'erreurs pour le Service d'Archives {0}", $sa->get('identifier')));
        }

        return empty($errors);
    }

    /**
     * Vérification du chaînage des journaux
     * @param EntityInterface $sa
     * @return array filename => type_d_erreur => [prev => $prevVal, current => $currentVal]
     * @throws VolumeException
     */
    public function chainingCheck(EntityInterface $sa): array
    {
        $loc = TableRegistry::getTableLocator();
        $StoredFiles = $loc->get('StoredFiles');
        $query = $StoredFiles->find()
            ->innerJoinWith('ArchiveBinaries')
            ->innerJoinWith('ArchiveBinaries.TechnicalArchiveUnits')
            ->innerJoinWith('ArchiveBinaries.TechnicalArchiveUnits.TechnicalArchives')
            ->where(
                [
                    'ArchiveBinaries.type' => 'original_data',
                    'TechnicalArchives.type' => TechnicalArchivesTable::TYPE_EVENTS_LOGS,
                    'TechnicalArchives.archival_agency_id' => $sa->get('id'),
                ]
            )
            ->orderBy(
                [
                    'TechnicalArchives.created' => 'asc',
                    'ArchiveBinaries.filename' => 'asc',
                ]
            );

        $prevName = null;
        $prevHash = null;
        $prevSize = null;
        $errors = [];

        /** @var StoredFile $file */
        foreach ($query as $file) {
            $volumeManager = $this->getVolumeManager($file->get('secure_data_space_id'));
            $filename = $file->get('name');
            $content = $volumeManager->fileGetContent($filename);

            $currentHash = hash($file->get('hash_algo'), $content);
            $currentSize = strlen($content);

            $dom = new DOMDocument();
            $dom->loadXML($content);
            $util = new DOMUtility($dom);
            $node = $util->node(
                'ns:object/ns:objectIdentifier/ns:objectIdentifierValue[text()="EventsLogsChain"]/../..'
            );

            if (!$node) { // log initial
                $prevHash = $currentHash;
                $prevSize = $currentSize;
                $prevName = basename($filename);
                continue;
            }

            $xmlName = (string)$util->nodeValue(
                './/ns:significantPropertiesType[text()="filename"]/following-sibling::*',
                $node
            );
            $xmlHash = (string)$util->nodeValue(
                './/ns:significantPropertiesType[text()="hash"]/following-sibling::*',
                $node
            );
            $xmlSize = (int)$util->nodeValue(
                './/ns:significantPropertiesType[text()="size"]/following-sibling::*',
                $node
            );

            if ($prevName !== $xmlName) {
                $errors[$filename]['name'] = [
                    'prev' => $prevName,
                    'current' => $xmlName,
                ];
            }
            if ($prevHash !== $xmlHash) {
                $errors[$filename]['hash'] = [
                    'prev' => $prevHash,
                    'current' => $xmlHash,
                ];
            }
            if ($prevSize !== $xmlSize) {
                $errors[$filename]['size'] = [
                    'prev' => $prevSize,
                    'current' => $xmlSize,
                ];
            }

            $prevHash = $currentHash;
            $prevSize = $currentSize;
            $prevName = basename($filename);
        }

        return $errors;
    }

    /**
     * Donne l'ECS avec mise en cache
     * @param int $secure_data_space_id
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(int $secure_data_space_id): VolumeManager
    {
        if (!isset($this->volumeManagers[$secure_data_space_id])) {
            $this->volumeManagers[$secure_data_space_id] = new VolumeManager($secure_data_space_id);
        }
        return $this->volumeManagers[$secure_data_space_id];
    }
}
