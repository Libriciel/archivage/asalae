<?php

/**
 * Asalae\Command\RestoreArchivesCommand
 */

namespace Asalae\Command;

use Asalae\Exception\GenericException;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Table\AgreementsTable;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\ProfilesTable;
use Asalae\Model\Table\ServiceLevelsTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Utility\Check;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Utility\Antivirus\AntivirusInterface;
use AsalaeCore\Utility\Antivirus\Clamav;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\PreControlMessages;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\NotImplementedException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateTime;
use DOMElement;
use Exception;
use FileValidator\Utility\FileValidator;
use Libriciel\Filesystem\Utility\Filesystem;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use Throwable;
use DOMDocument;

/**
 * Restauration du registre des entrées et des transferts à partir du stockage des archives
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2023, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RestoreArchivesCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    private $io;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var AgreementsTable
     */
    private $Agreements;

    /**
     * @var OrgEntitiesTable
     */
    private $OrgEntities;

    /**
     * @var ProfilesTable
     */
    private $Profiles;

    /**
     * @var ServiceLevelsTable
     */
    private $ServiceLevels;

    /**
     * @var TransfersTable
     */
    private $Transfers;

    /**
     * @var array
     */
    private $referentielErrors = [];

    /**
     * @var array
     */
    private $getTransferInfosCache = [];

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'restore archives';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(
            __("Permet de restaurer les archives et les transferts à partir du stockage des archives")
        );
        $parser->addArgument(
            'archives_folder',
            [
                'help' => __("Dossier de sauvegarde des archives"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'user',
            [
                'help' => __("Username de l'utilisateur"),
                'required' => true,
            ]
        );
        $parser->addOption(
            'dryrun',
            [
                'help' => __(
                    "Vérifie les paramètres sans importer"
                ),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'no-virus-scan',
            [
                'help' => __(
                    "N'effectue pas le scan antivirus"
                ),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'no-check-prerequisites',
            [
                'help' => __(
                    "Ne vérifie pas les prérequis"
                ),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'max-execution-time',
            [
                'help' => __(
                    "Durée d'exécution maximum du script en secondes (max_execution_time)"
                ),
                'default' => '3600',
            ]
        );
        $parser->addOption(
            'number-to-restore',
            [
                'help' => __(
                    "Nombre d'archives à restaurer"
                ),
                'default' => null,
            ]
        );
        $parser->addOption(
            'resume',
            [
                'help' => __(
                    "Vérifie les paramètres sans importer"
                ),
                'boolean' => true,
            ]
        );

        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     * @throws Exception|Throwable
     */
    public function execute(Arguments $args, ConsoleIo $io): int
    {
        $this->io = $io;

        ini_set('max_execution_time', $args->getOption('max-execution-time'));

        $archivesFolder = realpath($args->getArgument('archives_folder'));
        $this->io->out();
        $this->io->out(__("Restauration des archives présentes dans le répertoire {0}", $archivesFolder));

        if (!is_dir($archivesFolder)) {
            $io->abort(__("Le dossier {0} n'existe pas", $archivesFolder));
        }
        if (!is_readable($archivesFolder)) {
            $io->abort(__("Impossible de lire le dossier {0}", $archivesFolder));
        }

        if (!$args->getOption('no-check-prerequisites')) {
            $this->checkPrerequisites();
        }

        $this->Agreements = $this->fetchTable('Agreements');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $this->Profiles = $this->fetchTable('Profiles');
        $this->ServiceLevels = $this->fetchTable('ServiceLevels');
        $this->Transfers = $this->fetchTable('Transfers');

        $username = $args->getArgument('user');
        $user = $this->fetchTable('Users')->find()
            ->where(
                [
                    'Users.username' => $username,
                ]
            )
            ->contain(['OrgEntities' => ['ArchivalAgencies']])
            ->first();
        if (!$user) {
            $this->io->abort(__("L'utilisateur {0} n'existe pas", $username));
        }
        $this->userId = Hash::get($user, 'id');

        // liste des répertoires de stockage des archives
        $this->io->out(__("lecture des répertoires d'archives"));
        $messyArchiveStorageFolders = $this->getArchiveStorageFolders($archivesFolder);
        $this->io->out("  " . __("détection de {0} répertoires d'archives", count($messyArchiveStorageFolders)));

        // Tri des répertoires de stockage des archives par date de création des archives
        $this->io->out(__("tri par ordre de création des archives"));
        $orderedArchiveStorageFolders = [];
        foreach ($messyArchiveStorageFolders as $i => $archiveStorageFolder) {
            $lifecycleFileUri = $this->getManagementFileUri($archiveStorageFolder, 'lifecycle');
            $eventsDate = $this->getAddEventsDate($lifecycleFileUri);
            $orderKey = $eventsDate['archive_add'] . $eventsDate['transfer_add'] . sprintf('%08d', $i);
            $orderedArchiveStorageFolders[$orderKey] = $archiveStorageFolder;
        }
        $messyArchiveStorageFolders = null;
        ksort($orderedArchiveStorageFolders);
        $this->io->out("  " . __("répertoires d'archives triés"));

        // suppression des transferts déjà présents en base de données
        $archiveStorageFolders = [];
        $this->io->out(__("analyse des répertoires pour lesquels les transferts sont présents en base de données"));
        foreach ($orderedArchiveStorageFolders as $archiveStorageFolder) {
            $transferInfos = $this->getTransferInfos($archiveStorageFolder);
            // lecture du Service d'Archives du transfert
            $sa = $this->getSA($transferInfos['archivalAgencyIdentifier']);
            if (!$sa) {
                $this->io->info(
                    __(
                        "transfert {0} ({1}) Service d'Archives {2} non trouvé en BDD : traitement interrompu",
                        $transferInfos['identifier'],
                        $archiveStorageFolder,
                        $transferInfos['archivalAgencyIdentifier']
                    )
                );
                $this->abort();
            }
            // recherche du transfert en base de données
            $transfer = $this->Transfers->find()
                ->where(
                    [
                        'archival_agency_id' => $sa->get('id'),
                        'transfer_identifier' => $transferInfos['identifier'],
                    ]
                )->first();
            if ($transfer) {
                if ($transfer->get('state') === 'creating' || $transfer->get('state') === 'preparating') {
                    // import précédent interrompu ou en cours dans un autre process
                    if ($args->getOption('resume')) {
                        $this->io->out(
                            "  " .
                            __(
                                "transfert {0} ({1}) trouvé en BDD dans l'état {2} : poursuite du traitement",
                                $transferInfos['identifier'],
                                $archiveStorageFolder,
                                $transfer->get('state')
                            )
                        );
                        $archiveStorageFolders[] = $archiveStorageFolder;
                    } else {
                        $this->io->info(
                            __(
                                "transfert {0} ({1}) trouvé en base de données dans l'état {2} : traitement interrompu",
                                $transferInfos['identifier'],
                                $archiveStorageFolder,
                                $transfer->get('state')
                            )
                        );
                        $this->io->info(
                            __(
                                "utiliser l'option resume pour reprendre la restauration des archives",
                            )
                        );
                        $this->abort();
                    }
                } elseif ($transfer->get('state') === 'archiving' || $transfer->get('state') === 'accepted') {
                    // import en cours de création, on passe au suivant
                    $this->io->out(
                        "  " .
                        __(
                            "transfert {0} : {1} trouvé en base de données dans l'état {2},  retiré du traitement",
                            $transferInfos['identifier'],
                            $archiveStorageFolder,
                            $transfer->get('state')
                        )
                    );
                } else {
                    // état non géré par le script, fin du script
                    $this->io->info(
                        __(
                            "transfert {0} ({1}) trouvé en BDD dans l'état {2} : état non géré, traitement interrompu",
                            $transferInfos['identifier'],
                            $archiveStorageFolder,
                            $transfer->get('state')
                        )
                    );
                    $this->abort();
                }
            } else {
                $archiveStorageFolders[] = $archiveStorageFolder;
            }
            if (
                $args->getOption('number-to-restore') !== null
                && (int)$args->getOption('number-to-restore') === count($archiveStorageFolders)
            ) {
                break;
            }
        }
        $orderedArchiveStorageFolders = null;
        if (count($archiveStorageFolders) === 0) {
            $this->io->info(__("aucun répertoire d'archives à traiter"));
            $this->abort();
        } elseif (count($archiveStorageFolders) === 1) {
            $this->io->out(__("traitement de 1 répertoire d'archives"));
        } else {
            $this->io->out(__("traitement de {0} répertoires d'archives", count($archiveStorageFolders)));
        }

        // vérification du référentiel des transferts et des descriptions archives
        $this->io->out(__("vérification du référentiel des transferts et des archives"));
        foreach ($archiveStorageFolders as $archiveStorageFolder) {
            $transferInfos = $this->getTransferInfos($archiveStorageFolder);
            $sa = $this->checkAndGetSA(
                $transferInfos['archivalAgencyIdentifier'],
                $transferInfos['archivalAgencyName']
            );
            if ($sa) {
                $this->checkEntitiesReferentiel($transferInfos, $sa);
                $this->checkBusinessReferentiel($transferInfos, $sa);
                $descriptionInfos = $this->getDescriptionInfos($archiveStorageFolder);
                $this->checkEntitiesReferentiel($descriptionInfos, $sa);
                $this->checkBusinessReferentiel($descriptionInfos, $sa);
            }
        }
        if ($this->referentielErrors) {
            foreach ($this->referentielErrors as $referentielError) {
                $this->io->warning($referentielError['error']);
                $this->io->warning($referentielError['message']);
            }
            $this->io->abort('Traitement interrompu');
        }

        if ($args->getOption('dryrun')) {
            $this->io->out(__("mode dryrun, aucun traitement effectué"));
        } else {
            $errors = [];
            foreach ($archiveStorageFolders as $archiveStorageFolder) {
                try {
                    $transferInfos = $this->getTransferInfos($archiveStorageFolder);
                    $this->io->out(
                        __(
                            "traitement du répertoire {0} : transfert {1}",
                            $archiveStorageFolder,
                            $transferInfos['identifier']
                        )
                    );

                    $transfer = $this->restoreArchive($archiveStorageFolder, $args);

                    $this->io->out("  " . __("ajout du job de création de l'archive"));
                    $Beanstalk = Utility::get('Beanstalk');
                    $Beanstalk->setTube('archive');
                    $Beanstalk->emit(
                        [
                            'transfer_id' => $transfer->get('id'),
                            'user_id' => $this->userId,
                        ]
                    );
                } catch (Exception $e) {
                    $errors[$archiveStorageFolder] = $e->getMessage();
                }
            }

            if ($errors) {
                $this->io->error(__("Archives en erreur :"));
                foreach ($errors as $path => $error) {
                    $this->io->error($path . ' : ' . $error);
                }
            }
        }

        $this->io->out(__("Fin du traitement"));

        return self::CODE_SUCCESS;
    }

    /**
     * Vérification du fonctionnement de l'appli pour pouvoir continuer
     * @return void
     * @throws Exception
     */
    private function checkPrerequisites(): void
    {
        /** @var Check $Check */
        $Check = Utility::get(Check::class);
        $needInstall = $Check->needInstall();
        if ($needInstall) {
            $this->io->error(__("Prérequis manquant : installation"));
        }
        $database = $Check->database();
        if (!$database) {
            $this->io->error(__("Prérequis manquant : base de données"));
        }
        $beanstalkd = $Check->beanstalkd();
        if (!$beanstalkd) {
            $this->io->error(__("Prérequis manquant : beanstalkd"));
        }
        $clamav = $Check->clamav();
        if (!$clamav) {
            $this->io->error(__("Prérequis manquant : clamav"));
        }
        $php = $Check->phpOk();
        if (!$php) {
            $this->io->error(__("Prérequis manquant : php"));
        }
        $writable = $Check->writableOk();
        if (!$writable) {
            $this->io->error(__("Prérequis manquant : writable"));
        }
        $config = !$Check->configurationErrors();
        if (!$config) {
            $this->io->error(__("Prérequis manquant : config"));
        }
        $volumes = $Check->volumesOK();
        if (!$volumes) {
            $this->io->error(__("Prérequis manquant : volumes"));
        }

        $ok = !$needInstall
            && $database
            && $clamav
            && $beanstalkd
            && $php
            && $writable
            && $config
            && $volumes;

        if (!$ok) {
            $this->io->abort(
                __("Veuillez vérifier le bon fonctionnement de l'application avant de relancer la commande")
            );
        }
    }

    /**
     * Retourne la liste des dossiers d'archives de façon récursive
     *
     * @param string $rootFolder
     * @return array
     */
    private function getArchiveStorageFolders(string $rootFolder): array
    {
        $res = [];
        $globRes = glob($rootFolder . '/*', GLOB_ONLYDIR);
        foreach ($globRes as $folder) {
            if (is_dir($folder . '/management_data')) {
                $res[] = $folder;
            } else {
                $subRes = $this->getArchiveStorageFolders($folder);
                if (!empty($subRes)) {
                    $res = array_merge($res, $subRes);
                }
            }
        }
        return $res;
    }

    /**
     * Retourne selon le type, le chemin du fichier de management d'une archive
     *
     * @param string $archiveStorageFolder
     * @param string $type
     * @return string
     */
    private function getManagementFileUri(string $archiveStorageFolder, string $type): string
    {
        $globRes = glob($archiveStorageFolder . '/management_data/*_' . $type . '.xml');
        if (!$globRes) {
            $this->abort(__("Fichier du transfert non trouvé dans {0}", $archiveStorageFolder));
        }
        return ($globRes[0]);
    }

    /**
     * Retourne les événements transfer_add et archive_add d'une archive
     *
     * @param string $lifecycleFilename
     * @return array
     */
    private function getAddEventsDate(string $lifecycleFilename): array
    {
        $ret = [];
        $eventTypes = [
            'transfer_add' => 'transfer_add',
            'archive_add' => 'archive_add',
        ];
        $domDoc = new DOMDocument();
        $domDoc->load($lifecycleFilename);
        $eventTypeNodes = $domDoc->getElementsByTagName('eventType');
        foreach ($eventTypeNodes as $eventTypeNode) {
            if (isset($eventTypes[$eventTypeNode->nodeValue])) {
                $eventNode = $eventTypeNode->parentNode;
                $eventDateTimeNode = $eventNode->getElementsByTagName('eventDateTime')->item(0);
                $ret[$eventTypeNode->nodeValue] = $eventDateTimeNode->nodeValue;
                unset($eventTypes[$eventTypeNode->nodeValue]);
                if (empty($eventTypes)) {
                    break;
                }
            }
        }
        return $ret;
    }

    /**
     * Retourne une liste d'informations extraites du fichier xml du transfert d'une archive
     *
     * @param string $archiveStorageFolder
     * @return array
     * @throws Exception
     */
    private function getTransferInfos(string $archiveStorageFolder): array
    {
        if (empty($this->getTransferInfosCache[$archiveStorageFolder])) {
            $ret = [];
            $transferFilename = $this->getManagementFileUri($archiveStorageFolder, 'transfer');
            $ret['fileUri'] = $transferFilename;
            $domDoc = new DOMDocument();
            $domDoc->load($transferFilename);
            $util = new DOMUtility($domDoc);
            switch ($util->namespace) {
                case NAMESPACE_SEDA_02:
                    $ret['sedaVersion'] = 'seda02';
                // no break
                case NAMESPACE_SEDA_10:
                    $ret['sedaVersion'] = $ret['sedaVersion'] ?? 'seda10';
                    $infos = [
                        'identifier' => '//ns:TransferIdentifier',
                        'archivalAgencyIdentifier' => '//ns:ArchivalAgency/ns:Identification',
                        'archivalAgencyName' => '//ns:ArchivalAgency/ns:Name',
                        'transferringAgencyIdentifier' => '//ns:TransferringAgency/ns:Identification',
                        'transferringAgencyName' => '//ns:TransferringAgency/ns:Name',
                        'originatingAgencyIdentifier' => '//ns:OriginatingAgency/ns:Identification',
                        'originatingAgencyName' => '//ns:OriginatingAgency/ns:Name',
                        'agreementIdentifier' => '//ns:Archive/ns:ArchivalAgreement',
                        'profileIdentifier' => '//ns:Archive/ns:ArchivalProfile',
                        'serviceLevelIdentifier' => '//ns:Archive/ns:ServiceLevel',
                    ];
                    break;
                case NAMESPACE_SEDA_21:
                    $ret['sedaVersion'] = 'seda21';
                // no break
                case NAMESPACE_SEDA_22:
                    $util->xpath->registerNamespace('seda10', 'fr:gouv:culture:archivesdefrance:seda:v1.0');
                    $ret['sedaVersion'] = $ret['sedaVersion'] ?? 'seda22';
                    $infos = [
                        'identifier' => '//ns:MessageIdentifier',
                        'archivalAgencyIdentifier' => '//ns:ArchivalAgency/ns:Identifier',
                        'archivalAgencyName' => '//ns:ArchivalAgency/ns:OrganizationDescriptiveMetadata/seda10:Name',
                        'transferringAgencyIdentifier' => '//ns:TransferringAgency/ns:Identifier',
                        'transferringAgencyName' =>
                            '//ns:TransferringAgency/ns:OrganizationDescriptiveMetadata/seda10:Name',
                        'originatingAgencyIdentifier' => '//ns:OriginatingAgency/ns:Identifier',
                        'originatingAgencyName' =>
                            '//ns:OriginatingAgency/ns:OrganizationDescriptiveMetadata/seda10:Name',
                        'agreementIdentifier' => '/ns:ArchiveTransfer/ns:ArchivalAgreement',
                        'profileIdentifier' => '//ns:ArchiveUnit/ns:ArchiveUnitProfile',
                        'serviceLevelIdentifier' => '//ns:ManagementMetadata/ns:ServiceLevel',
                    ];
                    break;
                default:
                    throw new Exception(
                        'unsuported namespace: ' . $util->namespace
                    );
            }
            foreach ($infos as $info => $xpath) {
                if ($xpath) {
                    $ret[$info] = $util->nodeValue($xpath) ?? '';
                } else {
                    $ret[$info] = '';
                }
            }
            $this->getTransferInfosCache[$archiveStorageFolder] = $ret;
        }
        return $this->getTransferInfosCache[$archiveStorageFolder];
    }

    /**
     * lecture du Service d'Archives en base de données
     *
     * @param string $archivalAgencyIdentifier
     * @return EntityInterface|null
     */
    private function getSA(string $archivalAgencyIdentifier): ?EntityInterface
    {
        /** @var OrgEntity $sa */
        $sa = $this->OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'Orgentities.identifier' => $archivalAgencyIdentifier,
                    'Orgentities.Active' => true,
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES,
                ]
            )
            ->first();
        return $sa;
    }

    /**
     * vérifie la présence en base de données du Service d'Archives
     *
     * @param string $archivalAgencyIdentifier
     * @param string $archivalAgencyName
     * @return EntityInterface|null
     */
    private function checkAndGetSA(string $archivalAgencyIdentifier, string $archivalAgencyName): ?EntityInterface
    {
        /** @var OrgEntity $sa */
        $sa = $this->getSA($archivalAgencyIdentifier);
        if (!$sa) {
            $refErrorKey = 'entity-' . $archivalAgencyIdentifier;
            if (!array_key_exists($refErrorKey, $this->referentielErrors)) {
                $this->referentielErrors[$refErrorKey] = [
                    'error' => "  " . __(
                        "Service d'Archives {0} non présent en base de données",
                        $archivalAgencyIdentifier
                    ),
                    'message' => "  -> " . __(
                        "ajouter l'entité de type Service d'Archives {0} ({1})",
                        $archivalAgencyIdentifier,
                        $archivalAgencyName
                    ),
                ];
            }
            return null;
        }
        return $sa;
    }

    /**
     * vérifie la présence en base de données du Service Versant et Producteur
     *
     * @param array           $infos
     * @param EntityInterface $sa
     * @return void
     */
    private function checkEntitiesReferentiel(array $infos, EntityInterface $sa): void
    {
        // vérification de la présence du SV
        if (!empty($infos['transferringAgencyIdentifier'])) {
            $sv = $this->OrgEntities->find()
                ->innerJoinWith('TypeEntities')
                ->where(
                    [
                        'Orgentities.identifier' => $infos['transferringAgencyIdentifier'],
                        'OrgEntities.archival_agency_id' => $sa->get('id'),
                        'TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES,
                    ]
                )
                ->first();
            if (!$sv) {
                $refErrorKey = 'entity-' . $infos['transferringAgencyIdentifier'];
                if (!array_key_exists($refErrorKey, $this->referentielErrors)) {
                    $this->referentielErrors[$refErrorKey] = [
                        'error' => "  " . __(
                            "Service versant {0} non présent en base de données",
                            $infos['transferringAgencyIdentifier']
                        ),
                        'message' => "  -> " . __(
                            "ajouter l'entité de type Service Versant {0} ({1})",
                            $infos['transferringAgencyIdentifier'],
                            $infos['transferringAgencyName']
                        ),
                    ];
                }
            }
        }
        // vérification de la présence du SP
        if (!empty($infos['originatingAgencyIdentifier'])) {
            $sp = $this->OrgEntities->find()
                ->innerJoinWith('TypeEntities')
                ->where(
                    [
                        'Orgentities.identifier' => $infos['originatingAgencyIdentifier'],
                        'OrgEntities.archival_agency_id' => $sa->get('id'),
                        'TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES,
                    ]
                )
                ->first();
            if (!$sp) {
                $refErrorKey = 'entity-' . $infos['originatingAgencyIdentifier'];
                if (!array_key_exists($refErrorKey, $this->referentielErrors)) {
                    $this->referentielErrors[$refErrorKey] = [
                        'error' => "  " . __(
                            "Service producteur {0} non présent en base de données",
                            $infos['originatingAgencyIdentifier']
                        ),
                        'message' => "  -> " . __(
                            "ajouter l'entité de type Service Versant ou Producteur {0} ({1})",
                            $infos['originatingAgencyIdentifier'],
                            $infos['originatingAgencyName']
                        ),
                    ];
                }
            }
        }
    }

    /**
     * vérifie la présence en base de données de l'accord, du profil et du niveau de service
     *
     * @param array           $infos
     * @param EntityInterface $sa
     * @return void
     */
    private function checkBusinessReferentiel(array $infos, EntityInterface $sa): void
    {
        // vérification de la présence de l'accord de versement
        if ($infos['agreementIdentifier']) {
            $agreement = $this->Agreements->find()
                ->where(
                    [
                        'Agreements.identifier' => $infos['agreementIdentifier'],
                        'Agreements.org_entity_id' => $sa->get('id'),
                    ]
                )
                ->first();
            if (!$agreement) {
                $refErrorKey = 'agreement-' . $infos['agreementIdentifier'];
                if (!array_key_exists($refErrorKey, $this->referentielErrors)) {
                    $this->referentielErrors[$refErrorKey] = [
                        'error' => "  " . __(
                            "Accord de versement {0} non présent en base de données",
                            $infos['agreementIdentifier']
                        ),
                        'message' => "  -> " . __(
                            "ajouter l'accord de versement {0}",
                            $infos['agreementIdentifier']
                        ),
                    ];
                }
            }
        }
        // vérification de la présence du profil d'archive
        if ($infos['profileIdentifier']) {
            $profile = $this->Profiles->find()
                ->where(
                    [
                        'Profiles.identifier' => $infos['profileIdentifier'],
                        'Profiles.org_entity_id' => $sa->get('id'),
                    ]
                )
                ->first();
            if (!$profile) {
                $refErrorKey = 'profile-' . $infos['profileIdentifier'];
                if (!array_key_exists($refErrorKey, $this->referentielErrors)) {
                    $this->referentielErrors[$refErrorKey] = [
                        'error' => "  " . __(
                            "Profil d'archive {0} non présent en base de données",
                            $infos['profileIdentifier']
                        ),
                        'message' => "  -> " . __(
                            "ajouter le profil d'archive {0}",
                            $infos['profileIdentifier']
                        ),
                    ];
                }
            }
        }
        // vérification de la présence du niveau de service
        if ($infos['serviceLevelIdentifier']) {
            $serviceLevel = $this->ServiceLevels->find()
                ->where(
                    [
                        'ServiceLevels.identifier' => $infos['serviceLevelIdentifier'],
                        'ServiceLevels.org_entity_id' => $sa->get('id'),
                    ]
                )
                ->first();
            if (!$serviceLevel) {
                $refErrorKey = 'serviceLevel-' . $infos['serviceLevelIdentifier'];
                if (!array_key_exists($refErrorKey, $this->referentielErrors)) {
                    $this->referentielErrors[$refErrorKey] = [
                        'error' => "  " . __(
                            "Niveau de service {0} non présent en base de données",
                            $infos['serviceLevelIdentifier']
                        ),
                        'message' => "  -> " . __(
                            "ajouter le niveau de service {0}",
                            $infos['serviceLevelIdentifier']
                        ),
                    ];
                }
            }
        }
    }

    /**
     * Retourne une liste d'informations extraites du fichier xml de description d'une archive
     *
     * @param string $archiveStorageFolder
     * @return array
     * @throws Exception
     */
    private function getDescriptionInfos($archiveStorageFolder): array
    {
        $ret = [];
        $descriptionFilename = $this->getManagementFileUri($archiveStorageFolder, 'description');
        $ret['fileUri'] = $descriptionFilename;
        $domDoc = new DOMDocument();
        $domDoc->load($descriptionFilename);
        $util = new DOMUtility($domDoc);
        switch ($util->namespace) {
            case NAMESPACE_SEDA_02:
                $ret['sedaVersion'] = 'seda02';
            // no break
            case NAMESPACE_SEDA_10:
                $ret['sedaVersion'] = $ret['sedaVersion'] ?? 'seda10';
                $infos = [
                    'archivalAgencyIdentifier' => '//ns:ArchivalAgencyArchiveIdentifier',
                    'originatingAgencyIdentifier' => '//ns:OriginatingAgency/ns:Identification',
                    'originatingAgencyName' => '//ns:OriginatingAgency/ns:Name',
                    'agreementIdentifier' => '//ns:ArchivalAgreement',
                    'profileIdentifier' => '//ns:Archive/ns:ArchivalProfile',
                    'serviceLevelIdentifier' => '//ns:Archive/ns:ServiceLevel',
                ];
                break;
            case NAMESPACE_SEDA_21:
                $ret['sedaVersion'] = 'seda21';
            // no break
            case NAMESPACE_SEDA_22:
                $util->xpath->registerNamespace('seda10', 'fr:gouv:culture:archivesdefrance:seda:v1.0');
                $ret['sedaVersion'] = $ret['sedaVersion'] ?? 'seda22';
                $infos = [
                    'archivalAgencyIdentifier' => '//ns:ArchivalAgencyArchiveUnitIdentifier',
                    'originatingAgencyIdentifier' => '//ns:OriginatingAgency/ns:Identifier',
                    'originatingAgencyName' => '//ns:OriginatingAgency/ns:OrganizationDescriptiveMetadata/seda10:Name',
                    'agreementIdentifier' => '',
                    'profileIdentifier' => '//ns:ArchiveUnit/ns:ArchiveUnitProfile',
                    'serviceLevelIdentifier' => '//ns:ManagementMetadata/ns:ServiceLevel',
                ];
                break;
            default:
                throw new Exception(
                    'unsuported namespace: ' . $util->namespace
                );
        }
        foreach ($infos as $info => $xpath) {
            if ($xpath) {
                $ret[$info] = $util->nodeValue($xpath) ?? '';
            } else {
                $ret[$info] = '';
            }
        }
        return $ret;
    }

    /**
     * Restauration de l'archive via la création d'un transfert
     * @param string    $dir
     * @param Arguments $args
     * @return EntityInterface
     * @throws Exception
     */
    private function restoreArchive(string $dir, Arguments $args): EntityInterface
    {
        $transferInfos = $this->getTransferInfos($dir);
        $sa = $this->getSA($transferInfos['archivalAgencyIdentifier']);
        // recherche du transfert en base de données
        $transfer = $this->Transfers->find()
            ->where(
                [
                    'archival_agency_id' => $sa->get('id'),
                    'transfer_identifier' => $transferInfos['identifier'],
                ]
            )->first();

        if ($transfer) {
            $this->io->out("  " . __("reprise de la tentative de restauration précédente"));
            $this->resumeAttachments($dir, $transfer, $args);
        } else {
            $this->io->out("  " . __("création du transfert en base de données"));
            $transfer = $this->createTransfer($dir);
            $this->createAttachments($dir, $transfer, $args);
        }

        // avancement du l'état
        $this->Transfers->transitionOrFail($transfer, TransfersTable::T_ANALYSE);
        $this->Transfers->transitionOrFail($transfer, TransfersTable::T_CONTROL);
        $this->Transfers->transitionOrFail($transfer, TransfersTable::T_ARCHIVE);

        $this->Transfers->saveOrFail($transfer);

        return $transfer;
    }

    /**
     * Reprend l'ajout les pjs en base de données et copie les fichiers dans le répertoire des transferts
     * @param string          $dir
     * @param EntityInterface $transfer
     * @param Arguments       $args
     * @throws GenericException|Exception
     */
    private function resumeAttachments(string $dir, EntityInterface $transfer, Arguments $args)
    {
        $util = DOMUtility::load($transfer->get('xml'));
        $seda2 = in_array($transfer->get('message_version'), ['seda2.1', 'seda2.2']);
        $q = $util->xpath->query($seda2 ? '//ns:BinaryDataObject' : '//ns:Document');

        $path = AbstractGenericMessageForm::getDataDirectory($transfer->get('id')) . DS . 'attachments' . DS;
        if (!file_exists($path)) {
            Filesystem::mkdir($path);
        }

        /** @var Clamav|AntivirusInterface $Antivirus */
        $Antivirus = Utility::get(Configure::read('Antivirus.classname'));
        $algo = Configure::read('hash_algo', 'sha256');

        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $this->io->out("  " . __("ajout des fichiers en base de données et dans le répertoire du transfert"));
        foreach ($q as $document) {
            $attachmentFilename = ArchiveBinariesTable::getBinaryFilename($util, $document);
            $file = $dir . DS . 'original_data' . DS . $attachmentFilename;

            // lecture ou création de l'attachment en base de données
            $attachment = $TransferAttachments->find()
                ->where(['transfer_id' => $transfer->get('id'), 'filename' => $attachmentFilename])
                ->first();
            if (!$attachment) {
                // antivirus
                $virus = null;
                if (!$args->getOption('no-virus-scan')) {
                    try {
                        $viruses = $Antivirus->scan($file);
                        if ($viruses) {
                            $virus = $viruses[$file];
                        }
                    } catch (Exception $e) {
                        $this->log($e->getMessage());
                    }
                }
                // analyse des fichiers (format)
                $exec = Utility::get('Exec')->command('sf -json', $file);
                $sfJson = json_decode($exec->stdout, true);

                $node = ArchiveBinariesTable::findBinaryByFilename($util, $attachmentFilename);

                $attachment = $TransferAttachments->newEntity(
                    [
                        'transfer_id' => $transfer->id,
                        'filename' => $attachmentFilename,
                        'size' => filesize($file),
                        'hash' => hash_file($algo, $file),
                        'hash_algo' => $algo,
                        'deletable' => true,
                        'mime' => Hash::get($sfJson, 'files.0.matches.0.mime') ?: mime_content_type($file),
                        'extension' => pathinfo($file, PATHINFO_EXTENSION),
                        'format' => Hash::get($sfJson, 'files.0.matches.0.id') ?: '',
                        'virus_name' => $virus,
                        'xpath' => $node ? $util->getElementXpath($node, '') : null,
                        'valid' => Utility::get(FileValidator::class)->isValid($file),
                    ]
                );
                $TransferAttachments->saveOrFail($attachment);
            }
            // copie du fichier
            $destFileUri = $attachment->get('path');
            if (!file_exists($destFileUri)) {
                Filesystem::copy($file, $attachment->get('path'));
            }
        }
    }

    /**
     * Ajoute le transfert en base de données
     * @param string $dir
     * @return EntityInterface
     * @throws GenericException|Exception
     */
    private function createTransfer(string $dir): EntityInterface
    {
        // remplacement de l'archive du transfert par l'archive de la description
        $transferFile = $this->getManagementFileUri($dir, 'transfer');
        $descriptionFile = $this->getManagementFileUri($dir, 'description');

        $utilTransfer = DOMUtility::load($transferFile);
        $utilArchive = DOMUtility::load($descriptionFile);

        switch ($utilTransfer->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $queryPath = '//ns:Archive';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $queryPath = '//ns:DescriptiveMetadata';
                break;
            default:
                throw new NotImplementedException(
                    sprintf(
                        'not implemented for namespace "%s"',
                        $utilTransfer->namespace
                    )
                );
        }

        $transferElement = $utilTransfer->xpath->query($queryPath)->item(0);
        $this->deleteChildren($transferElement);

        /** @var DOMElement $archiveElement */
        $archiveElement = $utilArchive->xpath->query($queryPath)->item(0);
        foreach ($archiveElement->childNodes as $i => $child) {
            $transferElement->appendChild(
                $transferElement->ownerDocument->importNode($child, true)
            );
            if ($child->nodeName == 'ArchiveUnit' && $i === 0) {
                /** @var DOMElement $firstChild */
                $firstChild = $transferElement->firstChild;
                $firstChild->removeAttributeNS(NAMESPACE_SEDA_10, 'default');
            }
        }

        $tempXml = sys_get_temp_dir() . DS . uniqid() . '.xml';
        Filesystem::dumpFile($tempXml, $utilTransfer->dom->saveXML());

        // creation de l'entité
        $transfer = $this->newEntityFromXml($tempXml);
        if ($transfer->getErrors()) {
            throw new BadRequestException(
                __(
                    "Erreur lors de la création du transfert, celui-ci doit être au format SEDA 1.0 ou 2.1"
                )
            );
        }

        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir . DS . 'original_data')
        );
        $count = 0;
        $size = 0;
        /** @var SplFileInfo $file */
        foreach ($iterator as $file) {
            if ($file->isDir()) {
                continue;
            }
            $size += $file->getSize();
            $count++;
        }
        $this->Transfers->patchEntity(
            $transfer,
            [
                'created_user_id' => $this->userId,
                'is_conform' => true,
                'is_accepted' => true,
                'data_size' => $size,
                'file_deleted' => true,
                'data_count' => $count,
                'files_deleted' => false,
            ] + $transfer->toArray(),
        );

        $this->Transfers->saveOrFail($transfer);

        Filesystem::rename(
            $tempXml,
            $transfer->get('xml')
        );
        Filesystem::copy(
            $transferFile,
            $transfer->get('xml') . ".orig"
        );

        $this->Transfers->transitionOrFail($transfer, TransfersTable::T_PREPARE);

        $this->Transfers->saveOrFail($transfer);

        return $transfer;
    }

    /**
     * Supprime tous les enfants du noeud
     *
     * @param DOMElement $node
     * @return void
     */
    private function deleteChildren(DOMElement $node): void
    {
        while (isset($node->firstChild)) {
            $node->removeChild($node->firstChild);
        }
    }

    /**
     * Créer une entité à partir d'un chemin vers un fichier xml
     *
     * @param string $xmlPath
     * @return EntityInterface
     * @throws Exception
     */
    private function newEntityFromXml(string $xmlPath): EntityInterface
    {
        if (!is_file($xmlPath)) {
            throw new GenericException(__("Le fichier indiqué n'existe pas"));
        }

        $norme = $this->Transfers->detectMessageVersion($xmlPath);
        if (!$norme) {
            return $this->Transfers->newEntity(
                ['message_version' => '', 'filename' => basename($xmlPath)]
            );
        }
        $util = DOMUtility::load($xmlPath);
        $preControl = new PreControlMessages($xmlPath);
        $ns = $util->namespace;

        /**
         * mapping
         */
        $date = $util->xpath->query($preControl->datePaths[$ns])->item(0);
        $comment = $util->xpath->query($preControl->commentPaths[$ns])->item(0);
        $identifier = $util->xpath->query($preControl->identifierIdPaths[$ns])
            ->item(0);
        $sa = $util->xpath->query(
            $preControl->identifierArchivalAgencyPaths[$ns]
        )->item(0);
        $sv = $util->xpath->query(
            $preControl->identifierTransferringAgencyPaths[$ns]
        )->item(0);
        $sl = $util->xpath->query('//ns:ServiceLevel[1]')->item(0);
        $ag = $util->xpath->query('//ns:ArchivalAgreement[1]')->item(0);
        $pr = $util->xpath->query('//ns:ArchivalProfile[1]')->item(0);

        $data = [
            'filename' => basename($xmlPath),
            'message_version' => $norme,
            'state' => $this->Transfers->initialState,
            'is_modified' => false,
            'transfer_date' => $date ? new DateTime($date->nodeValue) : null,
            'transfer_comment' => $comment?->nodeValue,
            'transfer_identifier' => $identifier?->nodeValue,
            'sa' => $sa?->nodeValue,
            'sv' => $sv?->nodeValue,
            'sl' => $sl?->nodeValue,
            'ag' => $ag?->nodeValue,
            'pr' => $pr?->nodeValue,
            'archival_agency_id' => null,
            'transferring_agency_id' => null,
            'service_level_id' => null,
            'agreement_id' => null,
            'profile_id' => null,
        ];

        $sa = $this->OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'OrgEntities.identifier' => $data['sa'],
                    'TypeEntities.code' => 'SA',
                ]
            )
            ->first();
        if ($sa) {
            $data['archival_agency_id'] = $sa->get('id');
            $sv = $this->OrgEntities->find()
                ->where(
                    [
                        'identifier' => $data['sv'],
                        'lft >=' => $sa->get('lft'),
                        'rght <=' => $sa->get('rght'),
                    ]
                )
                ->first();
            if (!$sv) {
                throw new GenericException(__("Le Service Versant {0} n'existe pas", $data['sv']));
            }
            $data['transferring_agency_id'] = $sv->get('id');
            if ($data['sl']) {
                $sl = $this->ServiceLevels->find()
                    ->where(
                        [
                            'org_entity_id' => $sa->get('id'),
                            'identifier' => $data['sl'],
                        ]
                    )
                    ->first();
                if (!$sl) {
                    throw new GenericException(__("Le Niveau de service {0} n'existe pas", $data['sl']));
                }
                $data['service_level_id'] = $sl->get('id');
            }
            if ($data['ag']) {
                $ag = $this->Agreements->find()
                    ->where(
                        [
                            'org_entity_id' => $sa->get('id'),
                            'identifier' => $data['ag'],
                        ]
                    )
                    ->first();
                if (!$ag) {
                    throw new GenericException(__("L'Accord de versement {0} n'existe pas", $data['ag']));
                }
                $data['agreement_id'] = $ag->get('id');
            }
            if ($data['pr']) {
                $pr = $this->Profiles->find()
                    ->where(
                        [
                            'org_entity_id' => $sa->get('id'),
                            'identifier' => $data['pr'],
                        ]
                    )
                    ->first();
                if (!$pr) {
                    throw new GenericException(__("Le Profil {0} n'existe pas", $data['pr']));
                }
                $data['profile_id'] = $pr->get('id');
            }
            unset($data['sl'], $data['ag'], $data['pr']);// SimpleXMLElement = impossible à serialiser l'entité
        }

        return $this->Transfers->newEntity($data, ['validate' => false]);
    }

    /**
     * Ajoute les pjs en base de données et copie les fichiers dans le répertoire des transferts
     * @param string          $dir
     * @param EntityInterface $transfer
     * @param Arguments       $args
     * @throws GenericException|Exception
     */
    private function createAttachments(string $dir, EntityInterface $transfer, Arguments $args)
    {
        $targetUri = $dir . DS . 'original_data';

        $this->io->out("  " . __("détection du format des fichiers"));
        // analyse des fichiers (format)
        $exec = Utility::get('Exec')->command('sf -json', $dir);
        $json = json_decode($exec->stdout, true);

        $sf = [];
        foreach (Hash::get($json, 'files', []) as $file) {
            $format = Hash::get($file, 'matches.0.id') ?: 'UNKNOWN';
            $sf[$file['filename']] = [
                'format' => $format === 'UNKNOWN' ? '' : $format,
                'mime' => mime_content_type($file['filename']),
                'extension' => pathinfo($file['filename'], PATHINFO_EXTENSION),
            ];
        }

        // antivirus
        if (!$args->getOption('no-virus-scan')) {
            $this->io->out("  " . __("détection des virus"));

            try {
                /** @var Clamav|AntivirusInterface $Antivirus */
                $Antivirus = Utility::get(Configure::read('Antivirus.classname'));
                $viruses = $Antivirus->scan($targetUri);
            } catch (Exception $e) {
                $this->log($e->getMessage());
                $viruses = null;
            }
        } else {
            $viruses = null;
        }

        $util = DOMUtility::load($transfer->get('xml'));
        $seda2 = in_array($transfer->get('message_version'), ['seda2.1', 'seda2.2']);
        $q = $util->xpath->query($seda2 ? '//ns:BinaryDataObject' : '//ns:Document');

        $path = AbstractGenericMessageForm::getDataDirectory($transfer->get('id')) . DS . 'attachments' . DS;
        if (!file_exists($path)) {
            Filesystem::mkdir($path);
        }

        $algo = Configure::read('hash_algo', 'sha256');

        $this->io->out("  " . __("ajout des fichiers en base de données et dans le répertoire du transfert"));
        foreach ($q as $document) {
            $filename = ArchiveBinariesTable::getBinaryFilename($util, $document);
            $file = $dir . DS . 'original_data' . DS . $filename;

            if ($viruses === null) {
                $virus = null;
            } elseif (isset($viruses[$file])) {
                $virus = $viruses[$file];
            } else {
                $virus = '';
            }
            $node = ArchiveBinariesTable::findBinaryByFilename($util, $filename);
            $xpath = $node ? $util->getElementXpath($node, '') : null;

            $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
            $TransferAttachments->saveOrFail(
                $attachment = $TransferAttachments->newEntity(
                    [
                        'transfer_id' => $transfer->id,
                        'filename' => $filename,
                        'size' => filesize($file),
                        'hash' => hash_file($algo, $file),
                        'hash_algo' => $algo,
                        'deletable' => true,
                        'mime' => mime_content_type($file),
                        'extension' => $sf[$file]['extension'],
                        'format' => $sf[$file]['format'],
                        'virus_name' => $virus,
                        'xpath' => $xpath,
                        'valid' => Utility::get(FileValidator::class)->isValid($file),
                    ]
                )
            );
            Filesystem::copy($file, $attachment->get('path'));
        }
    }
}
