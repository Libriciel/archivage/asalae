<?php

/**
 * Asalae\Command\IntegritySanitizeCommand
 */

namespace Asalae\Command;

use Cake\Command\Command;
use Cake\Command\Helper\ProgressHelper;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;

/**
 * Corrige les mentions d'intégrités par héritage
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class IntegritySanitizeCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'integrity sanitize';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __(
                "Corrige les mentions d'intégrités par héritage"
            )
        );
        return $parser;
    }

    /**
     * Main
     * @param Arguments $args
     * @param ConsoleIo $io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io1 = $io;
        $query = $this->fetchTable('StoredFiles')
            ->find()
            ->contain(
                [
                    'ArchiveFiles',
                    'ArchiveBinaries',
                    'StoredFilesVolumes',
                ]
            );
        $io1->out('stored_file_volume -> archive_file/binary', 0);
        /** @var ProgressHelper $progress */
        $progress = $io1->helper('Progress');
        $progress->init(['total' => $query->count()]);
        foreach ($query as $storedFile) {
            $integrityOk = null;
            $hasOneIntegrityOk = false;
            /** @var EntityInterface $storedFileVolume */
            foreach ($storedFile->get('stored_files_volumes') as $storedFileVolume) {
                $ok = $storedFileVolume->get('is_integrity_ok');
                if ($ok === false) {
                    $integrityOk = false;
                    break;
                } elseif ($ok) {
                    $hasOneIntegrityOk = true;
                }
            }
            // si $integrityOk = null c'est qu'il n'y a pas de false
            if ($integrityOk === null && $hasOneIntegrityOk) {
                $integrityOk = true;
            }

            /** @var EntityInterface $archiveFile */
            foreach ($storedFile->get('archive_files') as $archiveFile) {
                if ($archiveFile->get('is_integrity_ok') !== $integrityOk) {
                    $this->fetchTable('ArchiveFiles')
                        ->updateAll(
                            ['is_integrity_ok' => $integrityOk],
                            ['id' => $archiveFile->id]
                        );
                }
            }
            /** @var EntityInterface $archiveBinary */
            foreach ($storedFile->get('archive_binaries') as $archiveBinary) {
                if ($archiveBinary->get('is_integrity_ok') !== $integrityOk) {
                    $this->fetchTable('ArchiveBinaries')
                        ->updateAll(
                            ['is_integrity_ok' => $integrityOk],
                            ['id' => $archiveBinary->id]
                        );
                }
            }

            $progress->increment();
            $progress->draw();
        }

        $io1->nl(2);
        $io1->out('archive_file/binary -> archive', 0);
        $Archives = $this->fetchTable('Archives');
        $query = $Archives->find();
        $progress->init(['total' => $query->count()]);
        $StoredFilesVolumes = $this->fetchTable('StoredFilesVolumes');
        /** @var EntityInterface $archive */
        foreach ($query as $archive) {
            $baseQuery = $StoredFilesVolumes->find()
                ->leftJoinWith('StoredFiles.ArchiveBinaries.ArchiveUnits')
                ->leftJoinWith('StoredFiles.ArchiveFiles')
                ->where(
                    [
                        [
                            'OR' => [
                                'ArchiveUnits.id IS' => null,
                                'ArchiveUnits.archive_id' => $archive->id,
                            ],
                        ],
                        [
                            'OR' => [
                                'ArchiveFiles.id IS' => null,
                                'ArchiveFiles.archive_id' => $archive->id,
                            ],
                        ],
                    ]
                );
            $integrityKo = (clone $baseQuery)
                ->where(['StoredFilesVolumes.is_integrity_ok IS' => false])
                ->first();
            if ($integrityKo && $archive->get('is_integrity_ok') !== false) {
                $archive->set('is_integrity_ok', false);
                $archive->set('integrity_date', $integrityKo->get('integrity_date'));
                $Archives->saveOrFail($archive);
            } elseif (!$integrityKo && $archive->get('is_integrity_ok') !== true) {
                $integrityOk = (clone $baseQuery)
                    ->where(['StoredFilesVolumes.is_integrity_ok IS' => true])
                    ->first();
                $archive->set('is_integrity_ok', $integrityOk ? true : null);
                $archive->set('integrity_date', $integrityOk ? $integrityOk->get('integrity_date') : null);
                $Archives->saveOrFail($archive);
            } elseif (!$integrityKo) {
                $integrityNull = (clone $baseQuery)
                    ->where(['StoredFilesVolumes.is_integrity_ok IS' => null])
                    ->first();
                if ($integrityNull) {
                    $archive->set('is_integrity_ok');
                    $archive->set('integrity_date');
                    $Archives->saveOrFail($archive);
                }
            }

            $progress->increment();
            $progress->draw();
        }

        $io1->nl(2);
        $io1->out('binary -> technical_archive', 0);
        $TechnicalArchives = $this->fetchTable('TechnicalArchives');
        $query = $TechnicalArchives->find();
        $progress->init(['total' => $query->count()]);
        $StoredFilesVolumes = $this->fetchTable('StoredFilesVolumes');
        /** @var EntityInterface $technicalArchive */
        foreach ($query as $technicalArchive) {
            $baseQuery = $StoredFilesVolumes->find()
                ->innerJoinWith('StoredFiles.ArchiveBinaries.TechnicalArchiveUnits')
                ->where(['TechnicalArchiveUnits.technical_archive_id' => $technicalArchive->id]);
            $integrityKo = (clone $baseQuery)
                ->where(['StoredFilesVolumes.is_integrity_ok IS' => false])
                ->first();
            if ($integrityKo && $technicalArchive->get('is_integrity_ok') !== false) {
                $technicalArchive->set('is_integrity_ok', false);
                $technicalArchive->set('integrity_date', $integrityKo->get('integrity_date'));
                $TechnicalArchives->saveOrFail($technicalArchive);
            } elseif (!$integrityKo && $technicalArchive->get('is_integrity_ok') !== true) {
                $integrityOk = (clone $baseQuery)
                    ->where(['StoredFilesVolumes.is_integrity_ok IS' => true])
                    ->first();
                $technicalArchive->set('is_integrity_ok', $integrityOk ? true : null);
                $technicalArchive->set('integrity_date', $integrityOk ? $integrityOk->get('integrity_date') : null);
                $TechnicalArchives->saveOrFail($technicalArchive);
            }

            $progress->increment();
            $progress->draw();
        }
        $io1->nl();
    }
}
