<?php

/**
 * Asalae\Command\DeleteTransfersCommand
 */

namespace Asalae\Command;

use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\OaipmhArchivesTable;
use Asalae\Model\Table\OaipmhTokensTable;
use Asalae\Model\Table\TransferErrorsTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Exception\GenericException;
use Asalae\Model\Table\TransfersTable;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use Cake\Error\Debugger;
use Cake\I18n\Number;
use Cake\ORM\Table;
use Cake\Command\Helper\ProgressHelper;
use Cake\Utility\Hash;
use Exception;

/**
 * Commande de suppression des transferts et de tout ce qui va avec
 * A n'utiliser que pour nettoyer des instances de test
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveBinariesTable ArchiveBinaries
 * @property ArchiveFilesTable    ArchiveFiles
 * @property ArchivesTable        Archives
 * @property OaipmhArchivesTable  OaipmhArchives
 * @property OaipmhTokensTable    OaipmhTokens
 * @property TransferErrorsTable  TransferErrors
 * @property TransfersTable       Transfers
 */
class DeleteTransfersCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    public $io;
    /**
     * @var Arguments
     */
    public $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'delete transfers';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();
        $parser->addOption(
            'conditions-json',
            [
                'help' => __("Conditions au format json"),
            ]
        );
        $parser->addOption(
            'input-json',
            [
                'help' => __(
                    "Permet la saisie de json sans avoir à échapper les caractères"
                ),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'datasource',
            [
                'help' => __("Datasource utilisé"),
                'default' => 'default',
            ]
        );
        $parser->addOption(
            'headless',
            [
                'help' => __("Pas d'interactivité"),
                'boolean' => true,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->args = $args;
        $this->io = $io;
        $opts = ['connectionName' => $args->getOption('datasource')];
        $Transfers = $this->fetchTable('Transfers', $opts);
        // nécessaire à cause des jointures ???
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries', $opts);
        $ArchiveFiles = $this->fetchTable('ArchiveFiles', $opts);
        $Archives = $this->fetchTable('Archives', $opts);
        $this->fetchTable('OaipmhArchives', $opts);
        $OaipmhTokens = $this->fetchTable('OaipmhTokens', $opts);
        $TransferErrors = $this->fetchTable('TransferErrors', $opts);
        $json = $args->getOption('conditions-json');
        if (!$json && $args->getOption('headless')) {
            $io->abort(__("Le mode headless doit être lancé avec un --conditions-json valide"));
        }
        if (!$json && $args->getOption('input-json')) {
            $json = $io->ask(
                __("Veuillez saisir les conditions sous la forme json")
            );
        }
        if ($json) {
            $conditions = json_decode($json, true);
            $io->out(__("Conditions: {0}", Debugger::exportVar($conditions)));
            if (!$conditions) {
                throw new GenericException('Failed to parse json');
            }
        } else {
            $conditions = [];
            $continue = false;
            $io->out(
                __(
                    "Les conditions doivent êtres saisies sous la forme (Model.field <operator?>) (value)"
                )
            );
            while (!$conditions || $continue) {
                $key = $io->ask(
                    __(
                        "Veuillez saisir un champ pour la condition (ex: Transfers.state)"
                    )
                );
                $value = $io->ask(
                    __(
                        "Veuillez saisir une valeur pour la condition (ex: Transfers.state)"
                    )
                );
                if ($key) {
                    $this->checkConditions([$key => $value]);
                    $conditions[$key] = $value;
                }
                $io->out(
                    __("Conditions: {0}", Debugger::exportVar($conditions))
                );
                $continue = $io->askChoice(
                    __("Ajouter une autre condition ?"),
                    ['y', 'n'],
                    'n'
                ) === 'y';
            }
        }
        $io->info(
            __(
                "Rappeler cette commande :\n{0}",
                'bin/cake delete transfers --conditions-json '
                . escapeshellarg(
                    addcslashes(json_encode($conditions, JSON_UNESCAPED_SLASHES), '"')
                )
            )
        );
        $this->checkConditions($conditions);
        $q = $Transfers->find();
        $query = $q
            ->leftJoinWith('Archives')
            ->where($conditions)
            ->contain(
                [
                    'Archives',
                ]
            );
        $count = $query->count();
        $io->out(__("Nombre de transferts : {0}", $count));
        if ($count === 0) {
            return;
        }
        /** @var ProgressHelper $progress */
        $progress = $io->helper('Progress');
        $progress->init(['total' => $count]);
        $transferAttachments = $Transfers->find()
            ->select(
                [
                    'attachments' => $q->func()->count('*'),
                    'size' => $q->func()->sum('size'),
                ]
            )
            ->leftJoinWith('Archives')
            ->innerJoinWith('TransferAttachments')
            ->where($conditions)
            ->disableHydration()
            ->toArray();
        $io->out(
            __(
                "Nombre de fichiers liés : {0}",
                Hash::get($transferAttachments, '0.attachments')
            )
        );
        $io->out(
            __(
                "Taille totale des fichiers liés : {0}",
                Number::toReadableSize(
                    Hash::get($transferAttachments, '0.size')
                )
            )
        );

        $countArchives = $Transfers->find()
            ->innerJoinWith('Archives')
            ->where($conditions)
            ->count();
        if ($countArchives) {
            $io->out(__("Nombre d'archives : {0}", $countArchives));

            $countArchiveUnits = $Transfers->find()
                ->innerJoinWith('Archives')
                ->innerJoinWith('Archives.ArchiveUnits')
                ->where($conditions)
                ->count();
            $io->out(
                __("Nombre d'unités d'archives : {0}", $countArchiveUnits)
            );

            $archiveBinaries = $Transfers->find()
                ->select(
                    [
                        'count' => $q->func()->count('*'),
                        'size' => $q->func()->sum('size'),
                    ]
                )
                ->innerJoinWith('Archives')
                ->innerJoinWith('Archives.ArchiveUnits')
                ->innerJoinWith('Archives.ArchiveUnits.ArchiveBinaries')
                ->innerJoinWith(
                    'Archives.ArchiveUnits.ArchiveBinaries.StoredFiles'
                )
                ->where($conditions)
                ->andWhere(['ArchiveBinaries.type' => 'original_data'])
                ->disableHydration()
                ->toArray();
            $io->out(
                __(
                    "Nombre de fichiers d'archives originaux par volumes : {0}",
                    Hash::get($archiveBinaries, '0.count')
                )
            );
            $io->out(
                __(
                    "Taille totale des fichiers originaux par volumes : {0}",
                    Number::toReadableSize(
                        Hash::get($archiveBinaries, '0.size')
                    )
                )
            );

            $archiveBinaries = $Transfers->find()
                ->select(
                    [
                        'count' => $q->func()->count('*'),
                        'size' => $q->func()->sum('size'),
                    ]
                )
                ->innerJoinWith('Archives')
                ->innerJoinWith('Archives.ArchiveUnits')
                ->innerJoinWith('Archives.ArchiveUnits.ArchiveBinaries')
                ->innerJoinWith(
                    'Archives.ArchiveUnits.ArchiveBinaries.StoredFiles'
                )
                ->where($conditions)
                ->andWhere(['ArchiveBinaries.type !=' => 'original_data'])
                ->disableHydration()
                ->toArray();
            $io->out(
                __(
                    "Nombre de fichiers d'archives de managements, horodatages et de conversions par volumes : {0}",
                    Hash::get($archiveBinaries, '0.count')
                )
            );
            $io->out(
                __(
                    "Taille totale des fichiers de managements, horodatages et de conversions par volumes : {0}",
                    Number::toReadableSize(
                        Hash::get($archiveBinaries, '0.size')
                    )
                )
            );
        }
        $io->warning(
            __(
                "La suppression de ces transferts supprimera tout ce qui y est lié"
            )
        );
        $io->warning(
            __(
                "Cette opération n'est pas réversible et ne doit en aucun cas être utilisée en production"
            )
        );
        if (!$args->getOption('headless')) {
            $response = $io->ask(
                __(
                    "Si vous avez compris et êtes certain de vouloir "
                    . "supprimer ces transferts, veuillez saisir le mot ''{0}''",
                    'delete'
                )
            );
            if ($response !== 'delete') {
                $io->abort(__("Vous n'avez pas taper le mot ''{0}''", 'delete'));
            }
        }
        $conn = $Transfers->getConnection();

        $io->out();

        /** @var EntityInterface $transfer */
        foreach ($query as $transfer) {
            $progress->increment();
            $progress->draw();
            $conn->begin();
            /** @var EntityInterface $archive */
            foreach ($transfer->get('archives') as $archive) {
                $manager = $this->getManager(
                    $archive->get('secure_data_space_id')
                );
                $sq = $OaipmhTokens->find()
                    ->select(['OaipmhTokens.id'])
                    ->innerJoinWith('OaipmhArchives')
                    ->where(['OaipmhArchives.archive_id' => $archive->id]);
                $OaipmhTokens->deleteAll(
                    ['id IN' => $sq]
                ); // dependant => true
                $Archives->getAssociation('DestructionNotificationXpaths')
                    ->deleteAll(['archive_id' => $archive->id]);
                $Archives->getAssociation('ArchiveBinaryConversions')
                    ->deleteAll(['archive_id' => $archive->id]);

                $archiveFiles = $ArchiveFiles->find()
                    ->where(['ArchiveFiles.archive_id' => $archive->id])
                    ->contain(['StoredFiles']);
                /** @var EntityInterface $archiveFile */
                foreach ($archiveFiles as $archiveFile) {
                    $io->verbose(
                        'ArchiveFile: ' . $archiveFile->get('filename')
                    );
                    $ArchiveFiles->delete($archiveFile);
                    if ($archiveFile->get('stored_file_id')) {
                        $manager->fileDelete(
                            Hash::get($archiveFile, 'stored_file.name')
                        );
                    }
                }

                $archiveBinaries = $ArchiveBinaries->find()
                    ->innerJoinWith('ArchiveUnits')
                    ->where(['ArchiveUnits.archive_id' => $archive->id])
                    ->orderBy(['ArchiveBinaries.original_data_id IS NULL'])
                    ->contain(['StoredFiles']);
                /** @var EntityInterface $archiveBinary */
                foreach ($archiveBinaries as $archiveBinary) {
                    $io->verbose(
                        'ArchiveBinary: ' . $archiveBinary->get('filename')
                    );
                    $ArchiveBinaries->delete($archiveBinary);
                    if ($archiveBinary->get('stored_file_id')) {
                        $manager->fileDelete(
                            Hash::get($archiveBinary, 'stored_file.name')
                        );
                    }
                }
                $io->verbose(
                    'Archive: ' . $archive->get('archival_agency_identifier')
                );
                $Archives->delete($archive);
            }
            $TransferErrors->deleteAll(['transfer_id' => $transfer->id]);
            $io->verbose('Transfer: ' . $transfer->get('transfer_identifier'));
            $Transfers->delete($transfer);
            $conn->commit();
        }
        $progress->draw();

        $io->out();
        $io->success('done');
    }

    /**
     * Vérifi que le model et le field des conditions existent
     * @param array $conditions
     * @return array
     */
    private function checkConditions(array $conditions): array
    {
        $models = [];
        $opts = ['connectionName' => $this->args->getOption('datasource')];
        foreach ($conditions as $key => $values) {
            if (strtoupper($key) === 'OR') {
                $this->checkConditions($values);
            } elseif (strpos($key, '.') === false) {
                $this->io->abort(
                    __("Les conditions doivent être sous la forme Model.field")
                );
            } elseif (preg_match('/^(\w+)\.(\w+)(?: \w)?$/', $key, $m)) {
                /** @var Table $Model */
                $Model = $this->fetchTable($m[1], $opts);
                if (!$Model->getSchema()->hasColumn($m[2])) {
                    throw new GenericException(
                        __(
                            "Le model {0} n'a pas de champ {1}",
                            $m[1],
                            $m[2]
                        )
                    );
                }
                $models[] = $m[1];
            }
        }
        return $models;
    }

    /**
     * Donne un volume manager
     * @param int $secure_data_space_id
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getManager($secure_data_space_id): VolumeManager
    {
        if (!isset($this->managers[$secure_data_space_id])) {
            $this->managers[$secure_data_space_id] = new VolumeManager(
                $secure_data_space_id
            );
        }
        return $this->managers[$secure_data_space_id];
    }
}
