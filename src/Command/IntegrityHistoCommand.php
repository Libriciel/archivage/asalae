<?php

/**
 * Asalae\Command\IntegrityHistoCommand
 */

namespace Asalae\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;

/**
 * Affiche le taux de vérification de l'intégrité des fichiers des archives
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class IntegrityHistoCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'integrity histo';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __(
                "Affichage de l'historique du contrôle d'intégrité des fichiers"
                . " des archives jour par jour pour la période de délai minimum entre deux vérifications"
            )
        );
        return $parser;
    }

    /**
     * Main
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $archives = $this->fetchTable('Archives');

        // lecture du nombre total d'archives
        $archivesCount = $archives->find()
            ->count();
        if (!$archivesCount) {
            $io->abort(__("Aucune archive en base de données, affichage de l'historique impossible") . "\n");
        }

        // lecture du nombre d'archives contrôlées
        $archivesControledCount = $archives->find()
            ->where(['is_integrity_ok IS NOT' => null])
            ->count();
        if (!$archivesControledCount) {
            $io->abort(__("Aucune archive contrôlée, affichage de l'historique impossible") . "\n");
        }

        // lecture du nombre d'archives non encore contrôlées
        $archivesNotControledCount = $archives->find()
            ->where(['is_integrity_ok IS' => null])
            ->count();

        // lecture des dates extrêmes de contrôle
        $oldestIntegrityDate = $archives->find()
            ->where(['integrity_date IS NOT' => null])
            ->orderBy(['integrity_date' => 'asc'])
            ->first()
            ->get('integrity_date');
        $latestIntegrityDate = $archives->find()
            ->where(['integrity_date IS NOT' => null])
            ->orderBy(['integrity_date' => 'desc'])
            ->first()
            ->get('integrity_date');

        // lecture du délai minimum entre deux vérifications (en jours) sur le cron du crontrôle d'intégrité
        $crons = $this->fetchTable('Crons');
        $cronCheckIntegrity = $crons->find()
            ->where(['classname' => 'Asalae\Cron\ArchivesIntegrityControl'])
            ->first();
        $appMetas = (array)json_decode($cronCheckIntegrity->get('app_meta'), true);
        $minDelay = $appMetas['min_delay'] ?? 0;

        // calcul du tableau de contrôle d'intégrité par jour pendant la période du délai minimum entre 2 contrôles
        $endDate = new DateTime();
        $endDate->modify('+1 day');
        $startDate = new DateTime();
        $startDate->modify('-' . $minDelay . ' day');
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startDate, $interval, $endDate);

        $data = [
            [
                __("Date contrôle"),
                __("Nombre contrôlées"),
                __("Pourcentage contrôlées"),
                __("Cumul # contrôlées"),
                __("Cumul % contrôlées"),
            ],
        ];
        $archivesControledCount = 0;
        foreach ($period as $date) {
            $archivesControledPerDayCount = $archives->find()
                ->where(
                    [
                        'integrity_date >=' => $date->format('Y-m-d 00:00:00'),
                        'integrity_date <=' => $date->format('Y-m-d 23:59:59'),
                    ]
                )
                ->count();
            $archivesControledCount += $archivesControledPerDayCount;
            $data[] = [
                $date->format('Y-m-d'),
                $archivesControledPerDayCount,
                sprintf("%02d %%", ($archivesControledPerDayCount / $archivesCount * 100)),
                $archivesControledCount,
                sprintf("%02d %%", ($archivesControledCount / $archivesCount * 100)),
            ];
        }

        // affichage de la réponse
        $io->out(__("Historique du contrôle d'intégrité des fichiers des archives"));
        $values = [
            __("nombre total d'archives") => $archivesCount,
            __("nombre d'archives non contrôlées") => $archivesNotControledCount,
            __("délai minimum en jours entre 2 contrôles") => $minDelay,
            __("date de contrôle la plus ancienne") => $oldestIntegrityDate
                ? $oldestIntegrityDate->format('Y-m-d')
                : 'nd',
            __("date de contrôle la plus récente") => $latestIntegrityDate
                ? $latestIntegrityDate->format('Y-m-d')
                : 'nd',
        ];
        if ($oldestIntegrityDate && $latestIntegrityDate) {
            $text = __("nombre de jours entre contrôles plus récent ancien");
            $values[$text] = $latestIntegrityDate->diff($oldestIntegrityDate)->format("%a");
        }
        foreach ($values as $text => $value) {
            $io->out(sprintf(' %s : %s', $text, $value));
        }
        $io->helper('Table')->output($data);
    }
}
