<?php

/**
 * AsalaeCore\Command\PdfFilesCommand
 */

namespace Asalae\Command;

use Asalae\Model\Table\MailsTable;
use Asalae\Model\Table\UsersTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Command\Seda2PdfCommand;
use AsalaeCore\Factory\Utility;
use Cake\Console\Arguments;
use Cake\Console\ConsoleInputArgument;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Mailer\Mailer;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInterval;
use DateTime;
use ErrorException;
use Exception;

/**
 * Pdf asynchrone
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property UsersTable Users
 */
class PdfFilesCommand extends Seda2PdfCommand
{
    public const string MAX_EXECUTION_TIME = 'PT1H';

    /**
     * @var array fichiers temporaire à supprimer
     */
    private $tmpFiles = [];

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'pdf_files';
    }

    /**
     * Méthode principale
     *
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $callbackUrl = $args->getArgument('callback-url');
        if (!is_dir(TMP . 'seda2pdf')) {
            mkdir(TMP . 'seda2pdf');
        }
        $tmpFilename = TMP . 'seda2pdf' . DS . hash('sha256', $callbackUrl);
        $userId = $args->getArgument('user_id');

        // supprime la liste des users attendant le pdf si le process n'existe plus
        if (is_file($tmpFilename)) {
            $data = file($tmpFilename);
            preg_match("/pid=(\d+)/", $data[2] ?? 'pid=0', $m);
            $pid = $m[1] ?? '0';
            if (!file_exists('/proc/' . $pid)) {
                unlink($tmpFilename);
            }
        }

        // chaque appel de cette commande ajoute un utilisateur à une liste (pour l'envoi d'emails)
        if (is_file($tmpFilename)) {
            $data = array_map('trim', $data ?? file($tmpFilename));
            $now = new DateTime();
            $limit = (new DateTime($data[0]))->add(
                new DateInterval(self::MAX_EXECUTION_TIME)
            );
            if ($now < $limit) {
                if (!in_array($userId, $data)) {
                    // ajoute le user_id à la liste en cours
                    $handle = fopen($tmpFilename, 'a');
                    fwrite($handle, $userId . PHP_EOL);
                    fclose($handle);
                }
                $io->abort(__("Une tache est déjà en cours"));
            } else {
                unlink($tmpFilename);
            }
        }

        // création de la liste des utilisateurs ayant demandé la conversion
        $this->tmpFiles[] = $tmpFilename;
        $handle = fopen($tmpFilename, 'w');
        fwrite($handle, (new DateTime())->format(DATE_RFC3339) . PHP_EOL);
        fwrite($handle, $callbackUrl . PHP_EOL);
        fwrite($handle, 'pid=' . getmypid() . PHP_EOL);
        fwrite($handle, str_repeat('/', 80) . PHP_EOL);
        fwrite($handle, $userId . PHP_EOL);
        fclose($handle);

        // transforme les fichier "seda" et "out" en fichier temporaire pour la gestion par volumes
        $nArgs = $args->getArguments();
        $secureDataSpaceId = $args->getArgument('secure_data_space_id');
        $volumeManager = $secureDataSpaceId ? new VolumeManager(
            (int)$secureDataSpaceId
        ) : null;
        $seda = $args->getArgument('seda');
        if (!is_file($seda) && $secureDataSpaceId) {
            $nArgs[0] = sys_get_temp_dir() . DS . uniqid('pdf-files-');
            $nArgs[1] = sys_get_temp_dir() . DS . uniqid('pdf-files-');
            $volumeManager->fileDownload($seda, $nArgs[0]);
            $this->tmpFiles[] = $nArgs[0];
            $this->tmpFiles[] = $nArgs[1];
        }

        // récupère le nom des arguments pour setter les fichiers temporaire pour la classe parente
        $argNames = [];
        /** @var ConsoleInputArgument $arg */
        foreach ($this->getOptionParser()->arguments() as $arg) {
            $argNames[] = $arg->name();
        }

        // on crée le pdf
        set_error_handler(
            function ($num, $str, $file, $line) {
                throw new ErrorException($str, 0, $num, $file, $line);
            }
        );
        $pdf = $args->getArgument('out');
        try {
            parent::execute(
                new Arguments($nArgs, $args->getOptions(), $argNames),
                $io
            );
        } finally {
            restore_error_handler();
        }
        if (!is_file($args->getArgument('out'))) {
            $pdf = $nArgs[1];
        }

        // upload du pdf sur les volumes
        if ($upload = $args->getArgument('upload')) {
            if (
                $args->getOption('force')
                && $volumeManager->fileExists($upload)
            ) {
                $volumeManager->fileDelete($upload);
            }
            $volumeManager->fileUpload($pdf, $upload);
        }

        // envoi de l'email
        $this->sendMail($args, $io);

        $io->out('done');
    }

    /**
     * Gets the option parser instance and configures it.
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'user_id',
            [
                'help' => __("Id de l'utilisateur cible"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'callback-url',
            [
                'help' => __("Url de redirection pour récupérer le fichier"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'secure_data_space_id',
            [
                'help' => __("Id de l'espace de stockage sécurisé (optionnel)"),
            ]
        );
        $parser->addArgument(
            'upload',
            [
                'help' => __("Chemin d'upload (optionnel)"),
            ]
        );
        return $parser;
    }

    /**
     * Envoi les emails suite au traitement
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    private function sendMail(Arguments $args, ConsoleIo $io)
    {
        $callbackUrl = $args->getArgument('callback-url');
        $tmpFilename = TMP . 'seda2pdf' . DS . hash('sha256', $callbackUrl);
        $userId = $args->getArgument('user_id');
        $user = $this->fetchTable('Users')->find()
            ->where(['Users.id' => $userId])
            ->contain(
                [
                    'OrgEntities' => [
                        'ArchivalAgencies' => [
                            'Configurations' => function (Query $q) {
                                return $q->where(
                                    ['Configurations.name' => 'mail-title-prefix']
                                )
                                    ->limit(1);
                            },
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        $prefix = Hash::get(
            $user,
            'org_entity.archival_agency.configurations.0.setting',
            '[asalae]'
        );
        $userIds = array_filter(
            array_map('trim', file($tmpFilename)),
            'is_numeric'
        );
        $users = $this->fetchTable('Users')->find()
            ->select(['email'])
            ->where(
                [
                    'id IN' => $userIds,
                    'email IS NOT' => null,
                    'email !=' => '',
                    'email NOT LIKE' => '%@test.fr',
                ]
            )
            ->distinct(['email'])
            ->disableHydration()
            ->all()
            ->map(
                function ($data) {
                    return $data['email'];
                }
            )
            ->toArray();

        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $email->viewBuilder()
            ->setTemplate('pdf_file_available')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);

        $email->setEmailFormat('both')
            ->setSubject(
                $prefix . ' ' . __("Fichier disponnible au téléchargement")
            )
            ->setViewVars(
                [
                    'filename' => basename($args->getArgument('out')),
                    'link' => $callbackUrl,
                ]
            )
            ->setTo($users);
        /** @var MailsTable $Mails */
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        $success = $Mails->asyncMail($email, $userId);
        if ($success) {
            $io->success(__("Email envoyé avec succès"));
            $message = __(
                "Le fichier PDF est prêt, un email a été envoyé, "
                . "il est téléchargeable depuis le lien suivant : {0}",
                '<a href="' . $callbackUrl . '" target="_blank" download>'
                . __("Téléchargement du PDF")
                . '</a>'
            );
        } else {
            $io->error(__("Echec lors de l'envoi de l'email"));
            $message = __(
                "Echec lors de l'envoi de l'email lors de la création du pdf"
            );
        }

        $Notify = Utility::get('Notify');
        foreach ($userIds as $user_id) {
            $Notify->send(
                $user_id,
                [],
                $message,
                $success ? 'alert-success' : 'alert-danger'
            );
        }
    }

    /**
     * Supprime les fichiers temporaires
     */
    public function __destruct()
    {
        foreach ($this->tmpFiles as $tmp) {
            if (is_file($tmp)) {
                unlink($tmp);
            }
        }
    }
}
