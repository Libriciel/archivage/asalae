<?php

/**
 * Asalae\Command\VolumeManager\RepairCommand
 */

namespace Asalae\Command\VolumeManager;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Exception\StopException;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/**
 * synchronise les volumes d'un espace de stockage
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RepairCommand extends AbstractVolumeManagerCommand
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume_manager repair';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserRepair = new ConsoleOptionParser();
        $parserRepair->setDescription(
            '*************************************************************' . PHP_EOL
            . __(
                "Tente de resynchroniser les volumes d'un espace de stockage"
            ) . PHP_EOL
            . '*************************************************************'
        );
        $parserRepair->addArgument(
            'secure_data_space_id',
            [
                'help' => __(
                    "Identifiant (id) de l'espace de stockage à traiter"
                ),
                'required' => true,
            ]
        );
        $parserRepair->addOption(
            'integrity-check',
            [
                'help' => __("Vérifi le hash de chaques fichiers (très long)"),
                'boolean' => true,
            ]
        );
        $parserRepair->addOption(
            'dry-run',
            [
                'help' => __("Ne corrige pas les erreurs"),
                'boolean' => true,
            ]
        );
        return $parserRepair;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $secure_data_space_id = $this->args->getArgument('secure_data_space_id');
        $dir = sys_get_temp_dir() . DS . uniqid('volume_manager_repair-');
        mkdir($dir);
        $loc = TableRegistry::getTableLocator();
        $StoredFiles = $loc->get('StoredFiles');
        $manager = new VolumeManager($secure_data_space_id);
        $drivers = $manager->getVolumes();
        foreach ($drivers as $driver) {
            if (!$driver->ping()) {
                $e = new VolumeException(
                    VolumeException::VOLUME_NOT_ACCESSIBLE
                );
                throw $e->wrap($driver);
            }
        }
        $query = $StoredFiles->find()
            ->where(['secure_data_space_id' => $secure_data_space_id])
            ->orderBy(['StoredFiles.name']);
        $count = $query->count();
        $time = microtime(true);
        $i = 0;
        $this->io->out(__("Fichiers à vérifier: {0}", $count));
        /** @var EntityInterface $storedFile */
        foreach ($query as $storedFile) {
            $i++;
            $this->io->verbose($storedFile->get('name'));
            if ((microtime(true) - $time) > 60.) {
                $time = microtime(true);
                $this->io->out(sprintf('%d / %d', $i, $count));
            }
            $this->checkRepairStoredFile($storedFile, $drivers);
        }
        $this->io->out('done');
    }

    /**
     * Détache un volume d'un espace de stockage
     * @param string $volume_id
     * @throws VolumeException
     */
    public function truncateVolume(string $volume_id)
    {
        $loc = TableRegistry::getTableLocator();
        $Volumes = $loc->get('Volumes');
        $volume = $Volumes->get(1);
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        try {
            $driver = VolumeManager::getDriverById($volume_id);
        } catch (VolumeException) {
            $this->io->warning(__("Impossible de se connecter au volume"));
            $drop = $this->args->getOption('force')
                ? 'y'
                : $this->io->askChoice(
                    __(
                        "Les fichiers sur le volume ne seront pas supprimés. Voulez-vous continuer ?"
                    ),
                    ['y', 'n'],
                    'n'
                );
            if ($drop === 'y') {
                $StoredFilesVolumes->deleteAll(['volume_id' => $volume_id]);
                $volume->set('secure_data_space_id');
                $Volumes->saveOrFail($volume);
            }
            return;
        }

        $query = $StoredFilesVolumes->find()
            ->where(['volume_id' => $volume_id]);
        /** @var EntityInterface $link */
        foreach ($query as $link) {
            if ($driver->fileExists($link->get('storage_ref'))) {
                try {
                    $driver->fileDelete($link->get('storage_ref'));
                } catch (VolumeException) {
                    $this->io->err(
                        __(
                            "Impossible de supprimer le fichier ''{0}''",
                            $link->get('storage_ref')
                        )
                    );
                }
            }
            $StoredFilesVolumes->deleteAll(['id' => $link->id]);
        }

        $volume->set('secure_data_space_id');
        $Volumes->saveOrFail($volume);
    }

    /**
     * Vérifi l'existance d'un fichier
     * @param int    $secure_data_space_id
     * @param string $storagePath
     * @throws VolumeException
     */
    public function exists($secure_data_space_id, string $storagePath)
    {
        $manager = new VolumeManager($secure_data_space_id);
        if ($manager->fileExists($storagePath)) {
            $this->io->success(__("Le fichier existe"));
        } else {
            $this->io->warning($msg = __("Le fichier n'existe pas"));
            throw new StopException($msg, self::CODE_ERROR);
        }
    }

    /**
     * Télécharge un fichier
     * @param int    $secure_data_space_id
     * @param string $storagePath
     * @param string $destination
     * @throws VolumeException
     */
    public function download(
        $secure_data_space_id,
        string $storagePath,
        string $destination
    ) {
        if (is_file($destination)) {
            if ($this->args->getOption('overwrite')) {
                unlink($destination);
            } else {
                $this->io->warning($msg = __("Un fichier du même nom existe déjà"));
                throw new StopException($msg, self::CODE_ERROR);
            }
        }

        $manager = new VolumeManager($secure_data_space_id);
        $manager->fileDownload($storagePath, $destination);
        if (is_file($destination)) {
            $this->io->success(__("Le fichier a été téléchargé avec succès"));
        } else {
            $this->io->warning($msg = __("Erreur lors du téléchargement"));
            throw new StopException($msg, self::CODE_ERROR);
        }
    }

    /**
     * Upload un fichier
     * @param int    $secure_data_space_id
     * @param string $uploadFile
     * @param string $storagePath
     * @throws VolumeException
     */
    public function upload(
        $secure_data_space_id,
        string $uploadFile,
        string $storagePath
    ) {
        if (!is_file($uploadFile)) {
            $this->io->warning($msg = __("Le fichier à uploader n'existe pas"));
            throw new StopException($msg, self::CODE_ERROR);
        }

        $manager = new VolumeManager($secure_data_space_id);
        if ($manager->fileExists($storagePath)) {
            if ($this->args->getOption('overwrite')) {
                $storedFile = TableRegistry::getTableLocator()->get(
                    'StoredFiles'
                )
                    ->find()
                    ->where(
                        [
                            'StoredFiles.secure_data_space_id' => $secure_data_space_id,
                            'StoredFiles.name' => $storagePath,
                        ]
                    )
                    ->firstOrFail();
                $manager->replace($storedFile->id, $uploadFile);
            } else {
                throw new VolumeException(VolumeException::FILE_ALREADY_EXISTS);
            }
        } else {
            $manager->fileUpload($uploadFile, $storagePath);
        }
        if ($manager->fileExists($storagePath)) {
            $this->io->success(__("Le fichier a été uploadé avec succès"));
            if ($eventIds = $this->args->getOption('set-event-in-lifecycle')) {
                foreach (explode(',', $eventIds) as $eventId) {
                    $EventLogs = TableRegistry::getTableLocator()->get(
                        'EventLogs'
                    );
                    $EventLogs->updateAll(
                        ['in_lifecycle' => true],
                        ['id' => $eventId]
                    );
                }
            }
            // supprime le fichier cache au bout de 600s si le fichier n'a pas été locké à nouveau
            if (
                $this->args->getOption('sleep-then-delete') && is_file(
                    $uploadFile . '.lock'
                )
            ) {
                unlink($uploadFile . '.lock');
                sleep((int)$this->args->getOption('sleep-then-delete'));//NOSONAR
                if (!is_file($uploadFile . '.lock')) {
                    unlink($uploadFile);
                }
            }
        } else {
            $this->io->warning($msg = __("Erreur lors de l'upload"));
            throw new StopException($msg, self::CODE_ERROR);
        }
    }

    /**
     * Supprime un fichier
     * @param int    $secure_data_space_id
     * @param string $storagePath
     * @throws VolumeException
     */
    public function delete($secure_data_space_id, string $storagePath)
    {
        $manager = new VolumeManager($secure_data_space_id);
        $manager->fileDelete($storagePath);
        if (!$manager->fileExists($storagePath)) {
            $this->io->success(__("Le fichier a été supprimé avec succès"));
        } else {
            $this->io->warning($msg = __("Erreur lors de la suppression du fichier"));
            throw new StopException($msg, self::CODE_ERROR);
        }
    }

    /**
     * Affiche le contenu d'un fichier sur la sortie standard
     * @param int    $secure_data_space_id
     * @param string $storagePath
     * @throws VolumeException
     */
    public function cat($secure_data_space_id, string $storagePath)
    {
        $manager = new VolumeManager($secure_data_space_id);
        $manager->readfile($storagePath);
    }

    /**
     * Donne la liste des volumes
     * @param string $message
     * @return void
     */
    public function info(string $message)
    {
        $fields = ['key', 'value'];
        $storedFile = TableRegistry::getTableLocator()->get('StoredFiles')->find()
            ->where(['StoredFiles.name' => $message])
            ->contain(
                [
                    'Volumes',
                    'ArchiveFiles' => [
                        'Archives' => ['Transfers'],
                    ],
                    'ArchiveBinaries' => [
                        'ArchiveUnits' => [
                            'Archives' => ['Transfers'],
                        ],
                    ],
                ]
            )
            ->first();
        $sep = str_repeat('-', 64);
        $data = [
            ['key' => '', 'value' => ''],
            ['key' => $this->title('stored_file'), 'value' => $sep],
            ['key' => 'id', 'value' => $storedFile->get('id')],
            ['key' => 'name', 'value' => $storedFile->get('name')],
            ['key' => 'hash', 'value' => $storedFile->get('hash')],
            ['key' => 'hash_algo', 'value' => $storedFile->get('hash_algo')],
            ['key' => 'size', 'value' => Number::toReadableSize($storedFile->get('size'))],
            ['key' => 'secure_data_space_id', 'value' => $storedFile->get('secure_data_space_id')],
            ['key' => 'created', 'value' => $storedFile->get('created')],
        ];

        foreach ($storedFile->get('volumes') as $volume) {
            $data[] = ['key' => '', 'value' => ''];
            $data[] = ['key' => $this->title('volume'), 'value' => $sep];
            $data[] = ['key' => 'id', 'value' => $volume->get('id')];
            $data[] = ['key' => 'name', 'value' => $volume->get('name')];
            $data[] = ['key' => 'read_duration', 'value' => $volume->get('read_duration') . 's'];
            $data[] = [
                'key' => 'is_integrity_ok',
                'value' => var_export($volume->get('_joinData')['is_integrity_ok'], true),
            ];
            $data[] = ['key' => 'driver', 'value' => $volume->get('driver')];
            if ($volume->get('driver') === 'FILESYSTEM') {
                $data[] = [
                    'key' => 'directory',
                    'value' => json_decode($volume->get('fields'), true)['path'],
                ];
            } else {
                $data[] = [
                    'key' => 'endpoint',
                    'value' => json_decode($volume->get('fields'), true)['endpoint'],
                ];
            }
        }
        foreach ($storedFile->get('archive_files') as $archiveFile) {
            $data[] = ['key' => '', 'value' => ''];
            $data[] = ['key' => $this->title('archive_file'), 'value' => $sep];
            $data[] = ['key' => 'id', 'value' => $archiveFile->get('id')];
            $data[] = ['key' => 'type', 'value' => $archiveFile->get('type')];
            $data[] = [
                'key' => 'is_integrity_ok',
                'value' => var_export($archiveFile->get('is_integrity_ok'), true),
            ];
        }
        $archive = Hash::get($storedFile, 'archive_files.0.archive');
        if ($archive) {
            $this->appendArchive($data, $archive);
        }
        foreach ($storedFile->get('archive_binaries') as $archiveBinary) {
            $data[] = ['key' => '', 'value' => ''];
            $data[] = ['key' => $this->title('archive_binary'), 'value' => $sep];
            $data[] = ['key' => 'id', 'value' => $archiveBinary->get('id')];
            $data[] = ['key' => 'type', 'value' => $archiveBinary->get('type')];
            $data[] = ['key' => 'mime', 'value' => $archiveBinary->get('mime')];
            $data[] = ['key' => 'format', 'value' => $archiveBinary->get('format')];
            $data[] = ['key' => 'extension', 'value' => $archiveBinary->get('extension')];
            $data[] = [
                'key' => 'is_integrity_ok',
                'value' => var_export($archiveBinary->get('is_integrity_ok'), true),
            ];

            foreach ($archiveBinary->get('archive_units') as $archiveUnit) {
                $data[] = ['key' => '', 'value' => ''];
                $data[] = ['key' => $this->title('archive_unit'), 'value' => $sep];
                $data[] = ['key' => 'id', 'value' => $archiveUnit->get('id')];
                $data[] = [
                    'key' => 'archival_agency_identifier',
                    'value' => $archiveUnit->get('archival_agency_identifier'),
                ];
                $data[] = ['key' => 'xpath', 'value' => $archiveUnit->get('xpath')];
            }
        }
        $archive = Hash::get($storedFile, 'archive_binaries.0.archive_units.0.archive');
        if ($archive) {
            $this->appendArchive($data, $archive);
        }

        array_unshift($data, $fields);

        $this->io->helper('Table')->output($data);
    }
}
