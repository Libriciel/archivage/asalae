<?php

/**
 * Asalae\Command\VolumeManager\VolumeManagerCommand
 */

namespace Asalae\Command\VolumeManager;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeInterface;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;

/**
 * Gestion des espaces de stockages et de leurs volumes
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class AbstractVolumeManagerCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Ajout l'archive et le transfert à $data
     * @param array           $data
     * @param EntityInterface $archive
     * @return void
     */
    protected function appendArchive(array &$data, EntityInterface $archive)
    {
        $data[] = ['key' => '', 'value' => ''];
        $data[] = ['key' => $this->title('archive'), 'value' => str_repeat('-', 64)];
        $data[] = ['key' => 'id', 'value' => $archive->get('id')];
        $data[] = ['key' => 'state', 'value' => $archive->get('state')];
        $data[] = ['key' => 'units_count', 'value' => $archive->get('units_count')];
        $data[] = ['key' => 'original_size', 'value' => Number::toReadableSize($archive->get('original_size'))];
        $data[] = ['key' => 'original_count', 'value' => $archive->get('original_count')];
        $data[] = ['key' => 'integrity_date', 'value' => $archive->get('integrity_date')];
        $data[] = ['key' => 'is_integrity_ok', 'value' => var_export($archive->get('is_integrity_ok'), true)];
        $data[] = ['key' => 'created', 'value' => $archive->get('created')];

        /** @var EntityInterface $transfer */
        $transfer = Hash::get($archive, 'transfers.0');
        $data[] = ['key' => '', 'value' => ''];
        $data[] = ['key' => $this->title('transfer'), 'value' => str_repeat('-', 64)];
        $data[] = ['key' => 'id', 'value' => $transfer->get('id')];
        $data[] = ['key' => 'state', 'value' => $transfer->get('state')];
        $data[] = ['key' => 'message_version', 'value' => $transfer->get('message_version')];
        $data[] = ['key' => 'created', 'value' => $transfer->get('created')];
    }

    /**
     * Donne un titre du genre ---$str-----------------
     * @param string $str
     * @return string
     */
    protected function title(string $str)
    {
        return sprintf(
            '---%s%s',
            strtoupper($str),
            str_repeat('-', 25 - strlen($str))
        );
    }

    /**
     * Vérifi un stored_file pour voir s'il a besoin d'être réparé
     * @param EntityInterface   $storedFile
     * @param VolumeInterface[] $drivers
     * @return void
     * @throws VolumeException
     */
    protected function checkRepairStoredFile(EntityInterface $storedFile, array $drivers)
    {
        $inDb = [];
        $inVolume = [];
        $goodIntegrities = [];
        $badIntegrities = [];
        $loc = TableRegistry::getTableLocator();
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $stv = $StoredFilesVolumes->find()
            ->where(['stored_file_id' => $storedFile->id]);
        // on récupère la liste des liens existant par volumes
        /** @var EntityInterface $link */
        foreach ($stv as $link) {
            $volume_id = $link->get('volume_id');
            if (!isset($drivers[$volume_id])) {
                continue;
            }
            $inDb[$volume_id] = $link->get('storage_ref');
            $inVolume[$volume_id] = $drivers[$volume_id]->fileExists(
                $link->get('storage_ref')
            );
            if ($this->args->getOption('integrity-check')) {
                try {
                    $hashOnVolume = $drivers[$volume_id]->hash($link->get('storage_ref'));
                } catch (VolumeException) {
                    $hashOnVolume = null;
                }
                if ($hashOnVolume === $storedFile->get('hash')) {
                    $goodIntegrities[$volume_id] = $link->get('storage_ref');
                } else {
                    $badIntegrities[$volume_id] = $link->get('storage_ref');
                }
            } else {
                if ($inDb[$volume_id] && $inVolume[$volume_id]) {
                    $goodIntegrities[$volume_id] = $link->get('storage_ref');
                } else {
                    $badIntegrities[$volume_id] = $link->get('storage_ref');
                }
            }
        }
        if (!$inDb) {
            $this->io->err(
                __(
                    "Stored file sans aucunes références: {0}",
                    $storedFile->get('name')
                )
            );
            return;
        }
        if (!$goodIntegrities) {
            $this->io->err(
                __(
                    "Stored file - toutes les références disponibles sont corrompues: {0}",
                    $storedFile->get('name')
                )
            );
            return;
        }
        $corrupted = [];
        foreach ($drivers as $volume_id => $driver) {
            $inDbNotInVolume = isset($inDb[$volume_id]) && !$inVolume[$volume_id];
            $notInDb = !isset($inDb[$volume_id]);
            $integrityFailed = isset($badIntegrities[$volume_id]);
            if (!$inDbNotInVolume && !$notInDb && !$integrityFailed) {
                continue;
            }
            $this->outputFileError(
                $volume_id,
                $storedFile->get('name'),
                $inDbNotInVolume,
                $notInDb
            );
            if (!$this->args->getOption('dry-run')) {
                $this->repairStoredFile(
                    $storedFile,
                    $driver,
                    $drivers,
                    $inVolume,
                    $inDb,
                    $goodIntegrities,
                    $volume_id,
                    $corrupted
                );
            }
        }
        if (!empty($corrupted)) {
            $this->io->err(
                __("Un ou plusieurs fichiers n'ont pas pu être restaurés")
            );
        }
    }

    /**
     * Affiche un message d'erreur pour un fichier spécifique
     * @param int    $volume_id
     * @param string $name
     * @param bool   $inDbNotInVolume
     * @param bool   $notInDb
     * @return void
     */
    protected function outputFileError(
        int $volume_id,
        string $name,
        bool $inDbNotInVolume,
        bool $notInDb
    ) {
        if ($inDbNotInVolume) {
            $error = __("présent en base de données mais absent sur le volume");
        } elseif ($notInDb) {
            $error = __("non référencé en base de données");
        } else {
            $error = __("échec du test d'intégrité");
        }
        $this->io->out(
            __(
                "volume {0} ({1}): {2}",
                $volume_id,
                $name,
                $error
            )
        );
    }

    /**
     * Tente de réparer un fichier
     * @param EntityInterface $storedFile
     * @param VolumeInterface $driver
     * @param array           $drivers
     * @param bool[]          $inVolume        [vol_id => (bool)exists]
     * @param bool[]          $inDb            [vol_id => (bool)exists]
     * @param string[]        $goodIntegrities [vol_id => storage_ref]
     * @param int             $volume_id
     * @param array           $corrupted
     * @return void
     */
    protected function repairStoredFile(
        EntityInterface $storedFile,
        VolumeInterface $driver,
        array $drivers,
        array $inVolume,
        array $inDb,
        array $goodIntegrities,
        int $volume_id,
        array &$corrupted
    ) {
        $loc = TableRegistry::getTableLocator();
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $newRef = null;
        foreach ($inVolume as $vol_id => $exists) {
            if (!$exists || !isset($goodIntegrities[$vol_id])) {
                continue;
            }
            /** @noinspection PhpConditionAlreadyCheckedInspection faux positif */
            $newRef = null;
            try {
                if ($driver->fileExists($inDb[$vol_id])) {
                    $driver->fileDelete($inDb[$vol_id]);
                }
                $newRef = $drivers[$vol_id]->streamTo(
                    $inDb[$vol_id],
                    $driver,
                    $storedFile->get('name')
                );
                $link = $StoredFilesVolumes->find()
                    ->where(
                        [
                            'StoredFilesVolumes.volume_id' => $volume_id,
                            'StoredFilesVolumes.stored_file_id' => $storedFile->get('id'),
                        ]
                    )
                    ->first();
                if ($link) {
                    $link->set('storage_ref', $newRef);
                } else {
                    $link = $StoredFilesVolumes->newEntity(
                        [
                            'storage_ref' => $newRef,
                            'volume_id' => $volume_id,
                            'stored_file_id' => $storedFile->id,
                        ]
                    );
                }
                $StoredFilesVolumes->saveOrFail($link);
                break;
            } catch (Exception $e) {
                $prev = $e->getPrevious();
                if ($prev) {
                    debug($prev->getMessage());
                }
                try {
                    if ($newRef) {
                        $driver->fileDelete($newRef);
                    }
                } catch (VolumeException) {
                }
                $newRef = null;
            }
        }
        if (!$newRef) {
            $this->io->err(
                __(
                    "Echec de réparation du fichier ({0}) sur volume {1}",
                    $storedFile->get('name'),
                    $volume_id
                )
            );
            $corrupted[] = $storedFile->get('name');
        } else {
            $this->io->success(
                __(
                    "Fichier {0} restauré sur volume id {1}",
                    $storedFile->get('name'),
                    $volume_id
                )
            );
        }
    }
}
