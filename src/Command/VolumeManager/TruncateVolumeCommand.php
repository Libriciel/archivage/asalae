<?php

/**
 * Asalae\Command\VolumeManager\TruncateVolumeCommand
 */

namespace Asalae\Command\VolumeManager;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;

/**
 * Détache un volume d'un espace de stockage
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TruncateVolumeCommand extends AbstractVolumeManagerCommand
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume_manager truncate_volume';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            '*************************************************************' . PHP_EOL
            . __(
                "Déconnecte un volume en supprimant toutes les références de la BDD"
            ) . PHP_EOL
            . '*************************************************************'
        );
        $parser->addArgument(
            'volume_id',
            [
                'help' => __(
                    "Identifiant (id) de l'espace de stockage à traiter"
                ),
                'required' => true,
            ]
        );
        $parser->addOption(
            'force',
            [
                'help' => __(
                    "Détruit un volume même s'il n'est pas accessible"
                ),
                'boolean' => true,
                'short' => 'f',
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $volume_id = $this->args->getArgument('volume_id');
        $loc = TableRegistry::getTableLocator();
        $Volumes = $loc->get('Volumes');
        $volume = $Volumes->get(1);
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        try {
            $driver = VolumeManager::getDriverById($volume_id);
        } catch (VolumeException) {
            $this->io->warning(__("Impossible de se connecter au volume"));
            $drop = $this->args->getOption('force')
                ? 'y'
                : $this->io->askChoice(
                    __(
                        "Les fichiers sur le volume ne seront pas supprimés. Voulez-vous continuer ?"
                    ),
                    ['y', 'n'],
                    'n'
                );
            if ($drop === 'y') {
                $StoredFilesVolumes->deleteAll(['volume_id' => $volume_id]);
                $volume->set('secure_data_space_id');
                $Volumes->saveOrFail($volume);
            }
            return;
        }

        $query = $StoredFilesVolumes->find()
            ->where(['volume_id' => $volume_id]);
        /** @var EntityInterface $link */
        foreach ($query as $link) {
            if ($driver->fileExists($link->get('storage_ref'))) {
                try {
                    $driver->fileDelete($link->get('storage_ref'));
                } catch (VolumeException) {
                    $this->io->err(
                        __(
                            "Impossible de supprimer le fichier ''{0}''",
                            $link->get('storage_ref')
                        )
                    );
                }
            }
            $StoredFilesVolumes->deleteAll(['id' => $link->id]);
        }

        $volume->set('secure_data_space_id');
        $Volumes->saveOrFail($volume);
    }
}
