<?php

/**
 * Asalae\Command\VolumeManager\DownloadCommand
 */

namespace Asalae\Command\VolumeManager;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Exception\StopException;

/**
 * Télécharge un fichier
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DownloadCommand extends AbstractVolumeManagerCommand
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume_manager download';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Télécharge un fichier") . PHP_EOL
            . '*************************************************************'
        );
        $parser->addArgument(
            'secure_data_space_id',
            [
                'help' => __("Identifiant de l'espace de stockage"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'storage_path',
            [
                'help' => __("Chemin relatif vers le fichier"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'destination',
            [
                'help' => __("Chemin absolu vers le fichier de destination"),
                'required' => true,
            ]
        );
        $parser->addOption(
            'overwrite',
            [
                'short' => 'o',
                'boolean' => true,
                'help' => __(
                    "Écrase le fichier si un fichier du même nom existe"
                ),
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $secure_data_space_id = $this->args->getArgument('secure_data_space_id');
        $storagePath = $this->args->getArgument('storage_path');
        $destination = $this->args->getArgument('destination');
        if (is_file($destination)) {
            if ($this->args->getOption('overwrite')) {
                unlink($destination);
            } else {
                $this->io->warning($msg = __("Un fichier du même nom existe déjà"));
                throw new StopException($msg, self::CODE_ERROR);
            }
        }

        $manager = new VolumeManager($secure_data_space_id);
        $manager->fileDownload($storagePath, $destination);
        if (is_file($destination)) {
            $this->io->success(__("Le fichier a été téléchargé avec succès"));
        } else {
            $this->io->warning($msg = __("Erreur lors du téléchargement"));
            throw new StopException($msg, self::CODE_ERROR);
        }
    }
}
