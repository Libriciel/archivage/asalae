<?php

/**
 * Asalae\Command\VolumeManager\ListCommand
 */

namespace Asalae\Command\VolumeManager;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\I18n\Date as CakeDate;
use Cake\ORM\TableRegistry;
use DateTimeInterface;

/**
 * Donne la liste des volumes
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ListCommand extends AbstractVolumeManagerCommand
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume_manager list';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Donne la liste des espaces de stockages") . PHP_EOL
            . '*************************************************************'
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $fields = ['id', 'name', 'description', 'entity'];
        $data = TableRegistry::getTableLocator()->get('SecureDataSpaces')->find()
            ->select(
                [
                    'SecureDataSpaces.id',
                    'SecureDataSpaces.name',
                    'SecureDataSpaces.description',
                    'entity' => 'OrgEntities.name',
                ]
            )
            ->orderBy(['SecureDataSpaces.name' => 'asc'])
            ->contain(['OrgEntities'])
            ->disableHydration()
            ->all()
            ->map(
                function ($data) {
                    foreach ($data as $k => $v) {
                        if (($v instanceof DateTimeInterface || $v instanceof CakeDate)) {
                            $data[$k] = $v->format('Y-m-d H:i:s');
                        }
                        $data[$k] = (string)$data[$k];
                    }
                    return $data;
                }
            )
            ->toArray();
        array_unshift($data, $fields);

        $this->io->helper('Table')->output($data);
    }
}
