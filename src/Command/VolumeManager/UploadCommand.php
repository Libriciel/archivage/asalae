<?php

/**
 * Asalae\Command\VolumeManager\UploadCommand
 */

namespace Asalae\Command\VolumeManager;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Exception\StopException;
use Cake\ORM\TableRegistry;

/**
 * Upload un fichier
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UploadCommand extends AbstractVolumeManagerCommand
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume_manager upload';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Upload un fichier") . PHP_EOL
            . '*************************************************************'
        );
        $parser->addArgument(
            'secure_data_space_id',
            [
                'help' => __("Identifiant de l'espace de stockage"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'upload_file',
            [
                'help' => __("Chemin absolu vers le fichier à uploader"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'storage_path',
            [
                'help' => __("Chemin relatif vers le fichier"),
                'required' => true,
            ]
        );
        $parser->addOption(
            'overwrite',
            [
                'short' => 'o',
                'boolean' => true,
                'help' => __(
                    "Écrase le fichier si un fichier du même nom existe"
                ),
            ]
        );
        $parser->addOption(
            'sleep-then-delete',
            [
                'help' => '<integer> - ' . __(
                    "Supprimera le fichier uploadé dans X secondes"
                ),
            ]
        );
        $parser->addOption(
            'set-event-in-lifecycle',
            [
                'help' => '<integer> - '
                    . __(
                        "ID de l'evenement sur lequel appliquer un {0}",
                        'in_lifecycle'
                    ),
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $secure_data_space_id = $this->args->getArgument('secure_data_space_id');
        $uploadFile = $this->args->getArgument('upload_file');
        $storagePath = $this->args->getArgument('storage_path');
        if (!is_file($uploadFile)) {
            $this->io->warning($msg = __("Le fichier à uploader n'existe pas"));
            throw new StopException($msg, self::CODE_ERROR);
        }

        $manager = new VolumeManager($secure_data_space_id);
        if ($manager->fileExists($storagePath)) {
            if ($this->args->getOption('overwrite')) {
                $storedFile = TableRegistry::getTableLocator()->get('StoredFiles')
                    ->find()
                    ->where(
                        [
                            'StoredFiles.secure_data_space_id' => $secure_data_space_id,
                            'StoredFiles.name' => $storagePath,
                        ]
                    )
                    ->firstOrFail();
                $manager->replace($storedFile->id, $uploadFile);
            } else {
                throw new VolumeException(VolumeException::FILE_ALREADY_EXISTS);
            }
        } else {
            $manager->fileUpload($uploadFile, $storagePath);
        }
        if ($manager->fileExists($storagePath)) {
            $this->io->success(__("Le fichier a été uploadé avec succès"));
            if ($eventIds = $this->args->getOption('set-event-in-lifecycle')) {
                foreach (explode(',', $eventIds) as $eventId) {
                    $EventLogs = TableRegistry::getTableLocator()->get(
                        'EventLogs'
                    );
                    $EventLogs->updateAll(
                        ['in_lifecycle' => true],
                        ['id' => $eventId]
                    );
                }
            }
            // supprime le fichier cache au bout de 600s si le fichier n'a pas été locké à nouveau
            if ($this->args->getOption('sleep-then-delete') && is_file($uploadFile . '.lock')) {
                unlink($uploadFile . '.lock');
                sleep((int)$this->args->getOption('sleep-then-delete'));//NOSONAR
                if (!is_file($uploadFile . '.lock')) {
                    unlink($uploadFile);
                }
            }
        } else {
            $this->io->warning($msg = __("Erreur lors de l'upload"));
            throw new StopException($msg, self::CODE_ERROR);
        }
    }
}
