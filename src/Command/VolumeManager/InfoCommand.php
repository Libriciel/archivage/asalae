<?php

/**
 * Asalae\Command\VolumeManager\TruncateVolumeCommand
 */

namespace Asalae\Command\VolumeManager;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/**
 * Permet d'optinir des informations sur un enregistrement
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class InfoCommand extends AbstractVolumeManagerCommand
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume_manager info';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Permet d'optinir des informations sur un enregistrement") . PHP_EOL
            . '*************************************************************'
        );
        $parser->addArgument(
            'storage_path',
            [
                'help' => __("Chemin vers le fichier (stored_files.name)"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $storagePath = $this->args->getArgument('storage_path');
        $fields = ['key', 'value'];
        $storedFile = TableRegistry::getTableLocator()->get('StoredFiles')->find()
            ->where(['StoredFiles.name' => $storagePath])
            ->contain(
                [
                    'Volumes',
                    'ArchiveFiles' => [
                        'Archives' => ['Transfers'],
                    ],
                    'ArchiveBinaries' => [
                        'ArchiveUnits' => [
                            'Archives' => ['Transfers'],
                        ],
                    ],
                ]
            )
            ->first();
        $sep = str_repeat('-', 64);
        $data = [
            ['key' => '', 'value' => ''],
            ['key' => $this->title('stored_file'), 'value' => $sep],
            ['key' => 'id', 'value' => $storedFile->get('id')],
            ['key' => 'name', 'value' => $storedFile->get('name')],
            ['key' => 'hash', 'value' => $storedFile->get('hash')],
            ['key' => 'hash_algo', 'value' => $storedFile->get('hash_algo')],
            ['key' => 'size', 'value' => Number::toReadableSize($storedFile->get('size'))],
            ['key' => 'secure_data_space_id', 'value' => $storedFile->get('secure_data_space_id')],
            ['key' => 'created', 'value' => $storedFile->get('created')],
        ];

        foreach ($storedFile->get('volumes') as $volume) {
            $data[] = ['key' => '', 'value' => ''];
            $data[] = ['key' => $this->title('volume'), 'value' => $sep];
            $data[] = ['key' => 'id', 'value' => $volume->get('id')];
            $data[] = ['key' => 'name', 'value' => $volume->get('name')];
            $data[] = ['key' => 'read_duration', 'value' => $volume->get('read_duration') . 's'];
            $data[] = [
                'key' => 'is_integrity_ok',
                'value' => var_export($volume->get('_joinData')['is_integrity_ok'], true),
            ];
            $data[] = ['key' => 'driver', 'value' => $volume->get('driver')];
            if ($volume->get('driver') === 'FILESYSTEM') {
                $data[] = [
                    'key' => 'directory',
                    'value' => json_decode($volume->get('fields'), true)['path'],
                ];
            } else {
                $data[] = [
                    'key' => 'endpoint',
                    'value' => json_decode($volume->get('fields'), true)['endpoint'],
                ];
            }
        }
        foreach ($storedFile->get('archive_files') as $archiveFile) {
            $data[] = ['key' => '', 'value' => ''];
            $data[] = ['key' => $this->title('archive_file'), 'value' => $sep];
            $data[] = ['key' => 'id', 'value' => $archiveFile->get('id')];
            $data[] = ['key' => 'type', 'value' => $archiveFile->get('type')];
            $data[] = [
                'key' => 'is_integrity_ok',
                'value' => var_export($archiveFile->get('is_integrity_ok'), true),
            ];
        }
        $archive = Hash::get($storedFile, 'archive_files.0.archive');
        if ($archive) {
            $this->appendArchive($data, $archive);
        }
        foreach ($storedFile->get('archive_binaries') as $archiveBinary) {
            $data[] = ['key' => '', 'value' => ''];
            $data[] = ['key' => $this->title('archive_binary'), 'value' => $sep];
            $data[] = ['key' => 'id', 'value' => $archiveBinary->get('id')];
            $data[] = ['key' => 'type', 'value' => $archiveBinary->get('type')];
            $data[] = ['key' => 'mime', 'value' => $archiveBinary->get('mime')];
            $data[] = ['key' => 'format', 'value' => $archiveBinary->get('format')];
            $data[] = ['key' => 'extension', 'value' => $archiveBinary->get('extension')];
            $data[] = [
                'key' => 'is_integrity_ok',
                'value' => var_export($archiveBinary->get('is_integrity_ok'), true),
            ];

            foreach ($archiveBinary->get('archive_units') as $archiveUnit) {
                $data[] = ['key' => '', 'value' => ''];
                $data[] = ['key' => $this->title('archive_unit'), 'value' => $sep];
                $data[] = ['key' => 'id', 'value' => $archiveUnit->get('id')];
                $data[] = [
                    'key' => 'archival_agency_identifier',
                    'value' => $archiveUnit->get('archival_agency_identifier'),
                ];
                $data[] = ['key' => 'xpath', 'value' => $archiveUnit->get('xpath')];
            }
        }
        $archive = Hash::get($storedFile, 'archive_binaries.0.archive_units.0.archive');
        if ($archive) {
            $this->appendArchive($data, $archive);
        }

        array_unshift($data, $fields);

        $this->io->helper('Table')->output($data);
    }
}
