<?php

/**
 * Asalae\Command\EventlogsThreadCommand
 */

namespace Asalae\Command;

use Asalae\Model\Entity\StoredFile;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\TechnicalArchivesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Utility\DOMUtility;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DOMDocument;
use DOMElement;

/**
 * Vérification du chaînage des event logs pour une archive
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2023, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EventLogsThreadCommand extends Command
{
    /**
     * @var VolumeManager[]
     */
    private $volumeManagers = [];

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'event_logs thread';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();
        $parser->setDescription(__("Vérification du chaînage des journaux d'événements pour une archive"));
        $parser->addArgument(
            'archival_agency_identifier',
            [
                'help' => __("Identifiant du Service d'Archives"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'archive_identifier',
            [
                'help' => __("Identifiant de l'archive"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io): int
    {
        $archivalAgencyIdentifier = $args->getArgument('archival_agency_identifier');
        $archiveIdentifier = $args->getArgument('archive_identifier');

        $loc = TableRegistry::getTableLocator();
        $sa = $loc->get('OrgEntities')
            ->find()
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'identifier' => $archivalAgencyIdentifier,
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES,
                ]
            )
            ->firstOrFail();

        $archive = $loc->get('Archives')
            ->find()
            ->where(
                [
                    'Archives.archival_agency_identifier' => $archiveIdentifier,
                    'Archives.archival_agency_id' => $sa->get('id'),
                ]
            )
            ->contain(
                [
                    'Transfers',
                    'ArchiveUnits' => ['ArchiveBinaries'],
                    'ArchiveFiles' => ['StoredFiles' => ['SecureDataSpaces', 'Volumes']],
                ]
            )
            ->firstOrFail();

        $states = [
            ArchivesTable::S_CREATING,
            ArchivesTable::S_DESCRIBING,
            ArchivesTable::S_STORING,
            ArchivesTable::S_MANAGING,
        ];
        if (in_array($archive->get('state'), $states)) {
            $io->error(__("L'état de l'archive doit être au moins disponible"));
            return static::CODE_ERROR;
        }

        $query = $this->getTechnicalArchivesFiles($sa->get('id'));
        // récupérer tous les events liés à :
        //  -   Transfers:<id>
        //  -   Archives:<id>
        //  -   ArchiveBinaries:<id>
        //  -   ArchiveFiles:<id>
        //  -   ArchiveUnits:<i>
        $eventsIdentifier = array_merge(
            [
                "text()='Archives:{$archive->get('id')}'",
            ],
            array_map(
                fn($e) => "text()='Transfers:$e'",
                Hash::extract($archive, 'transfers.{n}.id')
            ),
            array_map(
                fn($e) => "text()='ArchiveBinaries:$e'",
                Hash::extract($archive, 'archive_units.{n}.archive_binaries.{n}.id')
            ),
            array_map(
                fn($e) => "text()='ArchiveFiles:$e'",
                Hash::extract($archive, 'archive_files.{n}.id')
            ),
            array_map(
                fn($e) => "text()='ArchiveUnits:$e'",
                Hash::extract($archive, 'archive_units.{n}.id')
            ),
        );

        /** @var DOMElement[][] $events */
        $events = [];
        $prevName = null;
        $prevFilename = null;
        $prevHash = null;
        $prevSize = null;
        $workDone = false;

        /** @var StoredFile $storedFile */
        foreach ($query as $storedFile) {
            $filename = $storedFile->get('name');
            $events[$filename] = [];
            $volumeManager = $this->getVolumeManager($storedFile->get('secure_data_space_id'));
            $content = $volumeManager->fileGetContent($filename);

            $currentName = basename($filename);
            $currentHash = hash($storedFile->get('hash_algo'), $content);
            $currentSize = strlen($content);

            $dom = new DOMDocument();
            $dom->loadXML($content);
            $util = new DOMUtility($dom);
            $logChainNode = $util->node(
                'ns:object/ns:objectIdentifier/ns:objectIdentifierValue[text()="EventsLogsChain"]/../..'
            );

            $eventsQuery = $util->xpath->query(
                sprintf(
                    'ns:event/ns:linkingObjectIdentifier/ns:linkingObjectIdentifierValue[%s]/../..',
                    implode(' or ', $eventsIdentifier)
                )
            );

            /** @var DOMElement $event */
            foreach ($eventsQuery as $event) {
                $eventType = $util->nodeValue('.//ns:eventType', $event);
                if ($eventType === 'transfer_add') {
                    $workDone = true;
                }
                $events[$filename][] = [
                    'date' => $util->nodeValue('.//ns:eventDateTime[text()]', $event),
                    'event' => $this->formatEvent($event, $util),
                ];
            }

            $xmlName = $logChainNode ?
                (string)$util->nodeValue(
                    './/ns:significantPropertiesType[text()="filename"]/following-sibling::*',
                    $logChainNode
                )
                : null;
            $xmlHash = $logChainNode ?
                (string)$util->nodeValue(
                    './/ns:significantPropertiesType[text()="hash"]/following-sibling::*',
                    $logChainNode
                )
                : null;
            $xmlSize = $logChainNode ?
                (int)$util->nodeValue(
                    './/ns:significantPropertiesType[text()="size"]/following-sibling::*',
                    $logChainNode
                )
                : null;

            if ($prevName === null) { // pour le premier
                $prevFilename = $filename;
                $prevName = $xmlName;
                $prevHash = $xmlHash;
                $prevSize = $xmlSize;
                continue;
            }

            if ($prevName !== $currentName) {
                $io->err(
                    __(
                        "Les fichiers <error>{0}</error> (chaînage) et <error>{1}</error> (actuel) "
                        . "présentent une erreur de chaînage au niveau du nom",
                        $prevFilename,
                        $currentName
                    )
                );
                $io->err(__("Valeur du chaînage : <error>{0}</error>", $prevName));
                $io->abort(__("valeur du fichier précédent : <error>{0}</error>", $currentName));
            }
            if ($prevHash !== $currentHash) {
                $io->err(
                    __(
                        "Les fichiers <error>{0}</error> (chaînage) et <error>{1}</error> (actuel) "
                        . "présentent une erreur de chaînage au niveau du hash",
                        $prevFilename,
                        $currentName
                    )
                );
                $io->err(__("Valeur du chaînage : <error>{0}</error>", $prevHash));
                $io->abort(__("valeur du fichier précédent : <error>{0}</error>", $currentHash));
            }
            if ($prevSize !== $currentSize) {
                $io->err(
                    __(
                        "Les fichiers <error>{0}</error> (chaînage) et <error>{1}</error> (actuel) "
                        . "présentent une erreur de chaînage au niveau de la taille",
                        $prevFilename,
                        $currentName
                    )
                );
                $io->err(__("Valeur du chaînage : <error>{0}</error>", $prevSize));
                $io->abort(__("valeur du fichier précédent : <error>{0}</error>", $currentSize));
            }

            if ($workDone) { // pas besoin d'aller plus loin passé la création du transfert (transfer_add)
                break;
            }

            $prevFilename = $filename;
            $prevName = $xmlName;
            $prevHash = $xmlHash;
            $prevSize = $xmlSize;
        }

        // affichage
        foreach (array_reverse($events, true) as $eventLogName => $eventLog) {
            $out = __("Export du journal : <info>{0}</info>", $eventLogName);
            if (count($eventLog)) {
                $io->out($out, 2);
            } else {
                $io->verbose($out, 2);
            }
            foreach ($eventLog as $event) {
                $io->out($event);
            }
        }

        return static::CODE_SUCCESS;
    }

    /**
     * Requête pour les stored files des archives techniques
     * @param int $saId
     * @return Query
     */
    private function getTechnicalArchivesFiles(int $saId): Query
    {
        return TableRegistry::getTableLocator()->get('StoredFiles')->find()
            ->innerJoinWith('ArchiveBinaries')
            ->innerJoinWith('ArchiveBinaries.TechnicalArchiveUnits')
            ->innerJoinWith(
                'ArchiveBinaries.TechnicalArchiveUnits.TechnicalArchives'
            )
            ->where(
                [
                    'ArchiveBinaries.type' => 'original_data',
                    'TechnicalArchives.type' => TechnicalArchivesTable::TYPE_EVENTS_LOGS,
                    'TechnicalArchives.archival_agency_id' => $saId,
                ]
            )
            ->orderBy(
                [
                    'TechnicalArchives.created' => 'desc',
                    'ArchiveBinaries.filename' => 'desc',
                ]
            )
            ->contain('ArchiveBinaries');
    }

    /**
     * Donne l'ECS avec mise en cache
     * @param int $secure_data_space_id
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(int $secure_data_space_id): VolumeManager
    {
        if (!isset($this->volumeManagers[$secure_data_space_id])) {
            $this->volumeManagers[$secure_data_space_id] = new VolumeManager($secure_data_space_id);
        }
        return $this->volumeManagers[$secure_data_space_id];
    }

    /**
     * Formatage propre de l'event pour affichage
     * @param DOMElement $event
     * @param DOMUtility $util
     * @return string
     */
    private function formatEvent(DOMElement $event, DOMUtility $util): string
    {
        return sprintf(
            "type:\t\t%s\noutcome:\t%s\ndetail:\t\t%s\n",
            $util->nodeValue('.//ns:eventType', $event),
            $util->nodeValue('.//ns:eventOutcome', $event),
            $util->nodeValue('.//ns:eventOutcomeDetailNote', $event),
        );
    }
}
