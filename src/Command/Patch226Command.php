<?php

/**
 * Asalae\Command\Patch226Command
 */

namespace Asalae\Command;

use Asalae\Model\Table\ArchiveUnitsTable;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Database\Expression\IdentifierExpression;
use Exception;

/**
 * Patch 2.2.6
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Patch226Command extends Command
{
    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();

        $parser->setDescription(__("Calcul du max_aggregated_access_end_date des archive_units"));

        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $q = $ArchiveUnits->updateQuery();

        $io->out(__("Mise à jour du champ ArchiveUnits.description_access_rule_id"));
        $subquery = $this->fetchTable('ArchiveDescriptions')->find()
            ->select(['ArchiveDescriptions.access_rule_id'])
            ->where(['ArchiveDescriptions.archive_unit_id' => new IdentifierExpression('archive_units.id')]);
        $ArchiveUnits->updateQuery()
            ->set(['description_access_rule_id' => $subquery])
            ->where(['xml_node_tagname NOT LIKE' => 'ArchiveUnit%'])
            ->execute();

        $io->out(__("Mise à jour du champ ArchiveUnits.max_keywords_access_end_date"));
        $subquery = $this->fetchTable('ArchiveKeywords')->find()
            ->select(['access_rule_id' => $q->func()->max('AccessRules.end_date')])
            ->innerJoinWith('AccessRules')
            ->where(
                [
                    'ArchiveKeywords.archive_unit_id' => new IdentifierExpression('archive_units.id'),
                    'ArchiveKeywords.access_rule_id !='
                        => new IdentifierExpression('archive_units.description_access_rule_id'),
                ]
            );
        $ArchiveUnits->updateQuery()
            ->set(['max_keywords_access_end_date' => $subquery])
            ->where(['xml_node_tagname NOT LIKE' => 'ArchiveUnit%'])
            ->execute();

        $io->out(__("Mise à jour du champ ArchiveUnits.max_aggregated_access_end_date"));
        $io->out(__("end_date des access_rules..."));

        $mtime = microtime(true);
        $fn = function ($i, $count, $type) use ($io, &$mtime) {
            if (microtime(true) - $mtime > 10.0) {
                $mtime = microtime(true);
                if ($type === 'access_rules') {
                    $io->out(__("AccessRule {0}/{1}", $i, $count));
                } else {
                    $io->out(__("ArchiveUnit {0}/{1}", $i, $count));
                }
            }
        };
        $updatedCount = $ArchiveUnits->updateAll(['max_aggregated_access_end_date' => null], []);
        $ArchiveUnits->calcMaxAggregatedAccessEndDate([], $fn);

        $io->out(__("  {0} unités d'archives mis à jour", $updatedCount));

        $io->success('done');
    }
}
