<?php

/**
 * Asalae\Command\ChangePathToDataCommand
 */

namespace Asalae\Command;

use AsalaeCore\Utility\Config;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Table;
use Exception;

/**
 * Modifi le chemin de configuration App.paths.data
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2023, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ChangePathToDataCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'change_path_to_data';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __(
                "Permet de modifier la config App.paths.data. Modifi "
                . "uniquement la configuration ainsi que les références en base "
                . "de données, ne déplace aucun fichiers."
            )
        );
        $parser->addArgument(
            'new_path',
            [
                'help' => __("Chemin vers le nouveau data"),
                'required' => true,
            ]
        );
        $parser->addOption(
            'dryrun',
            [
                'help' => __("Rollback les modifications"),
                'boolean' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;
        $path = $args->getArgument('new_path');
        if (!file_exists($path)) {
            $io->abort(sprintf("%s does not exists", $path));
        } elseif (!is_dir($path)) {
            $io->abort(sprintf("%s is not a directory", $path));
        } elseif (!is_readable($path)) {
            $io->abort(sprintf("%s is not readable", $path));
        } elseif (!is_writable($path)) {
            $io->abort(sprintf("%s is not writable", $path));
        } elseif (substr($path, -1) === '/') {
            $io->abort(sprintf("%s must not end with /", $path));
        }

        $originalPathToData = Configure::read('App.paths.data') . '/';
        $originalPathToDataLen = strlen($originalPathToData);
        $originalEscapedPathToData = str_replace('/', '\\\\/', $originalPathToData);
        $databaseFields = [
            'Fileuploads.path',
            'Mediainfos.complete_name',
            'Timestampers.fields',
            'Volumes.fields',
        ];
        $conn = ConnectionManager::get('default');
        $conn->begin();
        foreach ($databaseFields as $key) {
            [$modelName, $field] = explode('.', $key);
            $Model = $this->fetchTable($modelName);
            $query = $Model->find()
                ->where(
                    [
                        'OR' => [
                            ["$key LIKE" => "%$originalPathToData%"],
                            ["$key LIKE" => "%$originalEscapedPathToData%"],
                        ],
                    ]
                );
            /** @var EntityInterface $entity */
            foreach ($query as $entity) {
                $oldValue = $entity->get($field);
                if (@json_decode($oldValue, true)) {
                    $this->updateJsonField($Model, $entity, $field, $oldValue, $path);
                    continue;
                }

                // cas commence par App.paths.data
                $pos = strpos($oldValue, $originalPathToData);
                if ($pos === 0) {
                    $newValue = $path . '/' . substr($oldValue, $originalPathToDataLen);
                    if ($newValue !== $oldValue) {
                        $this->updateNewValue($Model, $entity, $field, $oldValue, $newValue);
                    }
                }
            }
        }
        if ($args->getOption('dryrun')) {
            $conn->rollback();
        } else {
            $conn->commit();
            Config::writeAndSave('App.paths.data', $path);
        }

        $io->out('done');
    }

    /**
     * Met à jour un champ json
     * @param Table           $Model
     * @param EntityInterface $entity
     * @param string          $field
     * @param string          $oldValue
     * @param string          $path
     * @return void
     */
    private function updateJsonField(
        Table $Model,
        EntityInterface $entity,
        string $field,
        string $oldValue,
        string $path
    ) {
        $escapedPath = str_replace('/', '\\/', $path);
        $originalPathToData = Configure::read('App.paths.data') . '/';
        $originalPathToDataLen = strlen($originalPathToData);
        $originalEscapedPathToData = str_replace('/', '\\/', $originalPathToData);
        $originalEscapedPathToDataLen = strlen($originalEscapedPathToData);

        $pos = strpos($oldValue, '"' . $originalPathToData);
        // cas dans un champ json : caractère " suivi du path
        if ($pos !== false) {
            $newValue = sprintf(
                '%s"%s/%s',
                substr($oldValue, 0, $pos),
                $path,
                substr($oldValue, $pos + $originalPathToDataLen + 1)
            );
            if ($newValue === $oldValue) {
                return;
            }
            if (@json_decode($newValue, true)) {
                $this->updateNewValue($Model, $entity, $field, $oldValue, $newValue);
            } else {
                $this->io->abort(sprintf('not a valid json: %s', $newValue));
            }
            // on vérifi s'il reste des références
            $this->updateJsonField($Model, $entity, $field, $newValue, $path);
            return;
        }

        $pos = strpos($oldValue, '"' . $originalEscapedPathToData);
        if ($pos !== false) {
            // cas dans un champ json avec / échapés : caractère " suivi du path échapé
            $newValue = sprintf(
                '%s"%s\\/%s',
                substr($oldValue, 0, $pos),
                $escapedPath,
                substr($oldValue, $pos + $originalEscapedPathToDataLen + 1)
            );
            if ($newValue === $oldValue) {
                return;
            }
            if (@json_decode($newValue, true)) {
                $this->updateNewValue($Model, $entity, $field, $oldValue, $newValue);
            } else {
                $this->io->abort(sprintf('not a valid json: %s', $newValue));
            }
            // on vérifi s'il reste des références
            $this->updateJsonField($Model, $entity, $field, $newValue, $path);
        }
    }

    /**
     * Met à jour un champs avec le nouveau chemin
     * @param Table           $Model
     * @param EntityInterface $entity
     * @param string          $field
     * @param string          $oldValue
     * @param string          $newValue
     * @return void
     */
    private function updateNewValue(
        Table $Model,
        EntityInterface $entity,
        string $field,
        string $oldValue,
        string $newValue
    ) {
        $this->io->out(
            sprintf(
                "%s.%s id=%d :\nold: %s\nnew: %s\n",
                $Model->getAlias(),
                $field,
                $entity->get('id'),
                $oldValue,
                $newValue
            )
        );
        if (!$this->args->getOption('dryrun')) {
            $Model->updateAll(
                [$field => $newValue],
                ['id' => $entity->get('id')]
            );
        }
    }
}
