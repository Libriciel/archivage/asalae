<?php

/**
 * Asalae\Command\DataCoherenceGetErrorCommand
 */

namespace Asalae\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Vérifie une entité en particulier et donne les erreurs
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DataCoherenceGetErrorCommand extends DataCoherenceAllCommand
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'data_coherence get_error';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserGetError = parent::getOptionParser();
        $parserGetError->setDescription(
            '*************************************************************' . PHP_EOL
            . __(
                "Donne l'erreur pour une référence de type ModelName:id"
            ) . PHP_EOL
            . '*************************************************************'
        );
        $parserGetError->addArgument(
            'ref',
            [
                'help' => __("Référence au format ModelName:id"),
                'required' => true,
            ]
        );
        return $parserGetError;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $linkEntity = $this->args->getArgument('ref');
        if (!strpos($linkEntity, ':')) {
            $this->io->abort(
                __("Vous devez indiquer une référence au format ModelName:id")
            );
        }
        [$modelName, $id] = explode(':', $linkEntity);
        $loc = TableRegistry::getTableLocator();
        $model = $loc->get($modelName);
        $entity = $model->get($id);
        $model->patchEntity($entity, $entity->toArray());
        if (!$entity->getErrors()) {
            $this->io->out(__("Aucune erreur détectée"));
        }
        foreach ($entity->getErrors() as $fieldname => $errors) {
            foreach ($errors as $error) {
                $this->io->err(sprintf('%s: %s', $fieldname, $error));
            }
        }
    }
}
