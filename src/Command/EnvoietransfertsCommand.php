<?php

/**
 * Asalae\Command\EnvoietransfertsCommand
 */

namespace Asalae\Command;

use Asalae\Model\Table\OrgEntitiesTable;
use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Command\PromptOptionsTrait;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\CommandInterface;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Datacompressor\Utility\DataCompressor;
use DOMDocument;
use Exception;
use Faker\Factory;
use Faker\Generator;
use FileConverters\Utility\FileConverters;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;
use Libriciel\Filesystem\Utility\Filesystem;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as SymfonfyResponse;

/**
 * Génère des transferts aléatoire
 *
 * @category    Command
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EnvoietransfertsCommand extends Command
{
    /**
     * Traits
     */
    use PromptOptionsTrait;

    /**
     * @var string url vers le webservice de transferts de l'application
     */
    public const string TARGET_URL = 'api/restservices/seda-messages';
    /**
     * @var string url vers le webservice de version de l'application
     */
    public const string TARGET_VERSION_URL = 'restservices/versions';
    /**
     * @var array liste des formats des fichiers des pièces jointes
     */
    public const array ALLOWED_ATTACHMENT_EXTENSIONS = ['txt', 'odt', 'pdf', 'png', 'jpg'];
    /**
     * @var array liste des codes du sort final
     */
    public const array ALLOWED_APPRAISAL_CODES = ['conserver', 'detruire'];
    /**
     * @var array liste des versions du seda
     */
    public const array ALLOWED_SEDA_VERSIONS = ['0.2', '1.0'];
    public const string TMPL_SEDA02 = <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<ArchiveTransfer xmlns="fr:gouv:ae:archive:draft:standard_echange_v0.2">
  <Comment>{{archiveTransferComment}}</Comment>
  <Date>{{date}}</Date>
  <TransferIdentifier>{{archiveTransferIdentifier}}</TransferIdentifier>
  <TransferringAgency>
    <Identification>{{serviceversant}}</Identification>
  </TransferringAgency>
  <ArchivalAgency>
    <Identification>{{servicearchives}}</Identification>
  </ArchivalAgency>
  {{integrity}}
  <Contains>
    <ArchivalAgreement>{{accordversement}}</ArchivalAgreement>
    {{profilarchive}}
    <DescriptionLanguage listVersionID="edition 2009">fr</DescriptionLanguage>
    <DescriptionLevel listVersionID="edition 2009">item</DescriptionLevel>
    <Name>{{archiveName}}</Name>
    <ContentDescription>
      <Language listVersionID="edition 2009">fr</Language>
      <OriginatingAgency>
        <Identification>{{serviceproducteur}}</Identification>
      </OriginatingAgency>
      <ContentDescriptive>
        <KeywordContent>motclearchive01</KeywordContent>
        <KeywordReference>ref motclearchive01</KeywordReference>
      </ContentDescriptive>
      <ContentDescriptive>
        <KeywordContent>motclearchive02</KeywordContent>
        <KeywordReference>ref motclearchive02</KeywordReference>
      </ContentDescriptive>
    </ContentDescription>
    <Appraisal>
      <Code listVersionID="edition 2009">{{sortFinal}}</Code>
      <Duration>P{{duaAnnees}}Y</Duration>
      <StartDate>{{duaStartDate}}</StartDate>
    </Appraisal>
    {{documentBloc1}}
    <Contains>
      <DescriptionLevel listVersionID="edition 2009">item</DescriptionLevel>
      <Name>Objet d'archive 1</Name>
      <ContentDescription>
        <Language listVersionID="edition 2009">fr</Language>
        <ContentDescriptive>
          <KeywordContent>motcleobjetarchive01</KeywordContent>
          <KeywordReference>ref motcleobjetarchive01</KeywordReference>
        </ContentDescriptive>
        <ContentDescriptive>
          <KeywordContent>motcleobjetarchive02</KeywordContent>
          <KeywordReference>ref motcleobjetarchive02</KeywordReference>
        </ContentDescriptive>
      </ContentDescription>
      {{documentBloc2}}
      <Contains>
        <DescriptionLevel listVersionID="edition 2009">item</DescriptionLevel>
        <Name>Objet d'archive 2</Name>
        {{documentBloc3}}
      </Contains>
    </Contains>
  </Contains>
</ArchiveTransfer>
EOT;
    public const string TMPL_DOC_BLOCK_SEDA02 = <<<EOT
    <Document>
      <Attachment filename="{{filename}}"></Attachment>
      <Type listVersionID="edition 2009">CDO</Type>
    </Document>
EOT;
    public const string TMPL_INTEGRITY_BLOCK_SEDA02 = <<<EOT
<Integrity>
    <Contains algorithme="http://www.w3.org/2001/04/xmlenc#sha256">{{sha256}}</Contains>
    <UnitIdentifier>{{filename}}</UnitIdentifier>
  </Integrity>

EOT;
    public const string TMPL_SEDA10 = <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<ArchiveTransfer xmlns="fr:gouv:culture:archivesdefrance:seda:v1.0">
  <Comment>{{archiveTransferComment}}</Comment>
  <Date>{{date}}</Date>
  <TransferIdentifier>{{archiveTransferIdentifier}}</TransferIdentifier>
  <ArchivalAgency>
    <Identification>{{servicearchives}}</Identification>
  </ArchivalAgency>
  <TransferringAgency>
    <Identification>{{serviceversant}}</Identification>
  </TransferringAgency>
  <Archive>
    <ArchivalAgreement>{{accordversement}}</ArchivalAgreement>
    {{profilarchive}}
    <DescriptionLanguage listVersionID="edition 2011">fra</DescriptionLanguage>
    <Name>{{archiveName}}</Name>
    <ContentDescription>
      <DescriptionLevel listVersionID="edition 2009">item</DescriptionLevel>
      <Language listVersionID="edition 2011">fra</Language>
      <Keyword>
        <KeywordContent>motclearchive01</KeywordContent>
        <KeywordReference>ref motclearchive01</KeywordReference>
      </Keyword>
      <Keyword>
        <KeywordContent>motclearchive02</KeywordContent>
        <KeywordReference>ref motclearchive02</KeywordReference>
      </Keyword>
      <OriginatingAgency>
        <Identification>{{serviceproducteur}}</Identification>
      </OriginatingAgency>
    </ContentDescription>
    <AccessRestrictionRule>
      <Code listVersionID="edition 2009">AR038</Code>
      <StartDate>2019-11-15</StartDate>
    </AccessRestrictionRule>
    <AppraisalRule>
      <Code listVersionID="edition 2009">{{sortFinal}}</Code>
      <Duration>P{{duaAnnees}}Y</Duration>
      <StartDate>{{duaStartDate}}</StartDate>
    </AppraisalRule>
    <ArchiveObject>
      <Name>Objet d'archive 1</Name>
      <ContentDescription>
        <DescriptionLevel listVersionID="edition 2009">item</DescriptionLevel>
        <Language listVersionID="edition 2011">fra</Language>
        <Keyword>
          <KeywordContent>motcleobjetarchive01</KeywordContent>
          <KeywordReference>ref motcleobjetarchive01</KeywordReference>
        </Keyword>
        <Keyword>
          <KeywordContent>motcleobjetarchive02</KeywordContent>
          <KeywordReference>ref motcleobjetarchive02</KeywordReference>
        </Keyword>
      </ContentDescription>
      <ArchiveObject>
        <Name>Objet d'archive 2</Name>
        {{documentBloc3}}
      </ArchiveObject>
      {{documentBloc2}}
    </ArchiveObject>
    {{documentBloc1}}
  </Archive>
</ArchiveTransfer>
EOT;
    public const string TMPL_DOC_BLOCK_SEDA10 = <<<EOT
    <Document>
      <Attachment filename="{{filename}}"></Attachment>
      <Integrity algorithme="http://www.w3.org/2001/04/xmlenc#sha256">{{sha256}}</Integrity>
      <Language listVersionID="edition 2011">fra</Language>
      <Size unitCode="AD">{{filesize}}</Size>
      <Type listVersionID="edition 2009">CDO</Type>
    </Document>
EOT;
    /**
     * @var Generator instance de Faker
     */
    public static $Faker;
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;
    /**
     * @var array  liste des fichiers des pièces jointes
     */
    private $files = [];

    /**
     * Constructeur de classe
     */
    public function __construct()
    {
        if (empty(self::$Faker)) {
            self::$Faker = Factory::create();
        }
    }

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'envoietransferts';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities');
        $sa = $OrgEntities->find()
            ->where(['is_main_archival_agency IS' => true])
            ->first();
        if ($sa) {
            $ta = $OrgEntities->find(
                'list',
                valueField: 'identifier'
            )
                ->orderBy(['OrgEntities.name'])
                ->innerJoinWith('TypeEntities')
                ->where(
                    [
                        'TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES,
                        'OrgEntities.lft >=' => $sa->get('lft'),
                        'OrgEntities.rght <=' => $sa->get('rght'),
                    ]
                )
                ->all()
                ->toArray();
            $oa = $OrgEntities->find('list', valueField: 'identifier')
                ->innerJoinWith('TypeEntities')
                ->where(
                    [
                        'TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES,
                        'OrgEntities.lft >=' => $sa->get('lft'),
                        'OrgEntities.rght <=' => $sa->get('rght'),
                    ]
                )
                ->orderBy(['OrgEntities.name' => 'asc'])
                ->all()
                ->toArray();
            $agreements = $loc->get('Agreements')->find(
                'list',
                valueField: 'identifier'
            )
                ->where(['org_entity_id' => $sa->id, 'active' => true])
                ->orderBy(['name' => 'asc'])
                ->all()
                ->toArray();
            $profiles = $loc->get('Profiles')->find(
                'list',
                valueField: 'identifier'
            )
                ->where(['org_entity_id' => $sa->id, 'active' => true])
                ->orderBy(['name' => 'asc'])
                ->all()
                ->toArray();
        }

        $parser = new AppConsoleOptionParser();
        $parser->addOption(
            'url',
            [
                'short' => 'u',
                'help' => __d(
                    'envoietransferts_shell',
                    "url de base de asalae (ex: https://www.asalae.org)"
                ),
                'default' => Configure::read('App.fullBaseUrl'),
            ]
        );
        $parser->addOption(
            'wslogin',
            [
                'short' => 'l',
                'help' => __d(
                    'envoietransferts_shell',
                    "identifiant de connexion pour les webservices"
                ),
                'default' => 'admin',
            ]
        );
        $parser->addOption(
            'wspasswd',
            [
                'short' => 'p',
                'password' => true,
                'help' => __d(
                    'envoietransferts_shell',
                    "mot de passe de connexion pour les webservices"
                ),
            ]
        );
        $parser->addOption(
            'nbtransferts',
            [
                'short' => 'n',
                'help' => __d('envoietransferts_shell', "nombre de transferts"),
                'default' => '3',
            ]
        );
        $parser->addOption(
            'servicearchives',
            [
                'short' => 'a',
                'help' => __d(
                    'envoietransferts_shell',
                    "identifiant service d'archives"
                ),
                'default' => $sa ? $sa->get('identifier') : 'FRAD000',
            ]
        );
        $parser->addOption(
            'serviceversant',
            [
                'short' => 'b',
                'help' => __d(
                    'envoietransferts_shell',
                    "identifiant service versant"
                ),
                'default' => $sa ? implode(',', $ta ?? []) : 'FRVERS001',
            ]
        );
        $parser->addOption(
            'serviceproducteur',
            [
                'short' => 'c',
                'help' => __d(
                    'envoietransferts_shell',
                    "identifiant(s) service(s) producteur(s)"
                ),
                'default' => $sa ? implode(',', $oa ?? []) : 'FRPROD001',
            ]
        );
        $parser->addOption(
            'accordversement',
            [
                'short' => 'd',
                'help' => __d(
                    'envoietransferts_shell',
                    "identifiant de l'accord versement"
                ),
                'default' => $sa ? implode(',', $agreements ?? [])
                    : 'ACCORD001',
            ]
        );
        $parser->addOption(
            'profilarchive',
            [
                'short' => 'e',
                'help' => __d(
                    'envoietransferts_shell',
                    "identifiant du profil d'archive"
                ),
                'default' => $sa ? implode(',', $profiles ?? []) : 'PROFIL001',
            ]
        );
        $parser->addOption(
            'sortfinal',
            [
                'short' => 'f',
                'help' => __d(
                    'envoietransferts_shell',
                    "sort final de l'archive"
                ),
                'default' => implode(',', self::ALLOWED_APPRAISAL_CODES),
            ]
        );
        $parser->addOption(
            'nbpjs',
            [
                'short' => 'g',
                'help' => __d(
                    'envoietransferts_shell',
                    "nombre des pièces jointes"
                ),
                'default' => '3',
            ]
        );
        $parser->addOption(
            'nbpjsuniques',
            [
                'short' => 'q',
                'help' => __d(
                    'envoietransferts_shell',
                    "nombre des pièces jointes uniques "
                    . "(au-delà de ce nombre les pièces jointes seront des copies des précédentes)"
                ),
                'default' => '100',
            ]
        );
        $parser->addOption(
            'taillepjmin',
            [
                'short' => 'i',
                'help' => __d(
                    'envoietransferts_shell',
                    "taille en mo minimum des pièces jointes"
                ),
                'default' => '1',
            ]
        );
        $parser->addOption(
            'taillepjmax',
            [
                'short' => 'j',
                'help' => __d(
                    'envoietransferts_shell',
                    "taille en mo maximum des pièces jointes"
                ),
                'default' => '5',
            ]
        );
        $parser->addOption(
            'taillepjunite',
            [
                'short' => 'k',
                'help' => __d(
                    'envoietransferts_shell',
                    "unité de la taille des pièces jointes"
                ),
                'default' => 'Ko',
                'choices' => ['Mo', 'Ko', 'O'],
            ]
        );
        $parser->addOption(
            'extpj',
            [
                'short' => 'm',
                'help' => __d(
                    'envoietransferts_shell',
                    "extension des pièces jointes"
                ),
                'default' => implode(',', self::ALLOWED_ATTACHMENT_EXTENSIONS),
            ]
        );
        $parser->addOption(
            'versionseda',
            [
                'short' => 'v',
                'help' => __d('envoietransferts_shell', "version du SEDA"),
                'default' => implode(',', self::ALLOWED_SEDA_VERSIONS),
            ]
        );
        $parser->setDescription(
            [
                __d(
                    'envoietransferts_shell',
                    "Script de génération et d'envoi via RESTFul de transferts aléatoires"
                ),
                "--------------------------------------------------------------------",
                "",
                __d(
                    'envoietransferts_shell',
                    "les transferts sont générés de façon aléatoire et sont envoyés via HTTP à une instance asalae."
                ),
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws GuzzleException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $this->promptOptions();
        $this->checkOptions();
        /** @var Client $client */
        $client = Utility::get(
            Client::class,
            [
                'base_uri' => $this->args->getOption('url'),
                'auth' => [$this->args->getOption('wslogin'), $this->args->getOption('wspasswd')],
                'verify' => false, // certificats autosigné
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]
        );

        $this->io->out(
            __d(
                'envoietransferts_shell',
                "Génération et envoi de transferts aléatoires"
            ),
            2
        );
        $this->io->out(
            __d('envoietransferts_shell', "Lecture des informations de asalae")
        );
        try {
            $response = $client->request(
                'GET',
                self::TARGET_VERSION_URL
            );
        } catch (ClientException $e) {
            $this->handleClientException($e, self::TARGET_VERSION_URL);
            throw new Exception(); // never reach it
        }
        $body = $response->getBody();
        $raw = $body->read($body->getSize());
        $data = json_decode($raw);
        $this->io->out(
            __d(
                'envoietransferts_shell',
                "  version : {0}\n denomination : {1}",
                $data->version ?? '',
                '?'
            ),
            2
        );

        Filesystem::begin();
        DataCompressor::useTransaction();
        $nbtransferts = (int)$this->args->getOption('nbtransferts');
        for ($i = 1; $i <= $nbtransferts; $i++) {
            $this->io->out(__d('envoietransferts_shell', "Transfert #{0}", $i));
            $response = $this->postTransfers($client);
            $body = $response->getBody();
            $raw = $body->read($body->getSize());
            $data = json_decode($raw);
            if (isset($data->date_du_depot) && $data->date_du_depot) {
                $result = __("oui");
            } else {
                $result = __("non");
                if (isset($data->errors)) {
                    $result .= ' ' . implode(', ', $data->errors);
                }
            }
            $this->io->out(
                '  ' . __d(
                    'envoietransferts_shell',
                    "réussite du transfert : {0}",
                    $result
                )
            );
        }
        Filesystem::rollback();
    }

    /**
     * Assure les validations manquante des options
     */
    private function checkOptions()
    {
        $numeric = ['nbtransferts', 'nbpjs', 'taillepjmin', 'taillepjmax'];
        foreach ($numeric as $key) {
            $value = $this->args->getOption($key);
            if (!is_numeric($value)) {
                throw new InvalidArgumentException(
                    __d(
                        'envoietransferts_shell',
                        "L'option {0} doit être numérique",
                        $key
                    )
                );
            }
            if ($value <= 0) {
                throw new InvalidArgumentException(
                    __d(
                        'envoietransferts_shell',
                        "L'option {0} doit être supérieur à 0",
                        $key
                    )
                );
            }
        }
        if ($this->args->getOption('taillepjmin') > $this->args->getOption('taillepjmax')) {
            throw new InvalidArgumentException(
                __d(
                    'envoietransferts_shell',
                    "{0} ne peut être supérieur à {1}",
                    'taillepjmin',
                    'taillepjmax'
                )
            );
        }
        if (empty($this->args->getOption('wspasswd'))) {
            $this->params['wspasswd'] = $this->enterPassword();
        }
        if (!empty($this->args->getOption('extpj'))) {
            $extpjs = array_map('trim', explode(',', $this->args->getOption('extpj')));
            foreach ($extpjs as $element) {
                if (!in_array($element, self::ALLOWED_ATTACHMENT_EXTENSIONS)) {
                    throw new InvalidArgumentException(
                        __d(
                            'envoietransferts_shell',
                            "{0} n'est pas une extension de fichier acceptée",
                            $element
                        )
                    );
                }
            }
        }
        if (!empty($this->args->getOption('sortfinal'))) {
            $elements = array_map(
                'trim',
                explode(',', $this->args->getOption('sortfinal'))
            );
            foreach ($elements as $element) {
                if (!in_array($element, self::ALLOWED_APPRAISAL_CODES)) {
                    throw new InvalidArgumentException(
                        __d(
                            'envoietransferts_shell',
                            "{0} n'est pas un code de sort final accepté",
                            $element
                        )
                    );
                }
            }
        }
    }

    /**
     * Gestion des erreurs 400+
     * @param ClientException $e
     * @param string          $url
     */
    protected function handleClientException(ClientException $e, string $url)
    {
        $msg = __("Unknown");
        $code = CommandInterface::CODE_ERROR;
        if (isset(SymfonfyResponse::$statusTexts[$e->getCode()])) {
            $msg = $e->getCode() . ' ' . SymfonfyResponse::$statusTexts[$e->getCode()];
        }
        // limite du code a 255 -> 401 transformé en 41
        if (preg_match('/^(\d)0(\d)$/', (string)$e->getCode(), $m)) {
            $code = $m[1] . $m[2];
        }
        $this->io->abort($msg . ' ' . $url . PHP_EOL . $e->getMessage(), $code);
    }

    /**
     * Génère et envoi un transfert
     * @param Client $client
     * @return ResponseInterface
     * @throws Exception
     * @throws GuzzleException
     */
    private function postTransfers(Client $client)
    {
        $this->io->out(
            '  ' . __d(
                'envoietransferts_shell',
                "création des fichiers du transfert"
            )
        );
        $versionseda = self::$Faker->randomElement(
            array_map('trim', explode(',', $this->args->getOption('versionseda')))
        );
        $identifier = str_replace('-', '', self::$Faker->uuid);
        $atIdentifier = 'atr_alea_' . $identifier;
        $pa = self::$Faker->randomElement(
            array_map(
                'trim',
                explode(',', (string)$this->args->getOption('profilarchive'))
            )
        );
        $sortFinal = self::$Faker->randomElement(
            array_map('trim', explode(',', $this->args->getOption('sortfinal')))
        );
        $serviceversant = self::$Faker->randomElement(
            array_map('trim', explode(',', $this->args->getOption('serviceversant')))
        );
        $serviceproducteur = self::$Faker->randomElement(
            array_map('trim', explode(',', $this->args->getOption('serviceproducteur')))
        );
        $accordversement = self::$Faker->randomElement(
            array_map('trim', explode(',', $this->args->getOption('accordversement')))
        );
        $nbpjs = (int)$this->args->getOption('nbpjs');
        $documentsBlocs = ['', '', ''];
        $integriryBlocs = '';
        $this->files = [];
        $xmlContent = '';
        for ($j = 1; $j <= $nbpjs; $j++) {
            $filename = $this->generateFile();
            $basename = basename($filename);
            $this->files[] = $filename;
            switch ($versionseda) {
                case '0.2':
                    $documentsBlocs[$j % 3] .= $this->renderXml(
                        self::TMPL_DOC_BLOCK_SEDA02,
                        ['filename' => $basename]
                    );
                    $integriryBlocs .= $this->renderXml(
                        self::TMPL_INTEGRITY_BLOCK_SEDA02,
                        [
                            'filename' => $basename,
                            'sha256' => hash_file('sha256', $filename),
                        ]
                    );
                    $xmlContent = $this->renderXml(
                        self::TMPL_SEDA02,
                        [
                            'archiveTransferComment' => 'Transfert aléatoire ' . $atIdentifier,
                            'date' => date('c'),
                            'archiveTransferIdentifier' => $atIdentifier,
                            'serviceversant' => $serviceversant,
                            'servicearchives' => $this->args->getOption(
                                'servicearchives'
                            ),
                            'integrity' => $integriryBlocs,
                            'accordversement' => $accordversement,
                            'profilarchive' => $pa
                                ? "<ArchivalProfile>$pa</ArchivalProfile>" : '',
                            'archiveName' => 'Archive aléatoire ' . $identifier,
                            'serviceproducteur' => $serviceproducteur,
                            'sortFinal' => $sortFinal,
                            'duaAnnees' => self::$Faker->numberBetween(0, 10),
                            'duaStartDate' => self::$Faker->dateTimeThisDecade()
                                ->format('Y-m-d'),
                            'documentBloc1' => $documentsBlocs[0],
                            'documentBloc2' => $documentsBlocs[1],
                            'documentBloc3' => $documentsBlocs[2],
                        ]
                    );
                    break;
                case '1.0':
                    $documentsBlocs[$j % 3] .= $this->renderXml(
                        self::TMPL_DOC_BLOCK_SEDA10,
                        [
                            'filename' => $basename,
                            'sha256' => hash_file('sha256', $filename),
                            'filesize' => filesize($filename),
                        ]
                    );
                    $xmlContent = $this->renderXml(
                        self::TMPL_SEDA10,
                        [
                            'archiveTransferComment' => 'Transfert aléatoire ' . $atIdentifier,
                            'date' => date('c'),
                            'archiveTransferIdentifier' => $atIdentifier,
                            'serviceversant' => $serviceversant,
                            'servicearchives' => $this->args->getOption(
                                'servicearchives'
                            ),
                            'accordversement' => $accordversement,
                            'profilarchive' => $pa
                                ? "<ArchivalProfile>$pa</ArchivalProfile>" : '',
                            'archiveName' => 'Archive aléatoire ' . $identifier,
                            'serviceproducteur' => $serviceproducteur,
                            'sortFinal' => $sortFinal,
                            'duaAnnees' => self::$Faker->numberBetween(0, 10),
                            'duaStartDate' => self::$Faker->dateTimeThisDecade()
                                ->format('Y-m-d'),
                            'documentBloc1' => $documentsBlocs[0],
                            'documentBloc2' => $documentsBlocs[1],
                            'documentBloc3' => $documentsBlocs[2],
                        ]
                    );
                    break;
            }
        }
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xmlContent);
        $zipFilename = sys_get_temp_dir() . DS . $atIdentifier . '.zip';
        DataCompressor::compress($this->files, $zipFilename);
        $this->io->out(
            '  '
            . __d(
                'envoietransferts_shell',
                "taille du fichier zip {0}",
                Number::toReadableSize(filesize($zipFilename))
            )
        );
        $this->io->out('  ' . __d('envoietransferts_shell', "envoi du transfert"));
        try {
            $fhandle = fopen($zipFilename, 'r');
            $response = $client->request(
                'POST',
                self::TARGET_URL,
                [
                    'headers' => [
                        'Accept' => 'application/json',
                        'X-Requested-With' => 'asalae2',
                    ],
                    'multipart' => [
                        [
                            'name' => 'seda_message',
                            'filename' => $atIdentifier . '.xml',
                            'contents' => $dom->saveXML(),
                        ],
                        [
                            'name' => 'attachments',
                            'contents' => $fhandle,
                        ],
                    ],
                ]
            );
            if (is_resource($fhandle)) {
                fclose($fhandle);
            }
        } catch (ClientException $e) {
            $this->handleClientException($e, self::TARGET_URL);
            throw new Exception();
        }
        return $response;
    }

    /**
     * Génère un fichier
     * @return string path/to/file
     * @throws Exception
     */
    private function generateFile(): string
    {
        if (count($this->files) > (int)$this->args->getOption('nbpjsuniques')) {
            $sourceFilename = $this->files[rand(0, count($this->files) - 1)]; // NOSONAR
            $ext = pathinfo($sourceFilename, PATHINFO_EXTENSION);
            $targetFilename = sys_get_temp_dir() . DS . 'fichier_' . self::$Faker->uuid . '.' . $ext;
            Filesystem::copy($sourceFilename, $targetFilename);
        } else {
            $units = self::$Faker->numberBetween(
                $this->args->getOption('taillepjmin'),
                $this->args->getOption('taillepjmax')
            );
            $targetFilesize = $units
                * ($this->args->getOption('taillepjunite') === 'Ko' ? 1024 : 1)
                * ($this->args->getOption('taillepjunite') === 'Mo' ? 1024 * 1024 : 1);
            $nbSentences = $this->args->getOption('taillepjunite') === 'O' ? 1 : 3;
            $content = '';
            while (strlen($content) < $targetFilesize) {
                $content .= self::$Faker->paragraph(
                    $nbSentences
                ) . PHP_EOL . PHP_EOL;
            }

            $targetFilename = sys_get_temp_dir() . DS . 'fichier_' . self::$Faker->uuid;
            Filesystem::dumpFile($targetFilename, $content);

            $extpjs = array_map('trim', explode(',', $this->args->getOption('extpj') ?: ''));
            $ext = self::$Faker->randomElement($extpjs);
            if ($ext === 'txt') {
                Filesystem::rename($targetFilename, $targetFilename . '.txt');
                $targetFilename = $targetFilename . '.txt';
            } else {
                FileConverters::convert(
                    $targetFilename,
                    $ext,
                    $targetFilename . '.' . $ext
                );
                Filesystem::remove($targetFilename);
                $targetFilename = $targetFilename . '.' . $ext;
                Filesystem::addToDeleteIfRollback($targetFilename);
            }
        }

        return $targetFilename;
    }

    /**
     * Moteur de rendu pour le xml
     * @param string $template
     * @param array  $params
     * @return array|string|string[]
     */
    private function renderXml(string $template, array $params)
    {
        foreach ($params as $key => $value) {
            $template = str_replace('{{' . $key . '}}', $value, $template);
        }
        return $template;
    }
}
