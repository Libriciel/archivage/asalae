<?php

/**
 * Asalae\Command\Indicators\UpdateCommitCommand
 */

namespace Asalae\Command\Indicators;

use AsalaeCore\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Datasource\ConnectionManager;
use Exception;

/**
 * Effectue la mise à jours des indicateurs
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UpdateCommitCommand extends AbstractIndicatorsUpdateCommand
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'indicators_update commit';
    }

    /**
     * Met à jour les indicateurs en base de données
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     */
    public function execute(Arguments $args, ConsoleIo $io): int
    {
        $this->args = $args;
        $this->io = $io;

        // vérification des fichiers de la session
        $this->checkSessionExistOrFail();

        // chargement de la session en mémoire
        $this->loadSessionData();

        // vérification du calcul préalable des indicateurs
        if (!$this->sessionData[self::SESSION_TRACKING]['ready_to_commit']) {
            $this->io->warning(__("les indicateurs ne sont pas calculés ou caclulés partiellement"));
            $this->io->info(__("utiliser la commande calculate pour lancer le calcul des indicateurs"));
            $this->io->abort(__("traitement annulé"));
        }

        $conn = ConnectionManager::get('default');

        try {
            $this->io->out(__("Mise à jour des indicateurs en base de données"), 2);

            // ouverture de la session base de données
            $conn->begin();

            // suppression des indicateurs pré session
            $this->fetchTable('ArchiveIndicators')->deleteAll(
                [
                    'id <=' => $this->sessionData[self::SESSION_TRACKING]['archive_indicators_last_id'],
                ]
            );
            $this->fetchTable('MessageIndicators')->deleteAll(
                [
                    'id <= ' => $this->sessionData[self::SESSION_TRACKING]['message_indicators_last_id'],
                ]
            );

            // écriture des indicateurs en base de données
            $this->io->out(__("Ecriture des indicateurs des archives"));
            foreach ($this->sessionData[self::ARCHIVES_INDICATORS] as $archiveIndicatorData) {
                $archiveIndicator = $this->fetchTable('ArchiveIndicators')->newEntity($archiveIndicatorData);
                $this->fetchTable('ArchiveIndicators')->save($archiveIndicator);
            }

            $this->io->out(__("Ecriture des indicateurs des messages"));
            foreach ($this->sessionData[self::MESSAGES_INDICATORS] as $messageIndicator) {
                $messageIndicator = $this->fetchTable('MessageIndicators')->newEntity($messageIndicator);
                $this->fetchTable('MessageIndicators')->save($messageIndicator);
            }

            // validation de la session base de données
            $conn->commit();

            // suppression des fichiers de session
            $this->removeSessionFiles();

            $this->io->out(__("Indicateurs mis à jour en base de données, session supprimée"), 2);
        } catch (Exception $e) {
            if ($conn->inTransaction()) {
                $conn->rollback();
            }
            $this->io->error(__("Erreur : {0}", $e->getMessage()));
            $this->io->abort(__("Traitement annulé"));
        }

        return Command::CODE_SUCCESS;
    }
}
