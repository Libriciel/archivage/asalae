<?php

/**
 * Asalae\Command\Indicators\UpdateRollbackCommand
 */

namespace Asalae\Command\Indicators;

use AsalaeCore\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;

/**
 * Effectue la mise à jours des indicateurs
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UpdateRollbackCommand extends AbstractIndicatorsUpdateCommand
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'indicators_update rollback';
    }

    /**
     * Suppression des fchiers de session
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     */
    public function execute(Arguments $args, ConsoleIo $io): int
    {
        $this->args = $args;
        $this->io = $io;

        $this->removeSessionFiles();
        $this->io->out(__("Session annulée : suppression des fichiers de session"), 2);

        return Command::CODE_SUCCESS;
    }
}
