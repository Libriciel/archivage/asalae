<?php

/**
 * Asalae\Command\Indicators\UpdateBeginCommand
 */

namespace Asalae\Command\Indicators;

use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\DeliveryRequestsTable;
use Asalae\Model\Table\DestructionRequestsTable;
use Asalae\Model\Table\OutgoingTransfersTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use AsalaeCore\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;

/**
 * Effectue la mise à jours des indicateurs
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UpdateBeginCommand extends AbstractIndicatorsUpdateCommand
{
    /**
     * constantes de la classe
     */
    public const array ARCHIVES_CREATION_STATES = [
        ArchivesTable::S_CREATING,
        ArchivesTable::S_DESCRIBING,
        ArchivesTable::S_STORING,
        ArchivesTable::S_MANAGING,
    ];

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'indicators_update begin';
    }

    /**
     * Intitialise les fichiers de la sessions
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     */
    public function execute(Arguments $args, ConsoleIo $io): int
    {
        $this->args = $args;
        $this->io = $io;

        // vérification de la présence des archives
        $this->checkArchivesExistOrFail();

        // vérification d'une session déjà existante
        $this->checkSessionNotExistOrFail();

        // création du répertoire de la session
        if (!is_dir($this->sessionDirname)) {
            mkdir($this->sessionDirname, 0770, true);
        }

        $this->io->out(__("Création des fichiers de session"), 2);

        // requêtes
        $archivesQuery = $this->fetchTable('Archives')->find()
            ->where(['state not in' => self::ARCHIVES_CREATION_STATES])
            ->orderBy('id')
            ->disableHydration();
        $rejectedInTransfersQuery = $this->fetchTable('Transfers')->find()
            ->where(['is_accepted' => false])
            ->orderBy('id')
            ->disableHydration();
        $rejectedDeliveryRequestsQuery = $this->fetchTable('DeliveryRequests')->find()
            ->where(['state' => DeliveryRequestsTable::S_REJECTED])
            ->orderBy('id')
            ->disableHydration();
        $rejectedDestructionRequestsQuery = $this->fetchTable('DestructionRequests')->find()
            ->where(['state' => DestructionRequestsTable::S_REJECTED])
            ->orderBy('id')
            ->disableHydration();
        $rejectedRestitutionRequestsQuery = $this->fetchTable('RestitutionRequests')->find()
            ->where(['state' => RestitutionRequestsTable::S_REJECTED])
            ->orderBy('id')
            ->disableHydration();
        $rejectedOutTransfersQuery = $this->fetchTable('OutgoingTransfers')->find()
            ->where(['state' => OutgoingTransfersTable::S_REJECTED])
            ->orderBy('id')
            ->disableHydration();

        // données de suivi global
        $archivesCount = $archivesQuery->count();
        $rejectedInTransfersCount = $rejectedInTransfersQuery->count();
        $rejectedDeliveryRequestsCount = $rejectedDeliveryRequestsQuery->count();
        $rejectedDestructionRequestsCount = $rejectedDestructionRequestsQuery->count();
        $rejectedRestitutionRequestsCount = $rejectedRestitutionRequestsQuery->count();
        $rejectedOutTransfersCount = $rejectedOutTransfersQuery->count();
        $archive_indicators_last_id = $this->fetchTable('ArchiveIndicators')
            ->find()
            ->orderByDesc('id')
            ->first()
            ?->get('id');
        $message_indicators_last_id = $this->fetchTable('MessageIndicators')
            ->find()
            ->orderByDesc('id')
            ->first()
            ?->get('id');
        $this->sessionData[self::SESSION_TRACKING] = [
            'creation_date' => date('Y-m-d H:i:s'),
            'updating_date' => '',
            'calculating_start_date' => '',
            'calculating_end_date' => '',
            'calculating_duration' => 0,
            'ready_to_commit' => false,
            'archive_indicators_last_id' => $archive_indicators_last_id,
            'message_indicators_last_id' => $message_indicators_last_id,
            'archives_count' => $archivesCount,
            'archives_processed' => 0,
            'rejected_in_transfers_count' => $rejectedInTransfersCount,
            'rejected_in_transfers_processed' => 0,
            'rejected_delivery_requests_count' => $rejectedDeliveryRequestsCount,
            'rejected_delivery_requests_processed' => 0,
            'rejected_destruction_requests_count' => $rejectedDestructionRequestsCount,
            'rejected_destruction_requests_processed' => 0,
            'rejected_restitution_requests_count' => $rejectedRestitutionRequestsCount,
            'rejected_restitution_requests_processed' => 0,
            'rejected_out_transfers_count' => $rejectedOutTransfersCount,
            'rejected_out_transfers_processed' => 0,
        ];

        // données de suivi des archives
        foreach ($archivesQuery as $i => $archive) {
            $this->sessionData[self::ARCHIVES_TRACKING][] = [
                'index' => ($i + 1) . '/' . $archivesCount,
                'processed' => false,
                'id' => $archive['id'],
                'state' => $archive['state'],
            ];
        }

        // données de suivi des transferts entrants rejetés
        foreach ($rejectedInTransfersQuery as $i => $rejectedInTransfer) {
            $this->sessionData[self::REJECTED_IN_TRANSFER_TRACKING][] = [
                'index' => ($i + 1) . '/' . $rejectedInTransfersCount,
                'processed' => false,
                'id' => $rejectedInTransfer['id'],
            ];
        }

        // données de suivi des demandes de communication rejetées
        foreach ($rejectedDeliveryRequestsQuery as $i => $rejectedDeliveryRequest) {
            $this->sessionData[self::REJECTED_DELIVERY_REQUEST_TRACKING][] = [
                'index' => ($i + 1) . '/' . $rejectedDeliveryRequestsCount,
                'processed' => false,
                'id' => $rejectedDeliveryRequest['id'],
            ];
        }

        // données de suivi des demandes d'élimination rejetées
        foreach ($rejectedDestructionRequestsQuery as $i => $rejectedDestructionRequest) {
            $this->sessionData[self::REJECTED_DESTRUCTION_REQUEST_TRACKING][] = [
                'index' => ($i + 1) . '/' . $rejectedDestructionRequestsCount,
                'processed' => false,
                'id' => $rejectedDestructionRequest['id'],
            ];
        }

        // données de suivi des demandes de restitution rejetées
        foreach ($rejectedRestitutionRequestsQuery as $i => $rejectedRestitutionRequest) {
            $this->sessionData[self::REJECTED_RESTITUTION_REQUEST_TRACKING][] = [
                'index' => ($i + 1) . '/' . $rejectedRestitutionRequestsCount,
                'processed' => false,
                'id' => $rejectedRestitutionRequest['id'],
            ];
        }

        // données de suivi des transferts sortants rejetés
        foreach ($rejectedOutTransfersQuery as $i => $rejectedOutTransfer) {
            $this->sessionData[self::REJECTED_OUT_TRANSFER_TRACKING][] = [
                'index' => ($i + 1) . '/' . $rejectedOutTransfersCount,
                'processed' => false,
                'id' => $rejectedOutTransfer['id'],
            ];
        }

        // sauvegarde des fichiers de session
        $this->saveSessionData();

        // affichage de la session
        $this->status();

        return Command::CODE_SUCCESS;
    }

    /**
     * traitement interrompu si aucune archives
     * @return void
     */
    private function checkArchivesExistOrFail(): void
    {
        $archivesCount = $this->fetchTable('Archives')
            ->find()
            ->where(['state not in' => self::ARCHIVES_CREATION_STATES])
            ->count();
        if ($archivesCount === 0) {
            $this->io->warning(__("aucune archives en base de données"));
            $this->io->abort(__("traitement annulé"));
        }
    }
}
