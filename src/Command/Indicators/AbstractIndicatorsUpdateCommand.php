<?php

/**
 * Asalae\Command\Indicators\AbstractIndicatorsUpdateCommand
 */

namespace Asalae\Command\Indicators;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;

/**
 * Effectue la mise à jours des indicateurs
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class AbstractIndicatorsUpdateCommand extends Command
{
    /**
     * constantes de la classe
     */
    public const string SESSION_TRACKING = 'session_tracking';
    public const string ARCHIVES_TRACKING = 'archives_tracking';
    public const string REJECTED_IN_TRANSFER_TRACKING = 'rejected_in_transfer_tracking';
    public const string REJECTED_DELIVERY_REQUEST_TRACKING = 'rejected_delivery_request_tracking';
    public const string REJECTED_DESTRUCTION_REQUEST_TRACKING = 'rejected_destruction_request_tracking';
    public const string REJECTED_RESTITUTION_REQUEST_TRACKING = 'rejected_restitution_request_tracking';
    public const string REJECTED_OUT_TRANSFER_TRACKING = 'rejected_out_transfer_tracking';
    public const string ARCHIVES_INDICATORS = 'archives_indicators';
    public const string MESSAGES_INDICATORS = 'messages_indicators';
    /**
     * @var ConsoleIo
     */
    public $io;
    /**
     * @var Arguments
     */
    public $args;
    /**
     * répertoire des fichiers de la session
     * @var string
     */
    public $sessionDirname;

    /**
     * liste des fichiers de la session
     * @var string[]
     */
    public $sessionFiles = [
        self::SESSION_TRACKING,
        self::ARCHIVES_TRACKING,
        self::ARCHIVES_INDICATORS,
        self::MESSAGES_INDICATORS,
        self::REJECTED_IN_TRANSFER_TRACKING,
        self::REJECTED_DELIVERY_REQUEST_TRACKING,
        self::REJECTED_DESTRUCTION_REQUEST_TRACKING,
        self::REJECTED_RESTITUTION_REQUEST_TRACKING,
        self::REJECTED_OUT_TRANSFER_TRACKING,
    ];

    /**
     * stockage des données de session
     * @var array
     */
    public $sessionData = [
        self::SESSION_TRACKING => [],
        self::ARCHIVES_TRACKING => [],
        self::REJECTED_IN_TRANSFER_TRACKING => [],
        self::REJECTED_DELIVERY_REQUEST_TRACKING => [],
        self::REJECTED_DESTRUCTION_REQUEST_TRACKING => [],
        self::REJECTED_RESTITUTION_REQUEST_TRACKING => [],
        self::REJECTED_OUT_TRANSFER_TRACKING => [],
        self::ARCHIVES_INDICATORS => [],
        self::MESSAGES_INDICATORS => [],
    ];

    /**
     * Constructeur de classe
     */
    public function __construct()
    {
        // fichiers de la session
        $this->sessionDirname
            = rtrim(Configure::read('App.paths.data'), DS)
            . DS . 'tmp' . DS . 'indicators_update_session' . DS;
    }

    /**
     * Gets the option parser instance and configures it.
     * @return ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();

        $parser->setDescription(
            __("Effectue la mise à jour des indicateurs des archives") . PHP_EOL
            . __("La mise à jour se déroule comme suit : ") . PHP_EOL
            . __("- begin : ouverture de la session") . PHP_EOL
            . __("- calculate : calcul des indicateurs en local") . PHP_EOL
            . __("- commit : mise à jour des indicateurs en base de données") . PHP_EOL
            . __("La commande status permet d'afficher le suivi de la session") . PHP_EOL
            . __("La commande show permet d'afficher les indicateurs calculées") . PHP_EOL
        );

        return $parser;
    }

    /**
     * traitement interrompu si une session existe déjà
     * @return void
     */
    protected function checkSessionNotExistOrFail(): void
    {
        if (is_dir($this->sessionDirname)) {
            $this->io->warning(__("une session est déjà en cours {0}", $this->sessionDirname));
            $this->io->info(__("utiliser la commande rollback pour annuler la session"));
            $this->io->abort(__("traitement annulé"));
        }
    }

    /**
     * sauvegarde les données de session
     * @return void
     */
    protected function saveSessionData(): void
    {
        // Calcul de ready_to_commit
        $ref = &$this->sessionData[self::SESSION_TRACKING];
        $ref['ready_to_commit']
            = $ref['archives_count'] === $ref['archives_processed']
            && $ref['rejected_in_transfers_count'] === $ref['rejected_in_transfers_processed']
            && $ref['rejected_delivery_requests_count'] === $ref['rejected_delivery_requests_processed']
            && $ref['rejected_destruction_requests_count'] === $ref['rejected_destruction_requests_processed']
            && $ref['rejected_restitution_requests_count'] === $ref['rejected_restitution_requests_processed']
            && $ref['rejected_out_transfers_count'] === $ref['rejected_out_transfers_processed'];

        // date de fin et durée du calcul
        if (!$ref['calculating_end_date'] && $ref['ready_to_commit']) {
            $ref['calculating_end_date'] = date('c');
            $ref['calculating_duration']
                = strtotime($ref['calculating_end_date'])
                - strtotime($ref['calculating_start_date']);
        }

        foreach ($this->sessionFiles as $sessionName) {
            $sessionFileUri = $this->getSessionFileUri($sessionName);
            file_put_contents(
                $sessionFileUri,
                json_encode($this->sessionData[$sessionName], JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES)
            );
        }
    }

    /**
     * retourne l'uri du fichier de session selon son nom
     * @param string $name
     * @return string
     */
    private function getSessionFileUri(string $name): string
    {
        return $this->sessionDirname . $name . '.json';
    }

    /**
     * suppression des fichiers de la session
     * @return void
     */
    protected function removeSessionFiles(): void
    {
        foreach ($this->sessionFiles as $sessionName) {
            $sessionFileUri = $this->getSessionFileUri($sessionName);
            if (is_file($sessionFileUri)) {
                unlink($sessionFileUri);
            }
        }
        if (is_dir($this->sessionDirname)) {
            rmdir($this->sessionDirname);
        }
    }

    /**
     * affiche les informations de la session en cours
     * @return void
     */
    protected function status(): void
    {
        // vérification des fichiers de la session
        $this->checkSessionExistOrFail();

        // chargement de la session en mémoire
        $this->loadSessionData();

        // affichage des données de la session
        $sessionData = $this->sessionData[self::SESSION_TRACKING];
        $this->io->out(__("Affichage des informations de la session {0}", $this->sessionDirname));
        $this->io->out(__("- date de création de la session  : {0}", $sessionData['creation_date']));
        $this->io->out(__("- date de la dernière mise à jour : {0}", $sessionData['updating_date']));
        $this->io->out(__("- date de début du calcul : {0}", $sessionData['calculating_start_date']));
        $this->io->out(__("- date de fin du calcul : {0}", $sessionData['calculating_end_date']));
        $this->io->out(__("- durée en secondes du calcul : {0}", $sessionData['calculating_duration']));
        $this->io->out(__("- peut être commitée : {0}", $sessionData['ready_to_commit'] ? 'oui' : 'non'));
        $this->io->out(__("- nombre total d'archives    : {0}", $sessionData['archives_count']));
        $this->io->out(__("- nombre d'archives traitées : {0}", $sessionData['archives_processed']));
        $this->io->out(
            __(
                "- nombre total de transferts entrants rejetés   : {0}",
                $sessionData['rejected_in_transfers_count']
            )
        );
        $this->io->out(
            __(
                "- nombre de transferts entrants rejetés traités : {0}",
                $sessionData['rejected_in_transfers_processed']
            )
        );
        $this->io->out(
            __(
                "- nombre total de demandes de communication rejetées    : {0}",
                $sessionData['rejected_delivery_requests_count']
            )
        );
        $this->io->out(
            __(
                "- nombre de demandes de communication rejetées traitées : {0}",
                $sessionData['rejected_delivery_requests_processed']
            )
        );
        $this->io->out(
            __(
                "- nombre total de demandes d'élimination rejetées    : {0}",
                $sessionData['rejected_destruction_requests_count']
            )
        );
        $this->io->out(
            __(
                "- nombre de demandes d'élimination rejetées traitées : {0}",
                $sessionData['rejected_destruction_requests_processed']
            )
        );
        $this->io->out(
            __(
                "- nombre total de demandes de restitution rejetées    : {0}",
                $sessionData['rejected_restitution_requests_count']
            )
        );
        $this->io->out(
            __(
                "- nombre de demandes de restitution rejetées traitées : {0}",
                $sessionData['rejected_restitution_requests_processed']
            )
        );
        $this->io->out(
            __(
                "- nombre total de transferts sortants rejetés   : {0}",
                $sessionData['rejected_out_transfers_count']
            )
        );
        $this->io->out(
            __(
                "- nombre de transferts sortants rejetés traités : {0}",
                $sessionData['rejected_out_transfers_processed']
            ),
            2
        );
    }

    /**
     * traitement interrompu si une session n'existe pas
     * @return void
     */
    protected function checkSessionExistOrFail(): void
    {
        if (!is_dir($this->sessionDirname)) {
            $this->io->warning(__("aucune session en cours {0}", $this->sessionDirname));
            $this->io->info(__("utiliser la commande begin pour initialiser la session"));
            $this->io->abort(__("traitement annulé"));
        }
        foreach ($this->sessionFiles as $sessionFile) {
            if (!is_file($this->getSessionFileUri($sessionFile))) {
                $this->io->warning(__("fichier de session {0} non trouvé", $sessionFile));
                $this->io->info(__("utiliser les commandes rollback puis begin pour initialiser la session"));
                $this->io->abort(__("traitement annulé"));
            }
        }
    }

    /**
     * charge les données de session
     * @return void
     */
    protected function loadSessionData(): void
    {
        foreach ($this->sessionFiles as $sessionName) {
            $sessionFileUri = $this->getSessionFileUri($sessionName);
            $this->sessionData[$sessionName] = json_decode(file_get_contents($sessionFileUri), true);
        }
    }
}
