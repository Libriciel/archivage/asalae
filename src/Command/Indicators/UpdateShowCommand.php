<?php

/**
 * Asalae\Command\Indicators\UpdateShowCommand
 */

namespace Asalae\Command\Indicators;

use Asalae\Model\Table\MessageIndicatorsTable;
use AsalaeCore\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\I18n\Number;

/**
 * Effectue la mise à jours des indicateurs
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UpdateShowCommand extends AbstractIndicatorsUpdateCommand
{
    /**
     * constantes de la classe
     */
    public const string SHOW_BY_TRANSFERRING_AGENCY = 'transferring_agency';
    public const string SHOW_BY_ORIGINATING_AGENCY = 'originating_agency';
    public const string SHOW_SUBJECT_ARCHIVES = 'archives';
    public const string SHOW_SUBJECT_IN_TRANSFERS = 'in_transfers';
    public const string SHOW_SUBJECT_DELIVERIES = 'deliveries';
    public const string SHOW_SUBJECT_DESTRUCTIONS = 'destructions';
    public const string SHOW_SUBJECT_OUT_TRANSFERS = 'out_transfers';

    /**
     * définition du type et des colonnes d'affichage des indicateurs des messages
     * @var array
     */
    public $showMessagesDefinitions = [];

    /**
     * Constructeur de classe
     */
    public function __construct()
    {
        parent::__construct();
        // filtres et colonnes pour l'affichage des indicateurs des messages
        $this->showMessagesDefinitions = [
            self::SHOW_SUBJECT_IN_TRANSFERS => [
                'message_type' => MessageIndicatorsTable::IN_TRANSFER,
                'name' => __("transferts"),
                'columns' => [
                    'original_files_size' => __("Volume total"),
                    'original_files_count' => __("Nb PJs"),
                    'message_count' => __("Nb Tr."),
                    'validation_duration' => __("Délai max"),
                    'rejected_message_count' => __("Nb Tr. rejetés"),
                ],
            ],
            self::SHOW_SUBJECT_DELIVERIES => [
                'message_type' => MessageIndicatorsTable::DELIVERY,
                'name' => __("communications"),
                'columns' => [
                    'original_files_size' => __("Volume total"),
                    'original_files_count' => __("Nb de documents"),
                    'message_count' => __("Nb de communications"),
                    'rejected_message_count' => __("Nb de demandes rejetées"),
                    'derogation_message_count' => __("Nb de dérogations instruites"),
                    'rejected_derogation_message_count' => __("Nb de dérogations rejetées"),
                ],
            ],
            self::SHOW_SUBJECT_DESTRUCTIONS => [
                'message_type' => MessageIndicatorsTable::DESTRUCTION,
                'name' => __("éliminations"),
                'columns' => [
                    'original_files_size' => __("Volume total"),
                    'original_files_count' => __("Nb de documents"),
                    'message_count' => __("Nb d'éliminations"),
                    'rejected_message_count' => __("Nb de demandes rejetées"),
                ],
            ],
            self::SHOW_SUBJECT_OUT_TRANSFERS => [
                'message_type' => MessageIndicatorsTable::OUT_TRANSFER,
                'name' => __("transferts sortants"),
                'columns' => [
                    'original_files_size' => __("Volume total"),
                    'original_files_count' => __("Nb de pièces jointes"),
                    'message_count' => __("Nb de transfert"),
                    'validation_duration' => __("Délai max de traitement"),
                    'rejected_message_count' => __("Nb de transferts rejetés"),
                ],
            ],
        ];
    }

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'indicators_update show';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();

        /**
         * affichage de la taille des fichiers de façon lisible pour un humain
         */
        $parser->addOption(
            'show-readable-size',
            [
                'help' => __(
                    "Affichage lisible des tailles des fichiers"
                ),
                'boolean' => true,
            ]
        );

        /**
         * affichage par service versant ou producteur
         */
        $parser->addOption(
            'show-by',
            [
                'help' => __(
                    "Affichage par service versant* ou producteur"
                ),
                'default' => self::SHOW_BY_TRANSFERRING_AGENCY,
                'choices' => [self::SHOW_BY_TRANSFERRING_AGENCY, self::SHOW_BY_ORIGINATING_AGENCY],
            ]
        );

        /**
         * choix de l'indicateur affiché (archives, transferts entrants, ...)
         */
        $parser->addOption(
            'show-subject',
            [
                'help' => __(
                    "Indicateur affiché : archives*, transferts,..."
                ),
                'default' => self::SHOW_SUBJECT_ARCHIVES,
                'choices' => [
                    self::SHOW_SUBJECT_ARCHIVES,
                    self::SHOW_SUBJECT_IN_TRANSFERS,
                    self::SHOW_SUBJECT_DELIVERIES,
                    self::SHOW_SUBJECT_DESTRUCTIONS,
                    self::SHOW_SUBJECT_OUT_TRANSFERS,
                ],
            ]
        );

        return $parser;
    }

    /**
     * Affiche les indicateurs calculés
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     */
    public function execute(Arguments $args, ConsoleIo $io): int
    {
        $this->args = $args;
        $this->io = $io;

        // vérification des fichiers de la session
        $this->checkSessionExistOrFail();

        // chargement de la session en mémoire
        $this->loadSessionData();

        // vérification du calcul préalable des indicateurs
        if (!$this->sessionData[self::SESSION_TRACKING]['ready_to_commit']) {
            $this->io->warning(__("les indicateurs ne sont pas calculés ou caclulés partiellement"));
            $this->io->info(__("utiliser la commande calculate pour lancer le calcul des indicateurs"));
            $this->io->abort(__("traitement annulé"));
        }

        if ($this->args->getOption('show-subject') === self::SHOW_SUBJECT_ARCHIVES) {
            $this->showArchivesIndicators();
        } else {
            $this->showMessagesIndicators();
        }

        return Command::CODE_SUCCESS;
    }

    /**
     * affiche les indicateurs calculés des archives
     * @return void
     */
    private function showArchivesIndicators(): void
    {
        // versant ou producteur
        $byTransferringAgencies = $this->args->getOption('show-by') === self::SHOW_BY_TRANSFERRING_AGENCY;
        $byServiceId = $byTransferringAgencies ? 'transferring_agency_id' : 'originating_agency_id';

        // compilation des indicateurs des archives par service d'archives et service versant/producteur
        $data = [];
        foreach ($this->sessionData[self::ARCHIVES_INDICATORS] as $archiveIndicator) {
            if (!array_key_exists($archiveIndicator['archival_agency_id'], $data)) {
                $data[$archiveIndicator['archival_agency_id']] = [];
            }
            $dataSA = &$data[$archiveIndicator['archival_agency_id']];
            if (!array_key_exists($archiveIndicator[$byServiceId], $dataSA)) {
                $name = $this->fetchTable('OrgEntities')->get($archiveIndicator[$byServiceId])->get('name');
                $dataSA[$archiveIndicator[$byServiceId]] = [
                    'agency_name' => $name,
                    'total_size' => 0,
                    'original_size' => 0,
                    'preservation_size' => 0,
                    'dissemination_size' => 0,
                    'timestamp_size' => 0,
                    'archive_count' => 0,
                    'units_count' => 0,
                    'original_count' => 0,
                ];
            }
            $dataSVSP = &$dataSA[$archiveIndicator[$byServiceId]];
            $dataSVSP['total_size']
                += $archiveIndicator['original_size']
                + $archiveIndicator['timestamp_size']
                + $archiveIndicator['preservation_size']
                + $archiveIndicator['dissemination_size'];
            $dataSVSP['original_size'] += $archiveIndicator['original_size'];
            $dataSVSP['preservation_size'] += $archiveIndicator['preservation_size'];
            $dataSVSP['dissemination_size'] += $archiveIndicator['dissemination_size'];
            $dataSVSP['timestamp_size'] += $archiveIndicator['timestamp_size'];
            $dataSVSP['archive_count'] += $archiveIndicator['archive_count'];
            $dataSVSP['units_count'] += $archiveIndicator['units_count'];
            $dataSVSP['original_count'] += $archiveIndicator['original_count'];
        }

        // tri en mise en forme
        ksort($data);
        foreach ($data as &$dataSA) {
            ksort($dataSA);
            if ($this->args->getOption('show-readable-size')) {
                foreach ($dataSA as &$dataSVSP) {
                    $dataSVSP['total_size'] = Number::toReadableSize($dataSVSP['total_size']);
                    $dataSVSP['original_size'] = Number::toReadableSize($dataSVSP['original_size']);
                    $dataSVSP['preservation_size'] = Number::toReadableSize($dataSVSP['preservation_size']);
                    $dataSVSP['dissemination_size'] = Number::toReadableSize($dataSVSP['dissemination_size']);
                    $dataSVSP['timestamp_size'] = Number::toReadableSize($dataSVSP['timestamp_size']);
                }
            }
        }
        unset($dataSA, $dataSVSP);

        // affichage des indicateurs des archives
        $entete = [
            $byTransferringAgencies ? __("Service versant") : __("Service producteur"),
            __("Volume total"),
            __("Volume doc."),
            __("Volume cons."),
            __("Volume diff."),
            __("Volume horo."),
            __("Nb archives"),
            __("Nb unités archives"),
            __("Nb Doc."),
        ];
        foreach ($data as $sa_id => $dataSA) {
            $this->io->out(
                $this->io->nl()
                . __(
                    "Indicateurs par service {0} des archives pour le service d'Archives {1}",
                    $byTransferringAgencies ? 'versant' : 'producteur',
                    $this->fetchTable('OrgEntities')->get($sa_id)->get('name')
                ) . $this->io->nl()
            );
            $this->io->helper('Table')->output(array_merge([$entete], $dataSA));
        }
    }

    /**
     * affiche les indicateurs calculés des messages
     * @return void
     */
    private function showMessagesIndicators(): void
    {
        // versant ou producteur
        $byTransferringAgencies = $this->args->getOption('show-by') === self::SHOW_BY_TRANSFERRING_AGENCY;
        $byServiceId = $byTransferringAgencies ? 'transferring_agency_id' : 'originating_agency_id';
        $showMessageDefinition = $this->showMessagesDefinitions[$this->args->getOption('show-subject')];

        // compilation des indicateurs des messages par service d'archives et service versant/producteur
        $data = [];
        foreach ($this->sessionData[self::MESSAGES_INDICATORS] as $messageIndicator) {
            if ($messageIndicator['message_type'] !== $showMessageDefinition['message_type']) {
                continue;
            }
            if (!array_key_exists($messageIndicator['archival_agency_id'], $data)) {
                $data[$messageIndicator['archival_agency_id']] = [];
            }
            $dataSA = &$data[$messageIndicator['archival_agency_id']];
            if (!array_key_exists($messageIndicator[$byServiceId], $dataSA)) {
                $name = $this->fetchTable('OrgEntities')->get($messageIndicator[$byServiceId])->get('name');
                $dataSA[$messageIndicator[$byServiceId]] = [
                    'agency_name' => $name,
                    'message_count' => 0,
                    'rejected_message_count' => 0,
                    'derogation_message_count' => 0,
                    'rejected_derogation_message_count' => 0,
                    'original_files_count' => 0,
                    'original_files_size' => 0,
                    'validation_duration' => 0,
                ];
            }
            $dataSVSP = &$dataSA[$messageIndicator[$byServiceId]];
            if ($messageIndicator['derogation']) {
                $dataSVSP['derogation_message_count'] += $messageIndicator['message_count'];
            }
            $dataSVSP['validation_duration']
                = max($dataSVSP['validation_duration'], $messageIndicator['validation_duration']);
            if ($messageIndicator['accepted']) {
                $dataSVSP['message_count'] += $messageIndicator['message_count'];
                $dataSVSP['original_files_count'] += $messageIndicator['original_files_count'];
                $dataSVSP['original_files_size'] += $messageIndicator['original_files_size'];
            } else {
                $dataSVSP['rejected_message_count'] += $messageIndicator['message_count'];
                if ($messageIndicator['derogation']) {
                    $dataSVSP['rejected_derogation_message_count'] += $messageIndicator['message_count'];
                }
                if ($messageIndicator['message_type'] === MessageIndicatorsTable::IN_TRANSFER) {
                    $dataSVSP['message_count'] += $messageIndicator['message_count'];
                    $dataSVSP['original_files_count'] += $messageIndicator['original_files_count'];
                    $dataSVSP['original_files_size'] += $messageIndicator['original_files_size'];
                }
            }
        }

        // tri en mise en forme
        ksort($data);
        foreach ($data as &$dataSA) {
            ksort($dataSA);
            foreach ($dataSA as &$dataSVSP) {
                $dataSVSP = $this->formatMessageTableRow($dataSVSP, array_keys($showMessageDefinition['columns']));
            }
        }
        unset($dataSA, $dataSVSP);

        // affichage des indicateurs des archives
        $entete = array_merge(
            [$byTransferringAgencies ? __("Service versant") : __("Service producteur")],
            array_values($showMessageDefinition['columns'])
        );
        foreach ($data as $sa_id => &$dataSA) {
            $this->io->out(
                $this->io->nl()
                . __(
                    "Indicateurs par service {0} des {1} pour le service d'Archives {2}",
                    $byTransferringAgencies ? 'versant' : 'producteur',
                    $showMessageDefinition['name'],
                    $this->fetchTable('OrgEntities')->get($sa_id)->get('name')
                ) . $this->io->nl()
            );
            ksort($dataSA);
            $this->io->helper('Table')->output(array_merge([$entete], $dataSA));
        }
    }

    /**
     * Met en forme une ligne pour l'affichage des indicateurs des messages
     * @param array $dataSVSP données brutes des indicateurs
     * @param array $columns  colonnes du tableau des indicateurs
     * @return array
     */
    private function formatMessageTableRow(array $dataSVSP, array $columns): array
    {
        $dataShow['agency_name'] = $dataSVSP['agency_name'];
        foreach ($columns as $column) {
            switch ($column) {
                case 'original_files_size':
                    $dataShow[$column]
                        = $this->args->getOption('show-readable-size')
                        ? Number::toReadableSize($dataSVSP[$column])
                        : $dataSVSP[$column];
                    break;
                case 'validation_duration':
                    $dataShow[$column] = (new CakeDateTime($dataSVSP[$column]))->format('H:i:s');
                    break;
                default:
                    $dataShow[$column] = $dataSVSP[$column];
            }
        }
        return $dataShow;
    }
}
