<?php

/**
 * Asalae\Command\Indicators\UpdateCalculateCommand
 */

namespace Asalae\Command\Indicators;

use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\DeliveryRequestsTable;
use Asalae\Model\Table\DestructionRequestsTable;
use Asalae\Model\Table\MessageIndicatorsTable;
use Asalae\Model\Table\OutgoingTransferRequestsTable;
use Asalae\Model\Table\OutgoingTransfersTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Command\Command;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Utility\DOMUtility;
use Cake\Command\Helper\ProgressHelper;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Database\Expression\IdentifierExpression;
use DOMElement;

/**
 * Effectue la mise à jours des indicateurs
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UpdateCalculateCommand extends AbstractIndicatorsUpdateCommand
{
    /**
     * constantes de la classe
     */
    public const string KEY_SEPARATOR = '.';

    /**
     * archive lue en base de données en cours de traitement
     * @var array
     */
    public $archive;

    /**
     * utilitaire du dom de la description de l'archive en cours de contrôle
     * @var DOMUtility
     */
    public $descriptionDomUtility;

    /**
     * gestionnaires des ECS
     * @var VolumeManager[]
     */
    public $volumeManagers;

    /**
     * utilitaire du dom du cycle de vie de l'archive en cours de contrôle
     * @var DOMUtility
     */
    public $lifecycleDomUtility;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'indicators_update calculate';
    }

    /**
     * Calcule les indicateurs et les stocke dans les fichiers de la session
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io): int
    {
        $this->args = $args;
        $this->io = $io;

        $this->checkSessionExistOrFail();

        $this->loadSessionData();

        // date de départ du calcul
        if (!$this->sessionData[self::SESSION_TRACKING]['calculating_start_date']) {
            $this->sessionData[self::SESSION_TRACKING]['calculating_start_date'] = date('c');
        }

        /** @var ProgressHelper $progress */
        $progress = $this->io->helper('Progress');
        $progress->init(['total' => $this->sessionData[self::SESSION_TRACKING]['archives_count']]);

        // archives et messages acceptés
        $this->io->out(__("Calcul des indicateurs des archives"));
        $this->io->out(" ");
        foreach ($this->sessionData[self::ARCHIVES_TRACKING] as $i => &$trackingData) {
            $progress->increment();
            if ($trackingData['processed']) {
                $progress->draw();
                continue;
            }
            // lecture de l'archive
            $this->archive = $this->fetchTable('Archives')->find()
                ->where(['id' => $trackingData['id']])
                ->contain(['Transfers'])
                ->disableHydration()
                ->firstOrFail();
            $this->archive['state'] = $trackingData['state'];

            // chargement des xml
            $this->setDescriptionDomUtility();
            $this->setLifecycleDomUtility();

            // création de l'archive (archive_indicators et message_indicators)
            $aiCreationData = $this->indicatorsArchiveCreation();

            // éliminations partielles de l'archive (archive_indicators et message_indicators)
            $aiPartDestructionsData = $this->indicatorsArchivePartialDestructions();

            switch ($this->archive['state']) {
                case ArchivesTable::S_DESTROYED:
                    //  élimination de l'archive (archive_indicators et message_indicators)
                    $this->indicatorsArchiveDestruction($aiCreationData, $aiPartDestructionsData);
                    break;
                case ArchivesTable::S_RESTITUTED:
                    // restitution de l'archive (archive_indicators et message_indicators)
                    $this->indicatorsArchiveRestitution($aiCreationData, $aiPartDestructionsData);
                    break;
                case ArchivesTable::S_TRANSFERRED:
                    // transfert sortant de l'archive (archive_indicators et message_indicators)
                    $this->indicatorsArchiveOutTransfer($aiCreationData, $aiPartDestructionsData);
                    break;
            }

            // communications (archive_indicators et message_indicators)
            $this->indicatorsDeliveries();

            // mise à jour du suivi
            $trackingData['processed'] = true;
            $this->sessionData[self::SESSION_TRACKING]['archives_processed']++;
            $this->sessionData[self::SESSION_TRACKING]['updating_date'] = date('Y-m-d H:i:s');

            $this->descriptionDomUtility = null;
            $this->lifecycleDomUtility = null;

            $progress->draw();
            if (($i + 1) % 100 === 0) {
                $this->saveSessionData();
            }
        }
        unset($trackingData);

        $this->io->out(" ", 2);
        $this->io->out(__("Calcul des indicateurs des demandes rejetées"), 2);

        // transferts rejetés
        foreach ($this->sessionData[self::REJECTED_IN_TRANSFER_TRACKING] as $trackingKey => $trackingData) {
            if ($trackingData['processed']) {
                continue;
            }
            $this->indicatorsMessageInTransfers($trackingData['id']);

            // mise à jour du suivi
            $this->sessionData[self::REJECTED_IN_TRANSFER_TRACKING][$trackingKey]['processed'] = true;
            $this->sessionData[self::SESSION_TRACKING]['rejected_in_transfers_processed']++;
            $this->sessionData[self::SESSION_TRACKING]['updating_date'] = date('Y-m-d H:i:s');
        }

        // demandes de communication rejetées
        foreach ($this->sessionData[self::REJECTED_DELIVERY_REQUEST_TRACKING] as $trackingKey => $trackingData) {
            if ($trackingData['processed']) {
                continue;
            }
            $this->indicatorsMessageRejectedDeliveryRequests($trackingData['id']);

            // mise à jour du suivi
            $this->sessionData[self::REJECTED_DELIVERY_REQUEST_TRACKING][$trackingKey]['processed'] = true;
            $this->sessionData[self::SESSION_TRACKING]['rejected_delivery_requests_processed']++;
            $this->sessionData[self::SESSION_TRACKING]['updating_date'] = date('Y-m-d H:i:s');
        }

        // demandes d'élimination rejetées
        foreach ($this->sessionData[self::REJECTED_DESTRUCTION_REQUEST_TRACKING] as $trackingKey => $trackingData) {
            if ($trackingData['processed']) {
                continue;
            }
            $this->indicatorsMessageRejectedDestructionRequests($trackingData['id']);

            // mise à jour du suivi
            $this->sessionData[self::REJECTED_DESTRUCTION_REQUEST_TRACKING][$trackingKey]['processed'] = true;
            $this->sessionData[self::SESSION_TRACKING]['rejected_destruction_requests_processed']++;
            $this->sessionData[self::SESSION_TRACKING]['updating_date'] = date('Y-m-d H:i:s');
        }

        // demandes de restitution rejetées
        foreach ($this->sessionData[self::REJECTED_RESTITUTION_REQUEST_TRACKING] as $trackingKey => $trackingData) {
            if ($trackingData['processed']) {
                continue;
            }
            $this->indicatorsMessageRejectedRestitutionRequests($trackingData['id']);

            // mise à jour du suivi
            $this->sessionData[self::REJECTED_RESTITUTION_REQUEST_TRACKING][$trackingKey]['processed'] = true;
            $this->sessionData[self::SESSION_TRACKING]['rejected_restitution_requests_processed']++;
            $this->sessionData[self::SESSION_TRACKING]['updating_date'] = date('Y-m-d H:i:s');
        }

        // transferts sortants rejetées
        foreach ($this->sessionData[self::REJECTED_OUT_TRANSFER_TRACKING] as $trackingKey => $trackingData) {
            if ($trackingData['processed']) {
                continue;
            }
            $this->indicatorsMessageOutTransfers($trackingData['id']);

            // mise à jour du suivi
            $this->sessionData[self::REJECTED_OUT_TRANSFER_TRACKING][$trackingKey]['processed'] = true;
            $this->sessionData[self::SESSION_TRACKING]['rejected_out_transfers_processed']++;
            $this->sessionData[self::SESSION_TRACKING]['updating_date'] = date('Y-m-d H:i:s');
        }

        $this->saveSessionData();

        return Command::CODE_SUCCESS;
    }

    /**
     * initialise le domUtility de la description de l'archive courante
     * @return void
     * @throws VolumeException
     */
    private function setDescriptionDomUtility(): void
    {
        if (!$this->descriptionDomUtility) {
            $this->descriptionDomUtility = DOMUtility::loadXML($this->getArchiveFileContent('description'));
        }
    }

    /**
     * retourne le contenu d'un fichier de l'archive en cours de contrôle en fonction de son type
     * @param string $type
     * @return string|null
     * @throws VolumeException
     */
    private function getArchiveFileContent(string $type): ?string
    {
        $ArchiveFiles = $this->fetchTable('ArchiveFiles');
        // lecture du fichier de l'archive
        $archiveFile = $ArchiveFiles->find()
            ->where(
                [
                    'archive_id' => $this->archive['id'],
                    'type' => $type,
                ]
            )
            ->first();

        if (!$archiveFile) {
            return null;
        }
        $volumeManager = $this->getVolumeManager($this->archive['secure_data_space_id']);
        return $volumeManager->get($archiveFile->get('stored_file_id'));
    }

    /**
     * Donne l'ECS
     * @param int $secure_data_space_id
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager($secure_data_space_id): VolumeManager
    {
        if (!isset($this->volumeManagers[$secure_data_space_id])) {
            $this->volumeManagers[$secure_data_space_id] = new VolumeManager(
                $secure_data_space_id
            );
        }
        return $this->volumeManagers[$secure_data_space_id];
    }

    /**
     * initialise le domUtility du cycle de vie de l'archive courante
     * @return void
     * @throws VolumeException
     */
    private function setLifecycleDomUtility(): void
    {
        if (!$this->lifecycleDomUtility) {
            $this->lifecycleDomUtility = DOMUtility::loadXML($this->getArchiveFileContent('lifecycle'));
        }
    }

    /**
     * création de l'archive
     * @return array
     * @throws VolumeException
     */
    private function indicatorsArchiveCreation(): array
    {
        $data = [
            'archive_date' => $this->archive['created'],
            'archival_agency_id' => $this->archive['archival_agency_id'],
            'transferring_agency_id' => $this->archive['transferring_agency_id'],
            'originating_agency_id' => $this->archive['originating_agency_id'],
            'archive_count' => 1,
            'units_count' => $this->archiveUnitsCountFromDescription(),
            'original_count' => $this->binariesCountFromDescription(),
            'original_size' => $this->binariesSizeFromDescription(),
            'timestamp_count' => $this->archive['timestamp_count'],
            'timestamp_size' => $this->archive['timestamp_size'],
            'preservation_count' => $this->archive['preservation_count'],
            'preservation_size' => $this->archive['preservation_size'],
            'dissemination_count' => $this->archive['dissemination_count'],
            'dissemination_size' => $this->archive['dissemination_size'],
            'agreement_id' => $this->archive['agreement_id'],
            'profile_id' => $this->archive['profile_id'],
        ];

        $this->sessionData[self::ARCHIVES_INDICATORS][] = $data;

        // ajout de l'indicateur des messages
        foreach ($this->archive['transfers'] ?? [] as $transfer) {
            $this->indicatorsMessageInTransfers($transfer['id']);
        }

        return [
            'archive_count' => $data['archive_count'],
            'units_count' => $data['units_count'],
            'original_count' => $data['original_count'],
            'original_size' => $data['original_size'],
            'timestamp_count' => $data['timestamp_count'],
            'timestamp_size' => $data['timestamp_size'],
            'preservation_count' => $data['preservation_count'],
            'preservation_size' => $data['preservation_size'],
            'dissemination_count' => $data['dissemination_count'],
            'dissemination_size' => $data['dissemination_size'],
        ];
    }

    /**
     * retourne le nombre d'unité d'archives à partir du fichier de description
     * @param string|null $archiveUnitIdentifier
     * @return int
     * @throws VolumeException
     */
    private function archiveUnitsCountFromDescription(string $archiveUnitIdentifier = null): int
    {
        $this->setDescriptionDomUtility();
        if ($archiveUnitIdentifier) {
            $qContent = DOMUtility::xpathQuote($archiveUnitIdentifier);
            switch ($this->descriptionDomUtility->namespace) {
                case NAMESPACE_SEDA_02:
                case NAMESPACE_SEDA_10:
                    $contextNode = $this->descriptionDomUtility->node(
                        'ns:ArchivalAgencyArchiveIdentifier[text()=' . $qContent . ']/..'
                        . '|//ns:ArchivalAgencyObjectIdentifier[text()=' . $qContent . ']/..'
                    );
                    break;
                default:
                    $contextNode = $this->descriptionDomUtility->node(
                        '//ns:ArchiveUnit/ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier'
                        . '[text()=' . $qContent . ']/../..'
                    );
            }
        } else {
            $contextNode = null;
        }
        switch ($this->descriptionDomUtility->namespace) {
            case NAMESPACE_SEDA_02:
                $unitsCount
                    = $this->descriptionDomUtility->xpath->query('.//ns:Contains', $contextNode)->length
                    + $this->descriptionDomUtility->xpath->query('.//ns:Document', $contextNode)->length
                    + 1;
                break;
            case NAMESPACE_SEDA_10:
                $unitsCount
                    = $this->descriptionDomUtility->xpath->query('.//ns:ArchiveObject', $contextNode)->length
                    + $this->descriptionDomUtility->xpath->query('.//ns:Document', $contextNode)->length
                    + 1;
                break;
            default:
                $unitsCount = $this->descriptionDomUtility->xpath
                        ->query('.//ns:ArchiveUnit[not(./ns:ArchiveUnitRefId)]', $contextNode)->length
                    + ($contextNode ? 1 : 0);
        }
        return $unitsCount;
    }

    /**
     * retourne le nombre de binaires à partir du fichier de description
     * @param string|null $archiveUnitIdentifier
     * @return int
     * @throws VolumeException
     */
    private function binariesCountFromDescription(string $archiveUnitIdentifier = null): int
    {
        $this->setDescriptionDomUtility();
        if ($archiveUnitIdentifier) {
            $qContent = DOMUtility::xpathQuote($archiveUnitIdentifier);
            switch ($this->descriptionDomUtility->namespace) {
                case NAMESPACE_SEDA_02:
                case NAMESPACE_SEDA_10:
                    $contextNode = $this->descriptionDomUtility->node(
                        'ns:ArchivalAgencyArchiveIdentifier[text()=' . $qContent . ']/..'
                        . '|//ns:ArchivalAgencyObjectIdentifier[text()=' . $qContent . ']/..'
                    );
                    break;
                default:
                    $contextNode = $this->descriptionDomUtility->node(
                        '//ns:ArchiveUnit/ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier[text()=' .
                        $qContent . ']/../..'
                    );
            }
        } else {
            $contextNode = null;
        }
        switch ($this->descriptionDomUtility->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $binariesCount
                    = $this->descriptionDomUtility->xpath->query('.//ns:Document', $contextNode)->length;
                break;
            default:
                $binariesCount = $this->binariesCountFromDescriptionSeda2x($archiveUnitIdentifier);
        }
        return $binariesCount;
    }

    /**
     * retourne le nombre de binaires à partir du fichier de description en seda 2.x
     * @param string|null $archiveUnitIdentifier
     * @return int
     * @throws VolumeException
     */
    private function binariesCountFromDescriptionSeda2x(string $archiveUnitIdentifier = null): int
    {
        $this->setDescriptionDomUtility();
        $binaryDataObjectIds = [];
        if ($archiveUnitIdentifier) {
            $qContent = DOMUtility::xpathQuote($archiveUnitIdentifier);
            $contextNode = $this->descriptionDomUtility->node(
                '//ns:ArchiveUnit/ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier[text()=' .
                $qContent . ']/../..'
            );
        } else {
            $contextNode = null;
        }
        // références sur les binaires
        $dataObjectReferenceIdNodes = $this->descriptionDomUtility->xpath
            ->query('.//ns:DataObjectReferenceId', $contextNode);
        foreach ($dataObjectReferenceIdNodes as $dataObjectReferenceIdNode) {
            if (!in_array($dataObjectReferenceIdNode->nodeValue, $binaryDataObjectIds)) {
                $binaryDataObjectIds[] = $dataObjectReferenceIdNode->nodeValue;
            }
        }
        // références vers les groupes de binaires
        $dataObjectGroupReferenceIdNodes = $this->descriptionDomUtility->xpath
            ->query('.//ns:DataObjectGroupReferenceId', $contextNode);
        foreach ($dataObjectGroupReferenceIdNodes as $dataObjectGroupReferenceIdNode) {
            // lecture des binaires du groupe
            $binaryDataObjectNodes = $this->descriptionDomUtility->xpath->query(
                '//ns:DataObjectGroup[@id="'
                . $dataObjectGroupReferenceIdNode->nodeValue . '"]/ns:BinaryDataObject'
            );
            foreach ($binaryDataObjectNodes as $binaryDataObjectNode) {
                if (!in_array($binaryDataObjectNode->getAttribute('id'), $binaryDataObjectIds)) {
                    $binaryDataObjectIds[] = $binaryDataObjectNode->getAttribute('id');
                }
            }
        }
        return count($binaryDataObjectIds);
    }

    /**
     * retourne la taille des binaires à partir du fichier de description et du fichier du cycle de vie
     * @param string|null $archiveUnitIdentifier
     * @return int
     * @throws VolumeException
     */
    private function binariesSizeFromDescription(string $archiveUnitIdentifier = null): int
    {
        $binariesSize = 0;
        $this->setDescriptionDomUtility();
        $this->setLifecycleDomUtility();
        if ($archiveUnitIdentifier) {
            $qContent = DOMUtility::xpathQuote($archiveUnitIdentifier);
            switch ($this->descriptionDomUtility->namespace) {
                case NAMESPACE_SEDA_02:
                case NAMESPACE_SEDA_10:
                    $contextNode = $this->descriptionDomUtility->node(
                        'ns:ArchivalAgencyArchiveIdentifier[text()=' . $qContent . ']/..'
                        . '|//ns:ArchivalAgencyObjectIdentifier[text()=' . $qContent . ']/..'
                    );
                    break;
                default:
                    $contextNode = $this->descriptionDomUtility->node(
                        '//ns:ArchiveUnit/ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier[text()=' .
                        $qContent . ']/../..'
                    );
            }
        } else {
            $contextNode = null;
        }
        switch ($this->descriptionDomUtility->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $attachmentNodes
                    = $this->descriptionDomUtility->xpath->query('.//ns:Document/ns:Attachment', $contextNode);
                foreach ($attachmentNodes as $attachmentNode) {
                    $qContent = DOMUtility::xpathQuote($attachmentNode->getAttribute('filename'));
                    $sizeNode = $this->lifecycleDomUtility->xpath->query(
                        'ns:object[@xsi:type="premis:file"]/ns:originalName[text()='
                        . $qContent . ']/../ns:objectCharacteristics/ns:size'
                    );
                    if ($sizeNode->length > 0) {
                        $binariesSize += $sizeNode->item(0)->nodeValue;
                    }
                }
                break;
            default:
                $binariesSize = $this->binariesSizeFromDescriptionSeda2x($archiveUnitIdentifier);
        }
        return $binariesSize;
    }

    /**
     * retourne la taille des binaires à partir du fichier de description en seda 2.x et du fichier du cycle de vie
     * @param string|null $archiveUnitIdentifier
     * @return int
     * @throws VolumeException
     */
    private function binariesSizeFromDescriptionSeda2x(string $archiveUnitIdentifier = null): int
    {
        $this->setDescriptionDomUtility();
        $this->setLifecycleDomUtility();
        if ($archiveUnitIdentifier) {
            $qContent = DOMUtility::xpathQuote($archiveUnitIdentifier);
            $contextNode = $this->descriptionDomUtility->node(
                '//ns:ArchiveUnit/ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier[text()=' .
                $qContent . ']/../..'
            );
        } else {
            $contextNode = null;
        }
        $binaryDataObjectIds = [];
        // références sur les binaires
        $dataObjectReferenceIdNodes = $this->descriptionDomUtility->xpath
            ->query('.//ns:DataObjectReferenceId', $contextNode);
        foreach ($dataObjectReferenceIdNodes as $dataObjectReferenceIdNode) {
            if (!in_array($dataObjectReferenceIdNode->nodeValue, $binaryDataObjectIds)) {
                $binaryDataObjectIds[] = $dataObjectReferenceIdNode->nodeValue;
            }
        }
        // références vers les groupes de binaire
        $dataObjectGroupReferenceIdNodes = $this->descriptionDomUtility->xpath
            ->query('.//ns:DataObjectGroupReferenceId', $contextNode);
        foreach ($dataObjectGroupReferenceIdNodes as $dataObjectGroupReferenceIdNode) {
            // lecture des binaires du groupe
            $binaryDataObjectNodes = $this->descriptionDomUtility->xpath->query(
                '//ns:DataObjectGroup[@id="'
                . $dataObjectGroupReferenceIdNode->nodeValue . '"]/ns:BinaryDataObject'
            );
            foreach ($binaryDataObjectNodes as $binaryDataObjectNode) {
                if (!in_array($binaryDataObjectNode->getAttribute('id'), $binaryDataObjectIds)) {
                    $binaryDataObjectIds[] = $binaryDataObjectNode->getAttribute('id');
                }
            }
        }
        $binariesSize = 0;
        foreach ($binaryDataObjectIds as $binaryDataObjectId) {
            $filename = $this->getFilenameFromDescriptionSeda2x($binaryDataObjectId);
            $sizeNode = $this->lifecycleDomUtility->xpath->query(
                'ns:object[@xsi:type="premis:file"]/ns:originalName[text()='
                . DOMUtility::xpathQuote($filename) . ']/../ns:objectCharacteristics/ns:size'
            );
            if ($sizeNode->length > 0) {
                $binariesSize += $sizeNode->item(0)->nodeValue;
            }
        }
        return $binariesSize;
    }

    /**
     * retourne le nom d'un fichier en fonction de son xml_id dans le fichier de description
     * @param string $binaryDataObjectId
     * @return string
     * @throws VolumeException
     */
    private function getFilenameFromDescriptionSeda2x(string $binaryDataObjectId): string
    {
        $attachmentNodes = $this->descriptionDomUtility->xpath
            ->query('//ns:BinaryDataObject[@id="' . $binaryDataObjectId . '"]/ns:Attachment');
        if ($attachmentNodes->length > 0) {
            /** @var DOMElement $attachmentNode */
            $attachmentNode = $attachmentNodes->item(0);
            return $attachmentNode->getAttribute('filename');
        }
        $filenameNode = $this->descriptionDomUtility->xpath
            ->query('//ns:BinaryDataObject[@id="' . $binaryDataObjectId . '"]/ns:FileInfo/ns:Filename');
        if ($filenameNode->length > 0) {
            return $filenameNode->item(0)->nodeValue;
        }
        // dernière chance : non trouvé dans la description, alors on le cherche dans le transfert
        $transferDomUtility = DOMUtility::loadXML($this->getArchiveFileContent('transfer'));
        $attachmentNodes = $transferDomUtility->xpath
            ->query('//ns:BinaryDataObject[@id="' . $binaryDataObjectId . '"]/ns:Attachment');
        if ($attachmentNodes->length > 0) {
            /** @var DOMElement $attachmentNode */
            $attachmentNode = $attachmentNodes->item(0);
            return $attachmentNode->getAttribute('filename');
        }
        $filenameNode = $transferDomUtility->xpath
            ->query('//ns:BinaryDataObject[@id="' . $binaryDataObjectId . '"]/ns:FileInfo/ns:Filename');
        if ($filenameNode->length > 0) {
            return $filenameNode->item(0)->nodeValue;
        }

        return '';
    }

    /**
     * transferts entrants acceptés ou rejetés
     * @param int $transferId
     * @return void
     */
    private function indicatorsMessageInTransfers(int $transferId): void
    {
        $transfer = $this->fetchTable('Transfers')->find()
            ->where(['id' => $transferId])
            ->disableHydration()
            ->firstOrFail();

        $messageKey = implode(
            self::KEY_SEPARATOR,
            [
                MessageIndicatorsTable::IN_TRANSFER,
                $transfer['archival_agency_id'],
                $transfer['transfer_identifier'],
            ]
        );

        $this->sessionData[self::MESSAGES_INDICATORS][$messageKey] = [
            'message_date' => $transfer['created'],
            'message_type' => MessageIndicatorsTable::IN_TRANSFER,
            'message_count' => 1,
            'accepted' => $transfer['is_accepted'],
            'derogation' => false,
            'validation_duration' => $this->durationBetweenTwoStates(
                $transfer['state_history'],
                TransfersTable::S_VALIDATING,
                $transfer['is_accepted'] ? TransfersTable::S_ACCEPTED : TransfersTable::S_REJECTED
            ),
            'archival_agency_id' => $transfer['archival_agency_id'],
            'transferring_agency_id' => $transfer['transferring_agency_id'],
            'originating_agency_id' => $transfer['originating_agency_id'],
            'original_files_count' => $transfer['data_count'],
            'original_files_size' => $transfer['data_size'],
            'agreement_id' => $transfer['agreement_id'],
            'profile_id' => $transfer['profile_id'],
        ];
    }

    /**
     * retourne le nombre de secondes entre 2 états,
     * retourne 0 si au moins un des 2 états n'est pas présent
     * @param string $jsonStateHistory
     * @param string $startState
     * @param string $endState
     * @return int
     */
    private function durationBetweenTwoStates(string $jsonStateHistory, string $startState, string $endState): int
    {
        $stateHistories = json_decode($jsonStateHistory, true);
        $startTime = null;
        $endTime = null;
        foreach ($stateHistories as $stateHistory) {
            if ($stateHistory['state'] === $startState) {
                $startTime = strtotime($stateHistory['date']);
            } elseif ($stateHistory['state'] === $endState) {
                $endTime = strtotime($stateHistory['date']);
            }
            if ($startTime && $endTime) {
                break;
            }
        }
        if ($startTime && $endTime) {
            return $endTime - $startTime;
        } else {
            return 0;
        }
    }

    /**
     * éliminations partielles de l'archive
     * @return int[] données détruites
     * @throws VolumeException
     */
    private function indicatorsArchivePartialDestructions(): array
    {
        $destroyedData = [
            'archive_count' => 0,
            'units_count' => 0,
            'original_count' => 0,
            'original_size' => 0,
            'timestamp_count' => 0,
            'timestamp_size' => 0,
            'preservation_count' => 0,
            'preservation_size' => 0,
            'dissemination_count' => 0,
            'dissemination_size' => 0,
        ];

        $ArchiveFiles = $this->fetchTable('ArchiveFiles');
        // présence du fichier de management deleted.json : éliminations ou élimination(s) partielle(s)
        if (!$ArchiveFiles->exists(['archive_id' => $this->archive['id'], 'type' => 'deleted'])) {
            return $destroyedData;
        }

        // lecture des événements des éliminations
        $this->setLifecycleDomUtility();
        $qContent = DOMUtility::xpathQuote('archiveunit_delete');
        $deleteEventNodes = $this->lifecycleDomUtility->xpath
            ->query('ns:event/ns:eventType[text()=' . $qContent . ']/..');
        if ($deleteEventNodes->length === 0) {
            $this->io->abort(
                __(
                    "Erreur : événements archiveunit_delete non trouvé dans le fichier"
                    . " du cycle de vie de l'archive {0} (id {1})",
                    $this->archive['archival_agency_identifier'],
                    $this->archive['id']
                )
            );
        }
        foreach ($deleteEventNodes as $i => $deleteEventNode) {
            if (($i + 1) === $deleteEventNodes->length && $this->archive['state'] === ArchivesTable::S_DESTROYED) {
                // si archive éliminée, le dernier événement sera traité plus loin dans le code
                break;
            }
            // date de l'événement
            $eventDateTimeNode = $this->lifecycleDomUtility->node('./ns:eventDateTime', $deleteEventNode);
            // recherche de l'identifiant de l'unité d'archive supprimée
            $deletedAUPremisId = $this->lifecycleDomUtility->nodeValue(
                './ns:linkingObjectIdentifier/ns:linkingObjectIdentifierValue',
                $deleteEventNode
            );
            $qContent = DOMUtility::xpathQuote($deletedAUPremisId);
            $deletedAUIdentifier = $this->lifecycleDomUtility->node(
                'ns:object/ns:objectIdentifier/ns:objectIdentifierValue[text()='
                . $qContent . ']/../../ns:originalName'
            );

            // ajout de l'indicateur des archives
            $originalCount = $this->binariesCountFromDescription($deletedAUIdentifier->nodeValue);
            $originalSize = $this->binariesSizeFromDescription($deletedAUIdentifier->nodeValue);
            $data = [
                'archive_date' => $eventDateTimeNode->nodeValue,
                'archival_agency_id' => $this->archive['archival_agency_id'],
                'transferring_agency_id' => $this->archive['transferring_agency_id'],
                'originating_agency_id' => $this->archive['originating_agency_id'],
                'archive_count' => 0,
                'units_count' => 0 - $this->archiveUnitsCountFromDescription($deletedAUIdentifier->nodeValue),
                'original_count' => 0 - $originalCount,
                'original_size' => 0 - $originalSize,
                'timestamp_count' => 0,
                'timestamp_size' => 0,
                'preservation_count' => 0,
                'preservation_size' => 0,
                'dissemination_count' => 0,
                'dissemination_size' => 0,
                'agreement_id' => $this->archive['agreement_id'],
                'profile_id' => $this->archive['profile_id'],
            ];

            $this->sessionData[self::ARCHIVES_INDICATORS][] = $data;

            $destroyedData['units_count'] -= $data['units_count'];
            $destroyedData['original_count'] -= $data['original_count'];
            $destroyedData['original_size'] -= $data['original_size'];
            $destroyedData['timestamp_count'] -= $data['timestamp_count'];
            $destroyedData['timestamp_size'] -= $data['timestamp_size'];
            $destroyedData['preservation_count'] -= $data['preservation_count'];
            $destroyedData['preservation_size'] -= $data['preservation_size'];
            $destroyedData['dissemination_count'] -= $data['dissemination_count'];
            $destroyedData['dissemination_size'] -= $data['dissemination_size'];

            // ajout de l'indicateur des messages
            $destructionNotification = $this->fetchTable('DestructionNotifications')->find()
                ->innerJoinWith('DestructionNotificationXpaths')
                ->where(
                    [
                        'DestructionNotificationXpaths.archive_id' => $this->archive['id'],
                        'DestructionNotificationXpaths.archive_unit_identifier' => $deletedAUIdentifier->nodeValue,
                    ]
                )
                ->contain(['DestructionRequests'])
                ->disableHydration()
                ->firstOrFail();

            $messageKey = implode(
                self::KEY_SEPARATOR,
                [
                    MessageIndicatorsTable::DESTRUCTION,
                    $destructionNotification['destruction_request']['archival_agency_id'],
                    $destructionNotification['identifier'],
                    $this->archive['transferring_agency_id'],
                    $this->archive['agreement_id'],
                    $this->archive['profile_id'],
                ]
            );
            if (empty($this->sessionData[self::MESSAGES_INDICATORS][$messageKey])) {
                $this->sessionData[self::MESSAGES_INDICATORS][$messageKey] = [
                    'message_date' => $destructionNotification['created'],
                    'message_type' => MessageIndicatorsTable::DESTRUCTION,
                    'message_count' => 1,
                    'accepted' => true,
                    'derogation' => false,
                    'validation_duration' => $this->durationBetweenTwoStates(
                        $destructionNotification['destruction_request']['states_history'],
                        DestructionRequestsTable::S_VALIDATING,
                        DestructionRequestsTable::S_ACCEPTED
                    ),
                    'archival_agency_id' => $destructionNotification['destruction_request']['archival_agency_id'],
                    'transferring_agency_id' => $this->archive['transferring_agency_id'],
                    'originating_agency_id' => $destructionNotification['destruction_request']['originating_agency_id'],
                    'original_files_count' => $originalCount,
                    'original_files_size' => $originalSize,
                    'agreement_id' => $this->archive['agreement_id'],
                    'profile_id' => $this->archive['profile_id'],
                ];
            } else {
                $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_count'] += $originalCount;
                $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_size'] += $originalSize;
            }
        }
        return $destroyedData;
    }

    /**
     * suppression de l'archive
     * @param array $archiveCreationData
     * @param array $aiPartDestructionsData
     * @return void
     */
    private function indicatorsArchiveDestruction(array $archiveCreationData, array $aiPartDestructionsData): void
    {
        // date de l'opération d'élimination (dernier événément en cas d'élimination patielle précédente)
        $deleteEventNodes = $this->lifecycleDomUtility->xpath
            ->query('(//ns:event/ns:eventType[text() = "archiveunit_delete"])[last()]/..');
        if ($deleteEventNodes->length === 0) {
            $this->io->abort(
                __(
                    "Erreur : événement {0} non trouvé dans le fichier"
                    . " du cycle de vie de l'archive {1} (id {2})",
                    'archiveunit_delete',
                    $this->archive['archival_agency_identifier'],
                    $this->archive['id']
                )
            );
        }
        $deleteEventNode = $deleteEventNodes->item(0);
        // date de l'événement
        $eventDateTimeNode = $this->lifecycleDomUtility->node('./ns:eventDateTime', $deleteEventNode);
        // recherche de l'identifiant de l'unité d'archive supprimée
        $deletedAUPremisId = $this->lifecycleDomUtility
            ->nodeValue('./ns:linkingObjectIdentifier/ns:linkingObjectIdentifierValue', $deleteEventNode);
        $qContent = DOMUtility::xpathQuote($deletedAUPremisId);
        $deletedAUIdentifier = $this->lifecycleDomUtility->node(
            'ns:object/ns:objectIdentifier/ns:objectIdentifierValue[text()=' . $qContent . ']/../../ns:originalName'
        );

        // ajout de l'indicateur des archives
        $data = [
            'archive_date' => $eventDateTimeNode->nodeValue,
            'archival_agency_id' => $this->archive['archival_agency_id'],
            'transferring_agency_id' => $this->archive['transferring_agency_id'],
            'originating_agency_id' => $this->archive['originating_agency_id'],
            'archive_count' => $aiPartDestructionsData['archive_count'] - $archiveCreationData['archive_count'],
            'units_count' => $aiPartDestructionsData['units_count'] - $archiveCreationData['units_count'],
            'original_count' => $aiPartDestructionsData['original_count'] - $archiveCreationData['original_count'],
            'original_size' => $aiPartDestructionsData['original_size'] - $archiveCreationData['original_size'],
            'timestamp_count' => 0 - $archiveCreationData['timestamp_count'],
            'timestamp_size' => 0 - $archiveCreationData['timestamp_size'],
            'preservation_count' => 0 - $archiveCreationData['preservation_count'],
            'preservation_size' => 0 - $archiveCreationData['preservation_size'],
            'dissemination_count' => 0 - $archiveCreationData['dissemination_count'],
            'dissemination_size' => 0 - $archiveCreationData['dissemination_size'],
            'agreement_id' => $this->archive['agreement_id'],
            'profile_id' => $this->archive['profile_id'],
        ];

        $this->sessionData[self::ARCHIVES_INDICATORS][] = $data;

        // ajout de l'indicateur des messages
        $destructionNotification = $this->fetchTable('DestructionNotifications')->find()
            ->innerJoinWith('DestructionNotificationXpaths')
            ->where(
                [
                    'DestructionNotificationXpaths.archive_id' => $this->archive['id'],
                    'DestructionNotificationXpaths.archive_unit_identifier' => $deletedAUIdentifier->nodeValue,
                ]
            )
            ->contain(['DestructionRequests'])
            ->disableHydration()
            ->firstOrFail();

        $messageKey = implode(
            self::KEY_SEPARATOR,
            [
                MessageIndicatorsTable::DESTRUCTION,
                $destructionNotification['destruction_request']['archival_agency_id'],
                $destructionNotification['identifier'],
                $this->archive['transferring_agency_id'],
                $this->archive['agreement_id'],
                $this->archive['profile_id'],
            ]
        );
        if (empty($this->sessionData[self::MESSAGES_INDICATORS][$messageKey])) {
            $this->sessionData[self::MESSAGES_INDICATORS][$messageKey] = [
                'message_date' => $destructionNotification['created'],
                'message_type' => MessageIndicatorsTable::DESTRUCTION,
                'message_count' => 1,
                'accepted' => true,
                'derogation' => false,
                'validation_duration' => $this->durationBetweenTwoStates(
                    $destructionNotification['destruction_request']['states_history'],
                    DestructionRequestsTable::S_VALIDATING,
                    DestructionRequestsTable::S_ACCEPTED
                ),
                'archival_agency_id' => $destructionNotification['destruction_request']['archival_agency_id'],
                'transferring_agency_id' => $this->archive['transferring_agency_id'],
                'originating_agency_id' => $destructionNotification['destruction_request']['originating_agency_id'],
                'original_files_count' => 0 - $data['original_count'],
                'original_files_size' => 0 - $data['original_size'],
                'agreement_id' => $this->archive['agreement_id'],
                'profile_id' => $this->archive['profile_id'],
            ];
        } else {
            $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_count']
                -= $data['original_count'];
            $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_size']
                -= $data['original_size'];
        }
    }

    /**
     * restitution de l'archive
     * @param array $archiveCreationData
     * @param array $aiPartDestructionsData
     * @return void
     */
    private function indicatorsArchiveRestitution(array $archiveCreationData, array $aiPartDestructionsData): void
    {
        // date de l'opération de restitution
        $restitutionBuildEventNodes = $this->lifecycleDomUtility->xpath->query(
            '//ns:event/ns:eventType[text() = "restitution_build" or text() = "restitution"]/..'
        );
        if ($restitutionBuildEventNodes->length === 0) {
            $this->io->abort(
                __(
                    "Erreur : événement {0} non trouvé dans le fichier"
                    . " du cycle de vie de l'archive {1} (id {2})",
                    'restitution_build',
                    $this->archive['archival_agency_identifier'],
                    $this->archive['id']
                )
            );
        }
        $restitutionBuildEventNode = $restitutionBuildEventNodes->item(0);
        // date de l'événement
        $eventDateTimeNode = $this->lifecycleDomUtility->node('./ns:eventDateTime', $restitutionBuildEventNode);

        // ajout de l'indicateur des archives
        $data = [
            'archive_date' => $eventDateTimeNode->nodeValue,
            'archival_agency_id' => $this->archive['archival_agency_id'],
            'transferring_agency_id' => $this->archive['transferring_agency_id'],
            'originating_agency_id' => $this->archive['originating_agency_id'],
            'archive_count' => $aiPartDestructionsData['archive_count'] - $archiveCreationData['archive_count'],
            'units_count' => $aiPartDestructionsData['units_count'] - $archiveCreationData['units_count'],
            'original_count' => $aiPartDestructionsData['original_count'] - $archiveCreationData['original_count'],
            'original_size' => $aiPartDestructionsData['original_size'] - $archiveCreationData['original_size'],
            'timestamp_count' => 0 - $archiveCreationData['timestamp_count'],
            'timestamp_size' => 0 - $archiveCreationData['timestamp_size'],
            'preservation_count' => 0 - $archiveCreationData['preservation_count'],
            'preservation_size' => 0 - $archiveCreationData['preservation_size'],
            'dissemination_count' => 0 - $archiveCreationData['dissemination_count'],
            'dissemination_size' => 0 - $archiveCreationData['dissemination_size'],
            'agreement_id' => $this->archive['agreement_id'],
            'profile_id' => $this->archive['profile_id'],
        ];

        $this->sessionData[self::ARCHIVES_INDICATORS][] = $data;

        // ajout de l'indicateur des messages
        $restitutionRequest = $this->fetchTable('RestitutionRequests')->find()
            ->leftJoin(
                ['arq' => 'archives_restitution_requests'],
                ['arq.restitution_request_id' => new IdentifierExpression('RestitutionRequests.id')]
            )
            ->where(
                [
                    'arq.archive_id' => $this->archive['id'],
                ]
            )
            ->disableHydration()
            ->first();
        if (!$restitutionRequest) {
            // on ne retrouve pas la demande de restitution
            $messageKey = implode(
                self::KEY_SEPARATOR,
                [
                    MessageIndicatorsTable::RESTITUTION,
                    'unknown_restitution_request',
                    uuid_create(),
                ]
            );
            $validationDuration = 0;
        } else {
            $messageKey = implode(
                self::KEY_SEPARATOR,
                [
                    MessageIndicatorsTable::RESTITUTION,
                    $restitutionRequest['archival_agency_id'],
                    $restitutionRequest['identifier'],
                    $this->archive['transferring_agency_id'],
                    $this->archive['originating_agency_id'],
                    $this->archive['agreement_id'],
                    $this->archive['profile_id'],
                ]
            );
            $validationDuration = $this->durationBetweenTwoStates(
                $restitutionRequest['states_history'],
                RestitutionRequestsTable::S_VALIDATING,
                RestitutionRequestsTable::S_ACCEPTED
            );
        }

        if (empty($this->sessionData[self::MESSAGES_INDICATORS][$messageKey])) {
            $this->sessionData[self::MESSAGES_INDICATORS][$messageKey] = [
                'message_date' => $eventDateTimeNode->nodeValue,
                'message_type' => MessageIndicatorsTable::RESTITUTION,
                'message_count' => 1,
                'accepted' => true,
                'derogation' => false,
                'validation_duration' => $validationDuration,
                'archival_agency_id' => $this->archive['archival_agency_id'],
                'transferring_agency_id' => $this->archive['transferring_agency_id'],
                'originating_agency_id' => $this->archive['originating_agency_id'],
                'original_files_count' => 0 - $data['original_count'],
                'original_files_size' => 0 - $data['original_size'],
                'agreement_id' => $this->archive['agreement_id'],
                'profile_id' => $this->archive['profile_id'],
            ];
        } else {
            $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_count']
                -= $data['original_count'];
            $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_size']
                -= $data['original_size'];
        }
    }

    /**
     * transfert sortant de l'archive
     * @param array $archiveCreationData
     * @param array $aiPartDestructionsData
     * @return void
     */
    private function indicatorsArchiveOutTransfer(array $archiveCreationData, array $aiPartDestructionsData): void
    {
        // date du transfert sortant
        $outTransfertSentdEventNodes = $this->lifecycleDomUtility->xpath->query(
            '//ns:event/ns:eventType[text() = "outgoing_transfer_transfer" or text() = "outgoing_transfer"]/..'
        );
        if ($outTransfertSentdEventNodes->length === 0) {
            $this->io->abort(
                __(
                    "Erreur : événement {0} non trouvé dans le fichier"
                    . " du cycle de vie de l'archive {1} (id {2})",
                    'outgoing_transfer_transfer',
                    $this->archive['archival_agency_identifier'],
                    $this->archive['id']
                )
            );
        }
        $outTransfertSentdEventNode = $outTransfertSentdEventNodes->item(0);
        // date de l'événement
        $eventDateTimeNode = $this->lifecycleDomUtility->node('./ns:eventDateTime', $outTransfertSentdEventNode);

        // ajout de l'indicateur des archives
        $data = [
            'archive_date' => $eventDateTimeNode->nodeValue,
            'archival_agency_id' => $this->archive['archival_agency_id'],
            'transferring_agency_id' => $this->archive['transferring_agency_id'],
            'originating_agency_id' => $this->archive['originating_agency_id'],
            'archive_count' => $aiPartDestructionsData['archive_count'] - $archiveCreationData['archive_count'],
            'units_count' => $aiPartDestructionsData['units_count'] - $archiveCreationData['units_count'],
            'original_count' => $aiPartDestructionsData['original_count'] - $archiveCreationData['original_count'],
            'original_size' => $aiPartDestructionsData['original_size'] - $archiveCreationData['original_size'],
            'timestamp_count' => 0 - $archiveCreationData['timestamp_count'],
            'timestamp_size' => 0 - $archiveCreationData['timestamp_size'],
            'preservation_count' => 0 - $archiveCreationData['preservation_count'],
            'preservation_size' => 0 - $archiveCreationData['preservation_size'],
            'dissemination_count' => 0 - $archiveCreationData['dissemination_count'],
            'dissemination_size' => 0 - $archiveCreationData['dissemination_size'],
            'agreement_id' => $this->archive['agreement_id'],
            'profile_id' => $this->archive['profile_id'],
        ];

        $this->sessionData[self::ARCHIVES_INDICATORS][] = $data;

        // ajout de l'indicateur des messages
        // fonctionnement différent pour les archives transférées avec des versions pré 2.1.11 et 2.2.1
        $outTransfertDestroydEventNodes = $this->lifecycleDomUtility->xpath
            ->query('//ns:event/ns:eventType[text() = "outgoing_transfer_destroy"]/..');
        if ($outTransfertDestroydEventNodes->length === 0) {
            // on ne peut pas retrouver la demande de restitution
            $messageKey = implode(
                self::KEY_SEPARATOR,
                [
                    MessageIndicatorsTable::OUT_TRANSFER,
                    'unknown_outgoing_transfert_request',
                    uuid_create(),
                ]
            );
            $validationDuration = 0;
        } else {
            $outTransfertDestroydEventNode = $outTransfertDestroydEventNodes->item(0);
            $eventOutcomeDetailNote = $this->lifecycleDomUtility->nodeValue(
                './ns:eventOutcomeInformation/ns:eventOutcomeDetail/ns:eventOutcomeDetailNote',
                $outTransfertDestroydEventNode
            );
            $otrIdentifierPos = strrpos($eventOutcomeDetailNote, ' ');
            $otrIdentifier = substr($eventOutcomeDetailNote, $otrIdentifierPos + 1);
            $outgoingTransferRequest = $this->fetchTable('OutgoingTransferRequests')->find()
                ->where(
                    [
                        'archival_agency_id' => $this->archive['archival_agency_id'],
                        'identifier' => $otrIdentifier,
                    ]
                )
                ->disableHydration()
                ->firstOrFail();

            $messageKey = implode(
                self::KEY_SEPARATOR,
                [
                    MessageIndicatorsTable::OUT_TRANSFER,
                    $outgoingTransferRequest['archival_agency_id'],
                    $outgoingTransferRequest['identifier'],
                    $this->archive['transferring_agency_id'],
                    $this->archive['originating_agency_id'],
                    $this->archive['agreement_id'],
                    $this->archive['profile_id'],
                ]
            );
            $validationDuration = $this->durationBetweenTwoStates(
                $outgoingTransferRequest['states_history'],
                OutgoingTransferRequestsTable::S_VALIDATING,
                OutgoingTransferRequestsTable::S_ACCEPTED
            );
        }
        if (empty($this->sessionData[self::MESSAGES_INDICATORS][$messageKey])) {
            $this->sessionData[self::MESSAGES_INDICATORS][$messageKey] = [
                'message_date' => $eventDateTimeNode->nodeValue,
                'message_type' => MessageIndicatorsTable::OUT_TRANSFER,
                'message_count' => 1,
                'accepted' => true,
                'derogation' => false,
                'validation_duration' => $validationDuration,
                'archival_agency_id' => $this->archive['archival_agency_id'],
                'transferring_agency_id' => $this->archive['transferring_agency_id'],
                'originating_agency_id' => $this->archive['originating_agency_id'],
                'original_files_count' => 0 - $data['original_count'],
                'original_files_size' => 0 - $data['original_size'],
                'agreement_id' => $this->archive['agreement_id'],
                'profile_id' => $this->archive['profile_id'],
            ];
        } else {
            $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['message_count']++;
            $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_count']
                -= $data['original_count'];
            $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_size']
                -= $data['original_size'];
        }
    }

    /**
     * communications de l'archive
     * @return void
     * @throws VolumeException
     */
    private function indicatorsDeliveries(): void
    {
        // lecture des événements des communications
        $this->setLifecycleDomUtility();
        $qContent = DOMUtility::xpathQuote('add_delivery');
        $deliveryEventNodes = $this->lifecycleDomUtility->xpath
            ->query('ns:event/ns:eventType[text()=' . $qContent . ']/..');
        foreach ($deliveryEventNodes as $deliveryEventNode) {
            // date de l'événement
            $eventDateTimeNode = $this->lifecycleDomUtility->node('./ns:eventDateTime', $deliveryEventNode);
            // id de l'événement
            $eventIdentifierValue = $this->lifecycleDomUtility->nodeValue(
                './ns:eventIdentifier/ns:eventIdentifierValue',
                $deliveryEventNode
            );
            $eventLogsId = substr($eventIdentifierValue, strpos($eventIdentifierValue, ':') + 1);
            // identifiant de l'UA communiquée
            $eventOutcomeDetailNote = $this->lifecycleDomUtility->nodeValue(
                './ns:eventOutcomeInformation/ns:eventOutcomeDetail/ns:eventOutcomeDetailNote',
                $deliveryEventNode
            );
            $startPos = strlen("Communication de l'unité d'archives ");
            $endPos = strpos($eventOutcomeDetailNote, " et de ses sous unités d'archives dans la communication");
            $archiveUnitIdentifier = substr($eventOutcomeDetailNote, $startPos, $endPos - $startPos);
            // recherche de la demande de communication via les événements
            $eventLogDeliveryRequestValidation = $this->fetchTable('EventLogs')->find()
                ->where(
                    [
                        'id <' => $eventLogsId,
                        'type' => 'validate_DeliveryRequest',
                    ]
                )
                ->orderByDesc('id')
                ->disableHydration()
                ->first();
            if (!$eventLogDeliveryRequestValidation) {
                // on ne retrouve pas la demande de communication
                $messageKey = implode(
                    self::KEY_SEPARATOR,
                    [
                        MessageIndicatorsTable::DELIVERY,
                        'unknown_delivery_request',
                        uuid_create(),
                    ]
                );
                $validationDuration = 0;
                $derogation = false;
            } else {
                $deliveryRequest = $this->fetchTable('DeliveryRequests')->find()
                    ->where(
                        [
                            'archival_agency_id' => $this->archive['archival_agency_id'],
                            'id' => $eventLogDeliveryRequestValidation['object_foreign_key'],
                        ]
                    )
                    ->disableHydration()
                    ->firstOrFail();
                $messageKey = implode(
                    self::KEY_SEPARATOR,
                    [
                        MessageIndicatorsTable::DELIVERY,
                        $deliveryRequest['archival_agency_id'],
                        $deliveryRequest['identifier'],
                        $this->archive['transferring_agency_id'],
                        $this->archive['originating_agency_id'],
                        $this->archive['agreement_id'],
                        $this->archive['profile_id'],
                    ]
                );
                $validationDuration = $this->durationBetweenTwoStates(
                    $deliveryRequest['states_history'],
                    DeliveryRequestsTable::S_VALIDATING,
                    DeliveryRequestsTable::S_ACCEPTED
                );
                $derogation = $deliveryRequest['derogation'];
            }

            $originalCount = $this->binariesCountFromDescription($archiveUnitIdentifier);
            $originalSize = $this->binariesSizeFromDescription($archiveUnitIdentifier);

            if (empty($this->sessionData[self::MESSAGES_INDICATORS][$messageKey])) {
                $this->sessionData[self::MESSAGES_INDICATORS][$messageKey] = [
                    'message_date' => $eventDateTimeNode->nodeValue,
                    'message_type' => MessageIndicatorsTable::DELIVERY,
                    'message_count' => 1,
                    'accepted' => true,
                    'derogation' => $derogation,
                    'validation_duration' => $validationDuration,
                    'archival_agency_id' => $this->archive['archival_agency_id'],
                    'transferring_agency_id' => $this->archive['transferring_agency_id'],
                    'originating_agency_id' => $this->archive['originating_agency_id'],
                    'original_files_count' => $originalCount,
                    'original_files_size' => $originalSize,
                    'agreement_id' => $this->archive['agreement_id'],
                    'profile_id' => $this->archive['profile_id'],
                ];
            } else {
                $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_count']
                    += $originalCount;
                $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_size']
                    += $originalSize;
            }
        }
    }

    /**
     * demandes de communication rejetées
     * @param int $deliveryRequestId
     * @return void
     */
    private function indicatorsMessageRejectedDeliveryRequests(int $deliveryRequestId): void
    {
        $deliveryRequest = $this->fetchTable('DeliveryRequests')->find()
            ->leftJoinWith('ArchiveUnits')
            ->where(['DeliveryRequests.id' => $deliveryRequestId])
            ->contain(['ArchiveUnits'])
            ->disableHydration()
            ->firstOrFail();

        if (!$deliveryRequest['archive_units']) {
            // warning si ['archive_units'] est vide et on sort sans rien faire
            $this->io->warning(
                __(
                    "  - les unités d'archives de la demande de communication {0} ne sont plus accessibles",
                    $deliveryRequest['identifier']
                )
            );
        } else {
            foreach ($deliveryRequest['archive_units'] as $archiveUnit) {
                $this->archive = $this->fetchTable('Archives')->find()
                    ->where(['id' => $archiveUnit['archive_id']])
                    ->disableHydration()
                    ->firstOrFail();
                $messageKey = implode(
                    self::KEY_SEPARATOR,
                    [
                        MessageIndicatorsTable::DELIVERY,
                        $deliveryRequest['archival_agency_id'],
                        $deliveryRequest['identifier'],
                        $this->archive['transferring_agency_id'],
                        $this->archive['originating_agency_id'],
                        $this->archive['agreement_id'],
                        $this->archive['profile_id'],
                    ]
                );

                if (empty($this->sessionData[self::MESSAGES_INDICATORS][$messageKey])) {
                    $this->sessionData[self::MESSAGES_INDICATORS][$messageKey] = [
                        'message_date' => $deliveryRequest['last_state_update'],
                        'message_type' => MessageIndicatorsTable::DELIVERY,
                        'message_count' => 1,
                        'accepted' => false,
                        'derogation' => $deliveryRequest['derogation'],
                        'validation_duration' => $this->durationBetweenTwoStates(
                            $deliveryRequest['states_history'],
                            DeliveryRequestsTable::S_VALIDATING,
                            DeliveryRequestsTable::S_REJECTED
                        ),
                        'archival_agency_id' => $deliveryRequest['archival_agency_id'],
                        'transferring_agency_id' => $this->archive['transferring_agency_id'],
                        'originating_agency_id' => $this->archive['originating_agency_id'],
                        'original_files_count' => $archiveUnit['original_total_count'],
                        'original_files_size' => $archiveUnit['original_total_size'],
                        'agreement_id' => $this->archive['agreement_id'],
                        'profile_id' => $this->archive['profile_id'],
                    ];
                } else {
                    $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_count']
                        += $archiveUnit['original_total_count'];
                    $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_size']
                        += $archiveUnit['original_total_size'];
                }
            }
        }
    }

    /**
     * demandes d'élimination rejetées
     * @param int $destructionRequestId
     * @return void
     */
    private function indicatorsMessageRejectedDestructionRequests(int $destructionRequestId): void
    {
        $destructionRequest = $this->fetchTable('DestructionRequests')->find()
            ->leftJoinWith('ArchiveUnits')
            ->where(['DestructionRequests.id' => $destructionRequestId])
            ->contain(['ArchiveUnits'])
            ->disableHydration()
            ->firstOrFail();

        if (!$destructionRequest['archive_units']) {
            // warning si ['archive_units'] est vide et on sort sans rien faire
            $this->io->warning(
                __(
                    "  - les unités d'archives de la demande d'élimination {0} ne sont plus accessibles",
                    $destructionRequest['identifier']
                )
            );
        } else {
            foreach ($destructionRequest['archive_units'] as $archiveUnit) {
                $this->archive = $this->fetchTable('Archives')->find()
                    ->where(['id' => $archiveUnit['archive_id']])
                    ->disableHydration()
                    ->firstOrFail();
                $messageKey = implode(
                    self::KEY_SEPARATOR,
                    [
                        MessageIndicatorsTable::DESTRUCTION,
                        $destructionRequest['archival_agency_id'],
                        $destructionRequest['identifier'],
                        $this->archive['transferring_agency_id'],
                        $this->archive['originating_agency_id'],
                        $this->archive['agreement_id'],
                        $this->archive['profile_id'],
                    ]
                );

                if (empty($this->sessionData[self::MESSAGES_INDICATORS][$messageKey])) {
                    $this->sessionData[self::MESSAGES_INDICATORS][$messageKey] = [
                        'message_date' => $destructionRequest['last_state_update'],
                        'message_type' => MessageIndicatorsTable::DESTRUCTION,
                        'message_count' => 1,
                        'accepted' => false,
                        'derogation' => false,
                        'validation_duration' => $this->durationBetweenTwoStates(
                            $destructionRequest['states_history'],
                            DestructionRequestsTable::S_VALIDATING,
                            DestructionRequestsTable::S_REJECTED
                        ),
                        'archival_agency_id' => $destructionRequest['archival_agency_id'],
                        'transferring_agency_id' => $this->archive['transferring_agency_id'],
                        'originating_agency_id' => $this->archive['originating_agency_id'],
                        'original_files_count' => $archiveUnit['original_total_count'],
                        'original_files_size' => $archiveUnit['original_total_size'],
                        'agreement_id' => $this->archive['agreement_id'],
                        'profile_id' => $this->archive['profile_id'],
                    ];
                } else {
                    $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_count']
                        += $archiveUnit['original_total_count'];
                    $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_size']
                        += $archiveUnit['original_total_size'];
                }
            }
        }
    }

    /**
     * demandes de restitution rejetées
     * @param int $restitutionRequestId
     * @return void
     */
    private function indicatorsMessageRejectedRestitutionRequests(int $restitutionRequestId): void
    {
        $restitutionRequest = $this->fetchTable('RestitutionRequests')->find()
            ->leftJoinWith('Archives')
            ->where(['RestitutionRequests.id' => $restitutionRequestId])
            ->contain(['Archives'])
            ->disableHydration()
            ->firstOrFail();

        foreach ($restitutionRequest['archives'] as $this->archive) {
            $messageKey = implode(
                self::KEY_SEPARATOR,
                [
                    MessageIndicatorsTable::RESTITUTION,
                    $restitutionRequest['archival_agency_id'],
                    $restitutionRequest['identifier'],
                    $this->archive['transferring_agency_id'],
                    $this->archive['originating_agency_id'],
                    $this->archive['agreement_id'],
                    $this->archive['profile_id'],
                ]
            );

            if (empty($this->sessionData[self::MESSAGES_INDICATORS][$messageKey])) {
                $this->sessionData[self::MESSAGES_INDICATORS][$messageKey] = [
                    'message_date' => $restitutionRequest['last_state_update'],
                    'message_type' => MessageIndicatorsTable::RESTITUTION,
                    'message_count' => 1,
                    'accepted' => false,
                    'derogation' => false,
                    'validation_duration' => $this->durationBetweenTwoStates(
                        $restitutionRequest['states_history'],
                        RestitutionRequestsTable::S_VALIDATING,
                        RestitutionRequestsTable::S_REJECTED
                    ),
                    'archival_agency_id' => $restitutionRequest['archival_agency_id'],
                    'transferring_agency_id' => $this->archive['transferring_agency_id'],
                    'originating_agency_id' => $this->archive['originating_agency_id'],
                    'original_files_count' => $this->archive['original_count'],
                    'original_files_size' => $this->archive['original_size'],
                    'agreement_id' => $this->archive['agreement_id'],
                    'profile_id' => $this->archive['profile_id'],
                ];
            } else {
                $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_count']
                    += $this->archive['original_count'];
                $this->sessionData[self::MESSAGES_INDICATORS][$messageKey]['original_files_size']
                    += $this->archive['original_size'];
            }
        }
    }

    /**
     * transferts sortants rejetés
     * @param int $transferId
     * @return void
     */
    private function indicatorsMessageOutTransfers(int $transferId): void
    {
        $outTransfer = $this->fetchTable('OutgoingTransfers')->find()
            ->leftJoinWith('ArchiveUnits')
            ->where(['OutgoingTransfers.id' => $transferId])
            ->contain(['ArchiveUnits.Archives'])
            ->disableHydration()
            ->firstOrFail();

        $messageKey = implode(
            self::KEY_SEPARATOR,
            [
                MessageIndicatorsTable::OUT_TRANSFER,
                $outTransfer['archive_unit']['archive']['archival_agency_id'],
                $outTransfer['identifier'],
            ]
        );

        $this->sessionData[self::MESSAGES_INDICATORS][$messageKey] = [
            'message_date' => $outTransfer['created'],
            'message_type' => MessageIndicatorsTable::OUT_TRANSFER,
            'message_count' => 1,
            'accepted' => false,
            'derogation' => false,
            'validation_duration' => $this->durationBetweenTwoStates(
                $outTransfer['states_history'],
                OutgoingTransfersTable::S_SENT,
                OutgoingTransfersTable::S_REJECTED
            ),
            'archival_agency_id' => $outTransfer['archive_unit']['archive']['archival_agency_id'],
            'transferring_agency_id' => $outTransfer['archive_unit']['archive']['transferring_agency_id'],
            'originating_agency_id' => $outTransfer['archive_unit']['archive']['originating_agency_id'],
            'original_files_count' => $outTransfer['archive_unit']['archive']['original_count'],
            'original_files_size' => $outTransfer['archive_unit']['archive']['original_size'],
            'agreement_id' => $outTransfer['archive_unit']['archive']['agreement_id'],
            'profile_id' => $outTransfer['archive_unit']['archive']['profile_id'],
        ];
    }
}
