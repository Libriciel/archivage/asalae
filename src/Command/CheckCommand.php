<?php

/**
 * Asalae\Command\CheckCommand
 */

namespace Asalae\Command;

use Asalae\Utility\Check as CheckUtility;
use AsalaeCore\Factory\Utility;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;

/**
 * Vérification de l'application
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CheckCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'check';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * @param ConsoleOptionParser $parser Option parser to update.
     * @return ConsoleOptionParser
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription(
            __("Permet de savoir si l'application est en état de fonctionner")
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        /** @var CheckUtility $Check */
        $Check = Utility::get(CheckUtility::class);
        $missings = $Check->missings();
        if ($missings) {
            $io->error('missing binaries: ' . implode(', ', $missings));
        }
        $missingsExtPhp = $Check->missingsExtPhp();
        if ($missingsExtPhp) {
            $io->error('missing ext-php: ' . implode(', ', $missingsExtPhp));
        }
        $database = $Check->database();
        if (!$database) {
            $io->error('unable to connect to database');
        }
        $beanstalkd = $Check->beanstalkd();
        if (!$beanstalkd) {
            $io->error('unable to connect to beanstalkd');
        }
        $ratchet = $Check->ratchet();
        if (!$ratchet) {
            $io->error('unable to connect to ratchet');
        }
        $clamav = $Check->clamav();
        if (!$clamav) {
            $io->error('clamav failed to scan file');
        }
        $php = $Check->phpOk();
        if (!$php) {
            $io->error('bad php configuration');
        }
        $cron = $Check->cronOk();
        if (!$cron) {
            $io->error('no recent activity detected for cron jobs');
        }
        $systemOk = empty($missings) && empty($missingsExtPhp)
            && $database && $ratchet && $clamav && $beanstalkd && $php && $cron;
        if ($systemOk) {
            $io->success('OK');
        }
        $this->abort($systemOk ? self::CODE_SUCCESS : self::CODE_ERROR);
    }
}
