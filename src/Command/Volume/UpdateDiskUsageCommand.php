<?php

/**
 * Asalae\Command\Volume\UpdateDiskUsageCommand
 */

namespace Asalae\Command\Volume;

use Asalae\Model\Table\VolumesTable;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;

/**
 * Met à jour le disk_usage
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UpdateDiskUsageCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume update_disk_usage';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserUdu = new ConsoleOptionParser();
        $parserUdu->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Met à jour le disk_usage d'un volume") . PHP_EOL
            . '*************************************************************'
        );
        $parserUdu->addArgument(
            'volume_id',
            [
                'help' => __("Identifiant du volume"),
                'required' => true,
            ]
        );
        return $parserUdu;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $volume_id = $args->getArgument('volume_id');
        /** @var VolumesTable $Volumes */
        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        $volume = $Volumes->get($volume_id);
        $before = $volume->get('disk_usage');
        $Volumes->updateDiskUsage($volume_id);
        $volume = $Volumes->get($volume_id);
        $this->io->out(
            __(
                "Volume id={0} mis à jour. {1} -> {2}",
                $volume_id,
                Number::toReadableSize($before),
                Number::toReadableSize($volume->get('disk_usage'))
            )
        );
    }
}
