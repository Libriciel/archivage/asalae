<?php

/**
 * Asalae\Command\Volume\ListCommand
 */

namespace Asalae\Command\Volume;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;

/**
 * Donne la liste des volumes
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ListCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume list';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Donne la liste des volumes") . PHP_EOL
            . '*************************************************************'
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $fields = [
            'id',
            'name',
            'driver',
            'active',
            'disk_usage',
            'max_disk_usage',
            'secure_data_space',
        ];
        $data = TableRegistry::getTableLocator()->get('Volumes')->find()
            ->select(
                [
                    'Volumes.id',
                    'Volumes.name',
                    'Volumes.driver',
                    'Volumes.active',
                    'Volumes.disk_usage',
                    'Volumes.max_disk_usage',
                    'secure_data_space' => 'SecureDataSpaces.name',
                ]
            )
            ->orderBy(['Volumes.name' => 'asc'])
            ->contain(['SecureDataSpaces'])
            ->disableHydration()
            ->all()
            ->map(
                function ($data) {
                    if ($data['driver'] === 'MINIO') {
                        $data['driver'] = 'S3 - MINIO';
                    }
                    return array_map('strval', $data);
                }
            )
            ->toArray();
        array_unshift($data, $fields);

        $this->io->helper('Table')->output($data);
    }
}
