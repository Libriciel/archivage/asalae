<?php

/**
 * Asalae\Command\Volume\DownloadCommand
 */

namespace Asalae\Command\Volume;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Exception\StopException;

/**
 * Télécharge un fichier
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DownloadCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume download';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserDownload = new ConsoleOptionParser();

        $parserDownload->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Télécharge un fichier") . PHP_EOL
            . '*************************************************************'
        );

        $parserDownload->addArgument(
            'volume_id',
            [
                'help' => __("Identifiant du volume"),
                'required' => true,
            ]
        );
        $parserDownload->addArgument(
            'storage_path',
            [
                'help' => __("Chemin relatif vers le fichier"),
                'required' => true,
            ]
        );
        $parserDownload->addArgument(
            'destination',
            [
                'help' => __("Chemin absolu vers le fichier de destination"),
                'required' => true,
            ]
        );
        $parserDownload->addOption(
            'overwrite',
            [
                'short' => 'o',
                'boolean' => true,
                'help' => __(
                    "Écrase le fichier si un fichier du même nom existe"
                ),
            ]
        );
        return $parserDownload;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $volume_id = $args->getArgument('volume_id');
        $storagePath = $args->getArgument('storage_path');
        $destination = $args->getArgument('destination');
        if (is_file($destination)) {
            if ($this->args->getOption('overwrite')) {
                unlink($destination);
            } else {
                $this->io->warning($msg = __("Un fichier du même nom existe déjà"));
                throw new StopException($msg, self::CODE_ERROR);
            }
        }

        $driver = VolumeManager::getDriverById($volume_id);
        $driver->fileDownload($storagePath, $destination);
        if (is_file($destination)) {
            $this->io->success(__("Le fichier a été téléchargé avec succès"));
        } else {
            $this->io->warning($msg = __("Erreur lors du téléchargement"));
            throw new StopException($msg, self::CODE_ERROR);
        }
    }
}
