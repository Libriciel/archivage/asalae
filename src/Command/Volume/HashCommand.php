<?php

/**
 * Asalae\Command\Volume\HashCommand
 */

namespace Asalae\Command\Volume;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

/**
 * Donne le hash d'un fichier
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class HashCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume hash';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserHash = new ConsoleOptionParser();
        $parserHash->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Donne le hash d'un fichier") . PHP_EOL
            . '*************************************************************'
        );
        $parserHash->addArgument(
            'volume_id',
            [
                'help' => __("Identifiant du volume"),
                'required' => true,
            ]
        );
        $parserHash->addArgument(
            'storage_path',
            [
                'help' => __("Chemin relatif vers le fichier"),
                'required' => true,
            ]
        );
        $parserHash->addArgument(
            'hash_algo',
            [
                'help' => __("Algorithme de hashage"),
                'required' => false,
            ]
        );
        $parserHash->addOption(
            'base64',
            [
                'short' => 'b',
                'boolean' => true,
                'help' => __("Donne le hash sous format base64"),
            ]
        );
        return $parserHash;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $volume_id = $args->getArgument('volume_id');
        $storagePath = $args->getArgument('storage_path');
        $hash_algo = $args->getArgument('hash_algo') ?: 'sha256';
        $driver = VolumeManager::getDriverById($volume_id);
        $hash = $driver->hash($storagePath, $hash_algo);
        if ($this->args->getOption('base64')) {
            $hash = base64_encode(hex2bin($hash));
        }
        $this->io->out($hash);
    }
}
