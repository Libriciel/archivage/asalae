<?php

/**
 * Asalae\Command\Volume\DeleteCommand
 */

namespace Asalae\Command\Volume;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Driver\Volume\VolumeInterface;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Exception\StopException;
use Cake\ORM\TableRegistry;

/**
 * Supprime un fichier
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DeleteCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume delete';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserDelete = new ConsoleOptionParser();
        $parserDelete->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Supprime un fichier sur le volume") . PHP_EOL
            . '*************************************************************'
        );
        $parserDelete->addArgument(
            'volume_id',
            [
                'help' => __("Identifiant du volume"),
                'required' => true,
            ]
        );
        $parserDelete->addArgument(
            'storage_path',
            [
                'help' => __("Chemin relatif vers le fichier"),
                'required' => true,
            ]
        );
        $parserDelete->addOption(
            'force',
            [
                'short' => 'f',
                'boolean' => true,
                'help' => __(
                    "Permet la suppression du fichier même si une référence existe en base de données"
                ),
            ]
        );
        return $parserDelete;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $volume_id = $args->getArgument('volume_id');
        $storagePath = $args->getArgument('storage_path');
        $StoredFilesVolumes = TableRegistry::getTableLocator()->get(
            'StoredFilesVolumes'
        );
        if (
            !$this->args->getOption('force')
            && $StoredFilesVolumes->exists(
                ['volume_id' => $volume_id, 'storage_ref' => $storagePath]
            )
        ) {
            $msg = __(
                "Une référence du fichier existe encore en base (utiliser --force pour passer outre)"
            );
            $this->io->warning($msg);
            throw new StopException($msg, self::CODE_ERROR);
        }
        /** @var VolumeInterface|VolumeFilesystem $driver */
        $driver = VolumeManager::getDriverById($volume_id);
        $driver->fileDelete($storagePath);
        if (!$driver->fileExists($storagePath)) {
            $this->io->success(__("Le fichier a été supprimé avec succès"));
        } else {
            $this->io->warning($msg = __("Erreur lors de la suppression du fichier"));
            throw new StopException($msg, self::CODE_ERROR);
        }
    }
}
