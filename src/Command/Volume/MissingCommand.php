<?php

/**
 * Asalae\Command\Volume\MissingCommand
 */

namespace Asalae\Command\Volume;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeInterface;
use Cake\Command\Command;
use Cake\Command\Helper\ProgressHelper;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;

/**
 * Recherche des fichiers manquants sur le volume
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MissingCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume missing';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserMissings = new ConsoleOptionParser();
        $parserMissings->setDescription(
            '*************************************************************' . PHP_EOL
            . __(
                "Recherche d'anomalies entre la base de données et le volume"
            ) . PHP_EOL
            . '*************************************************************'
        );
        $parserMissings->addArgument(
            'volume_id',
            [
                'help' => __("Identifiant du volume"),
                'required' => false,
            ]
        );
        return $parserMissings;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $volume_id = $args->getArgument('volume_id');
        $StoredFilesVolumes = TableRegistry::getTableLocator()->get(
            'StoredFilesVolumes'
        );
        $query = $StoredFilesVolumes->find();
        if ($volume_id) {
            $query->where(['volume_id' => $volume_id]);
        } else {
            $query->where(['Volumes.active IS' => true]);
            $query->innerJoinWith('Volumes');
        }
        /** @var ProgressHelper $progress */
        $progress = $this->io->helper('Progress');
        $progress->init(['total' => $query->count()]);
        /** @var VolumeInterface[] $drivers */
        $drivers = [];
        $errors = [];
        /** @var EntityInterface $storedFileVolume */
        foreach ($query as $i => $storedFileVolume) {
            $volume_id = $storedFileVolume->get('volume_id');
            if (!isset($drivers[$volume_id])) {
                $drivers[$volume_id] = VolumeManager::getDriverById($volume_id);
            }
            if (
                !$drivers[$volume_id]->fileExists(
                    $storedFileVolume->get('storage_ref')
                )
            ) {
                $errors[]
                    = __(
                        "Erreur sur StoredFilesVolumes id={0} / Volume id={1} / reference={2}",
                        $storedFileVolume->id,
                        $volume_id,
                        $storedFileVolume->get('storage_ref')
                    );
            }
            $progress->increment();
            if ($i % 10 === 0) {
                $progress->draw();
            }
        }
        $this->io->out();
        array_map([$this->io, 'warning'], $errors);
    }
}
