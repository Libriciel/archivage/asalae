<?php

/**
 * Asalae\Command\Volume\UploadCommand
 */

namespace Asalae\Command\Volume;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Exception\StopException;

/**
 * Upload un fichier
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UploadCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume upload';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserUpload = new ConsoleOptionParser();
        $parserUpload->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Upload un fichier") . PHP_EOL
            . '*************************************************************'
        );
        $parserUpload->addArgument(
            'volume_id',
            [
                'help' => __("Identifiant du volume"),
                'required' => true,
            ]
        );
        $parserUpload->addArgument(
            'upload_file',
            [
                'help' => __("Chemin absolu vers le fichier à uploader"),
                'required' => true,
            ]
        );
        $parserUpload->addArgument(
            'storage_path',
            [
                'help' => __("Chemin relatif vers le fichier"),
                'required' => true,
            ]
        );
        return $parserUpload;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $volume_id = $args->getArgument('volume_id');
        $uploadFile = $args->getArgument('upload_file');
        $storagePath = $args->getArgument('storage_path');
        if (!is_file($uploadFile)) {
            $this->io->warning($msg = __("Le fichier à uploader n'existe pas"));
            throw new StopException($msg, self::CODE_ERROR);
        }
        $driver = VolumeManager::getDriverById($volume_id);
        $driver->fileUpload($uploadFile, $storagePath);
        if ($driver->fileExists($storagePath)) {
            $this->io->success(__("Le fichier a été uploadé avec succès"));
        } else {
            $this->io->warning($msg = __("Erreur lors de l'upload"));
            throw new StopException($msg, self::CODE_ERROR);
        }
    }
}
