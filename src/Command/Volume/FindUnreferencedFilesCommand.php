<?php

/**
 * Asalae\Command\Volume\FindUnreferencedFilesCommand
 */

namespace Asalae\Command\Volume;

use Asalae\Model\Table\VolumesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Driver\Volume\VolumeInterface;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;

/**
 * Trouve les fichiers présent sur volume mais pas en BDD
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FindUnreferencedFilesCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume find_unreferenced_files';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserFUF = new ConsoleOptionParser();
        $parserFUF->setDescription(
            '*************************************************************' . PHP_EOL
            . __(
                "Trouve les fichiers présents sur volume mais pas en BDD"
            ) . PHP_EOL
            . '*************************************************************'
        );
        $parserFUF->addArgument(
            'volume_id',
            [
                'help' => __("Identifiant du volume"),
                'required' => true,
            ]
        );
        $parserFUF->addOption(
            'delete',
            [
                'help' => __("Supprime les fichiers sans référence"),
                'boolean' => true,
            ]
        );
        return $parserFUF;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $volume_id = $args->getArgument('volume_id');
        /** @var VolumesTable $Volumes */
        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        $refs = $Volumes->findUnreferencedFiles($volume_id);
        if ($this->args->getOption('delete')) {
            /** @var VolumeInterface|VolumeFilesystem $driver */
            $driver = VolumeManager::getDriverById($volume_id);
            foreach ($refs as $ref) {
                $driver->fileDelete($ref);
                $this->io->out(__("Supprimé: {0}", $ref));
            }
        } else {
            foreach ($refs as $ref) {
                $this->io->out($ref);
            }
        }
    }
}
