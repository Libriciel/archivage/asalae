<?php

/**
 * Asalae\Command\Volume\ExistsCommand
 */

namespace Asalae\Command\Volume;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Exception\StopException;

/**
 * Vérifi l'existance d'un fichier
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ExistsCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume exists';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserExists = new ConsoleOptionParser();

        $parserExists->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Vérifie l’existence d'un fichier") . PHP_EOL
            . '*************************************************************'
        );
        $parserExists->addArgument(
            'volume_id',
            [
                'help' => __("Identifiant du volume"),
                'required' => true,
            ]
        );
        $parserExists->addArgument(
            'storage_path',
            [
                'help' => __("Chemin relatif vers le fichier"),
                'required' => true,
            ]
        );
        return $parserExists;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $volume_id = $args->getArgument('volume_id');
        $storagePath = $args->getArgument('storage_path');
        $driver = VolumeManager::getDriverById($volume_id);
        if ($driver->fileExists($storagePath)) {
            $this->io->success(__("Le fichier existe"));
        } else {
            $this->io->warning($msg = __("Le fichier n'existe pas"));
            throw new StopException($msg, self::CODE_ERROR);
        }
    }
}
