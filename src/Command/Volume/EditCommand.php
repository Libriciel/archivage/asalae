<?php

/**
 * Asalae\Command\Volume\EditCommand
 */

namespace Asalae\Command\Volume;

use Asalae\Controller\AdminsController;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Utility\Security;

/**
 * Modification du champ json "fields"
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EditCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume edit';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserEdit = new ConsoleOptionParser();
        $parserEdit->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Permet de modifier les champs protégés 'fields'") . PHP_EOL
            . '*************************************************************'
        );
        $parserEdit->addArgument(
            'volume_id',
            [
                'help' => __("Identifiant du volume"),
                'required' => true,
            ]
        );
        $parserEdit->addOption(
            'data',
            [
                'help' => __("Champ fields au format json"),
            ]
        );
        return $parserEdit;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $volume_id = $args->getArgument('volume_id');
        $volume = $this->fetchTable('Volumes')->get($volume_id);
        $decryptKey = hash('sha256', AdminsController::DECRYPT_KEY);
        $driver = $volume->get('driver');
        $configured = Configure::read(
            'Volumes.drivers.' . $driver . '.fields',
            []
        );
        $passwordFields = array_keys(
            array_filter($configured, fn($v) => ($v['type'] ?? '') === 'password')
        );

        if ($this->args->getOption('data')) {
            $data = json_decode($this->args->getOption('data'), true);
            if (!$data) {
                $this->io->abort(__("data n'est pas un json valide"));
            }
        } else {
            $currentData = json_decode($volume->get('fields'), true);
            foreach ($passwordFields as $passwordField) {
                if (!empty($currentData[$passwordField])) {
                    $currentData[$passwordField] = Security::decrypt(
                        base64_decode($currentData[$passwordField]),
                        $decryptKey
                    );
                }
            }
            $data = [];
            foreach ($currentData as $key => $value) {
                $data[$key] = $this->io->ask($key, $value);
            }
        }

        // affiche le data pour une utilisation avec l'option --data
        $json = json_encode($data, JSON_UNESCAPED_SLASHES);
        $this->io->out('<info>' . escapeshellarg($json) . '</info>');

        // chiffrement du mot de passe
        foreach ($passwordFields as $passwordField) {
            if (!empty($data[$passwordField])) {
                $data[$passwordField] = base64_encode(
                    Security::encrypt($data[$passwordField], $decryptKey)
                );
            }
        }

        $volume->set('fields', json_encode($data, JSON_UNESCAPED_SLASHES));
        $this->fetchTable('Volumes')->save($volume);
    }
}
