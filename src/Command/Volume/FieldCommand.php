<?php

/**
 * Asalae\Command\Volume\FieldCommand
 */

namespace Asalae\Command\Volume;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;

/**
 * Affiche un champ unique
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FieldCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume field';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $fields = $this->fetchTable('Volumes')->getSchema()->columns();
        $parserField = new ConsoleOptionParser();
        $parserField->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Affiche un champ unique d'un volume") . PHP_EOL
            . '*************************************************************'
        );
        $parserField->addArgument(
            'volume_id',
            [
                'help' => __("Identifiant du volume"),
                'required' => true,
            ]
        );
        $parserField->addArgument(
            'field',
            [
                'help' => __("Nom du champ à afficher"),
                'required' => true,
                'choices' => $fields,
            ]
        );
        return $parserField;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $volume_id = $args->getArgument('volume_id');
        $field = $args->getArgument('field');
        $this->io->out(
            TableRegistry::getTableLocator()->get('Volumes')
                ->get($volume_id)
                ->get($field)
        );
    }
}
