<?php

/**
 * Asalae\Command\Volume\LsCommand
 */

namespace Asalae\Command\Volume;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

/**
 * Affiche une liste de fichiers par répertoires
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class LsCommand extends Command
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Arguments
     */
    protected Arguments $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'volume ls';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserLs = new ConsoleOptionParser();
        $parserLs->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Affiche une liste de fichiers par répertoires") . PHP_EOL
            . '*************************************************************'
        );
        $parserLs->addArgument(
            'volume_id',
            [
                'help' => __("Identifiant du volume"),
                'required' => true,
            ]
        );
        $parserLs->addArgument(
            'dir',
            [
                'help' => __("Dossier dans le volume"),
                'required' => false,
            ]
        );
        $parserLs->addOption(
            'all-files',
            [
                'help' => __("Donne la liste de tous les fichiers"),
                'boolean' => true,
            ]
        );
        return $parserLs;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $volume_id = $args->getArgument('volume_id');
        $storagePath = $args->getArgument('dir') ?: '/';
        $driver = VolumeManager::getDriverById($volume_id);
        $allFiles = $this->args->getOption('all-files') ?: false;
        foreach ($driver->ls(ltrim($storagePath, '/'), $allFiles) as $file) {
            $this->io->out($file);
        }
    }
}
