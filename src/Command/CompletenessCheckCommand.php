<?php

/**
 * Asalae\Command\CompletenessCheckCommand
 */

namespace Asalae\Command;

use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchivesTransfersTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Table\StoredFilesVolumesTable;
use Asalae\Model\Table\VolumesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeInterface;
use AsalaeCore\Utility\DOMUtility;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Exception\StopException;
use Cake\Datasource\EntityInterface;
use Exception;

/**
 * Effectue le contrôle d'exhaustivité des archives en base de données
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CompletenessCheckCommand extends Command
{
    /**
     * @var Arguments
     */
    public $args;

    /**
     * @var ConsoleIo
     */
    public $io;
    /**
     * @var ArchivesTable
     */
    public $Archives;
    /**
     * @var ArchivesTransfersTable
     */
    public $ArchivesTransfers;
    /**
     * @var ArchiveUnitsTable
     */
    public $ArchiveUnits;
    /**
     * @var ArchiveFilesTable
     */
    public $ArchiveFiles;
    /**
     * @var StoredFilesTable
     */
    public $StoredFiles;
    /**
     * @var StoredFilesVolumesTable
     */
    public $StoredFilesVolumes;
    /**
     * @var VolumesTable
     */
    public $Volumes;
    /**
     * @var ArchiveBinariesTable
     */
    public $ArchiveBinaries;
    /**
     * archive lue en base de données en cours de contrôle
     * @var EntityInterface
     */
    public $archive;
    /**
     * gestionnaire de l'espace de conservation sécurisé de l'archive en cours de contrôle
     * @var VolumeManager
     */
    public $volumeManager;
    /**
     * utilitaire du dom de la description de l'archive en cours de contrôle
     * @var DOMUtility
     */
    public $descriptionDomUtility;
    /**
     * tableau des noeuds des ua éliminées dans le fichiers de description de l'archive en cours de contrôle
     * @var array
     */
    public $deletedNodes;
    /**
     * tableau de décompte des fichiers de management contrôlés par type
     * @var array
     */
    public $managementFilesCounts = [
        'transfer' => 0,
        'description' => 0,
        'lifecycle' => 0,
        'pubdesc' => 0,
        'deleted' => 0,
        'archiving_certificate' => 0,
    ];
    /**
     * tableau de définition des fichiers de management des archives
     * @var array
     */
    public $managementFilesDefinitions;
    /**
     * tableau de décompte des fichiers des binaires contrôlés par type
     * @var array
     */
    public $binariesFilesCounts = [
        'original_data' => 0,
        'original_timestamp' => 0,
        'preservation_data' => 0,
        'preservation_timestamp' => 0,
        'dissemination_data' => 0,
        'dissemination_timestamp' => 0,
    ];
    /**
     * tableau de définition des fichiers des binaires des archives
     * @var array
     */
    public $binariesFilesDefinitions;
    /**
     * @var VolumeManager[]
     */
    private $volumeManagers = [];
    /**
     * @var VolumeInterface[]
     */
    private $volumeDrivers = [];
    /**
     * états finaux des archives dont les fichiers et le UA ont été supprimés
     * @var array
     */
    private $archiveDeletedStates = [
        ArchivesTable::S_DESTROYED,
        ArchivesTable::S_RESTITUTED,
        ArchivesTable::S_TRANSFERRED,
    ];

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->managementFilesDefinitions = [
            'transfer' => [
                'info' => __("du transfert"),
                'required' => true,
                'required_for_states' => [],
            ],
            'description' => [
                'info' => __("de description"),
                'required' => true,
                'required_for_states' => [],
            ],
            'lifecycle' => [
                'info' => __("du cycle de vie"),
                'required' => true,
                'required_for_states' => [],
            ],
            'pubdesc' => [
                'info' => __("de description publique"),
                'required' => false,
                'required_for_states' => [],
            ],
            'deleted' => [
                'info' => __("des unités d'archives supprimées"),
                'required' => false,
                'required_for_states' => [ArchivesTable::S_DESTROYED],
            ],
            'archiving_certificate' => [
                'info' => __("de l'attestation d'archivage"),
                'required' => false,
                'required_for_states' => [],
            ],
        ];

        $this->binariesFilesDefinitions = [
            'original_data' => [
                'info' => __("original"),
                'required' => true,
            ],
            'original_timestamp' => [
                'info' => __("d'horodatage du fichier original"),
                'required' => false,
            ],
            'preservation_data' => [
                'info' => __("de conversion de conservation"),
                'required' => false,
            ],
            'preservation_timestamp' => [
                'info' => __("d'horodatage du fichier de conversion de conservation"),
                'required' => false,
            ],
            'dissemination_data' => [
                'info' => __("de conversion de diffusion"),
                'required' => false,
            ],
            'dissemination_timestamp' => [
                'info' => __("d'horodatage du fichier de conversion de diffusion"),
                'required' => false,
            ],
        ];

        // Initialisations des tables
        $this->Archives = $this->fetchTable('Archives');
        $this->ArchivesTransfers = $this->fetchTable('ArchivesTransfers');
        $this->ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $this->ArchiveFiles = $this->fetchTable('ArchiveFiles');
        $this->StoredFiles = $this->fetchTable('StoredFiles');
        $this->StoredFilesVolumes = $this->fetchTable('StoredFilesVolumes');
        $this->Volumes = $this->fetchTable('Volumes');
        $this->ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
    }

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'completeness_check';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __(
                "Effectue le contrôle d'exhaustivité des archives en base de données "
                . "et en option, vérifie la présence des fichiers sur les volumes de stockage"
            )
        );
        $parser->addOption(
            'stop-at-first-error',
            [
                'help' => __("arrête l'analyse à la première erreur détectée"),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'file-exists',
            [
                'help' => __("vérifie la présence des fichiers sur les volumes de stockage"),
                'boolean' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws VolumeException
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        $this->args = $args;

        $excludedArchiveStates = [
            ArchivesTable::S_CREATING,
            ArchivesTable::S_DESCRIBING,
            ArchivesTable::S_STORING,
            ArchivesTable::S_MANAGING,
        ];
        $archivesQuery = $this->Archives->find()
            ->where(
                [
                    'state NOT IN' => $excludedArchiveStates,
                ]
            )
            ->orderBy('id');

        $io->out(__("Contrôle d'exhaustivité des archives en base de données"));

        // lecture du nombre total d'archives
        $archivesCount = $archivesQuery->count();
        if (!$archivesCount) {
            $io->abort(__("Aucune archive en base de données") . "\n");
        }
        $io->out();
        $io->out(__("Nombre d'archives à contrôler : {0}", $archivesCount));
        $io->out();

        // parcours des archives
        foreach ($archivesQuery as $i => $this->archive) {
            $this->io->verbose(
                __(
                    "  {0}/{1} : archive {2} (id {3})",
                    $i + 1,
                    $archivesCount,
                    $this->archive->get('archival_agency_identifier'),
                    $this->archive->get('id')
                )
            );

            // initialisation du volumemanager
            $this->volumeManager = $this->getVolumeManager($this->archive->get('secure_data_space_id'));

            // vérification des fichiers de management
            $this->checkManagementFiles();
            $this->checkLinkedTransfertsManagementFiles();

            // initialisation pour les comparaisons ua, binaries entre db et filesystem
            $archiveDescriptionContent = $this->getArchiveFileContent('description');
            $this->deletedNodes = json_decode($this->getArchiveFileContent('deleted', false) ?: "{}", true);

            if (!$this->deletedNodes && $archiveDescriptionContent) {
                $this->descriptionDomUtility = DOMUtility::loadXML($archiveDescriptionContent);

                // vérification des unités d'archives en base de données
                $this->checkArchiveUnits();

                // vérification du nombre de binaires en base de données
                $this->checkBinariesOriginalDataCount();
            }

            // vérification du stockage des fichiers des binaires
            $this->checkBinariesStorages();
        }

        // affichage du nombre de fichiers contrôlés
        if ($this->args->getOption('verbose')) {
            $io->hr();
            $io->out(__("Décompte des fichiers de management contrôlés"));
            foreach ($this->managementFilesCounts as $managementFileType => $managementFileCount) {
                $io->out(
                    '  ' . __(
                        "fichiers {0} : {1}",
                        $this->managementFilesDefinitions[$managementFileType]['info'],
                        $managementFileCount
                    )
                );
            }
            $io->out(__("Décompte des fichiers des binaires contrôlés"));
            foreach ($this->binariesFilesCounts as $binariesFileType => $binariesFileCount) {
                $io->out(
                    '  ' . __(
                        "fichiers {0} : {1}",
                        $this->binariesFilesDefinitions[$binariesFileType]['info'],
                        $binariesFileCount
                    )
                );
            }
            $io->out();
        }
        $io->out(__("Contrôle terminé"));
    }

    /**
     * Donne l'ECS avec mise en cache
     * @param int $secure_data_space_id
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(int $secure_data_space_id): VolumeManager
    {
        if (!isset($this->volumeManagers[$secure_data_space_id])) {
            $this->volumeManagers[$secure_data_space_id] = new VolumeManager($secure_data_space_id);
        }
        return $this->volumeManagers[$secure_data_space_id];
    }

    /**
     * vérifie les fichiers de management d'une archive
     * @return void
     * @throws VolumeException
     */
    private function checkManagementFiles()
    {
        foreach ($this->managementFilesDefinitions as $managementFileType => $managementFileDefinition) {
            $archive_file = $this->ArchiveFiles->find()
                ->where(
                    [
                        'archive_id' => $this->archive->get('id'),
                        'type' => $managementFileType,
                    ]
                )
                ->first();
            $archiveFileRequired
                = $managementFileDefinition['required']
                || in_array($this->archive->get('state'), $managementFileDefinition['required_for_states']);
            if ($archiveFileRequired && !$archive_file) {
                $this->io->error(
                    __(
                        "Erreur détectée pour l'archive {0} (id:{1}) :",
                        $this->archive->get('archival_agency_identifier'),
                        $this->archive->get('id')
                    )
                );
                $this->io->error(
                    __(
                        "Le fichier {0} est absent en base de données dans la table archive_files",
                        $managementFileDefinition['info']
                    )
                );
                $errorMessage = "record not found : SELECT * FROM archive_files WHERE archive_id = "
                    . $this->archive->get('id') . " AND type = '" . $managementFileType . "';";
                if ($this->args->getOption('stop-at-first-error')) {
                    $this->io->abort($errorMessage);
                } else {
                    $this->io->error($errorMessage);
                }
            }
            if ($archive_file) {
                $this->checkStoredFile($archive_file->get('stored_file_id'), $managementFileDefinition['info']);
                $this->managementFilesCounts[$managementFileType]++;
            }
        }
    }

    /**
     * @param int    $stored_file_id
     * @param string $file_type_name
     * @return void
     * @throws VolumeException
     */
    private function checkStoredFile(int $stored_file_id, string $file_type_name)
    {
        $stored_file = $this->StoredFiles->find()
            ->where(['id' => $stored_file_id])
            ->first();
        if ($stored_file) {
            $this->checkStoredFileVolume($stored_file_id, $file_type_name);
        } else {
            $this->io->error(
                __(
                    "Erreur détectée pour l'archive {0} (id:{1}) :",
                    $this->archive->get('archival_agency_identifier'),
                    $this->archive->get('id')
                )
            );
            $this->io->error(
                __(
                    "Le fichier {0} est absent en base de données dans la table stored_files",
                    $file_type_name
                )
            );
            $errorMessage = "record not found : SELECT * FROM stored_files WHERE id = " . $stored_file_id . ";";
            if ($this->args->getOption('stop-at-first-error')) {
                $this->io->abort($errorMessage);
            } else {
                $this->io->error($errorMessage);
            }
        }
    }

    /**
     * vérifie les liens de stockage sur chaque volume pour un fichier stocké
     * @param int    $stored_file_id
     * @param string $file_type_name
     * @return void
     * @throws VolumeException
     */
    private function checkStoredFileVolume(int $stored_file_id, string $file_type_name)
    {
        $volumes = $this->Volumes->find()
            ->where(['secure_data_space_id' => $this->archive->get('secure_data_space_id')])
            ->all();
        foreach ($volumes as $volume) {
            $stored_files_volume = $this->StoredFilesVolumes->find()
                ->where(
                    [
                        'stored_file_id' => $stored_file_id,
                        'volume_id' => $volume->get('id'),
                    ]
                )
                ->first();
            if ($stored_files_volume && $this->args->getOption('file-exists')) {
                $this->checkStoredFileVolumeFileExists(
                    $stored_files_volume->get('volume_id'),
                    $stored_files_volume->get('storage_ref'),
                    $file_type_name
                );
            } elseif (!$stored_files_volume) {
                $this->io->error(
                    __(
                        "Erreur détectée pour l'archive {0} (id:{1}) :",
                        $this->archive->get('archival_agency_identifier'),
                        $this->archive->get('id')
                    )
                );
                $this->io->error(
                    __(
                        "La référence de stockage du fichier {0} sur le volume de stockage {1} (id:{2}) "
                        . "est absente en base de données dans la table stored_files_volumes",
                        $file_type_name,
                        $volume->get('name'),
                        $volume->get('id')
                    )
                );
                $errorMessage = "record not found : SELECT * FROM stored_files_volumes WHERE stored_file_id = "
                    . $stored_file_id . " AND volume_id = " . $volume->get('id') . ";";
                if ($this->args->getOption('stop-at-first-error')) {
                    $this->io->abort($errorMessage);
                } else {
                    $this->io->error($errorMessage);
                }
            }
        }
    }

    /**
     * vérifie que le fichier est présent sur le volume (ne vérifie pas son intégrité))
     * @param int    $volume_id
     * @param string $storage_ref
     * @param string $file_type_name
     * @return void
     * @throws VolumeException
     */
    private function checkStoredFileVolumeFileExists(int $volume_id, string $storage_ref, string $file_type_name)
    {
        $volumeDriver = $this->getVolumeDriver($volume_id);
        if (!$volumeDriver->fileExists($storage_ref)) {
            $volume = $this->Volumes->find()
                ->where(['id' => $volume_id])
                ->firstOrFail();
            $this->io->error(
                __(
                    "Erreur détectée pour l'archive {0} (id:{1}) :",
                    $this->archive->get('archival_agency_identifier'),
                    $this->archive->get('id')
                )
            );
            $this->io->error(
                __(
                    "Le fichier {0} n'est pas présent sur le volume de stockage {1} (id:{2}) "
                    . "avec la référence de stockage {3}",
                    $file_type_name,
                    $volume->get('name'),
                    $volume->get('id'),
                    $storage_ref
                )
            );
            $errorMessage = "No such file";
            if ($this->args->getOption('stop-at-first-error')) {
                $this->io->abort($errorMessage);
            } else {
                $this->io->error($errorMessage);
            }
        }
    }

    /**
     * retourne le driver d'un volume par son id avec mise en cache
     * @param int $volume_id
     * @return VolumeInterface
     * @throws VolumeException
     */
    private function getVolumeDriver(int $volume_id): VolumeInterface
    {
        if (!isset($this->volumeDrivers[$volume_id])) {
            $volume = $this->Volumes->find()
                ->where(['id' => $volume_id])
                ->firstOrFail();
            $this->volumeDrivers[$volume_id] = $this->getVolumeManager($volume->get('secure_data_space_id'))
                ->getVolumes()[$volume_id];
        }
        return $this->volumeDrivers[$volume_id];
    }

    /**
     * vérifie les fichiers de management des transferts liés
     * @return void
     * @throws VolumeException
     */
    private function checkLinkedTransfertsManagementFiles()
    {
        // cas des archives composites
        $archivesTransfersCount = $this->ArchivesTransfers->find()
            ->where(
                [
                    'archive_id' => $this->archive->get('id'),
                ]
            )->count();
        for ($i = 2; $i <= $archivesTransfersCount; $i++) {
            $filename = sprintf(
                '%s_transfer_%02d.xml',
                $this->archive->get('archival_agency_identifier'),
                $i
            );
            $archive_file = $this->ArchiveFiles->find()
                ->where(
                    [
                        'archive_id' => $this->archive->get('id'),
                        'type' => 'transfer',
                        'filename' => $filename,
                    ]
                )
                ->first();
            if (!$archive_file) {
                $this->io->error(
                    __(
                        "Erreur détectée pour l'archive {0} (id:{1}) :",
                        $this->archive->get('archival_agency_identifier'),
                        $this->archive->get('id')
                    )
                );
                $this->io->error(
                    __(
                        "Le fichier {0} est absent en base de données dans la table archive_files",
                        __("du transfert lié")
                    )
                );
                $errorMessage = "record not found : SELECT * FROM archive_files WHERE archive_id = "
                    . $this->archive->get('id') . " AND type = 'transfer' AND filename = '" . $filename . "';";
                if ($this->args->getOption('stop-at-first-error')) {
                    $this->io->abort($errorMessage);
                } else {
                    $this->io->error($errorMessage);
                }
            }
            if ($archive_file) {
                $this->checkStoredFile($archive_file->get('stored_file_id'), __("du transfert lié"));
                $this->managementFilesCounts['transfer']++;
            }
        }
    }

    /**
     * retourne le contenu d'un fichier de l'archive en cours de contrôle en fonction de son type
     * @param string $type
     * @param bool   $required
     * @return string|null
     */
    private function getArchiveFileContent(string $type, bool $required = true): ?string
    {
        $fileContent = null;
        try {
            $archiveFile = $this->ArchiveFiles->find()
                ->where(
                    [
                        'archive_id' => $this->archive->get('id'),
                        'type' => $type,
                    ]
                )
                ->firstOrFail();
            $fileContent = $this->volumeManager->get($archiveFile->get('stored_file_id'));
        } catch (Exception) {
            if ($required) {
                $this->io->error(
                    __(
                        "Erreur détectée pour l'archive {0} (id:{1}) :",
                        $this->archive->get('archival_agency_identifier'),
                        $this->archive->get('id')
                    )
                );
                $this->io->error(
                    __(
                        "Lecture du fichier de management de type {0} impossible",
                        $type
                    )
                );
                $errorMessage = "No content found";
                if ($this->args->getOption('stop-at-first-error')) {
                    $this->io->abort($errorMessage);
                } else {
                    $this->io->error($errorMessage);
                }
            }
        }
        return $fileContent;
    }

    /**
     * vérifie les unités d'archives de l'archive courante
     * @return void
     * @throws StopException
     */
    private function checkArchiveUnits()
    {
        // calcul du nombre d'unités d'archives et de documents dans le fichier de description xml
        if (in_array($this->archive->get('state'), $this->archiveDeletedStates)) {
            $archiveDescriptionArchiveUnitsCount = 0;
        } else {
            switch ($this->descriptionDomUtility->namespace) {
                case NAMESPACE_SEDA_02:
                    $archiveDescriptionArchiveUnitsCount
                        = $this->descriptionDomUtility->xpath->query('//ns:Contains')->length
                        + $this->descriptionDomUtility->xpath->query('//ns:Document')->length
                        + 1;
                    break;
                case NAMESPACE_SEDA_10:
                    $archiveDescriptionArchiveUnitsCount
                        = $this->descriptionDomUtility->xpath->query('//ns:ArchiveObject')->length
                        + $this->descriptionDomUtility->xpath->query('//ns:Document')->length
                        + 1;
                    break;
                default:
                    $archiveDescriptionArchiveUnitsCount = $this->descriptionDomUtility->xpath
                        ->query('//ns:ArchiveUnit[not(./ns:ArchiveUnitRefId)]')->length;
            }
        }

        // lecture en base de données du nombre d'unité d'archives
        $archiveUnitsCount = $this->ArchiveUnits->find()->where(['archive_id' => $this->archive->get('id')])->count();

        // comparaison bdd - description xml
        if ($archiveUnitsCount !== $archiveDescriptionArchiveUnitsCount) {
            $this->io->error(
                __(
                    "Erreur détectée pour l'archive {0} (id:{1}) :",
                    $this->archive->get('archival_agency_identifier'),
                    $this->archive->get('id')
                )
            );
            $errorMessage = __(
                "Le nombre d'unités d'archives en base de données ({0}) est différent du nombre " .
                "d'unités d'archives du fichier de description ({1})",
                $archiveUnitsCount,
                $archiveDescriptionArchiveUnitsCount
            );
            if ($this->args->getOption('stop-at-first-error')) {
                $this->io->abort($errorMessage);
            } else {
                $this->io->error($errorMessage);
            }
        }
    }

    /**
     * vérifie le nombre des fichiers binaires des fichiers originaux d'une archive
     * @return void
     * @throws StopException
     */
    private function checkBinariesOriginalDataCount()
    {
        // calcul du nombre de binaires dans le fichier de description
        if (in_array($this->archive->get('state'), $this->archiveDeletedStates)) {
            $archiveDescriptionBinariesCount = 0;
        } else {
            switch ($this->descriptionDomUtility->namespace) {
                case NAMESPACE_SEDA_02:
                case NAMESPACE_SEDA_10:
                    $archiveDescriptionBinariesCount
                        = $this->descriptionDomUtility->xpath->query('//ns:Document')->length;
                    break;
                default:
                    $archiveDescriptionBinariesCount
                        = $this->descriptionDomUtility->xpath->query('//ns:BinaryDataObject')->length;
            }
        }

        // lecture du nombre de fichiers originaux en base de données
        $subquery = $this->ArchiveUnits->find()
            ->select(['ArchiveBinariesArchiveUnits.archive_binary_id'])
            ->innerJoinWith('ArchiveBinariesArchiveUnits')
            ->where(['archive_id' => $this->archive->get('id'),]);
        $originalBinariesCount = $this->ArchiveBinaries->find()
            ->where(
                [
                    'ArchiveBinaries.type' => 'original_data',
                    'ArchiveBinaries.id IN' => $subquery,
                ]
            )
            ->count();

        // comparaison bdd - description xml
        if ($originalBinariesCount !== $archiveDescriptionBinariesCount) {
            $this->io->error(
                __(
                    "Erreur détectée pour l'archive {0} (id:{1}) :",
                    $this->archive->get('archival_agency_identifier'),
                    $this->archive->get('id')
                )
            );
            $errorMessage = __(
                "Le nombre de fichiers binaires originaux en base de données ({0}) est différent " .
                "du nombre des fichiers binaires du fichier de description ({1})",
                $originalBinariesCount,
                $archiveDescriptionBinariesCount
            );
            if ($this->args->getOption('stop-at-first-error')) {
                $this->io->abort($errorMessage);
            } else {
                $this->io->error($errorMessage);
            }
        }
    }

    /**
     * vérifie que les fichiers binaires ont bien un fichier stocké et toutes les références sur le ou les volumes
     * @return void
     * @throws StopException|VolumeException
     */
    private function checkBinariesStorages()
    {
        // lecture des binaires liés aux unités d'archives
        $subquery = $this->ArchiveUnits->find()
            ->select(['ArchiveBinariesArchiveUnits.archive_binary_id'])
            ->innerJoinWith('ArchiveBinariesArchiveUnits')
            ->where(['archive_id' => $this->archive->get('id')]);
        $archiveBinaries = $this->ArchiveBinaries
            ->find()
            ->where(['id IN' => $subquery])
            ->orderBy('id');
        foreach ($archiveBinaries as $archiveBinary) {
            if ($archiveBinary->get('stored_file_id')) {
                $this->checkStoredFile(
                    $archiveBinary->get('stored_file_id'),
                    $this->binariesFilesDefinitions[$archiveBinary->get('type')]['info']
                );
            } else {
                $this->io->error(
                    __(
                        "Erreur détectée pour l'archive {0} (id:{1}) :",
                        $this->archive->get('archival_agency_identifier'),
                        $this->archive->get('id')
                    )
                );
                $this->io->error(
                    __(
                        "L'id de la référence de stockage du fichier {0} est nulle "
                        . "en base de données dans la table archive_binaries",
                        $this->binariesFilesDefinitions[$archiveBinary->get('type')]['info']
                    )
                );
                $errorMessage = "null : SELECT stored_file_id FROM archive_binaries WHERE id = "
                    . $archiveBinary->get('id') . ";";
                if ($this->args->getOption('stop-at-first-error')) {
                    $this->io->abort($errorMessage);
                } else {
                    $this->io->error($errorMessage);
                }
            }
            $this->binariesFilesCounts[$archiveBinary->get('type')]++;
        }
    }
}
