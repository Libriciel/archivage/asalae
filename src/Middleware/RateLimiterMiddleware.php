<?php

/**
 * Asalae\Middleware\RateLimiterMiddleware
 */

namespace Asalae\Middleware;

use Asalae\Model\Table\EventLogsTable;
use AsalaeCore\Middleware\RateLimiterMiddleware as CoreRateLimiterMiddleware;
use AsalaeCore\Utility\Premis;
use Cake\Core\Configure;
use Cake\Http\ServerRequest;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\TableRegistry;
use DateInterval;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Empêche une tentative d'intrusion par brute-force
 * Doit être placé après le middleware d'authentication
 * (les Identifiers garnissent le cache et définissent self::$username et self::$attempt)
 *
 * @category Middleware
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RateLimiterMiddleware extends CoreRateLimiterMiddleware
{
    /**
     * Middleware
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface|void
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $max = Configure::read('Login.attempt_limit.ignore_first_intrusions_warnings', 3);
        if (parent::$username && parent::$consecutiveFailedAttempts > $max) {
            $this->writeEventLog($request);
        }
        return parent::process($request, $handler);
    }

    /**
     * Ecrit en bdd l'evenement de tentative d'intrusion (sauf si cooldown)
     * @param ServerRequestInterface $request
     * @return void
     * @throws Exception
     */
    private function writeEventLog(ServerRequestInterface $request)
    {
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $last = $EventLogs->find()
            ->select(['created'])
            ->where(['type' => 'login_security_alert'])
            ->orderByDesc('created')
            ->disableHydration()
            ->first();
        $last = $last ? $last['created'] : new CakeDateTime('1900-01-01');
        $cooldown = new DateInterval(
            sprintf(
                'PT%dS',
                Configure::read('Login.attempt_limit.log_cooldown', 10)
            )
        );
        if ($last->add($cooldown) > new CakeDateTime()) {
            return;
        }

        $Versions = $loc->get('Versions');
        $app = Configure::read('App.name');
        $version = $Versions->find()
            ->where(['subject' => $app])
            ->orderBy(['id' => 'desc'])
            ->firstOrFail();
        $Users = $loc->get('Users');
        $user = $Users->find()
            ->where(['username' => parent::$username])
            ->first();
        if (!$user) {
            $user = new Premis\Agent('NOT_FOUND', parent::$username);
        }
        $ip = $request instanceof ServerRequest
            ? $request->clientIp()
            : ($request->getServerParams()['REMOTE_ADDR'] ?? '');
        $connectionMessage = __(
            "{0} tentatives de connexions sur l'utilisateur ''{1}'' par l'ip {2}",
            parent::$consecutiveFailedAttempts,
            h(parent::$username),
            $ip
        );
        $entry = $EventLogs->newEntry(
            'login_security_alert',
            'warning',
            $connectionMessage,
            $user,
            $version
        );
        $EventLogs->save($entry);
    }
}
