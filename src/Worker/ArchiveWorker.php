<?php

/**
 * Asalae\Worker\ArchiveWorker
 */

namespace Asalae\Worker;

use Asalae\Exception\GenericException;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\ServiceLevelsTable;
use Asalae\Model\Table\TransfersTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Datasource\EntityInterface;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotImplementedException;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInterval;
use DateInvalidTimeZoneException;
use DOMDocument;
use DOMElement;
use DOMNodeList;
use DOMXPath;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Créer l'archive d'un transfert
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var array
     */
    private static $accessRuleCodes;
    /**
     * @var DOMDocument
     */
    protected $dom;
    /**
     * @var EntityInterface
     */
    private $Transfer;
    /**
     * @var string namespace xml
     */
    private $namespace;
    /**
     * @var DOMXPath
     */
    private $xpath;

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        $this->out(
            __(
                "Création des archives du transfert id={0}",
                $data['transfer_id']
            )
        );
        /** @var TransfersTable $Transfers */
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $this->Transfer = $Transfers->find()
            ->where(['Transfers.id' => $data['transfer_id']])
            ->contain(
                [
                    'ArchivalAgencies',
                    'TransferringAgencies',
                    'ServiceLevels',
                    'Profiles',
                    'Archives',
                ]
            )
            ->first();
        if (!$this->Transfer) {
            $this->out(
                __(
                    "Impossible de trouver le transfert id={0}",
                    $data['transfer_id']
                )
            );
            throw new CantWorkException();
        }
        if ($this->Transfer->get('state') !== TransfersTable::S_ARCHIVING) {
            $this->out(
                __(
                    "Etat du transfert: {0}. Annulation de l'archivage.",
                    $this->Transfer->get('state')
                )
            );
            throw new CantWorkException();
        }
        if (!is_file($this->Transfer->get('xml'))) {
            $this->out(
                __(
                    "Impossible de trouver le bordereau lié au transfert id={0}",
                    $data['transfer_id']
                )
            );
            throw new CantWorkException();
        }
        $this->dom = new DOMDocument();
        $this->dom->load($this->Transfer->get('xml'));
        $this->namespace = $this->dom->documentElement->getAttributeNode(
            'xmlns'
        )->nodeValue;
        $this->xpath = new DOMXPath($this->dom);
        $this->xpath->registerNamespace('ns', $this->namespace);
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
                $nodeArchives = $this->xpath->query(
                    '/ns:ArchiveTransfer/ns:Contains'
                );
                break;
            case NAMESPACE_SEDA_10:
                $nodeArchives = $this->xpath->query(
                    '/ns:ArchiveTransfer/ns:Archive'
                );
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $nodeArchives = $this->xpath->query(
                    '/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata/ns:ArchiveUnit'
                );
                break;
            default:
                throw new NotImplementedException(
                    sprintf(
                        'not implemented for namespace "%s"',
                        $this->namespace
                    )
                );
        }
        if ($this->Transfer->get('archives')) {
            $this->out(
                __n(
                    "Une archive existe déjà pour le transfert {0}.",
                    "Des archives existent déjà pour le transfert {0}.",
                    count($this->Transfer->get('archives')),
                    $this->Transfer->get('id')
                )
            );
            if (
                $nodeArchives->count() === count(
                    $this->Transfer->get('archives')
                )
            ) {
                $this->emit('archive-unit', ['transfer_id' => $data['transfer_id']]);

                throw new CantWorkException();
            }
        }

        $conn = $Transfers->getConnection();
        $success = true;
        $conn->begin();
        $archiveIds = [];

        if ($this->isComposite()) {
            $archive = $this->handleComposites($nodeArchives);
            if ($archive->get('state') !== ArchivesTable::S_UPDATE_CREATING) {
                $conn->rollback();
                $this->out(
                    __(
                        "L'archive est en cours de mise à jour, nouvelle"
                        . " tentative dans 10 minutes avec un nouveau job"
                    )
                );
                $this->emit('archive', $data, 1000, 10 * 60);
            } else {
                $conn->commit();
                $this->emit('archive-unit', $data + ['composite_id' => $archive->id]);
            }
            return;
        }

        $this->deleteTmpFiles();

        /** @var DOMElement $nodeArchive */
        foreach ($nodeArchives as $index => $nodeArchive) {
            $archive = $this->createArchive($nodeArchive, $index);
            $exists = $Archives->find()
                ->innerJoinWith('Transfers')
                ->where(
                    [
                        'Transfers.id' => $this->Transfer->get('id'),
                        'Archives.name' => $archive->get('name'),
                        'Archives.xml_node_tagname' => sprintf(
                            '%s[%d]',
                            $nodeArchive->nodeName,
                            $index + 1
                        ),
                    ]
                )
                ->first();
            if ($exists) {
                $this->out(
                    __(
                        "Une archive avec le nom ''{0}'' existe déjà pour le transfert {1} (id={2})",
                        $archive->get('name'),
                        $this->Transfer->get('transfer_identifier'),
                        $this->Transfer->get('id')
                    )
                );
                $archiveIds[] = $exists->get('id');
                continue;
            }
            if ($Archives->save($archive)) {
                $this->out(
                    __("Archive créée avec l'id {0}", $archive->get('id'))
                );
                $archiveIds[] = $archive->get('id');
            } else {
                $this->out(__("Erreur lors de la sauvegarde de l'archive"));
                $this->out(Debugger::exportVar($archive->getErrors()));
                $success = false;
                break;
            }
        }
        if ($success) {
            foreach ($archiveIds as $archive_id) {
                Utility::get('Notify')->emit(
                    'new_archive',
                    [
                        'id' => $archive_id,
                        'sa_id' => $this->Transfer->get('archival_agency_id'),
                    ]
                );
            }
            $conn->commit();
            $this->emit('archive-unit', $data);
        } else {
            $conn->rollback();
            throw new Exception('failed');
        }
    }

    /**
     * Vrai si c'est une archive de rattachement
     * @return bool
     */
    private function isComposite(): bool
    {
        if (in_array($this->namespace, [NAMESPACE_SEDA_02, NAMESPACE_SEDA_10])) {
            return false;
        }
        $path = 'boolean(//ns:RepositoryArchiveUnitPID)';
        return (bool)$this->xpath->evaluate($path);
    }

    /**
     * Crée un lien entre l'archive composite et le transfert
     * @param DOMNodeList $nodeArchives
     * @return EntityInterface archive lié
     * @throws CantWorkException
     * @throws DateInvalidTimeZoneException
     */
    private function handleComposites(DOMNodeList $nodeArchives): EntityInterface
    {
        $path = 'ns:Content/ns:RelatedObjectReference/ns:IsPartOf/ns:RepositoryArchiveUnitPID';
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');
        $ArchivesTransfers = $loc->get('ArchivesTransfers');
        $nodeArchive = $nodeArchives->item(0);
        $node = $this->xpath->query($path, $nodeArchive)->item(0);
        if (!$node) {
            throw new GenericException("RepositoryArchiveUnitPID not found");
        }
        $value = trim($node->nodeValue);
        if (strpos($value, 'ArchivalAgencyIdentifier:') !== false) {
            $conditions = [
                'ArchiveUnits.archival_agency_identifier' => substr(
                    $value,
                    strlen('ArchivalAgencyIdentifier:')
                ),
            ];
        } elseif (strpos($value, 'TransferringAgencyIdentifier:') !== false) {
            $conditions = [
                'ArchiveUnits.transferring_agency_identifier' => substr(
                    $value,
                    strlen('TransferringAgencyIdentifier:')
                ),
            ];
        } elseif (strpos($value, 'OriginatingAgencyIdentifier:') !== false) {
            $conditions = [
                'ArchiveUnits.originating_agency_identifier' => substr(
                    $value,
                    strlen('OriginatingAgencyIdentifier:')
                ),
            ];
        } else {
            $conditions = [
                'OR' => [
                    'ArchiveUnits.archival_agency_identifier' => $value,
                    'ArchiveUnits.transferring_agency_identifier' => $value,
                    'ArchiveUnits.originating_agency_identifier' => $value,
                ],
            ];
        }
        $conditions['Archives.archival_agency_id'] = Hash::get($this->Transfer, 'archival_agency.id');

        $archive = $Archives->find()
            ->innerJoinWith('ArchiveUnits')
            ->where($conditions)
            ->first();

        if (!$archive) {
            throw new GenericException("The target archive was not found");
        } elseif ($archive->get('state') !== ArchivesTable::S_AVAILABLE) {
            $this->out(
                __(
                    "Etat de l'archive id={0}: {1}. Annulation de l'archivage.",
                    $archive->get('id'),
                    $archive->get('state')
                )
            );
            throw new CantWorkException();
        }
        $ArchivesTransfers->findOrCreate(
            [
                'archive_id' => $archive->id,
                'transfer_id' => $this->Transfer->id,
            ]
        );
        $Archives->transitionOrFail($archive, ArchivesTable::T_UPDATE);
        $Archives->saveOrFail($archive);
        return $archive;
    }

    /**
     * Delete les fichiers tmp du transfer
     * @return void
     * @throws Exception
     */
    private function deleteTmpFiles(): void
    {
        $path = AbstractGenericMessageForm::getDataDirectory($this->Transfer->get('id')) . DS . 'tmp' . DS;
        if (is_dir($path)) {
            Filesystem::remove($path);
        }
    }

    /**
     * Création de l'entité d'archive selon le noeud archive du DOMDocument
     * @param DOMElement $archiveNode
     * @param int        $index
     * @return EntityInterface
     * @throws Exception
     */
    private function createArchive(DOMElement $archiveNode, int $index): EntityInterface
    {
        $saId = Hash::get($this->Transfer, 'archival_agency.id');
        $originatingAgency = $this->getOriginatingAgency($archiveNode)
            ?: $this->Transfer->get('transferring_agency');
        if (!$originatingAgency) {
            throw new Exception("orginating agency not found");
        }
        $agreement = $this->getAgreement($archiveNode);
        $profile = $this->getProfile($archiveNode);
        /** @var ServiceLevelsTable $ServiceLevels */
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $serviceLevel = $this->getServiceLevel($archiveNode)
            ?: $ServiceLevels->getDefault($saId);
        $service_level_id = Hash::get($serviceLevel ?: [], 'id');
        $secure_data_space_id = Hash::get(
            $serviceLevel ?: [],
            'secure_data_space_id'
        )
            ?: Hash::get(
                $this->Transfer,
                'archival_agency.default_secure_data_space_id'
            );

        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        /** @var CountersTable $Counters */
        $Counters = TableRegistry::getTableLocator()->get('Counters');
        $archiveIdentifier = $Counters->next(
            $saId,
            'ArchivalAgencyArchiveIdentifier',
            [],
            function ($archiveIdentifier) use ($Archives, $saId) {
                $cond = [
                    'archival_agency_identifier' => $archiveIdentifier,
                    'archival_agency_id' => $saId,
                ];
                return !$Archives->exists($cond);
            }
        );

        return $Archives->newEntity(
            [
                'state' => $Archives->initialState,
                'is_integrity_ok' => null,
                'archival_agency_id' => $saId,
                'transferring_agency_id' => $this->Transfer->get(
                    'transferring_agency_id'
                ),
                'originating_agency_id' => $originatingAgency->get('id'),
                'transferring_operator_id' => null,
                'agreement_id' => $agreement?->get('id'),
                'profile_id' => $profile?->get('id'),
                'transfer_id' => $this->Transfer->get('id'),
                'service_level_id' => $service_level_id,
                'access_rule_id' => $this->getAccessRuleId($archiveNode),
                'appraisal_rule_id' => $this->getAppraisalRuleId($archiveNode),
                'archival_agency_identifier' => $archiveIdentifier,
                'transferring_agency_identifier' => $this->getTransferringAgencyIdentifier(
                    $archiveNode
                ),
                'originating_agency_identifier' => $this->getOriginatingAgencyIdentifier(
                    $archiveNode
                ),
                'secure_data_space_id' => $secure_data_space_id,
                'storage_path' => implode(
                    '/',
                    [
                        urlencode($originatingAgency->get('identifier')),
                        $profile ? urlencode($profile->get('identifier'))
                            : 'no_profile',
                        date('Y-m'),
                        urlencode($archiveIdentifier),
                    ]
                ),
                'name' => $this->getName($archiveNode),
                'original_size' => 0,
                'description' => $this->getDescription($archiveNode),
                'oldest_date' => $this->getOldestDate($archiveNode),
                'latest_date' => $this->getLatestDate($archiveNode),
                'transferred_size' => 0,
                'management_size' => 0,
                'preservation_size' => 0,
                'dissemination_size' => 0,
                'timestamp_size' => 0,
                'transferred_count' => 0,
                'management_count' => 0,
                'original_count' => 0,
                'preservation_count' => 0,
                'dissemination_count' => 0,
                'timestamp_count' => 0,
                'xml_node_tagname' => sprintf(
                    '%s[%d]',
                    $archiveNode->nodeName,
                    $index + 1
                ),
            ]
        );
    }

    /**
     * Donne le service producteur d'une archive
     * @param DOMElement $archiveNode
     * @return null|EntityInterface
     * @throws Exception
     */
    private function getOriginatingAgency(DOMElement $archiveNode)
    {
        $identifier = null;
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $node = $this->xpath->query(
                    'ns:ContentDescription/ns:OriginatingAgency/ns:Identification',
                    $archiveNode
                )->item(0);
                if ($node) {
                    $identifier = trim($node->nodeValue);
                }
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $node = $this->xpath->query(
                    'ns:Content/ns:OriginatingAgency/ns:Identifier',
                    $archiveNode
                )->item(0);
                if ($node) {
                    $identifier = trim($node->nodeValue);
                }
        }
        if ($identifier) {
            return TableRegistry::getTableLocator()->get('OrgEntities')->find()
                ->where(
                    [
                        'OrgEntities.identifier' => $identifier,
                        'OrgEntities.lft >=' => Hash::get(
                            $this->Transfer,
                            'archival_agency.lft'
                        ),
                        'OrgEntities.rght <=' => Hash::get(
                            $this->Transfer,
                            'archival_agency.rght'
                        ),
                    ]
                )
                ->first();
        }
        return null;
    }

    /**
     * Donne l'accord de versement d'une archive
     * @param DOMElement $archiveNode
     * @return null|EntityInterface
     * @throws Exception
     */
    private function getAgreement(DOMElement $archiveNode)
    {
        $identifier = null;
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $node = $this->xpath->query(
                    'ns:ArchivalAgreement',
                    $archiveNode
                )->item(0);
                if ($node) {
                    $identifier = trim($node->nodeValue);
                }
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $node = $this->xpath->query(
                    'ns:ArchivalAgreement',
                    $archiveNode->ownerDocument->documentElement
                )->item(0);
                if ($node) {
                    $identifier = trim($node->nodeValue);
                }
        }
        if ($identifier) {
            return TableRegistry::getTableLocator()->get('Agreements')->find()
                ->where(
                    [
                        'Agreements.identifier' => $identifier,
                        'Agreements.org_entity_id' => Hash::get(
                            $this->Transfer,
                            'archival_agency.id'
                        ),
                    ]
                )
                ->first();
        }
        return null;
    }

    /**
     * Donne le profil d'archive d'une archive
     * @param DOMElement $archiveNode
     * @return null|EntityInterface
     * @throws Exception
     */
    private function getProfile(DOMElement $archiveNode)
    {
        $identifier = null;
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $node = $this->xpath->query('ns:ArchivalProfile', $archiveNode)
                    ->item(0);
                if ($node) {
                    $identifier = trim($node->nodeValue);
                }
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $node = $this->xpath->query(
                    'ns:DataObjectPackage/ns:ManagementMetadata/ns:ArchivalProfile',
                    $archiveNode->ownerDocument->documentElement
                )->item(0);
                if ($node) {
                    $identifier = trim($node->nodeValue);
                }
        }
        if ($identifier) {
            return TableRegistry::getTableLocator()->get('Profiles')->find()
                ->where(
                    [
                        'Profiles.identifier' => $identifier,
                        'Profiles.org_entity_id' => Hash::get(
                            $this->Transfer,
                            'archival_agency.id'
                        ),
                    ]
                )
                ->first();
        }
        return null;
    }

    /**
     * Donne le niveau de service d'une archive
     * @param DOMElement $archiveNode
     * @return null|EntityInterface
     * @throws Exception
     */
    private function getServiceLevel(DOMElement $archiveNode)
    {
        $identifier = null;
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $node = $this->xpath->query('ns:ServiceLevel', $archiveNode)
                    ->item(0);
                if ($node) {
                    $identifier = trim($node->nodeValue);
                }
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $node = $this->xpath->query(
                    'ns:DataObjectPackage/ns:ManagementMetadata/ns:ServiceLevel',
                    $archiveNode->ownerDocument->documentElement
                )->item(0);
                if ($node) {
                    $identifier = trim($node->nodeValue);
                }
        }
        if ($identifier) {
            return TableRegistry::getTableLocator()->get('ServiceLevels')->find()
                ->where(
                    [
                        'ServiceLevels.identifier' => $identifier,
                        'ServiceLevels.org_entity_id' => Hash::get(
                            $this->Transfer,
                            'archival_agency.id'
                        ),
                    ]
                )
                ->contain(['SecureDataSpaces'])
                ->first();
        }
        return null;
    }

    /**
     * Donne l'id de l'access rule selon AccessRestriction/Code
     * @param DOMElement $archiveNode
     * @return null|int
     * @throws Exception
     */
    private function getAccessRuleId(DOMElement $archiveNode)
    {
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
                $accessRule = $this->xpath->query(
                    'ns:AccessRestriction',
                    $archiveNode
                )->item(0);
                $accessRuleCode = 'Code';
                break;
            case NAMESPACE_SEDA_10:
                $accessRule = $this->xpath->query(
                    'ns:AccessRestrictionRule',
                    $archiveNode
                )->item(0);
                $accessRuleCode = 'Code';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $accessRule = $this->xpath->query(
                    'ns:Management/ns:AccessRule',
                    $archiveNode
                )->item(0);
                $accessRuleCode = 'Rule';
                break;
            default:
                $accessRule = null;
                $accessRuleCode = null;
        }
        if ($accessRule) {
            $code = $this->xpath->query('ns:' . $accessRuleCode, $accessRule)
                ->item(0);
            $startDate = $this->xpath->query('ns:StartDate', $accessRule)->item(0);
        }
        $AccessRuleCodes = TableRegistry::getTableLocator()->get('AccessRuleCodes');
        $rule = $AccessRuleCodes->find()
            ->select(['id', 'duration'])
            ->where(['code' => empty($code) ? 'AR062' : trim($code->nodeValue)])
            ->firstOrFail();
        $AccessRules = TableRegistry::getTableLocator()->get('AccessRules');
        $startDate = new CakeDateTime(
            empty($startDate) ? date('Y-m-d') : trim($startDate->nodeValue)
        );
        $accessRule = $AccessRules->newEntity(
            [
                'access_rule_code_id' => $rule->get('id'),
                'start_date' => clone $startDate,
                'end_date' => $startDate->add(
                    new DateInterval($rule->get('duration'))
                ),
            ]
        );
        $AccessRules->saveOrFail($accessRule);
        return $accessRule->get('id');
    }

    /**
     * Donne l'id de l'access rule selon AccessRestriction/Code
     * @param DOMElement $archiveNode
     * @return null|int
     * @throws Exception
     */
    private function getAppraisalRuleId(DOMElement $archiveNode)
    {
        $AppraisalRuleCodes = TableRegistry::getTableLocator()->get('AppraisalRuleCodes');
        /** @var DOMElement $node */
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
                $appraisalRule = $this->xpath->query(
                    'ns:Appraisal',
                    $archiveNode
                )->item(0);
                break;
            case NAMESPACE_SEDA_10:
                $appraisalRule = $this->xpath->query(
                    'ns:AppraisalRule',
                    $archiveNode
                )->item(0);
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $ar = $this->xpath->query(
                    'ns:Management/ns:AppraisalRule',
                    $archiveNode
                )->item(0);
                $ruleNode = $this->xpath->query('ns:Rule', $ar)->item(0);
                if ($ruleNode) {
                    $rule = $AppraisalRuleCodes->find()
                        ->where(
                            [
                                'OR' => [
                                    'org_entity_id IS' => null,
                                    'org_entity_id' => $this->Transfer->get('archival_agency_id'),
                                ],
                                'code' => trim($ruleNode->nodeValue),
                            ]
                        )
                        ->first();
                }
                $startDate = $this->xpath->query('ns:StartDate', $ar)->item(0);
                $code = $this->xpath->query('ns:FinalAction', $ar)->item(0);
                $appraisalRule = null; // gestion custom
                break;
            default:
                $appraisalRule = null;
        }
        if ($appraisalRule) {
            $code = $this->xpath->query('ns:Code', $appraisalRule)->item(0);
            $duration = $this->xpath->query('ns:Duration', $appraisalRule)->item(0);
            if (empty($duration)) {
                $duration = 'P0Y';
            } else {
                $duration = trim($duration->nodeValue);
            }
            $appraisalRuleCode = 'AP' . $duration;
            $startDate = $this->xpath->query('ns:StartDate', $appraisalRule)->item(0);
        } else {
            $appraisalRuleCode = 'APP0Y';
        }
        $code = strtolower(empty($code) ? 'keep' : trim($code->nodeValue));
        if ($code === 'conserver') {
            $code = 'keep';
        } elseif ($code === 'detruire') {
            $code = 'destroy';
        }
        if (empty($rule)) {
            $rule = $AppraisalRuleCodes->find()
                ->select(['id', 'duration'])
                ->where(['code' => $appraisalRuleCode])
                ->first();
        }
        if (empty($rule)) {
            $rule = $AppraisalRuleCodes->find()
                ->select(['id', 'duration'])
                ->where(['code' => 'APP0Y'])
                ->firstOrFail();
        }

        $AppraisalRules = TableRegistry::getTableLocator()->get(
            'AppraisalRules'
        );
        $startDate = new CakeDateTime(
            empty($startDate) ? date('Y-m-d') : trim($startDate->nodeValue)
        );
        $appraisalRule = $AppraisalRules->newEntity(
            [
                'appraisal_rule_code_id' => $rule->get('id'),
                'final_action_code' => $code,
                'start_date' => clone $startDate,
                'end_date' => $startDate->add(
                    new DateInterval($rule->get('duration'))
                ),
            ]
        );
        $AppraisalRules->saveOrFail($appraisalRule);
        return $appraisalRule->get('id');
    }

    /**
     * Donne l'identifiant d'archive défini par le versant
     * @param DOMElement $archiveNode
     * @return null|string
     */
    private function getTransferringAgencyIdentifier(DOMElement $archiveNode)
    {
        /** @var DOMElement $node */
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $node = $this->xpath->query(
                    'ns:TransferringAgencyArchiveIdentifier',
                    $archiveNode
                )->item(0);
                return $node ? trim($node->nodeValue) : null;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $node = $this->xpath->query(
                    'ns:Content/ns:TransferringAgencyArchiveUnitIdentifier',
                    $archiveNode
                )->item(0);
                return $node ? trim($node->nodeValue) : null;
            default:
                return null;
        }
    }

    /**
     * Donne l'identifiant d'archive défini par le producteur
     * @param DOMElement $archiveNode
     * @return string|null
     */
    private function getOriginatingAgencyIdentifier(DOMElement $archiveNode)
    {
        /** @var DOMElement $node */
        switch ($this->namespace) {
            case NAMESPACE_SEDA_10:
                $node = $this->xpath->query(
                    'ns:OriginatingAgencyArchiveIdentifier',
                    $archiveNode
                )->item(0);
                return $node ? trim($node->nodeValue) : null;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $node = $this->xpath->query(
                    'ns:Content/ns:OriginatingAgencyArchiveUnitIdentifier',
                    $archiveNode
                )
                    ->item(0);
                return $node ? trim($node->nodeValue) : null;
            default:
                return null;
        }
    }

    /**
     * Donne le nom d'une archive
     * @param DOMElement $archiveNode
     * @return null|string
     */
    private function getName(DOMElement $archiveNode)
    {
        /** @var DOMElement $node */
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $node = $this->xpath->query(
                    'ns:Name',
                    $archiveNode
                )->item(0);
                return $node ? trim($node->nodeValue) : null;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $node = $this->xpath->query(
                    'ns:Content/ns:Title',
                    $archiveNode
                )->item(0);
                return $node ? trim($node->nodeValue) : null;
            default:
                return null;
        }
    }

    /**
     * Donne la description de l'archive
     * @param DOMElement $archiveNode
     * @return null|string
     * @throws Exception
     */
    private function getDescription(DOMElement $archiveNode)
    {
        /** @var DOMElement $node */
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $node = $this->xpath->query(
                    'ns:ContentDescription/ns:Description',
                    $archiveNode
                )->item(0);
                return $node ? trim($node->nodeValue) : null;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $nodes = $this->xpath->query(
                    'ns:Content/ns:Description',
                    $archiveNode
                );
                $description = [];
                /** @var DOMElement $node */
                foreach ($nodes as $node) {
                    $description[] = trim($node->nodeValue, '. ');
                }
                return implode('. ', $description);
            default:
                return null;
        }
    }

    /**
     * Donne la date de l'archive (oldest)
     * @param DOMElement $archiveNode
     * @return CakeDateTime|null
     * @throws Exception
     */
    private function getOldestDate(DOMElement $archiveNode)
    {
        /** @var DOMElement $node */
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $node = $this->xpath->query(
                    'ns:ContentDescription/ns:OldestDate',
                    $archiveNode
                )->item(0);
                return $node ? new CakeDateTime(trim($node->nodeValue)) : null;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $node = $this->xpath->query(
                    'ns:Content/ns:StartDate',
                    $archiveNode
                )->item(0);
                return $node ? new CakeDateTime(trim($node->nodeValue)) : null;
            default:
                return null;
        }
    }

    /**
     * Donne la date de l'archive (latest)
     * @param DOMElement $archiveNode
     * @return CakeDateTime|null
     * @throws Exception
     */
    private function getLatestDate(DOMElement $archiveNode)
    {
        /** @var DOMElement $node */
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $node = $this->xpath->query(
                    'ns:ContentDescription/ns:LatestDate',
                    $archiveNode
                )->item(0);
                return $node ? new CakeDateTime(trim($node->nodeValue)) : null;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $node = $this->xpath->query(
                    'ns:Content/ns:EndDate',
                    $archiveNode
                )->item(0);
                return $node ? new CakeDateTime(trim($node->nodeValue)) : null;
            default:
                return null;
        }
    }

    /**
     * Garde en mémoire les codes/durées du access_rule_codes
     * @return array ['AR038' => 'P0Y', ...]
     */
    public static function getAccessRuleCodes(): array
    {
        if (empty(self::$accessRuleCodes)) {
            self::$accessRuleCodes = TableRegistry::getTableLocator()
                ->get('AccessRuleCodes')
                ->find(
                    'list',
                    keyField: 'code',
                    valueField: 'duration'
                )
                ->toArray();
        }
        return self::$accessRuleCodes;
    }
}
