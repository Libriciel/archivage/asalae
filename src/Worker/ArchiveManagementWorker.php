<?php

/**
 * Asalae\Worker\ArchiveManagementWorker
 */

namespace Asalae\Worker;

use Asalae\Model\Entity\Archive;
use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Entity\LifecycleXmlArchiveFile;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Entity\SecureDataSpace;
use Asalae\Model\Entity\Transfer;
use Asalae\Model\Entity\User;
use Asalae\Model\Entity\Volume;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\DescriptionXmlArchiveFilesTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\LifecycleXmlArchiveFilesTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\VolumesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Premis;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Datasource\EntityInterface;
use Cake\Error\Debugger;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInvalidTimeZoneException;
use DateTimeInterface;
use DOMDocument;
use DOMElement;
use DOMException;
use Exception;
use Pheanstalk\Exception\ServerException;
use ZMQSocketException;

/**
 * Créer les fichiers description et cycle de vie d'une archive
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveManagementWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var array
     */
    private static $formatNames;
    /**
     * @var Archive
     */
    private $Archive;
    /**
     * @var VolumeManager
     */
    private $VolumeManager;
    /**
     * @var Premis\Agent
     */
    private $agent;
    /**
     * @var string
     */
    private $identifier;
    /**
     * @var string
     */
    private $basePath;

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        $this->out(
            __(
                "Création des fichiers de management du transfert id={0}",
                $data['transfer_id']
            )
        );
        if (isset($data['composite_id'])) {
            $this->handleComposite($data);
            $this->createJobForNextTransfer($data['composite_id']);
            return;
        }

        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        /** @var VolumesTable $Volumes */
        $Volumes = $loc->get('Volumes');
        $ArchiveBinaryConversions = $loc->get('ArchiveBinaryConversions');
        $query = $Archives->find()
            ->innerJoinWith('Transfers')
            ->where(['Transfers.id' => $data['transfer_id']])
            ->orderBy(['Archives.id']);
        $secureDataSpaces = [];
        /** @var EntityInterface $archive */
        foreach ($query as $archive) {
            $this->createArchiveManagementForArchive($archive->id);
            $this->saveIndicators($archive);
            if ($ArchiveBinaryConversions->exists(['archive_id' => $archive->id])) {
                $this->emit('conversion', ['archive_id' => $archive->id]);
            }
            $secureDataSpaces[$archive->get('secure_data_space_id')] = true;
            $this->createJobForNextTransfer($archive->id);
        }
        $q = $Volumes->find()
            ->where(['secure_data_space_id IN' => array_keys($secureDataSpaces)]);
        foreach ($q as $volume) {
            $Volumes->updateDiskUsage($volume->id);
        }
    }

    /**
     * Gestion des archives composite (seda >= 2.0)
     * @param array $data
     * @return void
     * @throws CantWorkException
     * @throws DOMException
     * @throws ServerException
     * @throws VolumeException
     * @throws ZMQSocketException
     * @throws Exception
     */
    private function handleComposite(array $data)
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');
        $ArchiveFiles = $loc->get('ArchiveFiles');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $this->Archive = $Archives->find()
            ->where(
                [
                    'Archives.id' => $data['composite_id'],
                    'Archives.state' => ArchivesTable::S_UPDATE_STORING,
                ]
            )
            ->contain(
                [
                    'ArchivalAgencies' => ['Configurations'],
                    'Transfers' => function (Query $q) use ($data) {
                        return $q->where(['Transfers.id' => $data['transfer_id']])
                            ->contain(['Users']);
                    },
                    'SecureDataSpaces' => ['Volumes'],
                ]
            )
            ->first();
        if (!$this->Archive) {
            $this->out(
                __("Impossible de trouver l'archive id={0}", $data['composite_id'])
            );
            throw new CantWorkException();
        }
        $this->VolumeManager = new VolumeManager(
            $this->Archive->get('secure_data_space_id')
        );
        $conn = $ArchiveFiles->getConnection();
        $conn->begin();
        try {
            $Archives->transitionOrFail($this->Archive, ArchivesTable::T_MANAGE);
            $Archives->saveOrFail($this->Archive);
            $this->appendCompositeToDescription($data);
            $this->appendCompositeToLifecycle($data);
            $this->uploadTransfer();
            $this->finalise();
            $ArchiveUnits->updateCountSize(
                $ArchiveUnits->find()
                    ->where(['ArchiveUnits.archive_id' => $data['composite_id']])
            );
            $Archives->updateCountSize([$this->Archive]);
            $ArchiveUnits->calcDuaExpiredRoot(['ArchiveUnits.archive_id' => $data['composite_id']]);

            $loc = TableRegistry::getTableLocator();
            $ArchiveIndicators = $loc->get('ArchiveIndicators');
            $archive = $Archives->get($data['composite_id']);
            $unitsCount = $loc->get('CompositeArchiveUnits')->find()
                ->where(['transfer_id' => $data['transfer_id']])
                ->count();
            $entity = $ArchiveIndicators->newEntity(
                [
                    'archive_date' => new CakeDateTime(),
                    'archival_agency_id' => $archive->get('archival_agency_id'),
                    'transferring_agency_id' => $archive->get('transferring_agency_id'),
                    'originating_agency_id' => $archive->get('originating_agency_id'),
                    'archive_count' => 0,
                    'units_count' => $unitsCount,
                    'original_count' =>
                        $archive->get('original_count') - $this->Archive->get('original_count'),
                    'original_size' =>
                        $archive->get('original_size') - $this->Archive->get('original_size'),
                    'timestamp_count' =>
                        $archive->get('timestamp_count') - $this->Archive->get('timestamp_count'),
                    'timestamp_size' =>
                        $archive->get('timestamp_size') - $this->Archive->get('timestamp_size'),
                    'preservation_count' =>
                        $archive->get('preservation_count') - $this->Archive->get('preservation_count'),
                    'preservation_size' =>
                        $archive->get('preservation_size') - $this->Archive->get('preservation_size'),
                    'dissemination_count' =>
                        $archive->get('dissemination_count') - $this->Archive->get('dissemination_count'),
                    'dissemination_size' =>
                        $archive->get('dissemination_size') - $this->Archive->get('dissemination_size'),
                    'agreement_id' => $this->Archive->get('agreement_id'),
                    'profile_id' => $this->Archive->get('profile_id'),
                ]
            );
            $ArchiveIndicators->saveOrFail($entity);

            $conn->commit();
            $this->Archive = $Archives->get($this->Archive->id);
            Utility::get('Notify')->emit(
                'archive_state_update',
                [
                    'id' => $this->Archive->id,
                    'state' => $this->Archive->get('state'),
                    'statetrad' => $this->Archive->get('statetrad'),
                    'is_large' => $this->Archive->get('management_size') > 5000000,
                ]
            );

            $ArchiveBinaryConversions = $loc->get('ArchiveBinaryConversions');
            if ($ArchiveBinaryConversions->exists(['archive_id' => $this->Archive->id])) {
                $this->emit('conversion', ['archive_id' => $this->Archive->id]);
            }
        } catch (Exception $e) {
            $conn->rollback();
            throw $e;
        }
    }

    /**
     * Ajoute les nouvelles archive_units à la description
     * @param array $data
     * @throws DOMException
     * @throws ServerException
     * @throws VolumeException
     */
    private function appendCompositeToDescription(array $data): void
    {
        $descUtil = $this->appendTransferNodes($data);
        $this->addMissingNodes($descUtil, $data);
        $this->addArchivalAgencyIdentifiers($descUtil, $data);
        $tmpFilename = sys_get_temp_dir() . DS . uniqid('management', true) . '.xml';
        $descUtil->dom->preserveWhiteSpace = false;
        $descUtil->dom->formatOutput = true;
        $descUtil->dom->save($tmpFilename);
        try {
            $loc = TableRegistry::getTableLocator();
            /** @var DescriptionXmlArchiveFilesTable $DescriptionXmlArchiveFiles */
            $DescriptionXmlArchiveFiles = $loc->get('DescriptionXmlArchiveFiles');
            /** @var DescriptionXmlArchiveFile $description */
            $description = $DescriptionXmlArchiveFiles->find()
                ->where(
                    [
                        'type' => ArchiveFilesTable::TYPE_DESCRIPTION,
                        'archive_id' => $data['composite_id'],
                    ]
                )
                ->firstOrFail();
            $this->VolumeManager->replace($description->get('stored_file_id'), $tmpFilename);
        } finally {
            unlink($tmpFilename);
        }
    }

    /**
     * Importe les noeuds BinaryDataObject et ArchiveUnit dans la description
     * @param array $data
     * @return DOMUtility $util de la description modifiée
     * @throws VolumeException
     */
    private function appendTransferNodes(array $data): DOMUtility
    {
        $loc = TableRegistry::getTableLocator();
        /** @var DescriptionXmlArchiveFilesTable $DescriptionXmlArchiveFiles */
        $DescriptionXmlArchiveFiles = $loc->get('DescriptionXmlArchiveFiles');
        /** @var DescriptionXmlArchiveFile $description */
        $description = $DescriptionXmlArchiveFiles->find()
            ->where(
                [
                    'type' => ArchiveFilesTable::TYPE_DESCRIPTION,
                    'archive_id' => $data['composite_id'],
                ]
            )
            ->firstOrFail();
        $descUtil = new DOMUtility($description->getDom());
        $parentNode = $descUtil->dom->documentElement;

        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        /** @var Transfer $transfer */
        $transfer = $Transfers->get($data['transfer_id']);
        $transferUtil = DOMUtility::load($transfer->get('xml'));
        $xpathQuery = $transferUtil->xpath->query(
            'ns:DataObjectPackage/ns:DataObjectGroup|ns:DataObjectPackage/ns:BinaryDataObject'
        );
        /** @var DOMElement $node */
        foreach ($xpathQuery as $node) {
            $descUtil->appendChild($parentNode, $descUtil->dom->importNode($node, true));
        }

        $ArchiveUnits = $loc->get('ArchiveUnits');
        $query = $ArchiveUnits->find()
            ->innerJoinWith('CompositeArchiveUnits')
            ->where(
                [
                    'transfer_id' => $data['transfer_id'],
                ]
            )
            ->contain(['CompositeArchiveUnits', 'ParentArchiveUnits'])
            ->orderByAsc('ArchiveUnits.id');
        $reg = '#^/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata/ns:ArchiveUnit(\[\d+])?$#';
        foreach ($query as $archiveUnit) {
            $transferXpath = Hash::get($archiveUnit, 'composite_archive_unit.transfer_xpath');
            // on n'insère que les 1ers niveaux
            if (!preg_match($reg, $transferXpath)) {
                continue;
            }
            /** @var EntityInterface $parentArchiveUnit */
            $parentArchiveUnit = Hash::get($archiveUnit, 'parent_archive_unit');
            $parentNode = $descUtil->node($parentArchiveUnit->get('xpath'));
            $transferNode = $transferUtil->node($transferXpath);
            $descUtil->appendChild($parentNode, $descUtil->dom->importNode($transferNode, true));
        }
        return $descUtil;
    }

    /**
     * Ajoute des informations dans la description (size, format, hash...)
     * @param DOMUtility $util
     * @param array      $data
     * @return void
     * @throws DOMException
     * @throws ServerException
     */
    private function addMissingNodes(DOMUtility $util, array $data)
    {
        // remplace les Uri par des Attachment
        $uris = $util->xpath->query('//ns:BinaryDataObject/ns:Uri');
        /** @var DOMElement $Uri */
        foreach ($uris as $Uri) {
            $Attachment = $util->dom->createElementNS(
                $util->namespace,
                'Attachment'
            );
            $Attachment->setAttribute('filename', trim($Uri->nodeValue));
            $Uri->parentNode->insertBefore($Attachment, $Uri);
            $Uri->parentNode->removeChild($Uri);
        }

        $loc = TableRegistry::getTableLocator();
        $Pronoms = $loc->get('Pronoms');
        $TransferAttachments = $loc->get('TransferAttachments');
        $query = $TransferAttachments->find()
            ->where(['transfer_id' => $data['transfer_id']]);
        $puids = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('filetype')
        );
        $mimes = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('mime')
        );
        $count = $query->count();
        foreach ($query as $i => $attachment) {
            if ($i % 100 === 0) {
                $this->out(sprintf('%d / %d', $i, $count));
            }
            $documentNode = $util->node(
                sprintf(
                    '//ns:BinaryDataObject/ns:Attachment[@filename=%s]/..',
                    $util::xpathQuote($attachment->get('filename'))
                )
            );
            /**
             * Size
             */
            /** @var DOMElement $sizeNode */
            if ($attachmentSize = $attachment->get('size')) {
                $sizeNode = $util->node('ns:Size', $documentNode);
                if (!$sizeNode) {
                    $sizeNode = $util->appendChild(
                        $documentNode,
                        $util->createElement('Size', $attachmentSize)
                    );
                }
                $sizeNode->nodeValue = $attachmentSize;
            }

            /**
             * Integrity
             */
            $integrity = $util->node('ns:MessageDigest', $documentNode);
            if (!$integrity) {
                $integrity = $util->appendChild(
                    $documentNode,
                    $util->createElement(
                        'MessageDigest',
                        $attachment->get('hash')
                    )
                );
                $integrity->setAttribute(
                    'algorithm',
                    $attachment->get('hash_algo')
                );
            }
            $algo = $integrity->getAttributeNode('algorithm');

            /**
             * Format
             */
            $formatIdentification = $util->node(
                'ns:FormatIdentification',
                $documentNode
            );
            $puid = $attachment->get('format');
            $mime = $attachment->get('mime');
            if (!$formatIdentification && ($puid || $mime)) {
                $formatIdentification = $util->appendChild(
                    $documentNode,
                    $util->createElement('FormatIdentification')
                );
            }
            if ($puid && in_array($puid, $puids)) {
                if (!isset(self::$formatNames[$puid])) {
                    self::$formatNames[$puid] = Hash::get(
                        $Pronoms->find()
                            ->select(['name'])
                            ->where(['puid' => $puid])
                            ->disableHydration()
                            ->first() ?: [],
                        'name',
                        ''
                    );
                }
                if (self::$formatNames[$puid]) {
                    $formatLitteral = $util->node(
                        'ns:FormatLitteral',
                        $formatIdentification
                    );
                    if (!$formatLitteral) {
                        $formatLitteral = $util->appendChild(
                            $formatIdentification,
                            $util->createElement(
                                'FormatLitteral',
                                self::$formatNames[$puid]
                            )
                        );
                    }
                    $formatLitteral->nodeValue = self::$formatNames[$puid];
                }
                $formatId = $util->node(
                    'ns:FormatId',
                    $formatIdentification
                );
                if (!$formatId) {
                    $formatId = $util->appendChild(
                        $formatIdentification,
                        $util->createElement('FormatId', $mime)
                    );
                }
                $formatId->nodeValue = $puid;
            }
            if ($mime && in_array($mime, $mimes)) {
                $mimeType = $util->node(
                    'ns:MimeType',
                    $formatIdentification
                );
                if (!$mimeType) {
                    $mimeType = $util->appendChild(
                        $formatIdentification,
                        $util->createElement('MimeType', $mime)
                    );
                }
                $mimeType->nodeValue = $mime;
            }
            $integrity->nodeValue = $attachment->get('hash');
            $algo->nodeValue = $attachment->get('hash_algo');
        }
    }

    /**
     * Ajoute les archival_agency_identifier dans la description
     * @param DOMUtility $util
     * @param array      $data
     * @return void
     * @throws Exception
     */
    private function addArchivalAgencyIdentifiers(DOMUtility $util, array $data)
    {
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        $ArchiveUnits = $loc->get('ArchiveUnits');
        /** @var Transfer $transfer */
        $transfer = $Transfers->get($data['transfer_id']);
        $query = $ArchiveUnits->find()
            ->innerJoinWith('CompositeArchiveUnits')
            ->where(['transfer_id' => $data['transfer_id']])
            ->orderBy(['lft' => 'asc']);
        $paths = [];
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $archiveUnit) {
            $unit = $archiveUnit->toArray();
            if (isset($paths[$unit['parent_id']])) {
                $id = $paths[$unit['parent_id']] . '/ns:' . $unit['xml_node_tagname'];
            } else {
                $id = $archiveUnit->get('xpath');
            }
            // on ne mémorise que les noeuds de type parent
            if ($unit['lft'] + 1 !== $unit['rght']) {
                $paths[$unit['id']] = $id;
            }
            $query = $util->xpath->query($id);
            if (!$query) {
                throw new Exception('xpath query failed: ' . $id);
            }
            $node = $query->item(0);
            if (!$node instanceof DOMElement) {
                throw new Exception(
                    'archive_unit ' . $id . ' not found in xml'
                );
            }
            $content = $util->xpath->query('ns:Content', $node)->item(0);
            $identifier = $util->xpath->query(
                'ns:ArchivalAgencyArchiveUnitIdentifier',
                $content
            );
            if ($identifier->count()) {
                DOMUtility::setValue(
                    $identifier->item(0),
                    $unit['archival_agency_identifier']
                );
                continue;
            }
            // le noeud identifiant n'existe pas, il faut l'ajouter
            $path = str_replace(['ns:', '/'], ['', '.'], ltrim($id, '/')) . '.Content';
            $path = preg_replace('/\[\d+]/', '', $path);
            $identifier = $transfer->appendNode(
                $transfer->get('archival_agency_id'),
                'ArchivalAgencyArchiveUnitIdentifier',
                $path,
                $util->dom,
                $content
            );
            DOMUtility::setValue(
                $identifier,
                $unit['archival_agency_identifier']
            );
        }
    }

    /**
     * Ajoute les éléments du transfert dans le fichier de cycle de vie
     * @param array $data
     * @return void
     * @throws DOMException
     * @throws ServerException
     * @throws VolumeException
     */
    private function appendCompositeToLifecycle(array $data)
    {
        $loc = TableRegistry::getTableLocator();
        /** @var DescriptionXmlArchiveFilesTable $DescriptionXmlArchiveFiles */
        $DescriptionXmlArchiveFiles = $loc->get('DescriptionXmlArchiveFiles');
        /** @var DescriptionXmlArchiveFile $description */
        $description = $DescriptionXmlArchiveFiles->find()
            ->where(
                [
                    'type' => ArchiveFilesTable::TYPE_DESCRIPTION,
                    'archive_id' => $data['composite_id'],
                ]
            )
            ->contain(['StoredFiles'])
            ->firstOrFail();
        /** @var EntityInterface $storedFile */
        $storedFile = $description->get('stored_file');
        /** @var LifecycleXmlArchiveFilesTable $LifecycleXmlArchiveFiles */
        $LifecycleXmlArchiveFiles = $loc->get('LifecycleXmlArchiveFiles');
        /** @var LifecycleXmlArchiveFile $lifecycleEntity */
        $lifecycleEntity = $LifecycleXmlArchiveFiles->find()
            ->where(
                [
                    'type' => ArchiveFilesTable::TYPE_LIFECYCLE,
                    'archive_id' => $data['composite_id'],
                ]
            )
            ->firstOrFail();
        file_put_contents(TMP . 'old_premis.xml', $lifecycleEntity->getDom()->saveXML());
        $premis = Premis::loadDocument($lifecycleEntity->getDom());
        $lifecycleDom = $this->generatePremis(true);
        file_put_contents(TMP . 'composite_premis.xml', $premis->generate());
        $newPremis = Premis::loadDocument($lifecycleDom);
        $premis->merge($newPremis);
        /** @var Premis\File $desc */
        $desc = $premis->getObject('local', 'ArchiveFiles:' . $description->id);
        $desc->messageDigest = $storedFile->get('hash');
        if ($storedFile->get('hash_algo') !== 'sha256') {
            $desc->messageDigestAlgorithm = [
                '@' => $storedFile->get('hash_algo'),
            ];
        }
        $desc->size = $storedFile->get('size');
        file_put_contents(TMP . 'merged_premis.xml', $premis->generate());
        $tmpFilename = sys_get_temp_dir() . DS . uniqid('management', true) . '.xml';
        file_put_contents($tmpFilename, $premis->generate());
        try {
            $this->VolumeManager->replace($lifecycleEntity->get('stored_file_id'), $tmpFilename);
        } finally {
            unlink($tmpFilename);
        }
    }

    /**
     * Donne le fichier premis du cycle de vie
     * @param bool $composite
     * @return DOMDocument
     * @throws DOMException
     * @throws ServerException
     */
    private function generatePremis(bool $composite = false): DOMDocument
    {
        $loc = TableRegistry::getTableLocator();
        /** @var Transfer $transferEntity */
        $transferEntity = Hash::get($this->Archive, 'transfers.0');
        $transfer_identifier = $transferEntity->get('transfer_identifier');
        /** @var CakeDateTime $transfer_date */
        $transfer_date = Hash::get($this->Archive, 'transfers.0.created');
        $transfer_date = $transfer_date->format(DATE_RFC3339);
        $archive_date = $this->Archive->get('created');
        /** @var CakeDateTime $archive_date */
        $archive_date = $archive_date->format(DATE_RFC3339);
        $user_id = Hash::get($this->Archive, 'transfers.0.user_id');

        $premis = new Premis();
        $agent = $this->getUserAgent();

        /** @var Premis\IntellectualEntity $transfer */
        $transfer = $transferEntity->toPremisObject();

        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->find()
            ->where(
                [
                    'type' => 'transfer_add',
                    'object_model' => 'Transfers',
                    'object_foreign_key' => $transferEntity->id,
                ]
            )
            ->first();
        $details = __(
            "Prise en compte du transfert ''{0}''",
            $transfer_identifier
        );
        if (!$entry) {
            $entry = $EventLogs->newEntry(
                'transfer_add',
                'success',
                $details,
                $user_id,
                $transferEntity
            );
            $entry->set('in_lifecycle', false);
            $EventLogs->saveOrFail($entry);
        }
        $createTransferEvent = new Premis\Event(
            'transfer_add',
            'local',
            'EventLogs:' . $entry->id
        );
        $createTransferEvent->detail = __("création du transfert");
        $createTransferEvent->outcome = 'success';
        $createTransferEvent->outcomeDetail = $details;
        $createTransferEvent->dateTime = $transfer_date;
        $createTransferEvent->addAgent($agent);
        $createTransferEvent->addObject($transfer);
        $premis->add($createTransferEvent);

        $validationEvents = $EventLogs->find()
            ->where(
                [
                    'type IN' => ['validate_transfer', 'invalidate_transfer', 'validation_stepback_transfer'],
                    'object_model' => 'Transfers',
                    'object_foreign_key' => $transferEntity->id,
                ]
            )
            ->orderByAsc('id');
        foreach ($validationEvents as $entry) {
            $premis->add($entry->get('premis_event'));
        }

        if (!$composite) {
            /** @var Premis\IntellectualEntity $archive */
            $archive = $this->Archive->toPremisObject();

            $entry = $EventLogs->newEntry(
                'archive_add',
                'success',
                $details = __(
                    "Création de l'archive ''{0}''",
                    $this->Archive->get('archival_agency_identifier')
                ),
                $user_id,
                $this->Archive
            );
            $entry->set('in_lifecycle', false);
            $EventLogs->saveOrFail($entry);
            $createArchiveEvent = new Premis\Event(
                'archive_add',
                'local',
                'EventLogs:' . $entry->id
            );
            $createArchiveEvent->detail = __("création de l'archive");
            $createArchiveEvent->outcome = 'success';
            $createArchiveEvent->outcomeDetail = $details;
            $createArchiveEvent->dateTime = $archive_date;
            $createArchiveEvent->addAgent($agent);
            $createArchiveEvent->addObject($archive);

            /** @var SecureDataSpace $secureDataSpace */
            $secureDataSpace = $this->Archive->get('secure_data_space');
            $createArchiveEvent->addObject($secureDataSpace->toPremisObject());
            /** @var Volume $volume */
            foreach ($secureDataSpace->get('volumes') as $volume) {
                $createArchiveEvent->addObject($volume->toPremisObject());
            }
            $premis->add($createArchiveEvent);
        }

        $Archives = $loc->get('Archives');
        $units = $Archives->find()
            ->select(
                [
                    'archive_unit_id' => 'ArchiveUnits.id',
                    'archival_agency_identifier' => 'ArchiveUnits.archival_agency_identifier',
                    'name' => 'ArchiveUnits.name',
                    'archive_binary_id' => 'ArchiveBinaries.id',
                    'filename' => 'ArchiveBinaries.filename',
                    'format' => 'Pronoms.name',
                    'puid' => 'Pronoms.puid',
                    'hash' => 'StoredFiles.hash',
                    'hash_algo' => 'StoredFiles.hash_algo',
                    'size' => 'StoredFiles.size',
                    'space_id' => 'SecureDataSpaces.id',
                    'space' => 'SecureDataSpaces.name',
                    'created' => 'StoredFiles.created',
                ]
            )
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('SecureDataSpaces')
            ->innerJoinWith('ArchiveUnits.ArchiveBinaries')
            ->leftJoinWith('ArchiveUnits.ArchiveBinaries.Pronoms')
            ->leftJoinWith('ArchiveUnits.ArchiveBinaries.StoredFiles')
            ->where(
                [
                    'Archives.id' => $this->Archive->get('id'),
                    'ArchiveBinaries.type' => 'original_data',
                ]
            )
            ->orderBy(['ArchiveUnits.lft' => 'asc']);
        if ($composite) {
            $units->innerJoinWith('ArchiveUnits.CompositeArchiveUnits')
                ->where(['CompositeArchiveUnits.transfer_id' => $transferEntity->id]);
        }
        $dom = $premis->generateDocument();
        $total = $units->count();
        $this->out(__("Cycle de vie: {0} unités à décrire.", $total));
        $c = 0;
        $objects = '';
        $events = '';

        /** @var EntityInterface $entity */
        foreach ($units as $entity) {
            $c++;
            if ($c % 1000 === 0) {
                $this->out(sprintf('%d / %d', $c, $total));
            }

            /**
             * Archive binary
             */
            $binary = new Premis\File(
                $entity->get('format') ?: 'unknown',
                'local',
                'ArchiveBinaries:' . $entity->get('archive_binary_id')
            );
            $binary->originalName = $entity->get('filename');
            $binary->messageDigest = $entity->get('hash');
            if ($entity->get('format')) {
                $binary->formatRegistryName = 'pronom';
                $binary->formatRegistryKey = $entity->get('puid');
            } else {
                $binary->formatName = 'UNKNOWN';
            }
            if ($entity->get('hash_algo') !== 'sha256') {
                $binary->messageDigestAlgorithm = [
                    '@' => $entity->get(
                        'hash_algo'
                    ),
                ];
            }
            $binary->messageDigest = $entity->get('hash');
            $binary->size = $entity->get('size');
            $objects .= $binary;
            /** @var CakeDateTime $created */
            $created = $entity->get('created');

            $entry = $EventLogs->newEntry(
                'archivebinary_add',
                'success',
                $details = __(
                    "Stockage du fichier ''{0}'' sur l'espace de conservation ''{1}''",
                    $entity->get('filename'),
                    $entity->get('space')
                ),
                $user_id
            );
            $entry->set('in_lifecycle', false);
            $entry->set('object_model', 'ArchiveBinaries');
            $entry->set(
                'object_foreign_key',
                $entity->get('archive_binary_id')
            );
            $EventLogs->saveOrFail($entry);
            $createArchiveBinaryEvent = new Premis\Event(
                'archivebinary_add',
                'local',
                'EventLogs:' . $entry->id
            );
            $createArchiveBinaryEvent->detail = __("stockage d'un fichier");
            $createArchiveBinaryEvent->outcome = 'success';
            $createArchiveBinaryEvent->outcomeDetail = $details;
            $createArchiveBinaryEvent->dateTime = $created->format(DATE_RFC3339);
            $createArchiveBinaryEvent->addAgent($agent);
            $createArchiveBinaryEvent->addObject($binary);
            $events .= $createArchiveBinaryEvent;
        }
        $dom2 = new DOMDocument();
        $dom2->formatOutput = true;
        $dom2->preserveWhiteSpace = false;
        $xml = $dom->saveXML();
        $firstEvent = strpos($xml, '<event');
        $firstAgent = strpos($xml, '<agent');
        $oldEvents = substr($xml, $firstEvent, $firstAgent - $firstEvent);
        $oldAgents = substr($xml, $firstAgent);
        $dom2->loadXML(
            substr(
                $xml,
                0,
                $firstEvent
            ) . $objects . $oldEvents . $events . $oldAgents
        );
        if ($dom2->schemaValidate(PREMIS_V3)) {
            $this->out('Premis OK');
        } else {
            $this->out('BAD Premis');
            throw new Exception();
        }
        return $dom2;
    }

    /**
     * Donne l'Agent lié au créateur de l'archive
     * @return Premis\Agent
     */
    private function getUserAgent(): Premis\Agent
    {
        /** @var User $user */
        $user = Hash::get($this->Archive, 'transfers.0.user');
        return $user->toPremisAgent();
    }

    /**
     * Upload le transfert lié
     * @return void
     * @throws VolumeException
     */
    private function uploadTransfer()
    {
        /** @var EntityInterface $transfer */
        $transfer = Hash::get($this->Archive, 'transfers.0');
        $loc = TableRegistry::getTableLocator();
        $ArchiveFiles = $loc->get('ArchiveFiles');
        $transferArchiveFiles = $ArchiveFiles->find()
            ->where(
                [
                    'archive_id' => $this->Archive->id,
                    'type' => ArchiveFilesTable::TYPE_TRANSFER,
                ]
            )
            ->orderBy(['id']);
        // on cherche le dernier index utilisé pour définir le filename
        $index = 1;
        /** @var EntityInterface $transferArchiveFile */
        foreach ($transferArchiveFiles as $transferArchiveFile) {
            if (preg_match('/^.*_transfer_(\d+)\.xml$/', $transferArchiveFile->get('filename'), $m)) {
                $index = max($index, (int)$m[1]);
            }
        }
        $index++;
        $filename = sprintf(
            '%s_transfer_%02d.xml',
            $this->Archive->get('archival_agency_identifier'),
            $index
        );
        $storedFile = $this->VolumeManager->fileUpload(
            $transfer->get('xml'),
            sprintf(
                '%s/management_data/%s',
                $this->Archive->get('storage_path'),
                $filename
            )
        );
        $archiveFile = $ArchiveFiles->newEntity(
            [
                'archive_id' => $this->Archive->get('id'),
                'stored_file_id' => $storedFile->get('id'),
                'is_integrity_ok' => null,
                'type' => ArchiveFilesTable::TYPE_TRANSFER,
                'filename' => $filename,
            ]
        );
        $ArchiveFiles->saveOrFail($archiveFile);

        $transfer->set('archive_file_id', $transferArchiveFile->get('id'));
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        $Transfers->saveOrFail($transfer);
    }

    /**
     * Applique les transitions
     * @return void
     * @throws DateInvalidTimeZoneException
     */
    private function finalise()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $Archives->transitionOrFail(
            $this->Archive,
            ArchivesTable::T_FINISH
        );
        $Archives->saveOrFail($this->Archive);
        $ArchiveUnits->transitionAll(
            ArchiveUnitsTable::T_FINISH,
            ['archive_id' => $this->Archive->id]
        );
        $transferCompleted = $Archives->find()
                ->innerJoinWith('Transfers')
                ->where(
                    [
                        'Transfers.id' => Hash::get($this->Archive, 'transfers.0.id'),
                        'Archives.state IN' => [
                            ArchivesTable::S_CREATING,
                            ArchivesTable::S_DESCRIBING,
                            ArchivesTable::S_STORING,
                            ArchivesTable::S_MANAGING,
                        ],
                    ]
                )
                ->count() === 0;
        if ($transferCompleted) {
            /** @var TransfersTable $Transfers */
            $Transfers = $loc->get('Transfers');
            $transfer = Hash::get($this->Archive, 'transfers.0');
            $Transfers->transitionOrFail(
                $transfer,
                TransfersTable::T_ACCEPT
            );
            $Transfers->saveOrFail($transfer);
        }
        $this->setEventsInLifecycle();
    }

    /**
     * Passe le champ in_lifecycle à true après création du fichier lifecycle
     */
    private function setEventsInLifecycle()
    {
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $EventLogs->updateAll(
            ['in_lifecycle' => true],
            [
                'object_model' => 'Transfers',
                'object_foreign_key' => Hash::get($this->Archive, 'transfers.0.id'),
            ]
        );
        $EventLogs->updateAll(
            ['in_lifecycle' => true],
            [
                'object_model' => 'Archives',
                'object_foreign_key' => $this->Archive->id,
            ]
        );
        $ArchiveFiles = TableRegistry::getTableLocator()->get('ArchiveFiles');
        $query = $ArchiveFiles->find()
            ->select(['id'])
            ->where(['archive_id' => $this->Archive->id]);
        $EventLogs->updateAll(
            ['in_lifecycle' => true],
            [
                'object_model' => 'ArchiveFiles',
                'object_foreign_key IN' => $query,
            ]
        );
        $ArchiveBinaries = TableRegistry::getTableLocator()->get(
            'ArchiveBinaries'
        );
        $query = $ArchiveBinaries->find()
            ->select(['id'])
            ->innerJoinWith('ArchiveUnits')
            ->where(['archive_id' => $this->Archive->id]);
        $EventLogs->updateAll(
            ['in_lifecycle' => true],
            [
                'object_model' => 'ArchiveBinaries',
                'object_foreign_key IN' => $query,
            ]
        );
    }

    /**
     * S'il existe un transfert lié en attente, il est lancé à la suite
     * @param int $archive_id
     * @return void
     */
    private function createJobForNextTransfer(int $archive_id): void
    {
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        $nextTransfers = $Transfers->find()
            ->where(
                [
                    'Transfers.composite_id' => $archive_id,
                    'Transfers.state' => TransfersTable::S_ARCHIVING,
                ]
            )
            ->orderBy(['Transfers.id']);
        if ($nextTransfers->count() === 0) {
            return;
        }

        $BeanstalkJobs = $loc->get('BeanstalkJobs');
        /** @var EntityInterface $nextTransfer */
        foreach ($nextTransfers as $nextTransfer) {
            $exists = $BeanstalkJobs->exists(
                [
                    'tube' => 'archive',
                    'object_model' => 'Transfer',
                    'object_foreign_key' => $nextTransfer->id,
                ]
            );
            if ($exists) {
                continue;
            }
            $job = [
                'transfer_id' => $nextTransfer->id,
                'user_id' => $nextTransfer->get('created_user_id'),
                'composite_id' => $archive_id,
            ];
            $this->emit('archive', $job);
            $this->out(
                __(
                    "Nouveau job archive créé pour le transfert id={0}",
                    $nextTransfer->id
                )
            );
            break;
        }
    }

    /**
     * Pout chaque archives d'un transfert, créé les fichiers de management
     * @param int $archive_id
     * @throws CantWorkException
     * @throws DOMException
     * @throws ServerException
     * @throws VolumeException
     * @throws ZMQSocketException
     * @throws DateInvalidTimeZoneException
     */
    private function createArchiveManagementForArchive($archive_id)
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');
        $ArchiveFiles = $loc->get('ArchiveFiles');
        $this->Archive = $Archives->find()
            ->where(
                [
                    'Archives.id' => $archive_id,
                    'Archives.state' => ArchivesTable::S_STORING,
                ]
            )
            ->contain(
                [
                    'ArchivalAgencies' => ['Configurations'],
                    'Transfers' => ['Users'],
                    'SecureDataSpaces' => ['Volumes'],
                ]
            )
            ->first();
        if (!$this->Archive) {
            $this->out(
                __("Impossible de trouver l'archive id={0}", $archive_id)
            );
            throw new CantWorkException();
        }
        $this->VolumeManager = new VolumeManager(
            $this->Archive->get('secure_data_space_id')
        );
        $count = $ArchiveFiles->find()
            ->where(['ArchiveFiles.archive_id' => $archive_id])
            ->count();
        if ($count) {
            $this->out(
                __n(
                    "Un archive_file existe déjà pour l'archive {0}.",
                    "Des archive_files existent déjà pour l'archive {0}.",
                    $count,
                    $this->Archive->get('id')
                )
            );
            throw new CantWorkException();
        }

        $conn = $ArchiveFiles->getConnection();
        $conn->begin();
        $Archives->transitionOrFail($this->Archive, ArchivesTable::T_MANAGE);
        $Archives->saveOrFail($this->Archive);

        if ($this->createArchiveFiles()) {
            $this->finalise();
            $conn->commit();
            Utility::get('Notify')->emit(
                'archive_state_update',
                [
                    'id' => $this->Archive->id,
                    'state' => $this->Archive->get('state'),
                    'statetrad' => $this->Archive->get('statetrad'),
                    'is_large' => $this->Archive->get('management_size') > 5000000,
                ]
            );
        } else {
            $conn->rollback();
            throw new Exception('failed');
        }
    }

    /**
     * Créer le fichier de description d'archive, le fichier de cycle de vie et le fichier de transfert
     * @return bool
     * @throws VolumeException
     * @throws ServerException|DOMException
     */
    private function createArchiveFiles(): bool
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');

        $this->identifier = $this->Archive->get('archival_agency_identifier');
        $this->basePath = $this->Archive->get('storage_path') . '/management_data/';
        $this->VolumeManager = new VolumeManager(
            $this->Archive->get('secure_data_space_id')
        );
        $volumes = $this->VolumeManager->getVolumes();
        foreach (['description', 'lifecycle', 'transfer'] as $type) {
            $ref = sprintf(
                '%s%s_%s.xml',
                $this->basePath,
                urlencode($this->identifier),
                $type
            );
            foreach ($volumes as $volume_id => $volume) {
                if ($volume->fileExists($ref)) {
                    $this->out(
                        __(
                            "Le fichier {0} existe déjà sur le volume {1} et a été supprimé",
                            $ref,
                            $volume_id
                        )
                    );
                    $volume->fileDelete($ref);
                }
            }
        }

        /** @var Transfer $transfer */
        $transfer = Hash::get($this->Archive, 'transfers.0');
        $premis = $this->generatePremis();
        $this->agent = $this->getUserAgent();
        $lifecycle = $this->saveArchiveFile(
            $premis,
            'lifecycle',
            $premis->saveXML()
        );
        $this->out(__("Vérification des métadonnées de fichiers..."));
        $fn = function ($filename, $i, $count) {
            if ($i % 100 === 0) {
                $this->out(sprintf('%d / %d', $i, $count));
            }
        };
        $this->saveArchiveFile(
            $premis,
            'description',
            $transfer->getArchiveXml($this->Archive->get('id'), $fn)
        );
        $transferArchiveFile = $this->saveArchiveFile(
            $premis,
            'transfer',
            file_get_contents($transfer->get('xml'))
        );
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        $transfer->set('archive_file_id', $transferArchiveFile->get('id'));
        $Transfers->saveOrFail($transfer);
        /** @var OrgEntity $archivalAgency */
        $archivalAgency = $this->Archive->get('archival_agency');
        if ($archivalAgency->getConfig(ConfigurationsTable::CONF_ATTEST_ARCHIVING_CERT) === 'always') {
            $this->out(__("Création de l'attestation d'archivage..."));
            $this->saveArchiveFile(
                $premis,
                'archiving_certificate',
                $Archives->renderArchivingCertificatePdf($this->Archive->id),
                'pdf'
            );
        }

        $size = $this->Archive->get('management_size') ?: 0;
        $count = $this->Archive->get('management_count') ?: 0;

        $newPremis = $premis->saveXML();
        $size += strlen($newPremis) - $lifecycle->get('size');
        $this->VolumeManager->set(
            $lifecycle->get('stored_file_id'),
            $newPremis
        );
        $this->Archive->set('management_size', $size);
        $this->Archive->set('management_count', $count);

        return (bool)$Archives->save($this->Archive);
    }

    /**
     * Ajoute un fichier de management en bdd, dans le premis (lifecycle)
     * et le journal d'evenements
     * @param DOMDocument $premis
     * @param string      $type
     * @param string      $content
     * @param string      $ext
     * @return EntityInterface
     * @throws VolumeException
     * @throws DOMException
     */
    private function saveArchiveFile(
        DOMDocument $premis,
        string $type,
        string $content,
        string $ext = 'xml'
    ): EntityInterface {
        $loc = TableRegistry::getTableLocator();
        $ArchiveFiles = $loc->get('ArchiveFiles');
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $user_id = Hash::get($this->Archive, 'transfers.0.user_id');
        $filename = urlencode($this->identifier) . '_' . $type . '.' . $ext;
        $exists = $ArchiveFiles->find()
            ->where(
                [
                    'archive_id' => $this->Archive->get('id'),
                    'type' => $type,
                ]
            )
            ->first();
        if ($exists) {
            $this->out(
                __(
                    "Un fichier de type {0} existe déjà pour l'archive {1}",
                    $type,
                    $this->Archive->get('id')
                )
            );
            return $exists;
        }
        $this->deleteIfExists($this->basePath . $filename);
        $storedFile = $this->VolumeManager->filePutContent(
            $this->basePath . $filename,
            $content
        );
        $archiveFile = $ArchiveFiles->newEntity(
            [
                'archive_id' => $this->Archive->get('id'),
                'stored_file_id' => $storedFile->get('id'),
                'is_integrity_ok' => null,
                'type' => $type,
                'filename' => $filename,
            ]
        );
        $this->Archive->set(
            'management_size',
            $this->Archive->get('management_size') + $storedFile->get('size')
        );
        $this->Archive->set(
            'management_count',
            $this->Archive->get('management_count') + 1
        );
        if (!$ArchiveFiles->save($archiveFile)) {
            $this->out(
                $msg = __("Erreur lors de la sauvegarde d'un fichier d'archive")
            );
            $this->out(Debugger::exportVar($archiveFile->getErrors()));
            throw new Exception($msg);
        }
        $binary = new Premis\File(
            'Extensible Markup Language',
            'local',
            'ArchiveFiles:' . $archiveFile->get('id')
        );
        $binary->originalName = $storedFile->get('name');
        $binary->messageDigest = $storedFile->get('hash');
        $binary->formatRegistryName = 'pronom';
        $binary->formatRegistryKey = 'fmt/101';
        $binary->relationship = [
            'relatedObjectIdentifierType' => 'local',
            'relatedObjectIdentifierValue' => 'Archives:' . $this->Archive->get('id'),
        ];
        $binary->storages[] = [
            'contentLocationType' => 'local',
            'contentLocationValue' => 'SecureDataSpaces:' . $storedFile->get('secure_data_space_id'),
            'storageMedium' => Hash::get($this->Archive, 'secure_data_spaces.name'),
        ];
        $entry = $EventLogs->newEntry(
            'archivefile_add',
            'success',
            $details = __(
                "Stockage du fichier ''{0}'' sur l'espace de conservation ''{1}''",
                $filename,
                Hash::get($this->Archive, 'secure_data_space.name')
            ),
            $user_id,
            $archiveFile
        );
        $entry->set('in_lifecycle', false);
        $EventLogs->saveOrFail($entry);
        $event = new Premis\Event(
            'archivefile_add',
            'local',
            'EventLogs:' . $entry->id
        );
        $event->detail = __("stockage du fichier");
        $event->outcome = 'success';
        $event->outcomeDetail = $details;
        $date = $storedFile->get('created');
        $event->dateTime = ($date instanceof DateTimeInterface || $date instanceof CakeDate)
            ? $date->format(DATE_RFC3339)
            : $date;
        $event->addAgent($this->agent);
        $event->addObject($binary);
        Premis::appendEvent($premis, $event);
        return $archiveFile;
    }

    /**
     * Supprime le fichier s'il existe déjà
     * @param string $storageReference
     * @throws VolumeException
     */
    private function deleteIfExists(string $storageReference)
    {
        foreach ($this->VolumeManager->getVolumes() as $volume_id => $driver) {
            if ($driver->fileExists($storageReference)) {
                $this->out(
                    __(
                        "Le fichier {0} existe déjà sur le volume {1} et a été supprimé",
                        $storageReference,
                        $volume_id
                    )
                );
                $driver->fileDelete($storageReference);
            }
        }
    }

    /**
     * Ajoute les indicateurs
     * @param EntityInterface $archive
     */
    private function saveIndicators(EntityInterface $archive)
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveIndicators = $loc->get('ArchiveIndicators');
        $entity = $ArchiveIndicators->newEntity(
            [
                'archive_date' => new CakeDateTime(),
                'archival_agency_id' => $archive->get('archival_agency_id'),
                'transferring_agency_id' => $archive->get('transferring_agency_id'),
                'originating_agency_id' => $archive->get('originating_agency_id'),
                'archive_count' => 1,
                'units_count' => $archive->get('units_count'),
                'original_count' => $archive->get('original_count'),
                'original_size' => $archive->get('original_size'),
                'timestamp_count' => $archive->get('timestamp_count'),
                'timestamp_size' => $archive->get('timestamp_size'),
                'preservation_count' => $archive->get('preservation_count'),
                'preservation_size' => $archive->get('preservation_size'),
                'dissemination_count' => $archive->get('dissemination_count'),
                'dissemination_size' => $archive->get('dissemination_size'),
                'agreement_id' => $archive->get('agreement_id'),
                'profile_id' => $archive->get('profile_id'),
            ]
        );
        $ArchiveIndicators->saveOrFail($entity);
    }
}
