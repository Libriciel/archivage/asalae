<?php

/**
 * Asalae\Worker\MailWorker
 */

namespace Asalae\Worker;

use Asalae\Model\Entity\Mail;
use Asalae\Model\Table\MailsTable;
use AsalaeCore\View\AppView;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;
use Cake\View\Helper\UrlHelper;
use DateTime;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Envoi des mails
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MailWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var MailsTable
     */
    private $Mails;

    /**
     * WorkerInterface constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $this->Mails = TableRegistry::getTableLocator()->get('Mails');
    }

    /**
     * Effectue le travail voulu
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        /** @var Mail $mail */
        $mail = $this->Mails->get($data['mail_id']);
        if ($debugMailUrl = $this->debugMail($mail)) {
            $this->out(__("DEBUG - Afficher le mail : {0}", $debugMailUrl));
        } else {
            $mail->send();
            // on s'assure de ne pas envoyer des mails trop vite
            sleep(1);//NOSONAR
        }
    }

    /**
     * Si le debug est activé, renvoi une url pour consulter le mail
     * @param Mail $mailEntity
     * @return string|null url de consultation
     * @throws Exception
     */
    public function debugMail(Mail $mailEntity): ?string
    {
        /** @var Mailer $email */
        $email = $mailEntity->getMailer();
        if (Configure::read('debug_mails')) {
            $basePath = Configure::read('debug_mails_basepath', TMP . 'debug_mails');
            $filename = (new DateTime())->format('Ymd_H-i') . uniqid('_sendTestMail_') . '.html';
            Filesystem::dumpFile(
                $basePath . DS . $filename,
                $email->getMessage()->getBodyHtml()
            );
            $url = new UrlHelper(new AppView());
            return $url->build('/devs/debug-mail/' . base64_encode($filename), ['fullBase' => true]);
        }
        return null;
    }
}
