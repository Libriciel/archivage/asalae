<?php

/**
 * Asalae\Worker\BatchTreatmentWorker
 */

namespace Asalae\Worker;

use Asalae\Exception\GenericException;
use Asalae\Model\Entity\ArchiveKeyword;
use Asalae\Model\Entity\BatchTreatment;
use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Entity\PublicDescriptionArchiveFile;
use Asalae\Model\Entity\StoredFile;
use Asalae\Model\Table\AccessRuleCodesTable;
use Asalae\Model\Table\AccessRulesTable;
use Asalae\Model\Table\AppraisalRuleCodesTable;
use Asalae\Model\Table\AppraisalRulesTable;
use Asalae\Model\Table\ArchiveBinariesArchiveUnitsTable;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveDescriptionsTable;
use Asalae\Model\Table\ArchiveIndicatorsTable;
use Asalae\Model\Table\ArchiveKeywordsTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\PublicDescriptionArchiveFilesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\DataType\CommaArrayString;
use AsalaeCore\DataType\JsonString;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Form\MessageSchema\MessageSchema;
use AsalaeCore\Form\MessageSchema\Seda02Schema;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Form\MessageSchema\Seda21Schema;
use AsalaeCore\Form\MessageSchema\Seda22Schema;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Asalae\Model\Table\ArchiveUnitsBatchTreatmentsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\BatchTreatmentsTable;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInterval;
use DateTime;
use DateTimeInterface;
use DOMElement;
use DOMNode;
use Exception;
use FileConverters\Exception\FileNotReadableException;
use FileConverters\Utility\DetectFileFormat;
use FileConverters\Utility\FileConverters;
use Throwable;

/**
 * Transferts sortant
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class BatchTreatmentWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var AccessRuleCodesTable
     */
    private $AccessRuleCodes;
    /**
     * @var AccessRulesTable
     */
    private $AccessRules;
    /**
     * @var AppraisalRuleCodesTable
     */
    private $AppraisalRuleCodes;
    /**
     * @var AppraisalRulesTable
     */
    private $AppraisalRules;
    /**
     * @var ArchiveBinariesTable
     */
    private $ArchiveBinaries;
    /**
     * @var ArchiveBinariesArchiveUnitsTable
     */
    private $ArchiveBinariesArchiveUnits;
    /**
     * @var ArchiveDescriptionsTable
     */
    private $ArchiveDescriptions;
    /**
     * @var ArchiveIndicatorsTable
     */
    private $ArchiveIndicators;
    /**
     * @var ArchiveKeywordsTable
     */
    private $ArchiveKeywords;
    /**
     * @var ArchiveUnitsTable
     */
    private $ArchiveUnits;
    /**
     * @var ArchiveUnitsBatchTreatmentsTable
     */
    private $ArchiveUnitsBatchTreatments;
    /**
     * @var ArchivesTable
     */
    private $Archives;
    /**
     * @var BatchTreatmentsTable
     */
    private $BatchTreatments;
    /**
     * @var EventLogsTable
     */
    private $EventLogs;
    /**
     * @var PublicDescriptionArchiveFilesTable
     */
    private $PublicDescriptionArchiveFiles;
    /**
     * @var BatchTreatment|EntityInterface
     */
    private $batchTreatment;
    /**
     * @var array
     */
    private $btData;
    /**
     * @var VolumeManager[]
     */
    private $dataSpaces;

    /**
     * WorkerInterface constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $loc = TableRegistry::getTableLocator();
        $this->AccessRuleCodes = $loc->get('AccessRuleCodes');
        $this->AccessRules = $loc->get('AccessRules');
        $this->AppraisalRuleCodes = $loc->get('AppraisalRuleCodes');
        $this->AppraisalRules = $loc->get('AppraisalRules');
        $this->ArchiveBinaries = $loc->get('ArchiveBinaries');
        $this->ArchiveBinariesArchiveUnits = $loc->get('ArchiveBinariesArchiveUnits');
        $this->ArchiveDescriptions = $loc->get('ArchiveDescriptions');
        $this->ArchiveIndicators = $loc->get('ArchiveIndicators');
        $this->ArchiveKeywords = $loc->get('ArchiveKeywords');
        $this->ArchiveUnits = $loc->get('ArchiveUnits');
        $this->ArchiveUnitsBatchTreatments = $loc->get('ArchiveUnitsBatchTreatments');
        $this->Archives = $loc->get('Archives');
        $this->BatchTreatments = $loc->get('BatchTreatments');
        $this->EventLogs = $loc->get('EventLogs');
        $this->PublicDescriptionArchiveFiles = $loc->get('PublicDescriptionArchiveFiles');
    }

    /**
     * Effectue le travail voulu
     * @param mixed $data
     * @throws Exception|Throwable
     */
    public function work($data)
    {
        if (empty($data['batch_treatment_id'])) {
            $this->out(__("Job vide"));
            throw new CantWorkException();
        }
        $this->dataSpaces = [];
        $this->batchTreatment = $this->BatchTreatments->get($data['batch_treatment_id']);
        // FIXME solution temporaire, changer la gestion des erreurs et des workers killed
        switch ($this->batchTreatment->get('state')) {
            case BatchTreatmentsTable::S_ABORTED:
                $this->BatchTreatments->transition($this->batchTreatment, BatchTreatmentsTable::T_RENEW);
                break;
            case BatchTreatmentsTable::S_PROCESSING:
                $this->BatchTreatments->transition($this->batchTreatment, BatchTreatmentsTable::T_CANCEL);
                break;
            case BatchTreatmentsTable::S_READY:
                break;
            default:
                throw new GenericException('invalid state: ' . $this->batchTreatment->get('state'));
        }
        $this->BatchTreatments->transition($this->batchTreatment, BatchTreatmentsTable::T_PROCESS);
        $this->BatchTreatments->saveOrFail($this->batchTreatment);

        $count = $this->ArchiveUnitsBatchTreatments->find()
            ->where(['batch_treatment_id' => $this->batchTreatment->id])
            ->count();
        $this->out(__("Nombre d'unités d'archives dans le traitement: {0}", $count));

        try {
            $this->checkAvailables();

            $this->btData = (array)$this->batchTreatment->get('data');
            switch (Hash::get($this->btData, 'type')) {
                case 'appraisal':
                    $this->appraisal();
                    break;
                case 'restriction':
                    $this->restriction();
                    break;
                case 'name':
                    $this->name();
                    break;
                case 'description':
                    $this->description();
                    break;
                case 'keyword':
                    $this->keyword();
                    break;
                case 'convert':
                    $this->convert();
                    break;
                default:
                    throw new GenericException('invalid data: id=' . $this->batchTreatment->id);
            }
        } catch (Throwable $e) {
            $conn = $this->BatchTreatments->getConnection();
            while ($conn->inTransaction()) {
                $conn->rollback();
            }
            $this->BatchTreatments->transition($this->batchTreatment, BatchTreatmentsTable::T_CANCEL);
            $this->BatchTreatments->saveOrFail($this->batchTreatment);
            throw $e;
        }
        $count = $this->ArchiveUnitsBatchTreatments->find()
            ->where(['batch_treatment_id' => $this->batchTreatment->id])
            ->count();
        $this->BatchTreatments->transition(
            $this->batchTreatment,
            $count === 0
                ? BatchTreatmentsTable::T_TERMINATE
                : BatchTreatmentsTable::T_CANCEL
        );
        $this->BatchTreatments->saveOrFail($this->batchTreatment);
        $msg = __("Traitement terminé") . ' '
            . ($count === 0 ? __("avec succès") : __("en échec"));
        $this->out($msg);
        Notify::send(
            $this->userId,
            [],
            __("<h4>Worker Traitements par lots</h4>") . $msg,
            $count === 0 ? 'alert-info' : 'alert-warning'
        );
        $this->out(__("notification envoyée à {0}: {1}", 'user_' . $this->userId, $msg));
    }

    /**
     * Affiche des erreurs pour les ArchiveUnits non en état available
     */
    private function checkAvailables()
    {
        $query = $this->queryArchiveUnits()
            ->select(['link_id' => 'ArchiveUnitsBatchTreatments.id'])
            ->enableAutoFields()
            ->where(
                [
                    'ArchiveUnitsBatchTreatments.batch_treatment_id' => $this->batchTreatment->id,
                    'OR' => [
                        'ArchiveUnits.state !=' => 'available',
                        'Archives.state !=' => 'available',
                    ],
                ],
                [],
                true
            );
        /** @var EntityInterface $au */
        foreach ($query as $au) {
            $m = $au->get('state') !== ArchiveUnitsTable::S_AVAILABLE
                ? __("L'unité d'archive")
                : __("L'Archive");
            $this->out(__("{0} n°{1} n'est pas en état disponible", $m, $au->id));
            $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $au->get('link_id')]);
        }
    }

    /**
     * Donne la liste des unités d'archives sous forme de query
     * @return Query
     */
    private function queryArchiveUnits(): Query
    {
        return $this->ArchiveUnits->find()
            ->innerJoinWith('Archives')
            ->innerJoinWith('ArchiveUnitsBatchTreatments')
            ->where(
                [
                    'ArchiveUnitsBatchTreatments.batch_treatment_id' => $this->batchTreatment->id,
                    'Archives.state' => ArchivesTable::S_AVAILABLE,
                    'ArchiveUnits.state' => ArchiveUnitsTable::S_AVAILABLE,
                ]
            )
            ->orderBy(['ArchiveUnits.lft']);
    }

    /**
     * Modification de la DUA/Sort final
     */
    private function appraisal()
    {
        $query = $this->queryArchiveUnits()
            ->select(['link_id' => 'ArchiveUnitsBatchTreatments.id'])
            ->where(['ArchiveUnits.xml_node_tagname NOT LIKE' => 'Document%'])
            ->enableAutoFields()
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        'AppraisalRules' => ['AppraisalRuleCodes'],
                        'Transfers',
                    ],
                    'AppraisalRules' => ['AppraisalRuleCodes'],
                    'ParentArchiveUnits' => [
                        'AppraisalRules' => ['AppraisalRuleCodes'],
                    ],
                    'ArchiveUnitsBatchTreatments',
                ]
            );
        $final = Hash::get($this->btData, 'appraisal__final');
        $code = Hash::get($this->btData, 'appraisal__duration');
        $rule = $code
            ? $this->AppraisalRuleCodes->find()
                ->select(['id', 'duration'])
                ->where(['code' => $code])
                ->firstOrFail()
            : null;
        $duration = $rule ? $rule->get('duration') : null;
        $startDate = Hash::get($this->btData, 'appraisal__final_start_date');
        $startDate = $startDate ? new DateTime($startDate) : null;
        $duaExpired = [];
        $conn = $this->BatchTreatments->getConnection();
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $archiveUnit) {
            $conn->begin();
            /** @var EntityInterface $appraisalRule */
            $appraisalRule = $archiveUnit->get('appraisal_rule');
            /** @var EntityInterface $parentAppraisalRule */
            $parentAppraisalRule = Hash::get($archiveUnit, 'parent_archive_unit.appraisal_rule');
            $oldAppraisalRule = $appraisalRule;

            $finalOk = $final === null || $appraisalRule->get('final') === $final;
            $durationOk = $duration === null
                || Hash::get($appraisalRule, 'appraisal_rule_code.duration') === $duration;
            $startOk = $startDate === null || $appraisalRule->get('start_date') === $startDate;

            // identique à la sélection
            if ($finalOk && $durationOk && $startOk) {
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                $conn->commit();
                continue;
            }

            $newEntity = true;
            // si parent !=
            if ($parentAppraisalRule && $parentAppraisalRule->id !== $appraisalRule->id) {
                $finalParentOk = $final === null || $parentAppraisalRule->get('final') === $final;
                $durationParentOk = $duration === null
                    || Hash::get($parentAppraisalRule, 'appraisal_rule_code.duration') === $duration;
                $startParentOk = $startDate === null
                    || $parentAppraisalRule->get('start_date') === $startDate;

                // si parent correspond au choix -> héritage
                if ($finalParentOk && $durationParentOk && $startParentOk) {
                    // propage la modifications aux enfants qui héritent du même appraisal_rule
                    $this->ArchiveUnits->updateAll(
                        ['appraisal_rule_id' => $parentAppraisalRule->id],
                        [
                            'archive_id' => $archiveUnit->get('archive_id'),
                            'lft >=' => $archiveUnit->get('lft'),
                            'rght <=' => $archiveUnit->get('rght'),
                            'appraisal_rule_id' => $appraisalRule->id,
                        ]
                    );
                    $appraisalRule = $parentAppraisalRule;
                    $newEntity = false;
                }
            }

            if ($newEntity) {
                $dataAppraisalRule = [
                    'final_action_code' => $final ?: $appraisalRule->get('final_action_code'),
                    'appraisal_rule_code_id' => $rule ? $rule->id : $appraisalRule->get('appraisal_rule_code_id'),
                    'start_date' => $startDate ?: $appraisalRule->get('start_date'),
                ];
                // si il a héritage et que use_parent_appraisal_rule est à false, on créer un appraisal_rule
                $appraisalRule = $this->AppraisalRules->newEntity($dataAppraisalRule, ['validate' => false]);
                $this->AppraisalRules->saveOrFail($appraisalRule);
                $appraisalRule->set(
                    'appraisal_rule_code',
                    $this->AppraisalRuleCodes->get($appraisalRule->get('appraisal_rule_code_id'))
                );
                $this->ArchiveUnits->updateAll(
                    ['appraisal_rule_id' => $appraisalRule->id],
                    [
                        'appraisal_rule_id' => $oldAppraisalRule->id,
                        'archive_id' => $archiveUnit->get('archive_id'),
                        'lft >=' => $archiveUnit->get('lft'),
                        'rght <=' => $archiveUnit->get('rght'),
                    ]
                );
                if (
                    $archiveUnit->get('parent_id') === null
                    && Hash::get($archiveUnit, 'archive.appraisal_rule_id') !== $appraisalRule->id
                ) {
                    $this->Archives->updateAll(
                        ['appraisal_rule_id' => $appraisalRule->id],
                        ['id' => $archiveUnit->get('archive_id')]
                    );
                }
            }

            // Supprime l'appraisal rule si il n'y y a plus de liens
            if ($oldAppraisalRule->get('has_many_count') === 0) {
                $this->AppraisalRules->delete($oldAppraisalRule);
            }

            /**
             * ================== Mise à jour de la description ================
             */
            $archiveUnit->set('appraisal_rule', $appraisalRule);
            if ($this->ArchiveUnits->updateDescriptionXml($archiveUnit)) {
                /**
                 * Mise à jour du cycle de vie
                 */
                $params = array_filter(
                    [
                        'final' => $final,
                        'duration' => $duration,
                        'start_date' => $startDate,
                    ]
                );
                foreach ($params as $key => $value) {
                    if ($value instanceof DateTimeInterface) {
                        $value = $value->format('Y-m-d');
                    }
                    $params[$key] = "$key=$value";
                }
                $entry = $this->EventLogs->newEntry(
                    'batch_treatment_appraisal',
                    'success',
                    __(
                        "Traitement par lot de la DUA/Sort final: {0}",
                        implode(', ', $params)
                    ),
                    $this->userId,
                    $archiveUnit
                );
                $this->EventLogs->saveOrFail($entry);
                /** @var EntityInterface $archive */
                $storedFile = Hash::get($archiveUnit, 'archive.lifecycle_xml_archive_file.stored_file', []);
                $duaExpired[$archiveUnit->get('archive_id')] = $archiveUnit->get('archive_id');
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                $conn->commit();
                if (!$this->Archives->updateLifecycleXml($storedFile, $entry)) {
                    throw new GenericException(
                        __(
                            "Echec lors de la mise à jour du cycle de vie de l'archive n°{0}",
                            $archiveUnit->get('archive_id')
                        )
                    );
                }
            } else {
                throw new GenericException(
                    __(
                        "Echec lors de la mise à jour de la description de l'archive n°{0}",
                        $archiveUnit->get('archive_id')
                    )
                );
            }
        }
        foreach ($duaExpired as $archive_id) {
            $this->ArchiveUnits->calcDuaExpiredRoot(
                ['ArchiveUnits.archive_id' => $archive_id]
            );
        }
    }

    /**
     * Modification de la communicabilité
     */
    private function restriction()
    {
        $query = $this->queryArchiveUnits()
            ->select(['link_id' => 'ArchiveUnitsBatchTreatments.id'])
            ->where(['ArchiveUnits.xml_node_tagname NOT LIKE' => 'Document%'])
            ->enableAutoFields()
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        'AccessRules' => ['AccessRuleCodes'],
                        'Transfers',
                    ],
                    'AccessRules' => ['AccessRuleCodes'],
                    'ParentArchiveUnits' => [
                        'AccessRules' => ['AccessRuleCodes'],
                    ],
                    'ArchiveUnitsBatchTreatments',
                ]
            );
        $code = Hash::get($this->btData, 'restriction__code');
        $rule = $code
            ? $this->AccessRuleCodes->find()
                ->select(['id'])
                ->where(['code' => $code])
                ->firstOrFail()
            : null;
        $startDate = Hash::get($this->btData, 'restriction__start_date');
        $startDate = $startDate ? new DateTime($startDate) : null;
        $conn = $this->BatchTreatments->getConnection();
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $archiveUnit) {
            $conn->begin();
            /** @var EntityInterface $accessRule */
            $accessRule = $archiveUnit->get('access_rule');
            /** @var EntityInterface $parentAccessRule */
            $parentAccessRule = Hash::get($archiveUnit, 'parent_archive_unit.access_rule');
            $oldAccessRule = $accessRule;

            $codeOk = $code === null || Hash::get($accessRule, 'access_rule_code.code') === $code;
            $startOk = $startDate === null || $accessRule->get('start_date') === $startDate;

            // identique à la sélection
            if ($codeOk && $startOk) {
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                $conn->commit();
                continue;
            }

            // si parent !=
            if ($parentAccessRule && $parentAccessRule->id !== $accessRule->id) {
                $codeParentOk = $code === null || $parentAccessRule->get('code') === $code;
                $startParentOk = $startDate === null
                    || $parentAccessRule->get('start_date') === $startDate;

                // si parent correspond au choix -> héritage
                if ($codeParentOk && $startParentOk) {
                    // propage la modifications aux enfants qui héritent du même access_rule
                    $this->ArchiveUnits->updateAll(
                        ['access_rule_id' => $parentAccessRule->id],
                        [
                            'archive_id' => $archiveUnit->get('archive_id'),
                            'lft >=' => $archiveUnit->get('lft'),
                            'rght <=' => $archiveUnit->get('rght'),
                            'access_rule_id' => $accessRule->id,
                        ]
                    );
                    $accessRule = $parentAccessRule;
                    $newEntity = false;
                } else {
                    $newEntity = true;
                }
            } else {
                $newEntity = true;
            }

            if ($newEntity) {
                $dataAccessRule = [
                    'access_rule_code_id' => $rule ? $rule->id : $accessRule->get('access_rule_code_id'),
                    'start_date' => $startDate ?: $accessRule->get('start_date'),
                ];
                // si il a héritage et que use_parent_access_rule est à false, on créer un access_rule
                $accessRule = $this->AccessRules->newEntity($dataAccessRule, ['validate' => false]);
                $this->AccessRules->saveOrFail($accessRule);
                $accessRule->set(
                    'access_rule_code',
                    $this->AccessRuleCodes->get($accessRule->get('access_rule_code_id'))
                );
                $this->ArchiveUnits->updateAll(
                    ['access_rule_id' => $accessRule->id],
                    [
                        'access_rule_id' => $oldAccessRule->id,
                        'archive_id' => $archiveUnit->get('archive_id'),
                        'lft >=' => $archiveUnit->get('lft'),
                        'rght <=' => $archiveUnit->get('rght'),
                    ]
                );
                if (
                    $archiveUnit->get('parent_id') === null
                    && Hash::get($archiveUnit, 'archive.access_rule_id') !== $accessRule->id
                ) {
                    $this->Archives->updateAll(
                        ['access_rule_id' => $accessRule->id],
                        ['id' => $archiveUnit->get('archive_id')]
                    );
                }
            }

            // Supprime l'access rule si il n'y y a plus de liens
            if ($oldAccessRule->get('has_many_count') === 0) {
                $this->AccessRules->delete($oldAccessRule);
            }

            /**
             * ================== Mise à jour de la description ================
             */
            $archiveUnit->set('access_rule', $accessRule);
            if ($this->ArchiveUnits->updateDescriptionXml($archiveUnit)) {
                /**
                 * Mise à jour du cycle de vie
                 */
                $params = array_filter(
                    [
                        'code' => $code,
                        'start_date' => $startDate,
                    ]
                );
                foreach ($params as $key => $value) {
                    if ($value instanceof DateTimeInterface) {
                        $value = $value->format('Y-m-d');
                    }
                    $params[$key] = "$key=$value";
                }
                $entry = $this->EventLogs->newEntry(
                    'batch_treatment_restriction',
                    'success',
                    __(
                        "Traitement par lot de la communicabilité: {0}",
                        implode(', ', $params)
                    ),
                    $this->userId,
                    $archiveUnit
                );
                $this->EventLogs->saveOrFail($entry);
                /** @var EntityInterface $archive */
                $storedFile = Hash::get($archiveUnit, 'archive.lifecycle_xml_archive_file.stored_file', []);
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                $conn->commit();
                if (!$this->Archives->updateLifecycleXml($storedFile, $entry)) {
                    throw new GenericException(
                        __(
                            "Echec lors de la mise à jour du cycle de vie de l'archive n°{0}",
                            $archiveUnit->get('archive_id')
                        )
                    );
                }
            } else {
                throw new GenericException(
                    __(
                        "Echec lors de la mise à jour de la description de l'archive n°{0}",
                        $archiveUnit->get('archive_id')
                    )
                );
            }
        }
    }

    /**
     * Modification du Name
     */
    private function name()
    {
        $query = $this->queryArchiveUnits()
            ->select(['link_id' => 'ArchiveUnitsBatchTreatments.id'])
            ->where(['ArchiveUnits.xml_node_tagname NOT LIKE' => 'Document%'])
            ->enableAutoFields()
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        'Transfers',
                    ],
                    'ArchiveUnitsBatchTreatments',
                ]
            );
        $nameResearch = Hash::get($this->btData, 'name__research');
        $nameReplace = Hash::get($this->btData, 'name__replace');
        $conn = $this->BatchTreatments->getConnection();
        $archives = [];
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $archiveUnit) {
            $match = mb_strpos($archiveUnit->get('name'), $nameResearch);
            if ($match === false) {
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                continue;
            }
            $conn->begin();

            $newName = str_replace($nameResearch, $nameReplace, $archiveUnit->get('name'));
            if ($newName !== $archiveUnit->get('name')) {
                $archiveUnit->set('name', $newName);
                $this->ArchiveUnits->save($archiveUnit);
                if ($archiveUnit->get('parent_id') === null) {
                    /** @var EntityInterface $archive */
                    $archive = $archiveUnit->get('archive');
                    $archive->set('name', $newName);
                    $this->Archives->save($archive);
                }
            }

            $archiveUnit->set('name', $newName);
            if ($this->ArchiveUnits->updateDescriptionXml($archiveUnit)) {
                /**
                 * Mise à jour du cycle de vie
                 */
                $params = array_filter(
                    [
                        'research' => $nameResearch,
                        'replace' => $nameReplace,
                    ]
                );
                foreach ($params as $key => $value) {
                    $params[$key] = "$key=$value";
                }
                $entry = $this->EventLogs->newEntry(
                    'batch_treatment_name',
                    'success',
                    __(
                        "Traitement par lot du nom: {0}",
                        implode(', ', $params)
                    ),
                    $this->userId,
                    $archiveUnit
                );
                $this->EventLogs->saveOrFail($entry);
                /** @var EntityInterface $archive */
                $storedFile = Hash::get($archiveUnit, 'archive.lifecycle_xml_archive_file.stored_file', []);
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                $conn->commit();
                if (!$this->Archives->updateLifecycleXml($storedFile, $entry)) {
                    throw new GenericException(
                        __(
                            "Echec lors de la mise à jour du cycle de vie de l'archive n°{0}",
                            $archiveUnit->get('archive_id')
                        )
                    );
                }
                $archives[$archiveUnit->get('archive_id')] = $archiveUnit->get('archive');
            } else {
                throw new GenericException(
                    __(
                        "Echec lors de la mise à jour de la description de l'archive n°{0}",
                        $archiveUnit->get('archive_id')
                    )
                );
            }
        }
        $this->updateArchives($archives);
    }

    /**
     * Met à jour les champs recherches et supprime la description publique
     * @param EntityInterface[] $archives
     * @throws VolumeException
     */
    private function updateArchives(array $archives)
    {
        foreach ($archives as $archive) {
            $this->ArchiveUnits->updateSearchField(
                ['Archives.id' => $archive->id]
            );
            $this->ArchiveUnits->updateFullSearchField(
                ['Archives.id' => $archive->id]
            );
            /** @var PublicDescriptionArchiveFile $archiveFile */
            $archiveFile = Hash::get($archive, 'archive.public_description_archive_file');
            if ($archiveFile) {
                $this->PublicDescriptionArchiveFiles->deleteOrFail($archiveFile);
                $manager = $this->getVolumeManager($archive);
                $manager->fileDelete(Hash::get($archiveFile, 'stored_file.name'));
                $archive->set('next_pubdesc');
                $this->Archives->saveOrFail($archive);
            }
        }
    }

    /**
     * Donne le VolumeManager lié à une archive
     * @param EntityInterface $archive
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(EntityInterface $archive): VolumeManager
    {
        if (!isset($this->dataSpaces[$archive->get('secure_data_space_id')])) {
            $this->dataSpaces[$archive->get('secure_data_space_id')]
                = new VolumeManager($archive->get('secure_data_space_id'));
        }
        return $this->dataSpaces[$archive->get('secure_data_space_id')];
    }

    /**
     * Modification de la Description
     */
    private function description()
    {
        $query = $this->queryArchiveUnits()
            ->select(['link_id' => 'ArchiveUnitsBatchTreatments.id'])
            ->where(['ArchiveUnits.xml_node_tagname NOT LIKE' => 'Document%'])
            ->enableAutoFields()
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        'Transfers',
                    ],
                    'ArchiveUnitsBatchTreatments',
                    'ArchiveDescriptions',
                ]
            );
        $descriptionResearch = Hash::get($this->btData, 'description__research');
        $descriptionReplace = Hash::get($this->btData, 'description__replace');
        $conn = $this->BatchTreatments->getConnection();
        $archives = [];
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $archiveUnit) {
            $description = Hash::get($archiveUnit, 'archive_description.description');
            $match = mb_strpos((string)$description, $descriptionResearch);
            if ($match === false) {
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                continue;
            }
            $conn->begin();

            $newDescription = str_replace($descriptionResearch, $descriptionReplace, $description);
            $this->ArchiveDescriptions->updateAll(
                ['description' => $newDescription],
                ['id' => Hash::get($archiveUnit, 'archive_description.id')]
            );
            if ($archiveUnit->get('parent_id') === null) {
                $this->Archives->updateAll(
                    ['description' => $newDescription],
                    ['id' => $archiveUnit->get('archive_id')]
                );
            }

            /** @var EntityInterface $archiveDescription */
            $archiveDescription = $archiveUnit->get('archive_description');
            $archiveDescription->set('description', $newDescription);
            if ($this->ArchiveUnits->updateDescriptionXml($archiveUnit)) {
                /**
                 * Mise à jour du cycle de vie
                 */
                $params = array_filter(
                    [
                        'research' => $descriptionResearch,
                        'replace' => $descriptionReplace,
                    ]
                );
                foreach ($params as $key => $value) {
                    $params[$key] = "$key=$value";
                }
                $entry = $this->EventLogs->newEntry(
                    'batch_treatment_description',
                    'success',
                    __(
                        "Traitement par lot de la description: {0}",
                        implode(', ', $params)
                    ),
                    $this->userId,
                    $archiveUnit
                );
                $this->EventLogs->saveOrFail($entry);
                /** @var EntityInterface $archive */
                $storedFile = Hash::get($archiveUnit, 'archive.lifecycle_xml_archive_file.stored_file', []);
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                $conn->commit();
                if (!$this->Archives->updateLifecycleXml($storedFile, $entry)) {
                    throw new GenericException(
                        __(
                            "Echec lors de la mise à jour du cycle de vie de l'archive n°{0}",
                            $archiveUnit->get('archive_id')
                        )
                    );
                }
                $archives[$archiveUnit->get('archive_id')] = $archiveUnit->get('archive');
            } else {
                throw new GenericException(
                    __(
                        "Echec lors de la mise à jour de la description de l'archive n°{0}",
                        $archiveUnit->get('archive_id')
                    )
                );
            }
        }
        $this->updateArchives($archives);
    }

    /**
     * Modification des mots-clés
     */
    private function keyword()
    {
        switch (Hash::get($this->btData, 'keyword__type')) {
            case 'replace':
                $this->keywordReplace();
                break;
            case 'add':
                $this->keywordAdd();
                break;
            case 'delete':
                $this->keywordDelete();
                break;
        }
    }

    /**
     * Remplacement d'un mot clé par un autre
     */
    private function keywordReplace()
    {
        $query = $this->queryArchiveUnits()
            ->select(['link_id' => 'ArchiveUnitsBatchTreatments.id'])
            ->where(['ArchiveUnits.xml_node_tagname NOT LIKE' => 'Document%'])
            ->enableAutoFields()
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        'Transfers',
                    ],
                    'ArchiveUnitsBatchTreatments',
                    'ArchiveKeywords' => function (Query $q) {
                        return $q->where(
                            ['content' => Hash::get($this->btData, 'keyword__research')]
                        );
                    },
                ]
            );
        $keywordReplace = Hash::get($this->btData, 'keyword__replace');
        $keywordResearch = Hash::get($this->btData, 'keyword__research');
        $conn = $this->BatchTreatments->getConnection();
        $archives = [];
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $archiveUnit) {
            if (count($archiveUnit->get('archive_keywords')) === 0) {
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                continue;
            }
            $conn->begin();
            /** @var DescriptionXmlArchiveFile $desc */
            $desc = Hash::get($archiveUnit, 'archive.description_xml_archive_file');
            $util = new DOMUtility($desc->getDom());
            $auNode = $util->node($archiveUnit->get('xpath'));
            if (!$auNode) {
                throw new GenericException(
                    'archive_unit not found in description.xml (archive_unit=' . $archiveUnit->id . ')'
                );
            }

            /**
             * modifie le mot clé en base et dans le xml
             */
            /** @var EntityInterface $archiveKeyword */
            foreach ($archiveUnit->get('archive_keywords') as $archiveKeyword) {
                $this->updateKeyword(
                    $util,
                    $auNode,
                    $archiveKeyword,
                    $keywordReplace
                );
            }
            if ($this->ArchiveUnits->saveAsync($archiveUnit, $util, false)) {
                /**
                 * Mise à jour du cycle de vie
                 */
                $params = array_filter(
                    [
                        'research' => $keywordResearch,
                        'replace' => $keywordReplace,
                    ]
                );
                foreach ($params as $key => $value) {
                    $params[$key] = "$key=$value";
                }
                $entry = $this->EventLogs->newEntry(
                    'batch_treatment_keyword_replace',
                    'success',
                    __(
                        "Traitement par lot des mots-clés (remplacement): {0}",
                        implode(', ', $params)
                    ),
                    $this->userId,
                    $archiveUnit
                );
                $this->EventLogs->saveOrFail($entry);
                /** @var EntityInterface $archive */
                $storedFile = Hash::get($archiveUnit, 'archive.lifecycle_xml_archive_file.stored_file', []);
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                $conn->commit();
                if (!$this->Archives->updateLifecycleXml($storedFile, $entry)) {
                    throw new GenericException(
                        __(
                            "Echec lors de la mise à jour du cycle de vie de l'archive n°{0}",
                            $archiveUnit->get('archive_id')
                        )
                    );
                }
                $archives[$archiveUnit->get('archive_id')] = $archiveUnit->get('archive');
            } else {
                throw new GenericException(
                    __(
                        "Echec lors de la mise à jour de la description de l'archive n°{0}",
                        $archiveUnit->get('archive_id')
                    )
                );
            }
        }
        $this->updateArchives($archives);
    }

    /**
     * Update Keyword
     * @param DOMUtility      $util
     * @param DOMNode         $auNode
     * @param EntityInterface $archiveKeyword
     * @param mixed           $keywordReplace
     * @return void
     */
    protected function updateKeyword(
        DOMUtility $util,
        DOMNode $auNode,
        EntityInterface $archiveKeyword,
        $keywordReplace
    ): void {
        $keywordQuery = $util->xpath->query(
            ArchiveKeyword::DOM_REL_APPEND_PATH[$util->namespace],
            $auNode
        );
        /** @var DOMElement $keywordContentNode */
        foreach ($keywordQuery as $keywordContentNode) {
            if ($keywordContentNode->nodeValue === $archiveKeyword->get('content')) {
                /** @var DOMElement $parentNode */
                $parentNode = $keywordContentNode->parentNode;
                // supprime le nœuds KeywordReference
                $keywordReference = $parentNode
                    ->getElementsByTagName('KeywordReference')
                    ->item(0);
                if ($keywordReference) {
                    $keywordContentNode->parentNode->removeChild($keywordReference);
                }
                // note: foreach ne fonctionne pas pour supprimer plusieurs attributs
                $attributes = $keywordContentNode->attributes;
                while ($attributes->length) {
                    $keywordContentNode->removeAttributeNode($attributes->item(0));
                }
                DOMUtility::setValue($keywordContentNode, $keywordReplace);
                $archiveKeyword->set('content', $keywordReplace);
                $archiveKeyword->set('reference');
                $this->ArchiveKeywords->saveOrFail($archiveKeyword);
                break;
            }
        }
    }

    /**
     * Ajout d'un mot clé
     */
    private function keywordAdd()
    {
        $query = $this->queryArchiveUnits()
            ->select(['link_id' => 'ArchiveUnitsBatchTreatments.id'])
            ->where(['ArchiveUnits.xml_node_tagname NOT LIKE' => 'Document%'])
            ->enableAutoFields()
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        'Transfers',
                    ],
                    'ArchiveUnitsBatchTreatments',
                    'ArchiveKeywords' => function (Query $q) {
                        return $q->where(
                            ['content' => Hash::get($this->btData, 'keyword__add')]
                        );
                    },
                    'ArchiveDescriptions' => [
                        'AccessRules' => ['AccessRuleCodes'],
                    ],
                    'ParentArchiveUnits' => [
                        'AccessRules' => ['AccessRuleCodes'],
                    ],
                ]
            );
        $keywordAdd = Hash::get($this->btData, 'keyword__add');
        $conn = $this->BatchTreatments->getConnection();
        $archives = [];
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $archiveUnit) {
            if (count($archiveUnit->get('archive_keywords')) > 0) {
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                continue;
            }
            $conn->begin();
            /** @var DescriptionXmlArchiveFile $desc */
            $desc = Hash::get($archiveUnit, 'archive.description_xml_archive_file');
            $util = new DOMUtility($desc->getDom());
            $auNode = $util->node($archiveUnit->get('xpath'));
            if (!$auNode) {
                throw new GenericException(
                    'archive_unit not found in description.xml (archive_unit=' . $archiveUnit->id . ')'
                );
            }
            /** @var Seda02Schema|Seda10Schema|Seda21Schema|Seda22Schema $schemaClassname */
            $schemaClassname = MessageSchema::getSchemaClassname($util->namespace);
            $descriptionTagname = $schemaClassname::getTagname('ContentDescription', 'Archive');

            /**
             * ajout de la description si elle n'existe pas
             */
            $descriptionNode = $util->node('ns:' . $descriptionTagname, $auNode);
            if (!$descriptionNode) {
                $descriptionNode = $util->appendChild(
                    $auNode,
                    $util->createElement($descriptionTagname)
                );
                if ($util->namespace === NAMESPACE_SEDA_02) {
                    $util->appendChild(
                        $descriptionNode,
                        $util->createElement(
                            $schemaClassname::getTagname('Language', 'Archive'),
                            'fr'
                        )
                    );
                } elseif ($util->namespace === NAMESPACE_SEDA_10) {
                    $util->appendChild(
                        $descriptionNode,
                        $util->createElement(
                            $schemaClassname::getTagname('DescriptionLevel', 'Archive'),
                            'item'
                        )
                    );
                    $util->appendChild(
                        $descriptionNode,
                        $util->createElement(
                            $schemaClassname::getTagname('Language', 'Archive'),
                            'fra'
                        )
                    );
                }
                // NOTE: pas de champ obligatoire en 2.1
            }
            $accessRule = Hash::get($archiveUnit, 'parent_archive_unit.access_rule');
            if (!$archiveUnit->get('archive_description')) {
                if (!$accessRule) {
                    $accessCode = $this->AccessRuleCodes->find()
                        ->where(['code' => 'AR038'])
                        ->firstOrFail();
                    $startDate = new DateTime();
                    $accessRule = $this->AccessRules->newEntity(
                        [
                            'access_rule_code_id' => $accessCode->id,
                            'start_date' => clone $startDate,
                            'end_date' => $startDate->add(new DateInterval($accessCode->get('duration'))),
                        ]
                    );
                    $this->AccessRules->saveOrFail($accessRule);
                }
                $archiveDescription = $this->ArchiveDescriptions->newEntity(
                    [
                        'archive_unit_id' => $archiveUnit->id,
                        'access_rule_id' => $accessRule->id,
                    ]
                );
                $this->ArchiveDescriptions->saveOrFail($archiveDescription);
                $archiveUnit->set('archive_description', $archiveDescription);
            } elseif (!$accessRule) {
                $accessRule = Hash::get($archiveUnit, 'archive_description.access_rule');
            }

            /**
             * Ajout du mot clé
             */
            $keywordsTagname = $schemaClassname::getTagname('Keyword', 'Archive');
            $index = $util->xpath->query('ns:' . $keywordsTagname, $descriptionNode)->count() + 1;
            $keyword = $this->ArchiveKeywords->newEntity(
                [
                    'archive_unit_id' => $archiveUnit->id,
                    'access_rule_id' => $accessRule->id,
                    'content' => $keywordAdd,
                    'xml_node_tagname' => "{$keywordsTagname}[$index]",
                ]
            );
            $keyword->set('access_rule', $accessRule);
            $this->ArchiveKeywords->saveOrFail($keyword, ['ignoreCallbacks' => true]);
            $keywordNode = $util->appendChild(
                $descriptionNode,
                $util->createElement($keywordsTagname)
            );
            $util->appendChild(
                $keywordNode,
                $util->createElement('KeywordContent', $keywordAdd)
            );

            if ($this->ArchiveUnits->saveAsync($archiveUnit, $util, false)) {
                /**
                 * Mise à jour du cycle de vie
                 */
                $params = array_filter(
                    [
                        'add' => $keywordAdd,
                    ]
                );
                foreach ($params as $key => $value) {
                    $params[$key] = "$key=$value";
                }
                $entry = $this->EventLogs->newEntry(
                    'batch_treatment_keyword_add',
                    'success',
                    __(
                        "Traitement par lot des mots-clés (ajout): {0}",
                        implode(', ', $params)
                    ),
                    $this->userId,
                    $archiveUnit
                );
                $this->EventLogs->saveOrFail($entry);
                /** @var EntityInterface $archive */
                $storedFile = Hash::get($archiveUnit, 'archive.lifecycle_xml_archive_file.stored_file', []);
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                $conn->commit();
                if (!$this->Archives->updateLifecycleXml($storedFile, $entry)) {
                    throw new GenericException(
                        __(
                            "Echec lors de la mise à jour du cycle de vie de l'archive n°{0}",
                            $archiveUnit->get('archive_id')
                        )
                    );
                }
                $archives[$archiveUnit->get('archive_id')] = $archiveUnit->get('archive');
            } else {
                throw new GenericException(
                    __(
                        "Echec lors de la mise à jour de la description de l'archive n°{0}",
                        $archiveUnit->get('archive_id')
                    )
                );
            }
        }
        $this->updateArchives($archives);
    }

    /**
     * Suppression d'un mot clé
     */
    private function keywordDelete()
    {
        $query = $this->queryArchiveUnits()
            ->select(['link_id' => 'ArchiveUnitsBatchTreatments.id'])
            ->where(['ArchiveUnits.xml_node_tagname NOT LIKE' => 'Document%'])
            ->enableAutoFields()
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        'Transfers',
                    ],
                    'ArchiveUnitsBatchTreatments',
                    'ArchiveKeywords' => function (Query $q) {
                        return $q->where(
                            ['content' => Hash::get($this->btData, 'keyword__delete')]
                        );
                    },
                ]
            );
        $keywordDelete = Hash::get($this->btData, 'keyword__delete');
        $conn = $this->BatchTreatments->getConnection();
        $archives = [];
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $archiveUnit) {
            if (count($archiveUnit->get('archive_keywords')) === 0) {
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                continue;
            }
            $conn->begin();
            /** @var DescriptionXmlArchiveFile $desc */
            $desc = Hash::get($archiveUnit, 'archive.description_xml_archive_file');
            $util = new DOMUtility($desc->getDom());
            $auNode = $util->node($archiveUnit->get('xpath'));
            if (!$auNode) {
                throw new GenericException(
                    'archive_unit not found in description.xml (archive_unit=' . $archiveUnit->id . ')'
                );
            }
            /**
             * modifi le mot clé en base et dans le xml
             */
            /** @var EntityInterface $archiveKeyword */
            $removed = false;
            foreach ($archiveUnit->get('archive_keywords') as $archiveKeyword) {
                if (
                    $removed
                    && preg_match('/^(.*)\[(\d+)]$/', $archiveKeyword->get('xml_node_tagname'), $m)
                ) {
                    $archiveKeyword->set('xml_node_tagname', $m[1] . '[' . ($m[2] - 1) . ']');
                    $this->ArchiveKeywords->saveOrFail($archiveKeyword);
                }
                $keywordQuery = $util->xpath->query(ArchiveKeyword::DOM_REL_APPEND_PATH[$util->namespace], $auNode);
                /** @var DOMElement $keywordContentNode */
                foreach ($keywordQuery as $keywordContentNode) {
                    if ($keywordContentNode->nodeValue === $archiveKeyword->get('content')) {
                        $keywordContentNode->parentNode->parentNode->removeChild($keywordContentNode->parentNode);
                        $this->ArchiveKeywords->deleteOrFail($archiveKeyword);
                        $removed = true;
                        break;
                    }
                }
            }

            if ($this->ArchiveUnits->saveAsync($archiveUnit, $util, false)) {
                /**
                 * Mise à jour du cycle de vie
                 */
                $params = array_filter(
                    [
                        'delete' => $keywordDelete,
                    ]
                );
                foreach ($params as $key => $value) {
                    $params[$key] = "$key=$value";
                }
                $entry = $this->EventLogs->newEntry(
                    'batch_treatment_keyword_delete',
                    'success',
                    __(
                        "Traitement par lot des mots-clés (suppression): {0}",
                        implode(', ', $params)
                    ),
                    $this->userId,
                    $archiveUnit
                );
                $this->EventLogs->saveOrFail($entry);
                /** @var EntityInterface $archive */
                $storedFile = Hash::get($archiveUnit, 'archive.lifecycle_xml_archive_file.stored_file', []);
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                $conn->commit();
                if (!$this->Archives->updateLifecycleXml($storedFile, $entry)) {
                    throw new GenericException(
                        __(
                            "Echec lors de la mise à jour du cycle de vie de l'archive n°{0}",
                            $archiveUnit->get('archive_id')
                        )
                    );
                }
                $archives[$archiveUnit->get('archive_id')] = $archiveUnit->get('archive');
            } else {
                throw new GenericException(
                    __(
                        "Echec lors de la mise à jour de la description de l'archive n°{0}",
                        $archiveUnit->get('archive_id')
                    )
                );
            }
        }
        $this->updateArchives($archives);
    }

    /**
     * Traitements sur les conversions
     */
    private function convert()
    {
        switch (Hash::get($this->btData, 'convert')) {
            case 'do':
                $this->convertDo();
                break;
            case 'delete':
                $this->convertDelete();
                break;
        }
    }

    /**
     * Lance la conversion sur les fichiers des archives units
     */
    private function convertDo()
    {
        // On commence par supprimer les unités d'archives qui ne sont pas liés à un fichier
        // supprime également les AU qui ont déjà des conversions
        $query = $this->queryArchiveUnits()
            ->select(['ArchiveUnitsBatchTreatments.id'], true)
            ->leftJoinWith('ArchiveBinaries')
            ->where(
                [
                    'OR' => [
                        'ArchiveBinaries.id IS' => null,
                        'ArchiveBinaries.type IN' => [
                            'dissemination_data',
                            'dissemination_timestamp',
                            'preservation_data',
                            'preservation_timestamp',
                        ],
                    ],
                ]
            );
        $this->ArchiveUnitsBatchTreatments->deleteAll(['id IN' => $query]);

        $query = $this->queryArchiveUnits()
            ->select(['link_id' => 'ArchiveUnitsBatchTreatments.id'])
            ->innerJoinWith('ArchiveBinaries')
            ->where(['ArchiveBinaries.type' => 'original_data'])
            ->enableAutoFields()
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        'ServiceLevels',
                        'Transfers',
                    ],
                    'ArchiveUnitsBatchTreatments',
                    'ArchiveBinaries' => function (Query $q) {
                        return $q->where(['ArchiveBinaries.type' => 'original_data'])
                            ->contain(['StoredFiles']);
                    },
                ]
            );
        $conn = $this->BatchTreatments->getConnection();
        $conversions = [];
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $archiveUnit) {
            /** @var EntityInterface $binary */
            $binary = Hash::get($archiveUnit, 'archive_binaries.0');
            if ($binary->get('app_meta') === null) {
                $this->updateAppMeta($archiveUnit->get('archive'), $binary);
            }

            /** @var JsonString $meta */
            $meta = $binary->get('app_meta');
            $meta = $meta->getArrayCopy();
            // s'il n'y a pas de category, il n'est pas possible de convertir le fichier
            if (empty($meta['category'])) {
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                continue;
            }

            $conn->begin();
            if ($this->checkConversions($binary, $archiveUnit->get('archive'))) {
                $conversions[$archiveUnit->get('archive_id')] = $archiveUnit->get('archive_id');

                $entry = $this->EventLogs->newEntry(
                    'batch_treatment_convert_do',
                    'success',
                    __("Traitement par lot des conversions (convertir)"),
                    $this->userId,
                    $archiveUnit
                );
                $this->EventLogs->saveOrFail($entry);
                /** @var EntityInterface $archive */
                $storedFile = Hash::get($archiveUnit, 'archive.lifecycle_xml_archive_file.stored_file', []);
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                $conn->commit();
                if (!$this->Archives->updateLifecycleXml($storedFile, $entry)) {
                    throw new GenericException(
                        __(
                            "Echec lors de la mise à jour du cycle de vie de l'archive n°{0}",
                            $archiveUnit->get('archive_id')
                        )
                    );
                }
            } else {
                $this->BatchTreatments->transition($this->batchTreatment, BatchTreatmentsTable::T_TERMINATE);
                $conn->commit();
            }
        }
        foreach ($conversions as $archive_id) {
            $this->emit('conversion', ['archive_id' => $archive_id]);
        }
    }

    /**
     * Met à jour le app_meta au besoin
     * @param EntityInterface $archive
     * @param EntityInterface $binary
     * @throws VolumeException
     * @throws FileNotReadableException
     */
    private function updateAppMeta(EntityInterface $archive, EntityInterface $binary)
    {
        /** @var StoredFile $storedFile */
        $storedFile = $binary->get('stored_file');
        $manager = $this->getVolumeManager($archive);
        $dir = sys_get_temp_dir() . DS . 'update_binaries_meta';
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        $tmpFile = $dir . DS . basename($storedFile->get('name'));
        $manager->fileDownload(
            $storedFile->get('name'),
            $tmpFile
        );
        try {
            $detector = new DetectFileFormat($tmpFile);
            $binary->set('in_rgi', $detector->rgiCompatible());
            $binary->set(
                'app_meta',
                array_filter(
                    [
                        'category' => $detector->getCategory(),
                        'codecs' => $detector->getCodecs(),
                        'puid' => $detector->getPuid(),
                    ]
                )
            );
            $this->ArchiveBinaries->saveOrFail($binary);
        } finally {
            if (is_file($tmpFile)) {
                unlink($tmpFile);
            }
            if (is_dir($dir)) {
                rmdir($dir);
            }
        }
    }

    /**
     * Création des entrées dans archive_binary_conversions
     * @param EntityInterface $archiveBinary
     * @param EntityInterface $archive
     * @return int created ArchiveBinaryConversions count
     */
    private function checkConversions(
        EntityInterface $archiveBinary,
        EntityInterface $archive
    ): int {
        /** @var JsonString $meta */
        $meta = $archiveBinary->get('app_meta');
        $meta = $meta->getArrayCopy();
        $serviceLevel = $archive->get('service_level');
        $mime = $archiveBinary->get('mime');
        $blackList = FileConverters::$blacklist;
        $canConvert = true;
        foreach ($blackList as $regex) {
            if (preg_match($regex, $mime)) {
                $canConvert = false;
                break;
            }
        }
        if (!$serviceLevel || $canConvert === false) {
            return 0;
        }
        $category = $meta['category'] ?? '';
        $ArchiveBinaryConversions = TableRegistry::getTableLocator()->get('ArchiveBinaryConversions');

        // Gestion des preservation
        $preservations = $serviceLevel->get('convert_preservation');
        $conversions = $archive->get('conversions');

        $countEntities = 0;

        if (
            $category
            && $preservations instanceof CommaArrayString
            && in_array($category, $preservations->getArrayCopy())
            && $archiveBinary->get('in_rgi') === false
        ) {
            $confkey = "conversion-preservation-$category";
            $entity = $ArchiveBinaryConversions->newEntity(
                [
                    'archive_id' => $archive->id,
                    'archive_binary_id' => $archiveBinary->id,
                    'type' => 'preservation',
                ]
            );
            if ($category === 'video') {
                $ArchiveBinaryConversions->patchEntity(
                    $entity,
                    [
                        'format' => $conversions["$confkey-container"],
                        'audio_codec' => $conversions["$confkey-codec-audio"],
                        'video_codec' => $conversions["$confkey-codec-video"],
                    ] + $entity->toArray()
                );
            } else {
                $entity->set('format', $conversions[$confkey]);
            }
            $ArchiveBinaryConversions->saveOrFail($entity);
            $countEntities++;
        }

        // Gestion des dissemination
        $dissemination = $serviceLevel->get('convert_dissemination');
        if (
            $category
            && $dissemination instanceof CommaArrayString
            && in_array($category, $dissemination->getArrayCopy())
        ) {
            $detector = new DetectFileFormat('.');
            $detector->mime = $mime;
            $detector->category = $category;
            $detector->codecs = $meta['codecs'] ?? '';
            $detector->puid = $meta['puid'] ?? '';

            $confkey = "conversion-dissemination-$category";
            if ($category === 'audio') {
                $needConversion = !$detector->is($conversions[$confkey], $detector::COMPARE_CONTAINER);
            } elseif ($category === 'video') {
                $needConversion = !$detector->is($conversions["$confkey-container"], $detector::COMPARE_CONTAINER)
                    || !$detector->is($conversions["$confkey-codec-video"], $detector::COMPARE_VIDEO_CODEC)
                    || !$detector->is($conversions["$confkey-codec-audio"], $detector::COMPARE_AUDIO_CODEC);
            } else {
                $compare = preg_match('/^document_(.*)$/', $conversions[$confkey], $m)
                    ? $m[1]
                    : $conversions[$confkey];
                if ($compare === 'pdf') {
                    $compare = 'pdfa';
                }
                $needConversion = !$detector->is($compare);
            }
            if ($needConversion) {
                $entity = $ArchiveBinaryConversions->newEntity(
                    [
                        'archive_id' => $archive->id,
                        'archive_binary_id' => $archiveBinary->id,
                        'type' => 'dissemination',
                    ]
                );
                if ($category === 'video') {
                    $ArchiveBinaryConversions->patchEntity(
                        $entity,
                        [
                            'format' => $conversions["$confkey-container"],
                            'audio_codec' => $conversions["$confkey-codec-audio"],
                            'video_codec' => $conversions["$confkey-codec-video"],
                        ] + $entity->toArray()
                    );
                } else {
                    $entity->set('format', $conversions[$confkey]);
                }
                $ArchiveBinaryConversions->saveOrFail($entity);
                $countEntities++;
            }
        }
        return $countEntities;
    }

    /**
     * Supprime les conversions des archives units
     */
    private function convertDelete()
    {
        // On commence par supprimer les unités d'archives qui ne sont pas liés à un fichier
        $query = $this->queryArchiveUnits()
            ->select(['ArchiveUnitsBatchTreatments.id'], true)
            ->leftJoinWith('ArchiveBinaries')
            ->where(['ArchiveBinaries.id IS' => null]);
        $this->ArchiveUnitsBatchTreatments->deleteAll(['id IN' => $query]);

        $query = $this->queryArchiveUnits()
            ->select(['link_id' => 'ArchiveUnitsBatchTreatments.id'])
            ->enableAutoFields()
            ->innerJoinWith('ArchiveBinaries')
            ->where(['ArchiveBinaries.type' => 'original_data'])
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        'Transfers',
                    ],
                    'ArchiveUnitsBatchTreatments',
                    'ArchiveBinariesArchiveUnits' => [
                        'ArchiveBinaries' => function (Query $q) {
                            return $q->where(
                                [
                                    'ArchiveBinaries.type IN' => [
                                        'dissemination_data',
                                        'dissemination_timestamp',
                                        'preservation_data',
                                        'preservation_timestamp',
                                    ],
                                ]
                            )
                                ->contain(['StoredFiles']);
                        },
                    ],
                ]
            );
        $conn = $this->BatchTreatments->getConnection();
        $calc = [];
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $archiveUnit) {
            $binaries = array_filter(
                Hash::extract($archiveUnit, 'archive_binaries_archive_units.{n}.archive_binary')
            );
            if (count($binaries) === 0) {
                $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
                continue;
            }
            $conn->begin();
            $volumeManager = $this->getVolumeManager($archiveUnit->get('archive'));
            foreach ($binaries as $binary) {
                $this->ArchiveBinariesArchiveUnits->deleteAll(['archive_binary_id' => $binary['id']]);
                $this->ArchiveBinaries->deleteAll(['id' => $binary['id']]);
                $volumeManager->fileDelete(Hash::get($binary, 'stored_file.name'));
            }

            $entry = $this->EventLogs->newEntry(
                'batch_treatment_convert_delete',
                'success',
                __("Traitement par lot des conversions (suppression)"),
                $this->userId,
                $archiveUnit
            );
            $this->EventLogs->saveOrFail($entry);
            /** @var EntityInterface $archive */
            $storedFile = Hash::get($archiveUnit, 'archive.lifecycle_xml_archive_file.stored_file', []);
            $this->ArchiveUnitsBatchTreatments->deleteAll(['id' => $archiveUnit->get('link_id')]);
            $calc[$archiveUnit->get('archive_id')] = $archiveUnit->get('archive_id');
            $conn->commit();
            if (!$this->Archives->updateLifecycleXml($storedFile, $entry)) {
                throw new GenericException(
                    __(
                        "Echec lors de la mise à jour du cycle de vie de l'archive n°{0}",
                        $archiveUnit->get('archive_id')
                    )
                );
            }
        }
        // calcule la volumétrie des archives, 100 par 100
        $offset = 0;
        $count = count($calc);
        while ($count > 0) {
            $archives = $this->Archives->find()
                ->where(['id IN' => array_slice($calc, $offset, 100)]);
            foreach ($archives as $archive) {
                $oldCounters = $archive->toArray();
                $this->Archives->updateCountSize([$archive]);
                $this->saveIndicators($archive, $oldCounters);
            }
            $count -= 100;
            $offset += 100;
        }
    }

    /**
     * Ajoute les indicateurs de suppression de conversions
     * @param EntityInterface $archive
     * @param array           $oldCounters
     */
    private function saveIndicators(EntityInterface $archive, array $oldCounters)
    {
        $entity = $this->ArchiveIndicators->newEntity(
            [
                'archive_date' => new DateTime(),
                'archival_agency_id' => $archive->get('archival_agency_id'),
                'transferring_agency_id' => $archive->get('transferring_agency_id'),
                'originating_agency_id' => $archive->get('originating_agency_id'),
                'archive_count' => 0,
                'units_count' => 0,
                'original_count' => 0,
                'original_size' => 0,
                'timestamp_count' => 0,
                'timestamp_size' => 0,
                'preservation_count' => 0 - $oldCounters['preservation_count'],
                'preservation_size' => 0 - $oldCounters['preservation_size'],
                'dissemination_count' => 0 - $oldCounters['dissemination_count'],
                'dissemination_size' => 0 - $oldCounters['dissemination_size'],
                'agreement_id' => $archive->get('agreement_id'),
                'profile_id' => $archive->get('profile_id'),
            ]
        );
        $this->ArchiveIndicators->saveOrFail($entity);
    }
}
