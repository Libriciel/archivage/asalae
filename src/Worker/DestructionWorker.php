<?php

/**
 * Asalae\Worker\DestructionWorker
 */

namespace Asalae\Worker;

use Asalae\Model\Entity\ArchiveUnit;
use Asalae\Model\Entity\DeletedJsonArchiveFile;
use Asalae\Model\Entity\DestructionNotification;
use Asalae\Model\Entity\DestructionRequest;
use Asalae\Model\Entity\PublicDescriptionArchiveFile;
use Asalae\Model\Entity\StoredFile;
use Asalae\Model\Table\AccessRulesTable;
use Asalae\Model\Table\AppraisalRulesTable;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveIndicatorsTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsDestructionRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\DeletedJsonArchiveFilesTable;
use Asalae\Model\Table\DestructionNotificationsTable;
use Asalae\Model\Table\DestructionNotificationXpathsTable;
use Asalae\Model\Table\DestructionRequestsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\PublicDescriptionArchiveFilesTable;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Table\VolumesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInvalidTimeZoneException;
use DateTime;
use DOMException;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Elimination d'archives
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DestructionWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var AccessRulesTable
     */
    private $AccessRules;
    /**
     * @var AppraisalRulesTable
     */
    private $AppraisalRules;
    /**
     * @var ArchiveBinariesTable
     */
    private $ArchiveBinaries;
    /**
     * @var ArchiveIndicatorsTable
     */
    private $ArchiveIndicators;
    /**
     * @var ArchiveUnitsTable
     */
    private $ArchiveUnits;
    /**
     * @var ArchiveUnitsDestructionRequestsTable
     */
    private $ArchiveUnitsDestructionRequests;
    /**
     * @var ArchivesTable
     */
    private $Archives;
    /**
     * @var CountersTable
     */
    private $Counters;
    /**
     * @var DeletedJsonArchiveFilesTable
     */
    private $DeletedJsonArchiveFiles;
    /**
     * @var DestructionNotificationXpathsTable
     */
    private $DestructionNotificationXpaths;
    /**
     * @var DestructionNotificationsTable
     */
    private $DestructionNotifications;
    /**
     * @var DestructionRequestsTable
     */
    private $DestructionRequests;
    /**
     * @var EventLogsTable
     */
    private $EventLogs;
    /**
     * @var PublicDescriptionArchiveFilesTable
     */
    private $PublicDescriptionArchiveFiles;
    /**
     * @var VolumesTable
     */
    private $Volumes;
    /**
     * @var StoredFilesTable
     */
    private $StoredFiles;

    /**
     * @var VolumeManager[]
     */
    private $dataSpaces = [];

    /**
     * WorkerInterface constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $loc = TableRegistry::getTableLocator();
        $this->AccessRules = $loc->get('AccessRules');
        $this->AppraisalRules = $loc->get('AppraisalRules');
        $this->ArchiveBinaries = $loc->get('ArchiveBinaries');
        $this->ArchiveIndicators = $loc->get('ArchiveIndicators');
        $this->ArchiveUnits = $loc->get('ArchiveUnits');
        $this->ArchiveUnitsDestructionRequests = $loc->get(
            'ArchiveUnitsDestructionRequests'
        );
        $this->Archives = $loc->get('Archives');
        $this->Counters = $loc->get('Counters');
        $this->DeletedJsonArchiveFiles = $loc->get('DeletedJsonArchiveFiles');
        $this->DestructionNotificationXpaths = $loc->get(
            'DestructionNotificationXpaths'
        );
        $this->DestructionNotifications = $loc->get('DestructionNotifications');
        $this->DestructionRequests = $loc->get('DestructionRequests');
        $this->EventLogs = $loc->get('EventLogs');
        $this->PublicDescriptionArchiveFiles = $loc->get(
            'PublicDescriptionArchiveFiles'
        );
        $this->StoredFiles = $loc->get('StoredFiles');
        $this->Volumes = $loc->get('Volumes');
    }

    /**
     * Effectue le travail voulu
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        if (empty($data['destruction_request_id'])) {
            $this->out(__("Job vide"));
            throw new CantWorkException();
        }
        $this->dataSpaces = [];
        $this->out(
            __("Demande d'élimination' id={0}", $data['destruction_request_id'])
        );
        /** @var DestructionRequest $destructionRequest */
        $destructionRequest = $this->DestructionRequests->find()
            ->where(
                [
                    'DestructionRequests.id' => $data['destruction_request_id'],
                    'DestructionRequests.state IN' => [
                        DestructionRequestsTable::S_ACCEPTED,
                        DestructionRequestsTable::S_FILES_DESTROYING,
                        DestructionRequestsTable::S_DESCRIPTION_DELETING,
                    ],
                ]
            )
            ->contain(
                [
                    'ArchivalAgencies' => ['Configurations'],
                    'OriginatingAgencies',
                ]
            )
            ->first();
        if (!$destructionRequest) {
            $this->out(
                __(
                    "La demande d'élimination n'existe pas ou n'est pas 
                dans un état qui permet d'être traité par le worker"
                )
            );
            throw new CantWorkException();
        }

        $archives = $this->Archives->find()
            ->distinct('Archives.id')
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.ArchiveUnitsDestructionRequests')
            ->where(
                ['ArchiveUnitsDestructionRequests.destruction_request_id' => $destructionRequest->id]
            )
            ->contain(
                [
                    'DeletedJsonArchiveFiles' => ['StoredFiles'],
                    'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                ]
            )
            ->toArray();
        if (count($archives) === 0) {
            $this->out(
                __(
                    "Plus aucune archive n'est liée à cette demande d'élimination"
                )
            );
            throw new CantWorkException();
        }
        $c = 0;
        $archiveUnits = [];
        $query = $this->ArchiveUnitsDestructionRequests->find()
            ->select(
                [
                    'archival_agency_identifier' => 'ArchiveUnits.archival_agency_identifier',
                    'archive_id' => 'ArchiveUnits.archive_id',
                    'name' => 'ArchiveUnits.name',
                    'lft' => 'ArchiveUnits.lft',
                    'rght' => 'ArchiveUnits.rght',
                ]
            )
            ->innerJoinWith('ArchiveUnits')
            ->where(['destruction_request_id' => $destructionRequest->id])
            ->disableHydration();
        $units = [];

        $conn = $this->ArchiveUnitsDestructionRequests->getConnection();
        foreach ($query as $unit) {
            if ($c < 100) {
                $archiveUnits[] = $unit['archival_agency_identifier'] . ' - ' . $unit['name'];
                $c++;
            }
            $xpath = $this->ArchiveUnits->find()
                ->select(['xml_node_tagname'])
                ->where(['lft <=' => $unit['lft'], 'rght >=' => $unit['rght']])
                ->orderBy(['lft'])
                ->disableHydration()
                ->all()
                ->map(
                    function ($v) {
                        return $v['xml_node_tagname'] ?? '';
                    }
                )
                ->toArray();
            $unit['xpath'] = implode('/', $xpath);
            $units[] = $unit;
        }

        // état du DestructionRequest -> S_FILES_DESTROYING
        if ($destructionRequest->get('state') === DestructionRequestsTable::S_ACCEPTED) {
            $this->destroyFiles($destructionRequest, $archives);
        }

        // obligé de tout recommencer en cas d'echec pour la notification d'élimination
        $conn->begin();
        try {
            $sds = [];
            foreach ($archives as $archive) {
                $oldCounters = $archive->toArray();
                $this->deleteArchiveUnits($destructionRequest, [$archive]);
                $this->Archives->updateCountSize([$archive]);
                $this->updateState([$archive]);
                $this->deletePublicDescription([$archive]);
                $this->saveIndicators(
                    $this->Archives->get($archive->id),
                    $oldCounters
                );
                $sdsId = $archive->get('secure_data_space_id');
                $sds[$sdsId] = $sdsId;

                // Suppression du pdf d'archive
                $pdf = $archive->get('pdf');
                $storedFile = $this->StoredFiles->find()
                    ->where(
                        [
                            'name' => $pdf,
                            'secure_data_space_id' => $sdsId,
                        ]
                    )
                    ->first();
                if ($storedFile) {
                    $manager = $this->getVolumeManager($archive);
                    $manager->fileDelete($storedFile->get('name'));
                }

                // Suppression du zip d'archive
                $zip = $archive->get('zip');
                if (is_file($zip)) {
                    unlink($zip);
                }
            }

            foreach ($sds as $sdsId) {
                $volumes = $this->Volumes->find()->where(
                    ['secure_data_space_id' => $sdsId]
                );
                /** @var EntityInterface $volume */
                foreach ($volumes as $volume) {
                    $this->Volumes->updateDiskUsage($volume->id);
                }
            }
            $this->createNotify($destructionRequest, $archiveUnits, $units);
            $destructionRequest = $this->DestructionRequests->find()
                ->where(['DestructionRequests.id' => $data['destruction_request_id']])
                ->contain(
                    [
                        'ArchivalAgencies' => ['Configurations'],
                        'OriginatingAgencies',
                    ]
                )
                ->firstOrFail();

            // génère l'attestation
            $this->out(__("Création de l'attestation d'élimination..."));
            $destructionRequest->get('certification');

            // met à jour le champ certified
            $this->DestructionRequests->saveOrFail($destructionRequest);

            $conn->commit();
        } catch (Exception $e) {
            $conn->rollback();
            throw $e;
        }
        while ($conn->inTransaction()) {
            $conn->rollback();
        }
        $Exec = Utility::get('Exec');
        $Exec->command(
            CAKE_SHELL,
            'lifecycle',
            'upload'
        );
    }

    /**
     * Suppression des fichiers liés aux archive_units
     * @param EntityInterface   $destructionRequest
     * @param EntityInterface[] $archives
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    private function destroyFiles(
        EntityInterface $destructionRequest,
        array $archives
    ) {
        if (
            $this->DestructionRequests->transition(
                $destructionRequest,
                DestructionRequestsTable::T_DESTROY_FILES
            )
        ) {
            $this->DestructionRequests->saveOrFail($destructionRequest);
        }
        foreach ($archives as $archive) {
            $this->out(
                __(
                    "Suppression des fichiers de l'archive {0}",
                    $archive->get('archival_agency_identifier')
                )
            );

            $archiveUnits = $this->ArchiveUnits->find()
                ->innerJoinWith('ArchiveUnitsDestructionRequests')
                ->where(
                    [
                        'ArchiveUnitsDestructionRequests.destruction_request_id' => $destructionRequest->id,
                        'ArchiveUnits.archive_id' => $archive->id,
                    ]
                );
            /** @var EntityInterface $archiveUnit */
            foreach ($archiveUnits as $archiveUnit) {
                $this->out(
                    __(
                        "Suppression des fichiers de l'unité d'archives {0}",
                        $archiveUnit->get('archival_agency_identifier')
                    )
                );
                $this->deleteArchiveUnitFiles($archive, $archiveUnit);
            }
        }
        $this->checkAccessRules();
        $this->checkAppraisalRules();
    }

    /**
     * Suppression des fichiers liés à une unité d'archives
     * @param EntityInterface $archive
     * @param EntityInterface $archiveUnit
     * @throws VolumeException
     */
    private function deleteArchiveUnitFiles(
        EntityInterface $archive,
        EntityInterface $archiveUnit
    ) {
        // met à jour le lft et rght
        $archiveUnit = $this->ArchiveUnits->get($archiveUnit->id);
        $query = $this->ArchiveUnits->find()
            ->innerJoinWith('ArchiveBinaries')
            ->where(
                [
                    'ArchiveUnits.archive_id' => $archive->id,
                    'ArchiveUnits.lft >=' => $archiveUnit->get('lft'),
                    'ArchiveUnits.rght <=' => $archiveUnit->get('rght'),
                ]
            )
            ->contain(
                [
                    'ArchiveBinaries' => ['StoredFiles'],
                ]
            );
        $manager = $this->getVolumeManager($archive);
        /** @var EntityInterface $unit */
        foreach ($query as $unit) {
            /** @var EntityInterface $binary */
            foreach ($unit->get('archive_binaries') as $binary) {
                $this->ArchiveBinaries->updateAll(
                    ['stored_file_id' => null],
                    ['id' => $binary->id]
                );
                /** @var StoredFile $storedFile */
                $storedFile = $binary->get('stored_file');
                if (
                    $storedFile && $manager->fileExists(
                        $storedFile->get('name')
                    )
                ) {
                    $manager->fileDelete($storedFile->get('name'));
                }
            }
        }
    }

    /**
     * Donne le VolumeManager lié à une archive
     * @param EntityInterface $archive
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(EntityInterface $archive): VolumeManager
    {
        if (!isset($this->dataSpaces[$archive->get('secure_data_space_id')])) {
            $this->dataSpaces[$archive->get('secure_data_space_id')]
                = new VolumeManager($archive->get('secure_data_space_id'));
        }
        return $this->dataSpaces[$archive->get('secure_data_space_id')];
    }

    /**
     * Supprime les AccessRules orphelins
     */
    private function checkAccessRules()
    {
        $rules = $this->AccessRules->find()
            ->select(['AccessRules.id'])
            ->leftJoinWith('ArchiveDescriptions')
            ->leftJoinWith('ArchiveKeywords')
            ->leftJoinWith('ArchiveUnits')
            ->leftJoinWith('Archives')
            ->where(
                [
                    'ArchiveDescriptions.id IS' => null,
                    'ArchiveKeywords.id IS' => null,
                    'ArchiveUnits.id IS' => null,
                    'Archives.id IS' => null,
                ]
            )
            ->disableHydration();
        foreach ($rules as $rule) {
            $this->AccessRules->deleteAll(['id' => $rule['id']]);
        }
    }

    /**
     * Supprime les AppraisalRules orphelins
     */
    private function checkAppraisalRules()
    {
        $rules = $this->AppraisalRules->find()
            ->select(['AppraisalRules.id'])
            ->leftJoinWith('ArchiveUnits')
            ->leftJoinWith('Archives')
            ->where(
                [
                    'ArchiveUnits.id IS' => null,
                    'Archives.id IS' => null,
                ]
            )
            ->disableHydration();
        foreach ($rules as $rule) {
            $this->AppraisalRules->deleteAll(['id' => $rule['id']]);
        }
    }

    /**
     * Suppression des archive_units
     * @param EntityInterface   $destructionRequest
     * @param EntityInterface[] $archives
     * @throws DOMException
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    private function deleteArchiveUnits(
        EntityInterface $destructionRequest,
        array $archives
    ) {
        if (
            $this->DestructionRequests->transition(
                $destructionRequest,
                DestructionRequestsTable::T_DELETE_DESCRIPTION
            )
        ) {
            $this->DestructionRequests->saveOrFail($destructionRequest);
        }
        foreach ($archives as $archive) {
            $this->out(
                __(
                    "Suppression des unités d'archives de l'archive {0}",
                    $archive->get('archival_agency_identifier')
                )
            );

            $archiveUnits = $this->ArchiveUnits->find()
                ->innerJoinWith('ArchiveUnitsDestructionRequests')
                ->where(
                    [
                        'ArchiveUnitsDestructionRequests.destruction_request_id' => $destructionRequest->id,
                        'ArchiveUnits.archive_id' => $archive->id,
                    ]
                )
                ->orderBy(['ArchiveUnits.lft' => 'desc']);
            /** @var EntityInterface $archiveUnit */
            foreach ($archiveUnits as $archiveUnit) {
                $this->out(
                    __(
                        "Elimination de l'unité d'archives {0}",
                        $archiveUnit->get('archival_agency_identifier')
                    )
                );
                $entry = $this->EventLogs->newEntry(
                    'archiveunit_delete',
                    'success',
                    __(
                        "Elimination de l'unité d'archives {0} suite à la demande d'élimination {1}",
                        $archiveUnit->get('archival_agency_identifier'),
                        $destructionRequest->get('identifier')
                    ),
                    $destructionRequest->get('created_user_id'),
                    $archiveUnit
                );
                $entry->set('in_lifecycle', false);
                $this->EventLogs->saveOrFail($entry);
                $this->deleteArchiveUnit($archive, $archiveUnit);
                /** @var EntityInterface $archive */
                $storedFile = Hash::get(
                    $archive,
                    'lifecycle_xml_archive_file.stored_file',
                    []
                );
                $update = $this->Archives->updateLifecycleXml(
                    $storedFile,
                    $entry,
                    true
                );
                if (!$update) {
                    throw $this->Archives->exception ?: new Exception('lifecycle save failed');
                }
            }
            $this->calcCountSizeArchiveUnits($archive);
        }
    }

    /**
     * Supprime les unités d'archives avec toutes les entrées liés
     * @param EntityInterface $archive
     * @param ArchiveUnit     $archiveUnit
     * @throws VolumeException
     */
    private function deleteArchiveUnit(
        EntityInterface $archive,
        ArchiveUnit $archiveUnit
    ) {
        $deletedArchiveUnitXPath = $archiveUnit->get('xpath');
        $this->ArchiveUnits->cascadeDelete($archiveUnit);
        if (empty($archive->get('deleted_json_archive_file'))) {
            $archive->set(
                'deleted_json_archive_file',
                $this->initializeDeletedJson($archive, $deletedArchiveUnitXPath)
            );
        } else {
            $this->appendDeletedJson($archive, $deletedArchiveUnitXPath);
        }
        // Suppression du zip des unités d'archives parentes
        $parents = $this->ArchiveUnits->find()
            ->where(
                [
                    'archive_id' => $archiveUnit->get('archive_id'),
                    'lft <=' => $archiveUnit->get('lft'),
                    'rght >=' => $archiveUnit->get('rght'),
                ]
            )
            ->all();
        foreach ($parents as $parent) {
            $filename = $parent->get('zip');
            if (is_file($filename)) {
                unlink($filename);
            }
        }
        if ($parents->count() > 0) {
            $this->ArchiveUnits->calcMaxAggregatedAccessEndDate(
                ['ArchiveUnits.archive_id' => $archiveUnit->get('archive_id')]
            );
        }
    }

    /**
     * Créé le fichier deleted.json
     * @param EntityInterface $archive
     * @param string          $deletedArchiveUnitXPath
     * @return EntityInterface $archiveFile
     * @throws VolumeException
     */
    private function initializeDeletedJson(
        EntityInterface $archive,
        string $deletedArchiveUnitXPath
    ): EntityInterface {
        $archiveFile = $this->DeletedJsonArchiveFiles->newEntity(
            [
                'archive_id' => $archive->id,
                'type' => 'deleted',
                'filename' => $archive->get(
                    'archival_agency_identifier'
                ) . '_deleted.json',
            ]
        );
        $basePath = $archive->get('storage_path') . '/management_data/';
        $manager = $this->getVolumeManager($archive);
        $storedFile = $manager->filePutContent(
            $basePath . $archiveFile->get('filename'),
            json_encode([$deletedArchiveUnitXPath], JSON_UNESCAPED_SLASHES)
        );
        $archiveFile->set('stored_file_id', $storedFile->id);
        $archiveFile->set('stored_file', $storedFile);
        $this->DeletedJsonArchiveFiles->saveOrFail($archiveFile);
        return $archiveFile;
    }

    /**
     * Ajoute un xpath au fichier deleted.json
     * @param EntityInterface $archive
     * @param string          $deletedArchiveUnitXPath
     * @throws VolumeException
     */
    private function appendDeletedJson(
        EntityInterface $archive,
        string $deletedArchiveUnitXPath
    ) {
        /** @var DeletedJsonArchiveFile $archiveFile */
        $archiveFile = $archive->get('deleted_json_archive_file');
        $archiveFile->data = []; // évite les effets de cache si plusieurs UA de la même archive
        $manager = $this->getVolumeManager($archive);
        $data = $archiveFile->getData();
        $data[] = $deletedArchiveUnitXPath;
        $manager->set($archiveFile->get('stored_file_id'), json_encode($data, JSON_UNESCAPED_SLASHES));
    }

    /**
     * Calcule les count et les size liés à une archive
     * @param EntityInterface $archive
     * @throws Exception
     */
    private function calcCountSizeArchiveUnits(EntityInterface $archive)
    {
        $query = $this->ArchiveUnits->find();
        $subquerySumSize = $this->ArchiveUnits->selectQuery()
            ->select(['sum' => $query->func()->sum('original_local_size')])
            ->from(['ausize' => 'archive_units'], true)
            ->where(
                [
                    'ausize.lft >=' => new IdentifierExpression('ArchiveUnits.lft'),
                    'ausize.rght <=' => new IdentifierExpression('ArchiveUnits.rght'),
                ]
            );
        $subqueryCount = (clone $subquerySumSize)->select(
            ['count' => $query->func()->sum('original_local_count')],
            true
        );
        $query->select(
            [
                'ArchiveUnits.id',
                'ArchiveUnits.original_total_size',
                'ArchiveUnits.original_total_count',
                'new_original_total_size' => $subquerySumSize,
                'new_original_total_count' => $subqueryCount,
            ]
        )
            ->enableAutoFields() // afterSave
            ->where(['ArchiveUnits.archive_id' => $archive->id]);
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $archiveUnit) {
            $data = [
                'original_total_size' => $archiveUnit->get('new_original_total_size'),
                'original_total_count' => $archiveUnit->get('new_original_total_count'),
            ];
            $this->ArchiveUnits->patchEntity($archiveUnit, $data);
            $this->ArchiveUnits->save($archiveUnit);
        }
    }

    /**
     * Si il n'y a plus d'archive_units, on applique la transition 'destruct'
     * @param EntityInterface[] $archives
     * @throws DateInvalidTimeZoneException
     */
    private function updateState(array $archives)
    {
        $archives = $this->Archives->find()
            ->where(['Archives.id IN' => Hash::extract($archives, '{n}.id')])
            ->leftJoinWith('FirstArchiveUnits')
            ->where(['FirstArchiveUnits.id IS' => null]);
        /** @var EntityInterface $archive */
        foreach ($archives as $archive) {
            if ($this->Archives->transition($archive, ArchivesTable::T_DESTRUCT)) {
                $this->Archives->saveOrFail($archive);
            }
        }
    }

    /**
     * Supprime la description publique
     * @param EntityInterface[] $archives
     * @throws VolumeException
     */
    private function deletePublicDescription(array $archives)
    {
        $archives = $this->Archives->find()
            ->where(['Archives.id IN' => Hash::extract($archives, '{n}.id')])
            ->innerJoinWith('PublicDescriptionArchiveFiles')
            ->contain(
                ['PublicDescriptionArchiveFiles' => ['StoredFiles']]
            );
        /** @var EntityInterface $archive */
        foreach ($archives as $archive) {
            /** @var PublicDescriptionArchiveFile $archiveFile */
            $archiveFile = Hash::get(
                $archive,
                'archive.public_description_archive_file'
            );
            if ($archiveFile) {
                $this->PublicDescriptionArchiveFiles->deleteOrFail(
                    $archiveFile
                );
                $manager = $this->getVolumeManager($archive);
                $manager->fileDelete(
                    Hash::get($archiveFile, 'stored_file.name')
                );
                $archive->set('next_pubdesc');
                $this->Archives->saveOrFail($archive);
            }
        }
    }

    /**
     * Ajoute les indicateurs de destruction
     * @param EntityInterface $archive
     * @param array           $oldCounters
     */
    private function saveIndicators(
        EntityInterface $archive,
        array $oldCounters
    ) {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $au = $ArchiveUnits->exists(
            ['ArchiveUnits.archive_id' => $archive->id]
        );

        $entity = $this->ArchiveIndicators->newEntity(
            [
                'archive_date' => new DateTime(),
                'archival_agency_id' => $archive->get('archival_agency_id'),
                'transferring_agency_id' => $archive->get(
                    'transferring_agency_id'
                ),
                'originating_agency_id' => $archive->get(
                    'originating_agency_id'
                ),
                'archive_count' => $au ? 0 : -1,
                'units_count' => $archive->get(
                    'units_count'
                ) - $oldCounters['units_count'],
                'original_count' => $archive->get(
                    'original_count'
                ) - $oldCounters['original_count'],
                'original_size' => $archive->get(
                    'original_size'
                ) - $oldCounters['original_size'],
                'timestamp_count' => $archive->get(
                    'timestamp_count'
                ) - $oldCounters['timestamp_count'],
                'timestamp_size' => $archive->get(
                    'timestamp_size'
                ) - $oldCounters['timestamp_size'],
                'preservation_count' => $archive->get(
                    'preservation_count'
                ) - $oldCounters['preservation_count'],
                'preservation_size' => $archive->get(
                    'preservation_size'
                ) - $oldCounters['preservation_size'],
                'dissemination_count' => $archive->get(
                    'dissemination_count'
                ) - $oldCounters['dissemination_count'],
                'dissemination_size' => $archive->get(
                    'dissemination_size'
                ) - $oldCounters['dissemination_size'],
                'agreement_id' => $archive->get('agreement_id'),
                'profile_id' => $archive->get('profile_id'),
            ]
        );
        $this->ArchiveIndicators->saveOrFail($entity);
    }

    /**
     * Créé la notification
     * @param EntityInterface $destructionRequest
     * @param array           $archiveUnits
     * @param array           $units
     * @throws Exception
     */
    private function createNotify(
        EntityInterface $destructionRequest,
        array $archiveUnits,
        array $units
    ) {
        if (
            $this->DestructionRequests->transition(
                $destructionRequest,
                DestructionRequestsTable::T_NOTIFY
            )
        ) {
            $this->DestructionRequests->saveOrFail($destructionRequest);
        }
        // Note: ne pas save $destructionRequest apres lui avoir passé les units
        $destructionRequest->set('archive_units', $units);
        /** @var DestructionNotification $notification */
        $notification = $this->DestructionNotifications->newEntity(
            [
                'identifier' => $this->Counters->next(
                    $destructionRequest->get('archival_agency_id'),
                    'ArchiveDestructionNotification'
                ),
                'destruction_request_id' => $destructionRequest->id,
                'comment' => __n(
                    "Elimination de l'unité d'archives :\n- {0}",
                    "Elimination des unités d'archives :\n- {0}",
                    count($archiveUnits),
                    implode("\n- ", $archiveUnits)
                ),
            ]
        );
        $this->DestructionNotifications->saveOrFail($notification);
        foreach ($units as $archiveUnit) {
            $xpathEntity = $this->DestructionNotificationXpaths->newEntity(
                [
                    'destruction_notification_id' => $notification->id,
                    'archive_id' => $archiveUnit['archive_id'],
                    'archive_unit_identifier' => $archiveUnit['archival_agency_identifier'],
                    'archive_unit_name' => $archiveUnit['name'],
                    'description_xpath' => $archiveUnit['xpath'],
                ]
            );
            $this->DestructionNotificationXpaths->saveOrFail($xpathEntity);
        }
        $notification->set('destruction_request', $destructionRequest);
        Filesystem::dumpFile(
            $notification->get('xml'),
            $notification->generateXml()
        );
    }
}
