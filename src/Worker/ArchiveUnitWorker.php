<?php

/**
 * Asalae\Worker\ArchiveWorker
 */

namespace Asalae\Worker;

use Asalae\Exception\GenericException;
use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\MessageSchema\MessageSchema;
use AsalaeCore\Form\MessageSchema\Seda02Schema;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Form\MessageSchema\Seda21Schema;
use AsalaeCore\Form\MessageSchema\Seda22Schema;
use AsalaeCore\I18n\Date as CoreDate;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Error\Debugger;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInterval;
use DateInvalidTimeZoneException;
use DateTime;
use DateTimeInterface;
use DOMElement;
use DOMNode;
use DOMXPath;
use Exception;
use Pheanstalk\Pheanstalk;
use ZMQSocketException;

/**
 * Créer l'archive d'un transfert
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveUnitWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var Transfer
     */
    public $Transfer;
    /**
     * @var EntityInterface|Entity
     */
    private $Archive;

    /**
     * @var string namespace xml
     */
    private $namespace;

    /**
     * @var DOMXPath
     */
    private $xpath;

    /**
     * @var int compteur d'archive units
     */
    private $counter = 0;

    /**
     * @var int nombre d'archive units trouvés dans le xml
     */
    private $countXML = 0;

    /**
     * @var int
     */
    private $countSave;

    /**
     * @var DOMUtility
     */
    private $util;

    /**
     * @var array xpaths des archive_units
     */
    private $xpaths = [];

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        $this->out(
            __(
                "Création des archive_units du transfert id={0}",
                $data['transfer_id']
            )
        );
        $this->counter = 0;
        $this->countSave = 0;
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $Transfers = $loc->get('Transfers');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $this->Transfer = $Transfers->get($data['transfer_id']);
        $this->setUtil(DOMUtility::load($this->Transfer->get('xml')));
        if (isset($data['composite_id'])) {
            $this->handleComposites($data);
        } else {
            $query = $Archives->find()
                ->innerJoinWith('Transfers')
                ->where(['Transfers.id' => $data['transfer_id']])
                ->orderBy(['Archives.id']);
            /** @var EntityInterface $archive */
            foreach ($query as $archive) {
                if ($archive->get('state') !== ArchivesTable::S_CREATING) {
                    $this->out(
                        __(
                            "Etat de l'archive: {0}. Annulation.",
                            $archive->get('state')
                        )
                    );
                    throw new CantWorkException();
                }
                $this->counter = (int)$archive->get('archive_unit_sequence');
                $this->createArchiveUnitsForArchive($archive->id);

                $this->out(__("Calcul des dates de fin de dérogations"));
                $ArchiveUnits->calcMaxAggregatedAccessEndDate(
                    ['ArchiveUnits.archive_id' => $archive->id],
                    function ($i, $count) {
                        if ($i % 100 === 0) {
                            $this->out(sprintf('%d / %d', $i, $count));
                        }
                    }
                );
            }
        }
        $saveRefs = Configure::read('ArchiveUnitWorker.saveRefs', TMP)
            . $data['transfer_id'] . '_saverefs.serialize';
        file_put_contents($saveRefs, serialize($this->xpaths));

        $query = $Transfers->find();
        $maxsize = $query
            ->select(['max' => $query->func()->max('TransferAttachments.size')])
            ->innerJoinWith('TransferAttachments')
            ->where(['Transfers.id' => $data['transfer_id']])
            ->first();
        // 1s / 10Mo (hash_file('sha256'))
        $maxsize = (int)(($maxsize ? ($maxsize->get('max')) : 0) / 10000000);
        $this->emit(
            'archive-binary',
            $data,
            Pheanstalk::DEFAULT_PRIORITY,
            Pheanstalk::DEFAULT_DELAY,
            max($maxsize, Pheanstalk::DEFAULT_TTR)
        );
    }

    /**
     * Setter util
     * @param DOMUtility $util
     */
    public function setUtil(DOMUtility $util)
    {
        $this->util = $util;
        $this->namespace = $this->util->namespace;
        $this->xpath = $this->util->xpath;
        $this->xpaths = [];
    }

    /**
     * Gestion des archives composites (SEDA > 2.0)
     * @param array $data
     * @return void
     * @throws Exception
     */
    private function handleComposites(array $data)
    {
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $conn = $ArchiveUnits->getConnection();
        $conn->begin();
        $archiveUnitsPath = 'ns:DataObjectPackage/ns:DescriptiveMetadata/ns:ArchiveUnit';
        $refPath = 'ns:Content/ns:RelatedObjectReference/ns:IsPartOf/ns:RepositoryArchiveUnitPID';
        $this->Archive = null;
        foreach ($this->xpath->query($archiveUnitsPath) as $archiveUnitNode) {
            $ref = $this->util->node($refPath, $archiveUnitNode);
            if (!$ref) {
                throw new GenericException($refPath . ' not found');
            }
            $this->addArchiveUnit($archiveUnitNode, trim($ref->nodeValue), $data['composite_id']);
        }
        if (!$this->Archive || $this->Archive->id !== $data['composite_id']) {
            throw new GenericException('No reference was found');
        }
        $this->updateArchive();
        $conn->commit();
    }

    /**
     * Ajoute une unité d'archive issue d'un transfert lié
     * @param DOMNode $archiveUnitNode
     * @param string  $identifier
     * @param int     $archive_id
     * @return void
     * @throws Exception
     */
    private function addArchiveUnit(DOMNode $archiveUnitNode, string $identifier, int $archive_id)
    {
        if (strpos($identifier, 'ArchivalAgencyIdentifier:') !== false) {
            $conditions = [
                'ArchiveUnits.archival_agency_identifier' => substr(
                    $identifier,
                    strlen('ArchivalAgencyIdentifier:')
                ),
            ];
        } elseif (strpos($identifier, 'TransferringAgencyIdentifier:') !== false) {
            $conditions = [
                'ArchiveUnits.transferring_agency_identifier' => substr(
                    $identifier,
                    strlen('TransferringAgencyIdentifier:')
                ),
            ];
        } elseif (strpos($identifier, 'OriginatingAgencyIdentifier:') !== false) {
            $conditions = [
                'ArchiveUnits.originating_agency_identifier' => substr(
                    $identifier,
                    strlen('OriginatingAgencyIdentifier:')
                ),
            ];
        } else {
            $conditions = [
                'OR' => [
                    'ArchiveUnits.archival_agency_identifier' => $identifier,
                    'ArchiveUnits.transferring_agency_identifier' => $identifier,
                    'ArchiveUnits.originating_agency_identifier' => $identifier,
                ],
            ];
        }
        $conditions['ArchiveUnits.archive_id'] = $archive_id;
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $this->Archive = $Archives->get($archive_id);
        if ($this->Archive->get('state') !== ArchivesTable::S_UPDATE_CREATING) {
            $this->out(
                __(
                    "Etat de l'archive: {0}. Annulation.",
                    $this->Archive->get('state')
                )
            );
            throw new CantWorkException();
        }
        $this->counter = (int)$this->Archive->get('archive_unit_sequence');
        $parentEntity = $ArchiveUnits->find()
            ->where($conditions)
            ->contain(
                [
                    'ArchiveDescriptions' => [
                        'AccessRules',
                    ],
                    'AccessRules',
                    'AppraisalRules',
                ]
            )
            ->firstOrFail();
        $lastChild = $ArchiveUnits->find()
            ->where(['parent_id' => $parentEntity->id])
            ->orderByDesc('lft')
            ->first();
        $index = 0;
        if (
            $lastChild
            && preg_match('/^.*\[(\d+)]$/', $lastChild->get('xml_node_tagname'), $m)
        ) {
            $index = (int)$m[1];
        }
        $index++;
        $this->createArchiveUnits(
            $archiveUnitNode,
            Hash::get($parentEntity, 'archive_description.access_rule')
                ?: $parentEntity->get('access_rule'),
            $index,
            $parentEntity
        );
        $Archives->saveOrFail($this->Archive);
    }

    /**
     * Insert en base les archive_units de façon récursive
     * @param DOMElement           $archiveUnitNode
     * @param EntityInterface|null $descriptionAccessRule
     * @param int                  $indexRoot
     * @param EntityInterface|null $parentEntity
     * @return bool
     * @throws Exception
     */
    private function createArchiveUnits(
        DOMElement $archiveUnitNode,
        EntityInterface $descriptionAccessRule = null,
        int $indexRoot = 1,
        EntityInterface $parentEntity = null
    ) {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $CompositeArchiveUnits = $loc->get('CompositeArchiveUnits');
        $currentArchiveUnitEntity = $this->createArchiveUnit(
            $archiveUnitNode,
            $indexRoot,
            $parentEntity
        );
        if (
            $currentArchiveUnitEntity->isNew()
            && !$ArchiveUnits->save($currentArchiveUnitEntity)
        ) {
            $this->out(__("Erreur lors de la sauvegarde de l'archive_unit"));
            $this->out(
                Debugger::exportVar($currentArchiveUnitEntity->getErrors())
            );
            return false;
        }
        $this->xpaths[$currentArchiveUnitEntity->id] = $this->util->getElementXpath($archiveUnitNode);
        $CompositeArchiveUnits->findOrCreate(
            [
                'transfer_id' => $this->Transfer->id,
                'archive_unit_id' => $currentArchiveUnitEntity->id,
                'transfer_xpath' => $this->xpaths[$currentArchiveUnitEntity->id],
            ]
        );
        $this->inc();

        /** @var Seda02Schema|Seda10Schema|Seda21Schema|Seda22Schema $schema */
        $schema = MessageSchema::getSchemaClassname($this->namespace);
        $units = $this->xpath->query(
            'ns:' . $schema::getTagname('ArchiveObject', 'Archive'),
            $archiveUnitNode
        );
        $documentNodes = [];
        // en seda2.1, il n'y a pas de noeud Document, le fichier est mentionné via un ArchiveUnit
        if (!in_array($this->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            $documentNodes = $this->xpath->query(
                'ns:' . $schema::getTagname('Document', 'Archive'),
                $archiveUnitNode
            );
        }
        $descriptionNode = $this->xpath->query(
            'ns:' . $schema::getTagname('ContentDescription', 'Archive'),
            $archiveUnitNode
        )->item(0);
        $keywordNodes = $descriptionNode
            ? $this->xpath->query(
                'ns:' . $schema::getTagname('Keyword', 'Archive'),
                $descriptionNode
            )
            : [];

        if ($descriptionNode) {
            $this->createDescription(
                $archiveUnitNode,
                $currentArchiveUnitEntity,
                $parentEntity,
                $descriptionAccessRule
            );
        }
        foreach ($documentNodes as $index => $documentNode) {
            $this->createDocumentArchiveUnit(
                $documentNode,
                $index + 1,
                $currentArchiveUnitEntity
            );
        }
        foreach ($keywordNodes as $index => $keywordNode) {
            $this->createKeyword(
                $keywordNode,
                $descriptionAccessRule,
                $currentArchiveUnitEntity,
                $index + 1
            );
        }
        foreach ($units as $index => $archiveUnitNode) {
            $name = $this->getName($archiveUnitNode);
            if (empty($name)) {
                continue;
            }
            if (
                !$this->createArchiveUnits(
                    $archiveUnitNode,
                    $descriptionAccessRule,
                    $index + 1,
                    $currentArchiveUnitEntity
                )
            ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Crée l'unité d'archives à partir d'un element dom
     * @param DOMElement           $archiveUnitNode
     * @param int                  $index
     * @param EntityInterface|null $parentEntity
     * @return EntityInterface
     * @throws Exception
     */
    private function createArchiveUnit(
        DOMElement $archiveUnitNode,
        int $index,
        EntityInterface $parentEntity = null
    ): EntityInterface {
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $archiveArchivalAgencyIdentifier = $this->Archive->get(
            'archival_agency_identifier'
        );
        if (empty($parentEntity)) {
            $archivalAgencyIdentifier = $archiveArchivalAgencyIdentifier;
            $name = $this->Archive->get('name');
        } else {
            $this->counter++;
            $archivalAgencyIdentifier = sprintf(
                '%s_%04d',
                $archiveArchivalAgencyIdentifier,
                $this->counter
            );
            $this->Archive->set('archive_unit_sequence', $this->counter);
            $Archives->updateAll(
                ['archive_unit_sequence' => $this->counter],
                ['id' => $this->Archive->id]
            );
            $name = $this->getName($archiveUnitNode);
        }
        $conditions = [
            'archive_id' => $this->Archive->get('id'),
            'xml_node_tagname' => $archiveUnitNode->tagName . '[' . $index . ']',
        ];

        /** @noinspection PhpNullSafeOperatorCanBeUsedInspection */
        if ($parentEntity) {
            $conditions['parent_id'] = $parentEntity->get('id');
        } else {
            $conditions['parent_id IS'] = null;
        }
        $entity = $ArchiveUnits->find()
            ->where($conditions)
            ->first();
        if (empty($entity)) {
            $accessRule = $this->getAccessRule($archiveUnitNode, $parentEntity);
            $appraisalRule = $this->getAppraisalRule(
                $archiveUnitNode,
                $parentEntity
            );
            $entity = $ArchiveUnits->newEntity(
                [
                    'state' => $ArchiveUnits->initialState,
                    'archive_id' => $this->Archive->get('id'),
                    'access_rule_id' => $accessRule->get('id'),
                    'appraisal_rule_id' => $appraisalRule->get('id'),
                    'archival_agency_identifier' => $archivalAgencyIdentifier,
                    'transferring_agency_identifier' => $this->getTransferringAgencyIdentifier(
                        $archiveUnitNode,
                        $parentEntity
                    ),
                    'originating_agency_identifier' => $this->getOriginatingAgencyIdentifier(
                        $archiveUnitNode,
                        $parentEntity
                    ),
                    'name' => $name,
                    'xml_node_tagname' => $archiveUnitNode->tagName . '[' . $index . ']',
                    'original_local_size' => 0,
                    'original_total_size' => 0,
                    'parent_id' => $parentEntity?->get('id'),
                    'original_total_count' => 0,
                    'original_local_count' => 0,
                ]
            );
            $entity->set('access_rule', $accessRule);
            $entity->set('appraisal_rule', $appraisalRule);
        }
        return $entity;
    }

    /**
     * Donne le nom d'une archive
     * @param DOMElement $archiveUnitNode
     * @return null|string
     */
    private function getName(DOMElement $archiveUnitNode)
    {
        /** @var DOMElement $node */
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $node = $this->xpath->query('ns:Name', $archiveUnitNode)->item(
                    0
                );
                return $node ? trim($node->nodeValue) : null;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $node = $this->xpath->query(
                    'ns:Content/ns:Title',
                    $archiveUnitNode
                )->item(0);
                return $node ? trim($node->nodeValue) : null;
            default:
                return null;
        }
    }

    /**
     * Donne l'AccessRestriction lié à une archive unit
     * @param DOMElement           $archiveUnitNode
     * @param EntityInterface|null $parentEntity
     * @return EntityInterface
     * @throws Exception
     */
    private function getAccessRule(
        DOMElement $archiveUnitNode,
        EntityInterface $parentEntity = null
    ) {
        if (empty($parentEntity)) {
            return $this->Archive->get('access_rule');
        }
        $codeTagname = 'ns:Code';
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
                $accessRule
                    = $this->xpath->query(
                        'ns:AccessRestriction',
                        $archiveUnitNode
                    )
                    ->item(0);
                break;
            case NAMESPACE_SEDA_10:
                $accessRule
                    = $this->xpath->query(
                        'ns:AccessRestrictionRule',
                        $archiveUnitNode
                    )
                    ->item(0);
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $accessRule
                    = $this->xpath->query(
                        'ns:Management/ns:AccessRule',
                        $archiveUnitNode
                    )
                    ->item(0);
                $codeTagname = 'ns:Rule';
                break;
            default:
                $accessRule = null;
        }
        if ($accessRule) {
            $code = $this->xpath->query($codeTagname, $accessRule)->item(0);
            $startDate = $this->xpath->query('ns:StartDate', $accessRule)->item(0);
        }
        if (empty($code)) {
            return $parentEntity->get('access_rule');
        }
        $AccessRuleCodes = TableRegistry::getTableLocator()->get(
            'AccessRuleCodes'
        );
        $rule = $AccessRuleCodes->find()
            ->select(['id', 'duration'])
            ->where(['code' => trim($code->nodeValue)])
            ->firstOrFail();
        $AccessRules = TableRegistry::getTableLocator()->get('AccessRules');
        $startDate = new CakeDateTime(
            empty($startDate) ? date('Y-m-d') : trim($startDate->nodeValue)
        );
        $entityStartdate = Hash::get($parentEntity, 'access_rule.start_date');
        if ($entityStartdate instanceof CakeDate || $entityStartdate instanceof CoreDate) {
            $entityStartdate = $entityStartdate->format('Y-m-d');
        }
        if (
            $rule->get('id') === Hash::get(
                $parentEntity,
                'access_rule.access_rule_code_id'
            )
            && $startDate->format('Y-m-d') === $entityStartdate
        ) {
            return $parentEntity->get('access_rule');
        }
        $accessRule = $AccessRules->newEntity(
            [
                'access_rule_code_id' => $rule->get('id'),
                'start_date' => clone $startDate,
                'end_date' => $startDate->add(
                    new DateInterval($rule->get('duration'))
                ),
            ]
        );
        $AccessRules->saveOrFail($accessRule);
        return $accessRule;
    }

    /**
     * Donne l'AppraisalRule lié à une archive unit
     * @param DOMElement           $archiveUnitNode
     * @param EntityInterface|null $parentEntity
     * @return EntityInterface
     * @throws Exception
     */
    private function getAppraisalRule(
        DOMElement $archiveUnitNode,
        EntityInterface $parentEntity = null
    ) {
        if (empty($parentEntity)) {
            return $this->Archive->get('appraisal_rule');
        }
        /** @var DOMElement $node */
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
                $appraisalRule
                    = $this->xpath->query('ns:Appraisal', $archiveUnitNode)
                    ->item(0);
                break;
            case NAMESPACE_SEDA_10:
                $appraisalRule
                    = $this->xpath->query('ns:AppraisalRule', $archiveUnitNode)
                    ->item(0);
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $appraisalRule
                    = $this->xpath->query(
                        'ns:Management/ns:AppraisalRule',
                        $archiveUnitNode
                    )
                    ->item(0);
                break;
            default:
                return $parentEntity->get('appraisal_rule');
        }
        $AppraisalRuleCodes = TableRegistry::getTableLocator()->get('AppraisalRuleCodes');
        if ($appraisalRule && in_array($this->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            $finalAction = $this->xpath->query('ns:FinalAction', $appraisalRule)->item(0);
            $code = empty($finalAction) ? 'keep' : trim(strtolower($finalAction->nodeValue));
            $rule = $this->xpath->query('ns:Rule', $appraisalRule)->item(0);
            $startDate = $this->xpath->query('ns:StartDate', $appraisalRule)->item(0);
            if (empty($rule)) {
                return $parentEntity->get('appraisal_rule');
            }

            $appraisalRuleCode = $AppraisalRuleCodes->find()
                ->select(['id', 'duration'])
                ->where(['code' => trim($rule->nodeValue)])
                ->first();
            $duration = $appraisalRuleCode->get('duration');
        } else {
            if ($appraisalRule) {
                $code = $this->xpath->query('ns:Code', $appraisalRule)->item(0);
                $duration = $this->xpath->query('ns:Duration', $appraisalRule)->item(0);
                $startDate = $this->xpath->query('ns:StartDate', $appraisalRule)->item(0);
            }
            if (empty($duration)) {
                return $parentEntity->get('appraisal_rule');
            }
            $duration = trim($duration->nodeValue);
            $code = empty($code) ? 'keep' : trim($code->nodeValue);
            if ($code === 'conserver') {
                $code = 'keep';
            } elseif ($code === 'detruire') {
                $code = 'destroy';
            }
            $appraisalRuleCode = $AppraisalRuleCodes->find()
                ->select(['id', 'duration'])
                ->where(['code' => 'AP' . $duration])
                ->first();
        }

        if (empty($appraisalRuleCode)) {
            $appraisalRuleCode = $AppraisalRuleCodes->find()
                ->select(['id', 'duration'])
                ->where(['code' => 'APP0Y'])
                ->firstOrFail();
        }

        $AppraisalRules = TableRegistry::getTableLocator()->get(
            'AppraisalRules'
        );
        $startDate = new CakeDateTime(
            empty($startDate) ? date('Y-m-d') : trim($startDate->nodeValue)
        );
        $parentStartDate = Hash::get(
            $parentEntity,
            'appraisal_rule.start_date'
        );
        if ($parentStartDate instanceof CakeDate || $parentStartDate instanceof CoreDate) {
            $parentStartDate = $parentStartDate->format('Y-m-d');
        }
        $parentAppraisalRuleCodeId = Hash::get(
            $parentEntity,
            'appraisal_rule.appraisal_rule_code_id'
        );
        $parentFinalActionCode = Hash::get(
            $parentEntity,
            'appraisal_rule.final_action_code'
        );
        if (
            $appraisalRuleCode->get('id') === $parentAppraisalRuleCodeId
            && $code === $parentFinalActionCode
            && $startDate->format('Y-m-d') === $parentStartDate
        ) {
            return $parentEntity->get('appraisal_rule');
        }
        $appraisalRule = $AppraisalRules->newEntity(
            [
                'appraisal_rule_code_id' => $appraisalRuleCode->get('id'),
                'final_action_code' => $code,
                'start_date' => clone $startDate,
                'end_date' => $startDate->add(
                    new DateInterval($duration)
                ),
            ]
        );
        $AppraisalRules->saveOrFail($appraisalRule);
        return $appraisalRule;
    }

    /**
     * Donne l'identifiant d'archive défini par le versant
     * @param DOMElement           $archiveUnitNode
     * @param EntityInterface|null $parentEntity
     * @return string|null
     */
    private function getTransferringAgencyIdentifier(
        DOMElement $archiveUnitNode,
        EntityInterface $parentEntity = null
    ): ?string {
        /** @var DOMElement $node */
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $id = $parentEntity
                    ? 'TransferringAgencyObjectIdentifier'
                    : 'TransferringAgencyArchiveIdentifier';
                $node = $this->xpath->query(
                    'ns:' . $id,
                    $archiveUnitNode
                )->item(0);
                return $node ? trim($node->nodeValue) : null;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $node = $this->xpath->query(
                    'ns:Content/ns:TransferringAgencyArchiveUnitIdentifier',
                    $archiveUnitNode
                )->item(0);
                return $node ? trim($node->nodeValue) : null;
            default:
                return null;
        }
    }

    /**
     * Donne l'identifiant d'archive défini par le producteur
     * @param DOMElement           $archiveUnitNode
     * @param EntityInterface|null $parentEntity
     * @return string|null
     */
    private function getOriginatingAgencyIdentifier(
        DOMElement $archiveUnitNode,
        EntityInterface $parentEntity = null
    ): ?string {
        /** @var DOMElement $node */
        switch ($this->namespace) {
            case NAMESPACE_SEDA_10:
                $id = $parentEntity
                    ? 'OriginatingAgencyObjectIdentifier'
                    : 'OriginatingAgencyArchiveIdentifier';
                $node = $this->xpath->query(
                    'ns:' . $id,
                    $archiveUnitNode
                )->item(0);
                return $node ? trim($node->nodeValue) : null;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $node = $this->xpath->query(
                    'ns:Content/ns:OriginatingAgencyArchiveUnitIdentifier',
                    $archiveUnitNode
                )
                    ->item(0);
                return $node ? trim($node->nodeValue) : null;
            default:
                return null;
        }
    }

    /**
     * Incrémente le compteur, lance la commande touch tout les 100
     */
    private function inc()
    {
        $this->countSave++;
        if ($this->countSave % 100 === 0) {
            $this->out(sprintf('%d / %d', $this->countSave, $this->countXML));
        }
    }

    /**
     * Ajoute une entrée dans ArchiveDescriptions pour l'archive_unit
     * @param DOMElement           $archiveUnitNode
     * @param EntityInterface      $archiveUnitEntity
     * @param EntityInterface|null $parentEntity
     * @param EntityInterface|null $descriptionAccessRule
     * @return EntityInterface
     * @throws Exception
     */
    private function createDescription(
        DOMElement $archiveUnitNode,
        EntityInterface $archiveUnitEntity,
        EntityInterface $parentEntity = null,
        EntityInterface &$descriptionAccessRule = null
    ) {
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
                $contentDescription = $this->xpath->query(
                    'ns:ContentDescription',
                    $archiveUnitNode
                )->item(0);
                $description = $this->xpath->query(
                    'ns:Description',
                    $contentDescription
                )->item(0);
                $description = $description ?
                    trim($description->nodeValue)
                    : null;
                $history = $this->xpath->query(
                    'ns:CustodialHistory',
                    $contentDescription
                )->item(0);
                if ($history) {
                    $history = trim($history->nodeValue);
                }
                $file_plan_position = $this->xpath->query(
                    'ns:FilePlanPosition',
                    $contentDescription
                )->item(0);
                $oldest_date = $this->xpath->query(
                    'ns:OldestDate',
                    $contentDescription
                )->item(0);
                $latest_date = $this->xpath->query(
                    'ns:LatestDate',
                    $contentDescription
                )->item(0);
                break;
            case NAMESPACE_SEDA_10:
                $contentDescription = $this->xpath->query(
                    'ns:ContentDescription',
                    $archiveUnitNode
                )->item(0);
                $description = $this->xpath->query(
                    'ns:Description',
                    $contentDescription
                )->item(0);
                $description = $description ?
                    trim($description->nodeValue)
                    : null;
                $history = [];
                $items = $this->xpath->query(
                    'ns:CustodialHistory/ns:CustodialHistoryItem',
                    $contentDescription
                );
                /** @var DOMElement $item */
                foreach ($items as $item) {
                    $history[] = trim($item->nodeValue, '. ');
                }
                $history = implode('. ', $history);
                $file_plan_position = $this->xpath->query(
                    'ns:FilePlanPosition',
                    $contentDescription
                )->item(0);
                $oldest_date = $this->xpath->query(
                    'ns:OldestDate',
                    $contentDescription
                )->item(0);
                $latest_date = $this->xpath->query(
                    'ns:LatestDate',
                    $contentDescription
                )->item(0);
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $contentDescription = $this->xpath->query(
                    'ns:Content',
                    $archiveUnitNode
                )->item(0);
                $descriptions = $this->xpath->query(
                    'ns:Description',
                    $contentDescription
                );
                $description = [];
                /** @var DOMElement $item */
                foreach ($descriptions as $item) {
                    $description[] = trim($item->nodeValue, '. ');
                }
                $description = implode('. ', $description);
                $history = [];
                $items = $this->xpath->query(
                    'ns:CustodialHistory/ns:CustodialHistoryItem',
                    $contentDescription
                );
                /** @var DOMElement $item */
                foreach ($items as $item) {
                    $history[] = trim($item->nodeValue, '. ');
                }
                $history = implode('. ', $history);
                $file_plan_position = $this->xpath->query(
                    'ns:FilePlanPosition',
                    $contentDescription
                )->item(0);
                $oldest_date = $this->xpath->query(
                    'ns:StartDate',
                    $contentDescription
                )->item(0);
                $latest_date = $this->xpath->query(
                    'ns:EndDate',
                    $contentDescription
                )->item(0);
                break;
        }
        $ArchiveDescriptions = TableRegistry::getTableLocator()->get(
            'ArchiveDescriptions'
        );
        $exists = $ArchiveDescriptions->find()
            ->where(['archive_unit_id' => $archiveUnitEntity->get('id')])
            ->first();
        if ($exists) {
            return $exists;
        }
        $accessRule = $this->getDescriptionAccessRule(
            $archiveUnitNode,
            $descriptionAccessRule,
            $parentEntity
        );
        $description = $ArchiveDescriptions->newEntity(
            [
                'archive_unit_id' => $archiveUnitEntity->get('id'),
                'access_rule_id' => $accessRule->get('id'),
                'description' => empty($description) ? null : $description,
                'history' => empty($history) ? null : $history,
                'file_plan_position' => empty($file_plan_position) ? null
                    : trim($file_plan_position->nodeValue),
                'oldest_date' => empty($oldest_date)
                    ? null
                    : new CakeDateTime(
                        trim($oldest_date->nodeValue)
                    ),
                'latest_date' => empty($latest_date)
                    ? null
                    : new CakeDateTime(
                        trim($latest_date->nodeValue)
                    ),
            ]
        );
        $description->set('access_rule', $accessRule);
        $descriptionAccessRule = $accessRule;
        $ArchiveDescriptions->saveOrFail($description);
        $archiveUnitEntity->set('archive_description', $description);
        if (
            $accessRule->get('id') !== $archiveUnitEntity->get('access_rule_id')
            && strpos($this->Transfer->get('message_version'), 'seda2') === false
        ) {
            $archiveUnitEntity->set('description_access_rule_id', $accessRule->get('id'));
            $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
            $ArchiveUnits->updateAll(
                ['description_access_rule_id' => $accessRule->get('id')],
                ['id' => $archiveUnitEntity->id]
            );
        }
        return $description;
    }

    /**
     * Donne un acces_rule qui dépend de la valeur dans le xml ou bien qui dépend
     * de la valeur du parent.
     * Si le parent n'a pas de valeur, on prend la valeur par défaut (AR038)
     * Si la valeur xml est identique au parent, on récupère le access_rule du parent
     * @param DOMElement           $archiveUnitNode
     * @param EntityInterface|null $descriptionAccessRule
     * @param EntityInterface|null $parentEntity
     * @return EntityInterface
     * @throws Exception
     */
    private function getDescriptionAccessRule(
        DOMElement $archiveUnitNode,
        EntityInterface $descriptionAccessRule = null,
        EntityInterface $parentEntity = null
    ) {
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
                $accessRestrictionNode = $this->xpath->query(
                    'ns:ContentDescription/ns:AccessRestriction',
                    $archiveUnitNode
                )->item(0);
                break;
            case NAMESPACE_SEDA_10:
                $accessRestrictionNode = $this->xpath->query(
                    'ns:ContentDescription/ns:AccessRestrictionRule',
                    $archiveUnitNode
                )->item(0);
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $accessRestrictionNode = $this->xpath->query(
                    'ns:Management/ns:AccessRule',
                    $archiveUnitNode
                )->item(0);
                break;
        }
        if (!empty($accessRestrictionNode)) {
            $codeTagname = ($this->namespace === NAMESPACE_SEDA_21 || $this->namespace === NAMESPACE_SEDA_22)
                ? 'Rule'
                : 'Code';
            $code = $this->xpath->query(
                'ns:' . $codeTagname,
                $accessRestrictionNode
            )->item(0);
            $startDate = $this->xpath->query(
                'ns:StartDate',
                $accessRestrictionNode
            )->item(0);
        }
        if (!empty($code)) {
            $code = trim($code->nodeValue);
            $AccessRuleCodes = TableRegistry::getTableLocator()->get(
                'AccessRuleCodes'
            );
            $rule = $AccessRuleCodes->find()
                ->select(['id', 'duration'])
                ->where(['code' => $code])
                ->firstOrFail();
            $startDate = new CakeDateTime(
                empty($startDate) ? date('Y-m-d') : trim($startDate->nodeValue)
            );
            $entityStartdate = $parentEntity
                ? Hash::get(
                    $parentEntity,
                    'archive_description.access_rule.start_date'
                )
                : $startDate;
            if ($entityStartdate instanceof CakeDate || $entityStartdate instanceof CoreDate) {
                $entityStartdate = $entityStartdate->format('Y-m-d');
            }
            if (
                $parentEntity
                && $rule->get('id') === Hash::get(
                    $parentEntity,
                    'archive_description.access_rule.access_rule_code_id'
                )
                && $startDate->format('Y-m-d') === $entityStartdate
            ) {
                return Hash::get(
                    $parentEntity,
                    'archive_description.access_rule'
                );
            }
        }
        if (!isset($rule)) {
            if ($parentEntity === null) {
                $code = 'AR038';
                $startDate = new CakeDateTime(date('Y-m-d'));
                $AccessRuleCodes = TableRegistry::getTableLocator()->get(
                    'AccessRuleCodes'
                );
                $rule = $AccessRuleCodes->find()
                    ->select(['id', 'duration'])
                    ->where(['code' => $code])
                    ->firstOrFail();
            } else {
                return $descriptionAccessRule;
            }
        }
        $AccessRules = TableRegistry::getTableLocator()->get('AccessRules');
        /** @noinspection PhpUndefinedVariableInspection */
        $accessRule = $AccessRules->newEntity(
            [
                'access_rule_code_id' => $rule->get('id'),
                'start_date' => clone $startDate,
                'end_date' => $startDate->add(
                    new DateInterval($rule->get('duration'))
                ),
            ]
        );
        $AccessRules->saveOrFail($accessRule);
        return $accessRule;
    }

    /**
     * Insert en base les archive_units de façon récursive
     * @param DOMElement           $documentNode
     * @param int                  $index
     * @param EntityInterface|null $currentArchiveUnitEntity
     * @return bool
     * @throws Exception
     */
    private function createDocumentArchiveUnit(
        DOMElement $documentNode,
        int $index,
        EntityInterface $currentArchiveUnitEntity
    ) {
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $accessRule = $currentArchiveUnitEntity->get('access_rule');
        $appraisalRule = $currentArchiveUnitEntity->get('appraisal_rule');
        $this->counter++;
        $archivalAgencyIdentifier = sprintf(
            '%s_%04d',
            $this->Archive->get('archival_agency_identifier'),
            $this->counter
        );
        $this->Archive->set('archive_unit_sequence', $this->counter);
        $Archives->updateAll(
            ['archive_unit_sequence' => $this->counter],
            ['id' => $this->Archive->id]
        );
        $name = ArchiveBinariesTable::getBinaryFilename($this->util, $documentNode);
        $entity = $ArchiveUnits->find()
            ->where(
                [
                    'archive_id' => $this->Archive->get('id'),
                    'xml_node_tagname' => $documentNode->tagName . '[' . $index . ']',
                    'parent_id' => $currentArchiveUnitEntity->get('id'),
                ]
            )
            ->first();
        if (!$entity) {
            $entity = $ArchiveUnits->newEntity(
                [
                    'state' => $ArchiveUnits->initialState,
                    'archive_id' => $this->Archive->get('id'),
                    'access_rule_id' => $accessRule->get('id'),
                    'appraisal_rule_id' => $appraisalRule->get('id'),
                    'archival_agency_identifier' => $archivalAgencyIdentifier,
                    'transferring_agency_identifier'
                    => $this->getDocumentTransferringAgencyIdentifier(
                        $documentNode
                    ),
                    'originating_agency_identifier'
                    => $this->getDocumentOriginatingAgencyIdentifier(
                        $documentNode
                    ),
                    'name' => $name,
                    'xml_node_tagname' => $documentNode->tagName . '[' . $index . ']',
                    'original_local_size' => 0,
                    'original_total_size' => 0,
                    'parent_id' => $currentArchiveUnitEntity->get('id'),
                    'original_total_count' => 0,
                    'original_local_count' => 0,
                ]
            );
            $entity->set('access_rule', $accessRule);
            $entity->set('appraisal_rule', $appraisalRule);
            if (!$ArchiveUnits->save($entity)) {
                $this->out(
                    __("Erreur lors de la sauvegarde de l'archive_unit")
                );
                $this->out(
                    Debugger::exportVar($currentArchiveUnitEntity->getErrors())
                );
                return false;
            }
        }
        $this->xpaths[$entity->id] = $this->util->getElementXpath($documentNode);
        $this->inc();
        return true;
    }

    /**
     * Donne le transferring_agency_identifier pour un Document
     * @param DOMElement $documentNode
     * @return null|string
     */
    private function getDocumentTransferringAgencyIdentifier(
        DOMElement $documentNode
    ) {
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
                return null;
            case NAMESPACE_SEDA_10:
                $transferringAgencyIdentifierNode = $this->xpath->query(
                    'ns:TransferringAgencyDocumentIdentifier',
                    $documentNode
                )->item(0);
                break;
        }
        if (!empty($transferringAgencyIdentifierNode)) {
            return $transferringAgencyIdentifierNode->nodeValue;
        }
        return null;
    }

    /**
     * Donne le originating_agency_identifier pour un Document
     * @param DOMElement $documentNode
     * @return null|string
     */
    private function getDocumentOriginatingAgencyIdentifier(
        DOMElement $documentNode
    ) {
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
                return null;
            case NAMESPACE_SEDA_10:
                $originatingAgencyIdentifierNode = $this->xpath->query(
                    'ns:OriginatingAgencyDocumentIdentifier',
                    $documentNode
                )->item(0);
                break;
        }
        if (!empty($originatingAgencyIdentifierNode)) {
            return $originatingAgencyIdentifierNode->nodeValue;
        }
        return null;
    }

    /**
     * Ajoute une entrée dans ArchiveKeywords pour l'archive_unit
     * @param DOMElement      $keywordNode
     * @param EntityInterface $descriptionAccessRule
     * @param EntityInterface $currentArchiveUnitEntity
     * @param int             $index
     * @return EntityInterface|bool
     * @throws Exception
     */
    private function createKeyword(
        DOMElement $keywordNode,
        EntityInterface $descriptionAccessRule,
        EntityInterface $currentArchiveUnitEntity,
        int $index
    ) {
        $content = $this->xpath->query('ns:KeywordContent', $keywordNode)->item(
            0
        );
        $value = trim($content->nodeValue);
        if (!$value) {
            return false;
        }
        $reference = $this->xpath->query('ns:KeywordReference', $keywordNode)
            ->item(0);
        $type = $this->xpath->query('ns:KeywordType', $keywordNode)->item(0);
        $ArchiveKeywords = TableRegistry::getTableLocator()->get(
            'ArchiveKeywords'
        );
        $conditions = [
            'archive_unit_id' => $currentArchiveUnitEntity->get('id'),
            'content' => $value,
        ];
        if ($reference) {
            $conditions['reference'] = trim($reference->nodeValue);
        } else {
            $conditions['reference IS'] = null;
        }
        if ($type) {
            $conditions['type'] = trim($type->nodeValue);
        } else {
            $conditions['type IS'] = null;
        }
        $exists = $ArchiveKeywords->find()
            ->where($conditions)
            ->first();
        if ($exists) {
            return $exists;
        }

        $access_rule = $this->getKeywordAccessRule(
            $keywordNode,
            $descriptionAccessRule
        );
        $keyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => $currentArchiveUnitEntity->get('id'),
                'access_rule_id' => $access_rule->get('id'),
                'content' => trim($content->nodeValue),
                'reference' => $reference ? trim($reference->nodeValue) : null,
                'type' => $type ? trim($type->nodeValue) : null,
                'xml_node_tagname' => $keywordNode->tagName . '[' . $index . ']',
            ]
        );
        $keyword->set('access_rule', $access_rule);
        $ArchiveKeywords->saveOrFail($keyword, ['ignoreCallbacks' => true]);
        if (
            $descriptionAccessRule !== $access_rule
            && strpos($this->Transfer->get('message_version'), 'seda2') === false
        ) {
            $maxEndDate = $currentArchiveUnitEntity->get('max_keywords_access_end_date');
            if (!$maxEndDate || $maxEndDate < $access_rule->get('end_date')) {
                $currentArchiveUnitEntity->set('max_keywords_access_end_date', $access_rule->get('end_date'));
                $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
                $ArchiveUnits->updateAll(
                    ['max_keywords_access_end_date' => $access_rule->get('end_date')],
                    ['id' => $currentArchiveUnitEntity->id]
                );
            }
        }
        return $keyword;
    }

    /**
     * Donne un acces_rule qui dépend de la valeur dans le xml ou bien qui dépend
     * de la valeur du parent.
     * Si le parent n'a pas de valeur, on prend la valeur de la description
     * Si la valeur xml est identique au parent, on récupère le access_rule du parent
     * @param DOMElement      $keywordNode
     * @param EntityInterface $descriptionAccessRule
     * @return EntityInterface
     * @throws Exception
     */
    private function getKeywordAccessRule(
        DOMElement $keywordNode,
        EntityInterface $descriptionAccessRule
    ) {
        // Note l'access rule du seda2.1 n'est plus lié à archive_keyword
        switch ($this->namespace) {
            case NAMESPACE_SEDA_02:
                $accessRestrictionNode = $this->xpath->query(
                    'ns:AccessRestriction',
                    $keywordNode
                )->item(0);
                break;
            case NAMESPACE_SEDA_10:
                $accessRestrictionNode = $this->xpath->query(
                    'ns:AccessRestrictionRule',
                    $keywordNode
                )->item(0);
                break;
        }
        if (!empty($accessRestrictionNode)) {
            $code = $this->xpath->query(
                'ns:Code',
                $accessRestrictionNode
            )->item(0);
            $startDate = $this->xpath->query(
                'ns:StartDate',
                $accessRestrictionNode
            )->item(0);
        } else {
            return $descriptionAccessRule;
        }
        if (!empty($code)) {
            $code = trim($code->nodeValue);
            $AccessRuleCodes = TableRegistry::getTableLocator()->get(
                'AccessRuleCodes'
            );
            $rule = $AccessRuleCodes->find()
                ->select(['id', 'duration'])
                ->where(['code' => $code])
                ->firstOrFail();
            $startDate = new CakeDateTime(
                empty($startDate) ? date('Y-m-d') : trim($startDate->nodeValue)
            );
            $oldStartDate = $descriptionAccessRule->get('start_date');
            if (($oldStartDate instanceof DateTimeInterface || $oldStartDate instanceof \Cake\I18n\Date)) {
                $oldStartDate = $oldStartDate->format('Y-m-d');
            }
            if (
                $rule->get('id') === $descriptionAccessRule->get(
                    'access_rule_code_id'
                )
                && $startDate->format('Y-m-d') === $oldStartDate
            ) {
                return $descriptionAccessRule;
            }
        }
        $AccessRules = TableRegistry::getTableLocator()->get('AccessRules');
        /** @noinspection PhpUndefinedVariableInspection */
        $accessRule = $AccessRules->newEntity(
            [
                'access_rule_code_id' => $rule->get('id'),
                'start_date' => clone $startDate,
                'end_date' => $startDate->add(
                    new DateInterval($rule->get('duration'))
                ),
            ]
        );
        $AccessRules->saveOrFail($accessRule);
        return $accessRule;
    }

    /**
     * Met à jour les compteurs, les champs de recherches et de DUA de l'archive
     * @return void
     * @throws ZMQSocketException
     * @throws DateInvalidTimeZoneException
     */
    private function updateArchive()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');
        $ArchiveDescriptions = $loc->get('ArchiveDescriptions');
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $ArchiveUnits->updateSearchField(
            ['Archives.id' => $this->Archive->id],
            function ($i, $count) {
                if ($i % 100 === 0) {
                    $this->out(
                        sprintf('updateSearchField %d / %d', $i, $count)
                    );
                }
            }
        );
        $this->countSave = 0;
        $ArchiveUnits->updateFullSearchField(
            ['Archives.id' => $this->Archive->id],
            function ($i, $count) {
                if ($i % 100 === 0) {
                    $this->out(
                        sprintf('updateFullSearchField %d / %d', $i, $count)
                    );
                }
            }
        );
        $this->out(
            __("ArchiveUnits créées pour l'archive {0}", $this->Archive->id)
        );

        // dua root
        $this->countSave = 0;
        $ArchiveUnits->calcDuaExpiredRoot(
            ['ArchiveUnits.archive_id' => $this->Archive->id],
            function ($i, $count) {
                if ($i % 100 === 0) {
                    $this->out(
                        sprintf('calcDuaExpiredRoot %d / %d', $i, $count)
                    );
                }
            }
        );

        $units_count = $ArchiveUnits->find()->where(
            ['archive_id' => $this->Archive->get('id')]
        )->count();

        // met à jour next_archive
        $nextDescription = $ArchiveDescriptions->find()
            ->select(['end_date' => 'DescriptionAccessRules.end_date'])
            ->innerJoinWith('DescriptionAccessRules')
            ->innerJoinWith('ArchiveUnits')
            ->where(['ArchiveUnits.archive_id' => $this->Archive->id])
            ->andWhere(
                [
                    'DescriptionAccessRules.end_date >' => (new DateTime())->format(
                        'Y-m-d'
                    ),
                ]
            )
            ->orderBy(['DescriptionAccessRules.end_date' => 'asc'])
            ->first();
        $nextKeyword = $ArchiveKeywords->find()
            ->select(['end_date' => 'KeywordAccessRules.end_date'])
            ->innerJoinWith('KeywordAccessRules')
            ->innerJoinWith('ArchiveUnits')
            ->where(['ArchiveUnits.archive_id' => $this->Archive->id])
            ->andWhere(
                [
                    'KeywordAccessRules.end_date >' => (new DateTime())->format(
                        'Y-m-d'
                    ),
                ]
            )
            ->orderBy(['KeywordAccessRules.end_date' => 'asc'])
            ->first();
        $nextDescription = $nextDescription ? $nextDescription->get(
            'end_date'
        ) : null;
        $nextKeyword = $nextKeyword ? $nextKeyword->get('end_date') : null;
        $next = $nextDescription && $nextKeyword
            ? min($nextDescription, $nextKeyword)
            : ($nextDescription ?: $nextKeyword);
        $this->Archive->set('next_search', $next);
        $this->Archive->set('units_count', $units_count);
        $Archives->transitionOrFail($this->Archive, ArchivesTable::T_DESCRIBE);
        $Archives->saveOrFail($this->Archive);

        Utility::get('Notify')->emit(
            'archive_state_update',
            [
                'id' => $this->Archive->id,
                'state' => $this->Archive->get('state'),
                'statetrad' => $this->Archive->get('statetrad'),
                'units_count' => $units_count,
            ]
        );
    }

    /**
     * Pour chaque archive d'un transfert, créé les archive_units
     * @param int $archive_id
     * @throws CantWorkException
     * @throws DateInvalidTimeZoneException
     * @throws VolumeException
     * @throws ZMQSocketException
     */
    public function createArchiveUnitsForArchive($archive_id)
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');

        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');
        $this->Archive = $Archives->find()
            ->where(
                [
                    'Archives.id' => $archive_id,
                ]
            )
            ->contain(
                [
                    'Transfers',
                    'AccessRules',
                    'AppraisalRules',
                    'ArchiveFiles',
                ]
            )
            ->first();
        if (!$this->Archive) {
            $this->out(
                __("Impossible de trouver l'archive id={0}", $archive_id)
            );
            throw new CantWorkException();
        }
        $manager = new VolumeManager(
            $this->Archive->get('secure_data_space_id')
        );
        $storagePath = $this->Archive->get('storage_path');
        foreach ($manager->getVolumes() as $volume_id => $volume) {
            if ($volume && $volume->dirExists($storagePath)) {
                throw new Exception(
                    sprintf(
                        '%s already exists in volume %d',
                        $storagePath,
                        $volume_id
                    )
                );
            }
        }

        $countDBArchiveUnits = $ArchiveUnits->find()
            ->where(['archive_id' => $archive_id])
            ->count();

        $archive = $this->xpath->query($path = $this->Archive->get('xpath'))
            ->item(0);
        if (!$archive instanceof DOMElement) {
            throw new Exception(
                sprintf(
                    '%s not found in archive %d',
                    $path,
                    $this->Archive->get('id')
                )
            );
        }
        $countXML = $archive->getElementsByTagName('ArchiveObject')->count()
            + $archive->getElementsByTagName('Contains')->count()
            + $archive->getElementsByTagName('Document')->count()
            + $archive->getElementsByTagName('ArchiveUnit')->count()
            + $archive->getElementsByTagName('DataObjectGroupReferenceId')
                ->count()
            + 1;
        $this->countXML = $countXML;
        $this->out(__("Nombre d'archive-units à traiter: {0}", $countXML));

        if ($countDBArchiveUnits === $countXML) {
            $this->out(
                __n(
                    "Une archive_unit existe déjà pour l'archive {0}.",
                    "Des archive_units existent déjà pour l'archive {0}.",
                    $countDBArchiveUnits,
                    $this->Archive->get('id')
                )
            );
            throw new CantWorkException();
        }

        $conn = $Archives->getConnection();
        $conn->begin();
        $exception = null;
        $success = $this->createArchiveUnits($archive);
        if ($success) {
            $this->updateArchive();
            $conn->commit();
        } else {
            $conn->rollback();
            throw new Exception('failed', 0, $exception);
        }
    }

    /**
     * Getter pour les xpaths d'archive_units
     * @return array
     */
    public function getXpaths()
    {
        return $this->xpaths;
    }
}
