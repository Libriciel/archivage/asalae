<?php

/**
 * Asalae\Worker\AnalyseWorker
 */

namespace Asalae\Worker;

use Asalae\Model\Table\TransfersTable;
use Asalae\Utility\TransferAnalyse;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Command\WorkerInterface;
use Beanstalk\Exception\CantWorkException;
use Cake\ORM\TableRegistry;
use DateInvalidTimeZoneException;
use Exception;

/**
 * Effectue l'analyse d'un transfert
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AnalyseWorker extends AbstractAsalaeWorkerV4 implements WorkerInterface
{
    public const string TUBE = 'analyse';

    /**
     * @var array Prérequis du worker ['monprerequis' => true, ...]
     */
    public $prerequisites = [
        [
            'name' => 'siegfried',
            'command' => 'sf -version',
            'missing' => null,
        ],
        [
            'name' => 'clamav',
            'command' => 'clamdscan --version',
            'missing' => null,
        ],
    ];

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws CantWorkException
     * @throws DateInvalidTimeZoneException
     */
    public function work($data)
    {
        $this->out(__("Analyse du transfert {0}", $data['transfer_id']));
        /** @var TransfersTable $Transfers */
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $transfer = $Transfers->find()
            ->where(['Transfers.id' => $data['transfer_id']])
            ->first();
        if (!$transfer) {
            $this->out(
                __(
                    "Impossible de trouver le transfert id={0}",
                    $data['transfer_id']
                )
            );
            throw new CantWorkException();
        }
        if (($state = $transfer->get('state')) !== 'analysing') {
            $this->out(
                $message = __(
                    "L'état du transfert ({0}) ne permet pas d'effectuer le job",
                    $state
                )
            );
            throw new CantWorkException($message);
        }

        /** @var TransferAnalyse $analyse */
        $analyse = Utility::get(TransferAnalyse::class, $transfer);
        try {
            $analyse->analyse(
                function ($filename, $i, $count) {
                    if ($i % 100 === 0) {
                        $this->out(sprintf('%d / %d', $i, $count));
                    }
                }
            );
        } catch (Exception $e) {
            throw new Exception(__("Analyse antivirus impossible"), 0, $e);
        } finally {
            Utility::destruct(TransferAnalyse::class);
        }

        $Transfers->transitionOrFail($transfer, TransfersTable::T_CONTROL);
        $Transfers->saveOrFail($transfer);

        $this->emit('control', $data);
    }
}
