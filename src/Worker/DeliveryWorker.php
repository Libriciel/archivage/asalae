<?php

/**
 * Asalae\Worker\DeliveryWorker
 */

namespace Asalae\Worker;

use Asalae\Model\Entity\Delivery;
use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Entity\StoredFile;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsDeliveryRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\DeliveriesTable;
use Asalae\Model\Table\DeliveryRequestsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Datacompressor\Utility\DataCompressor;
use ErrorException;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Throwable;

/**
 * Créer la communication
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DeliveryWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var ArchivesTable
     */
    private $Archives;

    /**
     * @var ArchiveUnitsTable
     */
    private $ArchiveUnits;

    /**
     * @var DeliveryRequestsTable
     */
    private $DeliveryRequests;

    /**
     * @var CountersTable
     */
    private $Counters;

    /**
     * @var ArchiveUnitsDeliveryRequestsTable
     */
    private $ArchiveUnitsDeliveryRequests;

    /**
     * @var DeliveriesTable
     */
    private $Deliveries;

    /**
     * @var EventLogsTable
     */
    private $EventLogs;

    /**
     * @var VolumeManager[]
     */
    private $dataSpaces = [];

    /**
     * WorkerInterface constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $loc = TableRegistry::getTableLocator();
        $this->ArchiveUnits = $loc->get('ArchiveUnits');
        $this->ArchiveUnitsDeliveryRequests = $loc->get(
            'ArchiveUnitsDeliveryRequests'
        );
        $this->Archives = $loc->get('Archives');
        $this->Counters = $loc->get('Counters');
        $this->Deliveries = $loc->get('Deliveries');
        $this->DeliveryRequests = $loc->get('DeliveryRequests');
        $this->EventLogs = $loc->get('EventLogs');
    }

    /**
     * Effectue le travail voulu
     * @param mixed $data
     * @throws Exception|Throwable
     */
    public function work($data)
    {
        if (empty($data['delivery_request_id'])) {
            $this->out(__("Job vide"));
            throw new CantWorkException();
        }
        $this->dataSpaces = [];
        $this->out(
            __("Demande de communication id={0}", $data['delivery_request_id'])
        );
        $deliveryRequest = $this->DeliveryRequests->find()
            ->where(['DeliveryRequests.id' => $data['delivery_request_id']])
            ->contain(
                [
                    'Requesters' => ['ArchivalAgencies'],
                    'ArchiveUnits',
                ]
            )
            ->firstOrFail();
        /** @var Delivery $delivery */
        $delivery = $this->Deliveries->find()
            ->where(['Deliveries.delivery_request_id' => $deliveryRequest->id])
            ->first();
        if ($delivery && $delivery->get('state') !== DeliveriesTable::S_CREATING) {
            $this->out(
                __(
                    "Une communication existe déjà pour la demande n°{0}",
                    $data['delivery_request_id']
                )
            );
            throw new CantWorkException();
        } elseif (!$delivery) {
            $identifier = $this->Counters->next(
                Hash::get($deliveryRequest, 'requester.archival_agency.id'),
                'ArchiveDelivery'
            );
            $query = $this->ArchiveUnitsDeliveryRequests->query();
            /** @var EntityInterface $links */
            $links = $this->ArchiveUnitsDeliveryRequests->find()
                ->select(
                    [
                        'archive_units_count' => $query->func()->count('*'),
                        'original_count' => $query->func()->sum(
                            'ArchiveUnits.original_total_count'
                        ),
                        'original_size' => $query->func()->sum(
                            'ArchiveUnits.original_total_size'
                        ),
                    ]
                )
                ->innerJoinWith('ArchiveUnits')
                ->where(['delivery_request_id' => $deliveryRequest->id])
                ->all()
                ->toArray()
            [0];
            $units = [];
            /** @var EntityInterface $archiveUnit */
            foreach ($deliveryRequest->get('archive_units') as $archiveUnit) {
                $units[] = '- ' . $archiveUnit->get('archival_agency_identifier')
                    . ' - ' . $archiveUnit->get('name');
            }
            /** @var Delivery $delivery */
            $delivery = $this->Deliveries->newEntity(
                [
                    'identifier' => $identifier,
                    'delivery_request_id' => $data['delivery_request_id'],
                    'state' => $this->Deliveries->initialState,
                    'comment' => __n(
                        "Communication de l'unité d'archives demandée par {0} :\n{1}",
                        "Communication des unités d'archives demandées par {0} :\n{1}",
                        $links->get('archive_units_count'),
                        Hash::get($deliveryRequest, 'requester.name'),
                        implode("\n", $units)
                    ),
                ] + array_map('intval', $links->toArray())
            );
            $conn = $this->Deliveries->getConnection();
            $conn->begin();
            $this->Deliveries->saveOrFail($delivery);
            $delivery->set('delivery_request', $deliveryRequest);
            Filesystem::dumpFile(
                $delivery->get('xml'),
                $delivery->generateXml()
            );
            $lifecycles = [];
            $archiveIds = [];
            foreach ($deliveryRequest->get('archive_units') as $archiveUnit) {
                $entry = $this->EventLogs->newEntry(
                    'add_delivery',
                    'success',
                    __d(
                        'delivery_worker',
                        "Communication de l'unité d'archives {0}"
                        . " et de ses sous unités d'archives dans la communication {1}",
                        $archiveUnit->get('archival_agency_identifier'),
                        $identifier
                    ),
                    $data['user_id'] ?? '',
                    $delivery
                );
                $entry->set('in_lifecycle', false);
                $this->EventLogs->saveOrFail($entry);
                $archive_id = $archiveUnit->get('archive_id');
                if (!isset($lifecycles[$archive_id])) {
                    $lifecycles[$archive_id] = [];
                    $archiveIds[] = $archive_id;
                }
                $lifecycles[$archive_id][] = $entry;
            }
            $conn->commit();
            $archives = $this->Archives->find()
                ->where(['Archives.id IN' => $archiveIds])
                ->contain(['LifecycleXmlArchiveFiles' => ['StoredFiles']]);
            foreach ($archives as $archive) {
                $storedFile = Hash::get(
                    $archive,
                    'lifecycle_xml_archive_file.stored_file',
                    []
                );
                foreach ($lifecycles[$archive->id] as $entry) {
                    $this->Archives->updateLifecycleXml($storedFile, $entry);
                }
            }
        }
        $zip = $delivery->get('zip');
        if (is_file($zip)) {
            $this->out(__("Le fichier ZIP existe déjà"));
            throw new CantWorkException();
        }
        $this->out(
            __(
                "Traitement de la demande de communication {0}",
                $deliveryRequest->get('identifier')
            )
        );

        $archives = $this->Archives->find()
            ->distinct('Archives.id')
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.ArchiveUnitsDeliveryRequests')
            ->where(
                ['ArchiveUnitsDeliveryRequests.delivery_request_id' => $deliveryRequest->id]
            )
            ->contain(
                [
                    'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                    'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                ]
            );

        /** @var EntityInterface $archive */
        foreach ($archives as $archive) {
            $this->out(
                __("archive {0}", $archive->get('archival_agency_identifier'))
            );
            /** @var DescriptionXmlArchiveFile $desc */
            $desc = $archive->get('description_xml_archive_file');
            $util = new DOMUtility($desc->getDom());

            $this->lifecycle($archive, $delivery);
            $archiveUnits = $this->ArchiveUnits->find()
                ->innerJoinWith('ArchiveUnitsDeliveryRequests')
                ->where(
                    [
                        'ArchiveUnitsDeliveryRequests.delivery_request_id' => $deliveryRequest->id,
                        'ArchiveUnits.archive_id' => $archive->id,
                    ]
                );
            /** @var EntityInterface $archiveUnit */
            foreach ($archiveUnits as $archiveUnit) {
                $this->out(
                    __(
                        "unité d'archives {0}",
                        $archiveUnit->get('archival_agency_identifier')
                    )
                );
                $xpath = $archiveUnit->get('xpath');
                $element = $util->xpath->query($xpath)->item(0);
                if (!$element) {
                    throw new Exception(
                        sprintf(
                            'ArchiveUnit.%d (%s) not found in Archive.%d',
                            $archiveUnit->id,
                            $xpath,
                            $archive->id
                        )
                    );
                }
                $xml = $util->dom->saveXML($element);
                $this->archiveUnitDescription(
                    $archive,
                    $archiveUnit,
                    $delivery,
                    $xml
                );
                $this->archiveUnitFiles($archive, $archiveUnit, $delivery);
            }
        }
        $this->out(__("Création du fichier {0}", $zip));
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        try {
            DataCompressor::compress($delivery->get('basepath'), $zip);
        } catch (Throwable $e) {
            restore_error_handler();
            throw $e;
        }
        restore_error_handler();

        $this->Deliveries->transitionOrFail(
            $delivery,
            DeliveriesTable::T_BUILD
        );
        $this->Deliveries->saveOrFail($delivery);
        $this->DeliveryRequests->transitionOrFail(
            $deliveryRequest,
            DeliveryRequestsTable::T_BUILD_DELIVERY
        );
        $this->DeliveryRequests->saveOrFail($deliveryRequest);
        Filesystem::remove($delivery->get('basepath'));
    }

    /**
     * Créé le lifecycle au besoin
     * @param EntityInterface $archive
     * @param Delivery        $delivery
     * @throws Exception
     */
    private function lifecycle(EntityInterface $archive, Delivery $delivery)
    {
        $path = $delivery->getArchiveLifecyclePath($archive);
        if (is_file($path) && filesize($path) === 0) {
            unlink($path);
        }
        if (!is_file($path)) {
            /** @var StoredFile $storedFile */
            $storedFile = Hash::get(
                $archive,
                'lifecycle_xml_archive_file.stored_file',
                []
            );
            $this->getVolumeManager($archive)->fileDownload(
                $storedFile->get('name'),
                $path
            );
            if (
                hash_file(
                    $storedFile->get('hash_algo'),
                    $path
                ) !== $storedFile->get('hash')
            ) {
                unlink($path);
                throw new Exception(
                    "Corrupted file: " . $storedFile->id . ' - ' . $storedFile->get(
                        'name'
                    )
                );
            }
        }
    }

    /**
     * Donne le VolumeManager lié à une archive
     * @param EntityInterface $archive
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager(EntityInterface $archive): VolumeManager
    {
        if (!isset($this->dataSpaces[$archive->get('secure_data_space_id')])) {
            $this->dataSpaces[$archive->get('secure_data_space_id')]
                = new VolumeManager($archive->get('secure_data_space_id'));
        }
        return $this->dataSpaces[$archive->get('secure_data_space_id')];
    }

    /**
     * Créé la description de l'unité d'archives au besoin
     * @param EntityInterface $archive
     * @param EntityInterface $archiveUnit
     * @param Delivery        $delivery
     * @param string          $xml
     * @throws Exception
     */
    private function archiveUnitDescription(
        EntityInterface $archive,
        EntityInterface $archiveUnit,
        Delivery $delivery,
        string $xml
    ) {
        $path = $delivery->getArchiveUnitDescriptionPath(
            $archive,
            $archiveUnit
        );
        if (is_file($path) && (filesize($path) === 0 || md5_file($path) !== md5($xml))) { // NOSONAR
            unlink($path);
        }
        if (!is_file($path)) {
            Filesystem::dumpFile($path, $xml);
        }
    }

    /**
     * Ecrit les fichiers des unités d'archives
     * @param EntityInterface $archive
     * @param EntityInterface $archiveUnit
     * @param Delivery        $delivery
     * @throws Exception
     */
    private function archiveUnitFiles(
        EntityInterface $archive,
        EntityInterface $archiveUnit,
        Delivery $delivery
    ) {
        $basePath = $delivery->getArchivePath($archive);
        $query = $this->ArchiveUnits->find()
            ->innerJoinWith('ArchiveBinaries')
            ->where(
                [
                    'ArchiveUnits.archive_id' => $archive->id,
                    'ArchiveUnits.lft >=' => $archiveUnit->get('lft'),
                    'ArchiveUnits.rght <=' => $archiveUnit->get('rght'),
                ]
            )
            ->contain(
                [
                    'ArchiveBinaries' => function (Query $q) {
                        return $q->where(
                            [
                                'ArchiveBinaries.type IN' => [
                                    'original_data',
                                    'original_timestamp',
                                    'dissemination_data',
                                    'dissemination_timestamp',
                                ],
                            ]
                        )->contain(['StoredFiles']);
                    },
                ]
            );
        /** @var EntityInterface $unit */
        foreach ($query as $unit) {
            /** @var EntityInterface $binary */
            foreach ($unit->get('archive_binaries') as $binary) {
                $path = $basePath . DS
                    . $binary->get('type') . DS
                    . $binary->get('filename');
                if (is_file($path) && filesize($path) === 0) {
                    unlink($path);
                }
                if (is_file($path)) {
                    continue;
                }
                /** @var StoredFile $storedFile */
                $storedFile = $binary->get('stored_file');
                $this->getVolumeManager($archive)->fileDownload(
                    $storedFile->get('name'),
                    $path
                );
                if (hash_file($storedFile->get('hash_algo'), $path) !== $storedFile->get('hash')) {
                    unlink($path);
                    throw new Exception(
                        "Corrupted file: " . $storedFile->id . ' - '
                        . $storedFile->get('name')
                    );
                }
            }
        }
    }
}
