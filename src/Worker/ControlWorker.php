<?php

/**
 * Asalae\Worker\ControlWorker
 */

namespace Asalae\Worker;

use Asalae\Model\Table\TransferErrorsTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\ValidationProcessesTable;
use Asalae\Utility\ControlTrait;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\PreControlMessages;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\ORM\Exception\PersistenceFailedException;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Effectue le contrôle (check) sur un transfert
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ControlWorker extends AbstractAsalaeWorkerV4
{
    /**
     * Traits
     */
    use ControlTrait;

    /**
     * @var array Prérequis du worker ['monprerequis' => true, ...]
     */
    public $prerequisites = [
        [
            'name' => 'siegfried',
            'command' => 'sf -version',
            'missing' => null,
        ],
        [
            'name' => 'imagemagick',
            'command' => 'identify -version',
            'missing' => null,
        ],
        [
            'name' => 'mediainfo',
            'command' => 'mediainfo --help; [ $? -eq 0 -o $? -eq 255 ]',
            'missing' => null,
        ],
        [
            'name' => 'poppler-utils',
            'command' => 'pdfinfo --help',
            'missing' => null,
        ],
        [
            'name' => 'catdoc',
            'command' => 'catdoc',
            'missing' => null,
        ],
        [
            'name' => 'zip',
            'command' => 'zip --help',
            'missing' => null,
        ],
        [
            'name' => 'gzip',
            'command' => 'gzip --help',
            'missing' => null,
        ],
        [
            'name' => 'bzip2',
            'command' => 'bzip2 --help',
            'missing' => null,
        ],
        [
            'name' => 'gzip',
            'command' => 'gzip --help',
            'missing' => null,
        ],
        [
            'name' => 'webp',
            'command' => 'dwebp -version',
            'missing' => null,
        ],
        [
            'name' => 'libxml2-utils',
            'command' => 'xmllint -version',
            'missing' => null,
        ],
        [
            'name' => 'xlsx2csv',
            'command' => 'xlsx2csv --help',
            'missing' => null,
        ],
    ];
    /**
     * @var int
     */
    private $countSave;

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        unset($this->util);
        $this->out(
            __("Contrôle du transfert id={0}", $data['transfer_id'])
        );
        /** @var TransfersTable $Transfers */
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        /** @var TransferErrorsTable $TransferErrors */
        $TransferErrors = TableRegistry::getTableLocator()->get(
            'TransferErrors'
        );
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = TableRegistry::getTableLocator()->get(
            'ValidationProcesses'
        );

        $this->Transfer = $Transfers->find()
            ->where(['Transfers.id' => $data['transfer_id']])
            ->first();
        if (!$this->Transfer) {
            $this->out(
                __(
                    "Impossible de trouver le transfert id={0}",
                    $data['transfer_id']
                )
            );
            throw new CantWorkException();
        }
        if ($this->Transfer->get('state') !== TransfersTable::S_CONTROLLING) {
            $this->out(
                __(
                    "Etat du transfert: {0}. Annulation du contrôle.",
                    $this->Transfer->get('state')
                )
            );
            throw new CantWorkException();
        }

        $this->PreControlMessages = new PreControlMessages(
            $this->Transfer->get('xml')
        );

        $errors = [];
        $this->validateMessage($errors);
        $this->validateAttachments(
            $errors,
            function ($filename, $i, $count) {
                if ($i % 100 === 0) {
                    $this->out(sprintf('node %d / %d', $i, $count));
                }
            },
            function ($filename, $i, $count) {
                if ($i % 100 === 0) {
                    $this->out(sprintf('attachement %d / %d', $i, $count));
                }
            }
        );
        $sa = $this->validateSa($errors);
        $sv = $this->validateSv($errors, $sa);
        $this->validateSp($errors, $sa);
        $this->validatekeywords($errors);
        $this->validateProfiles($errors, $sa);
        $this->validateServiceLevels($errors, $sa);
        $firstAgreement = $this->validateAgreements($errors, $sa, $sv);
        $this->validateRules($errors, $sa);
        $this->validateBinaries($errors);
        $this->validateDataObjectReferences($errors);
        $this->validateArchiveUnit($errors);
        $this->validateComposite($errors, $sa);
        $valid = true;
        $conn = $TransferErrors->getConnection();
        $conn->begin();
        $TransferErrors->deleteAll(
            ['transfer_id' => $this->Transfer->get('id')]
        );
        try {
            foreach ($errors as $value) {
                $TransferErrors->saveOrFail($TransferErrors->newEntity($value));
                if ($value['level'] === 'error') {
                    $valid = false;
                }
            }
            $this->Transfer->set('is_conform', $valid);
            if (
                $valid
                && ($firstAgreement && $firstAgreement->get('auto_validate'))
            ) {
                $Transfers->transitionOrFail(
                    $this->Transfer,
                    TransfersTable::T_ARCHIVE
                );
                $this->Transfer->set('is_accepted', true);
                $emitData = $data;
                $message = __(
                    "Le transfert {0} est conforme et a été validé automatiquement.",
                    $this->Transfer->get('transfer_identifier')
                );
                $notifyClass = 'alert-success';
            } else {
                $Transfers->transitionOrFail(
                    $this->Transfer,
                    TransfersTable::T_VALIDATE
                );
                $params = [
                    'user_id' => $data['user_id'] ?? null,
                    'valid' => $valid,
                    'org_entity_id' => $this->Transfer->get(
                        'archival_agency_id'
                    ),
                    'agreement' => $firstAgreement,
                ];
                try {
                    $Transfers->saveOrFail(
                        $this->Transfer
                    ); // set is_accepted pour les mails
                    // @throws NotFoundException si pas de circuit trouvé
                    $process = $ValidationProcesses->createNewProcess(
                        $this->Transfer,
                        $params
                    );
                    $ValidationProcesses->saveOrFail($process);
                    $ValidationProcesses->notifyProcess($process);
                    if ($valid) {
                        $message = __(
                            "Le transfert {0} est conforme.",
                            $this->Transfer->get('transfer_identifier')
                        );
                        $notifyClass = 'alert-info';
                        if (
                            $firstAgreement && $firstAgreement->get(
                                'auto_validate'
                            )
                        ) {
                            $Transfers->patchEntity(
                                $this->Transfer,
                                ['is_accepted' => true]
                            );
                            $Transfers->transitionOrFail(
                                $this->Transfer,
                                TransfersTable::T_ARCHIVE
                            );
                        }
                    } else {
                        $message = __(
                            "Le transfert {0} est non conforme.",
                            $this->Transfer->get('transfer_identifier')
                        );
                        $notifyClass = 'alert-warning';
                    }
                } catch (PersistenceFailedException | RecordNotFoundException) {
                    $message = __(
                        "Le transfert {0} est {1}. Il a été refusé automatiquement 
                            car le circuit de validation n'a pas été trouvé.",
                        $this->Transfer->get('transfer_identifier'),
                        $valid ? __("conforme") : __("non conforme")
                    );
                    $notifyClass = 'alert-warning';
                    $Transfers->patchEntity(
                        $this->Transfer,
                        ['is_accepted' => false]
                    );
                    $Transfers->transitionOrFail(
                        $this->Transfer,
                        TransfersTable::T_REFUSE
                    );
                    $emitData = $data;
                    $TransferErrors->saveOrFail(
                        $TransferErrors->newEntity(
                            [
                                'transfer_id' => $this->Transfer->get('id'),
                                'level' => 'error',
                                'code' => 300,
                                'message' => $message,
                            ]
                        )
                    );
                }
            }
            $Transfers->saveOrFail($this->Transfer);
            $conn->commit();
            if (!empty($emitData)) {
                $Transfers->createCompositeIfNeeded($this->Transfer);
                $this->emit('archive', $emitData);
            }
            Notify::send(
                $this->userId,
                [],
                __("<h4>Worker Controls</h4>") . h($message),
                $notifyClass
            );
            $this->out(__("notification envoyée à {0}: {1}", 'user_' . $this->userId, h($message)));
        } catch (Exception $e) {
            $conn->rollback();
            throw $e;
        }
    }
}
