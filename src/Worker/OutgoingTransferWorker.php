<?php

/**
 * Asalae\Worker\OutgoingTransferWorker
 */

namespace Asalae\Worker;

use Asalae\Exception\GenericException;
use Asalae\Model\Entity\OutgoingTransfer;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\OutgoingTransferRequestsTable;
use Asalae\Model\Table\OutgoingTransfersTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Transferts sortant
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OutgoingTransferWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var string
     */
    public $logfile;
    /**
     * @var EntityInterface
     */
    private $otr;
    /**
     * @var ArchiveUnitsTable
     */
    private $ArchiveUnits;
    /**
     * @var CountersTable
     */
    private $Counters;
    /**
     * @var OutgoingTransferRequestsTable
     */
    private $OutgoingTransferRequests;
    /**
     * @var OutgoingTransfersTable
     */
    private $OutgoingTransfers;

    /**
     * WorkerInterface constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $loc = TableRegistry::getTableLocator();
        $this->ArchiveUnits = $loc->get('ArchiveUnits');
        $this->Counters = $loc->get('Counters');
        $this->OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
        $this->OutgoingTransfers = $loc->get('OutgoingTransfers');
        $this->logfile = LOGS . 'worker_outgoing-transfer.log';
    }

    /**
     * Effectue le travail voulu
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        if (empty($data['outgoing_transfer_request_id'])) {
            $this->out(__("Job vide"));
            throw new CantWorkException();
        }
        $this->otr = $this->OutgoingTransferRequests->find()
            ->where(
                ['OutgoingTransferRequests.id' => $data['outgoing_transfer_request_id']]
            )
            ->firstOrFail();
        if ($this->otr->get('state') !== OutgoingTransferRequestsTable::S_ACCEPTED) {
            $this->out(
                __(
                    "Etat de la demande de transfert sortant: {0}. Annulation de l'envoi.",
                    $this->otr->get('state')
                )
            );
            throw new CantWorkException();
        }

        $archiveUnits = $this->ArchiveUnits->find()
            ->innerJoinWith('ArchiveUnitsOutgoingTransferRequests')
            ->leftJoinWith('OutgoingTransfers')
            ->where(
                [
                    'otr_id' => $this->otr->id,
                    'OR' => [
                        'OutgoingTransfers.id IS' => null,
                        'OutgoingTransfers.state !=' => OutgoingTransfersTable::S_SENT,
                    ],
                ]
            )
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'TransferringAgencies',
                        'Profiles',
                        'Agreements',
                    ],
                ]
            );
        $archiveUnitsCount = $archiveUnits->count();
        $prev = microtime(true);
        /** @var EntityInterface $archiveUnit */
        foreach ($archiveUnits as $i => $archiveUnit) {
            $ntime = microtime(true);
            // message chaques minutes
            if ($ntime - $prev > 60) {
                $prev = $ntime;
                $this->out(__("Archive {0}/{1}", $i + 1, $archiveUnitsCount));
            }

            $outgoingTransfer = $this->createOutgoingTransfer($archiveUnit);
            if (!is_file($outgoingTransfer->get('zip'))) {
                $xml = $outgoingTransfer->get('xml');
                $dir = dirname($xml);
                if (!is_dir($dir)) {
                    Filesystem::mkdir($dir);
                }
                $outgoingTransfer->generateDom()->save($xml);
                $this->downloadFiles($outgoingTransfer);
                $this->createZip($outgoingTransfer);
                $this->deleteFiles($outgoingTransfer);
            }

            if (
                $this->OutgoingTransfers->transition(
                    $outgoingTransfer,
                    OutgoingTransfersTable::T_CREATE
                )
            ) {
                $this->OutgoingTransfers->saveOrFail($outgoingTransfer);
            }

            try {
                $this->postOutgoingTransfer($outgoingTransfer);
            } catch (Exception $e) {
                if (
                    $this->OutgoingTransfers->transition(
                        $outgoingTransfer,
                        OutgoingTransfersTable::T_ERROR
                    )
                ) {
                    $outgoingTransfer->set('error_msg', $e->getMessage());
                    $this->OutgoingTransfers->saveOrFail($outgoingTransfer);
                }
                continue;
            }
            if (
                $this->OutgoingTransfers->transition(
                    $outgoingTransfer,
                    OutgoingTransfersTable::T_SEND
                )
            ) {
                $this->OutgoingTransfers->saveOrFail($outgoingTransfer);
            }
            if (is_file($outgoingTransfer->get('zip'))) {
                unlink($outgoingTransfer->get('zip'));
            }
        }
        if (isset($e)) {
            $this->OutgoingTransferRequests->transition(
                $this->otr,
                OutgoingTransferRequestsTable::T_ERROR
            );
            $this->OutgoingTransferRequests->saveOrFail($this->otr);
            throw $e;
        }

        $this->OutgoingTransferRequests->transition(
            $this->otr,
            OutgoingTransferRequestsTable::T_SEND_TRANSFERS
        );
        $this->OutgoingTransferRequests->saveOrFail($this->otr);
    }

    /**
     * Création de l'outgoing_transfer lié à un archive_unit
     * @param EntityInterface $archiveUnit
     * @return OutgoingTransfer
     * @throws Exception
     */
    private function createOutgoingTransfer(EntityInterface $archiveUnit): OutgoingTransfer
    {
        $outgoingTransfer = $this->OutgoingTransfers->find()
            ->where(
                [
                    'outgoing_transfer_request_id' => $this->otr->id,
                    'archive_unit_id' => $archiveUnit->id,
                ]
            )
            ->first();
        if (!$outgoingTransfer) {
            /** @var OutgoingTransfer $outgoingTransfer */
            $outgoingTransfer = $this->OutgoingTransfers->newEntity(
                [
                    'identifier' => $this->Counters->next(
                        $this->otr->get('archival_agency_id'),
                        'OutgoingTransfer'
                    ),
                    'comment' => __(
                        "Transferts de l'archive {0}",
                        $archiveUnit->get('archival_agency_identifier')
                    ),
                    'outgoing_transfer_request_id' => $this->otr->id,
                    'archive_unit_id' => $archiveUnit->id,
                    'state' => $this->OutgoingTransfers->initialState,
                ]
            );
            $this->OutgoingTransfers->saveOrFail($outgoingTransfer);
        }
        $outgoingTransfer->set('archive_unit', $archiveUnit);
        $outgoingTransfer->set('outgoing_transfer_request', $this->otr);
        return $outgoingTransfer;
    }

    /**
     * Télécharge les fichiers
     * @param OutgoingTransfer $outgoingTransfer
     * @throws Exception
     */
    private function downloadFiles(OutgoingTransfer $outgoingTransfer)
    {
        $secure_data_space_id = (int)Hash::get(
            $outgoingTransfer,
            'archive_unit.archive.secure_data_space_id'
        );
        $basePath = $outgoingTransfer->get('attachements_path');
        /** @var EntityInterface $archiveUnit */
        $archiveUnit = $outgoingTransfer->get('archive_unit');
        $storedFiles = $this->ArchiveUnits->find()
            ->select(
                [
                    'filename' => 'ArchiveBinaries.filename',
                    'path' => 'StoredFiles.name',
                ]
            )
            ->innerJoinWith('ArchiveBinaries')
            ->innerJoinWith('ArchiveBinaries.StoredFiles')
            ->where(
                [
                    'ArchiveUnits.lft >=' => $archiveUnit->get('lft'),
                    'ArchiveUnits.rght <=' => $archiveUnit->get('rght'),
                    'ArchiveBinaries.type' => 'original_data',
                ]
            )
            ->disableHydration();
        $manager = new VolumeManager($secure_data_space_id);
        foreach ($storedFiles as $storedFile) {
            $destination = $basePath . DS . $storedFile['filename'];
            if (!is_file($destination)) {
                $manager->fileDownload($storedFile['path'], $destination);
            }
        }
    }

    /**
     * Zip les fichiers de façon asynchrone
     * @param OutgoingTransfer $outgoingTransfer
     * @throws Exception
     */
    private function createZip(OutgoingTransfer $outgoingTransfer)
    {
        /** @var Exec $Exec */
        $Exec = Utility::get('Exec');
        $Exec->command(
            CAKE_SHELL,
            'zip_files',
            'outgoing_transfer',
            $outgoingTransfer->id
        );
        if (!is_file($outgoingTransfer->get('zip'))) {
            throw new GenericException(
                __(
                    "Echec de compression du fichier {0}",
                    basename($outgoingTransfer->get('zip'))
                )
            );
        }
    }

    /**
     * Supprime les fichiers après les avoir zippés
     * @param OutgoingTransfer $outgoingTransfer
     * @throws Exception
     */
    private function deleteFiles(OutgoingTransfer $outgoingTransfer)
    {
        Filesystem::remove($outgoingTransfer->getDataDirectory());
    }

    /**
     * Envoi du transfert sortant par API
     * @param OutgoingTransfer $outgoingTransfer
     * @throws Exception
     */
    private function postOutgoingTransfer(OutgoingTransfer $outgoingTransfer)
    {
        /** @var Exec $Exec */
        $Exec = Utility::get('Exec');
        $result = $Exec->command(
            CAKE_SHELL,
            'envoietransferts',
            'outgoing_transfer',
            $outgoingTransfer->id
        );
        $content = $result['stdout'] . PHP_EOL . $result['stderr'];
        if (strpos($content, 'done') === false) {
            $this->out(trim($content));
            throw new GenericException(
                __(
                    "Echec lors de l'envoi de {0}",
                    basename($outgoingTransfer->get('zip'))
                ) . " \n" . trim($content)
            );
        }
    }
}
