<?php

/**
 * Asalae\Worker\TimestampWorker
 */

namespace Asalae\Worker;

use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\TransfersTable;
use AsalaeCore\Driver\Timestamping\TimestampingGeneric;
use AsalaeCore\Driver\Timestamping\TimestampingLocal;
use AsalaeCore\Driver\Timestamping\TimestampingOpensign;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Utility\Timestamper;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use DateInvalidTimeZoneException;
use Libriciel\Filesystem\Utility\Filesystem;
use ReflectionException;

/**
 * Effectue l'horodatage sur un transfert
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TimestampWorker extends AbstractAsalaeWorkerV4
{
    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws CantWorkException
     * @throws DateInvalidTimeZoneException
     * @throws ReflectionException
     */
    public function work($data)
    {
        $this->out(__("Horodatage du transfert {0}", $data['transfer_id']));
        /** @var TransfersTable $Transfers */
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $TransferAttachements = TableRegistry::getTableLocator()->get(
            'TransferAttachments'
        );
        /** @var Transfer $transfer */
        $transfer = $Transfers->find()
            ->where(['Transfers.id' => $data['transfer_id']])
            ->first();
        if (!$transfer) {
            $this->out(
                __(
                    "Impossible de trouver le transfert id={0}",
                    $data['transfer_id']
                )
            );
            throw new CantWorkException();
        }
        if ($transfer->get('state') !== TransfersTable::S_TIMESTAMPING) {
            $this->out(
                __(
                    "Annulation car l'état actuel du transfert ({0}) ne permet pas l'horodatage",
                    $transfer->get('state')
                )
            );
            throw new CantWorkException();
        }
        $transactionUid = uniqid();
        Filesystem::begin($transactionUid);
        $baseDir = AbstractGenericMessageForm::getDataDirectory(
            $data['transfer_id']
        );
        $baseDirLen = strlen($baseDir);
        $tokenBasedir = $baseDir . DS . 'timestamp';

        /** @var TimestampingLocal|TimestampingOpensign|TimestampingGeneric $driver */
        if (!empty($data['ts_msg_id'])) {
            $this->out(__("Horodatage du bordereau"));
            if (!is_file($transfer->get('xml'))) {
                $this->out(
                    __(
                        "Le fichier {0} n'a pas été trouvé",
                        $transfer->get('xml')
                    )
                );
                Filesystem::rollback($transactionUid);
                throw new NotFoundException();
            }
            $driver = Timestamper::getDriverById($data['ts_msg_id']);
            $relativePath = substr($transfer->get('xml'), $baseDirLen);
            $token = $driver->generateToken($transfer->get('xml'));
            $tokenPath = $tokenBasedir . $relativePath . '.tsr';
            Filesystem::dumpFile($tokenPath, $token);
        }
        if (!empty($data['ts_pjs_id'])) {
            $basePath = $baseDir . DS . 'attachments';

            $this->out(__("Horodatage des pièces jointes"));
            $driver = Timestamper::getDriverById($data['ts_pjs_id']);

            $query = $TransferAttachements->find()
                ->where(['transfer_id' => $transfer->id]);
            $count = $query->count();
            foreach ($query as $i => $attachment) {
                if ($i % 100 === 0) {
                    $this->out(sprintf('%d / %d', $i + 1, $count));
                }
                $file = $basePath . DS . $attachment->get('filename');
                if (!is_file($file)) {
                    $this->out(__("Le fichier {0} n'a pas été trouvé", $file));
                    Filesystem::rollback($transactionUid);
                    throw new NotFoundException();
                }

                $relativePath = substr($file, $baseDirLen);
                $token = $driver->generateToken($file);
                $tokenPath = $tokenBasedir . $relativePath . '.tsr';
                Filesystem::dumpFile($tokenPath, $token);
            }
        }

        $Transfers->transition($transfer, TransfersTable::T_ANALYSE);
        $Transfers->saveOrFail($transfer);
        Filesystem::commit($transactionUid);

        $analyseData = [
            'user_id' => $data['user_id'],
            'transfer_id' => $data['transfer_id'],
        ];
        $this->emit('analyse', $analyseData);
    }
}
