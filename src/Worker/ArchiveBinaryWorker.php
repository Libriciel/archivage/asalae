<?php

/**
 * Asalae\Worker\ArchiveBinaryWorker
 */

namespace Asalae\Worker;

use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\VolumesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\DataType\CommaArrayString;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use DOMElement;
use Exception;
use FileConverters\Exception\FileNotReadableException;
use FileConverters\Utility\DetectFileFormat;
use FileConverters\Utility\FileConverters;
use ZMQSocketException;

/**
 * Créer les archive_binaries d'une archive
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveBinaryWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var array
     */
    private $xpaths;

    /**
     * @var EntityInterface[]
     */
    private $archives;

    /**
     * @var DOMUtility
     */
    private $util;

    /**
     * @var VolumeManager[]
     */
    private $managers;

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        $loc = TableRegistry::getTableLocator();
        $this->out(
            __(
                "Création des binaries du transfert id={0}",
                $data['transfer_id']
            )
        );
        $this->xpaths = [];
        $this->archives = [];
        $this->managers = [];
        $Transfers = $loc->get('Transfers');
        $transfer = $Transfers->get($data['transfer_id']);

        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');
        $query = $Archives->find()
            ->innerJoinWith('Transfers')
            ->where(['Transfers.id' => $data['transfer_id']])
            ->contain(
                [
                    'ServiceLevels',
                    'ArchivalAgencies' => [
                        'Configurations' => function (Query $q) {
                            return $q->where(
                                ['Configurations.name LIKE' => 'conversion-%']
                            );
                        },
                    ],
                ]
            );
        if (isset($data['composite_id'])) {
            $query->where(['Archives.id' => $data['composite_id']]);
        }
        if ($query->count() === 0) {
            $this->out(
                __(
                    "Impossible de trouver les archives pour le transfert id={0}",
                    $data['transfer_id']
                )
            );
            throw new CantWorkException();
        }
        $states = [ArchivesTable::S_DESCRIBING, ArchivesTable::S_UPDATE_DESCRIBING];
        foreach ($query as $archive) {
            if (!in_array($archive->get('state'), $states)) {
                $this->out(
                    __(
                        "Etat de l'archive: {0}. Annulation.",
                        $archive->get('state')
                    )
                );
                throw new CantWorkException();
            }
            $this->archives[$archive->id] = $archive;
            $secureDataSpaceId = $archive->get('secure_data_space_id');
            if (!isset($this->managers[$secureDataSpaceId])) {
                $this->managers[$secureDataSpaceId]
                    = new VolumeManager($secureDataSpaceId);
            }
        }
        $this->util = DOMUtility::load($transfer->get('xml'));

        if (isset($data['composite_id'])) {
            $this->handleComposites($data);
            $archive = $query->first();
            $Archives->transitionOrFail($archive, ArchivesTable::T_STORE);
            $Archives->saveOrFail($archive);
            $this->emit('archive-management', $data);
            $this->archives = [$archive];
            $this->updateDiskUsage();
            return;
        }

        $saveRefs = Configure::read('ArchiveUnitWorker.saveRefs', TMP)
            . $data['transfer_id'] . '_saverefs.serialize';
        if ($links = is_readable($saveRefs)) {
            $this->xpaths = unserialize(file_get_contents($saveRefs));
        }
        if (!$this->xpaths) {
            $query = $loc->get('ArchiveUnits')->find()
                ->innerJoinWith('Archives')
                ->innerJoinWith('Archives.Transfers')
                ->where(['Transfers.id' => $data['transfer_id']])
                ->orderBy(['ArchiveUnits.id']);
            $this->xpaths = [];
            foreach ($query as $archiveUnit) {
                $archiveUnit->set('context', 'ArchiveTransfer');
                $unit = $this->util->xpath->query($archiveUnit->get('xpath'))
                    ->item(0);
                // Note: ArchiveUnit->xpath != getElementXpath
                $this->xpaths[$archiveUnit->id] = $this->util->getElementXpath(
                    $unit
                );
            }
        }
        $TransferAttachments = $loc->get('TransferAttachments');
        $query = $TransferAttachments->selectQuery()
            ->where(
                [
                    'TransferAttachments.transfer_id' => $data['transfer_id'],
                ]
            );
        $count = $query->count();
        /** @var EntityInterface $attachment */
        foreach ($query as $i => $attachment) {
            if ($i % 100 === 0) {
                $this->out(sprintf('%d / %d', $i + 1, $count));
            }
            $node = ArchiveBinariesTable::findBinaryByFilename($this->util, $attachment->get('filename'));
            $archiveUnits = $this->findLinkedArchiveUnits($node);
            $this->createArchiveBinaries($attachment, $archiveUnits);
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateCountSize(
            $ArchiveUnits->find()
                ->where(
                    [
                        'ArchiveUnits.archive_id IN' => array_keys(
                            $this->archives
                        ),
                    ]
                )
        );
        $Archives->updateCountSize($this->archives);
        foreach ($this->archives as $archive) {
            $archive->set('transferred_size', $archive->get('original_size'));
            $archive->set('transferred_count', $archive->get('original_count'));
            $Archives->transitionOrFail($archive, ArchivesTable::T_STORE);
            $Archives->saveOrFail($archive);
        }
        $this->emit('archive-management', $data);
        if ($links) {
            unlink($saveRefs);
        }
        $this->updateDiskUsage();
    }

    /**
     * Gestion des archives composites (SEDA > 2.0)
     * @param array $data
     * @return void
     * @throws Exception
     */
    private function handleComposites(array $data)
    {
        $loc = TableRegistry::getTableLocator();
        $TransferAttachments = $loc->get('TransferAttachments');
        $query = $TransferAttachments->selectQuery()
            ->where(
                [
                    'TransferAttachments.transfer_id' => $data['transfer_id'],
                ]
            );
        $count = $query->count();
        /** @var EntityInterface $attachment */
        foreach ($query as $i => $attachment) {
            if ($i % 100 === 0) {
                $this->out(sprintf('%d / %d', $i + 1, $count));
            }
            $node = ArchiveBinariesTable::findBinaryByFilename($this->util, $attachment->get('filename'));
            $archiveUnits = $this->findLinkedComposedArchiveUnits($node, $data);
            $this->createArchiveBinaries($attachment, $archiveUnits);
        }
    }

    /**
     * Liste des ids archive_unit lié à un binary
     * @param DOMElement $archiveBinary
     * @param array      $data
     * @return EntityInterface[]
     * @throws Exception
     */
    private function findLinkedComposedArchiveUnits(DOMElement $archiveBinary, array $data): array
    {
        $CompositeArchiveUnits = TableRegistry::getTableLocator()->get('CompositeArchiveUnits');
        $archive_units = [];
        // gestion des DataObjectReferenceId
        $refId = $archiveBinary->getAttribute('id');
        $normalized = trim(preg_replace('/\s+/', ' ', $refId));
        $path = sprintf(
            '//ns:DataObjectReferenceId[normalize-space(.)=%s]/../..',
            $this->util->xpathQuote($normalized)
        );
        foreach ($this->util->xpath->query($path) as $archiveUnitNode) {
            $composite = $CompositeArchiveUnits->find()
                ->where(
                    [
                        'transfer_id' => $data['transfer_id'],
                        'transfer_xpath' => $this->util->getElementXpath($archiveUnitNode),
                    ]
                )
                ->first();
            if ($composite) {
                $archive_units[$composite->get('archive_unit_id')] = $composite->get('archive_unit_id');
            }
        }

        // gestion des DataObjectGroupReferenceId
        /** @var DOMElement $parentNode */
        $parentNode = $archiveBinary->parentNode;
        if ($parentNode->tagName === 'DataObjectGroup') {
            $refGroupId = $parentNode->getAttribute('id');
        } else {
            $n = $this->util->xpath->query(
                'ns:DataObjectGroupId',
                $archiveBinary
            )
                ->item(0);
            $refGroupId = $n?->nodeValue;
        }
        if ($refGroupId) {
            $normalized = trim(preg_replace('/\s+/', ' ', $refGroupId));
            $path = sprintf(
                '//ns:DataObjectGroupReferenceId[normalize-space(.)=%s]/../..',
                $this->util->xpathQuote($normalized)
            );
            foreach ($this->util->xpath->query($path) as $archiveUnitNode) {
                $composite = $CompositeArchiveUnits->find()
                    ->where(
                        [
                            'transfer_id' => $data['transfer_id'],
                            'transfer_xpath' => $this->util->getElementXpath($archiveUnitNode),
                        ]
                    )
                    ->first();
                if ($composite) {
                    $archive_units[$composite->get('archive_unit_id')] = $composite->get('archive_unit_id');
                }
            }
        }
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');

        return $ArchiveUnits->find()
            ->where(['id IN' => $archive_units])
            ->all()
            ->indexBy('id')
            ->toArray();
    }

    /**
     * Créer les archive_binaries lié à un attachment
     * @param EntityInterface   $attachment
     * @param EntityInterface[] $archiveUnits
     * @throws VolumeException
     * @throws FileNotReadableException
     */
    private function createArchiveBinaries(
        EntityInterface $attachment,
        array $archiveUnits
    ) {
        $archives = [];
        $archiveUnitsArchives = [];
        foreach ($archiveUnits as $archiveUnit) {
            if (!isset($this->archives[$archiveUnit->get('archive_id')])) {
                continue;
            }
            if (!isset($archives[$archiveUnit->get('archive_id')])) {
                $archives[$archiveUnit->get('archive_id')]
                    = $this->archives[$archiveUnit->get('archive_id')];
            }
            $archiveUnitsArchives[$archiveUnit->get(
                'archive_id'
            )][] = $archiveUnit;
        }
        $baseDir = AbstractGenericMessageForm::getDataDirectory(
            $attachment->get('transfer_id')
        );
        $tokenBasedir = $baseDir . DS . 'timestamp';
        $tokenPath = $tokenBasedir . DS . 'attachments'
            . DS . $attachment->get('filename') . '.tsr';

        foreach ($archives as $archive) {
            $binary = $this->createArchiveBinary(
                $attachment,
                $archive,
                $archiveUnitsArchives[$archive->id]
            );
            if (is_file($tokenPath)) {
                $this->createTimestampBinary(
                    $attachment,
                    $archive,
                    $archiveUnitsArchives[$archive->id],
                    $binary
                );
            }
        }
    }

    /**
     * Créé un archive_binary pour une archive
     * @param EntityInterface $attachment
     * @param EntityInterface $archive
     * @param array           $archiveUnits
     * @return EntityInterface
     * @throws VolumeException
     * @throws FileNotReadableException
     */
    private function createArchiveBinary(
        EntityInterface $attachment,
        EntityInterface $archive,
        array $archiveUnits
    ): EntityInterface {
        $loc = TableRegistry::getTableLocator();
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $archiveBinary = $ArchiveBinaries->find()
            ->innerJoinWith('ArchiveUnits')
            ->where(
                [
                    'ArchiveUnits.archive_id' => $archive->id,
                    'ArchiveBinaries.filename' => $attachment->get('filename'),
                    'ArchiveBinaries.type' => 'original_data',
                ]
            )
            ->first();
        if (!$archiveBinary) {
            $path = $archive->get('storage_path') . '/original_data/'
                . $attachment->get('filename');
            $this->deleteIfExists(
                $path,
                (int)$archive->get('secure_data_space_id')
            );
            $storedFile = $this->managers[$archive->get('secure_data_space_id')]
                ->fileUpload($attachment->get('path'), $path);
            $detector = new DetectFileFormat($attachment->get('path'));
            $archiveBinary = $ArchiveBinaries->newEntity(
                [
                    'is_integrity_ok' => null,
                    'type' => 'original_data',
                    'filename' => $attachment->get('filename'),
                    'format' => $attachment->get('format'),
                    'mime' => $attachment->get('mime')
                        ?: mime_content_type($attachment->get('path')),
                    'extension' => $attachment->get('extension'),
                    'stored_file_id' => $storedFile->get('id'),
                    'in_rgi' => $detector->rgiCompatible(),
                    'app_meta' => array_filter(
                        [
                            'category' => $detector->getCategory(),
                            'codecs' => $detector->getCodecs(),
                            'puid' => $detector->getPuid(),
                        ]
                    ),
                ]
            );
            $ArchiveBinaries->saveOrFail($archiveBinary);
        }
        $this->createLinkBetweenBinaryAndUnit($archiveBinary, $archiveUnits);
        $this->checkConversions(
            $attachment->get('path'),
            $archiveBinary,
            $this->archives[$archive->id]
        );
        return $archiveBinary;
    }

    /**
     * Supprime le fichier s'il existe déjà
     * @param string $storageReference
     * @param int    $secure_data_space_id
     * @throws VolumeException
     */
    private function deleteIfExists(
        string $storageReference,
        int $secure_data_space_id
    ) {
        $volumeManager = $this->managers[$secure_data_space_id];
        foreach ($volumeManager->getVolumes() as $volume_id => $driver) {
            if ($driver->fileExists($storageReference)) {
                $this->out(
                    __(
                        "Le fichier {0} existe déjà sur le volume {1} et a été supprimé",
                        $storageReference,
                        $volume_id
                    )
                );
                $driver->fileDelete($storageReference);
            }
        }
    }

    /**
     * Créé les liens entre binary et archive_units
     * @param EntityInterface   $archiveBinary
     * @param EntityInterface[] $archiveUnits
     */
    private function createLinkBetweenBinaryAndUnit(
        EntityInterface $archiveBinary,
        array $archiveUnits
    ) {
        $loc = TableRegistry::getTableLocator();
        $ArchiveBinariesArchiveUnits = $loc->get('ArchiveBinariesArchiveUnits');
        foreach ($archiveUnits as $archiveUnit) {
            $ArchiveBinariesArchiveUnits->findOrCreate(
                [
                    'archive_binary_id' => $archiveBinary->id,
                    'archive_unit_id' => $archiveUnit->id,
                ]
            );
        }
    }

    /**
     * Création des entrées dans archive_binary_conversions
     * @param string          $path
     * @param EntityInterface $archiveBinary
     * @param EntityInterface $archive
     * @throws FileNotReadableException
     */
    private function checkConversions(
        string $path,
        EntityInterface $archiveBinary,
        EntityInterface $archive
    ) {
        $serviceLevel = $archive->get('service_level');
        if (!$serviceLevel || FileConverters::canConvert($path) === false) {
            return;
        }
        $detector = new DetectFileFormat($path);
        $category = $detector->getCategory();
        $ArchiveBinaryConversions = TableRegistry::getTableLocator()->get(
            'ArchiveBinaryConversions'
        );

        // Gestion des preservation
        $preservations = $serviceLevel->get('convert_preservation');
        $conversions = $archive->get('conversions');

        if (
            $category
            && $preservations instanceof CommaArrayString
            && in_array($category, $preservations->getArrayCopy())
            && $detector->rgiCompatible() === false
        ) {
            $confkey = "conversion-preservation-$category";
            $entity = $ArchiveBinaryConversions->newEntity(
                [
                    'archive_id' => $archive->id,
                    'archive_binary_id' => $archiveBinary->id,
                    'type' => 'preservation',
                ]
            );
            if ($category === 'video') {
                $ArchiveBinaryConversions->patchEntity(
                    $entity,
                    [
                        'format' => $conversions["$confkey-container"],
                        'audio_codec' => $conversions["$confkey-codec-audio"],
                        'video_codec' => $conversions["$confkey-codec-video"],
                    ] + $entity->toArray()
                );
            } else {
                $entity->set('format', $conversions[$confkey]);
            }
            $ArchiveBinaryConversions->saveOrFail($entity);
        }

        // Gestion des dissemination
        $dissemination = $serviceLevel->get('convert_dissemination');
        if (
            $category
            && $dissemination instanceof CommaArrayString
            && in_array($category, $dissemination->getArrayCopy())
        ) {
            $confkey = "conversion-dissemination-$category";
            if ($category === 'audio') {
                $needConversion = !$detector->is(
                    $conversions[$confkey],
                    $detector::COMPARE_CONTAINER
                );
            } elseif ($category === 'video') {
                $sameContainer = $detector->is(
                    $conversions["$confkey-container"],
                    $detector::COMPARE_CONTAINER
                );
                $sameVideo = $detector->is(
                    $conversions["$confkey-codec-video"],
                    $detector::COMPARE_VIDEO_CODEC
                );
                $sameAudio = $detector->is(
                    $conversions["$confkey-codec-audio"],
                    $detector::COMPARE_AUDIO_CODEC
                );
                $needConversion = !$sameContainer || !$sameVideo || !$sameAudio;
            } else {
                $compare = preg_match(
                    '/^document_(.*)$/',
                    $conversions[$confkey],
                    $m
                )
                    ? $m[1]
                    : $conversions[$confkey];
                if ($compare === 'pdf') {
                    $compare = 'pdfa';
                }
                $needConversion = !$detector->is($compare);
            }
            if ($needConversion) {
                $entity = $ArchiveBinaryConversions->newEntity(
                    [
                        'archive_id' => $archive->id,
                        'archive_binary_id' => $archiveBinary->id,
                        'type' => 'dissemination',
                    ]
                );
                if ($category === 'video') {
                    $ArchiveBinaryConversions->patchEntity(
                        $entity,
                        [
                            'format' => $conversions["$confkey-container"],
                            'audio_codec' => $conversions["$confkey-codec-audio"],
                            'video_codec' => $conversions["$confkey-codec-video"],
                        ] + $entity->toArray()
                    );
                } else {
                    $entity->set('format', $conversions[$confkey]);
                }
                $ArchiveBinaryConversions->saveOrFail($entity);
            }
        }
    }

    /**
     * Créé un archive_binary d'horodatage
     * @param EntityInterface $attachment
     * @param EntityInterface $archive
     * @param array           $archiveUnits
     * @param EntityInterface $originalBinary
     * @return EntityInterface
     * @throws VolumeException
     */
    private function createTimestampBinary(
        EntityInterface $attachment,
        EntityInterface $archive,
        array $archiveUnits,
        EntityInterface $originalBinary
    ): EntityInterface {
        $loc = TableRegistry::getTableLocator();
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $archiveBinary = $ArchiveBinaries->find()
            ->innerJoinWith('ArchiveUnits')
            ->where(
                [
                    'ArchiveUnits.archive_id' => $archive->id,
                    'ArchiveBinaries.filename' => $attachment->get('filename')
                        . '.tsr',
                    'ArchiveBinaries.type' => 'original_timestamp',
                ]
            )
            ->first();
        if (!$archiveBinary) {
            $baseDir = AbstractGenericMessageForm::getDataDirectory(
                $attachment->get('transfer_id')
            );
            $tokenBasedir = $baseDir . DS . 'timestamp';
            $tokenPath = $tokenBasedir . DS . 'attachments'
                . DS . $attachment->get('filename') . '.tsr';
            $path = $archive->get('storage_path')
                . '/original_timestamp/' . $attachment->get(
                    'filename'
                ) . '.tsr';
            $this->deleteIfExists(
                $path,
                (int)$archive->get('secure_data_space_id')
            );
            $storedFile = $this->managers[$archive->get('secure_data_space_id')]
                ->fileUpload($tokenPath, $path);
            $archiveBinary = $ArchiveBinaries->newEntity(
                [
                    'is_integrity_ok' => null,
                    'type' => 'original_timestamp',
                    'filename' => $attachment->get('filename') . '.tsr',
                    'format' => null,
                    'mime' => 'application/octet-stream',
                    'extension' => 'tsr',
                    'stored_file_id' => $storedFile->get('id'),
                    'original_data_id' => $originalBinary->id,
                    'in_rgi' => false,
                    'app_meta' => [],
                ]
            );
            $ArchiveBinaries->saveOrFail($archiveBinary);
        }
        $this->createLinkBetweenBinaryAndUnit($archiveBinary, $archiveUnits);
        return $archiveBinary;
    }

    /**
     * Met à jour le compteur d'espace disque des volumes
     * @return void
     * @throws ZMQSocketException
     */
    private function updateDiskUsage()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var VolumesTable $Volumes */
        $Volumes = $loc->get('Volumes');
        $Archives = $loc->get('Archives');
        $updatedVolumes = [];
        foreach ($this->archives as $archive) {
            $q = $Volumes->find()->where(
                [
                    'secure_data_space_id' => $archive->get(
                        'secure_data_space_id'
                    ),
                ]
            );
            foreach ($q as $volume) {
                if (!isset($updatedVolumes[$volume->id])) {
                    $Volumes->updateDiskUsage($volume->id);
                    $updatedVolumes[$volume->id] = true;
                }
            }
            Utility::get('Notify')->emit(
                'archive_state_update',
                [
                    'id' => $archive->id,
                    'state' => $archive->get('state'),
                    'statetrad' => $archive->get('statetrad'),
                    'units_count' => $Archives->get($archive->id)->get('units_count'),
                ]
            );
        }
    }

    /**
     * Liste des ids archive_unit lié à un binary
     * @param DOMElement $archiveBinary
     * @return EntityInterface[]
     * @throws Exception
     */
    private function findLinkedArchiveUnits(DOMElement $archiveBinary): array
    {
        $archive_units = [];
        if (in_array($this->util->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            $firstArchiveUnit = $this->util->node(
                '/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata/ns:ArchiveUnit'
            );
            $first_archive_unit_id = array_search(
                $this->util->getElementXpath($firstArchiveUnit),
                $this->xpaths
            );
            // gestion des DataObjectReferenceId
            $refId = $archiveBinary->getAttribute('id');
            $normalized = trim(preg_replace('/\s+/', ' ', $refId));
            $path = sprintf(
                '//ns:DataObjectReferenceId[normalize-space(.)=%s]/../..',
                $this->util->xpathQuote($normalized)
            );
            foreach ($this->util->xpath->query($path) as $archiveUnitNode) {
                $archive_unit_id = array_search(
                    $this->util->getElementXpath($archiveUnitNode),
                    $this->xpaths
                );
                if ($archive_unit_id) {
                    $archive_units[$archive_unit_id] = $archive_unit_id;
                }
            }

            // gestion des DataObjectGroupReferenceId
            /** @var DOMElement $parentNode */
            $parentNode = $archiveBinary->parentNode;
            if ($parentNode->tagName === 'DataObjectGroup') {
                $refGroupId = $parentNode->getAttribute('id');
            } else {
                $n = $this->util->xpath->query(
                    'ns:DataObjectGroupId',
                    $archiveBinary
                )
                    ->item(0);
                $refGroupId = $n?->nodeValue;
            }
            if ($refGroupId) {
                $normalized = trim(preg_replace('/\s+/', ' ', $refGroupId));
                $path = sprintf(
                    '//ns:DataObjectGroupReferenceId[normalize-space(.)=%s]/../..',
                    $this->util->xpathQuote($normalized)
                );
                foreach ($this->util->xpath->query($path) as $archiveUnitNode) {
                    $archive_unit_id = array_search(
                        $this->util->getElementXpath($archiveUnitNode),
                        $this->xpaths
                    );
                    if ($archive_unit_id) {
                        $archive_units[$archive_unit_id] = $archive_unit_id;
                    }
                }
            }
            if (empty($archive_units)) {
                $refElem = $this->util->createElement('DataObjectReference');
                $refElem->appendChild(
                    $this->util->createElement('DataObjectId', $refId)
                );
                $firstArchiveUnit->appendChild($refElem);
                $archive_units[$first_archive_unit_id] = $first_archive_unit_id;
            }
        } else {
            $archive_unit_id = array_search(
                $this->util->getElementXpath($archiveBinary),
                $this->xpaths
            );
            if ($archive_unit_id) {
                $archive_units[$archive_unit_id] = $archive_unit_id;
            } else {
                throw new Exception(
                    $this->util->getElementXpath(
                        $archiveBinary
                    ) . ' not found in archive unit list'
                );
            }
        }
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');

        return $ArchiveUnits->find()
            ->where(['id IN' => $archive_units])
            ->all()
            ->indexBy('id')
            ->toArray();
    }
}
