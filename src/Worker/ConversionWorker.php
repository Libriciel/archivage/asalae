<?php

/**
 * Asalae\Worker\ConversionWorker
 */

namespace Asalae\Worker;

use Asalae\Exception\GenericException;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\VersionsTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Timestamping\TimestampingInterface;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Timestamper;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateTime;
use Exception;
use FileConverters\Exception\FileNotReadableException;
use FileConverters\Utility\FileConverters;
use FileValidator\Utility\FileValidator;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Converti les fichiers d'une archive
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConversionWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var VolumeManager
     */
    private $volumeManager;
    /**
     * @var EntityInterface
     */
    private $archive;
    /**
     * @var EntityInterface[]
     */
    private $volumes;

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        $this->out(
            __(
                "Conversions des fichiers de l'archive id={0}",
                $data['archive_id']
            )
        );
        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $this->archive = $Archives->find()
            ->where(['Archives.id' => $data['archive_id']])
            ->contain(
                [
                    'ServiceLevels' => ['TsConvs'],
                    'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                ]
            )
            ->firstOrFail();
        $this->volumeManager = new VolumeManager(
            $this->archive->get('secure_data_space_id')
        );
        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        $this->volumes = $Volumes->find()
            ->where(
                [
                    'secure_data_space_id' => $this->archive->get(
                        'secure_data_space_id'
                    ),
                ]
            )
            ->toArray();

        $ArchiveBinaryConversions = TableRegistry::getTableLocator()->get(
            'ArchiveBinaryConversions'
        );
        $subquery = $ArchiveBinaryConversions->find()
            ->select(['archive_binary_id'])
            ->where(['archive_id' => $data['archive_id']]);
        $count = $subquery->count();
        if ($count === 0) {
            $this->out(__("Pas de conversion pour cette archive"));
            return;
        }

        $ArchiveBinaries = TableRegistry::getTableLocator()->get(
            'ArchiveBinaries'
        );
        $archiveBinaryConversions = TableRegistry::getTableLocator()->get(
            'archiveBinaryConversions'
        );
        $query = $ArchiveBinaries->find()
            ->where(['ArchiveBinaries.id IN' => $subquery])
            ->orderBy(['ArchiveBinaries.filename']);
        $count = $query->count();

        $timespamper = Hash::get(
            $this->archive,
            'service_level.ts_conv.timestamper_id'
        );
        if ($timespamper) {
            $timespamper = Timestamper::getDriverById($timespamper);
        }
        $conn = $ArchiveBinaries->getConnection();

        $errors = [];
        $e = null;
        $i = 0;
        $prev = microtime(true);
        /** @var EntityInterface $archiveBinary */
        foreach ($query as $archiveBinary) {
            $archiveBinary = $ArchiveBinaries->find()
                ->where(['ArchiveBinaries.id' => $archiveBinary->id])
                ->contain(
                    ['StoredFiles', 'ArchiveBinaryConversions', 'ArchiveUnits']
                )
                ->firstOrFail();

            $i++;
            $ntime = microtime(true);

            // message chaques minutes
            if ($ntime - $prev > 60) {
                $prev = $ntime;
                $filename = Hash::get(
                    $archiveBinary,
                    'archive_binary.filename'
                );
                $this->out(__("Fichier {0}/{1} : {2}", $i, $count, $filename));
            }

            $tmpFile = $this->download($archiveBinary);

            /** @var EntityInterface $archiveBinaryConversion */
            foreach ($archiveBinary->get('archive_binary_conversions') as $archiveBinaryConversion) {
                try {
                    $conn->begin();
                    $convertedFile = $this->convert(
                        $tmpFile,
                        $archiveBinary,
                        $archiveBinaryConversion
                    );
                    $this->upload(
                        $convertedFile,
                        $archiveBinary,
                        $archiveBinaryConversion
                    );
                    $this->timestamp(
                        $convertedFile,
                        $archiveBinary,
                        $archiveBinaryConversion,
                        $timespamper
                    );
                    unlink($convertedFile);
                    $archiveBinaryConversions->delete($archiveBinaryConversion);
                    $conn->commit();
                } catch (Exception $e) {
                    if ($conn->inTransaction()) {
                        $conn->rollback();
                    }
                    $error = __(
                        "Echec de la conversion de {0} en {1} pour la {2} dans l'archive {3} ({4})",
                        $archiveBinary->get('filename'),
                        $archiveBinaryConversion->get('format'),
                        $archiveBinaryConversion->get('type'),
                        $this->archive->get('archival_agency_identifier'),
                        $this->archive->id
                    );
                    $errors[] = $error;
                    $this->out($error);
                }
            }
            Filesystem::remove(dirname($tmpFile));
        }

        $oldCounters = $this->archive->toArray();
        $this->out(__("Mise à jour des compteurs..."));
        $Archives->updateCountSize([$this->archive]);

        if ($e) {
            throw new GenericException(implode(" ; \n", $errors), 500, $e);
        }

        /** @var VersionsTable $Versions */
        $Versions = TableRegistry::getTableLocator()->get('Versions');
        /** @var EventLogsTable $EventLogs */
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $entry = $EventLogs->newEntry(
            'convert',
            'success',
            __(
                "Conversion des fichiers de l'archive {0} ({1})",
                $this->archive->get('archival_agency_identifier'),
                $this->archive->id
            ),
            $Versions->getAppVersion(),
            $this->archive
        );
        $entry->set('in_lifecycle', false);
        $EventLogs->saveOrFail($entry);
        /** @var EntityInterface $storedFile */
        $storedFile = Hash::get(
            $this->archive,
            'lifecycle_xml_archive_file.stored_file',
            []
        );
        $Archives->updateLifecycleXml($storedFile, $entry);
        $this->saveIndicators($this->archive, $oldCounters);
    }

    /**
     * Télécharge le fichier a convertir
     * @param EntityInterface $archiveBinary
     * @return string
     * @throws VolumeException
     */
    private function download(EntityInterface $archiveBinary): string
    {
        $tmpDir = Configure::read(
            'Conversions.tmpDir',
            TMP . 'conversions'
        ) . DS . $archiveBinary->id . DS;
        if (!is_dir($tmpDir)) {
            mkdir($tmpDir, 0774, true);
        }
        $filesize = $archiveBinary->get('size');
        $requiredSpace = $filesize * 2;
        $localFreeSpace = disk_free_space($tmpDir);

        // vérifi l'espace disque local (fichier téléchargé + fichier converti de même taille)
        if ($localFreeSpace < $requiredSpace) {
            throw new GenericException(
                __(
                    "Taille disque inssufisante pour réaliser la conversion:"
                    . " taille restante: {0} ; taille nécessaire : {1}",
                    Number::toReadableSize($localFreeSpace),
                    Number::toReadableSize($requiredSpace)
                )
            );
        }

        // vérifier espace disque volume
        foreach ($this->volumes as $volume) {
            // Note: espace requis théorique basé sur 2 fichiers convertis de même taille que le fichier d'origine
            if (($volume->get('disk_usage') + $requiredSpace) > $volume->get('max_disk_usage')) {
                throw new GenericException(
                    __(
                        "Taille du volume {0} inssufisante pour stocker le fichier converti:"
                        . " taille restante: {0} ; taille nécessaire : {1}",
                        sprintf(
                            '"%s" (id=%d)',
                            $volume->get('name'),
                            $volume->id
                        ),
                        Number::toReadableSize($volume->get('max_disk_usage') - $volume->get('disk_usage')),
                        Number::toReadableSize($requiredSpace)
                    )
                );
            }
        }

        $targetPath = $tmpDir . basename($archiveBinary->get('filename'));
        if (is_file($targetPath)) {
            unlink($targetPath);
        }
        $this->volumeManager->fileDownload(
            Hash::get($archiveBinary, 'stored_file.name'),
            $targetPath
        );
        return $targetPath;
    }

    /**
     * Effectue la conversion d'un fichier
     * @param string          $tmpFile
     * @param EntityInterface $archiveBinary
     * @param EntityInterface $archiveBinaryConversion
     * @return string
     * @throws FileNotReadableException
     */
    private function convert(
        string $tmpFile,
        EntityInterface $archiveBinary,
        EntityInterface $archiveBinaryConversion
    ): string {
        $ext = pathinfo($tmpFile, PATHINFO_EXTENSION);
        $type = $archiveBinaryConversion->get('format');
        $targetFile = preg_replace("/\.$ext$/", "_$ext.$type", $tmpFile);

        $cmd = FileConverters::convert(
            $tmpFile,
            $type,
            $targetFile,
            [
                'return_cmd' => true,
                'video' => $archiveBinaryConversion->get('video_codec'),
                'audio' => $archiveBinaryConversion->get('audio_codec'),
            ]
        );
        $Exec = Utility::get(Exec::class);
        $timeout = Configure::read('Conversion.timeout', 36000); // 10h
        if ($timeout) {
            $cmd = sprintf(
                "timeout %.1F %s",
                (float)$timeout,
                $cmd
            );
        }
        $outputStd = Configure::read('Conversion.output_std', true); // affiche le stdout et stderr
        if ($outputStd) {
            $Exec->proc($cmd);
        } else {
            $Exec->defaultStdout = LOGS . 'last_conversion.log';
            $Exec->command($cmd);
        }

        if (!is_file($targetFile)) {
            $this->out(
                __(
                    "Conversion en échec: `{0}`",
                    $cmd ?: __("Pas convertisseur trouvé pour ce fichier")
                )
            );
            throw new GenericException(
                __(
                    "Echec de la conversion de {0} en {1}",
                    $archiveBinary->get('filename'),
                    $type
                )
            );
        }

        return $targetFile;
    }

    /**
     * Upload du fichier téléchargé
     * @param string          $convertedFile
     * @param EntityInterface $archiveBinary
     * @param EntityInterface $archiveBinaryConversion
     * @throws VolumeException
     */
    private function upload(
        string $convertedFile,
        EntityInterface $archiveBinary,
        EntityInterface $archiveBinaryConversion
    ) {
        $dirname = dirname($archiveBinary->get('filename')) . DS;
        if ($dirname === './') {
            $dirname = '';
        }
        $type = $archiveBinaryConversion->get('type') . '_data';
        $dir = $this->archive->get(
            'storage_path'
        ) . '/' . $type . '/' . $dirname;
        $storedFile = $this->volumeManager->fileUpload(
            $convertedFile,
            $dir . basename($convertedFile)
        );
        $loc = TableRegistry::getTableLocator();
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $ArchiveBinariesArchiveUnits = $loc->get('ArchiveBinariesArchiveUnits');

        $binary = $ArchiveBinaries->newEntity(
            [
                'type' => $type,
                'filename' => $dirname . basename($convertedFile),
                'format' => Hash::get(
                    FileValidator::getPuid($convertedFile) ?: [],
                    '0.id'
                ),
                'mime' => mime_content_type($convertedFile),
                'extension' => pathinfo($convertedFile, PATHINFO_EXTENSION),
                'stored_file_id' => $storedFile->id,
                'original_data_id' => $archiveBinary->id,
            ]
        );
        $ArchiveBinaries->saveOrFail($binary);

        $link = $ArchiveBinariesArchiveUnits->newEntity(
            [
                'archive_binary_id' => $binary->id,
                'archive_unit_id' => Hash::get(
                    $archiveBinary,
                    'archive_units.0.id'
                ),
            ]
        );
        $ArchiveBinariesArchiveUnits->saveOrFail($link);
    }

    /**
     * Horodatage du fichier converti
     * @param string                     $convertedFile
     * @param EntityInterface            $archiveBinary
     * @param EntityInterface            $archiveBinaryConversion
     * @param TimestampingInterface|null $timespamper
     * @throws Exception
     */
    private function timestamp(
        string $convertedFile,
        EntityInterface $archiveBinary,
        EntityInterface $archiveBinaryConversion,
        ?TimestampingInterface $timespamper
    ) {
        if (!$timespamper) {
            return;
        }
        $dirname = dirname($archiveBinary->get('filename')) . DS;
        if ($dirname === './') {
            $dirname = '';
        }
        $loc = TableRegistry::getTableLocator();
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $ArchiveBinariesArchiveUnits = $loc->get('ArchiveBinariesArchiveUnits');
        $type = $archiveBinaryConversion->get('type') . '_timestamp';
        $tsr = $convertedFile . '.tsr';
        $tsrDir = $this->archive->get('storage_path') . '/' . $type . '/' . $dirname;
        $storedFile = $this->volumeManager->filePutContent(
            $tsrDir . basename($tsr),
            $timespamper->generateToken($convertedFile)
        );

        $binary = $ArchiveBinaries->newEntity(
            [
                'type' => $type,
                'filename' => $dirname . basename($tsr),
                'format' => null,
                'mime' => mime_content_type($convertedFile),
                'extension' => 'tsr',
                'stored_file_id' => $storedFile->id,
                'original_data_id' => $archiveBinary->id,
                'in_rgi' => false,
                'app_meta' => [],
            ]
        );
        $ArchiveBinaries->saveOrFail($binary);

        $link = $ArchiveBinariesArchiveUnits->newEntity(
            [
                'archive_binary_id' => $binary->id,
                'archive_unit_id' => Hash::get($archiveBinary, 'archive_units.0.id'),
            ]
        );
        $ArchiveBinariesArchiveUnits->saveOrFail($link);
    }

    /**
     * Ajoute les indicateurs de conversions
     * @param EntityInterface $archive
     * @param array           $oldCounters
     */
    private function saveIndicators(
        EntityInterface $archive,
        array $oldCounters
    ) {
        $loc = TableRegistry::getTableLocator();
        $ArchiveIndicators = $loc->get('ArchiveIndicators');
        $entity = $ArchiveIndicators->newEntity(
            [
                'archive_date' => new DateTime(),
                'archival_agency_id' => $archive->get('archival_agency_id'),
                'transferring_agency_id' => $archive->get('transferring_agency_id'),
                'originating_agency_id' => $archive->get('originating_agency_id'),
                'archive_count' => 0,
                'units_count' => 0,
                'original_count' => 0,
                'original_size' => 0,
                'timestamp_count' => $archive->get('timestamp_count') - $oldCounters['timestamp_count'],
                'timestamp_size' => $archive->get('timestamp_size') - $oldCounters['timestamp_size'],
                'preservation_count' => $archive->get('preservation_count') - $oldCounters['preservation_count'],
                'preservation_size' => $archive->get('preservation_size') - $oldCounters['preservation_size'],
                'dissemination_count' => $archive->get('dissemination_count') - $oldCounters['dissemination_count'],
                'dissemination_size' => $archive->get('dissemination_size') - $oldCounters['dissemination_size'],
                'agreement_id' => $archive->get('agreement_id'),
                'profile_id' => $archive->get('profile_id'),
            ]
        );
        $ArchiveIndicators->saveOrFail($entity);
    }
}
