<?php

/**
 * Asalae\Worker\RestitutionBuildWorker
 */

namespace Asalae\Worker;

use Asalae\Exception\GenericException;
use Asalae\Model\Entity\Restitution;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsRestitutionRequestsTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Table\RestitutionsTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Number;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Datacompressor\Utility\DataCompressor;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Throwable;

/**
 * Restitution d'archives
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RestitutionBuildWorker extends AbstractAsalaeWorkerV4
{
    public const int TOUCH_DELAY = 30;

    /**
     * @var ArchiveUnitsRestitutionRequestsTable
     */
    private $ArchiveUnitsRestitutionRequests;
    /**
     * @var ArchivesTable
     */
    private $Archives;
    /**
     * @var ArchiveBinariesTable
     */
    private $ArchiveBinaries;
    /**
     * @var CountersTable
     */
    private $Counters;
    /**
     * @var EventLogsTable
     */
    private $EventLogs;
    /**
     * @var RestitutionRequestsTable
     */
    private $RestitutionRequests;
    /**
     * @var RestitutionsTable
     */
    private $Restitutions;
    /**
     * @var VolumeManager[]
     */
    private $volumeManagers;
    /**
     * @var int
     */
    private $auActual = 0;
    /**
     * @var int
     */
    private $auCount = 0;
    /**
     * @var int
     */
    private $fileActual = 0;
    /**
     * @var int
     */
    private $fileCount = 0;
    /**
     * @var float
     */
    private $touchTime;

    /**
     * WorkerInterface constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $loc = TableRegistry::getTableLocator();
        $this->ArchiveBinaries = $loc->get('ArchiveBinaries');
        $this->ArchiveUnitsRestitutionRequests = $loc->get(
            'ArchiveUnitsRestitutionRequests'
        );
        $this->Archives = $loc->get('Archives');
        $this->EventLogs = $loc->get('EventLogs');
        $this->Counters = $loc->get('Counters');
        $this->RestitutionRequests = $loc->get('RestitutionRequests');
        $this->Restitutions = $loc->get('Restitutions');
    }

    /**
     * Effectue le travail voulu
     * @param mixed $data
     * @throws Throwable
     */
    public function work($data)
    {
        if (
            $this->Restitutions->exists(
                ['restitution_request_id' => $data['restitution_request_id']]
            )
        ) {
            $this->out(__("Une restitution existe déjà pour cette demande"));
            throw new CantWorkException();
        }
        $this->volumeManagers = [];
        $restitutionRequest = $this->RestitutionRequests->find()
            ->where(
                [
                    'RestitutionRequests.id' => $data['restitution_request_id'],
                    'RestitutionRequests.state' => RestitutionRequestsTable::S_ACCEPTED,
                ]
            )
            ->contain(
                [
                    'ArchivalAgencies',
                    'ArchiveUnits' => function (Query $q) {
                        return $q->orderBy(
                            ['ArchiveUnits.archival_agency_identifier']
                        )->limit(100);
                    },
                    'CreatedUsers' => [
                        'OrgEntities',
                    ],
                    'OriginatingAgencies',
                ]
            )
            ->firstOrFail();
        $this->out(
            __("Demande de restitution id={0}", $data['restitution_request_id'])
        );

        $query = $this->RestitutionRequests->find();
        $counts = $query
            ->select(
                [
                    'archive_units_count' => $query->func()->count('*'),
                    'original_count' => $query->func()->sum(
                        'ArchiveUnits.original_total_count'
                    ),
                    'original_size' => $query->func()->sum(
                        'ArchiveUnits.original_total_size'
                    ),
                ]
            )
            ->innerJoinWith('ArchiveUnits')
            ->where(['RestitutionRequests.id' => $data['restitution_request_id']])
            ->disableHydration()
            ->all()
            ->first();
        foreach ($counts as $key => $value) {
            $counts[$key] = (int)$value;
        }
        $freeSpace = disk_free_space(Configure::read('App.paths.data'));
        if (($counts['original_size'] * 2) > $freeSpace) {
            $this->out(
                __(
                    "Espace disque disponible/nécessaire: {0} / {1}",
                    Number::toReadableSize($freeSpace),
                    Number::toReadableSize($counts['original_size'] * 2)
                )
            );
            throw new GenericException(
                __(
                    "Espace disque disponible insuffisant pour réaliser cette restitution"
                )
            );
        }
        if ($counts['archive_units_count'] === 0) {
            throw new GenericException(
                __("Demande de restitution sans unités d'archives")
            );
        }

        $this->out(
            __(
                "Unités d'archives: {0}; Fichiers: {1}; Taille des fichiers: {2}",
                $counts['archive_units_count'],
                $counts['original_count'],
                Number::toReadableSize($counts['original_size'])
            )
        );
        $this->touchTime = microtime(true) + self::TOUCH_DELAY;
        $this->auActual = 0;
        $this->auCount = (int)$counts['archive_units_count'];
        $this->fileActual = 0;
        $this->fileCount = (int)$counts['original_count'];

        $conn = $this->Restitutions->getConnection();
        $conn->begin();
        try {
            $links = [];
            /** @var EntityInterface $archiveUnit */
            foreach (
                $restitutionRequest->get(
                    'archive_units'
                ) as $archiveUnit
            ) {
                $links[] = '- ' . $archiveUnit->get(
                    'archival_agency_identifier'
                ) . ' - ' . $archiveUnit->get('name');
            }
            /** @var Restitution $restitution */
            $restitution = $this->Restitutions->newEntity(
                [
                    'identifier' => $this->Counters->next(
                        $restitutionRequest->get('archival_agency_id'),
                        'ArchiveRestitution'
                    ),
                    'restitution_request_id' => $data['restitution_request_id'],
                    'state' => $this->Restitutions->initialState,
                    'comment' => __n(
                        "Restitution de l'unité d'archives demandée par {0} :\n{1}",
                        "Restitution des unités d'archives demandées par {0} :\n{1}",
                        $this->auCount,
                        Hash::get($restitutionRequest, 'created_user.name'),
                        implode("\n", $links)
                    ),
                ] + $counts
            );
            $restitution->set('restitution_request', $restitutionRequest);
            $restitution->set(
                'archival_agency',
                $restitutionRequest->get('archival_agency')
            );
            $restitution->set(
                'originating_agency',
                $restitutionRequest->get('originating_agency')
            );
            $this->Restitutions->saveOrFail($restitution);

            $this->downloadFiles($restitution);
            $this->createXml($restitution);
            $this->zipFiles($restitution);
            $this->deleteFiles($restitution);

            $this->RestitutionRequests->transitionOrFail(
                $restitutionRequest,
                RestitutionRequestsTable::T_BUILD_RESTITUTION
            );
            $this->RestitutionRequests->saveOrFail($restitutionRequest);
            $this->Restitutions->transitionOrFail(
                $restitution,
                RestitutionsTable::T_BUILD
            );
            $this->Restitutions->saveOrFail($restitution);

            // mise à jour du cycle de vie
            $links = $this->ArchiveUnitsRestitutionRequests->find()
                ->innerJoinWith('ArchiveUnits')
                ->innerJoinWith('ArchiveUnits.Archives')
                ->where(['restitution_request_id' => $restitutionRequest->id])
                ->distinct(['ArchiveUnits.archive_id'])
                ->contain(
                    [
                        'ArchiveUnits' => [
                            'Archives' => [
                                'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                            ],
                        ],
                    ]
                );
            foreach ($links as $link) {
                $unit = $link->get('archive_unit');
                $entry = $this->EventLogs->newEntry(
                    'restitution_build',
                    'success',
                    __(
                        "Préparation des fichiers de restitution de " .
                        "l'archive {0} suite à la demande de restitution {1}",
                        $unit->get('archival_agency_identifier'),
                        $restitutionRequest->get('identifier')
                    ),
                    $restitutionRequest->get('created_user_id'),
                    $unit
                );
                $entry->set('in_lifecycle', false);
                $this->EventLogs->saveOrFail($entry);
                /** @var EntityInterface $archive */
                $storedFile = Hash::get(
                    $unit,
                    'archive.lifecycle_xml_archive_file.stored_file',
                    []
                );
                $this->Archives->updateLifecycleXml($storedFile, $entry, true);
            }

            $conn->commit();
        } catch (Throwable $e) {
            $conn->rollback();
            if (isset($restitution) && $restitution->id) {
                $this->deleteFiles($restitution, true);
            }
            throw $e;
        } finally {
            if (
                isset($restitution) && $restitution->id && is_dir(
                    $restitution->get('basepath')
                )
            ) {
                Filesystem::removeEmptySubFolders(
                    $restitution->get('basepath')
                );
            }
        }
    }

    /**
     * Télécharge les fichiers liés a une restitution
     * @param Restitution $restitution
     * @throws VolumeException
     */
    private function downloadFiles(Restitution $restitution)
    {
        $links = $this->ArchiveUnitsRestitutionRequests->find()
            ->innerJoinWith('ArchiveUnits')
            ->where(
                [
                    'restitution_request_id' => Hash::get(
                        $restitution,
                        'restitution_request_id'
                    ),
                ]
            )
            ->contain(
                [
                    'ArchiveUnits' => [
                        'Archives' => [
                            'ArchiveFiles' => [
                                'StoredFiles',
                            ],
                        ],
                    ],
                ]
            );
        $basePath = $restitution->get('basepath');
        /** @var EntityInterface $archiveUnit */
        foreach ($links as $link) {
            $archiveUnit = $link->get('archive_unit');
            $this->auActual++;
            $this->touchMsg();
            $volumeManager = $this->getVolumeManager(
                Hash::get($archiveUnit, 'archive.secure_data_space_id')
            );
            $path = $basePath . DS . $archiveUnit->get(
                'archival_agency_identifier'
            ) . DS;
            $binaries = $this->ArchiveBinaries->find()
                ->innerJoinWith('ArchiveUnits')
                ->where(
                    [
                        'ArchiveUnits.lft >=' => $archiveUnit->get('lft'),
                        'ArchiveUnits.rght <=' => $archiveUnit->get('rght'),
                        'ArchiveBinaries.type IN' => [
                            'original_data',
                            'original_timestamp',
                        ],
                    ]
                )
                ->contain(['StoredFiles']);
            foreach ($binaries as $binary) {
                $this->fileActual++;
                $this->touchMsg();
                $subdir = $binary->get('type') === 'original_data'
                    ? 'original_data'
                    : 'original_timestamp';
                $dir = $path . $subdir . DS . $binary->get('filename');
                $volumeManager->fileDownload(
                    Hash::get($binary, 'stored_file.name'),
                    $dir
                );
            }
            /** @var EntityInterface $archiveFile */
            foreach (
                Hash::get(
                    $archiveUnit,
                    'archive.archive_files'
                ) as $archiveFile
            ) {
                $subdir = 'management_data';
                $dir = $path . $subdir . DS . $archiveFile->get('filename');
                $volumeManager->fileDownload(
                    Hash::get($archiveFile, 'stored_file.name'),
                    $dir
                );
            }
        }
    }

    /**
     * Message de feedback
     */
    private function touchMsg()
    {
        $time = microtime(true);
        if ($time > $this->touchTime) {
            $this->touchTime = $time + self::TOUCH_DELAY;
            $this->out(
                __(
                    "unités d'archives: {0} / {1}; fichiers: {2} / {3}",
                    $this->auActual,
                    $this->auCount,
                    $this->fileActual,
                    $this->fileCount
                )
            );
        }
    }

    /**
     * Donne le VolumeManager pour un secure_data_space donné
     * @param int $secure_data_space_id
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getVolumeManager($secure_data_space_id)
    {
        if (!isset($this->volumeManagers[$secure_data_space_id])) {
            $this->volumeManagers[$secure_data_space_id]
                = new VolumeManager($secure_data_space_id);
        }
        return $this->volumeManagers[$secure_data_space_id];
    }

    /**
     * Création du ArchiveRestitutionRequestReply
     * @param Restitution $restitution
     * @throws Exception
     */
    private function createXml(Restitution $restitution)
    {
        $path = $restitution->get('basepath') . DS . $restitution->get(
            'identifier'
        ) . '_restitution.xml';
        file_put_contents($path, $restitution->generateXml());
    }

    /**
     * Effectue la compression des fichiers d'une restitution
     * @param Restitution $restitution
     * @throws Exception
     */
    private function zipFiles(Restitution $restitution)
    {
        $basePath = $restitution->get('basepath');
        $zipPath = $basePath . DS . $restitution->get(
            'identifier'
        ) . '_restitution.zip';
        DataCompressor::compress($restitution->get('basepath'), $zipPath);
    }

    /**
     * Supprime les fichiers précédement téléchargés pour la restitution
     * @param Restitution $restitution
     * @param bool        $deleteZip
     */
    private function deleteFiles(
        Restitution $restitution,
        bool $deleteZip = false
    ) {
        $basePath = $restitution->get('basepath');
        if (!$basePath) {
            throw new GenericException(
                'no basepath'
            ); // protection pour éviter de supprimer la racine...
        }
        $zipPath = $basePath . DS . $restitution->get(
            'identifier'
        ) . '_restitution.zip';
        foreach (Filesystem::listFiles($basePath) as $filename) {
            if ($deleteZip || $filename !== $zipPath) {
                unlink($filename);
            }
        }
    }
}
