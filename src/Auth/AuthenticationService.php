<?php

/**
 * Asalae\Auth\AuthenticationService
 */

namespace Asalae\Auth;

use ArrayAccess;
use Asalae\Model\Table\EventLogsTable;
use AsalaeCore\Auth\AuthenticationService as CoreAuthenticationService;
use Cake\Core\Configure;
use Cake\Http\Session;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Authentication Service
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property Table $Versions
 */
class AuthenticationService extends CoreAuthenticationService
{
    /**
     * Sets identity data and persists it in the authenticators that support it.
     *
     * @param ServerRequestInterface $request  The request.
     * @param ResponseInterface      $response The response.
     * @param ArrayAccess|array      $identity Identity data.
     * @return array
     * @throws Exception
     */
    public function persistIdentity(ServerRequestInterface $request, ResponseInterface $response, $identity): array
    {
        $id = Hash::get($identity, 'id');
        if (Hash::get($identity, 'auth_basic')) {
            return parent::persistIdentity($request, $response, $identity);
        }
        if ($id && Hash::get($identity, 'agent_type') === 'person') {
            $sessionKey = $this->getConfig('sessionKey');
            /** @var Session $session */
            $session = $request->getAttribute('session');
            if (
                $session
                && !$session->read($sessionKey . '.registered')
            ) {
                $app = Configure::read('App.name');
                $connectionMessage = __(
                    "L'utilisateur ''{0}''",
                    h(Hash::get($identity, 'username'))
                );
                $loc = TableRegistry::getTableLocator();
                /** @var EventLogsTable $EventLogs */
                $EventLogs = $loc->get('EventLogs');
                $version = $loc->get('Versions')->find()
                    ->where(['subject' => $app])
                    ->orderBy(['id' => 'desc'])
                    ->first();
                if ($version) {
                    $entry = $EventLogs->newEntry(
                        'login',
                        'success',
                        __("Connexion de {0} à {1}", $connectionMessage, $app),
                        $identity,
                        $version
                    );
                    $EventLogs->save($entry);
                }
                $session->write($sessionKey . '.registered', true);
            }
        }
        return parent::persistIdentity($request, $response, $identity);
    }
}
