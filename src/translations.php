<?php

/**
 * Importé dans webroot/js/asalae.translation.js.php
 * Permet un bon fonctionnement du I18nShell::extract
 */

return [
    // global-top
    //      Filters
    'Retirer le filtre de recherche'
    => __d('js', "Retirer le filtre de recherche"),
    'Supprimer les filtres'
    => __d('js', "Supprimer les filtres"),
    'Rechercher'
    => __d('js', "Rechercher"),
    'Cette action supprimera la sauvegarde de façon définitive. Voulez-vous continuer ?'
    => __d(
        'js',
        "Cette action supprimera la sauvegarde de façon définitive. Voulez-vous continuer ?"
    ),
    'Impossible de sauvegarder en cas de formulaire vide ou en erreur!'
    => __d(
        'js',
        "Impossible de sauvegarder en cas de formulaire vide ou en erreur!"
    ),
    'Cette action va modifier la sauvegarde de façon définitive. Voulez-vous continuer ?'
    => __d(
        'js',
        "Cette action va modifier la sauvegarde de façon définitive. Voulez-vous continuer ?"
    ),
    'Nouvelle sauvegarde'
    => __d('js', "Nouvelle sauvegarde"),
    'Filtrer'
    => __d('js', "Filtrer"),
    'Réinitialiser les filtres'
    => __d('js', "Réinitialiser les filtres"),
    'Déconnecté'
    => __d('js', "Déconnecté"),
    "Erreur {0}: {1}" => __d('js', "Erreur {0}: ligne: {1}"),
    "message: {0}" => __d('js', "message: {0}"),
    "Impossible d'enregistrer, aucun filtre n'a été sélectionné."
    => __d('js', "Impossible d'enregistrer, aucun filtre n'a été sélectionné."),
    // Notifications
    "à l'instant" => __d('js', "à l'instant"),
    "depuis {0} minute(s)" => __d('js', "depuis {0} minute(s)"),
    "depuis {0} heure(s)" => __d('js', "depuis {0} heure(s)"),
    "depuis {0} jour(s)" => __d('js', "depuis {0} jour(s)"),

    // asalae.modal
    "Une erreur inattendue a eu lieu"
    => __d('js', "Une erreur inattendue a eu lieu"),
    "Vous devez être connecté pour accéder à ce contenu"
    => __d('js', "Vous devez être connecté pour accéder à ce contenu"),
    "Vous n'avez pas l'autorisation d'accéder à ce contenu"
    => __d('js', "Vous n'avez pas l'autorisation d'accéder à ce contenu"),
    "Une erreur a eu lieu lors de la suppression"
    => __d('js', "Une erreur a eu lieu lors de la suppression"),

    // asalae.upload.js
    "Validation..." => __d('js', "Validation..."),
    "Mettre l'upload en pause" => __d('js', "Mettre l'upload en pause"),
    "Reprise de l'upload" => __d('js', "Reprise de l'upload"),
    "Annuler l'upload" => __d('js', "Annuler l'upload"),
    "Relancer l'upload"
    => __d('js', "Relancer l'upload"),
    "Nouvelle tentative dans {0} secondes"
    => __d('js', "Nouvelle tentative dans {0} secondes"),
    "Validation du fichier en cours, veuillez patienter"
    => __d('js', "Validation du fichier en cours, veuillez patienter"),
    "Finalisation..."
    => __d('js', "Finalisation..."),
    "Votre navigateur ne supporte pas la fragmentation de fichiers, merci de le mettre à jour."
    => __d(
        'js',
        "Votre navigateur ne supporte pas la fragmentation de fichiers, merci de le mettre à jour."
    ),
    "Ou contactez un administrateur."
    => __d('js', "Ou contactez un administrateur."),
    "Fichier"
    => __d('js', "Fichier"),
    "Êtes-vous sûr de vouloir supprimer ce fichier ?"
    => __d('js', "Êtes-vous sûr de vouloir supprimer ce fichier ?"),
    "Êtes-vous sûr de vouloir supprimer les fichiers sélectionnés ?"
    => __d(
        'js',
        "Êtes-vous sûr de vouloir supprimer les fichiers sélectionnés ?"
    ),

    // asalae.paginator.js
    "Affichage des résultats de {0} à {1} sur les<br>{2} résultats"
    => __d(
        'js',
        "Affichage des résultats de {0} à {1} sur les<br>{2} résultats"
    ),
    "Précédent"
    => __d('js', "Précédent"),
    "Suivant"
    => __d('js', "Suivant"),
    "Page: {0}"
    => __d('js', "Page: {0}"),
    "Retirer le tri et les filtres"
    => __d('js', "Retirer le tri et les filtres"),

    // asalae.libersign
    "L'extension de navigateur LiberSign est requise pour vous permettre d'effectuer une signature numérique."
    => __d(
        'js',
        "L'extension de navigateur LiberSign est requise 
        pour vous permettre d'effectuer une signature numérique."
    ),
    "Aucun certificat n'a été trouvé"
    => __d('js', "Aucun certificat n'a été trouvé"),
    "Erreur lors de la tentative de récupération de vos certificats"
    => __d(
        'js',
        "Erreur lors de la tentative de récupération de vos certificats"
    ),
    "Identifiant"
    => __d('js', "Identifiant"),
    "Nom"
    => __d('js', "Nom"),
    "E-mail"
    => __d('js', "E-mail"),
    "Émetteur"
    => __d('js', "Émetteur"),
    "Sélectionner {0}"
    => __d('js', "Sélectionner {0}"),
    "Date d'expiration"
    => __d('js', "Date d'expiration"),
    "Signer"
    => __d('js', "Signer"),
    "Une erreur a eu lieu lors de la signature"
    => __d('js', "Une erreur a eu lieu lors de la signature"),

// extented_jquery
    "Conflit d'id (DOM) détecté"
    => __d('js', "Conflit d'id (DOM) détecté"),

// src
    'Vous avez apporté des modifications au formulaire. Souhaitez-vous les sauvegarder ?'
    => __d(
        'js',
        "Vous avez apporté des modifications au formulaire. Souhaitez-vous les sauvegarder ?"
    ),
    "Utilisateur : {0}, validation par {1}"
    => __d('js', "Utilisateur : {0}, validation par {1}"),
    "Êtes-vous sûr de vouloir supprimer cet enregistrement ?"
    => __d('js', "Êtes-vous sûr de vouloir supprimer cet enregistrement ?"),
    "Veuillez sélectionner un fichier"
    => __d('js', "Veuillez sélectionner un fichier"),
    "Veuillez saisir le nombre de chiffres du numéro de la séquence (entre 1 et 10)"
    => __d(
        'js',
        "Veuillez saisir le nombre de chiffres du numéro de la séquence (entre 1 et 10)"
    ),
    "Vous n'avez pas saisi une valeur correcte"
    => __d('js', "Vous n'avez pas saisi une valeur correcte"),
    'Êtes-vous sûr de vouloir supprimer les mots-clés sélectionnés ?'
    => __d(
        'js',
        "Êtes-vous sûr de vouloir supprimer les mots-clés sélectionnés ?"
    ),
    'Import de {0} mots-clés effectué avec succès'
    => __d('js', "Import de {0} mots-clés effectué avec succès"),
    "Une erreur a eu lieu lors de l'import"
    => __d('js', "Une erreur a eu lieu lors de l'import"),
    "Voulez-vous vraiment supprimer ce service d'horodatage ?"
    => __d('js', "Voulez-vous vraiment supprimer ce service d'horodatage ?"),
    "Cette action peut avoir pour conséquence de consommer un jeton, ce qui peut 
    entraîner un coût supplémentaire. Voulez-vous continuer ?"
    => __d(
        'js',
        "Cette action peut avoir pour conséquence de consommer un jeton, ce qui peut 
    entraîner un coût supplémentaire. Voulez-vous continuer ?"
    ),
    "OK - Le service d'horodatage a été vérifié avec succès"
    => __d('js', "OK - Le service d'horodatage a été vérifié avec succès"),
    "Une erreur a eu lieu: {0}"
    => __d('js', "Une erreur a eu lieu: {0}"),
    "Services d'horodatage disponibles"
    => __d('js', "Services d'horodatage disponibles"),
    "OK - Le volume a été vérifié avec succès"
    => __d('js', "OK - Le volume a été vérifié avec succès"),
    "Volumes utilisées"
    => __d('js', "Volumes utilisées"),
    "Le verrouillage est géré automatiquement, et ne doit être modifié qu'en cas de plantage.
    Voulez-vous vraiment modifier cette valeur ?"
    => __d(
        'js',
        "Le verrouillage est géré automatiquement, et ne doit être modifié qu'en cas de plantage.
        Voulez-vous vraiment modifier cette valeur ?"
    ),
    "Cette action va forcer le lancement de la tâche planifiée, voulez-vous continuer ?"
    => __d(
        'js',
        "Cette action va forcer le lancement de la tâche planifiée, voulez-vous continuer ?"
    ),
    "Services versants disponibles"
    => __d('js', "Services versants disponibles"),
    "Services producteurs disponibles"
    => __d('js', "Services producteurs disponibles"),
    "niveaux de service disponibles"
    => __d('js', "niveaux de service disponibles"),
    "Ajouter"
    => __d('js', "Ajouter"),
    "Supprimer"
    => __d('js', "Supprimer"),
    "Vous êtes sur le point de supprimer un nœud et tout ce qu'il contient.
    Cette action ne pourra pas être annulée. Voulez-vous continuer ?"
    => __d(
        'js',
        "Vous êtes sur le point de supprimer un nœud et tout ce qu'il contient. 
        Cette action ne pourra pas être annulée. Voulez-vous continuer ?"
    ),
    "Ajouter un ensemble de mots-clés"
    => __d('js', "Ajouter un ensemble de mots-clés"),
    "Déplacer tout en haut"
    => __d('js', "Déplacer tout en haut"),
    "Déplacer vers le haut"
    => __d('js', "Déplacer vers le haut"),
    "Déplacer vers le bas"
    => __d('js', "Déplacer vers le bas"),
    "Déplacer tout en bas"
    => __d('js', "Déplacer tout en bas"),
    "Déplacer"
    => __d('js', "Déplacer"),
    "N'est pas une uri relative correcte"
    => __d('js', "N'est pas une uri relative correcte"),
    "Êtes-vous sûr de vouloir ajouter ces fichiers ?"
    => __d('js', "Êtes-vous sûr de vouloir ajouter ces fichiers ?"),
    "Êtes-vous sûr de vouloir supprimer ces enregistrements ?"
    => __d('js', "Êtes-vous sûr de vouloir supprimer ces enregistrements ?"),
    "Profils d'archives disponibles"
    => __d('js', "Profils d'archives disponibles"),
    "Êtes-vous sûr de vouloir supprimer les transferts sélectionnés ?"
    => __d(
        'js',
        "Êtes-vous sûr de vouloir supprimer les transferts sélectionnés ?"
    ),
    "Êtes-vous sûr de vouloir envoyer les transferts sélectionnés ?"
    => __d(
        'js',
        "Êtes-vous sûr de vouloir envoyer les transferts sélectionnés ?"
    ),
    "Un fichier est lié à ce document, souhaitez-vous le supprimer ?"
    => __d(
        'js',
        "Un fichier est lié à ce document, souhaitez-vous le supprimer ?"
    ),
    "en cours"
    => __d('js', "en cours"),
    "acceptés"
    => __d('js', "acceptés"),
    "refusés"
    => __d('js', "refusés"),
    "Terminé"
    => __d('js', "Terminé"),
    "En erreur"
    => __d('js', "En erreur"),
    "En cours"
    => __d('js', "En cours"),
    "{0} ({1} par {2})"
    => __d('js', "{0} ({1} par {2})"),
    "Type"
    => __d('js', "Type"),
    "Acteurs"
    => __d('js', "Acteurs"),
    "Ldaps disponibles"
    => __d('js', "Ldaps disponibles"),
    "Résultats de la recherche (cliquer ici pour revenir)"
    => __d('js', "Résultats de la recherche (cliquer ici pour revenir)"),
    "ce fichier sera supprimé lors de l'enregistrement"
    => __d('js', "ce fichier sera supprimé lors de l'enregistrement"),
    "Un cron a été ajouté (id={0}), vous pouvez le visualiser dans la vignette Tâches planifiées."
    => __d(
        'js',
        "Un cron a été ajouté (id={0}), vous pouvez le visualiser dans la vignette Tâches planifiées."
    ),
    "Email: {0}"
    => __d('js', "Email: {0}"),
    "Réparer un transfert"
    => __d('js', "Réparer un transfert"),
    "Créer un transfert à partir des modifications"
    => __d('js', "Créer un transfert à partir des modifications"),
    "Télécharger le transfert"
    => __d('js', "Télécharger le transfert"),
    "Précontrôle"
    => __d('js', "Précontrôle"),
    "Annuler"
    => __d('js', "Annuler"),
    "Enregistrer"
    => __d('js', "Enregistrer"),
    "Ce transfert est prêt à être importé"
    => __d('js', "Ce transfert est prêt à être importé"),
    "{0} messages ont été envoyés"
    => __d('js', "{0} messages ont été envoyés"),
    "1 message a été envoyé"
    => __d('js', "1 message a été envoyé"),
    "aucun message n'a été envoyé"
    => __d('js', "aucun message n'a été envoyé"),
    "Message envoyé avec succès"
    => __d('js', "Message envoyé avec succès"),
    "La force de votre mot de passe ({0}) est inférieure à la force minimum configurée ({1}).\\n
    L'ANSSI recommande un mot de passe de plus de 12 caractères (chiffres, lettres, majuscules et spéciaux)'"
    => __d(
        'js',
        "La force de votre mot de passe ({0}) est inférieure à la force minimum configurée ({1}).\\n
        L'ANSSI recommande un mot de passe de plus de 12 caractères (chiffres, lettres, majuscules et spéciaux)"
    ),
    "Veuillez entrer le mail de destination"
    => __d('js', "Veuillez entrer le mail de destination"),
    "Annuler les modifications sur {0}"
    => __d('js', "Annuler les modifications sur {0}"),
    "Enregistrer les modifications sur {0}"
    => __d('js', "Enregistrer les modifications sur {0}"),
    "Générer un nouvel identifiant"
    => __d('js', "Générer un nouvel identifiant"),
    "Nouvel identifiant: {0}"
    => __d('js', "Nouvel identifiant: {0}"),
    "Attention, vous reprenez l'édition de ce transfert qui avait commencé le {0}"
    => __d(
        'js',
        "Attention, vous reprenez l'édition de ce transfert qui avait commencé le {0}"
    ),
    "L'édition de ce transfert ne s'est pas correctement terminée. Voulez-vous reprendre l'édition?
    Attention si vous cliquez sur Annuler, les éventuelles modifications seront perdues."
    => __d(
        'js',
        "L'édition de ce transfert ne s'est pas correctement terminée.
        Voulez-vous reprendre l'édition?
        Attention si vous cliquez sur Annuler, les éventuelles modifications seront perdues."
    ),
    "Attention, il manque des informations obligatoires au regard du SEDA.
    Voulez-vous enregistrer le transfert dans l'état ?"
    => __d(
        'js',
        "Attention, il manque des informations obligatoires au regard du SEDA. 
        Voulez-vous enregistrer le transfert dans l'état ?"
    ),
    "L'édition de ce transfert ne s'est pas correctement terminée ou est encore en cours. 
    Merci d'éditer ce transfert avant de pouvoir l'envoyer."
    => __d(
        'js',
        "L'édition de ce transfert ne s'est pas correctement terminée ou est encore en cours. 
        Merci d'éditer ce transfert avant de pouvoir l'envoyer."
    ),
    "Des erreurs on été détectées et bloquent l'envoi.
    Merci de corriger ces erreurs en éditant le transferts."
    => __d(
        'js',
        "Des erreurs on été détectées et bloquent l'envoi. 
        Merci de corriger ces erreurs en éditant le transferts."
    ),
    "Enregistrer dans l'état"
    => __d('js', "Enregistrer dans l'état"),
    "Liste des jobs du tube {0}"
    => __d('js', "Liste des jobs du tube {0}"),
    "Liste des jobs {0} du tube {1}"
    => __d('js', "Liste des jobs {0} du tube {1}"),
    "Fermer"
    => __d('js', "Fermer"),
    "Supprimer l'option {0}"
    => __d('js', "Supprimer l'option {0}"),
    "Worker en cours d'exécution, continuer d'attendre ?"
    => __d('js', "Worker en cours d'exécution, continuer d'attendre ?"),
    "Veuillez sélectionner un certificat"
    => __d('js', "Veuillez sélectionner un certificat"),
    "Sélectionnez au moins un transfert"
    => __d('js', "Sélectionnez au moins un transfert"),
    "Sélectionner une page"
    => __d('js', "Sélectionner une page"),
    "Veuillez sélectionner un numéro de page"
    => __d('js', "Veuillez sélectionner un numéro de page"),
    "Fichier non trouvé dans le XML"
    => __d('js', "Fichier non trouvé dans le XML"),
    "L'élément a bien été supprimé.\\nIl est lié au fichier \"{0}\", si vous souhaitez également le supprimer,\\n
     veuillez écrire \"delete\" puis cliquer sur OK."
    => __d(
        'js',
        "L'élément a bien été supprimé.\\nIl est lié au fichier \"{0}\", si vous souhaitez également le supprimer,\\n
        veuillez écrire \"delete\" puis cliquer sur OK."
    ),
    "Il y a {0} fichiers liés à ce noeud:\\n{1}\\npour les supprimer, veuillez saisir le mot \"delete\""
    => __d(
        'js',
        "Il y a {0} fichiers liés à ce noeud:\\n{1}\\npour les supprimer, 
        veuillez saisir le mot \"delete\""
    ),
    "Aucun résultat"
    => __d('js', "Aucun résultat"),
    "Oui"
    => __d('js', "Oui"),
    "Non"
    => __d('js', "Non"),
    "LatestDate ne peut pas être plus ancienne que OldestDate"
    => __d('js', "LatestDate ne peut pas être plus ancienne que OldestDate"),
    "Service d'Archives"
    => __d('js', "Service d'Archives"),
    "Service demandeur"
    => __d('js', "Service demandeur"),
    "Service producteur"
    => __d('js', "Service producteur"),
    "Mot de passe"
    => __d('js', "Mot de passe"),
    "Connexion réussie!"
    => __d('js', "Connexion réussie!"),
    "Echec de connexion"
    => __d('js', "Echec de connexion"),
    "Tester"
    => __d('js', "Tester"),
    "{0} en BDD ; {1} en attente"
    => __d('js', "{0} en BDD ; {1} en attente"),
    "Autorisé"
    => __d('js', "Autorisé"),
    "Non autorisé"
    => __d('js', "Non autorisé"),
    "Valide"
    => __d('js', "Valide"),
    "Pas assez complexe"
    => __d('js', "Pas assez complexe"),
    "Identique au mot de passe"
    => __d('js', "Identique au mot de passe"),
    "Différent du mot de passe"
    => __d('js', "Différent du mot de passe"),
    "Attention, le fichier téléchargé a été détecté non intègre.
Ne tentez pas de l'utiliser et supprimez le des téléchargements.
Merci de signaler cette anomalie à votre administrateur."
    => __d(
        'js',
        "Attention, le fichier téléchargé a été détecté non intègre.
Ne tentez pas de l'utiliser et supprimez le des téléchargements.
Merci de signaler cette anomalie à votre administrateur."
    ),
    "Le transfert est peut-être encore en cours d'envoi.
Si vous souhaitez supprimer ce transfert, taper le mot 'delete' et cliquez sur le bouton OK"
    => __d(
        'js',
        "Le transfert est peut-être encore en cours d'envoi.
Si vous souhaitez supprimer ce transfert, taper le mot 'delete' et cliquez sur le bouton OK"
    ),
    "Numéro de page ({0}) supérieur au nombre total de pages ({1})"
    => __d(
        'js',
        "Numéro de page ({0}) supérieur au nombre total de pages ({1})"
    ),
    "Message de l'administrateur" => __d('js', "Message de l'administrateur"),
    "Ce rôle n'est pas disponible pour le type d'entité sélectionné"
    => __d('js', "Ce rôle n'est pas disponible pour le type d'entité sélectionné"),
    "Actualiser" => __d('js', "Actualiser"),
    "Doit contenir au moins un élément de séquence" => __d('js', "Doit contenir au moins un élément de séquence"),
];
