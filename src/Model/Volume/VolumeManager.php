<?php

/**
 * Asalae\Model\Volume\VolumeManager
 */

namespace Asalae\Model\Volume;

use Asalae\Model\Entity\SecureDataSpace;
use Asalae\Model\Entity\Volume;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Table\StoredFilesVolumesTable;
use Asalae\Model\Table\VolumesTable;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Driver\Volume\VolumeInterface;
use AsalaeCore\Exception\IntegrityException;
use AsalaeCore\Utility\FormatError;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Log\Log;
use Cake\ORM\Entity;
use Cake\ORM\Exception\PersistenceFailedException;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use DateTime;
use Exception;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Permet la gestion des volumes au sein d'un espace de conservation sécurisé
 *
 * @category Volume
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class VolumeManager
{
    public const string ROLLBACK_FILE_EXT = 'rollback';
    public const string DRIVERS_CONFIG_PATH = 'Volumes.drivers';
    public const string HASH_ALGO = 'sha256';
    /**
     * @var string clé de cryptage/décryptage (hashé en sha256)
     */
    public const string DECRYPT_KEY = 'timestamper';
    /**
     * Emplacement pour les transactions format fichier
     */
    public const string TRANSACTIONS_BASE_DIR = TMP . 'volumes';
    /**
     * @var StoredFilesTable
     */
    public $StoredFiles;
    /**
     * @var VolumesTable
     */
    public $Volumes;
    /**
     * @var StoredFilesVolumesTable
     */
    public $StoredFilesVolumes;
    /**
     * @var Filesystem
     */
    public $Filesystem;
    /**
     * @var SecureDataSpace
     */
    private $space;
    /**
     * @var VolumeInterface[]
     */
    private $volumes;
    /**
     * @var array Liste des actions à effectuer en cas de rollback
     */
    private $toCallForRollback = [];
    /**
     * @var string chemin vers le dossier de transaction fichier
     */
    private $transactionDir;
    /**
     * @var VolumeException un des volume ne fonctionne pas (read only)
     */
    private $volumeDown = null;

    /**
     * Manager constructor.
     * @param integer $secure_data_space_id
     * @throws VolumeException
     */
    public function __construct(int $secure_data_space_id)
    {
        $this->space = TableRegistry::getTableLocator()->get('SecureDataSpaces')
            ->find()
            ->where(['SecureDataSpaces.id' => $secure_data_space_id])
            ->contain(['Volumes' => ['sort' => ['read_duration']]])
            ->firstOrFail();
        $allDown = true;
        $decryptKey = hash('sha256', self::DECRYPT_KEY);
        /** @var Volume $volume */
        foreach ($this->space->get('volumes') as $volume) {
            if ($volume->get('active') === false) {
                continue;
            }
            $config = Configure::read(
                self::DRIVERS_CONFIG_PATH . '.' . $volume->get('driver')
            );

            /** @var string|VolumeInterface $classname */
            $classname = '\\' . ltrim($config['class'], '\\');
            $id = $volume->get('id');
            $fields = json_decode($volume->get('fields'), true);
            $args = [];
            $configFields = (array)Hash::get($config, 'fields', []);
            foreach ($configFields as $field => $params) {
                $value = null;
                if (!empty($params['read_config'])) {
                    $value = Configure::read($params['read_config']);
                } elseif (isset($fields[$field])) {
                    $value = $fields[$field];
                } elseif (!empty($params['default'])) {
                    $value = $params['default'];
                }
                if ($id && isset($params['type']) && $params['type'] === 'password') {
                    $value = Security::decrypt(
                        base64_decode($fields[$field]),
                        $decryptKey
                    );
                }
                $args[$field] = $value ?: '';
            }

            $args['volume_entity'] = $volume;
            $this->volumes[$volume->get('id')] = false;
            try {
                $this->volumes[$volume->get('id')] = new $classname($args);
                $allDown = false;
            } /** @noinspection PhpRedundantCatchClauseInspection */ catch (VolumeException $e) {
                $this->volumeDown = $e->wrap($volume);
            }
        }
        if ($allDown) {
            if (isset($e)) {
                throw $e;
            }
            throw new VolumeException(VolumeException::VOLUME_NOT_ACCESSIBLE);
        }
        $this->StoredFiles = TableRegistry::getTableLocator()->get(
            'StoredFiles'
        );
        $this->Volumes = TableRegistry::getTableLocator()->get('Volumes');
        $this->StoredFilesVolumes = TableRegistry::getTableLocator()->get(
            'StoredFilesVolumes'
        );

        $this->transactionDir = self::TRANSACTIONS_BASE_DIR
            . DS
            . ((new DateTime())->format('Ymdhis'))
            . getmypid();
        $this->Filesystem = new Filesystem();
    }

    /**
     * Equivalent du fileGetContent mais avec un stored_file_id
     * @param string|int $id stored_file_id
     * @return string
     * @throws VolumeException
     */
    public function get($id): string
    {
        return $this->read(['StoredFiles.id' => $id]);
    }

    /**
     * Effectue une lecture sur un volume de l'espace de stockage (priorité aux
     * plus rapides).
     * Appelera $callback sur le driver cible (fileGetContent, readfile)
     * @param array  $conditions
     * @param string $callback   fileGetContent || readfile
     * @return string|bool contenu du fileGetContent ou succès du readfile
     * @throws VolumeException
     */
    private function read(
        array $conditions,
        string $callback = 'fileGetContent'
    ): string {
        $links = $this->StoredFilesVolumes->find()
            ->select(
                [
                    'volume_id',
                    'storage_ref',
                    'volume_name' => 'Volumes.name',
                    'filename' => 'StoredFiles.name',
                    'hash' => 'StoredFiles.hash',
                    'hash_algo' => 'StoredFiles.hash_algo',
                    'size' => 'StoredFiles.size',
                ]
            )
            ->innerJoinWith('StoredFiles')
            ->innerJoinWith('Volumes')
            ->where($conditions)
            ->orderBy(
                [
                    'StoredFilesVolumes.is_integrity_ok IS NOT NULL', // is_integrity_ok true en 1er
                    'StoredFilesVolumes.is_integrity_ok IS FALSE', // is_integrity_ok false en dernier
                    'Volumes.read_duration', // rangé ensuite par vitesse de lecture
                ]
            );
        if ($links->count() === 0) {
            throw new RecordNotFoundException(
                sprintf(
                    'Record not found in table "%s"',
                    $this->StoredFilesVolumes->getTable()
                )
            );
        }
        /** @var EntityInterface $link */
        foreach ($links as $link) {
            $filename = $link->get('filename');
            $driver = $this->volumes[$link->get('volume_id')] ?? false;
            if ($driver === false) {
                continue;
            }
            try {
                return $driver->$callback(
                    $link->get('storage_ref'),
                    $link->get('hash_algo'),
                    $link->get('hash')
                );
            } catch (VolumeException | IntegrityException $e) {
                $volume = new Entity(
                    [
                        'id' => $link->get('volume_id'),
                        'name' => $link->get('volume_name'),
                    ]
                );
                $e->wrap($volume, $filename);
            }
        }
        if (isset($e)) {
            throw $e;
        }
        throw new VolumeException(VolumeException::VOLUME_NOT_ACCESSIBLE);
    }

    /**
     * Décrypte un json crypté
     * @param string $bin
     * @return string
     */
    public static function decrypt(string $bin): string
    {
        $key = hash(self::HASH_ALGO, __CLASS__);
        return (string)Security::decrypt($bin, $key);
    }

    /**
     * Destructeur de classe
     */
    public function __destruct()
    {
        try {
            $this->rollback();
        } catch (Exception) {
        }
    }

    /**
     * Met fin à une transaction (restaure à l'état initial)
     */
    private function rollback()
    {
        if (!is_dir($this->transactionDir)) {
            return;
        }
        $failed = [];
        foreach ($this->toCallForRollback as $rollback) {
            try {
                call_user_func_array($rollback['callback'], $rollback['args']);
            } catch (\Exception $e) {
                Log::error((string)$e);
                $failed[] = $rollback;
            }
        }
        if ($failed) {
            file_put_contents(
                $this->transactionDir . '.serialize',
                serialize($failed)
            );
        }
        $this->toCallForRollback = [];
        $this->Filesystem->remove($this->transactionDir);
    }

    /**
     * Fonction de suppression statique (pour rollback)
     * @param string $ref
     * @param int    $volume_id
     * @throws VolumeException
     */
    public static function remove(string $ref, int $volume_id)
    {
        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        $volume = $Volumes->get($volume_id);
        $config = Configure::read(
            self::DRIVERS_CONFIG_PATH . '.' . $volume->get('driver')
        );
        $classname = '\\' . ltrim($config['class'], '\\');
        $args = json_decode($volume->get('fields'), true);
        /** @var \AsalaeCore\Driver\Volume\VolumeInterface $driver */
        $driver = new $classname($args);
        try {
            $driver->fileDelete($ref);
        } catch (VolumeException $e) {
            if ($e->getCode() !== 404) {
                throw $e->wrap($volume, $ref);
            }
        }
    }

    /**
     * Supprime un fichier sur un volume de stockage en fournissant sa
     * référence de stockage. Dans le cas d'un volume de type File System,
     * il faut utiliser la commande shred pour supprimer les fichiers.
     * @param string $storageReference
     *      Référence de stockage du fichier sur le volume.
     *      Cette référence a été retournée par les fonctions d'écriture des fichiers sur le volume.
     * @throws VolumeException
     *      FILE_NOT_FOUND
     *          Le fichier n'est pas présent ou n'est pas accessible.
     *      FILE_DELETE_ERROR
     *          La suppression du fichier ne s'est pas correctement effectuée.
     */
    public function fileDelete(string $storageReference)
    {
        if ($this->volumeDown) {
            throw $this->volumeDown;
        }
        /** @var EntityInterface $file */
        $file = $this->StoredFiles->find()
            ->where(
                [
                    'StoredFiles.secure_data_space_id' => $this->space->get(
                        'id'
                    ),
                    'StoredFiles.name' => $storageReference,
                ]
            )
            ->contain(['StoredFilesVolumes' => ['Volumes']])
            ->firstOrFail();
        $size = $file->get('size');
        $exp = new QueryExpression("disk_usage - $size");
        /** @var Connection $conn */
        $conn = ConnectionManager::get('default');
        $conn->begin();
        /** @var EntityInterface $link */
        foreach ($file->get('stored_files_volumes') as $link) {
            /** @var EntityInterface $volume */
            $volume = $link->get('volume');
            if (isset($this->volumes[$volume->get('id')])) {
                $driver = $this->volumes[$volume->get('id')];
                try {
                    if (!$driver || !$this->retryIfFailed([$driver, 'ping'], [])) {
                        throw new VolumeException(
                            VolumeException::VOLUME_NOT_ACCESSIBLE
                        );
                    }
                    if ($driver->fileExists($storageReference)) {
                        $this->retryIfFailed(
                            [$driver, 'fileDelete'],
                            [$storageReference]
                        );
                    }
                } catch (VolumeException) {
                }
            }
            $this->Volumes->updateAll(
                ['disk_usage' => $exp],
                ['id' => $volume->id]
            );
            $this->Volumes->updateAll(
                ['disk_usage' => 0],
                ['disk_usage <' => 0]
            );
            $this->StoredFilesVolumes->delete($link);
        }
        $this->StoredFiles->delete($file);
        $conn->commit();
    }

    /**
     * Débute une transaction
     */
    private function begin()
    {
        if (!is_dir($this->transactionDir)) {
            mkdir($this->transactionDir, 0777, true);
        }
    }

    /**
     * Permet d'executer x fois la commande avec 1s de pause pour gérer les
     * interruptions momentanés
     * @param callable $fn
     * @param array    $args
     * @return mixed
     */
    public function retryIfFailed(callable $fn, array $args)
    {
        $retryLimit = Configure::read('VolumeManager.retry', 10);
        for ($i = 0; $i < $retryLimit; $i++) {
            try {
                return call_user_func_array($fn, $args);
            } /** @noinspection PhpRedundantCatchClauseInspection */ catch (VolumeException $e) {
                sleep(1); //NOSONAR
            }
        }
        if (isset($e)) {
            throw $e;
        }
    }

    /**
     * Teste la présence d'un fichier sur un volume de stockage
     * en fournissant sa référence de stockage
     * @param string $name     Nom du fichier
     * @param bool   $checkAll si true, vérifi tous les volumes, sinon renvoi true si trouvé
     *                         sur un des volumes
     * @return bool true si le fichier est présent, false dans le cas contraire
     * @throws VolumeException
     */
    public function fileExists(string $name, bool $checkAll = false): bool
    {
        if ($this->volumeDown && $checkAll) {
            throw $this->volumeDown;
        }
        /** @var EntityInterface $file */
        $file = $this->StoredFiles->find()
            ->where(
                [
                    'StoredFiles.secure_data_space_id' => $this->space->get(
                        'id'
                    ),
                    'StoredFiles.name' => $name,
                ]
            )
            ->contain(
                [
                    'StoredFilesVolumes' => function (Query $q) {
                        return $q->orderBy(
                            [
                                'StoredFilesVolumes.is_integrity_ok IS NOT NULL', // is_integrity_ok true en 1er
                                'StoredFilesVolumes.is_integrity_ok IS FALSE', // is_integrity_ok false en dernier
                                'Volumes.read_duration', // rangé ensuite par vitesse de lecture
                            ]
                        )
                            ->contain(['Volumes']);
                    },
                ]
            )
            ->first();
        if (!$file) {
            return false;
        }
        $allExists = false;
        /** @var EntityInterface $link */
        foreach ($file->get('stored_files_volumes') as $link) {
            /** @var EntityInterface $volume */
            $volume = $link->get('volume');
            $driver = $this->volumes[$volume->get('id')] ?? false;
            $exists = $driver && $driver->fileExists($link->get('storage_ref'));
            if ($exists && !$checkAll) {
                return true;
            } elseif (!$exists && $checkAll) {
                return false;
            } elseif ($exists && $checkAll) {
                $allExists = true;
            }
        }
        return $checkAll ? $allExists : false;
    }

    /**
     * Met fin à une transaction (garde les modifications)
     */
    private function commit()
    {
        $this->toCallForRollback = [];
        $this->Filesystem->remove($this->transactionDir);
    }

    /**
     * Ecrit le contenu d'un fichier dans un fichier de destination
     * sur un volume de stockage. Le nom du fichier de destination doit
     * être unique et peut comporter une arborescence de dossiers et sous dossiers.
     * L'arborescence utilise le caractère / comme le système de fichier linux.
     * @param string $destinationFilename
     * @param string $content
     * @return EntityInterface StoredFile
     * @throws VolumeException
     *      FILE_ALREADY_EXISTS
     *          Le fichier de destination existe déjà sur le volume de stockage
     *      FILE_WRITE_ERROR
     *          L'écriture du fichier de destination ne s'est pas correctement effectuée
     *      FULL_DISK
     *          L'écriture ne s'est pas effectué car la taille du fichier excède la taille restante
     */
    public function filePutContent(string $destinationFilename, string $content)
    {
        return $this->put(
            $destinationFilename,
            strlen($content),
            hash(self::HASH_ALGO, $content),
            'filePutContent',
            $destinationFilename,
            $content
        );
    }

    /**
     * Factorisation du code entre filePutContent et fileUpload
     * @param string $destinationFilename
     * @param int    $filesize
     * @param string $hash
     * @param string $action
     * @param mixed  ...$args
     * @return EntityInterface StoredFile
     * @throws VolumeException
     */
    private function put(
        string $destinationFilename,
        $filesize,
        string $hash,
        string $action,
        ...$args
    ) {
        $filesize = (int)$filesize;
        if ($this->volumeDown) {
            throw $this->volumeDown;
        }
        // Effectue des vérifications (dépassement de disk_usage)
        $this->checkSpace($filesize);

        // Récupère les meta données
        try {
            $volumeFile = $this->StoredFiles->findOrCreate(
                [
                    'secure_data_space_id' => $this->space->get('id'),
                    'name' => $destinationFilename,
                    'size' => $filesize,
                    'hash' => $hash,
                    'hash_algo' => self::HASH_ALGO,
                ]
            );
        } catch (PersistenceFailedException $e) {
            $volumeFile = $this->StoredFiles->newEmptyEntity()->setError('name', $e->getMessage());
        }
        if ($volumeFile->getErrors()) {
            FormatError::logEntityErrors($volumeFile);
            throw new VolumeException(VolumeException::FILE_WRITE_ERROR);
        }

        /** @var Connection $conn */
        $conn = ConnectionManager::get('default');
        $conn->begin();

        // Sauvergarde en base
        $volumes = [];
        $exp = new QueryExpression("disk_usage + $filesize");
        /** @var Volume $volume */
        foreach ($this->space->get('volumes') as $volume) {
            $this->Volumes->updateAll(
                ['disk_usage' => $exp],
                ['id' => $volume->id]
            );
            $volumes[$volume->id] = $volume;
        }
        $success = $this->StoredFiles->save($volumeFile) !== false;

        // Crée un fichier sur les volumes
        if (!$success) {
            $conn->rollback();
            throw new VolumeException(VolumeException::FILE_WRITE_ERROR);
        }

        $links = [];
        $this->begin();
        $id = null;
        try {
            foreach ($this->volumes as $id => $vol) {
                if ($vol === false) {
                    throw new VolumeException(
                        VolumeException::VOLUME_NOT_ACCESSIBLE
                    );
                }
                $refName = $this->retryIfFailed([$vol, $action], $args);
                $this->deleteIfRollback($refName, $id);
                $links[] = $this->StoredFilesVolumes->newEntity(
                    [
                        'storage_ref' => $refName,
                        'volume_id' => $id,
                        'stored_file_id' => $volumeFile->get('id'),
                    ]
                );
            }
            if (!$this->StoredFilesVolumes->saveMany($links)) {
                throw new Exception();
            }
        } catch (VolumeException $e) {
            $conn->rollback();
            $this->rollback();
            $exp = new QueryExpression("disk_usage - $filesize");
            /** @var Volume $volume */
            foreach ($this->space->get('volumes') as $volume) {
                $this->Volumes->updateAll(
                    ['disk_usage' => $exp],
                    ['id' => $volume->id]
                );
            }
            throw $e->wrap($volumes[$id] ?? new Entity(), $destinationFilename);
        }
        $this->commit();
        $conn->commit();
        return $volumeFile;
    }

    /**
     * Vérifi qu'on ne va pas tomber dans un fulldisk
     * @param int $filesize
     * @throws VolumeException
     */
    private function checkSpace($filesize)
    {
        /** @var Volume $volume */
        foreach ($this->space->get('volumes') as $volume) {
            if ($volume->get('disk_usage') + $filesize > $volume->get('max_disk_usage')) {
                $e = new VolumeException(VolumeException::FULL_DISK);
                throw $e->wrap($volume);
            }
        }
    }

    /**
     * Ajoute une suppression en cas de rollback
     * @param string $storageReference
     * @param int    $volume_id
     */
    private function deleteIfRollback(string $storageReference, int $volume_id)
    {
        $this->toCallForRollback[] = [
            'callback' => [$this->volumes[$volume_id], 'fileDelete'],
            'args' => [$storageReference],
        ];
        $data = json_encode(
            [
                'callback' => [__CLASS__, 'remove'],
                'args' => [$storageReference, $volume_id],
            ],
            JSON_UNESCAPED_SLASHES
        );
        $filename = $this->transactionDir
            . DS
            . count($this->toCallForRollback)
            . '.' . self::ROLLBACK_FILE_EXT;
        file_put_contents($filename, self::encrypt($data));
    }

    /**
     * Encrypte un json par mesure de sécurité
     * @param string $data
     * @return string
     */
    public static function encrypt(string $data): string
    {
        $key = hash(self::HASH_ALGO, __CLASS__);
        return Security::encrypt($data, $key);
    }

    /**
     * Retourne le contenu d'un fichier sur un volume de stockage
     * en fournissant sa référence de stockage.
     * @param string $name
     *      Nom du fichier (chemin relatif)
     * @return string binaire du fichier
     * @throws VolumeException
     *      FILE_NOT_FOUND
     *          Le fichier n'existe pas ou n'est pas accessible
     *      FILE_READ_ERROR
     *          La lecture du fichier ne s'est pas correctement effectuée
     */
    public function fileGetContent(string $name): string
    {
        return $this->read(
            [
                'StoredFiles.secure_data_space_id' => $this->space->get('id'),
                'StoredFiles.name' => $name,
            ]
        );
    }

    /**
     * Ecrit sur le File System un fichier stocké sur un volume de stockage
     * en fournissant sa référence de stockage.
     * @param string $name
     *      Nom du fichier (chemin relatif)
     * @param string $destinationFileUri
     *      Chemin absolu sur le File System du fichier de destination dans
     *      lequel sera copié le fichier présent sur le volume de stockage.
     * @throws VolumeException
     *      FILE_NOT_FOUND
     *          Le fichier n'est pas présent ou n'est pas accessible.
     *      LOCAL_FILE_WRITE_ERROR
     *          L'écriture du fichier ne s'est pas correctement effectuée
     */
    public function fileDownload(string $name, string $destinationFileUri)
    {
        $dir = dirname($destinationFileUri);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        if (is_file($destinationFileUri)) {
            throw new VolumeException(VolumeException::FILE_ALREADY_EXISTS);
        }
        /** @var EntityInterface $file */
        $file = $this->StoredFiles->find()
            ->select(
                [
                    'id' => 'Volumes.id',
                    'name' => 'Volumes.name',
                    'storage_ref' => 'StoredFilesVolumes.storage_ref',
                    'hash' => 'StoredFiles.hash',
                    'hash_algo' => 'StoredFiles.hash_algo',
                ]
            )
            ->innerJoinWith('StoredFilesVolumes')
            ->innerJoinWith('StoredFilesVolumes.Volumes')
            ->where(
                [
                    'StoredFiles.secure_data_space_id' => $this->space->get(
                        'id'
                    ),
                    'StoredFiles.name' => $name,
                ]
            )
            ->orderBy(
                [
                    'StoredFilesVolumes.is_integrity_ok IS NOT NULL', // is_integrity_ok true en 1er
                    'StoredFilesVolumes.is_integrity_ok IS FALSE', // is_integrity_ok false en dernier
                    'Volumes.read_duration', // rangé ensuite par vitesse de lecture
                ]
            )
            ->all();
        /** @var EntityInterface $link */
        foreach ($file as $link) {
            $driver = $this->volumes[$link->get('id')] ?? false;
            if ($driver === false) {
                continue;
            }
            try {
                $driver->fileDownload(
                    $link->get('storage_ref'),
                    $destinationFileUri,
                    $link->get('hash_algo'),
                    $link->get('hash')
                );
                return;
            } catch (VolumeException $e) {
                $e->wrap($link, $name);
            }
        }
        if (isset($e)) {
            throw $e;
        }
        throw new VolumeException(VolumeException::VOLUME_NOT_ACCESSIBLE);
    }

    /**
     * Vérifi qu'un fichier n'existe pas déjà sur un des volumes
     * @param string $name
     * @return bool
     * @throws VolumeException
     */
    public function fileNotExists(string $name): bool
    {
        if ($this->volumeDown) {
            throw $this->volumeDown;
        }

        /** @var EntityInterface $file */
        $file = $this->StoredFiles->find()
            ->where(
                [
                    'StoredFiles.secure_data_space_id' => $this->space->get(
                        'id'
                    ),
                    'StoredFiles.name' => $name,
                ]
            )
            ->contain(
                [
                    'StoredFilesVolumes' => function (Query $q) {
                        return $q->orderBy(
                            [
                                'StoredFilesVolumes.is_integrity_ok IS NOT NULL', // is_integrity_ok true en 1er
                                'StoredFilesVolumes.is_integrity_ok IS FALSE', // is_integrity_ok false en dernier
                                'Volumes.read_duration', // rangé ensuite par vitesse de lecture
                            ]
                        )
                            ->contain(['Volumes']);
                    },
                ]
            )
            ->first();
        if (!$file) {
            return true;
        }
        /** @var EntityInterface $link */
        foreach ($file->get('stored_files_volumes') as $link) {
            /** @var EntityInterface $volume */
            $volume = $link->get('volume');
            $driver = $this->volumes[$volume->get('id')] ?? false;
            $exists = $driver && $driver->fileExists($link->get('storage_ref'));
            if ($exists) {
                return false;
            }
        }
        return true;
    }

    /**
     * effectue le rollback des processus tués
     */
    public static function rollbackKilledProcesses()
    {
        foreach (glob(self::TRANSACTIONS_BASE_DIR . DS . '*') as $pid) {
            $files = glob($pid . DS . '*');
            uasort(
                $files,
                function ($a, $b) {
                    $a = (int)pathinfo($a, PATHINFO_FILENAME);
                    $b = (int)pathinfo($b, PATHINFO_FILENAME);
                    return ($a > $b) ? -1 : 1;
                }
            );
            foreach ($files as $filename) {
                if (
                    pathinfo(
                        $filename,
                        PATHINFO_EXTENSION
                    ) !== self::ROLLBACK_FILE_EXT
                ) {
                    unlink($filename);
                    continue;
                }
                $process = file_get_contents($filename);
                if (empty($process)) {
                    continue;
                }
                $data = json_decode(self::decrypt($process), true);
                if (empty($data) || !is_array($data)) {
                    throw new Exception('unable to rollback');
                }
                call_user_func_array($data['callback'], $data['args']);
                unlink($filename);
            }
            rmdir($pid);
        }
    }

    /**
     * Permet de modifier le contenu d'un fichier par son id
     * @param int    $id
     * @param string $content
     * @throws VolumeException
     */
    public function set(int $id, string $content)
    {
        if ($this->volumeDown) {
            throw $this->volumeDown;
        }
        $tmpFile = sys_get_temp_dir() . DS . uniqid('upload-', true);
        file_put_contents($tmpFile, $content);
        try {
            $this->replace($id, $tmpFile);
        } finally {
            unlink($tmpFile);
        }
    }

    /**
     * Permet de modifier le contenu d'un fichier par son id
     * @param int    $stored_file_id
     * @param string $filename
     * @throws VolumeException
     */
    public function replace(int $stored_file_id, string $filename)
    {
        if ($this->volumeDown) {
            throw $this->volumeDown;
        }
        $filesize = filesize($filename);
        // Effectue des vérifications (dépassement de disk_usage)
        $this->checkSpace($filesize);

        $file = $this->StoredFiles->find()
            ->where(['StoredFiles.id' => $stored_file_id])
            ->contain(['StoredFilesVolumes' => ['Volumes']])
            ->firstOrFail();

        $diffsize = $filesize - $file->get('size');
        $this->StoredFiles->patchEntity(
            $file,
            [
                'size' => $filesize,
                'hash' => hash_file(self::HASH_ALGO, $filename),
                'hash_algo' => self::HASH_ALGO,
            ]
        );
        $conn = $this->StoredFiles->getConnection();
        $conn->begin();

        $exp = new QueryExpression("disk_usage + $diffsize");
        /** @var Volume $volume */
        foreach ($this->space->get('volumes') as $volume) {
            $this->Volumes->updateAll(
                ['disk_usage' => $exp],
                ['id' => $volume->id]
            );
        }
        $success = $this->StoredFiles->save($file) !== false;

        // Crée un fichier sur les volumes
        if (!$success) {
            $conn->rollback();
            throw new VolumeException(VolumeException::FILE_WRITE_ERROR);
        }

        $backups = [];
        try {
            foreach ($this->volumes as $volume_id => $vol) {
                if ($vol === false) {
                    throw new VolumeException(
                        VolumeException::VOLUME_NOT_ACCESSIBLE
                    );
                }
                $now = (new DateTime())->format('Ymdhisu');
                $uploadName = sprintf('%s.%s.upload', $file->get('name'), $now);
                $backup = sprintf('%s.%s.backup', $file->get('name'), $now);

                // on prépare les infos nécessaire au rollback
                $backups[] = [$vol, $uploadName, $backup, $stored_file_id];

                // on upload le fichier avec un .timestamp.upload
                $vol->fileUpload($filename, $uploadName);
                // on s'assure que le fichier s'est bien uploadé
                if ($vol->hash($uploadName, $file->get('hash_algo')) === $file->get('hash')) {
                    // on renomme l'ancien fichier en .backup
                    if ($vol->fileExists($file->get('name'))) {
                        $vol->rename($file->get('name'), $backup);
                    }
                    // on renomme le .timestamp.upload vers son nom définitif
                    $vol->rename($uploadName, $file->get('name'));
                } else {
                    throw new VolumeException(
                        VolumeException::FILE_WRITE_ERROR
                    );
                }
            }
        } catch (VolumeException $e) {
            $conn->rollback();
            Log::error(
                __(
                    "Une erreur à eu lieu pendant une écriture. Retour de {0} fichiers...",
                    count($backups)
                )
            );
            foreach ($this->space->get('volumes') as $volume) {
                if ($volume->id === $volume_id) {
                    $e->wrap($volume, $file->get('name'));
                }
            }
            $e->wrap($volume, $file->get('name'));
            $this->rollbackReplace($backups, $file);
            throw $e;
        }
        // on supprime les .bak si tout s'est bien terminé
        foreach ($backups as $backup) {
            if (call_user_func([$backup[0], 'fileExists'], $backup[2])) {
                call_user_func([$backup[0], 'fileDelete'], $backup[2]);
            }
        }
        $conn->commit();
    }

    /**
     * Ecrit un fichier présent sur le FileSystem
     * dans un fichier de destination sur un volume de stockage.
     * Le nom du fichier de destination doit être unique et peut comporter une
     * arborescence de dossiers et sous dossiers.
     * L'arborescence utilise le caractère / comme le système de fichier linux.
     * @param string $sourceFileUri
     *      Chemin absolu du fichier à copier sur le volume de stockage.
     * @param string $destinationFilename
     *      Nom du fichier de destination comportant une arborescence relative
     *      (ex: /ARC001/publications/offre_01.odt).
     *      Le fichier de destination ne doit pas exister.
     * @return EntityInterface StoredFile
     * @throws VolumeException
     *      FILE_ALREADY_EXISTS
     *          Le fichier de destination existe déjà sur le volume de stockage
     *      FILE_WRITE_ERROR
     *          L'écriture du fichier de destination ne s'est pas correctement effectuée
     *      LOCAL_FILE_NOT_ACCESSIBLE
     *          Le fichier source n'existe pas ou n'est pas accessible
     */
    public function fileUpload(
        string $sourceFileUri,
        string $destinationFilename
    ) {
        return $this->put(
            $destinationFilename,
            filesize($sourceFileUri),
            hash_file(self::HASH_ALGO, $sourceFileUri),
            'fileUpload',
            $sourceFileUri,
            $destinationFilename
        );
    }

    /**
     * Remet le volume à son état avant le renommage
     * @param array           $backups
     * @param EntityInterface $file
     */
    private function rollbackReplace(array $backups, EntityInterface $file)
    {
        foreach ($backups as $backupData) {
            /** @var VolumeInterface|VolumeFilesystem $vol */
            [$vol, $uploadName, $backup, $stored_file_id] = $backupData;
            try {
                $existsFile = $vol->fileExists($file->get('name'));
                $existsUpload = $vol->fileExists($uploadName);
                $existsBackup = $vol->fileExists($backup);

                if ($existsUpload && $existsBackup === false && $existsFile) {
                    // cas fichier uploadé avec mauvais hash ou plantage au backup
                    $vol->fileDelete($uploadName);
                    Log::error(
                        __(
                            "Rollback: Le fichier {0} a été supprimé sur le volume n°{2}",
                            $uploadName,
                            $stored_file_id
                        )
                    );
                } elseif ($existsBackup && $existsFile === false) {
                    // cas plantage du renommage de l'upload vers son nom définitif
                    $vol->rename($backup, $file->get('name'));
                    Log::error(
                        __(
                            "Rollback: Le fichier {0} a été renommé en {1} sur le volume n°{2}",
                            $backup,
                            $file->get('name'),
                            $stored_file_id
                        )
                    );
                    if ($existsUpload) {
                        $vol->fileDelete($uploadName);
                    }
                } elseif ($existsUpload === false && $existsBackup && $existsFile) {
                    // cas le plantage a eu lieu sur un autre volume, on restaure le .bak
                    $vol->fileDelete($file->get('name'));
                    $vol->rename($backup, $file->get('name'));
                    Log::error(
                        __(
                            "Rollback: Le fichier {0} a été renommé en {1} sur le volume n°{2}",
                            $backup,
                            $file->get('name'),
                            $stored_file_id
                        )
                    );
                }
            } catch (VolumeException) {
                Log::error(
                    __(
                        "Echec lors du rollback du fichier {0} sur le volume n°{1}",
                        $file->get('name'),
                        $stored_file_id
                    )
                );
            }
        }
    }

    /**
     * Donne un VolumeManager à partir d'un stored_file_id
     * @param int|string $stored_file_id
     * @return VolumeManager
     * @throws VolumeException
     */
    public static function createWithStoredFileId($stored_file_id): VolumeManager
    {
        $StoredFiles = TableRegistry::getTableLocator()->get('StoredFiles');
        $secureDataSpaceId = $StoredFiles->find()
            ->select(['secure_data_space_id'])
            ->where(['id' => $stored_file_id])
            ->firstOrFail()
            ->get('secure_data_space_id');
        return new self($secureDataSpaceId);
    }

    /**
     * Getter du driver d'un volume
     * @param int    $volume_id
     * @param string $key
     * @return VolumeInterface
     * @throws VolumeException
     */
    public static function getDriverById(
        int $volume_id,
        string $key = self::DECRYPT_KEY
    ) {
        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        $volume = $Volumes->get($volume_id);
        return self::getDriver($volume, $key);
    }

    /**
     * Getter du driver d'un volume
     * @param EntityInterface $volume
     * @param string          $key
     * @return VolumeInterface
     * @throws VolumeException
     * @noinspection PhpDocRedundantThrowsInspection faux positif sur VolumeException
     */
    public static function getDriver(
        EntityInterface $volume,
        string $key = self::DECRYPT_KEY
    ) {
        $config = Configure::read(
            self::DRIVERS_CONFIG_PATH . '.' . $volume->get('driver')
        );
        /** @var class-string<VolumeInterface> $classname */
        $classname = '\\' . ltrim($config['class'], '\\');
        $fields = json_decode($volume->get('fields'), true);
        $decryptKey = hash('sha256', $key);
        $args = [];
        foreach ((array)Hash::get($config, 'fields', []) as $field => $params) {
            $value = null;
            if (!empty($params['read_config'])) {
                $value = Configure::read($params['read_config']);
            } elseif (isset($fields[$field])) {
                $value = $fields[$field];
            } elseif (!empty($params['default'])) {
                $value = $params['default'];
            }
            if (isset($params['type']) && $params['type'] === 'password') {
                $value = Security::decrypt(
                    base64_decode($fields[$field]),
                    $decryptKey
                );
            }
            $args[$field] = $value ?: '';
        }

        $args['volume_entity'] = $volume;
        return new $classname($args);
    }

    /**
     * Getter des volumes liés à un espace de stockage
     * @return VolumeInterface[]
     */
    public function getVolumes(): array
    {
        return $this->volumes;
    }

    /**
     * Lecture du fichier
     * @param string $name
     * @return bool success
     * @throws VolumeException
     */
    public function readfile(string $name): bool
    {
        return $this->read(
            [
                'StoredFiles.secure_data_space_id' => $this->space->get('id'),
                'StoredFiles.name' => $name,
            ],
            'readfile'
        );
    }

    /**
     * Vérifi que le hash du fichier correspond à ce qui est stocké en base
     * @param string $name
     * @return bool
     * @throws VolumeException
     */
    public function fileIntegrity(string $name): bool
    {
        /** @var EntityInterface $file */
        $file = $this->StoredFiles->find()
            ->where(
                [
                    'StoredFiles.secure_data_space_id' => $this->space->get(
                        'id'
                    ),
                    'StoredFiles.name' => $name,
                ]
            )
            ->contain(['StoredFilesVolumes' => ['Volumes']])
            ->firstOrFail();
        /** @var EntityInterface $link */
        foreach ($file->get('stored_files_volumes') as $link) {
            /** @var EntityInterface $volume */
            $volume = $link->get('volume');
            if (!isset($this->volumes[$volume->get('id')])) {
                continue;
            }
            $driver = $this->volumes[$volume->get('id')];
            if ($driver === false) {
                throw new VolumeException(
                    VolumeException::VOLUME_NOT_ACCESSIBLE
                );
            }
            try {
                $driverHash = $driver->hash(
                    $link->get('storage_ref'),
                    $file->get('hash_algo')
                );
                if ($file->get('hash') !== $driverHash) {
                    return false;
                }
            } catch (VolumeException $e) {
                throw $e->wrap($volume, $file->get('name'));
            }
        }
        return true;
    }

    /**
     * Permet de serializer le manager (pour le rollback)
     */
    public function __sleep()
    {
        return [
            'space',
            'volumes',
            'transactionDir',
            'volumeDown',
            'Filesystem',
        ];
    }

    /**
     * Permet de unserializer le manager (pour le rollback)
     */
    public function __wakeup()
    {
        $loc = TableRegistry::getTableLocator();
        $this->StoredFiles = $loc->get('StoredFiles');
        $this->Volumes = $loc->get('Volumes');
        $this->StoredFilesVolumes = $loc->get('StoredFilesVolumes');
    }

    /**
     * Upload un fichier d'un espace de stockage vers un volume spécifique
     * @param string          $name
     * @param VolumeInterface $targetVolume
     * @param int             $volume_id
     * @throws VolumeException
     */
    public function uploadToVolume(
        string $name,
        VolumeInterface $targetVolume,
        int $volume_id
    ) {
        /** @var EntityInterface $file */
        $file = $this->StoredFiles->find()
            ->where(
                [
                    'StoredFiles.secure_data_space_id' => $this->space->id,
                    'StoredFiles.name' => $name,
                ]
            )
            ->contain(
                [
                    'StoredFilesVolumes' => function (Query $q) {
                        return $q->orderBy(
                            [
                                'StoredFilesVolumes.is_integrity_ok IS NOT NULL', // is_integrity_ok true en 1er
                                'StoredFilesVolumes.is_integrity_ok IS FALSE', // is_integrity_ok false en dernier
                                'Volumes.read_duration', // rangé ensuite par vitesse de lecture
                            ]
                        )
                            ->contain(['Volumes']);
                    },
                ]
            )
            ->firstOrFail();
        /** @var EntityInterface $link */
        foreach ($file->get('stored_files_volumes') as $link) {
            /** @var EntityInterface $volume */
            $volume = $link->get('volume');
            $driver = $this->volumes[$volume->get('id')] ?? false;
            if ($driver === false) {
                continue;
            }
            try {
                $ref = $driver->streamTo(
                    $link->get('storage_ref'),
                    $targetVolume
                );
                $nlink = $this->StoredFilesVolumes->newEntity(
                    [
                        'storage_ref' => $ref,
                        'volume_id' => $volume_id,
                        'stored_file_id' => $file->id,
                    ]
                );
                $this->StoredFilesVolumes->saveOrFail($nlink);
                return;
            } catch (VolumeException $e) {
                $e->wrap($volume, $file->get('name'));
            }
        }
        if (isset($e)) {
            throw $e;
        }
        throw new VolumeException(VolumeException::VOLUME_NOT_ACCESSIBLE);
    }

    /**
     * Getter
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->$name;
    }
}
