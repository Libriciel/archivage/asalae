<?php

/**
 * Asalae\Model\Table\CronsTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\CronsTable as CoreCronsTable;
use Cake\Event\Event;
use Cake\ORM\Entity;

/**
 * Table crons
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CronsTable extends CoreCronsTable implements SerializeDeletedEntitiesInterface, AfterDeleteInterface
{
    use SerializeDeleteAllTrait;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->hasMany('EventLogs');
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }
}
