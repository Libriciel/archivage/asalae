<?php

/**
 * Asalae\Model\Table\UsersTable
 */

namespace Asalae\Model\Table;

use ArrayAccess;
use Asalae\Model\Entity\User;
use ArrayObject;
use AsalaeCore\Controller\Component\AclComponent;
use AsalaeCore\Model\Behavior\AclBehavior;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Model\Table\BeforeSaveInterface;
use AsalaeCore\Model\Table\UsersTable as CoreUsersTable;
use AsalaeCore\Utility\Session;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Http\ServerRequest;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use Random\RandomException;
use Symfony\Component\Mercure\Exception\RuntimeException as MercureRuntimeException;
use ZMQSocketException;

/**
 * Table users
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AclBehavior
 * @mixin TimestampBehavior
 */
class UsersTable extends CoreUsersTable implements
    BeforeDeleteInterface,
    BeforeSaveInterface,
    AfterSaveInterface,
    SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->hasMany('AccessTokens')
            ->setDependent(true);
        $this->hasMany('BeanstalkJobs');
        $this->hasMany('EventLogs');
        $this->hasMany('Fileuploads');
        $this->hasMany('Killables')
            ->setDependent(true);
        $this->hasMany('Transfers')
            ->setForeignKey('created_user_id');
        $this->hasMany('ValidationActors')
            ->setForeignKey('app_foreign_key')
            ->setConditions(
                function (QueryExpression $qe, Query $q) {
                    $assocAlias = $q->getRepository()->getAlias(); // ArchivalAgencies
                    return $qe->and(["$assocAlias.app_type" => 'USER']);
                }
            );
        $this->belongsToMany('ArchivalAgencies')
            ->setClassName('OrgEntities')
            ->setThrough('SuperArchivistsArchivalAgencies');
        $this->hasMany('SuperArchivistsArchivalAgencies');
        $this->hasOne('ChangeEntityRequests');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->boolean('is_validator')
            ->notEmptyString('is_validator');

        $validator
            ->boolean('use_cert')
            ->notEmptyString('use_cert');

        return parent::validationDefault($validator);
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        // en cas de changement d'entité
        if (
            !$entity->isNew()
            && $entity->isDirty('org_entity_id')
            && (int)$entity->getOriginal('org_entity_id') !== (int)$entity->get('org_entity_id')
            && $entity->get('org_entity_id')
            && $entity->getOriginal('org_entity_id')
        ) {
            $this->cleanValidationActors($entity);

            // suppression des transferts en préparation
            $Transfers = TableRegistry::getTableLocator()->get('Transfers');
            $query = $Transfers->find()
                ->where(
                    [
                        'created_user_id' => $entity->id,
                        'state' => TransfersTable::S_PREPARATING,
                    ]
                );
            foreach ($query as $transfer) {
                $Transfers->delete($transfer, ['unlink' => true]);
            }

            // suppression des demandes de communication en préparation
            $DeliveryRequests = TableRegistry::getTableLocator()->get('DeliveryRequests');
            $query = $DeliveryRequests->find()
                ->where(
                    [
                        'created_user_id' => $entity->id,
                        'state' => DeliveryRequestsTable::S_CREATING,
                    ]
                );
            foreach ($query as $deliveryRequest) {
                $DeliveryRequests->delete($deliveryRequest);
            }

            // suppression des demandes d'élimination en préparation
            $DestructionRequests = TableRegistry::getTableLocator()->get('DestructionRequests');
            $query = $DestructionRequests->find()
                ->where(
                    [
                        'created_user_id' => $entity->id,
                        'state' => DestructionRequestsTable::S_CREATING,
                    ]
                );
            foreach ($query as $destructionRequest) {
                $DestructionRequests->delete($destructionRequest);
            }

            // suppression des demandes de restitution en préparation
            $RestitutionRequests = TableRegistry::getTableLocator()->get('RestitutionRequests');
            $query = $RestitutionRequests->find()
                ->where(
                    [
                        'created_user_id' => $entity->id,
                        'state' => RestitutionRequestsTable::S_CREATING,
                    ]
                );
            foreach ($query as $restitutionRequest) {
                $RestitutionRequests->delete($restitutionRequest);
            }

            // suppression des demandes de transferts sortants en préparation
            $OutgoingTransferRequests = TableRegistry::getTableLocator()->get('OutgoingTransferRequests');
            $query = $OutgoingTransferRequests->find()
                ->where(
                    [
                        'created_user_id' => $entity->id,
                        'state' => OutgoingTransferRequestsTable::S_CREATING,
                    ]
                );
            foreach ($query as $outgoingTransferRequest) {
                $OutgoingTransferRequests->delete($outgoingTransferRequest);
            }

            $Sessions = TableRegistry::getTableLocator()->get('Sessions');
            $Sessions->deleteAll(['user_id' => $entity->id]);
        }
    }

    /**
     * Supprime les Validation Actors en cas de changement d'entité et promotion/révocation de super archiviste
     * @param Entity $entity
     * @return void
     * @throws Exception
     */
    protected function cleanValidationActors(Entity $entity): void
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $originalEntity = $OrgEntities->find()
            ->where(['OrgEntities.id' => $entity->getOriginal('org_entity_id')])
            ->contain(['ArchivalAgencies', 'TypeEntities'])
            ->firstOrFail();
        $originalArchivalAgencyId = Hash::get($originalEntity, 'archival_agency.id');

        $newEntity = $OrgEntities->find()
            ->where(['OrgEntities.id' => $entity->get('org_entity_id')])
            ->contain(['ArchivalAgencies', 'TypeEntities'])
            ->firstOrFail();
        $newArchivalAgencyId = Hash::get($newEntity, 'archival_agency.id');

        if ($originalArchivalAgencyId === $newArchivalAgencyId) { // on garde le même SA, pas de soucis
            return;
        }

        $originalTypeEntity = Hash::get($originalEntity, 'type_entity.code');
        $originalEntityIsSE = in_array($originalTypeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT);

        $newTypeEntity = Hash::get($newEntity, 'type_entity.code');
        $newEntityIsSE = in_array($newTypeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT);

        // Est ce qu'on assigne un nouveau super archiviste à son SA d'origine ?
        // si oui, pas besoin de supprimer ses étapes de validation
        $superArchivisteKeepHisEntity = in_array(
            $entity->getOriginal('org_entity_id'),
            Hash::extract($entity, 'archival_agencies.{n}.id')
        );

        $ValidationActors = TableRegistry::getTableLocator()->get('ValidationActors');
        if ((!$originalEntityIsSE && !$newEntityIsSE) || ($newEntityIsSE && !$superArchivisteKeepHisEntity)) {
            // on supprime si on est pas dans une promotion/révocation de super archiviste ou promotion : on garde sulement si le SA d'origine est assigné
            $ValidationActors
                ->deleteAll(['app_type' => 'USER', 'app_foreign_key' => $entity->id]);
        } elseif ($originalEntityIsSE) { // revocation : on supprime si le SA ne correspond pas OU pas d'ACL
            $ids = $ValidationActors
                ->find()
                ->select(['ValidationActors.id'])
                ->innerJoinWith('ValidationStages')
                ->innerJoinWith('ValidationStages.ValidationChains')
                ->where(
                    [
                        'ValidationActors.app_type' => 'USER',
                        'ValidationActors.app_foreign_key' => $entity->id,
                        'ValidationChains.org_entity_id IS NOT' => Hash::get($newEntity, 'id')
                    ]
                );

            $ValidationActors
                ->deleteAll(['id IN' => $ids]);

            // entité correspond : on regarde les droits pour les acteurs restants
            $validationActors = $ValidationActors
                ->find()
                ->contain(['ValidationStages', 'ValidationStages.ValidationChains'])
                ->where(
                    ['ValidationActors.app_type' => 'USER', 'ValidationActors.app_foreign_key' => $entity->id]
                );

            $registry = new ComponentRegistry(new Controller(new ServerRequest()));
            $Acl = new AclComponent($registry);

            $role = TableRegistry::getTableLocator()->get('Roles')->get($entity->get('role_id'));
            foreach ($validationActors as $actor) {
                $check = $Acl->check(
                    $role,
                    self::getAcoPath(
                        Hash::get($actor, 'validation_stage.validation_chain.app_type')
                    )
                );
                if (!$check) {
                    TableRegistry::getTableLocator()->get('ValidationActors')->delete($actor);
                }
            }
        }
    }

    /**
     * retourne le aco sous forme de path correspondant au type du circuit de validation
     * utilisé pour filtrer les acteurs de type utilisateurs dans add et edit
     *
     * @param string $validation_chain_app_type
     * @return string
     * @throws Exception
     */
    public static function getAcoPath($validation_chain_app_type): string
    {
        return match ($validation_chain_app_type) {
            ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM, ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM => 'controllers/ValidationProcesses/myTransfers',
            ValidationChainsTable::ARCHIVE_DELIVERY_REQUEST => 'controllers/ValidationProcesses/deliveryRequests',
            ValidationChainsTable::ARCHIVE_DESTRUCTION_REQUEST => 'controllers/ValidationProcesses/destructionRequests',
            ValidationChainsTable::ARCHIVE_RESTITUTION_REQUEST => 'controllers/ValidationProcesses/restitutionRequests',
            ValidationChainsTable::ARCHIVE_OUTGOING_TRANSFER_REQUEST => 'controllers/ValidationProcesses/outgoingTransferRequests',
            default => throw new Exception(
                __(
                    "type de circuit de validation non géré : {0}",
                    h($validation_chain_app_type)
                )
            ),
        };
    }

    /**
     * Met à jour l'alias de l'aro lors de la modification d'un user
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws ZMQSocketException|RandomException
     * @override CoreUsersTable::afterSave()
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        if ($entity->isNew() || $entity->isDirty('username')) {
            TableRegistry::getTableLocator()->get('Aros')->updateQuery()
                ->set(['alias' => $entity->get('username')])
                ->where(['Aros.model' => 'Users', 'foreign_key' => $entity->get('id')])
                ->execute();
        }
        if (!empty($options['renewSession'])) {
            try {
                Session::emitReset($entity->get('id'));
            } catch (MercureRuntimeException) {
            }
        }
    }

    /**
     * Converts data from an ArrayAccess object into a User entity.
     *
     * @param ArrayAccess $data The data to be converted into a User entity.
     * @return User The User entity created from the provided data.
     */
    public function getUserEntityFromArrayObject(ArrayAccess $data): User
    {
        /** @var User $entity */
        $entity = $this->newEntity((array)$data);
        $this->recursiveClean($entity);
        return $entity;
    }

    /**
     * Recursively cleans an entity and its nested entities.
     *
     * @param EntityInterface $entity The entity to be cleaned, including nested entities if present.
     * @return void
     */
    private function recursiveClean(EntityInterface $entity): void
    {
        $entity->clean();
        $entity->setNew(false);
        foreach ($entity->getVisible() as $field) {
            $value = $entity->get($field);
            if ($value instanceof EntityInterface) {
                $this->recursiveClean($value);
            }
        }
    }

    /**
     * Modifies the provided query to include additional containment for generic user-related associations.
     *
     * @param Query $query The query to be modified with containment.
     * @return Query The modified query with the specified associations set for containment.
     */
    public function setGenericUserContain(Query $query): Query
    {
        return $query->contain(
            [
                'OrgEntities' => [
                    'ArchivalAgencies',
                    'TypeEntities'
                ],
                'Ldaps',
                'Roles',
            ]
        );
    }
}
