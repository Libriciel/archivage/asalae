<?php

/**
 * Asalae\Model\Table\AgreementsServiceLevelsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;

/**
 * Table agreements_service_levels
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AgreementsServiceLevelsTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Agreements');
        $this->belongsTo('ServiceLevels');
    }
}
