<?php

/**
 * Asalae\Model\Table\TechnicalArchivesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table technical_archives
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin StateMachineBehavior
 * @mixin OptionsBehavior
 * @property TechnicalArchiveUnitsTable TechnicalArchiveUnits
 */
class TechnicalArchivesTable extends Table implements
    BeforeDeleteInterface,
    SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    public const string S_CREATING = 'creating';
    public const string S_DESCRIBING = 'describing';
    public const string S_STORING = 'storing';
    public const string S_AVAILABLE = 'available';
    public const string S_DESTROYING = 'destroying';

    public const string T_DESCRIBE = 'describe';
    public const string T_STORE = 'store';
    public const string T_FINISH = 'finish';
    public const string T_DESTRUCT = 'destruct';

    public const string TYPE_ARCHIVAL_AGENCY_EVENTS_LOGS = 'ARCHIVAL_AGENCY_EVENTS_LOGS';
    public const string TYPE_DESTRUCTION = 'DESTRUCTION';
    public const string TYPE_EVENTS_LOGS = 'EVENTS_LOGS'; // deprecated
    public const string TYPE_OPERATING_EVENTS_LOGS = 'OPERATING_EVENTS_LOGS';
    public const string TYPE_RESTITUTION = 'RESTITUTION';
    public const string TYPE_ADDED_BY_USER = 'ADDED_BY_USER';

    /**
     * @var string Etat initial
     */
    public $initialState = self::S_CREATING;

    /**
     * @var array Changement d'êtats selon action
     */
    public $transitions = [
        self::T_DESCRIBE => [
            self::S_CREATING => self::S_DESCRIBING,
        ],
        self::T_STORE => [
            self::S_DESCRIBING => self::S_STORING,
        ],
        self::T_FINISH => [
            self::S_STORING => self::S_AVAILABLE,
        ],
        self::T_DESTRUCT => [
            self::S_AVAILABLE => self::S_DESTROYING,
        ],
    ];

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    self::S_CREATING,
                    self::S_DESCRIBING,
                    self::S_STORING,
                    self::S_AVAILABLE,
                    self::S_DESTROYING,
                ],
                'type' => [
                    self::TYPE_ADDED_BY_USER,
                    self::TYPE_ARCHIVAL_AGENCY_EVENTS_LOGS,
                    self::TYPE_DESTRUCTION,
                    self::TYPE_EVENTS_LOGS,
                    self::TYPE_OPERATING_EVENTS_LOGS,
                    self::TYPE_RESTITUTION,
                ],
            ]
        );

        $this->belongsTo('ArchivalAgencies')
            ->setClassName('OrgEntities');
        $this->belongsTo('SecureDataSpaces');
        $this->hasMany('TechnicalArchiveUnits')->setDependent(true);
        $this->hasOne('FirstTechnicalArchiveUnits')
            ->setForeignKey('technical_archive_id')
            ->setClassName('TechnicalArchiveUnits')
            ->setConditions(
                ['FirstTechnicalArchiveUnits.parent_id IS' => null]
            );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->boolean('is_integrity_ok')
            ->allowEmptyString('is_integrity_ok');

        $validator
            ->scalar('archival_agency_identifier')
            ->maxLength('archival_agency_identifier', 255)
            ->requirePresence('archival_agency_identifier', 'create')
            ->notEmptyString('archival_agency_identifier');

        $validator
            ->scalar('storage_path')
            ->maxLength('storage_path', 255)
            ->requirePresence('storage_path', 'create')
            ->notEmptyString('storage_path');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->requirePresence('original_size', 'create')
            ->notEmptyString('original_size');

        $validator
            ->integer('original_count')
            ->requirePresence('original_count', 'create')
            ->notEmptyString('original_count');

        $validator
            ->requirePresence('timestamp_size', 'create')
            ->notEmptyString('timestamp_size');

        $validator
            ->integer('timestamp_count')
            ->requirePresence('timestamp_count', 'create')
            ->notEmptyString('timestamp_count');

        $validator
            ->requirePresence('management_size', 'create')
            ->notEmptyString('management_size');

        $validator
            ->integer('management_count')
            ->requirePresence('management_count', 'create')
            ->notEmptyString('management_count');

        $validator
            ->dateTime('last_state_update')
            ->allowEmptyDateTime('last_state_update');

        return $validator;
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws VolumeException
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );

        $ArchiveBinaries = TableRegistry::getTableLocator()->get('ArchiveBinaries');
        $query = $this->TechnicalArchiveUnits->find()
            ->where(['TechnicalArchiveUnits.technical_archive_id' => $entity->id])
            ->contain(['ArchiveBinaries' => ['StoredFiles']]);
        $volumeManager = new VolumeManager($entity->get('secure_data_space_id'));
        foreach ($query as $technicalArchiveUnit) {
            foreach ($technicalArchiveUnit->get('archive_binaries') as $archiveBinary) {
                $ArchiveBinaries->deleteOrFail($archiveBinary);
                $volumeManager->fileDelete(Hash::get($archiveBinary, 'stored_file.name'));
            }
        }
    }

    /**
     * Query pour le contôle d'intégrité
     * @return Query
     * @throws Exception
     */
    public function findByIntegrity(): Query
    {
        return $this->find()
            ->select(
                [
                    'TechnicalArchives.id',
                    'TechnicalArchives.archival_agency_identifier',
                    'TechnicalArchives__archive_binary_id' => 'ArchiveBinaries.id',
                    'TechnicalArchives__size' => 'StoredFiles.size',
                    'TechnicalArchives__hash' => 'StoredFiles.hash',
                    'TechnicalArchives__hash_algo' => 'StoredFiles.hash_algo',
                    'TechnicalArchives__storage_ref' => 'StoredFilesVolumes.storage_ref',
                    'TechnicalArchives__volume_id' => 'StoredFilesVolumes.volume_id',
                    'TechnicalArchives__volume_name' => 'Volumes.name',
                ]
            )
            ->innerJoinWith('TechnicalArchiveUnits')
            ->innerJoinWith('TechnicalArchiveUnits.ArchiveBinaries')
            ->innerJoinWith('TechnicalArchiveUnits.ArchiveBinaries.StoredFiles')
            ->innerJoinWith(
                'TechnicalArchiveUnits.ArchiveBinaries.StoredFiles.StoredFilesVolumes'
            )
            ->innerJoinWith(
                'TechnicalArchiveUnits.ArchiveBinaries.StoredFiles.StoredFilesVolumes.Volumes'
            )
            ->where(['Volumes.active IS' => true])
            ->orderBy(
                [
                    'TechnicalArchives.is_integrity_ok IS NULL' => 'desc',
                    // nouveaux en 1er
                    'TechnicalArchives.is_integrity_ok' => 'asc',
                    // ensuite ceux en echec
                    'TechnicalArchives.integrity_date' => 'asc',
                    // par dernière vérif
                    'TechnicalArchives.created' => 'asc',
                    // par date de création
                    'StoredFilesVolumes.volume_id' => 'asc',
                    'StoredFilesVolumes.storage_ref' => 'asc',
                ]
            );
    }

    /**
     * Met à jour les compteurs des archives dans $query
     * @param Query|EntityInterface[] $technicalArchives
     */
    public function updateCountSize($technicalArchives)
    {
        $q = $this->TechnicalArchiveUnits->query();
        foreach ($technicalArchives as $technicalArchive) {
            $technicalArchiveUnit = $this->TechnicalArchiveUnits->find()
                ->where(['technical_archive_id' => $technicalArchive->id, 'parent_id IS' => null])
                ->first();
            if ($technicalArchiveUnit) {
                $baseBinaries = $this->TechnicalArchiveUnits->find()
                    ->select(
                        [
                            'count' => $q->func()->count('*'),
                            'size' => $q->func()->sum('size'),
                        ]
                    )
                    ->innerJoinWith('ArchiveBinaries')
                    ->innerJoinWith('ArchiveBinaries.StoredFiles')
                    ->where(
                        [
                            'TechnicalArchiveUnits.technical_archive_id' => $technicalArchive->id,
                            'TechnicalArchiveUnits.lft >=' => $technicalArchiveUnit->get('lft'),
                            'TechnicalArchiveUnits.rght <=' => $technicalArchiveUnit->get('rght'),
                        ]
                    );
                $original = (clone $baseBinaries)
                    ->where(['ArchiveBinaries.type' => 'original_data'])
                    ->toArray();
                $timestamp = (clone $baseBinaries)
                    ->where(['ArchiveBinaries.type LIKE' => '%_timestamp'])
                    ->toArray();
                $data = [
                    'original_size' => (int)Hash::get($original, '0.size', 0),
                    'timestamp_size' => (int)Hash::get($timestamp, '0.size', 0),
                    'original_count' => (int)Hash::get($original, '0.count', 0),
                    'timestamp_count' => (int)Hash::get($timestamp, '0.count', 0),
                ];
            } else {
                $data = [
                    'management_size' => 0,
                    'original_size' => 0,
                    'timestamp_size' => 0,
                    'management_count' => 0,
                    'original_count' => 0,
                    'timestamp_count' => 0,
                ];
            }
            $this->patchEntity($technicalArchive, $data);
            $this->saveOrFail($technicalArchive);
        }
    }
}
