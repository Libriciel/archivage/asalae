<?php

/**
 * Asalae\Model\Table\KillablesTable
 */

namespace Asalae\Model\Table;

use Asalae\Exception\GenericException;
use AsalaeCore\Model\Table\BeforeFindInterface;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use DateInterval;
use DateTime;
use Exception;

/**
 * Table killables
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class KillablesTable extends Table implements BeforeFindInterface
{
    /**
     * @var float durée en secondes minimum entre 2 vérifications
     */
    public $cooldown = 1.0;

    /**
     * @var float microtime
     */
    private $lastMicrotime;

    /**
     * @var bool table à jour
     */
    private $sync = false;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->belongsTo('Users');
        $this->belongsTo('Sessions')
            ->setForeignKey(false)
            ->setConditions(
                [
                    'Sessions.token' => new IdentifierExpression(
                        $this->getAlias() . '.session_token'
                    ),
                ]
            );
        if (isset($config['cooldown'])) {
            $this->cooldown = $config['cooldown'];
        }
        $this->lastMicrotime = microtime(true);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('session_token')
            ->maxLength('session_token', 255)
            ->allowEmptyString('session_token');

        $validator
            ->integer('pid')
            ->requirePresence('pid', 'create')
            ->notEmptyString('pid')
            ->add(
                'pid',
                'unique',
                ['rule' => 'validateUnique', 'provider' => 'table']
            );

        $validator
            ->dateTime('timeout')
            ->requirePresence('timeout', 'create')
            ->notEmptyDateTime('timeout');

        return $validator;
    }

    /**
     * Ajoute un process tuable
     * @param string       $name
     * @param string|int   $user_id
     * @param int|DateTime $timeout
     * @param string|null  $session_token
     * @return EntityInterface|false
     * @throws Exception
     */
    public function newKillableProcess(
        string $name,
        $user_id,
        $timeout,
        string $session_token = null
    ) {
        if (is_numeric($timeout)) {
            $timeout = (new DateTime())->add(
                new DateInterval('PT' . (int)$timeout . 'S')
            );
        }
        $entity = $this->newEntity(
            [
                'name' => $name,
                'pid' => getmypid(),
                'user_id' => $user_id,
                'timeout' => $timeout,
                'session_token' => $session_token,
                'killed' => false,
            ]
        );
        return $this->save($entity);
    }

    /**
     * The Model.beforeFind
     *
     * @param \Cake\Event\Event $event The beforeFind event
     * @param Query             $query Query
     * @return Query
     * @throws Exception
     */
    public function beforeFind(Event $event, $query)
    {
        if (!$this->sync) {
            $this->sync = true;
            $this->deleteAll(['timeout <' => new DateTime()]);
        }
        return $query;
    }

    /**
     * Vérifie si un ordre d'arrêt a été émis
     * @param bool $throw si vrai; throw une KilledException si process tué
     * @return bool|mixed si vrai; on doit intérrompre le script
     */
    public function checkKill($throw = false)
    {
        if ($this->lastMicrotime + $this->cooldown > microtime(true)) {
            return false;
        }
        $this->lastMicrotime = microtime(true);
        $pid = getmypid();
        $row = $this->selectQuery()
            ->select(['id', 'killed'])
            ->from('killables')
            ->where(['pid' => $pid])
            ->disableHydration()
            ->first();
        $killed = $row ? $row['killed'] : false;
        if ($throw && $killed) {
            $this->deleteAll(['id' => $row['id']]);
            throw new GenericException();
        }

        return $killed;
    }

    /**
     * Tue un process tuable
     * @param int|string $pid
     * @param int|string $user_id
     */
    public function kill($pid, $user_id)
    {
        $this->updateAll(
            ['killed' => true],
            ['pid' => $pid, 'user_id' => $user_id]
        );
    }
}
