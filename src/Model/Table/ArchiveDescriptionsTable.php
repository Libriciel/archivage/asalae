<?php

/**
 * Asalae\Model\Table\ArchiveDescriptionsTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\ORM\Table as CoreTable;
use Cake\Validation\Validator;

/**
 * Table archive_descriptions
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveDescriptionsTable extends CoreTable
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->belongsTo('ArchiveUnits');
        $this->belongsTo('AccessRules');
        $this->belongsTo('DescriptionAccessRules')
            ->setClassName('AccessRules')
            ->setForeignKey('access_rule_id');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('history')
            ->allowEmptyString('history');

        $validator
            ->scalar('file_plan_position')
            ->allowEmptyFile('file_plan_position');

        $validator
            ->date('oldest_date')
            ->allowEmptyDate('oldest_date');

        $validator
            ->date('latest_date')
            ->allowEmptyDate('latest_date');

        return $validator;
    }
}
