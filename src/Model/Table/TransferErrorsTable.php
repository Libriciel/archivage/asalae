<?php

/**
 * Asalae\Model\Table\TransferErrorsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;

/**
 * Table transfer_errors
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TransferErrorsTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Transfers');
    }
}
