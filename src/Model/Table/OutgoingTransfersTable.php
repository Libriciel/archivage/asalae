<?php

/**
 * Asalae\Model\Table\OutgoingTransfersTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\StateChangeInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateInvalidTimeZoneException;
use DateTime;
use DOMException;
use Exception;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table outgoing_transfers
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @mixin StateMachineBehavior
 * @mixin TimestampBehavior
 * @property ArchiveUnitsTable|BelongsTo             ArchiveUnits
 * @property OutgoingTransferRequestsTable|BelongsTo OutgoingTransferRequests
 */
class OutgoingTransfersTable extends Table implements StateChangeInterface, AfterDeleteInterface
{
    public const string S_ACCEPTED = 'accepted';
    public const string S_CREATING = 'creating';
    public const string S_DELETED = 'deleted';
    public const string S_ERROR = 'error';
    public const string S_READY_TO_SEND = 'ready_to_send';
    public const string S_RECEIVED = 'received';
    public const string S_REJECTED = 'rejected';
    public const string S_SENT = 'sent';

    public const string T_ACCEPT = 'accept';
    public const string T_ACKNOWLEDGE = 'acknowledge';
    public const string T_CREATE = 'create';
    public const string T_DELETE = 'delete';
    public const string T_ERROR = 'error';
    public const string T_REFUSE = 'refuse';
    public const string T_SEND = 'send';

    /**
     * @var string Etat initial
     */
    public $initialState = self::S_CREATING;

    /**
     * @var array Changement d'êtats selon action
     */
    public $transitions = [
        self::T_CREATE => [
            self::S_CREATING => self::S_READY_TO_SEND,
        ],
        self::T_SEND => [
            self::S_READY_TO_SEND => self::S_SENT,
            self::S_ERROR => self::S_SENT,
        ],
        self::T_ACKNOWLEDGE => [
            self::S_SENT => self::S_RECEIVED,
        ],
        self::T_ACCEPT => [
            self::S_RECEIVED => self::S_ACCEPTED,
        ],
        self::T_REFUSE => [
            self::S_RECEIVED => self::S_REJECTED,
        ],
        self::T_ERROR => [
            self::S_READY_TO_SEND => self::S_ERROR,
        ],
        self::T_DELETE => [
            self::S_ERROR => self::S_DELETED,
        ],
    ];

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    self::S_ACCEPTED,
                    self::S_CREATING,
                    self::S_DELETED,
                    self::S_ERROR,
                    self::S_READY_TO_SEND,
                    self::S_RECEIVED,
                    self::S_REJECTED,
                    self::S_SENT,
                ],
            ]
        );

        $this->belongsTo('OutgoingTransferRequests')
            ->setJoinType('INNER');
        $this->belongsTo('ArchiveUnits')
            ->setJoinType('INNER');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier');

        $validator
            ->scalar('comment')
            ->allowEmptyString('comment');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->dateTime('last_state_update')
            ->allowEmptyDateTime('last_state_update');

        $validator
            ->scalar('states_history')
            ->allowEmptyString('states_history');

        $validator
            ->scalar('error_msg')
            ->allowEmptyFile('error_msg');

        return $validator;
    }

    /**
     * Appelé après la transition
     * @param EntityInterface $entity
     * @param string          $state
     * @throws DOMException
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    public function onStateChange(EntityInterface $entity, string $state)
    {
        if ($state === self::S_ACCEPTED) {
            $transferRequest = $this->OutgoingTransferRequests->find()
                ->where(['OutgoingTransferRequests.id' => $entity->get('outgoing_transfer_request_id')])
                ->contain(['ArchivingSystems'])
                ->firstOrFail();
            $sae = $transferRequest->get('archiving_system');
            $archiveUnit = $this->ArchiveUnits->find()
                ->where(['ArchiveUnits.id' => $entity->get('archive_unit_id')])
                ->contain(
                    [
                        'Archives' => [
                            'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        ],
                    ]
                )
                ->firstOrFail();
            $archive = $archiveUnit->get('archive');
            /** @var EventLogsTable $EventLogs */
            $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
            $entry = $EventLogs->newEntry(
                'outgoing_transfer_transfer',
                'success',
                __(
                    "Transfert de l'archive ''{0}'' vers le SAE ''{1}''",
                    $archive->get('archival_agency_identifier'),
                    $sae->get('name')
                ),
                $transferRequest->get('created_user_id'),
                $archive
            );
            $entry->set('in_lifecycle', false);
            $EventLogs->saveOrFail($entry);
            $storedFile = Hash::get(
                $archive,
                'lifecycle_xml_archive_file.stored_file',
                []
            );
            $this->ArchiveUnits->Archives->updateLifecycleXml(
                $storedFile,
                $entry,
                true
            );
        } elseif ($state === self::S_REJECTED) {
            $archiveUnit = $this->ArchiveUnits->find()
                ->where(['ArchiveUnits.id' => $entity->get('archive_unit_id')])
                ->contain(['Archives'])
                ->firstOrFail();
            $archive = $archiveUnit->get('archive');
            $this->ArchiveUnits->Archives->transition(
                $archive,
                ArchivesTable::T_CANCEL
            );
            $this->ArchiveUnits->Archives->saveOrFail($archive);
            $this->ArchiveUnits->transitionAll(
                ArchiveUnitsTable::T_CANCEL,
                [
                    'lft >=' => $archiveUnit->get('lft'),
                    'rght <=' => $archiveUnit->get('rght'),
                ]
            );
        }

        if ($state === self::S_ACCEPTED || $state === self::S_REJECTED) {
            $this->saveIndicators($entity, $state);
        }
    }

    /**
     * Indicateurs pour transfers sortants au moment de leur acceptation / refus par le SAE distant
     * @param EntityInterface $entity
     * @param string          $state
     * @return void
     * @throws Exception
     */
    protected function saveIndicators(EntityInterface $entity, string $state): void
    {
        $transferRequest = $this->OutgoingTransferRequests->find()
            ->where(['OutgoingTransferRequests.id' => $entity->get('outgoing_transfer_request_id')])
            ->firstOrFail();
        $date = new DateTime();
        $statesHistory = json_decode($transferRequest->get('states_history'), true);
        foreach ($statesHistory as $history) {
            if ($history['state'] === OutgoingTransferRequestsTable::S_VALIDATING) {
                $date = new DateTime($history['date']);
                break;
            }
        }
        $q = $this->query();
        $data = $this->ArchiveUnits->find()
            ->select(
                [
                    'transferring_agency_id' => 'Archives.transferring_agency_id',
                    'originating_agency_id' => 'Archives.originating_agency_id',
                    'agreement_id' => 'Archives.agreement_id',
                    'profile_id' => 'Archives.profile_id',
                    'original_files_count' => $q->func()->sum('ArchiveUnits.original_total_count'),
                    'original_files_size' => $q->func()->sum('ArchiveUnits.original_total_size'),
                ]
            )
            ->innerJoinWith('Archives')
            ->where(['ArchiveUnits.id' => $entity->get('archive_unit_id')])
            ->groupBy(
                [
                    'Archives.transferring_agency_id',
                    'Archives.originating_agency_id',
                    'Archives.agreement_id',
                    'Archives.profile_id',
                ]
            )
            ->disableHydration()
            ->toArray();

        // récupère la durée en secondes depuis l'envoi
        $int = $date->diff(new DateTime());
        $duration = (new DateTime('@0'))->add($int)->getTimestamp();

        $commons = [
            'message_date' => $date,
            'message_type' => MessageIndicatorsTable::OUT_TRANSFER,
            'message_count' => 1,
            'accepted' => $state === self::S_ACCEPTED,
            'derogation' => false,
            'validation_duration' => $duration,
            'archival_agency_id' => $transferRequest->get('archival_agency_id'),
        ];
        $MessageIndicators = TableRegistry::getTableLocator()->get('MessageIndicators');
        foreach ($data as $values) {
            $values = array_map('intval', array_filter($values)); // sum = float
            $indic = $MessageIndicators->newEntity($values + $commons);
            $MessageIndicators->saveOrFail($indic);
        }
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $unit = $ArchiveUnits->get($entity->get('archive_unit_id'));
        $otrId = $entity->get('outgoing_transfer_request_id');
        $ArchiveUnits->transitionAll(
            ArchiveUnitsTable::T_CANCEL,
            ['lft >=' => $unit->get('lft'), 'rght <=' => $unit->get('rght')]
        );
        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $Archives->transitionAll(
            ArchivesTable::T_CANCEL,
            ['id' => $unit->get('archive_id')]
        );

        $links = TableRegistry::getTableLocator()->get(
            'ArchiveUnitsOutgoingTransferRequests'
        );
        $links->deleteAll(
            [
                'archive_unit_id' => $entity->get('archive_unit_id'),
                'otr_id' => $otrId,
            ]
        );
        $count = $links->find()->where(['otr_id' => $otrId])->count();
        if ($count === 0) {
            /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
            $OutgoingTransferRequests = TableRegistry::getTableLocator()->get(
                'OutgoingTransferRequests'
            );
            $OutgoingTransferRequests->transitionAll(
                OutgoingTransferRequestsTable::T_DELETE_ALL_TRANSFERS,
                ['id' => $otrId]
            );
        } else {
            $OutgoingTransfers = TableRegistry::getTableLocator()->get(
                'OutgoingTransfers'
            );
            $creatings = $OutgoingTransfers->find()
                ->where(
                    [
                        'state IN' => [
                            OutgoingTransfersTable::S_CREATING,
                            OutgoingTransfersTable::S_READY_TO_SEND,
                            OutgoingTransfersTable::S_ERROR,
                        ],
                        'outgoing_transfer_request_id' => $otrId,
                    ]
                )
                ->count();
            if ($creatings === 0) {
                /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
                $OutgoingTransferRequests = TableRegistry::getTableLocator()
                    ->get('OutgoingTransferRequests');
                $OutgoingTransferRequests->transitionAll(
                    OutgoingTransferRequestsTable::T_SEND_TRANSFERS,
                    ['id' => $otrId]
                );
            }
        }
    }
}
