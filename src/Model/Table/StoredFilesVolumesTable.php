<?php

/**
 * Asalae\Model\Table\StoredFilesVolumesTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;

/**
 * Table stored_files_volumes
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class StoredFilesVolumesTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsTo('Volumes');
        $this->belongsTo('StoredFiles');

        parent::initialize($config);
    }
}
