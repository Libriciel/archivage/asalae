<?php

/**
 * Asalae\Model\Table\AppraisalRulesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\BeforeSaveInterface;
use AsalaeCore\ORM\Table as CoreTable;
use Cake\Event\Event;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateInterval;
use DateTimeInterface;
use Exception;

/**
 * Table appraisal_rules
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 */
class AppraisalRulesTable extends CoreTable implements BeforeSaveInterface
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'final_action_code' => [
                    'keep',
                    'destroy',
                ],
            ]
        );

        $this->belongsTo('AppraisalRuleCodes');
        $this->hasMany('ArchiveUnits');
        $this->hasMany('Archives');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('final_action_code')
            ->maxLength('final_action_code', 255)
            ->requirePresence('final_action_code', 'create')
            ->notEmptyString('final_action_code');

        $validator
            ->requirePresence('start_date', 'create')
            ->notEmptyDate('start_date', false);

        $validator
            ->date('end_date')
            ->requirePresence('end_date', 'create')
            ->allowEmptyDate('end_date');

        return $validator;
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $date = $entity->get('start_date');
        if (!($date instanceof DateTimeInterface || $date instanceof CakeDate)) {
            $date = CakeDateTime::parseDate($date);
        }
        $dirty = $entity->isDirty('appraisal_rule_code_id')
            || $entity->isDirty('start_date');
        if (
            !($date instanceof DateTimeInterface || $date instanceof CakeDate) || $entity->isDirty(
                'end_date'
            ) || !$dirty
        ) {
            return;
        }
        if ($entity->isDirty('appraisal_rule_code_id')) {
            $entity->unset('appraisal_rule_code');
        }
        if (!$duration = Hash::get($entity, 'appraisal_rule_code.duration')) {
            $appraisal = TableRegistry::getTableLocator()->get(
                'AppraisalRuleCodes'
            )->find()
                ->select(['duration'])
                ->where(['id' => $entity->get('appraisal_rule_code_id')])
                ->disableHydration()
                ->first();
            if (!$appraisal) {
                return;
            }
            $duration = $appraisal['duration'];
        }
        $endDate = (clone $date)->add(new DateInterval($duration));
        $entity->set('end_date', $endDate);
    }
}
