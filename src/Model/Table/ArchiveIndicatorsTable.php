<?php

/**
 * Asalae\Model\Table\ArchiveIndicatorsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table archive_indicators
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveIndicatorsTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('OrgEntities')
            ->setForeignKey('archival_agency_id');
        $this->belongsTo('OrgEntities')
            ->setForeignKey('transferring_agency_id');
        $this->belongsTo('OrgEntities')
            ->setForeignKey('originating_agency_id');
        $this->belongsTo('Agreements');
        $this->belongsTo('Profiles');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->date('archive_date')
            ->requirePresence('archive_date', 'create')
            ->notEmptyDate('archive_date');

        $validator
            ->integer('archive_count')
            ->requirePresence('archive_count', 'create')
            ->notEmptyString('archive_count');

        $validator
            ->integer('units_count')
            ->requirePresence('units_count', 'create')
            ->notEmptyString('units_count');

        $validator
            ->integer('original_count')
            ->requirePresence('original_count', 'create')
            ->notEmptyString('original_count');

        $validator
            ->requirePresence('original_size', 'create')
            ->notEmptyString('original_size');

        $validator
            ->integer('timestamp_count')
            ->allowEmptyString('timestamp_count');

        $validator
            ->allowEmptyString('timestamp_size');

        $validator
            ->integer('preservation_count')
            ->allowEmptyString('preservation_count');

        $validator
            ->allowEmptyString('preservation_size');

        $validator
            ->integer('dissemination_count')
            ->allowEmptyString('dissemination_count');

        $validator
            ->allowEmptyString('dissemination_size');

        return $validator;
    }
}
