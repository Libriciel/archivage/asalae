<?php

/**
 * Asalae\Model\Table\ArchiveUnitsDestructionRequestsTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table archive_units_destruction_requests
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property DestructionRequestsTable|BelongsTo DestructionRequests
 */
class ArchiveUnitsDestructionRequestsTable extends Table implements AfterDeleteInterface
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('ArchiveUnits');
        $this->belongsTo('DestructionRequests');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $destructionRequest = $this->DestructionRequests->get(
            $entity->get('destruction_request_id')
        );
        $query = $this->DestructionRequests->find();
        $counts = $query->select(
            [
                'archive_units_count' => $query->func()->count('*'),
                'original_count' => $query->func()->sum('original_total_count'),
                'original_size' => $query->func()->sum('original_total_size'),
            ]
        )
            ->innerJoinWith('ArchiveUnitsDestructionRequests')
            ->innerJoinWith('ArchiveUnitsDestructionRequests.ArchiveUnits')
            ->where(
                [
                    'DestructionRequests.id' => $entity->get(
                        'destruction_request_id'
                    ),
                ]
            )
            ->disableHydration()
            ->all()
            ->first();
        $this->DestructionRequests->patchEntity($destructionRequest, $counts);
        $this->DestructionRequests->save($destructionRequest);
    }
}
