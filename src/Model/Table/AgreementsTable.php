<?php

/**
 * Asalae\Model\Table\AgreementsTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Form\MessageSchema\MessageSchema;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\Model\Table\AgreementsTable as CoreAgreementsTable;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Model\Table\BeforeSaveInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;

/**
 * Table Agreements
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @mixin TimestampBehavior
 */
class AgreementsTable extends CoreAgreementsTable implements
    AfterDeleteInterface,
    AfterSaveInterface,
    BeforeDeleteInterface,
    BeforeSaveInterface,
    SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->hasMany('Archives');
        $this->hasMany('Transfers');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('identifier');
        $validator->notEmptyString('name');
        $validator->notEmptyString('org_entity_id');
        $validator->notEmptyString('improper_chain_id');
        $validator->allowEmptyString(
            'proper_chain_id',
            __("Il faut choisir un circuit de validation des transferts conformes"),
            function ($context) {
                return !empty($context['data']['auto_validate']);
            }
        );
        $validator->add(
            'allow_all_transferring_agencies',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if (
                        empty($context['data']['transferring_agencies'])
                        || !empty($context['data']['transferring_agencies']['_ids'])
                    ) {
                        return true;
                    }
                    return (bool)$value;
                },
                'message' => __("Il faut choisir un ou plusieurs services versants"),
            ]
        );
        $validator->add(
            'allow_all_originating_agencies',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if (
                        empty($context['data']['originating_agencies'])
                        || !empty($context['data']['originating_agencies']['_ids'])
                    ) {
                        return true;
                    }
                    return (bool)$value;
                },
                'message' => __("Il faut choisir un ou plusieurs services producteurs"),
            ]
        );
        $validator->add(
            'allow_all_service_levels',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if (
                        empty($context['data']['service_levels'])
                        || !empty($context['data']['service_levels']['_ids'])
                    ) {
                        return true;
                    }
                    return (bool)$value;
                },
                'message' => __("Il faut choisir un ou plusieurs niveaux de service"),
            ]
        );
        $validator->add(
            'allow_all_profiles',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if (
                        empty($context['data']['profiles'])
                        || !empty($context['data']['profiles']['_ids'])
                    ) {
                        return true;
                    }
                    return (bool)$value;
                },
                'message' => __("Il faut choisir un ou plusieurs profils d'archives"),
            ]
        );
        $validator->add(
            'identifier',
            [
                'unique' => [
                    'rule' => ['validateUnique', ['scope' => 'org_entity_id']],
                    'provider' => 'table',
                    'message' => __("Cet identifiant est déjà utilisé"),
                ],
            ]
        );

        $validator->add(
            'default_agreement',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if ($value || empty($context['data']['org_entity_id'])) {
                        return true;
                    }
                    /** @var Table $table */
                    $table = $context['providers']['table'];
                    $conditions = [
                        'org_entity_id' => Hash::get($context, 'data.org_entity_id'),
                        'default_agreement IS' => true,
                    ];
                    if (!empty($id = Hash::get($context, 'data.id'))) {
                        $conditions['id !='] = $id;
                    }
                    $count = $table->find()
                        ->where($conditions)
                        ->count();
                    return $count > 0;
                },
                'message' => __("Il doit y avoir un accord de versement par défaut"),
            ]
        );

        $validator->add(
            'default_agreement',
            'active_and_default',
            [
                'rule' => function ($value, $context) {
                    return !$value || ($context['data']['active'] ?? true);
                },
                'message' => __("Un accord de versement par défaut doit être actif"),
            ]
        );

        $validator->add(
            'proper_chain_id',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if (empty($value)) {
                        return true;
                    }
                    $options = $this->findListProperChains($context['data']['org_entity_id']);
                    return isset($options->toArray()[(int)$value]);
                },
                'message' => __("Ce circuit de validation des transferts conformes n'est pas disponible"),
            ]
        );
        $validator->add(
            'improper_chain_id',
            'custom',
            [
                'rule' => function ($value, $context) {
                    $options = $this->findListImproperChains($context['data']['org_entity_id']);
                    return isset($options->toArray()[(int)$value]);
                },
                'message' => __("Ce circuit de validation des transferts non conformes n'est pas disponible"),
            ]
        );

        return $validator;
    }

    /**
     * Donne les options actives de type conformes pour un service d'archive
     * @param int $org_entity_id
     * @return Query
     */
    public function findListProperChains($org_entity_id): Query
    {
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = TableRegistry::getTableLocator()->get('ValidationChains');
        return $ValidationChains->findListOptions(
            $org_entity_id,
            ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM
        );
    }

    /**
     * Donne les options actives de type conformes pour un service d'archive
     * @param int $org_entity_id
     * @return Query
     */
    public function findListImproperChains($org_entity_id): Query
    {
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = TableRegistry::getTableLocator()->get('ValidationChains');
        return $ValidationChains->findListOptions(
            $org_entity_id,
            ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM
        );
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        if (
            in_array(
                'default_agreement',
                $entity->get('_dirty')
            ) && $entity->get('default_agreement')
        ) {
            $this->updateAll(
                ['default_agreement' => false],
                [
                    'id !=' => $entity->get('id'),
                    'org_entity_id' => $entity->get('org_entity_id'),
                ]
            );
        }
        if (
            $entity->isNew() || $entity->isDirty('name') || $entity->isDirty(
                'identifier'
            )
        ) {
            MessageSchema::clearCache($entity->get('org_entity_id'));
        }
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        if (
            $entity->getOriginal('identifier') !== $entity->get('identifier')
            && !$entity->get('editable_identifier')
        ) {
            throw new Exception("Agreement identifier is not editable");
        }

        $entity->set('_dirty', $entity->getDirty());
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable') && !$entity->get(
                    'default_agreement'
                );
            }
        );
        return parent::buildRules($rules);
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        MessageSchema::clearCache($entity->get('org_entity_id'));
    }
}
