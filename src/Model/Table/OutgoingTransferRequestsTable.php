<?php

/**
 * Asalae\Model\Table\OutgoingTransferRequestsTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Exception;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table outgoing_transfer_requests
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @mixin StateMachineBehavior
 * @mixin TimestampBehavior
 */
class OutgoingTransferRequestsTable extends Table implements
    BeforeDeleteInterface,
    SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    public const string S_ACCEPTED = 'accepted';
    public const string S_ALL_TRANSFERS_REJECTED = 'all_transfers_rejected';
    public const string S_ARCHIVE_DESCRIPTION_DELETING = 'archive_description_deleting';
    public const string S_ARCHIVE_DESTRUCTION_SCHEDULED = 'archive_destruction_scheduled';
    public const string S_ARCHIVE_FILES_DESTROYING = 'archive_files_destroying';
    public const string S_CREATING = 'creating';
    public const string S_FINISHED = 'finished';
    public const string S_REJECTED = 'rejected';
    public const string S_TRANSFERS_PROCESSED = 'transfers_processed';
    public const string S_TRANSFERS_RECEIVED = 'transfers_received';
    public const string S_TRANSFERS_SENT = 'transfers_sent';
    public const string S_TRANSFERS_ERRORS = 'transfers_errors';
    public const string S_VALIDATING = 'validating';
    public const string S_ALL_TRANSFERS_DELETED = 'all_transfers_deleted';

    public const string T_SEND = 'send';
    public const string T_REFUSE = 'refuse';
    public const string T_ACCEPT = 'accept';
    public const string T_SEND_TRANSFERS = 'send_transfers';
    public const string T_ACKNOWLEDGE_TRANSFERS = 'acknowledge_transfers';
    public const string T_PROCESS_TRANSFERS = 'process_transfers';
    public const string T_SCHEDULE_ARCHIVE_DESTRUCTION = 'schedule_archive_destruction';
    public const string T_CANCEL_ARCHIVE_DESTRUCTION = 'cancel_archive_destruction';
    public const string T_DESTROY_ARCHIVE_FILES = 'destroy_archive_files';
    public const string T_DELETE_ARCHIVE_DESCRIPTION = 'delete_archive_description';
    public const string T_TERMINATE = 'terminate';
    public const string T_REJECT_ALL_TRANSFERS = 'reject_all_transfers';
    public const string T_DELETE_ALL_TRANSFERS = 'delete_all_transfers';
    public const string T_ERROR = 'error';
    public const string T_RESEND = 'resend';

    /**
     * @var string Etat initial
     */
    public $initialState = self::S_CREATING;

    /**
     * @var array Changement d'êtats selon action
     */
    public $transitions = [
        self::T_SEND => [
            self::S_CREATING => self::S_VALIDATING,
        ],
        self::T_REFUSE => [
            self::S_VALIDATING => self::S_REJECTED,
        ],
        self::T_ACCEPT => [
            self::S_VALIDATING => self::S_ACCEPTED,
        ],
        self::T_SEND_TRANSFERS => [
            self::S_ACCEPTED => self::S_TRANSFERS_SENT,
            self::S_TRANSFERS_ERRORS => self::S_TRANSFERS_SENT,
        ],
        self::T_ACKNOWLEDGE_TRANSFERS => [
            self::S_TRANSFERS_SENT => self::S_TRANSFERS_RECEIVED,
        ],
        self::T_PROCESS_TRANSFERS => [
            self::S_TRANSFERS_RECEIVED => self::S_TRANSFERS_PROCESSED,
        ],
        self::T_SCHEDULE_ARCHIVE_DESTRUCTION => [
            self::S_TRANSFERS_PROCESSED => self::S_ARCHIVE_DESTRUCTION_SCHEDULED,
        ],
        self::T_CANCEL_ARCHIVE_DESTRUCTION => [
            self::S_ARCHIVE_DESTRUCTION_SCHEDULED => self::S_TRANSFERS_PROCESSED,
        ],
        self::T_DESTROY_ARCHIVE_FILES => [
            self::S_ARCHIVE_DESTRUCTION_SCHEDULED => self::S_ARCHIVE_FILES_DESTROYING,
        ],
        self::T_DELETE_ARCHIVE_DESCRIPTION => [
            self::S_ARCHIVE_FILES_DESTROYING => self::S_ARCHIVE_DESCRIPTION_DELETING,
        ],
        self::T_TERMINATE => [
            self::S_ARCHIVE_DESCRIPTION_DELETING => self::S_FINISHED,
        ],
        self::T_REJECT_ALL_TRANSFERS => [
            self::S_TRANSFERS_RECEIVED => self::S_ALL_TRANSFERS_REJECTED,
        ],
        self::T_DELETE_ALL_TRANSFERS => [
            self::S_TRANSFERS_ERRORS => self::S_ALL_TRANSFERS_DELETED,
        ],
        self::T_ERROR => [
            self::S_ACCEPTED => self::S_TRANSFERS_ERRORS,
        ],
        self::T_RESEND => [
            self::S_TRANSFERS_ERRORS => self::S_ACCEPTED,
        ],
    ];

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    self::S_ACCEPTED,
                    self::S_ALL_TRANSFERS_REJECTED,
                    self::S_ARCHIVE_DESCRIPTION_DELETING,
                    self::S_ARCHIVE_DESTRUCTION_SCHEDULED,
                    self::S_ARCHIVE_FILES_DESTROYING,
                    self::S_CREATING,
                    self::S_FINISHED,
                    self::S_REJECTED,
                    self::S_TRANSFERS_PROCESSED,
                    self::S_TRANSFERS_RECEIVED,
                    self::S_TRANSFERS_SENT,
                    self::S_VALIDATING,
                ],
            ]
        );

        $this->belongsTo('ArchivalAgencies')
            ->setClassName('OrgEntities')
            ->setJoinType('INNER');
        $this->belongsTo('ArchivingSystems')
            ->setJoinType('INNER');
        $this->belongsTo('CreatedUsers')
            ->setClassName('Users')
            ->setJoinType('INNER');
        $this->belongsToMany('ArchiveUnits')
            ->setForeignKey('otr_id');
        $this->hasMany('ValidationProcesses')
            ->setForeignKey('app_foreign_key')
            ->setConditions(
                ['ValidationProcesses.app_subject_type' => 'OutgoingTransferRequests']
            );
        $this->hasMany('OutgoingTransfers');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier');

        $validator
            ->scalar('comment')
            ->allowEmptyString('comment');

        $validator
            ->scalar('archival_agency_identifier')
            ->maxLength('archival_agency_identifier', 255)
            ->requirePresence('archival_agency_identifier', 'create')
            ->notEmptyString('archival_agency_identifier');

        $validator
            ->scalar('archival_agency_name')
            ->maxLength('archival_agency_name', 255)
            ->requirePresence('archival_agency_name', 'create')
            ->notEmptyString('archival_agency_name');

        $validator
            ->scalar('transferring_agency_identifier')
            ->maxLength('transferring_agency_identifier', 255)
            ->allowEmptyString('transferring_agency_identifier');

        $validator
            ->scalar('transferring_agency_name')
            ->maxLength('transferring_agency_name', 255)
            ->allowEmptyString('transferring_agency_name');

        $validator
            ->scalar('agreement_identifier')
            ->maxLength('agreement_identifier', 255)
            ->allowEmptyString('agreement_identifier');

        $validator
            ->scalar('profile_identifier')
            ->maxLength('profile_identifier', 255)
            ->allowEmptyFile('profile_identifier');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->dateTime('last_state_update')
            ->allowEmptyDateTime('last_state_update');

        $validator
            ->scalar('states_history')
            ->maxLength('states_history', 255)
            ->allowEmptyString('states_history');

        $validator
            ->integer('archives_count')
            ->requirePresence('archives_count', 'create')
            ->notEmptyString('archives_count');

        $validator
            ->integer('archive_units_count')
            ->requirePresence('archive_units_count', 'create')
            ->notEmptyString('archive_units_count');

        $validator
            ->integer('original_count')
            ->requirePresence('original_count', 'create')
            ->notEmptyString('original_count');

        $validator
            ->requirePresence('original_size', 'create')
            ->notEmptyString('original_size');

        $validator
            ->scalar('originating_agency_identifier')
            ->maxLength('originating_agency_identifier', 255)
            ->allowEmptyString('originating_agency_identifier');

        $validator
            ->scalar('originating_agency_name')
            ->maxLength('originating_agency_name', 255)
            ->allowEmptyString('originating_agency_name');

        return $validator;
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivesTable $Archives */
        $Archives = $loc->get('Archives');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnitsOutgoingTransferRequests = $loc->get('ArchiveUnitsOutgoingTransferRequests');
        $archiveUnits = $ArchiveUnits->find()
            ->innerJoinWith('ArchiveUnitsOutgoingTransferRequests')
            ->where(['otr_id' => $entity->id]);
        $conn = $archiveUnits->getConnection();
        $transaction = $conn->inTransaction() === false;
        try {
            if ($transaction) {
                $conn->begin();
            }
            /** @var EntityInterface $archiveUnit */
            foreach ($archiveUnits as $archiveUnit) {
                $Archives->transitionAll(
                    ArchivesTable::T_CANCEL,
                    ['Archives.id' => $archiveUnit->get('archive_id')]
                );
                $ArchiveUnits->transitionAll(
                    ArchiveUnitsTable::T_CANCEL,
                    ['ArchiveUnits.id' => $archiveUnit->id]
                );
            }
            $ArchiveUnitsOutgoingTransferRequests->deleteAll(
                ['otr_id' => $entity->id]
            );
            if ($transaction) {
                $conn->commit();
            }
        } catch (Exception $e) {
            if ($transaction) {
                $conn->rollback();
            }
            $event->stopPropagation();
            throw $e;
        }

        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }
}
