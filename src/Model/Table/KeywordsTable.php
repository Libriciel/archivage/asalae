<?php

/**
 * Asalae\Model\Table\KeywordsTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Table\KeywordsTable as CoreKeywordsTable;
use AsalaeCore\Utility\LimitBreak;
use Cake\I18n\I18n;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Behavior\TreeBehavior;
use Cake\Utility\Hash;
use Cake\Utility\Xml;
use ForceUTF8\Encoding;

/**
 * Table keywords
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin TreeBehavior
 */
class KeywordsTable extends CoreKeywordsTable
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');

        $this->belongsTo('KeywordLists');
        $this->belongsTo('ParentKeywords')
            ->setClassName('Keywords')
            ->setForeignKey('parent_id');
    }

    /**
     * Importe un fichier en skos
     * @param string $rdf contenu du fichier rdf (fichier skos)
     * @return array entities
     */
    public function importSkosArray(string $rdf): array
    {
        $xml = Xml::build($rdf);
        $data = Xml::toArray($xml);
        $entities = [];
        $langFull = str_replace('_', '-', strtolower(I18n::getLocale()));
        $lang = explode('-', $langFull)[0];
        foreach (Hash::extract($data, '{s}.skos:Concept')[0] as $concept) {
            if (empty($concept['skos:prefLabel'])) {
                continue;
            }
            $codes = array_filter(
                array_keys($concept),
                function ($v) {
                    return str_contains($v, 'identifier');
                }
            );
            $name = $this->getSkosLabel($concept, array_unique([$langFull, $lang, 'en', 'fr', 'es', 'it', 'de']));
            if (empty($name)) {
                continue;
            }
            $code = !empty($codes) ? $concept[current($codes)] : $this->genCode($name);
            $entities[] = [$code, $name];
        }
        return $entities;
    }

    /**
     * Importe un fichier en csv
     *
     * $params = [
     *  'separator' => ',',   // Séparateur de colonnes
     *  'delimiter' => '"',   // Délimiteur de texte
     *  'way' => 'row',       // row = un mot clé par ligne, col = un mot clé par colonne
     *  'pos_label_col' => 1, // Numéro de colonne du 1er label
     *  'pos_label_row' => 1, // Numéro de ligne du 1er label
     *  'auto_code' => false, // Défini le code à partir du nom si à true
     *  'pos_code_col' => 1,  // Numéro de colonne du 1er code
     *  'pos_code_row' => 1,  // Numéro de ligne du 1er code
     * ]
     *
     * @param resource $file
     * @param array    $params
     * @return array $entities
     */
    public function importCsvArray($file, array $params = []): array
    {
        $size = fstat($file)['size'];
        LimitBreak::setTimeLimit($size / 10000); // 1s pour 10ko
        $params += [
            'separator' => ',',
            'delimiter' => '"',
            'way' => 'row',
            'pos_label_col' => 1,
            'pos_label_row' => 1,
            'auto_code' => false,
            'pos_code_col' => 2,
            'pos_code_row' => 1,
        ];
        $params['pos_label_col'] = (int)$params['pos_label_col'];
        $params['pos_label_row'] = (int)$params['pos_label_row'];
        $params['auto_code'] = (bool)$params['auto_code'];
        $params['pos_code_col'] = (int)$params['pos_code_col'];
        $params['pos_code_row'] = (int)$params['pos_code_row'];
        rewind($file);
        $row = 1;
        $labels = [];
        $codes = [];
        while (($data = fgetcsv($file, 0, $params['separator'], $params['delimiter'])) !== false) {
            if ($params['way'] === 'row') {
                if ($row >= $params['pos_label_row']) {
                    $labels[] = Encoding::toUTF8($data[$params['pos_label_col'] - 1] ?? '');
                }
                if (!$params['auto_code'] && $row >= $params['pos_code_row']) {
                    $codes[] = Encoding::toUTF8($data[$params['pos_code_col'] - 1] ?? '');
                }
            } else {
                $count = count($data);
                if ($row === $params['pos_label_row']) {
                    for ($i = $params['pos_label_col'] - 1; $i < $count; $i++) {
                        $labels[] = Encoding::toUTF8($data[$i]);
                    }
                }
                if ($row === $params['pos_code_row']) {
                    for ($i = $params['pos_code_col'] - 1; $i < $count; $i++) {
                        $codes[] = Encoding::toUTF8($data[$i]);
                    }
                }
            }
            $row++;
        }
        $entities = [];
        foreach ($labels as $key => $name) {
            if (empty($name)) {
                continue;
            }
            $code = $params['auto_code'] || empty($codes[$key])
                ? $this->genCode($name)
                : $codes[$key];
            $entities[] = [$code, $name];
        }
        return $entities;
    }
}
