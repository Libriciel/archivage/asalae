<?php

/**
 * Asalae\Model\Table\ArchiveBinariesTechnicalArchiveUnitsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;

/**
 * Table archive_binaries_technical_archive_units
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveBinariesTechnicalArchiveUnitsTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('ArchiveBinaries');
        $this->belongsTo('TechnicalArchiveUnits');
    }
}
