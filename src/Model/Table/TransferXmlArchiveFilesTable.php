<?php

/**
 * Asalae\Model\Table\TransferXmlArchiveFilesTable
 */

namespace Asalae\Model\Table;

/**
 * Table archive_files (xxx_transfer.xml)
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TransferXmlArchiveFilesTable extends ArchiveFilesTable
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('archive_files');
        $this->belongsTo('Archives')
            ->setForeignKey('archive_id')
            ->setConditions(['TransferXmlArchiveFiles.type' => ArchiveFilesTable::TYPE_TRANSFER]);
        $this->belongsTo('StoredFiles');
    }
}
