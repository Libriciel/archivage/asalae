<?php

/**
 * Asalae\Model\Table\FileuploadsProfilesTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;

/**
 * Table fileuploads_profiles
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FileuploadsProfilesTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Fileuploads');
        $this->belongsTo('Profiles');
    }
}
