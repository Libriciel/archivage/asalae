<?php

/**
 * Asalae\Model\Table\ValidationHistoriesTable
 */

namespace Asalae\Model\Table;

use Asalae\Model\Entity\TransferError;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use Cake\Database\Expression\IdentifierExpression;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;

/**
 * Table validation_histories
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin OptionsBehavior
 */
class ValidationHistoriesTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'action' => ['validate', 'invalidate', 'stepback'],
            ]
        );

        $this->belongsTo('ValidationProcesses');
        $this->belongsTo('ValidationActors');

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     * @throws Exception
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('action')
            ->maxLength('action', 255)
            ->requirePresence('action', 'create')
            ->notEmptyString('action')
            ->inList(
                'action',
                $this->getBehavior('Options')->getConfig('action')
            )
            ->add(
                'action',
                'prev_check',
                [
                    'rule' => function ($value, $context) {
                        if ($value !== 'stepback') {
                            return true;
                        }
                        if (!isset($context['data']['prev'])) {
                            $context['data']['prev'] = (bool)$this->find()
                                ->select(['ValidationStages.id'])
                                ->innerJoinWith('ValidationActors')
                                ->innerJoinWith(
                                    'ValidationActors.ValidationStages'
                                )
                                ->innerJoinWith('ValidationChains')
                                ->innerJoin(
                                    ['stages' => 'validation_stages'],
                                    [
                                        'stages.validation_chain_id'
                                        => new IdentifierExpression(
                                            'ValidationChains.id'
                                        ),
                                    ]
                                )
                                ->where(
                                    [
                                        'ValidationStages.ord >'
                                        => new IdentifierExpression(
                                            'stages.ord'
                                        ),
                                    ]
                                )
                                ->limit(1);
                        }
                        return $context['data']['prev'];
                    },
                    'message' => __("Il n'y a pas d'étape précédente"),
                ]
            )
            ->add(
                'action',
                'validate_check',
                [
                    'rule' => function ($value, $context) {
                        if (
                            $value === 'validate' && Hash::get(
                                $context,
                                'data.next'
                            ) === false
                        ) {
                            $process_id = Hash::get(
                                $context,
                                'data.validation_process_id'
                            );
                            return TableRegistry::getTableLocator()
                                    ->get('ValidationProcesses')
                                    ->find()
                                    ->select(['ValidationProcesses.id'])
                                    ->innerJoinWith('Transfers')
                                    ->innerJoinWith('Transfers.TransferErrors')
                                    ->where(
                                        [
                                            'ValidationProcesses.id' => $process_id,
                                            'TransferErrors.code IN' => TransferError::INVALIDATE_CODES,
                                        ]
                                    )
                                    ->limit(1)
                                    ->count() === 0;
                        }
                        return true;
                    },
                    'message' => __(
                        "La validation n'est pas possible pour ce transfert"
                    ),
                ]
            );

        $validator
            ->scalar('comment')
            ->requirePresence('comment', 'create')
            ->allowEmptyString('comment');

        $validator
            ->scalar('app_meta')
            ->allowEmptyString('app_meta');

        $validator
            ->integer('current_step')
            ->requirePresence('current_step', 'create')
            ->notEmptyString('current_step');

        $needSignature = function ($context) {
            return Hash::get($context, 'data.type_validation') === 'S';
        };
        $validator
            ->notEmptyString('signature', null, $needSignature)
            ->requirePresence('signature', $needSignature);

        return $validator;
    }

    /**
     * options conditionnelles pour le champ action
     * @param bool $prev
     * @param bool $allowedValidate
     * @return array
     * @throws Exception
     */
    public function optionsAction(bool $prev, bool $allowedValidate = true)
    {
        $options = $this->options('action');
        if (!$prev) {
            unset($options['stepback']);
        }
        if (!$allowedValidate) {
            unset($options['validate']);
        }
        return $options;
    }
}
