<?php

/**
 * Asalae\Model\Table\PublicDescriptionArchiveFilesTable
 */

namespace Asalae\Model\Table;

use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Utility\DOMUtility;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use DateTime;
use DateTimeInterface;
use DOMElement;

/**
 * Table archive_files (xxx_pubdesc.xml) pour la description publique
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class PublicDescriptionArchiveFilesTable extends ArchiveFilesTable
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('archive_files');
        $this->belongsTo('Archives')
            ->setForeignKey('archive_id')
            ->setConditions(
                ['PublicDescriptionArchiveFiles.type' => ArchiveFilesTable::TYPE_PUBLIC_DESCRIPTION]
            );
        $this->belongsTo('StoredFiles');
    }

    /**
     * Créer un archive_file de type pubdesc
     * @param DescriptionXmlArchiveFile $description
     * @return EntityInterface
     * @throws VolumeException
     */
    public function createFromDescriptionXml(
        DescriptionXmlArchiveFile $description
    ): EntityInterface {
        $util = new DOMUtility($description->getDom());
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $today = (new DateTime())->format('Y-m-d');
        $basequery = $ArchiveUnits->find()
            ->where(
                [
                    'ArchiveUnits.archive_id' => $description->get(
                        'archive_id'
                    ),
                    'AccessRules.end_date >' => $today,
                ]
            )
            ->orderBy(['ArchiveUnits.lft'])
            ->contain(
                ['Archives' => ['Transfers']]
            ); // pour calculer xpath (selon version seda)
        $toDelete = [];
        $paths = [];

        /**
         * Retrait des documents d'une archive ou d'un objet d'archives non communicable
         */
        $query = (clone $basequery)
            ->innerJoinWith('AccessRules')
            ->andWhere(['ArchiveUnits.xml_node_tagname LIKE' => 'Document%']);
        foreach ($query as $unit) {
            $path = $this->parentXPath($unit, $paths)
                . '/ns:' . $unit->get('xml_node_tagname');
            $node = $util->xpath->query($path)->item(0);
            if ($node) {
                $toDelete[] = $node;
            } else {
                trigger_error(
                    'node not found: ' . str_replace('/ns:', '/', $path)
                );
            }
        }

        /**
         * Retrait des mots-clés non communicable
         */
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $query = $ArchiveKeywords->find()
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.Archives')
            ->innerJoinWith('AccessRules')
            ->where(
                [
                    'ArchiveUnits.archive_id' => $description->get(
                        'archive_id'
                    ),
                    'AccessRules.end_date >' => $today,
                ]
            )
            ->contain(['ArchiveUnits' => ['Archives' => ['Transfers']]]);

        foreach ($query as $keyword) {
            $unit = $keyword->get('archive_unit');
            $identifier = DOMUtility::xpathQuote($keyword->get('content'));
            $basePath = $this->parentXPath($unit, $paths)
                . '/ns:' . $unit->get(
                    'xml_node_tagname'
                ); // Archive|ArchiveObject|ArchiveUnit;

            $unitNode = $util->xpath->query($basePath)->item(0);
            $contentNode = $unitNode
                ? $util->xpath->query(
                    'ns:ContentDescription|ns:Content',
                    $unitNode
                )->item(0)
                : null;
            $q = sprintf('[normalize-space(.)=%s]', $identifier);
            $node = $contentNode
                ? $util->xpath->query(
                    '(ns:ContentDescriptive|ns:Keyword)/ns:KeywordContent' . $q,
                    $contentNode
                )->item(0)
                : null;
            if ($node) {
                $toDelete[] = $node->parentNode;
            } else {
                if (!$unitNode) {
                    trigger_error(__("{0} not found", $basePath));
                } elseif (!$contentNode) {
                    trigger_error(
                        __("{0} not found", 'ns:ContentDescription|ns:Content')
                    );
                } else {
                    trigger_error(
                        __(
                            "{0} not found",
                            '(ns:ContentDescriptive|ns:Keyword)/ns:KeywordContent' . $q
                        )
                    );
                }
            }
        }

        /**
         * Retrait des descriptions non communicable
         */
        $query = (clone $basequery)
            ->innerJoinWith('ArchiveDescriptions')
            ->innerJoinWith('ArchiveDescriptions.AccessRules');
        foreach ($query as $unit) {
            $basePath = $this->parentXPath($unit, $paths)
                . '/ns:' . $unit->get(
                    'xml_node_tagname'
                ); // Archive|ArchiveObject|ArchiveUnit;

            $unitNode = $util->xpath->query($basePath)->item(0);
            $node = $unitNode
                ? $util->xpath->query(
                    'ns:ContentDescription|ns:Content',
                    $unitNode
                )->item(0)
                : null;
            if ($node) {
                if (in_array($util->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
                    $except = ['Title', 'ArchivalAgencyArchiveUnitIdentifier'];
                    /** @var DOMElement $subnode */
                    foreach ($node->childNodes as $subnode) {
                        if (!in_array($subnode->tagName, $except)) {
                            $toDelete[] = $subnode;
                        }
                    }
                } else {
                    $toDelete[] = $node;
                }
            } else {
                if (!$unitNode) {
                    trigger_error(__("{0} not found", $basePath));
                } else {
                    trigger_error(
                        __("{0} not found", 'ns:ContentDescription|ns:Content')
                    );
                }
            }
            if ($unitNode && in_array($util->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
                $dataObjectReferences = $util->xpath->query(
                    'ns:DataObjectReference',
                    $unitNode
                );
                /** @var DOMElement $subnode */
                foreach ($dataObjectReferences as $subnode) {
                    $toDelete[] = $subnode;
                }
            }
        }
        foreach ($toDelete as $node) {
            if ($node->parentNode && $node->parentNode->childNodes->count() === 1) {
                $node->parentNode->parentNode->removeChild($node->parentNode);
            } elseif ($node->parentNode) {
                $node->parentNode->removeChild($node);
            }
        }

        $Archives = $loc->get('Archives');
        $archive = $Archives->get($description->get('archive_id'));
        return $this->newEntity(
            [
                'archive_id' => $archive->id,
                'type' => 'pubdesc',
                'filename' => $archive->get(
                    'archival_agency_identifier'
                ) . '_pubdesc.xml',
                'content' => $util->dom->saveXML(),
            ],
            ['validate' => false]
        );
    }

    /**
     * Donne le xpath d'un archive_unit
     * @param EntityInterface $unit
     * @param array           $paths
     * @return string
     */
    private function parentXPath(EntityInterface $unit, array &$paths): string
    {
        if (!$unit->get('parent_id')) {
            if (!isset($paths[$unit->id])) {
                $unit->set('context', 'Archive');
                $paths[$unit->id] = $unit->get('xpath');
            }
            return dirname($paths[$unit->id]);
        }
        if (!isset($paths[$unit->get('parent_id')])) {
            $unit->set('context', 'Archive');
            $ppath = dirname($unit->get('xpath'));
            $paths[$unit->get('parent_id')] = $ppath;
        }
        return $paths[$unit->get('parent_id')];
    }

    /**
     * Donne la date de la prochaine mise à jour de la description publique
     * @param int|string $archive_id
     * @return DateTimeInterface|null
     */
    public function nextRebuildDate($archive_id)
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $ArchiveDescriptions = $loc->get('ArchiveDescriptions');
        $condition = [
            'ArchiveUnits.archive_id' => $archive_id,
            'AccessRules.end_date >' => (new DateTime())->format('Y-m-d'),
        ];
        $unit = $ArchiveUnits->find()
            ->select(['end_date' => 'AccessRules.end_date'])
            ->innerJoinWith('AccessRules')
            ->where($condition)
            ->orderBy(['AccessRules.end_date' => 'asc'])
            ->first();
        $keyword = $ArchiveKeywords->find()
            ->select(['end_date' => 'AccessRules.end_date'])
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('AccessRules')
            ->where($condition)
            ->orderBy(['AccessRules.end_date' => 'asc'])
            ->first();
        $description = $ArchiveDescriptions->find()
            ->select(['end_date' => 'AccessRules.end_date'])
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('AccessRules')
            ->where($condition)
            ->orderBy(['AccessRules.end_date' => 'asc'])
            ->first();
        $unit = $unit ? $unit->get('end_date') : null;
        $keyword = $keyword ? $keyword->get('end_date') : null;
        $description = $description ? $description->get('end_date') : null;
        $next = array_filter([$unit, $keyword, $description]);
        return $next ? min($next) : null;
    }
}
