<?php

/**
 * Asalae\Model\Table\VolumesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Driver\Volume\VolumeS3;
use AsalaeCore\Error\ErrorTrap;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterSaveInterface;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Table volumes
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin OptionsBehavior
 * @property StoredFilesVolumesTable StoredFilesVolumes
 */
class VolumesTable extends Table implements AfterSaveInterface, SerializeDeletedEntitiesInterface
{
    /**
     * Traits
     */
    use SerializeDeleteAllTrait;

    public const string TEST_FILENAME = 'test_file.txt';

    /**
     * @var bool Permet de désactiver cette option pour les migrations de test
     */
    public static $skipReadCalculation = false;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'driver' => array_keys(Configure::read('Volumes.drivers')),
            ]
        );

        $this->belongsTo('SecureDataSpaces');
        $this->hasMany('StoredFilesVolumes');
        $this->belongsToMany('StoredFiles');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add(
                'name',
                [
                    'unique' => [
                        'rule' => ['validateUnique'],
                        'provider' => 'table',
                        'message' => __("Ce nom est déjà utilisé"),
                    ],
                ]
            );

        $validator
            ->scalar('fields')
            ->requirePresence(
                'fields',
                function ($context) {
                    if (!$context['newRecord']) {
                        return false;
                    }
                    // requis si nouveau et que les champs virtuels (setters) ne sont pas définis
                    return !isset($context['data']['bucket']) && !isset($context['data']['path']);
                }
            )
            ->add(
                'fields',
                'custom',
                [
                    'rule' => function ($value, $context) {
                        if (empty($context['data']['id'])) {
                            return true;
                        }
                        $entity = $this->get($context['data']['id']);
                        if (!$entity->get('is_used')) {
                            return true; // on peux modifier si le volume n'est pas utilisé
                        }
                        $initial = json_decode($entity->get('fields'), true);
                        $current = json_decode($value, true);

                        $configured = Configure::read(
                            'Volumes.drivers.' . $entity->get('driver') . '.fields'
                        );
                        foreach (array_keys($configured) as $field) {
                            $a = $initial[$field] ?? null;
                            $b = $current[$field] ?? null;
                            if ($a !== $b) {
                                return false;
                            }
                        }
                        return true;
                    },
                    'message' => __("Les champs liés au Driver d'un volume utilisé ne peuvent être modifiés"),
                ]
            )
            ->notEmptyString('fields');

        $validator
            ->scalar('driver')
            ->maxLength('driver', 255)
            ->requirePresence('driver', 'create')
            ->notEmptyString('driver');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->boolean('active')
            ->allowEmptyString('active')
            ->add(
                'active',
                'disablable',
                [
                    'rule' => function ($value, $context) {
                        if ($value || $context['newRecord']) {
                            return true;
                        }
                        $entity = $this->get($context['data']['id']);
                        return $entity->get('removable');
                    },
                    'message' => __(
                        "Impossible de désactiver un volume lié à un Espace de conservation sécurisé"
                    ),
                ]
            );

        $validator
            ->allowEmptyString('disk_usage');

        $validator
            ->allowEmptyString('max_disk_usage');

        return $validator;
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function afterSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        if ($entity->isNew() || !empty($options['update_read_duration'])) {
            $this->calcReadDuration($entity);
        }
    }

    /**
     * Ecrit un fichier de 25Mo sur le volume et mesure la durée de lecture du
     * fichier. Le fichier est ensuite supprimé
     * Met à jour le champ "read_duration" dans la table
     * @param EntityInterface $volume
     * @throws Exception
     */
    public function calcReadDuration(EntityInterface $volume)
    {
        if (empty($volume->id) || self::$skipReadCalculation) {
            return;
        }
        $filename = self::TEST_FILENAME;
        if (!is_file(TMP . $filename)) {
            Filesystem::createDummyFile(TMP . $filename, 25000000); // 25Mo
        }
        $md5 = md5_file(TMP . $filename);
        try {
            $errHandler = ErrorTrap::captureErrors(
                function () use ($volume, $filename, $md5) {
                    /** @var VolumeFilesystem|VolumeS3 $instance */
                    $instance = VolumeManager::getDriver($volume);
                    if ($instance->fileExists($filename)) {
                        $instance->fileDelete($filename);
                    }
                    if (!$instance->fileUpload(TMP . $filename, $filename)) {
                        return false;
                    }
                    $begin = microtime(true);
                    $content = $instance->fileGetContent($filename);
                    $diff = microtime(true) - $begin;
                    if (!$content) {
                        return false;
                    }
                    if (md5($content) !== $md5) { // NOSONAR
                        return false;
                    }
                    $instance->fileDelete($filename);
                    return $diff;
                }
            );
            if (
                !empty($errHandler['result']) && is_float(
                    $errHandler['result']
                )
            ) {
                $this->updateAll(
                    ['read_duration' => $errHandler['result']],
                    ['id' => $volume->id]
                );
            }
        } catch (Exception) {
        }
        if (is_file(TMP . $filename)) {
            unlink(TMP . $filename);
        }
    }

    /**
     * Met à jour le champ disk_usage d'un volume
     * @param int $volume_id
     * @return int
     */
    public function updateDiskUsage($volume_id): int
    {
        $query = $this->StoredFilesVolumes->find();
        $result = $query->select(
            ['size' => $query->func()->sum('StoredFiles.size')]
        )
            ->innerJoinWith('StoredFiles')
            ->where(['StoredFilesVolumes.volume_id' => $volume_id])
            ->all();
        $size = (int)$result->first()->get('size');
        $this->updateAll(['disk_usage' => $size], ['id' => $volume_id]);
        return $size;
    }

    /**
     * Donne la liste des fichiers présents sur le volume mais pas
     * sur stored_files (table)
     * @param int  $volume_id
     * @param bool $delete
     * @return array
     * @throws VolumeException
     */
    public function findUnreferencedFiles(
        $volume_id,
        bool $delete = false
    ): array {
        $driver = VolumeManager::getDriverById($volume_id);
        $refs = [];
        foreach ($driver->ls('', true) as $ref) {
            if ($ref === '--- / ---') {
                continue;
            }
            $exists = $this->StoredFilesVolumes->exists(
                ['volume_id' => $volume_id, 'storage_ref' => $ref]
            );
            if (!$exists) {
                $refs[] = $ref;
                if ($delete) {
                    $driver->fileDelete($ref);
                }
            }
        }
        return $refs;
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }
}
