<?php

/**
 * Asalae\Model\Table\StoredFilesTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table stored_files
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class StoredFilesTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');

        $this->belongsTo('SecureDataSpaces');
        $this->hasMany('StoredFilesVolumes');
        $this->hasMany('ArchiveBinaries');
        $this->hasMany('ArchiveFiles');
        $this->belongsToMany('Volumes');

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 2048)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add(
                'name',
                [
                    'unique' => [
                        'rule' => [
                            'validateUnique',
                            ['scope' => 'secure_data_space_id'],
                        ],
                        'provider' => 'table',
                        'message' => __("Ce nom est déjà utilisé"),
                    ],
                ]
            );

        $validator
            ->requirePresence('size', 'create')
            ->notEmptyString('size');

        $validator
            ->scalar('hash')
            ->maxLength('hash', 255)
            ->requirePresence('hash', 'create')
            ->notEmptyString('hash');

        $validator
            ->scalar('hash_algo')
            ->maxLength('hash_algo', 255)
            ->requirePresence('hash_algo', 'create')
            ->notEmptyString('hash_algo');

        return $validator;
    }
}
