<?php

/**
 * Asalae\Model\Table\EventLogsTable
 */

namespace Asalae\Model\Table;

use Asalae\Model\Entity\Cron;
use Asalae\Model\Entity\EventLog;
use Asalae\Model\Entity\StoredFile;
use Asalae\Model\Entity\User;
use AsalaeCore\Model\Entity\PremisAgentEntityInterface;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\Utility\Premis;
use Cake\Core\App;
use Cake\Datasource\EntityInterface;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\Validation\Validator;
use DOMDocument;

/**
 * Table event_logs
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class EventLogsTable extends Table
{
    /**
     * @var int utilisateur sélectionné (gestion par super archiviste)
     */
    public static $selectionnedUserId;
    /**
     * @var int service d'archives sélectionné (gestion par super archiviste)
     */
    public static $selectionnedArchivalAgencyId;

    /**
     * @var array liste les noms d'entités de type PremisObjectEntityInterface
     */
    public static $premisObjectEntities = [];

    /**
     * @var array liste les noms d'entités de type PremisAgentEntityInterface
     */
    public static $premisAgentEntities = [];

    /**
     * EventLogsTable constructor.
     * @param array $config List of options for this table
     */
    public function __construct(array $config = [])
    {
        if (empty(self::$premisObjectEntities)) {
            $paths = App::classPath('Model/Table');
            $models = glob($paths[0] . '*Table.php');
            $namespace = 'Asalae\Model\Entity\\';
            foreach ($models as $file) {
                $name = basename($file, 'Table.php');
                $entityClassname = Inflector::singularize($name);
                if (!class_exists($namespace . $entityClassname)) {
                    continue;
                }
                $underscore = Inflector::underscore($entityClassname);
                $implements = class_implements($namespace . $entityClassname);
                if (in_array(PremisObjectEntityInterface::class, $implements)) {
                    self::$premisObjectEntities[$underscore] = $name;
                }
                if (in_array(PremisAgentEntityInterface::class, $implements)) {
                    self::$premisAgentEntities[$underscore . 'agent'] = $entityClassname . 'agents';
                }
            }
        }
        parent::__construct($config);
    }

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $alias = $this->getAlias();

        $this->addBehavior('Timestamp');

        $this->belongsTo('Useragents')
            ->setClassName('Users')
            ->setForeignKey('user_id');
        $this->belongsTo('Cronagents')
            ->setClassName('Crons')
            ->setForeignKey('cron_id');
        $this->belongsTo('ArchivalAgencies')
            ->setClassName('OrgEntities');

        /**
         * Ajoute un belongsTo sur les PremisObjectEntityInterface
         * foreign_key = object_foreign_key
         */
        foreach (self::$premisObjectEntities as $name) {
            $this->belongsTo($name)
                ->setForeignKey('object_foreign_key')
                ->setConditions(["$alias.object_model" => $name]);
        }
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('outcome')
            ->maxLength('outcome', 255)
            ->requirePresence('outcome', 'create')
            ->notEmptyString('outcome');

        $validator
            ->scalar('outcome_detail')
            ->requirePresence('outcome_detail', 'create')
            ->notEmptyString('outcome_detail');

        $validator
            ->scalar('object_model')
            ->maxLength('object_model', 255)
            ->allowEmptyString('object_model');

        $validator
            ->integer('object_foreign_key')
            ->allowEmptyString('object_foreign_key');

        $validator
            ->boolean('exported')
            ->requirePresence('exported', 'create')
            ->notEmptyString('exported');

        $validator
            ->scalar('object_serialized')
            ->allowEmptyString('object_serialized');

        $validator
            ->scalar('agent_serialized')
            ->allowEmptyString('agent_serialized');

        $validator
            ->boolean('in_lifecycle')
            ->allowEmptyString('in_lifecycle');

        return $validator;
    }

    /**
     * Créé un nouvel evenement
     * @param string                                    $type
     * @param string                                    $outcome
     * @param string                                    $detail
     * @param EntityInterface|int|Premis\Agent|null     $agent   user_id ou User ou Cron
     * @param EntityInterface|Premis\PremisObject| null $object
     * @return EventLog
     */
    public function newEntry(
        string $type,
        string $outcome,
        string $detail,
        $agent = null,
        $object = null
    ): EventLog {
        $entry = new EventLog(
            [
                'type' => $type,
                'outcome' => $outcome,
                'outcome_detail' => $detail,
                'exported' => false,
            ]
        );
        if ($agent instanceof User) {
            $entry->set('user_id', $agent->get('id'));
        } elseif ($agent instanceof Cron) {
            $entry->set('cron_id', $agent->get('id'));
        } elseif (is_numeric($agent)) {
            $entry->set('user_id', $agent);
        } elseif ($agent instanceof Premis\Agent) {
            $entry->set('agent_serialized', serialize($agent));
        }
        if ($object instanceof Premis\IntellectualEntity) {
            $entry->set('object_serialized', serialize($object));
        } elseif ($object !== null) {
            if ($source = $object->getSource()) {
                $entry->set('object_model', $source);
            } else {
                $classname = get_class($object);
                $entityName = trim(
                    substr($classname, strrpos($classname, '\\')),
                    '\\'
                );
                $entry->set('object_model', Inflector::pluralize($entityName));
            }
            $entry->set('object_foreign_key', $object->get('id'));
        }
        $this->addArchivalAgency($entry);
        return $entry;
    }

    /**
     * Ajoute le archival_agency_id dans l'entité à partir de user_id ou de l'objet
     * @param EntityInterface $entry
     * @return void
     */
    private function addArchivalAgency(EntityInterface $entry)
    {
        $loc = TableRegistry::getTableLocator();
        if ($entry->get('user_id') && $entry->get('user_id') === self::$selectionnedUserId) {
            $entry->set('archival_agency_id', self::$selectionnedArchivalAgencyId);
            return;
        }
        if ($entry->get('object_model')) {
            switch ($entry->get('object_model')) {
                case 'Archives':
                    $Archives = $loc->get('Archives');
                    $archivalAgency = $Archives->find()
                        ->select(['archival_agency_id' => 'Archives.archival_agency_id'])
                        ->where(['Archives.id' => $entry->get('object_foreign_key')])
                        ->disableHydration()
                        ->first();
                    break;
                case 'ArchiveFiles':
                    $ArchiveFiles = $loc->get('ArchiveFiles');
                    $archivalAgency = $ArchiveFiles->find()
                        ->select(['archival_agency_id' => 'Archives.archival_agency_id'])
                        ->innerJoinWith('Archives')
                        ->where(['ArchiveFiles.id' => $entry->get('object_foreign_key')])
                        ->disableHydration()
                        ->first();
                    break;
                case 'ArchiveBinaries':
                    $ArchiveBinaries = $loc->get('ArchiveBinaries');
                    $archivalAgency = $ArchiveBinaries->find()
                        ->select(['archival_agency_id' => 'Archives.archival_agency_id'])
                        ->innerJoinWith('ArchiveUnits.Archives')
                        ->where(['ArchiveBinaries.id' => $entry->get('object_foreign_key')])
                        ->disableHydration()
                        ->first();
                    break;
                case 'Transfers':
                    $Transfers = $loc->get('Transfers');
                    $archivalAgency = $Transfers->find()
                        ->select(['archival_agency_id' => 'Transfers.archival_agency_id'])
                        ->where(['Transfers.id' => $entry->get('object_foreign_key')])
                        ->disableHydration()
                        ->first();
                    break;
            }
        }
        if (empty($archivalAgency) && $entry->get('user_id')) {
            $Users = $loc->get('Users');
            $archivalAgency = $Users->find()
                ->select(['archival_agency_id' => 'ArchivalAgencies.id'])
                ->innerJoinWith('OrgEntities.ArchivalAgencies')
                ->where(['Users.id' => $entry->get('user_id')])
                ->disableHydration()
                ->first();
        }
        if (isset($archivalAgency)) {
            $entry->set('archival_agency_id', $archivalAgency['archival_agency_id']);
        }
    }

    /**
     * Exporte les events sous forme de xml (premis v3.0)
     * @param array                           $conditions
     * @param EntityInterface|StoredFile|null $previous
     * @param callable|null                   $touch      met
     *                                                    à
     *                                                    jour
     *                                                    CronExecutions.last_update
     * @return DOMDocument
     */
    public function export(
        array $conditions = [],
        EntityInterface $previous = null,
        callable $touch = null
    ): DOMDocument {
        $agents = [];
        $agentIdentifiers = [];
        $objectIdentifiers = [];
        $objects = [];
        $objectStr = '';
        $eventStr = '';
        $agentStr = '';
        if ($previous) {
            $object = new Premis\IntellectualEntity('local', 'EventsLogsChain');
            $object->originalName = 'previous file';
            $object->significantProperties['filename'] = basename(
                $previous->get('name')
            );
            $object->significantProperties['size'] = $previous->get('size');
            $object->significantProperties['hash_algo'] = $previous->get(
                'hash_algo'
            );
            $object->significantProperties['hash'] = $previous->get('hash');
            $objectStr .= $object;
        }

        /** @var string[]|array[]|CakeDateTime[] $event */
        foreach ($this->getQueryExport($conditions) as $event) {
            $ev = $this->toPremisEvent($event, $objects, $agents);
            foreach ($ev->objects as $object) {
                if (!isset($objectIdentifiers[$object->identifierValue])) {
                    $objectStr .= $object;
                    $objectIdentifiers[$object->identifierValue] = true;
                }
            }
            $eventStr .= $ev;
            foreach ($ev->agents as $agent) {
                if (!isset($agentIdentifiers[$agent->identifierValue])) {
                    $agentStr .= $agent;
                    $agentIdentifiers[$agent->identifierValue] = true;
                }
            }
            if ($touch) {
                $touch();
            }
        }
        $dom = new DOMDocument();
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $xml = '<premis xmlns="' . Premis::NS_PREMIS . '" ' .
            'xmlns:premis="' . Premis::NS_PREMIS . '" ' .
            'xmlns:xlink="' . Premis::NS_XLINK . '" ' .
            'xmlns:xsi="' . Premis::NS_XSI . '" version="3.0">';
        $dom->loadXML($xml . $objectStr . $eventStr . $agentStr . '</premis>');

        return $dom;
    }

    /**
     * Donne le query pour l'export xml (premis) des events
     * @param array $conditions
     * @return Query
     */
    public function getQueryExport(array $conditions = []): Query
    {
        $models = array_values(self::$premisAgentEntities);
        return $this->find()
            ->where($conditions)
            ->orderBy(
                [
                    'EventLogs.created' => 'asc',
                    'EventLogs.id' => 'asc',
                ]
            )
            ->contain(array_values($models));
    }

    /**
     * Transforme un EventLog en Premis\Event
     * @param EntityInterface $event
     * @param array           $objects
     * @param array           $agents
     * @return Premis\Event
     */
    public function toPremisEvent(
        EntityInterface $event,
        array &$objects = [],
        array &$agents = []
    ): Premis\Event {
        $eventId = 'EventLogs:' . $event->id;
        $ev = new Premis\Event($event->get('type'), 'local', $eventId);
        $ev->dateTime = ($created = $event->get('created')) instanceof CakeDateTime
            ? $created->format(DATE_RFC3339)
            : $created;
        $ev->detail = $this->typeToDetail($event->get('type'));
        $ev->outcome = $event->get('outcome');
        $ev->outcomeDetail = $event->get('outcome_detail');
        /**
         * Agents
         */
        if ($event->get('user_id')) {
            $modelEntity = $event->get('useragent');
        } elseif ($event->get('cron_id')) {
            $modelEntity = $event->get('cronagent');
        } elseif ($event->get('agent_serialized')) {
            $agent = unserialize($event->get('agent_serialized'));
            $agents[$agent->identifierValue] = $agents[$agent->identifierValue] ?? $agent;
            $ev->addAgent($agents[$agent->identifierValue]);
        }
        if (isset($modelEntity) && $modelEntity instanceof PremisAgentEntityInterface) {
            $agent = $modelEntity->toPremisAgent();
            $agents[$agent->identifierValue] = $agents[$agent->identifierValue] ?? $agent;
            $ev->addAgent($agents[$agent->identifierValue]);
        }
        /**
         * Objets
         */
        if ($event['object_serialized']) {
            /** @var Premis\IntellectualEntity $obj */
            $obj = unserialize($event['object_serialized']);
            if (!isset($objects[$obj->identifierValue])) {
                $objects[$obj->identifierValue] = $obj;
            }
            $ev->addObject($objects[$obj->identifierValue]);
            return $ev;
        }
        $model = $event->get('object_model');
        $id = $event->get('object_foreign_key');
        if ($id && isset($this->$model) && $this->$model instanceof BelongsTo) {
            $objectEntity = $this->$model->find()
                ->where(['id' => $id], [], true)
                ->first();
            if ($objectEntity instanceof PremisObjectEntityInterface) {
                $object = $objectEntity->toPremisObject();
                $identifier = $object->getIdentifierValue();
                $objects[$identifier] = $objects[$identifier] ?? $object;
                $ev->addObject($object);
                return $ev;
            }
        }
        trigger_error(
            __(
                "Objet non trouvé (supprimé ou jointure manquante) ({0}:{1})",
                $event['object_model'],
                $event['object_foreign_key']
            )
        );
        return $ev;
    }

    /**
     * Donne le detail (commentaire) pour un type d'evenement particulier
     * @param string $type
     * @return string
     */
    private function typeToDetail($type): string
    {
        switch ($type) {
            case 'login':
                return __("Tentative de connexion à l'application");
            case 'transfer_add':
                return __("création du transfert");
        }
        return $type;
    }

    /**
     * Ajoute dans le champ 'object_serialized' une entité serialized en premis
     * @param string                             $alias
     * @param string|int                         $id
     * @param string|PremisObjectEntityInterface $value
     */
    public static function serializeObject(string $alias, $id, $value = '')
    {
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $conditions = [
            'object_model' => $alias,
            'object_foreign_key' => $id,
        ];
        if ($value instanceof PremisObjectEntityInterface) {
            $object = $value->toPremisObject();
        } else {
            $object = new Premis\IntellectualEntity(
                'local',
                $alias . ':' . $id
            );
            if ($value) {
                $object->originalName = h($value);
            }
        }
        $serialized = serialize($object);
        $EventLogs->updateAll(
            ['object_serialized' => $serialized],
            $conditions
        );
    }
}
