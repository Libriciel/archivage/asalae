<?php

/**
 * Asalae\Model\Table\MediainfoTextsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table mediainfo_texts
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MediainfoTextsTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Mediainfos');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('format')
            ->maxLength('format', 512)
            ->allowEmptyString('format');

        $validator
            ->scalar('codec_id_info')
            ->maxLength('codec_id_info', 512)
            ->allowEmptyString('codec_id_info');

        $validator
            ->scalar('duration')
            ->maxLength('duration', 512)
            ->allowEmptyString('duration');

        $validator
            ->scalar('bit_rate')
            ->maxLength('bit_rate', 512)
            ->allowEmptyString('bit_rate');

        $validator
            ->scalar('count_of_elements')
            ->maxLength('count_of_elements', 512)
            ->allowEmptyString('count_of_elements');

        $validator
            ->scalar('stream_size')
            ->maxLength('stream_size', 512)
            ->allowEmptyString('stream_size');

        $validator
            ->scalar('title')
            ->maxLength('title', 512)
            ->allowEmptyString('title');

        $validator
            ->scalar('language')
            ->maxLength('language', 512)
            ->allowEmptyString('language');

        $validator
            ->scalar('_default')
            ->maxLength('_default', 512)
            ->allowEmptyString('_default');

        $validator
            ->scalar('forced')
            ->maxLength('forced', 512)
            ->allowEmptyString('forced');

        return $validator;
    }
}
