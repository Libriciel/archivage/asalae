<?php

/**
 * Asalae\Model\Table\OrgEntitiesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use Asalae\Controller\Component\SealComponent;
use Asalae\Model\Behavior\SealBehavior;
use Asalae\Model\Entity\User;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Model\Table\BeforeSaveInterface;
use AsalaeCore\Model\Table\OrgEntitiesTable as CoreOrgEntitiesTable;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Behavior\TreeBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use ReflectionException;

/**
 * Table org_entities
 *
 * Autres alias:
 * ServiceArchives
 * TransferringAgencies
 * OriginatingAgencies
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin SealBehavior
 * @mixin TimestampBehavior
 * @mixin TreeBehavior
 * @property TypeEntitiesTable|BelongsTo TypeEntities
 */
class OrgEntitiesTable extends CoreOrgEntitiesTable implements
    AfterSaveInterface,
    AfterDeleteInterface,
    BeforeDeleteInterface,
    BeforeSaveInterface,
    SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');

        try {
            $subquery = $this->selectQuery()
                ->select(['Eaccpfs.id'])
                ->from(['Eaccpfs' => 'eaccpfs'])
                ->where(
                    [
                        'Eaccpfs.org_entity_id' => new IdentifierExpression(
                            'OrgEntities.id'
                        ),
                    ]
                )
                ->orderBy(['Eaccpfs.created' => 'desc'])
                ->limit(1);
            $this->hasOne('LastEaccpfs')
                ->setClassName(EaccpfsTable::class)
                ->setForeignKey(false)
                ->setConditions(['LastEaccpfs.id IN' => $subquery]);
        } catch (Exception) { // BDD down
            $this->hasOne('LastEaccpfs')
                ->setClassName(EaccpfsTable::class)
                ->setForeignKey(false)
                ->setConditions(['LastEaccpfs.id IS' => null]);
        }
        $this->hasMany('TransfersHasArchivalAgencies')
            ->setClassName('Transfers')
            ->setForeignKey('archival_agency_id');
        $this->hasMany('TransfersHasTransferringAgencies')
            ->setClassName('Transfers')
            ->setForeignKey('transferring_agency_id');
        $this->hasMany('ArchivesHasArchivalAgencies')
            ->setClassName('Archives')
            ->setForeignKey('archival_agency_id');
        $this->hasMany('ArchivesHasTransferringAgencies')
            ->setClassName('Archives')
            ->setForeignKey('transferring_agency_id');

        $this->belongsTo('ParentOrgEntities')
            ->setClassName('OrgEntities')
            ->setForeignKey('parent_id');
        $this->belongsTo('TypeEntities');
        $this->belongsTo('Timestampers');
        $this->belongsTo('ArchivalAgencies')
            ->setClassName('OrgEntities')
            ->setForeignKey(false)
            ->setConditions(
                function (QueryExpression $qe, Query $q) {
                    $alias = $this->getAlias(); // OrgEntities
                    $assocAlias = $q->getRepository()->getAlias(); // ArchivalAgencies
                    $query = $this->selectQuery()
                        ->select(['aa.id'])
                        ->from(['aa' => 'org_entities'])
                        ->innerJoin(
                            ['aat' => 'type_entities'],
                            ['aat.id' => $q->identifier('aa.type_entity_id')]
                        )
                        ->where(
                            [
                                'aat.code IN' => array_merge(
                                    OrgEntitiesTable::CODE_OPERATING_DEPARTMENT,
                                    OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES
                                ),
                                'aa.lft <=' => $q->identifier("$alias.lft"),
                                'aa.rght >=' => $q->identifier("$alias.rght"),
                            ]
                        )
                        ->orderByDesc(new QueryExpression("aat.code = 'SA'"))
                        ->orderByAsc('aa.lft')
                        ->limit(1);
                    return $qe->and(["$assocAlias.id" => $query]);
                }
            );
        $this->hasMany('AccessRuleCodes');
        $this->hasMany('Agreements');
        $this->hasMany('AgreementsOriginatingAgencies');
        $this->hasMany('AgreementsTransferringAgencies');
        $this->hasMany('AppraisalRuleCodes');
        $this->hasMany('Configurations')->setDependent(true);
        $this->hasMany('Counters')->setDependent(true);
        $this->hasMany('Eaccpfs')->setDependent(true);
        $this->hasMany('KeywordLists')->setDependent(true);
        $this->hasMany('ChildOrgEntities')
            ->setForeignKey('parent_id')
            ->setClassName('OrgEntities');
        $this->hasMany('Profiles');
        $this->hasMany('Roles');
        $this->hasMany('Sequences')->setDependent(true);
        $this->hasMany('ServiceLevels');
        $this->hasMany('Users');
        $this->hasMany('ValidationChains');
        $this->hasMany('SecureDataSpaces')->setDependent(
            true
        ); // evite d'être mutuellement en deletable => false
        $this->hasMany('Ldaps');
        $this->hasMany('ConversionPolicies');
        $this->hasMany('TechnicalArchives')
            ->setForeignKey('archival_agency_id');
        $this->hasMany('ArchivingSystems');
        $this->belongsToMany('Timestampers');
        $this->belongsToMany('SuperArchivists')
            ->setClassName('Users')
            ->setForeignKey('archival_agency_id')
            ->setTargetForeignKey('user_id')
            ->setThrough('SuperArchivistsArchivalAgencies');
        $this->hasMany('BaseChangeEntityRequests')
            ->setClassName('ChangeEntityRequests')
            ->setForeignKey('base_archival_agency_id');
        $this->hasMany('TargetChangeEntityRequests')
            ->setClassName('ChangeEntityRequests')
            ->setForeignKey('target_archival_agency_id');
        $this->hasMany('EventLogs')
            ->setForeignKey('archival_agency_id');
        $this->hasMany('OriginatingAgencyMessageIndicators')
            ->setClassName('MessageIndicators')
            ->setForeignKey('originating_agency_id');
        $this->hasMany('TransferringAgencyMessageIndicators')
            ->setClassName('MessageIndicators')
            ->setForeignKey('transferring_agency_id');
        $this->hasMany('ArchivalAgencyMessageIndicators')
            ->setClassName('MessageIndicators')
            ->setForeignKey('archival_agency_id');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier')
            ->add(
                'identifier',
                [
                    'sa_unique' => [ // cas service d'archives (parent_id = service d'exploitation)
                        'rule' => ['validateUnique', ['scope' => 'parent_id']],
                        'provider' => 'table',
                        'message' => __("Cet identifiant est déjà utilisé"),
                    ],
                ]
            )
            ->add(
                'identifier',
                [
                    'unique' => [ // cas entités d'un même service d'archives
                        'rule' => function ($value, $context) {
                            $archivalAgencyId = Hash::get(
                                $context,
                                'data.archival_agency.id',
                                Hash::get($context, 'data.archival_agency_id')
                            );
                            $id = Hash::get($context, 'data.id');
                            if ($archivalAgencyId) {
                                return count(
                                    $this->find()
                                            ->select(['existing' => 1])
                                            ->innerJoinWith('ArchivalAgencies')
                                            ->where(
                                                [
                                                    'ArchivalAgencies.id' => $archivalAgencyId,
                                                    'OrgEntities.identifier' => $value,
                                                ]
                                                + ($id ? ['OrgEntities.id !=' => $id] : []) // pas la même entité
                                            )
                                            ->disableHydration()
                                            ->limit(1)
                                            ->toArray()
                                ) === 0;
                            }
                            return true;
                        },
                        'message' => __("Cet identifiant est déjà utilisé"),
                    ],
                ]
            );

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        $validator
            ->boolean('is_main_archival_agency')
            ->allowEmptyString('is_main_archival_agency');

        $validator->add(
            'timestamper_id',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if (!empty($value) || empty($context['data']['type_entity_id'])) {
                        return true;
                    }
                    $Types = TableRegistry::getTableLocator()->get(
                        'TypeEntities'
                    );
                    $saId = $Types->find()->select(['id'])->where(
                        ['code' => 'SA']
                    )->first()->get('id');
                    return (int)$context['data']['type_entity_id'] !== $saId;
                },
                'message' => __("Filiation impossible"),
            ]
        );

        $validator->add(
            'type_entity_id',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if (empty($context['data']['id'])) {
                        return true;
                    }
                    $Users = TableRegistry::getTableLocator()->get('Users');
                    $rolesUserResults = $Users->find()
                        ->select(['role_id'])
                        ->where(
                            [
                                'org_entity_id' => $context['data']['id'],
                                'agent_type' => 'person',
                                'active IS' => true,
                            ]
                        )
                        ->all()
                        ->toArray();
                    $rolesUser = (array)array_map(
                        function (User $v) {
                            return $v->get('role_id');
                        },
                        $rolesUserResults
                    );
                    if (empty($rolesUser)) {
                        return true;
                    }
                    $rolesEntiteResults = TableRegistry::getTableLocator()->get(
                        'RolesTypeEntities'
                    )->find()
                        ->where(['type_entity_id' => $value])
                        ->all()
                        ->toArray();
                    $rolesEntite = (array)array_map(
                        function (Entity $v) {
                            return $v->get('role_id');
                        },
                        $rolesEntiteResults
                    );
                    return empty(
                        array_diff(
                            array_unique($rolesUser),
                            $rolesEntite
                        )
                    );
                },
                'message' => __(
                    "Un utilisateur de l'entité possède un rôle non disponible dans ce type d'entité"
                ),
            ]
        );
        $validator->allowEmptyString(
            'default_secure_data_space_id',
            null,
            function ($context) {
                if (!empty(Hash::get($context, 'data.type_entity_id'))) {
                    if (empty(Hash::get($context, 'data.type_entity'))) {
                        $typeEntity = $this->TypeEntities->find()
                            ->where(['id' => $context['data']['type_entity_id']])
                            ->first();
                        $entityTypeCode = $typeEntity ? $typeEntity->get('code') : null;
                        $context['data']['type_entity'] = $entityTypeCode;
                    } else {
                        $entityTypeCode = Hash::get($context, 'data.type_entity.code');
                    }
                    return $entityTypeCode !== 'SA';
                }
                return true;
            }
        );
        $validator->allowEmptyString(
            'timestamper_id',
            null,
            function ($context) {
                if (!empty(Hash::get($context, 'data.type_entity_id'))) {
                    if (empty(Hash::get($context, 'data.type_entity'))) {
                        $TypeEntities = TableRegistry::getTableLocator()->get(
                            'TypeEntities'
                        );
                        $context['data']['type_entity']
                            = $TypeEntities->find()->where(
                                ['id' => $context['data']['type_entity_id']]
                            )->first();
                    }
                    if (Hash::get($context, 'data.type_entity.code') !== 'SA') {
                        return true;
                    }
                    $isMain = Hash::get(
                        $context,
                        'data.is_main_archival_agency'
                    );
                    return !$isMain; // timestamper_id obligatoire si main
                }
                return true;
            }
        );

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('short_name')
            ->maxLength('short_name', 40)
            ->allowEmptyString('short_name');

        return $validator;
    }

    /**
     * Donne les options de select d'entités
     * @param array              $params
     * @param SealComponent|null $Seal
     * @return Query
     * @throws ReflectionException
     */
    public function listOptions(array $params = [], SealComponent $Seal = null): Query
    {
        $params += [
            'valueField' => function ($entity) {
                return Hash::get($entity, 'identifier') . ' - ' . Hash::get($entity, 'name');
            },
            'groupField' => function ($entity) {
                $parents = Hash::get($entity, 'path');
                return $parents ?: Hash::get($entity, 'archival_agency');
            },
        ];
        $keyField = $params['keyField'] ?? null;
        $valueField = $params['valueField'] ?? null;
        $groupField = $params['groupField'] ?? null;
        $query = $Seal === null
            ? $this->find(
                'list',
                keyField: $keyField,
                valueField: $valueField,
                groupField: $groupField
            )
            : $Seal->table('OrgEntities')->find(
                'sealList',
                keyField: $keyField,
                valueField: $valueField,
                groupField: $groupField
            );
        return $query
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.name',
                    'OrgEntities.identifier',
                    'archival_agency' => 'ArchivalAgencies.name',
                    'path' => $this->queryParentGroupSelect(),
                ]
            )
            ->contain(['ArchivalAgencies', 'TypeEntities'])
            ->orderBy(['OrgEntities.lft', 'OrgEntities.identifier']);
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function afterSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        parent::afterSave($event, $entity, $options);
        if (
            $entity->isNew()
            && $entity->get('code') === 'SA'
            && !($options['dumpSave'] ?? false)
        ) {
            $this->initCounters($entity->get('id'));
        } elseif (
            $entity->isDirty('name')
            || $entity->isDirty('identifier')
            || $entity->isDirty('description')
        ) {
            $Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs');
            $query = $Eaccpfs->find()->where(
                ['org_entity_id' => $entity->get('id')]
            );
            /** @var EntityInterface $eaccpf */
            foreach ($query as $eaccpf) {
                $data = json_decode($eaccpf->get('data'), true);
                $data['eac-cpf']['cpfDescription']['identity']['nameEntry']['part']
                    = $entity->get('name');
                $data['eac-cpf']['control']['recordId']
                    = $entity->get('identifier');
                if ($desc = $entity->get('description')) {
                    $data = Hash::insert(
                        $data,
                        'eac-cpf.cpfDescription.identity.descriptiveNote.p',
                        $desc
                    );
                }
                $Eaccpfs->patchEntity(
                    $eaccpf,
                    [
                        'name' => $entity->get('name'),
                        'record_id' => $entity->get('identifier'),
                        'data' => json_encode($data, JSON_UNESCAPED_SLASHES),
                    ],
                    ['validate' => false]
                );
                $Eaccpfs->saveOrFail($eaccpf);
            }
        }
        if (
            $entity->isDirty('is_main_archival_agency')
            && $entity->get('is_main_archival_agency')
        ) {
            $this->updateAll(
                ['is_main_archival_agency' => false],
                ['id !=' => $entity->get('id')]
            );
        }
        $Exec = Utility::get('Exec');
        $Exec->async(CAKE_SHELL, 'session', 'org_entities');
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $SecureDataSpaces = TableRegistry::getTableLocator()->get(
            'SecureDataSpaces'
        );
        $SecureDataSpaces->updateAll(
            ['org_entity_id' => null],
            ['org_entity_id' => $entity->get('id')]
        );
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function afterDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $SecureDataSpaces = TableRegistry::getTableLocator()->get('SecureDataSpaces');
        $SecureDataSpaces->updateAll(
            ['is_default' => false],
            ['org_entity_id IS' => null]
        );

        $Exec = Utility::get('Exec');
        $Exec->async(CAKE_SHELL, 'session', 'org_entities');
        parent::afterDelete($event, $entity, $options);
    }

    /**
     * Donne le CST d'un service d'archives
     * (le service d'archives lui même si il n'y en a pas)
     * @param int $archivalAgencyId
     * @return EntityInterface
     */
    public function cst(int $archivalAgencyId): EntityInterface
    {
        return $this->find()
            ->innerJoinWith('TypeEntities')
            ->innerJoinWith('ArchivalAgencies')
            ->where(
                [
                    'ArchivalAgencies.id' => $archivalAgencyId,
                    'TypeEntities.code IN' => ['SA', 'CST'],
                    'OrgEntities.active' => true,
                ]
            )
            ->orderBy(
                [
                    "TypeEntities.code = 'CST'" => 'desc',
                    'OrgEntities.id',
                ]
            )
            ->firstOrFail();
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @return false|void
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        // bloque certains changements de type d'entités
        if (!$entity->isNew() && $entity->isDirty('type_entity_id') && $entity->getOriginal('type_entity_id')) {
            $prevTypeEntity = $this->TypeEntities->get($entity->getOriginal('type_entity_id'));
            $Archives = TableRegistry::getTableLocator()->get('Archives');
            if ($prevTypeEntity->get('code') === 'SV') {
                $Transfers = TableRegistry::getTableLocator()->get('Transfers');
                if (
                    $Transfers->exists(['transferring_agency_id' => $entity->id])
                    || $Archives->exists(['transferring_agency_id' => $entity->id])
                ) {
                    return false;
                }
            } elseif ($prevTypeEntity->get('code') === 'SP') {
                $newTypeEntity = $this->TypeEntities->get($entity->get('type_entity_id'));
                if (
                    $newTypeEntity->get('code') !== 'SV'
                    && $Archives->exists(['originating_agency_id' => $entity->id])
                ) {
                    return false;
                }
            }
        }
    }

    /**
     * Donne le service d'archives pour une entité donné
     * @param int $org_entity_id
     * @return EntityInterface
     */
    public function getArchivalAgency(int $org_entity_id): EntityInterface
    {
        $q = $this->query();
        $currentEntity = $this->find()
            ->select(['SubArchivalAgencies.id', 'ArchivalAgencies.id'])
            ->innerJoin(
                ['TypeEntities' => 'type_entities'],
                ['TypeEntities.id' => $q->identifier('OrgEntities.type_entity_id')]
            )
            ->leftJoin(
                ['SubArchivalAgencies' => 'org_entities'],
                [
                    'TypeEntities.code' => 'SE',
                    'SubArchivalAgencies.is_main_archival_agency IS' => true,
                ]
            )
            ->leftJoinWith('ArchivalAgencies')
            ->where(['OrgEntities.id' => $org_entity_id])
            ->firstOrFail();
        $archivalAgencyId = Hash::get($currentEntity, 'SubArchivalAgencies.id')
            ?: Hash::get($currentEntity, '_matchingData.ArchivalAgencies.id');
        return $this->find()->where(['id' => $archivalAgencyId])->firstOrFail();
    }
}
