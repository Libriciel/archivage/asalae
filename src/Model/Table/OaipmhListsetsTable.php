<?php

/**
 * Asalae\Model\Table\OaipmhListsetsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table oaipmh_listsets
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OaipmhListsetsTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('OaipmhTokens');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('spec')
            ->maxLength('spec', 512) // 255 + prefix
            ->requirePresence('spec', 'create')
            ->notEmptyString('spec');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
