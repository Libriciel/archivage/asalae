<?php

/**
 * Asalae\Model\Table\FileuploadsTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Table\FileuploadsTable as CoreFileuploadsTable;
use AsalaeCore\Model\Table\MediainfosTable;
use AsalaeCore\Model\Table\SiegfriedsTable;

/**
 * Table fileuploads
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property MediainfosTable Mediainfos
 * @property SiegfriedsTable Siegfrieds
 */
class FileuploadsTable extends CoreFileuploadsTable
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsTo('Users');
        $this->belongsToMany('Profiles');
        parent::initialize($config);
    }
}
