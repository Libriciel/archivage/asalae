<?php

/**
 * Asalae\Model\Table\TypeEntitiesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\AclBehavior;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\Model\Table\TypeEntitiesTable as CoreTypeEntitiesTable;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;

/**
 * Table type_entities
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AclBehavior
 * @mixin TimestampBehavior
 * @property RolesTable Roles
 */
class TypeEntitiesTable extends CoreTypeEntitiesTable implements
    AfterDeleteInterface,
    AfterSaveInterface
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsToMany('Roles');

        parent::initialize($config);
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        parent::afterSave($event, $entity, $options);
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        parent::afterDelete($event, $entity, $options);
    }
}
