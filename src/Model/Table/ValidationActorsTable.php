<?php

/**
 * Asalae\Model\Table\ValidationActorsTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\UniqueTrait;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Table validation_actors
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin OptionsBehavior
 */
class ValidationActorsTable extends Table
{
    /**
     * Traits
     */
    use UniqueTrait;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');

        $this->belongsTo('ValidationStages');
        $this->belongsTo('Users')
            ->setForeignKey('app_foreign_key')
            ->setConditions(['ValidationActors.app_type' => 'USER']);
        $this->hasMany('ValidationHistories');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'app_type' => [
                    'USER',
                    'MAIL',
                    'SERVICE_ARCHIVES',
                    'SERVICE_DEMANDEUR',
                    'SERVICE_PRODUCTEUR',
                ],
                'type_validation' => [
                    'V', // Visa
                    'S', // Signature
                ],
            ]
        );

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->allowEmptyString(
            'app_foreign_key',
            null,
            function ($context) {
                if (empty($context['data']['app_type'])) {
                    return false;
                }
                return $context['data']['app_type'] !== 'USER';
            }
        );
        $validator->allowEmptyString(
            'type_validation',
            null,
            function ($context) {
                return ($context['data']['app_type'] ?? '') !== 'USER';
            }
        );
        $validator->allowEmptyString(
            'app_key',
            null,
            function ($context) {
                if (empty($context['data']['app_type'])) {
                    return false;
                }
                return $context['data']['app_type'] !== 'MAIL';
            }
        );
        $validator->email(
            'app_key',
            false,
            null,
            function ($context) {
                return ($context['data']['app_type'] ?? '') === 'MAIL';
            }
        );
        $validator->allowEmptyString(
            'actor_name',
            null,
            function ($context) {
                if (empty($context['data']['app_type'])) {
                    return false;
                }
                return $context['data']['app_type'] !== 'MAIL';
            }
        );

        $this->addUniqueRule(
            $validator,
            'app_foreign_key',
            __("Cet utilisateur est déjà présent à cette étape de validation"),
            ['scope' => ['app_type', 'validation_stage_id']]
        );

        return $validator;
    }

    /**
     * Donne l'acteur d'un user selon l'étape (stage)
     * @param integer               $stage_id
     * @param integer               $saId
     * @param array|EntityInterface $user
     * @return Query
     */
    public function findByUser($stage_id, $saId, $user): Query
    {
        $userId = Hash::get($user, 'id');
        $orgEntityId = Hash::get($user, 'org_entity_id');
        $isValidator = Hash::get($user, 'is_validator');
        $conditions = [
            'validation_stage_id' => $stage_id,
            'OR' => [
                [
                    'ValidationActors.app_foreign_key' => $userId,
                    'ValidationActors.app_type' => 'USER',
                ],
            ],
        ];
        if ($isValidator && $orgEntityId === $saId) {
            $conditions['OR'][] = [
                'ValidationActors.app_type' => 'SERVICE_ARCHIVES',
            ];
        }
        return $this->find()->where($conditions);
    }
}
