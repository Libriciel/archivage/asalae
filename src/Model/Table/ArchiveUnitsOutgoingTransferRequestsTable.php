<?php

/**
 * Asalae\Model\Table\ArchiveUnitsOutgoingTransferRequestsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table archive_units_outgoing_transfer_requests
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveUnitsOutgoingTransferRequestsTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('ArchiveUnits');
        $this->belongsTo('OutgoingTransferRequests')
            ->setForeignKey('otr_id');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }
}
