<?php

/**
 * Asalae\Model\Table\BatchTreatmentsTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\DataType\JsonString;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\BeforeMarshalInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table batch_treatments
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @mixin StateMachineBehavior
 * @mixin TimestampBehavior
 */
class BatchTreatmentsTable extends Table implements BeforeMarshalInterface
{
    public const string T_CANCEL = 'cancel';
    public const string T_FAIL = 'fail';
    public const string T_PROCESS = 'process';
    public const string T_READY = 'ready';
    public const string T_RENEW = 'renew';
    public const string T_TERMINATE = 'terminate';

    public const string S_ABORTED = 'aborted';
    public const string S_CREATING = 'creating';
    public const string S_FINISHED = 'finished';
    public const string S_PROCESSING = 'processing';
    public const string S_READY = 'ready';

    /**
     * @var string Etat initial
     */
    public $initialState = self::S_CREATING;

    /**
     * @var array Changement d'êtats selon action
     */
    public $transitions = [
        self::T_READY => [
            self::S_CREATING => self::S_READY,
        ],
        self::T_PROCESS => [
            self::S_READY => self::S_PROCESSING,
        ],
        self::T_CANCEL => [
            self::S_PROCESSING => self::S_READY,
        ],
        self::T_FAIL => [
            self::S_PROCESSING => self::S_ABORTED,
            self::S_READY => self::S_ABORTED,
        ],
        self::T_RENEW => [
            self::S_ABORTED => self::S_READY,
        ],
        self::T_TERMINATE => [
            self::S_PROCESSING => self::S_FINISHED,
        ],
    ];

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    self::S_ABORTED,
                    self::S_CREATING,
                    self::S_FINISHED,
                    self::S_PROCESSING,
                    self::S_READY,
                ],
            ]
        );

        $this->belongsTo('ArchivalAgencies')
            ->setClassName('OrgEntities');
        $this->belongsTo('Users');
        $this->belongsToMany('ArchiveUnits');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->scalar('state_history')
            ->allowEmptyString('state_history');

        $validator
            ->dateTime('last_state_update')
            ->allowEmptyString('last_state_update');

        $validator
            ->scalar('data')
            ->requirePresence('data', 'create')
            ->notEmptyString('data');

        return $validator;
    }

    /**
     * The Model.beforeMarshall modify request data before it is converted into
     * entities.
     *
     * @param Event       $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     */
    public function beforeMarshal(
        Event $event,
        ArrayObject $data,
        ArrayObject $options
    ) {
        if (isset($data['data']) && is_array($data['data'])) {
            $data['data'] = (string)new JsonString($data['data']);
        }
    }
}
