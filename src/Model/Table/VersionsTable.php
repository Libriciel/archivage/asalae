<?php

/**
 * Asalae\Model\Table\VersionsTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Table\VersionsTable as CoreVersionsTable;
use Cake\Core\Configure;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\TableRegistry;

/**
 * Table versions
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class VersionsTable extends CoreVersionsTable implements SerializeDeletedEntitiesInterface
{
    /**
     * Serialize les entités avant suppression
     * @param mixed $conditions
     * @return int Returns the number of affected rows.
     */
    public function deleteAllSerialize(array $conditions)
    {
        return 0;
    }

    /**
     * Ajoute les versions liés au patchs stockés dans phinxlog
     */
    public function insertMissingVersions()
    {
        $migrations = glob(CONFIG . DS . 'Migrations' . DS . '*_Patch*.php');
        $Phinxlog = TableRegistry::getTableLocator()->get('Phinxlog');
        $appName = Configure::read('App.name');
        foreach ($migrations as $migration) {
            [$timestamp, $name] = explode('_', basename($migration, '.php'));
            $version = implode('.', str_split(str_replace('Patch', '', $name)));
            if (
                !$this->exists(
                    ['subject' => $appName, 'version' => $version]
                )
            ) {
                $phinx = $Phinxlog->find()
                    ->where(['version' => $timestamp])
                    ->first();
                if (!$phinx) {
                    continue;
                }
                $phinxDate = $phinx->get('end_time');
                $this->insertQuery()
                    ->insert(['subject', 'version', 'created'])
                    ->values(
                        [
                            'subject' => $appName,
                            'version' => $version,
                            'created' => $phinxDate,
                        ]
                    )
                    ->execute();
            }
        }
    }
}
