<?php

/**
 * Asalae\Model\Table\EaccpfsTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Behavior\AliasBehavior;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\EaccpfsTable as CoreEaccpfsTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table eaccpfs
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin OptionsBehavior
 * @mixin AliasBehavior
 */
class EaccpfsTable extends CoreEaccpfsTable
{
}
