<?php

/**
 * Asalae\Model\Table\CronExecutionsTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Table\CronExecutionsTable as CoreCronExecutionTable;

/**
 * Table cron_executions
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CronExecutionsTable extends CoreCronExecutionTable
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsTo('Crons');

        parent::initialize($config);
    }
}
