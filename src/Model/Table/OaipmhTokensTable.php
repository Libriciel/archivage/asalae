<?php

/**
 * Asalae\Model\Table\OaipmhTokensTable
 */

namespace Asalae\Model\Table;

use Asalae\Controller\OaipmhController;
use Asalae\Exception\OaipmhException;
use Asalae\Model\Entity\Archive;
use Asalae\Model\Entity\OaipmhArchive;
use Asalae\Model\Entity\OaipmhListset;
use Asalae\Model\Entity\OaipmhToken;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\LimitBreak;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Entity;
use Cake\ORM\Exception\PersistenceFailedException;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateInterval;
use DateTime;
use DOMDocument;
use DOMElement;
use DOMException;
use Exception;
use Faker\Provider\Uuid;

/**
 * Table oaipmh_tokens
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OaipmhTokensTable extends Table
{
    /**
     * Valeurs possibles de metadataPrefixes
     */
    public const array METADATA_PREFIXES = ['oai_dc', 'seda0.2', 'seda1.0', 'seda2.1', 'seda2.2'];

    /**
     * États d'une archive qui permettent de la voir pour l'Oai-Pmh
     */
    public const array ARCHIVE_VISIBLE_STATES = [
        'available',
        'destroying',
        'destroyed',
        'restituting',
        'restituted',
        'transferring',
        'transferred',
        'freezed',
    ];

    /**
     * Configuration initiale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->hasMany('OaipmhListsets')
            ->setDependent(true);
        $this->hasMany('OaipmhArchives')
            ->setDependent(true);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('value')
            ->maxLength('value', 255)
            ->requirePresence('value', 'create')
            ->notEmptyString('value');

        $validator
            ->scalar('verb')
            ->maxLength('verb', 255)
            ->requirePresence('verb', 'create')
            ->notEmptyString('verb');

        $validator
            ->scalar('metadata_prefix')
            ->maxLength('metadata_prefix', 255)
            ->allowEmptyString('metadata_prefix');

        $validator
            ->dateTime('expiration_date')
            ->requirePresence('expiration_date', 'create')
            ->notEmptyDateTime('expiration_date');

        $validator
            ->integer('complete_list_size')
            ->requirePresence('complete_list_size', 'create')
            ->notEmptyString('complete_list_size');

        $validator
            ->integer('cursor')
            ->requirePresence('cursor', 'create')
            ->notEmptyString('cursor');

        $validator
            ->integer('version')
            ->requirePresence('version', 'create')
            ->notEmptyString('version');

        return $validator;
    }

    /**
     * Retourne le resumptionToken ou false si absent
     *
     * @param string $value
     * @param string $verb
     * @param int    $version
     * @return OaipmhToken|false
     * @throws Exception
     */
    public function getTokenForVerb(string $value, string $verb, int $version)
    {
        /** @var OaipmhToken|null $token */
        $token = $this->find()
            ->where(
                [
                    'value' => $value,
                    'verb' => $verb,
                    'expiration_date >=' => new DateTime(),
                    'version' => $version,
                ]
            )
            ->first();

        return $token ?? false;
    }

    /**
     * Méthode générique pour les listes
     *
     * @param array            $params
     * @param OaipmhToken|null $token
     * @param string           $verb
     * @param DOMDocument      $dom
     * @throws Exception
     */
    public function listRequestForVerb(
        array $params,
        $token,
        string $verb,
        DOMDocument $dom
    ) {
        if ($token === null) {
            $this->initialRequestForVerb($verb, $params, $dom);
        } else {
            $this->tokenRequestForVerb($verb, $token, $params, $dom);
        }
    }

    /**
     * Récupère toutes les données,
     * enregistre les records si besoin de tokens, ainsi que le (V1) ou les (V2) tokens associés.
     *
     * @param string      $verb
     * @param array       $params
     * @param DOMDocument $dom
     * @throws Exception
     */
    public function initialRequestForVerb(
        string $verb,
        array $params,
        DOMDocument $dom
    ): void {
        $serviceId = $params['service_archive_id'];
        $version = $params['version'];

        $pagination = Configure::read('OAIPMH.maxListSize', 500);
        $query = $this->getQueryForVerb($verb, $serviceId, $params);

        $queryCount = $query instanceof Query ? $query->count() : count($query);
        if ($queryCount === 0 && in_array($verb, ['listIdentifiers', 'listRecords'])) {
            throw new OaipmhException(OaipmhException::NO_MATCH);
        }

        LimitBreak::setMemoryLimit($queryCount * 30000 + 1000000);

        /** @var OaipmhListsetsTable|OaipmhArchivesTable $recordTable */
        $recordTable = TableRegistry::getTableLocator()
            ->get($verb === 'listSets' ? 'OaipmhListsets' : 'OaipmhArchives');

        $element = $dom->createElementNS(
            OaipmhController::NS_OAIPMH,
            ucfirst($verb)
        );
        $dom->firstChild->appendChild($element);

        if ($queryCount <= $pagination) { // cas sans token
            /** @var OaipmhListset|Archive $result */
            foreach ($query as $result) {
                $result->addOaipmhXmlToDom(
                    $dom,
                    $verb,
                    $params['metadataPrefix'] ?? null
                );
            }
            return;
        }

        $i = 0;
        $firstResults = [];
        $prevToken = null;
        $duration = Configure::read('OAIPMH.expiration', 3600);
        $expiration = (new DateTime())->add(
            new DateInterval('PT' . $duration . 'S')
        );

        $this->getConnection()
            ->begin();
        /** @var EntityInterface $token */
        try {
            foreach ($query as $result) {
                if ($i < $pagination) {     // on ne sauvegarde ni crée de token pour les premiers résultats
                    $firstResults[] = $result;
                } else {
                    // création du token : à chaque fois en V2, seulement la première fois en V1
                    if ($i % $pagination === 0 && ($version === 2 || $i === $pagination)) {
                        $token = $this->newEntity(
                            [
                                'value' => Uuid::uuid(),
                                'verb' => $verb,
                                'metadata_prefix' => $params['metadataPrefix'] ?? null,
                                'expiration_date' => $expiration,
                                'complete_list_size' => $queryCount,
                                'cursor' => $i,
                                'version' => $version,
                            ]
                        ); // En v2 chaque token référencie son prédécesseur
                        if ($version === 2 && $prevToken !== null) {
                            /** @var EntityInterface $prevToken */
                            $token->set('prev_id', $prevToken->get('id'));
                        }
                        $prevToken = $token;
                        $this->saveOrFail($token);
                        $nextToken = $nextToken ?? $token;
                        // on a besoin du token suivant le 1er pour retourner sa valeur,
                        // en v1 ce sera le même token
                    }
                    // chaque record référencie son token
                    if ($verb === 'listSets') {
                        $result->set('oaipmh_token_id', $token->get('id'));
                        $record = $result;
                    } else {
                        $record = $recordTable->newEntity(
                            [
                                'oaipmh_token_id' => $token->get('id'),
                                'archive_id' => $result->get('id'),
                                'datetime' => $result->get('modified'),
                                'deleted' => $result->get(
                                    'state'
                                ) !== 'available',
                            ]
                        );
                    }
                    $recordTable->saveOrFail($record);
                    unset($record);
                }
                $i++;
            }
            $this->getConnection()
                ->commit();
        } catch (PersistenceFailedException $e) {
            $this->getConnection()
                ->rollback();
            throw $e;
        }

        /** @var OaipmhListset|Archive $result */
        foreach ($firstResults as $result) {
            $result->addOaipmhXmlToDom(
                $dom,
                $verb,
                $params['metadataPrefix'] ?? null
            );
        }
        $nextToken = $nextToken ?? new Entity();
        $xmlToken = $dom->createElementNS(
            OaipmhController::NS_OAIPMH,
            'resumptionToken'
        );
        $xmlToken->appendChild(DOMUtility::createDomTextNode($dom, $nextToken->get('value')));
        $expirationDate = $token->get('expiration_date')
            ->format(OaipmhController::OAIPMH_DATE_FORMAT);
        $xmlToken->setAttribute('expirationDate', $expirationDate);
        $xmlToken->setAttribute('completeListSize', $queryCount);
        $xmlToken->setAttribute('cursor', 0);
        $element->appendChild($xmlToken);
    }

    /**
     * Récupère la query en fonction du verbe.
     * Pour listSets, on aura directement un tableau d'entités car beaucoup moins de résultats
     *
     * @param string $verb
     * @param int    $serviceId
     * @param array  $params
     * @return array|OaipmhListset[]
     * @throws Exception
     */
    protected function getQueryForVerb(
        string $verb,
        int $serviceId,
        array $params
    ) {
        switch ($verb) {
            case 'listSets':
                $query = $this->findListSetsForService($serviceId);
                break;
            case 'listIdentifiers':
                $query = $this->findListIdentifiersForService(
                    $serviceId,
                    $params
                );
                break;
            case 'listRecords':
                $query = $this->findlistRecordsForService($serviceId, $params);
                break;
            default:
                throw new Exception(
                    'Unhandled verb : ' . $verb . ' for ' . __FUNCTION__
                );
        }

        return $query;
    }

    /**
     * Trouve les données pour chaque type de set possible
     *
     * @param int $serviceId
     * @return OaipmhListset[]
     */
    protected function findListSetsForService(int $serviceId): array
    {
        $sets = array_merge(
            $this->findOriginatingSets($serviceId),
            $this->findTransferringSets($serviceId),
            $this->findAgreementSets($serviceId),
            $this->findProfileSets($serviceId)
        );

        return TableRegistry::getTableLocator()
            ->get('OaipmhListsets')
            ->newEntities($sets);
    }

    /**
     * Originating sets
     *
     * @param int $serviceId
     * @return OaipmhListset[]
     */
    protected function findOriginatingSets(int $serviceId): array
    {
        $query = TableRegistry::getTableLocator()
            ->get('Archives')
            ->selectQuery()
            ->select(
                [
                    'Archives.originating_agency_id',
                    'OriginatingAgencies.name',
                    'OriginatingAgencies.identifier',
                ]
            )
            ->groupBy(
                [
                    'Archives.originating_agency_id',
                    'OriginatingAgencies.name',
                    'OriginatingAgencies.identifier',
                ]
            )
            ->contain('OriginatingAgencies')
            ->where(
                [
                    'Archives.archival_agency_id' => $serviceId,
                    'Archives.state IN' => self::ARCHIVE_VISIBLE_STATES,
                ]
            )
            ->orderBy(['Archives.originating_agency_id'])
            ->all()
            ->toArray();

        return array_map(
            function (EntityInterface $result): array {
                return [
                    'spec' => 'OriginatingAgency:'
                        . $this->encode(Hash::get($result, 'originating_agency.identifier')),
                    'name' => Hash::get($result, 'originating_agency.name'),
                ];
            },
            $query
        );
    }

    /**
     * Encodage des caractères spéciaux pour passer la validation xml de l'oaipmh
     * ([A-Za-z0-9-_.!~*'()] autorisés uniquement)
     * @param string $identifier
     * @return string
     */
    public function encode(string $identifier): string
    {
        return str_replace(
            '%',
            '~',
            str_replace(
                ['%21', '%2A', '%27', '%28', '%29'],
                ['!', '*', '\'', '(', ')'],
                rawurlencode($identifier)
            )
        );
    }

    /**
     * Transferring sets
     *
     * @param int $serviceId
     * @return OaipmhListset[]
     */
    protected function findTransferringSets(int $serviceId): array
    {
        $query = TableRegistry::getTableLocator()
            ->get('Archives')
            ->selectQuery()
            ->select(
                [
                    'Archives.transferring_agency_id',
                    'TransferringAgencies.name',
                    'TransferringAgencies.identifier',
                ]
            )
            ->groupBy(
                [
                    'Archives.transferring_agency_id',
                    'TransferringAgencies.name',
                    'TransferringAgencies.identifier',
                ]
            )
            ->contain('TransferringAgencies')
            ->where(
                [
                    'Archives.archival_agency_id' => $serviceId,
                    'Archives.state IN' => self::ARCHIVE_VISIBLE_STATES,
                ]
            )
            ->orderBy(['Archives.transferring_agency_id'])
            ->all()
            ->toArray();

        return array_map(
            function (EntityInterface $result): array {
                return [
                    'spec' => 'TransferringAgency:'
                        . $this->encode(Hash::get($result, 'transferring_agency.identifier')),
                    'name' => Hash::get($result, 'transferring_agency.name'),
                ];
            },
            $query
        );
    }

    /**
     * Agreements sets
     *
     * @param int $serviceId
     * @return OaipmhListset[]
     */
    protected function findAgreementSets(int $serviceId): array
    {
        $query = TableRegistry::getTableLocator()->get('Archives')
            ->selectQuery()
            ->select(
                [
                    'Archives.agreement_id',
                    'Agreements.name',
                    'Agreements.identifier',
                ]
            )
            ->groupBy(
                [
                    'Archives.agreement_id',
                    'Agreements.name',
                    'Agreements.identifier',
                ]
            )
            ->contain('Agreements')
            ->where(
                [
                    'Archives.agreement_id IS NOT NULL',
                    'archival_agency_id' => $serviceId,
                    'Archives.state IN' => self::ARCHIVE_VISIBLE_STATES,
                ]
            )
            ->orderBy(['Archives.agreement_id'])
            ->all()
            ->toArray();

        return array_map(
            function (EntityInterface $result): array {
                return [
                    'spec' => 'ArchivalAgreement:'
                        . $this->encode(Hash::get($result, 'agreement.identifier')),
                    'name' => Hash::get($result, 'agreement.name'),
                ];
            },
            $query
        );
    }

    /**
     * Profile sets
     *
     * @param int $serviceId
     * @return OaipmhListset[]
     */
    protected function findProfileSets(int $serviceId): array
    {
        $query = TableRegistry::getTableLocator()
            ->get('Archives')
            ->selectQuery()
            ->select(
                ['Archives.profile_id', 'Profiles.name', 'Profiles.identifier']
            )
            ->groupBy(
                ['Archives.profile_id', 'Profiles.name', 'Profiles.identifier']
            )
            ->contain('Profiles')
            ->where(
                [
                    'Archives.profile_id IS NOT NULL',
                    'archival_agency_id' => $serviceId,
                    'Archives.state IN' => self::ARCHIVE_VISIBLE_STATES,
                ]
            )
            ->orderBy(['Archives.profile_id'])
            ->all()
            ->toArray();

        return array_map(
            function (EntityInterface $result): array {
                return [
                    'spec' => 'ArchivalProfile:'
                        . $this->encode(Hash::get($result, 'profile.identifier')),
                    'name' => Hash::get($result, 'profile.name'),
                ];
            },
            $query
        );
    }

    /**
     * Liste des identifiants des archives (cotes)
     *
     * @param int   $serviceId
     * @param array $params
     * @return Query
     * @throws OaipmhException
     */
    protected function findListIdentifiersForService(
        int $serviceId,
        array $params
    ): Query {
        $metadataPrefix = $this->extractMetadataPrefix($params);
        $from = $this->extractFromParam($params);
        $until = $this->extractUntilParam($params);
        $set = $this->extractSetParam($params);

        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()
            ->get('Archives');
        $query = $Archives->find()
            ->select(
                [
                    'Archives.id',
                    'Archives.state',
                    'Archives.archival_agency_identifier',
                    'Archives.modified',
                ]
            )
            ->where(
                [
                    'Archives.archival_agency_id' => $serviceId,
                    'Archives.state IN' => self::ARCHIVE_VISIBLE_STATES,
                ]
            );

        if ($metadataPrefix !== 'oai_dc') {
            $query->innerJoinWith('Transfers')
                ->where(['Transfers.message_version' => $metadataPrefix]);
        }
        if ($from !== null) {
            $query->where(['Archives.modified >=' => $from]);
        }
        if ($until !== null) {
            $query->where(['Archives.modified <=' => $until]);
        }
        if ($set !== null) {
            $contain = [
                'OriginatingAgency' => 'ArchivalAgencies',
                'TransferringAgency' => 'TransferringAgencies',
                'ArchivalAgreement' => 'Agreements',
                'ArchivalProfile' => 'Profiles',
            ][$set[0]];
            $query->where(
                [
                    $contain . '.identifier' => $set[1],
                ]
            )
                ->contain($contain);
        }

        return $query->orderBy(['Archives.id']);
    }

    /**
     * Extract the metadataPrefix parameter and remove it from the param array
     * Also check if the value is correct
     *
     * @param array $params
     * @return string
     * @throws OaipmhException
     */
    protected function extractMetadataPrefix(array &$params): string
    {
        if (!isset($params['metadataPrefix'])) {
            throw new OaipmhException(OaipmhException::MISSING_METADATAPREFIX);
        }
        if (!in_array($params['metadataPrefix'], self::METADATA_PREFIXES)) {
            throw new OaipmhException(
                OaipmhException::CANNOT_DISSEMINATE_FORMAT
            );
        }

        $metadataPrefix = (string)$params['metadataPrefix'];
        unset($params['metadataPrefix']);
        return $metadataPrefix;
    }

    /**
     * Extract the from parameter, convert it and remove it from the param array
     *
     * @param array $params
     * @return DateTime|null
     * @throws OaipmhException
     */
    protected function extractFromParam(array &$params): ?DateTime
    {
        $from = empty($params['from'])
            ? null
            : DateTime::createFromFormat(
                OaipmhController::OAIPMH_DATE_FORMAT,
                $params['from']
            );
        if ($from === false) {
            throw new OaipmhException(OaipmhException::ILLEGAL_FROM);
        }
        unset($params['from']);
        return $from;
    }

    /**
     * Extract the until parameter, convert it and remove it from the param array
     *
     * @param array $params
     * @return DateTime|null
     * @throws OaipmhException
     */
    protected function extractUntilParam(array &$params): ?DateTime
    {
        $until = empty($params['until'])
            ? null
            : DateTime::createFromFormat(
                OaipmhController::OAIPMH_DATE_FORMAT,
                $params['until']
            );
        if ($until === false) {
            throw new OaipmhException(OaipmhException::ILLEGAL_UNTIL);
        }
        unset($params['until']);
        return $until;
    }

    /**
     * @param array $params
     * @return array|null
     * @throws OaipmhException
     */
    protected function extractSetParam(array &$params): ?array
    {
        $set = empty($params['set'])
            ? null
            : explode(':', $params['set'], 2);
        if (
            $set !== null
            && (count($set) !== 2
            || !in_array(
                $set[0],
                [
                    'ArchivalAgreement',
                    'ArchivalProfile',
                    'OriginatingAgency',
                    'TransferringAgency',
                ]
            ))
        ) {
            throw new OaipmhException(OaipmhException::ILLEGAL_SET);
        }

        if ($set) {
            $set[1] = $this->decode($set[1]);
        }

        unset($params['set']);
        return $set;
    }

    /**
     * Décodage depuis l'HTML "tildé"
     * @param string $identifier
     * @return string
     */
    public function decode(string $identifier): string
    {
        return rawurldecode(
            preg_replace('/~([\dA-F]{2})/', '%$1', $identifier)
        );
    }

    /**
     * Liste des descriptions des archives (cotes)
     *
     * @param int   $serviceId
     * @param array $params
     * @return Query
     * @throws OaipmhException
     */
    public function findlistRecordsForService(
        int $serviceId,
        array $params
    ): Query {
        $metadataPrefix = $this->extractMetadataPrefix($params);
        $from = $this->extractFromParam($params);
        $until = $this->extractUntilParam($params);
        $set = $this->extractSetParam($params);

        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()
            ->get('Archives');
        $query = $Archives->find()
            ->contain('DescriptionXmlArchiveFiles')
            ->where(
                [
                    'Archives.archival_agency_id' => $serviceId,
                    'Archives.state IN' => self::ARCHIVE_VISIBLE_STATES,
                ]
            );

        if ($metadataPrefix !== 'oai_dc') {
            $query->innerJoinWith('Transfers')
                ->where(['Transfers.message_version' => $metadataPrefix]);
        }
        if ($from !== null) {
            $query->where(['Archives.modified >=' => $from]);
        }
        if ($until !== null) {
            $query->where(['Archives.modified <=' => $until]);
        }
        if ($set !== null) {
            $contain = [
                'OriginatingAgency' => 'OriginatingAgencies',
                'TransferringAgency' => 'TransferringAgencies',
                'ArchivalAgreement' => 'Agreements',
                'ArchivalProfile' => 'Profiles',
            ][$set[0]];
            $query->where(
                [
                    $contain . '.identifier' => $set[1],
                    'Archives.archival_agency_id' => $serviceId,
                ]
            )
                ->contain($contain);
        }
        return $query->orderBy(['Archives.id']);
    }

    /**
     * @param string      $verb
     * @param OaipmhToken $token
     * @param array       $params
     * @param DOMDocument $dom
     * @throws Exception
     */
    protected function tokenRequestForVerb(
        string $verb,
        OaipmhToken $token,
        array $params,
        DOMDocument $dom
    ) {
        $version = $params['version'];
        /** @var OaipmhListsetsTable|OaipmhArchivesTable $OaipmhRecords */
        $tableName = $verb === 'listSets' ? 'OaipmhListsets' : 'OaipmhArchives';
        $OaipmhRecords = TableRegistry::getTableLocator()
            ->get($tableName);

        $query = $OaipmhRecords->find();

        if ($tableName === 'OaipmhArchives') {
            $query->contain(
                [
                    'Archives' => ['DescriptionXmlArchiveFiles'],
                    'OaipmhTokens',
                ]
            );
        }
        $query->where([$tableName . '.oaipmh_token_id' => $token->get('id')])
            ->orderBy($tableName . '.id');

        if ($version === 1) {
            $limit = Configure::read('OAIPMH.maxListSize', 500);
            // les premiers résultats n'ont pas été sauvegardés puisque retournés directement, donc ($cursor - $limit)
            $offset = $token->get('cursor') - $limit;
            $query->limit($limit)
                ->offset($offset);
        }

        $element = $dom->createElementNS(
            OaipmhController::NS_OAIPMH,
            ucfirst($verb)
        );
        $dom->firstChild->appendChild($element);

        /** @var OaipmhListset|OaipmhArchive $result */
        foreach (
            $query->all()
                ->toArray() as $result
        ) {
            $result->addOaipmhXmlToDom(
                $dom,
                $verb,
                $token->get('metadata_prefix')
            );
        }

        $element->appendChild($this->updateToken($token, $version, $dom));
    }

    /**
     * Met à jour le token et en retourne le XML
     *
     * @param OaipmhToken $token
     * @param int         $version
     * @param DOMDocument $dom
     * @return DOMElement
     * @throws DOMException
     */
    public function updateToken(
        OaipmhToken $token,
        int $version,
        DOMDocument $dom
    ): DOMElement {
        return $version === 1
            ? $this->updateTokenV1($token, $dom)
            : $this->updateTokenV2($token, $dom);
    }

    /**
     * Ancienne version de l'update : le même token avec cursor incrémenté
     *
     * @param OaipmhToken $token
     * @param DOMDocument $dom
     * @return DOMElement
     * @throws DOMException
     */
    protected function updateTokenV1(
        OaipmhToken $token,
        DOMDocument $dom
    ): DOMElement {
        $newCursor = $token->get('cursor') + Configure::read(
            'OAIPMH.maxListSize',
            500
        );
        $isOver = $newCursor >= $token->get('complete_list_size');

        $value = $isOver ? null : $token->get('value');
        $cursor = $token->get('cursor');

        $node = $dom->createElementNS(
            OaipmhController::NS_OAIPMH,
            'resumptionToken'
        );
        $node->appendChild(DOMUtility::createDomTextNode($dom, $value));
        $node->setAttribute(
            'completeListSize',
            $token->get('complete_list_size')
        );
        $node->setAttribute('cursor', $cursor);
        $expirationDate = $token->get('expiration_date')
            ->format(OaipmhController::OAIPMH_DATE_FORMAT);
        $node->setAttribute('expirationDate', $expirationDate);

        if ($isOver) {
            $this->deleteOrFail($token);
        } else {
            $token->set('cursor', $newCursor);
            $this->saveOrFail($token);
        }

        return $node;
    }

    /**
     * Version idempotent de l'update :
     * Récupération de l'éventuel token suivant. S'il y en a un, on utilise sa valeur,
     * sinon valeur vide et pas d'attribut expirationDate.
     *
     * @param OaipmhToken $token
     * @param DOMDocument $dom
     * @return DOMElement
     * @throws DOMException
     */
    protected function updateTokenV2(
        OaipmhToken $token,
        DOMDocument $dom
    ): DOMElement {
        $nextToken = $this->getNextToken($token);
        $value = $nextToken?->get('value');

        $node = $dom->createElementNS(
            OaipmhController::NS_OAIPMH,
            'resumptionToken'
        );
        $node->appendChild(DOMUtility::createDomTextNode($dom, $value));
        $node->setAttribute(
            'completeListSize',
            $token->get('complete_list_size')
        );
        $node->setAttribute('cursor', $token->get('cursor'));
        $expirationDate = $token->get('expiration_date')
            ->format(OaipmhController::OAIPMH_DATE_FORMAT);
        $node->setAttribute('expirationDate', $expirationDate);

        return $node;
    }

    /**
     * For Idempotent
     *
     * @param OaipmhToken|EntityInterface $token
     * @return array|EntityInterface|null
     */
    protected function getNextToken(EntityInterface $token)
    {
        return $this->find()
            ->where(['prev_id' => $token->get('id')])
            ->first();
    }

    /**
     * @param int   $serviceId
     * @param array $params
     * @return EntityInterface|null
     * @throws OaipmhException
     */
    public function getRecordForService(int $serviceId, array $params): ?EntityInterface
    {
        if (!isset($params['identifier'])) {
            throw new OaipmhException(OaipmhException::MISSING_IDENTIFIER);
        }
        $identifier = $params['identifier'];
        unset($params['identifier']);

        $metadataPrefix = $this->extractMetadataPrefix($params);

        if (count($params)) {
            throw new OaipmhException(
                OaipmhException::INCLUDE_ILLEGAL_ARGUMENTS
            );
        }

        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()
            ->get('Archives');
        $query = $Archives->find()
            ->where(
                [
                    'Archives.archival_agency_id' => $serviceId,
                    'Archives.archival_agency_identifier' => $identifier,
                    'Archives.state IN' => self::ARCHIVE_VISIBLE_STATES,
                ]
            );

        if ($metadataPrefix !== 'oai_dc') {
            $query->contain(
                [
                    'Transfers',
                    'DescriptionXmlArchiveFiles',
                ]
            )
                ->innerJoinWith('Transfers')
                ->where(['Transfers.message_version' => $metadataPrefix]);
        }

        return $query->first();
    }
}
