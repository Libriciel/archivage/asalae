<?php

/**
 * Asalae\Model\Table\ArchiveBinariesArchiveUnitsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table archive_binaries_archive_units
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveBinariesArchiveUnitsTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('ArchiveBinaries');
        $this->belongsTo('ArchiveUnits');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }
}
