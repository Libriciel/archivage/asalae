<?php

/**
 * Asalae\Model\Table\TransfersTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Form\MessageSchema\MessageSchema;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Model\Table\BeforeSaveInterface;
use AsalaeCore\Model\Table\StateChangeInterface;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\PreControlMessages;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Http\Exception\NotImplementedException;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\HasOne;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateTime;
use DOMDocument;
use DOMElement;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table transfers
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017-2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin OptionsBehavior
 * @mixin StateMachineBehavior
 * @property AgreementsTable|BelongsTo        Agreements
 * @property ArchivesTable|BelongsToMany      Archives
 * @property ArchivesTransfersTable|HasMany   ArchivesTransfers
 * @property OrgEntitiesTable|BelongsTo       ArchivalAgencies
 * @property OrgEntitiesTable|BelongsTo       OriginatingAgencies
 * @property OrgEntitiesTable|BelongsTo       TransferringAgencies
 * @property ProfilesTable|BelongsTo          Profiles
 * @property ServiceLevelsTable|BelongsTo     ServiceLevels
 * @property TransferAttachmentsTable|HasMany TransferAttachments
 * @property TransferLocksTable|HasOne        TransferLocks
 */
class TransfersTable extends Table implements
    BeforeDeleteInterface,
    AfterDeleteInterface,
    AfterSaveInterface,
    BeforeSaveInterface,
    StateChangeInterface,
    SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    public const string S_CREATING = 'creating';
    public const string S_PREPARATING = 'preparating';
    public const string S_TIMESTAMPING = 'timestamping';
    public const string S_ANALYSING = 'analysing';
    public const string S_CONTROLLING = 'controlling';
    public const string S_VALIDATING = 'validating';
    public const string S_ARCHIVING = 'archiving';
    public const string S_REJECTED = 'rejected';
    public const string S_ACCEPTED = 'accepted';

    public const string T_PREPARE = 'prepare';
    public const string T_TIMESTAMP = 'timestamp';
    public const string T_ANALYSE = 'analyse';
    public const string T_CONTROL = 'control';
    public const string T_VALIDATE = 'validate';
    public const string T_ARCHIVE = 'archive';
    public const string T_REFUSE = 'refuse';
    public const string T_ACCEPT = 'accept';
    public const string T_IMPORT = 'import';
    /**
     * @var array Liste des normes seda avec leurs namespaces
     */
    public const array NORMES_SEDA = [
        'fr:gouv:ae:archive:draft:standard_echange_v0.2' => 'seda0.2',
        'fr:gouv:culture:archivesdefrance:seda:v1.0' => 'seda1.0',
        'fr:gouv:culture:archivesdefrance:seda:v2.0' => 'seda2.0',
        'fr:gouv:culture:archivesdefrance:seda:v2.1' => 'seda2.1',
        'fr:gouv:culture:archivesdefrance:seda:v2.2' => 'seda2.2',
    ];
    /**
     * @var string Etat initial
     */
    public $initialState = self::S_CREATING;
    /**
     * @var array Changement d'êtats selon action
     */
    public $transitions = [
        self::T_PREPARE => [
            self::S_CREATING => self::S_PREPARATING,
            self::S_REJECTED => self::S_PREPARATING,
        ],
        self::T_TIMESTAMP => [
            self::S_PREPARATING => self::S_TIMESTAMPING,
        ],
        self::T_ANALYSE => [
            self::S_PREPARATING => self::S_ANALYSING,
            self::S_TIMESTAMPING => self::S_ANALYSING,
        ],
        self::T_CONTROL => [
            self::S_ANALYSING => self::S_CONTROLLING,
        ],
        self::T_VALIDATE => [
            self::S_CONTROLLING => self::S_VALIDATING,
        ],
        self::T_ARCHIVE => [
            self::S_VALIDATING => self::S_ARCHIVING,
            self::S_CONTROLLING => self::S_ARCHIVING,
        ],
        self::T_REFUSE => [
            self::S_VALIDATING => self::S_REJECTED,
            self::S_CONTROLLING => self::S_REJECTED,
        ],
        self::T_ACCEPT => [
            self::S_ARCHIVING => self::S_ACCEPTED,
        ],
        self::T_IMPORT => [
            self::S_CREATING => self::S_ACCEPTED,
        ],
    ];

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior(
            'StateMachine.StateMachine',
            [
                'fields' => [
                    'states_history' => 'state_history',
                    // FIXME renommer le champ
                ],
            ]
        );
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'message_version' => [
                    'seda0.2',
                    'seda1.0',
                    'seda2.0',
                    'seda2.1',
                    'seda2.2',
                ],
                'state' => [
                    self::S_CREATING,
                    self::S_PREPARATING,
                    self::S_TIMESTAMPING,
                    self::S_ANALYSING,
                    self::S_CONTROLLING,
                    self::S_VALIDATING,
                    self::S_ARCHIVING,
                    self::S_REJECTED,
                    self::S_ACCEPTED,
                ],
            ]
        );

        $this->belongsToMany('Archives');
        $this->hasMany('ArchivesTransfers');
        $this->belongsTo('ArchivalAgencies')
            ->setClassName('OrgEntities')
            ->setForeignKey('archival_agency_id')
            ->setJoinType('INNER');
        $this->belongsTo('TransferringAgencies')
            ->setClassName('OrgEntities')
            ->setForeignKey('transferring_agency_id')
            ->setJoinType('INNER');
        $this->belongsTo('OriginatingAgencies')
            ->setClassName('OrgEntities')
            ->setForeignKey('originating_agency_id')
            ->setJoinType('INNER');
        $this->belongsTo('OrgEntities')
            ->setForeignKey(false)
            ->setConditions(
                [
                    'OrgEntities.id IN' => [
                        new IdentifierExpression('Transfers.archival_agency_id'),
                        new IdentifierExpression('Transfers.transferring_agency_id'),
                        new IdentifierExpression('Transfers.originating_agency_id'),
                    ],
                ]
            );
        $this->belongsTo('Agreements');
        $this->belongsTo('Profiles');
        $this->belongsTo('ServiceLevels');
        $this->belongsTo('CreatedUsers')
            ->setClassName('Users')
            ->setForeignKey('created_user_id');
        $this->belongsTo('Users')
            ->setForeignKey('created_user_id');
        $this->hasMany('TransferAttachments')->setDependent(true);
        $this->hasMany('TransferErrors')->setDependent(true);
        $this->hasMany('CompositeArchiveUnits');
        $this->hasOne('TransferLocks')->setDependent(true);
        $this->hasMany('ValidationProcesses')
            ->setForeignKey('app_foreign_key')
            ->setConditions(
                ['ValidationProcesses.app_subject_type' => 'Transfers']
            );
        $this->hasMany('EventLogs')
            ->setForeignKey('object_foreign_key')
            ->setConditions(['EventLogs.object_model' => 'Transfers']);
    }

    /**
     * Validation pour l'étape 1 de la création du transfert par formulaire
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationCreate(Validator $validator): Validator
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->requirePresence('start_date', 'create')
            ->date('start_date');

        $validator
            ->notEmptyDate(
                'final_start_date',
                null,
                function ($context) {
                    return (bool)Hash::get($context, 'data.final');
                }
            )
            ->date('final_start_date');

        return $validator;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('transfer_comment')
            ->allowEmptyString('transfer_comment');

        $validator
            ->requirePresence('transfer_date', 'create');

        $validator
            ->scalar('transfer_identifier')
            ->maxLength('transfer_identifier', 255)
            ->requirePresence('transfer_identifier', 'create')
            ->allowEmptyString('transfer_identifier')
            ->add(
                'transfer_identifier',
                'not_like_autoidentifier',
                [
                    'rule' => function ($value, $context) {
                        $archival_agency_id = Hash::get(
                            $context,
                            'data.archival_agency_id'
                        );
                        if (
                            !Hash::get(
                                $context,
                                'data.manual_identifier'
                            ) || !$archival_agency_id
                        ) {
                            return true;
                        }
                        $def = TableRegistry::getTableLocator()->get('Counters')
                            ->find()
                            ->select('definition_mask')
                            ->where(
                                [
                                    'org_entity_id' => $archival_agency_id,
                                    'identifier' => 'ArchiveTransfer',
                                ]
                            )
                            ->firstOrFail()
                            ->get('definition_mask');
                        return !preg_match(
                            '/' . preg_replace('/#[^#]+#/', '.+', $def) . '$/',
                            $value
                        );
                    },
                    'message' => __(
                        "Cette forme d'identifiant est réservé aux identifiants auto"
                    ),
                ]
            );

        $validator
            ->scalar('message_version')
            ->maxLength('message_version', 255)
            ->requirePresence('message_version', 'create')
            ->allowEmptyString('message_version');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->allowEmptyString('state');

        $validator
            ->scalar('state_history')
            ->allowEmptyString('state_history');

        $validator
            ->dateTime('last_state_update')
            ->allowEmptyDateTime('last_state_update');

        $validator
            ->boolean('is_conform')
            ->allowEmptyString('is_conform');

        $validator
            ->boolean('is_modified')
            ->requirePresence('is_modified', 'create')
            ->allowEmptyString('is_modified');

        $validator
            ->boolean('is_accepted')
            ->allowEmptyString('is_accepted');

        $validator
            ->scalar('filename')
            ->maxLength('filename', 255)
            ->requirePresence('filename', 'create')
            ->allowEmptyString('filename');

        $validator
            ->integer('data_size')
            ->allowEmptyString('data_size');

        $validator
            ->requirePresence('archival_agency_id', 'create');

        $validator
            ->requirePresence('transferring_agency_id', 'create');

        $validator
            ->requirePresence('created_user_id', 'create')
            ->add(
                'created_user_id',
                'valid_sa',
                [
                    'rule' => function ($value, $context) {
                        $saId = Hash::get($context, 'data.archival_agency_id');
                        if (!Hash::get($context, 'newRecord')) {
                            return true;
                        }
                        $Users = TableRegistry::getTableLocator()->get('Users');
                        $user = $Users->find()
                            ->select(
                                [
                                    'archival_agency_id' => 'ArchivalAgencies.id',
                                    'type_entity' => 'TypeEntities.code',
                                ]
                            )
                            ->innerJoinWith('OrgEntities')
                            ->innerJoinWith('OrgEntities.TypeEntities')
                            ->leftJoinWith('OrgEntities.ArchivalAgencies')
                            ->where(['Users.id' => $value])
                            ->disableHydration()
                            ->first();
                        return $user
                            && (Hash::get($user, 'archival_agency_id') === $saId
                                || Hash::get($user, 'type_entity') === 'SE');
                    },
                    'message' => __(
                        "Le service d'archives indiqué dans le bordereau ne 
                        correspond pas avec celui de l'utilisateur courant"
                    ),
                ]
            );

        $validator
            ->boolean('files_deleted')
            ->allowEmptyString('files_deleted');

        $validator
            ->integer('data_count')
            ->allowEmptyString('data_count', null, 'create');

        return $validator;
    }

    /**
     * Get the different message_version from a given archive service
     *
     * @param int $serviceArchiveId
     * @return string[]
     */
    public function getMessageVersions(int $serviceArchiveId): array
    {
        $query = $this->find()
            ->distinct('message_version')
            ->select(['message_version'])
            ->where(['archival_agency_id' => $serviceArchiveId])
            ->all()
            ->toArray();
        return Hash::extract($query, '{n}.message_version');
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $dir = AbstractGenericMessageForm::getDataDirectory($entity->get('id'));
        if (is_dir($dir)) {
            Filesystem::begin('delete-transfer');
            Filesystem::remove($dir);
            Filesystem::commit('delete-transfer');
        }
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     * Transfers.data_size update except if state === accepted (retention)
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $ValidationProcesses = TableRegistry::getTableLocator()->get('ValidationProcesses');
        $validationProcess = $ValidationProcesses->find()
            ->where(
                [
                    'app_foreign_key' => $entity->get('id'),
                    'app_subject_type' => 'Transfers',
                ]
            )
            ->first();

        if (!$validationProcess) {
            return;
        }

        $ValidationHistories = TableRegistry::getTableLocator()->get('ValidationHistories');
        $ValidationHistories->deleteAll(['validation_process_id' => $validationProcess->get('id')]);

        $ValidationProcesses->delete($validationProcess);
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function afterSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        if ($entity->isNew() && $entity->get('created_user_id')) {
            $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
            $file = $Fileuploads->find()->where(
                [
                    'user_id' => $entity->get('created_user_id'),
                    'name' => $entity->get('filename'),
                ]
            )->first();
            if ($file) {
                Filesystem::begin('transfers-aftersave');
                Filesystem::copy($file->get('path'), $entity->get('xml'));
                $Fileuploads->delete($file);
                Filesystem::commit('transfers-aftersave');
            }
        } else {
            $zip = $entity->get('zip');
            if (is_file($zip)) {
                unlink($zip);
            }
            $pdf = $entity->get('pdf');
            if (is_file($pdf)) {
                unlink($pdf);
            }
        }
    }

    /**
     * Reporte les modifications du xml sur les valeurs de champs de l'entité
     * @param EntityInterface $entity
     * @return EntityInterface
     * @throws \Exception
     */
    public function applyChanges(EntityInterface $entity)
    {
        $data = $this->newEntityFromXml($entity->get('xml'))->toArray();
        $this->patchEntity(
            $entity,
            [
                'transfer_comment' => $data['transfer_comment'],
                'transfer_date' => $data['transfer_date'],
                'transfer_identifier' => $data['transfer_identifier'],
                'transferring_agency_id' => $data['transferring_agency_id'],
                'originating_agency_id' => $data['originating_agency_id'],
                'agreement_id' => $data['agreement_id'],
                'profile_id' => $data['profile_id'],
                'service_level_id' => $data['service_level_id'],
            ]
        );
        return $entity;
    }

    /**
     * Créer une entité à partir d'un chemin vers un fichier xml
     *
     * @param string $xmlPath
     * @return EntityInterface
     * @throws \Exception
     */
    public function newEntityFromXml(string $xmlPath): EntityInterface
    {
        if (!is_file($xmlPath)) {
            throw new \Exception(__("Le fichier indiqué n'existe pas"));
        }

        $norme = $this->detectMessageVersion($xmlPath);
        if (!$norme) {
            return $this->newEntity(
                ['message_version' => '', 'filename' => basename($xmlPath)]
            );
        }
        $util = DOMUtility::load($xmlPath);
        $preControl = new PreControlMessages($xmlPath);
        $ns = $util->namespace;

        /**
         * mapping
         */
        $date = $util->xpath->query($preControl->datePaths[$ns])->item(0);
        $comment = $util->xpath->query($preControl->commentPaths[$ns])->item(0);
        $identifier = $util->xpath->query($preControl->identifierIdPaths[$ns])->item(0);
        $sa = $util->xpath->query($preControl->identifierArchivalAgencyPaths[$ns])->item(0);
        $sv = $util->xpath->query($preControl->identifierTransferringAgencyPaths[$ns])->item(0);
        $sp = $util->xpath->query($preControl->identifierOriginatingAgencyPaths[$ns])->item(0);
        $sl = $util->xpath->query('//ns:ServiceLevel[1]')->item(0);
        $ag = $util->xpath->query('//ns:ArchivalAgreement[1]')->item(0);
        $pr = $util->xpath->query('//ns:ArchivalProfile[1]')->item(0);

        $data = [
            'filename' => basename($xmlPath),
            'message_version' => $norme,
            'state' => $this->initialState,
            'is_modified' => false,
            'transfer_date' => $date ? new DateTime($date->nodeValue) : null,
            'transfer_comment' => $comment?->nodeValue,
            'transfer_identifier' => $identifier?->nodeValue,
            'sa' => $sa?->nodeValue,
            'sv' => $sv?->nodeValue,
            'sp' => $sp?->nodeValue,
            'sl' => $sl?->nodeValue,
            'ag' => $ag?->nodeValue,
            'pr' => $pr?->nodeValue,
            'archival_agency_id' => null,
            'transferring_agency_id' => null,
            'originating_agency_id' => null,
            'service_level_id' => null,
            'agreement_id' => null,
            'profile_id' => null,
        ];
        $sa = $this->ArchivalAgencies->find()
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'ArchivalAgencies.identifier' => $data['sa'],
                    'TypeEntities.code' => 'SA',
                ]
            )
            ->first();
        if ($sa) {
            $data['archival_agency_id'] = $sa->get('id');
            $sv = $this->TransferringAgencies->find()
                ->where(
                    [
                        'identifier' => $data['sv'],
                        'lft >=' => $sa->get('lft'),
                        'rght <=' => $sa->get('rght'),
                    ]
                )
                ->first();
            $data['transferring_agency_id'] = $sv ? $sv->get('id') : null;
            $sp = $this->OriginatingAgencies->find()
                ->where(
                    [
                        'identifier' => $data['sp'] ?: $data['sv'],
                        'lft >=' => $sa->get('lft'),
                        'rght <=' => $sa->get('rght'),
                    ]
                )
                ->first();
            $data['originating_agency_id'] = $sp ? $sp->get('id') : $data['transferring_agency_id'];

            if ($data['sl']) {
                $sl = $this->ServiceLevels->find()
                    ->where(
                        [
                            'org_entity_id' => $sa->get('id'),
                            'identifier' => $data['sl'],
                        ]
                    )
                    ->first();
                $data['service_level_id'] = $sl ? $sl->get('id') : null;
            }
            if ($data['ag']) {
                $ag = $this->Agreements->find()
                    ->where(
                        [
                            'org_entity_id' => $sa->get('id'),
                            'identifier' => $data['ag'],
                        ]
                    )
                    ->first();
                $data['agreement_id'] = $ag ? $ag->get('id') : null;
            }
            if ($data['pr']) {
                $pr = $this->Profiles->find()
                    ->where(
                        [
                            'org_entity_id' => $sa->get('id'),
                            'identifier' => $data['pr'],
                        ]
                    )
                    ->first();
                $data['profile_id'] = $pr ? $pr->get('id') : null;
            }
            unset($data['sl'], $data['ag'], $data['pr']);// SimpleXMLElement = impossible à serialiser l'entité
        }

        return $this->newEntity($data, ['validate' => false]);
    }

    /**
     * Envoi la version de norme compatible avec le xml fourni
     *
     * @param string $xmlPath
     * @return string|false Envoi faux si non compatible avec une norme connue
     * @throws \Exception
     */
    public function detectMessageVersion(string $xmlPath)
    {
        $xml = new DOMDocument();
        if (!@$xml->load($xmlPath)) {
            return false;
        }
        $root = $xml->documentElement;
        $namespace = ($xmlns = $root->getAttributeNode('xmlns'))
            ? $xmlns->nodeValue : null;
        if ($namespace && isset(self::NORMES_SEDA[$namespace])) {
            return self::NORMES_SEDA[$namespace];
        }
        return false;
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        if (
            $entity->isNew()
            && empty($options['auto_identifier'])
            && is_file($entity->get('tmp_xml') ?: '')
        ) {
            $this->autoIdentifier($entity);
        }
    }

    /**
     * legacy: #A_CALCULER_LORS_DU_VERROUILLAGE# donne un identifiant généré
     * Modifi le fichier xml source et sauvegarde l'entité
     * @param EntityInterface $entity
     * @return string nouvel identifiant
     * @throws \Exception
     */
    public function autoIdentifier(EntityInterface $entity): string
    {
        if (
            $entity->get(
                'transfer_identifier'
            ) !== '#A_CALCULER_LORS_DU_VERROUILLAGE#'
        ) {
            return '';
        }
        $file = $entity->get('tmp_xml');
        if (!is_file($file)) {
            throw new \Exception('File not found');
        }
        $preControl = new PreControlMessages($file);
        $sa = $preControl->getArchivalAgency();
        if (!$sa) {
            throw new \Exception('No ArchivalAgency found');
        }
        /** @var CountersTable $Counters */
        $Counters = TableRegistry::getTableLocator()->get('Counters');
        $identifier = $Counters->next(
            $sa->id,
            'ArchiveTransfer',
            [],
            function ($generated) use ($sa) {
                $cond = [
                    'transfer_identifier' => $generated,
                    'archival_agency_id' => $sa->id,
                ];
                return !$this->exists($cond);
            }
        );
        $node = $preControl->getTransferIdentifier(true);
        if (!$node) {
            throw new \Exception('No identifier found');
        }
        DOMUtility::setValue($node, $identifier);
        if (!$preControl->getDom()->save($file)) {
            throw new \Exception('XML save failed');
        }
        $this->saveOrFail(
            $entity->set('transfer_identifier', $identifier),
            ['auto_identifier' => true]
        );
        return $identifier;
    }

    /**
     * Donne le filename lié à un Document
     * @param EntityInterface $transfer
     * @param DOMElement      $element
     * @return string|null
     */
    public function extractFilename(
        EntityInterface $transfer,
        DOMElement $element
    ) {
        switch (
            MessageSchema::getNamespace(
                $transfer->get('message_version')
            )
        ) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                if ($element->tagName !== 'Document') {
                    return null;
                }
                /** @var DOMElement $filename */
                $filename = $element->getElementsByTagName('Attachment')->item(
                    0
                );
                return $filename->getAttribute('filename');
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                if ($element->tagName !== 'BinaryDataObject') {
                    return null;
                }
                $filename = $element->getElementsByTagName('Filename')->item(0);
                return $filename?->nodeValue;
            default:
                throw new NotImplementedException();
        }
    }

    /**
     * Appelé après la transition
     * @param EntityInterface $entity
     * @param string          $state
     * @throws Exception
     */
    public function onStateChange(EntityInterface $entity, string $state)
    {
        if ($state === self::S_ACCEPTED || $state === self::S_REJECTED) {
            $util = DOMUtility::load($entity->get('xml'));
            switch ($util->namespace) {
                case NAMESPACE_SEDA_02:
                    $originatingAgencyNode = $util->node(
                        '/ns:ArchiveTransfer/ns:Contains/ns:ContentDescription/ns:OriginatingAgency/ns:Name'
                    );
                    break;
                case NAMESPACE_SEDA_10:
                    $originatingAgencyNode = $util->node(
                        '/ns:ArchiveTransfer/ns:Archive/ns:ContentDescription/ns:OriginatingAgency/ns:Name'
                    );
                    break;
                case NAMESPACE_SEDA_21:
                case NAMESPACE_SEDA_22:
                    $originatingAgencyNode = $util->node(
                        '/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata'
                        . '/ns:ArchiveUnit/ns:Content/ns:OriginatingAgency/ns:Identifier'
                    );
                    break;
                default:
                    throw new NotImplementedException(
                        sprintf(
                            'not implemented for namespace "%s"',
                            $util->namespace
                        )
                    );
            }
            $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
            $sa = $OrgEntities->get($entity->get('archival_agency_id'));
            if ($entity->get('originating_agency_id') === null) {
                /** @var EntityInterface $originatingAgency */
                if ($originatingAgencyNode) {
                    $originatingAgency = $OrgEntities->find()
                        ->where(
                            [
                                'identifier' => trim($originatingAgencyNode->nodeValue),
                                'lft >=' => $sa->get('lft'),
                                'rght <=' => $sa->get('rght'),
                            ]
                        )
                        ->first();
                }
                if (empty($originatingAgency)) {
                    $originatingAgency = $OrgEntities->find()
                        ->where(
                            [
                                'id' => $entity->get('transferring_agency_id'),
                                'lft >=' => $sa->get('lft'),
                                'rght <=' => $sa->get('rght'),
                            ]
                        )
                        ->firstOrFail();
                }
                $originatingAgencyId = $originatingAgency->id;
            } else {
                $originatingAgencyId = $entity->get('originating_agency_id');
            }

            $date = new DateTime();
            $stateHistory = json_decode($entity->get('state_history'), true);
            foreach ($stateHistory as $history) {
                if ($history['state'] === self::S_VALIDATING) {
                    $date = new DateTime($history['date']);
                    break;
                }
            }
            // récupère la durée en secondes depuis le début de validation du transfert
            $int = $date->diff(new DateTime());
            $duration = (new DateTime('@0'))->add($int)->getTimestamp();

            $MessageIndicators = TableRegistry::getTableLocator()->get('MessageIndicators');
            $indic = $MessageIndicators->newEntity(
                [
                    'message_date' => $date,
                    'message_type' => MessageIndicatorsTable::IN_TRANSFER,
                    'message_count' => 1,
                    'accepted' => $entity->get('is_accepted'),
                    'derogation' => false,
                    'validation_duration' => $duration,
                    'archival_agency_id' => $sa->id,
                    'transferring_agency_id' => $entity->get('transferring_agency_id'),
                    'originating_agency_id' => $originatingAgencyId,
                    'original_files_count' => $entity->get('data_count') ?? 0,
                    'original_files_size' => $entity->get('data_size') ?? 0,
                    'agreement_id' => $entity->get('agreement_id'),
                    'profile_id' => $entity->get('profile_id'),
                ]
            );
            $MessageIndicators->saveOrFail($indic);
        }
    }

    /**
     * Met à jour les champs data_count et data_size du transfert
     * @param array $conditions
     * @return void
     */
    public function calcCountSize(array $conditions)
    {
        $transfers = $this->find()
            ->where($conditions);
        foreach ($transfers as $transfer) {
            $query = $this->TransferAttachments->find();
            $results = $query->select(
                [
                    'data_count' => $query->func()->count('*'),
                    'data_size' => $query->func()->sum('size'),
                ]
            )
                ->where(['transfer_id' => $transfer->id])
                ->disableHydration()
                ->all()
                ->toArray()
            [0];
            $transfer->set('data_count', (int)$results['data_count']);
            $transfer->set('data_size', (int)$results['data_size']);
            $this->saveOrFail($transfer);
        }
    }

    /**
     * Créé un transfer_lock lié à un transfert
     * @param int  $transferId
     * @param bool $uploading
     * @return EntityInterface
     */
    public function lock(int $transferId, bool $uploading): EntityInterface
    {
        $lock = $this->TransferLocks->find()
            ->where(['transfer_id' => $transferId])
            ->first();
        if (!$lock) {
            $lock = $this->TransferLocks->newEntity(['transfer_id' => $transferId]);
        }
        $lock->set('pid', getmypid());
        $lock->set('uploading', $uploading);
        if ($this->TransferLocks->save($lock)) {
            $this->TransferLocks->deleteAll(['id !=' => $lock->id, 'transfer_id' => $transferId]);
            return $lock;
        }
        return $lock;
    }

    /**
     * Supprime le transfer_lock lié à un transfert
     * @param int $transferId
     * @return bool
     */
    public function unlock(int $transferId): bool
    {
        $lock = $this->TransferLocks->find()
            ->where(['transfer_id' => $transferId])
            ->first();
        if ($lock) {
            return $this->TransferLocks->delete($lock);
        }
        return false;
    }

    /**
     * Si le transfert est lié à une archive composite, on s'assure qu'une entrée
     * existe dans la table archives_transferts
     * @param EntityInterface $entity
     * @return void
     * @throws Exception
     */
    public function createCompositeIfNeeded(EntityInterface $entity): void
    {
        try {
            $util = DOMUtility::load($entity->get('xml'));
        } catch (Exception) {
            return;
        }
        if (in_array($util->namespace, [NAMESPACE_SEDA_02, NAMESPACE_SEDA_10]) || $entity->get('composite_id')) {
            return;
        }

        $path = '/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata/ns:ArchiveUnit';
        $nodeArchive = $util->node($path);
        if (!$nodeArchive) {
            return;
        }
        $path = 'ns:Content/ns:RelatedObjectReference/ns:IsPartOf/ns:RepositoryArchiveUnitPID';
        $node = $util->node($path, $nodeArchive);
        if (!$node) {
            return;
        }
        $value = trim($node->nodeValue);
        if (strpos($value, 'ArchivalAgencyIdentifier:') !== false) {
            $conditions = [
                'ArchiveUnits.archival_agency_identifier' => substr(
                    $value,
                    strlen('ArchivalAgencyIdentifier:')
                ),
            ];
        } elseif (strpos($value, 'TransferringAgencyIdentifier:') !== false) {
            $conditions = [
                'ArchiveUnits.transferring_agency_identifier' => substr(
                    $value,
                    strlen('TransferringAgencyIdentifier:')
                ),
            ];
        } elseif (strpos($value, 'OriginatingAgencyIdentifier:') !== false) {
            $conditions = [
                'ArchiveUnits.originating_agency_identifier' => substr(
                    $value,
                    strlen('OriginatingAgencyIdentifier:')
                ),
            ];
        } else {
            $conditions = [
                'OR' => [
                    'ArchiveUnits.archival_agency_identifier' => $value,
                    'ArchiveUnits.transferring_agency_identifier' => $value,
                    'ArchiveUnits.originating_agency_identifier' => $value,
                ],
            ];
        }
        $archive = $this->Archives->find()
            ->innerJoinWith('ArchiveUnits')
            ->where($conditions)
            ->first();
        if ($archive) {
            $entity->set('composite_id', $archive->id);
            $this->saveOrFail($entity);
        }
    }
}
