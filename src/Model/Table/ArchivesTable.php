<?php

/**
 * Asalae\Model\Table\ArchivesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use Asalae\Command\Lifecycle\UploadCommand;
use Asalae\Controller\Component\SealComponent;
use Asalae\Exception\GenericException;
use Asalae\Model\Entity\Archive;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Model\Table\BeforeSaveInterface;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Premis;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Http\Exception\BadRequestException;
use Cake\I18n\Date as CakeDate;
use Cake\Log\Log;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateTimeInterface;
use DOMDocument;
use DOMElement;
use DOMException;
use Exception;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table archives
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin StateMachineBehavior
 * @mixin OptionsBehavior
 * @property ArchiveUnitsTable|HasMany            ArchiveUnits
 * @property ArchiveFilesTable|HasMany            ArchiveFiles
 * @property ArchivesTransfersTable|BelongsToMany ArchivesTransfers
 * @property EventLogsTable|HasMany               EventLogs
 */
class ArchivesTable extends Table implements
    BeforeDeleteInterface,
    BeforeSaveInterface,
    AfterSaveInterface,
    SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    public const string T_DESCRIBE = 'describe';
    public const string T_STORE = 'store';
    public const string T_MANAGE = 'manage';
    public const string T_FINISH = 'finish';
    public const string T_REQUEST_DESTRUCTION = 'request_destruction';
    public const string T_DESTRUCT = 'destruct';
    public const string T_CANCEL = 'cancel';
    public const string T_REQUEST_RESTITUTION = 'request_restitution';
    public const string T_RESTITUTE = 'restitute';
    public const string T_REQUEST_TRANSFER = 'request_transfer';
    public const string T_TRANSFER = 'transfer';
    public const string T_FREEZE = 'freeze';
    public const string T_UNFREEZE = 'unfreeze';
    public const string T_IMPORT = 'import';
    public const string T_VALIDATE = 'validate';
    public const string T_UPDATE = 'update';

    public const string S_CREATING = 'creating';
    public const string S_DESCRIBING = 'describing';
    public const string S_STORING = 'storing';
    public const string S_MANAGING = 'managing';
    public const string S_AVAILABLE = 'available';
    public const string S_DESTRUCTION_REQUESTING = 'destruction_requesting';
    public const string S_DESTROYING = 'destroying';
    public const string S_DESTROYED = 'destroyed';
    public const string S_RESTITUTION_REQUESTING = 'restitution_requesting';
    public const string S_RESTITUTING = 'restituting';
    public const string S_RESTITUTED = 'restituted';
    public const string S_TRANSFER_REQUESTING = 'transfer_requesting';
    public const string S_TRANSFERRING = 'transferring';
    public const string S_TRANSFERRED = 'transferred';
    public const string S_FREEZED = 'freezed';
    public const string S_FREEZED_DESTROYING = 'freezed_destroying';
    public const string S_FREEZED_RESTITUTING = 'freezed_restituting';
    public const string S_FREEZED_TRANSFERRING = 'freezed_transferring';
    public const string S_UPDATE_CREATING = 'update_creating';
    public const string S_UPDATE_DESCRIBING = 'update_describing';
    public const string S_UPDATE_STORING = 'update_storing';
    public const string S_UPDATE_MANAGING = 'update_managing';
    /**
     * valeur d'entité (peut être un champ virtuel) => chemin xpath
     * @var array lien entre valeurs d'entités et chemin xpath
     */
    public const array MAPPING = [
        NAMESPACE_SEDA_02 => [
            'name' => 'ns:Name',
            'description' => 'ns:ContentDescription/ns:Description',
            'first_archive_unit.archive_description.oldest_date' => 'ns:ContentDescription/ns:OldestDate',
            'first_archive_unit.archive_description.latest_date' => 'ns:ContentDescription/ns:LatestDate',
            'first_archive_unit.archive_description.history' => 'ns:ContentDescription/ns:CustodialHistory',
            'first_archive_unit.archive_description.description' => 'ns:ContentDescription/ns:Description',
            'first_archive_unit.archive_description.access_rule.access_rule_code.code'
            => 'ns:ContentDescription/ns:AccessRestriction/ns:Code',
            'first_archive_unit.archive_description.access_rule.start_date'
            => 'ns:ContentDescription/ns:AccessRestriction/ns:StartDate',
            'agreement.identifier' => 'ns:ArchivalAgreement',
            'profile.identifier' => 'ns:ArchivalProfile',
            'originating_agency.description' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Description',
            'originating_agency.identifier' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Identification',
            'originating_agency.name' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Name',
            'access_rule.access_rule_code.code' => 'ns:AccessRestriction/ns:Code',
            'access_rule.start_date' => 'ns:AccessRestriction/ns:StartDate',
            'appraisal_rule.final' => 'ns:Appraisal/ns:Code',
            'appraisal_rule.final_action_code_xml' => 'ns:Appraisal/ns:Code',
            'appraisal_rule.appraisal_rule_code.duration' => 'ns:Appraisal/ns:Duration',
            'appraisal_rule.start_date' => 'ns:Appraisal/ns:StartDate',
            'archival_agency_identifier' => 'ns:ArchivalAgencyArchiveIdentifier',
            'transferring_agency_identifier' => 'ns:TransferringAgencyArchiveIdentifier',
            'service_level' => 'ns:ServiceLevel',
        ],
        NAMESPACE_SEDA_10 => [
            'name' => 'ns:Name',
            'description' => 'ns:ContentDescription/ns:Description',
            'first_archive_unit.archive_description.oldest_date' => 'ns:ContentDescription/ns:OldestDate',
            'first_archive_unit.archive_description.latest_date' => 'ns:ContentDescription/ns:LatestDate',
            'first_archive_unit.archive_description.history'
            => 'ns:ContentDescription/ns:CustodialHistory/ns:CustodialHistoryItem',
            'first_archive_unit.archive_description.description' => 'ns:ContentDescription/ns:Description',
            'first_archive_unit.archive_description.access_rule.access_rule_code.code'
            => 'ns:ContentDescription/ns:AccessRestrictionRule/ns:Code',
            'first_archive_unit.archive_description.access_rule.start_date'
            => 'ns:ContentDescription/ns:AccessRestrictionRule/ns:StartDate',
            'agreement.identifier' => 'ns:ArchivalAgreement',
            'profile.identifier' => 'ns:ArchivalProfile',
            'originating_agency.description' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Description',
            'originating_agency.identifier' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Identification',
            'originating_agency.name' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Name',
            'access_rule.access_rule_code.code' => 'ns:AccessRestrictionRule/ns:Code',
            'access_rule.start_date' => 'ns:AccessRestrictionRule/ns:StartDate',
            'appraisal_rule.final' => 'ns:AppraisalRule/ns:Code',
            'appraisal_rule.final_action_code_xml' => 'ns:AppraisalRule/ns:Code',
            'appraisal_rule.appraisal_rule_code.duration' => 'ns:AppraisalRule/ns:Duration',
            'appraisal_rule.start_date' => 'ns:AppraisalRule/ns:StartDate',
            'archival_agency_identifier' => 'ns:ArchivalAgencyArchiveIdentifier',
            'transferring_agency_identifier' => 'ns:TransferringAgencyArchiveIdentifier',
            'originating_agency_identifier' => 'ns:OriginatingAgencyArchiveIdentifier',
            'service_level' => 'ns:ServiceLevel',
        ],
        NAMESPACE_SEDA_21 => [
            'name' => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:Title',
            'description' => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:Description',
            'first_archive_unit.archive_description.oldest_date'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:StartDate',
            'first_archive_unit.archive_description.latest_date'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:EndDate',
            'first_archive_unit.archive_description.history'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:CustodialHistory/ns:CustodialHistoryItem',
            'first_archive_unit.archive_description.description'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:Description',
            'first_archive_unit.archive_description.access_rule.access_rule_code.code'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AccessRule/ns:Rule',
            'first_archive_unit.archive_description.access_rule.start_date'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AccessRule/ns:StartDate',
            'profile.identifier' => 'ns:ManagementMetadata/ns:ArchivalProfile',
            'originating_agency.identifier'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:OriginatingAgency/ns:Identifier',
            'access_rule.access_rule_code.code'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AccessRule/ns:Rule',
            'access_rule.start_date'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AccessRule/ns:StartDate',
            'appraisal_rule.final'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AppraisalRule/ns:FinalAction',
            'appraisal_rule.final_action_code_xml21'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AppraisalRule/ns:FinalAction',
            'appraisal_rule.appraisal_rule_code.rule'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AppraisalRule/ns:Rule',
            'appraisal_rule.start_date'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AppraisalRule/ns:StartDate',
            'archival_agency_identifier'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier',
            'transferring_agency_identifier'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:TransferringAgencyArchiveUnitIdentifier',
            'originating_agency_identifier'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:OriginatingAgencyArchiveUnitIdentifier',
            'service_level' => 'ns:ManagementMetadata/ns:ServiceLevel',
        ],
        NAMESPACE_SEDA_22 => [
            'name' => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:Title',
            'description' => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:Description',
            'first_archive_unit.archive_description.oldest_date'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:StartDate',
            'first_archive_unit.archive_description.latest_date'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:EndDate',
            'first_archive_unit.archive_description.history'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:CustodialHistory/ns:CustodialHistoryItem',
            'first_archive_unit.archive_description.description'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:Description',
            'first_archive_unit.archive_description.access_rule.access_rule_code.code'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AccessRule/ns:Rule',
            'first_archive_unit.archive_description.access_rule.start_date'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AccessRule/ns:StartDate',
            'profile.identifier' => 'ns:ManagementMetadata/ns:ArchivalProfile',
            'originating_agency.identifier'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:OriginatingAgency/ns:Identifier',
            'access_rule.access_rule_code.code'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AccessRule/ns:Rule',
            'access_rule.start_date'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AccessRule/ns:StartDate',
            'appraisal_rule.final'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AppraisalRule/ns:FinalAction',
            'appraisal_rule.final_action_code_xml21'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AppraisalRule/ns:FinalAction',
            'appraisal_rule.appraisal_rule_code.rule'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AppraisalRule/ns:Rule',
            'appraisal_rule.start_date'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Management/ns:AppraisalRule/ns:StartDate',
            'archival_agency_identifier'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier',
            'transferring_agency_identifier'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:TransferringAgencyArchiveUnitIdentifier',
            'originating_agency_identifier'
            => 'ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:OriginatingAgencyArchiveUnitIdentifier',
            'service_level' => 'ns:ManagementMetadata/ns:ServiceLevel',
        ],
    ];
    /**
     * @var string Etat initial
     */
    public $initialState = self::S_CREATING;
    /**
     * @var array Changement d'êtats selon action
     */
    public $transitions = [
        self::T_DESCRIBE => [
            self::S_CREATING => self::S_DESCRIBING,
            self::S_UPDATE_CREATING => self::S_UPDATE_DESCRIBING,
        ],
        self::T_STORE => [
            self::S_DESCRIBING => self::S_STORING,
            self::S_UPDATE_DESCRIBING => self::S_UPDATE_STORING,
        ],
        self::T_MANAGE => [
            self::S_STORING => self::S_MANAGING,
            self::S_UPDATE_STORING => self::S_UPDATE_MANAGING,
        ],
        self::T_FINISH => [
            self::S_MANAGING => self::S_AVAILABLE,
            self::S_UPDATE_MANAGING => self::S_AVAILABLE,
        ],
        self::T_REQUEST_DESTRUCTION => [
            self::S_AVAILABLE => self::S_DESTRUCTION_REQUESTING,
        ],
        self::T_DESTRUCT => [
            self::S_DESTROYING => self::S_DESTROYED,
        ],
        self::T_CANCEL => [
            self::S_DESTRUCTION_REQUESTING => self::S_AVAILABLE,
            self::S_RESTITUTION_REQUESTING => self::S_AVAILABLE,
            self::S_TRANSFER_REQUESTING => self::S_AVAILABLE,
            self::S_TRANSFERRING => self::S_AVAILABLE,
            self::S_FREEZED_DESTROYING => self::S_FREEZED,
            self::S_FREEZED_RESTITUTING => self::S_FREEZED,
            self::S_FREEZED_TRANSFERRING => self::S_FREEZED,
        ],
        self::T_REQUEST_RESTITUTION => [
            self::S_AVAILABLE => self::S_RESTITUTION_REQUESTING,
        ],
        self::T_RESTITUTE => [
            self::S_RESTITUTING => self::S_RESTITUTED,
        ],
        self::T_REQUEST_TRANSFER => [
            self::S_AVAILABLE => self::S_TRANSFER_REQUESTING,
        ],
        self::T_TRANSFER => [
            self::S_TRANSFERRING => self::S_TRANSFERRED,
        ],
        self::T_FREEZE => [
            self::S_AVAILABLE => self::S_FREEZED,
            self::S_DESTRUCTION_REQUESTING => self::S_FREEZED_DESTROYING,
            self::S_RESTITUTION_REQUESTING => self::S_FREEZED_RESTITUTING,
            self::S_TRANSFER_REQUESTING => self::S_FREEZED_TRANSFERRING,
        ],
        self::T_UNFREEZE => [
            self::S_FREEZED => self::S_AVAILABLE,
            self::S_FREEZED_DESTROYING => self::S_DESTRUCTION_REQUESTING,
            self::S_FREEZED_RESTITUTING => self::S_RESTITUTION_REQUESTING,
            self::S_FREEZED_TRANSFERRING => self::S_TRANSFER_REQUESTING,
        ],
        self::T_IMPORT => [
            self::S_CREATING => self::S_AVAILABLE,
        ],
        self::T_VALIDATE => [
            self::S_DESTRUCTION_REQUESTING => self::S_DESTROYING,
            self::S_RESTITUTION_REQUESTING => self::S_RESTITUTING,
            self::S_TRANSFER_REQUESTING => self::S_TRANSFERRING,
        ],
        self::T_UPDATE => [
            self::S_AVAILABLE => self::S_UPDATE_CREATING,
        ],
    ];
    /**
     * @var Exception dernière exception capturée par la classe
     */
    public $exception;
    /**
     * @var array Uri vers le schema xsd de validation xml
     */
    public $schemas = [
        NAMESPACE_SEDA_02 => SEDA_ARCHIVE_V02_XSD,
        NAMESPACE_SEDA_10 => SEDA_ARCHIVE_V10_XSD,
        NAMESPACE_SEDA_21 => SEDA_ARCHIVE_V21_XSD,
        NAMESPACE_SEDA_22 => SEDA_ARCHIVE_V22_XSD,
    ];

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    self::S_AVAILABLE,
                    self::S_CREATING,
                    self::S_DESCRIBING,
                    self::S_DESTROYED,
                    self::S_DESTROYING,
                    self::S_DESTRUCTION_REQUESTING,
                    self::S_FREEZED,
                    self::S_MANAGING,
                    self::S_RESTITUTED,
                    self::S_RESTITUTING,
                    self::S_RESTITUTION_REQUESTING,
                    self::S_STORING,
                    self::S_TRANSFERRED,
                    self::S_TRANSFERRING,
                    self::S_TRANSFER_REQUESTING,
                ],
            ]
        );

        $this->belongsTo('ArchivalAgencies')
            ->setClassName('OrgEntities')
            ->setForeignKey('archival_agency_id')
            ->setJoinType('INNER');
        $this->belongsTo('TransferringAgencies')
            ->setClassName('OrgEntities')
            ->setForeignKey('transferring_agency_id')
            ->setJoinType('INNER');
        $this->belongsTo('OriginatingAgencies')
            ->setClassName('OrgEntities')
            ->setForeignKey('originating_agency_id')
            ->setJoinType('INNER');
        $this->belongsTo('OrgEntities')
            ->setForeignKey(false)
            ->setConditions(
                [
                    'OrgEntities.id IN' => [
                        new IdentifierExpression(
                            'Archives.archival_agency_id'
                        ),
                        new IdentifierExpression(
                            'Archives.transferring_agency_id'
                        ),
                        new IdentifierExpression(
                            'Archives.originating_agency_id'
                        ),
                    ],
                ]
            );
        $this->belongsTo('Agreements');
        $this->belongsTo('Profiles');
        $this->belongsToMany('Transfers');
        $this->belongsTo('ServiceLevels');
        $this->belongsTo('AccessRules');
        $this->belongsTo('AppraisalRules');
        $this->belongsTo('SecureDataSpaces');
        $this->hasMany('ArchiveUnits');
        $this->hasMany('ArchiveFiles');
        $this->hasMany('DestructionNotificationXpaths');
        $this->hasMany('ArchiveBinaryConversions');
        $this->hasMany('ArchivesTransfers');
        $this->hasOne('DescriptionXmlArchiveFiles')
            ->setForeignKey('archive_id')
            ->setConditions(
                ['DescriptionXmlArchiveFiles.type' => ArchiveFilesTable::TYPE_DESCRIPTION]
            );
        $this->hasOne('PublicDescriptionArchiveFiles')
            ->setForeignKey('archive_id')
            ->setConditions(
                ['PublicDescriptionArchiveFiles.type' => ArchiveFilesTable::TYPE_PUBLIC_DESCRIPTION]
            );
        $this->hasOne('LifecycleXmlArchiveFiles')
            ->setForeignKey('archive_id')
            ->setConditions(['LifecycleXmlArchiveFiles.type' => ArchiveFilesTable::TYPE_LIFECYCLE]);
        $this->hasMany('TransferXmlArchiveFiles')
            ->setForeignKey('archive_id')
            ->setConditions(['TransferXmlArchiveFiles.type' => ArchiveFilesTable::TYPE_TRANSFER]);
        $this->hasOne('DeletedJsonArchiveFiles')
            ->setForeignKey('archive_id')
            ->setConditions(['DeletedJsonArchiveFiles.type' => ArchiveFilesTable::TYPE_DELETED]);
        $this->hasOne('AttestationPdfArchiveFiles')
            ->setForeignKey('archive_id')
            ->setConditions(['AttestationPdfArchiveFiles.type' => ArchiveFilesTable::TYPE_ARCHIVING_CERTIFICATE]);
        $this->hasMany('EventLogs')
            ->setForeignKey('object_foreign_key')
            ->setConditions(['EventLogs.object_model' => 'Archives']);
        $this->belongsToMany('RestitutionRequests');

        try {
            $subquery = $this->selectQuery()
                ->select(['au.id'])
                ->from(['au' => 'archive_units'])
                ->where(
                    [
                        'au.parent_id IS' => null,
                        'au.archive_id' => new IdentifierExpression($this->getAlias() . '.id'),
                    ]
                )
                ->orderBy(['au.id' => 'asc'])
                ->limit(1);
            $this->hasOne('FirstArchiveUnits')
                ->setClassName('ArchiveUnits')
                ->setConditions(
                    [
                        'FirstArchiveUnits.parent_id IS' => null,
                        'FirstArchiveUnits.id IN (' . $subquery->sql() . ')',
                    ]
                );
        } catch (Exception) { // BDD down
            $this->hasOne('FirstArchiveUnits')
                ->setClassName('ArchiveUnits')
                ->setConditions(['FirstArchiveUnits.parent_id IS' => null]);
        }
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->boolean('is_integrity_ok')
            ->allowEmptyString('is_integrity_ok');

        $validator
            ->scalar('archival_agency_identifier')
            ->maxLength('archival_agency_identifier', 255)
            ->requirePresence('archival_agency_identifier', 'create')
            ->notEmptyString('archival_agency_identifier')
            ->add(
                'archival_agency_identifier',
                'unique',
                [
                    'rule' => [
                        'validateUnique',
                        ['scope' => 'archival_agency_id'],
                    ],
                    'provider' => 'table',
                    'message' => __("Cet identifiant est déjà utilisé"),
                ]
            );

        $validator
            ->scalar('transferring_agency_identifier')
            ->maxLength('transferring_agency_identifier', 255)
            ->allowEmptyString('transferring_agency_identifier');

        $validator
            ->scalar('originating_agency_identifier')
            ->maxLength('originating_agency_identifier', 255)
            ->allowEmptyString('originating_agency_identifier');

        $validator
            ->scalar('storage_path')
            ->maxLength('storage_path', 255)
            ->requirePresence('storage_path', 'create')
            ->notEmptyString('storage_path');

        $validator
            ->scalar('name')
            ->allowEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->allowEmptyDate('oldest_date');

        $validator
            ->allowEmptyDate('latest_date');

        $validator
            ->requirePresence('transferred_size', 'create')
            ->notEmptyString('transferred_size');

        $validator
            ->requirePresence('management_size', 'create')
            ->notEmptyString('management_size');

        $validator
            ->requirePresence('original_size', 'create')
            ->notEmptyString('original_size');

        $validator
            ->requirePresence('preservation_size', 'create')
            ->notEmptyString('preservation_size');

        $validator
            ->requirePresence('dissemination_size', 'create')
            ->notEmptyString('dissemination_size');

        $validator
            ->requirePresence('timestamp_size', 'create')
            ->notEmptyString('timestamp_size');

        $validator
            ->integer('transferred_count')
            ->requirePresence('transferred_count', 'create')
            ->notEmptyString('transferred_count');

        $validator
            ->integer('management_count')
            ->requirePresence('management_count', 'create')
            ->notEmptyString('management_count');

        $validator
            ->integer('original_count')
            ->requirePresence('original_count', 'create')
            ->notEmptyString('original_count');

        $validator
            ->integer('preservation_count')
            ->requirePresence('preservation_count', 'create')
            ->notEmptyString('preservation_count');

        $validator
            ->integer('dissemination_count')
            ->requirePresence('dissemination_count', 'create')
            ->notEmptyString('dissemination_count');

        $validator
            ->integer('timestamp_count')
            ->requirePresence('timestamp_count', 'create')
            ->notEmptyString('timestamp_count');

        $validator
            ->scalar('xml_node_tagname')
            ->maxLength('xml_node_tagname', 255)
            ->requirePresence('xml_node_tagname', 'create')
            ->notEmptyString('xml_node_tagname');

        $validator
            ->dateTime('last_state_update')
            ->allowEmptyString('last_state_update');

        $validator
            ->dateTime('integrity_date')
            ->allowEmptyString('integrity_date');

        $validator
            ->integer('units_count')
            ->allowEmptyString('units_count');

        $validator
            ->dateTime('filesexist_date')
            ->allowEmptyDateTime('filesexist_date');

        $validator
            ->dateTime('next_search')
            ->allowEmptyDateTime('next_search');

        return $validator;
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }

    /**
     * Évite que la vérification d'intégrité ne mette à jour le champ modified
     *
     * @param Event          $event
     * @param Entity|Archive $entity
     * @param ArrayObject    $options
     */
    public function beforeSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        if (
            count($entity->getDirty()) === 2 && $entity->isDirty(
                'integrity_date'
            )
        ) {
            $entity->setDirty('modified', false);
        }
        $filename = $entity->get('zip');
        if (is_file($filename)) {
            unlink($filename);
        }
    }

    /**
     * Query pour le contôle d'intégrité
     * @return Query
     * @throws Exception
     */
    public function findByIntegrity(): Query
    {
        return $this->find()
            ->select(
                [
                    'Archives.id',
                    'Archives.archival_agency_identifier',
                    'Archives__archive_binary_id' => 'ArchiveBinaries.id',
                    'Archives__current_stored_file_id' => 'StoredFiles.id',
                    'Archives__size' => 'StoredFiles.size',
                    'Archives__hash' => 'StoredFiles.hash',
                    'Archives__hash_algo' => 'StoredFiles.hash_algo',
                    'Archives__storage_ref' => 'StoredFilesVolumes.storage_ref',
                    'Archives__volume_id' => 'StoredFilesVolumes.volume_id',
                    'Archives__volume_name' => 'Volumes.name',
                    'Archives__stored_file_id' => 'Lifecycles.id',
                    'Archives__stored_file_name' => 'Lifecycles.name',
                ]
            )
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.ArchiveBinaries')
            ->innerJoinWith('ArchiveUnits.ArchiveBinaries.StoredFiles')
            ->innerJoinWith(
                'ArchiveUnits.ArchiveBinaries.StoredFiles.StoredFilesVolumes'
            )
            ->innerJoinWith(
                'ArchiveUnits.ArchiveBinaries.StoredFiles.StoredFilesVolumes.Volumes'
            )
            ->leftJoinWith('LifecycleXmlArchiveFiles')
            ->leftJoinWith('LifecycleXmlArchiveFiles.Lifecycles')
            ->where(
                [
                    'Volumes.active IS' => true,
                    'Archives.state NOT IN' => [
                        ArchivesTable::S_CREATING,
                        ArchivesTable::S_DESCRIBING,
                        ArchivesTable::S_STORING,
                        ArchivesTable::S_MANAGING,
                    ],
                ]
            )
            ->orderBy(
                [
                    'Archives.is_integrity_ok IS NULL' => 'desc',
                    // nouveaux en 1er
                    'Archives.is_integrity_ok' => 'asc',
                    // ensuite ceux en echec
                    'Archives.integrity_date' => 'asc',
                    // par dernière vérif
                    'Archives.created' => 'asc',
                    // par date de création
                    'StoredFilesVolumes.volume_id' => 'asc',
                    'StoredFilesVolumes.storage_ref' => 'asc',
                ]
            );
    }

    /**
     * Query pour le contôle d'intégrité
     * @return Query
     * @throws Exception
     */
    public function findByArchiveFilesIntegrity(): Query
    {
        return $this->find()
            ->select(
                [
                    'Archives.id',
                    'Archives.archival_agency_identifier',
                    'Archives__archive_file_id' => 'ArchiveFiles.id',
                    'Archives__current_stored_file_id' => 'StoredFiles.id',
                    'Archives__size' => 'StoredFiles.size',
                    'Archives__hash' => 'StoredFiles.hash',
                    'Archives__hash_algo' => 'StoredFiles.hash_algo',
                    'Archives__storage_ref' => 'StoredFilesVolumes.storage_ref',
                    'Archives__volume_id' => 'StoredFilesVolumes.volume_id',
                    'Archives__volume_name' => 'Volumes.name',
                    'Archives__stored_file_id' => 'Lifecycles.id',
                    'Archives__stored_file_name' => 'Lifecycles.name',
                ]
            )
            ->innerJoinWith('ArchiveFiles')
            ->innerJoinWith('ArchiveFiles.StoredFiles')
            ->innerJoinWith('ArchiveFiles.StoredFiles.StoredFilesVolumes')
            ->innerJoinWith(
                'ArchiveFiles.StoredFiles.StoredFilesVolumes.Volumes'
            )
            ->leftJoinWith('LifecycleXmlArchiveFiles')
            ->leftJoinWith('LifecycleXmlArchiveFiles.Lifecycles')
            ->where(['Volumes.active IS' => true])
            ->orderBy(
                [
                    'Archives.is_integrity_ok IS NULL' => 'desc',
                    // nouveaux en 1er
                    'Archives.is_integrity_ok' => 'asc',
                    // ensuite ceux en echec
                    'Archives.integrity_date' => 'asc',
                    // par dernière vérif
                    'Archives.created' => 'asc',
                    // par date de création
                    'StoredFilesVolumes.volume_id' => 'asc',
                    'StoredFilesVolumes.storage_ref' => 'asc',
                ]
            );
    }

    /**
     * Query pour le files exists
     * @param Query $query
     * @param array $options [minDelay] en jours
     * @return \Cake\ORM\Query
     * @throws Exception
     * @noinspection PhpUnusedParameterInspection
     * @see          \Cake\ORM\Table::callFinder
     */
    public function findMissingFiles(Query $query, array $options): Query
    {
        return $query
            ->select(
                [
                    'Archives.id',
                    'Archives.archival_agency_identifier',
                    'Archives__archive_binary_id' => 'ArchiveBinaries.id',
                    'Archives__current_stored_file_id' => 'StoredFiles.id',
                    'Archives__size' => 'StoredFiles.size',
                    'Archives__hash' => 'StoredFiles.hash',
                    'Archives__hash_algo' => 'StoredFiles.hash_algo',
                    'Archives__storage_ref' => 'StoredFilesVolumes.storage_ref',
                    'Archives__volume_id' => 'StoredFilesVolumes.volume_id',
                    'Archives__volume_name' => 'Volumes.name',
                    'Archives__stored_file_id' => 'Lifecycles.id',
                    'Archives__stored_file_name' => 'Lifecycles.name',
                ]
            )
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.ArchiveBinaries')
            ->innerJoinWith('ArchiveUnits.ArchiveBinaries.StoredFiles')
            ->innerJoinWith(
                'ArchiveUnits.ArchiveBinaries.StoredFiles.StoredFilesVolumes'
            )
            ->innerJoinWith(
                'ArchiveUnits.ArchiveBinaries.StoredFiles.StoredFilesVolumes.Volumes'
            )
            ->leftJoinWith('LifecycleXmlArchiveFiles')
            ->leftJoinWith('LifecycleXmlArchiveFiles.Lifecycles')
            ->where(
                [
                    'Volumes.active IS' => true,
                    'Archives.is_integrity_ok IS' => true,
                ]
            )
            ->orderBy(
                [
                    'Archives.filesexist_date IS NULL' => 'desc',
                    // nouveaux en 1er
                    'Archives.filesexist_date' => 'asc',
                    // par dernière vérif
                    'Archives.created' => 'asc',
                    // par date de création
                    'Archives.id' => 'asc',
                    'StoredFilesVolumes.volume_id' => 'asc',
                    'StoredFilesVolumes.storage_ref' => 'asc',
                ]
            );
    }

    /**
     * Query pour la liste des archives
     * @param EntityInterface       $archivalAgency
     * @param EntityInterface|array $user
     * @return Query
     * @deprecated use findByIndexSealed instead
     */
    public function findByIndex(EntityInterface $archivalAgency, $user): Query
    {
        $org_entity_id = Hash::get($user, 'org_entity.id');
        if (!isset($org_entity_id)) {
            throw new BadRequestException();
        }
        $query = $this->find()
            ->innerJoinWith('ArchivalAgencies')
            ->innerJoinWith('TransferringAgencies')
            ->innerJoinWith('OriginatingAgencies')
            ->where(
                ['Archives.archival_agency_id' => $archivalAgency->get('id')]
            );
        $lft = Hash::get($user, 'org_entity.lft');
        $rght = Hash::get($user, 'org_entity.rght');
        $typeEntity = Hash::get($user, 'org_entity.type_entity.code');
        if (in_array($typeEntity, ['SA', 'CST'])) {
            $lft = $archivalAgency->get('lft');
            $rght = $archivalAgency->get('rght');
        }
        if (Hash::get($user, 'role.hierarchical_view')) {
            $query->andWhere(
                [
                    'OR' => [
                        [
                            'ArchivalAgencies.lft >=' => $lft,
                            'ArchivalAgencies.rght <=' => $rght,
                        ],
                        [
                            'TransferringAgencies.lft >=' => $lft,
                            'TransferringAgencies.rght <=' => $rght,
                        ],
                        [
                            'OriginatingAgencies.lft >=' => $lft,
                            'OriginatingAgencies.rght <=' => $rght,
                        ],
                    ],
                ]
            );
        } else {
            $query->andWhere(
                [
                    $org_entity_id . ' IN' => [
                        new IdentifierExpression('ArchivalAgencies.id'),
                        new IdentifierExpression('TransferringAgencies.id'),
                        new IdentifierExpression('OriginatingAgencies.id'),
                    ],
                ]
            );
        }
        $query->contain(
            [
                'AccessRules' => ['AccessRuleCodes'],
                'Agreements',
                'AppraisalRules' => ['AppraisalRuleCodes'],
                'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                'OriginatingAgencies',
                'Profiles',
                'ServiceLevels',
                'TransferringAgencies',
                'Transfers',
            ]
        );
        return $query;
    }

    /**
     * Remplace findByIndex(), la logique du hierarchicalView se trouve désormais
     * dans le component Seal
     * @param SealComponent $Seal
     * @return Query
     */
    public function findByIndexSealed(SealComponent $Seal): Query
    {
        return $Seal->table('Archives')->find('seal')
            ->contain(
                [
                    'AccessRules' => ['AccessRuleCodes'],
                    'Agreements',
                    'AppraisalRules' => ['AppraisalRuleCodes'],
                    'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                    'OriginatingAgencies',
                    'Profiles',
                    'ServiceLevels',
                    'TransferringAgencies',
                    'Transfers',
                ]
            );
    }

    /**
     * Ajoute FirstArchiveUnits en dehors de la requête (optimisation)
     * @param EntityInterface $archive
     * @return EntityInterface
     */
    public function addFirstArchiveUnitToArchive(EntityInterface $archive)
    {
        $archiveUnit = $this->ArchiveUnits->find()
            ->where(['parent_id IS' => null, 'archive_id' => $archive->get('id')])
            ->first();
        if ($archiveUnit) {
            $archive->set('first_archive_unit_id', $archiveUnit->get('id'));
        }
        return $archive;
    }

    /**
     * Met à jour la description liée à une archive selon les modifications
     * apportés dans $entity
     * @param EntityInterface $entity
     * @param bool            $async
     * @return bool success
     * @throws VolumeException
     */
    public function updateDescriptionXml(
        EntityInterface $entity,
        bool $async = false
    ): bool {
        /** @var EntityInterface $storedFile */
        $storedFile = Hash::get(
            $entity,
            'description_xml_archive_file.stored_file'
        );
        $stored_file_id = Hash::get(
            $entity,
            'description_xml_archive_file.stored_file.id'
        );
        if (empty($stored_file_id)) {
            return false;
        }
        $manager = VolumeManager::createWithStoredFileId($stored_file_id);
        $basedir = Configure::read(
            'Archives.updateDescription.path',
            rtrim(
                Configure::read('App.paths.data'),
                DS
            ) . DS . 'update_description'
        );
        if (!is_dir($basedir)) {
            mkdir($basedir, 0700, true);
        }
        $tmpFile = $basedir . DS . $stored_file_id . '-description.xml';
        if (!is_file($tmpFile)) {
            $manager->fileDownload(Hash::get($storedFile, 'name'), $tmpFile);
        }

        $util = DOMUtility::load($tmpFile);
        if (empty(self::MAPPING[$util->namespace])) {
            return false;
        }
        foreach (self::MAPPING[$util->namespace] as $hashpath => $xpath) {
            $value = Hash::get($entity, $hashpath);
            $element = $util->xpath->query($xpath)->item(0);
            if (empty($value)) {
                if ($element instanceof DOMElement) {
                    $element->parentNode->removeChild($element);
                }
                continue;
            }
            if (!$element instanceof DOMElement) {
                $element = $this->appendElement($xpath, $util);
                /** @var DOMElement $parentNode */
                $parentNode = $element->parentNode;
                // cas particulier
                if (
                    in_array(
                        $parentNode->tagName,
                        ['AppraisalRule', 'AccessRestriction']
                    )
                    && $element->tagName === 'Code'
                ) {
                    $element->setAttribute('listVersionID', "edition 2009");
                }
            }
            if ($value instanceof DateTimeInterface || $value instanceof CakeDate) {
                $value = $value->format('Y-m-d');
            }
            DOMUtility::setValue($element, $value);
        }
        if (!$util->dom->save($tmpFile)) {
            unlink($tmpFile);
            Log::error('unable to write on ' . $tmpFile);
            return false;
        }
        if (!$util->dom->schemaValidate($this->schemas[$util->namespace])) {
            unlink($tmpFile);
            Log::error('validation failed');
            if (Configure::read('debug')) {
                Log::debug('validation failed');
                $h = fopen(
                    Configure::read('App.paths.logs', LOGS) . DS . 'debug.log',
                    'a'
                );
                fwrite($h, PHP_EOL . $util->dom->saveXML() . PHP_EOL);
                fclose($h);
            }
            return false;
        }
        // si un fichier lock existe, on attend un peu si un upload est en cours
        $i = 0;
        while (is_file($tmpFile . '.lock') && $i < 10) {
            sleep(1); //NOSONAR
            $i++;
        }
        if (!is_file($tmpFile . '.lock')) {
            file_put_contents(
                $tmpFile . '.lock',
                'uploading since ' . date('Y-m-d H:i:s')
            );
        }
        $duration = Configure::read(
            'Archives.updateDescription.cache_duration',
            600
        );
        $Exec = Utility::get('Exec');
        try {
            if ($async) {
                /** @var EntityInterface $space */
                $space = $manager->__get('space');
                $Exec->defaultStdout = LOGS . 'update_description.log';
                // ex: bin/cake worker_manager 1 upload path/to/foo.bar --overwrite
                $Exec->async(
                    CAKE_SHELL,
                    'volume_manager',
                    'upload',
                    $space->id,
                    $tmpFile,
                    $storedFile->get('name'),
                    [
                        '--overwrite' => null,
                        '--sleep-then-delete' => $duration,
                    ]
                );
            } else {
                $manager->replace($stored_file_id, $tmpFile);
                unlink($tmpFile . '.lock');
                // supprime le fichier tmpFile dans 600s si il n'y a pas de .lock
                $Exec->async(
                    sprintf(
                        'sleep %d && [ ! -f %s ] && rm -f %s',
                        $duration,
                        escapeshellarg($tmpFile . '.lock'),
                        escapeshellarg($tmpFile)
                    )
                );
            }
        } catch (VolumeException $e) {
            Log::error((string)$e);
            $this->exception = $e;
            return false;
        }
        return true;
    }

    /**
     * Ajoute un element en profondeur
     * @param string     $xpath
     * @param DOMUtility $util
     * @return DOMElement
     * @throws Exception
     */
    private function appendElement(string $xpath, DOMUtility $util): DOMElement
    {
        $paths = explode('/ns:', substr($xpath, 3));
        $parent = $util->dom->documentElement;
        /** @var DOMElement $node */
        foreach ($paths as $path) {
            $found = false;
            foreach ($parent->childNodes as $node) {
                if ($node instanceof DOMElement && $node->tagName === $path) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $node = $util->appendChild(
                    $parent,
                    $util->createElement($path)
                );
            }
            $parent = $node;
        }
        if (empty($node)) {
            throw new \Exception();
        }
        return $node;
    }

    /**
     * Donne la dernière exception capturée
     * @return Exception|null
     */
    public function getLastException()
    {
        return $this->exception;
    }

    /**
     * Met à jour le cycle de vie de l'archive
     * @param EntityInterface|array             $storedFile
     * @param EntityInterface|EntityInterface[] $entry
     * @param bool                              $async
     * @return bool
     * @throws VolumeException
     * @throws DOMException
     */
    public function updateLifecycleXml($storedFile, $entry, bool $async = false)
    {
        $conn = $this->getConnection();
        if ($conn->inTransaction() && $async === false) {
            throw new GenericException(
                __(
                    "updateLifecycleXml() ne peut être lancé en async = false"
                    . " à l'interieur d'une transaction de base de données"
                )
            ); // évite une boucle infini où les deux processes s'attendent mutuellement
        }
        if (!is_array($entry)) {
            $entry = [$entry];
        }
        $stored_file_id = Hash::get($storedFile, 'id');
        if (empty($stored_file_id)) {
            trigger_error(
                __("Tentative de mise à jour de lifecycle sur storedFile vide")
            );
            return false;
        }
        $manager = VolumeManager::createWithStoredFileId($stored_file_id);

        $xml = Cache::read($stored_file_id . '-lifecycle.xml', 'lifecycle');
        if (!$xml) {
            $xml = $manager->fileGetContent(Hash::get($storedFile, 'name'));
        }

        $dom = new DOMDocument();
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $dom->loadXml($xml);
        foreach ($entry as $ent) {
            /** @var Premis\Event $event */
            $event = $ent->get('premis_event');
            Premis::appendEvent($dom, $event);
        }

        $i = 0;
        do {
            if (!Cache::enabled()) {
                break;
            }
            if ($i > 0) {
                sleep(1);// NO_SONAR
            }
            if ($i > 5) {
                throw new GenericException(__("Ecriture du cache impossible"));
            }
            $success = @Cache::write($stored_file_id . '-lifecycle.xml', $dom->saveXML(), 'lifecycle');
            $i++;
        } while ($success === false);

        if (!$dom->schemaValidate(PREMIS_V3)) {
            Cache::delete($stored_file_id . '-lifecycle.xml');
            Log::error('validation failed');
            if (Configure::read('debug')) {
                Log::debug('validation failed');
                $h = fopen(
                    Configure::read('App.paths.logs', LOGS) . DS . 'debug.log',
                    'a'
                );
                fwrite($h, PHP_EOL . $dom->saveXML() . PHP_EOL);
                fclose($h);
            }
            return false;
        }

        $Exec = Utility::get('Exec');
        /** @var EntityInterface $space */
        $space = $manager->__get('space');
        $Exec->defaultStdout = LOGS . 'update_lifecycle.log';
        $entriesIds = [];
        foreach ($entry as $ent) {
            $entriesIds[] = $ent->id;
        }
        UploadCommand::prepareUpload(
            $dom->saveXML(),
            $stored_file_id,
            $space->id,
            $entriesIds
        );
        if ($async) {
            $Exec->async(
                CAKE_SHELL,
                'lifecycle',
                'upload'
            );
        } else {
            // TODO faire en sorte de lancer la commande comme on lance les commandes dans les tests
            // TODO permettrai de lancer la mise à jour synchrone à l'interieur d'une transaction
            $result = $Exec->command(
                CAKE_SHELL,
                'lifecycle',
                'upload'
            );
            if (!$result['success']) {
                dlog($result);
                $this->exception = new GenericException($result['stderr'] . $result['stdout']);
                return false;
            }
        }
        return true;
    }

    /**
     * Met à jour les compteurs des archives dans $query
     * @param Query|EntityInterface[] $archives
     */
    public function updateCountSize($archives)
    {
        $q = $this->ArchiveUnits->query();
        foreach ($archives as $archive) {
            $archiveUnit = $this->ArchiveUnits->find()
                ->where(['archive_id' => $archive->id, 'parent_id IS' => null])
                ->first();
            $archiveFiles = $this->ArchiveFiles->find()
                ->select(
                    [
                        'count' => $q->func()->count('*'),
                        'size' => $q->func()->sum('StoredFiles.size'),
                    ]
                )
                ->innerJoinWith('StoredFiles')
                ->where(['ArchiveFiles.archive_id' => $archive->id]);
            $management = $archiveFiles->toArray();
            $managementSize = (int)Hash::get(
                $management,
                '0.size',
                0
            );
            $managementCount = (int)Hash::get(
                $management,
                '0.count',
                0
            );
            if ($archiveUnit) {
                $baseBinaries = $this->ArchiveUnits->find()
                    ->select(
                        [
                            'count' => $q->func()->count('*'),
                            'size' => $q->func()->sum('size'),
                        ]
                    )
                    ->innerJoinWith('ArchiveBinaries')
                    ->innerJoinWith('ArchiveBinaries.StoredFiles')
                    ->where(
                        [
                            'ArchiveUnits.archive_id' => $archive->id,
                            'ArchiveUnits.lft >=' => $archiveUnit->get('lft'),
                            'ArchiveUnits.rght <=' => $archiveUnit->get('rght'),
                        ]
                    );
                $original = (clone $baseBinaries)
                    ->where(['ArchiveBinaries.type' => 'original_data'])
                    ->toArray();
                $preservation = (clone $baseBinaries)
                    ->where(['ArchiveBinaries.type' => 'preservation_data'])
                    ->toArray();
                $dissemination = (clone $baseBinaries)
                    ->where(['ArchiveBinaries.type' => 'dissemination_data'])
                    ->toArray();
                $timestamp = (clone $baseBinaries)
                    ->where(['ArchiveBinaries.type LIKE' => '%_timestamp'])
                    ->toArray();
                $data = [
                    'management_size' => $managementSize,
                    'original_size' => (int)Hash::get($original, '0.size', 0),
                    'preservation_size' => (int)Hash::get(
                        $preservation,
                        '0.size',
                        0
                    ),
                    'dissemination_size' => (int)Hash::get(
                        $dissemination,
                        '0.size',
                        0
                    ),
                    'timestamp_size' => (int)Hash::get($timestamp, '0.size', 0),
                    'management_count' => $managementCount,
                    'original_count' => (int)Hash::get($original, '0.count', 0),
                    'preservation_count' => (int)Hash::get(
                        $preservation,
                        '0.count',
                        0
                    ),
                    'dissemination_count' => (int)Hash::get(
                        $dissemination,
                        '0.count',
                        0
                    ),
                    'timestamp_count' => (int)Hash::get(
                        $timestamp,
                        '0.count',
                        0
                    ),
                    'units_count' => $this->ArchiveUnits
                        ->find()
                        ->where(['archive_id' => $archive->id])
                        ->count(),
                ];
            } else {
                $data = [
                    'management_size' => $managementSize,
                    'original_size' => 0,
                    'preservation_size' => 0,
                    'dissemination_size' => 0,
                    'timestamp_size' => 0,
                    'management_count' => $managementCount,
                    'original_count' => 0,
                    'preservation_count' => 0,
                    'dissemination_count' => 0,
                    'timestamp_count' => 0,
                    'units_count' => 0,
                ];
            }
            $this->patchEntity($archive, $data);
            $this->saveOrFail($archive);
        }
    }

    /**
     * Donne le PDF du certificat d'archivage lié à une archive
     * @param int $id
     * @return string
     */
    public function renderArchivingCertificatePdf($id): string
    {
        try {
            $htmlFile = tempnam(sys_get_temp_dir(), uniqid('seda2pdf-'));
            file_put_contents($htmlFile, $this->renderArchivingCertificateHtml($id));
            $pdfFile = tempnam(sys_get_temp_dir(), uniqid('seda2pdf-'));
            exec(
                sprintf(
                    'cat %s | xvfb-run -a --server-args="-screen 0, 1024x768x24" wkhtmltopdf - %s 2>&1',
                    escapeshellarg($htmlFile),
                    escapeshellarg($pdfFile)
                ),
                $out,
                $code
            );
            if ($code !== 0) {
                throw new GenericException(var_export($out, true));
            }
            return file_get_contents($pdfFile);
        } finally {
            if (is_file($htmlFile)) {
                unlink($htmlFile);
            }
            if (isset($pdfFile) && is_file($pdfFile)) {
                unlink($pdfFile);
            }
        }
    }

    /**
     * Donne le HTML qui servira pour la transformation en PDF
     * @param int|string $id
     * @return string
     * @throws VolumeException
     */
    public function renderArchivingCertificateHtml($id): string
    {
        /** @var Archive $archive */
        $archive = $this->find()
            ->where(['Archives.id' => $id])
            ->contain(
                [
                    'ArchivalAgencies',
                    'TransferXmlArchiveFiles' => ['StoredFiles'],
                    'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                    'OriginatingAgencies',
                    'Profiles',
                    'SecureDataSpaces' => [
                        'Volumes' => ['sort' => 'Volumes.name'],
                    ],
                    'ServiceLevels',
                    'TransferringAgencies',
                    'Transfers' => ['sort' => 'Transfers.id'],
                ]
            )
            ->firstOrFail();
        return $archive->getArchivingCertificateHtml();
    }

    /**
     * Retourne les archives de la destruction request qui sont à l'état gelée
     * (ce qui bloque la validation)
     * @param EntityInterface $destructionRequest
     * @return Query
     */
    public function getFreezedArchivesDestruction(EntityInterface $destructionRequest): Query
    {
        return $this->getFreezedArchives('DestructionRequests', $destructionRequest);
    }

    /**
     * Requête générique pourla récupération des archives freezed dans une demande
     * @param string          $table
     * @param EntityInterface $entity
     * @return Query
     */
    private function getFreezedArchives(string $table, EntityInterface $entity): Query
    {
        return $this->find()
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.' . $table)
            ->where(
                [
                    $table . '.id' => $entity->get('id'),
                    'Archives.state IN' => [
                        ArchivesTable::S_FREEZED,
                        ArchivesTable::S_FREEZED_DESTROYING,
                        ArchivesTable::S_FREEZED_RESTITUTING,
                        ArchivesTable::S_FREEZED_TRANSFERRING,
                    ],
                ]
            )
            ->contain(['ArchiveUnits']);
    }

    /**
     * Retourne les archives de la restitution request qui sont à l'état gelée
     * (ce qui bloque la validation)
     * @param EntityInterface $restitutionRequest
     * @return Query
     */
    public function getFreezedArchivesRestitution(EntityInterface $restitutionRequest): Query
    {
        return $this->getFreezedArchives('RestitutionRequests', $restitutionRequest);
    }

    /**
     * Retourne les archives de la outgoing transfer request qui sont à l'état gelée
     * (ce qui bloque la validation)
     * @param EntityInterface $outgoingTransferRequest
     * @return Query
     */
    public function getFreezedArchivesOutgoingTransfer(EntityInterface $outgoingTransferRequest): Query
    {
        return $this->getFreezedArchives('OutgoingTransferRequests', $outgoingTransferRequest);
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        if ($entity->isNew() && $entity->get('transfer_id')) {
            $this->ArchivesTransfers->findOrCreate(
                [
                    'archive_id' => $entity->id,
                    'transfer_id' => $entity->get('transfer_id'),
                ]
            );
        }
    }
}
