<?php

/**
 * Asalae\Model\Table\ValidationChainsTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;

/**
 * Table validation_chains
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin OptionsBehavior
 * @property ValidationStagesTable ValidationStages
 */
class ValidationChainsTable extends Table implements
    AfterSaveInterface,
    BeforeDeleteInterface,
    SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    public const string ARCHIVE_TRANSFER_CONFORM = 'ARCHIVE_TRANSFER_CONFORM';
    public const string ARCHIVE_TRANSFER_NOT_CONFORM = 'ARCHIVE_TRANSFER_NOT_CONFORM';
    public const string ARCHIVE_DELIVERY_REQUEST = 'ARCHIVE_DELIVERY_REQUEST';
    public const string ARCHIVE_DESTRUCTION_REQUEST = 'ARCHIVE_DESTRUCTION_REQUEST';
    public const string ARCHIVE_RESTITUTION_REQUEST = 'ARCHIVE_RESTITUTION_REQUEST';
    public const string ARCHIVE_OUTGOING_TRANSFER_REQUEST = 'ARCHIVE_OUTGOING_TRANSFER_REQUEST';

    /**
     * @var array Cache pour findListOptions:
     * [
     *     org_entity_id: id => [
     *         type: string => result: Query
     *     ]
     * ]
     */
    private $cacheListOptions = [];

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');

        $this->belongsTo('OrgEntities');
        $this->hasMany('ValidationStages');
        $subquery = $this->selectQuery()
            ->select(['fs.id'])
            ->from(['fs' => 'validation_stages'])
            ->where(
                [
                    'fs.validation_chain_id' => new IdentifierExpression(
                        'ValidationChains.id'
                    ),
                ]
            )
            ->orderBy(['ord' => 'asc'])
            ->limit(1);
        $this->hasOne('FirstStages')
            ->setClassName('ValidationStages')
            ->setForeignKey(false)
            ->setConditions(['FirstStages.id IN' => $subquery]);
        $this->hasMany('ValidationProcesses');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'app_type' => [
                    self::ARCHIVE_TRANSFER_CONFORM,
                    self::ARCHIVE_TRANSFER_NOT_CONFORM,
                    self::ARCHIVE_DELIVERY_REQUEST,
                    self::ARCHIVE_DESTRUCTION_REQUEST,
                    self::ARCHIVE_RESTITUTION_REQUEST,
                    self::ARCHIVE_OUTGOING_TRANSFER_REQUEST,
                ],
            ]
        );

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('name');
        $validator->add(
            'name',
            [
                'unique' => [
                    'rule' => ['validateUnique', ['scope' => 'org_entity_id']],
                    'provider' => 'table',
                    'message' => __("Un enregistrement similaire existe déjà"),
                ],
            ]
        );
        $validator->allowEmptyString('default');
        $validator->add(
            'default',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if ($value || empty($context['data']['org_entity_id'])) {
                        return true;
                    }
                    /** @var Table $table */
                    $table = $context['providers']['table'];
                    $conditions = [
                        'org_entity_id' => Hash::get(
                            $context,
                            'data.org_entity_id',
                            0
                        ),
                        'app_type' => Hash::get($context, 'data.app_type', ''),
                        'app_meta LIKE' => '%"default":true%',
                    ];
                    if (!empty($id = Hash::get($context, 'data.id'))) {
                        $conditions['id !='] = $id;
                    }
                    $count = $table->find()
                        ->where($conditions)
                        ->count();
                    return $count > 0;
                },
                'message' => __(
                    "Pour chaque type de circuits, il doit y avoir un circuit par défaut"
                ),
            ]
        );

        return $validator;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function afterSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        if ($entity->isDirty('default')) {
            if ($entity->get('default')) {
                $query = $this->find()
                    ->where(
                        [
                            'org_entity_id' => $entity->get('org_entity_id'),
                            'app_type' => $entity->get('app_type'),
                            'id !=' => $entity->get('id'),
                        ]
                    );
                $toSave = [];
                /** @var EntityInterface $chain */
                foreach ($query as $chain) {
                    if ($chain->get('default')) {
                        $chain->set('default', false);
                        $toSave[] = $chain;
                    }
                }
                $this->saveMany($toSave, ['atomic' => false]);
            }
        }
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }

    /**
     * Trouve le circuit par défaut
     * @param Query       $query
     * @param string|null $app_type
     * @param int|null    $org_entity_id
     * @return Query
     */
    public function findDefault(Query $query, string $app_type = null, int $org_entity_id = null): Query
    {
        $conditions = [
            ['ValidationChains.app_meta LIKE' => '%"default":true%'],
            ['ValidationChains.app_meta LIKE' => '%"active":true%'],
        ];
        if (isset($app_type)) {
            $conditions = [
                'ValidationChains.app_type' => $app_type,
            ] + $conditions;
        }
        if (isset($org_entity_id)) {
            $conditions = [
                'ValidationChains.org_entity_id' => $org_entity_id,
            ] + $conditions;
        }
        return $query->where($conditions);
    }

    /**
     * Donne les options active pour un service d'archive et un type donné
     * @param int    $org_entity_id
     * @param string $type
     * @param array  $options
     * @return Query
     */
    public function findListOptions(
        int $org_entity_id,
        string $type,
        array $options = []
    ): Query {
        if (isset($this->cacheListOptions[$org_entity_id][$type])) {
            return $this->cacheListOptions[$org_entity_id][$type];
        }

        $options += [
            'valueField' => function (EntityInterface $entity) {
                $default = $entity->get('default') ? ' (default)' : '';
                return $entity->get('name') . $default;
            },
        ];
        $ValidationStages = $this->ValidationStages;
        $ValidationActors = $ValidationStages->ValidationActors;
        $query = $ValidationStages->find();
        $subsubquery = $ValidationActors->find()
            ->select(['count' => $query->func()->count('*')])
            ->where(
                [
                    $ValidationActors->aliasField('validation_stage_id')
                    => new IdentifierExpression('ValidationStages.id'),
                ]
            );

        $subquery = $query // validationStages avec acteurs
        ->select(['count' => $query->func()->count('*')])
            ->where(
                [
                    $ValidationStages->aliasField('validation_chain_id')
                    => new IdentifierExpression('ValidationChains.id'),
                    "({$subsubquery->sql()}) > 0",
                ]
            );

        $query = $ValidationStages->find();
        $subqueryEmpty = $query // validationStages sans acteurs
        ->select(['count' => $query->func()->count('*')])
            ->where(
                [
                    $ValidationStages->aliasField('validation_chain_id')
                    => new IdentifierExpression('ValidationChains.id'),
                    "({$subsubquery->sql()}) = 0",
                ]
            );

        $keyField = $options['keyField'] ?? null;
        $valueField = $options['valueField'] ?? null;
        $groupField = $options['groupField'] ?? null;
        return $this->cacheListOptions[$org_entity_id][$type] = $this->find(
            'list',
            keyField: $keyField,
            valueField: $valueField,
            groupField: $groupField
        )
            ->where(
                [
                    $this->aliasField('org_entity_id') => $org_entity_id,
                    $this->aliasField('app_type') => $type,
                    $this->aliasField('app_meta LIKE') => '%"active":true%',
                    "({$subquery->sql()}) > 0", // au moins un stage avec acteur
                    "({$subqueryEmpty->sql()}) = 0", // aucun stage sans acteur
                ]
            )
            ->orderBy(['name' => 'asc']);
    }
}
