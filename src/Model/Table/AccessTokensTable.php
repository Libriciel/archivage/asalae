<?php

/**
 * Asalae\Model\Table\AccessTokensTable
 */

namespace Asalae\Model\Table;

use Asalae\Model\Entity\AccessToken;
use Asalae\Model\Entity\Webservice;
use Cake\Core\Configure;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use DateTime;
use DateTimeZone;
use Exception;

/**
 * Table webservices
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @deprecated since 2.0.0a5
 */
class AccessTokensTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');

        $this->belongsTo('Users');

        parent::initialize($config);
    }

    /**
     * Créer un accès token de connexion
     * @param Webservice  $webservice
     * @param null|Entity $user
     * @return AccessToken
     * @throws Exception
     */
    public function createCodeToken(
        Webservice $webservice,
        $user = null
    ): AccessToken {
        $interval = Configure::read('Webservices.tokens-duration.code')
            ?: '1 minute';
        $date = new DateTime('now', new DateTimeZone('Europe/Paris'));
        $date->add(date_interval_create_from_date_string($interval));

        /** @var AccessToken $code */
        $code = $this->newEntity(
            [
                'webservice_id' => $webservice->get('id'),
                'user_id' => $user?->get('id'),
                'type' => 'code',
                'code' => $webservice->generateCode(),
                'expiration_date' => $date,
            ]
        );
        $this->save($code);
        return $code;
    }

    /**
     * Tokens échangés contre un token code
     * @param Webservice $webservice
     * @param null|int   $user_id
     * @return array
     * @throws Exception
     */
    public function createAccessTokens(Webservice $webservice, $user_id): array
    {
        $iAccess = Configure::read('Webservices.tokens-duration.access-token')
            ?: '1 hour';
        $iRefresh = Configure::read('Webservices.tokens-duration.refresh-token')
            ?: '1 day';
        $timezone = new DateTimeZone('Europe/Paris');
        $now = new DateTime('now', $timezone);
        $dateAccess = clone $now;
        $dateAccess->add(date_interval_create_from_date_string($iAccess));
        $dateRefresh = clone $now;
        $dateRefresh->add(date_interval_create_from_date_string($iRefresh));

        $access = $this->newEntity(
            [
                'webservice_id' => $webservice->get('id'),
                'user_id' => $user_id,
                'type' => 'access_token',
                'code' => $webservice->generateCode(),
                'expiration_date' => $dateAccess,
            ]
        );
        $this->save($access);

        $refresh = $this->newEntity(
            [
                'webservice_id' => $webservice->get('id'),
                'user_id' => $user_id,
                'type' => 'refresh_token',
                'code' => $webservice->generateCode(),
                'expiration_date' => $dateRefresh,
            ]
        );
        $this->save($refresh);

        return [
            'access_token' => $access->get('code'),
            'refresh_token' => $refresh->get('code'),
            'expiration_date' => $access->get('expiration_date'),
            'expires_in' => $dateAccess->getTimestamp() - $now->getTimestamp(),
        ];
    }
}
