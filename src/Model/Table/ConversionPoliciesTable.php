<?php

/**
 * Asalae\Model\Table\ConversionPoliciesTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\UniqueTrait;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table conversion_policies
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @deprecated logique déplacée dans ConfigurationsTable
 */
class ConversionPoliciesTable extends Table
{
    use UniqueTrait;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'method' => [
                    'always',
                    'on_demand',
                ],
                'usage' => [
                    'preservation',
                    'diffusion',
                ],
                'format' => [
                    'ac3',
                    'av_aac',
                    'flac',
                    'flac16',
                    'flac24',
                    'jpg',
                    'mkv',
                    'mp3',
                    'mp4',
                    'mpeg2',
                    'mpeg4',
                    'odp',
                    'ods',
                    'odt',
                    'ogg',
                    'pdf',
                    'png',
                    'theora',
                    'vorbis',
                    'VP8',
                    'wav',
                    'x264',
                    'x265',
                ],
            ]
        );
        $this->belongsTo('OrgEntities');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('media')
            ->maxLength('media', 255)
            ->requirePresence('media', 'create')
            ->notEmptyString('media');

        $validator
            ->scalar('usage')
            ->maxLength('usage', 255)
            ->requirePresence('usage', 'create')
            ->notEmptyString('usage');

        $validator
            ->scalar('method')
            ->maxLength('method', 255)
            ->requirePresence('method', 'create')
            ->notEmptyString('method');

        $validator
            ->scalar('format')
            ->maxLength('format', 255)
            ->requirePresence('format', 'create')
            ->notEmptyString('format');

        $validator
            ->allowEmptyString('params');

        $this->addUniqueRule(
            $validator,
            'media',
            __("Cet ensemble method/usage est déjà utilisé"),
            ['scope' => ['org_entity_id', 'usage']]
        );

        return $validator;
    }
}
