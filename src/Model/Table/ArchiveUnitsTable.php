<?php

/**
 * Asalae\Model\Table\ArchiveUnitsTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Model\Table\BeforeSaveInterface;
use AsalaeCore\ORM\Table as CoreTable;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\DatabaseUtility;
use AsalaeCore\Utility\Translit;
use Asalae\Controller\Component\SealComponent;
use Asalae\Model\Volume\VolumeManager;
use Cake\Collection\CollectionInterface;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\I18n\Date as CakeDate;
use AsalaeCore\I18n\Date as CoreDate;
use Cake\Log\Log;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\HasOne;
use Cake\ORM\Behavior\TreeBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateTime;
use DateTimeInterface;
use DOMElement;
use DOMNode;
use Exception;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table archive_units
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property HasMany|Table                            ArchiveKeywords
 * @mixin TreeBehavior
 * @mixin StateMachineBehavior
 * @property BelongsToMany|ArchiveBinariesTable       ArchiveBinaries
 * @property HasMany|ArchiveBinariesArchiveUnitsTable ArchiveBinariesArchiveUnits
 * @property HasMany|ArchivesTable                    Archives
 * @property VolumeException|Exception                $exception
 */
class ArchiveUnitsTable extends CoreTable implements SerializeDeletedEntitiesInterface, BeforeSaveInterface
{
    use SerializeDeleteAllTrait;

    public const string S_CREATING = 'creating';
    public const string S_AVAILABLE = 'available';
    public const string S_DESTROYING = 'destroying';
    public const string S_RESTITUTING = 'restituting';
    public const string S_TRANSFERRING = 'transferring';
    public const string S_FREEZED = 'freezed';
    public const string S_FREEZED_DESTROYING = 'freezed_destroying';
    public const string S_FREEZED_RESTITUTING = 'freezed_restituting';
    public const string S_FREEZED_TRANSFERRING = 'freezed_transferring';

    public const string T_FINISH = 'finish';
    public const string T_REQUEST_DESTRUCTION = 'request_destruction';
    public const string T_CANCEL = 'cancel';
    public const string T_REQUEST_RESTITUTION = 'request_restitution';
    public const string T_REQUEST_TRANSFER = 'request_transfer';
    public const string T_FREEZE = 'freeze';
    public const string T_UNFREEZE = 'unfreeze';
    /**
     * valeur d'entité (peut être un champ virtuel) => chemin xpath
     * @var array lien entre valeurs d'entités et chemin xpath
     */
    public const array MAPPING = [
        NAMESPACE_SEDA_02 => [
            'name' => 'ns:Name',
            'description' => 'ns:ContentDescription/ns:Description',
            'archive_description.oldest_date' => 'ns:ContentDescription/ns:OldestDate',
            'archive_description.latest_date' => 'ns:ContentDescription/ns:LatestDate',
            'archive_description.history' => 'ns:ContentDescription/ns:CustodialHistory',
            'archive_description.description' => 'ns:ContentDescription/ns:Description',
            'archive_description.access_rule.access_rule_code.code'
            => 'ns:ContentDescription/ns:AccessRestriction/ns:Code',
            'archive_description.access_rule.start_date'
            => 'ns:ContentDescription/ns:AccessRestriction/ns:StartDate',
            'agreement.identifier' => 'ns:ArchivalAgreement',
            'profile.identifier' => 'ns:ArchivalProfile',
            'archive.originating_agency.description' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Description',
            'archive.originating_agency.identifier' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Identification',
            'archive.originating_agency.name' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Name',
            'access_rule.access_rule_code.code' => 'ns:AccessRestriction/ns:Code',
            'access_rule.start_date' => 'ns:AccessRestriction/ns:StartDate',
            'appraisal_rule.final' => 'ns:Appraisal/ns:Code',
            'appraisal_rule.final_action_code_xml' => 'ns:Appraisal/ns:Code',
            'appraisal_rule.appraisal_rule_code.duration' => 'ns:Appraisal/ns:Duration',
            'appraisal_rule.start_date' => 'ns:Appraisal/ns:StartDate',
            'archival_agency_identifier' => [
                'ns:ArchivalAgencyArchiveIdentifier',
                'ns:ArchivalAgencyObjectIdentifier',
                'ns:ArchivalAgencyDocumentIdentifier',
            ],
            'transferring_agency_identifier' => 'ns:TransferringAgencyArchiveIdentifier',
            'service_level' => 'ns:ServiceLevel',
        ],
        NAMESPACE_SEDA_10 => [
            'name' => 'ns:Name',
            'description' => 'ns:ContentDescription/ns:Description',
            'archive_description.oldest_date' => 'ns:ContentDescription/ns:OldestDate',
            'archive_description.latest_date' => 'ns:ContentDescription/ns:LatestDate',
            'archive_description.history'
            => 'ns:ContentDescription/ns:CustodialHistory/ns:CustodialHistoryItem',
            'archive_description.description' => 'ns:ContentDescription/ns:Description',
            'archive_description.access_rule.access_rule_code.code'
            => 'ns:ContentDescription/ns:AccessRestrictionRule/ns:Code',
            'archive_description.access_rule.start_date'
            => 'ns:ContentDescription/ns:AccessRestrictionRule/ns:StartDate',
            'agreement.identifier' => 'ns:ArchivalAgreement',
            'profile.identifier' => 'ns:ArchivalProfile',
            'archive.originating_agency.description' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Description',
            'archive.originating_agency.identifier' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Identification',
            'archive.originating_agency.name' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Name',
            'access_rule.access_rule_code.code' => 'ns:AccessRestrictionRule/ns:Code',
            'access_rule.start_date' => 'ns:AccessRestrictionRule/ns:StartDate',
            'appraisal_rule.final' => 'ns:AppraisalRule/ns:Code',
            'appraisal_rule.final_action_code_xml' => 'ns:AppraisalRule/ns:Code',
            'appraisal_rule.appraisal_rule_code.duration' => 'ns:AppraisalRule/ns:Duration',
            'appraisal_rule.start_date' => 'ns:AppraisalRule/ns:StartDate',
            'archival_agency_identifier' => [
                'ns:ArchivalAgencyArchiveIdentifier',
                'ns:ArchivalAgencyObjectIdentifier',
                'ns:ArchivalAgencyDocumentIdentifier',
            ],
            'transferring_agency_identifier' => 'ns:TransferringAgencyArchiveIdentifier',
            'originating_agency_identifier' => 'ns:OriginatingAgencyArchiveIdentifier',
            'service_level' => 'ns:ServiceLevel',
        ],
        NAMESPACE_SEDA_21 => [
            'name' => 'ns:Content/ns:Title',
            'description' => 'ns:Content/ns:Description',
            'archive_description.oldest_date' => 'ns:Content/ns:StartDate',
            'archive_description.latest_date' => 'ns:Content/ns:EndDate',
            'archive_description.history' => 'ns:Content/ns:CustodialHistory/ns:CustodialHistoryItem',
            'archive_description.description' => 'ns:Content/ns:Description',
            'archive_description.access_rule.access_rule_code.code' => 'ns:Management/ns:AccessRule/ns:Rule',
            'archive_description.access_rule.start_date' => 'ns:Management/ns:AccessRule/ns:StartDate',
            'profile.identifier' => '../ns:ManagementMetadata/ns:ArchivalProfile',
            'archive.originating_agency.identifier' => 'ns:Content/ns:OriginatingAgency/ns:Identifier',
            'archive.originating_agency.name'
                => 'ns:Content/ns:OriginatingAgency/ns:OrganizationDescriptiveMetadata/seda1:Name',
            'archive.originating_agency.description'
                => 'ns:Content/ns:OriginatingAgency/ns:OrganizationDescriptiveMetadata/seda1:Description',
            'access_rule.access_rule_code.code' => 'ns:Management/ns:AccessRule/ns:Rule',
            'access_rule.start_date' => 'ns:Management/ns:AccessRule/ns:StartDate',
            'appraisal_rule.final' => 'ns:Management/ns:AppraisalRule/ns:FinalAction',
            'appraisal_rule.final_action_code_xml21' => 'ns:Management/ns:AppraisalRule/ns:FinalAction',
            'appraisal_rule.appraisal_rule_code.rule' => 'ns:Management/ns:AppraisalRule/ns:Rule',
            'appraisal_rule.start_date' => 'ns:Management/ns:AppraisalRule/ns:StartDate',
            'archival_agency_identifier' => 'ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier',
            'transferring_agency_identifier' => 'ns:Content/ns:TransferringAgencyArchiveUnitIdentifier',
            'originating_agency_identifier' => 'ns:Content/ns:OriginatingAgencyArchiveUnitIdentifier',
            'service_level' => '../ns:ManagementMetadata/ns:ServiceLevel',
        ],
        NAMESPACE_SEDA_22 => [
            'name' => 'ns:Content/ns:Title',
            'description' => 'ns:Content/ns:Description',
            'archive_description.oldest_date' => 'ns:Content/ns:StartDate',
            'archive_description.latest_date' => 'ns:Content/ns:EndDate',
            'archive_description.history' => 'ns:Content/ns:CustodialHistory/ns:CustodialHistoryItem',
            'archive_description.description' => 'ns:Content/ns:Description',
            'archive_description.access_rule.access_rule_code.code' => 'ns:Management/ns:AccessRule/ns:Rule',
            'archive_description.access_rule.start_date' => 'ns:Management/ns:AccessRule/ns:StartDate',
            'profile.identifier' => '../ns:ManagementMetadata/ns:ArchivalProfile',
            'archive.originating_agency.identifier' => 'ns:Content/ns:OriginatingAgency/ns:Identifier',
            'archive.originating_agency.name'
                => 'ns:Content/ns:OriginatingAgency/ns:OrganizationDescriptiveMetadata/seda1:Name',
            'archive.originating_agency.description'
                => 'ns:Content/ns:OriginatingAgency/ns:OrganizationDescriptiveMetadata/seda1:Description',
            'access_rule.access_rule_code.code' => 'ns:Management/ns:AccessRule/ns:Rule',
            'access_rule.start_date' => 'ns:Management/ns:AccessRule/ns:StartDate',
            'appraisal_rule.final' => 'ns:Management/ns:AppraisalRule/ns:FinalAction',
            'appraisal_rule.final_action_code_xml21' => 'ns:Management/ns:AppraisalRule/ns:FinalAction',
            'appraisal_rule.appraisal_rule_code.rule' => 'ns:Management/ns:AppraisalRule/ns:Rule',
            'appraisal_rule.start_date' => 'ns:Management/ns:AppraisalRule/ns:StartDate',
            'archival_agency_identifier' => 'ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier',
            'transferring_agency_identifier' => 'ns:Content/ns:TransferringAgencyArchiveUnitIdentifier',
            'originating_agency_identifier' => 'ns:Content/ns:OriginatingAgencyArchiveUnitIdentifier',
            'service_level' => '../ns:ManagementMetadata/ns:ServiceLevel',
        ],
    ];
    /**
     * @var string Etat initial
     */
    public $initialState = self::S_CREATING;
    /**
     * @var array Changement d'êtats selon action
     */
    public $transitions = [
        self::T_FINISH => [
            self::S_CREATING => self::S_AVAILABLE,
        ],
        self::T_REQUEST_DESTRUCTION => [
            self::S_AVAILABLE => self::S_DESTROYING,
        ],
        self::T_CANCEL => [
            self::S_DESTROYING => self::S_AVAILABLE,
            self::S_RESTITUTING => self::S_AVAILABLE,
            self::S_TRANSFERRING => self::S_AVAILABLE,
            self::S_FREEZED_DESTROYING => self::S_FREEZED,
            self::S_FREEZED_RESTITUTING => self::S_FREEZED,
            self::S_FREEZED_TRANSFERRING => self::S_FREEZED,
        ],
        self::T_REQUEST_RESTITUTION => [
            self::S_AVAILABLE => self::S_RESTITUTING,
        ],
        self::T_REQUEST_TRANSFER => [
            self::S_AVAILABLE => self::S_TRANSFERRING,
        ],
        self::T_FREEZE => [
            self::S_AVAILABLE => self::S_FREEZED,
            self::S_DESTROYING => self::S_FREEZED_DESTROYING,
            self::S_RESTITUTING => self::S_FREEZED_RESTITUTING,
            self::S_TRANSFERRING => self::S_FREEZED_TRANSFERRING,
        ],
        self::T_UNFREEZE => [
            self::S_FREEZED => self::S_AVAILABLE,
            self::S_FREEZED_DESTROYING => self::S_DESTROYING,
            self::S_FREEZED_RESTITUTING => self::S_RESTITUTING,
            self::S_FREEZED_TRANSFERRING => self::S_TRANSFERRING,
        ],
    ];
    /**
     * @var array Uri vers le schema xsd de validation xml
     */
    public $schemas = [
        NAMESPACE_SEDA_02 => SEDA_ARCHIVE_V02_XSD,
        NAMESPACE_SEDA_10 => SEDA_ARCHIVE_V10_XSD,
        NAMESPACE_SEDA_21 => SEDA_ARCHIVE_V21_XSD,
        NAMESPACE_SEDA_22 => SEDA_ARCHIVE_V22_XSD,
    ];
    /**
     * @var int
     */
    private $rght;
    private VolumeException|Exception $exception;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Tree');
        $this->addBehavior('StateMachine.StateMachine');

        $this->belongsTo('Archives');
        $this->belongsTo('AccessRules');
        $this->belongsTo('AppraisalRules');
        $this->belongsTo('ParentArchiveUnits')
            ->setClassName('ArchiveUnits')
            ->setForeignKey('parent_id');
        $subquery = $this->selectQuery()
            ->select(['sau.id'])
            ->from(['sau' => 'archive_units'])
            ->where(
                [
                    'sau.lft <' => new IdentifierExpression(
                        $this->getAlias() . '.lft'
                    ),
                    'sau.rght >' => new IdentifierExpression(
                        $this->getAlias() . '.rght'
                    ),
                ]
            );
        $this->belongsTo('AllParentArchiveUnits')
            ->setClassName('ArchiveUnits')
            ->setForeignKey(false)
            ->setConditions(['AllParentArchiveUnits.id IN' => $subquery]);
        $this->hasMany('ArchiveKeywords');
        $this->hasMany('ArchiveUnitsOutgoingTransferRequests');
        $this->hasMany('ChildArchiveUnits')
            ->setClassName('ArchiveUnits')
            ->setForeignKey('parent_id');
        $this->belongsToMany('ArchiveBinaries');
        $this->belongsToMany('OriginalDatas')
            ->setTargetForeignKey('archive_binary_id')
            ->setConditions(['OriginalDatas.type' => 'original_data'])
            ->setClassName('ArchiveBinaries');
        $this->hasOne('ArchiveDescriptions')
            ->setForeignKey('archive_unit_id');
        $this->belongsToMany('DeliveryRequests');
        $this->belongsToMany('DestructionRequests');
        $this->belongsToMany('RestitutionRequests');
        $this->belongsToMany('OutgoingTransferRequests')
            ->setTargetForeignKey('otr_id');
        $this->hasMany('ArchiveUnitsDeliveryRequests');
        $this->hasMany('ArchiveUnitsDestructionRequests');
        $this->hasMany('ArchiveUnitsRestitutionRequests');
        $this->hasMany('ArchiveUnitsOutgoingTransferRequests');
        $this->hasMany('ArchiveBinariesArchiveUnits');
        $this->hasMany('ArchiveUnitsBatchTreatments');
        $this->hasMany('OutgoingTransfers');
        $this->hasOne('CompositeArchiveUnits');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->scalar('archival_agency_identifier')
            ->maxLength('archival_agency_identifier', 255)
            ->requirePresence('archival_agency_identifier', 'create')
            ->notEmptyString('archival_agency_identifier');

        $validator
            ->scalar('transferring_agency_identifier')
            ->maxLength('transferring_agency_identifier', 255)
            ->allowEmptyString('transferring_agency_identifier');

        $validator
            ->scalar('originating_agency_identifier')
            ->maxLength('originating_agency_identifier', 255)
            ->allowEmptyString('originating_agency_identifier');

        $validator
            ->scalar('name')
            ->allowEmptyString('name');

        $validator
            ->requirePresence('original_local_size', 'create')
            ->notEmptyString('original_local_size');

        $validator
            ->requirePresence('original_total_size', 'create')
            ->notEmptyString('original_total_size');

        $validator
            ->scalar('xml_node_tagname')
            ->maxLength('xml_node_tagname', 255)
            ->requirePresence('xml_node_tagname', 'create')
            ->notEmptyString('xml_node_tagname');

        $validator
            ->integer('original_total_count')
            ->requirePresence('original_total_count', 'create')
            ->notEmptyString('original_total_count');

        $validator
            ->integer('original_local_count')
            ->requirePresence('original_local_count', 'create')
            ->notEmptyString('original_local_count');

        $validator
            ->scalar('search')
            ->allowEmptyString('search');

        $validator
            ->scalar('full_search')
            ->allowEmptyString('full_search');

        return $validator;
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }

    /**
     * Met à jour le champ search lié à une archive_unit
     * @param array         $conditions
     * @param callable|null $fn
     */
    public function updateSearchField(array $conditions, ?callable $fn = null)
    {
        $subquery = TableRegistry::getTableLocator()->get('AccessRules')->find()
            ->select(['AccessRules.id'])
            ->where(
                [
                    'AccessRules.id' => new IdentifierExpression(
                        'ArchiveDescriptions.access_rule_id'
                    ),
                    'AccessRules.end_date <=' => (new DateTime())->format(
                        'Y-m-d'
                    ),
                ]
            );
        $query = $this->find()
            ->select(
                [
                    'ArchiveUnits.id',
                    'ArchiveUnits.archival_agency_identifier',
                    'ArchiveUnits.name',
                    'description' => 'ArchiveDescriptions.description',
                    'history' => 'ArchiveDescriptions.history',
                ]
            )
            ->leftJoin(
                ['ArchiveDescriptions' => 'archive_descriptions'],
                [
                    'ArchiveDescriptions.archive_unit_id' => new IdentifierExpression(
                        'ArchiveUnits.id'
                    ),
                    'ArchiveDescriptions.access_rule_id IN' => $subquery,
                ]
            )
            ->innerJoinWith('Archives')
            ->where($conditions)
            ->disableHydration();
        $count = $query->count();
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $i => $archiveUnit) {
            if ($fn) {
                $fn($i, $count);
            }
            // contain ArchiveKeywords provoque un bug pdo (trop d'archive_units)
            $archiveUnit['archive_keywords'] = $this->ArchiveKeywords->find()
                ->select(['content'])
                ->innerJoinWith('KeywordAccessRules')
                ->where(
                    [
                        'archive_unit_id' => $archiveUnit['id'],
                        'KeywordAccessRules.end_date <=' => (new DateTime())->format(
                            'Y-m-d'
                        ),
                    ]
                )
                ->disableHydration()
                ->toArray();
            $search = [];
            $paths = [
                'archival_agency_identifier',
                'name',
                'description',
                'history',
                'archive_keywords.{n}.content',
            ];
            foreach ($paths as $path) {
                foreach (Hash::extract($archiveUnit, $path) as $val) {
                    if ($val) {
                        $search[] = '"' . Translit::asciiToLower($val) . '"';
                    }
                }
            }
            $this->updateQuery()
                ->set(['search' => implode(' ', $search)])
                ->where(['id' => $archiveUnit['id']])
                ->execute();
        }
    }

    /**
     * Défini le champ full_search lié à une archive_unit
     * @param array         $conditions
     * @param callable|null $fn
     */
    public function updateFullSearchField(
        array $conditions,
        ?callable $fn = null
    ) {
        $query = $this->find()
            ->select(
                [
                    'ArchiveUnits.id',
                    'ArchiveUnits.archival_agency_identifier',
                    'ArchiveUnits.name',
                    'description' => 'ArchiveDescriptions.description',
                    'history' => 'ArchiveDescriptions.history',
                ]
            )
            ->leftJoinWith('ArchiveDescriptions')
            ->innerJoinWith('Archives')
            ->where($conditions)
            ->disableHydration();
        $count = $query->count();
        /** @var EntityInterface $archiveUnit */
        foreach ($query as $i => $archiveUnit) {
            if ($fn) {
                $fn($i, $count);
            }
            // contain ArchiveKeywords provoque un bug pdo (trop d'archive_units)
            $archiveUnit['archive_keywords'] = $this->ArchiveKeywords->find()
                ->select(['content'])
                ->innerJoinWith('KeywordAccessRules')
                ->where(['archive_unit_id' => $archiveUnit['id']])
                ->disableHydration()
                ->toArray();
            $search = [];
            $paths = [
                'archival_agency_identifier',
                'name',
                'description',
                'history',
                'archive_keywords.{n}.content',
            ];
            foreach ($paths as $path) {
                foreach (Hash::extract($archiveUnit, $path) as $val) {
                    if ($val) {
                        $search[] = '"' . Translit::asciiToLower($val) . '"';
                    }
                }
            }
            $this->updateQuery()
                ->set(['full_search' => implode(' ', $search)])
                ->where(['id' => $archiveUnit['id']])
                ->execute();
        }
    }

    /**
     * Requète pour le catalogue (index et recherche)
     * @param string $type
     * @return Query
     */
    public function findByCatalog(string $type = 'all'): Query
    {
        return $this->find($type)
            ->contain(
                [
                    'Archives' => [
                        'Agreements',
                        'Profiles',
                        'ArchivalAgencies',
                        'TransferringAgencies',
                        'OriginatingAgencies',
                        'Transfers',
                    ],
                    'ArchiveDescriptions' => [
                        'AccessRules',
                    ],
                ]
            )
            ->orderBy(
                [
                    'Archives.id' => 'desc',
                    'ArchiveUnits.lft' => 'asc',
                ]
            );
    }

    /**
     * Requète pour l'index
     * @param EntityInterface       $archivalAgency
     * @param EntityInterface|array $user
     * @param bool                  $rootOnly
     * @return Query
     * @deprecated use findByIndexSealed instead
     */
    public function findByIndex(
        EntityInterface $archivalAgency,
        $user,
        bool $rootOnly
    ): Query {
        $query = $this->find()
            ->where(
                [
                    'ArchiveUnits.state IN' => [self::S_AVAILABLE, self::S_FREEZED],
                    'Archives.archival_agency_id' => $archivalAgency->id,
                ]
            )
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                        'Agreements',
                        'Profiles',
                        'ArchivalAgencies',
                        'OriginatingAgencies',
                        'TransferringAgencies',
                        'Transfers',
                    ],
                    'AppraisalRules',
                    'ArchiveDescriptions',
                ]
            );
        if ($rootOnly) {
            $query->where(['ArchiveUnits.parent_id IS' => null]);
        }
        $lft = Hash::get($user, 'org_entity.lft');
        $rght = Hash::get($user, 'org_entity.rght');
        $typeEntity = Hash::get($user, 'org_entity.type_entity.code');
        if (in_array($typeEntity, ['SA', 'CST'])) {
            $lft = $archivalAgency->get('lft');
            $rght = $archivalAgency->get('rght');
        }
        if (Hash::get($user, 'role.hierarchical_view')) {
            $query->andWhere(
                [
                    'OR' => [
                        [
                            'ArchivalAgencies.lft >=' => $lft,
                            'ArchivalAgencies.rght <=' => $rght,
                        ],
                        [
                            'TransferringAgencies.lft >=' => $lft,
                            'TransferringAgencies.rght <=' => $rght,
                        ],
                        [
                            'OriginatingAgencies.lft >=' => $lft,
                            'OriginatingAgencies.rght <=' => $rght,
                        ],
                    ],
                ]
            );
        } else {
            $query->andWhere(
                [
                    Hash::get($user, 'org_entity.id') . ' IN' => [
                        new IdentifierExpression('ArchivalAgencies.id'),
                        new IdentifierExpression('TransferringAgencies.id'),
                        new IdentifierExpression('OriginatingAgencies.id'),
                    ],
                ]
            );
        }
        return $query;
    }

    /**
     * Remplace findByIndex(), la logique du hierarchicalView se trouve désormais
     * dans le component Seal
     * @param SealComponent $Seal
     * @param bool          $rootOnly
     * @return Query
     */
    public function findByIndexSealed(SealComponent $Seal, bool $rootOnly): Query
    {
        $query = $Seal->table('ArchiveUnits')->find('seal')
            ->where(['ArchiveUnits.state IN' => [self::S_AVAILABLE, self::S_FREEZED]])
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                        'Agreements',
                        'Profiles',
                        'ArchivalAgencies',
                        'OriginatingAgencies',
                        'TransferringAgencies',
                        'Transfers',
                    ],
                    'AppraisalRules',
                    'ArchiveDescriptions',
                ]
            );
        if ($rootOnly) {
            $query->where(['ArchiveUnits.parent_id IS' => null]);
        }
        return $query;
    }

    /**
     * Retire les elements inutiles à une demande de communication
     * Si on demande l'archive et son archive unit, on ne garde que l'archive
     * car elle contient l'archive unit
     * @param Query $query
     * @return CollectionInterface
     */
    public function filterChilds(Query $query): CollectionInterface
    {
        $this->rght = 0;
        return $query
            ->select(['ArchiveUnits.rght'])
            ->orderBy(['ArchiveUnits.lft' => 'asc'], true)
            ->all()
            ->filter([$this, 'filterChildsMap']);
    }

    /**
     * Filtre les elements enfants
     * @param array|EntityInterface $value
     * @return bool
     */
    public function filterChildsMap($value): bool
    {
        if ($this->rght > $value['rght']) {
            return false;
        }
        $this->rght = $value['rght'];
        return true;
    }

    /**
     * Recalcule le champ expired_dua_root
     * @param array         $conditions
     * @param callable|null $fn
     */
    public function calcDuaExpiredRoot(
        array $conditions = [],
        ?callable $fn = null
    ) {
        $this->updateAll(
            ['expired_dua_root' => false],
            $conditions
        );
        foreach (['keep', 'destroy'] as $finalAction) {
            $query = $this->find()
                ->select(['id', 'lft', 'rght'])
                ->innerJoinWith('Archives') // pour les conditions
                ->innerJoinWith('AppraisalRules')
                ->where(
                    [
                        'AppraisalRules.end_date <=' => new DateTime(),
                        'AppraisalRules.final_action_code' => $finalAction,
                    ]
                )
                ->andWhere($conditions)
                ->orderBy(['ArchiveUnits.lft'])
                ->disableHydration();
            $count = $query->count();

            $rght = 0;
            foreach ($query as $i => $archiveUnit) {
                if ($fn) {
                    $fn($i + 1, $count);
                }
                if ($archiveUnit['rght'] < $rght) {
                    continue;
                }
                $this->updateAll(
                    ['expired_dua_root' => true],
                    ['id' => $archiveUnit['id']]
                );
                $rght = $archiveUnit['rght'];
            }
        }
    }

    /**
     * Met à jour la description liée à une archive selon les modifications
     * apportés dans $entity
     * @param EntityInterface $entity
     * @param bool            $async
     * @return bool success
     * @throws VolumeException
     */
    public function updateDescriptionXml(EntityInterface $entity, bool $async = false): bool
    {
        $entity->set('not_public', true);
        // Supprime les elements qui sont ajoutés dans description si description n'existe pas
        /** @var EntityInterface $archive */
        $archive = $entity->get('archive');
        if (!$entity->get('archive_description') && $archive) {
            $mutableArchive = clone $entity->get('archive');
            $entity->set('archive', $mutableArchive);
            $mutableArchive->unset('originating_agency');
        }
        /** @var EntityInterface $storedFile */
        $storedFile = Hash::get(
            $entity,
            'archive.description_xml_archive_file.stored_file'
        );
        $stored_file_id = Hash::get(
            $entity,
            'archive.description_xml_archive_file.stored_file.id'
        );
        if (empty($stored_file_id)) {
            return false;
        }
        $manager = VolumeManager::createWithStoredFileId($stored_file_id);
        $basedir = Configure::read(
            'Archives.updateDescription.path',
            TMP . 'update_description'
        );
        if (!is_dir($basedir)) {
            mkdir($basedir, 0700, true);
        }
        $tmpFile = $basedir . DS . $stored_file_id . '-description.xml';
        if (!is_file($tmpFile)) {
            $manager->fileDownload(Hash::get($storedFile, 'name'), $tmpFile);
        }

        $util = DOMUtility::load($tmpFile);
        if (empty(self::MAPPING[$util->namespace])) {
            return false;
        }
        $util->xpath->registerNamespace('seda1', NAMESPACE_SEDA_10);
        $archiveUnit = $util->xpath->query($entity->get('xpath'))->item(0);
        $type = $archiveUnit->nodeName;
        foreach (self::MAPPING[$util->namespace] as $hashpath => $xpaths) {
            $value = Hash::get($entity, $hashpath, false);
            $element = null;
            if (is_string($xpaths)) {
                $xpath = $xpaths;
            } else {
                $xpath = null;
                switch ($type) {
                    case 'Archive':
                        $xpath = $xpaths[0];
                        break;
                    case 'ArchiveObject':
                    case 'Contains':
                        $xpath = $xpaths[1];
                        break;
                    case 'Document':
                        $xpath = $xpaths[2];
                        break;
                }
            }
            $element = $util->xpath->query($xpath, $archiveUnit)->item(0);
            if ($value === null || $value === '') {
                if ($element instanceof DOMElement) {
                    $element->parentNode->removeChild($element);
                }
                continue;
            } elseif ($value === false) {
                continue;
            }
            if (!$element instanceof DOMElement) {
                $element = $this->getDomElement($xpath, $archiveUnit, $util);
            }
            if ($value instanceof DateTimeInterface || $value instanceof CakeDate) {
                $value = $value->format('Y-m-d');
            }
            DOMUtility::setValue($element, $value);
        }
        // héritage des access_rules et appraisal_rule
        $xpaths = [
            'use_parent_description_access_rule' => 'archive_description.access_rule.start_date',
            'use_parent_access_rule' => 'access_rule.start_date',
            'use_parent_appraisal_rule' => 'appraisal_rule.start_date',
        ];
        foreach ($xpaths as $field => $path) {
            if ($entity->get($field)) {
                $xpath = self::MAPPING[$util->namespace][$path];
                $element = $util->xpath->query($xpath, $archiveUnit)->item(0);
                $element?->parentNode->parentNode->removeChild(
                    $element->parentNode
                );
            }
        }
        return $this->saveAsync($entity, $util, $async);
    }

    /**
     * Récupère un élément
     * @param string       $xpath
     * @param DOMNode|null $archiveUnit
     * @param DOMUtility   $util
     * @return DOMElement|DOMNode
     * @throws Exception
     */
    private function getDomElement(string $xpath, ?DOMNode $archiveUnit, DOMUtility $util)
    {
        $element = $this->appendDeep(
            explode('/', $xpath),
            $archiveUnit,
            $util
        );
        // cas particulier
        /** @var DOMElement $parent */
        $parent = $element->parentNode;
        if (
            in_array($parent->tagName, ['Appraisal', 'AppraisalRule', 'AccessRestriction'])
            && $element->tagName === 'Code'
        ) {
            $element->setAttribute('listVersionID', "edition 2009");
        }
        return $element;
    }

    /**
     * Ajoute les noeuds en profondeur, renvoi le dernier noeud
     * @param array      $paths
     * @param DOMNode    $parent
     * @param DOMUtility $util
     * @return DOMElement|DOMNode
     * @throws Exception
     */
    private function appendDeep(array $paths, DOMNode $parent, DOMUtility $util)
    {
        $nextNode = array_shift($paths);
        $node = $util->xpath->query($nextNode, $parent)->item(0);
        if (!$node) {
            [$prefix, $nodeName] = explode(':', $nextNode);
            $newElement = $prefix === 'seda1'
                ? $util->dom->createElementNS(NAMESPACE_SEDA_10, $nodeName)
                : $util->createElement($nodeName);
            $node = $util->appendChild($parent, $newElement);
        }
        return $paths ? $this->appendDeep($paths, $node, $util) : $node;
    }

    /**
     * Logique de sauvegarde asynchrone de la description
     * @param EntityInterface $entity
     * @param DOMUtility      $util
     * @param bool            $async
     * @return bool
     * @throws VolumeException
     */
    public function saveAsync(
        EntityInterface $entity,
        DOMUtility $util,
        bool $async = true
    ): bool {
        /** @var EntityInterface $storedFile */
        $storedFile = Hash::get(
            $entity,
            'archive.description_xml_archive_file.stored_file'
        );
        $stored_file_id = Hash::get(
            $entity,
            'archive.description_xml_archive_file.stored_file.id'
        );
        if (empty($stored_file_id)) {
            $entity->setError('stored_file', 'not found');
            return false;
        }
        $manager = VolumeManager::createWithStoredFileId($stored_file_id);
        $basedir = Configure::read(
            'Archives.updateDescription.path',
            TMP . 'update_description'
        );
        if (!is_dir($basedir)) {
            mkdir($basedir, 0700, true);
        }
        $tmpFile = $basedir . DS . $stored_file_id . '-description.xml';
        if (!$util->dom->save($tmpFile)) {
            unlink($tmpFile);
            Log::error('unable to write on ' . $tmpFile);
            $entity->setError('tmp_file', __("Ecriture du fichier temporaire impossible"));
            return false;
        }
        if (!@$util->dom->schemaValidate($this->schemas[$util->namespace])) {
            unlink($tmpFile);
            Log::error('validation failed');
            $entity->setError('schema', __("Echec lors de la validation du SEDA"));
            if (Configure::read('debug')) {
                Log::debug('validation failed');
                $h = fopen(
                    Configure::read('App.paths.logs', LOGS) . DS . 'debug.log',
                    'a'
                );
                fwrite($h, PHP_EOL . $util->dom->saveXML() . PHP_EOL);
                fclose($h);
            }
            return false;
        }
        // si un fichier lock existe, on attend un peu si un upload est en cours
        $i = 0;
        while (is_file($tmpFile . '.lock') && $i < 10) {
            sleep(1); //NOSONAR
            $i++;
        }
        if (!is_file($tmpFile . '.lock')) {
            file_put_contents(
                $tmpFile . '.lock',
                'uploading since ' . date('Y-m-d H:i:s')
            );
        }
        $duration = Configure::read(
            'Archives.updateDescription.cache_duration',
            600
        );
        $Exec = Utility::get('Exec');
        try {
            if ($async) {
                /** @var EntityInterface $space */
                $space = $manager->__get('space');
                $Exec->defaultStdout = LOGS . 'update_description.log';
                // ex: bin/cake worker_manager 1 upload path/to/foo.bar --overwrite
                $Exec->async(
                    CAKE_SHELL,
                    'volume_manager',
                    'upload',
                    $space->id,
                    $tmpFile,
                    $storedFile->get('name'),
                    [
                        '--overwrite' => null,
                        '--sleep-then-delete' => $duration,
                    ]
                );
            } else {
                $manager->replace($stored_file_id, $tmpFile);
                unlink($tmpFile . '.lock');
                // supprime le fichier tmpFile dans 600s si il n'y a pas de .lock
                $Exec->async(
                    sprintf(
                        'sleep %d && [ ! -f %s ] && rm -f %s',
                        $duration,
                        escapeshellarg($tmpFile . '.lock'),
                        escapeshellarg($tmpFile)
                    )
                );
            }
        } catch (VolumeException $e) {
            Log::error((string)$e);
            $this->exception = $e;
            $entity->setError('exception', $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Ajoute un verrou exclusif pour le calcul du lft et du rght
     * @param EntityInterface $entity
     * @param array           $options
     * @return EntityInterface|false
     * @throws Exception
     */
    public function save(EntityInterface $entity, $options = []): false|EntityInterface
    {
        $conn = $this->getConnection();
        $conn->begin();
        try {
            // lock la table
            if (DatabaseUtility::driverName() === 'pgsql') {
                $conn->execute("LOCK {$this->getTable()} IN EXCLUSIVE MODE;");
            }
            $successEntity = parent::save($entity, $options);
            $conn->commit();
        } catch (Exception $e) {
            $conn->rollback();
            throw $e;
        }
        return $successEntity;
    }

    /**
     * Supprime un archive_unit et tous ses enfants
     * @param EntityInterface $entity
     */
    public function cascadeDelete(EntityInterface $entity)
    {
        $archiveUnits = $this->find()
            ->where(
                $conditions = [
                    'ArchiveUnits.archive_id' => $entity->get('archive_id'),
                    'ArchiveUnits.lft >=' => $entity->get('lft'),
                    'ArchiveUnits.rght <=' => $entity->get('rght'),
                ]
            )
            ->orderBy(['ArchiveUnits.lft' => 'desc']);
        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = TableRegistry::getTableLocator()->get(
            'ArchiveBinaries'
        );
        /** @var EntityInterface $unit */
        foreach ($archiveUnits as $unit) {
            $unit = $this->find()
                ->where(['ArchiveUnits.id' => $unit->id])
                ->contain(
                    [
                        // supprime en 1er les original_data_id non null
                        'ArchiveBinariesArchiveUnits' => function (Query $q) {
                            return $q
                                ->innerJoinWith('ArchiveBinaries')
                                ->orderBy(
                                    ['ArchiveBinaries.original_data_id is null']
                                );
                        },
                    ]
                )
                ->firstOrFail();

            $this->ArchiveBinariesArchiveUnits->deleteAll(
                ['archive_unit_id' => $unit->id]
            );
            $binaries = Hash::extract(
                $unit,
                'archive_binaries_archive_units.{n}.archive_binary_id'
            );
            foreach ($binaries as $binary_id) {
                // vérifi si il reste un lien vers le binary, si il n'y en a plus on supprime
                $exists = $this->ArchiveBinariesArchiveUnits->exists(
                    ['archive_binary_id' => $binary_id]
                );
                if (!$exists) {
                    $ArchiveBinaries->cascadeDelete(
                        $ArchiveBinaries->get($binary_id)
                    );
                    $ArchiveBinaries->deleteAll(['id' => $binary_id]);
                }
            }
            foreach ($this->associations() as $assoc) {
                if ((!$assoc instanceof HasMany) && (!$assoc instanceof hasOne)) {
                    continue;
                }
                $assoc->deleteAll([$assoc->getForeignKey() => $unit->id]);
            }
            $zip = $unit->get('zip');
            if (is_file($zip)) {
                unlink($zip);
            }
        }
        $this->deleteAll($conditions);
        $archive = $entity->get('archive')
            ?: $this->Archives->get(
                $entity->get('archive_id')
            );
        $this->Archives->updateCountSize([$archive]);
        $zip = $entity->get('zip');
        if (is_file($zip)) {
            unlink($zip);
        }
    }

    /**
     * Met à jour les compteurs des archive_units dans $query
     * @param Query|EntityInterface[] $archiveUnits
     */
    public function updateCountSize($archiveUnits)
    {
        $q = $this->query();
        // calcul des valeurs locales
        foreach ($archiveUnits as $archiveUnit) {
            $data = $this->ArchiveBinariesArchiveUnits->find()
                ->select(
                    [
                        'original_local_size' => $q->func()->sum(
                            'StoredFiles.size'
                        ),
                        'original_local_count' => $q->func()->count('*'),
                    ]
                )
                ->innerJoinWith('ArchiveBinaries')
                ->innerJoinWith('ArchiveBinaries.StoredFiles')
                ->where(
                    [
                        'archive_unit_id' => $archiveUnit->id,
                        'ArchiveBinaries.type' => 'original_data',
                    ]
                )
                ->disableHydration()
                ->toArray()
            [0];
            if ($data['original_local_size'] === null) {
                $data['original_local_size'] = 0;
            }
            $this->patchEntity($archiveUnit, $data);
            $this->saveOrFail($archiveUnit);
        }
        // calcul des totaux par somme des "local"
        foreach ($archiveUnits as $archiveUnit) {
            $data = $this->find()
                ->select(
                    [
                        'original_total_size' => $q->func()->sum(
                            'original_local_size'
                        ),
                        'original_total_count' => $q->func()->sum(
                            'original_local_count'
                        ),
                    ]
                )
                ->where(
                    [
                        'archive_id' => $archiveUnit->get('archive_id'),
                        'lft >=' => $archiveUnit->get('lft'),
                        'rght <=' => $archiveUnit->get('rght'),
                    ]
                )
                ->disableHydration()
                ->toArray()
            [0];
            $archiveUnit->set(
                'original_total_size',
                (int)$data['original_total_size']
            );
            $archiveUnit->set(
                'original_total_count',
                (int)$data['original_total_count']
            );
            $this->saveOrFail($archiveUnit);
        }
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws VolumeException
     */
    public function beforeSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $this->deletePublicDescription($entity);
    }

    /**
     * Supprime la description publique lié à l'archive de l'archive_unit
     * @param EntityInterface $entity
     * @throws VolumeException
     */
    public function deletePublicDescription(EntityInterface $entity)
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $archive = $Archives->find()
            ->where(['Archives.id' => $entity->get('archive_id')])
            ->contain(['PublicDescriptionArchiveFiles' => ['StoredFiles']])
            ->first();
        if (
            $archive && ($archiveFile = $archive->get(
                'public_description_archive_file'
            ))
        ) {
            /** @var PublicDescriptionArchiveFilesTable $PublicDescriptionArchiveFiles */
            $PublicDescriptionArchiveFiles = $loc->get(
                'PublicDescriptionArchiveFiles'
            );
            $PublicDescriptionArchiveFiles->deleteOrFail($archiveFile);
            $manager = VolumeManager::createWithStoredFileId(
                $archiveFile->get('stored_file_id')
            );
            $manager->fileDelete(Hash::get($archiveFile, 'stored_file.name'));
            $PublicDescriptionArchiveFiles->deleteAll(
                ['id' => $archiveFile->id]
            );
            $newPubdate = $PublicDescriptionArchiveFiles->nextRebuildDate(
                Hash::get($entity, 'archive_id')
            );
            $archive->set('next_pubdesc', $newPubdate);
            $Archives->save($archive);
        }
    }

    /**
     * Donne l'entité ArchiveUnit lié à un noeud XML
     * @param int        $archive_id
     * @param DOMElement $archiveUnitNode
     * @return EntityInterface|null
     */
    public function findArchiveUnitByDOMElement(
        int $archive_id,
        DOMElement $archiveUnitNode
    ): ?EntityInterface {
        $namespace = $archiveUnitNode->namespaceURI;
        $util = new DOMUtility($archiveUnitNode->ownerDocument);

        // on récupère le chemin xpath sous la force d'array et compatible avec archive_units.xml_node_tagname
        $exp = explode(
            '/',
            ltrim(
                $util->getElementXpath($archiveUnitNode, ''),
                '/'
            )
        );
        $xpathArray = [];
        foreach ($exp as $value) {
            if (
                ($namespace === NAMESPACE_SEDA_21 || $namespace === NAMESPACE_SEDA_22)
                && substr($value, 0, strlen('ArchiveUnit')) !== 'ArchiveUnit'
            ) {
                continue;
            }
            if (!preg_match('/\[\d+]$/', $value)) {
                $value = $value . '[1]';
            }
            $xpathArray[] = $value;
        }
        $currentXmlNodeTagname = array_pop($xpathArray);
        $query = $this->find()
            ->innerJoin(
                ['ArchiveUnits0' => 'archive_units'],
                ['ArchiveUnits0.id' => new IdentifierExpression("ArchiveUnits.id")]
            )
            ->where(
                [
                    'ArchiveUnits.archive_id' => $archive_id,
                    'ArchiveUnits.xml_node_tagname' => $currentXmlNodeTagname,
                ]
            );
        $lastJoin = 'ArchiveUnits0';
        foreach (array_reverse($xpathArray) as $key => $xmlNodeTagname) {
            $i = $key + 1;
            $query->innerJoin(
                ["ArchiveUnits$i" => 'archive_units'],
                ["ArchiveUnits$i.id" => new IdentifierExpression("ArchiveUnits$key.parent_id")]
            );
            $lastJoin = "ArchiveUnits$i";
        }
        $query->where(["$lastJoin.parent_id IS" => null]);

        return $query->first();
    }

    /**
     * Met à jour max_keywords_access_end_date et description_access_rule_id
     * @param EntityInterface $entity
     * @return void
     */
    public function updateLinksToAccessRules(EntityInterface $entity): void
    {
        $values = $this->find()
            ->where(['ArchiveUnits.id' => $entity->id])
            ->contain(
                [
                    'AccessRules',
                    'ArchiveDescriptions' => ['AccessRules'],
                    'ArchiveKeywords' => ['AccessRules'],
                ]
            )
            ->disableHydration()
            ->firstOrFail();
        $archiveDescriptionId = Hash::get($values, 'archive_description.access_rule_id');
        $endDates = Hash::extract($values, 'archive_keywords.{n}.access_rule.end_date');
        $keywordEndDate = $endDates ? max($endDates) : null;

        $this->updateAll(
            [
                'max_keywords_access_end_date' => $keywordEndDate,
                'description_access_rule_id' => $archiveDescriptionId,
            ],
            ['id' => $entity->id]
        );
        $entity->set('max_keywords_access_end_date', $keywordEndDate);
        $entity->set('description_access_rule_id', $archiveDescriptionId);
    }

    /**
     * Met à jour les max_aggregated_access_end_date
     * @param array         $conditions
     * @param callable|null $fn
     * @return int
     */
    public function calcMaxAggregatedAccessEndDate(array $conditions = [], ?callable $fn = null): int
    {
        $q = $this->selectQuery();
        $latestDates = $this->find()
            ->select(
                [
                    'id' => 'ArchiveUnits.id',
                    'ar_end_date' => $q->func()->max('ar.end_date', ['date']),
                    'adar_end_date' => $q->func()->max('adar.end_date', ['date']),
                    'ak_end_date' => $q->func()->max('au.max_keywords_access_end_date', ['date']),
                ]
            )
            ->innerJoin(
                ['au' => 'archive_units'],
                [
                    'au.lft >=' => new IdentifierExpression('ArchiveUnits.lft'),
                    'au.rght <=' => new IdentifierExpression('ArchiveUnits.rght'),
                ]
            )
            ->innerJoin(
                ['ar' => 'access_rules'],
                ['ar.id' => new IdentifierExpression('au.access_rule_id')]
            )
            ->leftJoin(
                ['adar' => 'access_rules'],
                ['adar.id' => new IdentifierExpression('au.description_access_rule_id')]
            )
            ->where(
                [
                    'au.archive_id' => new IdentifierExpression('ArchiveUnits.archive_id'),
                    'au.lft >=' => new IdentifierExpression('ArchiveUnits.lft'),
                    'au.rght <=' => new IdentifierExpression('ArchiveUnits.rght'),
                ]
            )
            ->orderByAsc('ArchiveUnits.id')
            ->groupBy('ArchiveUnits.id')
            ->disableHydration();
        $countQuery = $this->find();
        if ($conditions) {
            $latestDates = $latestDates->andWhere($conditions);
            $countQuery = $countQuery->andWhere($conditions);
        }
        $count = $countQuery->count();
        $i = 0;
        foreach ($latestDates as $archiveUnit) {
            if ($fn) {
                $fn($i, $count, 'archive_unit');
            }
            $i++;
            $this->updateAll(
                [
                    'max_aggregated_access_end_date' => max(
                        $archiveUnit['ar_end_date'],
                        $archiveUnit['adar_end_date'],
                        $archiveUnit['ak_end_date'],
                    ),
                ],
                ['id' => $archiveUnit['id']]
            );
        }
        return $count;
    }

    /**
     * Met à jour max_keywords_access_end_date pour une $archiveUnit donné
     * @param EntityInterface $entity
     * @return void
     */
    public function updateMaxKeywordsAccessEndDate(EntityInterface $entity): void
    {
        $q = $this->selectQuery();
        $ArchiveKeywords = $this->associations()->get('ArchiveKeywords');
        $subquery = $ArchiveKeywords->find()
            ->select(['access_rule_id' => $q->func()->max('AccessRules.end_date')])
            ->innerJoinWith('AccessRules')
            ->where(
                [
                    'ArchiveKeywords.archive_unit_id' => new IdentifierExpression('archive_units.id'),
                    'ArchiveKeywords.access_rule_id !='
                        => new IdentifierExpression('archive_units.description_access_rule_id'),
                ]
            );
        $this->updateQuery()
            ->set(['max_keywords_access_end_date' => $subquery])
            ->where(['id' => $entity->id, 'xml_node_tagname NOT LIKE' => 'ArchiveUnit%'])
            ->execute();
    }

    /**
     * Calcul les access_rules.end_date pour un archive_unit donné et le compare
     * au max_aggregated_access_end_date de ses enfants
     * met à jour max_aggregated_access_end_date avec la valeur la plus élevée
     * et ses parents
     * @param EntityInterface $entity
     * @return int
     * @throws Exception
     */
    public function updateMaxAggregatedAccessEndDate(EntityInterface $entity): int
    {
        $q = $this->selectQuery();
        $subquery = $this->find()
            ->select(['max_aggregated_access_end_date' => $q->func()->max('au.max_aggregated_access_end_date')])
            ->from(['au' => 'archive_units'])
            ->where(
                [
                    'au.archive_id' => new IdentifierExpression($this->getAlias() . '.archive_id'),
                    'au.lft >' => new IdentifierExpression($this->getAlias() . '.lft'),
                    'au.rght <' => new IdentifierExpression($this->getAlias() . '.rght'),
                ]
            );
        $contains = $this->find()
            ->select(
                [
                    'max_subquery_value' => $subquery,
                ]
            )
            ->where(['ArchiveUnits.id' => $entity->id])
            ->contain(
                [
                    'ParentArchiveUnits',
                    'AccessRules',
                    'ArchiveDescriptions' => ['AccessRules'],
                    'ArchiveKeywords' => ['AccessRules'],
                ]
            )
            ->enableAutoFields()
            ->firstOrFail();
        $subqueryValue = $contains->get('max_subquery_value');
        $endDates = max(
            array_merge(
                [
                    $subqueryValue ? new CoreDate($subqueryValue) : null,
                    Hash::get($contains, 'access_rule.end_date'),
                    Hash::get($contains, 'archive_description.access_rule.end_date'),
                ],
                Hash::extract($contains, 'archive_keywords.{n}.access_rule.end_date'),
            )
        );
        // si null ou date plus grande, on met à jour et on propage aux parents
        if (
            $entity->get('max_aggregated_access_end_date') === null
            || $endDates > $entity->get('max_aggregated_access_end_date')
        ) {
            $entity->set('max_aggregated_access_end_date', $endDates);
            $this->save($entity);
            return 1 + $this->updateAll(
                ['max_aggregated_access_end_date' => $endDates],
                [
                    'archive_id' => $entity->get('archive_id'),
                    'lft <' => $entity->get('lft'),
                    'rght >' => $entity->get('rght'),
                    'OR' => [
                        'max_aggregated_access_end_date IS' => null,
                        'max_aggregated_access_end_date <' => $endDates,
                    ],
                ]
            );
            // Si la date est plus petite, il faut recalculer la date de chaques parents
        } elseif ($endDates < $entity->get('max_aggregated_access_end_date')) {
            $entity->set('max_aggregated_access_end_date', $endDates);
            return $this->calcMaxAggregatedAccessEndDate(
                [
                    'ArchiveUnits.archive_id' => $entity->get('archive_id'),
                    'ArchiveUnits.lft <=' => $entity->get('lft'),
                    'ArchiveUnits.rght >=' => $entity->get('rght'),
                ]
            );
        }
        return 0;
    }
}
