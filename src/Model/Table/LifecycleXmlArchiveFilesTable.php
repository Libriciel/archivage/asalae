<?php

/**
 * Asalae\Model\Table\LifecycleXmlArchiveFilesTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Utility\DOMUtility;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\TableRegistry;
use DOMDocument;
use DOMElement;

/**
 * Table archive_files (xxx_lifecycle.xml)
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class LifecycleXmlArchiveFilesTable extends ArchiveFilesTable
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('archive_files');

        $this->belongsTo('Archives')
            ->setForeignKey('archive_id')
            ->setConditions(['LifecycleXmlArchiveFiles.type' => ArchiveFilesTable::TYPE_LIFECYCLE]);
        $this->belongsTo('StoredFiles');
        $this->belongsTo('Lifecycles')
            ->setClassName('StoredFiles')
            ->setForeignKey('stored_file_id');
    }

    /**
     * Donne une liste d'evenements PREMIS contenu dans le $dom avec pagination
     * @param DOMDocument $dom
     * @param array       $types filtres sur le eventType
     * @param int         $page
     * @param int         $nb    nombre d'élements sur une page
     * @return array
     */
    public function paginate(
        DOMDocument $dom,
        array $types,
        int $page = 1,
        int $nb = 100
    ): array {
        $util = new DOMUtility($dom);
        $contents = $this->prepareXpathQuery($types);
        $query = $util->xpath->query("ns:event/ns:eventType[$contents]");
        $output = [];
        $offset = $page * $nb - $nb;
        $users = [];
        $Users = TableRegistry::getTableLocator()->get('Users');
        $Crons = TableRegistry::getTableLocator()->get('Crons');
        foreach ($query as $element) {
            if (!$element instanceof DOMElement) {
                continue;
            }
            if ($offset) {
                $offset--;
                continue;
            }
            /** @var DOMElement $event */
            $event = $element->parentNode;
            $eventDateTime = $event->getElementsByTagName('eventDateTime')
                ->item(0);
            $eventOutcomeDetailNote = $event->getElementsByTagName(
                'eventOutcomeDetailNote'
            )->item(0);
            $agent = $event->getElementsByTagName('linkingAgentIdentifierValue')
                ->item(0);
            $user = null;
            if ($agent && strpos($agent->nodeValue, 'Users:') === 0) {
                $user_id = substr($agent->nodeValue, 6);
                if (
                    !isset($users[$user_id]) && ($user = $Users->find()->where(
                        ['id' => $user_id]
                    )->first())
                ) {
                    $name = $user->get('name');
                    $users[$user_id] = $user->get('username') . ($name
                            ? ' - ' . $name : '');
                }
                $user = $users[$user_id] ?? null;
            } elseif ($agent && strpos($agent->nodeValue, 'Crons:') === 0) {
                $cron_id = substr($agent->nodeValue, 6);
                if (
                    !isset($crons['cron:' . $cron_id]) && ($cron = $Crons->find()->where(['id' => $cron_id])->first())
                ) {
                    $crons['cron:' . $cron_id] = 'Cron: ' . $cron->get('name');
                }
                $user = $crons['cron:' . $cron_id] ?? null;
            } elseif ($agent) {
                if (!isset($users[$agent->nodeValue])) {
                    $contents = $this->prepareXpathQuery([$agent->nodeValue]);
                    $agentName = $util->xpath->query(
                        "ns:agent/ns:agentIdentifier/ns:agentIdentifierValue[$contents]/../../ns:agentName"
                    )
                        ->item(0);
                    if ($agentName) {
                        $users[$agent->nodeValue] = $agent->nodeValue . ' - ' . $agentName->nodeValue;
                    }
                }
                $user = $users[$agent->nodeValue] ?? null;
            }
            $date = new CakeDateTime($eventDateTime->nodeValue);
            $identifier = str_replace(
                ['/ns:'],
                '',
                $util->getElementXpath($element)
            );
            $identifier = str_replace(['[', ']'], '-', $identifier);
            $output[] = [
                'identifier' => $identifier,
                'date' => $date,
                'text' => $eventOutcomeDetailNote->nodeValue,
                'user' => $user,
            ];
            if (!--$nb) {
                break;
            }
        }
        return $output;
    }

    /**
     * Donne un contains text() multiple
     * @param array $types
     * @return string
     */
    private function prepareXpathQuery(array $types): string
    {
        $contents = [];
        foreach ($types as $type) {
            $esc = DOMUtility::xpathQuote($type);
            $contents[] = "contains(text(), $esc)";
        }
        return implode(' or ', $contents);
    }

    /**
     * Donne le count d'evenements d'une selon une liste d'eventType
     * @param DOMDocument $dom
     * @param array       $types
     * @return int
     */
    public function countTypes(DOMDocument $dom, array $types): int
    {
        $util = new DOMUtility($dom);
        $contents = $this->prepareXpathQuery($types);
        $query = $util->xpath->query("ns:event/ns:eventType[$contents]");
        return $query->count();
    }
}
