<?php

/**
 * Asalae\Model\Table\AccessRulesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\BeforeSaveInterface;
use AsalaeCore\ORM\Table as CoreTable;
use Cake\Event\Event;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateInterval;
use DateTimeInterface;
use Exception;

/**
 * Table access_rules
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AccessRulesTable extends CoreTable implements BeforeSaveInterface
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('AccessRuleCodes');
        $this->hasMany('Archives');
        $this->hasMany('ArchiveDescriptions');
        $this->hasMany('ArchiveKeywords');
        $this->hasMany('ArchiveUnits');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->requirePresence('start_date', 'create')
            ->notEmptyDate('start_date');

        $validator
            ->date('end_date')
            ->requirePresence('end_date', 'create')
            ->notEmptyDate('end_date');

        return $validator;
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $date = $entity->get('start_date');
        if (!($date instanceof DateTimeInterface || $date instanceof CakeDate)) {
            $date = CakeDateTime::parseDate($date);
        }
        $dirty = $entity->isDirty('access_rule_code_id')
            || $entity->isDirty('start_date');
        if (
            !($date instanceof DateTimeInterface || $date instanceof CakeDate) || $entity->isDirty(
                'end_date'
            ) || !$dirty
        ) {
            return;
        }
        if ($entity->isDirty('access_rule_code_id')) {
            $entity->unset('access_rule_code');
        }
        if (
            !$duration = Hash::get($entity, 'access_rule_code.duration')
        ) {
            if (!$entity->get('access_rule_code_id')) {
                return;
            }
            $access = TableRegistry::getTableLocator()
                ->get('AccessRuleCodes')
                ->find()
                ->select(['duration'])
                ->where(['id' => $entity->get('access_rule_code_id')])
                ->disableHydration()
                ->first();
            if (!$access) {
                return;
            }
            $duration = $access['duration'];
        }
        $endDate = (clone $date)->add(new DateInterval($duration));
        $entity->set('end_date', $endDate);
    }
}
