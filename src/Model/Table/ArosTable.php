<?php

/**
 * Asalae\Model\Table\ArosTable
 */

namespace Asalae\Model\Table;

use Acl\Model\Table\ArosTable as AclArosTable;
use Cake\ORM\Behavior\TreeBehavior;

/**
 * Table aros
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TreeBehavior
 */
class ArosTable extends AclArosTable
{
    /**
     * Initialize a table instance. Called after the constructor.
     *
     * You can use this method to define associations, attach behaviors
     * define validation and do any other initialization logic you need.
     *
     * ```
     *  public function initialize(array $config)
     *  {
     *      $this->belongsTo('Users');
     *      $this->belongsToMany('Tagging.Tags');
     *      $this->setPrimaryKey('something_else');
     *  }
     * ```
     *
     * @param array<string, mixed> $config Configuration options passed to the constructor
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Tree', ['type' => 'nested']);

        $this->belongsToMany('Acos')
            ->setThrough('ArosAcos');
        $this->hasMany('ArosAcos');
    }
}
