<?php

/**
 * Asalae\Model\Table\AgreementsOriginatingAgenciesTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;

/**
 * Table agreements_originating_agencies
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AgreementsOriginatingAgenciesTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Agreements');
        $this->belongsTo('OrgEntities');
    }
}
