<?php

/**
 * Asalae\Model\Table\ArchiveUnitsDeliveryRequestsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;

/**
 * Table archive_units_delivery_requests
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveUnitsDeliveryRequestsTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('ArchiveUnits');
        $this->belongsTo('DeliveryRequests');
    }
}
