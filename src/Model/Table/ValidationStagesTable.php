<?php

/**
 * Asalae\Model\Table\ValidationStagesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\Model\Table\BeforeSaveInterface;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Table validation_stages
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin OptionsBehavior
 * @property ValidationActorsTable ValidationActors
 */
class ValidationStagesTable extends Table implements AfterSaveInterface, BeforeSaveInterface, AfterDeleteInterface
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'all_actors_to_complete' => [
                    true,
                    false,
                ],
            ]
        );

        $this->belongsTo('ValidationChains');
        $this->hasMany('ValidationActors');
        $this->hasMany('ValidationProcesses')
            ->setForeignKey('current_stage_id');

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('name');
        $validator->add(
            'ord',
            'custom',
            [
                'rule' => function ($value) {
                    return (int)$value == $value && $value > 0;
                },
                'message' => __("La valeur n'est pas valide"),
            ]
        );
        $validator->add(
            'name',
            [
                'unique' => [
                    'rule' => [
                        'validateUnique',
                        ['scope' => 'validation_chain_id'],
                    ],
                    'provider' => 'table',
                    'message' => __("Un enregistrement similaire existe déjà"),
                ],
            ]
        );

        return $validator;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $conditions = [
            'validation_chain_id' => $entity->get('validation_chain_id'),
            'id !=' => $entity->get('id'),
        ];
        if ($entity->isNew()) {
            $conditions['ord >='] = $entity->get('ord');
            $this->updateAll(
                ['ord' => new QueryExpression('ord + 1')],
                $conditions
            );
        } elseif ($entity->isDirty('ord')) {
            $original = $entity->getOriginal('ord');
            $new = $entity->get('ord');
            $min = min($original, $new);
            $max = max($original, $new);
            $conditions[] = function (QueryExpression $exp) use ($min, $max) {
                return $exp->between('ord', $min, $max);
            };

            if ($original == $new) {
                /** @noinspection PhpUnnecessaryStopStatementInspection */
                return;
            } elseif ($original > $new) {
                $this->updateAll(
                    ['ord' => new QueryExpression('ord + 1')],
                    $conditions
                );
            } else {
                $this->updateAll(
                    ['ord' => new QueryExpression('ord - 1')],
                    $conditions
                );
            }
        }
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        if ($entity->isNew() && !$entity->get('ord')) {
            $last = $this->find()
                ->select(['ord'])
                ->where(
                    [
                        'validation_chain_id' => $entity->get(
                            'validation_chain_id'
                        ),
                    ]
                )
                ->orderBy(['ord' => 'desc'])
                ->first();
            $lastOrder = $last ? $last->get('ord') : 0;
            $entity->set('ord', $lastOrder + 1);
        }
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $this->updateAll(
            ['ord' => new QueryExpression('ord - 1')],
            [
                'validation_chain_id' => $entity->get('validation_chain_id'),
                'ord >' => $entity->get('ord'),
            ]
        );
    }

    /**
     * find('list') des stages d'un user
     * @param integer               $saId
     * @param array|EntityInterface $user
     * @return Query
     */
    public function findListForUser($saId, $user): Query
    {
        $userId = Hash::get($user, 'id');
        $orgEntityId = Hash::get($user, 'org_entity_id');
        $isValidator = Hash::get($user, 'is_validator');
        $typeEntity = Hash::get($user, 'org_entity.type_entity.code');
        $conditions = [
            'OR' => [
                [
                    'ValidationActors.app_foreign_key' => $userId,
                    'ValidationActors.app_type' => 'USER',
                    'ValidationChains.org_entity_id' => $saId,
                ],
            ],
        ];
        if (in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)) {
            $orgEntityId = $saId;
        }

        if ($isValidator && $orgEntityId === $saId) {
            $conditions['OR'][] = [
                'ValidationActors.app_type' => 'SERVICE_ARCHIVES',
                'ValidationChains.org_entity_id' => $saId,
            ];
        }
        return $this->find(
            'list',
            keyField: 'id',
            valueField: 'name',
            groupField: '_matchingData.ValidationChains.name',
        )
            ->select(
                [
                    'ValidationStages.name',
                    'ValidationStages.id',
                    'ValidationChains.name',
                ]
            )
            ->innerJoinWith('ValidationChains')
            ->innerJoinWith(
                'ValidationProcesses'
            ) // filtre current_stage_id != null
            ->innerJoinWith('ValidationActors')
            ->where($conditions)
            ->orderBy(['ValidationChains.name' => 'asc', 'ValidationStages.ord']);
    }
}
