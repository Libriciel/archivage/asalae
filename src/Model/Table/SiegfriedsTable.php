<?php

/**
 * Asalae\Model\Table\SiegfriedsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table siegfrieds
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SiegfriedsTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Fileuploads');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('pronom')
            ->maxLength('pronom', 255)
            ->allowEmptyString('pronom');

        $validator
            ->scalar('format')
            ->maxLength('format', 255)
            ->allowEmptyString('format');

        $validator
            ->scalar('version')
            ->maxLength('version', 255)
            ->allowEmptyString('version');

        $validator
            ->scalar('mime')
            ->maxLength('mime', 255)
            ->allowEmptyString('mime');

        $validator
            ->scalar('basis')
            ->maxLength('basis', 512)
            ->allowEmptyString('basis');

        $validator
            ->scalar('warning')
            ->maxLength('warning', 512)
            ->allowEmptyString('warning');

        return $validator;
    }
}
