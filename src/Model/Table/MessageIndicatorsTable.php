<?php

/**
 * Asalae\Model\Table\MessageIndicatorsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table message_indicators
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MessageIndicatorsTable extends Table
{
    /**
     * Messages Types
     */
    public const string IN_TRANSFER = 'IN_TRANSFER';
    public const string DELIVERY = 'DELIVERY';
    public const string DESTRUCTION = 'DESTRUCTION';
    public const string RESTITUTION = 'RESTITUTION';
    public const string OUT_TRANSFER = 'OUT_TRANSFER';

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('OrgEntities')
            ->setForeignKey('archival_agency_id');
        $this->belongsTo('OrgEntities')
            ->setForeignKey('transferring_agency_id');
        $this->belongsTo('OrgEntities')
            ->setForeignKey('originating_agency_id');
        $this->belongsTo('Agreements');
        $this->belongsTo('Profiles');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->date('message_date')
            ->requirePresence('message_date', 'create')
            ->notEmptyDate('message_date');

        $validator
            ->scalar('message_type')
            ->maxLength('message_type', 255)
            ->requirePresence('message_type', 'create')
            ->notEmptyString('message_type');

        $validator
            ->integer('message_count')
            ->requirePresence('message_count', 'create')
            ->notEmptyString('message_count');

        $validator
            ->boolean('accepted')
            ->requirePresence('accepted', 'create')
            ->notEmptyString('accepted');

        $validator
            ->boolean('derogation')
            ->requirePresence('derogation', 'create')
            ->notEmptyString('derogation');

        $validator
            ->integer('validation_duration')
            ->requirePresence('validation_duration', 'create')
            ->notEmptyString('validation_duration');

        $validator
            ->integer('original_files_count')
            ->requirePresence('original_files_count', 'create')
            ->notEmptyFile('original_files_count');

        $validator
            ->requirePresence('original_files_size', 'create')
            ->notEmptyFile('original_files_size');

        return $validator;
    }
}
