<?php

/**
 * Asalae\Model\Table\DescriptionXmlArchiveFilesTable
 */

namespace Asalae\Model\Table;

/**
 * Table archive_files (xxx_description.xml)
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DescriptionXmlArchiveFilesTable extends ArchiveFilesTable
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('archive_files');
        $this->belongsTo('Archives')
            ->setForeignKey('archive_id')
            ->setConditions(
                ['DescriptionXmlArchiveFiles.type' => ArchiveFilesTable::TYPE_DESCRIPTION]
            );
        $this->belongsTo('StoredFiles');
    }
}
