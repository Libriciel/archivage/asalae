<?php

/**
 * Asalae\Model\Table\DeletedJsonArchiveFilesTable
 */

namespace Asalae\Model\Table;

/**
 * Table archive_files (xxx_deleted.json)
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DeletedJsonArchiveFilesTable extends ArchiveFilesTable
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('archive_files');
        $this->belongsTo('Archives')
            ->setForeignKey('archive_id')
            ->setConditions(['DeletedJsonArchiveFiles.type' => ArchiveFilesTable::TYPE_DELETED]);
        $this->belongsTo('StoredFiles');
    }
}
