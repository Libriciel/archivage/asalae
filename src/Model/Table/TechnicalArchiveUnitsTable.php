<?php

/**
 * Asalae\Model\Table\TechnicalArchiveUnitsTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Behavior\OptionsBehavior;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Behavior\TreeBehavior;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table technical_archive_units
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @mixin TreeBehavior
 * @mixin StateMachineBehavior
 * @property ArchiveBinariesTechnicalArchiveUnitsTable ArchiveBinariesTechnicalArchiveUnits
 */
class TechnicalArchiveUnitsTable extends Table
{
    public const string S_AVAILABLE = 'available';
    public const string S_DESTROYING = 'destroying';

    public const string T_DESTRUCT = 'destruct';
    public const string T_CANCEL = 'cancel';

    /**
     * @var string Etat initial
     */
    public $initialState = self::S_AVAILABLE;

    /**
     * @var array Changement d'êtats selon action
     */
    public $transitions = [
        self::T_DESTRUCT => [
            self::S_AVAILABLE => self::S_DESTROYING,
        ],
        self::T_CANCEL => [
            self::S_DESTROYING => self::S_AVAILABLE,
        ],
    ];

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Tree');
        $this->addBehavior('StateMachine.StateMachine');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    self::S_AVAILABLE,
                    self::S_DESTROYING,
                ],
            ]
        );

        $this->belongsTo('TechnicalArchives');
        $this->belongsTo('ParentTechnicalArchiveUnits')
            ->setForeignKey('parent_id')
            ->setClassName('TechnicalArchiveUnits');
        $this->hasMany('ChildTechnicalArchiveUnits')
            ->setForeignKey('parent_id')
            ->setClassName('TechnicalArchiveUnits');
        $this->hasOne('LastChildTechnicalArchiveUnits')
            ->setForeignKey('parent_id')
            ->setClassName('TechnicalArchiveUnits')
            ->setConditions(
                [
                    'LastChildTechnicalArchiveUnits.rght'
                    => new QueryExpression($this->getAlias() . '.rght - 1'),
                ]
            );
        $this->belongsToMany('ArchiveBinaries')->setDependent(true);
        $this->hasMany('ArchiveBinariesTechnicalArchiveUnits');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->scalar('archival_agency_identifier')
            ->maxLength('archival_agency_identifier', 255)
            ->requirePresence('archival_agency_identifier', 'create')
            ->notEmptyString('archival_agency_identifier');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('history')
            ->allowEmptyString('history');

        $validator
            ->date('oldest_date')
            ->allowEmptyDate('oldest_date');

        $validator
            ->date('latest_date')
            ->allowEmptyDate('latest_date');

        $validator
            ->requirePresence('original_local_size', 'create')
            ->notEmptyString('original_local_size');

        $validator
            ->integer('original_local_count')
            ->requirePresence('original_local_count', 'create')
            ->notEmptyString('original_local_count');

        $validator
            ->requirePresence('original_total_size', 'create')
            ->notEmptyString('original_total_size');

        $validator
            ->integer('original_total_count')
            ->requirePresence('original_total_count', 'create')
            ->notEmptyString('original_total_count');

        $validator
            ->scalar('xml_node_tagname')
            ->maxLength('xml_node_tagname', 255)
            ->requirePresence('xml_node_tagname', 'create')
            ->allowEmptyString('xml_node_tagname');

        return $validator;
    }

    /**
     * Met à jour les compteurs des archive_units dans $query
     * @param Query|EntityInterface[] $technicalArchiveUnits
     */
    public function updateCountSize($technicalArchiveUnits)
    {
        $q = $this->query();
        // calcul des valeurs locales
        foreach ($technicalArchiveUnits as $technicalArchiveUnit) {
            $data = $this->ArchiveBinariesTechnicalArchiveUnits->find()
                ->select(
                    [
                        'original_local_size' => $q->func()->sum('StoredFiles.size'),
                        'original_local_count' => $q->func()->count('*'),
                    ]
                )
                ->innerJoinWith('ArchiveBinaries')
                ->innerJoinWith('ArchiveBinaries.StoredFiles')
                ->where(
                    [
                        'technical_archive_unit_id' => $technicalArchiveUnit->id,
                        'ArchiveBinaries.type' => 'original_data',
                    ]
                )
                ->disableHydration()
                ->toArray()
            [0];
            if ($data['original_local_size'] === null) {
                $data['original_local_size'] = 0;
            }
            $this->patchEntity($technicalArchiveUnit, $data);
            $this->saveOrFail($technicalArchiveUnit);
        }
        // calcul des totaux par somme des "local"
        foreach ($technicalArchiveUnits as $technicalArchiveUnit) {
            $data = $this->find()
                ->select(
                    [
                        'original_total_size' => $q->func()->sum('original_local_size'),
                        'original_total_count' => $q->func()->sum('original_local_count'),
                    ]
                )
                ->where(
                    [
                        'technical_archive_id' => $technicalArchiveUnit->get('technical_archive_id'),
                        'lft >=' => $technicalArchiveUnit->get('lft'),
                        'rght <=' => $technicalArchiveUnit->get('rght'),
                    ]
                )
                ->disableHydration()
                ->toArray()
            [0];
            $technicalArchiveUnit->set(
                'original_total_size',
                (int)$data['original_total_size']
            );
            $technicalArchiveUnit->set(
                'original_total_count',
                (int)$data['original_total_count']
            );
            $this->saveOrFail($technicalArchiveUnit);
        }
    }
}
