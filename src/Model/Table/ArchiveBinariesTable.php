<?php

/**
 * Asalae\Model\Table\ArchiveBinariesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\DataType\JsonString;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\BeforeMarshalInterface;
use AsalaeCore\Utility\DOMUtility;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\HasOne;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use DOMElement;
use DOMNode;

/**
 * Table archive_binaries
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 */
class ArchiveBinariesTable extends Table implements SerializeDeletedEntitiesInterface, BeforeMarshalInterface
{
    use SerializeDeleteAllTrait;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'type' => [
                    'original_data',
                    'original_timestamp',
                    'preservation_data',
                    'preservation_timestamp',
                    'dissemination_data',
                    'dissemination_timestamp',
                ],
            ]
        );

        $this->belongsTo('StoredFiles');
        $this->belongsTo('OriginalDatas')
            ->setClassName('ArchiveBinaries');
        $this->hasMany('OtherDatas')
            ->setForeignKey('original_data_id')
            ->setClassName('ArchiveBinaries');
        $this->hasMany('ArchiveBinariesArchiveUnits');
        $this->belongsToMany('ArchiveUnits');
        $this->belongsToMany('TechnicalArchiveUnits');
        $this->hasMany('ArchiveBinariesTechnicalArchiveUnits');
        $this->hasMany('ArchiveBinaryConversions');
        $this->hasMany('ArchiveBinariesArchiveUnits');
        $this->belongsTo('Pronoms')
            ->setForeignKey(false)
            ->setConditions(
                [
                    'Pronoms.puid' => new QueryExpression(
                        $this->getAlias() . '.format'
                    ),
                ]
            );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('is_integrity_ok')
            ->allowEmptyString('is_integrity_ok');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('filename')
            ->maxLength('filename', 2048)
            ->requirePresence('filename', 'create')
            ->notEmptyFile('filename');

        $validator
            ->scalar('format')
            ->maxLength('format', 255)
            ->allowEmptyString('format');

        $validator
            ->scalar('mime')
            ->maxLength('mime', 255)
            ->allowEmptyString('mime');

        $validator
            ->scalar('extension')
            ->maxLength('extension', 255)
            ->allowEmptyString('extension');

        return $validator;
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }

    /**
     * The Model.beforeMarshall modify request data before it is converted into
     * entities.
     *
     * @param Event       $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     */
    public function beforeMarshal(
        Event $event,
        ArrayObject $data,
        ArrayObject $options
    ) {
        if (isset($data['app_meta']) && is_array($data['app_meta'])) {
            $data['app_meta'] = (string)new JsonString($data['app_meta']);
        }
    }

    /**
     * Supprime le binary et tous ses liens
     * - archive_binaries_archive_units
     * - archive_binaries_technical_archive_units
     * - archive_binary_conversions
     * @param EntityInterface $entity
     */
    public function cascadeDelete(EntityInterface $entity)
    {
        foreach ($this->associations() as $assoc) {
            if ((!$assoc instanceof HasMany) && (!$assoc instanceof hasOne)) {
                continue;
            }
            $assoc->deleteAll([$assoc->getForeignKey() => $entity->id]);
        }
        $this->delete($entity);
    }

    /**
     * Trouve le binary portant le $filename dans $util
     * @param DOMUtility   $util
     * @param string       $filename
     * @param DOMNode|null $contextNode
     * @return DOMElement|null
     */
    public static function findBinaryByFilename(
        DOMUtility $util,
        string $filename,
        DOMNode $contextNode = null
    ): ?DOMNode {
        $quoted = DOMUtility::xpathQuote($filename);
        $normalized = trim(preg_replace('/\s+/', ' ', $quoted));
        switch ($util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                return $util->node(
                    '//ns:Attachment[@filename=' . $quoted . ']/..',
                    $contextNode
                );
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $path = sprintf(
                    '//ns:Filename[normalize-space(.)=%s]/../..',
                    $normalized
                );
                if ($node = $util->node($path, $contextNode)) {
                    return $node;
                }
                $path = '//ns:Attachment[@filename=' . $quoted . ']/..';
                if ($node = $util->node($path, $contextNode)) {
                    return $node;
                }
                $path = sprintf(
                    '//ns:Uri[normalize-space(.)=%s]/..',
                    $normalized
                );
                return $util->node($path, $contextNode);
        }
        return null;
    }

    /**
     * Donne la liste des fichiers contenus dans $util
     * @param DOMUtility $util
     * @param DOMNode    $contextNode ArchiveUnit
     * @return array
     */
    public static function listBinariesInDocument(
        DOMUtility $util,
        DOMNode $contextNode = null
    ): array {
        if (in_array($util->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            if ($contextNode && $contextNode->nodeName === 'BinaryDataObject') {
                $documents = [$contextNode];
            } else {
                $documents = $util->xpath->query('.//ns:BinaryDataObject', $contextNode);
            }
            /** @var DOMElement $document */
            foreach ($documents as $document) {
                $filename = trim(self::getBinaryFilename($util, $document));
                if ($filename) {
                    $filenames[$filename] = $document;
                }
            }
        } else {
            $documents = $util->xpath->query('.//ns:Attachment', $contextNode);
            /** @var DOMElement $document */
            foreach ($documents as $document) {
                $filename = $document->getAttribute('filename');
                $filenames[$filename] = $document;
            }
        }
        return $filenames ?? [];
    }

    /**
     * Donne le filename contenu dans un Document/BinaryDataObject
     * @param DOMUtility $util
     * @param DOMNode    $binaryData
     * @return string|null
     */
    public static function getBinaryFilename(DOMUtility $util, DOMNode $binaryData): ?string
    {
        if ($filenameNode = $util->node('ns:FileInfo/ns:Filename', $binaryData)) {
            $filename = $filenameNode->nodeValue;
        } elseif ($attachment = $util->node('ns:Attachment', $binaryData)) {
            /** @var DOMElement $attachment */
            $filename = $attachment->getAttribute('filename');
        } elseif ($uri = $util->node('ns:Uri', $binaryData)) {
            $filename = $uri->nodeValue;
        }
        return $filename ?? null;
    }
}
