<?php

/**
 * Asalae\Model\Table\CountersTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\CountersTable as CoreCountersTable;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;

/**
 * Table counters
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @property SequencesTable $Sequences
 */
class CountersTable extends CoreCountersTable implements SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }
}
