<?php

/**
 * Asalae\Model\Table\DeliveryRequestsTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Model\Table\StateChangeInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateTime;
use Exception;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table delivery_requests
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin StateMachineBehavior
 * @mixin OptionsBehavior
 * @property HasMany|ArchiveUnitsDeliveryRequestsTable ArchiveUnitsDeliveryRequests
 */
class DeliveryRequestsTable extends Table implements
    AfterSaveInterface,
    StateChangeInterface,
    BeforeDeleteInterface
{
    public const string S_CREATING = 'creating';
    public const string S_VALIDATING = 'validating';
    public const string S_DELIVERY_AVAILABLE = 'delivery_available';
    public const string S_DELIVERY_DOWNLOADED = 'delivery_downloaded';
    public const string S_DELIVERY_ACQUITTED = 'delivery_acquitted';
    public const string S_REJECTED = 'rejected';
    public const string S_ACCEPTED = 'accepted';
    public const string S_DELIVERY_DELETED = 'delivery_deleted';

    public const string T_SEND = 'send';
    public const string T_REFUSE = 'refuse';
    public const string T_ACCEPT = 'accept';
    public const string T_BUILD_DELIVERY = 'build_delivery';
    public const string T_DOWNLOAD_DELIVERY = 'download_delivery';
    public const string T_ACQUIT_DELIVERY = 'acquit_delivery';
    public const string T_DELETE_DELIVERY = 'delete_delivery';

    /**
     * @var string Etat initial
     */
    public $initialState = self::S_CREATING;

    /**
     * @var array Changement d'êtats selon action
     */
    public $transitions = [
        self::T_SEND => [
            self::S_CREATING => self::S_VALIDATING,
        ],
        self::T_REFUSE => [
            self::S_VALIDATING => self::S_REJECTED,
        ],
        self::T_ACCEPT => [
            self::S_VALIDATING => self::S_ACCEPTED,
        ],
        self::T_BUILD_DELIVERY => [
            self::S_ACCEPTED => self::S_DELIVERY_AVAILABLE,
        ],
        self::T_DOWNLOAD_DELIVERY => [
            self::S_DELIVERY_AVAILABLE => self::S_DELIVERY_DOWNLOADED,
        ],
        self::T_ACQUIT_DELIVERY => [
            self::S_DELIVERY_DOWNLOADED => self::S_DELIVERY_ACQUITTED,
        ],
        self::T_DELETE_DELIVERY => [
            self::S_DELIVERY_AVAILABLE => self::S_DELIVERY_DELETED,
            self::S_DELIVERY_DOWNLOADED => self::S_DELIVERY_DELETED,
            self::S_DELIVERY_ACQUITTED => self::S_DELIVERY_DELETED,
        ],
    ];

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    self::S_CREATING,
                    self::S_VALIDATING,
                    self::S_DELIVERY_AVAILABLE,
                    self::S_DELIVERY_DOWNLOADED,
                    self::S_DELIVERY_ACQUITTED,
                    self::S_REJECTED,
                    self::S_ACCEPTED,
                    self::S_DELIVERY_DELETED,
                ],
            ]
        );

        $this->belongsTo('ArchivalAgencies')
            ->setClassName('OrgEntities');
        $this->belongsTo('Requesters')
            ->setClassName('OrgEntities');
        $this->belongsTo('CreatedUsers')
            ->setClassName('Users');
        $this->belongsToMany('ArchiveUnits');
        $this->hasMany('ArchiveUnitsDeliveryRequests');
        $this->hasMany('ValidationProcesses')
            ->setForeignKey('app_foreign_key')
            ->setConditions(
                ['ValidationProcesses.app_subject_type' => 'DeliveryRequests']
            );
        $this->hasOne('Deliveries');
        $this->belongsTo('ValidationChains');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->scalar('comment')
            ->allowEmptyString('comment');

        $validator
            ->boolean('derogation')
            ->requirePresence('derogation', 'create')
            ->notEmptyString('derogation');

        $validator
            ->dateTime('last_state_update')
            ->allowEmptyDateTime('last_state_update');

        $validator
            ->scalar('states_history')
            ->allowEmptyString('states_history');

        $validator
            ->scalar('filename')
            ->maxLength('filename', 255)
            ->allowEmptyString('filename');

        $validator
            ->allowEmptyString('validation_chain_id')
            ->add(
                'validation_chain_id',
                'valid',
                [
                    'rule' => function ($value, $context) {
                        $archival_agency_id = Hash::get(
                            $context,
                            'data.archival_agency_id'
                        );
                        if (!$value || empty($archival_agency_id)) {
                            return true;
                        }
                        if (!Hash::get($context, 'data.derogation')) {
                            return false;
                        }
                        /** @var ValidationChainsTable $ValidationChains */
                        $ValidationChains = TableRegistry::getTableLocator()
                            ->get('ValidationChains');
                        $options = $ValidationChains->findListOptions(
                            $archival_agency_id,
                            $ValidationChains::ARCHIVE_DELIVERY_REQUEST
                        );
                        return in_array(
                            $value,
                            array_keys($options->toArray())
                        );
                    },
                ]
            );

        $validator
            ->allowEmptyString('archive_units')
            ->add(
                'archive_units',
                'valid',
                [
                    'rule' => function ($value, $context) {
                        $ids = $value['_ids'] ?? null;
                        $saId = Hash::get($context, 'data.archival_agency_id');
                        if (!$ids || !$saId) {
                            return true;
                        }
                        $ArchiveUnits = TableRegistry::getTableLocator()->get(
                            'ArchiveUnits'
                        );
                        $query = $ArchiveUnits->find()
                            ->select(['qexists' => 1])
                            ->innerJoinWith('Archives')
                            ->where(['Archives.archival_agency_id !=' => $saId])
                            ->disableHydration()
                            ->limit(1);
                        foreach (array_chunk($ids, 50000) as $ids_chunk) {
                            $exists = (bool)(clone $query)
                                ->andWhere(['ArchiveUnits.id IN' => $ids_chunk])
                                ->toArray();
                            if ($exists) {
                                return false;
                            }
                        }
                        return true;
                    },
                ]
            );

        return $validator;
    }

    /**
     * Si l'archive unit ou une des sous archive_unit a un element non communicable
     * il faut une dérogation pour la communication
     * @param array|Query $archive_units_ids
     * @return bool
     */
    public function derogationIsNeeded($archive_units_ids): bool
    {
        if (!$archive_units_ids) {
            return false;
        }
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $now = (new DateTime())->format('Y-m-d');

        return (bool)$ArchiveUnits->find()
            ->select(['existing' => 1])
            ->where(
                [
                    'ArchiveUnits.id IN' => $archive_units_ids,
                    'max_aggregated_access_end_date >' => $now,
                ]
            )
            ->limit(1)
            ->disableHydration()
            ->toArray();
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $query = $this->ArchiveUnitsDeliveryRequests->find();
        $volumetry = $query
            ->select(
                [
                    'archive_units_count' => $query->func()->count('*'),
                    'original_count' => $query->func()->sum(
                        'ArchiveUnits.original_total_count'
                    ),
                    'original_size' => $query->func()->sum(
                        'ArchiveUnits.original_total_size'
                    ),
                ]
            )
            ->innerJoinWith('ArchiveUnits')
            ->where(['delivery_request_id' => $entity->id])
            ->disableHydration()
            ->all()
            ->toArray()
        [0];
        $this->updateAll($volumetry, ['id' => $entity->id]);
    }

    /**
     * Appelé après la transition
     * @param EntityInterface $entity
     * @param string          $state
     * @throws Exception
     */
    public function onStateChange(EntityInterface $entity, string $state)
    {
        if ($state === self::S_ACCEPTED || $state === self::S_REJECTED) {
            $date = new DateTime();
            $statesHistory = json_decode($entity->get('states_history'), true);
            foreach ($statesHistory as $history) {
                if ($history['state'] === self::S_VALIDATING) {
                    $date = new DateTime($history['date']);
                    break;
                }
            }
            $q = $this->query();
            $data = $this->find()
                ->select(
                    [
                        'transferring_agency_id' => 'Archives.transferring_agency_id',
                        'originating_agency_id' => 'Archives.originating_agency_id',
                        'agreement_id' => 'Archives.agreement_id',
                        'profile_id' => 'Archives.profile_id',
                        'original_files_count' => $q->func()->sum('ArchiveUnits.original_total_count'),
                        'original_files_size' => $q->func()->sum('ArchiveUnits.original_total_size'),
                    ]
                )
                ->innerJoinWith('ArchiveUnits')
                ->innerJoinWith('ArchiveUnits.Archives')
                ->where(['DeliveryRequests.id' => $entity->id])
                ->groupBy(
                    [
                        'Archives.transferring_agency_id',
                        'Archives.originating_agency_id',
                        'Archives.agreement_id',
                        'Archives.profile_id',
                    ]
                )
                ->disableHydration()
                ->toArray();

            // récupère la durée en secondes depuis l'envoi
            $int = $date->diff(new DateTime());
            $duration = (new DateTime('@0'))->add($int)->getTimestamp();

            $commons = [
                'message_date' => $date,
                'message_type' => MessageIndicatorsTable::DELIVERY,
                'message_count' => 1,
                'accepted' => $state === self::S_ACCEPTED,
                'derogation' => $entity->get('derogation'),
                'validation_duration' => $duration,
                'archival_agency_id' => $entity->get('archival_agency_id'),
            ];
            $MessageIndicators = TableRegistry::getTableLocator()->get('MessageIndicators');

            foreach ($data as $values) {
                $values = array_map(
                    'intval',
                    array_filter($values, fn($e) => $e !== null)
                    // 0 acceptable pour original_total_count et original_total_size, null concerne les _id
                ); // sum = float
                $indic = $MessageIndicators->newEntity($values + $commons);
                $MessageIndicators->saveOrFail($indic);
            }
        }
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }
}
