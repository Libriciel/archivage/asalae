<?php

/**
 * Asalae\Model\Table\ArchiveKeywordsTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Model\Table\BeforeSaveInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateInterval;
use Exception;

/**
 * Table archive_keywords
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveKeywordsTable extends Table implements
    BeforeSaveInterface,
    AfterSaveInterface,
    AfterDeleteInterface,
    BeforeDeleteInterface,
    SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('ArchiveUnits');
        $this->belongsTo('AccessRules');
        $this->belongsTo('KeywordAccessRules')
            ->setClassName('AccessRules')
            ->setForeignKey('access_rule_id');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('content')
            ->maxLength('content', 255)
            ->requirePresence('content', 'create')
            ->notEmptyString('content')
            ->add(
                'content',
                'unique',
                [
                    'rule' => [
                        'validateUnique',
                        ['scope' => 'archive_unit_id'],
                    ],
                    'provider' => 'table',
                    'message' => __("Ce mot clé est déjà utilisé"),
                ]
            );

        $validator
            ->scalar('reference')
            ->maxLength('reference', 255)
            ->allowEmptyString('reference');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->allowEmptyString('type');

        $validator
            ->scalar('xml_node_tagname')
            ->maxLength('xml_node_tagname', 255)
            ->requirePresence('xml_node_tagname', 'create')
            ->notEmptyString('xml_node_tagname');

        return $validator;
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        if (isset($options['ignoreCallbacks'])) {
            return;
        }
        $create = false;
        $delete = false;
        $update = false;
        $accessRule = $entity->get('access_rule');
        if (!$accessRule) {
            return;
        }
        if ($entity->isNew()) {
            $create = $entity->get('use_parent_access_rule') === false;
        } elseif ($entity->get('use_parent_access_rule')) {
            if ($entity->isDirty('use_parent_access_rule')) {
                $count = Hash::get($entity, 'access_rule.has_many_count', 0);
                $delete = $count === 1;
            }
        } else {
            if ($entity->isDirty('use_parent_access_rule')) {
                $create = true;
            } elseif ($entity->isDirty('access_rule')) {
                $update = true;
            }
        }
        $loc = TableRegistry::getTableLocator();
        $AccessRules = $loc->get('AccessRules');
        $AccessRuleCodes = $loc->get('AccessRuleCodes');
        $access_rule_id = $entity->getOriginal('access_rule_id') ?: 0;
        if ($delete) {
            $entity->set('_delete_access', $access_rule_id);
            if ($entity->get('access_rule_id') === $access_rule_id) {
                $entity->set(
                    'access_rule_id',
                    Hash::get($entity, 'archive_unit.access_rule_id')
                );
            }
        }
        if ($update || $create) {
            /** @var CakeDateTime $startDate */
            $startDate = $accessRule->get('start_date');
            $code_id = $accessRule->get('access_rule_code_id');
            $rule = $AccessRuleCodes->get($code_id);
            $data = [
                'access_rule_code_id' => $code_id,
                'start_date' => clone $startDate,
                'end_date' => $startDate->add(
                    new DateInterval($rule->get('duration'))
                ),
            ];
            if ($create) {
                $accessRule = $AccessRules->newEntity($data);
            } else {
                $accessRule = $entity->get('access_rule');
                $AccessRules->patchEntity($accessRule, $data);
            }
            $AccessRules->saveOrFail($accessRule);
            $entity->setDirty('access_rule', false);
            $entity->set('access_rule_id', $accessRule->get('id'));
        }
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $access_rule_id = $entity->get('_delete_access');
        if ($access_rule_id) {
            $loc = TableRegistry::getTableLocator();
            $AccessRules = $loc->get('AccessRules');
            $AccessRules->deleteAll(['id' => $access_rule_id]);
        }
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $accessRule = Hash::get($entity, 'access_rule');
        if (
            $accessRule instanceof EntityInterface && $accessRule->get(
                'has_many_count'
            ) === 0
        ) {
            $loc = TableRegistry::getTableLocator();
            $AccessRules = $loc->get('AccessRules');
            $AccessRules->delete($accessRule);
        }
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }
}
