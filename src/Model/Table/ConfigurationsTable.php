<?php

/**
 * Asalae\Model\Table\ConfigurationsTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Table\ConfigurationsTable as CoreConfigurationsTable;

/**
 * Table configurations
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConfigurationsTable extends CoreConfigurationsTable
{
    public const string CONF_BACKGROUND_IMAGE = 'background-image';
    public const string CONF_BACKGROUND_POSITION = 'background-position';
    public const string CONF_HEADER_TITLE = 'header-title';
    public const string CONF_MAIL_TITLE_PREFIX = 'mail-title-prefix';
    public const string CONF_MAIL_HTML_SIGNATURE = 'mail-html-signature';
    public const string CONF_MAIL_TEXT_SIGNATURE = 'mail-text-signature';
    public const string CONF_DESTR_REQUEST_CHAIN = 'destruction-request-chain';
    public const string CONF_DR_NO_DEROGATION_CHAIN = 'delivery-request-no-derogation-chain';
    public const string CONF_DR_DEROGATION_CHAIN = 'delivery-request-derogation-chain';
    public const string CONF_DELIVERY_RETENTION_PERIOD = 'delivery-retention-period';
    public const string CONF_RR_CHAIN = 'restitution-request-chain';
    public const string CONF_RR_DESTR_DELAY = 'restitution-request-destruction-delay';
    public const string CONF_OTR_CHAIN = 'outgoing-transfer-request-chain';
    public const string CONF_OTR_DESTR_DELAY = 'outgoing-transfer-request-destruction-delay';
    public const string CONF_CONV_PRESERV_DOC_TEXT = 'conversion-preservation-document_text';
    public const string CONF_CONV_DISSEM_DOC_TEXT = 'conversion-dissemination-document_text';
    public const string CONF_CONV_PRESERV_DOC_PDF = 'conversion-preservation-document_pdf';
    public const string CONF_CONV_DISSEM_DOC_PDF = 'conversion-dissemination-document_pdf';
    public const string CONF_CONV_PRESERV_DOC_CALC = 'conversion-preservation-document_calc';
    public const string CONF_CONV_DISSEM_DOC_CALC = 'conversion-dissemination-document_calc';
    public const string CONF_CONV_PRESERV_DOC_PRES = 'conversion-preservation-document_presentation';
    public const string CONF_CONV_DISSEM_DOC_PRES = 'conversion-dissemination-document_presentation';
    public const string CONF_CONV_PRESERV_IMAGE = 'conversion-preservation-image';
    public const string CONF_CONV_DISSEM_IMAGE = 'conversion-dissemination-image';
    public const string CONF_CONV_PRESERV_AUDIO = 'conversion-preservation-audio';
    public const string CONF_CONV_DISSEM_AUDIO = 'conversion-dissemination-audio';
    public const string CONF_CONV_PRESERV_VIDEO_CONTAINER = 'conversion-preservation-video-container';
    public const string CONF_CONV_PRESERV_VIDEO_CODEC_VIDEO = 'conversion-preservation-video-codec-video';
    public const string CONF_CONV_PRESERV_VIDEO_CODEC_AUDIO = 'conversion-preservation-video-codec-audio';
    public const string CONF_CONV_DISSEM_VIDEO_CONTAINER = 'conversion-dissemination-video-container';
    public const string CONF_CONV_DISSEM_VIDEO_CODEC_VIDEO = 'conversion-dissemination-video-codec-video';
    public const string CONF_CONV_DISSEM_VIDEO_CODEC_AUDIO = 'conversion-dissemination-video-codec-audio';
    public const string CONF_ATTEST_ARCHIVING_CERT = 'attestation-archiving-certificate';
    public const string CONF_ATTEST_ARCHIVING_CERT_COMMITMENT = 'attestation-archiving-certificate-commitment';
    public const string CONF_ATTEST_DESTR_CERT_INTERMEDIATE = 'attestation-destruction-certificate-intermediate';

    public const array KEYS = [
        self::CONF_BACKGROUND_IMAGE,
        self::CONF_BACKGROUND_POSITION,
        self::CONF_HEADER_TITLE,
        self::CONF_MAIL_TITLE_PREFIX,
        self::CONF_MAIL_HTML_SIGNATURE,
        self::CONF_MAIL_TEXT_SIGNATURE,
        self::CONF_DESTR_REQUEST_CHAIN,
        self::CONF_DR_NO_DEROGATION_CHAIN,
        self::CONF_DR_DEROGATION_CHAIN,
        self::CONF_DELIVERY_RETENTION_PERIOD,
        self::CONF_RR_CHAIN,
        self::CONF_RR_DESTR_DELAY,
        self::CONF_OTR_CHAIN,
        self::CONF_OTR_DESTR_DELAY,
        self::CONF_CONV_PRESERV_DOC_TEXT,
        self::CONF_CONV_DISSEM_DOC_TEXT,
        self::CONF_CONV_PRESERV_DOC_PDF,
        self::CONF_CONV_DISSEM_DOC_PDF,
        self::CONF_CONV_PRESERV_DOC_CALC,
        self::CONF_CONV_DISSEM_DOC_CALC,
        self::CONF_CONV_PRESERV_DOC_PRES,
        self::CONF_CONV_DISSEM_DOC_PRES,
        self::CONF_CONV_PRESERV_IMAGE,
        self::CONF_CONV_DISSEM_IMAGE,
        self::CONF_CONV_PRESERV_AUDIO,
        self::CONF_CONV_DISSEM_AUDIO,
        self::CONF_CONV_PRESERV_VIDEO_CONTAINER,
        self::CONF_CONV_PRESERV_VIDEO_CODEC_VIDEO,
        self::CONF_CONV_PRESERV_VIDEO_CODEC_AUDIO,
        self::CONF_CONV_DISSEM_VIDEO_CONTAINER,
        self::CONF_CONV_DISSEM_VIDEO_CODEC_VIDEO,
        self::CONF_CONV_DISSEM_VIDEO_CODEC_AUDIO,
        self::CONF_ATTEST_ARCHIVING_CERT,
        self::CONF_ATTEST_ARCHIVING_CERT_COMMITMENT,
        self::CONF_ATTEST_DESTR_CERT_INTERMEDIATE,
    ];
}
