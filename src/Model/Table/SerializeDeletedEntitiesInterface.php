<?php

/**
 * Asalae\Model\Table\SerializeDeletedEntitiesInterface
 */

namespace Asalae\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Entity;

/**
 * Indique que des entités de la table sont utilisés en objet d'événements
 * (event_logs).
 * Si une entité est supprimée, il faut insérer son serialize dans event_logs
 * dans le champ object_serialized
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface SerializeDeletedEntitiesInterface
{
    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    );

    /**
     * Serialize les entités avant suppression
     * @param mixed $conditions
     * @return int Returns the number of affected rows.
     */
    public function deleteAllSerialize(array $conditions);
}
