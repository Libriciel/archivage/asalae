<?php

/**
 * Asalae\Model\Table\TransferAttachmentsTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use Asalae\Model\Entity\Transfer;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Utility\DOMUtility;
use Cake\Database\Expression\QueryExpression;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DOMElement;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Table transfer_attachments
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @property TransfersTable Transfers
 */
class TransferAttachmentsTable extends Table implements
    BeforeDeleteInterface,
    AfterSaveInterface,
    AfterDeleteInterface
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->belongsTo('Transfers');
        $this->belongsTo('Pronoms')
            ->setForeignKey(false)
            ->setConditions(
                [
                    'Pronoms.puid' => new QueryExpression(
                        $this->getAlias() . '.format'
                    ),
                ]
            );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('filename')
            ->maxLength('filename', 1024)
            ->requirePresence('filename', 'create')
            ->notEmptyString('filename')
            ->add(
                'name',
                [
                    'unique' => [
                        'rule' => 'validateUnique',
                        'provider' => 'table',
                        'message' => __("Ce nom de fichier est déjà utilisé"),
                    ],
                ]
            );

        $validator
            ->requirePresence('size', 'create')
            ->notEmptyString('size');

        $validator
            ->scalar('hash')
            ->maxLength('hash', 255)
            ->allowEmptyString('hash');

        $validator
            ->scalar('hash_algo')
            ->maxLength('hash_algo', 255)
            ->allowEmptyString('hash_algo');

        $validator
            ->scalar('virus_name')
            ->maxLength('virus_name', 255)
            ->allowEmptyString('virus_name');

        $validator
            ->boolean('valid')
            ->allowEmptyString('valid');

        $validator
            ->scalar('mime')
            ->maxLength('mime', 255)
            ->allowEmptyString('mime');

        $validator
            ->scalar('extension')
            ->maxLength('extension', 255)
            ->allowEmptyString('extension');

        $validator
            ->scalar('format')
            ->maxLength('format', 255)
            ->allowEmptyString('format');

        $validator
            ->scalar('xpath')
            ->allowEmptyString('xpath');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['transfer_id'], 'Transfers'));
        $rules->addDelete(
            function (Entity $entity) {
                if ($entity->get('force_delete') === true) {
                    return true;
                }
                /** @var Transfer $transfer */
                $transfer = $this->Transfers->get($entity->get('transfer_id'));
                return !in_array(
                    $entity->get('filename'),
                    $transfer->listFiles()
                );
            }
        );

        return $rules;
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        if (is_file($entity->get('path'))) {
            Filesystem::remove($entity->get('path'));
        }
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     * Transfers.data_size update
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        if ($entity->isNew()) {
            $this->Transfers->calcCountSize(['id' => $entity->get('transfer_id')]);

            if (!$entity->get('mime') && is_file($entity->get('path'))) {
                $this->updateAll(
                    ['mime' => mime_content_type($entity->get('path'))],
                    ['id' => $entity->id]
                );
            }
            if (!$entity->get('extension')) {
                $this->updateAll(
                    [
                        'extension' => pathinfo(
                            $entity->get('filename'),
                            PATHINFO_EXTENSION
                        ),
                    ],
                    ['id' => $entity->id]
                );
            }
        }
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     * Transfers.data_size update except if state === accepted (retention)
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        /** @var Transfer $transfer */
        $transfer = $this->Transfers
            ->find()
            ->where(['Transfers.id' => $entity->get('transfer_id')])
            ->firstOrFail();

        if ($transfer->get('state') !== TransfersTable::S_ACCEPTED) {
            $this->Transfers->saveOrFail($transfer);
            $this->Transfers->calcCountSize(['id' => $entity->get('transfer_id')]);
        }
    }

    /**
     * Calcule le xpath d'un attachment
     * @param Query      $query
     * @param DOMUtility $domutil
     * @return void
     */
    public function updateMissingXpaths(Query $query, DOMUtility $domutil)
    {
        $filenames = self::listFilenames($domutil);
        foreach ($query as $attachment) {
            $filename = Hash::get($attachment, 'filename');
            if (isset($filenames[$filename])) {
                $this->updateAll(
                    ['xpath' => $domutil->getElementXpath($filenames[$filename], '')],
                    ['id' => Hash::get($attachment, 'id')]
                );
            }
        }
    }

    /**
     * Donne la liste des fichiers (binaries)
     * @param DOMUtility $domutil
     * @return array
     */
    public static function listFilenames(DOMUtility $domutil): array
    {
        $filenames = [];
        if (in_array($domutil->namespace, [NAMESPACE_SEDA_02, NAMESPACE_SEDA_10])) {
            $q = $domutil->xpath->query('//ns:Document');
            /** @var DOMElement $element */
            foreach ($q as $document) {
                /** @var DOMElement $attachment */
                $attachment = $domutil->node('ns:Attachment', $document);
                $filenames[$attachment->getAttribute('filename')] = $document;
            }
        } elseif (in_array($domutil->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            $q = $domutil->xpath->query('//ns:BinaryDataObject');
            /** @var DOMElement $binary */
            foreach ($q as $binary) {
                /** @var DOMElement $attachment */
                $filename = $domutil->node('ns:FileInfo/ns:Filename', $binary);
                if ($filename) {
                    $filenames[trim($filename->nodeValue)] = $binary;
                    continue;
                }
                /** @var DOMElement $attachment */
                $attachment = $domutil->node('ns:Attachment', $binary);
                if ($attachment) {
                    $filenames[$attachment->getAttribute('filename')] = $binary;
                    continue;
                }
                /** @var DOMElement $uri */
                $uri = $domutil->node('ns:Uri', $binary);
                if ($uri) {
                    $filenames[trim($uri->nodeValue)] = $binary;
                }
            }
        }
        return $filenames;
    }
}
