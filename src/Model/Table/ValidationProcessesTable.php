<?php

/**
 * Asalae\Model\Table\ValidationProcessesTable
 */

namespace Asalae\Model\Table;

use Asalae\Model\Entity\ValidationActor;
use Asalae\Model\Entity\ValidationHistory;
use Asalae\Model\Entity\ValidationProcess;
use Asalae\Model\Entity\ValidationStage;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use InvalidArgumentException;

/**
 * Table validation_processes
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin OptionsBehavior
 */
class ValidationProcessesTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'processed' => [
                    false,
                    true,
                ],
                'validated' => [
                    null,
                    false,
                    true,
                ],
            ]
        );

        $this->belongsTo('ValidationChains');
        $this->belongsTo('CurrentStages', ['className' => 'ValidationStages']);
        $this->hasMany('ValidationHistories');
        $this->belongsTo('Transfers')
            ->setForeignKey('app_foreign_key')
            ->setConditions(
                ['ValidationProcesses.app_subject_type' => 'Transfers']
            );
        $this->belongsTo('DeliveryRequests')
            ->setForeignKey('app_foreign_key')
            ->setConditions(
                ['ValidationProcesses.app_subject_type' => 'DeliveryRequests']
            );
        $this->belongsTo('DestructionRequests')
            ->setForeignKey('app_foreign_key')
            ->setConditions(
                ['ValidationProcesses.app_subject_type' => 'DestructionRequests']
            );
        $this->belongsTo('RestitutionRequests')
            ->setForeignKey('app_foreign_key')
            ->setConditions(
                ['ValidationProcesses.app_subject_type' => 'RestitutionRequests']
            );
        $this->belongsTo('OutgoingTransferRequests')
            ->setForeignKey('app_foreign_key')
            ->setConditions(
                ['ValidationProcesses.app_subject_type' => 'OutgoingTransferRequests']
            );
        // NOTE: anormalement nécessaire depuis cakephp 3.6.8
        $this->setEntityClass('Asalae\Model\Entity\ValidationProcess');

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('app_subject_type')
            ->maxLength('app_subject_type', 255)
            ->allowEmptyString('app_subject_type');

        $validator
            ->scalar('app_subject_key')
            ->maxLength('app_subject_key', 255)
            ->allowEmptyString('app_subject_key');

        $validator
            ->boolean('processed')
            ->requirePresence('processed', 'create')
            ->notEmptyString('processed');

        $validator
            ->boolean('validated')
            ->allowEmptyString('validated');

        $validator
            ->scalar('app_meta')
            ->allowEmptyString('app_meta');

        $validator
            ->integer('app_foreign_key')
            ->requirePresence('app_foreign_key', 'create')
            ->notEmptyString('app_foreign_key');

        $validator
            ->integer('current_step')
            ->requirePresence('current_step', 'create')
            ->notEmptyString('current_step');

        return $validator;
    }

    /**
     * Effectue la validation d'un process
     * @param ValidationHistory $history
     * @return ValidationProcess
     * @throws Exception
     */
    public function validate(ValidationHistory $history): ValidationProcess
    {
        $history->set('action', 'validate');
        return $this->proceed('validate', $history);
    }

    /**
     * Considère l'entité comme traité avec ajout d'entrées dans l'historique
     * @param string            $action
     * @param ValidationHistory $history
     * @return ValidationProcess
     * @throws Exception
     */
    public function proceed(string $action, ValidationHistory $history): ValidationProcess
    {
        if (!$history->isNew()) {
            throw new Exception(
                __("Seul un nouvel historique peut servir à intervenir sur un process")
            );
        }
        $nextStage = false;
        /** @var ValidationProcess $process */
        $process = $this->find()
            ->where(
                [
                    'ValidationProcesses.id' => $history->get('validation_process_id'),
                ]
            )
            ->contain(
                [
                    'CurrentStages' => ['ValidationActors'],
                    'ValidationChains' => ['ValidationStages' => ['sort' => 'ord']],
                ]
            )
            ->firstOrFail();
        $actor_id = $history->get('validation_actor_id');
        $currentStage = $process->get('current_stage');
        $actors = $currentStage->get('validation_actors');
        $actorIds = array_map(
            function (ValidationActor $v) use (&$actor, $actor_id) {
                if ($v->get('id') === $actor_id) {
                    $actor = $v;
                }
                return $v->get('id');
            },
            $actors
        );
        if (empty($actor)) {
            throw new InvalidArgumentException(
                __("L'acteur ne semble pas faire parti des acteurs possible")
            );
        }
        if ($action === 'validate' && $currentStage->get('all_actors_to_complete')) {
            $countDone = TableRegistry::getTableLocator()->get('ValidationHistories')->find()
                ->where(
                    [
                        'validation_process_id' => $history->get('validation_process_id'),
                        'validation_actor_id IN' => $actorIds,
                        'action IN' => ['validate', 'invalidate'],
                        'current_step' => $process->get('current_step'),
                    ]
                )
                ->count();
            if ($countDone + 1 >= count($actors)) {
                $nextStage = true;
            }
        } elseif ($action === 'validate') {
            $nextStage = true;
        }

        $ord = 0;
        if ($nextStage) {
            $finalStage = true;
            $history->set('current_step', $process->get('current_step') + 1);
            $process->set('current_step', $process->get('current_step') + 1);
            // détecte le stage actuel et affecte le stage d'après
            /** @var ValidationStage $stage */
            foreach ($process->get('validation_chain')->get('validation_stages') as $stage) {
                if ($stage->get('id') == $process->get('current_stage_id')) {
                    $ord = (int)$stage->get('ord');
                } elseif ($ord && (int)$stage->get('ord') > $ord) {
                    $process->set('current_stage_id', $stage->get('id'));
                    $finalStage = false;
                    break;
                }
            }
            if ($finalStage) {
                $process->set('current_stage_id')
                    ->set('processed', true)
                    ->set('validated', true);
            }
        } elseif ($action === 'invalidate') {
            $history->set('current_step', $process->get('current_step') + 1);
            $process->set('current_step', $process->get('current_step') + 1);
            $process->set('current_stage_id')
                ->set('processed', true)
                ->set('validated', false);
        } elseif ($action === 'stepback') {
            $history->set('current_step', $process->get('current_step') + 1);
            $process->set('current_step', $process->get('current_step') + 1);
            $this->setCurrentStageId($process);
        } else {
            $history->set('current_step', $process->get('current_step'));
        }

        return $process;
    }

    /**
     * Permet d'obtenir la valeur du nouveau current_stage_id
     * @param ValidationProcess $process
     */
    private function setCurrentStageId(ValidationProcess $process)
    {
        if (is_numeric($stepback = $process->get('stepback'))) {
            $process->set('current_stage_id', $stepback);
            return;
        }

        $ord = 0;
        $stages = $process->get('validation_chain')->get('validation_stages');
        // détecte le stage actuel et affecte le stage d'avant
        for ($i = count($stages) - 1; $i >= 0; $i--) {
            /** @var ValidationStage $stage */
            $stage = $stages[$i];
            if ($stage->get('id') == $process->get('current_stage_id')) {
                $ord = (int)$stage->get('ord');
            } elseif ($ord && (int)$stage->get('ord') < $ord) {
                $process->set('current_stage_id', $stage->get('id'));
                break;
            }
        }
    }

    /**
     * invalide un process
     * @param ValidationHistory $history
     * @return ValidationProcess
     * @throws Exception
     */
    public function invalidate(ValidationHistory $history): ValidationProcess
    {
        $history->set('action', 'invalidate');
        return $this->proceed('invalidate', $history);
    }

    /**
     * Retour à l'étape précédente
     * @param ValidationHistory $history
     * @return ValidationProcess
     * @throws Exception
     */
    public function stepback(ValidationHistory $history): ValidationProcess
    {
        $history->set('action', 'stepback');
        return $this->proceed('stepback', $history);
    }

    /**
     * Initialise un nouveau process de validation
     * @param EntityInterface $entity
     * @param array           $params
     * @return ValidationProcess|EntityInterface
     * @throws Exception
     */
    public function createNewProcess(
        EntityInterface $entity,
        array $params
    ): EntityInterface {
        $params += [
            'org_entity_id' => null,
            'validation_chain_id' => null,
            'user_id' => null,
            'valid' => null,
            'agreement' => null,
        ];
        if (!$params['org_entity_id']) {
            throw new Exception('org_entity_id is required');
        }
        $process = $this->newEntity(
            [
                'app_subject_type' => $entity->getSource(),
                'app_foreign_key' => $entity->get('id'),
                'processed' => false,
                'current_step' => 1,
            ]
        );
        $ValidationChains = TableRegistry::getTableLocator()->get('ValidationChains');
        $query = $ValidationChains->find();
        if (!$params['validation_chain_id'] && $entity->getSource() === 'Transfers') {
            if (!$params['agreement']) {
                $params['agreement'] = TableRegistry::getTableLocator()->get('Agreements')
                    ->find()
                    ->where(
                        [
                            'org_entity_id' => $params['org_entity_id'],
                            'default_agreement' => true,
                        ]
                    )
                    ->first();
            }
            if ($params['valid'] && !$params['agreement']) {
                $query->where(
                    [
                        'ValidationChains.org_entity_id' => $params['org_entity_id'],
                        'ValidationChains.app_type' => ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM,
                        ['ValidationChains.app_meta LIKE' => '%"default":true%'],
                        ['ValidationChains.app_meta LIKE' => '%"active":true%'],
                    ]
                );
            } elseif ($params['valid']) {
                $query->where(
                    [
                        'ValidationChains.id' => $params['agreement']->get('proper_chain_id'),
                    ]
                );
            } elseif (!$params['agreement']) {
                $query->where(
                    [
                        'ValidationChains.org_entity_id' => $params['org_entity_id'],
                        'ValidationChains.app_type' => ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM,
                        ['ValidationChains.app_meta LIKE' => '%"default":true%'],
                        ['ValidationChains.app_meta LIKE' => '%"active":true%'],
                    ]
                );
            } else {
                $query->where(
                    [
                        'ValidationChains.id' => $params['agreement']->get('improper_chain_id'),
                    ]
                );
            }
        } else {
            $query->where(
                ['ValidationChains.id' => $params['validation_chain_id']]
            );
        }

        $chain = $query
            ->contain(['FirstStages' => ['ValidationActors']])
            ->firstOrFail();
        /** @var ValidationStage $stage */
        $stage = $chain->get('first_stage');
        if (!$stage) {
            throw new NotFoundException(
                sprintf(
                    "there is no stages in ValidationChains %d",
                    $chain->get('id')
                )
            );
        }
        $data = [
            'validation_chain_id' => $chain->get('id'),
            'current_stage_id' => $stage->get('id'),
            'created_user_id' => $params['user_id'],
        ];
        $this->patchEntity($process, $data + $process->toArray());
        return $process;
    }

    /**
     * Averti l'acteur de validation du process
     * @param EntityInterface $process
     * @throws Exception
     */
    public function notifyProcess(EntityInterface $process)
    {
        $ValidationActors = TableRegistry::getTableLocator()->get(
            'ValidationActors'
        );
        $query = $ValidationActors->find()
            ->where(
                ['validation_stage_id' => $process->get('current_stage_id')]
            );
        /** @var validationActor $actor */
        foreach ($query as $actor) {
            $actor->notify($process);
        }
    }

    /**
     * Détecte l'acteur selon le contexte de validation
     * @param int                   $saId
     * @param EntityInterface|array $user
     * @param string                $type
     * @param bool                  $distinct
     * @param null|int              $actorId
     * @return Query
     */
    public function findByValidations(
        $saId,
        $user,
        string $type,
        bool $distinct = false,
        $actorId = null
    ): Query {
        if (!$this->$type instanceof BelongsTo) {
            throw new BadRequestException();
        }
        $userId = Hash::get($user, 'id');
        $orgEntityId = Hash::get($user, 'org_entity_id');
        $typeEntity = Hash::get($user, 'org_entity.type_entity.code');
        $isValidator = Hash::get($user, 'is_validator');
        /** @var BelongsTo $link */
        $link = $this->getAssociation($type);

        if (!empty($actorId)) {
            $conditions = ['ValidationActors.id' => $actorId];
        } else {
            // Gestion pour le super archiviste
            if (in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)) {
                $orgEntityId = $saId;
            }
            $conditions = [
                'OR' => [
                    [
                        'ValidationActors.app_foreign_key' => $userId,
                        'ValidationActors.app_type' => 'USER',
                        $type . '.archival_agency_id' => $saId,
                    ],
                ],
            ];

            if ($isValidator && $orgEntityId === $saId && $link->hasField('archival_agency_id')) {
                $conditions['OR'][] = [
                    $type . '.archival_agency_id' => $saId,
                    'ValidationActors.app_type' => 'SERVICE_ARCHIVES',
                ];
            }
            if ($isValidator && $link->hasField('requester_id')) {
                $conditions['OR'][] = [
                    $type . '.requester_id' => $orgEntityId,
                    'ValidationActors.app_type' => 'SERVICE_DEMANDEUR',
                ];
            }
            if ($isValidator && $link->hasField('originating_agency_id')) {
                $conditions['OR'][] = [
                    $type . '.originating_agency_id' => $orgEntityId,
                    'ValidationActors.app_type' => 'SERVICE_PRODUCTEUR',
                ];
            }
        }

        $ValidationActors = TableRegistry::getTableLocator()->get('ValidationActors');
        $subquery = $ValidationActors->find();
        $subquery
            ->select(['ValidationActors.id'])
            ->where(
                [
                    'ValidationActors.validation_stage_id'
                    => $subquery->identifier('CurrentStages.id'),
                ]
            )
            ->where($conditions);
        if ($distinct) {
            $subquery->limit(1);
        }

        return $this->find()
            ->select(
                [
                    'test' => 'ValidationActors.app_type',
                    'validation_user_id' => $userId ?: '0',
                    'validation_org_entity_id' => $orgEntityId ?: '0',
                    'validation_actor_id' => new IdentifierExpression('ValidationActors.id'),
                ]
            )
            ->innerJoinWith('CurrentStages')
            ->innerJoinWith(
                'CurrentStages.ValidationActors',
                function (Query $q) use ($subquery) {
                    return $q->andWhere(['ValidationActors.id IN' => $subquery]);
                }
            )
            ->innerJoinWith('ValidationChains')
            ->leftJoinWith(
                'CurrentStages.ValidationActors.ValidationHistories',
                function (Query $q) use ($distinct) {
                    $q->where(
                        [
                            'ValidationHistories.validation_process_id'
                            => new IdentifierExpression('ValidationProcesses.id'),
                            'ValidationHistories.current_step'
                            => new IdentifierExpression('ValidationProcesses.current_step'),
                        ]
                    )
                        ->orderBy(['ValidationHistories.id' => 'desc']);
                    if ($distinct) {
                        $q->limit(1);
                    }
                    return $q;
                }
            )
            ->innerJoin(
                [$type => $link->getTable()],
                [
                    $type . '.' . $link->getPrimaryKey()
                    => new IdentifierExpression('ValidationProcesses.app_foreign_key'),
                ]
            )
            ->where(
                [
                    'ValidationProcesses.app_subject_type' => $type,
                    'ValidationHistories.id IS' => null,
                    'ValidationActors.id IS NOT' => null,
                    // nécessaire malgré le inner join
                ]
            )
            ->contain(
                [
                    'CurrentStages' => ['ValidationActors'],
                    'ValidationChains',
                ]
            )
            ->enableAutoFields();
    }
}
