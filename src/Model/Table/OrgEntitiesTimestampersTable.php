<?php

/**
 * Asalae\Model\Table\OrgEntitiesTimestampersTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;

/**
 * Table org_entities_timestampers
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class OrgEntitiesTimestampersTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsTo('OrgEntities');
        $this->belongsTo('Timestampers');
        $this->hasMany('ServiceLevelMsgs', ['className' => 'ServiceLevels'])
            ->setForeignKey('ts_msg_id');
        $this->hasMany('ServiceLevelPjss', ['className' => 'ServiceLevels'])
            ->setForeignKey('ts_pjs_id');
        $this->hasMany('ServiceLevelConvs', ['className' => 'ServiceLevels'])
            ->setForeignKey('ts_conv_id');

        parent::initialize($config);
    }
}
