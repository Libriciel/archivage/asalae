<?php

/**
 * Asalae\Model\Table\MediainfosTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table mediainfos
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MediainfosTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Fileuploads');
        $this->hasMany('MediainfoAudios');
        $this->hasMany('MediainfoImages');
        $this->hasMany('MediainfoTexts');
        $this->hasMany('MediainfoVideos');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('complete_name')
            ->maxLength('complete_name', 512)
            ->allowEmptyString('complete_name');

        $validator
            ->scalar('format')
            ->maxLength('format', 512)
            ->allowEmptyString('format');

        $validator
            ->scalar('format_version')
            ->maxLength('format_version', 512)
            ->allowEmptyString('format_version');

        $validator
            ->scalar('file_size')
            ->maxLength('file_size', 512)
            ->allowEmptyFile('file_size');

        $validator
            ->scalar('duration')
            ->maxLength('duration', 512)
            ->allowEmptyString('duration');

        $validator
            ->scalar('overall_bit_rate')
            ->maxLength('overall_bit_rate', 512)
            ->allowEmptyString('overall_bit_rate');

        $validator
            ->scalar('movie_name')
            ->maxLength('movie_name', 512)
            ->allowEmptyString('movie_name');

        $validator
            ->scalar('encoded_date')
            ->maxLength('encoded_date', 512)
            ->allowEmptyString('encoded_date');

        $validator
            ->scalar('writing_application')
            ->maxLength('writing_application', 512)
            ->allowEmptyString('writing_application');

        $validator
            ->scalar('writing_library')
            ->maxLength('writing_library', 512)
            ->allowEmptyString('writing_library');

        return $validator;
    }
}
