<?php

/**
 * Asalae\Model\Table\SequencesTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Table\SequencesTable as CoreSequencesTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table sequences
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class SequencesTable extends CoreSequencesTable
{
}
