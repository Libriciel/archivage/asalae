<?php

/**
 * Asalae\Model\Table\RolesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\AclBehavior;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Model\Table\RolesTable as CoreRolesTable;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Behavior\TreeBehavior;
use Cake\ORM\Entity;

/**
 * Table roles
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AclBehavior
 * @mixin TimestampBehavior
 * @mixin TreeBehavior
 */
class RolesTable extends CoreRolesTable implements
    BeforeDeleteInterface,
    SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    public const string CODE_ADMIN = 'ADMIN';
    public const string CODE_APPLICANT = 'AD';
    public const string CODE_ARCHIVES_REFERENT = 'ARA';
    public const string CODE_ARCHIVING_OPERATOR = 'AOA';
    public const string CODE_ARCHIVIST = 'AA';
    public const string CODE_CONTROLLER = 'AC';
    public const string CODE_PRODUCER = 'AP';
    public const string CODE_SUBMITTER = 'AV';
    public const string CODE_SUPER_ARCHIVIST = 'SAA';
    public const string CODE_WS_ADMINISTRATOR = 'WSA';
    public const string CODE_WS_REQUESTER = 'WSR';
    public const string CODE_WS_SUBMITTER = 'WSV';

    public const array SPECIAL_ROLE_CODES = [
        self::CODE_SUPER_ARCHIVIST,
        self::CODE_ADMIN,
    ];

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
        parent::beforeDelete($event, $entity, $options);
    }
}
