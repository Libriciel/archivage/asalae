<?php

/**
 * Asalae\Model\Table\ArchiveUnitsBatchTreatmentsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;

/**
 * Table archive_units_batch_treatments
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveUnitsBatchTreatmentsTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('ArchiveUnits');
        $this->belongsTo('BatchTreatments');
    }
}
