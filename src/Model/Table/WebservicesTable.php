<?php

/**
 * Asalae\Model\Table\WebservicesTable
 * @noinspection PhpDeprecationInspection - WebservicesTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Table\WebservicesTable as CoreWebservicesTable;

/**
 * Table webservices
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @deprecated since 2.0.0a5
 */
class WebservicesTable extends CoreWebservicesTable
{
}
