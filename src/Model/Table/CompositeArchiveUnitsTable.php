<?php

/**
 * Asalae\Model\Table\CompositeArchiveUnitsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;

/**
 * Table composite_archive_units
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CompositeArchiveUnitsTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Transfers');
        $this->belongsTo('ArchiveUnits');
    }
}
