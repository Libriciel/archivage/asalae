<?php

/**
 * Asalae\Model\Table\ArchiveFilesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table archive_files
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveFilesTable extends Table implements SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    public const string TYPE_DESCRIPTION = 'description';
    public const string TYPE_LIFECYCLE = 'lifecycle';
    public const string TYPE_TRANSFER = 'transfer';
    public const string TYPE_DELETED = 'deleted';
    public const string TYPE_ARCHIVING_CERTIFICATE = 'archiving_certificate';
    public const string TYPE_PUBLIC_DESCRIPTION = 'pubdesc';

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->belongsTo('Archives');
        $this->belongsTo('StoredFiles');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('is_integrity_ok')
            ->allowEmptyString('is_integrity_ok');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('filename')
            ->maxLength('filename', 255)
            ->requirePresence('filename', 'create')
            ->notEmptyFile('filename');

        return $validator;
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }
}
