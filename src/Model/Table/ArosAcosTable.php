<?php

/**
 * Asalae\Model\Table\ArosAcosTable
 */

namespace Asalae\Model\Table;

use Acl\Model\Table\PermissionsTable;
use Cake\ORM\Table;

/**
 * Table aros_acos
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property Table Aro
 * @property Table Aco
 */
class ArosAcosTable extends PermissionsTable
{
    /**
     * Initialize a table instance. Called after the constructor.
     *
     * You can use this method to define associations, attach behaviors
     * define validation and do any other initialization logic you need.
     *
     * ```
     *  public function initialize(array $config)
     *  {
     *      $this->belongsTo('Users');
     *      $this->belongsToMany('Tagging.Tags');
     *      $this->setPrimaryKey('something_else');
     *  }
     * ```
     *
     * @param array<string, mixed> $config Configuration options passed to the constructor
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->setTable('aros_acos');
        $this->belongsTo('Aros');
        $this->belongsTo('Acos');
        // ne pas appeler le parent::initialize() !
    }
}
