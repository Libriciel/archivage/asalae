<?php

/**
 * Asalae\Model\Table\SavedFiltersTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Table\SavedFiltersTable as CoreSavedFiltersTable;

/**
 * Table saved_filters
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SavedFiltersTable extends CoreSavedFiltersTable
{
}
