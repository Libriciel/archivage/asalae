<?php

/**
 * Asalae\Model\Table\RestitutionRequestsTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Model\Table\StateChangeInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use DateTime;
use Exception;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table restitution_requests
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @mixin StateMachineBehavior
 * @mixin TimestampBehavior
 */
class RestitutionRequestsTable extends Table implements
    BeforeDeleteInterface,
    SerializeDeletedEntitiesInterface,
    StateChangeInterface
{
    use SerializeDeleteAllTrait;

    public const string S_CREATING = 'creating';
    public const string S_VALIDATING = 'validating';
    public const string S_REJECTED = 'rejected';
    public const string S_ACCEPTED = 'accepted';
    public const string S_RESTITUTION_AVAILABLE = 'restitution_available';
    public const string S_RESTITUTION_DOWNLOADED = 'restitution_downloaded';
    public const string S_RESTITUTION_ACQUITTED = 'restitution_acquitted';
    public const string S_ARCHIVE_DESTRUCTION_SCHEDULED = 'archive_destruction_scheduled';
    public const string S_ARCHIVE_FILES_DESTROYING = 'archive_files_destroying';
    public const string S_ARCHIVE_DESCRIPTION_DELETING = 'archive_description_deleting';
    public const string S_RESTITUTED = 'restituted';

    public const string T_SEND = 'send';
    public const string T_REFUSE = 'refuse';
    public const string T_ACCEPT = 'accept';
    public const string T_BUILD_RESTITUTION = 'build_restitution';
    public const string T_DOWNLOAD_RESTITUTION = 'download_restitution';
    public const string T_ACQUIT_RESTITUTION = 'acquit_restitution';
    public const string T_DESTROY_ARCHIVE_FILES = 'destroy_archive_files';
    public const string T_DELETE_ARCHIVE_DESCRIPTION = 'delete_archive_description';
    public const string T_DELETE_DESCRIPTION = 'delete_description';
    public const string T_SCHEDULE_ARCHIVE_DESTRUCTION = 'schedule_archive_destruction';
    public const string T_CANCEL_ARCHIVE_DESTRUCTION = 'cancel_archive_destruction';

    /**
     * @var string Etat initial
     */
    public $initialState = self::S_CREATING;

    /**
     * @var array Changement d'êtats selon action
     */
    public $transitions = [
        self::T_SEND => [
            self::S_CREATING => self::S_VALIDATING,
        ],
        self::T_REFUSE => [
            self::S_VALIDATING => self::S_REJECTED,
        ],
        self::T_ACCEPT => [
            self::S_VALIDATING => self::S_ACCEPTED,
        ],
        self::T_BUILD_RESTITUTION => [
            self::S_ACCEPTED => self::S_RESTITUTION_AVAILABLE,
        ],
        self::T_DOWNLOAD_RESTITUTION => [
            self::S_RESTITUTION_AVAILABLE => self::S_RESTITUTION_DOWNLOADED,
        ],
        self::T_ACQUIT_RESTITUTION => [
            self::S_RESTITUTION_DOWNLOADED => self::S_RESTITUTION_ACQUITTED,
        ],
        self::T_DESTROY_ARCHIVE_FILES => [
            self::S_ARCHIVE_DESTRUCTION_SCHEDULED => self::S_ARCHIVE_FILES_DESTROYING,
        ],
        self::T_DELETE_ARCHIVE_DESCRIPTION => [
            self::S_ARCHIVE_FILES_DESTROYING => self::S_ARCHIVE_DESCRIPTION_DELETING,
        ],
        self::T_DELETE_DESCRIPTION => [
            self::S_ARCHIVE_DESCRIPTION_DELETING => self::S_RESTITUTED,
        ],
        self::T_SCHEDULE_ARCHIVE_DESTRUCTION => [
            self::S_RESTITUTION_ACQUITTED => self::S_ARCHIVE_DESTRUCTION_SCHEDULED,
        ],
        self::T_CANCEL_ARCHIVE_DESTRUCTION => [
            self::S_ARCHIVE_DESTRUCTION_SCHEDULED => self::S_RESTITUTION_ACQUITTED,
        ],
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    self::S_CREATING,
                    self::S_VALIDATING,
                    self::S_REJECTED,
                    self::S_ACCEPTED,
                    self::S_RESTITUTION_AVAILABLE,
                    self::S_RESTITUTION_DOWNLOADED,
                    self::S_RESTITUTION_ACQUITTED,
                    self::S_ARCHIVE_DESTRUCTION_SCHEDULED,
                    self::S_ARCHIVE_FILES_DESTROYING,
                    self::S_ARCHIVE_DESCRIPTION_DELETING,
                    self::S_RESTITUTED,
                ],
            ]
        );

        $this->belongsTo('ArchivalAgencies')
            ->setClassName('OrgEntities')
            ->setJoinType('INNER');
        $this->belongsTo('OriginatingAgencies')
            ->setClassName('OrgEntities')
            ->setJoinType('INNER');
        $this->belongsTo('CreatedUsers')
            ->setClassName('Users')
            ->setJoinType('INNER');
        $this->belongsToMany('ArchiveUnits');
        $this->belongsToMany('Archives');
        $this->hasMany('ArchiveUnitsRestitutionRequests');
        $this->hasMany('ValidationProcesses')
            ->setForeignKey('app_foreign_key')
            ->setConditions(
                ['ValidationProcesses.app_subject_type' => 'RestitutionRequests']
            );
        $this->hasOne('Restitutions');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier');

        $validator
            ->scalar('comment')
            ->requirePresence('comment', 'create')
            ->notEmptyString('comment');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->dateTime('last_state_update')
            ->allowEmptyDateTime('last_state_update');

        $validator
            ->scalar('states_history')
            ->allowEmptyString('states_history');

        $validator
            ->integer('archive_units_count')
            ->requirePresence('archive_units_count', 'create')
            ->notEmptyString('archive_units_count');

        $validator
            ->integer('original_count')
            ->requirePresence('original_count', 'create')
            ->notEmptyString('original_count');

        $validator
            ->requirePresence('original_size', 'create')
            ->notEmptyString('original_size');

        return $validator;
    }

    /**
     * Appelé après la transition
     * @param EntityInterface $entity
     * @param string          $state
     * @throws Exception
     */
    public function onStateChange(EntityInterface $entity, string $state)
    {
        if ($state === self::S_ACCEPTED || $state === self::S_REJECTED) {
            $date = new DateTime();
            $statesHistory = json_decode($entity->get('states_history'), true);
            foreach ($statesHistory as $history) {
                if ($history['state'] === self::S_VALIDATING) {
                    $date = new DateTime($history['date']);
                    break;
                }
            }
            $q = $this->query();
            $data = $this->find()
                ->select(
                    [
                        'transferring_agency_id' => 'Archives.transferring_agency_id',
                        'originating_agency_id' => 'Archives.originating_agency_id',
                        'agreement_id' => 'Archives.agreement_id',
                        'profile_id' => 'Archives.profile_id',
                        'original_files_count' => $q->func()->sum('ArchiveUnits.original_total_count'),
                        'original_files_size' => $q->func()->sum('ArchiveUnits.original_total_size'),
                    ]
                )
                ->innerJoinWith('ArchiveUnits')
                ->innerJoinWith('ArchiveUnits.Archives')
                ->where(['RestitutionRequests.id' => $entity->id])
                ->groupBy(
                    [
                        'Archives.transferring_agency_id',
                        'Archives.originating_agency_id',
                        'Archives.agreement_id',
                        'Archives.profile_id',
                    ]
                )
                ->disableHydration()
                ->toArray();

            // récupère la durée en secondes depuis l'envoi
            $int = $date->diff(new DateTime());
            $duration = (new DateTime('@0'))->add($int)->getTimestamp();

            $commons = [
                'message_date' => $date,
                'message_type' => MessageIndicatorsTable::RESTITUTION,
                'message_count' => 1,
                'accepted' => $state === self::S_ACCEPTED,
                'derogation' => false,
                'validation_duration' => $duration,
                'archival_agency_id' => $entity->get('archival_agency_id'),
            ];
            $MessageIndicators = TableRegistry::getTableLocator()->get('MessageIndicators');

            foreach ($data as $values) {
                $values = array_map(
                    'intval',
                    array_filter($values)
                ); // sum = float
                $indic = $MessageIndicators->newEntity($values + $commons);
                $MessageIndicators->saveOrFail($indic);
            }
        }
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }
}
