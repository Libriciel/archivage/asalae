<?php

/**
 * Asalae\Model\Table\ArchiveBinaryConversionsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table archive_binary_conversions
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class ArchiveBinaryConversionsTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->belongsTo('Archives');
        $this->belongsTo('ArchiveBinaries');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('format')
            ->maxLength('format', 255)
            ->requirePresence('format', 'create')
            ->notEmptyString('format');

        $validator
            ->scalar('audio_codec')
            ->maxLength('audio_codec', 255)
            ->allowEmptyString('audio_codec');

        $validator
            ->scalar('video_codec')
            ->maxLength('video_codec', 255)
            ->allowEmptyString('video_codec');

        return $validator;
    }
}
