<?php

/**
 * Asalae\Model\Table\DestructionNotificationXpathsTable
 */

namespace Asalae\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table destruction_notification_xpaths
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DestructionNotificationXpathsTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('DestructionNotifications');
        $this->belongsTo('Archives');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('archive_unit_identifier')
            ->maxLength('archive_unit_identifier', 255)
            ->requirePresence('archive_unit_identifier', 'create')
            ->notEmptyString('archive_unit_identifier');

        $validator
            ->scalar('archive_unit_name')
            ->requirePresence('archive_unit_name', 'create')
            ->notEmptyString('archive_unit_name');

        $validator
            ->scalar('description_xpath')
            ->requirePresence('description_xpath', 'create')
            ->notEmptyString('description_xpath');

        return $validator;
    }
}
