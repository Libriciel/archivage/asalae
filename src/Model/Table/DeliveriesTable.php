<?php

/**
 * Asalae\Model\Table\DeliveriesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table deliveries
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin StateMachineBehavior
 * @mixin OptionsBehavior
 * @property DeliveryRequestsTable DeliveryRequests
 */
class DeliveriesTable extends Table implements AfterDeleteInterface, SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    public const string S_CREATING = 'creating';
    public const string S_AVAILABLE = 'available';
    public const string S_DOWNLOADED = 'downloaded';
    public const string S_ACQUITTED = 'acquitted';

    public const string T_BUILD = 'build';
    public const string T_DOWNLOAD = 'download';
    public const string T_ACQUIT = 'acquit';

    /**
     * @var string Etat initial
     */
    public $initialState = self::S_CREATING;

    /**
     * @var array Changement d'êtats selon action
     */
    public $transitions = [
        self::T_BUILD => [
            self::S_CREATING => self::S_AVAILABLE,
        ],
        self::T_DOWNLOAD => [
            self::S_AVAILABLE => self::S_DOWNLOADED,
        ],
        self::T_ACQUIT => [
            self::S_DOWNLOADED => self::S_ACQUITTED,
        ],
    ];

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    self::S_CREATING,
                    self::S_AVAILABLE,
                    self::S_DOWNLOADED,
                    self::S_ACQUITTED,
                ],
            ]
        );

        $this->belongsTo('DeliveryRequests');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->scalar('comment')
            ->allowEmptyString('comment');

        $validator
            ->integer('archive_units_count')
            ->requirePresence('archive_units_count', 'create')
            ->notEmptyString('archive_units_count');

        $validator
            ->integer('original_count')
            ->requirePresence('original_count', 'create')
            ->notEmptyString('original_count');

        $validator
            ->requirePresence('original_size', 'create')
            ->notEmptyString('original_size');

        return $validator;
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }

    /**
     * Supprime le zip lié à la communication
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function afterDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        Filesystem::remove($entity->get('zip'));
        $this->DeliveryRequests->transitionAll(
            DeliveryRequestsTable::T_DELETE_DELIVERY,
            ['id' => $entity->get('delivery_request_id')]
        );
    }
}
