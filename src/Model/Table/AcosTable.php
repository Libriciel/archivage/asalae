<?php

/**
 * Asalae\Model\Table\AcosTable
 */

namespace Asalae\Model\Table;

use Acl\Model\Table\AcosTable as AclAcosTable;
use Cake\ORM\Behavior\TreeBehavior;

/**
 * Table acos
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TreeBehavior
 */
class AcosTable extends AclAcosTable
{
    /**
     * Initialize a table instance. Called after the constructor.
     *
     * You can use this method to define associations, attach behaviors
     * define validation and do any other initialization logic you need.
     *
     * ```
     *  public function initialize(array $config)
     *  {
     *      $this->belongsTo('Users');
     *      $this->belongsToMany('Tagging.Tags');
     *      $this->setPrimaryKey('something_else');
     *  }
     * ```
     *
     * @param array<string, mixed> $config Configuration options passed to the constructor
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Tree', ['type' => 'nested']);

        $this->belongsToMany('Aros')
            ->setThrough('ArosAcos');
        $this->hasMany('ArosAcos');
    }
}
