<?php

/**
 * Asalae\Model\Table\SerializeDeleteAllTrait
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Table;

/**
 * Assure la serialization de l'entité avant suppression
 * @mixin Table
 */
trait SerializeDeleteAllTrait
{
    /**
     * Deletes all records matching the provided conditions.
     *
     * This method will *not* trigger beforeDelete/afterDelete events. If you
     * need those first load a collection of records and delete them.
     *
     * This method will *not* execute on associations' `cascade` attribute. You should
     * use database foreign keys + ON CASCADE rules if you need cascading deletes combined
     * with this method.
     *
     * @param mixed $conditions Conditions to be used, accepts anything Query::where()
     *                          can take.
     * @return int Returns the number of affected rows.
     * @see \Cake\Datasource\RepositoryInterface::delete()
     */
    public function deleteAll($conditions): int
    {
        return $this->deleteAllSerialize($conditions);
    }

    /**
     * Serialize les entités avant suppression
     * @param mixed $conditions
     * @return int Returns the number of affected rows.
     */
    public function deleteAllSerialize($conditions)
    {
        $query = $this->find()
            ->where($conditions)
            ->distinct([$this->getAlias() . '.' . $this->getPrimaryKey()]);
        /** @var EntityInterface|PremisObjectEntityInterface $entity */
        foreach ($query as $entity) {
            EventLogsTable::serializeObject(
                $this->getAlias(),
                $entity->id,
                $entity
            );
        }
        $query = $this->deleteQuery()
            ->where($conditions);
        $statement = $query->execute();
        $statement->closeCursor();

        return $statement->rowCount();
    }
}
