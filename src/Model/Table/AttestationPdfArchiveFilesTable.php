<?php

/**
 * Asalae\Model\Table\AttestationPdfArchiveFilesTable
 */

namespace Asalae\Model\Table;

/**
 * Table archive_files (xxx_attestation.pdf)
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AttestationPdfArchiveFilesTable extends ArchiveFilesTable
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('archive_files');
        $this->belongsTo('Archives')
            ->setForeignKey('archive_id')
            ->setConditions(['AttestationPdfArchiveFiles.type' => ArchiveFilesTable::TYPE_ARCHIVING_CERTIFICATE]);
        $this->belongsTo('StoredFiles');
    }
}
