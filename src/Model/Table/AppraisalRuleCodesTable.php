<?php

/**
 * Asalae\Model\Table\AppraisalRuleCodesTable
 */

namespace Asalae\Model\Table;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Table appraisal_rule_codes
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AppraisalRuleCodesTable extends Table implements
    SerializeDeletedEntitiesInterface
{
    use SerializeDeleteAllTrait;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('OrgEntities');
        $this->hasMany('AppraisalRules');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 255)
            ->requirePresence('code', 'create')
            ->notEmptyString('code')
            ->add(
                'code',
                'unique',
                [
                    'rule' => function ($value, array $context) {
                        $conditions = [
                            'code' => $value,
                            'OR' => [
                                'org_entity_id IS' => null,
                                'org_entity_id' => Hash::get($context, 'data.org_entity_id', 0),
                            ],
                        ];
                        if (!$context['newRecord']) {
                            $conditions['id !='] = Hash::get($context, 'data.id', 0);
                        }
                        $count = $this->find()
                            ->where($conditions)
                            ->count();
                        return $count === 0;
                    },
                    'provider' => 'table',
                    'message' => __("Ce code est déjà utilisé"),
                ]
            );

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->scalar('duration')
            ->maxLength('duration', 255)
            ->requirePresence('duration', 'create')
            ->notEmptyString('duration')
            ->regex(
                'duration',
                "/^P\d+(Y(\d+M)?(\d+D)?|M(\d+D)?|D)$/",
                __("Erreur de syntaxe")
            );

        return $validator;
    }

    /**
     * Retourne la liste des codes de DUA pour un SA et une version du SEDA
     * Ajoute des groupes d'options si SEDA 2.x et si règles spécifiques
     *
     * @param int    $archivalAgencyId id du SA
     * @param string $sedaVersion      (seda0.2, seda1.0, seda2.1, seda2.2)
     * @param string $valueField       (code par
     *                                 défaut,
     *                                 id)
     * @return array
     */
    public function getRuleCodes(int $archivalAgencyId, string $sedaVersion, string $valueField = 'code'): array
    {
        $query = $this->find();
        if ($sedaVersion === 'seda1.0' || $sedaVersion === 'seda0.2') {
            $query->where(['AppraisalRuleCodes.org_entity_id IS' => null]);
            $valueField = $valueField === 'code' ? 'duration' : $valueField;
        } else {
            $query->where(
                [
                    'OR' => [
                        'AppraisalRuleCodes.org_entity_id IS' => null,
                        'AppraisalRuleCodes.org_entity_id' => $archivalAgencyId,
                    ],
                ]
            );
        }
        $appraisalRuleCodes = $query
            ->orderBy(['id'])
            ->all()
            ->toArray();

        $specificOptions = [];
        $globalOptions = [];
        /** @var EntityInterface $appraisalRuleCode */
        foreach ($appraisalRuleCodes as $appraisalRuleCode) {
            $newOption = [
                'value' => $appraisalRuleCode->get($valueField),
                'title' => $appraisalRuleCode->get('description'),
                'text' => $appraisalRuleCode->get('code') . ' - ' . $appraisalRuleCode->get('name'),
            ];
            if (empty($appraisalRuleCode->get('org_entity_id'))) {
                $globalOptions[] = $newOption;
            } else {
                $specificOptions[] = $newOption;
            }
        }
        if (empty($specificOptions)) {
            return $globalOptions;
        } else {
            return [
                __("Codes spécifiques") => $specificOptions,
                __("Codes globaux") => $globalOptions,
            ];
        }
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        EventLogsTable::serializeObject(
            $this->getAlias(),
            $entity->id,
            $entity
        );
    }
}
