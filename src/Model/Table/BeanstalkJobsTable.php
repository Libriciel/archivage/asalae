<?php

/**
 * Asalae\Model\Table\BeanstalkJobsTable
 */

namespace Asalae\Model\Table;

use AsalaeCore\Model\Behavior\OptionsBehavior;
use Beanstalk\Model\Table\BeanstalkJobsTable as BeanstalkBeanstalkJobsTable;
use Cake\ORM\Behavior\TimestampBehavior;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table beanstalk_jobs
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @mixin StateMachineBehavior
 * @mixin TimestampBehavior
 */
class BeanstalkJobsTable extends BeanstalkBeanstalkJobsTable
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine', ['fields' => ['state' => 'job_state']]);
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    'ready',
                    'reserved',
                    'delayed',
                    'buried',
                ],
                'job_state' => [
                    BeanstalkBeanstalkJobsTable::S_PENDING,
                    BeanstalkBeanstalkJobsTable::S_WORKING,
                    BeanstalkBeanstalkJobsTable::S_FAILED,
                ],
            ]
        );
        $this->setEntityClass('BeanstalkJob');
        $this->belongsTo('BeanstalkWorkers');
        $this->belongsTo('Users');
        $this->belongsTo('Archives')
            ->setForeignKey('object_foreign_key')
            ->setConditions([$this->getAlias() . '.object_model' => 'Archives']);
        $this->belongsTo('Transfers')
            ->setForeignKey('object_foreign_key')
            ->setConditions([$this->getAlias() . '.object_model' => 'Transfers']);
        $this->belongsTo('BatchTreatments')
            ->setForeignKey('object_foreign_key')
            ->setConditions([$this->getAlias() . '.object_model' => 'BatchTreatments']);
    }
}
