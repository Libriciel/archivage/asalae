<?php

/**
 * Asalae\Model\Behavior\SealBehavior
 */

namespace Asalae\Model\Behavior;

use Asalae\Controller\Component\SealComponent;
use Asalae\Exception\GenericException;
use Asalae\Model\Table\OrgEntitiesTable;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\Behavior;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Closure;

/**
 * Ajoute les conditions d'étanchéité
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SealBehavior extends Behavior
{
    /**
     * Default config
     *
     * These are merged with user-provided config when the component is used.
     *
     * @var array<string, mixed>
     */
    protected array $_defaultConfig = [
        'user' => [],
        'userId' => null,
        'orgEntity' => [],
        'orgEntityId' => null,
        'archivalAgency' => [],
        'archivalAgencyId' => null,
        'superArchivist' => false,
        'adminTech' => false,
        'condition' => false,
    ];

    /**
     * Constructor
     *
     * Merges config with the default and store in the config property
     *
     * @param \Cake\ORM\Table      $table  The table this behavior is attached to.
     * @param array<string, mixed> $config The config for this behavior.
     */
    public function __construct(Table $table, array $config = [])
    {
        foreach ($config as $key => $value) {
            if ($value instanceof EntityInterface) {
                $config[$key] = $value->toArray();
            }
        }

        // gestion super archivistes
        $archivalLft = Hash::get($config, 'archivalAgency.lft');
        $archivalRght = Hash::get($config, 'archivalAgency.rght');
        if (
            Hash::get($config, 'orgEntity.lft') < $archivalLft
            && Hash::get($config, 'orgEntity.rght') > $archivalRght
        ) {
            $config = Hash::insert($config, 'orgEntity.lft', $archivalLft);
            $config = Hash::insert($config, 'orgEntity.rght', $archivalRght);
        }

        parent::__construct($table, $config);
    }

    /**
     * Ajoute les conditions d'étanchéité
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findSeal(Query $query, array $options): Query
    {
        $modelName = $this->getConfig('model');
        $fn = "conditionsFor$modelName";
        if (!method_exists($this, $fn)) {
            throw new GenericException($fn . ' does not exists in SealBehavior');
        }
        if ($this->getConfig('adminTech')) {
            return $query;
        }
        if (!$this->getConfig('userId')) {
            throw new GenericException('A user must be logged in to request a seal');
        }
        if (!$this->getConfig('archivalAgencyId')) {
            throw new ForbiddenException(__("L'utilisateur doit être lié à un service d'archives"));
        }

        return $this->$fn($query, $options);
    }

    /**
     * equivalent du Table->exists()
     * @param array $conditions
     * @return bool
     */
    public function sealExists(array $conditions): bool
    {
        return (bool)count(
            $this->table()->find('seal')
                ->select(['existing' => 1])
                ->where($conditions)
                ->limit(1)
                ->disableHydration()
                ->toArray()
        );
    }

    /**
     * Ajoute les conditions d'étanchéité sur un find('list')
     * @param Query               $query
     * @param string|Closure|null $keyField
     * @param string|Closure|null $valueField
     * @param string|Closure|null $groupField
     * @param string              $valueSeparator
     * @return Query
     */
    public function findSealList(
        Query $query,
        string|Closure|null $keyField = null,
        string|Closure|null $valueField = null,
        string|Closure|null $groupField = null,
        string $valueSeparator = ';'
    ): Query {
        $modelName = $this->getConfig('model');
        $fn = "conditionsFor$modelName";
        if (!method_exists($this, $fn)) {
            throw new GenericException($fn . ' does not exists in SealBehavior');
        }
        if ($this->getConfig('adminTech')) {
            return $this->table()->findList($query, $keyField, $valueField, $groupField, $valueSeparator);
        }
        if (!$this->getConfig('userId')) {
            throw new GenericException('A user must be logged in to request a seal');
        }
        if (!$this->getConfig('archivalAgencyId')) {
            throw new ForbiddenException(__("L'utilisateur doit être lié à un service d'archives"));
        }
        $query = $this->table()->findList($query, $keyField, $valueField, $groupField, $valueSeparator);

        return $this->$fn($query);
    }

    /**
     * Ajoute les conditions d'étanchéité sur un find('threaded')
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findSealThreaded(Query $query, array $options): Query
    {
        $modelName = $this->getConfig('model');
        $fn = "conditionsFor$modelName";
        $keyField = $options['keyField'] ?? null;
        $parentField = $options['parentField'] ?? 'parent_id';
        $nestingKey = 'children';
        if (!method_exists($this, $fn)) {
            throw new GenericException($fn . ' does not exists in SealBehavior');
        }
        if ($this->getConfig('adminTech')) {
            return $this->table()->findThreaded($query, $keyField, $parentField, $nestingKey);
        }
        if (!$this->getConfig('userId')) {
            throw new GenericException('A user must be logged in to request a seal');
        }
        if (!$this->getConfig('archivalAgencyId')) {
            throw new ForbiddenException(__("L'utilisateur doit être lié à un service d'archives"));
        }
        $query = $this->table()->findThreaded($query, $keyField, $parentField, $nestingKey);

        return $this->$fn($query, $options);
    }

    /**
     * Conditions pour Agreements
     * @param Query $query
     * @return Query
     */
    public function conditionsForAgreements(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Ajoute les conditions selon la config condition
     * @param Query $query
     * @param array $baseModels
     * @return Query
     */
    private function appendConditions(Query $query, array $baseModels = ['ArchivalAgencies']): Query
    {
        switch ($this->getConfig('condition')) {
            case SealComponent::COND_ARCHIVAL_AGENCY:
                return $this->appendConditionsArchivalAgency($query, $baseModels);
            case SealComponent::COND_ARCHIVAL_AGENCY_CHILDS:
                return $this->appendConditionsArchivalAgencyChilds($query, $baseModels);
            case SealComponent::COND_HIERARCHICAL_VIEW:
                return $this->appendConditionsHierarchicalView($query, $baseModels);
            case SealComponent::COND_ORG_ENTITY:
                return $this->appendConditionsOrgEntity($query, $baseModels);
            case SealComponent::COND_ORG_ENTITY_CHILDS:
                return $this->appendConditionsOrgEntityChilds($query, $baseModels);
        }
        throw new GenericException('condition not found');
    }

    /**
     * Applique une condition type :
     *      Model.archival_agency_id = LoggedUser.archival_agency_id
     * @param Query $query
     * @param array $baseModels
     * @return Query
     */
    private function appendConditionsArchivalAgency(Query $query, array $baseModels): Query
    {
        $conditions = [];
        foreach ($baseModels as $baseModel) {
            $conditions["$baseModel.id"] = $this->getConfig('archivalAgencyId');
        }
        if (count($conditions) > 1) {
            $conditions = ['OR' => $conditions];
        }
        return $query->where($conditions);
    }

    /**
     * Applique une condition type :
     *      Model.lft >= ArchivalAgency.lft AND Model.rght <= ArchivalAgency.rght
     * @param Query $query
     * @param array $baseModels
     * @return Query
     */
    private function appendConditionsArchivalAgencyChilds(Query $query, array $baseModels): Query
    {
        $lft = $this->getConfig('archivalAgency.lft');
        $rght = $this->getConfig('archivalAgency.rght');
        if (!$lft || !$rght) {
            throw new GenericException('archivalAgency is required');
        }
        $conditions = [];
        foreach ($baseModels as $baseModel) {
            $conditions[] = [
                "$baseModel.lft >=" => $lft,
                "$baseModel.rght <=" => $rght,
            ];
        }
        if (count($conditions) > 1) {
            $conditions = ['OR' => $conditions];
        }
        return $query->where($conditions);
    }

    /**
     * Se distingue de "archivalAgencyChilds" par la nécéssité que le role
     * utilisateur doit avoir l'option "hierarchical_view"
     * De plus, selon le type de l'entité de l'utilisateur, le lft/rght est soit
     * sur le service d'archives, soit sur l'entité
     *
     * Applique une condition type :
     *      Model.lft >= ArchivalAgency.lft AND Model.rght >= ArchivalAgency.rght
     * @param Query $query
     * @param array $baseModels
     * @return Query
     */
    private function appendConditionsHierarchicalView(Query $query, array $baseModels): Query
    {
        $typeEntity = $this->getConfig('orgEntity.type_entity.code');
        if (empty($typeEntity)) {
            throw new GenericException('orgEntity.type_entity is required');
        }
        $query = $this->appendConditionsArchivalAgency($query, (array)current($baseModels));
        if (in_array($typeEntity, ['SA', 'CST'])) {
            $lft = $this->getConfig('archivalAgency.lft');
            $rght = $this->getConfig('archivalAgency.rght');
        } elseif ($this->getConfig('user.role.hierarchical_view')) {
            $lft = $this->getConfig('orgEntity.lft');
            $rght = $this->getConfig('orgEntity.rght');
        } else {
            return $this->appendConditionsOrgEntity($query, $baseModels);
        }
        if (!$lft || !$rght) {
            throw new GenericException('archivalAgency is required');
        }
        $conditions = [];
        foreach ($baseModels as $baseModel) {
            $andCond = [
                "$baseModel.lft >=" => $lft,
                "$baseModel.rght <=" => $rght,
            ];
            if (count($baseModels) > 1) {
                $conditions['OR'][] = $andCond;
            } else {
                $conditions[] = $andCond;
            }
        }
        return $query->where($conditions);
    }

    /**
     * Applique une condition type :
     *      Model.org_entity_id = LoggedUser.org_entity_id
     * @param Query $query
     * @param array $baseModels
     * @return Query
     */
    private function appendConditionsOrgEntity(Query $query, array $baseModels): Query
    {
        $conditions = [];
        foreach ($baseModels as $baseModel) {
            $conditions["$baseModel.id"] = $this->getConfig('orgEntityId');
        }
        if (count($conditions) > 1) {
            $conditions = ['OR' => $conditions];
        }
        return $query->where($conditions);
    }

    /**
     * Applique une condition type :
     *      Model.lft >= OrgEntity.lft AND Model.rght >= OrgEntity.rght
     * @param Query $query
     * @param array $baseModels
     * @return Query
     */
    private function appendConditionsOrgEntityChilds(Query $query, array $baseModels): Query
    {
        $lft = $this->getConfig('orgEntity.lft');
        $rght = $this->getConfig('orgEntity.rght');
        if (!$lft || !$rght) {
            throw new GenericException('orgEntity is required');
        }
        $conditions = [];
        foreach ($baseModels as $baseModel) {
            $conditions[] = [
                "$baseModel.lft >=" => $lft,
                "$baseModel.rght <=" => $rght,
            ];
        }
        if (count($conditions) > 1) {
            $conditions = ['OR' => $conditions];
        }
        return $query->where($conditions);
    }

    /**
     * Conditions pour ArchiveBinaries
     * @param Query $query
     * @return Query
     */
    public function conditionsForArchiveBinaries(Query $query): Query
    {
        $query
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.Archives')
            ->innerJoinWith('ArchiveUnits.Archives.ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour ArchiveBinariesArchiveUnits
     * @param Query $query
     * @return Query
     */
    public function conditionsForArchiveBinariesArchiveUnits(Query $query): Query
    {
        $query
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.Archives')
            ->innerJoinWith('ArchiveUnits.Archives.ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour ArchiveFiles
     * @param Query $query
     * @return Query
     */
    public function conditionsForArchiveFiles(Query $query): Query
    {
        $query
            ->innerJoinWith('Archives')
            ->innerJoinWith('Archives.ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour ArchiveKeywords
     * @param Query $query
     * @return Query
     */
    public function conditionsForArchiveKeywords(Query $query): Query
    {
        $query
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.Archives')
            ->innerJoinWith('ArchiveUnits.Archives.ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour ArchiveUnits
     * @param Query $query
     * @return Query
     */
    public function conditionsForArchiveUnits(Query $query): Query
    {
        $query
            ->innerJoinWith('Archives')
            ->innerJoinWith('Archives.ArchivalAgencies')
            ->innerJoinWith('Archives.TransferringAgencies')
            ->innerJoinWith('Archives.OriginatingAgencies');
        return $this->appendConditions(
            $query,
            ['ArchivalAgencies', 'TransferringAgencies', 'OriginatingAgencies']
        );
    }

    /**
     * Conditions pour ArchiveUnitsBatchTreatments
     * @param Query $query
     * @return Query
     */
    public function conditionsForArchiveUnitsBatchTreatments(Query $query): Query
    {
        $query
            ->innerJoinWith('BatchTreatments')
            ->innerJoinWith('BatchTreatments.ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour ArchiveUnitsDestructionRequests
     * @param Query $query
     * @return Query
     */
    public function conditionsForArchiveUnitsDestructionRequests(Query $query): Query
    {
        $query
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.Archives')
            ->innerJoinWith('ArchiveUnits.Archives.ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour ArchiveUnitsOutgoingTransferRequests
     * @param Query $query
     * @return Query
     */
    public function conditionsForArchiveUnitsOutgoingTransferRequests(Query $query): Query
    {
        $query
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.Archives')
            ->innerJoinWith('ArchiveUnits.Archives.ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour ArchiveUnitsRestitutionRequests
     * @param Query $query
     * @return Query
     */
    public function conditionsForArchiveUnitsRestitutionRequests(Query $query): Query
    {
        $query
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.Archives')
            ->innerJoinWith('ArchiveUnits.Archives.ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour Archives
     * @param Query $query
     * @return Query
     */
    public function conditionsForArchives(Query $query): Query
    {
        $query->innerJoinWith('ArchivalAgencies');
        if ($this->getConfig('condition') === SealComponent::COND_ARCHIVAL_AGENCY) {
            return $this->appendConditionsArchivalAgency($query, ['ArchivalAgencies']);
        }
        $query
            ->innerJoinWith('TransferringAgencies')
            ->innerJoinWith('OriginatingAgencies');
        return $this->appendConditions(
            $query,
            ['ArchivalAgencies', 'TransferringAgencies', 'OriginatingAgencies']
        );
    }

    /**
     * Conditions pour ArchivingSystems
     * @param Query $query
     * @return Query
     */
    public function conditionsForArchivingSystems(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour BatchTreatments
     * @param Query $query
     * @return Query
     */
    public function conditionsForBatchTreatments(Query $query): Query
    {
        $query
            ->innerJoinWith('ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour BeanstalkJobs
     * @param Query $query
     * @return Query
     */
    public function conditionsForBeanstalkJobs(Query $query): Query
    {
        $query
            ->innerJoinWith('Users')
            ->innerJoinWith('Users.OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour Counters
     * @param Query $query
     * @return Query
     */
    public function conditionsForCounters(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour Deliveries
     * @param Query $query
     * @return Query
     */
    public function conditionsForDeliveries(Query $query): Query
    {
        $query
            ->innerJoinWith('DeliveryRequests')
            ->innerJoinWith('DeliveryRequests.ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour DeliveryRequests
     * @param Query $query
     * @return Query
     */
    public function conditionsForDeliveryRequests(Query $query): Query
    {
        $baseModel = $this->getConfig('condition') === SealComponent::COND_ARCHIVAL_AGENCY
            ? 'ArchivalAgencies'
            : 'Requesters';
        $query->innerJoinWith($baseModel);
        return $this->appendConditions($query, [$baseModel]);
    }

    /**
     * Conditions pour DestructionNotifications
     * @param Query $query
     * @return Query
     */
    public function conditionsForDestructionNotifications(Query $query): Query
    {
        $query
            ->innerJoinWith('DestructionRequests')
            ->innerJoinWith('DestructionRequests.ArchivalAgencies');
        $baseModels = ['ArchivalAgencies'];
        if ($this->getConfig('condition') === SealComponent::COND_ORG_ENTITY) {
            $baseModels[] = 'OriginatingAgencies';
            $query
                ->innerJoinWith('DestructionRequests.OriginatingAgencies');
        }
        return $this->appendConditions($query, $baseModels);
    }

    /**
     * Conditions pour DestructionRequests
     * @param Query $query
     * @return Query
     */
    public function conditionsForDestructionRequests(Query $query): Query
    {
        $query
            ->innerJoinWith('ArchivalAgencies');
        $baseModels = ['ArchivalAgencies'];
        if ($this->getConfig('condition') === SealComponent::COND_ORG_ENTITY) {
            $baseModels[] = 'OriginatingAgencies';
            $query
                ->innerJoinWith('OriginatingAgencies');
        }
        return $this->appendConditions($query, $baseModels);
    }

    /**
     * Conditions pour Eaccpfs
     * @param Query $query
     * @return Query
     */
    public function conditionsForEaccpfs(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour Fileuploads
     * @param Query $query
     * @return Query
     */
    public function conditionsForFileuploads(Query $query): Query
    {
        $typeEntity = $this->getConfig('user.org_entity.type_entity.code');
        if (in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)) {
            $query
                ->innerJoinWith('Users')
                ->innerJoinWith('Users.ArchivalAgencies');
            return $this->appendConditions($query);
        }
        $query
            ->innerJoinWith('Users')
            ->innerJoinWith('Users.OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour KeywordLists
     * @param Query $query
     * @return Query
     */
    public function conditionsForKeywordLists(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour Keywords
     * @param Query $query
     * @return Query
     */
    public function conditionsForKeywords(Query $query): Query
    {
        $query
            ->innerJoinWith('KeywordLists')
            ->innerJoinWith('KeywordLists.OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour Ldaps
     * @param Query $query
     * @return Query
     */
    public function conditionsForLdaps(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour Ldaps
     * @param Query $query
     * @return Query
     */
    public function conditionsForOrgEntities(Query $query): Query
    {
        switch ($this->getConfig('condition')) {
            case SealComponent::COND_ARCHIVAL_AGENCY:
            case SealComponent::COND_HIERARCHICAL_VIEW:
                $baseModels = ['ArchivalAgencies', 'OrgEntities'];
                $query->contain('ArchivalAgencies');
                break;
            case SealComponent::COND_ARCHIVAL_AGENCY_CHILDS:
            case SealComponent::COND_ORG_ENTITY:
            case SealComponent::COND_ORG_ENTITY_CHILDS:
                $baseModels = ['OrgEntities'];
                break;
            default:
                throw new GenericException('condition not found');
        }
        return $this->appendConditions($query, $baseModels);
    }

    /**
     * Conditions pour OrgEntitiesTimestampers
     * @param Query $query
     * @return Query
     */
    public function conditionsForOrgEntitiesTimestampers(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour OutgoingTransferRequests
     * @param Query $query
     * @return Query
     */
    public function conditionsForOutgoingTransferRequests(Query $query): Query
    {
        $query
            ->innerJoinWith('ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour OutgoingTransfers
     * @param Query $query
     * @return Query
     */
    public function conditionsForOutgoingTransfers(Query $query): Query
    {
        $query
            ->innerJoinWith('OutgoingTransferRequests')
            ->innerJoinWith('OutgoingTransferRequests.ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour Profiles
     * @param Query $query
     * @return Query
     */
    public function conditionsForProfiles(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour RestitutionRequests
     * @param Query $query
     * @return Query
     */
    public function conditionsForRestitutionRequests(Query $query): Query
    {
        $query
            ->innerJoinWith('ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour Restitutions
     * @param Query $query
     * @return Query
     */
    public function conditionsForRestitutions(Query $query): Query
    {
        $query
            ->innerJoinWith('RestitutionRequests')
            ->innerJoinWith('RestitutionRequests.ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour Roles
     * @param Query $query
     * @return Query
     */
    public function conditionsForRoles(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour SecureDataSpaces
     * @param Query $query
     * @return Query
     */
    public function conditionsForSecureDataSpaces(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour Sequences
     * @param Query $query
     * @return Query
     */
    public function conditionsForSequences(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour ServiceLevels
     * @param Query $query
     * @return Query
     */
    public function conditionsForServiceLevels(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour TechnicalArchives
     * @param Query $query
     * @return Query
     */
    public function conditionsForTechnicalArchives(Query $query): Query
    {
        $query
            ->innerJoinWith('ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour TransferAttachments
     * @param Query $query
     * @return Query
     */
    public function conditionsForTransferAttachments(Query $query): Query
    {
        $query
            ->innerJoinWith('Transfers')
            ->innerJoinWith('Transfers.OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour TransferErrors
     * @param Query $query
     * @return Query
     */
    public function conditionsForTransferErrors(Query $query): Query
    {
        $query
            ->innerJoinWith('Transfers')
            ->innerJoinWith('Transfers.OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour Transfers
     * @param Query $query
     * @return Query
     */
    public function conditionsForTransfers(Query $query): Query
    {
        $query
            ->innerJoinWith('ArchivalAgencies')
            ->innerJoinWith('OriginatingAgencies')
            ->innerJoinWith('TransferringAgencies');
        return $this->appendConditions($query, ['ArchivalAgencies', 'OriginatingAgencies', 'TransferringAgencies']);
    }

    /**
     * Conditions pour Users
     * @param Query $query
     * @return Query
     */
    public function conditionsForUsers(Query $query): Query
    {
        $typeEntity = $this->getConfig('user.org_entity.type_entity.code');
        if (in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)) {
            $query
                ->leftJoinWith('SuperArchivistsArchivalAgencies')
                ->innerJoinWith('OrgEntities')
                ->innerJoinWith('OrgEntities.ArchivalAgencies');
            $query->where(
                [
                    'OR' => [
                        'ArchivalAgencies.id' => $this->getConfig('archivalAgencyId'),
                        'SuperArchivistsArchivalAgencies.archival_agency_id' => $this->getConfig('archivalAgencyId'),
                    ],
                ]
            );
            return $query;
        }
        $query
            ->innerJoinWith('OrgEntities')
            ->innerJoinWith('OrgEntities.ArchivalAgencies');
        return $this->appendConditions($query);
    }

    /**
     * Conditions pour ValidationChains
     * @param Query $query
     * @return Query
     */
    public function conditionsForValidationChains(Query $query): Query
    {
        $query
            ->innerJoinWith('OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour ValidationProcesses
     * @param Query $query
     * @return Query
     */
    public function conditionsForValidationProcesses(Query $query): Query
    {
        $query
            ->innerJoinWith('ValidationChains')
            ->innerJoinWith('ValidationChains.OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }

    /**
     * Conditions pour ValidationStages
     * @param Query $query
     * @return Query
     */
    public function conditionsForValidationStages(Query $query): Query
    {
        $query
            ->innerJoinWith('ValidationChains')
            ->innerJoinWith('ValidationChains.OrgEntities');
        return $this->appendConditions($query, ['OrgEntities']);
    }
}
