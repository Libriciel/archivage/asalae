<?php

/**
 * Asalae\Model\Entity\OaipmhArchive
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\ORM\Entity;
use DOMDocument;
use Exception;

/**
 * Entité de la table oaipmh_archives
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OaipmhArchive extends Entity
{
    /**
     * Appel direct à la méthode de l'archive
     *
     * @param DOMDocument $dom
     * @param string      $verb
     * @param string|null $metadataPrefix
     * @throws VolumeException
     */
    public function addOaipmhXmlToDom(
        DOMDocument $dom,
        string $verb,
        string $metadataPrefix = null
    ) {
        $archive = $this->_fields['archive'] ?? null;
        if (!$archive instanceof Archive) {
            throw new Exception(
                '$this->archive is not instance or Archive for ' . __FUNCTION__
            );
        }
        $archive->addOaipmhXmlToDom($dom, $verb, $metadataPrefix);
    }
}
