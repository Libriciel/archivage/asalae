<?php

/**
 * Asalae\Model\Entity\PublicDescriptionArchiveFile
 */

namespace Asalae\Model\Entity;

/**
 * Entité de la table archive_files (xxx_pubdesc.xml)
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class PublicDescriptionArchiveFile extends DescriptionXmlArchiveFile
{
}
