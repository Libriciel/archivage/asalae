<?php

/**
 * Asalae\Model\Entity\CompositeArchiveUnit
 */

namespace Asalae\Model\Entity;

use Cake\ORM\Entity;

/**
 * Entité de la table composite_archive_units
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CompositeArchiveUnit extends Entity
{
}
