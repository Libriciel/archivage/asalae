<?php

/**
 * Asalae\Model\Entity\ZipableEntityInterface
 */

namespace Asalae\Model\Entity;

/**
 * L'entité peut générer un zip destiné au téléchargement
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface ZippableEntityInterface
{
    /**
     * Chemin vers le dossier de téléchargement du zip
     * @return string
     */
    public static function getZipDownloadFolder(): string;

    /**
     * Estimated time for the creation of the zip
     * @return int
     */
    public function zipCreationTimeEstimation(): int;

    /**
     * Construit un fichier zip à partir de l'entité
     * @return string
     */
    public function makeZip(): string;
}
