<?php

/**
 * Asalae\Model\Entity\Profile
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Cake\Core\Configure;
use Cake\ORM\Association\HasMany;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table Profiles
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Profile extends Entity implements PremisObjectEntityInterface
{
    /**
     * @var array champs virtuel
     */
    protected array $_virtual = ['deletable', 'editableIdentifier'];

    /**
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $Profiles = TableRegistry::getTableLocator()->get('Profiles');
        $has = ['Archives', 'Transfers'];
        foreach ($has as $modelName) {
            /** @var HasMany $assoc */
            $assoc = $Profiles->{$modelName};
            $query = $assoc->find()
                ->where([$assoc->getForeignKey() => $this->_fields['id']]);
            if ($query->count() > 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Donne le chemin de base pour le stockage des fichiers
     * ex: $basepath.DS.$filename
     * @return string
     */
    protected function _getBasepath(): string
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        return rtrim(
            Configure::read('App.paths.data'),
            DS
        ) . DS . 'profiles' . DS . $this->_fields['id'];
    }

    /**
     * Indique si l'identifier du profil est modifiable :
     *      false si il existe au moins un transfert pour lequel transfer.profile_id = profile.id
     *          et transfert.state != 'rejected'
     *      true dans le cas contraire
     * @return bool
     */
    protected function _getEditableIdentifier(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $rejectedTransfers = $Transfers->find()
            ->where(
                [
                    'profile_id' => $this->_fields['id'],
                    'state IS NOT' => 'rejected',
                ]
            )
            ->count();

        return $rejectedTransfers === 0;
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'Profiles:' . $this->id
        );
        $object->originalName = $this->_fields['identifier'] ?? '';
        if ($name = ($this->_fields['name'] ?? '')) {
            $object->significantProperties['name'] = $name;
        }
        return $object;
    }
}
