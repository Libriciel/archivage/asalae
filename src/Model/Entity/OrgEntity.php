<?php

/**
 * Asalae\Model\Entity\OrgEntity
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\OrgEntity as CoreOrgEntity;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\Utility\Premis;
use Cake\ORM\Association\HasMany;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/**
 * Entité de la table org_entities
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OrgEntity extends CoreOrgEntity implements PremisObjectEntityInterface
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = [];

    /**
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        if ($this->_getCode() === 'SE') {
            return false;
        }
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $query = $Transfers->find()->where(
            [
                'OR' => [
                    'Transfers.archival_agency_id' => $this->_fields['id'],
                    'Transfers.transferring_agency_id' => $this->_fields['id'],
                ],
            ],
            [],
            true
        );
        if ($query->count() > 0) {
            return false;
        }
        $query = $Archives->find()->where(
            [
                'OR' => [
                    'Archives.archival_agency_id' => $this->_fields['id'],
                    'Archives.transferring_agency_id' => $this->_fields['id'],
                    'Archives.originating_agency_id' => $this->_fields['id'],
                    'Archives.transferring_operator_id' => $this->_fields['id'],
                ],
            ],
            [],
            true
        );
        if ($query->count() > 0) {
            return false;
        }
        $has = [
            'AccessRuleCodes',
            'Agreements',
            'AgreementsOriginatingAgencies',
            'AgreementsTransferringAgencies',
            'AppraisalRuleCodes',
            'ArchivalAgencyMessageIndicators',
            'ArchivingSystems',
            'ChildOrgEntities',
            'Ldaps',
            'OriginatingAgencyMessageIndicators',
            'Profiles',
            'Roles',
            'ServiceLevels',
            'TransferringAgencyMessageIndicators',
            'Users',
            'ValidationChains',
        ];
        foreach ($has as $modelName) {
            /** @var HasMany $assoc */
            $assoc = $OrgEntities->{$modelName};
            $query = $assoc->find()
                ->where([$assoc->getForeignKey() => $this->_fields['id']]);
            if ($query->count() > 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Impossible de modifier un identifiant qui sert dans un transfert ou une
     * archive
     * @return bool
     */
    protected function _getEditableIdentifier(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $id = $this->_fields['id'];
        $editableIdentifier = false;
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $hasTransfer = $Transfers->find()
            ->select('id')
            ->where(
                [
                    'OR' => [
                        'archival_agency_id' => $id,
                        'transferring_agency_id' => $id,
                    ],
                ]
            )
            ->disableHydration()
            ->first();
        if (!$hasTransfer) {
            $Archives = TableRegistry::getTableLocator()->get('Archives');
            $editableIdentifier = $Archives->find()
                    ->select('id')
                    ->where(
                        [
                            'OR' => [
                                'archival_agency_id' => $id,
                                'transferring_agency_id' => $id,
                                'originating_agency_id' => $id,
                            ],
                        ]
                    )
                    ->disableHydration()
                    ->first()
                === null;
        }
        return $editableIdentifier;
    }

    /**
     * Pour un Service d'Archive, récupère l'id de son ecs par défaut
     * Note : ne devrait pas pouvoir retourner null pour un SA,
     * seulement pour une org_entity non arechival_agency
     * @return int|null
     */
    protected function _getDefaultSecureDataSpaceId(): ?int
    {
        if (empty($this->_fields['id'])) {
            return null;
        }

        $secureDataSpace = TableRegistry::getTableLocator()->get('SecureDataSpaces')
            ->find()
            ->where(
                [
                    'org_entity_id' => $this->_fields['id'],
                    'is_default' => true,
                ]
            )
            ->first();

        return $secureDataSpace
            ? (int)$secureDataSpace->get('id')
            : null;
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'OrgEntities:' . $this->id
        );
        $object->originalName = $this->_fields['identifier'] ?? '';
        if ($name = ($this->_fields['name'] ?? '')) {
            $object->significantProperties['name'] = $name;
        }
        return $object;
    }

    /**
     * Donne la valeur d'une clé de configuration (service d'archives)
     * @param string $configName
     * @param mixed  $default
     * @return mixed|null
     */
    public function getConfig(string $configName, $default = null)
    {
        if (empty($this->get('configurations'))) {
            $Configurations = TableRegistry::getTableLocator()->get('Configurations');
            $configurations = $Configurations->find()
                ->where(['org_entity_id' => $this->id])
                ->toArray();
            $this->set('configurations', $configurations);
        }
        $configurations = $this->get('configurations') ?: [];
        foreach ($configurations as $configuration) {
            if (Hash::get($configuration, 'name') === $configName) {
                return $configuration->get('setting');
            }
        }
        return $default;
    }
}
