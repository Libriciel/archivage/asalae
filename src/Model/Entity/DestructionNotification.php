<?php

/**
 * Asalae\Model\Entity\DestructionNotification
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Date as CakeDate;
use Cake\Utility\Hash;
use Cake\View\ViewBuilder;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * Entité de la table destruction_notifications
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DestructionNotification extends Entity
{
    /**
     * @var array Champs virtuels
     */
    protected array $_virtual = ['basepath', 'xml'];

    /**
     * Donne l'uri du xml
     * @return string
     */
    public function _getXml(): string
    {
        if (empty($this->_fields['id']) || empty($this->_fields['identifier'])) {
            return '';
        }
        return $this->_getBasepath()
            . DS . $this->_fields['identifier'] . '_destruction_notification.xml';
    }

    /**
     * Donne le chemin de conservation des données d'une communication
     * @return string
     */
    protected function _getBasepath(): string
    {
        if (empty($this->id)) {
            return '';
        }
        return rtrim(
            Configure::read('App.paths.data'),
            DS
        ) . DS . 'destruction_notifications' . DS . $this->id;
    }

    /**
     * Donne le chemin de base d'une unité d'archives
     * @param EntityInterface $archive
     * @return string
     */
    public function getArchivePath(EntityInterface $archive): string
    {
        return $this->_getBasepath() . DS . urlencode(
            $archive->get('archival_agency_identifier')
        );
    }

    /**
     * Génère le fichier xml issue de l'entité
     * @return string|null Rendered content or null if content already rendered and returned earlier.
     * @throws Exception
     */
    public function generateXml()
    {
        $required = [
            'identifier',
            'destruction_request.created',
            'destruction_request.identifier',
            'destruction_request.archive_units.0.archival_agency_identifier',
            'destruction_request.archival_agency.identifier',
            'destruction_request.originating_agency.identifier',
        ];
        foreach ($required as $r) {
            if (Hash::get($this->_fields, $r) === null) {
                throw new Exception('missing required data (' . $r . ')');
            }
        }
        $aaIdentifiers = Hash::extract(
            $this->_fields,
            'destruction_request.archive_units.{n}.archival_agency_identifier'
        );
        $units = [];
        foreach ($aaIdentifiers as $identifier) {
            $units[] = htmlspecialchars(
                $identifier,
                ENT_XML1
            );
        }

        /** @var DateTime $date */
        $viewBuilder = new ViewBuilder();
        $viewBuilder->setTemplate('/DestructionNotifications/xml/seda_2.1');
        $viewBuilder->setLayout('ajax');
        $date = Hash::get($this, 'destruction_request.created');
        $viewBuilder->setVars(
            [
                'comment' => $this->getEscaped('destruction_request.comment'),
                'date' => ($date instanceof DateTimeInterface || $date instanceof CakeDate)
                    ? $date->format(DateTimeInterface::RFC3339)
                    : $date,
                'identifier' => $this->getEscaped('identifier'),
                'requestIdentifier' => $this->getEscaped(
                    'destruction_request.identifier'
                ),
                'archiveUnits' => $units,
                'archivalAgency' => $this->getEscaped(
                    'destruction_request.archival_agency.identifier'
                ),
                'originatingAgency' => $this->getEscaped(
                    'destruction_request.originating_agency.identifier'
                ),
            ]
        );
        return $viewBuilder->build()->render();
    }

    /**
     * Donne une version protégé du champ demandé
     * @param string $path
     * @return string
     */
    private function getEscaped(string $path)
    {
        return htmlspecialchars(
            Hash::get($this->_fields, $path, ''),
            ENT_XML1
        );
    }
}
