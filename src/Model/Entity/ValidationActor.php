<?php

/**
 * Asalae\Model\Entity\ValidationActor
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Table\MailsTable;
use Asalae\Model\Table\ValidationChainsTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Model\Entity\AppMetaTrait;
use AsalaeCore\ORM\Entity;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\NotImplementedException;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Entité de la table validation_actors
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ValidationActor extends Entity
{
    use AppMetaTrait;

    public const array MAP_TYPE_ACTOR = [
        ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM => [
            'USER',
            'MAIL',
            'SERVICE_ARCHIVES',
        ],
        ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM => [
            'USER',
            'MAIL',
            'SERVICE_ARCHIVES',
        ],
        ValidationChainsTable::ARCHIVE_DELIVERY_REQUEST => [
            'USER',
            'MAIL',
            'SERVICE_ARCHIVES',
            'SERVICE_DEMANDEUR',
        ],
        ValidationChainsTable::ARCHIVE_DESTRUCTION_REQUEST => [
            'USER',
            'MAIL',
            'SERVICE_ARCHIVES',
            'SERVICE_PRODUCTEUR',
        ],
        ValidationChainsTable::ARCHIVE_RESTITUTION_REQUEST => [
            'USER',
            'MAIL',
            'SERVICE_ARCHIVES',
            'SERVICE_PRODUCTEUR',
        ],
        ValidationChainsTable::ARCHIVE_OUTGOING_TRANSFER_REQUEST => [
            'USER',
            'MAIL',
            'SERVICE_ARCHIVES',
            'SERVICE_PRODUCTEUR',
        ],
    ];

    /**
     * @var array Champs du app_meta
     */
    protected $_metaFields = [
        'actor_name',
        'actor_info',
        'created_user_id',
        'type_validation',
    ];

    /**
     * Champs virtuels
     * @var array
     */
    protected array $_virtual = [
        'actor_name',
        'app_typetrad',
        'meta',
        'type_validation',
        'type_validationtrad',
    ];

    /**
     * Traductions des app_type
     * @return string
     */
    protected function _getAppTypetrad(): string
    {
        switch ($this->_fields['app_type'] ?? '') {
            case '':
                return '';
            case 'USER':
                return __dx('validation-actor', 'app_type', "USER");
            case 'MAIL':
                return __dx('validation-actor', 'app_type', "MAIL");
            case 'PARAPHEUR':
                return __dx('validation-actor', 'app_type', "PARAPHEUR");
            case 'SERVICE_ARCHIVES':
                return __dx('validation-actor', 'app_type', "SERVICE_ARCHIVES");
            case 'SERVICE_PRODUCTEUR':
                return __dx(
                    'validation-actor',
                    'app_type',
                    "SERVICE_PRODUCTEUR"
                );
            case 'SERVICE_DEMANDEUR':
                return __dx(
                    'validation-actor',
                    'app_type',
                    "SERVICE_DEMANDEUR"
                );
        }
        return $this->_fields['app_type'];
    }

    /**
     * Défini le champ type_validation du app_meta
     * @param string $value
     * @return string
     */
    protected function _setTypeValidation($value)
    {
        return $this->genericAppMetaSetter('type_validation', $value);
    }

    /**
     * Défini le champ actor_name du app_meta
     * @param string $value
     * @return string
     */
    protected function _setActorName($value)
    {
        return $this->genericAppMetaSetter('actor_name', $value);
    }

    /**
     * Traductions des type_validation
     * @return string
     */
    protected function _getTypeValidationtrad(): string
    {
        switch ($this->_fields['type_validation'] ?? '') {
            case '':
                return '';
            case 'V': // Visa
                return __dx('validation-actor', 'type_validation', "V");
            case 'S': // Signature
                return __dx('validation-actor', 'type_validation', "S");
        }
        return $this->_fields['type_validation'];
    }

    /**
     * Notification au besoin d'un nouveau process
     * @param EntityInterface $process
     * @return bool
     * @throws Exception
     */
    public function notify(EntityInterface $process): bool
    {
        if (($this->_fields['app_type'] ?? '') == 'MAIL') {
            return $this->createAndSubmitMail($process);
        }
        return true;
    }

    /**
     * Notification par email
     * @param EntityInterface $process
     * @return bool
     * @throws Exception
     */
    private function createAndSubmitMail(EntityInterface $process)
    {
        if (empty($this->_fields['app_key'])) {
            return false;
        }
        switch ($process->get('app_subject_type')) {
            case 'Transfers':
                return $this->sendMailTransfer($process);
            case 'DeliveryRequests':
                return $this->sendMailDeliveryRequest($process);
            case 'DestructionRequests':
                return $this->sendMailDestructionRequest($process);
            case 'RestitutionRequests':
                return $this->sendMailResitutionRequest($process);
            case 'OutgoingTransferRequests':
                return $this->sendMailOutgoingTransferRequest($process);
            default:
                throw new NotImplementedException();
        }
    }

    /**
     * Validation d'un transfert par email
     * @param EntityInterface $process
     * @return bool
     * @throws Exception
     */
    private function sendMailTransfer(EntityInterface $process)
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $code = $AuthUrls->newEntity(
            [
                'url' => sprintf(
                    '/validation-processes/process-transfer/%d/%d/%d',
                    $process->get('app_foreign_key'), // transfer_id
                    $this->id, // actor_id
                    $process->id // process_id
                ),
                'auth_sub_urls' => [
                    [
                        'url' => sprintf(
                            '/transfers/display-xml/%d',
                            $process->get('app_foreign_key') // transfer_id
                        ),
                    ],
                ],
            ]
        );
        $AuthUrls->saveOrFail($code);

        $transfer = TableRegistry::getTableLocator()->get('Transfers')->find()
            ->where(['id' => $process->get('app_foreign_key')])
            ->firstOrFail();
        $stage = TableRegistry::getTableLocator()
            ->get('ValidationStages')
            ->find()
            ->where(['id' => $process->get('current_stage_id')])
            ->firstOrFail();

        $prefix = $this->getEmailPrefix();
        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $email->setViewVars(
            [
                'htmlSignature' => $this->getHtmlSignature(),
                'textSignature' => $this->getTextSignature(),
            ]
        );
        $email->viewBuilder()
            ->setTemplate('process_transfer')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);
        $subject = $prefix . ' '
            . __(
                "Prendre une décision - transfert {0}",
                h($transfer->get('transfer_identifier'))
            );
        $email->setEmailFormat('both')
            ->setSubject($subject)
            ->setViewVars(
                [
                    'title' => __("Prendre une décision"),
                    'code' => $code,
                    'transfer' => $transfer,
                    'stage' => $stage,
                    'process' => $process,
                    'actor' => $this,
                ]
            )
            ->setTo($this->_fields['app_key']);

        /** @var MailsTable $Mails */
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        return $Mails->asyncMail($email, null);
    }

    /**
     * Donnne le prefix de l'email (configurable par service d'archives)
     * par défaut: '[asalae]'
     * @return string
     */
    private function getEmailPrefix(): string
    {
        return Configure::read('ConfigArchivalAgency.mail-title-prefix')
            ?: '[asalae]';
    }

    /**
     * Donne la signature html de l'email (configurable par service d'archives)
     * @return string|null
     */
    private function getHtmlSignature()
    {
        return Configure::read('ConfigArchivalAgency.mail-html-signature')
            ?: null;
    }

    /**
     * Donne la signature text de l'email (configurable par service d'archives)
     * @return string|null
     */
    private function getTextSignature()
    {
        return Configure::read('ConfigArchivalAgency.mail-text-signature')
            ?: null;
    }

    /**
     * Validation d'une demande de communication par email
     * @param EntityInterface $process
     * @return bool
     * @throws Exception
     */
    private function sendMailDeliveryRequest(EntityInterface $process)
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $code = $AuthUrls->newEntity(
            [
                'url' => sprintf(
                    '/validation-processes/process-delivery-request-mail/%d/%d',
                    $process->id, // process_id
                    $this->id // actor_id
                ),
            ]
        );
        $AuthUrls->saveOrFail($code);

        $deliveryRequest = TableRegistry::getTableLocator()->get(
            'DeliveryRequests'
        )->find()
            ->where(['id' => $process->get('app_foreign_key')])
            ->firstOrFail();
        $stage = TableRegistry::getTableLocator()
            ->get('ValidationStages')
            ->find()
            ->where(['id' => $process->get('current_stage_id')])
            ->firstOrFail();

        $prefix = $this->getEmailPrefix();
        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $email->setViewVars(
            [
                'htmlSignature' => $this->getHtmlSignature(),
                'textSignature' => $this->getTextSignature(),
            ]
        );
        $email->viewBuilder()
            ->setTemplate('process_delivery_request')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);
        $subject = $prefix . ' '
            . __(
                "Prendre une décision - demande de communication {0}",
                h($deliveryRequest->get('identifier'))
            );
        $email->setEmailFormat('both')
            ->setSubject($subject)
            ->setViewVars(
                [
                    'title' => __("Prendre une décision"),
                    'code' => $code,
                    'deliveryRequest' => $deliveryRequest,
                    'stage' => $stage,
                    'process' => $process,
                    'actor' => $this,
                ]
            )
            ->setTo($this->_fields['app_key']);

        /** @var MailsTable $Mails */
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        return $Mails->asyncMail($email, null);
    }

    /**
     * Validation d'une demande d'élimination par email
     * @param EntityInterface $process
     * @return bool
     * @throws Exception
     */
    private function sendMailDestructionRequest(EntityInterface $process)
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $code = $AuthUrls->newEntity(
            [
                'url' => sprintf(
                    '/validation-processes/process-destruction-request-mail/%d/%d',
                    $process->id, // process_id
                    $this->id // actor_id
                ),
            ]
        );
        $AuthUrls->saveOrFail($code);

        $destructionRequest = TableRegistry::getTableLocator()->get(
            'DestructionRequests'
        )->find()
            ->where(['id' => $process->get('app_foreign_key')])
            ->firstOrFail();
        $stage = TableRegistry::getTableLocator()
            ->get('ValidationStages')
            ->find()
            ->where(['id' => $process->get('current_stage_id')])
            ->firstOrFail();

        $prefix = $this->getEmailPrefix();
        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $email->viewBuilder()
            ->setTemplate('process_destruction_request')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);
        $subject = $prefix . ' '
            . __(
                "Prendre une décision - demande d'élimination {0}",
                h($destructionRequest->get('identifier'))
            );
        $email->setEmailFormat('both')
            ->setSubject($subject)
            ->setViewVars(
                [
                    'title' => __("Prendre une décision"),
                    'code' => $code,
                    'destructionRequest' => $destructionRequest,
                    'stage' => $stage,
                    'process' => $process,
                    'actor' => $this,
                ]
            )
            ->setTo($this->_fields['app_key']);

        /** @var MailsTable $Mails */
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        return $Mails->asyncMail($email, null);
    }

    /**
     * Validation d'une demande de restitution par email
     * @param EntityInterface $process
     * @return bool
     * @throws Exception
     */
    private function sendMailResitutionRequest(EntityInterface $process)
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $code = $AuthUrls->newEntity(
            [
                'url' => sprintf(
                    '/validation-processes/process-restitution-request-mail/%d/%d',
                    $process->id, // process_id
                    $this->id // actor_id
                ),
            ]
        );
        $AuthUrls->saveOrFail($code);

        $restitutionRequest = TableRegistry::getTableLocator()->get(
            'RestitutionRequests'
        )->find()
            ->where(['id' => $process->get('app_foreign_key')])
            ->firstOrFail();
        $stage = TableRegistry::getTableLocator()
            ->get('ValidationStages')
            ->find()
            ->where(['id' => $process->get('current_stage_id')])
            ->firstOrFail();

        $prefix = $this->getEmailPrefix();
        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $email->viewBuilder()
            ->setTemplate('process_restitution_request')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);
        $subject = $prefix . ' '
            . __(
                "Prendre une décision - demande de restitution {0}",
                h($restitutionRequest->get('identifier'))
            );
        $email->setEmailFormat('both')
            ->setSubject($subject)
            ->setViewVars(
                [
                    'title' => __("Prendre une décision"),
                    'code' => $code,
                    'restitutionRequest' => $restitutionRequest,
                    'stage' => $stage,
                    'process' => $process,
                    'actor' => $this,
                ]
            )
            ->setTo($this->_fields['app_key']);

        /** @var MailsTable $Mails */
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        return $Mails->asyncMail($email, null);
    }

    /**
     * Validation d'une demande de transfert sortant par email
     * @param EntityInterface $process
     * @return bool
     * @throws Exception
     */
    private function sendMailOutgoingTransferRequest(EntityInterface $process)
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $code = $AuthUrls->newEntity(
            [
                'url' => sprintf(
                    '/validation-processes/process-outgoing-transfer-request-mail/%d/%d',
                    $process->id, // process_id
                    $this->id // actor_id
                ),
            ]
        );
        $AuthUrls->saveOrFail($code);

        $outgoingTransferRequest = TableRegistry::getTableLocator()->get(
            'OutgoingTransferRequests'
        )->find()
            ->where(['id' => $process->get('app_foreign_key')])
            ->firstOrFail();
        $stage = TableRegistry::getTableLocator()
            ->get('ValidationStages')
            ->find()
            ->where(['id' => $process->get('current_stage_id')])
            ->firstOrFail();

        $prefix = $this->getEmailPrefix();
        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $email->viewBuilder()
            ->setTemplate('process_outgoing_transfer_request')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);
        $subject = $prefix . ' '
            . __(
                "Prendre une décision - demande de transfert sortant {0}",
                h($outgoingTransferRequest->get('identifier'))
            );
        $email->setEmailFormat('both')
            ->setSubject($subject)
            ->setViewVars(
                [
                    'title' => __("Prendre une décision"),
                    'code' => $code,
                    'outgoingTransferRequest' => $outgoingTransferRequest,
                    'stage' => $stage,
                    'process' => $process,
                    'actor' => $this,
                ]
            )
            ->setTo($this->_fields['app_key']);

        /** @var MailsTable $Mails */
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        return $Mails->asyncMail($email, null);
    }

    /**
     * Donne une phrase descriptive de l'acteur
     * @return string
     * @throws Exception
     */
    protected function _getActorInfo(): string
    {
        switch ($this->_fields['app_type']) {
            case 'USER':
                /** @var \Cake\Datasource\EntityInterface $user */
                $user = $this->getUser();
                $name = $user->get('name') ?: $user->get('username');
                return __("Utilisateur: {0}", h($name));
            case 'MAIL':
                $name = $this->get('actor_name');
                $email = $this->get('app_key');
                return __("E-mail: {0} ({1})", h($name), h($email));
            case 'SERVICE_ARCHIVES':
                return __("Service d'Archives");
            case 'SERVICE_DEMANDEUR':
                return __("Service demandeur");
            case 'SERVICE_PRODUCTEUR':
                return __("Service producteur");
            default:
                return '';
        }
    }

    /**
     * Donne l'utilisateur lié à un acteur de validation
     * @return EntityInterface
     * @throws Exception
     */
    public function getUser(): EntityInterface
    {
        if (
            empty($this->_fields['app_foreign_key'])
            && empty($this->get('user'))
        ) {
            throw new Exception();
        }
        if ($user = $this->get('user')) {
            if (is_array($user)) {
                $user = new Entity($user);
            }
            return $user;
        }
        $Users = TableRegistry::getTableLocator()->get('Users');
        $this->_fields['user'] = $Users->get($this->_fields['app_foreign_key']);
        return $this->_fields['user'];
    }
}
