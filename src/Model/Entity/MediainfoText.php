<?php

/**
 * Asalae\Model\Entity\MediainfoText
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table mediainfo_texts
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MediainfoText extends Entity
{
}
