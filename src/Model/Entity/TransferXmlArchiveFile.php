<?php

/**
 * Asalae\Model\Entity\TransferXmlArchiveFile
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use DOMDocument;

/**
 * Entité de la table archive_files (xxx_transfer.xml)
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TransferXmlArchiveFile extends ArchiveFile
{
    /**
     * @return DOMDocument|null
     * @throws VolumeException
     */
    public function getDom()
    {
        if (empty($this->dom) && !empty($this->_fields['stored_file_id'])) {
            $volumeManager = VolumeManager::createWithStoredFileId(
                $this->_fields['stored_file_id']
            );
            $this->dom = new DOMDocument();
            $this->dom->formatOutput = true;
            $this->dom->preserveWhiteSpace = false;
            $xml = $volumeManager->get($this->_fields['stored_file_id']);
            $this->dom->loadXML($xml);
        }
        return $this->dom;
    }
}
