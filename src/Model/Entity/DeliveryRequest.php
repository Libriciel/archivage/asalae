<?php

/**
 * Asalae\Model\Entity\DeliveryRequest
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Table\DeliveryRequestsTable;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\View\ViewBuilder;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * Entité de la table delivery_requests
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DeliveryRequest extends Entity implements PremisObjectEntityInterface
{
    /**
     * Champs virtuels
     * @var array
     */
    protected array $_virtual = [
        'deletable',
        'editable',
        'sendable',
        'statetrad',
        'xml',
        'pdf_basename',
    ];

    /**
     * Traductions des états
     * @return string
     */
    protected function _getStatetrad(): string
    {
        if (empty($this->_fields['state'])) {
            return '';
        }
        switch ($state = $this->_fields['state'] ?? '') {
            case DeliveryRequestsTable::S_CREATING:
                return __dx('delivery-request', 'state', "création");
            case DeliveryRequestsTable::S_VALIDATING:
                return __dx(
                    'delivery-request',
                    'state',
                    "en cours de validation"
                );
            case DeliveryRequestsTable::S_DELIVERY_AVAILABLE:
                return __dx(
                    'delivery-request',
                    'state',
                    "la communication a été générée"
                );
            case DeliveryRequestsTable::S_DELIVERY_DOWNLOADED:
                return __dx(
                    'delivery-request',
                    'state',
                    "la communication a été téléchargée"
                );
            case DeliveryRequestsTable::S_DELIVERY_ACQUITTED:
                return __dx(
                    'delivery-request',
                    'state',
                    "la communication a été acquittée"
                );
            case DeliveryRequestsTable::S_ACCEPTED:
                return __dx('delivery-request', 'state', "acceptée");
            case DeliveryRequestsTable::S_REJECTED:
                return __dx('delivery-request', 'state', "rejetée");
            case DeliveryRequestsTable::S_DELIVERY_DELETED:
                return __dx(
                    'delivery-request',
                    'state',
                    "la communication a été supprimée"
                );
            default:
                return $state;
        }
    }

    /**
     * Action edit
     * @return bool
     */
    protected function _getEditable()
    {
        if (empty($this->_fields['state'])) {
            return true;
        }
        return $this->_fields['state'] === 'creating';
    }

    /**
     * Action delete
     * @return bool
     */
    protected function _getDeletable()
    {
        if (empty($this->_fields['state'])) {
            return true;
        }
        return in_array($this->_fields['state'], ['creating', 'rejected']);
    }

    /**
     * Action delete
     * @return bool
     */
    protected function _getSendable()
    {
        if (empty($this->id) || empty($this->_fields['state'])) {
            return false;
        } elseif ($this->_fields['state'] === 'creating') {
            return true;
        }
        $link = TableRegistry::getTableLocator()->get(
            'ArchiveUnitsDeliveryRequests'
        );
        return !$link->exists(['delivery_request_id' => $this->id]);
    }

    /**
     * Donne l'uri du xml
     * @return string
     */
    public function _getXml(): string
    {
        if (empty($this->_fields['id']) || empty($this->_fields['filename'])) {
            return '';
        }
        return rtrim(Configure::read('App.paths.data'), DS)
            . DS . 'delivery-requests'
            . DS . $this->_fields['id']
            . DS . $this->_fields['filename'];
    }

    /**
     * Génère le fichier xml issue de l'entité
     * @return string|null Rendered content or null if content already rendered and returned earlier.
     * @throws Exception
     */
    public function generateXml(): ?string
    {
        $required = [
            'identifier',
            'derogation',
            'archive_units.0.archival_agency_identifier',
            'created',
            'archival_agency.identifier',
            'requester.identifier',
        ];
        foreach ($required as $r) {
            if (Hash::get($this->_fields, $r) === null) {
                throw new Exception('missing required data (' . $r . ')');
            }
        }
        $queryArchiveUnits = TableRegistry::getTableLocator()->get(
            'ArchiveUnits'
        )->find()
            ->innerJoinWith('ArchiveUnitsDeliveryRequests')
            ->where(
                [
                    'ArchiveUnitsDeliveryRequests.delivery_request_id' => $this->id
                        ?: 0,
                ]
            )
            ->orderBy(['ArchiveUnits.archival_agency_identifier'])
            ->disableHydration();
        $aaIdentifiers = Hash::extract(
            $this->_fields,
            'archive_units.{n}.archival_agency_identifier'
        );
        $units = [];
        foreach ($aaIdentifiers as $identifier) {
            $units[] = htmlspecialchars(
                $identifier,
                ENT_XML1
            );
        }

        /** @var DateTime $date */
        $viewBuilder = new ViewBuilder();
        $viewBuilder->setTemplate('/DeliveryRequests/xml/seda_2.1');
        $viewBuilder->setLayout('ajax');
        $viewBuilder->setVars(
            [
                'deliveryRequest' => $this,
                'comment' => $this->getEscaped('comment'),
                'date' => ($date = $this->_fields['created']) instanceof DateTimeInterface
                    ? $date->format(DATE_RFC3339)
                    : $date,
                'identifier' => $this->getEscaped('identifier'),
                'queryArchiveUnits' => $queryArchiveUnits,
                'derogation' => $this->getEscaped('derogation'),
                'archiveUnits' => $units,
                'archivalAgency' => $this->getEscaped(
                    'archival_agency.identifier'
                ),
                'requester' => $this->getEscaped('requester.identifier'),
            ]
        );
        return $viewBuilder->build()->render();
    }

    /**
     * Donne une version protégé du champ demandé
     * @param string $path
     * @return string
     */
    private function getEscaped(string $path): string
    {
        return htmlspecialchars(
            Hash::get($this->_fields, $path, ''),
            ENT_XML1
        );
    }

    /**
     * Donne le nom du fichier pdf
     * @return string
     */
    protected function _getPdfBasename(): string
    {
        $identifier = urlencode($this->_fields['identifier'] ?? 'undefined');
        return "delivery_request_$identifier.pdf";
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'DeliveryRequests:' . $this->id
        );
        $object->originalName = $this->_fields['identifier'] ?? '';
        return $object;
    }
}
