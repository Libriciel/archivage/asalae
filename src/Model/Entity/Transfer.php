<?php

/**
 * Asalae\Model\Entity\Transfer
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\PronomsTable;
use Asalae\Model\Table\TransfersTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Form\MessageForm;
use AsalaeCore\Form\MessageFormInterface;
use AsalaeCore\Form\MessageSchema\MessageSchema;
use AsalaeCore\Form\MessageSchema\Seda02Schema;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Form\MessageSchema\Seda21Schema;
use AsalaeCore\Form\MessageSchema\Seda22Schema;
use AsalaeCore\Form\Seda02Form;
use AsalaeCore\Form\Seda10Form;
use AsalaeCore\Form\Seda21Form;
use AsalaeCore\Form\Seda22Form;
use AsalaeCore\Model\Entity\DateTrait;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\HashUtility;
use AsalaeCore\Utility\PreControlMessages;
use AsalaeCore\Utility\Premis;
use Cake\Core\Configure;
use Cake\Database\Exception\DatabaseException;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\NotImplementedException;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\Number;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Text;
use Cake\Utility\Xml;
use Cake\View\ViewBuilder;
use Datacompressor\Utility\DataCompressor;
use DateInterval;
use DateTime;
use DateTimeInterface;
use DomDocument;
use DOMElement;
use DOMException;
use DOMNode;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use XsltProcessor;

/**
 * Entité de la table transferts
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017-2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property int $elementsPerPage
 */
class Transfer extends Entity implements
    ZippableEntityInterface,
    PremisObjectEntityInterface,
    EntityWithSessionDataInterface
{
    /**
     * Traits
     */
    use DateTrait;
    use ZippableEntityTrait;

    /**
     * @var string[] ex: 'fmt/18' => 'Acrobat PDF 1.4 - Portable Document Format'
     */
    private static $formatNames;
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = [
        'deletable',
        'editable',
        'force_deletable',
        'hiddable',
        'is_large',
        'message_versiontrad',
        'pdf',
        'pdf_basename',
        'statetrad',
        'statetrad_title',
        'is_locked',
        'has_expired_lock',
    ];
    /**
     * @var int
     */
    private $elementsPerPage;

    private DOMUtility $util;

    /**
     * Renvoi une réponse selon si le transfert a été accepté, la version du seda
     * et le type de message demandé
     * @param string $messageType Acknowledgement, ArchiveTransferReply ou Reply
     * @return array
     * @throws Exception
     */
    public function createResponse(string $messageType): array
    {
        $accepted = $this->get('is_accepted');
        $sedaVersion = $this->get('message_version');
        if (!in_array($sedaVersion, ['seda0.2', 'seda1.0', 'seda2.1', 'seda2.2'])) {
            throw new NotImplementedException($sedaVersion);
        }
        if (strtolower($messageType) === 'acknowledgement') {
            return $this->createAcknowledgement();
        } elseif ($accepted === null) {
            throw new InternalErrorException(
                'Transfert en cours de traitement'
            );
        } elseif ($sedaVersion === 'seda0.2' && $accepted) {
            return $this->createArchiveTransferAcceptance02();
        } else {
            return $this->createArchiveTransferReply($accepted);
        }
    }

    /**
     * Créé un Acknowledgement
     * @return array
     * @throws Exception
     */
    private function createAcknowledgement(): array
    {
        switch ($sedaVersion = $this->get('message_version')) {
            case 'seda0.2':
                return $this->createArchiveTransferReply02('000');
            case 'seda1.0':
                return $this->createAcknowledgement10();
            case 'seda2.1':
                return $this->createAcknowledgement21();
            case 'seda2.2':
                return $this->createAcknowledgement22();
            default:
                throw new NotImplementedException($sedaVersion);
        }
    }

    /**
     * Créé un ArchiveTransferReply pour du Seda 0.2
     * @param string $code
     * @return array
     * @throws Exception
     */
    private function createArchiveTransferReply02(string $code): array
    {
        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities');
        $sa = $this->get('archival_agency')
            ?: $OrgEntities->get(
                $this->get('archival_agency_id')
            );
        $ta = $this->get('transferring_agency')
            ?: $OrgEntities->get(
                $this->get('transferring_agency_id')
            );

        /** @var CountersTable $Counters */
        $Counters = $loc->get('Counters');
        $replyIdentifier = $Counters->next(
            $sa->get('id'),
            'ArchiveTransferReply'
        );

        /** @var DateTime $date */
        $date = $this->get('transfer_date');
        return [
            'ArchiveTransferReply' => [
                '@xmlns' => NAMESPACE_SEDA_02,
                'Comment' => $this->get('transfer_comment'),
                'Date' => $date->format(DATE_RFC3339),
                'ReplyCode' => [
                    '@' => $code,
                    '@listVersionID' => 'edition 2009',
                ],
                'TransferIdentifier' => $this->get('transfer_identifier'),
                'TransferReplyIdentifier' => $replyIdentifier,
                'TransferringAgency' => [
                    'Identification' => $ta->get('identifier'),
                    'Name' => $ta->get('name'),
                ],
                'ArchivalAgency' => [
                    'Identification' => $sa->get('identifier'),
                    'Name' => $sa->get('name'),
                ],
            ],
        ];
    }

    /**
     * Créé un Acknowledgement
     * @return array
     * @throws Exception
     */
    private function createAcknowledgement10(): array
    {
        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities');
        $sa = $this->get('archival_agency')
            ?: $OrgEntities->get(
                $this->get('archival_agency_id')
            );
        $ta = $this->get('transferring_agency')
            ?: $OrgEntities->get(
                $this->get('transferring_agency_id')
            );

        /** @var CountersTable $Counters */
        $Counters = $loc->get('Counters');
        $acknowledgementIdentifier = $Counters->next(
            $sa->get('id'),
            'AcknowledgementIdentifier'
        );

        /** @var DateTime $date */
        $date = $this->get('transfer_date');
        return [
            'Acknowledgement' => [
                '@xmlns' => NAMESPACE_SEDA_10,
                'Comment' => $this->get('transfer_comment'),
                'Date' => $date->format(DATE_RFC3339),
                'AcknowledgementIdentifier' => $acknowledgementIdentifier,
                'MessageReceivedIdentifier' => $this->get(
                    'transfer_identifier'
                ),
                'Receiver' => [
                    'Identification' => $sa->get('identifier'),
                    'Name' => $sa->get('name'),
                ],
                'Sender' => [
                    'Identification' => $ta->get('identifier'),
                    'Name' => $ta->get('name'),
                ],
            ],
        ];
    }

    /**
     * Créé un Acknowledgement
     * @return array
     * @throws Exception
     */
    private function createAcknowledgement21(): array
    {
        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities');
        $sa = $this->get('archival_agency')
            ?: $OrgEntities->get(
                $this->get('archival_agency_id')
            );
        $ta = $this->get('transferring_agency')
            ?: $OrgEntities->get(
                $this->get('transferring_agency_id')
            );

        /** @var CountersTable $Counters */
        $Counters = $loc->get('Counters');
        $acknowledgementIdentifier = $Counters->next(
            $sa->get('id'),
            'AcknowledgementIdentifier'
        );

        /** @var DateTime $date */
        $date = $this->get('transfer_date');
        return [
            'Acknowledgement' => [
                '@xmlns' => NAMESPACE_SEDA_21,

                // MessageType
                'Comment' => $this->get('transfer_comment'),
                'Date' => $date->format(DATE_RFC3339),
                'MessageIdentifier' => $acknowledgementIdentifier,

                'MessageReceivedIdentifier' => $this->get(
                    'transfer_identifier'
                ),
                'Sender' => [
                    'Identifier' => $ta->get('identifier'),
                ],
                'Receiver' => [
                    'Identifier' => $sa->get('identifier'),
                ],
            ],
        ];
    }

    /**
     * Créé un Acknowledgement
     * @return array
     * @throws Exception
     */
    private function createAcknowledgement22(): array
    {
        $data = $this->createAcknowledgement21();
        $data['Acknowledgement']['@xmlns'] = NAMESPACE_SEDA_22;
        return $data;
    }

    /**
     * Créé un ArchiveTransferAcceptance
     * @return array
     * @throws Exception
     */
    private function createArchiveTransferAcceptance02(): array
    {
        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities');
        $sa = $this->get('archival_agency')
            ?: $OrgEntities->get(
                $this->get('archival_agency_id')
            );
        $ta = $this->get('transferring_agency')
            ?: $OrgEntities->get(
                $this->get('transferring_agency_id')
            );

        /** @var CountersTable $Counters */
        $Counters = $loc->get('Counters');
        $transferAcceptanceIdentifier = $Counters->next(
            $sa->get('id'),
            'ArchiveTransferAcceptance'
        );

        $Archives = $loc->get('Archives');
        $archives = $Archives->find()
            ->innerJoinWith('Transfers')
            ->where(['Transfers.id' => $this->get('id')])
            ->contain(['DescriptionXmlArchiveFiles'])
            ->all()
            ->map(
                function (EntityInterface $v) {
                    /** @var DescriptionXmlArchiveFile $description */
                    $description = $v->get('description_xml_archive_file');
                    return Hash::get(
                        Xml::toArray($description->getDom()),
                        'Archive'
                    );
                }
            )
            ->toArray();

        /** @var DateTime $date */
        $date = $this->get('transfer_date');
        return [
            'ArchiveTransferAcceptance' => [
                '@xmlns' => NAMESPACE_SEDA_02,
                'Comment' => $this->get('transfer_comment'),
                'Date' => $date->format(DATE_RFC3339),
                'ReplyCode' => [
                    '@' => '000',
                    '@listVersionID' => 'edition 2009',
                ],
                'TransferAcceptanceIdentifier' => $transferAcceptanceIdentifier,
                'TransferIdentifier' => $this->get('transfer_identifier'),
                'TransferringAgency' => [
                    'Identification' => $ta->get('identifier'),
                    'Name' => $ta->get('name'),
                ],
                'ArchivalAgency' => [
                    'Identification' => $sa->get('identifier'),
                    'Name' => $sa->get('name'),
                ],
                'Archive' => count($archives) === 1 ? $archives[0] : $archives,
            ],
        ];
    }

    /**
     * Créé un ArchiveTransferReply pour du Seda 1.0
     * @param boolean $accepted
     * @return array
     * @throws Exception
     */
    private function createArchiveTransferReply($accepted): array
    {
        switch ($sedaVersion = $this->get('message_version')) {
            case 'seda0.2':
                return $this->createArchiveTransferReply02('101');
            case 'seda1.0':
                return $this->createArchiveTransferReply10($accepted);
            case 'seda2.1':
                return $this->createArchiveTransferReply21($accepted);
            case 'seda2.2':
                return $this->createArchiveTransferReply22($accepted);
            default:
                throw new NotImplementedException($sedaVersion);
        }
    }

    /**
     * Créé un ArchiveTransferReply pour du Seda 1.0
     * @param boolean $accepted
     * @return array
     * @throws Exception
     */
    private function createArchiveTransferReply10($accepted): array
    {
        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities');
        $sa = $this->get('archival_agency')
            ?: $OrgEntities->get(
                $this->get('archival_agency_id')
            );
        $ta = $this->get('transferring_agency')
            ?: $OrgEntities->get(
                $this->get('transferring_agency_id')
            );

        /** @var CountersTable $Counters */
        $Counters = $loc->get('Counters');
        $replyIdentifier = $Counters->next(
            $sa->get('id'),
            'ArchiveTransferReply'
        );

        /** @var DateTime $date */
        $date = $this->get('transfer_date');
        $return = [
            'ArchiveTransferReply' => [
                '@xmlns' => NAMESPACE_SEDA_10,
                'Comment' => $this->get('transfer_comment'),
                'Date' => $date->format(DATE_RFC3339),
                'GrantDate' => $date->format(DATE_RFC3339),
                'ReplyCode' => [
                    '@' => ($accepted ? '000' : '101'),
                    '@listVersionID' => 'edition 2009',
                ],
                'TransferIdentifier' => $this->get('transfer_identifier'),
                'TransferReplyIdentifier' => $replyIdentifier,
                'ArchivalAgency' => [
                    'Identification' => $sa->get('identifier'),
                    'Name' => $sa->get('name'),
                ],
                'TransferringAgency' => [
                    'Identification' => $ta->get('identifier'),
                    'Name' => $ta->get('name'),
                ],
            ],
        ];

        if ($accepted) {
            $Archives = $loc->get('Archives');
            $archives = $Archives->find()
                ->innerJoinWith('Transfers')
                ->where(['Transfers.id' => $this->get('id')])
                ->contain(['DescriptionXmlArchiveFiles'])
                ->orderBy(['Archives.id'])
                ->all()
                ->map(
                    function (EntityInterface $v) {
                        /** @var DescriptionXmlArchiveFile $description */
                        $description = $v->get('description_xml_archive_file');
                        return Hash::get(
                            Xml::toArray($description->getDom()),
                            'Archive'
                        );
                    }
                )
                ->toArray();
            $return['ArchiveTransferReply']['Archive'] = count($archives) === 1
                ? $archives[0] : $archives;
        }

        return $return;
    }

    /**
     * Créé un ArchiveTransferReply pour du Seda 2.1
     * @param boolean $accepted
     * @return array
     * @throws Exception
     */
    private function createArchiveTransferReply21($accepted): array
    {
        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities');
        $Agreements = $loc->get('Agreements');
        $sa = $this->get('archival_agency')
            ?: $OrgEntities->get(
                $this->get('archival_agency_id')
            );
        $ta = $this->get('transferring_agency')
            ?: $OrgEntities->get(
                $this->get('transferring_agency_id')
            );
        $agreement_id = $this->get('agreement_id');
        $agreement = $this->get('agreement')
            ?: ($agreement_id ? $Agreements->get($agreement_id) : null);

        /** @var CountersTable $Counters */
        $Counters = $loc->get('Counters');
        $replyIdentifier = $Counters->next(
            $sa->get('id'),
            'ArchiveTransferReply'
        );

        /** @var DateTime $date */
        $date = $this->get('transfer_date');
        $return = [
            'ArchiveTransferReply' => [
                '@xmlns' => NAMESPACE_SEDA_21,

                // MessageType
                'Comment' => $this->get('transfer_comment'),
                'Date' => $date->format(DATE_RFC3339),
                'MessageIdentifier' => $this->get('transfer_identifier'),
            ],
        ];
        // BusinessMessageType
        if ($agreement) {
            $return['ArchiveTransferReply']['ArchivalAgreement'] = $agreement->get(
                'identifier'
            );
        }
        $return['ArchiveTransferReply']['CodeListVersions'] = '';

        if ($accepted) {
            $Archives = $loc->get('Archives');
            $archives = $Archives->find()
                ->innerJoinWith('Transfers')
                ->where(['Transfers.id' => $this->get('id')])
                ->contain(['DescriptionXmlArchiveFiles'])
                ->orderBy(['Archives.id'])
                ->all()
                ->map(
                    function (EntityInterface $v) {
                        /** @var DescriptionXmlArchiveFile $description */
                        $description = $v->get('description_xml_archive_file');
                        $util = new DOMUtility($description->getDom());
                        return Hash::get($util->toArray(true), 'Archive');
                    }
                )
                ->toArray();
            $return['ArchiveTransferReply']['DataObjectPackage'] = [
                'DataObjectGroup' => [],
                'BinaryDataObject' => [],
                'PhysicalDataObject' => [],
                'DescriptiveMetadata' => [],
                'ManagementMetadata' => [],
            ];
            $ref =& $return['ArchiveTransferReply']['DataObjectPackage'];

            $dataObjectGroupIds = [];
            $binaryDataObjectIds = [];
            foreach ($archives as $i => $archive) {
                $archive += [
                    'DataObjectGroup' => [],
                    'BinaryDataObject' => [],
                    'PhysicalDataObject' => [],
                    'DescriptiveMetadata' => [],
                    'ManagementMetadata' => [],
                ];

                // prépare le nœud DataObjectGroup à l'ajout d'archives
                if ($archive['DataObjectGroup'] && !isset($archive['DataObjectGroup'][0])) {
                    $archive['DataObjectGroup'] = [$archive['DataObjectGroup']];
                }
                // prépare le nœud BinaryDataObject à l'ajout d'archives
                if ($archive['BinaryDataObject'] && !isset($archive['BinaryDataObject'][0])) {
                    $archive['BinaryDataObject'] = [$archive['BinaryDataObject']];
                }
                // prépare le nœud ArchiveUnit à l'ajout d'archives (note : toujours 1 par archives)
                $archive['DescriptiveMetadata']['ArchiveUnit'] = [
                    $archive['DescriptiveMetadata']['ArchiveUnit'],
                ];

                // 1ère archive
                if ($i === 0) {
                    $ref = $archive;
                    foreach ($archive['DataObjectGroup'] as $dataObjectGroup) {
                        $dataObjectGroupIds[] = $dataObjectGroup['@id'];
                    }
                    foreach ($archive['BinaryDataObject'] as $binaryDataObject) {
                        $binaryDataObjectIds[] = $binaryDataObject['@id'];
                    }
                    continue;
                }

                // merge DataObjectGroup avec filtre sur id
                foreach ($archive['DataObjectGroup'] as $dataObjectGroup) {
                    if (
                        !in_array(
                            $dataObjectGroup['@id'],
                            $dataObjectGroupIds
                        )
                    ) {
                        $dataObjectGroupIds[] = $dataObjectGroup['@id'];
                        $ref['DataObjectGroup'][] = $dataObjectGroup;
                    }
                }
                // merge BinaryDataObject avec filtre sur id
                foreach ($archive['BinaryDataObject'] as $binaryDataObject) {
                    if (
                        !in_array(
                            $binaryDataObject['@id'],
                            $binaryDataObjectIds
                        )
                    ) {
                        $binaryDataObjectIds[] = $binaryDataObject['@id'];
                        $ref['BinaryDataObject'][] = $binaryDataObject;
                    }
                }
                // merge ArchiveUnit
                $ref['DescriptiveMetadata']['ArchiveUnit'][] = $archive['DescriptiveMetadata']['ArchiveUnit'][0];
            }
            // Rétabli la structure XML encodé en array si n = 1
            if (isset($ref['DataObjectGroup'][0]) && !isset($ref['DataObjectGroup'][1])) {
                $ref['DataObjectGroup'] = $ref['DataObjectGroup'][0];
            }
            if (isset($ref['BinaryDataObject'][0]) && !isset($ref['BinaryDataObject'][1])) {
                $ref['BinaryDataObject'] = $ref['BinaryDataObject'][0];
            }
            $ref = array_filter($ref); // retire les nœuds vides
            if (!isset($ref['ManagementMetadata'])) {
                $ref['ManagementMetadata'] = []; // noeud obligatoire
            }
        }

        $return['ArchiveTransferReply'] += [
            // BusinessReplyMessageType
            'ReplyCode' => $accepted ? '000' : '101',
            'MessageRequestIdentifier' => $replyIdentifier,

            // ArchiveTransferReplyType
            'GrantDate' => $date->format(DATE_RFC3339),
            'ArchivalAgency' => [
                'Identifier' => $sa->get('identifier'),
            ],
            'TransferringAgency' => [
                'Identifier' => $ta->get('identifier'),
            ],
        ];
        return $return;
    }

    /**
     * Créé un ArchiveTransferReply pour du Seda 2.2
     * @param boolean $accepted
     * @return array
     * @throws Exception
     */
    private function createArchiveTransferReply22($accepted): array
    {
        $data = $this->createArchiveTransferReply21($accepted);
        $data['ArchiveTransferReply']['@xmlns'] = NAMESPACE_SEDA_22;
        return $data;
    }

    /**
     * Renvoi le document (xml) lié au transfert avec les balises Integrity
     * complétés
     * @param Entity[]|Query $files
     * @param callable|null  $fn    callback($filename)
     * @return bool
     * @throws Exception
     */
    public function fillMissingHashes($files, ?callable $fn = null): bool
    {
        switch ($this->_fields['message_version']) {
            case 'seda0.2':
                return $this->fillMissingHashes02($files, $fn);
            case 'seda1.0':
                return $this->fillMissingHashes10($files, $fn);
            case 'seda2.1':
            case 'seda2.2':
                return $this->fillMissingHashes21($files, $fn);
            default:
                return true;
        }
    }

    /**
     * Renvoi le document (xml) lié au transfert avec les balises Integrity
     * complétés pour du seda v0.2
     * @param Entity[]|Query $files
     * @param callable|null  $fn
     * @return bool
     * @throws Exception
     */
    private function fillMissingHashes02($files, ?callable $fn = null)
    {
        $xml = $this->get('xml');
        $domutil = $this->getUtil();
        $xmlIntegrities = [];
        $availableAlgos = hash_algos();
        $loc = TableRegistry::getTableLocator();
        $randomOrgEntityId = $loc
            ->get('OrgEntities')
            ->find()
            ->select(['archival_agency_id' => 'ArchivalAgencies.id'])
            ->innerJoinWith('ArchivalAgencies')
            ->first()
            ->get('archival_agency_id');
        $defaultAlgo = Configure::read('hash_algo', 'sha256');
        /** @var DOMElement $Integrity */
        foreach (
            $domutil->xpath->query(
                '/ns:ArchiveTransfer/ns:Integrity'
            ) as $Integrity
        ) {
            $filename = $domutil->xpath->query('ns:UnitIdentifier', $Integrity)
                ->item(0)->nodeValue;
            /** @var DOMElement $contains */
            $contains = $domutil->xpath->query('ns:Contains', $Integrity)->item(
                0
            );
            $hash = $contains->nodeValue;
            $algo = $contains->getAttribute('algorithme');
            $xmlIntegrities[$filename] = [
                'hash' => $hash,
                'algo' => HashUtility::toPhpAlgo($algo),
                'element' => $contains,
            ];
        }
        $TransferAttachments = $loc->get('TransferAttachments');
        foreach ($files as $file) {
            $filename = Hash::get($file, 'filename');
            $transferAttachment = (array)$TransferAttachments->find()
                ->where(['transfer_id' => $this->id, 'filename' => $filename])
                ->disableHydration()
                ->first();
            if ($fn) {
                $fn($filename);
            }
            $hash = Hash::get($transferAttachment, 'hash');
            $algo = Hash::get($transferAttachment, 'hash_algo');

            if (isset($xmlIntegrities[$filename])) {
                // si la balise Integrity existe déjà

                if (
                    in_array(
                        $xmlIntegrities[$filename]['algo'],
                        $availableAlgos
                    )
                ) {
                    // si l'algo fait parti des algos pris en charge

                    if ($algo === $xmlIntegrities[$filename]['algo']) {
                        $trueHash = $hash
                            ?: hash_file(
                                $xmlIntegrities[$filename]['algo'],
                                $file->get('path')
                            );
                    } else {
                        $trueHash = hash_file(
                            $xmlIntegrities[$filename]['algo'],
                            $file->get('path')
                        );
                    }
                    if (
                        HashUtility::hashMatch(
                            $xmlIntegrities[$filename]['hash'],
                            $trueHash
                        )
                    ) {
                        // si le hash dans la balise correspond au hash réel
                        continue;
                    } else {
                        /** @var DOMElement $element */
                        $element = $xmlIntegrities[$filename]['element'];
                        DOMUtility::setValue($element, $trueHash);
                        $element->setAttribute(
                            'algorithme',
                            $algo ?: $defaultAlgo
                        );
                    }
                } else {
                    /** @var DOMElement $element */
                    $element = $xmlIntegrities[$filename]['element'];
                    DOMUtility::setValue(
                        $element,
                        $hash ?: hash_file($defaultAlgo, $file->get('path'))
                    );
                    $element->setAttribute('algorithme', $algo ?: $defaultAlgo);
                }
            } else {
                // Si Integrity n'existe pas

                // Si le fichier n'est pas présent dans le bordereau
                $binary = ArchiveBinariesTable::findBinaryByFilename($domutil, $filename);
                if (!$binary) {
                    continue;
                }
                if ($file->get('hash')) {
                    $hash = $file->get('hash');
                    $hash_algo = $file->get('hash_algo');
                } else {
                    $hash = $hash
                        ?: hash_file(
                            $defaultAlgo,
                            $file->get('path')
                        );
                    $hash_algo = $algo ?: $defaultAlgo;
                }
                $Integrity = AbstractGenericMessageForm::appendNode(
                    $randomOrgEntityId,
                    'Integrity',
                    'ArchiveTransfer',
                    $domutil->dom,
                    $domutil->dom->documentElement
                );
                $Contains = $domutil->createElement('Contains', $hash);
                $Contains->setAttribute('algorithme', $hash_algo);
                $UnitIdentifier = $domutil->createElement(
                    'UnitIdentifier',
                    $filename
                );
                $Integrity->appendChild($Contains);
                $Integrity->appendChild($UnitIdentifier);
            }
        }
        return $domutil->dom->save($xml);
    }

    /**
     * Donne le DOMUtility en cache
     * @return DOMUtility
     * @throws Exception
     */
    public function getUtil(): DOMUtility
    {
        if (!isset($this->util)) {
            $this->util = DOMUtility::load($this->get('xml'));
        }
        return $this->util;
    }

    /**
     * Ajoute un element en respectant l'ordre dans le schema
     * @param int|string   $orgEntityId
     * @param string       $append      tagName de l'element
     *                                  à ajouter
     * @param string       $path
     * @param \DOMDocument $doc
     * @param \DOMElement  $parentNode  element sur lequel ajouter le nouveau noeud
     * @return \DOMElement
     * @throws NotImplementedException|Exception
     */
    public function appendNode(
        $orgEntityId,
        string $append,
        string $path,
        \DOMDocument $doc,
        \DOMElement $parentNode
    ): \DOMElement {
        switch ($this->_fields['message_version']) {
            case 'seda0.2':
            case 'seda1.0':
            case 'seda2.1':
            case 'seda2.2':
                $node = AbstractGenericMessageForm::appendNode(
                    $orgEntityId,
                    $append,
                    $path,
                    $doc,
                    $parentNode
                );
                break;
            default:
                throw new NotImplementedException();
        }
        return $node;
    }

    /**
     * Renvoi le document (xml) lié au transfert avec les balises Integrity
     * complétés pour du seda v1.0
     * @param Entity[]|Query $files
     * @param callable|null  $fn
     * @return bool
     * @throws Exception
     */
    private function fillMissingHashes10($files, ?callable $fn = null)
    {
        $xml = $this->get('xml');
        $domutil = $this->getUtil();
        $xmlIntegrities = [];
        $availableAlgos = hash_algos();
        $loc = TableRegistry::getTableLocator();
        $randomOrgEntityId = $loc
            ->get('OrgEntities')
            ->find()
            ->select(['archival_agency_id' => 'ArchivalAgencies.id'])
            ->innerJoinWith('ArchivalAgencies')
            ->first()
            ->get('archival_agency_id');
        $defaultAlgo = Configure::read('hash_algo', 'sha256');
        foreach ($domutil->xpath->query('//ns:Document') as $Document) {
            /** @var DOMElement $Integrity */
            $Integrity = $domutil->xpath->query('ns:Integrity', $Document)
                ->item(0);
            if ($Integrity) {
                /** @var DOMElement $attachment */
                $attachment = $domutil->xpath->query('ns:Attachment', $Document)
                    ->item(0);
                $filename = $attachment->getAttribute('filename');
                $hash = $Integrity->nodeValue;
                $algo = $Integrity->getAttribute('algorithme');
                $xmlIntegrities[$filename] = [
                    'hash' => $hash,
                    'algo' => HashUtility::toPhpAlgo($algo),
                    'element' => $Integrity,
                ];
            }
        }
        $TransferAttachments = $loc->get('TransferAttachments');
        foreach ($files as $file) {
            $filename = Hash::get($file, 'filename');
            $transferAttachment = (array)$TransferAttachments->find()
                ->where(['transfer_id' => $this->id, 'filename' => $filename])
                ->disableHydration()
                ->first();
            if ($fn) {
                $fn($filename);
            }
            $hash = Hash::get($transferAttachment, 'hash');
            $algo = Hash::get($transferAttachment, 'hash_algo');

            if (isset($xmlIntegrities[$filename])) {
                // si la balise Integrity existe déjà

                if (
                    in_array(
                        $xmlIntegrities[$filename]['algo'],
                        $availableAlgos
                    )
                ) {
                    // si l'algo fait parti des algos pris en charge

                    $trueHash = $hash
                        ?: hash_file(
                            $xmlIntegrities[$filename]['algo'],
                            $file->get('path')
                        );
                    if (
                        HashUtility::hashMatch(
                            $trueHash,
                            $xmlIntegrities[$filename]['hash']
                        )
                    ) {
                        // si le hash dans la balise correspond au hash réel
                        continue;
                    } else {
                        /** @var DOMElement $element */
                        $element = $xmlIntegrities[$filename]['element'];
                        DOMUtility::setValue($element, $trueHash);
                        $element->setAttribute('algorithme', $algo ?: $defaultAlgo);
                    }
                } else {
                    /** @var DOMElement $element */
                    $element = $xmlIntegrities[$filename]['element'];
                    DOMUtility::setValue(
                        $element,
                        $hash ?: hash_file($defaultAlgo, $file->get('path'))
                    );
                    $element->setAttribute('algorithme', $algo ?: $defaultAlgo);
                }
            } else {
                // Si Integrity n'existe pas

                // Si le fichier n'est pas présent dans le bordereau
                $binary = ArchiveBinariesTable::findBinaryByFilename($domutil, $filename);
                if (!$binary) {
                    continue;
                }
                if ($file->get('hash')) {
                    $hash = $file->get('hash');
                    $hash_algo = $file->get('hash_algo');
                } else {
                    $hash = $hash
                        ?: hash_file(
                            $defaultAlgo,
                            $file->get('path')
                        );
                    $hash_algo = $algo ?: $defaultAlgo;
                }
                $path = explode(
                    '/',
                    ltrim(
                        $domutil->getElementXpath($binary, ''),
                        '/'
                    )
                );
                // "/ns:Transfer/ns:Archive/ns:Document" -> "Transfer.Archive.Document"
                $schemaPath = preg_replace(
                    '/\[\d+]/',
                    '',
                    implode('.', $path)
                );
                $Integrity = AbstractGenericMessageForm::appendNode(
                    $randomOrgEntityId,
                    'Integrity',
                    $schemaPath,
                    $domutil->dom,
                    $binary
                );
                DOMUtility::setValue($Integrity, $hash);
                $Integrity->setAttribute('algorithme', $hash_algo);
            }
        }
        return $domutil->dom->save($xml);
    }

    /**
     * Renvoi le document (xml) lié au transfert avec les balises Integrity
     * complétés pour du seda v2.1
     * @param Entity[]|Query $files
     * @param callable|null  $fn
     * @return bool
     * @throws Exception
     */
    private function fillMissingHashes21($files, ?callable $fn = null)
    {
        $xml = $this->get('xml');
        $domutil = $this->getUtil();
        $xmlIntegrities = [];
        $availableAlgos = hash_algos();
        $loc = TableRegistry::getTableLocator();
        $randomOrgEntityId = $loc
            ->get('OrgEntities')
            ->find()
            ->select(['archival_agency_id' => 'ArchivalAgencies.id'])
            ->innerJoinWith('ArchivalAgencies')
            ->first()
            ->get('archival_agency_id');
        $defaultAlgo = Configure::read('hash_algo', 'sha256');
        $binaries = $domutil->xpath->query('//ns:BinaryDataObject');
        /** @var DOMElement $BinaryDataObject */
        foreach ($binaries as $BinaryDataObject) {
            /** @var DOMElement $MessageDigest */
            $MessageDigest = $domutil->node('ns:MessageDigest', $BinaryDataObject);
            if ($MessageDigest) {
                $filename = ArchiveBinariesTable::getBinaryFilename($domutil, $BinaryDataObject);
                $hash = $MessageDigest->nodeValue;
                $algo = $MessageDigest->getAttribute('algorithm');
                $xmlIntegrities[$filename] = [
                    'hash' => $hash,
                    'algo' => HashUtility::toPhpAlgo($algo),
                    'element' => $MessageDigest,
                ];
            }
        }
        $TransferAttachments = $loc->get('TransferAttachments');
        foreach ($files as $file) {
            $filename = Hash::get($file, 'filename');
            $transferAttachment = (array)$TransferAttachments->find()
                ->where(['transfer_id' => $this->id, 'filename' => $filename])
                ->disableHydration()
                ->first();
            if ($fn) {
                $fn($filename);
            }
            $hash = Hash::get($transferAttachment, 'hash');
            $algo = Hash::get($transferAttachment, 'hash_algo');

            if (isset($xmlIntegrities[$filename])) {
                // si la balise Integrity existe déjà

                if (in_array($xmlIntegrities[$filename]['algo'], $availableAlgos)) {
                    // si l'algo fait parti des algos pris en charge
                    $trueHash = $hash
                        ?: hash_file(
                            $xmlIntegrities[$filename]['algo'],
                            $file->get('path')
                        );
                    if (HashUtility::hashMatch($trueHash, $xmlIntegrities[$filename]['hash'])) {
                        // si le hash dans la balise correspond au hash réel
                        continue;
                    } else {
                        /** @var DOMElement $element */
                        $element = $xmlIntegrities[$filename]['element'];
                        DOMUtility::setValue($element, $trueHash);
                        $element->setAttribute(
                            'algorithm',
                            $algo ?: $defaultAlgo
                        );
                    }
                } else {
                    /** @var DOMElement $element */
                    $element = $xmlIntegrities[$filename]['element'];
                    DOMUtility::setValue(
                        $element,
                        $hash ?: hash_file($defaultAlgo, $file->get('path'))
                    );
                    $element->setAttribute('algorithm', $algo ?: $defaultAlgo);
                }
            } else {
                // Si Integrity n'existe pas

                // Si le fichier n'est pas présent dans le bordereau
                $binary = ArchiveBinariesTable::findBinaryByFilename($domutil, $filename);
                if (!$binary) {
                    continue;
                }
                if ($file->get('hash')) {
                    $hash = $file->get('hash');
                    $hash_algo = $file->get('hash_algo');
                } else {
                    $hash = $hash
                        ?: hash_file(
                            $defaultAlgo,
                            $file->get('path')
                        );
                    $hash_algo = $algo ?: $defaultAlgo;
                }
                $path = explode(
                    '/',
                    ltrim(
                        $domutil->getElementXpath($binary, ''),
                        '/'
                    )
                );
                // "/ns:Transfer/ns:Archive/ns:Document" -> "Transfer.Archive.Document"
                $schemaPath = preg_replace(
                    '/\[\d+]/',
                    '',
                    implode('.', $path)
                );
                $MessageDigest = AbstractGenericMessageForm::appendNode(
                    $randomOrgEntityId,
                    'MessageDigest',
                    $schemaPath,
                    $domutil->dom,
                    $BinaryDataObject
                );
                DOMUtility::setValue($MessageDigest, $hash);
                $MessageDigest->setAttribute('algorithm', $hash_algo);
            }
        }
        return $domutil->dom->save($xml);
    }

    /**
     * Génère un fichier html à partir du xml et de la norme fournie
     *
     * @return string|bool Le HTML généré ou faux en cas d'erreur
     */
    protected function _getHTML()
    {
        $xsl = new DomDocument();
        if (!$xsl->load($this->_getXSL())) {
            return false;
        }

        $document = new DomDocument();
        $document->load($this->get('xml'));

        $namespace = $document->documentElement->getAttributeNode(
            'xmlns'
        )->nodeValue;
        if (in_array($namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            $filenames = $document->getElementsByTagName('Filename');
            /** @var DOMElement $filename */
            foreach ($filenames as $filename) {
                $filename->setAttribute(
                    'b64filename',
                    str_replace(
                        ['+', '/'],
                        ['-', '_'],
                        base64_encode($filename->nodeValue)
                    )
                );
                $filename->setAttribute(
                    'basename',
                    basename($filename->nodeValue)
                );
            }

            // ajout d'élements pour rendu de la DUA
            $appraisals = $document->getElementsByTagName('AppraisalRule');
            $AppraisalRuleCodes = TableRegistry::getTableLocator()->get(
                'AppraisalRuleCodes'
            );
            $rules = $AppraisalRuleCodes->find('list', keyField: 'code')
                ->disableHydration()
                ->toArray();
            $finals = [
                'Destroy' => __("Détruire"),
                'Keep' => __("Conserver"),
            ];
            /** @var DOMElement $appraisal */
            foreach ($appraisals as $appraisal) {
                /** @var DOMElement $rule */
                $rule = $appraisal->getElementsByTagName('Rule')->item(0);
                if ($rule && isset($rules[$rule->nodeValue])) {
                    $rule->setAttribute('trad', $rules[$rule->nodeValue]);
                }
                /** @var DOMElement $final */
                $final = $appraisal->getElementsByTagName('FinalAction')->item(
                    0
                );
                if ($final && isset($finals[$final->nodeValue])) {
                    $final->setAttribute('trad', $finals[$final->nodeValue]);
                }
            }
            // ajout d'élements pour rendu de la communicabilité
            $accesses = $document->getElementsByTagName('AccessRule');
            $AccessRuleCodes = TableRegistry::getTableLocator()->get(
                'AccessRuleCodes'
            );
            $rules = $AccessRuleCodes->find('list', keyField: 'code')
                ->disableHydration()
                ->toArray();
            /** @var DOMElement $access */
            foreach ($accesses as $access) {
                /** @var DOMElement $rule */
                $rule = $access->getElementsByTagName('Rule')->item(0);
                if ($rule && isset($rules[$rule->nodeValue])) {
                    $rule->setAttribute('trad', $rules[$rule->nodeValue]);
                }
            }
        } else {
            $attachments = $document->getElementsByTagName('Attachment');
            /** @var DOMElement $attachment */
            foreach ($attachments as $attachment) {
                $attachmentFileNameNode = $attachment->attributes->getNamedItem(
                    'filename'
                );
                if (empty($attachmentFileNameNode)) {
                    continue;
                }
                $attachment->setAttribute(
                    'b64filename',
                    str_replace(
                        ['+', '/'],
                        ['-', '_'],
                        base64_encode($attachmentFileNameNode->nodeValue)
                    )
                );
                $attachment->setAttribute(
                    'basename',
                    basename($attachmentFileNameNode->nodeValue)
                );
            }
        }

        $xslt = new XsltProcessor();
        $xslt->importStylesheet($xsl);
        $xslt->setParameter(
            '',
            'afficheDocument',
            '/transfer-attachments/downloadByName/' . $this->_fields['id'] . '/'
        );
        $xslt->setParameter(
            '',
            'files_exists',
            !$this->_fields['files_deleted']
        );

        return $xslt->transformToXML($document);
    }

    /**
     * Permet d'obtenir le chemin vers le fichier xsl selon la norme utilisée
     *
     * @return string|bool Faux si la norme n'est pas dans la liste
     */
    protected function _getXSL()
    {
        $norme = $this->_fields['message_version'];

        switch ($norme) {
            case 'seda0.2':
                return SEDA_V02_XSL;
            case 'seda1.0':
                return SEDA_V10_XSL;
            case 'seda2.0':
                return SEDA_V20_XSL;
            case 'seda2.1':
                return SEDA_V21_XSL;
            case 'seda2.2':
                return SEDA_V22_XSL;
        }

        return false;
    }

    /**
     * Traductions des normes
     *
     * @return string
     */
    protected function _getMessageVersiontrad(): string
    {
        if (empty($this->_fields['message_version'])) {
            return '';
        }
        switch ($this->_fields['message_version']) {
            case 'seda0.2':
                return __dx('transfer', 'message_version', "seda0.2");
            case 'seda1.0':
                return __dx('transfer', 'message_version', "seda1.0");
            case 'seda2.0':
                return __dx('transfer', 'message_version', "seda2.0");
            case 'seda2.1':
                return __dx('transfer', 'message_version', "seda2.1");
            case 'seda2.2':
                return __dx('transfer', 'message_version', "seda2.2");
        }
        return $this->_fields['message_version'];
    }

    /**
     * Traductions des états
     *
     * @return string
     */
    protected function _getStatetrad(): string
    {
        if (empty($this->_fields['state'])) {
            return '';
        }
        switch ($this->_fields['state']) {
            case TransfersTable::S_CREATING:
                return __dx('transfer', 'state', "creating");
            case TransfersTable::S_PREPARATING:
                return __dx('transfer', 'state', "preparating");
            case TransfersTable::S_TIMESTAMPING:
                return __dx('transfer', 'state', "timestamping");
            case TransfersTable::S_ANALYSING:
                return __dx('transfer', 'state', "analysing");
            case TransfersTable::S_CONTROLLING:
                return __dx('transfer', 'state', "controlling");
            case TransfersTable::S_VALIDATING:
                return __dx('transfer', 'state', "validating");
            case TransfersTable::S_ARCHIVING:
                return __dx('transfer', 'state', "archiving");
            case TransfersTable::S_REJECTED:
                return __dx('transfer', 'state', "rejected");
            case TransfersTable::S_ACCEPTED:
                return __dx('transfer', 'state', "accepted");
        }
        return $this->_fields['state'];
    }

    /**
     * Traductions des title d'états
     *
     * @return string
     */
    protected function _getStatetradTitle(): string
    {
        if (empty($this->_fields['state'])) {
            return '';
        }
        switch ($this->_fields['state']) {
            case TransfersTable::S_CREATING:
                return __dx(
                    'transfer',
                    'state',
                    "le transfert est en cours de création"
                );
            case TransfersTable::S_PREPARATING:
                return __dx(
                    'transfer',
                    'state',
                    "le transfert est en cours de préparation"
                );
            case TransfersTable::S_TIMESTAMPING:
                return __dx(
                    'transfer',
                    'state',
                    "les fichiers du transfert sont en cours d'horodatage"
                );
            case TransfersTable::S_ANALYSING:
                return __dx(
                    'transfer',
                    'state',
                    "les fichiers du transfert sont en cours d'analyse"
                );
            case TransfersTable::S_CONTROLLING:
                return __dx(
                    'transfer',
                    'state',
                    "le transfert est en cours de vérification"
                );
            case TransfersTable::S_VALIDATING:
                return __dx(
                    'transfer',
                    'state',
                    "le transfert est en cours de validation dans un workflow"
                );
            case TransfersTable::S_ARCHIVING:
                return __dx(
                    'transfer',
                    'state',
                    "la création de l'archive est en cours"
                );
            case TransfersTable::S_ACCEPTED:
                return __dx('transfer', 'state', "le transfert a été accepté");
            case TransfersTable::S_REJECTED:
                return __dx('transfer', 'state', "le transfert a été rejeté");
        }
        return $this->_fields['state'];
    }

    /**
     * Estimated time for the creation of the zip
     * @return int
     */
    public function zipCreationTimeEstimation(): int
    {
        $loc = TableRegistry::getTableLocator();
        $TransferAttachments = $loc->get('TransferAttachments');
        $query = $TransferAttachments->find();
        $attachments = $query->select(
            [
                'count' => $query->func()->count('*'),
                'size' => $query->func()->sum('size'),
            ]
        )
            ->where(['transfer_id' => $this->id])
            ->disableHydration()
            ->toArray()
        [0];

        // 6s pour 100Mo + 0.0002s par fichiers
        return (int)($attachments['size'] / 100000000 * 6
            + ($attachments['count'] * 0.0002));
    }

    /**
     * Valide un fichier selon le schema lié à cette entité
     * @param string $path
     * @return array
     * @throws Exception
     */
    public function validateFile(string $path = '')
    {
        if (empty($path)) {
            $path = $this->get('xml');
        }
        switch ($this->_fields['message_version']) {
            case 'seda2.2':
            case 'seda2.1':
            case 'seda1.0':
            case 'seda0.2':
                $errors = $this->getForm()->validateFile($path);
                break;
            default:
                $errors = [__("Norme/version non trouvé")];
        }
        return $errors;
    }

    /**
     * Donne le formulaire lié à cette entité
     * @param int|string|null  $orgEntityId
     * @param string           $path
     * @param DOMDocument|null $doc
     * @param \DOMElement|null $dom
     * @return MessageFormInterface|Seda02Form|Seda10Form
     */
    public function getForm(
        $orgEntityId = null,
        string $path = '',
        \DOMDocument $doc = null,
        \DOMElement $dom = null
    ): MessageFormInterface {
        return MessageForm::buildForm(
            $this->_fields['message_version'],
            $orgEntityId ?: $this->_fields['archival_agency_id'],
            $path,
            $doc,
            $dom
        );
    }

    /**
     * Complète le xml avec ce qui est calculable automatiquement
     * @param string $path
     * @throws Exception
     */
    public function autoRepair(string $path = '')
    {
        if (empty($path)) {
            $path = $this->get('xml');
        }
        switch ($this->_fields['message_version']) {
            case 'seda0.2':
                Seda02Form::autoRepair($this, $path);
                break;
            case 'seda1.0':
                Seda10Form::autoRepair($this, $path);
                break;
            case 'seda2.1':
                Seda21Form::autoRepair($this, $path);
                break;
            case 'seda2.2':
                Seda22Form::autoRepair($this, $path);
                break;
        }
    }

    /**
     * Donne la liste des fichiers utilisés dans le xml donné ($path)
     * par rapport à la norme (version seda) de l'entité
     * @param string $path
     * @return array
     */
    public function listFiles(string $path = ''): array
    {
        if (empty($path)) {
            $path = $this->get('xml');
        }
        if (!is_file($path)) {
            return [];
        }
        switch ($this->_fields['message_version']) {
            case 'seda0.2':
                return Seda02Form::listFiles($path);
            case 'seda1.0':
                return Seda10Form::listFiles($path);
            case 'seda2.1':
                return Seda21Form::listFiles($path);
            case 'seda2.2':
                return Seda22Form::listFiles($path);
            default:
                return [];
        }
    }

    /**
     * Champ virtuel pour ArchiveTransfer/Archive/AccessRestrictionRule/StartDate
     * @param string|DateTimeInterface $value
     * @return string
     * @throws Exception
     */
    protected function _setStartDate($value)
    {
        if (!$value || !$this->getDatetime($value)) {
            return null;
        }
        return $this->getDatetime($value)->format('Y-m-d');
    }

    /**
     * Champ virtuel pour ArchiveTransfer/Archive/AppraisalRule/StartDate
     * @param string|DateTimeInterface $value
     * @return string
     * @throws Exception
     */
    protected function _setFinalStartDate($value)
    {
        if (!$value || !$this->getDatetime($value)) {
            return null;
        }
        return $this->getDatetime($value)->format('Y-m-d');
    }

    /**
     * Génère le xml en fonction des données de l'entité et des fichiers
     * @return string
     */
    public function generateXml(): string
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $Profiles = TableRegistry::getTableLocator()->get('Profiles');
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $viewBuilder = new ViewBuilder();
        $messageVersion = $this->_fields['message_version'] ?? null;
        if ($messageVersion === 'seda2.1') {
            $viewBuilder->setTemplate('/Transfers/xml/seda_2.1');
            $mapping = [
                'fonds' => 'Fonds',
                'subfonds' => 'Subfonds',
                'class' => 'Class',
                'collection' => 'Collection',
                'series' => 'Series',
                'subseries' => 'Subseries',
                'recordgrp' => 'RecordGrp',
                'subgrp' => 'SubGrp',
                'file' => 'File',
                'item' => 'Item',
                'OtherLevel' => 'OtherLevel',
            ];
            $descriptionLevel = $this->get('description_level')
                ? $mapping[$this->get('description_level')] ?? null
                : null;
        } elseif ($messageVersion === 'seda2.2') {
            $viewBuilder->setTemplate('/Transfers/xml/seda_2.2');
            $mapping = [
                'fonds' => 'Fonds',
                'subfonds' => 'Subfonds',
                'class' => 'Class',
                'collection' => 'Collection',
                'series' => 'Series',
                'subseries' => 'Subseries',
                'recordgrp' => 'RecordGrp',
                'subgrp' => 'SubGrp',
                'file' => 'File',
                'item' => 'Item',
                'OtherLevel' => 'OtherLevel',
            ];
            $descriptionLevel = $this->get('description_level')
                ? $mapping[$this->get('description_level')] ?? null
                : null;
        } else {
            $viewBuilder->setTemplate('/Transfers/xml/seda_1.0');
            $descriptionLevel = $this->get('description_level');
        }
        $viewBuilder->setLayout('ajax');

        $archival_agency = $OrgEntities->find()
            ->select(['identifier', 'name', 'description'])
            ->where(['id' => $this->get('archival_agency_id')])
            ->firstOrFail();
        $archival_agency->set(
            'description',
            $this->getEscaped($archival_agency->get('description'), false)
        );
        $archival_agency->set(
            'identifier',
            $this->getEscaped($archival_agency->get('identifier'), false)
        );
        $archival_agency->set(
            'name',
            $this->getEscaped($archival_agency->get('name'), false)
        );

        $transferring_agency = $OrgEntities->find()
            ->select(['identifier', 'name', 'description'])
            ->where(['id' => $this->get('transferring_agency_id')])
            ->firstOrFail();
        $transferring_agency->set(
            'description',
            $this->getEscaped($transferring_agency->get('description'), false)
        );
        $transferring_agency->set(
            'identifier',
            $this->getEscaped($transferring_agency->get('identifier'), false)
        );
        $transferring_agency->set(
            'name',
            $this->getEscaped($transferring_agency->get('name'), false)
        );

        $originating_agency = $this->get('originating_agency_id')
            ? $OrgEntities->find()
                ->select(['identifier', 'name', 'description'])
                ->where(['id' => $this->get('originating_agency_id')])
                ->firstOrFail()
            : null;

        if ($originating_agency) {
            $originating_agency->set(
                'description',
                $this->getEscaped(
                    $originating_agency->get('description'),
                    false
                )
            );
            $originating_agency->set(
                'identifier',
                $this->getEscaped($originating_agency->get('identifier'), false)
            );
            $originating_agency->set(
                'name',
                $this->getEscaped($originating_agency->get('name'), false)
            );
        }

        $agreement = ($agreement_id = $this->get('agreement_id'))
            ? $Agreements->find()
                ->select(['identifier'])
                ->where(['id' => $agreement_id])
                ->firstOrFail()
                ->get('identifier')
            : null;
        $profile = ($profile_id = $this->get('profile_id'))
            ? $Profiles->find()
                ->select(['identifier'])
                ->where(['id' => $profile_id])
                ->firstOrFail()
                ->get('identifier')
            : null;
        $service_level = ($service_level_id = $this->get('service_level_id'))
            ? $ServiceLevels->find()
                ->select(['identifier'])
                ->where(['id' => $service_level_id])
                ->firstOrFail()
                ->get('identifier')
            : null;
        $viewBuilder->setVars(
            [
                'transfer_comment' => $this->getEscaped('transfer_comment'),
                'transfer_date' => $this->get('transfer_date')->format(
                    'Y-m-d\TH:i:s'
                ),
                'transfer_identifier' => $this->getEscaped(
                    'transfer_identifier'
                ),
                'name' => $this->getEscaped('name'),
                'description_level' => $descriptionLevel,
                'code' => $this->getEscaped('code'),
                'start_date' => $this->get('start_date'),
                'final' => $this->get('final'),
                'duration' => $this->get('duration'),
                'final_start_date' => $this->get('final_start_date'),
                'archival_agency' => $archival_agency,
                'transferring_agency' => $transferring_agency,
                'agreement' => $this->getEscaped($agreement, false),
                'profile' => $this->getEscaped($profile, false),
                'service_level' => $this->getEscaped($service_level, false),
                'originating_agency' => $originating_agency,
                'uuid' => Text::uuid(),
            ]
        );
        return $viewBuilder->build()->render();
    }

    /**
     * Donne une version protégée du champ demandé
     * @param string|null $str        Chemin Hash::get ou str
     *                                à échaper
     * @param bool        $isProperty si vrai, $str === chemin Hash::get
     * @return string
     */
    private function getEscaped(
        string $str = null,
        bool $isProperty = true
    ): string {
        return $str
            ? htmlspecialchars(
                DOMUtility::removeForbiddenXmlChars($isProperty ? Hash::get($this->_fields, $str) : $str),
                ENT_XML1
            )
            : '';
    }

    /**
     * Structure par upload de fichiers
     * Génère la structure de l'element Archive selon $files
     * @param Entity[]|Query $files
     * @return int|bool the number of bytes written or false if an error occurred.
     * @throws Exception
     */
    public function generateArchiveContain($files)
    {
        $i = 0;
        $count = $files instanceof Query ? $files->count() : count($files);
        $begin = microtime(true);
        $msg = __("Ajout des blocs de fichiers dans le bordereau");
        $fn = function (string $filename) use (
            $msg,
            &$i,
            $count,
            $begin
        ) {
            $i++;
            $remain = $count - $i;
            $sec = (int)(microtime(true) - $begin);
            $time = (new DateTime('@' . $sec));
            $perSec = $i / ($sec ?: 1);
            $remainTime = (new DateTime(
                '@' . (int)($remain / $perSec)
            ));
            Utility::get('Notify')->emit(
                'transfer_' . $this->id . '_create_tree',
                sprintf(
                    '%s - %d / %d - %s: %s (est: %s)',
                    $time->format('H:i:s'),
                    $i,
                    $count,
                    $msg,
                    h($filename),
                    $remainTime->format('H:i:s')
                )
            );
        };
        switch ($this->getUtil()->namespace) {
            case NAMESPACE_SEDA_10:
                return $this->generateArchiveContain10($files, $fn);
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                return $this->generateArchiveContain21($files, $fn);
            default:
                return false;
        }
    }

    /**
     * Génère la structure de l'element Archive selon $files
     * Utilisé pour générer un seda1.0
     *
     * NOTE: la validation du schema à lieu après, dans le precontrole
     * @param Entity[]|Query $files
     * @param callable|null  $fn
     * @return int|bool the number of bytes written or false if an error occurred.
     * @throws DOMException
     */
    private function generateArchiveContain10($files, $fn = null)
    {
        $util = $this->getUtil();
        $namespace = $util->namespace;
        $dom = $util->dom;
        $archive = $util->node('/ns:ArchiveTransfer/ns:Archive[1]');
        $puids = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('filetype')
        );
        $mimes = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('mime')
        );

        foreach ($files as $file) {
            /** @var DOMElement $parentNode */
            $parentNode = $archive;
            if (strpos($file->get('filename'), '/')) {
                $parentNode = $this->findOrCreateArchiveObject(
                    $file->get('filename'),
                    $parentNode
                );
            }
            if ($fn) {
                $fn($file->get('filename'));
            }
            $normalized = trim(preg_replace('/\s+/', ' ', basename($file->get('filename'))));
            $path = sprintf(
                './ns:Document/ns:Attachment[@filename=normalize-space(%s)]',
                DOMUtility::xpathQuote($normalized)
            );
            $query = $util->xpath->query($path, $parentNode);
            if ($query->length) {
                continue; // l'archive object existe déjà
            }
            $node = $dom->createElementNS($namespace, 'Document');
            $attachment = $dom->createElementNS($namespace, 'Attachment');
            $puid = $file->get('format');
            if (!empty($puid) && in_array($puid, $puids)) {
                $attachment->setAttribute('format', $puid);
            }
            $mime = $file->get('mime');
            if (in_array($mime, $mimes)) {
                $attachment->setAttribute('mimeCode', $mime);
            }
            $attachment->setAttribute('filename', $file->get('filename'));
            $integrity = $dom->createElementNS(
                $namespace,
                'Integrity'
            );
            $integrity->appendChild(DOMUtility::createDomTextNode($dom, $file->get('hash')));
            $integrity->setAttribute('algorithme', $file->get('hash_algo'));
            $size = $dom->createElementNS(
                $namespace,
                'Size'
            );
            $size->appendChild(DOMUtility::createDomTextNode($dom, $file->get('size')));
            $size->setAttribute('unitCode', 'Q12'); // Q12 = octet
            $type = $dom->createElementNS($namespace, 'Type', 'CDO');
            $node->appendChild($attachment);
            $node->appendChild($integrity);
            $node->appendChild($size);
            $node->appendChild($type);
            $parentNode->appendChild($node);
        }
        return $dom->save($this->get('xml'));
    }

    /**
     * Trouve ou créer le(s) ArchiveObject selon $filename
     * @param string     $filename
     * @param DOMElement $parentNode
     * @return DOMElement
     * @throws DOMException
     */
    private function findOrCreateArchiveObject(
        string $filename,
        DOMElement $parentNode
    ): DOMElement {
        $util = $this->getUtil();
        $namespace = $util->namespace;
        $dir = dirname($filename);
        foreach (explode('/', $dir) as $part) {
            $normalized = trim(preg_replace('/\s+/', ' ', $part));
            $path = sprintf(
                './ns:ArchiveObject/ns:Name[normalize-space(.)=%s]',
                DOMUtility::xpathQuote($normalized)
            );
            $query = $util->xpath->query($path, $parentNode);
            if ($query->length) {
                $parentNode = $query->item(0)->parentNode;
            } else {
                $node = $util->dom->createElementNS($namespace, 'ArchiveObject');
                $name = $util->dom->createElementNS($namespace, 'Name');
                $name->textContent = $part;
                $this->appendArchiveObjectToArchiveObject($node, $parentNode);
                $node->appendChild($name);
                $parentNode = $node;
            }
        }
        return $parentNode;
    }

    /**
     * Ajoute un ArchiveObject à l'interieur d'un autre ArchiveObject (à la bonne position)
     * @param DOMElement $node
     * @param DOMElement $parentNode
     * @return DOMNode
     */
    private function appendArchiveObjectToArchiveObject(
        DOMElement $node,
        DOMElement $parentNode
    ): DOMNode {
        /** @var DOMElement $node */
        foreach ($parentNode->childNodes as $childNode) {
            if ($childNode->tagName === 'Document') {
                return $parentNode->insertBefore($node, $childNode);
            }
        }
        return $parentNode->appendChild($node);
    }

    /**
     * Génère la structure de l'element Archive selon $files
     * Utilisé pour générer un seda2.1
     *
     * NOTE: la validation du schema à lieu après, dans le precontrole
     * @param Entity[]|Query $files
     * @param callable|null  $fn
     * @return int|bool the number of bytes written or false if an error occurred.
     * @throws Exception
     */
    private function generateArchiveContain21($files, $fn = null)
    {
        $util = $this->getUtil();
        $namespace = $util->namespace;
        $dom = $util->dom;
        $base = '/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata/ns:ArchiveUnit[1]';
        $archive = $util->xpath->query($base)->item(0);
        $puids = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('filetype')
        );
        $mimes = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('mime')
        );

        $dataObjectPackage = $archive->parentNode->parentNode;
        $descriptiveMetadata = $archive->parentNode;
        foreach ($files as $file) {
            /** @var DOMElement $parentNode */
            $parentNode = $archive;
            if (strpos($file->get('filename'), '/')) {
                $parentNode = $this->findOrCreateArchiveUnit(
                    dirname($file->get('filename')),
                    $parentNode
                );
            }
            if ($fn) {
                $fn($file->get('filename'));
            }
            $normalized = trim(preg_replace('/\s+/', ' ', basename($file->get('filename'))));
            $path = sprintf(
                './ns:ArchiveUnit/ns:Content/ns:Title[normalize-space(.)=%s]',
                DOMUtility::xpathQuote($normalized)
            );
            $query = $util->xpath->query($path, $parentNode);
            if ($query->length) {
                continue; // l'archive unit existe déjà
            }
            $archiveUnit = $dom->createElementNS($namespace, 'ArchiveUnit');
            $idArchiveUnit = 'UUID-' . Text::uuid();
            $archiveUnit->setAttribute('id', $idArchiveUnit);

            $content = $dom->createElementNS($namespace, 'Content');
            $title = $dom->createElementNS(
                $namespace,
                'Title'
            );
            $title->appendChild(DOMUtility::createDomTextNode($dom, $file->get('basename')));
            $content->appendChild($title);
            $archiveUnit->appendChild($content);

            $idArchivebinary = 'UUID-' . Text::uuid();
            $archiveBinary = $dom->createElementNS(
                $namespace,
                'BinaryDataObject'
            );
            $archiveBinary->setAttribute('id', $idArchivebinary);

            $attachment = $dom->createElementNS($namespace, 'Attachment');
            $attachment->setAttribute('filename', $file->get('filename'));
            $archiveBinary->appendChild($attachment);

            $messageDigest = $dom->createElementNS(
                $namespace,
                'MessageDigest'
            );
            $messageDigest->appendChild(DOMUtility::createDomTextNode($dom, $file->get('hash')));
            $messageDigest->setAttribute('algorithm', $file->get('hash_algo'));
            $archiveBinary->appendChild($messageDigest);

            if ($fileSize = $file->get('size')) {
                $size = $dom->createElementNS(
                    $namespace,
                    'Size'
                );
                $size->appendChild(DOMUtility::createDomTextNode($dom, $fileSize));
                $archiveBinary->appendChild($size);
            }

            $puid = $file->get('format');
            $formatIdentification = $dom->createElementNS(
                $namespace,
                'FormatIdentification'
            );
            $hasFormat = false;
            $mime = $file->get('mime');
            if (in_array($mime, $mimes)) {
                $hasFormat = true;
                $mimeType = $dom->createElementNS(
                    $namespace,
                    'MimeType'
                );
                $mimeType->appendChild(DOMUtility::createDomTextNode($dom, $mime));
                $formatIdentification->appendChild($mimeType);
            }
            if (!empty($puid) && in_array($puid, $puids)) {
                $hasFormat = true;
                $formatId = $dom->createElementNS(
                    $namespace,
                    'FormatId'
                );
                $formatId->appendChild(DOMUtility::createDomTextNode($dom, $puid));
                $formatIdentification->appendChild($formatId);
            }
            if ($hasFormat) {
                $archiveBinary->appendChild($formatIdentification);
            }

            $fileInfo = $dom->createElementNS($namespace, 'FileInfo');
            $filename = $dom->createElementNS(
                $namespace,
                'Filename'
            );
            $filename->appendChild(DOMUtility::createDomTextNode($dom, $file->get('filename')));
            $fileInfo->appendChild($filename);
            if (is_readable($file->get('path'))) {
                $mtime = (new DateTime(
                    '@' . filemtime($file->get('path'))
                ))->format(DATE_RFC3339);
                $lastModified = $dom->createElementNS(
                    $namespace,
                    'LastModified'
                );
                $lastModified->appendChild(DOMUtility::createDomTextNode($dom, $mtime));
                $fileInfo->appendChild($lastModified);
            }
            $archiveBinary->appendChild($fileInfo);

            $dataObjectReference = $dom->createElementNS(
                $namespace,
                'DataObjectReference'
            );
            $dataObjectReferenceId = $dom->createElementNS(
                $namespace,
                'DataObjectReferenceId'
            );
            $dataObjectReferenceId->appendChild(DOMUtility::createDomTextNode($dom, $idArchivebinary));
            $dataObjectReference->appendChild($dataObjectReferenceId);
            $archiveUnit->appendChild($dataObjectReference);

            $parentNode->appendChild($archiveUnit);

            $dataObjectPackage->insertBefore(
                $archiveBinary,
                $descriptiveMetadata
            );
        }
        return $dom->save($this->get('xml'));
    }

    /**
     * Trouve ou créer le(s) ArchiveUnit selon $filename
     * @param string     $filename
     * @param DOMElement $parentNode
     * @return DOMElement
     * @throws DOMException
     */
    private function findOrCreateArchiveUnit(
        string $filename,
        DOMElement $parentNode
    ): DOMElement {
        $util = $this->getUtil();
        $namespace = $util->namespace;
        foreach (explode('/', $filename) as $part) {
            $normalized = trim(preg_replace('/\s+/', ' ', $part));
            $path = sprintf(
                './ns:ArchiveUnit/ns:Content/ns:Title[normalize-space(.)=%s]',
                DOMUtility::xpathQuote($normalized)
            );
            $query = $util->xpath->query($path, $parentNode);
            if ($query->length) {
                $parentNode = $query->item(0)->parentNode->parentNode;
            } else {
                $node = $util->dom->createElementNS($namespace, 'ArchiveUnit');
                $node->setAttribute('id', 'UUID-' . Text::uuid());
                $content = $util->dom->createElementNS($namespace, 'Content');
                $title = $util->dom->createElementNS($namespace, 'Title');
                $title->textContent = $part;
                $content->appendChild($title);
                $parentNode->appendChild($node);
                $node->appendChild($content);
                $parentNode = $node;
            }
        }
        return $parentNode;
    }

    /**
     * Cette entité peut-elle être supprimée ?
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        try {
            $Archives = TableRegistry::getTableLocator()->get('Archives');
            $query = $Archives->find()
                ->innerJoinWith('Transfers')
                ->where(['Transfers.id' => $this->_fields['id']]);
            if ($query->count()) {
                return false;
            }

            $ValidationProcesses = TableRegistry::getTableLocator()->get('ValidationProcesses');
            $query = $ValidationProcesses->find()
                ->where(
                    [
                        'app_foreign_key' => $this->_fields['id'],
                        'app_subject_type' => 'Transfers',
                        'processed' => false,
                    ]
                );
            return $query->count() === 0;
        } catch (DatabaseException) {
            return true;
        }
    }

    /**
     * Cette entité peut-elle être supprimée ? (cas webservice en preparation)
     * @return bool
     */
    protected function _getForceDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        if ($this->get('state') !== TransfersTable::S_PREPARATING) {
            return false;
        }
        try {
            $Users = TableRegistry::getTableLocator()->get('Users');
            $user = $Users->find()
                ->where(['id' => $this->_fields['created_user_id']])
                ->firstOrFail();
            return $user->get('agent_type') === 'software';
        } catch (\Exception) {
            return true;
        }
    }

    /**
     * Chemin vers le bordereau temporaire (copie de l'originale pour edition)
     * @param int $user_id
     * @return string
     */
    public function getTempXml(int $user_id): string
    {
        $id = $this->_fields['id'];
        return rtrim(
            Configure::read('App.paths.data'),
            DS
        ) . DS . 'transfer_' . $id . '_' . $user_id . '_edit.xml';
    }

    /**
     * Chemin vers le dossier temporaire pour la création du zip
     * @return string
     */
    public function getZipTempFolder(): string
    {
        return rtrim(
            Configure::read('App.paths.download', TMP . 'download'),
            DS
        )
            . DS . 'transfer' . DS . $this->_fields['id'] . DS;
    }

    /**
     * Chemin vers les meta-donnés liés à l'edition du transfert
     * @param int $user_id
     * @return string
     */
    public function getSerializedMeta(int $user_id): string
    {
        if (empty($this->_fields['id'])) {
            return '';
        }
        $id = $this->_fields['id'];
        return rtrim(Configure::read('App.paths.data'), DS)
            . DS . 'transfer_' . $id . '_' . $user_id . '_edit.serialize';
    }

    /**
     * Chemin vers le log des modifications
     * @param int $user_id
     * @return string
     */
    public function getEditedLogs(int $user_id): string
    {
        if (empty($this->_fields['id'])) {
            return '';
        }
        $id = $this->_fields['id'];
        return rtrim(
            Configure::read('App.paths.data'),
            DS
        ) . DS . 'transfer_' . $id . '_' . $user_id . '_edit.log';
    }

    /**
     * Enregistre les modifications de l'entité dans le fichier xml
     * @param string|null $xmlPath par defaut = $this->xml
     * @return bool|int the number of bytes written or false if an error occurred.
     * @throws Exception
     */
    public function saveChangesAsXml(string $xmlPath = null)
    {
        if (!$xmlPath && empty($this->_fields['id'])) {
            return false;
        }
        if ($xmlPath === null) {
            $xmlPath = $this->get('xml');
        }
        $util = DOMUtility::load($xmlPath);
        $preControl = new PreControlMessages($xmlPath);
        $ns = $util->namespace;

        $date = $this->get('transfer_date');
        if (($date instanceof DateTimeInterface || $date instanceof CakeDate)) {
            $date = $date->format(DATE_RFC3339);
        }

        switch ($this->get('message_version')) {
            case 'seda0.2':
            case 'seda1.0':
            case 'seda2.1':
            case 'seda2.2':
                if (
                    $util->xpath->query($preControl->commentPaths[$ns])->count() === 0
                ) {
                    /** @var DOMElement $element */
                    $element = $util->xpath->query('/ns:ArchiveTransfer')->item(
                        0
                    );
                    $this->appendNode(
                        $this->get('archival_agency_id'),
                        'Comment',
                        'ArchiveTransfer',
                        $util->dom,
                        $element
                    );
                } else {
                    DOMUtility::setValue(
                        $util->xpath->query($preControl->commentPaths[$ns])
                            ->item(0),
                        trim($this->get('transfer_comment'))
                    );
                }
                DOMUtility::setValue(
                    $util->xpath->query($preControl->datePaths[$ns])->item(0),
                    $date
                );
                DOMUtility::setValue(
                    $util->xpath->query($preControl->identifierIdPaths[$ns])
                        ->item(0),
                    trim($this->get('transfer_identifier'))
                );
                break;
            default:
                throw new NotImplementedException();
        }
        return $util->dom->save($xmlPath);
    }

    /**
     * est considéré comme large si plus gros que 2,5Mo
     * @return bool
     */
    protected function _getIsLarge(): bool
    {
        $xmlFileSize = 0;
        if ($this->get('files_deleted') === true) {
            if (!$this->get('archive_file_id')) {
                return 0;
            }
            $StoredFiles = TableRegistry::getTableLocator()->get('StoredFiles');
            $storedFile = $StoredFiles->find()
                ->innerJoinWith('ArchiveFiles')
                ->where(['ArchiveFiles.id' => $this->get('archive_file_id')])
                ->firstOrFail();
            $xmlFileSize = $storedFile->get('size');
        } else {
            $xml = $this->get('xml');
            if ($xml && is_readable($xml)) {
                $xmlFileSize = filesize($xml);
            }
        }
        return $xmlFileSize > 2500000;
    }

    /**
     * Vrai si le transfert est éditable
     * @return bool
     */
    protected function _getEditable(): bool
    {
        return (empty($this->_fields['state']))
            || ($this->_fields['state'] === TransfersTable::S_PREPARATING)
            || ($this->_fields['state'] === TransfersTable::S_VALIDATING
                && (Hash::get($this, 'is_creator') || Hash::get($this, 'is_validator')));
    }

    /**
     * Permet d'obtenir l'archive au format xml issue d'un transfer
     * @param int           $archive_id
     * @param callable|null $fn         callback pour la mise
     *                                  à jour des
     *                                  metadonnées fichiers
     * @return string
     * @throws Exception
     */
    public function getArchiveXml($archive_id = null, callable $fn = null)
    {
        if (empty($this->_fields['id']) || empty($this->_fields['filename'])) {
            return '';
        }
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        if ($archive_id) {
            $archive = $Archives->get($archive_id);
        } else {
            $archive = $Archives->newEntity(
                [
                    'xml_node_tagname' => $this->get(
                        'message_version'
                    ) === 'seda0.2'
                        ? 'Contains[1]'
                        : 'Archive[1]',
                ]
            );
        }

        /**
         * Vérifi et récupère l'archive dans le transfert
         */
        $domTransfer = new DOMDocument();
        $domTransfer->load($this->get('xml'));

        /**
         * Complète les métadonnées de fichiers
         */
        $util = new DOMUtility($domTransfer);
        $TransferAttachments = TableRegistry::getTableLocator()->get(
            'TransferAttachments'
        );
        $query = $TransferAttachments->find()->where(
            ['transfer_id' => $this->get('id')]
        );
        $count = $query->count();

        /** @var EntityInterface $attachment */
        foreach ($query as $i => $attachment) {
            if ($fn) {
                $fn($attachment->get('filename'), $i + 1, $count);
            }
            $this->setFormatAndHash($attachment, $util);
        }

        /**
         * Vérifi et récupère l'archive dans le transfert
         */
        $originalArchiveNode = $util->xpath->query(
            $archive->get('xpath')
        )->item(0);
        if (!$originalArchiveNode instanceof DOMElement) {
            throw new Exception($archive->get('xpath') . ' was not found');
        }
        $version = $this->_fields['message_version'] ?? NAMESPACE_SEDA_10;
        $schema = MessageSchema::getSchemaClassname($version);

        /**
         * Importe l'Archive du transfert vers un nouveau document
         */
        if (in_array($util->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            return $this->getArchiveXml21(
                $domTransfer,
                $originalArchiveNode,
                $archive
            );
        }
        $domArchive = new DOMDocument();
        $archiveNode = $domArchive->createElementNS(
            $util->namespace,
            'Archive'
        );
        $domArchive->appendChild($archiveNode);
        foreach ($originalArchiveNode->childNodes as $child) {
            $archiveNode->appendChild($domArchive->importNode($child, true));
        }

        /**
         * Ajoute les identifiants du service d'archives
         */
        $query = $loc->get('ArchiveUnits')->find()
            ->where(['archive_id' => $archive_id])
            ->orderBy(['lft' => 'asc'])
            ->disableHydration();
        $util = new DOMUtility($domArchive);
        $paths = [];
        /** @var Seda02Schema|Seda10Schema|Seda21Schema|Seda22Schema $schema */
        $archiveTagname = $schema::getTagname('Archive', 'Archive');
        $archiveObjectTagname = $schema::getTagname('ArchiveObject', 'Archive');
        $DocumentTagname = $schema::getTagname('Document', 'Archive');
        $archiveIdentifier = $schema::getTagname(
            'ArchivalAgencyArchiveIdentifier',
            'Archive'
        );
        $archiveObjectIdentifier = $schema::getTagname(
            'ArchivalAgencyObjectIdentifier',
            'Archive'
        );
        $documentIdentifier = $schema::getTagname(
            'ArchivalAgencyDocumentIdentifier',
            'Archive'
        );
        foreach ($query as $unit) {
            if ($unit['parent_id']) {
                $id = $paths[$unit['parent_id']] . '/ns:' . $unit['xml_node_tagname'];
                $nodename = preg_replace(
                    '/\[\d+]$/',
                    '',
                    $unit['xml_node_tagname']
                );
            } else {
                $id = '/ns:' . $archiveTagname; // Corrige le pb seda0.2
                $nodename = $archiveTagname;
            }
            // on ne mémorise que les noeuds de type parent
            if ($unit['lft'] + 1 !== $unit['rght']) {
                $paths[$unit['id']] = $id;
            }
            $query = $util->xpath->query($id);
            if (!$query) {
                throw new Exception('xpath query failed: ' . $id);
            }
            $node = $query->item(0);
            if (!$node instanceof DOMElement) {
                throw new Exception(
                    'archive_unit ' . $id . ' not found in xml'
                );
            }
            // on cherche le noeud identifiant l'archive unit
            switch ($nodename) {
                case $archiveTagname:
                    $identifierNodeTagname = $archiveIdentifier;
                    break;
                case $archiveObjectTagname:
                    $identifierNodeTagname = $archiveObjectIdentifier;
                    break;
                case $DocumentTagname:
                    $identifierNodeTagname = $documentIdentifier;
                    break;
                default:
                    throw new Exception(
                        'unknown tagname: ' . $unit['xml_node_tagname']
                    );
            }
            if (!$identifierNodeTagname) {
                continue;
            }
            $identifier = $util->xpath->query(
                'ns:' . $identifierNodeTagname,
                $node
            );
            if ($identifier->count()) {
                DOMUtility::setValue(
                    $identifier->item(0),
                    $unit['archival_agency_identifier']
                );
                continue;
            }
            // le noeud identifiant n'existe pas, il faut l'ajouter
            $identifier = $domArchive->createElementNS(
                $util->namespace,
                $identifierNodeTagname,
                $unit['archival_agency_identifier']
            );
            $util->appendChild($node, $identifier);
        }

        // réindent
        $dom2 = new DOMDocument();
        $dom2->formatOutput = true;
        $dom2->preserveWhiteSpace = false;
        $dom2->loadXML($domArchive->saveXML());
        return $dom2->saveXML();
    }

    /**
     * Ajoute/modifie les métadonnées d'un attachment dans la description
     * @param EntityInterface $attachment
     * @param DOMUtility      $util
     * @throws DOMException
     */
    private function setFormatAndHash(
        EntityInterface $attachment,
        DOMUtility $util
    ) {
        if (!$attachment->get('xpath')) {
            return;
        }
        /** @var DOMElement $attachmentNode */
        $attachmentNode = $util->node($attachment->get('ns_xpath'));
        if (!$attachmentNode) {
            return;
        }
        $puids = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('filetype')
        );
        $mimes = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('mime')
        );
        switch ($util->namespace) {
            case NAMESPACE_SEDA_02:
                $this->setFormatAndHashSEDA02($attachment, $util, $puids, $mimes);
                break;
            case NAMESPACE_SEDA_10:
                $this->setFormatAndHashSEDA10($attachment, $util, $puids, $mimes);
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $this->setFormatAndHashSEDA21($attachment, $util, $puids, $mimes);
                break;
        }
    }

    /**
     * @param EntityInterface $attachment
     * @param DOMUtility      $util
     * @param array           $puids
     * @param array           $mimes
     * @return void
     */
    private function setFormatAndHashSEDA02(
        EntityInterface $attachment,
        DOMUtility $util,
        array $puids,
        array $mimes
    ): void {
        /** @var DOMElement $attachmentNode */
        $attachmentNode = $util->node($attachment->get('ns_xpath'));
        if ($attachmentNode->tagName === 'Document') {
            $attachmentNode = $util->node('ns:Attachment', $attachmentNode);
        }
        $puid = $attachment->get('format');
        $mime = $attachment->get('mime');
        /**
         * Format
         */
        if ($puid && in_array($puid, $puids)) {
            $attachmentNode->setAttribute('format', $puid);
        }
        if ($mime && in_array($mime, $mimes)) {
            $attachmentNode->setAttribute('mimeCode', $mime);
        }
    }

    /**
     * @param EntityInterface $attachment
     * @param DOMUtility      $util
     * @param array           $puids
     * @param array           $mimes
     * @return void
     * @throws DOMException
     */
    private function setFormatAndHashSEDA10(
        EntityInterface $attachment,
        DOMUtility $util,
        array $puids,
        array $mimes
    ): void {
        /** @var DOMElement $attachmentNode */
        $attachmentNode = $util->node($attachment->get('ns_xpath'));
        if ($attachmentNode->tagName === 'Document') {
            $attachmentNode = $util->node('ns:Attachment', $attachmentNode);
        }
        $documentNode = $attachmentNode->parentNode;
        $puid = $attachment->get('format');
        $mime = $attachment->get('mime');
        /**
         * Size
         */
        /** @var DOMElement $sizeNode */
        $sizeNode = $util->node('ns:Size', $documentNode);
        if (!$sizeNode) {
            $sizeNode = $util->appendChild(
                $documentNode,
                $util->createElement('Size', $attachment->get('size'))
            );
        }
        $sizeNode->setAttribute('unitCode', 'AD');
        $sizeNode->nodeValue = $attachment->get('size');

        /**
         * Integrity
         */
        $integrity = $util->node('ns:Integrity', $documentNode);
        if (!$integrity) {
            $integrity = $util->appendChild(
                $documentNode,
                $util->createElement(
                    'Integrity',
                    $attachment->get('hash')
                )
            );
            $integrity->setAttribute(
                'algorithme',
                $attachment->get('hash_algo')
            );
        }
        $algo = $integrity->getAttributeNode('algorithme');

        /**
         * Format
         */
        if ($puid && in_array($puid, $puids)) {
            $attachmentNode->setAttribute('format', $puid);
        }
        if ($mime && in_array($mime, $mimes)) {
            $attachmentNode->setAttribute('mimeCode', $mime);
        }
        $integrity->nodeValue = $attachment->get('hash');
        $algo->nodeValue = $attachment->get('hash_algo');
    }

    /**
     * @param EntityInterface $attachment
     * @param DOMUtility      $util
     * @param array           $puids
     * @param array           $mimes
     * @return void
     * @throws DOMException
     */
    private function setFormatAndHashSEDA21(
        EntityInterface $attachment,
        DOMUtility $util,
        array $puids,
        array $mimes
    ): void {
        /** @var PronomsTable $Pronoms */
        $Pronoms = TableRegistry::getTableLocator()->get('Pronoms');
        /** @var DOMElement $attachmentNode */
        $attachmentNode = $util->node($attachment->get('ns_xpath'));
        if ($attachmentNode->tagName === 'Attachment') { // legacy
            $documentNode = $attachmentNode->parentNode;
        } else {
            $documentNode = $attachmentNode;
        }
        $puid = $attachment->get('format');
        $mime = $attachment->get('mime');
        /**
         * Size
         */
        /** @var DOMElement $sizeNode */
        if ($attachmentSize = $attachment->get('size')) {
            $sizeNode = $util->node('ns:Size', $documentNode);
            if (!$sizeNode) {
                $sizeNode = $util->appendChild(
                    $documentNode,
                    $util->createElement('Size', $attachmentSize)
                );
            }
            $sizeNode->nodeValue = $attachmentSize;
        }

        /**
         * Integrity
         */
        $integrity = $util->node('ns:MessageDigest', $documentNode);
        if (!$integrity) {
            $integrity = $util->appendChild(
                $documentNode,
                $util->createElement(
                    'MessageDigest',
                    $attachment->get('hash')
                )
            );
            $integrity->setAttribute(
                'algorithm',
                $attachment->get('hash_algo')
            );
        }
        $algo = $integrity->getAttributeNode('algorithm');

        /**
         * Format
         */
        $formatIdentification = $util->node(
            'ns:FormatIdentification',
            $documentNode
        );
        if (!$formatIdentification && ($puid || $mime)) {
            $formatIdentification = $util->appendChild(
                $documentNode,
                $util->createElement('FormatIdentification')
            );
        }
        if ($puid && in_array($puid, $puids)) {
            if (!isset(self::$formatNames[$puid])) {
                self::$formatNames[$puid] = Hash::get(
                    $Pronoms->find()
                        ->select(['name'])
                        ->where(['puid' => $puid])
                        ->disableHydration()
                        ->first() ?: [],
                    'name',
                    ''
                );
            }
            if (self::$formatNames[$puid]) {
                $formatLitteral = $util->node(
                    'ns:FormatLitteral',
                    $formatIdentification
                );
                if (!$formatLitteral) {
                    $formatLitteral = $util->appendChild(
                        $formatIdentification,
                        $util->createElement(
                            'FormatLitteral',
                            self::$formatNames[$puid]
                        )
                    );
                }
                $formatLitteral->nodeValue = self::$formatNames[$puid];
            }
            $formatId = $util->node(
                'ns:FormatId',
                $formatIdentification
            );
            if (!$formatId) {
                $formatId = $util->appendChild(
                    $formatIdentification,
                    $util->createElement('FormatId', $mime)
                );
            }
            $formatId->nodeValue = $puid;
        }
        if ($mime && in_array($mime, $mimes)) {
            $mimeType = $util->node(
                'ns:MimeType',
                $formatIdentification
            );
            if (!$mimeType) {
                $mimeType = $util->appendChild(
                    $formatIdentification,
                    $util->createElement('MimeType', $mime)
                );
            }
            $mimeType->nodeValue = $mime;
        }
        $integrity->nodeValue = $attachment->get('hash');
        $algo->nodeValue = $attachment->get('hash_algo');
    }

    /**
     * Donne un xml de type Archive en seda2.1 à partir d'un transfert
     * @param DomDocument     $domTransfer
     * @param DOMElement      $originalArchiveNode
     * @param EntityInterface $archive
     * @return string
     * @throws Exception
     */
    private function getArchiveXml21(
        DOMDocument $domTransfer,
        DOMElement $originalArchiveNode,
        EntityInterface $archive
    ): string {
        $util = new DOMUtility($domTransfer);
        $domArchive = new DOMDocument();
        $archiveNode = $domArchive->createElementNS(
            $util->namespace,
            'Archive'
        );
        $domArchive->appendChild($archiveNode);
        $baseArchive = $util->xpath->query('ns:DataObjectPackage')->item(0);
        $refs = [];
        $q = $util->xpath->query(
            '//ns:DataObjectGroupReferenceId|//ns:DataObjectReferenceId',
            $originalArchiveNode
        );
        /** @var DOMElement $refId */
        foreach ($q as $refId) {
            $refs[] = trim($refId->nodeValue);
        }
        foreach ($baseArchive->childNodes as $child) {
            if (!$child instanceof DOMElement) {
                $archiveNode->appendChild(
                    $domArchive->importNode($child, true)
                );
                continue;
            }
            switch ($child->tagName) {
                case 'DataObjectGroup':
                case 'BinaryDataObject':
                    $this->importArchiveBinary($archiveNode, $child, $refs);
                    break;
                case 'DescriptiveMetadata':
                    $this->importArchiveUnit(
                        $archiveNode,
                        $child,
                        $originalArchiveNode
                    );
                    break;
                default:
                    $archiveNode->appendChild(
                        $domArchive->importNode($child, true)
                    );
            }
        }
        $this->appendArchiveUnitIdentifiers($domArchive, $archive);
        // réindent
        $dom2 = new DOMDocument();
        $dom2->formatOutput = true;
        $dom2->preserveWhiteSpace = false;
        $dom2->loadXML($domArchive->saveXML());
        return $dom2->saveXML();
    }

    /**
     * Limite l'import des binary consernés par l'archive
     * @param DOMElement $archiveNode
     * @param DOMElement $element
     * @param array      $refs
     */
    private function importArchiveBinary(
        DOMElement $archiveNode,
        DOMElement $element,
        array $refs
    ) {
        // reference sur ArchiveUnit à l'interieur d'un groupe
        if ($element->tagName === 'DataObjectGroup') {
            foreach ($element->childNodes as $child) {
                if ($child instanceof DOMElement) {
                    $refId = $element->getAttribute('id');
                    $childId = $child->getAttribute('id');
                    if (
                        ($refId && in_array($refId, $refs))
                        || ($childId && in_array($childId, $refs))
                    ) {
                        $archiveNode->appendChild(
                            $archiveNode->ownerDocument->importNode(
                                $element,
                                true
                            )
                        );
                        return;
                    }
                }
            }
        }

        // Reference sur ArchiveUnit
        $refId = $element->getAttribute('id');
        if ($refId && in_array($refId, $refs)) {
            $archiveNode->appendChild(
                $archiveNode->ownerDocument->importNode($element, true)
            );
            return;
        }

        $refGroupId = null;
        if ($element->tagName === 'DataObjectGroup') {
            // reference sur groupe (parent du ArchiveBinary (DataObjectGroup))
            $refGroupId = $element->getAttribute('id');
        } else {
            // reference sur groupe (DataObjectGroupId dans ArchiveBinary)
            foreach ($element->childNodes as $child) {
                if ($child instanceof DOMElement && $child->tagName === 'DataObjectGroupId') {
                    $refGroupId = $child->nodeValue;
                    break;
                }
            }
        }
        if ($refGroupId && in_array($refGroupId, $refs)) {
            $archiveNode->appendChild(
                $archiveNode->ownerDocument->importNode($element, true)
            );
        }
    }

    /**
     * Importe uniquement l'archive_unit conserné par l'archive (et tout ce qu'elle contient)
     * @param DOMElement $archiveNode
     * @param DOMElement $descriptiveMetadataNode
     * @param DOMElement $originalArchiveNode
     */
    private function importArchiveUnit(
        DOMElement $archiveNode,
        DOMElement $descriptiveMetadataNode,
        DOMElement $originalArchiveNode
    ) {
        $dom = $archiveNode->ownerDocument;
        $descriptiveMetadataNodeImported = $dom->importNode(
            $descriptiveMetadataNode
        );
        $archiveNode->appendChild($descriptiveMetadataNodeImported);
        foreach ($descriptiveMetadataNode->childNodes as $archiveUnitNode) {
            if (
                !$archiveUnitNode instanceof DOMElement
                || $archiveUnitNode->tagName !== 'ArchiveUnit'
                || $archiveUnitNode->getAttribute(
                    'id'
                ) === $originalArchiveNode->getAttribute('id')
            ) {
                $descriptiveMetadataNodeImported->appendChild(
                    $dom->importNode($archiveUnitNode, true)
                );
            }
        }
    }

    /**
     * Ajoute les identifiants du service d'archives
     * @param DomDocument     $domArchive
     * @param EntityInterface $archive
     * @throws Exception
     */
    private function appendArchiveUnitIdentifiers(
        DomDocument $domArchive,
        EntityInterface $archive
    ) {
        $util = new DOMUtility($domArchive);
        $loc = TableRegistry::getTableLocator();
        $query = $loc->get('ArchiveUnits')->find()
            ->where(['archive_id' => $archive->id])
            ->orderBy(['lft' => 'asc'])
            ->disableHydration();
        $paths = [];
        foreach ($query as $unit) {
            if ($unit['parent_id']) {
                $id = $paths[$unit['parent_id']] . '/ns:' . $unit['xml_node_tagname'];
            } else {
                $id = 'ns:DescriptiveMetadata/ns:ArchiveUnit';
            }
            // on ne mémorise que les noeuds de type parent
            if ($unit['lft'] + 1 !== $unit['rght']) {
                $paths[$unit['id']] = $id;
            }
            $query = $util->xpath->query($id);
            if (!$query) {
                throw new Exception('xpath query failed: ' . $id);
            }
            $node = $query->item(0);
            if (!$node instanceof DOMElement) {
                throw new Exception(
                    'archive_unit ' . $id . ' not found in xml'
                );
            }
            $content = $util->xpath->query('ns:Content', $node)->item(0);
            if (!$content) {
                continue;
            }
            $identifier = $util->xpath->query(
                'ns:ArchivalAgencyArchiveUnitIdentifier',
                $content
            );
            if ($identifier->count()) {
                DOMUtility::setValue(
                    $identifier->item(0),
                    $unit['archival_agency_identifier']
                );
                continue;
            }
            // le noeud identifiant n'existe pas, il faut l'ajouter
            $path = 'Archive.' . str_replace(
                ['ns:', '/'],
                ['', '.'],
                $id
            ) . '.Content';
            $path = preg_replace('/\[\d+]/', '', $path);
            $identifier = $this->appendNode(
                $this->get('archival_agency_id'),
                'ArchivalAgencyArchiveUnitIdentifier',
                $path,
                $domArchive,
                $content
            );
            DOMUtility::setValue(
                $identifier,
                $unit['archival_agency_identifier']
            );
        }
    }

    /**
     * Champ virtuel accusé de reception
     * @return string|null
     * @throws Exception
     */
    protected function _getAcknowledgementFilename()
    {
        if (
            empty($this->_fields['state'])
            || empty($this->_fields['message_version'])
            || empty($this->_fields['transfer_identifier'])
            || !in_array(
                $this->_fields['state'],
                ['validating', 'archiving', 'rejected', 'accepted']
            )
        ) {
            return null;
        }
        switch ($this->_fields['message_version']) {
            case 'seda0.2':
                $nodename = 'ArchiveTransferReply';
                break;
            case 'seda1.0':
            case 'seda2.1':
            case 'seda2.2':
                $nodename = 'Acknowledgement';
                break;
            default:
                return null;
        }
        return sprintf(
            '%s_%s.xml',
            $nodename,
            urlencode($this->_fields['transfer_identifier'])
        );
    }

    /**
     * Champ virtuel notification
     * @return string|null
     * @throws Exception
     */
    protected function _getNotifyFilename()
    {
        if (
            empty($this->_fields['state'])
            || empty($this->_fields['message_version'])
            || empty($this->_fields['transfer_identifier'])
            || !in_array(
                $this->_fields['state'],
                ['rejected', 'accepted']
            )
        ) {
            return null;
        }
        switch ($this->_fields['message_version']) {
            case 'seda0.2':
                $nodename = $this->_fields['state'] === 'accepted'
                    ? 'ArchiveTransferAcceptance'
                    : 'ArchiveTransferReply-rep-';
                break;
            case 'seda1.0':
            case 'seda2.1':
            case 'seda2.2':
                $nodename = 'ArchiveTransferReply';
                break;
            default:
                return null;
        }
        return sprintf(
            '%s_%s.xml',
            $nodename,
            urlencode($this->_fields['transfer_identifier'])
        );
    }

    /**
     * Donne le sha256 du transfert (pour signature)
     * @return string|null
     * @throws Exception
     */
    protected function _getHash()
    {
        $xml = $this->_getXml();
        if (!$xml || !is_file($xml)) {
            return null;
        }
        return hash_file('sha256', $xml);
    }

    /**
     * Donne l'uri du xml
     * @return string
     * @throws Exception
     */
    public function _getXml(): string
    {
        if (
            empty($this->_fields['id'])
            || empty($this->_fields['filename'])
            || !isset($this->_fields['files_deleted'])
        ) {
            return '';
        }
        if ($this->_fields['files_deleted'] === true) {
            if (!$this->get('archive_file_id')) {
                throw new RecordNotFoundException(
                    sprintf(
                        'Foreign key "%s" not set in table "%s" for record id %d',
                        "archive_file_id",
                        $this->getSource(),
                        $this->id
                    )
                );
            }
            $downloadFolder = $this->getDownloadFolder();
            $StoredFiles = TableRegistry::getTableLocator()->get('StoredFiles');
            $storedFile = $StoredFiles->find()
                ->innerJoinWith('ArchiveFiles')
                ->where(['ArchiveFiles.id' => $this->get('archive_file_id')])
                ->firstOrFail();
            $xmlUri = $downloadFolder . $this->_fields['filename'];
            Filesystem::dumpFile($xmlUri, $storedFile->get('file'));
            return $xmlUri;
        } else {
            return AbstractGenericMessageForm::getDataDirectory($this->_fields['id'])
                . DS . 'message' . DS . $this->_fields['filename'];
        }
    }

    /**
     * Chemin vers le dossier de téléchargement
     * @return string
     */
    public function getDownloadFolder(): string
    {
        return rtrim(
            Configure::read('App.paths.data'),
            DS
        ) . DS . 'download' . DS . 'transfer' . DS . $this->_fields['id'] . DS;
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'Transfers:' . $this->id
        );
        $object->originalName = $this->_fields['transfer_identifier'] ?? '';
        if ($comment = ($this->_fields['transfer_comment'] ?? '')) {
            $object->significantProperties['comment'] = $comment;
        }
        if ($date = ($this->_fields['created'] ?? '')) {
            $object->significantProperties['date'] = $date;
        }
        return $object;
    }

    /**
     * Donne le chemin du fichier pdf
     * @return string
     */
    protected function _getPdf(): string
    {
        if (
            empty($this->_fields['id'])
            || empty($this->_fields['filename'])
            || !isset($this->_fields['files_deleted'])
        ) {
            return '';
        }
        if ($this->_fields['files_deleted'] === true) {
            return $this->getDownloadFolder() . $this->_getPdfBasename();
        } else {
            return AbstractGenericMessageForm::getDataDirectory($this->_fields['id'])
                . DS . 'message' . DS . $this->_getPdfBasename();
        }
    }

    /**
     * Donne le nom du fichier pdf
     * @return string
     */
    protected function _getPdfBasename(): string
    {
        return basename(
            $this->_fields['filename'] ?? 'undefined',
            '.xml'
        ) . '.pdf';
    }

    /**
     * Construit un fichier zip à partir de l'entité
     * @return string
     * @throws Exception
     */
    public function makeZip(): string
    {
        Filesystem::begin();
        try {
            $files = $this->getTransferFiles();
            $baseDir = AbstractGenericMessageForm::getDataDirectory($this->id);
            $basePath = $baseDir . DS . 'attachments' . DS;
            $readme = self::createReadme($basePath);
            $files[] = $readme;

            $zipFilename = $this->_getZip();
            DataCompressor::compress($files, $zipFilename);

            Filesystem::commit();
            return $zipFilename;
        } catch (Exception $e) {
            Filesystem::rollback();
            throw $e;
        } finally {
            if (isset($readme)) {
                Filesystem::remove($readme);
            }
        }
    }

    /**
     * Get file names for a transfer
     * @return array
     */
    private function getTransferFiles(): array
    {
        $files = [];
        $baseDir = AbstractGenericMessageForm::getDataDirectory($this->get('id'));
        $basePath = $baseDir . DS . 'attachments';

        $TransferAttachments = TableRegistry::getTableLocator()->get(
            'TransferAttachments'
        );
        $query = $TransferAttachments->find()
            ->where(['TransferAttachments.transfer_id' => $this->id]);

        /** @var EntityInterface $attachment */
        foreach ($query as $attachment) {
            $file = $basePath . DS . $attachment->get('filename');
            if (!is_file($file)) {
                throw new NotFoundException(
                    'file ' . $file . ' does not exists in ' . __FUNCTION__
                );
            }
            $files[] = $file;
        }
        return $files;
    }

    /**
     * Donne le chemin de stockage du zip
     * @return string
     */
    public function _getZip(): string
    {
        return self::getZipDownloadFolder() . $this->id . '_transfer_files.zip';
    }

    /**
     * Chemin vers le dossier de téléchargement du zip
     * @return string
     */
    public static function getZipDownloadFolder(): string
    {
        return rtrim(
            Configure::read('App.paths.data'),
            DS
        ) . DS . 'download' . DS . 'transfer' . DS;
    }

    /**
     * data_size avec Mb/Gb etc...
     * @return string
     */
    protected function _getDataSizeReadable()
    {
        return Number::toReadableSize($this->_fields['data_size'] ?? 0);
    }

    /**
     * Conditions pour masquer ce transfert (équivalent à une suppression)
     * @return bool
     */
    protected function _getHiddable(): bool
    {
        return Hash::get($this->_fields, 'created_user.agent_type') === 'software'
            && ($this->_fields['state'] ?? '') === TransfersTable::S_REJECTED;
    }

    /**
     * Ajoute des informations qui dépendent de l'utilisateur connectés
     * @param EntityInterface $user
     * @return Transfer
     */
    public function appendSessionData(EntityInterface $user): self
    {
        $this->_fields['is_creator'] = $this->get('created_user_id') === $user->id;
        $this->_fields['is_validator'] = false;
        if ($this->get('state') === TransfersTable::S_VALIDATING) {
            $actors = Hash::extract($this, 'validation_processes.{n}.current_stage.validation_actors.{n}');
            /** @var EntityInterface $actor */
            foreach ($actors as $actor) {
                $type = Hash::get($actor, 'app_type');
                if (
                    ($type === 'USER' && Hash::get($actor, 'app_foreign_key') === $user->id)
                    || ($type === 'SERVICE_ARCHIVES' && $user->get('is_validator'))
                ) {
                    $this->_fields['is_validator'] = true;
                    break;
                }
            }
        }
        return $this;
    }

    /**
     * Si le transfert possède un lock
     * @return bool
     */
    protected function _getIsLocked(): bool
    {
        return !empty($this->_fields['transfer_lock']);
    }

    /**
     * Si le transfert possède un lock expiré
     * @return bool
     * @throws Exception
     */
    protected function _getHasExpiredLock(): bool
    {
        $lockedToken = Hash::get($this, 'transfer_lock');
        if ($lockedToken) {
            if (Hash::get($lockedToken, 'uploading')) {
                $timeout = new DateTime();
                $timeout->sub(new DateInterval(Configure::read('TransferLocks.timeout', 'PT1M')));
                return Hash::get($lockedToken, 'modified') < $timeout;
            }
            $pid = Hash::get($lockedToken, 'pid');
            return !$pid || !file_exists('/proc/' . $pid);
        }
        return false;
    }

    /**
     * Détruit le cache et le recontruit pour $this->getUtil()
     * @return DOMUtility cache reconstruit
     * @throws Exception
     */
    public function reloadUtil(): DOMUtility
    {
        unset($this->util);
        return $this->getUtil();
    }
}
