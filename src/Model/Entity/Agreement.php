<?php

/**
 * Asalae\Model\Entity\Agreement
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\Agreement as CoreAgreement;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\Utility\Premis;
use Cake\ORM\Association\HasMany;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table Agreements
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Agreement extends CoreAgreement implements PremisObjectEntityInterface
{
    protected array $_virtual = ['editableIdentifier'];

    /**
     * Indique si l'identifier de l'agreement est modifiable :
     *      false si il existe au moins un transfert pour lequel transfer.agreement_id = agreement.id
     *          et transfert.state != 'rejected'
     *      true dans le cas contraire
     * @return bool
     */
    protected function _getEditableIdentifier(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $rejectedTransfers = $Transfers->find()
            ->where(
                [
                    'agreement_id' => $this->_fields['id'],
                    'state IS NOT' => 'rejected',
                ]
            )
            ->count();

        return $rejectedTransfers === 0;
    }


    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'Agreements:' . $this->id
        );
        $object->originalName = $this->_fields['identifier'] ?? '';
        if ($name = ($this->_fields['name'] ?? '')) {
            $object->significantProperties['name'] = $name;
        }
        return $object;
    }

    /**
     * Cette entité peut-elle être supprimée ?
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $has = ['Archives', 'Transfers'];
        foreach ($has as $modelName) {
            /** @var HasMany $assoc */
            $assoc = $Agreements->{$modelName};
            $query = $assoc->find()
                ->where([$assoc->getForeignKey() => $this->_fields['id']]);
            if ($query->count() > 0) {
                return false;
            }
        }
        return true;
    }
}
