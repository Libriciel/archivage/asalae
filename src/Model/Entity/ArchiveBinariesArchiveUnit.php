<?php

/**
 * Asalae\Model\Entity\ArchiveBinariesArchiveUnit
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table archive_binaries_archive_units
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveBinariesArchiveUnit extends Entity
{
}
