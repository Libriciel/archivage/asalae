<?php

/**
 * Asalae\Model\Entity\Restitution
 */

namespace Asalae\Model\Entity;

use Asalae\Exception\GenericException;
use Asalae\Model\Table\RestitutionsTable;
use AsalaeCore\ORM\Entity;
use Cake\Core\Configure;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\View\ViewBuilder;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * Entité de la table restitutions
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Restitution extends Entity
{
    /**
     * @var array Champs virtuels
     */
    protected array $_virtual = ['statetrad', 'basepath', 'xml', 'basename', 'zip'];

    /**
     * Traductions des états
     * @return string
     */
    protected function _getStatetrad(): string
    {
        if (empty($this->_fields['state'])) {
            return '';
        }
        switch ($this->_fields['state']) {
            case RestitutionsTable::S_CREATING:
                return __dx('restitution', 'state', "en cours de création");
            case RestitutionsTable::S_AVAILABLE:
                return __dx(
                    'restitution',
                    'state',
                    "disponible pour le téléchargement"
                );
            case RestitutionsTable::S_DOWNLOADED:
                return __dx('restitution', 'state', "restitution téléchargée");
            case RestitutionsTable::S_ACQUITTED:
                return __dx('restitution', 'state', "restitution acquittée");
        }
        return $this->_fields['state'];
    }

    /**
     * Donne l'uri du xml
     * @return string
     */
    public function _getXml(): string
    {
        if (empty($this->_fields['id']) || empty($this->_fields['identifier'])) {
            return '';
        }
        return $this->_getBasepath()
            . DS . $this->_fields['identifier'] . '_restitution.xml';
    }

    /**
     * Donne le chemin de conservation des données d'une restitution
     * @return string
     */
    protected function _getBasepath(): string
    {
        if (empty($this->id)) {
            return '';
        }
        return Configure::read(
            'App.paths.data'
        ) . DS . 'restitutions' . DS . $this->id;
    }

    /**
     * Génère le fichier xml issue de l'entité
     * @return string|null Rendered content or null if content already rendered and returned earlier.
     * @throws Exception
     */
    public function generateXml(): ?string
    {
        $required = [
            'id',
            'identifier',
            'created',
            'restitution_request_id',
            'restitution_request.identifier',
            'archival_agency.identifier',
            'originating_agency.identifier',
        ];
        foreach ($required as $r) {
            if (Hash::get($this->_fields, $r) === null) {
                throw new GenericException(
                    'missing required data (' . $r . ')'
                );
            }
        }
        $queryArchiveUnits = TableRegistry::getTableLocator()->get(
            'ArchiveUnits'
        )->find()
            ->innerJoinWith('ArchiveUnitsRestitutionRequests')
            ->where(
                [
                    'ArchiveUnitsRestitutionRequests.restitution_request_id' => $this->get(
                        'restitution_request_id'
                    ),
                ]
            )
            ->orderBy(['ArchiveUnits.archival_agency_identifier'])
            ->disableHydration();
        $date = $this->_fields['created'];
        if (!($date instanceof DateTimeInterface || $date instanceof CakeDate)) {
            $date = new DateTime($date);
        }

        /** @var DateTime $date */
        $viewBuilder = new ViewBuilder();
        $viewBuilder->setTemplate('/Restitutions/xml/seda_2.1');
        $viewBuilder->setLayout('ajax');
        $viewBuilder->setVars(
            [
                'restitutionRequest' => $this->get('restitution_request'),
                'comment' => $this->getEscaped('comment'),
                'date' => $date->format('Y-m-d'),
                'datetime' => $date->format(DATE_RFC3339),
                'identifier' => $this->getEscaped('identifier'),
                'requestIdentifier' => $this->getEscaped(
                    'restitution_request.identifier'
                ),
                'queryArchiveUnits' => $queryArchiveUnits,
                'archivalAgency' => $this->getEscaped(
                    'archival_agency.identifier'
                ),
                'originatingAgency' => $this->getEscaped(
                    'originating_agency.identifier'
                ),
            ]
        );
        return $viewBuilder->build()->render();
    }

    /**
     * Donne une version protégé du champ demandé
     * @param string $path
     * @return string
     */
    private function getEscaped(string $path)
    {
        return htmlspecialchars(
            Hash::get($this->_fields, $path),
            ENT_XML1
        );
    }

    /**
     * Donne le chemin du fichier zip
     * @return string
     */
    protected function _getZip(): string
    {
        if (empty($this->_fields['id']) || empty($this->_fields['identifier'])) {
            return '';
        }
        return $this->_getBasepath() . DS . $this->_getBasename();
    }

    /**
     * Donne le nom de fichier sans son dossier relatif
     * @return string
     */
    protected function _getBasename(): string
    {
        if (empty($this->_fields['identifier'])) {
            return '';
        }
        return $this->_fields['identifier'] . '_restitution.zip';
    }

    /**
     * original_total_size avec Mb/Gb etc...
     * @return string
     */
    protected function _getOriginalSizeReadable()
    {
        return Number::toReadableSize($this->_fields['original_size'] ?? 0);
    }
}
