<?php

/**
 * Asalae\Model\Entity\TransferAttachment
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Controller\Component\ReaderComponent;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table transfer_attachments
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TransferAttachment extends Entity
{
    /**
     * @var array mémorise le listFiles d'un transfer
     *            (avec 20 transfers * 100 attachments on évite 1980 requetes)
     */
    public static $listFiles = [];
    /**
     * @var array champs virtuel
     */
    protected array $_virtual = ['deletable', 'openable', 'basename', 'url_basename', 'force_delete'];

    /**
     * Chemin vers le fichier sur le disque
     * @return null|string
     */
    protected function _getPath()
    {
        if (empty($this->_fields['transfer_id']) || empty($this->_fields['filename'])) {
            return null;
        }
        $basePath = AbstractGenericMessageForm::getDataDirectory(
            $this->_fields['transfer_id']
        ) . DS . 'attachments';
        return $basePath . DS . $this->_fields['filename'];
    }

    /**
     * La suppression de cette entité est-elle possible ?
     * @return bool
     */
    protected function _getDeletable()
    {
        if (
            ($this->_fields['force_delete'] ?? null) === true
            || empty($this->_fields['id'])
            || empty($this->_fields['transfer_id'])
            || empty($this->_fields['filename'])
        ) {
            return true;
        }
        if (empty(self::$listFiles[$this->_fields['transfer_id']])) {
            /** @var Transfer $transfer */
            $transfer = TableRegistry::getTableLocator()->get('Transfers')->get(
                $this->_fields['transfer_id']
            );
            self::$listFiles[$this->_fields['transfer_id']] = $transfer->listFiles();
        }
        return !in_array(
            $this->_fields['filename'],
            self::$listFiles[$this->_fields['transfer_id']]
        );
    }

    /**
     * Vrai si le fichier peut être lu par un reader
     * @return bool
     */
    protected function _getOpenable()
    {
        if (!empty($this->_fields['mime'])) {
            foreach (ReaderComponent::getReaders() as $classname) {
                if (
                    forward_static_call(
                        [$classname, 'canRead'],
                        $this->_fields['mime']
                    )
                ) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Donne le nom de fichier sans son dossier relatif
     * @return string
     */
    protected function _getBasename(): string
    {
        return basename($this->_fields['filename'] ?? '');
    }

    /**
     * Donne le nom de fichier sans son dossier relatif formatté pour une url
     * @return string
     */
    protected function _getUrlBasename(): string
    {
        return preg_replace('/%[\da-fA-F]{2}/', '', urlencode(basename($this->_fields['filename'] ?? '')));
    }

    /**
     * Donne le xpath avec namespace ns:
     * @return string|null
     */
    protected function _getNsXpath()
    {
        if (empty($this->_fields['xpath'])) {
            return null;
        }
        return preg_replace('/\//', '/ns:', $this->_fields['xpath']);
    }
}
