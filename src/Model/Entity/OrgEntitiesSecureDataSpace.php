<?php

/**
 * Asalae\Model\Entity\OrgEntitiesSecureDataSpace
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table org_entities_secure_data_spaces
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OrgEntitiesSecureDataSpace extends Entity
{
}
