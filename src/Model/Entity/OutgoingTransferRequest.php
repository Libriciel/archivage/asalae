<?php

/**
 * Asalae\Model\Entity\OutgoingTransferRequest
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Table\OutgoingTransferRequestsTable;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table outgoing_transfer_requests
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OutgoingTransferRequest extends Entity implements PremisObjectEntityInterface
{
    /**
     * Champs virtuels
     * @var array
     */
    protected array $_virtual = [
        'deletable',
        'editable',
        'sendable',
        'resendable',
        'statetrad',
    ];

    /**
     * Traductions des états
     * @return string
     */
    protected function _getStatetrad(): string
    {
        $state = $this->_fields['state'] ?? '';
        switch ($state) {
            case OutgoingTransferRequestsTable::S_CREATING:
                $state = __dx('outgoing-transfer-request', 'state', "création");
                break;
            case OutgoingTransferRequestsTable::S_VALIDATING:
                $state = __dx(
                    'outgoing-transfer-request',
                    'state',
                    "en cours de validation"
                );
                break;
            case OutgoingTransferRequestsTable::S_ACCEPTED:
                $state = __dx('outgoing-transfer-request', 'state', "acceptée");
                break;
            case OutgoingTransferRequestsTable::S_REJECTED:
                $state = __dx('outgoing-transfer-request', 'state', "rejetée");
                break;
            case OutgoingTransferRequestsTable::S_ARCHIVE_DESTRUCTION_SCHEDULED:
                $state = __dx(
                    'outgoing-transfer-request',
                    'state',
                    "élimination planifiée"
                );
                break;
            case OutgoingTransferRequestsTable::S_ARCHIVE_FILES_DESTROYING:
                $state = __dx(
                    'outgoing-transfer-request',
                    'state',
                    "fichiers détruits"
                );
                break;
            case OutgoingTransferRequestsTable::S_ARCHIVE_DESCRIPTION_DELETING:
                $state = __dx(
                    'outgoing-transfer-request',
                    'state',
                    "unités d'archives détruites"
                );
                break;
            case OutgoingTransferRequestsTable::S_TRANSFERS_SENT:
                $state = __dx(
                    'outgoing-transfer-request',
                    'state',
                    "transferts envoyés"
                );
                break;
            case OutgoingTransferRequestsTable::S_TRANSFERS_RECEIVED:
                $state = __dx(
                    'outgoing-transfer-request',
                    'state',
                    "transferts reçus"
                );
                break;
            case OutgoingTransferRequestsTable::S_TRANSFERS_PROCESSED:
                $state = __dx(
                    'outgoing-transfer-request',
                    'state',
                    "transferts traités"
                );
                break;
            case OutgoingTransferRequestsTable::S_FINISHED:
                $state = __dx(
                    'outgoing-transfer-request',
                    'state',
                    "transféré et archives supprimées"
                );
                break;
            case OutgoingTransferRequestsTable::S_ALL_TRANSFERS_REJECTED:
                $state = __dx(
                    'outgoing-transfer-request',
                    'state',
                    "tous les transferts ont été rejetés"
                );
                break;
            case OutgoingTransferRequestsTable::S_TRANSFERS_ERRORS:
                $state = __dx(
                    'outgoing-transfer-request',
                    'state',
                    "certains transferts n'ont pas pu être envoyés"
                );
                break;
            case OutgoingTransferRequestsTable::S_ALL_TRANSFERS_DELETED:
                $state = __dx(
                    'outgoing-transfer-request',
                    'state',
                    "tous les transferts ont été supprimés"
                );
                break;
        }
        return $state;
    }

    /**
     * Action edit
     * @return bool
     */
    protected function _getEditable()
    {
        if (empty($this->_fields['state'])) {
            return true;
        }
        return $this->_fields['state'] === 'creating';
    }

    /**
     * Action supprimer
     * @return bool
     */
    protected function _getDeletable()
    {
        if (empty($this->_fields['state'])) {
            return true;
        }
        return in_array($this->_fields['state'], ['creating', 'rejected']);
    }

    /**
     * Action envoyer
     * @return bool
     */
    protected function _getSendable()
    {
        if (empty($this->_fields['state'])) {
            return true;
        }
        return $this->_fields['state'] === 'creating'
            && TableRegistry::getTableLocator()->get(
                'ArchiveUnitsOutgoingTransferRequests'
            )
                ->exists(['otr_id' => $this->id]);
    }

    /**
     * Getter pour le select (remplissage via js du identifier + name)
     * @return string
     */
    protected function _getTransferringAgency(): string
    {
        return $this->_fields['transferring_agency_identifier'] ?? '';
    }

    /**
     * Action renvoyer
     * @return bool
     */
    protected function _getResendable(): bool
    {
        return ($this->_fields['state'] ?? '') === OutgoingTransferRequestsTable::S_TRANSFERS_ERRORS;
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'OutgoingTransferRequests:' . $this->id
        );
        $object->originalName = $this->_fields['identifier'] ?? '';
        return $object;
    }
}
