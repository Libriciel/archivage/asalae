<?php

/**
 * Asalae\Model\Entity\ArchiveBinary
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Controller\Component\ReaderComponent;
use AsalaeCore\DataType\JsonString;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Exception;

/**
 * Entité de la table archive_binarys
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveBinary extends Entity implements PremisObjectEntityInterface
{
    /**
     * @var array champs virtuel
     */
    protected array $_virtual = ['openable', 'basename', 'url_basename', 'typetrad'];

    /**
     * Traductions des app_type
     * @return string
     */
    protected function _getTypetrad(): string
    {
        switch ($this->_fields['type'] ?? '') {
            case '':
                return '';
            case 'original_data':
                return __dx('archive-binary', 'app_type', "original_data");
            case 'original_timestamp':
                return __dx('archive-binary', 'app_type', "original_timestamp");
            case 'preservation_data':
                return __dx('archive-binary', 'app_type', "preservation_data");
            case 'preservation_timestamp':
                return __dx(
                    'archive-binary',
                    'app_type',
                    "preservation_timestamp"
                );
            case 'dissemination_data':
                return __dx('archive-binary', 'app_type', "dissemination_data");
            case 'dissemination_timestamp':
                return __dx(
                    'archive-binary',
                    'app_type',
                    "dissemination_timestamp"
                );
        }
        return $this->_fields['app_type'];
    }

    /**
     * Lecture du fichier
     * @return bool success
     * @throws VolumeException
     * @throws Exception
     */
    public function readfile(): bool
    {
        $storedFile = $this->_fields['stored_file'] ?? null;
        if (!$storedFile instanceof StoredFile) {
            throw new Exception('stored file not found');
        }
        return $storedFile->readfile();
    }

    /**
     * Vrai si le fichier peut être lu par un reader
     * @return bool
     */
    protected function _getOpenable()
    {
        if (!empty($this->_fields['mime'])) {
            foreach (ReaderComponent::getReaders() as $classname) {
                if (
                    forward_static_call(
                        [$classname, 'canRead'],
                        $this->_fields['mime']
                    )
                ) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Donne le nom de fichier sans son dossier relatif
     * @return string
     */
    protected function _getBasename(): string
    {
        return basename($this->_fields['filename'] ?? '');
    }

    /**
     * Donne le nom de fichier sans son dossier relatif formatté pour une url
     * @return string
     */
    protected function _getUrlBasename(): string
    {
        return preg_replace('/%[\da-fA-F]{2}/', '', urlencode(basename($this->_fields['filename'] ?? '')));
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'ArchiveBinaries:' . $this->id
        );
        $object->originalName = $this->_fields['filename'] ?? '';
        return $object;
    }

    /**
     * Getter du champ data (json)
     * @return JsonString|mixed|null
     */
    protected function _getAppMeta()
    {
        $value = $this->_fields['app_meta'] ?? null;
        return is_string($value)
            ? JsonString::createFromString($value)
            : $value;
    }

    /**
     * Setter du champ data (json)
     * @param mixed $value
     * @return JsonString
     */
    protected function _setAppMeta($value)
    {
        return is_array($value)
            ? new JsonString($value)
            : $value;
    }
}
