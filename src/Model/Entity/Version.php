<?php

/**
 * Asalae\Model\Entity\Version
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\Model\Entity\Version as CoreVersion;
use AsalaeCore\Utility\Premis;

/**
 * Entité de la table versions
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Version extends CoreVersion implements PremisObjectEntityInterface
{
    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'Versions:' . $this->id
        );
        $object->originalName = sprintf(
            '%s v.%s',
            $this->_fields['subject'] ?? '',
            $this->_fields['version'] ?? ''
        );
        return $object;
    }
}
