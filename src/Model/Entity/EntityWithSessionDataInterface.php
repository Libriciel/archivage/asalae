<?php

/**
 * Asalae\Model\Entity\EntityWithSessionDataInterface
 */

namespace Asalae\Model\Entity;

use Cake\Datasource\EntityInterface;

/**
 * Interface d'une entité avec données de session
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface EntityWithSessionDataInterface
{
    /**
     * Ajoute des informations qui dépendent de l'utilisateur connectés
     * @param EntityInterface $user
     * @return $this
     */
    public function appendSessionData(EntityInterface $user): self;
}
