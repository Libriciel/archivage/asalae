<?php

/**
 * Asalae\Model\Entity\DestructionNotificationXpath
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table destruction_notification_xpaths
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DestructionNotificationXpath extends Entity
{
}
