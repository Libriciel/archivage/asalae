<?php

/**
 * Asalae\Model\Entity\ArchiveUnit
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Premis;
use Cake\Core\Configure;
use Cake\I18n\Number;
use AsalaeCore\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Datacompressor\Exception\DataCompressorException;
use Datacompressor\Utility\DataCompressor;
use DateTime;
use DOMDocument;
use DOMElement;
use DOMNode;
use DOMXPath;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use XSLTProcessor;

/**
 * Entité de la table archive_units
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveUnit extends Entity implements PremisObjectEntityInterface, ZippableEntityInterface
{
    /**
     * Traits
     */
    use ZippableEntityTrait;

    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = ['editable', 'dates'];

    /**
     * Estimated time for the creation of the zip
     * @return int
     */
    public function zipCreationTimeEstimation(): int
    {
        $originalSize = $this->get('original_total_size');
        $filesCount = $this->get('original_total_count');

        // récupération des valeurs manuelle si absentes de l'entité
        if ($originalSize === null || $filesCount === null) {
            $entity = TableRegistry::getTableLocator()->get('Archives')->find()
                ->select(['original_size', 'original_count'])
                ->where(['id' => $this->get('id')])
                ->firstOrFail();
            $originalSize = $this->_fields['original_total_size'] = $entity->get(
                'original_size'
            );
            $filesCount = $this->_fields['original_total_count'] = $entity->get(
                'original_count'
            );
        }

        // 6s pour 100Mo + 0.0002s par fichiers
        return (int)($originalSize / 100000000 * 6 + ($filesCount * 0.0002));
    }

    /**
     * permet d'obtenir le chemin xpath du noeud sous la forme:
     * /ns:ArchiveTransfer/ns:Archive[1]/ns:ArchiveObject[1]/ns:ArchiveObject[1]
     * @return string
     */
    protected function _getXpath(): string
    {
        if (
            !$this->id
            || empty($this->_fields['lft'])
            || empty($this->_fields['rght'])
            || empty($this->_fields['archive_id'])
        ) {
            return '';
        }
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $aus = $ArchiveUnits->find()
            ->select(['xml_node_tagname'])
            ->where(
                [
                    'archive_id' => $this->_fields['archive_id'],
                    'lft <=' => $this->_fields['lft'],
                    'rght >=' => $this->_fields['rght'],
                ]
            )
            ->orderBy(['lft' => 'asc'])
            ->all()
            ->toArray();
        $aux = Hash::extract($aus, '{n}.xml_node_tagname');
        $context = $this->_fields['context'] ?? 'Archive';
        if ($context === 'Archive') {
            $aux[0] = 'Archive';
        } elseif ($context === 'ArchiveTransfer') {
            array_unshift($aux, 'ArchiveTransfer');
        }
        $output = '/ns:' . implode('/ns:', $aux);

        // set archive et transfer pour obtenir la version seda de l'archive_unit
        if (
            Hash::get($this, 'archive.transfers.0.id') === null
            && Hash::get($this->_fields, 'archive_id')
        ) {
            if (Hash::get($this, 'archive') === null) {
                $Archives = TableRegistry::getTableLocator()->get('Archives');
                $this->_fields['archive'] = $Archives->find()
                    ->where(['Archives.id' => Hash::get($this->_fields, 'archive_id')])
                    ->contain(['Transfers'])
                    ->first();
            } else {
                $Transfers = TableRegistry::getTableLocator()->get('Transfers');
                $this->_fields['archive']['transfers'][0] = $Transfers->find()
                    ->innerJoinWith('Archives')
                    ->where(['Archives.id' => Hash::get($this, 'archive_id')])
                    ->first();
            }
        }
        $messageVersion = Hash::get($this, 'archive.transfers.0.message_version');
        if ($messageVersion === 'seda2.1' || $messageVersion === 'seda2.2') {
            if ($context === 'Archive') {
                $output = preg_replace(
                    '/^\/ns:Archive($|\/)/',
                    '/ns:Archive/ns:DescriptiveMetadata/ns:ArchiveUnit$1',
                    $output
                );
            } elseif ($context === 'ArchiveTransfer') {
                $output = preg_replace(
                    '/^\/ns:ArchiveTransfer($|\/)/',
                    '/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata$1',
                    $output
                );
            }
        }
        return $output;
    }

    /**
     * Transforme le xml de description d'archives en HTML par xslt
     * @return bool|string
     * @throws VolumeException
     */
    protected function _getPublicHTML()
    {
        $document = $this->getPublicDom();
        $xslt = $this->getXslt($document);
        if (!$xslt) {
            return false;
        }
        $xslt->setParameter('', 'afficheDocument', '#');
        $xslt->setParameter('', 'ratchetUrl', Configure::read('Ratchet.connect'));
        $xslt->setParameter('', 'files_exists', false);

        return $xslt->transformToXML($document);
    }

    /**
     * Donne le dom du archive_unit
     * @return DOMDocument
     * @throws VolumeException
     */
    public function getPublicDom(): DOMDocument
    {
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $archive = $Archives->find()
            ->where(['Archives.id' => $this->_fields['archive_id']])
            ->contain(['PublicDescriptionArchiveFiles' => ['StoredFiles']])
            ->firstOrFail();
        /** @var PublicDescriptionArchiveFile $publicDesc */
        $publicDesc = Hash::get($archive, 'public_description_archive_file');
        $archiveDom = $publicDesc->getDom();
        $node = $this->findNodeInDom($archiveDom);
        $archiveUnitDom = new DOMDocument();
        $archiveUnitDom->formatOutput = true;
        $archiveUnitDom->preserveWhiteSpace = false;
        $node = $archiveUnitDom->importNode($node, true);
        $archiveUnitDom->appendChild($node);
        $this->replaceReferences($archiveUnitDom, $archiveDom);
        return $archiveUnitDom;
    }

    /**
     * Donne le dom du archive_unit
     * @return DOMDocument
     * @throws VolumeException
     */
    public function getDom(): DOMDocument
    {
        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $archive = $Archives->find()
            ->where(['Archives.id' => $this->_fields['archive_id']])
            ->contain(['DescriptionXmlArchiveFiles' => ['StoredFiles']])
            ->firstOrFail();
        /** @var DescriptionXmlArchiveFile $desc */
        $desc = Hash::get($archive, 'description_xml_archive_file');
        $archiveDom = $desc->getDom();
        $node = $this->findNodeInDom($archiveDom);
        $archiveUnitDom = new DOMDocument();
        $archiveUnitDom->formatOutput = true;
        $archiveUnitDom->preserveWhiteSpace = false;
        $node = $archiveUnitDom->importNode($node, true);
        $archiveUnitDom->appendChild($node);
        $this->replaceReferences($archiveUnitDom, $archiveDom);
        return $archiveUnitDom;
    }

    /**
     * Donne le noeud lié à l'archive unit
     * @param DOMDocument $dom
     * @return DOMNode|DOMElement
     * @throws Exception
     */
    private function findNodeInDom(DOMDocument $dom): DOMNode
    {
        $util = new DOMUtility($dom);
        $nodeName = [
            '//ns:ArchivalAgencyArchiveIdentifier',
            '//ns:ArchivalAgencyObjectIdentifier',
            '//ns:ArchivalAgencyDocumentIdentifier',
            '//ns:Identification', // Document seda0.2
            '//ns:ArchivalAgencyArchiveUnitIdentifier', // seda2.1
        ];
        $path = sprintf(
            '%s[normalize-space(.)=%s]',
            '(' . implode('|', $nodeName) . ')',
            DOMUtility::normalizeSpace(
                $this->_fields['archival_agency_identifier'],
                true
            )
        );
        $query = $util->xpath->query($path);
        if ($query->count() === 0) {
            throw new Exception(
                "$path not found in archive {$this->_fields['archive_id']}"
            );
        }
        return $util->namespace === NAMESPACE_SEDA_21 || $util->namespace === NAMESPACE_SEDA_22
            ? $query->item(0)->parentNode->parentNode
            : $query->item(0)->parentNode;
    }

    /**
     * Importe les références fichiers pour le seda 2.1
     * @param DOMDocument $archiveUnitDom
     * @param DOMDocument $archiveDom
     */
    private function replaceReferences(
        DOMDocument $archiveUnitDom,
        DOMDocument $archiveDom
    ) {
        $utilAu = new DOMUtility($archiveUnitDom);
        if (!in_array($utilAu->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            return;
        }
        $util = new DOMUtility($archiveDom);

        $query = $utilAu->xpath->query('//ns:DataObjectReferenceId');
        /** @var DOMElement $element */
        foreach ($query as $element) {
            $id = trim($element->nodeValue);
            $path = sprintf(
                '//ns:BinaryDataObject[@id=%s]',
                $util->xpathQuote($id)
            );
            $node = $util->node($path);
            if ($node) {
                $newNode = $archiveUnitDom->importNode($node, true);
                $element->parentNode->appendChild($newNode);
            }
        }

        $query = $utilAu->xpath->query('//ns:DataObjectGroupReferenceId');
        /** @var DOMElement $element */
        foreach ($query as $element) {
            $id = trim($element->nodeValue);
            $path = sprintf(
                '//ns:DataObjectGroup[@id=%s]',
                $util->xpathQuote($id)
            );
            $node = $util->node($path);
            if ($node) {
                $newNode = $archiveUnitDom->importNode($node, true);
                $element->parentNode->appendChild($newNode);
            }
        }
    }

    /**
     * Donne le XsltProcessor à partir du document
     * @param DOMDocument $document
     * @return XSLTProcessor|null
     * @throws VolumeException
     */
    private function getXslt(DOMDocument $document): ?XsltProcessor
    {
        $xsl = new DOMDocument();
        if (!$xsl->load($this->_getXSL())) {
            return null;
        }

        $namespace = $document->documentElement->getAttributeNode('xmlns')->nodeValue;
        if (in_array($namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            $filenames = $document->getElementsByTagName('Filename');
            /** @var DOMElement $filename */
            foreach ($filenames as $filename) {
                $filename->setAttribute(
                    'b64filename',
                    str_replace(
                        ['+', '/'],
                        ['-', '_'],
                        base64_encode($filename->nodeValue)
                    )
                );
                $filename->setAttribute(
                    'basename',
                    basename($filename->nodeValue)
                );
            }

            // ajout d'élements pour rendu de la DUA
            $appraisals = $document->getElementsByTagName('AppraisalRule');
            $AppraisalRuleCodes = TableRegistry::getTableLocator()->get('AppraisalRuleCodes');
            $rules = $AppraisalRuleCodes->find('list', keyField: 'code')
                ->disableHydration()
                ->toArray();
            $finals = [
                'Destroy' => __("Détruire"),
                'Keep' => __("Conserver"),
            ];
            /** @var DOMElement $appraisal */
            foreach ($appraisals as $appraisal) {
                /** @var DOMElement $rule */
                $rule = $appraisal->getElementsByTagName('Rule')->item(0);
                if ($rule && isset($rules[$rule->nodeValue])) {
                    $rule->setAttribute('trad', $rules[$rule->nodeValue]);
                }
                /** @var DOMElement $final */
                $final = $appraisal->getElementsByTagName('FinalAction')->item(0);
                if ($final && isset($finals[$final->nodeValue])) {
                    $final->setAttribute('trad', $finals[$final->nodeValue]);
                }
            }
            // ajout d'élements pour rendu de la communicabilité
            $accesses = $document->getElementsByTagName('AccessRule');
            $AccessRuleCodes = TableRegistry::getTableLocator()->get('AccessRuleCodes');
            $rules = $AccessRuleCodes->find('list', keyField: 'code')
                ->disableHydration()
                ->toArray();
            /** @var DOMElement $access */
            foreach ($accesses as $access) {
                /** @var DOMElement $rule */
                $rule = $access->getElementsByTagName('Rule')->item(0);
                if ($rule && isset($rules[$rule->nodeValue])) {
                    $rule->setAttribute('trad', $rules[$rule->nodeValue]);
                }
            }
        } else {
            $attachments = $document->getElementsByTagName('Attachment');
            /** @var DOMElement $attachment */
            foreach ($attachments as $attachment) {
                $attachmentFileNameNode = $attachment->attributes->getNamedItem('filename');
                if (empty($attachmentFileNameNode)) {
                    continue;
                }
                $attachment->setAttribute(
                    'b64filename',
                    base64_encode($attachmentFileNameNode->nodeValue)
                );
                $attachment->setAttribute(
                    'basename',
                    basename($attachmentFileNameNode->nodeValue)
                );
            }
        }

        // ajout des informations liés aux AU supprimés
        if (isset($this->_fields['archive_id'])) {
            /** @var ArchiveFilesTable $ArchiveFiles */
            $ArchiveFiles = TableRegistry::getTableLocator()->get('ArchiveFiles');
            /** @var ArchiveFile $destructionXpaths */
            $destructionXpaths = $ArchiveFiles->find()
                ->where(['archive_id' => $this->_fields['archive_id']])
                ->andWhere(['type' => 'deleted'])
                ->contain(['StoredFiles'])
                ->first();
            if ($destructionXpaths) {
                /** @var StoredFile $storedFile */
                $storedFile = $destructionXpaths->get('stored_file');
                $json = json_decode($storedFile->get('file'), true);
                $xpath = new DOMXPath($document);
                $xpath->registerNamespace('ns', $namespace);

                foreach ($json as $xpathQuery) {
                    if (strpos($xpathQuery, '/ns:Archive/ns:DescriptiveMetadata/') === 0) {
                        $xpathQuery = substr($xpathQuery, strlen('/ns:Archive/ns:DescriptiveMetadata'));
                    }
                    $node = $xpath->query($xpathQuery)->item(0);
                    if ($node instanceof DOMElement) {
                        $node->setAttribute('deleted', true);
                    }
                }
            }
        }

        $xslt = new XsltProcessor();
        $xslt->importStylesheet($xsl);
        return $xslt;
    }

    /**
     * Permet d'obtenir le chemin vers le fichier xsl selon la norme utilisée
     *
     * @return string|bool Faux si la norme n'est pas dans la liste
     * @throws VolumeException
     */
    protected function _getXSL()
    {
        $dom = isset($this->_fields['archive_id']) ? $this->getDom() : null;
        $namespace = $dom?->documentElement->getAttributeNode('xmlns')->nodeValue;
        switch ($namespace) {
            case NAMESPACE_SEDA_02:
                return SEDA_ARCHIVE_V02_XSL;
            case NAMESPACE_SEDA_10:
                return SEDA_ARCHIVE_V10_XSL;
            case NAMESPACE_SEDA_21:
                return SEDA_ARCHIVE_V21_XSL;
            case NAMESPACE_SEDA_22:
                return SEDA_ARCHIVE_V22_XSL;
        }

        return false;
    }

    /**
     * Transforme le xml de description d'archives en HTML par xslt
     * @return bool|string
     * @throws VolumeException
     */
    protected function _getHTML()
    {
        $document = $this->getDom();
        $xslt = $this->getXslt($document);
        if (!$xslt) {
            return false;
        }
        $archive_id = isset($this->_fields['archive_id'])
            ? $this->_fields['archive_id'] . '/' : '';
        if (Hash::get($this->_fields, 'archive.state') !== 'available') {
            $xslt->setParameter('', 'afficheDocument', '#');
            $xslt->setParameter('', 'ratchetUrl', Configure::read('Ratchet.connect'));
            $xslt->setParameter('', 'files_exists', false);
        } else {
            $xslt->setParameter(
                '',
                'afficheDocument',
                '/archives/downloadByName/' . $archive_id
            );
            $xslt->setParameter('', 'ratchetUrl', Configure::read('Ratchet.connect'));
            $xslt->setParameter('', 'files_exists', true);
        }
        return $xslt->transformToXML($document);
    }

    /**
     * Donne la description (masqué si non communicable)
     * @return string|null
     */
    protected function _getDescription()
    {
        return $this->_getIsDescCommunicable()
            ? Hash::get($this->_fields, 'archive_description.description')
            : '********';
    }

    /**
     * Cette archive_unit est-elle communicable ?
     * @return bool
     */
    protected function _getIsDescCommunicable(): bool
    {
        return Hash::get($this->_fields, 'not_public')
            || Hash::get(
                $this->_fields,
                'archive_description.access_rule.end_date'
            )
            <= new DateTime();
    }

    /**
     * Donne les dates extrêmes (masqué si non communicable)
     * @return string|null
     */
    protected function _getDates()
    {
        return $this->_getIsDescCommunicable()
            ? Hash::get($this->_fields, 'archive_description.dates')
            : '********';
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'ArchiveUnits:' . $this->id
        );
        $object->originalName = $this->_fields['archival_agency_identifier'] ?? '';
        if ($name = ($this->_fields['name'] ?? '')) {
            $object->significantProperties['name'] = $name;
        }
        return $object;
    }

    /**
     * vrai si l'entité est éditable
     * @return bool
     */
    protected function _getEditable(): bool
    {
        if (
            ($this->_fields['state'] ?? '') === 'available'
            && strpos(
                ($this->_fields['xml_node_tagname'] ?? ''),
                'Document'
            ) === false
        ) {
            return true;
        }
        return false;
    }

    /**
     * Construit un fichier zip à partir de l'entité
     * @return string
     * @throws VolumeException
     * @throws DataCompressorException
     */
    public function makeZip(): string
    {
        $tempFolder = $this->getZipTempFolder();
        if (is_dir($tempFolder)) {
            Filesystem::remove($tempFolder);
        }
        $storedFiles = Hash::get($this, 'archive.description_xml_archive_file.stored_file');
        if (empty($storedFiles)) {
            $Archives = TableRegistry::getTableLocator()->get('Archives');
            $archive = $Archives->find()
                ->where(['Archives.id' => $this->get('archive_id')])
                ->contain(['LifecycleXmlArchiveFiles' => ['StoredFiles']])
                ->firstOrFail();
            $this->set('archive', $archive);
        }

        Filesystem::begin();
        try {
            $files = $this->getArchiveUnitFiles();
            $files[] = self::createReadme($tempFolder);

            $zipFilename = $this->get('zip');
            DataCompressor::compress($files, $zipFilename);

            Filesystem::commit();
            return $zipFilename;
        } catch (Exception $e) {
            Filesystem::rollback();
            throw $e;
        } finally {
            if (is_dir($tempFolder)) {
                Filesystem::remove($tempFolder);
            }
        }
    }

    /**
     * Chemin vers le dossier temporaire pour la création du zip
     * @return string
     */
    public function getZipTempFolder(): string
    {
        return rtrim(
            Configure::read('App.paths.download', TMP . 'download'),
            DS
        )
            . DS . 'archive_unit' . DS . $this->_fields['id'] . DS;
    }

    /**
     * Get file names for an archive, files are copied into temp folder
     * @return array
     * @throws VolumeException
     */
    private function getArchiveUnitFiles(): array
    {
        $tempFolder = $this->getZipTempFolder();
        $files = [];
        $secureDataSpaceId = Hash::get($this, 'archive.secure_data_space_id');
        /** @var VolumeManager $volumeManager */
        $volumeManager = new VolumeManager($secureDataSpaceId);

        $ArchiveBinaries = TableRegistry::getTableLocator()->get(
            'ArchiveBinaries'
        );
        $query = $ArchiveBinaries->find()
            ->innerJoinWith('ArchiveUnits')
            ->where(
                [
                    'ArchiveUnits.lft >=' => $this->get('lft'),
                    'ArchiveUnits.rght <=' => $this->get('rght'),
                    'ArchiveBinaries.type IN' => [
                        'original_data',
                        'original_timestamp',
                    ],
                ]
            )
            ->distinct(['ArchiveBinaries.filename'])
            ->contain(['StoredFiles']);
        Filesystem::addToDeleteIfRollback($tempFolder);
        foreach ($query as $archiveBinary) {
            $subdir = $archiveBinary->get('type');
            $dest = $tempFolder . $subdir . DS . $archiveBinary->get('filename');
            $volumeManager->fileDownload(
                Hash::get($archiveBinary, 'stored_file.name'),
                $dest
            );
            $files[] = $dest;
        }

        $ArchiveFiles = TableRegistry::getTableLocator()->get('ArchiveFiles');
        $archiveFiles = $ArchiveFiles->find()
            ->where(
                [
                    'archive_id' => $this->get('archive_id'),
                    'type IN' => [
                        ArchiveFilesTable::TYPE_LIFECYCLE,
                        ArchiveFilesTable::TYPE_DESCRIPTION,
                        ArchiveFilesTable::TYPE_TRANSFER,
                    ],
                ]
            )
            ->contain(['StoredFiles']);
        foreach ($archiveFiles as $archiveFile) {
            $dest = sprintf(
                '%smanagement_data/%d_%s.xml',
                $tempFolder,
                $this->id,
                $archiveFile->get('type')
            );
            $volumeManager->fileDownload(
                Hash::get($archiveFile, 'stored_file.name'),
                $dest
            );
            $files[] = $dest;
        }

        return $files;
    }

    /**
     * original_total_size avec Mb/Gb etc...
     * @return string
     */
    protected function _getOriginalTotalSizeReadable()
    {
        return Number::toReadableSize($this->_fields['original_total_size'] ?? 0);
    }

    /**
     * Donne le chemin de stockage du zip
     * @return string
     */
    public function _getZip(): string
    {
        return self::getZipDownloadFolder() . $this->id . '_archive_units.zip';
    }

    /**
     * Chemin vers le dossier de téléchargement du zip
     * @return string
     */
    public static function getZipDownloadFolder(): string
    {
        return Configure::read('App.paths.data')
            . DS . 'download' . DS . 'archive_unit' . DS;
    }
}
