<?php

/**
 * Asalae\Model\Entity\EventLog
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Table\EventLogsTable;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table event_logs
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EventLog extends Entity
{
    /**
     * Transforme en Premis\Event l'entité
     * @return Premis\Event|null
     */
    protected function _getPremisEvent()
    {
        if (empty($this->_fields['id'])) {
            return null;
        }
        /** @var EventLogsTable $EventLogs */
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $query = $EventLogs->getQueryExport(
            ['EventLogs.id' => $this->_fields['id']]
        );
        $event = $query->first();
        return $EventLogs->toPremisEvent($event);
    }
}
