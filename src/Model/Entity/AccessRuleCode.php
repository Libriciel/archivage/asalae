<?php

/**
 * Asalae\Model\Entity\AccessRuleCode
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;
use Cake\ORM\Association\HasMany;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table access_rule_codes
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AccessRuleCode extends Entity
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = [
        'deletable',
    ];

    /**
     * Cette entité peut-elle être supprimée ?
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        if (empty($this->_fields['org_entity_id'])) {
            return false;
        }
        $AccessRuleCodes = TableRegistry::getTableLocator()->get('AccessRuleCodes');
        $has = [
            'AccessRules',
        ];
        foreach ($has as $modelName) {
            /** @var HasMany $assoc */
            $assoc = $AccessRuleCodes->{$modelName};
            $query = $assoc->find()
                ->where([$assoc->getForeignKey() => $this->_fields['id']]);
            if ($query->count() > 0) {
                return false;
            }
        }
        return true;
    }
}
