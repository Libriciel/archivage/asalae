<?php

/**
 * Asalae\Model\Entity\Fileupload
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\Fileupload as CoreFileupload;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Entité de la table fileuploads
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Fileupload extends CoreFileupload
{
    /**
     * List of property names that should **not** be included in JSON or Array
     * representations of this Entity.
     *
     * @var array
     */
    protected array $_hidden = ['path', 'mediainfo'];

    /**
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if ($this->isNew()) {
            return true;
        }
        /** @var Fileupload $entity */
        $entity = TableRegistry::getTableLocator()->get('Fileuploads')->find()
            ->where(['Fileuploads.id' => $this->_fields['id']])
            ->contain(['Profiles'])
            ->first();
        if ($entity->get('profiles')) {
            return false;
        }
        return true;
    }

    /**
     * Ajoute un Fileupload au dossier webroot de l'entité
     * @param Configuration|EntityInterface $configuration
     * @throws Exception
     */
    public function copyInEntityDirectory(EntityInterface $configuration)
    {
        $dataDir = rtrim(Configure::read('App.paths.data'), DS);
        $publicDir = rtrim(
            Configure::read('App.paths.config', $dataDir . DS . 'config'),
            DS
        );
        $webrootDir = rtrim(
            Configure::read('OrgEntities.public_dir', WWW_ROOT),
            DS
        );
        $org_entity_id = $configuration->get('org_entity_id');

        // supprime le fichier existant avec son dossier (s'il est bien dans org-entity-data)
        $existing = $configuration->get('setting');
        if (
            $existing
            && strpos(
                $existing,
                'org-entity-data' . DS . $org_entity_id . DS
            ) === 1
        ) {
            if (is_file($dataDir . $existing)) { // legacy
                Filesystem::remove(dirname($dataDir . $existing));
            }
            if (is_file($publicDir . $existing)) {
                Filesystem::remove(dirname($publicDir . $existing));
            }
            if (is_file($webrootDir . $existing)) {
                Filesystem::remove(dirname($webrootDir . $existing));
            }
        }

        // Génère un nouveau répertoire unique
        do {
            $randdir = bin2hex(random_bytes(16));
            $setting = sprintf('/org-entity-data/%d/%s/%s', $org_entity_id, $randdir, $this->get('name'));
            $targetPath = $publicDir . $setting;
        } while (is_file($targetPath));

        // copy du fichier
        Filesystem::copy($this->get('path'), $targetPath);
        Filesystem::copy($this->get('path'), $webrootDir . $setting);

        // Patch de l'entité
        $configuration->set('setting', $setting);
    }
}
