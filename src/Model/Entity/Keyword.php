<?php

/**
 * Asalae\Model\Entity\Keyword
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\Keyword as CoreKeyword;

/**
 * Entité de la table keywords
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Keyword extends CoreKeyword
{
}
