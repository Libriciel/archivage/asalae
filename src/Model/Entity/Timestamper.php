<?php

/**
 * Asalae\Model\Entity\Timestamper
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\Model\Entity\Timestamper as CoreTimestamper;
use AsalaeCore\Utility\Premis;
use Cake\Database\Expression\IdentifierExpression;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table timestampers
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Timestamper extends CoreTimestamper implements PremisObjectEntityInterface
{
    /**
     * @var array Champs virtuels
     */
    protected array $_virtual = ['deletable'];

    /**
     * Vrai si le timestamper n'est pas utilisé dans un ServiceLevel (peut être lié à une entité)
     * @return bool
     */
    protected function _getRemovable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $query = $ServiceLevels->find()
            ->innerJoin(
                ['org_link' => 'org_entities_timestampers'],
                [
                    'OR' => [
                        'ts_msg_id' => $ident = new IdentifierExpression(
                            'org_link.id'
                        ),
                        'ts_pjs_id' => $ident,
                        'ts_conv_id' => $ident,
                    ],
                ]
            )
            ->where(
                [
                    'org_link.timestamper_id' => $this->_fields['id'],
                ]
            );
        return $query->count() === 0;
    }

    /**
     * Vrai si le timestamper n'est pas utilisé dans une entité
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $query = $ServiceLevels->find()
            ->innerJoin(
                ['org_link' => 'org_entities_timestampers'],
                [
                    'org_link.timestamper_id' => $this->_fields['id'],
                ]
            );
        if ($query->count()) {
            return false;
        }

        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $query = $OrgEntities->find()
            ->where(['timestamper_id' => $this->_fields['id']]);

        return $query->count() === 0;
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'Timestampers:' . $this->id
        );
        $object->originalName = $this->_fields['name'] ?? '';
        return $object;
    }
}
