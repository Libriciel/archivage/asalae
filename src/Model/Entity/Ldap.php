<?php

/**
 * Asalae\Model\Entity\Ldap
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\Ldap as CoreLdap;

/**
 * Entité de la table ldaps
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Ldap extends CoreLdap
{
}
