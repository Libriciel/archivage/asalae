<?php

/**
 * Asalae\Model\Entity\ServiceLevel
 */

namespace Asalae\Model\Entity;

use AsalaeCore\DataType\CommaArrayString;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Cake\ORM\Association\HasMany;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table ServiceLevels
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ServiceLevel extends Entity implements PremisObjectEntityInterface
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = [
        'deletable',
        'formatstrad',
        'convert_preservationtrad',
        'convert_disseminationtrad',
        'editableIdentifier',
    ];

    /**
     * Traductions des enable_conservation et enable_diffusion
     * @return string
     */
    protected function _getFormatstrad(): string
    {
        return $this->formatTrad($this->_fields['formats'] ?? '');
    }

    /**
     * Traduction du format
     * @param string $format
     * @return string
     */
    private function formatTrad(string $format): string
    {
        switch ($format) {
            case 'document_text':
                $format = __dx('service-level', 'enable', "Document");
                break;
            case 'document_pdf':
                $format = __dx('service-level', 'enable', "Document PDF");
                break;
            case 'document_calc':
                $format = __dx('service-level', 'enable', "Feuille de calcul");
                break;
            case 'document_presentation':
                $format = __dx('service-level', 'enable', "Présentation");
                break;
            case 'image':
                $format = __dx('service-level', 'enable', "Image");
                break;
            case 'audio':
                $format = __dx('service-level', 'enable', "Audio");
                break;
            case 'video':
                $format = __dx('service-level', 'enable', "Video");
                break;
        }
        return $format;
    }

    /**
     * Cette entité peut-elle être supprimée ?
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (!empty($this->_fields['default_level'])) {
            return false;
        }
        if ($this->isNew()) {
            return true;
        }
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $has = ['Archives', 'Transfers'];
        foreach ($has as $modelName) {
            /** @var HasMany $assoc */
            $assoc = $ServiceLevels->{$modelName};
            $query = $assoc->find()
                ->where([$assoc->getForeignKey() => $this->_fields['id']]);
            if ($query->count() > 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Défini la valeur du champ
     * @param string|array|CommaArrayString $value
     * @return CommaArrayString|null
     */
    protected function _setConvertPreservation($value)
    {
        if (empty($value)) {
            return null;
        } elseif (is_array($value)) {
            $output = array_unique($value);
            sort($output);
            return new CommaArrayString($output);
        }
        return $value;
    }

    /**
     * Défini la valeur du champ
     * @param string|array|CommaArrayString $value
     * @return string|null
     */
    protected function _setConvertDissemination($value)
    {
        if (empty($value)) {
            return null;
        } elseif (is_array($value)) {
            $output = array_unique($value);
            sort($output);
            return new CommaArrayString($output);
        }
        return $value;
    }

    /**
     * Donne la liste traduite
     * @return CommaArrayString
     */
    protected function _getConvertPreservationtrad()
    {
        $pres = array_map(
            [$this, 'formatTrad'],
            (array)$this->_getConvertPreservation()
        );
        return new CommaArrayString($pres);
    }

    /**
     * Getter du champ convert_preservation
     * @return CommaArrayString|null
     */
    protected function _getConvertPreservation()
    {
        if (empty($this->_fields['convert_preservation'])) {
            return null;
        } elseif (is_string($this->_fields['convert_preservation'])) {
            return CommaArrayString::createFromString(
                $this->_fields['convert_preservation']
            );
        } else {
            return $this->_fields['convert_preservation'];
        }
    }

    /**
     * Donne la liste traduite
     * @return CommaArrayString
     */
    protected function _getConvertDisseminationtrad()
    {
        $pres = array_map(
            [$this, 'formatTrad'],
            (array)$this->_getConvertDissemination()
        );
        return new CommaArrayString($pres);
    }

    /**
     * Getter du champ convert_preservation
     * @return CommaArrayString|null
     */
    protected function _getConvertDissemination()
    {
        if (empty($this->_fields['convert_dissemination'])) {
            return null;
        } elseif (is_string($this->_fields['convert_dissemination'])) {
            return CommaArrayString::createFromString(
                $this->_fields['convert_dissemination']
            );
        } else {
            return $this->_fields['convert_dissemination'];
        }
    }

    /**
     * Indique si l'identifier du niveau de service est modifiable :
     *      false si il existe au moins un transfert pour lequel transfer.service_level_id = service_level.id
     *          et transfert.state != 'rejected'
     *      true dans le cas contraire
     * @return bool
     */
    protected function _getEditableIdentifier(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $rejectedTransfers = $Transfers->find()
            ->where(
                [
                    'service_level_id' => $this->_fields['id'],
                    'state IS NOT' => 'rejected',
                ]
            )
            ->count();

        return $rejectedTransfers === 0;
    }


    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'ServiceLevels:' . $this->id
        );
        $object->originalName = $this->_fields['identifier'] ?? '';
        if ($name = ($this->_fields['name'] ?? '')) {
            $object->significantProperties['name'] = $name;
        }
        return $object;
    }
}
