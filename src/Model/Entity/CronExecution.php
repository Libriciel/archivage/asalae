<?php

/**
 * Asalae\Model\Entity\CronExecution
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\CronExecution as CoreCronExecution;

/**
 * Entité de la table cron_executions
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CronExecution extends CoreCronExecution
{
}
