<?php

/**
 * Asalae\Model\Entity\ConversionPolicy
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\AppMetaTrait;
use AsalaeCore\ORM\Entity;

/**
 * Entité de la table conversion_policys
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @deprecated logique déplacée dans ConfigurationsTable
 */
class ConversionPolicy extends Entity
{
    use AppMetaTrait;

    /**
     * @var string champ app_meta
     */
    protected $metaFieldName = 'params';

    /**
     * @var array Champs du app_meta
     */
    protected $_metaFields = ['codec', 'audio'];

    /**
     * Setter du champ params
     * @param mixed $value
     * @return string|null
     */
    protected function _setParams($value)
    {
        return $this->_setAppMeta($value);
    }
}
