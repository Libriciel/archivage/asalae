<?php

/**
 * Asalae\Model\Entity\Volume
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;

/**
 * Entité de la table volumes
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Volume extends Entity implements PremisObjectEntityInterface
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = [
        'drivertrad',
        'deletable',
        'is_used',
        'fields_object',
    ];

    /**
     * Traductions des états
     *
     * @return null|string
     */
    protected function _getDrivertrad()
    {
        if (empty($this->_fields['driver'])) {
            return null;
        }
        return Configure::read(
            'Volumes.drivers.' . $this->_fields['driver'] . '.name'
        );
    }

    /**
     * Cette entité peut-elle être supprimée ?
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return false;
        }
        return empty($this->_fields['secure_data_space_id']);
    }

    /**
     * Cette entité est-elle utilisé ?
     * @return bool
     */
    protected function _getIsUsed(): bool
    {
        if (empty($this->_fields['id'])) {
            return false;
        }
        if (empty($this->_fields['is_used'])) {
            $StoredFilesVolumes = TableRegistry::getTableLocator()->get(
                'StoredFilesVolumes'
            );
            $this->_fields['is_used'] = $StoredFilesVolumes->exists(
                ['volume_id' => $this->_fields['id']]
            );
        }
        return $this->_fields['is_used'];
    }

    /**
     * Cette entité peut-elle être retiré ?
     * @return bool
     */
    protected function _getRemovable(): bool
    {
        if (empty($this->_fields['id'])) {
            return false;
        }
        if (empty($this->_fields['removable'])) {
            $StoredFilesVolumes = TableRegistry::getTableLocator()->get(
                'StoredFilesVolumes'
            );
            $this->_fields['removable'] = !$StoredFilesVolumes->exists(
                ['volume_id' => $this->_fields['id']]
            );
        }
        return $this->_fields['removable'];
    }

    /**
     * Setter du champ dans json field
     * @param string $value
     * @return string|null
     */
    protected function _setEndpoint($value)
    {
        return $this->setJsonField('endpoint', $value) ? $value : null;
    }

    /**
     * Set une valeur dans le champs json
     * @param string $key
     * @param mixed  $value
     * @return false|string
     */
    public function setJsonField(string $key, $value)
    {
        $fields = json_decode($this->_fields['fields'] ?? '', true);
        if (!$fields) {
            $fields = [];
        }
        if (!isset($fields[$key]) || $fields[$key] !== $value) {
            $this->setDirty('fields');
        }
        $fields[$key] = $value;
        $this->_fields['fields'] = json_encode($fields, JSON_UNESCAPED_SLASHES);
        return $this->_fields['fields'];
    }

    /**
     * Setter du champ dans json field
     * @param string $value
     * @return mixed|null
     */
    protected function _setUseProxy($value)
    {
        $value = filter_var(
            $value,
            FILTER_VALIDATE_BOOLEAN,
            FILTER_NULL_ON_FAILURE
        );
        return $this->setJsonField(
            'use_proxy',
            $value
        ) ? $value : null;
    }

    /**
     * Setter du champ dans json field
     * @param string $value
     * @return string|null
     */
    protected function _setRegion($value)
    {
        return $this->setJsonField('region', $value) ? $value : null;
    }

    /**
     * Setter du champ dans json field
     * @param string $value
     * @return string|null
     */
    protected function _setBucket($value)
    {
        return $this->setJsonField('bucket', $value) ? $value : null;
    }

    /**
     * Setter du champ dans json field
     * @param string $value
     * @return string|null
     */
    protected function _setCredentialsKey($value)
    {
        return $this->setJsonField('credentials_key', $value) ? $value : null;
    }

    /**
     * Setter du champ dans json field
     * @param string $value
     * @return string|null
     */
    protected function _setCredentialsSecret($value)
    {
        $decryptKey = hash('sha256', VolumeManager::DECRYPT_KEY);
        $value = base64_encode(Security::encrypt($value, $decryptKey));
        return $this->setJsonField('credentials_secret', $value) ? $value
            : null;
    }

    /**
     * Setter du champ dans json field
     * @param string $value
     * @return string|null
     */
    protected function _setVerify($value)
    {
        return $this->setJsonField('verify', $value) ? $value : null;
    }

    /**
     * Setter du champ dans json field
     * @param string $value
     * @return string|null
     */
    protected function _setPath($value)
    {
        return $this->setJsonField('path', $value) ? $value : null;
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'Volumes:' . $this->id
        );
        $object->originalName = $this->_fields['name'] ?? '';
        if ($driver = ($this->_fields['driver'] ?? '')) {
            $object->significantProperties['driver'] = $driver;
        }
        $otherFields = $this->_getFieldsObject();
        if (!$otherFields) {
            return $object;
        }
        if ($path = ($otherFields->path ?? '')) {
            $object->significantProperties['path'] = $path;
        }
        if ($endpoint = ($otherFields->endpoint ?? '')) {
            $object->significantProperties['endpoint'] = $endpoint;
        }
        if ($bucket = ($otherFields->bucket ?? '')) {
            $object->significantProperties['bucket'] = $bucket;
        }
        return $object;
    }

    /**
     * Transforme le champ json en array
     * @return mixed|null
     */
    protected function _getFieldsObject()
    {
        if (empty($this->_fields['fields']) || empty($this->_fields['driver'])) {
            return null;
        }
        if (is_array($this->_fields['fields'])) {
            return $this->_fields['fields'];
        }
        $fields = json_decode($this->_fields['fields']);
        $configured = Configure::read(
            'Volumes.drivers.' . $this->_fields['driver'] . '.fields',
            []
        );
        foreach ($configured as $field => $params) {
            if (isset($params['type']) && $params['type'] === 'password') {
                unset($fields->$field);
            }
        }
        return $fields;
    }
}
