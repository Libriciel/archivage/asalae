<?php

/**
 * Asalae\Model\Entity\ArchiveUnitsRestitutionRequest
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;

/**
 * Entité de la table archive_units_restitution_request
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveUnitsRestitutionRequest extends Entity implements PremisObjectEntityInterface
{
    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'ArchiveUnitsRestitutionRequests:' . $this->id
        );
        $object->originalName = $this->_fields['name'] ?? '';
        return $object;
    }
}
