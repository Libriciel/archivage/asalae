<?php

/**
 * Asalae\Model\Entity\SecureDataSpace
 */

namespace Asalae\Model\Entity;

use Asalae\Cron\ChangeVolumesInSpace;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Cake\ORM\Association\HasMany;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table secure_data_spaces
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SecureDataSpace extends Entity implements PremisObjectEntityInterface
{
    /**
     * @var array Champs virtuels
     */
    protected array $_virtual = ['deletable', 'volumesEditable'];

    /**
     * Vrai si l'espace de conservation sécurisé n'est pas utilisé dans un ServiceLevel (peut être lié à une entité)
     * @return bool
     */
    protected function _getRemovable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $query = $ServiceLevels->find()
            ->where(
                [
                    'ServiceLevels.secure_data_space_id' => $this->_fields['id'],
                ]
            );
        return $query->count() === 0;
    }

    /**
     * Vrai si l'espace de conservation sécurisé n'est pas utilisé dans une entité
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        if ($this->_fields['is_default']) {
            return false;
        }
        $SecureDataSpaces = TableRegistry::getTableLocator()->get(
            'SecureDataSpaces'
        );
        $has = [
            'Archives',
            'ServiceLevels',
            'StoredFiles',
            'Volumes',
        ];
        foreach ($has as $modelName) {
            /** @var HasMany $assoc */
            $assoc = $SecureDataSpaces->{$modelName};
            $query = $assoc->find()
                ->where([$assoc->getForeignKey() => $this->_fields['id']]);
            if ($query->count() > 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Détermine si on peut changer les volumes d'un ECS (impossible si un cron est déjà programmé)
     * @return bool
     */
    public function _getVolumesEditable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $Crons = TableRegistry::getTableLocator()->get('Crons');
        return !$Crons->exists(
            [
                'classname' => ChangeVolumesInSpace::class,
                'app_meta LIKE' => '%"space_id":"' . $this->_fields['id'] . '"%',
            ]
        );
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'SecureDataSpaces:' . $this->id
        );
        $object->originalName = $this->_fields['name'] ?? '';
        return $object;
    }
}
