<?php

/**
 * Asalae\Model\Entity\User
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\PremisAgentEntityInterface;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\Model\Entity\User as CoreUser;
use AsalaeCore\Utility\Premis;
use Cake\ORM\Association\HasMany;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table users
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class User extends CoreUser implements PremisAgentEntityInterface, PremisObjectEntityInterface
{
    /**
     * Vrai si l'utilisateur peut être supprimé
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $Users = TableRegistry::getTableLocator()->get('Users');
        $has = [
            'BeanstalkJobs',
            'Transfers',
            'ValidationActors',
            'EventLogs',
            'Fileuploads',
        ];
        foreach ($has as $modelName) {
            /** @var HasMany $assoc */
            $assoc = $Users->{$modelName};
            $query = $assoc->find()
                ->where([$assoc->getForeignKey() => $this->_fields['id']]);
            if ($query->count() > 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\Agent
     */
    public function toPremisAgent(): Premis\Agent
    {
        $agent = new Premis\Agent(
            'local',
            'Users:' . $this->id
        );
        $agent->name = [];
        if ($name = ($this->_fields['name'] ?? '')) {
            $agent->name[] = $name;
        }
        if (($this->_fields['agent_type'] ?? '') === 'software') {
            $agent->type['@valueURI']
                = 'http://id.loc.gov/vocabulary/preservation/agentType/sof'; // NOSONAR
            $agent->type['@'] = 'software';
        }
        return $agent;
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'Users:' . $this->id
        );
        $object->originalName = $this->_fields['username'] ?? '';
        if ($name = ($this->_fields['name'] ?? '')) {
            $object->significantProperties['name'] = $name;
        }
        if ($name = ($this->_fields['email'] ?? '')) {
            $object->significantProperties['email'] = $name;
        }
        return $object;
    }

    /**
     * Case à cocher virtuelle
     * @return bool
     */
    protected function _getNotifyDownloadPdf()
    {
        return isset($this->_fields['notify_download_pdf_frequency']);
    }

    /**
     * Case à cocher virtuelle
     * @return bool
     */
    protected function _getNotifyDownloadZip()
    {
        return isset($this->_fields['notify_download_zip_frequency']);
    }

    /**
     * Case à cocher virtuelle
     * @return bool
     */
    protected function _getNotifyValidation()
    {
        return isset($this->_fields['notify_validation_frequency']);
    }

    /**
     * Case à cocher virtuelle
     * @return bool
     */
    protected function _getNotifyJob()
    {
        return isset($this->_fields['notify_job_frequency']);
    }

    /**
     * Case à cocher virtuelle
     * @return bool
     */
    protected function _getNotifyAgreement()
    {
        return isset($this->_fields['notify_agreement_frequency']);
    }

    /**
     * Case à cocher virtuelle
     * @return bool
     */
    protected function _getNotifyTransferReport()
    {
        return isset($this->_fields['notify_transfer_report_frequency']);
    }
}
