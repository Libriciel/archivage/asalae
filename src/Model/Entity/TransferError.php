<?php

/**
 * Asalae\Model\Entity\TransferError
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table transfer_errors
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TransferError extends Entity
{
    /**
     * @var array erreur sur entité / fichier / empreinte
     *            un transfer_error dans un de ces codes
     *            invalide forcement son transfert
     * 200: service producteur
     * 201: service archive
     * 202: service versant
     * 207: Empreinte(s) invalide(s).
     * 209: archive absente
     * 211: Pièce attachée absente.
     * 212: BinaryDataObject du fichier non référencé dans les ArchiveUnits
     * 213: Mots-clés en double
     * 214: Mots-clés trop long
     * 215: Valeur de règle non autorisée (AppraisalRule, AccessRule)
     */
    public const array INVALIDATE_CODES = [
        self::CODE_UNRECOGNIZED_ORIGINATING_AGENCY,
        self::CODE_UNRECOGNIZED_ARCHIVAL_AGENCY,
        self::CODE_UNRECOGNIZED_TRANSFERRING_AGENCY,
        self::CODE_INVALID_HASH,
        self::CODE_ARCHIVE_MISSING,
        self::CODE_ATTACHMENT_MISSING,
        self::CODE_CUSTOM_BINARY_DATA,
        self::CODE_CUSTOM_KEYWORD,
        self::CODE_CUSTOM_NAME,
        self::CODE_CUSTOM_RULE,
        self::CODE_CUSTOM_COMPOSITE,
    ];

    /**
     * @see webroot/xmlSchemas/seda_v0-2/v02/codes/archives_echanges_v0-2_reply_code.xsd
     */
    public const int CODE_MALFORMED_MESSAGE = 101;
    public const int CODE_UNRECOGNIZED_ORIGINATING_AGENCY = 200;
    public const int CODE_UNRECOGNIZED_ARCHIVAL_AGENCY = 201;
    public const int CODE_UNRECOGNIZED_TRANSFERRING_AGENCY = 202;
    public const int CODE_INVALID_PROFILE = 203;
    public const int CODE_INVALID_HASH = 207;
    public const int CODE_ARCHIVE_MISSING = 209;
    public const int CODE_ATTACHMENT_MISSING = 211;
    public const int CODE_INVALID_AGREEMENT = 300;
    public const int CODE_AGREEMENT_TRANSFER_LIMIT = 301;
    public const int CODE_TRANSFERRING_AGENCY_NOT_ALLOWED = 303;
    public const int CODE_NON_COMPLIANT_FORMAT = 307;
    public const int CODE_AGREEMENT_EXPIRED = 312;
    /**
     * Valeurs données par asalae
     */
    public const int CODE_CUSTOM_BINARY_DATA = 212;
    public const int CODE_CUSTOM_KEYWORD = 213;
    public const int CODE_CUSTOM_NAME = 214;
    public const int CODE_CUSTOM_RULE = 215;
    public const int CODE_CUSTOM_COMPOSITE = 216;
}
