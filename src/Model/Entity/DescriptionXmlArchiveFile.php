<?php

/**
 * Asalae\Model\Entity\DescriptionXmlArchiveFile
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Utility\DOMUtility;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Volume\VolumeManager;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DOMDocument;
use DOMElement;
use DOMXPath;
use XSLTProcessor;

/**
 * Entité de la table archive_files (xxx_description.xml)
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DescriptionXmlArchiveFile extends ArchiveFile
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = ['is_large'];

    /**
     * Donne le document DOM lié à l'entité sans les ArchiveUnits détruites
     * @return DOMUtility|null
     * @throws VolumeException
     */
    public function getDomUtilWithoutDestructedBinaries()
    {
        if (!$this->getDom()) {
            return null;
        }
        $util = new DOMUtility($this->getDom());

        $loc = TableRegistry::getTableLocator();
        $StoredFiles = $loc->get('StoredFiles');
        /** @var StoredFile $deletedArchiveFile */
        $deletedArchiveFile = $StoredFiles->find()
            ->innerJoinWith('ArchiveFiles')
            ->where(['ArchiveFiles.archive_id' => $this->get('archive_id'), 'ArchiveFiles.type' => 'deleted'])
            ->first();
        if (!$deletedArchiveFile || !($json = json_decode($deletedArchiveFile->get('file'), true))) {
            return $util;
        }
        foreach ($json as $xpath) {
            $node = $util->node($xpath);
            $node?->parentNode->removeChild($node);
        }

        // seda 2.x - retrait des BinaryDataObject orphelins
        if (!in_array($util->namespace, [NAMESPACE_SEDA_02, NAMESPACE_SEDA_10])) {
            $binaryDataObjects = $util->xpath->query('//ns:BinaryDataObject');
            /** @var DOMElement $binaryDataObject */
            foreach ($binaryDataObjects as $binaryDataObject) {
                // on vérifi si le BinaryDataObject est sité dans un DataObjectReferenceId
                $id = $binaryDataObject->getAttribute('id');
                $qContent = $util->xpathQuote($id);
                $xpath = sprintf("boolean(//ns:DataObjectReferenceId[contains(text(), %s)])", $qContent);
                if ($util->xpath->evaluate($xpath)) {
                    continue;
                }

                // sinon, on vérifi si son groupe est sité dans un DataObjectGroupReferenceId
                /** @var DOMElement $parentNode */
                $parentNode = $binaryDataObject->parentNode;
                $groupId = $parentNode->getAttribute('id');
                $qContent = $util->xpathQuote($groupId);
                $xpath = sprintf("boolean(//ns:DataObjectGroupReferenceId[contains(text(), %s)])", $qContent);
                if ($util->xpath->evaluate($xpath)) {
                    continue;
                }

                // on retire le noeud
                $binaryDataObject->parentNode->removeChild($binaryDataObject);

                // on retire le groupe si celui-ci est vide
                if (!$util->xpath->evaluate('boolean(./ns:BinaryDataObject)', $parentNode)) {
                    $parentNode->parentNode->removeChild($parentNode);
                }
            }
        }

        return $util;
    }

    /**
     * Donne le document DOM lié à l'entité
     * @return DOMDocument|null
     * @throws VolumeException
     */
    public function getDom()
    {
        if (empty($this->dom) && !empty($this->_fields['stored_file_id'])) {
            $volumeManager = VolumeManager::createWithStoredFileId(
                $this->_fields['stored_file_id']
            );
            $this->dom = new DOMDocument();
            $this->dom->formatOutput = true;
            $this->dom->preserveWhiteSpace = false;
            $xml = $volumeManager->get($this->_fields['stored_file_id']);
            $this->dom->loadXML($xml);
        }
        return $this->dom;
    }

    /**
     * Transforme le xml de description d'archives en HTML par xslt
     * @return bool|string
     * @throws VolumeException
     */
    protected function _getHTML()
    {
        $document = $this->getDom();
        $xsl = new DomDocument();
        if (!$this->_getXSL() || !@$xsl->load($this->_getXSL())) {
            return false;
        }

        $namespace = $document->documentElement->getAttributeNode(
            'xmlns'
        )->nodeValue;
        if (in_array($namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            $filenames = $document->getElementsByTagName('Filename');
            /** @var DOMElement $filename */
            foreach ($filenames as $filename) {
                $filename->setAttribute(
                    'b64filename',
                    str_replace(
                        ['+', '/'],
                        ['-', '_'],
                        base64_encode($filename->nodeValue)
                    )
                );
                $filename->setAttribute(
                    'basename',
                    basename($filename->nodeValue)
                );
            }

            // ajout d'élements pour rendu de la DUA
            $appraisals = $document->getElementsByTagName('AppraisalRule');
            $AppraisalRuleCodes = TableRegistry::getTableLocator()->get(
                'AppraisalRuleCodes'
            );
            $rules = $AppraisalRuleCodes->find('list', keyField: 'code')
                ->disableHydration()
                ->toArray();
            $finals = [
                'Destroy' => __("Détruire"),
                'Keep' => __("Conserver"),
            ];
            /** @var DOMElement $appraisal */
            foreach ($appraisals as $appraisal) {
                /** @var DOMElement $rule */
                $rule = $appraisal->getElementsByTagName('Rule')->item(0);
                if ($rule && isset($rules[$rule->nodeValue])) {
                    $rule->setAttribute('trad', $rules[$rule->nodeValue]);
                }
                /** @var DOMElement $final */
                $final = $appraisal->getElementsByTagName('FinalAction')->item(
                    0
                );
                if ($final && isset($finals[$final->nodeValue])) {
                    $final->setAttribute('trad', $finals[$final->nodeValue]);
                }
            }
            // ajout d'élements pour rendu de la communicabilité
            $accesses = $document->getElementsByTagName('AccessRule');
            $AccessRuleCodes = TableRegistry::getTableLocator()->get(
                'AccessRuleCodes'
            );
            $rules = $AccessRuleCodes->find('list', keyField: 'code')
                ->disableHydration()
                ->toArray();
            /** @var DOMElement $access */
            foreach ($accesses as $access) {
                /** @var DOMElement $rule */
                $rule = $access->getElementsByTagName('Rule')->item(0);
                if ($rule && isset($rules[$rule->nodeValue])) {
                    $rule->setAttribute('trad', $rules[$rule->nodeValue]);
                }
            }
        } else {
            $attachments = $document->getElementsByTagName('Attachment');
            /** @var DOMElement $attachment */
            foreach ($attachments as $attachment) {
                $attachmentFileNameNode = $attachment->attributes->getNamedItem(
                    'filename'
                );
                if (empty($attachmentFileNameNode)) {
                    continue;
                }
                $attachment->setAttribute(
                    'b64filename',
                    str_replace(
                        ['+', '/'],
                        ['-', '_'],
                        base64_encode($attachmentFileNameNode->nodeValue)
                    )
                );
                $attachment->setAttribute(
                    'basename',
                    basename($attachmentFileNameNode->nodeValue)
                );
            }
        }

        $xslt = new XsltProcessor();
        $xslt->importStylesheet($xsl);
        $archive_id = isset($this->_fields['archive_id'])
            ? $this->_fields['archive_id'] . '/' : '';
        if (
            !in_array(
                Hash::get($this->_fields, 'archive.state'),
                [ArchivesTable::S_AVAILABLE, ArchivesTable::S_FREEZED]
            )
        ) {
            $xslt->setParameter('', 'afficheDocument', '#');
            $xslt->setParameter('', 'ratchetUrl', Configure::read('Ratchet.connect'));
            $xslt->setParameter('', 'files_exists', false);
        } else {
            $xslt->setParameter(
                '',
                'afficheDocument',
                '/archives/downloadByName/' . $archive_id
            );
            $xslt->setParameter('', 'ratchetUrl', Configure::read('Ratchet.connect'));
            $xslt->setParameter('', 'files_exists', true);
        }

        // ajout des informations liés aux AU supprimés
        if (isset($this->_fields['archive_id'])) {
            /** @var ArchiveFilesTable $ArchiveFiles */
            $ArchiveFiles = TableRegistry::getTableLocator()->get('ArchiveFiles');
            /** @var ArchiveFile $destructionXpaths */
            $destructionXpaths = $ArchiveFiles->find()
                ->where(['archive_id' => $this->_fields['archive_id']])
                ->andWhere(['type' => 'deleted'])
                ->contain(['StoredFiles'])
                ->first();
            if ($destructionXpaths) {
                /** @var StoredFile $storedFile */
                $storedFile = $destructionXpaths->get('stored_file');
                $json = json_decode($storedFile->get('file'), true);
                $xpath = new DOMXPath($document);
                $xpath->registerNamespace('ns', $namespace);

                foreach ($json as $xpathQuery) {
                    $node = $xpath->query($xpathQuery)->item(0);
                    if ($node instanceof DOMElement) {
                        $node->setAttribute('deleted', true);
                    }
                }
            }
        }

        return $xslt->transformToXML($document);
    }

    /**
     * Permet d'obtenir le chemin vers le fichier xsl selon la norme utilisée
     *
     * @return string|bool Faux si la norme n'est pas dans la liste
     * @throws VolumeException
     */
    protected function _getXSL()
    {
        $dom = $this->getDom();
        $xmlns = $dom?->documentElement?->getAttributeNode('xmlns');
        /** @noinspection PhpNullSafeOperatorCanBeUsedInspection false->nodeValue = error */
        $namespace = $xmlns ? $xmlns->nodeValue : null;
        switch ($namespace) {
            case NAMESPACE_SEDA_02:
                return SEDA_ARCHIVE_V02_XSL;
            case NAMESPACE_SEDA_10:
                return SEDA_ARCHIVE_V10_XSL;
            case NAMESPACE_SEDA_21:
                return SEDA_ARCHIVE_V21_XSL;
            case NAMESPACE_SEDA_22:
                return SEDA_ARCHIVE_V22_XSL;
        }

        return false;
    }

    /**
     * est considéré comme large si plus gros que 2,5Mo
     * @return bool
     */
    protected function _getIsLarge(): bool
    {
        return Hash::get($this->_fields, 'stored_file.size', 0) > 2500000;
    }
}
