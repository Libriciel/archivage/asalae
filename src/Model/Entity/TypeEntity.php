<?php

/**
 * Asalae\Model\Entity\TypeEntity
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\TypeEntity as CoreTypeEntity;

/**
 * Entité de la table type_entities
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TypeEntity extends CoreTypeEntity
{
}
