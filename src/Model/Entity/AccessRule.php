<?php

/**
 * Asalae\Model\Entity\AccessRule
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;
use Cake\ORM\Association\HasMany;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table access_rules
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AccessRule extends Entity
{
    /**
     * Donne le nombre de lignes en has_many
     * @return int
     */
    protected function _getHasManyCount(): int
    {
        if (empty($this->_fields['id'])) {
            return 0;
        }
        $AccessRules = TableRegistry::getTableLocator()->get('AccessRules');
        $count = 0;
        foreach ($AccessRules->associations() as $assoc) {
            if (!$assoc instanceof HasMany) {
                continue;
            }
            $count += $assoc->find()
                ->where([$assoc->getForeignKey() => $this->_fields['id']])
                ->count();
        }
        return $count;
    }
}
