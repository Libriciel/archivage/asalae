<?php

/**
 * Asalae\Model\Entity\ArchiveUnitsDestructionRequest
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table archive_units_destruction_requests
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveUnitsDestructionRequest extends Entity
{
}
