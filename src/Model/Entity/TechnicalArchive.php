<?php

/**
 * Asalae\Model\Entity\TechnicalArchive
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Table\TechnicalArchivesTable;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Cake\Utility\Hash;

/**
 * Entité de la table technical_archives
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TechnicalArchive extends Entity implements PremisObjectEntityInterface
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = [
        'deletable',
        'editable',
        'statetrad',
        'typetrad',
    ];

    /**
     * Traductions des états
     *
     * @return string
     */
    protected function _getStatetrad(): string
    {
        if (empty($this->_fields['state'])) {
            return '';
        }
        switch ($this->_fields['state']) {
            case 'creating':
                return __dx('technical_archive', 'state', "creating");
            case 'describing':
                return __dx('technical_archive', 'state', "describing");
            case 'storing':
                return __dx('technical_archive', 'state', "storing");
            case 'available':
                return __dx('technical_archive', 'state', "available");
            case 'destroying':
                return __dx('technical_archive', 'state', "destroying");
        }
        return $this->_fields['state'];
    }

    /**
     * Traductions des types
     *
     * @return string
     */
    protected function _getTypetrad(): string
    {
        if (empty($this->_fields['type'])) {
            return '';
        }
        switch ($this->_fields['type']) {
            case TechnicalArchivesTable::TYPE_ADDED_BY_USER:
                return __dx('technical_archive', 'type', "Ajoutée par un utilisateur");
            case TechnicalArchivesTable::TYPE_ARCHIVAL_AGENCY_EVENTS_LOGS:
                return __dx('technical_archive', 'type', "Événement du service d'Archives");
            case TechnicalArchivesTable::TYPE_DESTRUCTION:
                return __dx('technical_archive', 'type', "Eliminations");
            case TechnicalArchivesTable::TYPE_EVENTS_LOGS:
                return __dx('technical_archive', 'type', "Journal des événements");
            case TechnicalArchivesTable::TYPE_RESTITUTION:
                return __dx('technical_archive', 'type', "Restitutions");
            case TechnicalArchivesTable::TYPE_OPERATING_EVENTS_LOGS:
                return __dx('technical_archive', 'type', "Archive technique d'exploitation");
        }
        return $this->_fields['type'];
    }

    /**
     * Vrai si cette entité peut être modifié
     * @return bool
     */
    protected function _getEditable(): bool
    {
        return Hash::get($this->_fields, 'type') === TechnicalArchivesTable::TYPE_ADDED_BY_USER;
    }

    /**
     * Vrai si cette entité peut être supprimée
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        return Hash::get($this->_fields, 'type') === TechnicalArchivesTable::TYPE_ADDED_BY_USER;
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'TechnicalArchives:' . $this->id
        );
        $object->originalName = $this->_fields['archival_agency_identifier'] ?? '';
        if ($name = ($this->_fields['name'] ?? '')) {
            $object->significantProperties['name'] = $name;
        }
        if ($description = ($this->_fields['description'] ?? '')) {
            $object->significantProperties['description'] = $description;
        }
        return $object;
    }
}
