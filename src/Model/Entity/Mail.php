<?php

/**
 * Asalae\Model\Entity\Mail
 */

namespace Asalae\Model\Entity;

use AsalaeCore\DataType\SerializedObject;
use AsalaeCore\ORM\Entity;
use Cake\Mailer\Mailer;

/**
 * Entité de la table mails
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Mail extends Entity
{
    /**
     * Envoi l'email stocké
     * @return array
     */
    public function send(): array
    {
        return $this->getMailer()->send();
    }

    /**
     * Donne l'object Mailer stocké en base
     * @return Mailer
     */
    public function getMailer(): Mailer
    {
        $serializedObject = $this->_getSerialized();
        return $serializedObject->getObject();
    }

    /**
     * Getter du champ data (json)
     * @return SerializedObject|mixed|null
     */
    protected function _getSerialized()
    {
        $value = $this->_fields['serialized'] ?? null;
        if (gettype($value) === 'resource') {
            rewind($value);
            $value = stream_get_contents($value);
        }
        return is_string($value)
            ? (@SerializedObject::createFromObject($value) ?: $value)
            : $value;
    }

    /**
     * Setter du champ data (json)
     * @param mixed $value
     * @return SerializedObject
     */
    protected function _setSerialized($value)
    {
        return is_array($value)
            ? new SerializedObject($value)
            : $value;
    }
}
