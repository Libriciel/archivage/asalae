<?php

/**
 * Asalae\Model\Entity\ValidationProcess
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\AppMetaTrait;
use AsalaeCore\ORM\Entity;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;

/**
 * Entité de la table validation_processes
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ValidationProcess extends Entity
{
    use AppMetaTrait;

    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = ['processedtrad', 'validatedtrad', 'steptrad'];

    /**
     * @var array Champs du app_meta
     */
    protected $_metaFields = [
        'active',
        'default',
        'created_user_id',
        'modifieds',
        'stepback',
        'created_user_id',
    ];

    /**
     * Setter du champ virtuel stepback (stocké dans app_meta)
     * @param integer $value
     * @return integer
     */
    protected function _setStepback($value)
    {
        return $this->genericAppMetaSetter('stepback', $value);
    }

    /**
     * Traductions
     * @return string
     */
    protected function _getProcessedtrad(): string
    {
        if (!isset($this->_fields['processed'])) {
            return '';
        }
        return $this->_fields['processed']
            ? __dx('validation_process', 'processed', "true")
            : __dx('validation_process', 'processed', "false");
    }

    /**
     * Traductions
     * @return string
     */
    protected function _getValidatedtrad(): string
    {
        if (!isset($this->_fields['validated'])) {
            return '';
        }
        return $this->_fields['validated']
            ? __dx('validation_process', 'validated', "true")
            : __dx('validation_process', 'validated', "false");
    }

    /**
     * Traductions
     * @return string
     */
    protected function _getSteptrad(): string
    {
        if (
            !isset($this->_fields['processed'])
            || $this->_fields['processed']
            || !isset($this->_fields['current_stage_id'])
        ) {
            return '';
        }
        if (!isset($this->_fields['current_stage'])) {
            $this->_fields['current_stage']
                = TableRegistry::getTableLocator()->get('ValidationStages')
                ->get($this->_fields['current_stage_id']);
        }
        return Hash::get($this->_fields, 'current_stage.name');
    }

    /**
     * Permet de savoir si un agent de type MAIL fait parti de l'étape actuelle
     * @return bool
     */
    protected function _getHasMailAgent(): bool
    {
        if (empty($this->_fields['current_stage_id'])) {
            return false;
        }
        return TableRegistry::getTableLocator()
            ->get('ValidationActors')
            ->exists(
                [
                    'validation_stage_id' => $this->_fields['current_stage_id'],
                    'app_type' => 'MAIL',
                ]
            );
    }

    /**
     * On indique l'utilisateur et le service (entité) et ça nous dit si on peut
     * effectuer une validation (doit faire parti des acteurs de validation)
     * @return bool|EntityInterface|array false|validation_actor
     */
    protected function _getProcessable()
    {
        $user_id = (int)($this->_fields['validation_user_id'] ?? null);
        $org_entity_id = (int)($this->_fields['validation_org_entity_id'] ?? null);
        if (empty($this->_fields['current_stage'])) {
            return false;
        }
        $currentStage = $this->_fields['current_stage'];
        $actors = Hash::get($currentStage, 'validation_actors');
        if (!$actors) {
            $ValidationActors = TableRegistry::getTableLocator()->get(
                'ValidationActors'
            );
            $actors = $ValidationActors->find()
                ->where(
                    ['validation_stage_id' => $this->get('current_stage_id')]
                )
                ->all()
                ->toArray();
            if ($currentStage instanceof EntityInterface) {
                $currentStage->set('validation_actors', $actors);
            } else {
                $currentStage['validation_actors'] = $actors;
            }
        }
        $actor_id = $this->_fields['validation_actor_id'] ?? null;
        if ($actor_id) {
            foreach ($actors as $actor) {
                if (Hash::get($actor, 'id') === $actor_id) {
                    return $actor;
                }
            }
        }
        $archivalAgency = false;
        $requesterAgency = false;
        $originatingAgency = false;
        foreach ($actors as $actor) {
            switch (Hash::get($actor, 'app_type')) {
                case 'USER':
                    if (empty($user_id)) {
                        return false;
                    }
                    if (Hash::get($actor, 'app_foreign_key') === $user_id) {
                        return $actor;
                    }
                    break;
                case 'SERVICE_ARCHIVES':
                    $archivalAgency = $actor;
                    break;
                case 'SERVICE_DEMANDEUR':
                    $requesterAgency = $actor;
                    break;
                case 'SERVICE_PRODUCTEUR':
                    $originatingAgency = $actor;
                    break;
            }
        }
        if (empty($org_entity_id)) {
            return false;
        }
        if (
            $archivalAgency
            && $this->_getSubjectArchivalAgencyId() === $org_entity_id
        ) {
            return $archivalAgency;
        } elseif (
            $requesterAgency
            && $this->_getSubjectRequesterAgencyId() === $org_entity_id
        ) {
            return $requesterAgency;
        } elseif (
            $originatingAgency
            && $this->_getSubjectOriginatingAgencyId() === $org_entity_id
        ) {
            return $originatingAgency;
        }
        return false;
    }

    /**
     * Donne l'id du service d'archive du sujet de validation
     * @return int|null
     */
    protected function _getSubjectArchivalAgencyId()
    {
        if (empty($this->_fields['app_subject_type']) || empty($this->_fields['app_foreign_key'])) {
            return null;
        }
        $subject = Inflector::underscore(
            Inflector::singularize(
                $this->_fields['app_subject_type']
            )
        );
        return Hash::get($this->_fields, $subject . '.archival_agency_id');
    }

    /**
     * Donne l'id du service demandeur du sujet de validation
     * @return int|null
     */
    protected function _getSubjectRequesterAgencyId()
    {
        if (empty($this->_fields['app_subject_type']) || empty($this->_fields['app_foreign_key'])) {
            return null;
        }
        $subject = Inflector::underscore(
            Inflector::singularize(
                $this->_fields['app_subject_type']
            )
        );
        switch ($this->_fields['app_subject_type']) {
            case 'DeliveryRequests':
                return Hash::get($this->_fields, $subject . '.requester_id');
            case 'DestructionRequests':
            case 'Transfers':
            default:
                return null;
        }
    }

    /**
     * Donne l'id du service producteur du sujet de validation
     * @return int|null
     */
    protected function _getSubjectOriginatingAgencyId()
    {
        if (empty($this->_fields['app_subject_type']) || empty($this->_fields['app_foreign_key'])) {
            return null;
        }
        $subject = Inflector::underscore(
            Inflector::singularize(
                $this->_fields['app_subject_type']
            )
        );
        switch ($this->_fields['app_subject_type']) {
            case 'DestructionRequests':
                return Hash::get(
                    $this->_fields,
                    $subject . '.originating_agency_id'
                );
            case 'DeliveryRequests':
            case 'Transfers':
            default:
                return null;
        }
    }
}
