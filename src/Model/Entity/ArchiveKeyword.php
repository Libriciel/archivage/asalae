<?php

/**
 * Asalae\Model\Entity\ArchiveKeyword
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Cake\Utility\Hash;
use DOMAttr;
use DOMElement;

/**
 * Entité de la table archive_keywords
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveKeyword extends Entity implements PremisObjectEntityInterface
{
    public const array DOM_TAG_NAME = [
        NAMESPACE_SEDA_02 => 'ContentDescriptive',
        NAMESPACE_SEDA_10 => 'Keyword',
        NAMESPACE_SEDA_21 => 'Keyword',
        NAMESPACE_SEDA_22 => 'Keyword',
    ];
    public const array DOM_ACCESS_TAG_NAME = [
        NAMESPACE_SEDA_02 => 'AccessRestriction',
        NAMESPACE_SEDA_10 => 'AccessRestrictionRule',
        NAMESPACE_SEDA_21 => 'AccessRule',
        NAMESPACE_SEDA_22 => 'AccessRule',
    ];
    public const array DOM_APPEND_PATH = [
        NAMESPACE_SEDA_02 => '/ns:Archive/ns:ContentDescription/ns:ContentDescriptive/ns:KeywordContent',
        NAMESPACE_SEDA_10 => '/ns:Archive/ns:ContentDescription/ns:Keyword/ns:KeywordContent',
        NAMESPACE_SEDA_21
        => '/ns:Archive/ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:Keyword/ns:KeywordContent',
        NAMESPACE_SEDA_22
        => '/ns:Archive/ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content/ns:Keyword/ns:KeywordContent',
    ];
    public const array DOM_APPEND_CONTAINER = [
        NAMESPACE_SEDA_02 => '/ns:Archive/ns:ContentDescription',
        NAMESPACE_SEDA_10 => '/ns:Archive/ns:ContentDescription',
        NAMESPACE_SEDA_21 => '/ns:Archive/ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content',
        NAMESPACE_SEDA_22 => '/ns:Archive/ns:DescriptiveMetadata/ns:ArchiveUnit/ns:Content',
    ];
    public const array DOM_REL_APPEND_CONTAINER = [
        NAMESPACE_SEDA_02 => 'ns:ContentDescription',
        NAMESPACE_SEDA_10 => 'ns:ContentDescription',
        NAMESPACE_SEDA_21 => 'ns:Content',
        NAMESPACE_SEDA_22 => 'ns:Content',
    ];
    public const array DOM_REL_APPEND_PATH = [
        NAMESPACE_SEDA_02 => 'ns:ContentDescription/ns:ContentDescriptive/ns:KeywordContent',
        NAMESPACE_SEDA_10 => 'ns:ContentDescription/ns:Keyword/ns:KeywordContent',
        NAMESPACE_SEDA_21 => 'ns:Content/ns:Keyword/ns:KeywordContent',
        NAMESPACE_SEDA_22 => 'ns:Content/ns:Keyword/ns:KeywordContent',
    ];

    /**
     * Charge l'element dans l'entité, les paramètres sous forme de virtualFields
     * nomduchamp_nomDuParam
     * @param DOMElement $element
     * @return $this
     */
    public function loadDOMElement(DOMElement $element): self
    {
        foreach ($element->childNodes as $node) {
            if (
                !$node instanceof DOMElement || strpos(
                    $node->tagName,
                    'Keyword'
                ) !== 0
            ) {
                continue;
            }
            $prefix = strtolower(
                substr($node->tagName, strlen('Keyword'))
            ) . '_';
            /** @var DOMAttr $attr */
            foreach ($node->attributes as $attr) {
                $this->_fields[$prefix . $attr->name] = $attr->nodeValue;
            }
        }
        return $this;
    }

    /**
     * Vrai si le mot clé utilise la même règle d'accès que son archive_unit
     * @return bool
     */
    protected function _getUseParentAccessRule(): bool
    {
        if (isset($this->_fields['use_parent_access_rule'])) {
            return $this->_fields['use_parent_access_rule'];
        }
        return Hash::get($this->_fields, 'access_rule_id')
            === Hash::get(
                $this->_fields,
                'archive_unit.archive_description.access_rule_id'
            );
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'ArchiveKeywords:' . $this->id
        );
        $object->originalName = $this->_fields['content'] ?? '';
        return $object;
    }
}
