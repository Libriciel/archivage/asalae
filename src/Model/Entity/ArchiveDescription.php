<?php

/**
 * Asalae\Model\Entity\ArchiveDescription
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table archive_descriptions
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveDescription extends Entity
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = ['dates'];

    /**
     * Dates extrêmes
     * @return null|string
     */
    protected function _getDates()
    {
        $from = empty($this->_fields['oldest_date'])
            ? '[sd]'
            : (string)$this->_fields['oldest_date'];
        $to = empty($this->_fields['latest_date'])
            ? '[sd]'
            : (string)$this->_fields['latest_date'];
        return __("du {0} au {1}", $from, $to);
    }
}
