<?php

/**
 * Asalae\Model\Entity\ZipableEntityTrait
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;
use DateTime;

/**
 * Gestion du readme des entités zipable
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin Entity
 */
trait ZippableEntityTrait
{
    /**
     * Crée le fichier readme et en retourne le chemin
     * @param string $tempFolder
     * @return string
     */
    private static function createReadme(string $tempFolder): string
    {
        $date = new DateTime();
        $dest = $tempFolder . 'README.txt';
        $content = __(
            "Fichier zip créé le {0} à {1}, " .
            "ne contient pas les éventuelles modifications et actions qui auraient pu survenir depuis.",
            $date->format('Y-m-d'),
            $date->format('H:i:s')
        );
        file_put_contents($dest, $content);
        return $dest;
    }
}
