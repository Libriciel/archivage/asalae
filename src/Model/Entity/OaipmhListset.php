<?php

/**
 * Asalae\Model\Entity\OaipmhListset
 */

namespace Asalae\Model\Entity;

use Asalae\Controller\OaipmhController;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\DOMUtility;
use DOMDocument;
use Exception;

/**
 * Entité de la table oaipmh_listsets
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OaipmhListset extends Entity
{
    /**
     * Add the correct node to oaipmh dom
     *
     * @param DOMDocument $dom
     * @param string      $verb
     * @param string|null $metadataprefix
     * @throws Exception
     */
    public function addOaipmhXmlToDom(
        DOMDocument $dom,
        $verb = 'listSets',
        string $metadataprefix = null
    ) {
        if ($verb !== 'listSets') {
            throw new Exception('bad verb (' . $verb . ') for ' . __FUNCTION__);
        }
        if ($metadataprefix !== null) {
            throw new Exception(
                'bad params $metadataprefix(' . $metadataprefix . ') for ' . __FUNCTION__
            );
        }

        $setXML = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'set');
        $dom->firstChild->lastChild->appendChild($setXML);
        $spec = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'setSpec');
        $spec->appendChild(DOMUtility::createDomTextNode($dom, $this->get('spec')));
        $name = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'setName');
        $name->appendChild(DOMUtility::createDomTextNode($dom, $this->get('name')));
        $setXML->appendChild($spec);
        $setXML->appendChild($name);
    }
}
