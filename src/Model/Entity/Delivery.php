<?php

/**
 * Asalae\Model\Entity\Delivery
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Table\DeliveriesTable;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Date as CakeDate;
use Cake\Utility\Hash;
use Cake\View\ViewBuilder;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * Entité de la table deliverys
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Delivery extends Entity implements PremisObjectEntityInterface
{
    /**
     * @var array Champs virtuels
     */
    protected array $_virtual = ['statetrad', 'basepath', 'xml'];

    /**
     * Traductions des états
     * @return string
     */
    protected function _getStatetrad(): string
    {
        if (empty($this->_fields['state'])) {
            return '';
        }
        switch ($this->_fields['state']) {
            case DeliveriesTable::S_CREATING:
                return __dx('delivery', 'state', "en cours de création");
            case DeliveriesTable::S_AVAILABLE:
                return __dx(
                    'delivery',
                    'state',
                    "disponible pour le téléchargement"
                );
            case DeliveriesTable::S_DOWNLOADED:
                return __dx('delivery', 'state', "communication téléchargée");
            case DeliveriesTable::S_ACQUITTED:
                return __dx('delivery', 'state', "communication acquittée");
        }
        return $this->_fields['state'];
    }

    /**
     * Donne l'uri du xml
     * @return string
     */
    public function _getXml(): string
    {
        if (empty($this->_fields['id']) || empty($this->_fields['identifier'])) {
            return '';
        }
        return rtrim(Configure::read('App.paths.data'), DS)
            . DS . 'deliveries'
            . DS . $this->_fields['id']
            . DS . $this->_fields['identifier'] . '_delivery.xml';
    }

    /**
     * Donne le chemin de la description.xml de l'unité d'archives
     * @param EntityInterface $archive
     * @param EntityInterface $archiveUnit
     * @return string
     */
    public function getArchiveUnitDescriptionPath(
        EntityInterface $archive,
        EntityInterface $archiveUnit
    ): string {
        return $this->getArchivePath($archive)
            . DS . 'management_data'
            . DS . urlencode(
                $archiveUnit->get('archival_agency_identifier')
            ) . '_description.xml';
    }

    /**
     * Donne le chemin de base d'une unité d'archives
     * @param EntityInterface $archive
     * @return string
     */
    public function getArchivePath(EntityInterface $archive): string
    {
        return $this->_getBasepath() . DS . urlencode(
            $archive->get('archival_agency_identifier')
        );
    }

    /**
     * Donne le chemin de conservation des données d'une communication
     * @return string
     */
    protected function _getBasepath(): string
    {
        if (empty($this->id)) {
            return '';
        }
        return rtrim(
            Configure::read('App.paths.data'),
            DS
        ) . DS . 'deliveries' . DS . $this->id;
    }

    /**
     * Donne le chemin du lifecycle.xml de l'archive
     * @param EntityInterface $archive
     * @return string
     */
    public function getArchiveLifecyclePath(EntityInterface $archive): string
    {
        return $this->getArchivePath($archive)
            . DS . 'management_data'
            . DS . urlencode(
                $archive->get('archival_agency_identifier')
            ) . '_lifecycle.xml';
    }

    /**
     * Donne le chemin du delivery.xml de l'archive
     * @param EntityInterface $archive
     * @return string
     */
    public function getArchiveDeliveryPath(EntityInterface $archive): string
    {
        return $this->_getBasepath()
            . DS . urlencode(
                $archive->get('archival_agency_identifier')
            ) . '_delivery.xml';
    }

    /**
     * Génère le fichier xml issue de l'entité
     * @return string|null Rendered content or null if content already rendered and returned earlier.
     * @throws Exception
     */
    public function generateXml()
    {
        $required = [
            'delivery_request.comment',
            'delivery_request.created',
            'identifier',
            'created',
            'delivery_request.identifier',
            'delivery_request.archive_units.0.archival_agency_identifier',
            'delivery_request.requester.archival_agency.identifier',
            'delivery_request.requester.identifier',
        ];
        foreach ($required as $r) {
            if (Hash::get($this->_fields, $r) === null) {
                throw new Exception('missing required data (' . $r . ')');
            }
        }
        $aaIdentifiers = Hash::extract(
            $this->_fields,
            'delivery_request.archive_units.{n}.archival_agency_identifier'
        );
        $units = [];
        foreach ($aaIdentifiers as $identifier) {
            $units[] = htmlspecialchars(
                $identifier,
                ENT_XML1
            );
        }

        /** @var DateTime $date */
        $viewBuilder = new ViewBuilder();
        $viewBuilder->setTemplate('/Deliveries/xml/seda_2.1');
        $viewBuilder->setLayout('ajax');
        $date = Hash::get($this, 'delivery_request.created');
        $viewBuilder->setVars(
            [
                'comment' => $this->getEscaped('delivery_request.comment'),
                'date' => ($date instanceof DateTimeInterface || $date instanceof CakeDate)
                    ? $date->format(DateTimeInterface::RFC3339)
                    : $date,
                'identifier' => $this->getEscaped('identifier'),
                'request_identifier' => $this->getEscaped(
                    'delivery_request.identifier'
                ),
                'archiveUnits' => $units,
                'archivalAgency' => $this->getEscaped(
                    'delivery_request.requester.archival_agency.identifier'
                ),
                'requester' => $this->getEscaped(
                    'delivery_request.requester.identifier'
                ),
            ]
        );
        return $viewBuilder->build()->render();
    }

    /**
     * Donne une version protégé du champ demandé
     * @param string $path
     * @return string
     */
    private function getEscaped(string $path)
    {
        return htmlspecialchars(
            Hash::get($this->_fields, $path),
            ENT_XML1
        );
    }

    /**
     * Donne l'emplacement du zip lié à la communication
     * @return string
     */
    protected function _getZip()
    {
        return dirname($this->_getBasepath()) . DS . urlencode(
            $this->_fields['identifier'] ?? ''
        ) . '_delivery.zip';
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'Deliveries:' . $this->id
        );
        $object->originalName = $this->_fields['identifier'] ?? '';
        return $object;
    }
}
