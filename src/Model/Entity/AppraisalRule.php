<?php

/**
 * Asalae\Model\Entity\AppraisalRule
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;
use Cake\I18n\Date as CakeDate;
use DateTimeInterface;

/**
 * Entité de la table appraisal_rules
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AppraisalRule extends Entity
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = ['final_action_codetrad', 'dua'];

    /**
     * Traductions du champ final_action_code
     * @return string
     */
    protected function _getFinalActionCodetrad()
    {
        switch ($action = $this->_fields['final_action_code'] ?? '') {
            case 'keep':
                return __dx('appraisal_rule', 'final_action_code', "Conserver");
            case 'destroy':
                return __dx('appraisal_rule', 'final_action_code', "Détruire");
            default:
                return $action;
        }
    }

    /**
     * Donne la valeur pour le fichier xml (compatible avec le schema xml)
     * @return mixed|string
     */
    protected function _getFinalActionCodeXml()
    {
        switch ($action = $this->_fields['final_action_code'] ?? '') {
            case 'keep':
                return 'conserver';
            case 'destroy':
                return 'detruire';
            default:
                return $action;
        }
    }

    /**
     * Donne la valeur pour le fichier xml en seda2.1 (compatible avec le schema xml)
     * @return mixed|string
     */
    protected function _getFinalActionCodeXml21()
    {
        switch ($action = $this->_fields['final_action_code'] ?? '') {
            case 'keep':
                return 'Keep';
            case 'destroy':
                return 'Destroy';
            default:
                return $action;
        }
    }

    /**
     * Donne la dua sous la forme "25 ans, 1 mois et 1 jour"
     * @return mixed|string|null
     */
    protected function _getDua()
    {
        if (empty($this->_fields['start_date']) || empty($this->_fields['end_date'])) {
            return null;
        }
        $start = $this->_fields['start_date'];
        $end = $this->_fields['end_date'];
        if (
            !($start instanceof DateTimeInterface || $start instanceof CakeDate)
            || !($end instanceof DateTimeInterface || $end instanceof CakeDate)
        ) {
            return null;
        }
        $diff = $end->diff($start);
        $str = [];

        if ($diff->y) {
            $str[] = __n("{0} an", "{0} ans", $diff->y, $diff->y);
        }
        if ($diff->m) {
            $str[] = __n("{0} mois", "{0} mois", $diff->m, $diff->m);
        }
        if ($diff->d || empty($str)) {
            $str[] = __n("{0} jour", "{0} jours", $diff->d, $diff->d);
        }
        switch (count($str)) {
            case 1:
                return $str[0];
            case 2:
                return $str[0] . ' ' . __("et") . ' ' . $str[1];
            case 3:
                return $str[0] . ', ' . $str[1] . ' ' . __(
                    "et"
                ) . ' ' . $str[2];
        }
    }
}
