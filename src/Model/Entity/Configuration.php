<?php

/**
 * Asalae\Model\Entity\Configuration
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\Configuration as CoreConfiguration;

/**
 * Entité de la table configurations
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Configuration extends CoreConfiguration
{
}
