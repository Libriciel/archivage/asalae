<?php

/**
 * Asalae\Model\Entity\AppraisalRuleCode
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;
use Cake\ORM\Association\HasMany;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table appraisal_rule_codes
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AppraisalRuleCode extends Entity
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = [
        'deletable',
    ];

    /**
     * Cette entité peut-elle être supprimée ?
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        if (empty($this->_fields['org_entity_id'])) {
            return false;
        }
        $AppraisalRuleCodes = TableRegistry::getTableLocator()->get('AppraisalRuleCodes');
        $has = [
            'AppraisalRules',
        ];
        foreach ($has as $modelName) {
            /** @var HasMany $assoc */
            $assoc = $AppraisalRuleCodes->{$modelName};
            $query = $assoc->find()
                ->where([$assoc->getForeignKey() => $this->_fields['id']]);
            if ($query->count() > 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Donne le contenu du champ name à partir du champ duration
     * @return string
     */
    protected function _getRule()
    {
        return 'AP' . ($this->_fields['duration']);
    }
}
