<?php

/**
 * Asalae\Model\Entity\Eaccpf
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\Eaccpf as CoreEaccpf;

/**
 * Entité de la table eaccpfs
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Eaccpf extends CoreEaccpf
{
}
