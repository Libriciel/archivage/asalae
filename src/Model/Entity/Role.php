<?php

/**
 * Asalae\Model\Entity\Role
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\Model\Entity\Role as CoreRole;
use AsalaeCore\Utility\Premis;

/**
 * Entité de la table roles
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Role extends CoreRole implements PremisObjectEntityInterface
{
    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'Roles:' . $this->id
        );
        $object->originalName = $this->_fields['name'] ?? '';
        if ($name = ($this->_fields['code'] ?? '')) {
            $object->significantProperties['code'] = $name;
        }
        return $object;
    }
}
