<?php

/**
 * Asalae\Model\Entity\ArchiveFile
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Exception;

/**
 * Entité de la table archive_files
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveFile extends Entity implements PremisObjectEntityInterface
{
    /**
     * Lecture du fichier
     * @return bool success
     * @throws VolumeException
     * @throws Exception
     */
    public function readfile(): bool
    {
        $storedFile = $this->_fields['stored_file'] ?? null;
        if (!$storedFile instanceof StoredFile) {
            throw new Exception('stored file not found');
        }
        return $storedFile->readfile();
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'ArchiveFiles:' . $this->id
        );
        $object->originalName = $this->_fields['filename'] ?? '';
        return $object;
    }
}
