<?php

/**
 * Asalae\Model\Entity\RestitutionRequest
 */

namespace Asalae\Model\Entity;

use Asalae\Exception\GenericException;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Table\TechnicalArchivesTable;
use Asalae\Model\Table\TechnicalArchiveUnitsTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\HashUtility;
use AsalaeCore\Utility\Premis;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\NotImplementedException;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\I18n\Number;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\View\ViewBuilder;
use DateInvalidTimeZoneException;
use DateTimeInterface;
use DOMElement;
use Exception;

/**
 * Entité de la table restitution_requests
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RestitutionRequest extends Entity implements PremisObjectEntityInterface
{
    /**
     * Champs virtuels
     * @var array
     */
    protected array $_virtual = [
        'deletable',
        'editable',
        'sendable',
        'statetrad',
        'xml',
        'pdf_basename',
    ];

    /**
     * Traductions des états
     * @return string
     */
    protected function _getStatetrad(): string
    {
        $state = $this->_fields['state'] ?? '';
        switch ($state) {
            case RestitutionRequestsTable::S_CREATING:
                $state = __dx('restitution-request', 'state', "création");
                break;
            case RestitutionRequestsTable::S_VALIDATING:
                $state = __dx(
                    'restitution-request',
                    'state',
                    "en cours de validation"
                );
                break;
            case RestitutionRequestsTable::S_ACCEPTED:
                $state = __dx('restitution-request', 'state', "acceptée");
                break;
            case RestitutionRequestsTable::S_REJECTED:
                $state = __dx('restitution-request', 'state', "rejetée");
                break;
            case RestitutionRequestsTable::S_RESTITUTION_AVAILABLE:
                $state = __dx(
                    'restitution-request',
                    'state',
                    "restitution générée"
                );
                break;
            case RestitutionRequestsTable::S_RESTITUTION_DOWNLOADED:
                $state = __dx(
                    'restitution-request',
                    'state',
                    "restitution téléchargée"
                );
                break;
            case RestitutionRequestsTable::S_RESTITUTION_ACQUITTED:
                $state = __dx(
                    'restitution-request',
                    'state',
                    "restitution acquittée"
                );
                break;
            case RestitutionRequestsTable::S_ARCHIVE_DESTRUCTION_SCHEDULED:
                $state = __dx(
                    'restitution-request',
                    'state',
                    "élimination planifiée"
                );
                break;
            case RestitutionRequestsTable::S_ARCHIVE_FILES_DESTROYING:
                $state = __dx(
                    'restitution-request',
                    'state',
                    "fichiers détruits"
                );
                break;
            case RestitutionRequestsTable::S_ARCHIVE_DESCRIPTION_DELETING:
                $state = __dx(
                    'restitution-request',
                    'state',
                    "unités d'archives détruites"
                );
                break;
            case RestitutionRequestsTable::S_RESTITUTED:
                $state = __dx('restitution-request', 'state', "restitué");
                break;
        }
        return $state;
    }

    /**
     * Action edit
     * @return bool
     */
    protected function _getEditable()
    {
        if (empty($this->_fields['state'])) {
            return true;
        }
        return $this->_fields['state'] === 'creating';
    }

    /**
     * Action supprimer
     * @return bool
     */
    protected function _getDeletable()
    {
        if (empty($this->_fields['state'])) {
            return true;
        }
        return in_array($this->_fields['state'], ['creating', 'rejected']);
    }

    /**
     * Action envoyer
     * @return bool
     */
    protected function _getSendable()
    {
        if (empty($this->_fields['state'])) {
            return true;
        }
        return $this->_fields['state'] === 'creating'
            && TableRegistry::getTableLocator()->get(
                'ArchiveUnitsRestitutionRequests'
            )
                ->exists(['restitution_request_id' => $this->id]);
    }

    /**
     * Donne l'uri du xml
     * @return string
     */
    public function _getXml(): string
    {
        if (empty($this->_fields['id']) || empty($this->_fields['identifier'])) {
            return '';
        }
        return Configure::read('App.paths.data')
            . DS . 'restitution-requests'
            . DS . $this->_fields['id']
            . DS . $this->_fields['identifier'] . '_restitution_request.xml';
    }

    /**
     * Génère le fichier xml issue de l'entité
     * @return string|null Rendered content or null if content already rendered and returned earlier.
     * @throws Exception
     */
    public function generateXml(): ?string
    {
        $required = [
            'id',
            'identifier',
            'created',
            'archival_agency.identifier',
            'originating_agency.identifier',
        ];
        foreach ($required as $r) {
            if (Hash::get($this->_fields, $r) === null) {
                throw new GenericException(
                    'missing required data (' . $r . ')'
                );
            }
        }
        $queryArchiveUnits = TableRegistry::getTableLocator()->get(
            'ArchiveUnits'
        )->find()
            ->innerJoinWith('ArchiveUnitsRestitutionRequests')
            ->where(
                ['ArchiveUnitsRestitutionRequests.restitution_request_id' => $this->id]
            )
            ->orderBy(['ArchiveUnits.archival_agency_identifier'])
            ->disableHydration();
        $date = $this->_fields['created'];
        if (!($date instanceof DateTimeInterface || $date instanceof \Cake\I18n\Date)) {
            $date = new CakeDateTime($date);
        }

        /** @var CakeDateTime $date */
        $viewBuilder = new ViewBuilder();
        $viewBuilder->setTemplate('/RestitutionRequests/xml/seda_2.1');
        $viewBuilder->setLayout('ajax');
        $viewBuilder->setVars(
            [
                'restitutionRequest' => $this,
                'comment' => $this->getEscaped('comment'),
                'date' => $date->format('Y-m-d'),
                'datetime' => $date->format(DATE_RFC3339),
                'identifier' => $this->getEscaped('identifier'),
                'queryArchiveUnits' => $queryArchiveUnits,
                'archivalAgency' => $this->getEscaped(
                    'archival_agency.identifier'
                ),
                'originatingAgency' => $this->getEscaped(
                    'originating_agency.identifier'
                ),
            ]
        );
        return $viewBuilder->build()->render();
    }

    /**
     * Donne une version protégé du champ demandé
     * @param string $path
     * @return string
     */
    private function getEscaped(string $path)
    {
        return htmlspecialchars(
            Hash::get($this->_fields, $path),
            ENT_XML1
        );
    }

    /**
     * Donne le nom du fichier pdf
     * @return string
     */
    protected function _getPdfBasename(): string
    {
        $identifier = urlencode($this->_fields['identifier'] ?? 'undefined');
        return "restitution_request_$identifier.pdf";
    }

    /**
     * original_total_size avec Mb/Gb etc...
     * @return string
     */
    protected function _getOriginalSizeReadable()
    {
        return Number::toReadableSize($this->_fields['original_size'] ?? 0);
    }

    /**
     * Getter du pdf de l'attestation de restitution
     * @return string
     * @throws DateInvalidTimeZoneException
     * @throws VolumeException
     */
    protected function _getCertification(): string
    {
        if (empty($this->id)) {
            return '';
        }

        /** @var CakeDate $date */
        $date = $this->_fields['created'] ?? new CakeDate();
        $identifier = sprintf('%d_restitutions', $date->year);
        $filename = sprintf(
            "%s_restitution_certificate.pdf",
            $this->_fields['identifier']
        );

        $loc = TableRegistry::getTableLocator();
        $archiveBinary = $loc->get('ArchiveBinaries')
            ->find()
            ->innerJoinWith('TechnicalArchiveUnits')
            ->innerJoinWith('TechnicalArchiveUnits.TechnicalArchives')
            ->where(
                [
                    'ArchiveBinaries.filename' => $filename,
                    'TechnicalArchives.archival_agency_identifier' => $identifier,
                ]
            )
            ->contain(
                [
                    'StoredFiles',
                    'TechnicalArchiveUnits' => ['TechnicalArchives'],
                ]
            )
            ->first();
        /** @var OrgEntity $archivalAgency */
        $archivalAgency = $this->get('archival_agency');
        if (!$archivalAgency) {
            $archivalAgency = $loc->get('OrgEntities')->get($this->_fields['archival_agency_id']);
            $this->set('archival_agency', $archivalAgency);
        }

        return !$archiveBinary
            ? $this->createCertification()
            : $this->downloadRestitutionCertificate($archiveBinary);
    }

    /**
     * Génère et stocke l'attestation de restitution
     * @return string pdf (binaire)
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    public function createCertification(): string
    {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $secureDataSpaceId = Hash::get($this->_fields, 'archival_agency.default_secure_data_space_id');
        $manager = new VolumeManager($secureDataSpaceId);

        try {
            $loc = TableRegistry::getTableLocator();
            /** @var TechnicalArchivesTable $TechnicalArchives */
            $TechnicalArchives = $loc->get('TechnicalArchives');

            $this->getRequiredCertificationFields();
            $technicalArchive = $this->getTechnicalArchive();
            $firstArchiveUnit = $technicalArchive->get('first_technical_archive_unit');

            $filename = sprintf(
                "%s/%s_restitution_certificate.pdf",
                $technicalArchive->get('storage_path'),
                $this->_fields['identifier']
            );
            $binaryPdf = $this->renderRestitutionCertificatePdf();
            $TechnicalArchives->transition($technicalArchive, TechnicalArchivesTable::T_STORE);
            $TechnicalArchives->saveOrFail($technicalArchive);
            $storedFile = $manager->filePutContent($filename, $binaryPdf);
            $TechnicalArchives->transition($technicalArchive, TechnicalArchivesTable::T_STORE);
            $TechnicalArchives->saveOrFail($technicalArchive);
            $this->createTechnicalArchiveBinary($storedFile, $firstArchiveUnit);
            $this->updateTechnicalArchiveCounts($technicalArchive, $firstArchiveUnit);
            $TechnicalArchives->transition($technicalArchive, TechnicalArchivesTable::T_FINISH);
            $TechnicalArchives->saveOrFail($technicalArchive);
        } catch (Exception $e) {
            $conn->rollback();
            if (isset($filename)) {
                $this->deleteDriverFiles($manager, $filename);
            }
            throw $e;
        }
        $conn->commit();
        $this->set('restitution_certified', true);
        return $binaryPdf;
    }

    /**
     * on s'assure d'avoir toutes les données nécessaire au render de l'attestation
     * @return void
     */
    private function getRequiredCertificationFields()
    {
        $loc = TableRegistry::getTableLocator();
        if (!$this->get('archival_agency')) {
            $archivalAgency = $loc->get('OrgEntities')->get($this->_fields['archival_agency_id']);
            $this->set('archival_agency', $archivalAgency);
        }
        if (!$this->get('originating_agency')) {
            $originatingAgency = $loc->get('OrgEntities')->get($this->_fields['originating_agency_id']);
            $this->set('originating_agency', $originatingAgency);
        }
    }

    /**
     * On cherche l'archive technique sur laquelle stocker l'attestation
     * @return EntityInterface
     * @throws DateInvalidTimeZoneException
     */
    private function getTechnicalArchive(): EntityInterface
    {
        $loc = TableRegistry::getTableLocator();
        // On cherche l'archive technique sur laquelle stocker l'attestation
        /** @var CakeDate $date */
        $date = $this->_fields['created'] ?? new CakeDate();
        $identifier = sprintf('%d_restitutions', $date->year);
        /** @var TechnicalArchivesTable $TechnicalArchives */
        $TechnicalArchives = $loc->get('TechnicalArchives');
        $technicalArchive = $TechnicalArchives->find()
            ->where(
                [
                    'TechnicalArchives.type' => TechnicalArchivesTable::TYPE_RESTITUTION,
                    'TechnicalArchives.archival_agency_id' => $this->_fields['archival_agency_id'],
                    'TechnicalArchives.archival_agency_identifier' => $identifier,
                ]
            )
            ->contain(['FirstTechnicalArchiveUnits'])
            ->first();
        if (!$technicalArchive) {
            $technicalArchive = $this->initializeTechnicalArchive();
        }
        return $technicalArchive;
    }

    /**
     * Création de l'archive technique et de son unité d'archives racine
     * @return EntityInterface
     * @throws DateInvalidTimeZoneException
     */
    private function initializeTechnicalArchive(): EntityInterface
    {
        $loc = TableRegistry::getTableLocator();
        /** @var TechnicalArchivesTable $TechnicalArchives */
        $TechnicalArchives = $loc->get('TechnicalArchives');
        /** @var CakeDate $date */
        $date = $this->_fields['created'] ?? new CakeDate();
        $identifier = sprintf('%d_restitutions', $date->year);
        $name = __("Restitutions : {0}", $date->year);
        $technicalArchive = $TechnicalArchives->newEntity(
            [
                'type' => TechnicalArchivesTable::TYPE_RESTITUTION,
                'archival_agency_id' => $this->_fields['archival_agency_id'],
                'archival_agency_identifier' => $identifier,
                'secure_data_space_id' => Hash::get(
                    $this->_fields,
                    'archival_agency.default_secure_data_space_id'
                ),
                'storage_path' => sprintf(
                    '%s/technical_archives/%s',
                    Hash::get($this->_fields, 'archival_agency.identifier'),
                    $identifier
                ),
                'name' => $name,
                'state' => $TechnicalArchives->initialState,
                'original_size' => 0,
                'original_count' => 0,
                'timestamp_size' => 0,
                'timestamp_count' => 0,
                'management_size' => 0,
                'management_count' => 0,
                'created' => $date,
            ]
        );
        $TechnicalArchives->transitionOrFail($technicalArchive, TechnicalArchivesTable::T_DESCRIBE);
        $TechnicalArchives->saveOrFail($technicalArchive);

        /** @var TechnicalArchiveUnitsTable $TechnicalArchiveUnits */
        $TechnicalArchiveUnits = $loc->get('TechnicalArchiveUnits');
        $archiveUnit = $TechnicalArchiveUnits->newEntity(
            [
                'state' => $TechnicalArchiveUnits->initialState,
                'technical_archive_id' => $technicalArchive->id,
                'parent_id' => null,
                'archival_agency_identifier' => sprintf('%s_restitution_certificates', $identifier),
                'name' => $name,
                'description' => null,
                'oldest_date' => $date,
                'latest_date' => $date,
                'original_local_size' => 0,
                'original_local_count' => 0,
                'original_total_size' => 0,
                'original_total_count' => 0,
                'xml_node_tagname' => '',
                'created' => $date,
            ],
            ['setter' => false]
        );
        $TechnicalArchiveUnits->saveOrFail($archiveUnit);
        $technicalArchive->set('first_technical_archive_unit', $archiveUnit);

        $archiveUnit = $TechnicalArchiveUnits->newEntity(
            [
                'state' => $TechnicalArchiveUnits->initialState,
                'technical_archive_id' => $technicalArchive->id,
                'parent_id' => null,
                'archival_agency_identifier' => sprintf('%s_destruction_certificates', $identifier),
                'name' => $name,
                'description' => null,
                'oldest_date' => $date,
                'latest_date' => $date,
                'original_local_size' => 0,
                'original_local_count' => 0,
                'original_total_size' => 0,
                'original_total_count' => 0,
                'xml_node_tagname' => '',
                'created' => $date,
            ],
            ['setter' => false]
        );
        $TechnicalArchiveUnits->saveOrFail($archiveUnit);

        return $technicalArchive;
    }

    /**
     * Donne le PDF du certificat de restitution
     * @return string
     * @throws VolumeException
     */
    private function renderRestitutionCertificatePdf(): string
    {
        return $this->renderPdfFromHtml($this->getRestitutionCertificateHtml());
    }

    /**
     * Transforme un html en pdf
     * @param string $html
     * @return false|string
     */
    private function renderPdfFromHtml(string $html)
    {
        try {
            $htmlFile = tempnam(sys_get_temp_dir(), uniqid('seda2pdf-'));
            file_put_contents($htmlFile, $html);
            $pdfFile = tempnam(sys_get_temp_dir(), uniqid('seda2pdf-'));
            exec(
                sprintf(
                    'cat %s | xvfb-run -a --server-args="-screen 0, 1024x768x24" wkhtmltopdf - %s 2>&1',
                    escapeshellarg($htmlFile),
                    escapeshellarg($pdfFile)
                ),
                $out,
                $code
            );
            if ($code !== 0) {
                throw new GenericException(var_export($out, true));
            }
            return file_get_contents($pdfFile);
        } finally {
            if (is_file($htmlFile)) {
                unlink($htmlFile);
            }
            if (!empty($pdfFile) && is_file($pdfFile)) {
                unlink($pdfFile);
            }
        }
    }

    /**
     * Donne le HTML qui servira pour la transformation en PDF
     * @return string
     * @throws VolumeException
     */
    public function getRestitutionCertificateHtml()
    {
        $secureDataSpaces = [];

        $viewBuilder = (new ViewBuilder())
            ->disableAutoLayout()
            ->addHelper('Html2Pdf')
            ->setTemplatePath('RestitutionRequests')
            ->setTemplate('restitution_certificate')
            ->setVar('archival_agency', $this->get('archival_agency'));

        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $linkedArchives = $Archives->find()
            ->innerJoinWith('RestitutionRequests')
            ->where(['RestitutionRequests.id' => $this->id])
            ->contain(
                [
                    'Transfers',
                    'TransferringAgencies',
                    'SecureDataSpaces' => [
                        'Volumes' => fn(Query $q) => $q->orderBy(['Volumes.name']),
                    ],
                    'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                ]
            );
        $archiveUnits = [];
        /** @var EntityInterface $linkedArchive */
        foreach ($linkedArchives as $linkedArchive) {
            $secureDataSpaceId = $linkedArchive->get('secure_data_space_id');
            if (!isset($secureDataSpaces[$secureDataSpaceId])) {
                $secureDataSpaces[$secureDataSpaceId] = Hash::get(
                    $linkedArchive,
                    'secure_data_space'
                );
            }
            /** @var DescriptionXmlArchiveFile $descriptionXml */
            $descriptionXml = $linkedArchive->get('description_xml_archive_file');
            $this->util = new DOMUtility($descriptionXml->getDom());
            $seda2x = in_array($this->util->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22]);
            if ($seda2x) {
                $archiveUnitNode = $this->util->node('ns:DescriptiveMetadata/ns:ArchiveUnit');
            } else {
                $archiveUnitNode = $this->util->node('/ns:Archive');
            }
            if (empty($archiveUnitNode)) {
                throw new GenericException(
                    __(
                        "Unité d'archives non trouvée dans l'archive {0}",
                        $linkedArchive->get('archive_id')
                    )
                );
            }
            $unitIdentifier = $linkedArchive->get('archival_agency_identifier');
            switch ($this->util->namespace) {
                case NAMESPACE_SEDA_21:
                case NAMESPACE_SEDA_22:
                    $files = $this->extractDescriptionFileDataSeda2x($archiveUnitNode);
                    break;
                case NAMESPACE_SEDA_10:
                    $files = $this->extractDescriptionFileDataSeda10($archiveUnitNode);
                    break;
                case NAMESPACE_SEDA_02:
                    $transferXml = Hash::get($linkedArchive, 'transfers.0.xml');
                    $transferUtil = DOMUtility::load($transferXml);
                    $files = $this->extractDescriptionFileDataSeda02($archiveUnitNode, $transferUtil);
                    break;
                default:
                    throw new NotImplementedException(
                        sprintf(
                            'not implemented for namespace "%s"',
                            $this->util->namespace
                        )
                    );
            }
            $archiveUnits[$unitIdentifier] = [
                'identifier' => $unitIdentifier,
                'name' => $linkedArchive->get('name'),
                'transferring_agency' => sprintf(
                    '%s %s',
                    Hash::get($linkedArchive, 'transferring_agency.identifier'),
                    Hash::get($linkedArchive, 'transferring_agency.name')
                ),
                'hash_algo' => null, // donné par le dernier fichier
                'date' => Hash::get($linkedArchive, 'transfers.0.created'),
                'files' => $files,
            ];
            $archiveUnits[$unitIdentifier]['hash_algo'] = HashUtility::toPhpAlgo(
                $archiveUnits[$unitIdentifier]['files'][0]['hash_algo']
            );
        }
        $viewBuilder->setVar('restitutionRequest', $this);
        $viewBuilder->setVar('archiveUnits', $archiveUnits);
        $viewBuilder->setVar('secureDataSpaces', $secureDataSpaces);
        $viewBuilder->setVar('archivalAgency', $this->get('archival_agency'));
        $viewBuilder->setVar('originatingAgency', $this->get('originating_agency'));
        $statesHistory = json_decode($this->get('states_history'), true);
        foreach ($statesHistory as $history) {
            if ($history['state'] === RestitutionRequestsTable::S_ACCEPTED) {
                $validDate = new CakeDateTime($history['date']);
            }
            if ($history['state'] === RestitutionRequestsTable::S_RESTITUTION_ACQUITTED) {
                $restitutedDate = new CakeDateTime($history['date']);
            }
            if (isset($validDate) && isset($restitutedDate)) {
                break;
            }
        }
        $viewBuilder->setVar('validDate', $validDate ?? null);
        $viewBuilder->setVar('restitutedDate', $restitutedDate ?? null);
        return $viewBuilder->build()->render();
    }

    /**
     * Extraction des données à partir d'un ArchiveUnit (pour rendu html/pdf)
     * @param DOMElement $archiveUnitNode
     * @return array
     */
    private function extractDescriptionFileDataSeda2x(
        DOMElement $archiveUnitNode
    ): array {
        $archiveUnitIdentifier = $this->util->nodeValue(
            'ns:Content/ns:ArchivalAgencyArchiveUnitIdentifier',
            $archiveUnitNode
        );
        $xpathQuery = $this->util->xpath->query('ns:DataObjectReference', $archiveUnitNode);
        $files = [];
        foreach ($xpathQuery as $objectReference) {
            $ref = $this->util->nodeValue('ns:DataObjectReferenceId', $objectReference);
            $binaryDatas = [];
            if ($ref) {
                $node = $this->util->node(
                    sprintf(
                        '//ns:BinaryDataObject[@id=%s]',
                        $this->util->xpathQuote($ref)
                    )
                );
                if ($node) {
                    $binaryDatas[] = $node;
                }
            } else {
                $ref = $this->util->nodeValue('ns:DataObjectGroupReferenceId', $objectReference);
                $group = $this->util->node(
                    sprintf(
                        '//ns:DataObjectGroup[@id=%s]',
                        $this->util->xpathQuote($ref)
                    )
                );
                if ($group) {
                    $binaryDatas = iterator_to_array(
                        $this->util->xpath->query('ns:BinaryDataObject', $group)
                    );
                }
            }
            foreach ($binaryDatas as $binaryData) {
                $filename = ArchiveBinariesTable::getBinaryFilename($this->util, $binaryData);
                /** @var DOMElement $hash */
                $hash = $this->util->node('ns:MessageDigest', $binaryData);
                $hash_algo = $hash->getAttribute('algorithm');
                $files[] = [
                    'archival_agency_identifier' => $archiveUnitIdentifier,
                    'filename' => $filename,
                    'hash' => trim($hash->nodeValue),
                    'hash_algo' => $hash_algo,
                    'size' => $this->util->nodeValue('ns:Size', $binaryData),
                ];
            }
        }
        /** @var DOMElement $child */
        foreach ($this->util->xpath->query('ns:ArchiveUnit', $archiveUnitNode) as $child) {
            $files = array_merge($files, $this->extractDescriptionFileDataSeda2x($child));
        }
        return $files;
    }

    /**
     * Extraction des données à partir d'un ArchiveUnit (pour rendu html/pdf)
     * @param DOMElement $archiveUnitNode
     * @return array
     */
    private function extractDescriptionFileDataSeda10(
        DOMElement $archiveUnitNode
    ): array {
        $xpathQuery = $this->util->xpath->query('ns:Document', $archiveUnitNode);
        $files = [];
        foreach ($xpathQuery as $document) {
            /** @var DOMElement $attachment */
            $attachment = $this->util->node('ns:Attachment', $document);
            $filename = $attachment->getAttribute('filename');
            /** @var DOMElement $hash */
            $hash = $this->util->node('ns:Integrity', $document);
            $hash_algo = $hash->getAttribute('algorithme');
            $files[] = [
                'archival_agency_identifier' => $this->util->nodeValue(
                    'ns:ArchivalAgencyDocumentIdentifier',
                    $document
                ),
                'filename' => $filename,
                'hash' => trim($hash->nodeValue),
                'hash_algo' => $hash_algo,
                'size' => $this->util->nodeValue('ns:Size', $document),
            ];
        }
        /** @var DOMElement $child */
        foreach ($this->util->xpath->query('ns:ArchiveObject', $archiveUnitNode) as $child) {
            $files = array_merge($files, $this->extractDescriptionFileDataSeda10($child));
        }
        return $files;
    }

    /**
     * Extraction des données à partir d'un ArchiveUnit (pour rendu html/pdf)
     * @param DOMElement $archiveUnitNode
     * @param DOMUtility $transferUtil
     * @return array
     */
    private function extractDescriptionFileDataSeda02(
        DOMElement $archiveUnitNode,
        DOMUtility $transferUtil
    ): array {
        $xpathQuery = $this->util->xpath->query('ns:Document', $archiveUnitNode);
        $files = [];
        foreach ($xpathQuery as $document) {
            /** @var DOMElement $attachment */
            $attachment = $this->util->node('ns:Attachment', $document);
            $filename = $attachment->getAttribute('filename');

            $xpath = sprintf(
                '/ns:ArchiveTransfer/ns:Integrity/ns:UnitIdentifier[normalize-space(.)=%s]/../ns:Contains',
                DOMUtility::xpathQuote($filename)
            );
            /** @var DOMElement $hash */
            $hash = $transferUtil->node($xpath);
            $hash_algo = $hash->getAttribute('algorithme');
            $files[] = [
                'archival_agency_identifier' => $this->util->nodeValue(
                    'ns:Identification',
                    $document
                ),
                'filename' => $filename,
                'hash' => trim($hash->nodeValue),
                'hash_algo' => $hash_algo,
                'size' => null,
            ];
        }
        /** @var DOMElement $child */
        foreach ($this->util->xpath->query('ns:Contains', $archiveUnitNode) as $child) {
            $files = array_merge($files, $this->extractDescriptionFileDataSeda02($child, $transferUtil));
        }
        return $files;
    }

    /**
     * Création de l'archive_binary lié à l'attestation d'élimination
     * @param EntityInterface $storedFile
     * @param EntityInterface $archiveUnit
     * @return void
     */
    private function createTechnicalArchiveBinary(EntityInterface $storedFile, EntityInterface $archiveUnit)
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $original = $ArchiveBinaries->newEntity(
            [
                'type' => 'original_data',
                'filename' => basename($storedFile->get('name')),
                'format' => 'fmt/18',
                'mime' => 'application/pdf',
                'extension' => 'pdf',
                'stored_file_id' => $storedFile->id,
                'in_rgi' => false,
                'app_meta' => [],
            ]
        );
        $original->set('technical_archive_units', [$archiveUnit]);
        $ArchiveBinaries->saveOrFail($original);
    }

    /**
     * Met à jour les size et count lié à l'attestation d'élimination
     * @param EntityInterface $technicalArchive
     * @param EntityInterface $firstArchiveUnit
     * @return void
     */
    private function updateTechnicalArchiveCounts(
        EntityInterface $technicalArchive,
        EntityInterface $firstArchiveUnit
    ) {
        $loc = TableRegistry::getTableLocator();
        /** @var TechnicalArchiveUnitsTable $TechnicalArchiveUnits */
        $TechnicalArchiveUnits = $loc->get('TechnicalArchiveUnits');
        /** @var TechnicalArchivesTable $TechnicalArchives */
        $TechnicalArchives = $loc->get('TechnicalArchives');
        $TechnicalArchives->updateCountSize([$technicalArchive]);
        $TechnicalArchiveUnits->updateCountSize([$firstArchiveUnit]);
    }

    /**
     * En cas d'exception, on supprime le pdf qu'on a ajouté sur les volumes
     * @param VolumeManager $manager
     * @param string        $filename
     * @return void
     */
    private function deleteDriverFiles(VolumeManager $manager, string $filename)
    {
        try {
            foreach ($manager->getVolumes() as $driver) {
                if ($driver->fileExists($filename)) {
                    $driver->fileDelete($filename);
                }
            }
        } catch (VolumeException) {
        }
    }

    /**
     * Récupère le fichier
     * @param EntityInterface $archiveBinary
     * @return string
     * @throws VolumeException
     */
    private function downloadRestitutionCertificate(EntityInterface $archiveBinary)
    {
        $secureDataSpaceId = Hash::get(
            $archiveBinary,
            'technical_archive_units.0.technical_archive.secure_data_space_id'
        );
        $manager = new VolumeManager($secureDataSpaceId);
        return $manager->fileGetContent(Hash::get($archiveBinary, 'stored_file.name'));
    }

    /**
     * Getter du pdf de l'attestation d'élimination des archives restitués
     * @return string
     * @throws DateInvalidTimeZoneException
     * @throws VolumeException
     */
    protected function _getDestructionCertification(): string
    {
        if (empty($this->id)) {
            return '';
        }

        /** @var CakeDate $date */
        $date = $this->_fields['created'] ?? new CakeDate();
        $identifier = sprintf('%d_restitutions', $date->year);
        $filename = sprintf(
            "%s_destruction_certificate.pdf",
            $this->_fields['identifier']
        );

        $loc = TableRegistry::getTableLocator();
        $archiveBinary = $loc->get('ArchiveBinaries')
            ->find()
            ->innerJoinWith('StoredFiles')
            ->innerJoinWith('TechnicalArchiveUnits')
            ->innerJoinWith('TechnicalArchiveUnits.TechnicalArchives')
            ->where(
                [
                    'ArchiveBinaries.filename' => $filename,
                    'TechnicalArchives.archival_agency_identifier' => $identifier,
                ]
            )
            ->contain(
                [
                    'StoredFiles',
                    'TechnicalArchiveUnits' => ['TechnicalArchives'],
                ]
            )
            ->first();
        /** @var OrgEntity $archivalAgency */
        $archivalAgency = $this->get('archival_agency');
        if (!$archivalAgency) {
            $archivalAgency = $loc->get('OrgEntities')->get($this->_fields['archival_agency_id']);
            $this->set('archival_agency', $archivalAgency);
        }
        $destructionDelay = Configure::read('Attestations.destruction_delay', 0);
        $intermediate = $archivalAgency->getConfig(
            ConfigurationsTable::CONF_ATTEST_DESTR_CERT_INTERMEDIATE,
            false
        );

        /** @var CakeDateTime $createdDate */
        $createdDate = $this->get('restituted_date')
            ?: $this->get('created');
        $today = new CakeDateTime('00:00:00');
        $diffInDays = $createdDate->diffInDays($today);

        // on doit donner l'attestation définitive
        if (!$destructionDelay || $destructionDelay <= $diffInDays) {
            if ($archiveBinary) {
                // si le définitif existe déjà, on le télécharge
                if ($this->get('destruction_certified')) {
                    $pdf = $this->downloadDestructionCertificate($archiveBinary);
                } else {
                    // sinon on supprime l'intermédiaire
                    $this->deleteDestructionCertificate($archiveBinary);
                }
            }
            // on génère le certificat définitif si besoin
            $pdf = $pdf ?? $this->createDestructionCertification(true);
            $this->set('destruction_certified', true);
        } elseif ($intermediate) {
            if ($archiveBinary) {
                // si l'intermédiaire existe déjà, on le télécharge
                $pdf = $this->downloadDestructionCertificate($archiveBinary);
            } else {
                // sinon on le génère
                $pdf = $this->createDestructionCertification(false);
            }
            $this->set('destruction_certified', false);
        } else {
            $pdf = '';
        }
        return $pdf;
    }

    /**
     * Récupère le fichier
     * @param EntityInterface $archiveBinary
     * @return string
     * @throws VolumeException
     */
    private function downloadDestructionCertificate(EntityInterface $archiveBinary)
    {
        $secureDataSpaceId = Hash::get(
            $archiveBinary,
            'technical_archive_units.0.technical_archive.secure_data_space_id'
        );
        $manager = new VolumeManager($secureDataSpaceId);
        return $manager->fileGetContent(Hash::get($archiveBinary, 'stored_file.name'));
    }

    /**
     * Supprime le fichier
     * @param EntityInterface $archiveBinary
     * @throws VolumeException
     */
    private function deleteDestructionCertificate(EntityInterface $archiveBinary)
    {
        $loc = TableRegistry::getTableLocator();
        $secureDataSpaceId = Hash::get(
            $archiveBinary,
            'technical_archive_units.0.technical_archive.secure_data_space_id'
        );
        $loc->get('ArchiveBinaries')->deleteOrFail($archiveBinary);
        $manager = new VolumeManager($secureDataSpaceId);
        $manager->fileDelete(Hash::get($archiveBinary, 'stored_file.name'));
    }

    /**
     * Génère et stocke l'attestation d'élimination des archives restitués
     * @param bool $definitive
     * @return string pdf (binaire)
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    public function createDestructionCertification(bool $definitive): string
    {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $secureDataSpaceId = Hash::get($this->_fields, 'archival_agency.default_secure_data_space_id');
        $manager = new VolumeManager($secureDataSpaceId);

        try {
            $loc = TableRegistry::getTableLocator();
            /** @var TechnicalArchivesTable $TechnicalArchives */
            $TechnicalArchives = $loc->get('TechnicalArchives');

            $this->getRequiredCertificationFields();
            $technicalArchive = $this->getTechnicalArchive();
            /** @var CakeDate $date */
            $date = $this->_fields['created'] ?? new CakeDate();
            $identifier = sprintf('%d_restitutions', $date->year);
            $archiveUnit = TableRegistry::getTableLocator()
                ->get('TechnicalArchiveUnits')
                ->find()
                ->where(
                    [
                        'technical_archive_id' => $technicalArchive->id,
                        'archival_agency_identifier' => sprintf(
                            '%s_restitution_certificates',
                            $identifier
                        ),
                    ]
                )
                ->firstOrFail();

            $filename = sprintf(
                "%s/%s_destruction_certificate.pdf",
                $technicalArchive->get('storage_path'),
                $this->_fields['identifier']
            );
            $binaryPdf = $this->renderDestructionCertificatePdf($definitive);
            $storedFile = $manager->filePutContent($filename, $binaryPdf);
            $TechnicalArchives->transition($technicalArchive, TechnicalArchivesTable::T_STORE);
            $TechnicalArchives->saveOrFail($technicalArchive);
            $this->createTechnicalArchiveBinary($storedFile, $archiveUnit);
            $this->updateTechnicalArchiveCounts($technicalArchive, $archiveUnit);
            $TechnicalArchives->transition($technicalArchive, TechnicalArchivesTable::T_FINISH);
            $TechnicalArchives->saveOrFail($technicalArchive);
        } catch (Exception $e) {
            $conn->rollback();
            if (isset($filename)) {
                $this->deleteDriverFiles($manager, $filename);
            }
            throw $e;
        }
        $conn->commit();
        $this->set('destruction_certified', true);
        return $binaryPdf;
    }

    /**
     * Donne le PDF du certificat d'élimination des archives restitués
     * @param bool $definitive
     * @return string
     * @throws VolumeException
     */
    private function renderDestructionCertificatePdf(bool $definitive): string
    {
        return $this->renderPdfFromHtml($this->getDestructionCertificateHtml($definitive));
    }

    /**
     * Donne le HTML qui servira pour la transformation en PDF
     * @param bool $definitive
     * @return string
     * @throws VolumeException
     */
    public function getDestructionCertificateHtml(bool $definitive)
    {
        $secureDataSpaces = [];
        $destructionDelay = Configure::read('Attestations.destruction_delay', 0);

        $viewBuilder = (new ViewBuilder())
            ->disableAutoLayout()
            ->addHelper('Html2Pdf')
            ->setTemplatePath('RestitutionRequests')
            ->setTemplate('destruction_certificate')
            ->setVar('archival_agency', $this->get('archival_agency'))
            ->setVar('definitive', $definitive)
            ->setVar('destructionDelay', $destructionDelay);

        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $linkedArchives = $Archives->find()
            ->innerJoinWith('RestitutionRequests')
            ->where(['RestitutionRequests.id' => $this->id])
            ->contain(
                [
                    'Transfers',
                    'TransferringAgencies',
                    'SecureDataSpaces' => [
                        'Volumes' => fn(Query $q) => $q->orderBy(['Volumes.name']),
                    ],
                    'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                ]
            );
        $archiveUnits = [];
        /** @var EntityInterface $linkedArchive */
        foreach ($linkedArchives as $linkedArchive) {
            $secureDataSpaceId = $linkedArchive->get('secure_data_space_id');
            if (!isset($secureDataSpaces[$secureDataSpaceId])) {
                $secureDataSpaces[$secureDataSpaceId] = Hash::get(
                    $linkedArchive,
                    'secure_data_space'
                );
            }
            /** @var DescriptionXmlArchiveFile $descriptionXml */
            $descriptionXml = $linkedArchive->get('description_xml_archive_file');
            $this->util = new DOMUtility($descriptionXml->getDom());
            $seda2x = in_array($this->util->namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22]);
            if ($seda2x) {
                $archiveUnitNode = $this->util->node('ns:DescriptiveMetadata/ns:ArchiveUnit');
            } else {
                $archiveUnitNode = $this->util->node('/ns:Archive');
            }
            if (empty($archiveUnitNode)) {
                throw new GenericException(
                    __(
                        "Unité d'archives non trouvée dans l'archive {0}",
                        $linkedArchive->get('archive_id')
                    )
                );
            }
            $unitIdentifier = $linkedArchive->get('archival_agency_identifier');
            switch ($this->util->namespace) {
                case NAMESPACE_SEDA_21:
                case NAMESPACE_SEDA_22:
                    $files = $this->extractDescriptionFileDataSeda2x($archiveUnitNode);
                    break;
                case NAMESPACE_SEDA_10:
                    $files = $this->extractDescriptionFileDataSeda10($archiveUnitNode);
                    break;
                case NAMESPACE_SEDA_02:
                    $transferXml = Hash::get($linkedArchive, 'transfers.0.xml');
                    $transferUtil = DOMUtility::load($transferXml);
                    $files = $this->extractDescriptionFileDataSeda02($archiveUnitNode, $transferUtil);
                    break;
                default:
                    throw new NotImplementedException(
                        sprintf(
                            'not implemented for namespace "%s"',
                            $this->util->namespace
                        )
                    );
            }
            $archiveUnits[$unitIdentifier] = [
                'identifier' => $unitIdentifier,
                'name' => $linkedArchive->get('name'),
                'transferring_agency' => sprintf(
                    '%s %s',
                    Hash::get($linkedArchive, 'transferring_agency.identifier'),
                    Hash::get($linkedArchive, 'transferring_agency.name')
                ),
                'hash_algo' => null, // donné par le dernier fichier
                'date' => Hash::get($linkedArchive, 'transfers.0.created'),
                'files' => $files,
            ];
            $archiveUnits[$unitIdentifier]['hash_algo'] = HashUtility::toPhpAlgo(
                $archiveUnits[$unitIdentifier]['files'][0]['hash_algo']
            );
        }
        $viewBuilder->setVar('restitutionRequest', $this);
        $viewBuilder->setVar('archiveUnits', $archiveUnits);
        $viewBuilder->setVar('secureDataSpaces', $secureDataSpaces);
        $viewBuilder->setVar('archivalAgency', $this->get('archival_agency'));
        $viewBuilder->setVar('originatingAgency', $this->get('originating_agency'));
        $statesHistory = json_decode($this->get('states_history'), true);
        foreach ($statesHistory as $history) {
            if ($history['state'] === RestitutionRequestsTable::S_ACCEPTED) {
                $validDate = new CakeDateTime($history['date']);
            }
            if ($history['state'] === RestitutionRequestsTable::S_RESTITUTION_ACQUITTED) {
                $restitutedDate = new CakeDateTime($history['date']);
            }
            if (isset($validDate) && isset($restitutedDate)) {
                break;
            }
        }
        $viewBuilder->setVar('validDate', $validDate ?? null);
        $viewBuilder->setVar('restitutedDate', $restitutedDate ?? null);
        return $viewBuilder->build()->render();
    }

    /**
     * Donne la date de l'état restituted
     * @return CakeDateTime|null
     */
    protected function _getRestitutedDate(): ?CakeDateTime
    {
        if (empty($this->_fields['states_history'])) {
            return null;
        }
        $json = json_decode($this->_fields['states_history'], true);
        foreach ($json as $event) {
            if ($event['state'] === RestitutionRequestsTable::S_RESTITUTED) {
                return new CakeDateTime($event['date']);
            }
        }
        return null;
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'RestitutionRequests:' . $this->id
        );
        $object->originalName = $this->_fields['identifier'] ?? '';
        return $object;
    }
}
