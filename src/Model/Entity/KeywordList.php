<?php

/**
 * Asalae\Model\Entity\KeywordList
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\KeywordList as CoreKeywordList;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\Utility\Premis;

/**
 * Entité de la table keyword_lists
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class KeywordList extends CoreKeywordList implements PremisObjectEntityInterface
{
    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'KeywordLists:' . $this->id
        );
        $object->originalName = $this->_fields['identifier'] ?? '';
        if ($name = ($this->_fields['name'] ?? '')) {
            $object->significantProperties['name'] = $name;
        }
        return $object;
    }
}
