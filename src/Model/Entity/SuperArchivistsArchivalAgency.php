<?php

/**
 * Asalae\Model\Entity\SuperArchivistsArchivalAgency
 */

namespace Asalae\Model\Entity;

use Cake\ORM\Entity;

/**
 * Entité de la table super_archivists_archival_agencys
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SuperArchivistsArchivalAgency extends Entity
{
}
