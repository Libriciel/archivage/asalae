<?php

/**
 * Asalae\Model\Entity\ValidationStage
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\AppMetaTrait;
use AsalaeCore\ORM\Entity;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table validation_stages
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ValidationStage extends Entity
{
    use AppMetaTrait;

    /**
     * Champs virtuels
     * @var array
     */
    protected array $_virtual = [
        'all_actors_to_completetrad',
        'deletable',
    ];

    /**
     * @var array Champs du app_meta
     */
    protected $_metaFields = [
        'active',
        'default',
        'created_user_id',
        'modifieds',
    ];

    /**
     * Traductions des app_type
     * @return string
     */
    protected function _getAllActorsToCompletetrad(): string
    {
        $allActors = $this->_fields['all_actors_to_complete'] ?? '';
        if ($allActors === '') {
            return '';
        }
        switch ($allActors) {
            case false:
                return __dx('validation-stage', 'all_actors_to_complete', "OR");
            /** @noinspection PhpConditionAlreadyCheckedInspection faux positif */
            case true:
                return __dx(
                    'validation-stage',
                    'all_actors_to_complete',
                    "AND"
                );
        }
        return $allActors;
    }

    /**
     * List d'id des actors de type 'user' (afin d'empêcher les doublons)
     * @return array
     */
    protected function _getActorsIdList(): array
    {
        if (isset($this->_fields['id']) && !$this->has('validation_actors')) {
            $ValidationActors = TableRegistry::getTableLocator()->get('ValidationActors');
            $actors = $ValidationActors->find()
                ->where(['validation_stage_id' => $this->_fields['id']])
                ->all()
                ->toArray();
            $this->set('validation_actors', $actors);
        }

        return array_map(
            function (EntityInterface $v) {
                return $v->get('app_foreign_key');
            },
            array_filter(
                $this->get('validation_actors') ?: [],
                function (EntityInterface $v) {
                    return $v->get('app_type') === 'USER';
                }
            )
        );
    }

    /**
     * Cette entité peut-elle être supprimée ?
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        return empty($this->_fields['id'])
            || !TableRegistry::getTableLocator()
                ->get('ValidationProcesses')
                ->exists(
                    [
                        'current_stage_id' => $this->_fields['id'],
                        'validated IS' => null,
                    ]
                );
    }
}
