<?php

/**
 * Asalae\Model\Entity\Archive
 */

namespace Asalae\Model\Entity;

use Asalae\Controller\OaipmhController;
use Asalae\Exception\GenericException;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Volume\VolumeManager;
use Asalae\Utility\SedaAttachmentsIterator;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Premis;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\View\ViewBuilder;
use Datacompressor\Exception\DataCompressorException;
use Datacompressor\Utility\DataCompressor;
use DOMDocument;
use DOMElement;
use DOMException;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Entité de la table archives
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Archive extends Entity implements ZippableEntityInterface, PremisObjectEntityInterface
{
    /**
     * Traits
     */
    use ZippableEntityTrait;

    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = [
        'dates',
        'editable',
        'pdf',
        'pdf_basename',
        'statetrad',
        'statetrad_title',
        'is_built',
        'freezable',
        'unfreezable',
    ];

    /**
     * Dates extrêmes
     * @return string
     */
    protected function _getDates(): string
    {
        $from = empty($this->_fields['oldest_date'])
            ? '[sd]'
            : (string)$this->_fields['oldest_date'];
        $to = empty($this->_fields['latest_date'])
            ? '[sd]'
            : (string)$this->_fields['latest_date'];
        return __("du {0} au {1}", $from, $to);
    }

    /**
     * traduction du champ state
     * @return null|string
     */
    protected function _getStatetrad(): ?string
    {
        switch ($state = $this->_fields['state'] ?? '') {
            case ArchivesTable::S_CREATING:
                return __dx('archive', 'state', "création");
            case ArchivesTable::S_DESCRIBING:
                return __dx('archive', 'state', "écriture de la description");
            case ArchivesTable::S_STORING:
                return __dx('archive', 'state', "stockage des fichiers");
            case ArchivesTable::S_MANAGING:
                return __dx(
                    'archive',
                    'managing',
                    "création description et cycle de vie"
                );
            case ArchivesTable::S_AVAILABLE:
                return __dx('archive', 'state', "disponible");
            case ArchivesTable::S_DESTRUCTION_REQUESTING:
                return __dx('archive', 'state', "demande d'élimination en cours");
            case ArchivesTable::S_DESTROYING:
                return __dx('archive', 'state', "élimination en cours");
            case ArchivesTable::S_DESTROYED:
                return __dx('archive', 'state', "est éliminée");
            case ArchivesTable::S_RESTITUTION_REQUESTING:
                return __dx('archive', 'state', "demande de restitution en cours");
            case ArchivesTable::S_RESTITUTING:
                return __dx('archive', 'state', "restitution en cours");
            case ArchivesTable::S_RESTITUTED:
                return __dx('archive', 'state', "est restituée");
            case ArchivesTable::S_TRANSFER_REQUESTING:
                return __dx('archive', 'state', "demande de transfert sortant en cours");
            case ArchivesTable::S_TRANSFERRING:
                return __dx('archive', 'state', "transfert sortant en cours");
            case ArchivesTable::S_TRANSFERRED:
                return __dx('archive', 'state', "est transférée");
            case ArchivesTable::S_FREEZED:
            case ArchivesTable::S_FREEZED_DESTROYING:
            case ArchivesTable::S_FREEZED_RESTITUTING:
            case ArchivesTable::S_FREEZED_TRANSFERRING:
                return __dx('archive', 'state', "est gelée");
            default:
                return $state;
        }
    }

    /**
     * title du champ state
     * @return null|string
     */
    protected function _getStatetradTitle(): ?string
    {
        switch ($state = $this->_fields['state'] ?? '') {
            case ArchivesTable::S_CREATING:
                return __dx(
                    'archive',
                    'state',
                    "l'archive est en cours de création"
                );
            case ArchivesTable::S_DESCRIBING:
                return __dx(
                    'archive',
                    'state',
                    "la description de l'archive en base de données est en cours d'écriture"
                );
            case ArchivesTable::S_STORING:
                return __dx(
                    'archive',
                    'state',
                    "les fichiers de l'archive sont en cours de stockage"
                );
            case ArchivesTable::S_MANAGING:
                return __dx(
                    'archive',
                    'managing',
                    "création et stockage des fichiers de description et du cycle de vie"
                );
            case ArchivesTable::S_AVAILABLE:
                return __dx(
                    'archive',
                    'state',
                    "l'archive est créée et disponible"
                );
            case ArchivesTable::S_DESTRUCTION_REQUESTING:
                return __dx(
                    'archive',
                    'state',
                    "l'archive fait l'objet d'une procédure d'élimination"
                );
            case ArchivesTable::S_DESTROYING:
                return __dx(
                    'archive',
                    'state',
                    "Le processus d'élimination de l'archive est en cours"
                );
            case ArchivesTable::S_DESTROYED:
                return __dx('archive', 'state', "l'archive est éliminée");
            case ArchivesTable::S_RESTITUTION_REQUESTING:
                return __dx(
                    'archive',
                    'state',
                    "l'archive fait l'objet d'une procédure de restitution"
                );
            case ArchivesTable::S_RESTITUTING:
                return __dx(
                    'archive',
                    'state',
                    "Le processus de restitution de l'archive en cours"
                );
            case ArchivesTable::S_RESTITUTED:
                return __dx('archive', 'state', "l'archive est restituée");
            case ArchivesTable::S_TRANSFER_REQUESTING:
                return __dx(
                    'archive',
                    'state',
                    "l'archive fait l'objet d'une procédure de transfert sortant"
                );
            case ArchivesTable::S_TRANSFERRING:
                return __dx(
                    'archive',
                    'state',
                    "Le processus du transfert sortant de l'archive en cours"
                );
            case ArchivesTable::S_TRANSFERRED:
                return __dx('archive', 'state', "l'archive est transférée");
            case ArchivesTable::S_FREEZED:
            case ArchivesTable::S_FREEZED_DESTROYING:
            case ArchivesTable::S_FREEZED_RESTITUTING:
            case ArchivesTable::S_FREEZED_TRANSFERRING:
                return __dx('archive', 'state', "l'archive est gelée");
            default:
                return $state;
        }
    }

    /**
     * vrai si l'entité est éditable
     * @return bool
     */
    protected function _getEditable(): bool
    {
        if (($this->_fields['state'] ?? '') === 'available') {
            return true;
        }
        return false;
    }

    /**
     * Estimated time for the creation of the zip
     * @return int
     */
    public function zipCreationTimeEstimation(): int
    {
        $originalSize = $this->get('original_size');
        $filesCount = $this->get('original_count');

        // récupération des valeurs manuelle si absentes de l'entité
        if ($originalSize === null || $filesCount === null) {
            $entity = TableRegistry::getTableLocator()->get('Archives')
                ->find()
                ->select(['original_size', 'original_count'])
                ->where(['id' => $this->get('id')])
                ->firstOrFail();
            $originalSize = $this->_fields['original_size'] = $entity->get('original_size');
            $filesCount = $this->_fields['original_count'] = $entity->get('original_count');
        }

        // 6s pour 100Mo + 0.0002s par fichiers
        return (int)($originalSize / 100000000 * 6 + ($filesCount * 0.0002));
    }

    /**
     * Add the correct node to oaipmh dom
     * @param DOMDocument $dom
     * @param string      $verb
     * @param string|null $metadataPrefix
     * @throws VolumeException|Exception
     */
    public function addOaipmhXmlToDom(
        DOMDocument $dom,
        string $verb,
        string $metadataPrefix = null
    ): void {
        switch ($verb) {
            case 'listIdentifiers':
                $this->addOaipmhXMLForListIdentifier($dom);
                break;
            case 'listRecords':
            case 'GetRecord':
                $this->addOaipmhXMLForRecords($dom, $metadataPrefix);
                break;
            default:
                throw new Exception('bad verb (' . $verb . ') for ' . __FUNCTION__);
        }
    }

    /**
     * Add the correct node to oaipmh dom for listIdentifier verb
     * @param DOMDocument $dom
     * @throws DOMException
     */
    protected function addOaipmhXMLForListIdentifier(DOMDocument $dom): void
    {
        $recordXml = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'header');
        $dom->firstChild->lastChild->appendChild($recordXml);
        $id = $this->get('archival_agency_identifier');
        $identifier = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'identifier');
        $identifier->appendChild(DOMUtility::createDomTextNode($dom, $id));
        $date = $this->get('modified')
            ->format(OaipmhController::OAIPMH_DATE_FORMAT);
        $dateStamp = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'datestamp');
        $dateStamp->appendChild(DOMUtility::createDomTextNode($dom, $date));
        $recordXml->appendChild($identifier);
        $recordXml->appendChild($dateStamp);
        if ($this->get('state') !== ArchivesTable::S_AVAILABLE) {
            $recordXml->setAttribute('status', 'deleted');
        }
    }

    /**
     * Add the correct node to oaipmh dom for listRecord and getRecord verb
     * @param DOMDocument $dom
     * @param string      $metadataPrefix
     * @throws VolumeException|Exception
     */
    protected function addOaipmhXMLForRecords(DOMDocument $dom, string $metadataPrefix): void
    {
        $recordXml = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'record');
        $dom->firstChild->lastChild->appendChild($recordXml);
        $header = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'header');
        $recordXml->appendChild($header);
        $id = $this->get('archival_agency_identifier');
        $identifier = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'identifier');
        $identifier->appendChild(DOMUtility::createDomTextNode($dom, $id));
        $header->appendChild($identifier);
        $date = $this->get('modified')
            ->format(OaipmhController::OAIPMH_DATE_FORMAT);
        $dateStamp = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'datestamp');
        $dateStamp->appendChild(DOMUtility::createDomTextNode($dom, $date));
        $header->appendChild($dateStamp);
        if ($this->get('state') !== ArchivesTable::S_AVAILABLE) {
            $header->setAttribute('status', 'deleted');
        }

        $metadata = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'metadata');
        $recordXml->appendChild($metadata);

        $originatingAgency = TableRegistry::getTableLocator()->get('OrgEntities')->find()
            ->where(['OrgEntities.id' => $this->_fields['originating_agency_id']])
            ->firstOrFail();

        $transferringAgency = TableRegistry::getTableLocator()->get('OrgEntities')->find()
            ->where(['OrgEntities.id' => $this->_fields['transferring_agency_id']])
            ->first();

        switch ($metadataPrefix) {
            case 'oai_dc':
                $this->addNodeForOaiDc($dom, $metadata, $date);
                break;
            case 'seda0.2':
                $this->addNodeForSeda02($dom, $metadata);
                $this->addAboutNodesForSeda02($dom, $recordXml, $originatingAgency, $transferringAgency);
                break;
            case 'seda1.0':
                $this->addNodeForSeda10($dom, $metadata);
                $this->addAboutNodesForSeda10($dom, $recordXml, $originatingAgency, $transferringAgency);
                break;
            case 'seda2.1':
                $this->addNodeForSeda21($dom, $metadata);
                $this->addAboutNodesForSeda21($dom, $recordXml, $originatingAgency, $transferringAgency);
                break;
            case 'seda2.2':
                $this->addNodeForSeda22($dom, $metadata);
                $this->addAboutNodesForSeda22($dom, $recordXml, $originatingAgency, $transferringAgency);
                break;
            default:
                throw new Exception('Invalid metadataprefix : ' . $metadataPrefix . ' in ' . __FUNCTION__);
        }
    }

    /**
     * oai_dc part for addOaipmhXMLForRecords()
     * @param DOMDocument $dom
     * @param DOMElement  $metadata
     * @param string      $date
     * @throws DOMException
     */
    protected function addNodeForOaiDc(
        DOMDocument $dom,
        DOMElement $metadata,
        string $date
    ) {
        $seda = $dom->createElementNS(
            'http://www.openarchives.org/OAI/2.0/', // NOSONAR
            'oai_dc:dc'
        );
        $metadata->appendChild($seda);
        $seda->setAttributeNS(
            'http://www.w3.org/2000/xmlns/',
            'xmlns:oai_dc',
            'http://www.openarchives.org/OAI/2.0/oai_dc/' // NOSONAR
        );
        $seda->setAttributeNS(
            'http://www.w3.org/2000/xmlns/',
            'xmlns:dc',
            'http://purl.org/dc/elements/1.1/'
        );
        $seda->setAttributeNS(
            'http://www.w3.org/2000/xmlns/',
            'xmlns:xsi',
            Premis::NS_XSI
        );
        $seda->setAttributeNS(
            Premis::NS_XSI,
            'xsi:schemaLocation',
            'http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd' // NOSONAR
        );
        $seda->appendChild($dom->createElement('dc:date', $date));
        $seda->appendChild($dom->createElement('dc:title', $this->get('name')));
        $seda->appendChild(
            $dom->createElement('dc:description', $this->get('description'))
        );
        $seda->appendChild(
            $dom->createElement(
                'dc:identifier',
                $this->get('archival_agency_identifier')
            )
        );
    }

    /**
     * seda0.2 part for addOaipmhXMLForRecords()
     * @param DOMDocument $dom
     * @param DOMElement  $metadata
     * @throws VolumeException
     * @throws DOMException
     */
    protected function addNodeForSeda02(DOMDocument $dom, DOMElement $metadata): void
    {
        $seda = $dom->createElementNS(NAMESPACE_SEDA_02, 'seda');
        $metadata->appendChild($seda);
        /** @var DescriptionXmlArchiveFile $file */
        $file = $this->get('description_xml_archive_file');
        $archiveDom = $file->getDom();

        foreach ($archiveDom->documentElement->childNodes as $node) {
            $seda->appendChild($dom->importNode($node, true));
        }
    }

    /**
     * Ajout section about (OriginatingAgency et TransferringAgency) pour seda0.2
     * @param DOMDocument          $dom
     * @param DOMElement           $recordXml
     * @param EntityInterface      $originatingAgency
     * @param EntityInterface|null $transferringAgency
     * @return void
     * @throws DOMException
     */
    protected function addAboutNodesForSeda02(
        DOMDocument $dom,
        DOMElement $recordXml,
        EntityInterface $originatingAgency,
        ?EntityInterface $transferringAgency
    ): void {
        $this->addAboutNodeForSeda1(NAMESPACE_SEDA_02, $dom, $recordXml, $originatingAgency, $transferringAgency);
    }

    /**
     * Ajout section about (OriginatingAgency et TransferringAgency) pour seda0.2 et 1.0
     * @param string               $sedaNS
     * @param DOMDocument          $dom
     * @param DOMElement           $recordXml
     * @param EntityInterface      $originatingAgency
     * @param EntityInterface|null $transferringAgency
     * @return void
     * @throws DOMException
     */
    protected function addAboutNodeForSeda1(
        string $sedaNS,
        DOMDocument $dom,
        DOMElement $recordXml,
        EntityInterface $originatingAgency,
        ?EntityInterface $transferringAgency
    ): void {
        $about = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'about');
        $recordXml->appendChild($about);
        $originating = $dom->createElementNS($sedaNS, 'OriginatingAgency');
        $identifier = $dom->createElementNS($sedaNS, 'Identification', $originatingAgency->get('identifier'));
        $originating->appendChild($identifier);
        $about->appendChild($originating);
        $name = $dom->createElementNS($sedaNS, 'Name', $originatingAgency->get('name'));
        $originating->appendChild($name);

        if (!$transferringAgency) {
            return;
        }
        $about = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'about');
        $recordXml->appendChild($about);
        $transferring = $dom->createElementNS($sedaNS, 'TransferringAgency');
        $identifier = $dom->createElementNS($sedaNS, 'Identification', $transferringAgency->get('identifier'));
        $transferring->appendChild($identifier);
        $about->appendChild($transferring);
        $name = $dom->createElementNS($sedaNS, 'Name', $transferringAgency->get('name'));
        $transferring->appendChild($name);
    }

    /**
     * seda1.0 part for addOaipmhXMLForRecords()
     * @param DOMDocument $dom
     * @param DOMElement  $metadata
     * @throws VolumeException
     * @throws DOMException
     */
    protected function addNodeForSeda10(DOMDocument $dom, DOMElement $metadata): void
    {
        $seda = $dom->createElementNS(NAMESPACE_SEDA_10, 'seda');
        $metadata->appendChild($seda);
        /** @var DescriptionXmlArchiveFile $file */
        $file = $this->get('description_xml_archive_file');
        $archiveDom = $file->getDom();

        foreach ($archiveDom->documentElement->childNodes as $node) {
            $seda->appendChild($dom->importNode($node, true));
        }
    }

    /**
     * Ajout section about (OriginatingAgency et TransferringAgency) pour seda1.0
     * @param DOMDocument          $dom
     * @param DOMElement           $recordXml
     * @param EntityInterface      $originatingAgency
     * @param EntityInterface|null $transferringAgency
     * @return void
     * @throws DOMException
     */
    protected function addAboutNodesForSeda10(
        DOMDocument $dom,
        DOMElement $recordXml,
        EntityInterface $originatingAgency,
        ?EntityInterface $transferringAgency
    ): void {
        $this->addAboutNodeForSeda1(NAMESPACE_SEDA_10, $dom, $recordXml, $originatingAgency, $transferringAgency);
    }

    /**
     * seda2.1 part for addOaipmhXMLForRecords()
     * @param DOMDocument $dom
     * @param DOMElement  $metadata
     * @throws VolumeException
     * @throws DOMException
     */
    protected function addNodeForSeda21(DOMDocument $dom, DOMElement $metadata): void
    {
        $seda = $dom->createElementNS(NAMESPACE_SEDA_21, 'seda');
        $metadata->appendChild($seda);
        /** @var DescriptionXmlArchiveFile $file */
        $file = $this->get('description_xml_archive_file');
        $archiveDom = $file->getDom();

        foreach ($archiveDom->documentElement->childNodes as $node) {
            $seda->appendChild($dom->importNode($node, true));
        }
    }

    /**
     * Ajout section about (OriginatingAgency et TransferringAgency) pour seda2.1
     * @param DOMDocument          $dom
     * @param DOMElement           $recordXml
     * @param EntityInterface      $originatingAgency
     * @param EntityInterface|null $transferringAgency
     * @return void
     * @throws DOMException
     */
    protected function addAboutNodesForSeda21(
        DOMDocument $dom,
        DOMElement $recordXml,
        EntityInterface $originatingAgency,
        ?EntityInterface $transferringAgency
    ): void {
        $this->addAboutNodeForSeda2(NAMESPACE_SEDA_21, $dom, $recordXml, $originatingAgency, $transferringAgency);
    }

    /**
     * Ajout section about (OriginatingAgency et TransferringAgency) pour seda2.X
     * @param string               $sedaNS
     * @param DOMDocument          $dom
     * @param DOMElement           $recordXml
     * @param EntityInterface      $originatingAgency
     * @param EntityInterface|null $transferringAgency
     * @return void
     * @throws DOMException
     */
    protected function addAboutNodeForSeda2(
        string $sedaNS,
        DOMDocument $dom,
        DOMElement $recordXml,
        EntityInterface $originatingAgency,
        ?EntityInterface $transferringAgency
    ): void {
        $about = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'about');
        $recordXml->appendChild($about);
        $originating = $dom->createElementNS($sedaNS, 'OriginatingAgency');
        $identifier = $dom->createElementNS($sedaNS, 'Identifier', $originatingAgency->get('identifier'));
        $originating->appendChild($identifier);
        $about->appendChild($originating);

        $organizationDescriptiveMetadata = $dom->createElementNS($sedaNS, 'OrganizationDescriptiveMetadata');
        $originating->appendChild($organizationDescriptiveMetadata);
        $name = $dom->createElementNS(NAMESPACE_SEDA_10, 'Name', $originatingAgency->get('name'));
        $organizationDescriptiveMetadata->appendChild($name);

        if (!$transferringAgency) {
            return;
        }
        $about = $dom->createElementNS(OaipmhController::NS_OAIPMH, 'about');
        $recordXml->appendChild($about);
        $transferring = $dom->createElementNS($sedaNS, 'TransferringAgency');
        $identifier = $dom->createElementNS($sedaNS, 'Identifier', $transferringAgency->get('identifier'));
        $transferring->appendChild($identifier);
        $about->appendChild($transferring);
        $organizationDescriptiveMetadata = $dom->createElementNS($sedaNS, 'OrganizationDescriptiveMetadata');
        $transferring->appendChild($organizationDescriptiveMetadata);
        $name = $dom->createElementNS(NAMESPACE_SEDA_10, 'Name', $transferringAgency->get('name'));
        $organizationDescriptiveMetadata->appendChild($name);
    }

    /**
     * seda2.2 part for addOaipmhXMLForRecords()
     * @param DOMDocument $dom
     * @param DOMElement  $metadata
     * @throws VolumeException
     * @throws DOMException
     */
    protected function addNodeForSeda22(DOMDocument $dom, DOMElement $metadata): void
    {
        $seda = $dom->createElementNS(NAMESPACE_SEDA_22, 'seda');
        $metadata->appendChild($seda);
        /** @var DescriptionXmlArchiveFile $file */
        $file = $this->get('description_xml_archive_file');
        $archiveDom = $file->getDom();

        foreach ($archiveDom->documentElement->childNodes as $node) {
            $seda->appendChild($dom->importNode($node, true));
        }
    }

    /**
     * Ajout section about (OriginatingAgency et TransferringAgency) pour seda2.2
     * @param DOMDocument          $dom
     * @param DOMElement           $recordXml
     * @param EntityInterface      $originatingAgency
     * @param EntityInterface|null $transferringAgency
     * @return void
     * @throws DOMException
     */
    protected function addAboutNodesForSeda22(
        DOMDocument $dom,
        DOMElement $recordXml,
        EntityInterface $originatingAgency,
        ?EntityInterface $transferringAgency
    ): void {
        $this->addAboutNodeForSeda2(NAMESPACE_SEDA_22, $dom, $recordXml, $originatingAgency, $transferringAgency);
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity('local', 'Archives:' . $this->id);
        $object->originalName = $this->_fields['archival_agency_identifier'] ?? '';
        if ($name = ($this->_fields['name'] ?? '')) {
            $object->significantProperties['name'] = $name;
        }
        if ($description = ($this->_fields['description'] ?? '')) {
            $object->significantProperties['description'] = $description;
        }
        return $object;
    }

    /**
     * Permet de retrouver l'archive au sein de son transfert
     * @return string
     */
    public function _getXpath(): string
    {
        if (empty($this->_fields['xml_node_tagname'])) {
            return '';
        }
        if (
            preg_match('/(.*)\[\d+]/', $this->_fields['xml_node_tagname'], $m)
            && in_array($m[1], ['Archive', 'Contains'])
        ) {
            return '/ns:ArchiveTransfer/ns:' . $this->_fields['xml_node_tagname'];
        } else {
            // seda2.1
            return '/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata/ns:'
                . $this->_fields['xml_node_tagname'];
        }
    }

    /**
     * Donne le chemin du fichier pdf
     * @return string
     */
    protected function _getPdf(): string
    {
        if (
            empty($this->_fields['storage_path'])
            || empty($this->_fields['archival_agency_identifier'])
        ) {
            return '';
        }
        return $this->_fields['storage_path']
            . DS . 'message' . DS . $this->_getPdfBasename();
    }

    /**
     * Donne le nom du fichier pdf
     * @return string
     */
    protected function _getPdfBasename(): string
    {
        $identifier = urlencode(
            $this->_fields['archival_agency_identifier'] ?? 'undefined'
        );
        return "archive_$identifier.pdf";
    }

    /**
     * Simplifi la config lié à l'archive par le biais du service d'archives
     * @return array
     */
    protected function _getConversions(): array
    {
        if (
            empty($this->get('archival_agency'))
            && !empty($this->_fields['archival_agency_id'])
        ) {
            // ajoute le service d'archive s'il est absent de l'entité
            $this->set(
                'archival_agency',
                TableRegistry::getTableLocator()->get('OrgEntities')->find()
                    ->where(['OrgEntities.id' => $this->_fields['archival_agency_id']])
                    ->contain(
                        [
                            'Configurations' => function (Query $q) {
                                return $q->where(
                                    ['Configurations.name LIKE' => 'conversion-%']
                                );
                            },
                        ]
                    )
                    ->first()
            );
        } elseif (
            !Hash::get($this, 'archival_agency.configurations', [])
            && !empty($this->_fields['archival_agency_id'])
        ) {
            // ajoute la config si elle est absente du service d'archives
            $archivalAgency = $this->get('archival_agency');
            $archivalAgency->set(
                'configurations',
                TableRegistry::getTableLocator()->get('Configurations')->find()
                    ->where(
                        [
                            'Configurations.org_entity_id' => $archivalAgency->id,
                            'Configurations.name LIKE' => 'conversion-%',
                        ]
                    )
                    ->toArray()
            );
        }
        $configurations = Hash::get($this, 'archival_agency.configurations', []);
        $nConfig = [];
        foreach ($configurations as $conf) {
            $nConfig[$conf->get('name')] = $conf->get('setting');
        }
        return array_filter($nConfig) + [ // config par défaut si pas de valeur
            "conversion-dissemination-audio" => "ogg",
            "conversion-dissemination-document_calc" => "pdf",
            "conversion-dissemination-document_pdf" => "pdf",
            "conversion-dissemination-document_presentation" => "pdf",
            "conversion-dissemination-document_text" => "pdf",
            "conversion-dissemination-image" => "jpg",
            "conversion-dissemination-video-codec-audio" => "aac",
            "conversion-dissemination-video-codec-video" => "h264",
            "conversion-dissemination-video-container" => "mp4",
            "conversion-preservation-audio" => "flac",
            "conversion-preservation-document_calc" => "ods",
            "conversion-preservation-document_pdf" => "pdf",
            "conversion-preservation-document_presentation" => "odp",
            "conversion-preservation-document_text" => "odt",
            "conversion-preservation-image" => "png",
            "conversion-preservation-video-codec-audio" => "aac",
            "conversion-preservation-video-codec-video" => "h264",
            "conversion-preservation-video-container" => "mp4",
        ];
    }

    /**
     * Construit un fichier zip à partir de l'entité
     * @return string
     * @throws VolumeException
     * @throws DataCompressorException
     */
    public function makeZip(): string
    {
        $tempFolder = $this->getZipTempFolder();

        Filesystem::begin();
        try {
            $files = $this->getArchiveFiles();
            $files[] = self::createReadme($tempFolder);

            $zipFilename = $this->_getZip();

            DataCompressor::compress($files, $zipFilename);
            Filesystem::commit();
            return $zipFilename;
        } catch (Exception $e) {
            Filesystem::rollback();
            throw $e;
        } finally {
            if (is_dir($tempFolder)) {
                Filesystem::remove($tempFolder);
            }
        }
    }

    /**
     * Chemin vers le dossier temporaire pour la création du zip
     * @return string
     */
    public function getZipTempFolder(): string
    {
        return rtrim(
            Configure::read('App.paths.download', TMP . 'download'),
            DS
        )
            . DS . 'archive' . DS . $this->_fields['id'] . DS;
    }

    /**
     * Get file names for an archive, files are copied into temp folder
     * @return array
     * @throws VolumeException
     */
    private function getArchiveFiles(): array
    {
        $tempFolder = $this->getZipTempFolder();
        if (is_dir($tempFolder)) {
            Filesystem::remove($tempFolder);
        }
        $files = [];
        $secureDataSpaceId = $this->get('secure_data_space_id');
        /** @var VolumeManager $volumeManager */
        $volumeManager = new VolumeManager($secureDataSpaceId);

        $ArchiveBinaries = TableRegistry::getTableLocator()->get('ArchiveBinaries');
        $query = $ArchiveBinaries->find()
            ->innerJoinWith('ArchiveUnits')
            ->where(
                [
                    'ArchiveUnits.archive_id' => $this->id,
                    'ArchiveBinaries.stored_file_id IS NOT' => null,
                    'ArchiveBinaries.type IN' => [
                        'original_data',
                        'original_timestamp',
                    ],
                ]
            )
            ->contain(['StoredFiles']);
        Filesystem::addToDeleteIfRollback($tempFolder);
        foreach ($query as $archiveBinary) {
            $subdir = $archiveBinary->get('type');
            $dest = $tempFolder . $subdir . DS . $archiveBinary->get('filename');
            if (is_file($dest)) {
                unlink($dest);
            }
            $volumeManager->fileDownload(
                Hash::get($archiveBinary, 'stored_file.name'),
                $dest
            );
            $files[] = $dest;
        }

        foreach (Hash::extract($this, 'archive_files.{n}.stored_file.name') as $filename) {
            $dest = $tempFolder . 'management_data/' . basename($filename);
            if (is_file($dest)) {
                unlink($dest);
            }
            $volumeManager->fileDownload($filename, $dest);
            $files[] = $dest;
        }

        return $files;
    }

    /**
     * Donne le chemin de stockage du zip
     * @return string
     */
    public function _getZip(): string
    {
        return self::getZipDownloadFolder() . $this->id . '_archive_files.zip';
    }

    /**
     * Chemin vers le dossier de téléchargement du zip
     * @return string
     */
    public static function getZipDownloadFolder(): string
    {
        return rtrim(
            Configure::read('App.paths.data'),
            DS
        )
            . DS . 'download' . DS . 'archive' . DS;
    }

    /**
     * Les états à partir de available et après
     * @return bool
     */
    protected function _getIsBuilt(): bool
    {
        return in_array(
            $this->_fields['state'] ?? '',
            [
                ArchivesTable::S_AVAILABLE,
                ArchivesTable::S_DESTRUCTION_REQUESTING,
                ArchivesTable::S_DESTROYING,
                ArchivesTable::S_DESTROYED,
                ArchivesTable::S_RESTITUTION_REQUESTING,
                ArchivesTable::S_RESTITUTING,
                ArchivesTable::S_RESTITUTED,
                ArchivesTable::S_TRANSFER_REQUESTING,
                ArchivesTable::S_TRANSFERRING,
                ArchivesTable::S_TRANSFERRED,
                ArchivesTable::S_FREEZED,
                ArchivesTable::S_FREEZED_DESTROYING,
                ArchivesTable::S_FREEZED_RESTITUTING,
                ArchivesTable::S_FREEZED_TRANSFERRING,
            ]
        );
    }

    /**
     * L'archive peut-elle être gelée ?
     * @return bool
     */
    protected function _getFreezable(): bool
    {
        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        return $Archives->can($this, ArchivesTable::T_FREEZE);
    }

    /**
     * L'archive peut-elle être dégelée ?
     * @return bool
     */
    protected function _getUnfreezable(): bool
    {
        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        return $Archives->can($this, ArchivesTable::T_UNFREEZE);
    }

    /**
     * Donne le rendu HTML de l'attestation de d'archivage
     * @return string
     * @throws VolumeException
     * @see ArchivesTable::renderArchivingCertificateHtml()
     */
    public function getArchivingCertificateHtml(): string
    {
        /** @var DescriptionXmlArchiveFile $desc */
        $desc = Hash::get($this, 'description_xml_archive_file');
        if (!$desc) {
            throw new GenericException('description xml not found');
        }
        $dom = $desc->getDom();
        // ajoute les blocs Integrity à l'archive (présent seulement dans le transfer en seda 0.2)
        if (Hash::get($this, 'transfers.0.message_version') === 'seda0.2') {
            /** @var TransferXmlArchiveFile $transfer */
            $transfer = Hash::get($this, 'transfer_xml_archive_files.0');
            $transferDom = $transfer->getDom();
            foreach ($transferDom->getElementsByTagName('Integrity') as $integrity) {
                $dom->documentElement->appendChild($dom->importNode($integrity, true));
            }
        }
        $binaries = new SedaAttachmentsIterator($dom);

        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $transferReceipt = $EventLogs->find()
            ->where(
                [
                    'type' => 'transfer_add',
                    'object_model' => 'Transfers',
                    'object_foreign_key' => Hash::get($this, 'transfers.0.id'),
                ]
            )
            ->firstOrFail();

        $archiveCreate = $EventLogs->find()
            ->where(
                [
                    'type' => 'archive_add',
                    'object_model' => 'Archives',
                    'object_foreign_key' => $this->id,
                ]
            )
            ->firstOrFail();

        $query = $EventLogs->find()
            ->where(
                [
                    'type' => 'check_archive_integrity',
                    'object_model' => 'Archives',
                    'object_foreign_key' => $this->id,
                ]
            )
            ->orderByAsc('id');
        $firstIntegrity = null;
        $integrities = [];
        foreach ($query as $integrityEvent) {
            if (!$firstIntegrity) {
                $firstIntegrity = $integrityEvent;
                $integrities = [$firstIntegrity, $firstIntegrity];
            }
            $integrities[] = $integrityEvent;
            if (count($integrities) === 3) {
                array_shift($integrities);
            }
        }

        /** @var OrgEntity $archivalAgency */
        $archivalAgency = $this->get('archival_agency');
        $saCommitment = $archivalAgency->getConfig(ConfigurationsTable::CONF_ATTEST_ARCHIVING_CERT_COMMITMENT);

        $viewBuilder = (new ViewBuilder())
            ->disableAutoLayout()
            ->addHelper('Html2Pdf')
            ->setTemplatePath('Archives')
            ->setTemplate('archiving_certificate')
            ->setVars(
                [
                    'archive' => $this,
                    'archival_agency' => $this->get('archival_agency'),
                    'transferring_agency' => $this->get('transferring_agency'),
                    'originating_agency' => $this->get('originating_agency'),
                    'binaries' => $binaries,
                    'transferReceipt' => $transferReceipt,
                    'archiveCreate' => $archiveCreate,
                    'firstIntegrity' => $firstIntegrity,
                    'integrities' => $integrities,
                    'saCommitment' => $saCommitment,
                ]
            );
        return $viewBuilder->build()->render();
    }
}
