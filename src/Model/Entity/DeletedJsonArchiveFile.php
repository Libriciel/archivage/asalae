<?php

/**
 * Asalae\Model\Entity\DeletedJsonArchiveFile
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;

/**
 * Entité de la table archive_files (xxx_deleted.json)
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DeletedJsonArchiveFile extends ArchiveFile
{
    /**
     * @var array données contenu dans le fichier json sous forme d'array
     */
    public $data = [];

    /**
     * Donne les données du json
     * @return array
     * @throws VolumeException
     */
    public function getData()
    {
        if (empty($this->data) && !empty($this->_fields['stored_file_id'])) {
            $volumeManager = VolumeManager::createWithStoredFileId(
                $this->_fields['stored_file_id']
            );
            $this->data = json_decode(
                $volumeManager->get($this->_fields['stored_file_id'])
            );
        }
        return $this->data;
    }
}
