<?php

/**
 * Asalae\Model\Entity\ArchiveUnitsOutgoingTransferRequest
 */

namespace Asalae\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table archive_units_outgoing_transfer_requests
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchiveUnitsOutgoingTransferRequest extends Entity
{
}
