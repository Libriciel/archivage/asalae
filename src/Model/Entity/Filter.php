<?php

/**
 * Asalae\Model\Entity\Filter
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\Filter as FilterCore;

/**
 * Entité de la table filters
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Filter extends FilterCore
{
}
