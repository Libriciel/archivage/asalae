<?php

/**
 * Asalae\Model\Entity\AttestationPdfArchiveFile
 */

namespace Asalae\Model\Entity;

/**
 * Entité de la table archive_files (xxx_transfer.xml)
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AttestationPdfArchiveFile extends ArchiveFile
{
}
