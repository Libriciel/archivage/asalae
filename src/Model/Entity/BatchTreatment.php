<?php

/**
 * Asalae\Model\Entity\BatchTreatment
 */

namespace Asalae\Model\Entity;

use AsalaeCore\DataType\JsonString;
use AsalaeCore\ORM\Entity;

/**
 * Entité de la table batch_treatments
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class BatchTreatment extends Entity
{
    /**
     * Getter du champ data (json)
     * @return JsonString|mixed|null
     */
    protected function _getData()
    {
        $value = $this->_fields['data'] ?? null;
        return is_string($value)
            ? JsonString::createFromString($value)
            : $value;
    }

    /**
     * Setter du champ data (json)
     * @param mixed $value
     * @return JsonString
     */
    protected function _setData($value)
    {
        return is_array($value)
            ? new JsonString($value)
            : $value;
    }
}
