<?php

/**
 * Asalae\Model\Entity\ArchivingSystem
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\ArchivingSystem as CoreArchivingSystem;

/**
 * Entité de la table archiving_systems
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchivingSystem extends CoreArchivingSystem
{
}
