<?php

/**
 * Asalae\Model\Entity\StoredFile
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\ORM\Entity;
use Exception;

/**
 * Entité de la table stored_files
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class StoredFile extends Entity
{
    /**
     * @var VolumeManager
     */
    private $manager;

    /**
     * Donne le fichier lié au stored_file
     * @return string|void
     * @throws VolumeException
     */
    protected function _getFile()
    {
        if (empty($this->_fields['id'])) {
            return;
        }
        $manager = $this->getManager();
        return $manager->get($this->_fields['id']);
    }

    /**
     * Getter avec cache pour le VolumeManager
     * @return VolumeManager
     * @throws VolumeException
     */
    private function getManager(): VolumeManager
    {
        if (empty($this->manager)) {
            $this->manager = VolumeManager::createWithStoredFileId($this->_fields['id']);
        }

        return $this->manager;
    }

    /**
     * Lecture du fichier
     * @return bool success
     * @throws VolumeException
     */
    public function readfile(): bool
    {
        $manager = $this->getManager();
        return $manager->readfile($this->_fields['name']);
    }

    /**
     * Teste la présence du fichier sur son volume de stockage
     * @return bool
     * @throws VolumeException
     */
    public function fileExists(): bool
    {
        $manager = $this->getManager();
        return $manager->fileExists($this->_fields['name']);
    }

    /**
     * Donne la liste des volumes utilisés par le stored file avec la réponse ping
     * @return array
     * @throws VolumeException
     */
    public function checkVolumes(): array
    {
        $manager = $this->getManager();
        $volumes = [];
        foreach ($manager->getVolumes() as $volume_id => $volume) {
            try {
                $volumes[$volume_id] = $volume->ping();
            } catch (Exception) {
                $volumes[$volume_id] = false;
            }
        }
        return $volumes;
    }
}
