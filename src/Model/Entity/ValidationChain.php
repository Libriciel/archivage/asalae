<?php

/**
 * Asalae\Model\Entity\ValidationChain
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Table\ValidationChainsTable;
use AsalaeCore\Model\Entity\AppMetaTrait;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Premis;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table validation_chains
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ValidationChain extends Entity implements PremisObjectEntityInterface
{
    use AppMetaTrait;

    /**
     * Champs virtuels
     * @var array
     */
    protected array $_virtual = ['app_typetrad', 'deletable'];

    /**
     * @var array Champs du app_meta
     */
    protected $_metaFields = ['created_user_id', 'type_validation'];

    /**
     * Traductions des app_type
     * @return string
     */
    protected function _getAppTypetrad(): string
    {
        switch ($this->_fields['app_type'] ?? '') {
            case '':
                return '';
            case ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM:
                return __dx(
                    'validation-chain',
                    'app_type',
                    "ARCHIVE_TRANSFER_CONFORM"
                );
            case ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM:
                return __dx(
                    'validation-chain',
                    'app_type',
                    "ARCHIVE_TRANSFER_NOT_CONFORM"
                );
            case ValidationChainsTable::ARCHIVE_DESTRUCTION_REQUEST:
                return __dx(
                    'validation-chain',
                    'app_type',
                    "ARCHIVE_DESTRUCTION_REQUEST"
                );
            case ValidationChainsTable::ARCHIVE_DELIVERY_REQUEST:
                return __dx(
                    'validation-chain',
                    'app_type',
                    "ARCHIVE_DELIVERY_REQUEST"
                );
            case ValidationChainsTable::ARCHIVE_RESTITUTION_REQUEST:
                return __dx(
                    'validation-chain',
                    'app_type',
                    "ARCHIVE_RESTITUTION_REQUEST"
                );
            case ValidationChainsTable::ARCHIVE_OUTGOING_TRANSFER_REQUEST:
                return __dx(
                    'validation-chain',
                    'app_type',
                    "ARCHIVE_OUTGOING_TRANSFER_REQUEST"
                );
        }
        return $this->_fields['app_type'];
    }

    /**
     * Suppression autorisé de l'entité ?
     *      impossible si elle a des process
     *      impossible si utilisée dans un agreements (proper ou improper)
     *      si elle est "par défaut" :
     *          possible seulement si elle n'a pas d'étapes et est la seule de ce type pour le SA
     *      possible si pas par défaut
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }

        $ValidationProcesses = TableRegistry::getTableLocator()->get(
            'ValidationProcesses'
        );
        $processCount = $ValidationProcesses->find()
            ->where(['validation_chain_id' => $this->_fields['id']])
            ->count();
        if ($processCount > 0) {
            return false;
        }

        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $agreementsCount = $Agreements->find()
            ->where(
                [
                    'OR' => [
                        'proper_chain_id' => $this->_fields['id'],
                        'improper_chain_id' => $this->_fields['id'],
                    ],
                ]
            )
            ->count();
        if ($agreementsCount > 0) {
            return false;
        }

        if (empty($this->_fields['default'])) {
            return true;
        }

        $ValidationStages = TableRegistry::getTableLocator()->get(
            'ValidationStages'
        );
        $stagesCount = $ValidationStages->find()
            ->where(['validation_chain_id' => $this->_fields['id']])
            ->count();
        if ($stagesCount > 0) {
            return false;
        }

        $ValidationChains = TableRegistry::getTableLocator()->get(
            'ValidationChains'
        );
        $otherOfSameType = $ValidationChains->find()
            ->where(
                [
                    'org_entity_id' => $this->_fields['org_entity_id'],
                    'id !=' => $this->_fields['id'],
                    'app_type' => $this->_fields['app_type'],
                    ['ValidationChains.app_meta LIKE' => '%"active":true%'],
                ]
            )
            ->disableHydration()
            ->all()
            ->count();

        return $otherOfSameType === 0;
    }

    /**
     * Permet de savoir si un agent de type MAIL fait parti du circuit
     * @return bool
     */
    protected function _getHasMailAgent(): bool
    {
        if (empty($this->id)) {
            return false;
        }
        return (bool)count(
            TableRegistry::getTableLocator()
                ->get('ValidationStages')
                ->find()
                ->select(['existing' => 1])
                ->innerJoinWith('ValidationActors')
                ->where(
                    [
                        'ValidationStages.validation_chain_id' => $this->id,
                        'ValidationActors.app_type' => 'MAIL',
                    ]
                )
                ->limit(1)
                ->disableHydration()
                ->toArray()
        );
    }

    /**
     * Si un process à été lancé avec ce circuit
     * @return bool
     */
    protected function _getHasProcesses(): bool
    {
        if (empty($this->id)) {
            return false;
        }
        return TableRegistry::getTableLocator()
            ->get('ValidationProcesses')
            ->exists(['validation_chain_id' => $this->id]);
    }

    /**
     * Un circuit est utilisable s'il comporte au moins une étape qui possède au moins un acteur
     * @return bool
     */
    protected function _getUsable(): bool
    {
        if (empty($this->id)) {
            return false;
        }
        return (bool)count(
            TableRegistry::getTableLocator()
                ->get('ValidationStages')
                ->find()
                ->select(['existing' => 1])
                ->innerJoinWith('ValidationActors')
                ->where(
                    [
                        'ValidationStages.validation_chain_id' => $this->id,
                    ]
                )
                ->limit(1)
                ->disableHydration()
                ->toArray()
        );
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface|Premis\IntellectualEntity
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'ValidationChains:' . $this->id
        );
        $object->originalName = $this->_fields['name'] ?? '';
        if ($type = ($this->_fields['app_type'] ?? '')) {
            $object->significantProperties['type'] = $type;
        }
        return $object;
    }
}
