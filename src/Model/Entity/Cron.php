<?php

/**
 * Asalae\Model\Entity\Cron
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\Cron as CoreCron;
use AsalaeCore\Model\Entity\PremisAgentEntityInterface;
use AsalaeCore\Model\Entity\PremisObjectEntityInterface;
use AsalaeCore\Utility\Premis;

/**
 * Entité de la table crons
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Cron extends CoreCron implements PremisAgentEntityInterface, PremisObjectEntityInterface
{
    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\Agent
     */
    public function toPremisAgent(): Premis\Agent
    {
        $agent = new Premis\Agent(
            'local',
            'Crons:' . $this->id
        );
        $agent->name = [];
        if ($name = ($this->_fields['name'] ?? '')) {
            $agent->name[] = $name;
        }
        /** @noinspection HttpUrlsUsage */
        $agent->type['@valueURI']
            = 'http://id.loc.gov/vocabulary/preservation/agentType/sof'; // NOSONAR
        $agent->type['@'] = 'software';
        return $agent;
    }

    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface
     */
    public function toPremisObject(): Premis\ObjectInterface
    {
        $object = new Premis\IntellectualEntity(
            'local',
            'Crons:' . $this->id
        );
        $object->originalName = $this->_fields['name'] ?? '';
        return $object;
    }
}
