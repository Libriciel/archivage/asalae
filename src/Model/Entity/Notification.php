<?php

/**
 * Asalae\Model\Entity\Notification
 */

namespace Asalae\Model\Entity;

use AsalaeCore\Model\Entity\Notification as CoreNotification;

/**
 * Entité de la table notifications
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Notification extends CoreNotification
{
}
