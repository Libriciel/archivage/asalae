<?php

/**
 * Asalae\Model\Entity\ValidationHistory
 */

namespace Asalae\Model\Entity;

use ArrayAccess;
use AsalaeCore\Model\Entity\AppMetaTrait;
use AsalaeCore\ORM\Entity;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;

/**
 * Entité de la table validation_histories
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ValidationHistory extends Entity
{
    use AppMetaTrait;

    /**
     * @var array Champs du app_meta
     */
    protected $_metaFields = ['signature'];

    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = ['actiontrad', 'signature'];

    /**
     * Traductions des status
     *
     * @return string
     */
    protected function _getActiontrad(): string
    {
        switch ($this->_fields['action'] ?? '') {
            case '':
                return '';
            case 'validate':
                return __dx('validation-history', 'action', "validate");
            case 'invalidate':
                return __dx('validation-history', 'action', "invalidate");
            case 'stepback':
                return __dx('validation-history', 'action', "stepback");
        }
        return $this->_fields['action'];
    }

    /**
     * Setter du champ virtuel imported_from (stocké dans app_meta)
     * @param string $value
     * @return string
     */
    protected function _setSignature($value)
    {
        return !$value
            ? null
            : $this->genericAppMetaSetter(
                'signature',
                $value
            );
    }

    /**
     * Donne une phrase descriptive de l'acteur
     * @return string
     * @throws Exception
     */
    protected function _getActorInfo(): string
    {
        /** @var ValidationActor $actor */
        $actor = $this->_fields['validation_actor'];
        if (!$actor) {
            return '';
        }
        switch ($actor->get('app_type')) {
            case 'USER':
                /** @var EntityInterface $user */
                $user = $actor->getUser();
                $name = $user->get('name') ?: $user->get('username');
                return __("Utilisateur: {0}", h($name));
            case 'MAIL':
                $name = $actor->get('actor_name');
                $email = $actor->get('app_key');
                return __("E-mail: {0} ({1})", h($name), h($email));
            case 'SERVICE_ARCHIVES':
                $username = $this->_getCreatedUsername();
                $orgEntity = $this->_getCreatedOrgEntityName();
                return __(
                    "Service d'Archives: {0} ({1})",
                    $orgEntity,
                    $username
                );
            case 'SERVICE_DEMANDEUR':
                $username = $this->_getCreatedUsername();
                $orgEntity = $this->_getCreatedOrgEntityName();
                return __(
                    "Service demandeur: {0} ({1})",
                    $orgEntity,
                    $username
                );
            case 'SERVICE_PRODUCTEUR':
                $username = $this->_getCreatedUsername();
                $orgEntity = $this->_getCreatedOrgEntityName();
                return __(
                    "Service producteur: {0} ({1})",
                    $orgEntity,
                    $username
                );
            default:
                return '';
        }
    }

    /**
     * Donne le nom d'utilisateur du créateur de l'history
     * @return null|string
     */
    protected function _getCreatedUsername()
    {
        if (empty($this->meta->created_user_id)) {
            return null;
        }
        if (empty($this->_fields['created_user'])) {
            $Users = TableRegistry::getTableLocator()->get('Users');
            $this->_fields['created_user'] = $Users->get(
                $this->meta->created_user_id
            );
        }
        return Hash::get($this->_fields, 'created_user.username');
    }

    /**
     * Donne le nom de l'entité de l'utilisateur créateur de l'history
     * @return array|ArrayAccess|mixed|null
     */
    protected function _getCreatedOrgEntityName()
    {
        if (empty($this->meta->created_user_id)) {
            return null;
        }
        if (empty($this->_fields['created_org_entity'])) {
            $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
            $this->_fields['created_org_entity']
                = $OrgEntities->get(
                    Hash::get($this->_fields, 'created_user.org_entity_id')
                );
        }
        return Hash::get($this->_fields, 'created_org_entity.name');
    }
}
