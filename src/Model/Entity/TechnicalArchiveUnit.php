<?php

/**
 * Asalae\Model\Entity\TechnicalArchiveUnit
 */

namespace Asalae\Model\Entity;

use Asalae\Model\Table\TechnicalArchiveUnitsTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use FileConverters\Exception\FileNotReadableException;
use FileConverters\Utility\DetectFileFormat;

/**
 * Entité de la table technical_archive_units
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TechnicalArchiveUnit extends Entity
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected array $_virtual = ['statetrad'];

    /**
     * Ajoute des fichiers à l'archive unit
     * @param array $fileuploads
     * @throws VolumeException
     * @throws FileNotReadableException
     */
    public function addFiles(array $fileuploads)
    {
        $loc = TableRegistry::getTableLocator();
        if (empty($this->get('technical_archive'))) {
            $TechnicalArchives = $loc->get('TechnicalArchives');
            $technical_archive = $TechnicalArchives->get($this->get('technical_archive_id'));
            $this->set('technical_archive', $technical_archive);
        }
        $Fileuploads = $loc->get('Fileuploads');
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $ArchiveBinariesTechnicalArchiveUnits = $loc->get('ArchiveBinariesTechnicalArchiveUnits');
        $volumeManager = new VolumeManager(Hash::get($this, 'technical_archive.secure_data_space_id'));
        $basePath = Hash::get($this, 'technical_archive.storage_path') . '/';

        $binaries = $ArchiveBinaries->find()
            ->select('filename')
            ->where(['TechnicalArchiveUnits.id' => $this->id])
            ->innerJoinWith('TechnicalArchiveUnits')
            ->disableHydration()
            ->all()
            ->map(fn($e) => $e['filename'])
            ->toArray();

        foreach ($fileuploads as $fileupload) {
            // suppression des doublons
            if (!$fileupload->get('locked') && in_array($fileupload->get('name'), $binaries)) {
                $Fileuploads->delete($fileupload);
                continue;
            }

            $storedFile = $volumeManager->fileUpload(
                $fileupload->get('path'),
                $basePath . $fileupload->get('name')
            );
            $detector = new DetectFileFormat($fileupload->get('path'));
            $puid = $detector->getPuid();
            $format = $puid && !empty($puid['id']) ? $puid['id'] : null;
            $originalBinary = $ArchiveBinaries->newEntity(
                [
                    'is_integrity_ok' => null,
                    'type' => 'original_data',
                    'filename' => $fileupload->get('name'),
                    'format' => $format,
                    'mime' => mime_content_type($fileupload->get('path')),
                    'extension' => pathinfo($fileupload->get('path'), PATHINFO_EXTENSION),
                    'stored_file_id' => $storedFile->get('id'),
                    'in_rgi' => $detector->rgiCompatible(),
                    'app_meta' => array_filter(
                        [
                            'category' => $detector->getCategory(),
                            'codecs' => $detector->getCodecs(),
                            'puid' => $puid,
                        ]
                    ),
                ]
            );
            $ArchiveBinaries->saveOrFail($originalBinary);
            $link = $ArchiveBinariesTechnicalArchiveUnits->newEntity(
                [
                    'archive_binary_id' => $originalBinary->id,
                    'technical_archive_unit_id' => $this->id,
                ]
            );
            $ArchiveBinariesTechnicalArchiveUnits->saveOrFail($link);
            $Fileuploads->delete($fileupload);
        }
    }

    /**
     * Traductions des états
     *
     * @return string
     */
    protected function _getStatetrad(): string
    {
        if (empty($this->_fields['state'])) {
            return '';
        }
        switch ($this->_fields['state']) {
            case TechnicalArchiveUnitsTable::S_AVAILABLE:
                return __dx('technical_archive_unit', 'state', "available");
            case TechnicalArchiveUnitsTable::S_DESTROYING:
                return __dx('technical_archive_unit', 'state', "destroying");
        }
        return $this->_fields['state'];
    }
}
