<?php

/**
 * Asalae\Model\Entity\OutgoingTransfer
 */

namespace Asalae\Model\Entity;

use Asalae\Exception\GenericException;
use Asalae\Model\Table\OutgoingTransferRequestsTable;
use Asalae\Model\Table\OutgoingTransfersTable;
use Asalae\Utility\SedaValidator;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\DOMUtility;
use Cake\Core\Configure;
use Cake\I18n\Date as CakeDate;
use Cake\Utility\Hash;
use Cake\View\ViewBuilder;
use Datacompressor\Utility\DataCompressor;
use DateTime;
use DateTimeInterface;
use DOMDocument;
use DOMElement;
use Exception;

/**
 * Entité de la table outgoing_transfers
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OutgoingTransfer extends Entity
{
    /**
     * Champs virtuels
     * @var array
     */
    protected array $_virtual = [
        'statetrad',
        'deletable',
    ];

    /**
     * Traductions des états
     * @return string
     */
    protected function _getStatetrad(): string
    {
        $state = $this->_fields['state'] ?? '';
        switch ($state) {
            case OutgoingTransfersTable::S_CREATING:
                $state = __dx('outgoing-transfer', 'state', "création");
                break;
            case OutgoingTransfersTable::S_READY_TO_SEND:
                $state = __dx(
                    'outgoing-transfer',
                    'state',
                    "prêt à être envoyé"
                );
                break;
            case OutgoingTransfersTable::S_SENT:
                $state = __dx(
                    'outgoing-transfer',
                    'state',
                    "envoyé au SAE destinataire"
                );
                break;
            case OutgoingTransfersTable::S_RECEIVED:
                $state = __dx(
                    'outgoing-transfer',
                    'state',
                    "reçu par le SAE destinataire"
                );
                break;
            case OutgoingTransfersTable::S_ACCEPTED:
                $state = __dx('outgoing-transfer', 'state', "acceptée");
                break;
            case OutgoingTransfersTable::S_REJECTED:
                $state = __dx('outgoing-transfer', 'state', "rejetée");
                break;
            case OutgoingTransfersTable::S_ERROR:
                $state = __dx('outgoing-transfer', 'state', "en erreur");
                break;
            case OutgoingTransfersTable::S_DELETED:
                $state = __dx('outgoing-transfer', 'state', "supprimé");
                break;
        }
        return $state;
    }

    /**
     * Génère le fichier xml issue de l'entité
     * @return DOMDocument
     * @throws GenericException|VolumeException
     */
    public function generateDom(): DOMDocument
    {
        $required = [
            'outgoing_transfer_request.archival_agency_identifier',
            'outgoing_transfer_request.archival_agency_name',
            'archive_unit.archive.description_xml_archive_file',
            'archive_unit.archive.transferring_agency',
            'created',
            'identifier',
        ];
        foreach ($required as $r) {
            if (Hash::get($this->_fields, $r) === null) {
                throw new GenericException(
                    'missing required data (' . $r . ')'
                );
            }
        }

        /** @var DescriptionXmlArchiveFile $archiveFile */
        $archiveFile = Hash::get(
            $this->_fields,
            'archive_unit.archive.description_xml_archive_file'
        );
        $util = $archiveFile->getDomUtilWithoutDestructedBinaries();

        $viewBuilder = new ViewBuilder();
        switch ($util->namespace) {
            case NAMESPACE_SEDA_02:
                $viewBuilder->setTemplate('/OutgoingTransfers/xml/seda_0.2');
                $this->modsSeda02($util);
                break;
            case NAMESPACE_SEDA_10:
                $viewBuilder->setTemplate('/OutgoingTransfers/xml/seda_1.0');
                $this->modsSeda10($util);
                break;
            case NAMESPACE_SEDA_21:
                $viewBuilder->setTemplate('/OutgoingTransfers/xml/seda_2.1');
                $this->modsSeda21($util);
                break;
            case NAMESPACE_SEDA_22:
                $viewBuilder->setTemplate('/OutgoingTransfers/xml/seda_2.2');
                $this->modsSeda21($util);
                break;
        }

        $identifier = $this->getEscaped(
            'outgoing_transfer_request.transferring_agency_identifier'
        );
        if ($identifier) {
            $transferring_agency_identifier = $identifier;
            $transferring_agency_name = $this->getEscaped(
                'outgoing_transfer_request.transferring_agency_name'
            );
        } else {
            $transferring_agency_identifier = $this->getEscaped(
                'archive_unit.archive.transferring_agency.identifier'
            );
            $transferring_agency_name = $this->getEscaped(
                'archive_unit.archive.transferring_agency.name'
            );
        }

        /** @var DateTime $date */
        $viewBuilder->setLayout('ajax');
        $date = Hash::get($this, 'created');
        $viewBuilder->setVars(
            [
                'transfer_comment' => $this->getEscaped('comment'),
                'transfer_date' => ($date instanceof DateTimeInterface || $date instanceof CakeDate)
                    ? $date->format(DateTimeInterface::RFC3339)
                    : $date,
                'transfer_identifier' => $this->getEscaped('identifier'),
                'agreement' => $this->getEscaped(
                    'outgoing_transfer_request.agreement_identifier'
                ),// v2.1
                'archival_agency_identifier' => $this->getEscaped(
                    'outgoing_transfer_request.archival_agency_identifier'
                ),
                'archival_agency_name' => $this->getEscaped(
                    'outgoing_transfer_request.archival_agency_name'
                ),
                'transferring_agency_identifier' => $transferring_agency_identifier,
                'transferring_agency_name' => $transferring_agency_name,
                'archive' => $util->dom->documentElement,
            ]
        );
        $dom = new DOMDocument();
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $dom->loadXML($viewBuilder->build()->render());
        if (!SedaValidator::validate($dom)) {
            throw new GenericException('schema validation failed');
        }
        return $dom;
    }

    /**
     * Ajoute/modifi le xml en fonction du outgoing_transfer
     * @param DOMUtility $util
     * @throws Exception
     */
    private function modsSeda02(DOMUtility $util)
    {
        $agreement = $util->xpath->query('ns:ArchivalAgreement')->item(0);
        $agreementValue = Hash::get(
            $this->_fields,
            'outgoing_transfer_request.agreement_identifier'
        );
        if ($agreementValue) {
            if (!$agreement) {
                $util->appendChild(
                    $util->dom->documentElement,
                    $util->createElement(
                        'ArchivalAgreement',
                        $agreementValue
                    )
                );
            } else {
                $util::setValue($agreement, $agreementValue);
            }
        }

        $profile = $util->xpath->query('ns:ArchivalProfile')->item(0);
        $profileValue = Hash::get(
            $this->_fields,
            'outgoing_transfer_request.profile_identifier'
        );
        if ($profileValue) {
            if (!$profile) {
                $util->appendChild(
                    $util->dom->documentElement,
                    $util->createElement(
                        'ArchivalProfile',
                        $profileValue
                    )
                );
            } else {
                $util::setValue($profile, $profileValue);
            }
        }

        $service = $util->xpath->query('ns:ServiceLevel')->item(0);
        $serviceValue = Hash::get(
            $this->_fields,
            'outgoing_transfer_request.service_level_identifier'
        );
        if ($serviceValue) {
            if (!$service) {
                $util->appendChild(
                    $util->dom->documentElement,
                    $util->createElement(
                        'ServiceLevel',
                        $serviceValue
                    )
                );
            } else {
                $util::setValue($service, $serviceValue);
            }
        }

        $oas = $util->dom->getElementsByTagName('OriginatingAgency');
        $oaIdentifierValue = Hash::get(
            $this->_fields,
            'outgoing_transfer_request.originating_agency_identifier'
        );
        $oaNameValue = Hash::get(
            $this->_fields,
            'outgoing_transfer_request.originating_agency_name'
        );
        if ($oaIdentifierValue && $oaNameValue) {
            /** @var DOMElement $oa */
            foreach ($oas as $oa) {
                $identication = $oa->getElementsByTagName('Identification')
                    ->item(0);
                if ($identication) {
                    $util::setValue($identication, $oaIdentifierValue);
                }
                $name = $oa->getElementsByTagName('Name')->item(0);
                if ($name) {
                    $util::setValue($name, $oaNameValue);
                }
            }
            $firstContent = $util->node('ns:ContentDescription');
            if (
                $firstContent && (!$util->node(
                    'ns:OriginatingAgency',
                    $firstContent
                ))
            ) {
                $oa = $util->appendChild(
                    $firstContent,
                    $util->createElement('OriginatingAgency')
                );
                $util->appendChild(
                    $oa,
                    $util->createElement('Identification', $oaIdentifierValue)
                );
                $util->appendChild(
                    $oa,
                    $util->createElement('Name', $oaNameValue)
                );
            }
        }
    }

    /**
     * Ajoute/modifi le xml en fonction du outgoing_transfer
     * @param DOMUtility $util
     * @throws Exception
     */
    private function modsSeda10(DOMUtility $util)
    {
        $this->modsSeda02($util);
    }

    /**
     * Ajoute/modifie le xml en fonction du outgoing_transfer
     * @param DOMUtility $util
     * @throws Exception
     */
    private function modsSeda21(DOMUtility $util)
    {
        $managementMetadata = $util->xpath->query('ns:ManagementMetadata')
            ->item(0);
        if (!$managementMetadata) {
            $managementMetadata = $util->createElement('ManagementMetadata');
            $util->appendChild(
                $util->dom->documentElement,
                $managementMetadata
            );
        }
        $profileValue = Hash::get(
            $this->_fields,
            'outgoing_transfer_request.profile_identifier'
        );
        if ($profileValue) {
            $profile = $util->xpath->query(
                'ns:ArchivalProfile',
                $managementMetadata
            )->item(0);
            if (!$profile) {
                $util->appendChild(
                    $managementMetadata,
                    $util->createElement(
                        'ArchivalProfile',
                        $profileValue
                    )
                );
            } else {
                $util::setValue($profile, $profileValue);
            }
        }

        $serviceValue = Hash::get(
            $this->_fields,
            'outgoing_transfer_request.service_level_identifier'
        );
        if ($serviceValue) {
            $service = $util->xpath->query('ns:ServiceLevel', $managementMetadata)->item(0);
            if (!$service) {
                $util->appendChild(
                    $managementMetadata,
                    $util->createElement(
                        'ServiceLevel',
                        $serviceValue
                    )
                );
            } else {
                $util::setValue($service, $serviceValue);
            }
        }

        $oas = $util->dom->getElementsByTagName('OriginatingAgency');
        $oaIdentifierValue = Hash::get(
            $this->_fields,
            'outgoing_transfer_request.originating_agency_identifier'
        );
        if ($oaIdentifierValue) {
            /** @var DOMElement $oa */
            foreach ($oas as $oa) {
                $identication = $oa->getElementsByTagName('Identifier')->item(
                    0
                );
                if ($identication) {
                    $util::setValue($identication, $oaIdentifierValue);
                }
                $meta = $oa->getElementsByTagName(
                    'OrganizationDescriptiveMetadata'
                )->item(0);
                if ($meta) {
                    $meta->parentNode->removeChild($meta);
                }
            }
            $firstContent = $util->node(
                'ns:DescriptiveMetadata/ns:ArchiveUnit[1]/ns:Content'
            );
            if (
                $firstContent && (!$util->node(
                    'ns:OriginatingAgency',
                    $firstContent
                ))
            ) {
                $oa = $util->appendChild(
                    $firstContent,
                    $util->createElement('OriginatingAgency')
                );
                $util->appendChild(
                    $oa,
                    $util->createElement('Identifier', $oaIdentifierValue)
                );
            }
        }
    }

    /**
     * Donne une version protégé du champ demandé
     * @param string $path
     * @return string
     */
    private function getEscaped(string $path)
    {
        return htmlspecialchars(
            Hash::get($this->_fields, $path),
            ENT_XML1
        );
    }

    /**
     * Zip les fichiers du transfert sortant
     * @return bool
     * @throws Exception
     */
    public function makeZip(): bool
    {
        return DataCompressor::compress(
            $this->getDataDirectory(),
            $this->_getZip()
        );
    }

    /**
     * Donne l'uri du dossier "data" d'un transfert sortant
     * @return string
     */
    public function getDataDirectory(): string
    {
        return Configure::read(
            'App.paths.data'
        ) . DS . 'outgoing-transfers' . DS . (int)$this->id;
    }

    /**
     * Donne le chemin vers le fichier xml du transfert sortant
     * @return string
     */
    protected function _getZip(): string
    {
        $identifier = urlencode($this->_fields['identifier'] ?? 'undefined');
        return Configure::read(
            'App.paths.data'
        ) . DS . 'outgoing-transfers' . DS . "$identifier.zip";
    }

    /**
     * Donne le chemin vers le fichier xml du transfert sortant
     * @return string
     */
    protected function _getXml(): string
    {
        $identifier = urlencode($this->_fields['identifier'] ?? 'undefined');
        return $this->getDataDirectory() . DS . 'message' . DS . "$identifier.xml";
    }

    /**
     * Donne le chemin vers les fichiers du transfert sortant
     * @return string
     */
    protected function _getAttachementsPath(): string
    {
        return $this->getDataDirectory() . DS . 'attachments';
    }

    /**
     * Cette entité peut être supprimée ?
     * @return bool
     */
    protected function _getDeletable()
    {
        $inError = ($this->_fields['state'] ?? '') === OutgoingTransfersTable::S_ERROR;
        $requestInError = Hash::get(
            $this->_fields,
            'outgoing_transfer_request.state'
        )
            === OutgoingTransferRequestsTable::S_TRANSFERS_ERRORS;
        return $inError && $requestInError;
    }
}
