<?php

/**
 * Asalae\Model\Entity\ArchivesRestitutionRequest
 */

namespace Asalae\Model\Entity;

use Cake\ORM\Entity;

/**
 * Entité de la table archives_restitution_requests
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchivesRestitutionRequest extends Entity
{
}
