<?php

/**
 * Asalae\MinkSuite\MinkArchive
 */

namespace Asalae\MinkSuite;

use Asalae\Exception\GenericException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\MinkSuite\MinkCase;
use AsalaeCore\Utility\Exec;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * gestion des archives pour mink
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MinkArchive
{
    /**
     * Créé une archive et retourne son entité
     * @param array $params ['user_id' => (int)1, 'message_version' => (float)2.1]
     * @return EntityInterface
     * @throws Exception
     */
    public static function create(array $params = []): EntityInterface
    {
        $params += [
            'user_id' => 1,
            'message_version' => 2.1,
        ];
        /** @var Exec $Exec */
        $Exec = Utility::get('Exec');
        $result = $Exec->command(
            sprintf(
                '%s archive filler %d 1%s',
                MinkCase::DOCKER_EXEC,
                $params['user_id'],
                $params['message_version'] === 2.1 ? ' --v2.1' : ''
            )
        );
        if ($result->code !== 0) {
            throw new GenericException($result->stderr);
        }
        return TableRegistry::getTableLocator()->get('Archives')
            ->find()
            ->orderByDesc('Archives.id')
            ->contain(['Transfers'])
            ->firstOrFail();
    }

    /**
     * Supprime une archive
     * @param EntityInterface $archive
     * @return bool
     * @throws Exception
     */
    public static function delete(EntityInterface $archive): bool
    {
        return self::deleteById($archive->id);
    }

    /**
     * Supprime une archive
     * @param int $archive_id
     * @return bool
     * @throws Exception
     */
    public static function deleteById(int $archive_id): bool
    {
        /** @var Exec $Exec */
        $Exec = Utility::get('Exec');
        $conditions = json_encode(['Archives.id' => $archive_id]);
        $result = $Exec->command(
            sprintf(
                '%s delete transfers --conditions-json %s --headless',
                MinkCase::DOCKER_EXEC,
                escapeshellarg($conditions)
            )
        );
        if ($result->code !== 0) {
            return false;
        }
        return true;
    }
}
