<?php

/**
 * Asalae\Controller\CachableTrait
 */

namespace Asalae\Controller;

use AsalaeCore\Http\CallbackStream;
use Cake\Http\Response;

/**
 * Cache de fichier
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AppController
 */
trait CachableTrait
{
    /**
     * Permet de lire un fichier hors du webroot et permet la mise en cache par
     * le navigateur
     *
     * @param string $path
     * @return Response
     */
    private function renderCachable(string $path)
    {
        $duration = 3600 * 24 * 365;
        $lastModFile = filemtime($path);
        $date = gmdate(DATE_RFC1123, $lastModFile);
        $response = $this->getResponse()
            ->withHeader('Content-Type', mime_content_type($path))
            ->withHeader('Cache-control', 'max-age=' . $duration)
            ->withHeader('Expires', gmdate(DATE_RFC1123, time() + $duration))
            ->withHeader('Last-Modified', $date)
            ->withHeader('Etag', $lastModFile);

        $useCache = (!empty($_SERVER['HTTP_IF_NONE_MATCH']) && $_SERVER['HTTP_IF_NONE_MATCH'] === $lastModFile)
            || (!empty($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] === $date);

        return $useCache
            ? $response->withStatus(304)
            : $response->withHeader(
                'Content-Disposition',
                'attachment; filename="' . basename($path) . '"'
            )
                ->withHeader('Content-Length', $filesize = filesize($path))
                ->withHeader('File-Size', $filesize)
                ->withBody(
                    new CallbackStream(
                        function () use ($path) {
                            ob_end_flush();
                            ob_implicit_flush();
                            set_time_limit(0);
                            readfile($path);
                        }
                    )
                );
    }
}
