<?php

/**
 * Asalae\Controller\TransfersController
 */

namespace Asalae\Controller;

use Asalae\Controller\Traits\TransfersAddCompressedTrait;
use Asalae\Model\Entity\EntityWithSessionDataInterface;
use Asalae\Model\Table\AccessRuleCodesTable;
use Asalae\Model\Table\AppraisalRuleCodesTable;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\UsersTable;
use Asalae\Utility\JstreeSeda;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\FilterComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\JstableComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Http\Client\Adapter\StreamLargeFilesCurl;
use AsalaeCore\Utility\Antivirus\AntivirusInterface;
use AsalaeCore\Utility\Antivirus\Clamav;
use AsalaeCore\Utility\DatabaseUtility;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\LimitBreak;
use AsalaeCore\Utility\PreControlMessages;
use Asalae\Exception\AfterDeleteException;
use Asalae\Exception\GenericException;
use Asalae\Model\Entity\Transfer;
use Asalae\Model\Entity\TransferAttachment;
use Asalae\Model\Table\AgreementsTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\KillablesTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\ProfilesTable;
use Asalae\Model\Table\ServiceLevelsTable;
use Asalae\Model\Table\TransferAttachmentsTable;
use Asalae\Model\Table\TransferErrorsTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationProcessesTable;
use Asalae\Utility\TransferAnalyse;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Client;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\ServiceUnavailableException;
use Cake\Http\Response as CakeResponse;
use Cake\Http\Response;
use Cake\I18n\Number;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use Datacompressor\Exception\DataCompressorException;
use DateInvalidTimeZoneException;
use DOMAttr;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Datacompressor\Utility\DataCompressor;
use DateTime;
use Exception;
use FileValidator\Utility\FileValidator;
use Laminas\Diactoros\UploadedFile;
use Libriciel\Filesystem\Utility\Filesystem;
use Pheanstalk\Pheanstalk;
use Psr\Http\Message\ResponseInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ReflectionException;
use Seda2Pdf\Seda2Pdf;
use SplFileInfo;
use Transliterator;
use ZMQSocketException;

/**
 * Transferts d'archives
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AgreementsTable          Agreements
 * @property CountersTable            Counters
 * @property EventLogsTable           EventLogs
 * @property FileuploadsTable         Fileuploads
 * @property KillablesTable           Killables
 * @property OrgEntitiesTable         OrgEntities
 * @property ProfilesTable            Profiles
 * @property ServiceLevelsTable       ServiceLevels
 * @property TransferAttachmentsTable TransferAttachments
 * @property TransferErrorsTable      TransferErrors
 * @property TransfersTable           Transfers
 * @property ValidationChainsTable    ValidationChains
 * @property ValidationProcessesTable ValidationProcesses
 */
class TransfersController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;
    use TransfersAddCompressedTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'transfers-table';
    public const string TABLE_PREPARE_INDEX = 'transfers-prepare-table';
    public const string TABLE_SENT_INDEX = 'transfers-sent-table';
    public const string TABLE_MY_INDEX = 'transfers-my-table';
    public const string TABLE_ALL_INDEX = 'transfers-all-table';
    public const string TABLE_MY_ENTITY_INDEX = 'transfers-my-entity-table';
    public const string TABLE_MY_ACCEPTED_INDEX = 'transfers-my-accepted-table';
    public const string TABLE_MY_ENTITY_ACCEPTED_INDEX = 'transfers-my-accepted-table';
    public const string TABLE_MY_REJECTED_INDEX = 'transfers-my-rejected-table';
    public const string TABLE_MY_ENTITY_REJECTED_INDEX = 'transfers-my-rejected-table';
    public const string TABLE_UPLOAD = 'uploads-table';
    public const string MODAL_XML_TO_HTML = 'transfer-prepare-display-xml';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'Transfers.id' => 'desc',
        ],
    ];

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default' => [
                '*' => [
                    'seal' => 'archivalAgency',
                ],
                'post' => [
                    'function' => 'apiPost',
                ],
            ],
            'acknowledgement',
            'reply',
            'prepareChunked',
        ];
    }

    /**
     * Tableau de bord
     */
    public function index()
    {
        $prepCount = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.created_user_id' => $this->userId,
                    'Transfers.state' => TransfersTable::S_PREPARATING,
                ]
            )
            ->count();
        $this->set('prepCount', $prepCount);
        $sentCount = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.created_user_id' => $this->userId,
                    'Transfers.state IN' => [
                        TransfersTable::S_TIMESTAMPING,
                        TransfersTable::S_ANALYSING,
                        TransfersTable::S_CONTROLLING,
                    ],
                ]
            )
            ->count();
        $this->set('sentCount', $sentCount);
        $acceptedCount = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.created_user_id' => $this->userId,
                    'Transfers.state' => TransfersTable::S_ACCEPTED,
                ]
            )
            ->count();
        $this->set('acceptedCount', $acceptedCount);
        $rejectedCount = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.created_user_id' => $this->userId,
                    'Transfers.state' => TransfersTable::S_REJECTED,
                ]
            )
            ->count();
        $this->set('rejectedCount', $rejectedCount);

        $myCount = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.created_user_id' => $this->userId,
                ]
            )
            ->count();
        $this->set('myCount', $myCount);
    }

    /**
     * Formulaire d'ajout d'un transfert par upload de bordereau étape 1
     */
    public function addByUpload()
    {
        /** @var Transfer $entity */
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $entity = $Transfers->newEntity([], ['validate' => false]);

        if ($this->getRequest()->is('post')) {
            Filesystem::begin();

            /** @var FileuploadsTable $Fileuploads */
            $Fileuploads = $this->fetchTable('Fileuploads');
            $file = $Fileuploads->get(
                $this->getRequest()->getData('fileuploads.id')
            );
            $uri = $file->get('path');
            $entity = $Transfers->newEntityFromXml($uri);
            $Transfers->patchEntity(
                $entity,
                $entity->toArray() + [
                    'state' => $Transfers->initialState,
                    'is_modified' => false,
                    'created_user_id' => $this->userId,
                    'tmp_xml' => $uri,
                    'files_deleted' => false,
                ]
            );
            $entity->set('uri', $uri);
            if (!PreControlMessages::isValid($uri)) {
                $entity->setError(
                    'uri',
                    PreControlMessages::getLastErrorMessage()
                );
            }
            $Transfers->transition($entity, TransfersTable::T_PREPARE);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($Transfers->save($entity)) {
                /** @var EventLogsTable $EventLogs */
                $EventLogs = $this->fetchTable('EventLogs');
                $entry = $EventLogs->newEntry(
                    'transfer_add',
                    'success',
                    __(
                        "Prise en compte du transfert ''{0}''",
                        $entity->get('transfer_identifier')
                    ),
                    $this->userId,
                    $entity
                );
                $entry->set(
                    'in_lifecycle',
                    false
                ); // ajouté dans worker archive_management
                $EventLogs->saveOrFail($entry);
                if (!is_file($entity->get('xml'))) {
                    Filesystem::rename($uri, $entity->get('xml'));
                }
                Filesystem::commit();
                $this->Modal->success();
                $this->Modal->step(
                    'addTransferUploadStep2(' . $entity->get('id') . ')'
                );
                Utility::get('Notify')->emit(
                    'new_transfer',
                    [
                        'user_id' => $this->userId,
                        'sa_id' => $this->archivalAgencyId,
                        'count' => 1,
                    ]
                );
            } else {
                Filesystem::rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
    }

    /**
     * Formulaire d'ajout d'un transfert étape 1 (initialisation du bordereau)
     */
    public function add1()
    {
        /** @var Transfer $entity */
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $entity = $Transfers->newEntity([], ['validate' => false]);
        $request = $this->getRequest();

        if ($request->is('post')) {
            Filesystem::begin();
            $data = [
                'state' => $Transfers->initialState,
                'is_modified' => false,
                'filename' => uniqid('transfer-') . '.xml',
                'created_user_id' => $this->userId,
                'archival_agency_id' => $this->archivalAgencyId,
                'manual_identifier' => (bool)$request->getData('transfer_identifier'),
                'originating_agency_id' => $request->getData('originating_agency_id')
                        ?: $request->getData('transferring_agency_id'),
                'files_deleted' => false,
            ] + $request->getData();
            $Transfers->patchEntity($entity, $data, ['validate' => 'create']);
            if (!$entity->get('transfer_identifier') && empty($entity->getErrors())) {
                /** @var CountersTable $Counters */
                $Counters = $this->fetchTable('Counters');
                /** @var TransfersTable $Transfers */
                $Transfers = $this->fetchTable('Transfers');
                $generated = $Counters->next(
                    $data['archival_agency_id'],
                    'ArchiveTransfer',
                    [],
                    function ($generated) use ($data, $Transfers) {
                        $cond = [
                            'transfer_identifier' => $generated,
                            'archival_agency_id' => $this->archivalAgencyId,
                        ];
                        return !$Transfers->exists($cond);
                    }
                );
                $entity->set('transfer_identifier', $generated);
            }
            $uri = sys_get_temp_dir() . DS . uniqid() . $entity->get('filename');
            Filesystem::dumpFile($uri, $entity->generateXml());
            $entity->set(
                'uri',
                $uri
            ); // pour affichage d'une erreur dans le formulaire
            if (!PreControlMessages::isValid($uri)) {
                $entity->setError(
                    'uri',
                    PreControlMessages::getLastErrorMessage()
                );
            }
            $Transfers->transition($entity, TransfersTable::T_PREPARE);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($Transfers->save($entity)) {
                /** @var EventLogsTable $EventLogs */
                $EventLogs = $this->fetchTable('EventLogs');
                $entry = $EventLogs->newEntry(
                    'transfer_add',
                    'success',
                    __(
                        "Prise en compte du transfert ''{0}''",
                        $entity->get('transfer_identifier')
                    ),
                    $this->userId,
                    $entity
                );
                $entry->set(
                    'in_lifecycle',
                    false
                ); // ajouté dans worker archive_management
                $EventLogs->saveOrFail($entry);
                if (!is_file($entity->get('xml'))) {
                    Filesystem::rename($uri, $entity->get('xml'));
                }
                Filesystem::commit();
                $this->Modal->success();
                $this->Modal->step(
                    'addTransferStep2(' . $entity->get('id') . ')'
                );
                Utility::get('Notify')->emit(
                    'new_transfer',
                    [
                        'user_id' => $this->userId,
                        'sa_id' => $data['archival_agency_id'],
                        'count' => 1,
                    ]
                );
            } else {
                Filesystem::rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);

        // Options
        $this->set(
            'transferringAgencies',
            $this->getAgenciesOpts(OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES)
        );
        $this->set(
            'originating_agencies',
            $this->getAgenciesOpts(OrgEntitiesTable::CODE_ORIGINATING_AGENCIES)
        );
        $agreements = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('sealList')
            ->where(['Agreements.active' => true])
            ->orderBy(['Agreements.name' => 'asc']);
        $this->set('agreements', $agreements->all());
        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('sealList')
            ->where(['Profiles.active' => true])
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles->all());
        $service_levels = $this->Seal->archivalAgency()
            ->table('ServiceLevels')
            ->find('sealList')
            ->where(['ServiceLevels.active' => true])
            ->orderBy(['ServiceLevels.name' => 'asc']);
        $this->set('service_levels', $service_levels->all());
        $this->set('my_org_entity_id', $this->orgEntityId);
        /** @var AccessRuleCodesTable $AccessRuleCodes */
        $AccessRuleCodes = $this->fetchTable('AccessRuleCodes');
        $this->set('seda1AccessRuleCodes', $AccessRuleCodes->getRuleCodes($this->archivalAgencyId, 'seda1.0'));
        $this->set('seda2AccessRuleCodes', $AccessRuleCodes->getRuleCodes($this->archivalAgencyId, 'seda2.1'));
        /** @var AppraisalRuleCodesTable $AppraisalRuleCodes */
        $AppraisalRuleCodes = $this->fetchTable('AppraisalRuleCodes');
        $this->set('seda1AppraisalRuleCodes', $AppraisalRuleCodes->getRuleCodes($this->archivalAgencyId, 'seda1.0'));
        $this->set('seda2AppraisalRuleCodes', $AppraisalRuleCodes->getRuleCodes($this->archivalAgencyId, 'seda2.1'));
        $this->set('finals', Seda10Schema::getXsdOptions('appraisal_code'));
        $this->set('description_levels', Seda10Schema::getXsdOptions('level'));
        $this->set(
            'message_versions',
            [
                'seda1.0' => __dx('transfer', 'message_version', "seda1.0"),
                'seda2.1' => __dx('transfer', 'message_version', "seda2.1"),
                'seda2.2' => __dx('transfer', 'message_version', "seda2.2"),
            ]
        );
    }

    /**
     * Options pour le select d'un service
     * @param array $codes
     * @return array
     * @throws ReflectionException
     */
    private function getAgenciesOpts(array $codes): array
    {
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        return $OrgEntities->listOptions([], $this->Seal->orgEntityChilds())
            ->where(
                [
                    'TypeEntities.code IN' => $codes,
                    'OrgEntities.active' => true,
                ]
            )
            ->disableHydration()
            ->all()
            ->toArray();
    }

    /**
     * Etape 2 du transfert par upload de bordereau
     * @param string $id
     * @throws Exception
     */
    public function addByUpload2(string $id)
    {
        $this->set('isUpload', true);

        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $xml = $Transfers->find()
            ->select(['id', 'filename', 'files_deleted'])
            ->where(['Transfers.id' => $id])
            ->firstOrFail()
            ->get('xml');
        $domutil = DOMUtility::load($xml);
        $filenames = array_map(
            fn() => true, // converti [filename => domelement] en [filename => true]
            ArchiveBinariesTable::listBinariesInDocument($domutil)
        );
        $this->set('countDocuments', count($filenames));
        ksort($filenames);

        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        $query = $TransferAttachments->find()
            ->select(['filename'])
            ->where(['transfer_id' => $id])
            ->disableHydration();
        $matchingAttachments = 0;

        foreach ($query as $attachment) {
            $filename = $attachment['filename'];
            if (isset($filenames[$filename])) {
                $matchingAttachments++;
                unset($filenames[$filename]);
            }
        }
        $missingAttachments = [];
        foreach ($filenames as $filename => $exist) {
            $missingAttachments[] = $filename;
            if (count($missingAttachments) >= 5) {
                break;
            }
        }
        $this->set('countUploadsConform', $matchingAttachments);
        $this->set('missingAttachments', $missingAttachments);
        $this->set('filenames', $filenames);

        $this->add2($id, false);
    }

    /**
     * Formulaire d'ajout d'un transfert étape 2 (fichiers)
     * @param string $id
     * @param bool   $generateTree
     * @throws Exception
     */
    public function add2(string $id, $generateTree = true)
    {
        $this->_add2($id, $generateTree);
    }

    /**
     * Affiche le bordereau xml en html
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function displayXml(string $id)
    {
        $conditions = ['Transfers.id' => $id];
        // si on ne viens pas d'un email, condition sur le service d'archives
        if (
            !(empty($this->archivalAgencyId)
            && Hash::get(
                $this->getRequest()->getAttribute('identity'),
                'url_id'
            ))
        ) {
            $query = $this->Seal->archivalAgency()
                ->table('Transfers')
                ->find('seal');
        } else {
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $query = $Transfers->find();
        }
        $transfer = $query
            ->where($conditions)
            ->firstOrFail();
        return $this->getResponse()->withStringBody($transfer->get('HTML'));
    }

    /**
     * Affiche les details du transfert
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $entity = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(['Transfers.id' => $id])
            ->contain(
                [
                    'ArchivalAgencies',
                    'TransferringAgencies',
                    'Agreements',
                    'Profiles',
                    'CreatedUsers',
                    'TransferAttachments' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q
                            ->orderBy(['TransferAttachments.filename' => 'asc'])
                            ->limit($AjaxPaginatorComponent->getConfig('limit'));
                    },
                    'ServiceLevels',
                    'TransferErrors' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q
                            ->orderBy(
                                [
                                    'TransferErrors.level' => 'asc',
                                    'TransferErrors.id' => 'asc',
                                ]
                            )
                            ->limit($AjaxPaginatorComponent->getConfig('limit'));
                    },
                    'ValidationProcesses' => [
                        'ValidationChains',
                        'CurrentStages' => [
                            'ValidationActors',
                        ],
                        'ValidationHistories' => [
                            'sort' => [
                                'ValidationHistories.created' => 'asc',
                                'ValidationHistories.id' => 'asc',
                            ],
                            'ValidationActors' => [
                                'Users',
                                'ValidationStages',
                            ],
                        ],
                    ],
                ]
            )
            ->firstOrFail();

        $query = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(['Transfers.id' => $id])
            ->contain(['ArchivalAgencies', 'TransferringAgencies', 'OriginatingAgencies']);
        $query->firstOrFail();

        $this->set('entity', $entity);
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        $countUploads = $TransferAttachments->find()
            ->where(['TransferAttachments.transfer_id' => $id])
            ->count();
        $this->set('countUploads', $countUploads);
        /** @var TransferErrorsTable $TransferErrors */
        $TransferErrors = $this->fetchTable('TransferErrors');
        $countErrors = $TransferErrors->find()
            ->where(['TransferErrors.transfer_id' => $id])
            ->count();
        $this->set('countErrors', $countErrors);
        $this->set('id', $id);

        $codes = $TransferErrors->find()
            ->select(['TransferErrors.code'])
            ->where(['TransferErrors.transfer_id' => $id])
            ->distinct(['TransferErrors.code'])
            ->all()
            ->toArray();
        $this->set('errorCodes', Hash::combine($codes, '{n}.code', '{n}.code'));
        $AjaxPaginatorComponent->setViewPaginator(
            Hash::get($entity, 'transfer_attachments', []),
            $countUploads,
        );
    }

    /**
     * Action d'édition d'un transfert
     * @param string $id
     * @param mixed  ...$path
     * @return Response
     * @throws Exception
     */
    public function edit(string $id, ...$path)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var Transfer $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.id' => $id,
                    'Transfers.state IN' => [
                        TransfersTable::S_PREPARATING,
                        TransfersTable::S_VALIDATING,
                    ],
                ]
            )
            ->contain(['TransferLocks'])
            ->firstOrFail();

        $lock = $entity->get('transfer_lock');
        if ($entity->get('state') !== TransfersTable::S_PREPARATING && $lock) {
            $this->fetchTable('TransferLocks')->delete($lock);
            $entity->unset('transfer_lock');
        }

        if (!is_file($entity->get('xml'))) {
            throw new NotFoundException(
                __("Le fichier xml du transfert n'a pas été trouvé")
            );
        }

        // on prépare les xpaths des attachments
        if ($this->getRequest()->is('get')) {
            /** @var TransferAttachmentsTable $TransferAttachments */
            $TransferAttachments = $this->fetchTable('TransferAttachments');
            $domutil = DOMUtility::load($entity->get('xml'));
            $query = $TransferAttachments->find()
                ->select(['id', 'filename'])
                ->where(['transfer_id' => $id, 'xpath IS' => null])
                ->disableHydration();
            $count = $query->count();
            if ($count) {
                LimitBreak::setTimeLimit($count / 100);// 1s / 1000 fichiers
                $TransferAttachments->updateMissingXpaths($query, $domutil);
            }
        }
        $sessionToken = $this->getRequest()->getSession()->read(
            'Session.token'
        );

        $tempXml = $entity->getTempXml($this->userId);
        $serializedMeta = $entity->getSerializedMeta($this->userId);
        if ($this->getRequest()->getQuery('renew')) {
            if (is_file($tempXml)) {
                unlink($tempXml);
            }
            if (is_file($serializedMeta)) {
                unlink($serializedMeta);
            }
        }
        if (!file_exists($tempXml)) {
            $size = filesize($entity->get('xml'));
            LimitBreak::setTimeLimit($size / 10000);
            LimitBreak::setMemoryLimit($size * 10);
            copy($entity->get('xml'), $tempXml);
            if (file_exists($serializedMeta)) {
                unlink($serializedMeta);
            }
        }
        $size = filesize($tempXml);
        LimitBreak::setTimeLimit($size / 10000);
        LimitBreak::setMemoryLimit($size * 10);
        if (!file_exists($serializedMeta)) {
            $meta = [
                'isDirty' => false,
                'toDelete' => [],
                'session' => $sessionToken,
                'created' => $date = new CakeDateTime(),
                'modified' => $date,
            ];
            file_put_contents($serializedMeta, serialize($meta));
        } else {
            $meta = unserialize(file_get_contents($serializedMeta));
        }
        if ($this->getRequest()->is('delete')) {
            unlink($tempXml);
            unlink($serializedMeta);
            $logs = $entity->getEditedLogs($this->userId);
            if (is_file($logs)) {
                unlink($logs);
            }
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            return $this->renderDataToJson("done");
        }
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        if ($this->getRequest()->is('post')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $force = $this->getRequest()->getQuery('force');
            $errors = $force ? null : $entity->validateFile($tempXml);
            if (empty($errors)) {
                Filesystem::begin();
                Filesystem::rename($tempXml, $entity->get('xml'), true);
                /** @var TransfersTable $Transfers */
                $Transfers = $this->fetchTable('Transfers');
                $Transfers->applyChanges($entity);
                $filesToDelete = $meta['toDelete'] ?? [];
                foreach ($filesToDelete as $attachment_id) {
                    $file = $TransferAttachments->get($attachment_id);
                    $TransferAttachments->deleteOrFail($file);
                }
                $dir = AbstractGenericMessageForm::getDataDirectory(
                    $entity->get('id')
                );
                $this->removeEmptySubFolders($dir . DS . 'attachments');
                // force la sauvegarde pour set le champ "modified" (rendu pdf)
                $entity->setDirty('transfer_identifier');
                $entity->set('is_conform');

                $Transfers->saveOrFail($entity);
                Filesystem::remove($serializedMeta);
                $logs = $entity->getEditedLogs($this->userId);
                if (is_file($logs)) {
                    $this->updateAttachmentWithLogs($entity, $logs);
                    Filesystem::remove($logs);
                }
                Filesystem::commit();
                $this->Modal->success();
                /** @var Transfer $entity */
                $entity = $Transfers->find()
                    ->where(['Transfers.id' => $entity->get('id')])
                    ->contain(['ArchivalAgencies', 'TransferringAgencies', 'OriginatingAgencies'])
                    ->firstOrFail();
                $entity->appendSessionData($this->user);
                return $this->renderDataToJson($entity->toArray());
            } else {
                $this->Modal->fail();
                return $this->renderDataToJson($errors);
            }
        }

        $this->set('id', $id);
        $this->set('entity', $entity);
        $uploadId = 'transfer-edit-upload-table';
        $this->set('uploadId', $uploadId);
        $tableId = 'transfer-edit-files-table';
        $this->set('tableId', $tableId);

        $listFilesTemp = $entity->listFiles($tempXml);
        TransferAttachment::$listFiles[$id] = $listFilesTemp;
        $data = $TransferAttachments->find()
            ->where(['transfer_id' => $id])
            ->orderBy(
                [
                    'TransferAttachments.xpath IS NULL' => 'desc',
                    'TransferAttachments.filename' => 'asc',
                ]
            )
            ->limit($AjaxPaginatorComponent->getConfig('limit'))
            ->all()
            ->toArray();
        $this->set('data', $data);

        $countAttachments = $TransferAttachments->find()
            ->where(['TransferAttachments.transfer_id' => $id])
            ->count();
        $this->set('countAttachments', $countAttachments);

        $this->set('path', $path);
        $this->set('meta', $meta);
        $this->set('attachmentsToDelete', $meta['toDelete'] ?? []);
        $this->set('isDirty', $meta['isDirty'] ?? false);

        $target = $id . '/' . implode('/', $path);
        $refreshJsBase = 'actionEditTransfer';
        $refreshJsCallback = "$refreshJsBase('$target')";
        $this->set('refreshJsBase', $refreshJsBase);
        $this->set('refreshJsCallback', $refreshJsCallback);
        $AjaxPaginatorComponent->setViewPaginator($data, $countAttachments);
    }

    /**
     * Supprime les dossiers vide
     * @link https://stackoverflow.com/questions/1833518/remove-empty-subfolders-with-php
     * @param string $path
     * @return bool
     */
    private function removeEmptySubFolders($path)
    {
        if (!is_dir($path)) {
            return null;
        }
        $empty = true;
        foreach (glob($path . DS . "{*,.[!.]*,..?*}", GLOB_BRACE) as $file) {
            $empty &= is_dir($file) && $this->removeEmptySubFolders($file);
        }
        return $empty && @rmdir($path);
    }

    /**
     * Met à jour les xpath des attachments en parcourant les logs de modifications
     * @param Transfer $entity
     * @param string   $logs
     * @throws Exception
     */
    private function updateAttachmentWithLogs(Transfer $entity, string $logs)
    {
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        $util = DOMUtility::load($entity->get('xml'));
        $handle = fopen($logs, 'r');
        while (!feof($handle)) {
            $buffer = fgets($handle);
            if (!trim($buffer)) {
                continue;
            }
            [$action, $value] = explode(':', rtrim($buffer, PHP_EOL), 2);
            switch ($action) {
                case 'delete':
                    $TransferAttachments->updateAll(
                        ['xpath' => null],
                        [
                            'transfer_id' => $entity->id,
                            'xpath LIKE' => $value . '/%',
                        ]
                    );
                    break;
                case 'move':
                    $TransferAttachments->updateAll(
                        ['xpath' => null],
                        [
                            'transfer_id' => $entity->id,
                            'xpath LIKE' => dirname($value) . '/%',
                        ]
                    );
                    break;
                case 'detach':
                    $TransferAttachments->updateAll(
                        ['xpath' => null],
                        ['transfer_id' => $entity->id, 'filename' => $value]
                    );
                    break;
                case 'attach':
                    $node = ArchiveBinariesTable::findBinaryByFilename($util, $value);
                    if ($node) {
                        $TransferAttachments->updateAll(
                            ['xpath' => $util->getElementXpath($node, '')],
                            ['transfer_id' => $entity->id, 'filename' => $value]
                        );
                    }
                    break;
            }
        }
        fclose($handle);
    }

    /**
     * Donne le data jstree pour la description d'archive
     * @param string $id      transfer_id ou $base64Path
     * @param mixed  ...$path
     * @return Response
     * @throws Exception
     */
    public function getTree(string $id, ...$path)
    {
        $dom = new DOMDocument();
        if (is_numeric($id)) {
            /** @var Transfer $entity */
            $entity = $this->Seal->archivalAgency()
                ->table('Transfers')
                ->find('seal')
                ->where(['Transfers.id' => $id])
                ->firstOrFail();
            $tempXml = $entity->getTempXml($this->userId);
            if ($this->getRequest()->getQuery('tempfile')) {
                if (!$dom->load($tempXml)) {
                    throw new NotFoundException(__("Fichier temporaire manquant"));
                }
            } else {
                $dom->load($entity->get('xml'));
            }
        } else {
            $tempXml = $this->decodeXmlPath($id);
            $dom->load($tempXml);
        }
        $jstreeSeda = new JstreeSeda(
            $dom,
            'ArchiveTransfer',
            [
                'showHidden' => !is_numeric($id),
                'showAddable' => $this->getRequest()->getQuery('addable') ?? false,
            ]
        );
        $this->set('id', $id);
        $idNode = $this->getRequest()->getQuery('id');
        if ($idNode !== '#' && $idNode) {
            return $this->renderDataToJson($jstreeSeda->getByIdNode($idNode));
        } else {
            return $this->renderDataToJson($jstreeSeda->getJsTree(...$path));
        }
    }

    /**
     * Décode et contrôle $base64Path
     * @param string $base64Path
     * @return string
     */
    private function decodeXmlPath(string $base64Path): string
    {
        $encryptedPath = base64_decode(
            str_replace(['-', '_'], ['+', '/'], $base64Path)
        );
        $path = Security::decrypt(
            $encryptedPath,
            hash(UploadController::HASH_ALGO, UploadController::FILE_ENCRYPT_KEY)
        );
        if (strpos($path, DS . 'user_' . $this->userId . DS) === false) {
            throw new ForbiddenException();
        }
        return $path;
    }

    /**
     * Modifi une partie de l'xml du transfert
     * @param string $id
     * @param mixed  ...$path
     * @return Response
     * @throws Exception
     */
    public function partialEdit(string $id, ...$path)
    {
        $target = $id . '/' . implode('/', $path);
        if (is_numeric($id)) {
            /** @var Transfer $entity */
            $entity = $this->Seal->archivalAgency()
                ->table('Transfers')
                ->find('seal')
                ->where(['Transfers.id' => $id])
                ->firstOrFail();

            $tempXml = $entity->getTempXml($this->userId);
            LimitBreak::setMemoryLimit(filesize($tempXml) * 10);
            $refreshJsBase = 'actionEditTransfer';
        } else {
            $tempXml = $this->decodeXmlPath($id);
            LimitBreak::setMemoryLimit(filesize($tempXml) * 10);
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $entity = $Transfers->newEntityFromXml($tempXml);
            $refreshJsBase = 'repairTransfer';
        }
        $refreshJsCallback = "$refreshJsBase('$target')";
        $this->set('refreshJsBase', $refreshJsBase);
        $this->set('refreshJsCallback', $refreshJsCallback);

        $util = DOMUtility::load($tempXml);
        $oldXml = $util->dom->saveXML();
        $xpath = $util->xpath;
        $xpathExp = '/ns:' . implode('/ns:', $path);
        /** @var DOMElement $node */
        $node = $xpath->query($xpathExp)->item(0);

        $schemaPath = preg_replace('/\[\d+]/', '', implode('.', $path));
        $form = $entity->getForm(
            $this->superArchivist ? $this->archivalAgencyId : $this->orgEntityId,
            $schemaPath,
            $util->dom,
            $node
        );

        /** @var DOMAttr $value */
        $attributes = array_map(fn ($value) => $value->nodeValue, iterator_to_array($node->attributes));
        $this->set('attributes', $attributes);

        if ($this->getRequest()->is('post')) {
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $initialAttachement = $Transfers->extractFilename(
                $entity,
                $node
            );
            $data = $this->getRequest()->getData();
            if ($form->setEntity($entity)->execute($data)) {
                if ($entity->id) {
                    $this->logPartialEdit(
                        $entity,
                        $util,
                        $node,
                        $initialAttachement
                    );
                }
                $util->dom->save($tempXml);
                $this->setMetaIsDirty($entity, $oldXml, $util->dom);
                /** @use ModalComponent */
                $this->loadComponent('AsalaeCore.Modal');
                $this->Modal->success();
                return $this->renderDataToJson(implode('/', $path));
            }
        } else {
            $data = [];
            foreach ($form->getSchema()->fields() as $field) {
                $attr = null;
                if (strpos($field, '_@') !== false) {
                    [$field, $attr] = explode('_@', $field, 2);
                }
                if (strpos($field, '_#') !== false) {
                    [$field, $i] = explode('_#', $field, 2);
                } else {
                    $i = 0;
                }
                $subNodes = $xpath->query($xpathExp . '/ns:' . $field);
                /** @var DOMElement $subNode */
                if ($subNodes && ($subNode = $subNodes->item($i))) {
                    if ($attr) {
                        $data[$field][$i][$attr] = $subNode->getAttribute(
                            $attr
                        );
                    } else {
                        $data[$field][$i]['@'] = $subNode->nodeValue;
                    }
                } elseif ($attr) {
                    $data[$field][$i][$attr] = '';
                } else {
                    $data[$field][$i]['@'] = '';
                }
            }
        }

        $this->set('id', $id);
        $this->set('entity', $entity);
        $this->set('data', $data);
        $this->set('form', $form);
        $this->set('path', $path);
        $this->set('listFiles', $entity->listFiles($tempXml));
    }

    /**
     * Ajoute les "attach:" et "detach:" dans transfer_<id>_<user_id>_edit.log
     * @param Transfer    $entity
     * @param DOMUtility  $util
     * @param DOMElement  $node
     * @param string|null $initialAttachement
     * @return void
     */
    private function logPartialEdit(
        Transfer $entity,
        DOMUtility $util,
        DOMElement $node,
        ?string $initialAttachement
    ) {
        $logs = $entity->getEditedLogs($this->userId);
        $h = fopen($logs, 'a');
        fwrite(
            $h,
            'edit:' . $util->getElementXpath($node, '') . PHP_EOL
        );
        $newAttachment = $this->getRequest()->getData(
            'Attachment.0.filename'
        );
        if ($newAttachment !== $initialAttachement) {
            fwrite($h, 'detach:' . $initialAttachement . PHP_EOL);
            fwrite($h, 'attach:' . $newAttachment . PHP_EOL);

            // retire l'ancienne integrité pour un seda 0.2
            if ($initialAttachement && $util->namespace === NAMESPACE_SEDA_02) {
                $qContent = $util->xpathQuote($initialAttachement);
                $query = $util->xpath->query(
                    sprintf("ns:Integrity/ns:UnitIdentifier[contains(text(), %s)]", $qContent)
                );
                foreach ($query as $node) {
                    $node->parentNode->parentNode->removeChild(
                        $node->parentNode
                    );
                }
            }
        }
        fclose($h);
    }

    /**
     * Défini le isDirty dans le json d'édition
     * @param Transfer    $entity
     * @param string      $oldXml
     * @param DOMDocument $actualDom
     */
    private function setMetaIsDirty(
        Transfer $entity,
        string $oldXml,
        DOMDocument $actualDom
    ) {
        if ($actualDom->saveXML() !== $oldXml) {
            $serializedMeta = $entity->getSerializedMeta($this->userId);
            if (!is_file($serializedMeta)) {
                return;
            }
            $meta = unserialize(file_get_contents($serializedMeta));
            if (Hash::get($meta, 'isDirty') !== true) {
                $meta['isDirty'] = true;
                file_put_contents($serializedMeta, serialize($meta));
            }
        }
    }

    /**
     * Supprime l'élement indiqué dans path
     * @param string $id
     * @param mixed  ...$path
     * @return CakeResponse
     * @throws Exception
     */
    public function partialDelete(string $id, ...$path)
    {
        $this->getRequest()->allowMethod('delete');
        if (is_numeric($id)) {
            /** @var Transfer $entity */
            $entity = $this->Seal->archivalAgency()
                ->table('Transfers')
                ->find('seal')
                ->where(['Transfers.id' => $id])
                ->firstOrFail();
            $tempXml = $entity->getTempXml($this->userId);
        } else {
            $tempXml = $this->decodeXmlPath($id);
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $entity = $Transfers->newEntityFromXml($tempXml);
        }

        $util = DOMUtility::load($tempXml);
        $oldXml = $util->dom->saveXML();
        $xpath = $util->xpath;
        $xpathExp = '/ns:' . implode('/ns:', $path);
        /** @var DOMElement $node */
        $node = $xpath->query($xpathExp)->item(0);
        $schemaPath = preg_replace('/\[\d+]/', '', implode('.', $path));

        if (!$node) {
            throw new RecordNotFoundException(
                sprintf('XPath not found "%s"', $xpathExp)
            );
        }
        $nodeXpath = $util->getElementXpath($node, '');
        $form = $entity->getForm(
            null,
            $schemaPath,
            $util->dom,
            $node
        );

        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        $filenames = array_keys(ArchiveBinariesTable::listBinariesInDocument($util, $node));
        if ($filenames && $entity->id) {
            $files = $TransferAttachments->find()
                ->select(['id', 'filename'])
                ->where(
                    [
                        'transfer_id' => $id,
                        'filename IN' => $filenames,
                    ]
                )
                ->disableHydration()
                ->toArray();
        } else {
            $files = [];
        }
        $form->removeNode();
        if ($util->dom->save($tempXml)) {
            if ($entity->id) {
                $logs = $entity->getEditedLogs($this->userId);
                $h = fopen($logs, 'a');
                fwrite($h, 'delete:' . $nodeXpath . PHP_EOL);
                fclose($h);
            }
            $report = 'done';
            $this->setMetaIsDirty($entity, $oldXml, $util->dom);
        } else {
            $report = __("Une erreur a eu lieu");
        }
        $is_repair = !is_numeric($id);

        $toDelete = null;
        $serializedMeta = $entity->getSerializedMeta($this->userId);
        if (is_file($serializedMeta)) {
            $meta = unserialize(file_get_contents($serializedMeta));
            if (!empty($meta['toDelete'])) {
                $toDelete = $meta['toDelete'];
            }
        }

        $parentPath = $path;
        array_pop($parentPath);
        return $this->renderDataToJson(
            [
                'report' => $report,
                'is_repair' => $is_repair,
                'files' => $files,
                'toDelete' => $toDelete,
                'parentId' => $id . '/' . implode('/', $parentPath),
            ]
        );
    }

    /**
     * Ajout d'un nouvel element
     * @param string $id
     * @param string $append
     * @param mixed  ...$path
     * @return Response
     * @throws Exception
     */
    public function partialAdd(string $id, string $append, ...$path)
    {
        $orgEntityId = $this->orgEntityId;
        if (is_numeric($id)) {
            /** @var Transfer $entity */
            $entity = $this->Seal->archivalAgency()
                ->table('Transfers')
                ->find('seal')
                ->where(['Transfers.id' => $id])
                ->firstOrFail();
            $tempXml = $entity->getTempXml($this->userId);
        } else {
            $tempXml = $this->decodeXmlPath($id);
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $entity = $Transfers->newEntityFromXml($tempXml);
        }

        $util = DOMUtility::load($tempXml);
        $oldXml = $util->dom->saveXML();
        $xpath = $util->xpath;
        $xpathExp = '/ns:' . implode('/ns:', $path);
        $schemaPath = preg_replace('/\[\d+]/', '', implode('.', $path));
        /** @var DOMElement $parentNode */
        $parentNode = $xpath->query($xpathExp)->item(0);
        $node = $entity->appendNode(
            $orgEntityId,
            $append,
            $schemaPath,
            $util->dom,
            $parentNode
        );

        $path[] = $append;
        $xpathCount = $util->xpath;
        $xpathCountExp = '/ns:' . implode('/ns:', $path);
        $count = $xpathCount->query($xpathCountExp)->count();
        if ($count > 1) {
            $path[count($path) - 1] = end($path) . '[' . $count . ']';
        }

        $form = $entity->getForm(
            null,
            $schemaPath . '.' . $append,
            $util->dom,
            $node
        );
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->setEntity($entity)->execute($data)) {
                if ($entity->id) {
                    $logs = $entity->getEditedLogs($this->userId);
                    $h = fopen($logs, 'a');
                    fwrite(
                        $h,
                        'add:' . $util->getElementXpath($node, '') . PHP_EOL
                    );
                    if ($append === 'Document') {
                        fwrite(
                            $h,
                            'attach:' . $this->getRequest()->getData(
                                'Attachment.0.filename'
                            ) . PHP_EOL
                        );
                    } elseif ($append === 'BinaryDataObject') {
                        fwrite(
                            $h,
                            'attach:' . $this->getRequest()->getData(
                                'FileInfo.Filename'
                            ) . PHP_EOL
                        );
                    }
                    fclose($h);
                }
                $util->dom->save($tempXml);
                $this->setMetaIsDirty($entity, $oldXml, $util->dom);
                $this->Modal->success();
                return $this->renderDataToJson(implode('/', $path));
            } else {
                $this->Modal->fail();
            }
        } else {
            $data = [];
            foreach ($form->getSchema()->fields() as $field) {
                $attr = null;
                if (strpos($field, '_@') !== false) {
                    [$field, $attr] = explode('_@', $field, 2);
                }
                [$field, $i] = explode('_#', $field, 2);
                if ($attr) {
                    $data[$field][$i][$attr] = '';
                } else {
                    $data[$field][$i]['@'] = '';
                }
            }
        }
        $this->set('id', $id);
        $this->set('entity', $entity);
        $this->set('data', $data);
        $this->set('form', $form);
        $this->set('path', $path);
        $this->set('listFiles', $xmlFiles = $entity->listFiles($tempXml));

        $selectedAttachment = [];
        if (
            in_array($append, ['Document', 'BinaryDataObject']) && is_numeric(
                $id
            )
        ) {
            $name = [];
            $parent = $node;
            while (($parent = $parent->parentNode)) {
                foreach ($parent->childNodes as $child) {
                    if (
                        $child instanceof DOMElement && strtolower(
                            $child->nodeName
                        ) === 'name'
                    ) {
                        $name[] = trim($child->nodeValue);
                        break;
                    }
                }
            }
            array_pop($name); // fait sauter le niveau "ArchiveTransfer/Archive"
            $dir = implode('/', array_reverse($name));

            // filename dont le dir correspond en 1er
            /** @var TransferAttachmentsTable $TransferAttachments */
            $TransferAttachments = $this->fetchTable('TransferAttachments');
            $quoter = DatabaseUtility::getDriver()->quoter();
            $pdo = DatabaseUtility::getPdo();
            $order = count($name) > 1
                ? [
                    sprintf(
                        '%s LIKE %s',
                        $quoter->quoteIdentifier('filename'),
                        $pdo->quote($dir . '/%')
                    ) => 'desc',
                    sprintf(
                        '%s LIKE %s',
                        $quoter->quoteIdentifier('filename'),
                        $pdo->quote($dir . '/%/%')
                    ) => 'asc',
                ]
                : [];
            $order['filename'] = 'asc';

            $query = $TransferAttachments->find()
                ->select(['id', 'filename'])
                ->disableHydration()
                ->where(['transfer_id' => $id])
                ->orderBy($order);

            // Ajoute la condition que le fichier ne fasse pas parti des fichiers à supprimer
            $serializedMeta = $entity->getSerializedMeta($this->userId);
            if (is_file($serializedMeta)) {
                $meta = unserialize(file_get_contents($serializedMeta));
                if (!empty($meta['toDelete'])) {
                    $query->andWhere(['id NOT IN' => $meta['toDelete']]);
                }
            }
            foreach ($query as $attachment) {
                if (!in_array($attachment['filename'], $xmlFiles)) {
                    $selectedAttachment = [
                        'value' => $attachment['filename'],
                        'group' => dirname($attachment['filename']),
                    ];
                    break;
                }
            }
        }
        $this->set('selectedAttachment', $selectedAttachment);

        $target = $id . '/' . implode('/', $path);
        if (is_numeric($id)) {
            $refreshJsBase = 'actionEditTransfer';
        } else {
            $refreshJsBase = 'repairTransfer';
        }
        $refreshJsCallback = "$refreshJsBase('$target')";
        $this->set('refreshJsBase', $refreshJsBase);
        $this->set('refreshJsCallback', $refreshJsCallback);

        $this->viewBuilder()->setTemplate('partialEdit');
    }

    /**
     * Décompresse un fichier zip/tar/gz pour ajouter de nombreux fichiers à un
     * transfert
     * @param string $attachment_id
     * @return ResponseInterface
     * @throws Exception
     */
    public function addCompressed(string $attachment_id)
    {
        return $this->_addCompressed($attachment_id);
    }

    /**
     * Déplace un element avant/après un autre
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function moveNode(string $id)
    {
        $this->getRequest()->allowMethod('post');
        if (is_numeric($id)) {
            /** @var Transfer $entity */
            $entity = $this->Seal->archivalAgency()
                ->table('Transfers')
                ->find('seal')
                ->where(['Transfers.id' => $id])
                ->firstOrFail();
            $tempXml = $entity->getTempXml($this->userId);
        } else {
            $tempXml = $this->decodeXmlPath($id);
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $entity = $Transfers->newEntityFromXml($tempXml);
        }

        $util = DOMUtility::load($tempXml);
        $oldXml = $util->dom->saveXML();
        $xpath = $util->xpath;
        $move = $this->getRequest()->getData('move');
        $xpathExp = '/ns:' . str_replace('/', '/ns:', $move);
        $moveNode = $xpath->query($xpathExp)->item(0);
        if (!$moveNode instanceof DOMElement) {
            throw new RecordNotFoundException(
                sprintf('XPath not found "%s"', $xpathExp)
            );
        }
        $target = $this->getRequest()->getData('target');
        $xpathExp = '/ns:' . str_replace('/', '/ns:', $target);
        $target = $xpath->query($xpathExp)->item(0);
        if (!$target) {
            throw new RecordNotFoundException(
                sprintf('XPath not found "%s"', $xpathExp)
            );
        }
        $nodeXpath = $util->getElementXpath($moveNode, '');
        if ($this->getRequest()->getData('direction') === 'before') {
            $moveNode->parentNode->insertBefore($moveNode, $target);
        } else {
            $inserted = false;
            $ref =& $target;
            while (isset($ref->nextSibling)) {
                $ref =& $ref->nextSibling;
                if ($ref instanceof DOMElement) {
                    $moveNode->parentNode->insertBefore($moveNode, $ref);
                    $inserted = true;
                    break;
                }
            }
            if (!$inserted) {
                $moveNode->parentNode->appendChild($moveNode);
            }
        }

        // On récupère la nouvelle position xpath
        $count = 0;
        $xpath = '';
        foreach ($moveNode->parentNode->childNodes as $node) {
            if (!$node instanceof DOMElement) {
                continue;
            }
            if ($node->tagName === $moveNode->tagName) {
                $count++;
            }
            if ($node === $moveNode) {
                $xpath = substr($move, 0, strrpos($move, '/') ?: 0);
                $xpath .= '/' . $node->tagName . '[' . $count . ']';
            }
        }

        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        if ($entity->id) {
            $logs = $entity->getEditedLogs($this->userId);
            $h = fopen($logs, 'a');
            fwrite($h, 'move:' . $nodeXpath . PHP_EOL);
            fclose($h);
        }
        $this->Modal->success($util->dom->save($tempXml));
        $this->setMetaIsDirty($entity, $oldXml, $util->dom);
        return $this->renderDataToJson($xpath);
    }

    /**
     * Ajoute un ensemble de mots-clés
     * @param string $id
     * @param mixed  ...$path
     * @return Response
     * @throws Exception
     */
    public function addMultipleKeywords(string $id, ...$path)
    {
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        if ($this->getRequest()->is('post')) {
            if (is_numeric($id)) {
                /** @var Transfer $entity */
                $entity = $this->Seal->archivalAgency()
                    ->table('Transfers')
                    ->find('seal')
                    ->where(['Transfers.id' => $id])
                    ->firstOrFail();
                $tempXml = $entity->getTempXml($this->userId);
            } else {
                $tempXml = $this->decodeXmlPath($id);
                $entity = $Transfers->newEntityFromXml($tempXml);
            }

            $util = DOMUtility::load($tempXml);
            $oldXml = $util->dom->saveXML();
            $xpath = $util->xpath;
            $xpathExp = '/ns:' . implode('/ns:', $path);
            /** @var DOMElement $node */
            $node = $xpath->query($xpathExp)->item(0);
            $count = $xpath->query('ns:Keyword', $node)->count();
            $nodeXpath = $util->getElementXpath($node, '');

            $schemaPath = preg_replace('/\[\d+]/', '', implode('.', $path));
            $form = $entity->getForm(
                null,
                $schemaPath,
                $util->dom,
                $node
            );
            $loadKeywords = $this->getRequest()->getData('load-keywords', [])
                ?: [];
            $customKeywords = $this->getRequest()->getData('customKeyword', [])
                ?: [];
            $success = $form->addMultipleKeywords($loadKeywords)
                && $form->addMultipleCustomKeywords($customKeywords);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($success) {
                if ($entity->id) {
                    $logs = $entity->getEditedLogs($this->userId);
                    $h = fopen($logs, 'a');
                    foreach (
                        array_merge(
                            $loadKeywords,
                            $customKeywords
                        ) as $keyword
                    ) {
                        if (
                            !is_numeric(
                                $keyword
                            ) && !($keyword['name'] ?? false)
                        ) {
                            continue;
                        }
                        $count++;
                        fwrite(
                            $h,
                            'add:' . $nodeXpath . '/Keyword[' . $count . ']' . PHP_EOL
                        );
                    }
                    fclose($h);
                }
                $util->dom->save($tempXml);
                $this->setMetaIsDirty($entity, $oldXml, $util->dom);
                $this->Modal->success();
                return $this->renderDataToJson(implode('/', $path));
            } else {
                $this->Modal->fail();
            }
        }

        $this->set('id', $id);
    }

    /**
     * Liste de mes transferts en préparation
     */
    public function indexPreparating()
    {
        /** @use JstableComponent */
        $this->loadComponent('AsalaeCore.Jstable');
        $this->Jstable->configurablePagination(self::TABLE_PREPARE_INDEX);
        $this->set('tableId', self::TABLE_PREPARE_INDEX);
        $this->set('modalXmlToHtml', self::MODAL_XML_TO_HTML);
        $this->set(
            'groupedActions',
            [
                '0' => __("-- action groupée --"),
                'delete' => __("Supprimer"),
                'submit' => __("Envoyer"),
            ]
        );
        $this->indexCommons(
            [
                'state IN' => [
                    TransfersTable::S_CREATING,
                    TransfersTable::S_PREPARATING,
                ],
            ]
        );
    }

    /**
     * Code commun entre les méthodes index
     * @param array      $conditions
     * @param Query|null $query
     * @throws Exception
     */
    private function indexCommons($conditions = [], Query $query = null)
    {
        /** @use FilterComponent */
        $this->loadComponent('AsalaeCore.Filter');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        if (empty($query)) {
            $query = $this->Seal->archivalAgency()
                ->table('Transfers')
                ->find('seal')
                ->where(['Transfers.created_user_id' => $this->userId])
                ->contain(['ArchivalAgencies', 'TransferringAgencies', 'OriginatingAgencies']);
        }
        $query->contain(
            [
                'CreatedUsers',
                'ValidationProcesses' => [
                    'CurrentStages' => ['ValidationActors'],
                ],
            ]
        );
        $query->andWhere($conditions);
        $query->andWhere(['Transfers.hidden IS' => false]);

        $IndexComponent->setQuery($query)
            ->filter('transfer_identifier', IndexComponent::FILTER_ILIKE)
            ->filter('transferring_agency_id')
            ->filter('originating_agency_id')
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('transfer_date', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('state', IndexComponent::FILTER_IN)
            ->filter('created_user_id')
            ->filter('files_deleted', IndexComponent::FILTER_IS);

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/commons');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)
            ->map(fn(EntityWithSessionDataInterface $v) => $v->appendSessionData($this->user))
            ->toArray();
        $this->set('data', $data);

        // Options
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $ta = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES]
            );
        $this->set('transferring_agencies', $ta->all());

        $oa = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES]
            );
        $this->set('originating_agencies', $oa->all());

        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $this->set('states', $Transfers->options('state'));
        $hierarchicalView = Hash::get($this->user, 'role.hierarchical_view');
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $Users->setDisplayField('username');
        $cu = $Users->find(
            'list',
            keyField: 'id',
            valueField: function (EntityInterface $entity) {
                return $entity->get('username') . ($entity->get('name') ? ' - ' . $entity->get('name') : '');
            },
        )
            ->innerJoinWith('OrgEntities')
            ->where(
                [
                    'OrgEntities.lft' . ($hierarchicalView ? ' >='
                        : '') => Hash::get($this->orgEntity, 'lft'),
                    'OrgEntities.rght' . ($hierarchicalView ? ' <='
                        : '') => Hash::get($this->orgEntity, 'rght'),
                ]
            )
            ->orderBy(['Users.username']);
        $this->set('createdUsers', $cu);

        // override IndexComponent
        if (
            $this->getRequest()->is('ajax')
            && $this->viewBuilder()->getLayout() === null
        ) {
            $this->viewBuilder()->setTemplate('ajax_index');
        }
    }

    /**
     * Liste des transferts acceptés
     */
    public function indexAccepted()
    {
        /** @use JstableComponent */
        $this->loadComponent('AsalaeCore.Jstable');
        $this->Jstable->configurablePagination(self::TABLE_MY_ACCEPTED_INDEX);
        $this->set('tableId', self::TABLE_MY_ACCEPTED_INDEX);
        $this->set('modalXmlToHtml', self::MODAL_XML_TO_HTML);
        $this->set('transferEntrantIndex', true); // @temporary, cf. #304
        $query = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal');
        if (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->where(
                [
                    'TransferringAgencies.lft >=' => Hash::get(
                        $this->orgEntity,
                        'lft'
                    ),
                    'TransferringAgencies.rght <=' => Hash::get(
                        $this->orgEntity,
                        'rght'
                    ),
                ]
            );
        } else {
            $query->where(['TransferringAgencies.id' => $this->orgEntityId]);
        }
        $query->contain(['ArchivalAgencies', 'TransferringAgencies', 'OriginatingAgencies']);
        $this->indexCommons(['state' => TransfersTable::S_ACCEPTED], $query);
    }

    /**
     * Liste des transferts en rejetés
     */
    public function indexRejected()
    {
        /** @use JstableComponent */
        $this->loadComponent('AsalaeCore.Jstable');
        $this->Jstable->configurablePagination(self::TABLE_MY_REJECTED_INDEX);
        $this->set('tableId', self::TABLE_MY_REJECTED_INDEX);
        $this->set('modalXmlToHtml', self::MODAL_XML_TO_HTML);
        $this->set('transferEntrantIndex', true); // @temporary, cf. #304
        $query = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal');
        if (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->where(
                [
                    'TransferringAgencies.lft >=' => Hash::get(
                        $this->orgEntity,
                        'lft'
                    ),
                    'TransferringAgencies.rght <=' => Hash::get(
                        $this->orgEntity,
                        'rght'
                    ),
                ]
            );
        } else {
            $query->where(['TransferringAgencies.id' => $this->orgEntityId]);
        }
        $query->contain(['ArchivalAgencies', 'TransferringAgencies', 'OriginatingAgencies']);
        $this->indexCommons(['state' => TransfersTable::S_REJECTED], $query);
    }

    /**
     * Liste de mes transferts acceptés
     */
    public function indexMyAccepted()
    {
        /** @use JstableComponent */
        $this->loadComponent('AsalaeCore.Jstable');
        $this->Jstable->configurablePagination(self::TABLE_MY_ACCEPTED_INDEX);
        $this->set('tableId', self::TABLE_MY_ACCEPTED_INDEX);
        $this->set('modalXmlToHtml', self::MODAL_XML_TO_HTML);
        $this->indexCommons(['state' => TransfersTable::S_ACCEPTED]);
    }

    /**
     * Liste de mes transferts en rejetés
     */
    public function indexMyRejected()
    {
        /** @use JstableComponent */
        $this->loadComponent('AsalaeCore.Jstable');
        $this->Jstable->configurablePagination(self::TABLE_MY_REJECTED_INDEX);
        $this->set('tableId', self::TABLE_MY_REJECTED_INDEX);
        $this->set('modalXmlToHtml', self::MODAL_XML_TO_HTML);
        $this->indexCommons(['state' => TransfersTable::S_REJECTED]);
        $this->set(
            'groupedActions',
            [
                '0' => __("-- action groupée --"),
                'delete' => __("Supprimer"),
            ]
        );
    }

    /**
     * Liste de mes transferts envoyés
     */
    public function indexSent()
    {
        /** @use JstableComponent */
        $this->loadComponent('AsalaeCore.Jstable');
        $this->Jstable->configurablePagination(self::TABLE_SENT_INDEX);
        $this->set('tableId', self::TABLE_SENT_INDEX);
        $this->set('modalXmlToHtml', self::MODAL_XML_TO_HTML);
        $this->indexCommons(
            [
                'state IN' => [
                    TransfersTable::S_TIMESTAMPING,
                    TransfersTable::S_ANALYSING,
                    TransfersTable::S_CONTROLLING,
                    TransfersTable::S_VALIDATING,
                    TransfersTable::S_ARCHIVING,
                ],
            ]
        );
    }

    /**
     * Liste des mes transferts
     */
    public function indexMy()
    {
        /** @use JstableComponent */
        $this->loadComponent('AsalaeCore.Jstable');
        $this->Jstable->configurablePagination(self::TABLE_MY_INDEX);
        $this->set('tableId', self::TABLE_MY_INDEX);
        $this->set('modalXmlToHtml', self::MODAL_XML_TO_HTML);
        $this->set(
            'groupedActions',
            [
                '0' => __("-- action groupée --"),
                'delete' => __("Supprimer"),
                'submit' => __("Envoyer"),
            ]
        );
        $this->indexCommons();
    }

    /**
     * Liste des transferts de mon service
     */
    public function indexMyEntity()
    {
        /** @use JstableComponent */
        $this->loadComponent('AsalaeCore.Jstable');
        $this->Jstable->configurablePagination(self::TABLE_MY_ENTITY_INDEX);
        $this->set('tableId', self::TABLE_MY_ENTITY_INDEX);
        $this->set('modalXmlToHtml', self::MODAL_XML_TO_HTML);
        $query = $this->Seal->hierarchicalView()
            ->table('Transfers')
            ->find('seal');
        $query->contain(['ArchivalAgencies', 'TransferringAgencies', 'OriginatingAgencies']);
        $this->set(
            'groupedActions',
            [
                '0' => __("-- action groupée --"),
                'delete' => __("Supprimer"),
                'submit' => __("Envoyer"),
            ]
        );
        $this->indexCommons([], $query);
    }

    /**
     * Envoi du transfert à l'étape suivante (horodatage ou validation)
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function send(string $id)
    {
        $this->getRequest()->allowMethod('post');
        /** @var Beanstalk $Beanstalk */
        $Beanstalk = Utility::get('Beanstalk');
        if (!$Beanstalk->isConnected()) {
            throw new ServiceUnavailableException();
        }

        /** @var Transfer $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.id' => $id,
                    'Transfers.state' => TransfersTable::S_PREPARATING,
                ]
            )
            ->contain(
                [
                    'ServiceLevels' => ['TsMsgs', 'TsPjss', 'TsConvs'],
                    'TransferLocks',
                ]
            )
            ->firstOrFail();

        $lock = $entity->get('transfer_lock');
        if ($lock) {
            $this->fetchTable('TransferLocks')->delete($lock);
            $entity->unset('transfer_lock');
        }

        $serviceLevel = $entity->get('service_level');
        if (!$serviceLevel) {
            $serviceLevel = $this->Seal->archivalAgency()
                ->table('ServiceLevels')
                ->find('seal')
                ->where(['ServiceLevels.default_level' => true])
                ->contain(['TsMsgs', 'TsPjss', 'TsConvs'])
                ->first();
        }
        $tempXml = $entity->getTempXml($this->userId);
        $serializedMeta = $entity->getSerializedMeta($this->userId);
        $logs = $entity->getEditedLogs($this->userId);
        foreach ([$tempXml, $serializedMeta, $logs] as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        /** @var Beanstalk $Beanstalk */
        if (
            $serviceLevel && ($serviceLevel->get(
                'ts_msg_id'
            ) || $serviceLevel->get('ts_pjs_id'))
        ) {
            $job = [
                'transfer_id' => $entity->get('id'),
                'user_id' => $this->userId,
                'ts_msg_id' => Hash::get(
                    $serviceLevel,
                    'ts_msg.timestamper_id'
                ),
                'ts_pjs_id' => Hash::get(
                    $serviceLevel,
                    'ts_pjs.timestamper_id'
                ),
                'ts_conv_id' => Hash::get(
                    $serviceLevel,
                    'ts_conv.timestamper_id'
                ),
            ];
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $Transfers->transition($entity, TransfersTable::T_TIMESTAMP);
            $Transfers->saveOrFail($entity);
            $Beanstalk->setTube('timestamp');
        } else {
            $job = [
                'transfer_id' => $entity->get('id'),
                'user_id' => $this->userId,
            ];
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $Transfers->transition($entity, TransfersTable::T_ANALYSE);
            $Transfers->saveOrFail($entity);
            $Beanstalk->setTube('analyse');
        }
        $Transfers->createCompositeIfNeeded($entity);
        $Beanstalk->emit($job);
        Utility::get('Notify')->emit(
            'transfer_sent',
            [
                'user_id' => $this->userId,
                'sa_id' => $this->archivalAgencyId,
                'count' => 1,
            ]
        );

        return $this->renderDataToJson(['report' => 'done']);
    }

    /**
     * Pagination ajax de la vue des attachments
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function viewAttachments(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        $query = $this->Seal->archivalAgency()
            ->table('TransferAttachments')
            ->find('seal')
            ->innerJoinWith('Transfers')
            ->where(['TransferAttachments.transfer_id' => $id])
            ->orderBy(['TransferAttachments.filename' => 'asc']);

        $this->loadComponent(
            'AsalaeCore.Index',
            ['model' => 'TransferAttachments']
        );
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        $IndexComponent->setQuery($query)
            ->filter('filename', IndexComponent::FILTER_ILIKE)
            ->filter(
                'size',
                function ($s) {
                    if (!$s['value']) {
                        return;
                    }
                    $value = $s['value'] * $s['mult'];
                    $operator = $s['operator'];
                    if (!in_array($operator, ['=', '<=', '>=', '<', '>'])) {
                        throw new BadRequestException(
                            __("L'opérateur indiqué n'est pas autorisé")
                        );
                    }
                    return ['TransferAttachments.size ' . $operator => $value];
                }
            );

        if ($this->getRequest()->getQuery('tempfile')) {
            /** @var Transfer $transfer */
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $transfer = $Transfers->get($id);

            $tempXml = $transfer->getTempXml($this->userId);
            $listFilesTemp = $transfer->listFiles($tempXml);
            TransferAttachment::$listFiles[$id] = $listFilesTemp;
        }
        return $AjaxPaginatorComponent->json(
            $query,
            (bool)$this->getRequest()->getQuery('count')
        );
    }

    /**
     * Pagination ajax de la vue des errors
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function viewErrors(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        $query = $this->Seal->archivalAgency()
            ->table('TransferErrors')
            ->find('seal')
            ->innerJoinWith('Transfers')
            ->where(['TransferErrors.transfer_id' => $id]);

        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index', ['model' => 'TransferErrors']);
        $IndexComponent->setQuery($query)
            ->filter('level', null, 'TransferErrors.level')
            ->filter('code', null, 'TransferErrors.code');

        return $AjaxPaginatorComponent->json($query);
    }

    /**
     * Effectu l'analyse d'un transfert
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function analyse(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        /** @var Transfer $transfer */
        $transfer = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(['Transfers.id' => $id])
            ->firstOrFail();

        /** @var TransferAnalyse $TransferAnalyse */
        $TransferAnalyse = Utility::get(TransferAnalyse::class, $transfer);
        $query = $TransferAttachments->find();
        $count = $query->count();
        $size = $query
            ->select(
                ['sum' => $query->func()->sum('size')]
            )
            ->where(['transfer_id' => $id])
            ->first()
            ->get('sum');
        $timeout = (int)$size / 10000000 * 60 + $count * 2;
        LimitBreak::setTimeLimit(
            (int)$size / 10000000 * 60 + $count * 2
        ); // 1 min / 10Mo + 2s par fichiers
        LimitBreak::setMemoryLimit(
            filesize($transfer->get('xml')) * 10
        ); // 46Mo = 460Mo

        try {
            /** @var KillablesTable $Killables */
            $Killables = $this->fetchTable('Killables');
            $Killables->newKillableProcess(
                __METHOD__,
                $this->userId,
                $timeout,
                $token = $this->getRequest()->getSession()->read(
                    'Session.token'
                )
            );
            $TransferAnalyse->control("_{$this->userId}_$token");
        } catch (GenericException) {
            return $this->getResponse()->withStringBody(
                __("Requête interrompue par l'utilisateur")
            );
        }

        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $entity = $Transfers->find()
            ->where(['Transfers.id' => $id])
            ->contain(
                [
                    'TransferErrors' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q
                            ->orderBy(
                                [
                                    'TransferErrors.level' => 'asc',
                                    'TransferErrors.id' => 'asc',
                                ]
                            )
                            ->limit($AjaxPaginatorComponent->getConfig('limit'));
                    },
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);
        /** @var TransferErrorsTable $TransferErrors */
        $TransferErrors = $this->fetchTable('TransferErrors');
        $countErrors = $TransferErrors->find()
            ->where(['TransferErrors.transfer_id' => $id])
            ->count();
        $this->set('countErrors', $countErrors);
        $this->set('id', $id);
        $AjaxPaginatorComponent->setViewPaginator(Hash::get($entity, 'transfer_errors') ?: [], $countErrors);
    }

    /**
     * Cherche les occurences de $searchStr à l'interieur du xml, donne la liste
     * des noeuds (xpath) qui contiennent l'occurence.
     * @param string $id
     * @return Response ["/ArchiveTransfer/Archive/ContentDescription/Keyword[2]/KeywordContent", ...]
     * @throws Exception
     */
    public function searchInEdit(string $id)
    {
        $searchStr = $this->getRequest()->getData('search');

        if (is_numeric($id)) {
            /** @var Transfer $entity */
            $entity = $this->Seal->archivalAgency()
                ->table('Transfers')
                ->find('seal')
                ->where(['Transfers.id' => $id])
                ->firstOrFail();
            if ($this->getRequest()->getQuery('tempfile')) {
                $tempXml = $entity->getTempXml($this->userId);
                $domutil = DOMUtility::load($tempXml);
            } else {
                $path = $entity->get('xml');
                $domutil = DOMUtility::load($path);
            }
        } else {
            $tempXml = $this->decodeXmlPath($id);
            $domutil = DOMUtility::load($tempXml);
        }

        $results = [];
        foreach ($domutil->findByContent($searchStr) as $path) {
            if (count($results) > 100) {
                $results[] = [
                    'path' => __("il y a plus de 100 résultats"),
                    'value' => '',
                ];
                break;
            }
            if (strpos($path, '@')) {
                [$npath, $attr] = explode('@', $path);
                $npath = str_replace('/', '/ns:', $npath);
                /** @var DOMElement $element */
                $element = $domutil->xpath->query($npath)->item(0);
                $value = $element->getAttribute($attr);
            } else {
                $npath = str_replace('/', '/ns:', $path);
                $value = $domutil->xpath->query($npath)->item(0)->nodeValue;
            }
            $results[] = [
                'path' => $path,
                'value' => h($value),
            ];
        }
        return $this->renderDataToJson($results);
    }

    /**
     * Afficher le bordereau
     * @param string $id
     * @throws Exception
     */
    public function explore(string $id)
    {
        /** @var Transfer $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(['Transfers.id' => $id])
            ->firstOrFail();
        if (!is_file($entity->get('xml'))) {
            throw new NotFoundException(
                __("Le fichier xml du transfert n'a pas été trouvé")
            );
        }

        $this->set('id', $id);
        $this->set('entity', $entity);
        $tableId = 'transfer-explore-files-table';
        $this->set('tableId', $tableId);
    }

    /**
     * Modifi une partie de l'xml du transfert
     * @param string $id
     * @param string ...$path
     * @throws Exception
     */
    public function partialExplore(string $id, ...$path)
    {
        /** @var Transfer $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(['Transfers.id' => $id])
            ->firstOrFail();

        $dom = new \DOMDocument();
        $dom->load($entity->get('xml'));
        $xpath = new DOMXPath($dom);
        $namespace = $dom->documentElement->getAttributeNode(
            'xmlns'
        )->nodeValue;
        $xpath->registerNamespace('ns', $namespace);
        $xpathExp = '/ns:' . implode('/ns:', $path);
        $node = $xpath->query($xpathExp)->item(0);
        $nodes = [];
        $attrs = [];
        /** @var DOMAttr $attr */
        foreach ($node->attributes as $attr) {
            $attrs[$attr->nodeName] = $attr->nodeValue;
        }
        if ($attrs) {
            $nodes['@'][] = ['attrs' => $attrs, 'value' => null];
        }
        foreach ($node->childNodes as $child) {
            if ($child instanceof DOMElement) {
                foreach ($child->childNodes as $subChild) {
                    if ($subChild instanceof DOMElement) {
                        continue 2;
                    }
                }
                if (!isset($nodes[$child->nodeName])) {
                    $nodes[$child->nodeName] = [];
                }
                $attrs = [];
                foreach ($child->attributes as $attr) {
                    $attrs[$attr->nodeName] = $attr->nodeValue;
                }
                $nodes[$child->nodeName][] = [
                    'attrs' => $attrs,
                    'value' => $child->nodeValue,
                ];
            }
        }
        $this->set('path', $path);
        $this->set('nodes', $nodes);
    }

    /**
     * Supprime un fichier lié
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function deleteAttachment(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $filename = $this->getRequest()->getData('filename');
        $ids = $this->getRequest()->getData('ids');

        /** @var Transfer $transfer */
        $transfer = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(['Transfers.id' => $id])
            ->firstOrFail();
        $serializedMeta = $transfer->getSerializedMeta($this->userId);
        $meta = [];
        if (is_file($serializedMeta)) {
            $meta = unserialize(file_get_contents($serializedMeta));
            if (!isset($meta['toDelete'])) {
                $meta['toDelete'] = [];
            }
        }

        $deleted = [];
        $report = null;
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        if ($filename) {
            $attachment = $TransferAttachments->find()
                ->where(
                    [
                        'TransferAttachments.transfer_id' => $id,
                        'TransferAttachments.filename' => $filename,
                    ]
                )
                ->firstOrFail();

            $meta['toDelete'][] = $attachment->get('id');
            if (empty($attachment)) {
                throw new NotFoundException($filename . ' not found');
            }
            if (
                in_array($filename, $transfer->listFiles())
                && is_file($serializedMeta)
            ) {
                $report = file_put_contents($serializedMeta, serialize($meta))
                    ? 'scheduled'
                    : 'Erreur lors de la suppression';
            } else {
                /** @var TransferAttachmentsTable $TransferAttachments */
                $TransferAttachments = $this->fetchTable('TransferAttachments');
                $report = $TransferAttachments->delete($attachment)
                    ? 'done'
                    : 'Erreur lors de la suppression';
                $dir = AbstractGenericMessageForm::getDataDirectory($id);
                $this->removeEmptySubFolders($dir . DS . 'attachments');
                $deleted[] = $attachment->id;
            }
        } elseif ($ids) {
            $attachments = $TransferAttachments->find()
                ->where(
                    [
                        'TransferAttachments.transfer_id' => $id,
                        'TransferAttachments.id IN' => $ids,
                    ]
                )
                ->contain(['Transfers']);
            /** @var EntityInterface $attachment */
            foreach ($attachments as $attachment) {
                $filename = $attachment->get('filename');
                if (
                    in_array($filename, $transfer->listFiles())
                    && is_file($serializedMeta)
                ) {
                    $meta['toDelete'][] = $attachment->id;
                } else {
                    $TransferAttachments->deleteOrFail($attachment);
                    $dir = AbstractGenericMessageForm::getDataDirectory($id);
                    $this->removeEmptySubFolders($dir . DS . 'attachments');
                    $deleted[] = $attachment->id;
                }
            }
            $report = file_put_contents($serializedMeta, serialize($meta))
                ? 'done'
                : 'Erreur lors de la suppression';
        }
        $toDelete = $meta['toDelete'];
        if ($deleted) {
            $transfer->set('is_conform');
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $Transfers->saveOrFail($transfer);
        }

        return $this->renderDataToJson(
            [
                'report' => $report,
                'toDelete' => $toDelete,
                'deleted' => $deleted,
            ]
        );
    }

    /**
     * Supprime un transfert
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function delete(string $id)
    {
        $entity = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(['Transfers.id' => $id])
            ->firstOrFail();
        if (
            !$entity->get('force_deletable')
            && $entity->get('created_user_id') !== $this->userId
        ) {
            throw new ForbiddenException();
        }

        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $conn = $Transfers->getConnection();
        $conn->begin();
        Filesystem::begin();
        try {
            $success = $Transfers->delete($entity, ['unlink' => true]);
        } /** @noinspection PhpRedundantCatchClauseInspection */ catch (AfterDeleteException) {
            $success = false;
        }

        if ($success) {
            $report = 'done';
            $conn->commit();
            Filesystem::commit();
            switch ($entity->get('state')) {
                case 'creating':
                case 'preparating':
                    Utility::get('Notify')->emit(
                        'new_transfer',
                        [
                            'user_id' => $this->userId,
                            'sa_id' => $this->archivalAgencyId,
                            'count' => -1,
                        ]
                    );
                    break;
                case 'timestamping':
                case 'analysing':
                case 'controlling':
                    Utility::get('Notify')->emit(
                        'transfer_sent',
                        [
                            'user_id' => $this->userId,
                            'sa_id' => $this->archivalAgencyId,
                            'count' => -1,
                        ]
                    );
                    break;
            }
        } else {
            $report = __("Erreur lors de la suppression du transfert");
            $conn->rollback();
            Filesystem::rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Permet de télécharger le bordereau
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function download(string $id): Response
    {
        if (is_numeric($id)) {
            /** @var Transfer $transfer */
            $transfer = $this->Seal->archivalAgency()
                ->table('Transfers')
                ->find('seal')
                ->where(['Transfers.id' => $id])
                ->firstOrFail();

            $xml = $transfer->get('xml');
        } else {
            $xml = $this->decodeXmlPath($id);
        }
        return $this->getCoreResponse()
            ->withFileStream($xml, ['mime' => 'application/xml']);
    }

    /**
     * Création du zip des fichiers du transfert (si besoin),
     * ou création asynchrone si on juge le temps trop long (avec envoit de mail à l'utilisateur)
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function makeZip(string $id): Response
    {
        /** @var Transfer $transfer */
        $transfer = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.id' => $id,
                    'Transfers.files_deleted IS NOT' => true,
                ]
            )
            ->firstOrFail();

        $fileName = $transfer->get('zip');
        if ($this->getRequest()->getQuery('renew') && is_file($fileName)) {
            unlink($fileName);
        }
        // le zip existe déjà
        if (is_file($fileName)) {
            return $this->getResponse()
                ->withHeader('X-Asalae-ZipFileDownload', 'download');
        }

        // le temps de création du zip est jugé trop long
        $confLimit = Configure::read('Downloads.asyncFileCreationTimeLimit');
        /** @var Exec $exec */
        $exec = Utility::get(Exec::class);
        if (
            $transfer->zipCreationTimeEstimation() > $confLimit
            || $this->getRequest()->getQuery('forceAsync')
        ) {
            $exec->async(
                CAKE_SHELL,
                'zip_files',
                'transfer',
                $transfer->get('id'),
                $this->userId
            );

            return $this->getResponse()
                ->withHeader('X-Asalae-ZipFileDownload', 'email')
                ->withStringBody(
                    __(
                        "Le fichier est trop volumineux pour être téléchargé dans l'immédiat,"
                        . "un email vous sera envoyé dès qu'il sera disponible."
                    )
                );
        }

        // creation du fichier
        $result = $exec->command(
            CAKE_SHELL,
            'zip_files',
            'transfer',
            $transfer->get('id'),
            $this->userId,
            ['--no-mail' => null]
        );

        if (!$result->success) {
            if ($result->stderr) {
                Log::error($result->stderr);
            }debug($result->stderr);
            return $this->getResponse()
                ->withHeader('X-Asalae-ZipFileDownload', 'fail')
                ->withStatus(500)
                ->withStringBody(
                    __(
                        "Une erreur est survenue lors de la création du fichier zip,"
                        . " veuillez ré-essayer ou contacter un administrateur."
                    )
                );
        }

        return $this->getResponse()
            ->withHeader('X-Asalae-ZipFileDownload', 'download');
    }

    /**
     * Téléchargement du zip d'un transfert
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function downloadFiles(string $id): Response
    {
        /** @var Transfer $transfer */
        $transfer = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.id' => $id,
                    'Transfers.files_deleted IS NOT' => true,
                ]
            )
            ->firstOrFail();

        // regarder si le zip existe déjà
        $zip = $transfer->get('zip');
        if (!is_file($zip)) {
            throw new NotFoundException();
        }

        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        $entry = $EventLogs->newEntry(
            'download_TransferZip',
            'success',
            __(
                "Téléchargement des fichiers du transfert ''{0}'' par l'utilisateur ''{1}''",
                $transfer->get('transfer_identifier'),
                Hash::get($this->user, 'name')
                    ?: Hash::get(
                        $this->user,
                        'username'
                    )
            ),
            $this->userId,
            $transfer
        );
        $EventLogs->saveOrFail($entry);

        return $this->getCoreResponse()
            ->withFileStream($zip, ['mime' => 'application/zip'])
            ->withHeader('X-Asalae-ZipFileDownload', 'download');
    }

    /**
     * Copie d'un bordereau
     * @param string $id
     * @return Response
     * @throws ZMQSocketException
     * @throws DateInvalidTimeZoneException
     */
    public function duplicate(string $id)
    {
        $filename = uniqid('transfer-') . '.xml';
        $initialTransfer = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(['Transfers.id' => $id])
            ->firstOrFail();
        $uri = sys_get_temp_dir() . DS . uniqid() . $filename;

        Filesystem::copy($initialTransfer->get('xml'), $uri);

        /** @var CountersTable $Counters */
        $Counters = $this->fetchTable('Counters');
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $generated = $Counters->next(
            $this->archivalAgencyId,
            'ArchiveTransfer',
            [],
            function ($generated) use ($Transfers) {
                $cond = [
                    'transfer_identifier' => $generated,
                    'archival_agency_id' => $this->archivalAgencyId,
                ];
                return !$Transfers->exists($cond);
            }
        );

        $data = [
            'message_version' => $initialTransfer->get('message_version'),
            'state' => $Transfers->initialState,
            'is_modified' => false,
            'filename' => $filename,
            'archival_agency_id' => $this->archivalAgencyId,
            'state_history' => null,
            'last_state_update' => null,
            'is_conform' => null,
            'is_accepted' => null,
            'created_user_id' => $this->userId,
            'transfer_identifier' => $generated,
            'transfer_comment' => __(
                "{0} (copie de {1})",
                $initialTransfer->get('transfer_comment'),
                $initialTransfer->get('transfer_identifier')
            ),
            'uri' => $uri,
            'files_deleted' => false,
            'data_size' => 0,
            'data_count' => 0,
        ] + $initialTransfer->toArray();
        unset($data['id'], $data['created'], $data['modified']);
        /** @var Transfer $entity */
        $entity = $Transfers->newEntity($data);

        $entity->saveChangesAsXml($uri);
        if (!PreControlMessages::isValid($uri)) {
            $entity->setError('uri', PreControlMessages::getLastErrorMessage());
        }
        $Transfers->transition($entity, TransfersTable::T_PREPARE);
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        if ($Transfers->save($entity)) {
            if (!is_file($entity->get('xml'))) {
                Filesystem::rename($uri, $entity->get('xml'));
            }

            Utility::get('Notify')->emit(
                'new_transfer',
                [
                    'user_id' => $this->userId,
                    'sa_id' => $this->archivalAgencyId,
                    'count' => 1,
                ]
            );

            $this->Modal->success();
            $entity = $Transfers->find()
                ->where(['Transfers.id' => $entity->get('id')])
                ->contain(['ArchivalAgencies', 'TransferringAgencies', 'OriginatingAgencies'])
                ->firstOrFail();
            return $this->renderDataToJson($entity)->withHeader(
                'X-Asalae-Success',
                'true'
            );
        } else {
            clearstatcache(); // is_file could be wrong if we don't do that before
            if (is_file($uri)) {
                unlink($uri);
            }
            $this->Modal->fail();
            FormatError::logEntityErrors($entity);
            return $this->renderDataToJson(['errors' => $entity->getErrors()]);
        }
    }

    /**
     * Met un transfer 'rejected' à l'état 'preparating'
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function reEdit(string $id)
    {
        /** @var Transfer $transfer */
        $transfer = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.id' => $id,
                    'Transfers.state' => TransfersTable::S_REJECTED,
                    'Transfers.created_user_id' => $this->userId,
                ]
            )
            ->firstOrFail();

        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $Transfers->transitionOrFail(
            $transfer,
            TransfersTable::T_PREPARE
        );
        $Transfers->saveOrFail($transfer);
        return $this->renderDataToJson($transfer->toArray());
    }

    /**
     * Permet d'editer un transfert qui n'est pas dans l'application
     * @param string $base64Path
     * @param mixed  ...$path
     * @return Response
     * @throws Exception
     */
    public function repair(string $base64Path, ...$path)
    {
        $xmlPath = $this->decodeXmlPath($base64Path);
        $size = filesize($xmlPath);
        LimitBreak::setTimeLimit($size / 100000); // 46Mo = 460 seconds
        LimitBreak::setMemoryLimit($size * 10); // 46Mo = 460Mo
        /** @var Transfer $entity */
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $entity = $Transfers->newEntityFromXml($xmlPath);
        $entity->set('archival_agency_id', $this->archivalAgencyId);

        if ($this->getRequest()->is('delete')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            return $this->renderDataToJson("done");
        }
        if ($this->getRequest()->is('post')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($this->getRequest()->getData('precontrol')) {
                if ($errors = $entity->validateFile($xmlPath)) {
                    $this->Modal->fail();
                    return $this->renderDataToJson($errors);
                }
                $precontrol = new PreControlMessages($xmlPath);
                if ($precontrol->validate()) {
                    $this->Modal->success();
                } else {
                    $this->Modal->fail();
                    return $this->renderDataToJson(
                        PreControlMessages::$lastErrorMessage
                    );
                }
            }
        }

        $this->set('entity', $entity);
        $this->set('base64Path', $base64Path);

        $uploadId = 'transfer-repair-upload-table';
        $this->set('uploadId', $uploadId);
        $this->set('path', $path);

        $target = $base64Path . '/' . implode('/', $path);
        $refreshJsBase = 'repairTransfer';
        $refreshJsCallback = "$refreshJsBase('$target')";
        $this->set('refreshJsBase', $refreshJsBase);
        $this->set('refreshJsCallback', $refreshJsCallback);
    }

    /**
     * Modifie l'identifiant unique d'un transfert qui n'est pas dans l'application
     * @param string $base64Path
     * @return Response
     * @throws Exception
     */
    public function generateIdentifier(string $base64Path)
    {
        $this->getRequest()->allowMethod('post');
        $path = $this->decodeXmlPath($base64Path);
        $size = filesize($path);
        LimitBreak::setTimeLimit($size / 100000); // 46Mo = 460 seconds
        LimitBreak::setMemoryLimit($size * 10); // 46Mo = 460Mo
        $preControl = new PreControlMessages($path);
        /** @var CountersTable $Counters */
        $Counters = $this->fetchTable('Counters');
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $generated = $Counters->next(
            $this->archivalAgencyId,
            'ArchiveTransfer',
            [],
            function ($generated) use ($Transfers) {
                $cond = [
                    'transfer_identifier' => $generated,
                    'archival_agency_id' => $this->archivalAgencyId,
                ];
                return !$Transfers->exists($cond);
            }
        );
        /** @var DOMElement $identifier */
        $identifier = $preControl->getXpath()->query(
            $preControl->identifierIdPaths[$preControl->getNamespace()]
        )->item(0);
        DOMUtility::setValue($identifier, $generated);
        $preControl->getDom()->save($path);
        if (!$preControl->validate()) {
            throw new BadRequestException(
                PreControlMessages::getLastErrorMessage()
            );
        }
        $entity = $Transfers->newEntityFromXml($path);
        $Transfers->patchEntity(
            $entity,
            $entity->toArray() + [
                'state' => $Transfers->initialState,
                'is_modified' => false,
                'created_user_id' => $this->userId,
            ]
        );
        if ($entity->getErrors()) {
            $first = current($entity->getErrors());
            throw new BadRequestException(current($first));
        }

        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = $this->fetchTable('Fileuploads');
        $entity = $Fileuploads->newUploadedEntity(
            basename($path),
            $path,
            $this->userId
        );
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        if ($Fileuploads->save($entity)) {
            $this->Modal->success();
            return $this->renderDataToJson(
                $entity->toArray() + ['transfer_identifier' => $generated]
            );
        } else {
            $this->Modal->fail();
            FormatError::logEntityErrors($entity);
            return $this->renderDataToJson(['error' => $entity->getErrors()]);
        }
    }

    /**
     * Donne le chemin xpath d'un attachement
     * @param string $id
     * @param string $transfer_attachment_id
     * @return Response
     * @throws Exception
     */
    public function getXpath(
        string $id,
        string $transfer_attachment_id
    ): Response {
        if (is_numeric($id)) {
            /** @var Transfer $entity */
            $entity = $this->Seal->archivalAgency()
                ->table('Transfers')
                ->find('seal')
                ->where(['Transfers.id' => $id])
                ->firstOrFail();
            $tempXml = $entity->getTempXml($this->userId);
        } else {
            $tempXml = $this->decodeXmlPath($id);
        }
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        $attachment = $TransferAttachments->get($transfer_attachment_id);
        $util = DOMUtility::load($tempXml);
        $element = ArchiveBinariesTable::findBinaryByFilename($util, $attachment->get('filename'));
        if ($element) {
            return $this->renderDataToJson(
                $util->getElementXpath($element, '')
            );
        } else {
            return $this->renderJson('"not found"');
        }
    }

    /**
     * Création d'un transfert à partir d'un bordereau réparé
     * @param string $base64Path
     * @return Response
     * @throws ZMQSocketException
     * @throws DateInvalidTimeZoneException
     */
    public function createFromExisting(string $base64Path)
    {
        $uri = $tempXml = $this->decodeXmlPath($base64Path);
        $size = filesize($uri);
        LimitBreak::setTimeLimit($size / 100000); // 46Mo = 460 seconds
        LimitBreak::setMemoryLimit($size * 10); // 46Mo = 460Mo
        /** @var Transfer $entity */
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $entity = $Transfers->newEntityFromXml($uri);
        $Transfers->patchEntity(
            $entity,
            $entity->toArray() + [
                'state' => $Transfers->initialState,
                'is_modified' => false,
                'archival_agency_id' => $this->archivalAgencyId,
                'created_user_id' => $this->userId,
                'tmp_xml' => $tempXml,
                'files_deleted' => false,
            ]
        );
        $entity->set('uri', $uri);
        if (!PreControlMessages::isValid($uri)) {
            $entity->setError('uri', PreControlMessages::getLastErrorMessage());
        }
        $Transfers->transition($entity, TransfersTable::T_PREPARE);
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        $errors = $entity->validateFile($tempXml);
        if (empty($errors) && $Transfers->save($entity)) {
            if (!is_file($entity->get('xml'))) {
                Filesystem::rename($uri, $entity->get('xml'));
            }
            $this->Modal->success();
            $this->Modal->step(
                'addTransferUploadStep2(' . $entity->get('id') . ')'
            );
            Utility::get('Notify')->emit(
                'new_transfer',
                [
                    'user_id' => $this->userId,
                    'sa_id' => $this->archivalAgencyId,
                    'count' => 1,
                ]
            );
            return $this->renderDataToJson($entity->toArray());
        } else {
            $this->Modal->fail();
            FormatError::logEntityErrors($entity);
            return $this->renderDataToJson(
                $errors ?: (array)PreControlMessages::getLastErrorMessage()
            );
        }
    }

    /**
     * Permet d'obtenir les meta-données lié à l'édition d'un transfert
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function getEditMeta(string $id)
    {
        /** @var Transfer $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.id' => $id,
                    'Transfers.state IN' => [
                        TransfersTable::S_PREPARATING,
                        TransfersTable::S_VALIDATING,
                    ],
                ]
            )
            ->firstOrFail();
        if (!is_file($entity->get('xml'))) {
            throw new NotFoundException(
                __("Le fichier xml du transfert n'a pas été trouvé")
            );
        }
        $serializedMeta = $entity->getSerializedMeta($this->userId);
        $meta = is_file($serializedMeta) ? unserialize(
            file_get_contents($serializedMeta)
        ) : null;

        if ($this->getRequest()->getQuery('validate')) {
            if ($meta && $meta['isDirty']) {
                $meta['validate'] = null;
            } else {
                $meta = ['validate' => empty($entity->validateFile())];
            }
        }

        return $this->renderDataToJson($meta);
    }

    /**
     * Annule l'analyse
     * @return Response
     */
    public function abortAnalyse()
    {
        /** @var KillablesTable $Killables */
        $Killables = $this->fetchTable('Killables');
        $killables = $Killables->find()
            ->where(
                [
                    'session_token' => $this->getRequest()->getSession()->read(
                        'Session.token'
                    ),
                ]
            )
            ->all();
        /** @var EntityInterface $killable */
        foreach ($killables as $killable) {
            $Killables->kill($killable->get('pid'), $this->userId);
        }
        return $this->renderDataToJson('done');
    }

    /**
     * Page Tous les transferts
     * @throws Exception
     */
    public function indexAll()
    {
        /** @use JstableComponent */
        $this->loadComponent('AsalaeCore.Jstable');
        $this->Jstable->configurablePagination(self::TABLE_ALL_INDEX);
        $this->set('tableId', self::TABLE_ALL_INDEX);
        $this->set('modalXmlToHtml', self::MODAL_XML_TO_HTML);
        $this->set('transferEntrantIndex', true); // @temporary, cf. #304
        $query = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->contain(['ArchivalAgencies', 'TransferringAgencies', 'OriginatingAgencies']);
        $this->set(
            'groupedActions',
            [
                '0' => __("-- action groupée --"),
                'delete' => __("Supprimer"),
                'submit' => __("Envoyer"),
            ]
        );
        $this->indexCommons([], $query);
    }

    /**
     * select2 ajax pour les liens entre binary et archive_unit (seda2.1)
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function populateSelect(string $id)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->paginate['limit'] = Configure::read('Pagination.limit', 20);
        if (is_numeric($id)) {
            /** @var Transfer $entity */
            $entity = $this->Seal->archivalAgency()
                ->table('Transfers')
                ->find('seal')
                ->where(['Transfers.id' => $id])
                ->firstOrFail();
            $tempXml = $entity->getTempXml($this->userId);
        } else {
            $tempXml = $this->decodeXmlPath($id);
        }

        $output = ['results' => [], 'pagination' => ['more' => false]];
        $util = DOMUtility::load($tempXml);
        $DataObjectPackage = $util->node(
            '/ns:ArchiveTransfer/ns:DataObjectPackage'
        );
        $binaries = $util->xpath->query(
            '(ns:DataObjectGroup/ns:BinaryDataObject)|ns:BinaryDataObject',
            $DataObjectPackage
        );
        $page = $this->getRequest()->getData('page', 1);
        if ($binaries->count() > $this->paginate['limit'] * $page) {
            $output['pagination']['more'] = true;
        }
        $limit = $this->paginate['limit'];
        $offset = $limit * $page - $limit;
        /** @var DOMElement $binary */
        foreach ($binaries as $binary) {
            if ($offset) {
                $offset--;
                continue;
            }
            $binaryIdentifier = $binary->getAttribute('id');
            $filename = ArchiveBinariesTable::getBinaryFilename($util, $binary);
            $qContent = $util->xpathQuote($binaryIdentifier);
            $path = sprintf(".//ns:DataObjectReferenceId[contains(text(), %s)]", $qContent);
            $linked = $util->xpath->query($path)->count();
            /** @var DOMElement $binaryParent */
            $binaryParent = $binary->parentNode;
            if ($binaryParent->nodeName === 'DataObjectGroup') {
                $qContent = $util->xpathQuote($binaryParent->getAttribute('id'));
                $path = sprintf(".//ns:DataObjectGroupReferenceId[contains(text(), %s)]", $qContent);
                $linked += $util->xpath->query($path)->count();
            }
            $DataObjectGroupId = $util->node('./ns:DataObjectGroupId', $binary);
            if ($DataObjectGroupId) {
                $qContent = $util->xpathQuote(trim($DataObjectGroupId->nodeValue));
                $path = sprintf(".//ns:DataObjectGroupReferenceId[contains(text(), %s)]", $qContent);
                $linked += $util->xpath->query($path)->count();
            }
            $output['results'][] = [
                'id' => h($binaryIdentifier),
                'text' => sprintf('%s (%d)', h($filename), $linked),
            ];

            $limit--;
            if ($limit === 0) {
                break;
            }
        }

        return $this->renderDataToJson($output);
    }

    /**
     * select2 ajax pour les liens entre binary_group et archive_unit (seda2.1)
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function populateGroupSelect(string $id)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->paginate['limit'] = Configure::read('Pagination.limit', 20);
        if (is_numeric($id)) {
            /** @var Transfer $entity */
            $entity = $this->Seal->archivalAgency()
                ->table('Transfers')
                ->find('seal')
                ->where(['Transfers.id' => $id])
                ->firstOrFail();
            $tempXml = $entity->getTempXml($this->userId);
        } else {
            $tempXml = $this->decodeXmlPath($id);
        }

        $output = ['results' => [], 'pagination' => ['more' => false]];
        $util = DOMUtility::load($tempXml);
        $DataObjectPackage = $util->node(
            '/ns:ArchiveTransfer/ns:DataObjectPackage'
        );
        $groups = $util->xpath->query(
            'ns:DataObjectGroup',
            $DataObjectPackage
        );
        $page = $this->getRequest()->getData('page', 1);
        if ($groups->count() > $this->paginate['limit'] * $page) {
            $output['pagination']['more'] = true;
        }
        $limit = $this->paginate['limit'];
        $offset = $limit * $page - $limit;
        /** @var DOMElement $group */
        foreach ($groups as $group) {
            if ($offset) {
                $offset--;
                continue;
            }
            $groupIdentifier = $group->getAttribute('id');
            $filename = $util->nodeValue(
                'nsBinaryDataObject/ns:FileInfo/ns:Filename',
                $group
            );
            if (!$filename) {
                $filename = $util->node(
                    'ns:BinaryDataObject/ns:Attachment',
                    $group
                );
                if ($filename instanceof DOMElement) {
                    $filename = $filename->getAttribute('filename');
                } else {
                    $filename = $util->nodeValue(
                        'ns:BinaryDataObject/ns:Uri',
                        $group
                    );
                }
            }
            $qContent = $util->xpathQuote($groupIdentifier);
            $linked = $util->xpath->query(
                sprintf(".//ns:DataObjectGroupReferenceId[contains(text(), %s)]", $qContent)
            )->count();
            $output['results'][] = [
                'id' => h($groupIdentifier),
                'text' => sprintf('%s (%d)', h($filename), $linked),
            ];

            $limit--;
            if ($limit === 0) {
                break;
            }
        }

        return $this->renderDataToJson($output);
    }

    /**
     * Donne le fichier PDF lié à un transfert
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function downloadPdf(string $id): Response
    {
        /** @var Transfer $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(['Transfers.id' => $id])
            ->firstOrFail();
        $pdf = $entity->get('pdf');
        $mtime = is_file($pdf) ? new DateTime('@' . filemtime($pdf)) : null;
        if (
            !$mtime
            || $mtime <= $entity->get('modified')
            || $this->getRequest()->getQuery('renew')
        ) {
            $xml = $entity->get('xml');
            $generator = new Seda2Pdf($xml);
            $generator->generate($entity->get('pdf'));
        }
        return $this->getCoreResponse()
            ->withFileStream($pdf, ['mime' => 'application/pdf']);
    }

    /**
     * Commande une génération de fichier PDF
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function makePdf(string $id): Response
    {
        /** @var Transfer $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(['Transfers.id' => $id])
            ->firstOrFail();
        $pdf = $entity->get('pdf');
        $mtime = is_file($pdf) ? new DateTime('@' . filemtime($pdf)) : null;
        if (
            $mtime
            && $mtime > $entity->get('modified')
            && !$this->getRequest()->getQuery('renew')
        ) {
            return $this->getResponse()
                ->withHeader('X-Asalae-PdfFileDownload', 'download');
        } elseif (is_file($pdf)) {
            unlink($pdf);
        }

        /** @var Exec $exec */
        $exec = Utility::get(Exec::class);
        $command = sprintf(
            '%s %s --force',
            CAKE_SHELL,
            'pdf_files'
        );
        $exec->async(
            $command,
            $entity->get('xml'),
            $entity->get('pdf'),
            $this->userId,
            '/transfers/download-pdf/' . $id
        );

        return $this->getResponse()
            ->withHeader('X-Asalae-PdfFileDownload', 'email')
            ->withStringBody(
                __(
                    "Le fichier est trop volumineux pour être téléchargé dans l'immédiat,"
                    . "un email vous sera envoyé dès qu'il sera disponible."
                )
            );
    }

    /**
     * Réception d'un transfert
     * @throws Exception
     */
    protected function apiPost()
    {
        $this->apiControlPost();
        $request = $this->getRequest();
        $dryrun = $request->getData('dryrun');
        if ($dryrun) {
            return $this->renderDataToJson('ok');
        }
        $targetDir = Configure::read(
            'Transfers.post.downloadDir',
            rtrim(
                Configure::read('App.paths.data'),
                DS
            ) . DS . 'download' . DS . 'transfer'
        );
        $targetDir .= DS . uniqid();

        if ($size = (int)$request->getHeaderLine('File-Size')) {
            $limit = $size / 100000; // 2.5Go ~= 7h30
            LimitBreak::setTimeLimit($limit);
        } else {
            $limit = 0;
            set_time_limit(0); // anciennes versions asalae
        }
        $targetFilename = $this->moveOrDownloadTransfer(
            $targetDir . DS . 'download',
            $limit
        );
        DataCompressor::uncompress($targetFilename, $targetDir . DS . 'data');
        $basedir = $targetDir . DS . 'data';
        $messageFiles = is_dir($basedir . DS . 'message')
            ? glob($basedir . DS . 'message' . DS . '*.xml')
            : glob($basedir . DS . 'management_data' . DS . '*transfer.xml');
        $uri = end($messageFiles);
        $xml = Transliterator::create('NFC')
            ->transliterate(file_get_contents($uri));
        file_put_contents($uri, $xml);
        if (!PreControlMessages::isValid($uri)) {
            Filesystem::remove($targetDir);
            throw new BadRequestException(
                'PreControl failed: ' . PreControlMessages::$lastErrorMessage
            );
        }

        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $entity = $Transfers->newEntityFromXml($uri);
        $Transfers->patchEntity(
            $entity,
            $entity->toArray() + [
                'state' => $Transfers->initialState,
                'is_modified' => false,
                'created_user_id' => $this->userId,
                'tmp_xml' => $uri,
                'files_deleted' => false,
            ]
        );
        $Transfers->transition($entity, TransfersTable::T_PREPARE);
        $conn = $Transfers->getConnection();
        $conn->begin();
        $Transfers->saveOrFail($entity);
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        $entry = $EventLogs->newEntry(
            'transfer_add',
            'success',
            __(
                "Prise en compte du transfert ''{0}''",
                $entity->get('transfer_identifier')
            ),
            $this->userId,
            $entity
        );
        $entry->set('in_lifecycle', false);
        $EventLogs->saveOrFail($entry);
        if (!is_file($entity->get('xml'))) {
            Filesystem::rename($uri, $entity->get('xml'));
        }
        if (is_dir($basedir . DS . 'attachments')) {
            $this->appendAttachements($entity, $basedir . DS . 'attachments');
        } else {
            $this->appendAttachements($entity, $basedir . DS . 'original_data');
        }
        $this->sendTransfer($entity->id);
        $conn->commit();
        Filesystem::remove($targetDir);
        if (is_file($targetFilename)) {
            unlink($targetFilename); // Supprime le zip d'un upload par morceaux
        }

        $report = ['success' => true, 'id' => $entity->id];
        $uploadedUrl = $request->getData('transferUploadedUrl');
        if ($uploadedUrl) {
            $params = [
                'ssl_verify_peer' => false,
                'ssl_verify_host' => false,
                'redirect' => true,
            ];
            if (Configure::read('Proxy.host')) {
                $params['proxy'] = [
                    'proxy' => Configure::read(
                        'Proxy.host'
                    ) . ':' . Configure::read('Proxy.port'),
                    'username' => Configure::read('Proxy.login'),
                    'password' => Configure::read('Proxy.password'),
                ];
            }
            /** @var Client $client */
            $client = get_class(Utility::get(Client::class));
            $client = new $client($params);
            $response = $client->post($uploadedUrl);
            $report['uploaded_response'] = $response->getStringBody();
        }
        return $this->renderDataToJson($report);
    }

    /**
     * Vérifi que les données envoyés sont cohérentes
     * @throws BadRequestException|Exception
     */
    private function apiControlPost()
    {
        $request = $this->getRequest();
        $dryrun = $request->getData('dryrun');
        $transfer = $request->getUploadedFile('transfer')
            ?: $request->getData(
                'transfer'
            );
        $transferIdentifier = $request->getData('transferIdentifier');
        $archivalAgencyIdentifier = $request->getData(
            'archivalAgencyIdentifier'
        );
        $transferringAgencyIdentifier = $request->getData(
            'transferringAgencyIdentifier'
        );
        if ($dryrun && !$transferIdentifier) {
            throw new BadRequestException(
                'missing transferIdentifier FormData'
            );
        }
        if ((!$dryrun && !$transfer)) {
            throw new BadRequestException('missing transfer FormData');
        }
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        if (
            $transferIdentifier && $Transfers->exists(
                ['transfer_identifier' => $transferIdentifier]
            )
        ) {
            throw new BadRequestException(
                "message already exists: $transferIdentifier"
            );
        }
        if (
            $archivalAgencyIdentifier
            && $archivalAgencyIdentifier !== $this->archivalAgency->get('identifier')
        ) {
            throw new BadRequestException(
                "archivalAgencyIdentifier does not match user archivalAgency"
            );
        }
        if ($transferringAgencyIdentifier) {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = $this->Seal->archivalAgency()
                ->table('OrgEntities');
            $exists = $OrgEntities->sealExists(['OrgEntities.identifier' => $transferringAgencyIdentifier]);
            if (!$exists) {
                throw new BadRequestException(
                    "transferringAgencyIdentifier does not exists"
                );
            }
        }
    }

    /**
     * Gestion du téléchargement du transfert zippé
     * @param string $targetDir
     * @param int    $limit     durée d'execution
     *                          max pour le download
     * @return string uri du zip du transfert
     * @throws DataCompressorException
     */
    private function moveOrDownloadTransfer(string $targetDir, int $limit = 0): string
    {
        $request = $this->getRequest();
        if (!is_dir($targetDir)) {
            mkdir($targetDir, 0777, true);
        }
        $transfer = $request->getData('transfer');
        $uniqid = urlencode($transfer && is_string($transfer) ? $transfer : '');
        $preparedTransfer = Configure::read('App.paths.data')
            . DS . 'upload' . DS . $this->userId . DS . $uniqid
            . DS . 'completed';
        $preparedTransfer = is_dir($preparedTransfer) ? glob(
            $preparedTransfer . DS . '*'
        ) : null;

        /** @var UploadedFile $upload */
        if ($upload = $request->getUploadedFile('transfer')) {
            $targetFilename = $targetDir . DS . $upload->getClientFilename();
            $size = $upload->getSize();
            $upload->moveTo($targetFilename);
        } elseif (
            preg_match('/^[\da-f]+$/', $uniqid)
            && $preparedTransfer
            && $preparedTransfer[0]
        ) {
            $targetFilename = $preparedTransfer[0];
            $size = filesize($targetFilename);
        } elseif (
            ($url = $request->getData('transfer'))
            && preg_match('/^(http|ftp)s?:\/\/.*/', $url)
        ) {
            $targetFilename = $this->downloadDistant($url, $targetDir, $limit);
            $size = filesize($targetFilename);
        } else {
            throw new BadRequestException('no transfer file');
        }
        if (!$size || filesize($targetFilename) !== $size) {
            throw new BadRequestException('upload error');
        }
        $type = DataCompressor::detectType($targetFilename);
        $transferDigest = $request->getData('transferDigest');
        if ($transferDigest) {
            $filename = basename($targetFilename);
            $hash = hash_file('sha256', $targetFilename);
            if ($transferDigest !== md5("$filename:sha256:$hash")) { // NOSONAR
                unlink($targetFilename);
                throw new BadRequestException('file digest does not match');
            }
        }
        if (!$type) {
            throw new BadRequestException(
                __("Le fichier envoyé n'est pas un fichier zip")
            );
        } elseif ($type === 'zip') {
            DataCompressor::sanitizeZip($targetFilename);
        }
        return $targetFilename;
    }

    /**
     * Télécharge un fichier distant ($url) vers le dossier $targetDir en conservant
     * le nom initial et en suivant les redirections
     * @param string $url
     * @param string $targetDir
     * @param int    $limit     durée d'execution
     *                          max pour le download
     * @return string
     * @throws Exception
     */
    private function downloadDistant(string $url, string $targetDir, int $limit = 0)
    {
        if (!is_dir($targetDir)) {
            mkdir($targetDir, 0777, true);
        }
        $params = [
            'adapter' => StreamLargeFilesCurl::class,
            'ssl_verify_peer' => false,
            'ssl_verify_host' => false,
            'redirect' => true,
            'curl' => [
                CURLOPT_TIMEOUT => $limit ? $limit + 30 : 0,
            ],
        ];
        if (Configure::read('Proxy.host')) {
            $params['proxy'] = [
                'proxy' => Configure::read(
                    'Proxy.host'
                ) . ':' . Configure::read('Proxy.port'),
                'username' => Configure::read('Proxy.login'),
                'password' => Configure::read('Proxy.password'),
            ];
        }
        /** @var Client $client */
        $client = get_class(Utility::get(Client::class));
        $client = new $client($params);
        $response = $client->get($url);
        $disposition = $response->getHeaderLine('Content-Disposition');
        if (!preg_match('/filename="(.*)"|filename=(.*)/', $disposition, $m)) {
            throw new BadRequestException(
                'missing Content-Disposition filename'
            );
        }
        $filename = $m ? end($m) : '';
        $contentLength = (int)$response->getHeaderLine('Content-Length');
        $max_execution_time = (int)($contentLength / 100000); // 46Mo = 460 seconds
        LimitBreak::setTimeLimit(
            $max_execution_time
        ); // 0.1s/ko (comprend aussi la décompression)
        $stream = $response->getBody()->detach();
        $targetFilename = $targetDir . DS . $filename;
        $targetFile = fopen($targetFilename, 'w');
        $size = stream_copy_to_stream($stream, $targetFile);
        if ($contentLength !== $size) {
            trigger_error(
                sprintf(
                    'file length (%d) does not match the expected size (%d)',
                    $size,
                    $contentLength
                ),
                E_USER_WARNING
            );
        }
        return $targetFilename;
    }

    /**
     * Ajoute les attachements lié à un transfert envoyé par API
     * @param EntityInterface $transfer
     * @param string          $targetUri
     * @throws Exception
     */
    private function appendAttachements(
        EntityInterface $transfer,
        string $targetUri
    ) {
        try {
            /** @var Clamav|AntivirusInterface $Antivirus */
            $Antivirus = Utility::get(Configure::read('Antivirus.classname'));
            $viruses = $Antivirus->scan($targetUri);
        } catch (Exception $e) {
            $this->log($e->getMessage());
            $viruses = null;
        }

        $exec = Utility::get('Exec')->command('sf -json', $targetUri);
        $json = json_decode($exec->stdout, true);
        if (!$json) {
            Filesystem::rollback();
            throw new Exception('siegfried failed');
        }
        $sf = [];
        foreach (Hash::get($json, 'files', []) as $file) {
            $format = Hash::get($file, 'matches.0.id') ?: 'UNKNOWN';
            $sf[$file['filename']] = [
                'format' => $format === 'UNKNOWN' ? '' : $format,
                'mime' => mime_content_type($file['filename']),
                'extension' => pathinfo($file['filename'], PATHINFO_EXTENSION),
            ];
        }

        $len = strlen($targetUri) + 1;
        $algo = Configure::read('hash_algo', 'sha256');
        $util = DOMUtility::load($transfer->get('xml'));
        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($targetUri)
        );
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        /** @var SplFileInfo $file */
        foreach ($iterator as $file) {
            if ($file->isDir()) {
                continue;
            }
            $file = $file->getPathname();
            $filename = substr($file, $len);
            $exists = $TransferAttachments->find()
                ->where(
                    [
                        'transfer_id' => $transfer->id,
                        'filename' => $filename,
                    ]
                )
                ->count();
            if ($exists) {
                continue;
            }

            if ($viruses === null) {
                $virus = null;
            } elseif (isset($viruses[$file])) {
                $virus = $viruses[$file];
            } else {
                $virus = '';
            }

            $node = ArchiveBinariesTable::findBinaryByFilename($util, $filename);
            $xpath = $node ? $util->getElementXpath($node, '') : null;

            $entity = $TransferAttachments->newEntity(
                [
                    'transfer_id' => $transfer->id,
                    'filename' => $filename,
                    'size' => filesize($file),
                    'hash' => hash_file($algo, $file),
                    'hash_algo' => $algo,
                    'deletable' => true,
                    'mime' => mime_content_type($file),
                    'extension' => $sf[$file]['extension'],
                    'format' => $sf[$file]['format'],
                    'virus_name' => $virus,
                    'xpath' => $xpath,
                    'valid' => Utility::get(FileValidator::class)->isValid($file),
                ]
            );
            $TransferAttachments->saveOrFail($entity);
            Filesystem::rename($file, $entity->get('path'));
        }
    }

    /**
     * Comme Transfers::send
     * @param int|string $id
     * @throws Exception
     */
    private function sendTransfer($id)
    {
        /** @var Beanstalk $Beanstalk */
        $Beanstalk = Utility::get('Beanstalk');
        if (!$Beanstalk->isConnected()) {
            throw new ServiceUnavailableException();
        }

        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        /** @var Transfer $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.id' => $id,
                    'Transfers.state' => TransfersTable::S_PREPARATING,
                ]
            )
            ->contain(
                [
                    'ServiceLevels' => ['TsMsgs', 'TsPjss', 'TsConvs'],
                    'TransferLocks',
                ]
            )
            ->firstOrFail();

        $lock = $entity->get('transfer_lock');
        if ($lock) {
            $this->fetchTable('TransferLocks')->delete($lock);
            $entity->unset('transfer_lock');
        }

        $serviceLevel = $entity->get('service_level');
        if (!$serviceLevel) {
            $serviceLevel = $this->Seal->archivalAgency()
                ->table('ServiceLevels')
                ->find('seal')
                ->where(['ServiceLevels.default_level' => true])
                ->contain(['TsMsgs', 'TsPjss', 'TsConvs'])
                ->first();
        }

        /** @var Beanstalk $Beanstalk */
        if (
            $serviceLevel && ($serviceLevel->get(
                'ts_msg_id'
            ) || $serviceLevel->get('ts_pjs_id'))
        ) {
            $job = [
                'transfer_id' => $entity->get('id'),
                'user_id' => $this->userId,
                'ts_msg_id' => Hash::get(
                    $serviceLevel,
                    'ts_msg.timestamper_id'
                ),
                'ts_pjs_id' => Hash::get(
                    $serviceLevel,
                    'ts_pjs.timestamper_id'
                ),
                'ts_conv_id' => Hash::get(
                    $serviceLevel,
                    'ts_conv.timestamper_id'
                ),
            ];
            $Transfers->transition($entity, TransfersTable::T_TIMESTAMP);
            $Transfers->saveOrFail($entity);
            $Beanstalk->setTube('timestamp');
        } else {
            $job = [
                'transfer_id' => $entity->get('id'),
                'user_id' => $this->userId,
            ];
            $Transfers->transition($entity, TransfersTable::T_ANALYSE);
            $Transfers->saveOrFail($entity);
            $Beanstalk->setTube('analyse');
        }
        $Transfers->createCompositeIfNeeded($entity);
        $Beanstalk->emit($job, Pheanstalk::DEFAULT_PRIORITY, 1);
    }

    /**
     * Envoi par morceaux d'un transfert
     *
     * 204 - existe déjà
     * 206 - chunk transmis
     * 201 - fichier transmis en entier
     * 400 - mauvaise requête ou validation du fichier en echec
     *
     * @param string $uniqid
     * @return Response
     * @throws Exception
     */
    protected function prepareChunked(string $uniqid)
    {
        $request = $this->getRequest();
        $request->allowMethod('post');
        $data = [
            'filename' => $request->getQuery('filename'),
            'currentSize' => (int)$request->getQuery('currentSize'),
            'currentChunk' => (int)$request->getQuery('currentChunk'),
            'totalSize' => (int)$request->getQuery('totalSize'),
            'totalChunks' => (int)$request->getQuery('totalChunks'),
            'crc32b' => $request->getQuery('crc32b'),
            'finalSha256' => $request->getQuery('finalSha256'),
        ];
        if (preg_match('/[^\da-f]/', $uniqid)) {
            throw new BadRequestException('this uniqid is not allowed');
        }
        foreach ($data as $key => $value) {
            if (!$value) {
                throw new BadRequestException($key . ' queryParam is required');
            }
        }
        $baseDir = Configure::read('App.paths.data')
            . DS . 'upload' . DS . $this->userId . DS . urlencode($uniqid);
        $chunkDir = $baseDir . DS . 'chunks';
        $chunkCompletedDir = $baseDir . DS . 'chunksCompleted';
        $chunkMergedDir = $baseDir . DS . 'chunksMerged';
        $finalDir = $baseDir . DS . 'completed';
        $patialFinalFile = $finalDir . DS . '_' . $data['filename'];
        $finalFile = $finalDir . DS . $data['filename'];
        $readme = $baseDir . DS . 'readme.txt';
        $len = strlen((string)$data['totalChunks']);
        $chunkName = sprintf("%0{$len}d", $data['currentChunk']);
        $chunkFile = $chunkDir . DS . $chunkName;
        $chunkCompletedFile = $chunkCompletedDir . DS . $chunkName;
        $chunkMergedFile = $chunkCompletedDir . DS . $chunkName;

        if (file_exists($finalFile)) {
            if (hash_file('sha256', $finalFile) === $data['finalSha256']) {
                return $this->renderData("this file is already uploaded")
                    ->withStatus(204);
            } else {
                @unlink($finalFile);
            }
        }
        if (file_exists($chunkCompletedFile)) {
            return $this->renderData("this chunk already exists")
                ->withStatus(204);
        }

        if (!is_dir($chunkDir)) {
            mkdir($chunkDir, 0770, true);
            mkdir($chunkCompletedDir, 0770, true);
            mkdir($chunkMergedDir, 0770, true);
            mkdir($finalDir, 0770, true);
            file_put_contents($patialFinalFile, '');
            file_put_contents(
                $readme,
                sprintf(
                    "totalSize: %s\nsha256: %s\ndate: %s",
                    Number::toReadableSize($data['totalSize']),
                    h($request->getQuery('finalSha256')),
                    date(DATE_RFC3339)
                )
            );
        }

        $chunkHandle = fopen($chunkFile, 'w');
        $body = $request->getBody();
        $body->rewind();
        while (!$body->eof()) {
            fwrite($chunkHandle, $body->read(4096));
        }
        fclose($chunkHandle);

        if (
            filesize($chunkFile) !== $data['currentSize']
            || hash_file('crc32b', $chunkFile) !== $data['crc32b']
        ) {
            unlink($chunkFile);
            throw new BadRequestException("chunk's crc32b validation failed");
        }
        file_put_contents($chunkCompletedFile, '');

        $merged = glob($chunkMergedDir . DS . '*');
        $lastMergedChunk = (int)basename(end($merged));
        if (($lastMergedChunk + 1) === $data['currentChunk']) {
            $mergedHandle = fopen($patialFinalFile, 'a');
            $i = $data['currentChunk'];
            foreach (glob($chunkDir . DS . '*') as $chunkFile) {
                $currentChunkFilename = basename($chunkFile);
                if (
                    !file_exists(
                        $chunkCompletedDir . DS . $currentChunkFilename
                    )
                ) {
                    continue;
                }
                $chunkIndex = (int)$currentChunkFilename;
                if ($chunkIndex > $i) {
                    break;
                } elseif ($chunkIndex === $i) {
                    $i++;
                    $chunkHandle = fopen($chunkFile, 'r');
                    while (!feof($chunkHandle)) {
                        fwrite($mergedHandle, fread($chunkHandle, 4096));
                    }
                    fclose($chunkHandle);
                    unlink($chunkFile);
                    file_put_contents(
                        $chunkMergedDir . DS . $currentChunkFilename,
                        ''
                    );
                }
            }
            fclose($mergedHandle);
        }

        $allCompletedChunks = glob($chunkCompletedDir . DS . '*');
        $existingChunks = count($allCompletedChunks);

        if ($existingChunks === $data['totalChunks']) {
            $mergedHandle = fopen($chunkMergedFile, 'a');
            foreach (glob($chunkDir . DS . '*') as $chunkFile) {
                $currentChunkIndex = basename($chunkFile);
                $chunkIndex = (int)$chunkFile;
                if ($chunkIndex > $lastMergedChunk) {
                    $chunkHandle = fopen($chunkFile, 'r');
                    while (!feof($chunkHandle)) {
                        fwrite($mergedHandle, fread($chunkHandle, 4096));
                    }
                    fclose($chunkFile);
                    unlink($chunkFile);
                    file_put_contents(
                        $chunkMergedDir . DS . $currentChunkIndex,
                        ''
                    );
                }
            }
            fclose($mergedHandle);

            $totalsize = filesize($patialFinalFile);
            if ($totalsize === $data['totalSize']) {
                rename($patialFinalFile, $finalFile);
                if (
                    filesize($finalFile) !== $data['totalSize']
                    || hash_file('sha256', $finalFile) !== $data['finalSha256']
                ) {
                    unlink($finalFile);
                    throw new BadRequestException(
                        "final file's sha256 validation failed"
                    );
                }
                // nettoyage des fichiers temporaires
                foreach ([$chunkDir, $chunkMergedDir, $chunkCompletedDir] as $chunkDir) {
                    foreach (glob($chunkDir . DS . '*') as $filename) {
                        unlink($filename);
                    }
                    rmdir($chunkDir);
                }
                return $this->renderData("file upload completed")
                    ->withStatus(201);
            }
        }

        return $this->renderData("chunk uploaded")
            ->withStatus(206);
    }

    /**
     * API pour récupérer l'Accusé de réception
     * @param string $identifier
     * @return Response
     * @throws Exception
     */
    protected function acknowledgement(string $identifier)
    {
        $transferId = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.transfer_identifier' => $identifier,
                    'Transfers.created_user_id' => $this->userId,
                ]
            )
            ->firstOrFail()
            ->id;
        try {
            return $this->message($transferId, 'acknowledgement');
        } catch (Exception $e) {
            return $this->renderData(['message' => $e->getMessage()])
                ->withStatus(202);
        }
    }

    /**
     * Rendu du message (Acknowledgement, ArchiveTransferReply)
     * @param string $id
     * @param string $type
     * @return Response
     * @throws Exception
     */
    public function message(string $id, string $type)
    {
        /** @var Transfer $transfer */
        $transfer = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(['Transfers.id' => $id])
            ->contain(['ArchivalAgencies', 'TransferringAgencies', 'OriginatingAgencies'])
            ->firstOrFail();
        $dom = DOMUtility::arrayToDOMDocument($transfer->createResponse($type));
        return $this->renderXml($dom->saveXML());
    }

    /**
     * API pour récupérer la Notification d'acceptation
     * @param string $identifier
     * @return Response
     * @throws Exception
     */
    protected function reply(string $identifier)
    {
        $transferId = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.transfer_identifier' => $identifier,
                    'Transfers.created_user_id' => $this->userId,
                ]
            )
            ->firstOrFail()
            ->id;
        try {
            return $this->message($transferId, 'reply');
        } catch (Exception $e) {
            return $this->renderData(['message' => $e->getMessage()])
                ->withStatus(202);
        }
    }

    /**
     * Cache le transfert rejeté d'un webservice
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function hide(string $id): Response
    {
        $transfer = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->innerJoinWith('Users')
            ->where(
                [
                    'Transfers.id' => $id,
                    'Transfers.state' => TransfersTable::S_REJECTED,
                    'Users.agent_type' => 'software',
                ]
            )
            ->firstOrFail();
        $transfer->set('hidden', true);
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $Transfers->saveOrFail($transfer);
        $dir = AbstractGenericMessageForm::getDataDirectory($id);
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        return $this->renderDataToJson(['report' => 'done']);
    }
}
