<?php

/**
 * Asalae\Controller\HomeController
 */

namespace Asalae\Controller;

use Asalae\Model\Table\TransferAttachmentsTable;
use Asalae\Model\Table\TransferErrorsTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\ValidationActorsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationProcessesTable;
use AsalaeCore\Controller\RenderDataTrait;
use Cake\Utility\Hash;
use DateInterval;
use DateTime;

/**
 * Accueil / dashboard
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property TransferAttachmentsTable TransferAttachments
 * @property TransferErrorsTable      TransferErrors
 * @property TransfersTable           Transfers
 * @property ValidationActorsTable    ValidationActors
 * @property ValidationProcessesTable ValidationProcesses
 */
class HomeController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * Accueil
     */
    public function index()
    {
        if ($this->adminTenant) {
            return $this->redirect('/admin-tenants');
        }
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'Transfers',
            true
        )
            ->contain(['Transfers' => ['TransferringAgencies']])
            ->disableHydration();
        $queryValidate = clone $query;
        $queryValidate->andWhere(
            ['ValidationChains.app_type' => ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM]
        );
        $queryNotConform = clone $query;
        $queryNotConform->andWhere(
            ['ValidationChains.app_type' => ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM]
        );

        /** @var TransferErrorsTable $TransferErrors */
        $TransferErrors = $this->fetchTable('TransferErrors');
        $containErrors = function (array $e) use ($TransferErrors) {
            $transfer =& $e['transfer'];

            // effectue un contain avec limite individuelle d'enregistrements
            $errors = $TransferErrors->find()
                ->where(['transfer_id' => $transfer['id']])
                ->orderBy(['TransferErrors.id'])
                ->limit(10)
                ->toArray();

            $transfer['transfer_errors'] = $errors;
            return $e;
        };
        $validateConform = $queryValidate->limit(10)
            ->all()
            ->toArray();
        $validateNotConform = $queryNotConform->limit(10)
            ->all()
            ->map($containErrors)
            ->toArray();

        $this->set('to_validate_count', $queryValidate->count());
        $this->set('not_conform_count', $queryNotConform->count());
        $this->set('to_validate', $validateConform);
        $this->set('not_conform', $validateNotConform);

        $dataChartTransfers = [];
        $dataChartTransfersAccepted = [];
        $dataChartTransfersRefused = [];
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $now = new DateTime();
        $sixDays = new DateInterval('P6D');
        $sevenDays = new DateInterval('P7D');
        $sixDaysAgo = (clone $now)->sub($sixDays);
        $sevenDaysAgo = (clone $now)->sub($sevenDays);
        $query = $Transfers->find()
            ->select(['transfer_date', 'is_accepted'])
            ->where(
                [
                    'Transfers.archival_agency_id' => $this->archivalAgencyId,
                    'Transfers.transfer_date >' => $sixDaysAgo->format('Y-m-d'),
                ]
            )
            ->orderBy(['transfer_date' => 'asc'])
            ->contain(['ArchivalAgencies', 'TransferringAgencies']);
        if (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->where(
                [
                    'TransferringAgencies.lft >=' => Hash::get($this->orgEntity, 'lft'),
                    'TransferringAgencies.rght <=' => Hash::get($this->orgEntity, 'rght'),
                ]
            );
        } else {
            $query->where(['TransferringAgencies.id' => $this->orgEntityId]);
        }
        $oneDay = new DateInterval('P1D');
        while ($sevenDaysAgo < $now) {
            $sevenDaysAgo->add($oneDay);
            $day = (int)$sevenDaysAgo->format('d');
            $dataChartTransfers[$day] = 0;
            $dataChartTransfersAccepted[$day] = 0;
            $dataChartTransfersRefused[$day] = 0;
        }
        $dataChartRatio = [0, 0, 0]; // ["acceptés", "refusés", "en cours"]
        $all = $query
            ->disableHydration()
            ->all();
        foreach ($all as $data) {
            /** @var DateTime $edate */
            $edate = $data['transfer_date'];
            $day = (int)$edate->format('d');
            if (!isset($dataChartTransfers[$day])) {
                continue;
            }

            if ($data['is_accepted']) {
                $dataChartTransfersAccepted[$day]++;
                $dataChartRatio[0]++;
            } elseif ($data['is_accepted'] === false) {
                $dataChartRatio[1]++;
                $dataChartTransfersRefused[$day]++;
            } else {
                $dataChartTransfers[$day]++;
                $dataChartRatio[2]++;
            }
        }

        $this->set('dataChartTransfers', $dataChartTransfers);
        $this->set('dataChartTransfersAccepted', $dataChartTransfersAccepted);
        $this->set('dataChartTransfersRefused', $dataChartTransfersRefused);
        $this->set('dataChartRatio', $dataChartRatio);
    }
}
