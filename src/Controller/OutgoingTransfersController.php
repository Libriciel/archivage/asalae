<?php

/**
 * Asalae\Controller\OutgoingTransfersController
 */

namespace Asalae\Controller;

use Asalae\Model\Table\ArchiveUnitsOutgoingTransferRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\AuthUrlsTable;
use Asalae\Model\Table\OutgoingTransfersTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\RenderDataTrait;
use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Event\EventInterface;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Response;
use Cake\Http\Response as CakeResponse;
use Cake\ORM\Query;
use Exception;

/**
 * OutgoingTransferRequests
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AuthUrlsTable                             AuthUrls
 * @property OutgoingTransfersTable                    OutgoingTransfers
 * @property ArchiveUnitsOutgoingTransferRequestsTable ArchiveUnitsOutgoingTransferRequests
 */
class OutgoingTransfersController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use RenderDataTrait;
    use ApiTrait;

    /**
     * Options de pagination par défaut
     * @var array
     */
    public array $paginate = [
        'order' => [
            'OutgoingTransfers.id' => 'desc',
        ],
    ];

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'zip',
            'deleteZip',
        ];
    }

    /**
     * Authorize processTransfer (AuthUrls)
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse|void|null
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $AuthenticationComponent->addUnauthenticatedActions(['zip', 'deleteZip']);
        return parent::beforeFilter($event);
    }

    /**
     * Liste les transferts sortant en cours
     */
    public function indexPreparating()
    {
        $query = $this->Seal->archivalAgency()
            ->table('OutgoingTransfers')
            ->find('seal')
            ->where(
                [
                    'OutgoingTransfers.state IN' => [
                        OutgoingTransfersTable::S_CREATING,
                        OutgoingTransfersTable::S_READY_TO_SEND,
                        OutgoingTransfersTable::S_SENT,
                        OutgoingTransfersTable::S_RECEIVED,
                        OutgoingTransfersTable::S_ERROR,
                    ],
                ]
            );
        $this->indexCommons($query);
    }

    /**
     * Liste les enregistrements
     * @param Query $query
     * @return CakeResponse|void
     * @throws Exception
     */
    private function indexCommons(Query $query)
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');

        $query->contain(['OutgoingTransferRequests' => ['CreatedUsers' => ['OrgEntities']]]);
        $IndexComponent->setQuery($query)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('state', IndexComponent::FILTER_IN);

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/commons');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);

        // override $IndexComponent->init()
        $viewBuilder = $this->viewBuilder();
        if (
            $this->getRequest()->is('ajax') && $viewBuilder->getLayout() === null
        ) {
            $viewBuilder->setTemplate('ajax_index');
        }

        // options
        $this->set('states', $this->OutgoingTransfers->options('state'));
    }

    /**
     * Liste les transferts sortant acceptés
     */
    public function indexAccepted()
    {
        $query = $this->Seal->archivalAgency()
            ->table('OutgoingTransfers')
            ->find('seal')
            ->where(
                [
                    'OutgoingTransfers.state' => OutgoingTransfersTable::S_ACCEPTED,
                ]
            );
        $this->indexCommons($query);
    }

    /**
     * Liste les transferts sortant acceptés
     */
    public function indexRejected()
    {
        $query = $this->Seal->archivalAgency()
            ->table('OutgoingTransfers')
            ->find('seal')
            ->where(
                [
                    'OutgoingTransfers.state' => OutgoingTransfersTable::S_REJECTED,
                ]
            );
        $this->indexCommons($query);
    }

    /**
     * Liste tous les transferts sortant
     */
    public function indexAll()
    {
        $query = $this->Seal->archivalAgency()
            ->table('OutgoingTransfers')
            ->find('seal');
        $this->indexCommons($query);
    }

    /**
     * Visualisation d'un transfer sortant
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $ArchiveUnits->find();
        $subquery = $query->select(['count' => $query->func()->count('*')])
            ->from(['au' => 'archive_units'])
            ->where(
                [
                    'au.lft >=' => new IdentifierExpression('ArchiveUnits.lft'),
                    'au.rght <=' => new IdentifierExpression(
                        'ArchiveUnits.rght'
                    ),
                ]
            );
        $outgoingTransfers = $this->Seal->archivalAgency()
            ->table('OutgoingTransfers')
            ->find('seal')
            ->select(['au_count' => $subquery])
            ->where(['OutgoingTransfers.id' => $id])
            ->contain(
                [
                    'ArchiveUnits',
                    'OutgoingTransferRequests' => ['ArchivingSystems'],
                ]
            )
            ->enableAutoFields()
            ->firstOrFail();

        $this->set('id', $id);
        $this->set('outgoingTransfers', $outgoingTransfers);
    }

    /**
     * Suppression d'un transfert sortant
     * @param string $id
     * @return CakeResponse
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Seal->archivalAgency()
            ->table('OutgoingTransfers')
            ->find('seal')
            ->where(['OutgoingTransfers.id' => $id])
            ->contain(['OutgoingTransferRequests'])
            ->firstOrFail();

        $report = $this->OutgoingTransfers->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Donne le zip d'un transfert sortant
     * @param string $id
     * @return Response
     */
    protected function zip(string $id)
    {
        $request = $this->getRequest();
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $this->fetchTable('AuthUrls');
        if (!$AuthUrls->isAuthorized($request)) {
            throw new UnauthorizedException();
        }
        $AuthUrls->consume($request);
        $ot = $this->OutgoingTransfers->get($id);
        $zip = $ot->get('zip');

        return $this->getCoreResponse()
            ->withFileStream($zip, ['mime' => 'application/zip']);
    }

    /**
     * Supprime le zip d'un transfert sortant après son téléchargement
     * @param string $id
     * @return Response
     */
    protected function deleteZip(string $id)
    {
        $request = $this->getRequest();
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $this->fetchTable('AuthUrls');
        if (!$AuthUrls->isAuthorized($request)) {
            throw new UnauthorizedException();
        }
        $AuthUrls->consume($request);
        $ot = $this->OutgoingTransfers->get($id);
        $zip = $ot->get('zip');
        if (is_file($zip)) {
            unlink($zip);
        }
        return $this->renderDataToJson("done");
    }
}
