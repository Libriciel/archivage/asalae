<?php

/**
 * Asalae\Controller\LdapsController
 * @noinspection PhpInternalEntityUsedInspection
 */

namespace Asalae\Controller;

use Adldap\Adldap;
use Adldap\Auth\BindException;
use Adldap\Auth\Guard;
use Adldap\Auth\PasswordRequiredException;
use Adldap\Auth\UsernameRequiredException;
use Adldap\Connections\Provider;
use Adldap\Models\User;
use Asalae\Controller\Component\LoginComponent;
use Asalae\Model\Entity\Ldap;
use Asalae\Model\Table\LdapsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\UsersTable;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\FilterComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Error\Debug\TextFormatter;
use Cake\Error\Debugger;
use Cake\Http\Response;
use Cake\Http\Response as CakeResponse;
use Cake\Log\Log;
use Cake\Utility\Hash;
use ErrorException;
use Exception;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * Ldaps
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property LdapsTable       Ldaps
 * @property OrgEntitiesTable OrgEntities
 * @property UsersTable       Users
 */
class LdapsController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX_LDAP_USERS = 'ldaps-import-users-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'Ldaps.name' => 'asc',
        ],
    ];

    /**
     * Liste les enregistrements
     */
    public function index()
    {
        /** @use IndexComponent */
        $this->loadComponent('AsalaeCore.Index');

        $query = $this->Seal->archivalAgency()
            ->table('Ldaps')
            ->find('seal');

        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        /** @use FilterComponent */
        $this->loadComponent('AsalaeCore.Filter');
        $this->Filter->loadAndSave();

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);
    }

    /**
     * Action d'ajout
     */
    public function add()
    {
        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $entity = $Ldaps->newEntity([], ['validate' => false]);

        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $data['org_entity_id'] = $this->archivalAgencyId;
            $Ldaps->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($Ldaps->save($entity)) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
    }

    /**
     * Action modifier
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        $entity = $this->Seal->archivalAgency()
            ->table('Ldaps')
            ->find('seal')
            ->where(['Ldaps.id' => $id])
            ->firstOrFail();
        $request = $this->getRequest();
        if ($request->is('put')) {
            $data = $this->getRequest()->getData();
            $data['org_entity_id'] = $this->archivalAgencyId;
            $opts = [];
            foreach (
                $request->getData('custom_options', []) as $option => $val
            ) {
                if (defined('LDAP_OPT_' . $option)) {
                    $opts[constant('LDAP_OPT_' . $option)]
                        = in_array($option, Ldap::LDAP_OPTS_INT) ? (int)$val
                        : $val;
                }
            }
            $data['custom_options'] = $opts;
            /** @var LdapsTable $Ldaps */
            $Ldaps = $this->fetchTable('Ldaps');
            $Ldaps->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($Ldaps->save($entity)) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);
    }

    /**
     * Action supprimer
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Seal->archivalAgency()
            ->table('Ldaps')
            ->find('seal')
            ->where(['Ldaps.id' => $id])
            ->firstOrFail();

        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $report = $Ldaps->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Vérifi la connexion au serveur ldap (host:port uniquement)
     */
    public function ping()
    {
        /** @var Ldap $ldap */
        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $ldap = $Ldaps->newEntity($this->getRequest()->getData());
        try {
            $ldap->adldap()->connect();
            return $this->renderDataToJson('pong');
        } catch (Throwable $e) {
            return $this->renderDataToJson(
                $e->getMessage(),
                $this->getResponse()->withStatus(404)
            );
        }
    }

    /**
     * Donne une entrée LDAP
     */
    public function getRandomEntry()
    {
        $request = $this->getRequest();
        $opts = [];
        foreach ($request->getData('custom_options', []) as $option => $val) {
            if (defined('LDAP_OPT_' . $option)) {
                $opts[constant('LDAP_OPT_' . $option)]
                    = in_array($option, Ldap::LDAP_OPTS_INT) ? (int)$val : $val;
            }
        }
        $config = [
            'hosts' => [$request->getData('host')],
            'port' => (int)$request->getData('port'),
            'base_dn' => $request->getData('ldap_root_search'),
            'use_ssl' => (bool)$request->getData('use_ssl'),
            'use_tls' => (bool)$request->getData('use_tls'),
            'username' => $request->getData('user_query_login'),
            'password' => $request->getData('user_query_password'),
            'account_prefix' => $request->getData('account_prefix'),
            'account_suffix' => $request->getData('account_suffix'),
            'schema' => $request->getData('schema'),
            'follow_referrals' => (bool)$request->getData('follow_referrals'),
            'version' => $request->getData('version'),
            'timeout' => $request->getData('timeout'),
            'custom_options' => $opts,
        ];
        /** @var Adldap $ad */
        $ad = Utility::get(Adldap::class);
        $ad->addProvider($config);
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        try {
            $provider = $ad->connect();
            $search = $provider->search();

            $paginator = $search->paginate(100);
            /** @var User $entry */
            $iterator = $paginator->getIterator();
            $arr = iterator_to_array($iterator, false);
            $entry = $arr[array_rand($arr)];
        } catch (Throwable $e) {
            restore_error_handler();
            return $this->renderDataToJson(
                $e->getMessage(),
                $this->getResponse()->withStatus(404)
            );
        }
        Debugger::getInstance()->setConfig(
            'exportFormatter',
            TextFormatter::class
        );
        $attrs = [];
        foreach ($entry->getAttributes() as $key => $values) {
            if (
                is_string($values)
                || !is_string($values[0])
                || !json_encode($values[0])
                || strpos($key, 'password') !== false
            ) {
                continue;
            }
            $attrs[$key] = implode(', ', $values);
        }
        restore_error_handler();
        return $this->getResponse()
            ->withType('text')
            ->withStringBody(Debugger::exportVar($attrs));
    }

    /**
     * Donne le nombre de résultats correspondant au filtre
     * @return Response
     * @throws Exception
     */
    public function getCount()
    {
        $request = $this->getRequest();
        $opts = [];
        foreach ($request->getData('custom_options', []) as $option => $val) {
            if (defined('LDAP_OPT_' . $option)) {
                $opts[constant('LDAP_OPT_' . $option)]
                    = in_array($option, Ldap::LDAP_OPTS_INT) ? (int)$val : $val;
            }
        }
        $config = [
            'hosts' => [$request->getData('host')],
            'port' => (int)$request->getData('port'),
            'base_dn' => $request->getData('ldap_root_search'),
            'use_ssl' => (bool)$request->getData('use_ssl'),
            'use_tls' => (bool)$request->getData('use_tls'),
            'username' => $request->getData('user_query_login'),
            'password' => $request->getData('user_query_password'),
            'account_prefix' => $request->getData('account_prefix'),
            'account_suffix' => $request->getData('account_suffix'),
            'schema' => $request->getData('schema'),
            'follow_referrals' => (bool)$request->getData('follow_referrals'),
            'version' => $request->getData('version'),
            'timeout' => $request->getData('timeout'),
            'custom_options' => $opts,
        ];
        /** @var Adldap $ad */
        $ad = Utility::get(Adldap::class);
        $ad->addProvider($config);
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        try {
            $provider = $ad->connect();
            $search = $provider->search()->newQuery();

            $filter = $request->getData('ldap_users_filter');
            $search->rawFilter($filter);
            $paginator = $search->paginate(100);
            $count = $paginator->count();
        } catch (Throwable $e) {
            restore_error_handler();
            return $this->renderDataToJson(
                $e->getMessage(),
                $this->getResponse()->withStatus(404)
            );
        }
        restore_error_handler();
        return $this->renderDataToJson($count);
    }

    /**
     * Donne la liste des utilisateurs d'un ldap
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function importUsers(string $id)
    {
        /** @use IndexComponent */
        $this->loadComponent('AsalaeCore.Index');
        $entity = $this->Seal->archivalAgency()
            ->table('Ldaps')
            ->find('seal')
            ->where(['Ldaps.id' => $id])
            ->firstOrFail();
        $this->set('entity', $entity);
        $this->set('id', $id);
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');

        if ($this->getRequest()->is('post')) {
            return $this->postImportUsers($id);
        }

        $data = $this->paginateLdap($entity);
        $this->set('data', $data);

        if (strpos($this->getRequest()->getHeaderLine('Accept'), 'json') !== false) {
            return $this->renderDataToJson($data);
        }

        $this->set('user', $Users->newEntity([], ['validate' => false]));

        // options
        $sa = $this->archivalAgency;
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $entities = $OrgEntities->find(
            'list',
            valueField: function (EntityInterface $e) {
                $parent = Hash::get($e, 'parent_org_entity.name');
                return $parent
                    ? sprintf('%s (%s)', $e->get('name'), $parent)
                    : $e->get('name');
            },
            groupField: function (EntityInterface $entity) {
                $name = __("Service d'archive");
                if (Hash::get($entity, 'type_entity.code') === 'SO') {
                    $name = $entity->get('identifier') . ' - ' . $entity->get('name');
                } elseif (isset($entity->get('ParentSO')['name'])) {
                    $name = $entity->get('ParentSO')['identifier'] . ' - ' . $entity->get('ParentSO')['name'];
                }
                return $name;
            },
        )
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.identifier',
                    'OrgEntities.name',
                    'ParentSO.identifier',
                    'ParentSO.name',
                    'TypeEntities.code',
                ]
            )
            ->contain(['TypeEntities'])
            ->leftJoin(
                ['ParentSO' => 'org_entities'],
                ['ParentSO.id' => $OrgEntities->parentSoSubquery()]
            )
            ->where(
                [
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                ]
            )
            ->orderBy(
                [
                    new QueryExpression('ParentSO.name IS NULL DESC'),
                    'OrgEntities.lft' => 'asc',
                ]
            )
            ->contain(['ParentOrgEntities'])
            ->all();
        $this->set('entities', $entities);
    }

    /**
     * @param string $id
     * @return \AsalaeCore\Http\Response|CakeResponse
     * @throws Exception
     */
    protected function postImportUsers(string $id)
    {
        $org_entity_id = $this->getRequest()->getData('form.entity');
        $role_id = $this->getRequest()->getData('form.role');
        $validator = $this->getRequest()->getData('form.is_validator');
        $cert = $this->getRequest()->getData('form.use_cert');
        $success = true;
        $selections = [];
        $usernames = [];
        foreach ($this->getRequest()->getData('selected') as $selected) {
            $usernames[] = $selected['conf-username'];
        }

        // Vérifie l'existance d'un utilisateur ldap dans les utilisateurs asalae
        if (
            ($r = $this->checkExistsInAnotherArchivalAgency($usernames))
            || ($r = $this->checkExistsInAnotherLdap($id, $usernames))
            || ($r = $this->checkExists($usernames))
        ) {
            return $r;
        }
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $conn = $Users->getConnection();
        $conn->begin();
        foreach ($this->getRequest()->getData('selected') as $selected) {
            // si l'utilisateur existe et qu'on a accepté de le lier au LDAP
            if ($this->getRequest()->getData('bind_users_to_ldap')) {
                $user = $Users->find()
                    ->where(['username' => $selected['conf-username']])
                    ->first();
                if ($user) {
                    $user->set(
                        [
                            'ldap_id' => $id,
                            'ldap_login' => $selected['conf-login'],
                        ]
                    );
                    if (!$Users->save($user)) {
                        $success = false;
                        FormatError::logEntityErrors($user);
                        break;
                    }
                    $selected['user'] = $user->toArray();
                    $selections[] = $selected;
                    continue;
                }
            }
            $password = $Users->generatePassword();
            $user = $Users->newEntity(
                [
                    'username' => $selected['conf-username'],
                    'ldap_login' => $selected['conf-login'],
                    'name' => $selected['conf-name'] ?: null,
                    'email' => $selected['conf-email'] ?: null,
                    'password' => $password,
                    'confirm-password' => $password,
                    'high_contrast' => false,
                    'active' => true,
                    'role_id' => $role_id,
                    'org_entity_id' => $org_entity_id,
                    'ldap_id' => $id,
                    'is_validator' => $validator ?? true,
                    'use_cert' => $cert ?? false,
                ]
            );
            if ($user->getError('email')) {
                $user->set('email');
            }
            if (!$Users->save($user)) {
                $success = false;
                FormatError::logEntityErrors($user);
                break;
            }
            $selected['user'] = $user->toArray();
            $selections[] = $selected;
        }
        if ($success) {
            $conn->commit();
            $this->Modal->success();
            return $this->renderDataToJson($selections);
        } else {
            $conn->rollback();
            $this->Modal->fail();
            return $this->renderDataToJson('save failed');
        }
    }

    /**
     * Vérifi si un utilisateur LDAP choisi existe comme utilisateur d'un autre
     * service d'archives
     * @param array $usernames
     * @return Response
     * @throws Exception
     */
    private function checkExistsInAnotherArchivalAgency(array $usernames)
    {
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $existInAnotherAA = $Users->find()
            ->select(['username', 'org_entity_name' => 'OrgEntities.name'])
            ->innerJoinWith('OrgEntities')
            ->innerJoinWith('OrgEntities.ArchivalAgencies')
            ->where(
                [
                    'ArchivalAgencies.id !=' => $this->archivalAgencyId,
                    'Users.username IN' => $usernames,
                ]
            )
            ->limit(10)
            ->disableHydration()
            ->toArray();
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        if ($existInAnotherAA) {
            $this->Modal->fail();
            return $this->renderDataToJson(
                [
                    'error' => __dn(
                        'ldap',
                        "USER_CONFLICT_DIFFERENT_ARCHIVAL_AGENCY_SINGULAR",
                        "USER_CONFLICT_DIFFERENT_ARCHIVAL_AGENCY_PLURAL",
                        count($existInAnotherAA),
                        $this->printNames($existInAnotherAA, 'org_entity_name')
                    ),
                    'error_type' => 'USER_CONFLICT_DIFFERENT_ARCHIVAL_AGENCY',
                ]
            );
        }
    }

    /**
     * Affiche les noms sous la forme: "- username (entité lié)\n"
     * @param array  $data
     * @param string $path
     * @return string
     */
    private function printNames(array $data, string $path): string
    {
        $names = [];
        foreach ($data as $user) {
            $names[] = sprintf(
                '%s (%s)',
                $user['username'],
                Hash::get($user, $path)
            );
        }
        return count($names) > 1 ? implode("\n - ", $names) : $names[0];
    }

    /**
     * Vérifi si un utilisateur LDAP choisi existe comme utilisateur d'un autre
     * LDAP
     * @param int   $id
     * @param array $usernames
     * @return Response
     * @throws Exception
     */
    private function checkExistsInAnotherLdap(int $id, array $usernames)
    {
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $existInAnotherLdap = $Users->find()
            ->select(['username', 'ldap_name' => 'Ldaps.name'])
            ->innerJoinWith('Ldaps')
            ->where(
                [
                    'Ldaps.id !=' => $id,
                    'username IN' => $usernames,
                ]
            )
            ->limit(10)
            ->disableHydration()
            ->toArray();
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        if ($existInAnotherLdap) {
            $this->Modal->fail();
            return $this->renderDataToJson(
                [
                    'error' => __dn(
                        'ldap',
                        "USER_CONFLICT_DIFFERENT_LDAP_SINGULAR",
                        "USER_CONFLICT_DIFFERENT_LDAP_PLURAL",
                        count($existInAnotherLdap),
                        $this->printNames($existInAnotherLdap, 'ldap_name')
                    ),
                    'error_type' => 'USER_CONFLICT_DIFFERENT_LDAP',
                ]
            );
        }
    }

    /**
     * Vérifi si un utilisateur LDAP choisi existe comme utilisateur
     * @param array $usernames
     * @return Response
     */
    private function checkExists(array $usernames)
    {
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $exists = $Users->find()
            ->where(['username IN' => $usernames])
            ->limit(10)
            ->toArray();
        if ($exists && !$this->getRequest()->getData('bind_users_to_ldap')) {
            $this->Modal->fail();
            return $this->renderDataToJson(
                [
                    'error' => __dn(
                        'ldap',
                        "USER_CONFLICT_SAME_ARCHIVAL_AGENCY_SINGULAR",
                        "USER_CONFLICT_SAME_ARCHIVAL_AGENCY_PLURAL",
                        count($exists),
                        $this->printNames($exists, 'name')
                    ),
                    'error_type' => 'USER_CONFLICT_SAME_ARCHIVAL_AGENCY',
                ]
            );
        }
    }

    /**
     * Pagination ldap
     * @param Ldap $ldap
     * @return array
     * @throws Exception
     */
    private function paginateLdap(Ldap $ldap): array
    {
        $this->set('paginateLimit', $this->paginate['limit']);
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        /** @use LoginComponent */
        $this->loadComponent('Login');
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent =  $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var Adldap $ad */
        $ad = Utility::get(Adldap::class);
        $ad->addProvider($ldap->getLdapConfig());
        $data = [];
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        try {
            $provider = $ad->connect();
            $search = $provider->search()->newQuery();
            $search->sortBy($ldap->get('user_login_attribute'));

            $filter = $ldap->get('ldap_users_filter');
            $search->rawFilter($filter);

            foreach ($this->getRequest()->getQueryParams() as $field => $values) {
                if (!is_array($values)) {
                    continue;
                }
                foreach ($values as $value) {
                    $search->where($field, 'contains', $value);
                }
            }

            $paginator = $search->paginate(
                $this->paginate['limit'],
                $this->getRequest()->getQuery('page', 1) - 1
            );

            $archivalAgencyId = $this->archivalAgencyId ?? 0;
            /** @var User $entry */
            foreach ($paginator as $entry) {
                $login = $entry->getAttribute(
                    $ldap->get('user_login_attribute')
                );
                $username = $entry->getAttribute(
                    $ldap->get('user_username_attribute')
                );
                $name = $entry->getAttribute($ldap->get('user_name_attribute'));
                $mail = $entry->getAttribute($ldap->get('user_mail_attribute'));
                $attrs = array_filter($entry->getAttributes(), 'is_string');
                $d = [
                    'conf-login' => $login ? current($login) : 'null',
                    'conf-username' => $username ? current($username) : 'null',
                    'conf-name' => $name ? current($name) : null,
                    'conf-email' => $mail ? current($mail) : null,
                ];
                foreach ($attrs as $attr) {
                    $value = $entry->getAttribute($attr);
                    $value = json_encode($value) ? $value : null; //filtre les binaires
                    if ($value) {
                        $d[$attr] = current($value);
                    } else {
                        unset($d[$attr]);
                    }
                }
                $d['user'] = (isset($d['conf-username']) && $d['conf-username'] !== 'null')
                    ? $Users->find()
                        ->where(
                            [
                                'ldap_id' => $ldap->get('id'),
                                'username' => $d['conf-username'],
                            ]
                        )
                        ->contain(
                            [
                                'OrgEntities' => 'ArchivalAgencies',
                                'Roles',
                            ]
                        )
                        ->disableHydration()
                        ->first()
                    : null;

                if ($d['user']) {
                    $d['user']['same_archival_agency']
                        = Hash::get($d['user'], 'org_entity.archival_agency.id') === $archivalAgencyId;
                    $d['role'] = Hash::get($d['user'], 'role.name');
                    $d['org_entity'] = Hash::get($d['user'], 'org_entity.name');
                }
                $d['hash'] = hash('sha256', json_encode($d));
                $data[] = $d;
            }
            $this->set('countResults', $paginator->count());
            $AjaxPaginatorComponent->setViewPaginator($paginator->getResults(), $paginator->count());
        } catch (Throwable $e) {
            Log::error((string)$e);
            $this->set('countResults', 0);
            $this->Flash->error(__("Erreur LDAP"));
            $AjaxPaginatorComponent->setViewPaginator([], 0);
        }
        restore_error_handler();
        return $data;
    }

    /**
     * Donne le nombre de résultats correspondant au filtre
     * @return Response
     * @throws ErrorException
     */
    public function getFiltered()
    {
        $request = $this->getRequest();
        $opts = [];
        foreach ($request->getData('custom_options', []) as $option => $val) {
            if (defined('LDAP_OPT_' . $option)) {
                $opts[constant('LDAP_OPT_' . $option)]
                    = in_array($option, Ldap::LDAP_OPTS_INT) ? (int)$val : $val;
            }
        }
        $config = [
            'hosts' => [$request->getData('host')],
            'port' => (int)$request->getData('port'),
            'base_dn' => $request->getData('ldap_root_search'),
            'use_ssl' => (bool)$request->getData('use_ssl'),
            'use_tls' => (bool)$request->getData('use_tls'),
            'username' => $request->getData('user_query_login'),
            'password' => $request->getData('user_query_password'),
            'account_prefix' => $request->getData('account_prefix'),
            'account_suffix' => $request->getData('account_suffix'),
            'schema' => $request->getData('schema'),
            'follow_referrals' => (bool)$request->getData('follow_referrals'),
            'version' => $request->getData('version'),
            'timeout' => $request->getData('timeout'),
            'custom_options' => $opts,
        ];

        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        try {
            /** @var Adldap $ad */
            $ad = Utility::get(Adldap::class);
            $ad->addProvider($config);
            $provider = @$ad->connect();
            $search = $provider->search()->newQuery();

            $filter = $request->getData('ldap_users_filter');
            $search->rawFilter($filter);

            $paginator = $search->paginate(100);
            /** @var User $entry */
            $iterator = $paginator->getIterator();
            $count = $iterator->count();
            $iterator->seek(rand(0, $count - 1)); // NOSONAR
            $entry = $iterator->current();

            $login = $entry->getAttribute(
                $request->getData('user_login_attribute')
            );
            $username = $entry->getAttribute(
                $request->getData('user_username_attribute')
            );
            $name = $entry->getAttribute(
                $request->getData('user_name_attribute')
            );
            $mail = $entry->getAttribute(
                $request->getData('user_mail_attribute')
            );
            $data = [
                'login' => $login ? current($login) : null,
                'username' => $username ? current($username) : null,
                'name' => $name ? current($name) : null,
                'email' => $mail ? current($mail) : null,
            ];
        } catch (Throwable $e) {
            restore_error_handler();
            return $this->renderDataToJson(
                $e->getMessage(),
                $this->getResponse()->withStatus(404)
            );
        }
        Debugger::getInstance()->setConfig(
            'exportFormatter',
            TextFormatter::class
        );
        restore_error_handler();
        return $this->getResponse()
            ->withType('text')
            ->withStringBody(Debugger::exportVar($data));
    }

    /**
     * Test une connexion ldap avec le prefix et le suffix
     * @return Response
     * @throws Exception
     */
    public function testConnection()
    {
        $request = $this->getRequest();
        $opts = [];
        foreach ($request->getData('custom_options', []) as $option => $val) {
            if (defined('LDAP_OPT_' . $option)) {
                $opts[constant('LDAP_OPT_' . $option)]
                    = in_array($option, Ldap::LDAP_OPTS_INT) ? (int)$val : $val;
            }
        }
        $config = [
            'hosts' => [$request->getData('host')],
            'port' => (int)$request->getData('port'),
            'base_dn' => $request->getData('ldap_root_search'),
            'use_ssl' => (bool)$request->getData('use_ssl'),
            'use_tls' => (bool)$request->getData('use_tls'),
            'username' => $request->getData('user_query_login'),
            'password' => $request->getData('user_query_password'),
            'account_prefix' => $request->getData('account_prefix'),
            'account_suffix' => $request->getData('account_suffix'),
            'schema' => $request->getData('schema'),
            'follow_referrals' => (bool)$request->getData('follow_referrals'),
            'version' => $request->getData('version'),
            'timeout' => $request->getData('timeout'),
            'custom_options' => $opts,
        ];
        $username = $request->getData('test.username');
        $password = $request->getData('test.password');
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        try {
            /** @var Adldap $ad */
            $ad = Utility::get(Adldap::class);
            $ad->addProvider($config);
            /** @var Provider $provider */
            $provider = $ad->connect();
            /** @var Guard $auth */
            $auth = $provider->auth();
            $passwordOk = $auth->attempt($username, $password, true);
            if ($passwordOk) {
                restore_error_handler();
                return $this->getResponse()
                    ->withType('text')
                    ->withStringBody(
                        __(
                            "Connexion au LDAP avec l'utilisateur ''{0}'' réussie",
                            $username
                        )
                    );
            }
        } catch (BindException | UsernameRequiredException | PasswordRequiredException) {
        }
        restore_error_handler();
        return $this->getResponse()
            ->withType('text')
            ->withStringBody(__("Connexion refusée"));
    }

    /**
     * Liste les entrées d'un ldap
     * @param string $ldap_id
     * @return \AsalaeCore\Http\Response|MessageInterface|ResponseInterface|Response
     * @throws Exception
     */
    public function listLdapEntries(string $ldap_id)
    {
        /** @use IndexComponent */
        $this->loadComponent('AsalaeCore.Index');
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $this->paginate['limit'] = $AjaxPaginatorComponent->getConfig('limit');

        $entity = $this->Seal->archivalAgencyChilds()
            ->table('Ldaps')
            ->find('seal')
            ->where(['Ldaps.id' => $ldap_id])
            ->firstOrFail();
        $this->set('entity', $entity);
        $this->set('id', $ldap_id);
        $data = $this->paginateLdap($entity);
        $this->set('data', $data);

        // override IndexComponent
        $this->viewBuilder()->setTemplate('listLdapEntries');

        if ($this->getRequest()->accepts('application/json')) {
            if ($this->getRequest()->getHeaderLine('X-Paginator-Count')) {
                $viewCookies = $this->viewBuilder()->getVar('countResults');
                $response = $AjaxPaginatorComponent
                    ->getResponseWithCountHeaders($viewCookies);
            } else {
                $response = $this->getResponse();
            }
            $body = $response->getBody();
            $body->write(json_encode($data));
            $body->rewind();
            return $response
                ->withBody($body)
                ->withType('json');
        }
        $AjaxPaginatorComponent->setViewPaginator($data, $this->immutableViewVars['countResults']);
    }

    /**
     * Liste les infos d'une entrée LDAP
     * @param string $ldap_id
     * @param string $login
     * @throws ErrorException
     */
    public function viewEntry(string $ldap_id, string $login)
    {
        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $query = $Ldaps->find();
        $adminTech = $this->getRequest()->getSession()->read('Admin.tech');
        if (!$adminTech) {
            $sa = $this->archivalAgency;
            $query->innerJoinWith('OrgEntities')
                ->where(
                    [
                        'OrgEntities.lft >=' => $sa->get('lft'),
                        'OrgEntities.rght <=' => $sa->get('rght'),
                    ]
                );
        }
        /** @var Ldap $ldap */
        $ldap = $query->where(['Ldaps.id' => $ldap_id])->firstOrFail();
        $this->set('ldap', $ldap);

        /** @use LoginComponent */
        $this->loadComponent('Login');
        /** @var Adldap $ad */
        $ad = Utility::get(Adldap::class);
        $ad->addProvider($ldap->getLdapConfig());
        $data = [];
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        try {
            $provider = $ad->connect();
            $search = $provider->search()->newQuery();
            $search->sortBy($ldap->get('user_login_attribute'));

            $filter = $ldap->get('ldap_users_filter');
            $search->rawFilter($filter);
            $search->where(
                $ldap->get('user_username_attribute'),
                'contains',
                $login
            );
            /** @var \Adldap\Models\User $ldapUser */
            $ldapUser = $search->firstOrFail();
            foreach ($ldapUser->getAttributes() as $key => $values) {
                if (is_string($key)) {
                    $data[$key] = strpos($key, 'password') === false
                        ? $values
                        : ['********'];
                }
            }
        } catch (Throwable $e) {
            Log::error((string)$e);
            $this->set('countResults', 0);
            $this->Flash->error(__("Erreur LDAP"));
        }
        restore_error_handler();
        $this->set('data', $data);
    }

    /**
     * tente la connexion au ldap avec l'utilisateur choisi
     * @param string $ldap_id
     * @return Response
     * @throws ErrorException
     */
    public function testLogin(string $ldap_id)
    {
        $this->getRequest()->allowMethod('post');

        /** @var Ldap $ldap */
        $ldap = $this->Seal->archivalAgencyChilds()
            ->table('Ldaps')
            ->find('seal')
            ->where(['Ldaps.id' => $ldap_id])
            ->firstOrFail();
        $this->set('ldap', $ldap);

        /** @use LoginComponent */
        $this->loadComponent('Login');
        /** @var Adldap $ad */
        $ad = Utility::get(Adldap::class);
        $ad->addProvider($ldap->getLdapConfig());
        $passwordOk = null;
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        try {
            /** @var Provider $provider */
            $provider = $ad->connect();
            /** @var Guard $auth */
            $auth = $provider->auth();
            $passwordOk = $auth->attempt(
                $this->getRequest()->getData('username', ''),
                $this->getRequest()->getData('password', ''),
                true
            );
        } catch (Throwable $e) {
            Log::error((string)$e);
        }
        restore_error_handler();
        return $this->renderDataToJson($passwordOk);
    }

    /**
     * Donne la liste des attributs disponnibles
     * @return Response
     */
    public function listAvailableAttributes()
    {
        /** @var Ldap $ldap */
        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $ldap = $Ldaps->newEntity($this->getRequest()->getData());
        try {
            /** User $entry */
            $entry = $ldap->searchWithFilters()
                ->paginate(1)
                ->getIterator()
                ->current();
            $data = [];
            foreach ($entry->getAttributes() as $key => $values) {
                if (
                    is_string($values)
                    || !is_string($values[0])
                    || !json_encode($values[0])
                    || strpos($key, 'password') !== false
                ) {
                    continue;
                }
                $data[] = $key;
            }
            return $this->renderDataToJson($data);
        } catch (Exception $e) {
            return $this->renderDataToJson(
                $e->getMessage(),
                $this->getResponse()->withStatus(404)
            );
        }
    }
}
