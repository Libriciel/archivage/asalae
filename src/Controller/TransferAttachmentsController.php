<?php

/**
 * Asalae\Controller\TransferAttachmentsController
 */

namespace Asalae\Controller;

use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\TransferAttachmentsTable;
use Asalae\Model\Table\TransfersTable;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Utility\DOMUtility;
use Cake\Core\Configure;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Exception;
use FileValidator\Utility\FileValidator;

/**
 * TransferAttachments
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property TransferAttachmentsTable TransferAttachments
 * @property TransfersTable           Transfers
 */
class TransferAttachmentsController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     * @var array
     */
    public array $paginate = [];

    /**
     * Action supprimer
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Seal->archivalAgency()
            ->table('TransferAttachments')
            ->find('seal')
            ->innerJoinWith('Transfers')
            ->where(['TransferAttachments.id' => $id])
            ->contain(['Transfers'])
            ->firstOrFail();
        /** @var Transfer $transfer */
        $transfer = $entity->get('transfer');

        $serializedMeta = $transfer->getSerializedMeta($this->userId);
        // Si le fichier est mentionné dans le transfert - on se contente de le marquer
        if (
            in_array(
                $entity->get('filename'),
                $transfer->listFiles()
            ) && is_file($serializedMeta)
        ) {
            $meta = unserialize(file_get_contents($serializedMeta));
            if (!isset($meta['toDelete'])) {
                $meta['toDelete'] = [];
            }
            $meta['toDelete'][] = $entity->get('id');
            $report = file_put_contents($serializedMeta, serialize($meta))
                ? 'done'
                : 'Erreur lors de la suppression';
        } else {
            /** @var TransferAttachmentsTable $TransferAttachments */
            $TransferAttachments = $this->fetchTable('TransferAttachments');
            $report = $TransferAttachments->delete($entity)
                ? 'done'
                : 'Erreur lors de la suppression';
            $dir = AbstractGenericMessageForm::getDataDirectory(
                $entity->get('transfer_id')
            );
            $this->removeEmptySubFolders($dir . DS . 'attachments');
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Supprime les dossiers vide
     * @link https://stackoverflow.com/questions/1833518/remove-empty-subfolders-with-php
     * @param string $path
     * @return bool
     */
    private function removeEmptySubFolders($path)
    {
        if (!is_dir($path)) {
            return null;
        }
        $empty = true;
        foreach (glob($path . DS . "{*,.[!.]*,..?*}", GLOB_BRACE) as $file) {
            $empty &= is_dir($file) && $this->removeEmptySubFolders($file);
        }
        return $empty && rmdir($path);
    }

    /**
     * Donne des informations sur le fichier d'un transfert
     * @param int|string $transfer_id
     * @return Response
     * @throws Exception
     */
    public function getFileInfo($transfer_id)
    {
        $this->getRequest()->allowMethod('post');
        $filename = $this->getRequest()->getData('filename');
        $entity = $this->Seal->archivalAgency()
            ->table('TransferAttachments')
            ->find('seal')
            ->where(
                [
                    'TransferAttachments.transfer_id' => $transfer_id,
                    'TransferAttachments.filename' => $filename,
                ]
            )
            ->firstOrFail();
        $file = $entity->get('path');
        $format = null;
        foreach (FileValidator::getPuid($file) as $puid) {
            if ($puid['id'] === 'UNKNOWN') {
                continue;
            }
            $format = $format ?: $puid['id'];
            if (isset($puid['mime'])) {
                break;
            }
        }
        $output = [
            'format' => $format,
            'mime' => mime_content_type($file),
        ];
        return $this->renderDataToJson($output);
    }

    /**
     * permet de télécharger un fichier de pièce jointe
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function download(string $id)
    {
        $file = $this->Seal->archivalAgency()
            ->table('TransferAttachments')
            ->find('seal')
            ->where(['TransferAttachments.id' => $id])
            ->firstOrFail();

        return $this->getCoreResponse()
            ->withFileStream($file->get('path'));
    }

    /**
     * permet de télécharger un fichier de pièce jointe par son transfert et son nom encodé en base 64
     * @param string $transfer_id identifiant du transfert
     * @param string $b64Filename nom du fichier encodé en base 64
     * @return Response
     * @throws Exception
     */
    public function downloadByName(string $transfer_id, string $b64Filename)
    {
        $file = $this->Seal->archivalAgency()
            ->table('TransferAttachments')
            ->find('seal')
            ->where(
                [
                    'TransferAttachments.transfer_id' => $transfer_id,
                    'TransferAttachments.filename' => base64_decode(
                        str_replace(['-', '_'], ['+', '/'], $b64Filename)
                    ),
                ]
            )
            ->firstOrFail();

        return $this->getCoreResponse()
            ->withFileStream($file->get('path'));
    }

    /**
     * select2 ajax pour les transfer_attachements d'un transfert
     * @param string $transfer_id
     * @return Response
     * @throws Exception
     */
    public function populateSelect(string $transfer_id)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->paginate['limit'] = Configure::read('Pagination.limit', 20);

        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        /** @var Transfer $transfer */
        $transfer = $Transfers->get($transfer_id);
        $query = $this->Seal->archivalAgency()
            ->table('TransferAttachments')
            ->find(
                'sealList',
                keyField: 'filename',
                valueField: 'filename',
                groupField: fn(EntityInterface $v) => h(dirname($v->get('filename')))
            )
            ->where(['TransferAttachments.transfer_id' => $transfer_id])
            ->orderBy(['xpath IS NULL' => 'desc'])
            ->limit($this->paginate['limit']);

        if ($this->getRequest()->getQuery('tempfile')) {
            $tempXml = $transfer->getTempXml($this->userId);
            $serializedMeta = $transfer->getSerializedMeta($this->userId);
            $meta = unserialize(file_get_contents($serializedMeta));
            $domutil = DOMUtility::load($tempXml);
            if (!empty($meta['toDelete'])) {
                $query->andWhere(['id NOT IN' => $meta['toDelete']]);
            }
        } else {
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $path = $Transfers->find()
                ->select(['id', 'filename', 'files_deleted'])
                ->where(['id' => $transfer_id])
                ->firstOrFail()
                ->get('xml');
            $domutil = DOMUtility::load($path);
        }

        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        $value = $this->getRequest()->getData('term');
        if ($value) {
            $query->where(
                $this->Condition->ilike(
                    'TransferAttachments.filename',
                    '%' . pg_escape_string($value) . '%'
                )
            );
            $orderName = $this->Condition->ilike(
                'TransferAttachments.filename',
                pg_escape_string($value) . '%'
            );
            // Donne un <ORDER BY (Keywords.code ILIKE 'my_word%') desc>
            // -> Les mots qui commencent par 'my_word' en 1er
            $query->orderBy(
                new QueryExpression(
                    '(' . array_keys($orderName)[0] . ' \'' . current(
                        $orderName
                    ) . '\') desc'
                )
            );
        }
        $query->orderBy(['TransferAttachments.filename' => 'asc']);
        $page = $this->getRequest()->getData('page', 1);
        if ($page > 1) {
            $query->offset($this->paginate['limit'] * ($page - 1));
        }
        $data = $query->all()->toArray();
        $output = ['results' => [], 'pagination' => ['more' => false]];
        if ($query->count() > $this->paginate['limit'] * $page) {
            $output['pagination']['more'] = true;
        }
        foreach ($data as $group => $values) {
            $formatedValues = [];
            foreach ($values as $key => $value) {
                $node = ArchiveBinariesTable::findBinaryByFilename($domutil, $value);
                $formatedValues[] = [
                    'id' => $key,
                    'text' => h($value),
                    'disabled' => (bool)$node,
                ];
            }
            $output['results'][] = [
                'id' => 0,
                'text' => h($group),
                'children' => $formatedValues,
            ];
        }

        return $this->renderDataToJson($output);
    }

    /**
     * Ouvre une visionneuse pour le document en question
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function open(string $id)
    {
        /** @uses ReaderComponent */
        $this->loadComponent('AsalaeCore.Reader');
        $file = $this->Seal->archivalAgency()
            ->table('TransferAttachments')
            ->find('seal')
            ->where(['TransferAttachments.id' => $id])
            ->firstOrFail();
        if (!$this->Reader->canRead($file->get('mime'))) {
            throw new BadRequestException();
        }

        $reader = $this->Reader->get($file->get('mime'));
        $reader->setDownloadUrl(
            '/transfer-attachments/download/' . $file->id . '/' . $file->get('url_basename')
        );
        return $reader->getResponse();
    }
}
