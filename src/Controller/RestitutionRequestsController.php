<?php

/**
 * Asalae\Controller\RestitutionRequestsController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Entity\RestitutionRequest;
use Asalae\Model\Table\ArchivesRestitutionRequestsTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsRestitutionRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationProcessesTable;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\CallbackStream;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use DateInvalidTimeZoneException;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Psr\Http\Message\ResponseInterface;
use ZMQSocketException;

/**
 * restitutions
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveUnitsRestitutionRequestsTable ArchiveUnitsRestitutionRequests
 * @property ArchiveUnitsTable                    ArchiveUnits
 * @property ArchivesRestitutionRequestsTable     ArchivesRestitutionRequests
 * @property ArchivesTable                        Archives
 * @property CountersTable                        Counters
 * @property EventLogsTable                       EventLogs
 * @property OrgEntitiesTable                     OrgEntities
 * @property RestitutionRequestsTable             RestitutionRequests
 * @property ValidationChainsTable                ValidationChains
 * @property ValidationProcessesTable             ValidationProcesses
 */
class RestitutionRequestsController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     * @var array
     */
    public array $paginate = [
        'order' => [
            'RestitutionRequests.id' => 'desc',
        ],
    ];

    /**
     * Liste les demandes de restitution en préparation
     */
    public function indexPreparating()
    {
        $query = $this->Seal->archivalAgency()
            ->table('RestitutionRequests')
            ->find('seal')
            ->where(
                [
                    'RestitutionRequests.created_user_id' => $this->userId,
                    'RestitutionRequests.state' => RestitutionRequestsTable::S_CREATING,
                ]
            );
        $this->indexCommons($query);
    }

    /**
     * Liste les enregistrements
     * @param Query $query
     * @return Response|void
     * @throws Exception
     */
    private function indexCommons(Query $query)
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');

        $query->contain(['ArchiveUnits']);
        $query->where(
            ['RestitutionRequests.archival_agency_id' => $this->archivalAgencyId]
        )
            ->contain(['CreatedUsers' => ['OrgEntities']]);
        /** @var RestitutionRequestsTable $RestitutionRequests */
        $RestitutionRequests = $this->fetchTable('RestitutionRequests');
        $subquery = $RestitutionRequests->selectQuery()
            ->select(['dr.id'])
            ->from(['dr' => 'restitution_requests'])
            ->innerJoin(
                ['lk' => 'archive_units_restitution_requests'],
                [
                    'lk.restitution_request_id' => new IdentifierExpression(
                        'dr.id'
                    ),
                ]
            )
            ->innerJoin(
                ['au' => 'archive_units'],
                ['au.id' => new IdentifierExpression('lk.archive_unit_id')]
            )
            ->where(
                ['dr.id' => new IdentifierExpression('RestitutionRequests.id')]
            )
            ->limit(1);

        $IndexComponent->setQuery($query)
            ->filter(
                'archive_unit_identifier',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike(
                            'au.archival_agency_identifier',
                            $v
                        )
                    );
                    return $q->andWhere(['RestitutionRequests.id IN' => $sq]);
                }
            )
            ->filter(
                'archive_unit_name',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike('au.name', $v)
                    );
                    return $q->andWhere(['RestitutionRequests.id IN' => $sq]);
                }
            )
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter(
                'created_user',
                IndexComponent::FILTER_IN,
                'RestitutionRequests.created_user_id'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('state', IndexComponent::FILTER_IN);

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/commons');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) use ($ArchiveUnits) {
                    // effectue un contain avec limite individuelle d'enregistrements
                    $aus = $ArchiveUnits->find()
                        ->innerJoinWith('ArchiveUnitsRestitutionRequests')
                        ->where(['restitution_request_id' => $e->id])
                        ->orderBy(['archival_agency_identifier'])
                        ->limit(10)
                        ->toArray();
                    $e->set('archive_units', $aus);
                    return $e;
                }
            )
            ->toArray();
        $this->set('data', $data);

        // override $IndexComponent->init()
        $viewBuilder = $this->viewBuilder();
        if (
            $this->getRequest()->is('ajax') && $viewBuilder->getLayout() === null
        ) {
            $viewBuilder->setTemplate('ajax_index');
        }

        // options
        $this->set('states', $RestitutionRequests->options('state'));

        $created_users = $RestitutionRequests->find(
            'list',
            keyField: '_matchingData.CreatedUsers.id',
            valueField: '_matchingData.CreatedUsers.username',
            groupField: '_matchingData.OrgEntities.name',
        )
            ->select(
                ['CreatedUsers.id', 'CreatedUsers.username', 'OrgEntities.name']
            )
            ->innerJoinWith('CreatedUsers')
            ->innerJoinWith('CreatedUsers.OrgEntities')
            ->distinct(
                $distinct = [
                    'OrgEntities.name',
                    'CreatedUsers.username',
                ]
            )
            ->where(
                ['RestitutionRequests.archival_agency_id' => $this->archivalAgencyId]
            )
            ->orderBy($distinct);
        $this->set('created_users', $created_users);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * Liste de mes demandes de restitution
     */
    public function indexMy()
    {
        $query = $this->Seal->archivalAgency()
            ->table('RestitutionRequests')
            ->find('seal')
            ->where(['RestitutionRequests.created_user_id' => $this->userId]);
        $this->indexCommons($query);
    }

    /**
     * Liste de mes demandes de restitution
     */
    public function indexAll()
    {
        $type = Hash::get($this->orgEntity, 'type_entity.code');
        $query = $this->Seal->archivalAgency()
            ->table('RestitutionRequests')
            ->find('seal');
        if ($type === 'CST') {
            $query->where(
                ['RestitutionRequests.control_authority_id' => $this->orgEntityId]
            );
        } elseif ($type !== 'SA' && $type !== 'SE') {
            $query->where(
                ['RestitutionRequests.originating_agency_id' => $this->orgEntityId]
            );
        }
        $this->indexCommons($query);
    }

    /**
     * Action d'ajout
     */
    public function add()
    {
        $request = $this->getRequest();
        /** @var ArchiveUnitsRestitutionRequestsTable $ArchiveUnitsRestitutionRequests */
        $ArchiveUnitsRestitutionRequests = $this->fetchTable('ArchiveUnitsRestitutionRequests');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        // on créé l'entrée dans le get avec les paramètres des recherche des archive_units dans le query
        if ($request->is('get')) {
            /** @var CountersTable $Counters */
            $Counters = $this->fetchTable('Counters');
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = $this->fetchTable('OrgEntities');
            $control = $OrgEntities->cst($this->archivalAgencyId);

            $units = [];
            $query = $ArchiveUnits->findByIndexSealed(
                $this->Seal->hierarchicalView(),
                true
            );
            $query->contain(['ArchiveDescriptions']);
            /** @var IndexComponent $IndexComponent */
            $IndexComponent = $this->loadComponent('AsalaeCore.Index');
            $IndexComponent->setConfig('model', 'ArchiveUnits');
            $IndexComponent->setQuery($query)
                ->filter('agreement_id', null, 'Archives.agreement_id')
                ->filter(
                    'appraisal_rule_end',
                    IndexComponent::FILTER_DATECOMPARENOW,
                    'AppraisalRules.end_date'
                )
                ->filter(
                    'final_action_code',
                    IndexComponent::FILTER_EQUAL,
                    'AppraisalRules.final_action_code'
                )
                ->filter(
                    'archival_agency_identifier',
                    IndexComponent::FILTER_ILIKE
                )
                ->filter(
                    'created',
                    IndexComponent::FILTER_DATEOPERATOR,
                    'Archives.created'
                )
                ->filter(
                    'favoris',
                    function () {
                        $request = $this->getRequest();
                        $cookies = $request->getCookieParams();
                        $sessionCookies = $this->cookies;
                        if (is_array($sessionCookies)) {
                            $cookies = array_merge($cookies, $sessionCookies);
                        }
                        $cookies = array_filter($cookies, fn ($v) => $v !== '0');
                        $favoritesIds = [];
                        $regex = "/^table-favorite-archive-units-returnable-table-(\d+)$/";
                        foreach (array_keys($cookies) as $cookieName) {
                            if (preg_match($regex, $cookieName, $match)) {
                                $favoritesIds[] = $match[1];
                            }
                        }
                        if (empty($favoritesIds)) {
                            return [];
                        }
                        return ['ArchiveUnits.id IN' => $favoritesIds];
                    }
                )
                ->filter('name', IndexComponent::FILTER_ILIKE)
                ->filter(
                    'original_total_count',
                    IndexComponent::FILTER_COUNTOPERATOR
                )
                ->filter(
                    'original_total_size',
                    IndexComponent::FILTER_SIZEOPERATOR
                )
                ->filter(
                    'originating_agency_id',
                    null,
                    'Archives.originating_agency_id'
                )
                ->filter('profile_id', null, 'Archives.profile_id')
                ->filter(
                    'transferring_agency_id',
                    null,
                    'Archives.transferring_agency_id'
                );
            $auCount = 0;
            $originalCount = 0;
            $originalSize = 0;
            $ids = [];
            foreach ($query as $archiveUnit) {
                if (count($units) < 100) {
                    $units[] = $archiveUnit['archival_agency_identifier'] . ' - ' . $archiveUnit['name'];
                }
                $auCount++;
                $originalCount += $archiveUnit['original_total_count'];
                $originalSize += $archiveUnit['original_total_size'];
                $ids[] = $archiveUnit['id'];
            }
            if (count($units) === 0) {
                throw new BadRequestException(
                    __("Aucune unité d'archives supprimables actuellement")
                );
            }
            /** @var RestitutionRequestsTable $RestitutionRequests */
            $RestitutionRequests = $this->fetchTable('RestitutionRequests');
            $entity = $RestitutionRequests->newEntity(
                [
                    'identifier' => $Counters->next(
                        $this->archivalAgencyId,
                        'ArchiveRestitutionRequest'
                    ),
                    'comment' => __n(
                        "Demande de restitution de l'unité d'archive par le {0} :\n- {1}",
                        "Demande de restitution des unités d'archives par le {0} :\n- {1}",
                        count($units),
                        Hash::get($this->orgEntity, 'name'),
                        implode("\n- ", $units)
                    ),
                    'archival_agency_id' => $this->archivalAgencyId,
                    'originating_agency_id' => $request->getQuery(
                        'originating_agency_id.0'
                    ),
                    'control_authority_id' => $control->id,
                    'state' => $RestitutionRequests->initialState,
                    'archive_units_count' => $auCount,
                    'original_count' => $originalCount,
                    'original_size' => $originalSize,
                    'created_user_id' => $this->userId,
                ]
            );
            // note: il peut y avoir +65535 unités d'archives -> impossible d'insérer directement
            $RestitutionRequests->saveOrFail($entity);
            foreach ($ids as $archive_unit_id) {
                $ArchiveUnitsRestitutionRequests->insertQuery()
                    ->insert(['archive_unit_id', 'restitution_request_id'])
                    ->values(
                        [
                            'archive_unit_id' => $archive_unit_id,
                            'restitution_request_id' => $entity->id,
                        ]
                    )
                    ->execute();
            }
        } else {
            $entity = $this->Seal->archivalAgency()
                ->table('RestitutionRequests')
                ->find('seal')
                ->where(
                    ['RestitutionRequests.id' => $request->getData('id')]
                )
                ->contain(
                    [
                        'ArchiveUnits' => function (Query $q) {
                            return $q->limit(10);
                        },
                    ]
                )
                ->firstOrFail();
            $data = [
                'comment' => $request->getData('comment'),
            ];
            /** @var RestitutionRequestsTable $RestitutionRequests */
            $RestitutionRequests = $this->fetchTable('RestitutionRequests');
            $RestitutionRequests->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($RestitutionRequests->save($entity)) {
                $query = $ArchiveUnits->find()
                    ->innerJoinWith('ArchiveUnitsRestitutionRequests')
                    ->where(
                        ['ArchiveUnitsRestitutionRequests.restitution_request_id' => $entity->id]
                    );
                /** @var ArchivesTable $Archives */
                $Archives = $this->fetchTable('Archives');
                /** @var ArchivesRestitutionRequestsTable $ArchivesRestitutionRequests */
                $ArchivesRestitutionRequests = $this->fetchTable('ArchivesRestitutionRequests');
                /** @var EntityInterface $unit */
                foreach ($query as $unit) {
                    $ArchiveUnits->transitionAll(
                        ArchiveUnitsTable::T_REQUEST_RESTITUTION,
                        [
                            'lft >=' => $unit->get('lft'),
                            'rght <=' => $unit->get('rght'),
                        ]
                    );
                    $Archives->transitionAll(
                        ArchivesTable::T_REQUEST_RESTITUTION,
                        ['id' => $unit->get('archive_id')]
                    );
                    $ArchivesRestitutionRequests->insertQuery()
                        ->insert(['archive_id', 'restitution_request_id'])
                        ->values(
                            [
                                'archive_id' => $unit->get('archive_id'),
                                'restitution_request_id' => $entity->id,
                            ]
                        )
                        ->execute();
                }
                $this->Modal->success();
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAdd($entity);
                return $this->renderDataToJson($entity->toArray());
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $links = $ArchiveUnitsRestitutionRequests->find()
            ->where(['restitution_request_id' => $entity->id])
            ->contain(
                [
                    'ArchiveUnits' => [
                        'Archives' => ['DescriptionXmlArchiveFiles'],
                        'ArchiveDescriptions',
                    ],
                ]
            )
            ->orderBy(['ArchiveUnits.archival_agency_identifier']);
        $this->set('unitsCount', $unitsCount = $links->count());
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $entities = $links->limit($AjaxPaginatorComponent->getConfig('limit'))
            ->toArray();
        /** @var EntityInterface[] $archive_units */
        $archive_units = [];
        foreach ($entities as $link) {
            $archiveUnit = $link->get('archive_unit');
            $archiveUnit->set('not_public', true);
            $archiveUnit->setVirtual(['description', 'dates']);
            $archive_units[] = $archiveUnit;
        }
        $this->set('units', $archive_units);
        $this->set('entity', $entity);
        $AjaxPaginatorComponent->setViewPaginator($archive_units, $unitsCount);
    }

    /**
     * Action modifier
     * @param string $id
     * @return Response|null
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var ArchiveUnitsRestitutionRequestsTable $ArchiveUnitsRestitutionRequests */
        $ArchiveUnitsRestitutionRequests = $this->fetchTable('ArchiveUnitsRestitutionRequests');
        $request = $this->getRequest();
        $entity = $this->Seal->archivalAgency()
            ->table('RestitutionRequests')
            ->find('seal')
            ->where(['RestitutionRequests.id' => $id])
            ->contain(
                [
                    'ArchiveUnits' => function (Query $q) {
                        return $q->limit(10);
                    },
                ]
            )
            ->firstOrFail();
        if ($request->is('put')) {
            $data = [
                'comment' => $request->getData('comment'),
            ];
            /** @var RestitutionRequestsTable $RestitutionRequests */
            $RestitutionRequests = $this->fetchTable('RestitutionRequests');
            $RestitutionRequests->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($RestitutionRequests->save($entity)) {
                $this->Modal->success();
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logEdit($entity);
                return $this->renderDataToJson($entity->toArray());
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $links = $ArchiveUnitsRestitutionRequests->find()
            ->where(['restitution_request_id' => $entity->id])
            ->contain(
                [
                    'ArchiveUnits' => [
                        'Archives' => ['DescriptionXmlArchiveFiles'],
                        'ArchiveDescriptions',
                    ],
                ]
            )
            ->orderBy(['ArchiveUnits.archival_agency_identifier']);
        $this->set('unitsCount', $unitsCount = $links->count());
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $entities = $links->limit($AjaxPaginatorComponent->getConfig('limit'))
            ->toArray();
        /** @var EntityInterface[] $archive_units */
        $archive_units = [];
        foreach ($entities as $link) {
            $archiveUnit = $link->get('archive_unit');
            $archiveUnit->set('not_public', true);
            $archiveUnit->setVirtual(['description', 'dates']);
            $archive_units[] = $archiveUnit;
        }
        $this->set('units', $archive_units);
        $this->set('entity', $entity);
        $AjaxPaginatorComponent->setViewPaginator($archive_units, $unitsCount);
    }

    /**
     * Pagination des archive units dans la visualisation
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function paginateArchiveUnits(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $query = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal')
            ->innerJoinWith('ArchiveUnitsRestitutionRequests')
            ->innerJoinWith('Archives')
            ->where(
                ['ArchiveUnitsRestitutionRequests.restitution_request_id' => $id]
            )
            ->contain(
                [
                    'Archives' => ['DescriptionXmlArchiveFiles'],
                    'ArchiveDescriptions',
                ]
            );
        return $AjaxPaginatorComponent->json(
            $query,
            (bool)$this->getRequest()->getQuery('count')
        );
    }

    /**
     * Retire une unité d'archives d'une demande de restitution
     * @param string $id
     * @param string $archive_unit_id
     * @return Response
     */
    public function removeArchiveUnit(string $id, string $archive_unit_id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var ArchiveUnitsRestitutionRequestsTable $ArchiveUnitsRestitutionRequests */
        $ArchiveUnitsRestitutionRequests = $this->fetchTable('ArchiveUnitsRestitutionRequests');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $link = $this->Seal->archivalAgency()
            ->table('ArchiveUnitsRestitutionRequests')
            ->find('seal')
            ->where(
                [
                    'ArchiveUnitsRestitutionRequests.archive_unit_id' => $archive_unit_id,
                    'ArchiveUnitsRestitutionRequests.restitution_request_id' => $id,
                ]
            )
            ->contain(['ArchiveUnits'])
            ->firstOrFail();
        $report = $ArchiveUnitsRestitutionRequests->delete($link)
            ? 'done'
            : 'Erreur lors de la suppression';
        $unit = $link->get('archive_unit');
        $ArchiveUnits->transitionAll(
            ArchiveUnitsTable::T_CANCEL,
            ['lft >=' => $unit->get('lft'), 'rght <=' => $unit->get('rght')]
        );
        $Archives->transitionAll(
            ArchivesTable::T_CANCEL,
            ['id' => $unit->get('archive_id')]
        );

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Action supprimer
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Seal->archivalAgency()
            ->table('RestitutionRequests')
            ->find('seal')
            ->where(['RestitutionRequests.id' => $id])
            ->firstOrFail();
        if (!$entity->get('deletable')) {
            throw new ForbiddenException();
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $ArchiveUnits->find()
            ->innerJoinWith('ArchiveUnitsRestitutionRequests')
            ->where(
                ['ArchiveUnitsRestitutionRequests.restitution_request_id' => $entity->id]
            );
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        /** @var EntityInterface $unit */
        foreach ($query as $unit) {
            $ArchiveUnits->transitionAll(
                ArchiveUnitsTable::T_CANCEL,
                ['lft >=' => $unit->get('lft'), 'rght <=' => $unit->get('rght')]
            );
            $Archives->transitionAll(
                ArchivesTable::T_CANCEL,
                ['id' => $unit->get('archive_id')]
            );
        }

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logDelete($entity);
        /** @var RestitutionRequestsTable $RestitutionRequests */
        $RestitutionRequests = $this->fetchTable('RestitutionRequests');
        $report = $RestitutionRequests->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Envoi de la demande de restitution
     * @param string $id
     * @throws ZMQSocketException
     * @throws DateInvalidTimeZoneException
     */
    public function send(string $id)
    {
        /** @var RestitutionRequest $restitutionRequest */
        $restitutionRequest = $this->Seal->archivalAgency()
            ->table('RestitutionRequests')
            ->find('seal')
            ->where(
                [
                    'RestitutionRequests.id' => $id,
                    'RestitutionRequests.created_user_id' => $this->userId,
                ]
            )
            ->contain(['ArchivalAgencies', 'OriginatingAgencies'])
            ->firstOrFail();
        /** @var RestitutionRequestsTable $RestitutionRequests */
        $RestitutionRequests = $this->fetchTable('RestitutionRequests');
        if (
            !$RestitutionRequests->transition(
                $restitutionRequest,
                RestitutionRequestsTable::T_SEND
            )
        ) {
            throw new ForbiddenException();
        }
        /** @var Notify $Notify */
        $Notify = Utility::get('Notify');

        $request = $this->getRequest();
        $session = $request->getSession();

        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $validationChain = $session->read(
            'ConfigArchivalAgency.restitution-request-chain'
        ) ?: 'default';
        if ($validationChain === 'default') {
            $validationChain = $ValidationChains->find(
                'default',
                app_type: ValidationChainsTable::ARCHIVE_RESTITUTION_REQUEST,
                org_entity_id: $this->archivalAgencyId,
            )->firstOrFail()->get('id');
        }
        $params = [
            'user_id' => $this->userId,
            'org_entity_id' => $this->archivalAgencyId,
            'validation_chain_id' => $validationChain,
        ];
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $process = $ValidationProcesses->createNewProcess(
            $restitutionRequest,
            $params
        );
        $conn = $RestitutionRequests->getConnection();
        $conn->begin();
        $success = $ValidationProcesses->save($process)
            && $RestitutionRequests->save($restitutionRequest);
        $xml = $restitutionRequest->generateXml();
        Filesystem::dumpFile($restitutionRequest->get('xml'), $xml);
        if ($success) {
            $ValidationProcesses->notifyProcess($process);
            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logValidationSend($restitutionRequest);
            $conn->commit();
            $message = __(
                "La demande de restitution {0} a bien été envoyée.",
                $restitutionRequest->get('identifier')
            );
            $notifyClass = 'alert-warning';
        } else {
            $conn->rollback();
            $errors = FormatError::logEntityErrors($restitutionRequest);
            $message = __(
                "Une erreur a eu lieu lors de l'envoi de la demande de restitution {0} : {1}",
                $restitutionRequest->get('identifier'),
                $errors
            );
            $notifyClass = 'alert-danger';
        }
        $Notify->send(
            $this->userId,
            [$session->read('Session.token')],
            $message,
            $notifyClass
        );
        if (!$success) {
            throw new BadRequestException($message);
        }
    }

    /**
     * Visualisation d'une demande de restitution
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $restitutionRequest = $this->Seal->archivalAgency()
            ->table('RestitutionRequests')
            ->find('seal')
            ->where(['RestitutionRequests.id' => $id])
            ->contain(
                [
                    'ArchivalAgencies',
                    'OriginatingAgencies',
                    'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q->orderBy(
                            ['ArchiveUnits.archival_agency_identifier']
                        )
                            ->limit($AjaxPaginatorComponent->getConfig('limit'))
                            ->contain(
                                [
                                    'Archives' => ['DescriptionXmlArchiveFiles'],
                                    'ArchiveDescriptions',
                                ]
                            );
                    },
                    'CreatedUsers',
                    'ValidationProcesses' => [
                        'ValidationChains',
                        'CurrentStages' => [
                            'ValidationActors',
                        ],
                        'ValidationHistories' => [
                            'sort' => [
                                'ValidationHistories.created' => 'asc',
                                'ValidationHistories.id' => 'asc',
                            ],
                            'ValidationActors' => [
                                'Users',
                                'ValidationStages',
                            ],
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        /** @var EntityInterface[] $archive_units */
        $archive_units = Hash::get($restitutionRequest, 'archive_units');
        foreach ($archive_units as $archiveUnit) {
            $archiveUnit->set('not_public', true);
            $archiveUnit->setVirtual(['description', 'dates']);
        }
        $this->set('restitutionRequest', $restitutionRequest);
        /** @var ArchiveUnitsRestitutionRequestsTable $ArchiveUnitsRestitutionRequests */
        $ArchiveUnitsRestitutionRequests = $this->fetchTable('ArchiveUnitsRestitutionRequests');
        $unitsCount = $ArchiveUnitsRestitutionRequests->find()
            ->where(['restitution_request_id' => $id])
            ->count();
        $this->set('unitsCount', $unitsCount);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($restitutionRequest);
        $AjaxPaginatorComponent->setViewPaginator($archive_units, $unitsCount);
    }

    /**
     * Index des restitutions acquittés
     * @throws Exception
     */
    public function indexAcquitted()
    {
        $conditions = [
            'RestitutionRequests.state IN' => [
                RestitutionRequestsTable::S_RESTITUTION_ACQUITTED,
                RestitutionRequestsTable::S_ARCHIVE_DESTRUCTION_SCHEDULED,
            ],
        ];
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (
            $this->orgEntityId != $this->archivalAgencyId
            && !in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)
        ) {
            $conditions['RestitutionRequests.created_user_id'] = $this->userId;
        }
        $query = $this->Seal->archivalAgency()
            ->table('RestitutionRequests')
            ->find('seal')
            ->where($conditions);
        $this->indexCommons($query);
    }

    /**
     * Marque une demande de restitution pour élimination
     * @param string $id
     * @return Response
     * @throws DateInvalidTimeZoneException
     */
    public function scheduleArchiveDestruction(string $id)
    {
        $restitutionRequest = $this->Seal->archivalAgency()
            ->table('RestitutionRequests')
            ->find('seal')
            ->where(['RestitutionRequests.id' => $id])
            ->firstOrFail();
        /** @var RestitutionRequestsTable $RestitutionRequests */
        $RestitutionRequests = $this->fetchTable('RestitutionRequests');
        $RestitutionRequests->transitionOrFail(
            $restitutionRequest,
            RestitutionRequestsTable::T_SCHEDULE_ARCHIVE_DESTRUCTION
        );
        $RestitutionRequests->saveOrFail($restitutionRequest);
        return $this->renderDataToJson($restitutionRequest->toArray());
    }

    /**
     * Annule le marquage d'une demande de restitution pour élimination
     * @param string $id
     * @return Response
     * @throws DateInvalidTimeZoneException
     */
    public function cancelArchiveDestruction(string $id)
    {
        $restitutionRequest = $this->Seal->archivalAgency()
            ->table('RestitutionRequests')
            ->find('seal')
            ->where(['RestitutionRequests.id' => $id])
            ->firstOrFail();
        /** @var RestitutionRequestsTable $RestitutionRequests */
        $RestitutionRequests = $this->fetchTable('RestitutionRequests');
        $RestitutionRequests->transitionOrFail(
            $restitutionRequest,
            RestitutionRequestsTable::T_CANCEL_ARCHIVE_DESTRUCTION
        );
        $RestitutionRequests->saveOrFail($restitutionRequest);
        return $this->renderDataToJson($restitutionRequest->toArray());
    }

    /**
     * Téléchargement de l'attestation d'élimination
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function restitutionCertificate(string $id)
    {
        /** @var RestitutionRequest $restitutionRequest */
        /** @var RestitutionRequestsTable $RestitutionRequests */
        $RestitutionRequests = $this->fetchTable('RestitutionRequests');
        $restitutionRequest = $RestitutionRequests->find()
            ->where(
                [
                    'RestitutionRequests.id' => $id,
                    'RestitutionRequests.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['ArchivalAgencies', 'OriginatingAgencies'])
            ->firstOrFail();
        if ($sessionFile = $this->getRequest()->getQuery('sessionFile')) {
            Notify::emit(
                'download_file',
                [
                    'uuid' => $sessionFile,
                    'state' => 'downloading',
                    'last_update' => time(),
                ]
            );
        }
        $pdf = $restitutionRequest->get('certification');
        $filename = sprintf(
            "%s_restitution_certificate.pdf",
            $restitutionRequest->get('identifier')
        );
        // note met à jour le champ restitution_certified
        $RestitutionRequests->save($restitutionRequest);
        return $this->getResponse()
            ->withHeader('Content-Type', 'application/pdf')
            ->withHeader('Content-Length', $size = strlen($pdf))
            ->withHeader('File-Size', $size)
            ->withHeader(
                'Content-Disposition',
                'attachment; filename="' . $filename . '"'
            )
            ->withBody(
                new CallbackStream(
                    function () use ($pdf, $sessionFile) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        echo $pdf;
                        if ($sessionFile) {
                            Notify::emit(
                                'download_file',
                                [
                                    'uuid' => $sessionFile,
                                    'state' => 'finished',
                                    'last_update' => time(),
                                    'integrity' => true,
                                ]
                            );
                        }
                    }
                )
            );
    }

    /**
     * Téléchargement de l'attestation d'élimination
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function destructionCertificate(string $id)
    {
        /** @var RestitutionRequest $restitutionRequest */
        /** @var RestitutionRequestsTable $RestitutionRequests */
        $RestitutionRequests = $this->fetchTable('RestitutionRequests');
        $restitutionRequest = $RestitutionRequests->find()
            ->where(
                [
                    'RestitutionRequests.id' => $id,
                    'RestitutionRequests.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['ArchivalAgencies', 'OriginatingAgencies'])
            ->firstOrFail();

        if ($sessionFile = $this->getRequest()->getQuery('sessionFile')) {
            Notify::emit(
                'download_file',
                [
                    'uuid' => $sessionFile,
                    'state' => 'downloading',
                    'last_update' => time(),
                ]
            );
        }
        $pdf = $restitutionRequest->get('destruction_certification');
        $filename = sprintf(
            "%s_destruction_certificate.pdf",
            $restitutionRequest->get('identifier')
        );
        // note met à jour le champ restitution_certified
        $RestitutionRequests->save($restitutionRequest);
        return $this->getResponse()
            ->withHeader('Content-Type', 'application/pdf')
            ->withHeader('Content-Length', $size = strlen($pdf))
            ->withHeader('File-Size', $size)
            ->withHeader(
                'Content-Disposition',
                'attachment; filename="' . $filename . '"'
            )
            ->withBody(
                new CallbackStream(
                    function () use ($pdf, $sessionFile) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        echo $pdf;
                        if ($sessionFile) {
                            Notify::emit(
                                'download_file',
                                [
                                    'uuid' => $sessionFile,
                                    'state' => 'finished',
                                    'last_update' => time(),
                                    'integrity' => true,
                                ]
                            );
                        }
                    }
                )
            );
    }
}
