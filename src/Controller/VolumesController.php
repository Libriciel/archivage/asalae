<?php

/**
 * Asalae\Controller\UsersController
 */

namespace Asalae\Controller;

use Asalae\Model\Table\VolumesTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;

/**
 * Volumes
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property VolumesTable Volumes
 */
class VolumesController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
}
