<?php

/**
 * Asalae\Controller\AppraisalRuleCodesController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Table\AppraisalRuleCodesTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use Cake\Http\Response;
use Exception;

/**
 * Liste des codes des durées d'utilité administrative
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AppraisalRuleCodesTable $AppraisalRuleCodes
 */
class AppraisalRuleCodesController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX_GLOBALS = 'appraisal-rule-codes-globals-index-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'AppraisalRuleCodes.id' => 'asc',
        ],
    ];

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default',
        ];
    }

    /**
     * Liste des codes des durées d'utilité administrative
     */
    public function index()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        /** @var AppraisalRuleCodesTable $AppraisalRuleCodes */
        $AppraisalRuleCodes = $this->fetchTable('AppraisalRuleCodes');
        $query = $AppraisalRuleCodes->find()
            ->where(['org_entity_id' => $this->archivalAgencyId]);

        $IndexComponent->setQuery($query)
            ->filter('code', IndexComponent::FILTER_ILIKE)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('duration', IndexComponent::FILTER_ILIKE);

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);

        $dataGlobals = $AppraisalRuleCodes->find()
            ->where(['org_entity_id IS' => null])
            ->orderBy(['id'])
            ->toArray();
        $this->set('dataGlobals', $dataGlobals);
        $this->set('tableIdGlobals', self::TABLE_INDEX_GLOBALS);

        $request = $this->getRequest();
        if ($request->getQuery('export_csv')) {
            return $IndexComponent->renderCsv(array_merge($data, $dataGlobals));
        } else {
            $IndexComponent->exportCsv(); // génère le bouton uniquement
        }

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * ajout d'un code de durée d'utilité administrative
     * @throws Exception
     */
    public function add()
    {
        /** @var AppraisalRuleCodesTable $AppraisalRuleCodes */
        $AppraisalRuleCodes = $this->fetchTable('AppraisalRuleCodes');
        $entity = $AppraisalRuleCodes->newEntity(
            [
                'org_entity_id' => $this->archivalAgencyId,
            ],
            ['validate' => false]
        );
        $this->set('entity', $entity);
        if ($this->getRequest()->is('post')) {
            $data = [
                'org_entity_id' => $this->archivalAgencyId,
                'code' => $this->getRequest()->getData('code'),
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'duration' => $this->getRequest()->getData('duration'),
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($AppraisalRuleCodes, $entity, $data);
            if ($this->Modal->lastSaveIsSuccess()) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAdd($entity);
            }
        }
    }

    /**
     * Edition d'un code de durée d'utilité administrative
     * @param string $id
     * @return void
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var AppraisalRuleCodesTable $AppraisalRuleCodes */
        $AppraisalRuleCodes = $this->fetchTable('AppraisalRuleCodes');
        $entity = $AppraisalRuleCodes->find()
            ->where(
                [
                    'AppraisalRuleCodes.id' => $id,
                    'AppraisalRuleCodes.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);
        if ($this->getRequest()->is('put')) {
            $data = [
                'code' => $this->getRequest()->getData('code'),
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'duration' => $this->getRequest()->getData('duration'),
            ];
            if (!$entity->get('deletable')) {
                unset($data['code'], $data['duration']);
            }

            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($AppraisalRuleCodes, $entity, $data);
            if ($this->Modal->lastSaveIsSuccess()) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logEdit($entity);
            }
        }
    }

    /**
     * Action de suppression d'un code de durée d'utilité administrative
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        /** @var AppraisalRuleCodesTable $AppraisalRuleCodes */
        $AppraisalRuleCodes = $this->fetchTable('AppraisalRuleCodes');
        $entity = $AppraisalRuleCodes->find()
            ->where(
                [
                    'id' => $id,
                    'org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        if ($entity->get('deletable') === true) {
            $conn = $AppraisalRuleCodes->getConnection();
            $conn->begin();
            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logDelete($entity);
            if ($AppraisalRuleCodes->delete($entity)) {
                $report = 'done';
                $conn->commit();
            } else {
                $report = 'Erreur lors de la suppression';
                $conn->rollback();
            }
        } else {
            $report = 'Erreur lors de la suppression';
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Visualisation d'un code de durée d'utilité administrative
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        /** @var AppraisalRuleCodesTable $AppraisalRuleCodes */
        $AppraisalRuleCodes = $this->fetchTable('AppraisalRuleCodes');
        $entity = $AppraisalRuleCodes->find()
            ->where(
                [
                    'OR' => [
                        'AppraisalRuleCodes.org_entity_id' => $this->archivalAgencyId,
                        'AppraisalRuleCodes.org_entity_id IS' => null,
                    ],
                    'AppraisalRuleCodes.id' => $id,
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($entity);
    }

    /**
     * Retourne les options d'un select de AppraisalRuleCodes en SEDA 2.x
     * @throws Exception
     */
    public function populateSelect()
    {
        $this->viewBuilder()->setLayout('ajax');
        /** @var AppraisalRuleCodesTable $AppraisalRuleCodes */
        $AppraisalRuleCodes = $this->fetchTable('AppraisalRuleCodes');

        $appraisalRuleCodes = $AppraisalRuleCodes->getRuleCodes($this->archivalAgencyId, 'seda2.1');

        return $this->renderDataToJson($appraisalRuleCodes);
    }
}
