<?php

/**
 * Asalae\Controller\KeywordListsController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Table\KeywordListsTable;
use Asalae\Model\Table\KeywordsTable;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\DatabaseUtility;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Cake\ORM\Query;
use DateTime;
use Exception;

/**
 * Liste des mots-clés (thésaurus)
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property KeywordListsTable $KeywordLists
 * @property KeywordsTable     $Keywords
 */
class KeywordListsController extends AppController
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'keyword-lists-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'KeywordLists.id' => 'asc',
        ],
    ];

    /**
     * Liste des listes de mots-clés
     */
    public function index()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        /** @var KeywordsTable $Keywords */
        $Keywords = $this->fetchTable('Keywords');
        $countSq = $Keywords->find()
            ->select(['count' => $Keywords->query()->func()->count('*')])
            ->where(
                [
                    'Keywords.keyword_list_id' => new IdentifierExpression(
                        'KeywordLists.id'
                    ),
                    'Keywords.version' => new IdentifierExpression(
                        'KeywordLists.version'
                    ),
                ]
            );

        $query = $this->Seal->archivalAgency()
            ->table('KeywordLists')
            ->find('seal')
            ->select(['count' => $countSq])
            ->enableAutoFields();

        $IndexComponent->setQuery($query)
            ->filter('active', IndexComponent::FILTER_IS)
            ->filter('has_workspace', IndexComponent::FILTER_IS)
            ->filter(
                'count',
                function ($v, Query $q) use ($countSq) {
                    $operator = $v['operator'];
                    if (!in_array($operator, ['=', '<=', '>=', '<', '>'])) {
                        throw new BadRequestException(
                            __("L'opérateur indiqué n'est pas autorisé")
                        );
                    }
                    $sql = $countSq->sql();
                    $q->where(["($sql) $operator" => $v['value']]);
                }
            )
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('modified', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('version', IndexComponent::FILTER_COUNTOPERATOR);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * Ajout d'une liste de mots-clés
     */
    public function add()
    {
        /** @var KeywordListsTable $KeywordLists */
        $KeywordLists = $this->fetchTable('KeywordLists');
        $entity = $KeywordLists->newEntity(
            [
                'org_entity_id' => $this->archivalAgencyId,
                'active' => true,
            ],
            ['validate' => false]
        );
        $this->set('entity', $entity);
        if ($this->getRequest()->is('post')) {
            $data = [
                'identifier' => $this->getRequest()->getData('identifier'),
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'active' => $this->getRequest()->getData('active'),
                'has_workspace' => true,
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($KeywordLists, $entity, $data);
            if ($this->Modal->lastSaveIsSuccess()) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAdd($entity);
            }
        }
    }

    /**
     * Edition d'une liste de mots-clés
     * @param string $id
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var KeywordListsTable $KeywordLists */
        $KeywordLists = $this->fetchTable('KeywordLists');
        $entity = $this->Seal->archivalAgency()
            ->table('KeywordLists')
            ->find('seal')
            ->where(['KeywordLists.id' => $id])
            ->firstOrFail();
        $this->set('entity', $entity);
        if ($this->getRequest()->is('put')) {
            $data = [
                'identifier' => $this->getRequest()->getData('identifier'),
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'active' => $this->getRequest()->getData('active'),
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($KeywordLists, $entity, $data);
            if ($this->Modal->lastSaveIsSuccess()) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logEdit($entity);
            }
        }
    }

    /**
     * Suppression d'une liste de mots-clés
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        /** @var KeywordListsTable $KeywordLists */
        $KeywordLists = $this->fetchTable('KeywordLists');
        $entity = $this->Seal->archivalAgency()
            ->table('KeywordLists')
            ->find('seal')
            ->where(['KeywordLists.id' => $id])
            ->firstOrFail();

        $conn = $KeywordLists->getConnection();
        $conn->begin();
        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logDelete($entity);
        if ($KeywordLists->delete($entity)) {
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Visualisation d'une liste de mots-clés
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $entity = $this->Seal->archivalAgency()
            ->table('KeywordLists')
            ->find('seal')
            ->where(['KeywordLists.id' => $id])
            ->firstOrFail();
        $this->set('entity', $entity);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($entity);
    }

    /**
     * Création de la nouvelle version de travail
     * @param string $id
     * @return Response|null
     * @throws Exception
     */
    public function newVersion(string $id)
    {
        /** @var KeywordListsTable $KeywordLists */
        $KeywordLists = $this->fetchTable('KeywordLists');
        /** @var KeywordsTable $Keywords */
        $Keywords = $this->fetchTable('Keywords');
        $list = $this->Seal->archivalAgency()
            ->table('KeywordLists')
            ->find('seal')
            ->where(['KeywordLists.id' => $id])
            ->firstOrFail();
        $newVersionExists = $list->get('has_workspace');
        if ($newVersionExists) {
            $this->Flash->error(
                __(
                    "Il existe déjà une version de travail pour cette liste de mots-clés"
                )
            );
            return $this->redirect('/Keywords/index/' . $id);
        }
        $conn = $Keywords->getConnection();
        $conn->begin();

        try {
            $query = $Keywords->insertQuery();
            $q = $Keywords->selectQuery();
            $now = (new DateTime())->format(DATE_ATOM);
            $pdo = DatabaseUtility::getPdo();
            $query->insert(['keyword_list_id', 'code', 'name', 'app_meta', 'version', 'created', 'modified'])
                ->into('keywords')
                ->values(
                    $q->from('keywords')
                        ->select(
                            [
                                'keyword_list_id' => $id,
                                'code',
                                'name',
                                'app_meta',
                                'version' => 0,
                                'created' => $pdo->quote($now),
                                'modified' => $pdo->quote($now),
                            ]
                        )
                        ->where(
                            [
                                'keyword_list_id' => $id,
                                'version' => $list->get('version'),
                            ]
                        )
                );
            $query->execute();

            $this->Flash->success(__("Version de travail créée avec succès"));
        } catch (Exception) {
            $conn->rollback();
            $this->Flash->error(__("Echec lors de la création de la version de travail"));
            return $this->redirect('/Keywords/index/' . $id);
        }
        $list->set('has_workspace', true);
        if (!$KeywordLists->save($list)) {
            $conn->rollback();
            $this->Flash->error(
                __("Echec lors de la création de la version de travail")
            );
            return $this->redirect('/Keywords/index/' . $id);
        }
        $conn->commit();
        return $this->redirect('/Keywords/index/' . $id . '/0');
    }

    /**
     * Active la version des mots-clés
     * @param string $id
     * @return Response|null
     * @throws Exception
     */
    public function publishVersion(string $id)
    {
        /** @var KeywordListsTable $KeywordLists */
        $KeywordLists = $this->fetchTable('KeywordLists');
        /** @var KeywordsTable $Keywords */
        $Keywords = $this->fetchTable('Keywords');
        $list = $this->Seal->archivalAgency()
            ->table('KeywordLists')
            ->find('seal')
            ->where(['KeywordLists.id' => $id])
            ->firstOrFail();
        $newVersionExists = $list->get('has_workspace');
        if (!$newVersionExists) {
            $this->Flash->error(
                __("Impossible de publier une liste de mots-clés vide")
            );
            return $this->redirect('/Keywords/index/' . $id);
        }
        $q = $Keywords->query();
        $duplicates = $Keywords->find()
            ->select(['name', 'count' => $q->func()->count('*')])
            ->where(['keyword_list_id' => $id, 'version' => 0])
            ->groupBy(['name'])
            ->having([new QueryExpression('count > 1')])
            ->toArray();
        if ($duplicates) {
            $this->Flash->error(
                __("Impossible de publier une liste de mots-clés avec des doublons")
            );
            return $this->redirect('/Keywords/index/' . $id);
        }

        $newVersion = $list->get('version') + 1;
        $conn = $KeywordLists->getConnection();
        $conn->begin();
        $count = $Keywords->updateAll(
            ['version' => $newVersion],
            [
                'keyword_list_id' => $id,
                'version' => 0,
            ]
        );
        $list->set('version', $newVersion);
        $list->set('has_workspace', false);
        if ($count && $KeywordLists->save($list)) {
            $conn->commit();
        } else {
            $conn->rollback();
            $this->Flash->error(
                __(
                    "Une erreur est survenue lors de la création de la nouvelle version"
                )
            );
        }
        return $this->redirect('/KeywordLists/index');
    }

    /**
     * Supprime la version de travail
     * @param string $id
     * @return Response|null
     * @throws Exception
     */
    public function removeVersion(string $id)
    {
        /** @var KeywordListsTable $KeywordLists */
        $KeywordLists = $this->fetchTable('KeywordLists');
        /** @var KeywordsTable $Keywords */
        $Keywords = $this->fetchTable('Keywords');
        $list = $this->Seal->archivalAgency()
            ->table('KeywordLists')
            ->find('seal')
            ->where(['KeywordLists.id' => $id])
            ->firstOrFail();
        $Keywords->deleteAll(
            [
                'keyword_list_id' => $id,
                'version' => 0,
            ]
        );
        $list->set('has_workspace', false);
        $KeywordLists->saveOrFail($list);
        return $this->redirect('/KeywordLists/index');
    }
}
