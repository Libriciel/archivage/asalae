<?php

/**
 * Asalae\Controller\Component\MercureComponent
 */

namespace Asalae\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Http\Cookie\Cookie;
use Cake\Http\Exception\ForbiddenException;
use DateInterval;
use DateTime;
use Symfony\Component\Mercure\Hub;
use Symfony\Component\Mercure\Jwt\FactoryTokenProvider;
use Symfony\Component\Mercure\Jwt\LcobucciFactory;
use Symfony\Component\Mercure\Update;
use Throwable;

/**
 * Défini le cookie de connexion sur Mercure
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2024 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MercureComponent extends Component
{
    /**
     * Default config
     * These are merged with user-provided config when the component is used.
     * @var array<string, mixed>
     */
    protected array $_defaultConfig = [
        'cookieName' => 'mercureAuthorization',
        'relativeUrl' => '/mercure/.well-known/mercure',
        'sameSite' => 'strict',
        'secure' => true,
        'httpOnly' => true,
        'expireAt' => 10,
        'domain' => null,
        'publish' => [],
    ];

    private Hub $hub;

    /**
     * Constructor
     * @param ComponentRegistry    $registry
     * @param array<string, mixed> $config
     */
    public function __construct(ComponentRegistry $registry, array $config = [])
    {
        $config += Configure::read('Mercure', []);
        parent::__construct($registry, $config);
    }

    /**
     * @return Hub|null
     */
    private function getHub(): ?Hub
    {
        if (empty($this->hub)) {
            $url = $this->getConfig('url');
            $jwtSecret = $this->getConfig('jwt_secret');
            $jwtFactory = new LcobucciFactory($jwtSecret);
            $jwtProvider = new FactoryTokenProvider(
                $jwtFactory,
                subscribe: ['*'],
                publish: $this->getConfig('publish'),
            );
            $this->hub = new Hub(
                $url,
                $jwtProvider,
                $jwtFactory,
                $url,
            );
        }
        return $this->hub;
    }

    /**
     * @return void
     */
    public function beforeRender(): void
    {
        try {
            $url = $this->getConfig('url');
            $jwtSecret = $this->getConfig('jwt_secret');
            if (!$this->getConfig('enabled') || !$url || !$jwtSecret) {
                return;
            }
            $hub = $this->getHub();
            $relativeUrl = $this->getConfig('relativeUrl');

            $expireAt = $this->getConfig('expireAt');
            if ($expireAt) {
                $expireAt = (new DateTime())->add(new DateInterval('PT' . $expireAt . 'S'));
            }
            $cookie = new Cookie(
                $this->getConfig('cookieName'),
                $hub->getProvider()->getJwt(),
                $expireAt,
                $relativeUrl,
                $this->getConfig('domain'),
                $this->getConfig('secure'),
                $this->getConfig('httpOnly'),
                $this->getConfig('sameSite'),
            );
            $response = $this->getController()
                ->getResponse()
                ->withCookie($cookie);
            $this->getController()->setResponse($response);
            $this->getController()->set('mercure_url', $relativeUrl);
        } catch (Throwable $e) {
            trigger_error($e->getMessage());
        }
    }

    /**
     * Permet d'envoyer le message
     * @param string $chanel
     * @param mixed  $message
     * @return void
     */
    public function publish(string $chanel, mixed $message): void
    {
        if (!in_array($chanel, $this->getConfig('publish'))) {
            throw new ForbiddenException(__("Vous n'avez pas accès au chanel {0}", $chanel));
        }
        $this->beforeRender();
        $hub = $this->getHub();
        $update = new Update(
            [$chanel],
            json_encode($message),
            true,
        );
        $hub->publish($update);
    }
}
