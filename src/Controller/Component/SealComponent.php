<?php

/**
 * Asalae\Controller\Component\SealComponent
 */

namespace Asalae\Controller\Component;

use Asalae\Model\Behavior\SealBehavior;
use Asalae\Model\Table\AgreementsTable;
use Asalae\Model\Table\ArchiveBinariesArchiveUnitsTable;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchiveKeywordsTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsBatchTreatmentsTable;
use Asalae\Model\Table\ArchiveUnitsDestructionRequestsTable;
use Asalae\Model\Table\ArchiveUnitsOutgoingTransferRequestsTable;
use Asalae\Model\Table\ArchiveUnitsRestitutionRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\ArchivingSystemsTable;
use Asalae\Model\Table\BatchTreatmentsTable;
use Asalae\Model\Table\BeanstalkJobsTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\DeliveriesTable;
use Asalae\Model\Table\DeliveryRequestsTable;
use Asalae\Model\Table\DestructionNotificationsTable;
use Asalae\Model\Table\DestructionRequestsTable;
use Asalae\Model\Table\EaccpfsTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\KeywordListsTable;
use Asalae\Model\Table\KeywordsTable;
use Asalae\Model\Table\LdapsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\OrgEntitiesTimestampersTable;
use Asalae\Model\Table\OutgoingTransferRequestsTable;
use Asalae\Model\Table\OutgoingTransfersTable;
use Asalae\Model\Table\ProfilesTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Table\RestitutionsTable;
use Asalae\Model\Table\RolesTable;
use Asalae\Model\Table\SecureDataSpacesTable;
use Asalae\Model\Table\SequencesTable;
use Asalae\Model\Table\ServiceLevelsTable;
use Asalae\Model\Table\TechnicalArchivesTable;
use Asalae\Model\Table\TransferAttachmentsTable;
use Asalae\Model\Table\TransferErrorsTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\UsersTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationProcessesTable;
use Asalae\Model\Table\ValidationStagesTable;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Datasource\EntityInterface;
use Cake\Http\ServerRequest;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/**
 * Logiques d'accès à certaines fonctions
 * (hierarchical_view, super archivistes, service d'archives)
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AgreementsTable                           Agreements
 * @property ArchiveBinariesArchiveUnitsTable          ArchiveBinariesArchiveUnits
 * @property ArchiveBinariesTable                      ArchiveBinaries
 * @property ArchiveFilesTable                         ArchiveFiles
 * @property ArchiveKeywordsTable                      ArchiveKeywords
 * @property ArchiveUnitsBatchTreatmentsTable          ArchiveUnitsBatchTreatments
 * @property ArchiveUnitsDestructionRequestsTable      ArchiveUnitsDestructionRequests
 * @property ArchiveUnitsOutgoingTransferRequestsTable ArchiveUnitsOutgoingTransferRequests
 * @property ArchiveUnitsRestitutionRequestsTable      ArchiveUnitsRestitutionRequests
 * @property ArchiveUnitsTable                         ArchiveUnits
 * @property ArchivesTable                             Archives
 * @property ArchivingSystemsTable                     ArchivingSystems
 * @property BatchTreatmentsTable                      BatchTreatments
 * @property BeanstalkJobsTable                        BeanstalkJobs
 * @property CountersTable                             Counters
 * @property DeliveriesTable                           Deliveries
 * @property DeliveryRequestsTable                     DeliveryRequests
 * @property DestructionNotificationsTable             DestructionNotifications
 * @property DestructionRequestsTable                  DestructionRequests
 * @property EaccpfsTable                              Eaccpfs
 * @property FileuploadsTable                          Fileuploads
 * @property KeywordListsTable                         KeywordLists
 * @property KeywordsTable                             Keywords
 * @property LdapsTable                                Ldaps
 * @property OrgEntitiesTable                          OrgEntities
 * @property OrgEntitiesTimestampersTable              OrgEntitiesTimestampers
 * @property OutgoingTransferRequestsTable             OutgoingTransferRequests
 * @property OutgoingTransfersTable                    OutgoingTransfers
 * @property ProfilesTable                             Profiles
 * @property RestitutionRequestsTable                  RestitutionRequests
 * @property RestitutionsTable                         Restitutions
 * @property RolesTable                                Roles
 * @property SecureDataSpacesTable                     SecureDataSpaces
 * @property SequencesTable                            Sequences
 * @property ServiceLevelsTable                        ServiceLevels
 * @property TechnicalArchivesTable                    TechnicalArchives
 * @property TransferAttachmentsTable                  TransferAttachments
 * @property TransferErrorsTable                       TransferErrors
 * @property TransfersTable                            Transfers
 * @property UsersTable                                Users
 * @property ValidationChainsTable                     ValidationChains
 * @property ValidationProcessesTable                  ValidationProcesses
 * @property ValidationStagesTable                     ValidationStages
 */
class SealComponent extends Component
{
    public const string COND_ARCHIVAL_AGENCY = 'archivalAgency';
    public const string COND_ARCHIVAL_AGENCY_CHILDS = 'archivalAgencyChilds';
    public const string COND_HIERARCHICAL_VIEW = 'hierarchicalView';
    public const string COND_ORG_ENTITY = 'orgEntity';
    public const string COND_ORG_ENTITY_CHILDS = 'orgEntityChilds';

    /**
     * Default config
     *
     * These are merged with user-provided config when the component is used.
     *
     * @var array<string, bool, array>
     */
    protected array $_defaultConfig = [
        'user' => [],
        'userId' => null,
        'orgEntity' => [],
        'orgEntityId' => null,
        'archivalAgency' => [],
        'archivalAgencyId' => null,
        'superArchivist' => false,
        'adminTech' => false,
        'condition' => null,
    ];

    /**
     * Constructor
     *
     * @param \Cake\Controller\ComponentRegistry $registry A component registry
     *                                                     this component can use to lazy load its components.
     * @param array<string, mixed>               $config   Array of configuration settings.
     */
    public function __construct(ComponentRegistry $registry, array $config = [])
    {
        foreach ($config as $key => $value) {
            if ($value instanceof EntityInterface) {
                $config[$key] = $value->toArray();
            }
        }
        parent::__construct($registry, $config);
    }

    /**
     * Permet d'utiliser ce Component en dehors des controlleurs pour tester
     * les conditions lors d'un debug par exemple
     * @param int  $userId
     * @param bool $isAdminTech
     * @return static
     */
    public static function createForUser(int $userId, bool $isAdminTech = false): self
    {
        $registry = new ComponentRegistry(new Controller(new ServerRequest()));
        $user = TableRegistry::getTableLocator()->get('Users')->find()
            ->where(['Users.id' => $userId])
            ->contain(
                [
                    'OrgEntities' => [
                        'ArchivalAgencies' => ['Configurations'],
                        'TypeEntities',
                    ],
                    'Ldaps',
                    'Roles',
                ]
            )
            ->firstOrFail();
        return new self(
            $registry,
            [
                'user' => $user,
                'userId' => Hash::get($user, 'id'),
                'orgEntity' => Hash::get($user, 'org_entity', []),
                'orgEntityId' => Hash::get($user, 'org_entity_id'),
                'archivalAgency' => Hash::get($user, 'org_entity.archival_agency', []),
                'archivalAgencyId' => Hash::get($user, 'org_entity.archival_agency.id', []),
                'superArchivist' => Hash::get($user, 'role.code') === RolesTable::CODE_SUPER_ARCHIVIST,
                'adminTech' => $isAdminTech,
            ]
        );
    }

    /**
     * Défini le type de condition "archivalAgency"
     * Applique une condition type :
     *      Model.archival_agency_id = LoggedUser.archival_agency_id
     * @return $this
     * @link SealBehavior::appendConditionsArchivalAgency()
     */
    public function archivalAgency()
    {
        $this->setConfig('condition', self::COND_ARCHIVAL_AGENCY);
        return $this;
    }

    /**
     * Défini le type de condition "archivalAgencyChilds"
     * Applique une condition type :
     *      Model.lft >= ArchivalAgency.lft AND Model.rght >= ArchivalAgency.rght
     * @return $this
     * @link SealBehavior::appendConditionsArchivalAgencyChilds()
     */
    public function archivalAgencyChilds()
    {
        $this->setConfig('condition', self::COND_ARCHIVAL_AGENCY_CHILDS);
        return $this;
    }

    /**
     * Défini le type de condition "hierarchicalView"
     * Se distingue de "archivalAgencyChilds" par la nécéssité que le role
     * utilisateur doit avoir l'option "hierarchical_view"
     * De plus, selon le type de l'entité de l'utilisateur, le lft/rght est soit
     * sur le service d'archives, soit sur l'entité
     *
     * Applique une condition type :
     *      Model.lft >= ArchivalAgency.lft AND Model.rght >= ArchivalAgency.rght
     *
     * @return $this
     * @link SealBehavior::appendConditionsHierarchicalView()
     */
    public function hierarchicalView()
    {
        $this->setConfig('condition', self::COND_HIERARCHICAL_VIEW);
        return $this;
    }

    /**
     * Défini le type de condition "archivalAgency"
     * Applique une condition type :
     *      Model.archival_agency_id = LoggedUser.archival_agency_id
     * @return $this
     * @link SealBehavior::appendConditionsOrgEntity()
     */
    public function orgEntity()
    {
        $typeEntity = Hash::get($this->getConfig('orgEntity'), 'type_entity.code');
        if (in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)) {
            $this->setConfig('condition', self::COND_ARCHIVAL_AGENCY);
        } else {
            $this->setConfig('condition', self::COND_ORG_ENTITY);
        }

        return $this;
    }

    /**
     * Défini le type de condition "archivalAgency"
     * Applique une condition type :
     *      Model.archival_agency_id = LoggedUser.archival_agency_id
     * @return $this
     * @link SealBehavior::appendConditionsOrgEntityChilds()
     */
    public function orgEntityChilds()
    {
        $this->setConfig('condition', self::COND_ORG_ENTITY_CHILDS);
        return $this;
    }

    /**
     * Donne le model avec SealBehavior configuré
     * @param string $modelName
     * @return Table|null
     */
    public function table(string $modelName): Table
    {
        $this->setConfig('model', $modelName);
        $model = $this->getController()->fetchTable($modelName);
        if ($model->hasBehavior('Seal')) {
            $model->removeBehavior('Seal');
        }
        $model->addBehavior('Seal', $this->getConfig());
        return $model;
    }
}
