<?php

/**
 * Asalae\Controller\Component\LoginComponent
 */

namespace Asalae\Controller\Component;

use Adldap\Adldap;
use Adldap\Auth\BindException;
use Adldap\Auth\Guard;
use Adldap\Auth\PasswordRequiredException;
use Adldap\Connections\Provider;
use Adldap\Models\Model;
use Adldap\Query\Builder;
use Adldap\Query\Factory;
use AsalaeCore\Controller\Component\CookieComponent;
use AsalaeCore\Factory\Utility;
use Asalae\Controller\AppController;
use Asalae\Model\Entity\Ldap;
use Asalae\Model\Entity\User;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\VersionsTable;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;

/**
 * Logique de connexion
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class LoginComponent extends Component
{
    /**
     * @var array evite l'inscription dans EventLogs de plusieurs tentatives sur
     * une même connexion
     */
    public static $attempts = [];

    /**
     * Connecte un utilisateur (initialise la session)
     * @param string $username
     * @param string $password
     * @return bool
     * @throws Exception
     */
    public function logUser(string $username, string $password): bool
    {
        if (isset(self::$attempts[$username])) {
            return self::$attempts[$username];
        }
        $success = false;
        /** @var AppController $controller */
        $controller = $this->getController();
        $locator = TableRegistry::getTableLocator();
        $Users = $locator->get('Users');
        $Configurations = $locator->get('Configurations');
        $user = $Users->find()
            ->where(
                [
                    'Users.username' => $username,
                    'Users.active' => true,
                ]
            )
            ->innerJoinWith('OrgEntities')
            ->contain(
                [
                    'OrgEntities' => [
                        'ArchivalAgencies',
                        'TypeEntities',
                    ],
                    'Ldaps',
                    'Roles',
                ]
            )
            ->first();
        if ($user) {
            $hasher = new DefaultPasswordHasher();
            $controller->loadComponent('AsalaeCore.Modal');
            $request = $controller->getRequest();
            if ($code = $request->getHeaderLine('X-Encode-Password')) {
                $password = hash($code, $password);
            }
            $passwordOk = $hasher->check(
                $password,
                $user->get('password') ?: ''
            );
            if (!$passwordOk && $user->get('ldap_id')) {
                /** @var Ldap $ldap */
                $ldap = $user->get('ldap');
                $ldapLogin = $user->get('ldap_login');
                /** @var Adldap $ad */
                $ad = Utility::get(Adldap::class);
                $ad->addProvider($ldap->getLdapConfig());
                try {
                    /** @var Provider $provider */
                    $provider = $ad->connect();
                    /** @var Guard $auth */
                    $auth = $provider->auth();
                    $passwordOk = $auth->attempt($ldapLogin, $password, true);
                    if ($passwordOk) {
                        /** @var Factory $search */
                        $search = $provider->search();
                        /** @var Builder $users */
                        $users = $search->users();
                        /** @var Model|false $ldapUser */
                        $ldapUser = $users->findBy(
                            $ldap->get('user_login_attribute'),
                            $ldapLogin
                        );
                    }
                    if ($passwordOk && !empty($ldapUser)) {
                        $name = $ldapUser->getAttribute(
                            $ldap->get('user_name_attribute')
                        );
                        $mail = $ldapUser->getAttribute(
                            $ldap->get('user_mail_attribute')
                        );
                        if ($name) {
                            $user->set('name', current($name));
                        }
                        if ($mail) {
                            $user->set('email', current($mail));
                        }
                    }
                } catch (BindException) {
                    $controller->Flash->error(
                        __("Echec de la connexion au LDAP")
                    );
                } catch (PasswordRequiredException) {
                    // The user didn't supply a password.
                }
            }
            if ($passwordOk) {
                $connect = $user->toArray();
                $controller->Authentication->setIdentity($user);
                $config = [];
                $archivalAgencyId = Hash::get(
                    $connect,
                    'org_entity.archival_agency.id'
                );
                /** @var EntityInterface $conf */
                $q = $Configurations->find()->where(['org_entity_id' => $archivalAgencyId]);
                foreach ($q as $conf) {
                    $config[$conf->get('name')] = $conf->get('setting');
                }
                $session = $controller->getRequest()->getSession();
                $session->write('ConfigArchivalAgency', $config);
                $success = true;
            }
        }

        if ($user && $user->get('agent_type') === 'person') {
            $app = Configure::read('App.name');
            $connectionMessage = __("L'utilisateur ''{0}''", h($username));
            /** @var EventLogsTable $EventLogs */
            $EventLogs = $locator->get('EventLogs');
            /** @var VersionsTable $Versions */
            $Versions = $locator->get('Versions');
            $version = $Versions->find()
                ->where(['subject' => $app])
                ->orderBy(['id' => 'desc'])
                ->firstOrFail();
            $entry = $success
                ? $EventLogs->newEntry(
                    'login',
                    'success',
                    __("Connexion de {0} à {1}", $connectionMessage, $app),
                    $user,
                    $version
                )
                : $EventLogs->newEntry(
                    'login',
                    'fail',
                    __(
                        "Echec de connexion de {0} à {1}",
                        $connectionMessage,
                        $app
                    ),
                    null,
                    $version
                );
            $EventLogs->save($entry);
        }

        self::$attempts[$username] = $success;
        return $success;
    }

    /**
     * Charge les cookies enregistré en base de donnée
     * @param User|EntityInterface $user
     * @throws Exception
     */
    public function loadCookies(User $user)
    {
        $controller = $this->getController();
        /** @var CookieComponent $CookieComponent */
        $CookieComponent = $controller->loadComponent('AsalaeCore.Cookie');
        $CookieComponent->setConfig('encryption', false);
        $cookies = $controller->getRequest()->getCookieParams();
        unset($cookies['PHPSESSID'], $cookies['XDEBUG_SESSION']);
        foreach (array_keys($cookies) as $cookie) {
            $CookieComponent->delete($cookie);
        }
        if (!empty($user->cookies)) {
            foreach (json_decode($user->cookies, true) as $cookie => $value) {
                $CookieComponent->write($cookie, $value);
            }
        }
    }
}
