<?php

/**
 * Asalae\Controller\Component\SearchEngineComponent
 */

namespace Asalae\Controller\Component;

use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Utility\Translit;
use Cake\Controller\Component;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\QueryInterface;
use Cake\Http\ServerRequest;
use Cake\Utility\Hash;

/**
 * Moteur de recherche
 *
 * Le model conserné doit avoir un champs qui concatène le champs à rechercher
 * de cette façon: '"champ1" "champ2" "peut comporter plusieurs mots"'
 * Ils doivent être stockés en minuscule et sans accents
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ConditionComponent Condition
 */
class SearchEngineComponent extends Component
{
    /**
     * @var string[] Components
     */
    public array $components = ['AsalaeCore.Condition'];

    /**
     * Default config
     *
     * These are merged with user-provided config when the component is used.
     *
     * @var array
     */
    protected array $_defaultConfig = [
        'model' => null,
        'field' => 'search',
        'result_field' => 'archival_agency_identifier',
        'search' => 'search',
        'limit' => 100,
    ];

    /**
     * @var QueryInterface
     */
    private $query;

    /**
     * @var ServerRequest
     */
    private $request;

    /**
     * @var string texte saisi dans le champ de recherche
     */
    private $search;

    /**
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $field;

    /**
     * @var array liste des mots/phrases que doit contenir $this->field (en minuscule, sans accents)
     */
    private $formatedSearch = [];

    /**
     * @var array traductions
     */
    private $translations;

    /**
     * Constructor hook method.
     *
     * Implement this method to avoid having to overwrite
     * the constructor and call parent.
     *
     * @param array<string, mixed> $config The configuration settings provided to this component.
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->translations = [
            'archival_agency_identifier' => __("Cote"),
            'name' => __("Nom"),
            'description' => __("Description"),
            'history' => __("Historique"),
            'content' => __("Mot clé"),
        ];
    }

    /**
     * Ajoute des conditions et order by à la requete selon le contenu du request data
     * @param QueryInterface $query
     * @return QueryInterface
     */
    public function query(QueryInterface $query): QueryInterface
    {
        $this->query = $query;
        $this->request = $this->getController()->getRequest();
        $this->model = $this->getConfig('model')
            ?: $this->getController()
                ->getName();
        $this->field = $this->getConfig('field');
        $this->search = $this->formatSearchText();

        $this->addDoubleQuotedSentences();
        $this->addNegativeWords();
        $this->addSearchWords();
        $this->addRegexConditions();

        return $query;
    }

    /**
     * trim, retire les caractères blancs consécutifs et les paires de double quotes vide
     * @return string
     */
    private function formatSearchText(): string
    {
        $search = $this->request->is('post')
            ? $this->request->getData($this->getConfig('search'))
            : $this->request->getQuery($this->getConfig('search'));
        return preg_replace(
            '/\s+/',
            ' ',
            str_replace(
                '""',
                '',
                trim($search)
            )
        );
    }

    /**
     * Effectu une recherche sur la totalité des mots compris entre les doubles quotes (")
     *
     * exemple ({} défini le texte trouvé):
     * - search text: "le réquisitoire de la partie"
     * - matching: Après {le réquisitoire de la partie} avec indifférence.
     * - not matching: La partie du réquisitoire comprend le conseiller de ...
     */
    private function addDoubleQuotedSentences(): void
    {
        $doubleQuotes = mb_substr_count($this->search, '"');
        if ($doubleQuotes > 0 && $doubleQuotes % 2 === 0) {
            $offset = 0;
            $newSearch = $this->search;
            while (($pos = mb_strpos($this->search, '"', $offset)) !== false) {
                $nextpos = mb_strpos($this->search, '"', $pos + 1);
                $offset = max($pos, (int)$nextpos) + 1;
                $sentence = mb_substr(
                    $this->search,
                    $pos + 1,
                    $pos + $nextpos - 1
                );
                $formated = trim(Translit::asciiToLower($sentence), ',.-*!?+');
                $this->query->andWhere(
                    ["$this->model.$this->field LIKE" => '%' . $formated . '%']
                );
                $this->formatedSearch[] = $formated;
                // retire la "phrase" de la recherche
                $newSearch = str_replace('"' . $sentence . '"', '', $newSearch);
            }
            $this->search = $newSearch;
        }
    }

    /**
     * Retire des résultats de recherche les mots commençant par (-)
     *
     * exemple ({} défini le texte trouvé):
     * - recherche: réquisitoire -partie
     * - matching: le {réquisitoire} du conseillé
     * - not matching: Après le {réquisitoire} de la {partie} avec indifférence.
     */
    private function addNegativeWords(): void
    {
        foreach (explode(' ', $this->search) as $str) {
            $formated = trim(Translit::asciiToLower($str), ',.*!?+');
            if ($formated && $formated[0] === '-') {
                $formated = mb_substr($formated, 1);
                $this->query->andWhere(
                    ["$this->model.$this->field NOT LIKE" => '%' . $formated . '%']
                );
            }
        }
    }

    /**
     * La recherche doit contenir les mots choisis
     *
     * exemple ({} défini le texte trouvé):
     * - recherche: réquisitoire partie
     * - matching: Après le {réquisitoire} de la {partie} avec indifférence.
     * - not matching: Marchés publics
     */
    private function addSearchWords(): void
    {
        foreach (explode(' ', $this->search) as $str) {
            $formated = trim(Translit::asciiToLower($str), ',.*!?+');
            if ($formated && $formated[0] !== '-') {
                $this->query->andWhere(
                    ["$this->model.$this->field LIKE" => '%' . $formated . '%']
                );
                $this->formatedSearch[] = $formated;
            }
        }
    }

    /**
     * Ajout de filtres regex et d'order by pour accélérer la recherche
     */
    private function addRegexConditions(): void
    {
        $regexReadySearch = array_map(
            'preg_quote',
            $this->formatedSearch,
            ['/']
        );
        // fait appairaitre les termes exact au début
        $order = [
            $this->Condition->like(
                "$this->model.$this->field",
                '%"' . implode(' ', $this->formatedSearch) . '"%',
                true
            ) => 'desc',
            $this->Condition->regex(
                "$this->model.$this->field",
                implode('|', $regexReadySearch),
                true
            ) => 'desc',
        ];
        $conditions = [];
        foreach ($this->formatedSearch as $srch) {
            // Si le mot est suffisament long, on permet une recherche approchante
            $len = mb_strlen($srch);
            $maxDiff = min($len - 3, 5);
            if ($maxDiff <= 0) {
                continue;
            }
            $srch = preg_quote($srch);
            $order[$this->Condition->regex(
                "$this->model.$this->field",
                "[^\w]{$srch}[^\w]",
                true
            )] = 'desc';
            $maxDiffDiv2 = ceil($maxDiff / 2);
            $rexp = [
                // terme exacte ou "commence par"
                sprintf('[^\w]%s[\w]{0,%d}[^\w]', $srch, $maxDiff),
                // à l'interieur d'un mot
                sprintf(
                    '[^\w][\w]{1,%d}%s[\w]{1,%d}[^\w]',
                    $maxDiffDiv2,
                    $srch,
                    $maxDiffDiv2
                ),
                // fini par
                sprintf('[^\w][\w]{1,%d}%s[^\w]', $maxDiff, $srch),
            ];
            $conditions += $this->Condition->regex(
                "$this->model.$this->field",
                implode('|', $rexp)
            );
        }
        $order["length($this->model.$this->field)"] = 'asc';
        $this->query->orderBy($order)->andWhere($conditions);
    }

    /**
     * Donne la liste des recherches formatés (minuscule et sans accents)
     * @return array
     */
    public function getFormatedSearch(): array
    {
        return $this->formatedSearch;
    }

    /**
     * Liste les résultats de la recherche, ordonné par pertinence
     * @param array         $fields
     * @param callable|null $beforeScore
     * @param int           $timelimit
     * @return array
     */
    public function results(
        array $fields,
        ?callable $beforeScore = null,
        int $timelimit = 3
    ) {
        $results = [];
        $begin = microtime(true);
        $i = 0;
        /** @var EntityInterface $entity */
        foreach ($this->query as $entity) {
            if ($beforeScore) {
                $beforeScore($entity);
            }

            // limite la requete à $timelimit secondes
            if (microtime(true) - $begin > $timelimit) {
                break;
            }
            $result = [];
            $maxscore = 0;
            foreach ($fields as $path) {
                $score = $this->extractSearchData(
                    $path,
                    $this->formatedSearch,
                    $entity,
                    $result
                );
                $maxscore = max($score, $maxscore);
            }
            if ($result) {
                $results[] = [
                    'score' => $maxscore,
                    'id' => $entity->id,
                    'result' => $entity->get($this->getConfig('result_field')),
                    'scores' => $result,
                    'text' => $entity->get($this->getConfig('field')),
                    'entity' => $entity,
                    'i' => $i++,
                ];
                if (count($results) >= $this->getConfig('limit')) {
                    break;
                }
            }
        }
        uasort(
            $results,
            function ($a, $b) {
                if ($a['score'] === $b['score']) {
                    return $a['i'] < $b['i'] ? -1 : 1;
                }
                return ($a['score'] > $b['score']) ? -1 : 1;
            }
        );
        return $results;
    }

    /**
     * Donne la liste des similitudes entre les champs en BDD et la recherche
     * @param string          $path
     * @param array           $formatedSearch
     * @param EntityInterface $entity
     * @param array           $result
     * @return float
     */
    private function extractSearchData(
        string $path,
        array $formatedSearch,
        EntityInterface $entity,
        array &$result
    ): float {
        $maxscore = 0;
        $inlineSearch = implode(' ', $formatedSearch);
        foreach (Hash::extract($entity, $path) as $val) {
            if (!($val = trim($val))) {
                continue;
            }
            $exactMatch = [];
            $partialMatch = [];
            // récupère baz dans foo.bar.baz
            $fieldname = trim(mb_substr($path, mb_strrpos($path, '.')), '.');
            $formatedVal = Translit::asciiToLower($val, true);
            if ($formatedVal === $inlineSearch) {
                $maxscore = 100;
                $result[$fieldname] = [
                    'field' => $this->translations[$fieldname] ?? $fieldname,
                    'score' => 100,
                    'str' => str_replace( // autorise les <br />, <b> et </b> seulement
                        ['&lt;br /&gt;', '&lt;b&gt;', '&lt;/b&gt;'],
                        ['<br />', '<b>', '</b>'],
                        '<b>' . h(
                            preg_replace(
                                ['/\s+/', '/<\/b> <b>/'],
                                [' ', ' '],
                                $val
                            )
                        ) . '</b>'
                    ),
                ];
                break;// on ne peut pas faire mieu comme score, on arrete donc ici
            }
            /**
             * $val est la valeur du champ en BDD, $formatedSearch la liste des mots recherchés
             * on compare chaques mots en BDD avec chaques mots recherchés
             * - si mot identique, on ajoute $exactMatch[$mot] = mot non formaté
             * - si mot approchant, on ajoute $partialMatch[$mot][$diff] = mot non formaté
             *
             * Il peut y avoir maximum 3 différences dans un mot.
             * Le nombre exact de différences tolérés est la taille du mot - 3
             * exemples:
             * - mot de 3 lettres ou moins: 0 diff
             * - mot de 4 lettres : 1 diff
             * - mot de 9 lettres: 3 diff (max)
             */
            foreach ($formatedSearch as $search) {
                $quoted = preg_quote($search, '/');
                $len = mb_strlen($search);
                $maxDiff = min($len - 3, 5);
                $maxDiffDiv2 = ceil($maxDiff / 2);
                if (preg_match("/(?:^|\W)($quoted)(?:\W|$)/", $formatedVal, $m)) {
                    $pos = mb_strpos($formatedVal, $m[1]);
                    $exactMatch[$search] = mb_substr($val, $pos, $len);
                    continue;
                } elseif ($maxDiff <= 0) {
                    continue;
                }
                $regBefore = "/(?:^|\W)\w{1,$maxDiff}($quoted)(?:\W|$)/";
                $regBetween = "/(?:^|\W)\w{1,$maxDiffDiv2}($quoted)\w{1,$maxDiffDiv2}(?:\W|$)/";
                $regAfter = "/(?:^|\W)($quoted)\w{1,$maxDiff}(?:\W|$)/";
                // la partie à trouver ce situe au début du mot
                if (
                    preg_match($regBefore, $formatedVal, $m)
                    || preg_match($regBetween, $formatedVal, $m)
                    || preg_match($regAfter, $formatedVal, $m)
                ) {
                    $pos = mb_strpos($formatedVal, $m[1]);
                    $partialMatch[$search][strlen($m[1]) - $len] = mb_substr(
                        $val,
                        $pos,
                        $len
                    );
                }
            }
            $score = 0;
            foreach ($exactMatch as $match) {
                $esc = preg_quote($match, '/');
                $val = preg_replace(
                    "/(^|.*\W)($esc)(\W.*|$)/m",
                    '$1<b>$2</b>$3',
                    $val
                );
                $score += 2;
            }
            foreach ($partialMatch as $matches) {
                ksort($matches);
                $match = current($matches);
                $esc = preg_quote($match, '/');
                $val = preg_replace(
                    "/^(.*)($esc)(.*)$/m",
                    '$1<b>$2</b>$3',
                    $val
                );
                $score += 1;
            }
            if ($score) {
                $maxscore = max($score, $maxscore);
                $maxscore = min($maxscore, 99); // le score est limité à 100
                $result[$fieldname] = [
                    'field' => $this->translations[$fieldname] ?? $fieldname,
                    'score' => $score,
                    'str' => str_replace( // autorise les <br />, <b> et </b> seulement
                        ['&lt;br /&gt;', '&lt;b&gt;', '&lt;/b&gt;'],
                        ['<br />', '<b>', '</b>'],
                        h(
                            preg_replace(
                                ['/\s+/', '/<\/b> <b>/'],
                                [' ', ' '],
                                $val
                            )
                        )
                    ),
                ];
            }
        }
        return $maxscore;
    }
}
