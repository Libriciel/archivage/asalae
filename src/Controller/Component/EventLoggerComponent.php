<?php

/**
 * Asalae\Controller\Component\EventLoggerComponent
 */

namespace Asalae\Controller\Component;

use Asalae\Controller\AppController;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\VersionsTable;
use Asalae\View\Helper\TranslateHelper;
use Cake\Controller\Component;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\View\View;

/**
 * Journalisation des actions standards
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EventLoggerComponent extends Component
{
    /**
     * Ajoute au journal des evenements l'accès à un index
     * @param array{typeName: string, indexName: string} $params
     */
    public function logIndex(array $params = [])
    {
        /** @var AppController $Controller */
        $Controller = $this->getController();
        $session = $Controller->getRequest()->getSession();
        $lastIndex = $session->read('lastIndex');
        if ($lastIndex !== $Controller->getName()) {
            $loc = TableRegistry::getTableLocator();
            $session->write('lastIndex', $Controller->getName());
            /** @var EventLogsTable $EventLogs */
            $EventLogs = $loc->get('EventLogs');
            /** @var VersionsTable $Versions */
            $Versions = $loc->get('Versions');
            $translateHelper = new TranslateHelper(new View());
            $params += [
                'typeName' => $Controller->getName(),
                'indexName' => $translateHelper->permission($Controller->getName()),
            ];
            $entry = $EventLogs->newEntry(
                'display_' . $params['typeName'],
                'success',
                __(
                    "Affichage de la liste ({0}) par l'utilisateur ({1})",
                    $params['indexName'],
                    $Controller->Authentication->getIdentityData('username')
                ),
                $Controller->Authentication->getIdentityData('id'),
                $Versions->getAppVersion()
            );
            $EventLogs->saveOrFail($entry);
        }
    }

    /**
     * Ajoute au journal des evenements un ajout d'objet
     * @param EntityInterface $object
     */
    public function logAdd(EntityInterface $object)
    {
        /** @var AppController $Controller */
        $Controller = $this->getController();
        $translateHelper = new TranslateHelper(new View());
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->newEntry(
            'add_' . Inflector::singularize($Controller->getName()),
            'success',
            __(
                "Ajouté ({0}) par l'utilisateur ({1})",
                $translateHelper->permission($Controller->getName()),
                $Controller->Authentication->getIdentityData('username')
            ),
            $Controller->Authentication->getIdentityData('id'),
            $object
        );
        $EventLogs->saveOrFail($entry);
    }

    /**
     * Ajoute au journal des evenements une modification d'objet
     * @param EntityInterface $object
     */
    public function logEdit(EntityInterface $object)
    {
        /** @var AppController $Controller */
        $Controller = $this->getController();
        $translateHelper = new TranslateHelper(new View());
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->newEntry(
            'edit_' . Inflector::singularize($Controller->getName()),
            'success',
            __(
                "Modifié ({0}) par l'utilisateur ({1})",
                $translateHelper->permission($Controller->getName()),
                $Controller->Authentication->getIdentityData('username')
            ),
            $Controller->Authentication->getIdentityData('id'),
            $object
        );
        $EventLogs->saveOrFail($entry);
    }

    /**
     * Ajoute au journal des evenements d'une action unitaire sur un objet
     * ne convient pas pour les index
     * @param EntityInterface                                                                             $object
     * @param array{actionName: string, actionLiteral: string, objectName: string, objectLiteral: string} $params
     */
    public function logAction(EntityInterface $object, array $params = [])
    {
        $actionLiterals = [
            'view' => __("Visualisé"),
            'add' => __("Ajouté"),
            'edit' => __("Modifié"),
            'delete' => __("Supprimé"),
            'download' => __("Téléchargé"),
            'acquit' => __("Acquitté"),
        ];
        /** @var AppController $Controller */
        $Controller = $this->getController();
        $translateHelper = new TranslateHelper(new View());
        $actionName = empty($params['actionName'])
            ? $Controller->getRequest()->getParam('action')
            : $params['actionName'];
        $params += [
            'actionName' => $actionName,
            'actionLiteral' => $actionLiterals[$actionName] ?? $actionName,
            'objectName' => Inflector::singularize($Controller->getName()),
            'objectLiteral' => $translateHelper->permission($Controller->getName()),
        ];
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->newEntry(
            $params['actionName'] . '_' . $params['objectName'],
            'success',
            __(
                "{0} ({1}) par l'utilisateur ({2})",
                $params['actionLiteral'],
                $params['objectLiteral'],
                $Controller->Authentication->getIdentityData('username')
            ),
            $Controller->Authentication->getIdentityData('id'),
            $object
        );
        $EventLogs->saveOrFail($entry);
    }

    /**
     * Ajoute au journal des evenements une modification d'objet
     * @param EntityInterface $object
     */
    public function logDelete(EntityInterface $object)
    {
        /** @var AppController $Controller */
        $Controller = $this->getController();
        $translateHelper = new TranslateHelper(new View());
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->newEntry(
            'delete_' . Inflector::singularize($Controller->getName()),
            'success',
            __(
                "Supprimé ({0}) par l'utilisateur ({1})",
                $translateHelper->permission($Controller->getName()),
                $Controller->Authentication->getIdentityData('username')
            ),
            $Controller->Authentication->getIdentityData('id'),
            $object
        );
        $EventLogs->saveOrFail($entry);
    }

    /**
     * Ajoute au journal des evenements l'envoi dans un circuit de validation d'un objet
     * @param EntityInterface $object
     */
    public function logValidationSend(EntityInterface $object)
    {
        /** @var AppController $Controller */
        $Controller = $this->getController();
        $translateHelper = new TranslateHelper(new View());
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->newEntry(
            'process_' . Inflector::singularize($Controller->getName()),
            'success',
            __(
                "Envoyé pour validation ({0}) par l'utilisateur ({1})",
                $translateHelper->permission($Controller->getName()),
                $Controller->Authentication->getIdentityData('username')
            ),
            $Controller->Authentication->getIdentityData('id'),
            $object
        );
        $EventLogs->saveOrFail($entry);
    }

    /**
     * Ajoute au journal des evenements l'acceptation lors de la validation d'un objet
     * @param EntityInterface $object
     * @param string          $controllerName
     */
    public function logValidationAccept(EntityInterface $object, string $controllerName)
    {
        /** @var AppController $Controller */
        $Controller = $this->getController();
        $translateHelper = new TranslateHelper(new View());
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->newEntry(
            'validate_' . Inflector::singularize($controllerName),
            'success',
            __(
                "Accepté lors de la validation ({0}) par l'utilisateur ({1})",
                $translateHelper->permission($controllerName),
                $Controller->Authentication->getIdentityData('username')
            ),
            $Controller->Authentication->getIdentityData('id'),
            $object
        );
        $EventLogs->saveOrFail($entry);
    }

    /**
     * Ajoute au journal des evenements le rejet lors de la validation d'un objet
     * @param EntityInterface $object
     * @param string          $controllerName
     */
    public function logValidationReject(EntityInterface $object, string $controllerName)
    {
        /** @var AppController $Controller */
        $Controller = $this->getController();
        $translateHelper = new TranslateHelper(new View());
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $entry = $EventLogs->newEntry(
            'invalidate_' . Inflector::singularize($controllerName),
            'success',
            __(
                "Refus lors de la validation ({0}) par l'utilisateur ({1})",
                $translateHelper->permission($controllerName),
                $Controller->Authentication->getIdentityData('username')
            ),
            $Controller->Authentication->getIdentityData('id'),
            $object
        );
        $EventLogs->saveOrFail($entry);
    }
}
