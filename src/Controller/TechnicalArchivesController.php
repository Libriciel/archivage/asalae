<?php

/**
 * Asalae\Controller\TechnicalArchivesController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Entity\TechnicalArchiveUnit;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\TechnicalArchivesTable;
use Asalae\Model\Table\TechnicalArchiveUnitsTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Response;
use Cake\Utility\Hash;
use Exception;
use FileConverters\Exception\FileNotReadableException;
use Psr\Http\Message\ResponseInterface;

/**
 * TechnicalArchives
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveBinariesTable ArchiveBinaries
 * @property FileuploadsTable Fileuploads
 * @property TechnicalArchiveUnitsTable TechnicalArchiveUnits
 * @property TechnicalArchivesTable TechnicalArchives
 */
class TechnicalArchivesController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'TechnicalArchives.id' => 'asc',
        ],
    ];

    /**
     * Liste les enregistrements
     */
    public function index()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        /** @var TechnicalArchivesTable $TechnicalArchives */
        $TechnicalArchives = $this->fetchTable('TechnicalArchives');
        $query = $TechnicalArchives->find()
            ->where(['TechnicalArchives.archival_agency_id' => $this->archivalAgencyId]);

        $IndexComponent->setQuery($query)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('type', IndexComponent::FILTER_IN)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);
        $this->set('types', $TechnicalArchives->options('type'));
        $this->set('states', $TechnicalArchives->options('state'));
    }

    /**
     * Visualisation
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        /** @var TechnicalArchivesTable $TechnicalArchives */
        $TechnicalArchives = $this->fetchTable('TechnicalArchives');
        $entity = $TechnicalArchives->find()
            ->where(
                [
                    'TechnicalArchives.id' => $id,
                    'TechnicalArchives.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['FirstTechnicalArchiveUnits', 'SecureDataSpaces'])
            ->firstOrFail();
        $this->set('entity', $entity);

        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        $query = $ArchiveBinaries->find()
            ->innerJoinWith('TechnicalArchiveUnits')
            ->innerJoinWith('StoredFiles')
            ->where(['TechnicalArchiveUnits.technical_archive_id' => $id])
            ->contain(
                [
                    'StoredFiles',
                    'TechnicalArchiveUnits' => [
                        'TechnicalArchives',
                    ],
                ]
            )
            ->orderBy(['ArchiveBinaries.filename' => 'asc']);
        $count = $query->count();
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $this->set(
            'files',
            $files = $query->limit($AjaxPaginatorComponent->getConfig('limit'))
                ->all()
                ->toArray()
        );
        $this->set('filesCount', $count);
        $this->set('id', $id);
        $AjaxPaginatorComponent->setViewPaginator($files, $count);
    }

    /**
     * Téléchargement d'un fichier d'archive technique
     * @param string $archive_binary_id
     * @return Response
     */
    public function downloadFile(string $archive_binary_id)
    {
        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        /** @var EntityInterface $entity */
        $entity = $ArchiveBinaries->find()
            ->innerJoinWith('TechnicalArchiveUnits')
            ->innerJoinWith('TechnicalArchiveUnits.TechnicalArchives')
            ->innerJoinWith('StoredFiles')
            ->where(
                [
                    'ArchiveBinaries.id' => $archive_binary_id,
                    'TechnicalArchives.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'StoredFiles',
                    'TechnicalArchiveUnits' => [
                        'TechnicalArchives',
                    ],
                ]
            )
            ->firstOrFail();

        return $this->getResponse()
            ->withHeader(
                'Content-Type',
                $entity->get('mime') ?: 'application/octet-stream'
            )
            ->withHeader(
                'Content-Disposition',
                'attachment; filename="' . basename(
                    $entity->get('filename') . '"'
                )
            )
            ->withHeader(
                'Content-Length',
                Hash::get($entity, 'stored_file.size')
            )
            ->withHeader(
                'File-Size',
                Hash::get($entity, 'stored_file.size')
            )
            ->withStringBody(Hash::get($entity, 'stored_file.file'));
    }

    /**
     * pagination ajax des fichiers
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function paginateFiles(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        $query = $ArchiveBinaries->find()
            ->innerJoinWith('TechnicalArchiveUnits')
            ->innerJoinWith('TechnicalArchiveUnits.TechnicalArchives')
            ->innerJoinWith('StoredFiles')
            ->where(
                [
                    'TechnicalArchiveUnits.technical_archive_id' => $id,
                    'TechnicalArchives.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'StoredFiles',
                    'TechnicalArchiveUnits' => [
                        'TechnicalArchives',
                    ],
                ]
            )
            ->orderBy(['ArchiveBinaries.filename' => 'asc']);

        $this->loadComponent(
            'AsalaeCore.Index',
            ['model' => 'ArchiveBinaries']
        );
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        $IndexComponent->setQuery($query)
            ->filter('filename', IndexComponent::FILTER_ILIKE)
            ->filter(
                'size',
                function ($s) {
                    if (!$s['value']) {
                        return;
                    }
                    $value = $s['value'] * $s['mult'];
                    $operator = $s['operator'];
                    if (!in_array($operator, ['=', '<=', '>=', '<', '>'])) {
                        throw new BadRequestException(
                            __("L'opérateur indiqué n'est pas autorisé")
                        );
                    }
                    return ['StoredFiles.size ' . $operator => $value];
                }
            );
        return $AjaxPaginatorComponent->json($query);
    }

    /**
     * Ajout d'une archive technique
     * @return Response|void
     * @throws Exception
     */
    public function add()
    {
        $request = $this->getRequest();
        if ($request->is('post')) {
            $uploads = $request->getData('fileuploads');
            /** @var FileuploadsTable $Fileuploads */
            $Fileuploads = $this->fetchTable('Fileuploads');
            $uploadData = $uploads
                ? $Fileuploads->find()
                    ->where(
                        [
                            'Fileuploads.id IN' => $uploads['_ids'],
                            'Fileuploads.user_id' => $this->userId,
                        ]
                    )
                    ->toArray()
                : [];
            $this->set('uploadData', $uploadData);

            /** @var TechnicalArchivesTable $TechnicalArchives */
            $TechnicalArchives = $this->fetchTable('TechnicalArchives');
            /** @var TechnicalArchiveUnitsTable $TechnicalArchiveUnits */
            $TechnicalArchiveUnits = $this->fetchTable('TechnicalArchiveUnits');
            $sum = fn($total, $value) => $total + $value;
            $technicalArchive = $TechnicalArchives->newEntity(
                [
                    'state' => $TechnicalArchives->initialState,
                    'type' => TechnicalArchivesTable::TYPE_ADDED_BY_USER,
                    'archival_agency_id' => $this->archivalAgencyId,
                    'archival_agency_identifier' => $uuid = uuid_create(UUID_TYPE_TIME),
                    'secure_data_space_id' => $this->archivalAgency->get('default_secure_data_space_id'),
                    'storage_path' => sprintf(
                        '%s/technical_archives/%s',
                        $this->archivalAgency->get('identifier'),
                        $uuid
                    ),
                    'name' => $request->getData('name'),
                    'original_size' => $size = Hash::reduce($uploadData, '{n}.size', $sum) ?: 0,
                    'original_count' => count($uploadData),
                    'timestamp_size' => 0,
                    'timestamp_count' => 0,
                    'management_size' => 0,
                    'management_count' => 0,
                ]
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $conn = $TechnicalArchives->getConnection();
            $conn->begin();
            $TechnicalArchives
                ->transitionOrFail($technicalArchive, TechnicalArchivesTable::T_DESCRIBE)
                ->transitionOrFail($technicalArchive, TechnicalArchivesTable::T_STORE)
                ->transitionOrFail($technicalArchive, TechnicalArchivesTable::T_FINISH);
            $success = $TechnicalArchives->save($technicalArchive);
            if ($success) {
                /** @var TechnicalArchiveUnit $technicalArchiveUnit */
                $technicalArchiveUnit = $TechnicalArchiveUnits->newEntity(
                    [
                        'state' => $TechnicalArchiveUnits->initialState,
                        'technical_archive_id' => $technicalArchive->id,
                        'archival_agency_identifier' => $uuid,
                        'name' => $request->getData('name'),
                        'description' => $request->getData('technical_archive_units.0.description'),
                        'history' => $request->getData('technical_archive_units.0.history'),
                        'oldest_date' => $request->getData('technical_archive_units.0.oldest_date'),
                        'latest_date' => $request->getData('technical_archive_units.0.latest_date'),
                        'original_local_size' => $size,
                        'original_local_count' => count($uploadData),
                        'original_total_size' => $size,
                        'original_total_count' => count($uploadData),
                        'xml_node_tagname' => '',
                    ]
                );
                $success = $TechnicalArchiveUnits->save($technicalArchiveUnit);
            }
            if ($success) {
                $technicalArchiveUnit->addFiles($uploadData);
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAdd($technicalArchive);
                $conn->commit();
                $this->Modal->success();
                return $this->renderDataToJson($technicalArchive);
            } else {
                $conn->rollback();
                $this->Modal->fail();
                $this->set('entity', $technicalArchive);
            }
        } else {
            /** @var TechnicalArchivesTable $TechnicalArchives */
            $TechnicalArchives = $this->fetchTable('TechnicalArchives');
            $this->set('entity', $TechnicalArchives->newEmptyEntity());
        }
        $uploadId = 'technical-archive-upload-table';
        $this->set('uploadId', $uploadId);
    }

    /**
     * Modification d'une archive technique
     * @param string $id
     * @return Response|void
     * @throws VolumeException
     * @throws FileNotReadableException
     */
    public function edit(string $id)
    {
        $request = $this->getRequest();
        $entity = $this->Seal->archivalAgency()
            ->table('TechnicalArchives')
            ->find('seal')
            ->where(
                [
                    'TechnicalArchives.id' => $id,
                    'TechnicalArchives.type' => TechnicalArchivesTable::TYPE_ADDED_BY_USER,
                ]
            )
            ->contain(
                [
                    'TechnicalArchiveUnits' => [
                        'ArchiveBinaries' => [
                            'sort' => 'filename',
                            'StoredFiles',
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        /** @var TechnicalArchiveUnit $technicalArchiveUnit */
        $technicalArchiveUnit = $entity->get('technical_archive_units')[0];
        if ($request->is('put')) {
            $uploads = $request->getData('fileuploads');
            /** @var FileuploadsTable $Fileuploads */
            $Fileuploads = $this->fetchTable('Fileuploads');
            $uploadData = $uploads
                ? $Fileuploads->find()
                    ->where(
                        [
                            'Fileuploads.id IN' => $uploads['_ids'],
                            'Fileuploads.user_id' => $this->userId,
                        ]
                    )
                    ->toArray()
                : [];
            $this->set('uploadData', $uploadData);

            /** @var TechnicalArchivesTable $TechnicalArchives */
            $TechnicalArchives = $this->fetchTable('TechnicalArchives');
            /** @var TechnicalArchiveUnitsTable $TechnicalArchiveUnits */
            $TechnicalArchiveUnits = $this->fetchTable('TechnicalArchiveUnits');
            $data = $request->getData();
            $data['technical_archive_unit'][0]['name'] = $data['name'];
            $TechnicalArchives->patchEntity($entity, ['name' => $request->getData('name')]);
            $TechnicalArchiveUnits->patchEntity(
                $technicalArchiveUnit,
                [
                    'name' => $request->getData('name'),
                    'description' => $request->getData('technical_archive_units.0.description'),
                    'history' => $request->getData('technical_archive_units.0.history'),
                    'oldest_date' => $request->getData('technical_archive_units.0.oldest_date'),
                    'latest_date' => $request->getData('technical_archive_units.0.latest_date'),
                ]
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $conn = $TechnicalArchives->getConnection();
            $conn->begin();
            $success = $TechnicalArchives->save($entity)
                && $TechnicalArchiveUnits->save($technicalArchiveUnit);
            if ($success) {
                $technicalArchiveUnit->addFiles($uploadData);
                $TechnicalArchives->updateCountSize([$entity]);
                $TechnicalArchiveUnits->updateCountSize([$technicalArchiveUnit]);
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logEdit($entity);
                $conn->commit();
                $this->Modal->success();
                return $this->renderDataToJson($entity);
            } else {
                $conn->rollback();
                $this->Modal->fail();
            }
        }
        $this->set('entity', $entity);
        $uploadId = 'technical-archive-edit-upload-table';
        $this->set('uploadId', $uploadId);
    }

    /**
     * Téléchargement d'un fichier d'archive technique
     * @param string $archive_binary_id
     * @return Response
     * @throws VolumeException
     */
    public function deleteFile(string $archive_binary_id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        /** @var EntityInterface $entity */
        $archiveBinary = $ArchiveBinaries->find()
            ->innerJoinWith('TechnicalArchiveUnits')
            ->innerJoinWith('TechnicalArchiveUnits.TechnicalArchives')
            ->innerJoinWith('StoredFiles')
            ->where(
                [
                    'ArchiveBinaries.id' => $archive_binary_id,
                    'TechnicalArchives.archival_agency_id' => $this->archivalAgencyId,
                    'TechnicalArchives.type' => TechnicalArchivesTable::TYPE_ADDED_BY_USER,
                ]
            )
            ->contain(
                [
                    'StoredFiles',
                    'TechnicalArchiveUnits' => [
                        'TechnicalArchives',
                    ],
                ]
            )
            ->firstOrFail();
        $secureDataSpaceId = Hash::get(
            $archiveBinary,
            'technical_archive_units.0.technical_archive.secure_data_space_id'
        );
        $volumeManager = new VolumeManager($secureDataSpaceId);
        /** @var TechnicalArchivesTable $TechnicalArchives */
        $TechnicalArchives = $this->fetchTable('TechnicalArchives');
        $conn = $TechnicalArchives->getConnection();
        $conn->begin();
        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logDelete($archiveBinary);
        if ($ArchiveBinaries->delete($archiveBinary)) {
            $volumeManager->fileDelete(Hash::get($archiveBinary, 'stored_file.name'));
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Suppression d'une archive technique
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var TechnicalArchivesTable $TechnicalArchives */
        $TechnicalArchives = $this->fetchTable('TechnicalArchives');
        $entity = $this->Seal->archivalAgency()
            ->table('TechnicalArchives')
            ->find('seal')
            ->where(['TechnicalArchives.id' => $id])
            ->firstOrFail();
        if (!$entity->get('deletable')) {
            throw new ForbiddenException(
                __("Tentative de suppression d'une entité protégée")
            );
        }

        $conn = $TechnicalArchives->getConnection();
        $conn->begin();
        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logDelete($entity);
        if ($TechnicalArchives->delete($entity)) {
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }
}
