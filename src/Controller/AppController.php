<?php

/**
 * Asalae\Controller\AppController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Controller\Component\LoginComponent;
use Asalae\Controller\Component\MercureComponent;
use Asalae\Controller\Component\RequestHandlerComponent;
use Asalae\Controller\Component\SealComponent;
use Asalae\Controller\Component\SearchEngineComponent;
use Asalae\Exception\GenericException;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\FiltersTable;
use Asalae\Model\Table\NotificationsTable;
use Asalae\Model\Table\RolesTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\SavedFiltersTable;
use Asalae\Model\Table\UsersTable;
use Asalae\View\Helper\TranslateHelper;
use AsalaeCore\Auth\AuthenticationService;
use AsalaeCore\Controller\Component\AclComponent;
use AsalaeCore\Controller\Component\CookieComponent;
use AsalaeCore\Controller\Component\JstableComponent;
use AsalaeCore\Controller\Component\RestComponent;
use AsalaeCore\Controller\Controller as CoreController;
use AsalaeCore\Error\ErrorTrap;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\AppServerRequest;
use AsalaeCore\Http\Response as CoreResponse;
use Authentication\Controller\Component\AuthenticationComponent;
use Authorization\Controller\Component\AuthorizationComponent;
use Authorization\Identity;
use Cake\Controller\Exception\MissingActionException;
use Cake\Core\Configure;
use Cake\Database\Exception\DatabaseException;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Response as CakeResponse;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\View\View;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Tous les contrôleurs descendent de AppController
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 * @property AclComponent          Acl
 * @property AppServerRequest      request
 * @property ArchiveUnitsTable     ArchiveUnits
 * @property EventLoggerComponent  EventLogger
 * @property FiltersTable          Filters
 * @property LoginComponent        Login
 * @property NotificationsTable    Notifications
 * @property SavedFiltersTable     SavedFilters
 * @property SearchEngineComponent SearchEngine
 * @property SealComponent         Seal
 * @property UsersTable            Users
 */
class AppController extends CoreController
{
    /**
     * @var array Garde en mémoire les variables passés à la vue
     *            (remplace $this->viewVars qui est deprecated)
     */
    public $immutableViewVars = [];
    /**
     * @var null|EntityInterface|array utilisateur connecté
     */
    protected $user = [];
    /**
     * @var null|int id de l'utilisateur connecté
     */
    protected $userId = null;
    /**
     * @var null|EntityInterface entité de l'utilisateur connecté
     */
    protected $orgEntity = [];
    /**
     * @var null|int id de l'entité de l'utilisateur connecté
     */
    protected $orgEntityId = null;
    /**
     * @var null|EntityInterface service d'archives de l'utilisateur connecté
     */
    protected $archivalAgency = [];
    /**
     * @var null|int id du service d'archives de l'utilisateur connecté
     */
    protected $archivalAgencyId = null;
    /**
     * @var bool vrai si l'utilisateur connecté a un role code SAA
     */
    protected $superArchivist = false;
    /**
     * @var bool vrai si l'utilisateur connecté a un role code ADMIN
     */
    protected $adminTenant = false;
    /**
     * @var bool vrai si l'utilisateur connecté est un administrateur technique
     */
    protected $adminTech = false;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     */
    public function initialize(): void
    {
        // Note: nécessaire pour éviter des connexions postgres qui ne se ferment pas (origine du problème non trouvée)
        if (!ob_get_level()) {
            ob_start();
        }
        parent::initialize();
        $request = $this->getRequest();

        /** @use RequestHandlerComponent */
        $this->handleSessionReset();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash', ['duplicate' => false]);
        $this->loadComponent('Authentication.Authentication');
        /** @use AuthorizationComponent */
        $this->loadComponent('AsalaeCore.Authorization');
        /** @use RestComponent */
        $this->loadComponent('AsalaeCore.Rest');
        $this->Rest->setApiActions(static::getApiActions());
        /** @var AuthenticationService $authenticationService */
        $authenticationService = $this->getRequest()->getAttribute('authentication');
        $authUrl = $authenticationService
            ? $authenticationService->getConfig('unauthenticatedRedirect')
            : '';
        $this->set('authUrl', $authUrl);

        /** @var Identity $identity */
        $identity = $this->getRequest()->getAttribute('identity');
        $user = $identity?->getOriginalData();
        $connected = $user && Hash::get($user, 'id');
        $this->adminTech = $user && Hash::get($user, 'admin');
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');

        // Permet l'accès à l'application sans page de login (permet de profiter des outils d'analyse en ligne
        if (Configure::read("Auth.disabled") && !$connected) {
            /** @var UsersTable $Users */
            $Users = $this->fetchTable('Users');
            $query = $Users->find();
            $Users->setGenericUserContain($query);
            $user = $query->first();
            if ($user) {
                $connected = true;
                $AuthenticationComponent->setIdentity($user);
            }
        }

        // Evite des bugs dans les acls si action en uppercase
        $action = $request->getParam('action');
        if ($action && !in_array($action, get_class_methods($this))) {
            throw new MissingActionException(
                [
                    'controller' => $this->getName() . "Controller",
                    'action' => $request->getParam('action'),
                    'prefix' => $request->getParam('prefix') ?: '',
                    'plugin' => $request->getParam('plugin'),
                ]
            );
        }

        // donne aux reponses ajax l'état de la connexion et le layout
        if ($this->getRequest()->is('ajax')) {
            $this->setResponse(
                $this->getResponse()->withHeader(
                    'X-Connected',
                    $connected ? 'true' : 'false'
                )
            );
            if ($request->getHeaderLine('No-Layout')) {
                $this->viewBuilder()->setLayout('no-layout');
            }
        }

        // FIXME placer dans un middleware
        $this->setResponse(
            $this->getResponse()->withHeader(
                'X-Frame-Options',
                Configure::read('AppController.xFrameOptions', 'deny')
            )
        );

        if ($request->is('json')) {
            // erreurs récupérés dans $this->render()
            ErrorTrap::disableErrorOutput();
        }

        if ($connected) {
            /** @var UsersTable $Users */
            $Users = $this->fetchTable('Users');
            $this->user = !$user instanceof EntityInterface
                ? $Users->getUserEntityFromArrayObject($user)
                : $user;
            $this->userId = Hash::get($this->user, 'id');
            $this->orgEntity = Hash::get($this->user, 'org_entity', []);
            $this->orgEntityId = Hash::get($this->user, 'org_entity_id');
            $this->superArchivist = Hash::get($this->user, 'role.code') === RolesTable::CODE_SUPER_ARCHIVIST;
            $this->adminTenant = Hash::get($this->user, 'role.code') === RolesTable::CODE_ADMIN;
            $this->archivalAgency = Hash::get($this->orgEntity, 'archival_agency', []);
            $this->archivalAgencyId = Hash::get($this->archivalAgency, 'id');

            // Changement de service d'archives pour les super archivistes
            $sessionArchivalAgency = $request->getSession()->read('Auth.org_entity.archival_agency');
            if ($sessionArchivalAgency && Hash::get($sessionArchivalAgency, 'id') != $this->archivalAgencyId) {
                $this->archivalAgency = $sessionArchivalAgency;
                $this->archivalAgencyId = Hash::get($this->archivalAgency, 'id');
            }

            // Donne la liste des services d'archives accessibles par l'utilisateur et/ou défini archivalAgency
            $archivalAgenciesOpts = $this->fetchTable('OrgEntities')->find('list')
                ->innerJoinWith('TypeEntities')
                ->where(['TypeEntities.code' => 'SA'])
                ->orderByAsc('OrgEntities.name');
            if ($this->superArchivist) {
                $agencies = $this->fetchTable('SuperArchivistsArchivalAgencies')
                    ->find('list', valueField: 'archival_agency_id', keyField: 'archival_agency_id')
                    ->where(['user_id' => $this->userId])
                    ->orderBy(['archival_agency_id'])
                    ->toArray();
                if ($agencies) {
                    $archivalAgenciesOpts->where(['OrgEntities.id IN' => $agencies]);
                    if (!$this->archivalAgency || $this->archivalAgencyId === $this->orgEntityId) {
                        $mainArchivalAgency = $this->fetchTable('OrgEntities')->find()
                            ->where(['is_main_archival_agency' => true])
                            ->contain(['Configurations'])
                            ->first();
                        if ($mainArchivalAgency && in_array($mainArchivalAgency->id, $agencies)) {
                            $this->setArchivalAgency($mainArchivalAgency);
                        } else {
                            $this->setArchivalAgency($this->fetchTable('OrgEntities')->get(current($agencies)));
                        }
                    }
                } else {
                    $archivalAgenciesOpts->where(['1 = 0']);
                }
            } else {
                $archivalAgenciesOpts->where(['OrgEntities.id' => $this->archivalAgencyId ?: 0]);
            }
            $this->set('archivalAgenciesOpts', $archivalAgenciesOpts);
            // juste pour les tests
            if ($this->components()->has('Seal')) {
                $this->components()->unload('Seal');
            }

            // cookies
            /** @use JstableComponent */
            $this->loadComponent('AsalaeCore.Jstable');
            $this->Jstable->saveCookies();

            Configure::write(
                'ConfigArchivalAgency',
                $this->getRequest()->getSession()->read('ConfigArchivalAgency')
            );

            $this->set(
                'userCanSeeIndicators',
                IndicatorsController::userCanSeeIndicators($this->user, $this->orgEntity)
            );

            EventLogsTable::$selectionnedUserId = $this->userId;
            EventLogsTable::$selectionnedArchivalAgencyId = $this->archivalAgencyId;
        }
        $this->loadComponent(
            'Seal',
            [
                'user' => $this->user,
                'userId' => $this->userId,
                'orgEntity' => $this->orgEntity,
                'orgEntityId' => $this->orgEntityId,
                'archivalAgency' => $this->archivalAgency,
                'archivalAgencyId' => $this->archivalAgencyId,
                'superArchivist' => $this->superArchivist,
                'adminTech' => $this->adminTech,
            ]
        );

        // alerte espace disque faible (< 1Go)
        if (!is_dir(Configure::read('App.paths.data'))) {
            mkdir(Configure::read('App.paths.data'), 0777, true);
        }
        if (
            !$request->is('ajax')
            && (disk_free_space(ROOT) < 1000000000
            || disk_free_space(
                Configure::read('App.paths.data')
            ) < 1000000000
            || disk_free_space(sys_get_temp_dir()) < 1000000000)
        ) {
            $this->Flash->error(
                __("Espace disque faible, veuillez contacter un administrateur")
            );
        }
        /*
         * Enable the following components for recommended CakePHP security settings.
         * see http://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
        // FIXME placer dans un middleware
        // l'ip de l'utilisateur sera celle du proxy configuré
        if ($proxy = Configure::read('Proxy.host')) {
            $request->setTrustedProxies([$proxy]);
        }

        $publish = ['pong'];
        if ($this->adminTech) {
            $publish[] = 'user_admin_admin';
            $publish[] = 'close_user_admin_admin';
        } elseif ($this->userId) {
            $token = $this->getRequest()->getSession()->read('Session.token');
            $publish[] = 'user_' . $this->userId . '_' . $token;
            $publish[] = 'close_user_' . $this->userId . '_' . $token;
        }
        /** @see MercureComponent */
        $this->loadComponent('Mercure', ['publish' => $publish]);
    }

    /**
     * Surcharge $this->set afin de garder en mémoire les variables
     * dans $this->immutableViewVars
     * @param array|string $name
     * @param null         $value
     */
    public function set($name, $value = null)
    {
        if (is_array($name)) {
            if (is_array($value)) {
                $data = array_combine($name, $value);
            } else {
                $data = $name;
            }
        } else {
            $data = [$name => $value];
        }
        foreach ($data as $key => $value) {
            $this->immutableViewVars[$key] = $value;
        }
        parent::set($data);
    }

    /**
     * Modifi en session, le service d'archives
     * @param array|EntityInterface $archivalAgency
     * @return void
     */
    protected function setArchivalAgency($archivalAgency)
    {
        $this->archivalAgency = $archivalAgency;
        $this->archivalAgencyId = Hash::get($archivalAgency, 'id');
        if ($this->orgEntity instanceof EntityInterface) {
            $this->orgEntity->set('archival_agency', $archivalAgency);
        } else {
            $this->orgEntity['archival_agency'] = $archivalAgency;
        }
        if ($this->user instanceof EntityInterface) {
            $this->user->set('org_entity', $this->orgEntity);
        } else {
            $this->user['org_entity'] = $this->orgEntity;
        }
        $this->getRequest()->getSession()->write('Auth', $this->user);
    }

    /**
     * Before render callback.
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse|null|void
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeRender(EventInterface $event)
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $action = Inflector::underscore($request->getParam('action', ''));
        if ($action === 'api' && $request->getParam('pass.0')) {
            $action = Inflector::underscore($request->getParam('pass.0'));
        }
        $type = $this->getResponse()->getType();
        [, $subDir] = explode('/', $type);
        $templates = Configure::read(
            'App.paths.templates.0',
            ROOT . DS . 'templates' . DS
        );
        if ($request->is('ajax') && !$this->viewBuilder()->getLayout()) {
            $this->viewBuilder()->setLayout('ajax');
        }
        if (
            in_array($type, ['application/json', 'application/xml'])
            && !file_exists(
                $templates . $this->name . '/' . $subDir . '/' . $action . '.php'
            )
        ) {
            $this->set('viewVars', $this->immutableViewVars);
            $this->viewBuilder()
                ->setTemplatePath('default')
                ->setTemplate('default');
        }

        $notifications = [];
        try {
            $adminData = (array)$session->read('Admin');
            $highContrast = $session->read('Auth.high_contrast');
        } catch (DatabaseException) {
            $adminData = [];
            $highContrast = false;
        }
        $username = Hash::get($this->user ?: [], 'username');
        $path = Configure::read('App.paths.administrators_json');

        if ($this->userId && !$request->is('ajax')) {
            // evite une double exception lors de l'affichage d'erreur en cas d'exception dans les tests
            try {
                $results = $this->fetchTable('Notifications')
                    ->find()
                    ->where(['user_id' => $this->userId])
                    ->orderBy(['id' => 'asc'])
                    ->limit(101)
                    ->all();
                $notifications = $results->toArray();
            } catch (Exception) {
            }

            // Menu perso
            $this->set(
                'userMenu',
                (array)json_decode($session->read('Auth.menu'), true)
            );
        } elseif (Hash::get($adminData, 'tech') && is_writable($path)) {
            // cookies
            /** @use CookieComponent */
            $this->loadComponent('AsalaeCore.Cookie');
            $this->Cookie->setConfig('encryption', false);
            $cookies = $request->getCookieParams();
            unset($cookies['PHPSESSID'], $cookies['XDEBUG_SESSION']);
            foreach (array_keys($cookies) as $cookie) {
                $this->Cookie->delete($cookie);
            }

            $oldCookies = Hash::get($adminData, 'data.cookies', []);
            $newCookies = array_filter(
                array_merge($oldCookies, $cookies),
                function ($v) {
                    return !empty(json_decode($v));
                }
            );
            $session->write('Admin.data.cookies', $newCookies);
            $json = json_encode($newCookies);
            $this->cookies = $newCookies;
            if ($request->getHeaderLine('Accept') !== 'application/json') {
                $this->set('cookies', $json);
            }

            $path = Configure::read('App.paths.administrators_json');
            $admins = json_decode(file_get_contents($path)) ?: [];
            $dirty = false;
            foreach ($admins as $values) {
                if (
                    isset($values->username)
                    && Hash::get($adminData, 'data.username') === $values->username
                    && (!isset($values->cookies)
                    || json_encode($values->cookies) !== json_encode($newCookies))
                ) {
                    $values->cookies = $newCookies;
                    $dirty = true;
                    break;
                }
            }
            if ($dirty) {
                /** @var Filesystem $Filesystem */
                $Filesystem = Utility::get('Filesystem');
                try {
                    $Filesystem->begin();
                    $Filesystem->setSafemode(true);
                    $Filesystem->dumpFile(
                        $path,
                        json_encode($admins, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
                    );
                    $Filesystem->commit();
                } catch (Exception) {
                    $Filesystem->rollback();
                }
            }
        }
        $this->set('user_id', $this->userId);
        $this->set('username', $username);
        $this->set('saId', $this->archivalAgencyId);
        $this->set('superArchivist', $this->superArchivist);
        $this->set('notifications', json_encode($notifications));
        $this->set('high_contrast', $highContrast);

        $viewBuilder = $this->viewBuilder();
        if (!$viewBuilder->getVar('pageTitle')) {
            $traductionHelper = new TranslateHelper(new View());
            $trad = $traductionHelper->permission(
                $request->getParam('controller') ?: 'Home',
                $request->getParam('action') === 'index'
                    ? null
                    : $request->getParam('action')
            );
            $this->set('pageTitle', $trad);
        }

        return null;
    }

    /**
     * Juste pour l'IDE
     * @return CoreResponse
     */
    public function getCoreResponse(): CoreResponse
    {
        if ($this->response instanceof CoreResponse) {
            return $this->response;
        }
        throw new GenericException("Bad response class");
    }

    /**
     * Handles the session reset mechanism if a resetSession cookie is identified and valid.
     *
     * This method reads the resetSession cookie, verifies the associated user,
     * and if valid, triggers a session reset using the provided utility. It ensures
     * that session integrity is maintained and avoids conflicts in cookie encryption.
     *
     * @return void
     * @throws Exception
     */
    private function handleSessionReset(): void
    {
        $request = $this->getRequest();
        /** @var CookieComponent $Cookie */
        $Cookie = $this->loadComponent('AsalaeCore.Cookie');
        $this->components()->unload('Cookie'); // évite un conflit d'encryption
        $Cookie->configKey('resetSession', ['encryption' => 'aes']);
        $resetSession = $Cookie->read('resetSession') ?: [];
        /** @var Identity $identity */
        $identity = $this->getRequest()->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        $user_id = Hash::get($user ?: [], 'id');
        if ($resetSession && $resetSession['user_id'] === $user_id) {
            $valid = Utility::get('Session')->doReset(
                $request->getSession(),
                $resetSession['message']
            );
            if ($valid && $resetSession['super_archivist'] && $resetSession['last_archival_agency_id']) {
                $this->userId = $resetSession['user_id'];
                $this->changeAppLoggedArchivalAgency($resetSession['last_archival_agency_id']);
            }
            $cookie = $request->getCookieCollection()->get('resetSession');
            $response = $this->getResponse()->withExpiredCookie($cookie);
            $this->setResponse($response);
        }
        $cookies = $request->getCookieParams();
        unset($cookies['resetSession']);
        $this->setRequest($request->withCookieParams($cookies));
    }

    /**
     * Sélectionne le service d'archives de l'utilisateur (pour le super archiviste)
     * @param string $archivalAgencyId
     * @return void
     */
    protected function changeAppLoggedArchivalAgency(string $archivalAgencyId)
    {
        $auth = $this->getRequest()->getSession()->read('Auth');
        $orgEntity = Hash::get($auth, 'org_entity');
        $agencies = $this->fetchTable('SuperArchivistsArchivalAgencies')
            ->find('list', ['valueField' => 'archival_agency_id', 'keyField' => 'archival_agency_id'])
            // TODO 3.0 ->find('list', valueField: 'archival_agency_id', keyField: 'archival_agency_id')
            ->where(['user_id' => $this->userId])
            ->toArray();
        if (empty($agencies) || !in_array($archivalAgencyId, $agencies)) {
            throw new ForbiddenException();
        }
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $archivalAgency = $OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'OrgEntities.id' => $archivalAgencyId,
                    'TypeEntities.code' => 'SA',
                ]
            )
            ->contain(['Configurations'])
            ->firstOrFail();
        if ($orgEntity instanceof EntityInterface) {
            $orgEntity->set('archival_agency', $archivalAgency);
        } else {
            $orgEntity['archival_agency'] = $archivalAgency;
        }
        if ($auth instanceof EntityInterface) {
            $auth->set('org_entity', $orgEntity);
        } else {
            $auth['org_entity'] = $orgEntity;
        }
        $this->getRequest()->getSession()->write('Auth', $auth);
    }
}
