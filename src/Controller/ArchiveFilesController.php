<?php

/**
 * Asalae\Controller\ArchiveFilesController
 */

namespace Asalae\Controller;

use Asalae\Model\Entity\ArchiveFile;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\EventLogsTable;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Http\CallbackStream;
use AsalaeCore\Utility\Notify;
use Cake\Datasource\EntityInterface;
use Cake\Http\Response;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Exception;
use Throwable;

/**
 * ArchiveFiles
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveFilesTable ArchiveFiles
 * @property ArchivesTable     Archives
 * @property EventLogsTable    EventLogs
 */
class ArchiveFilesController extends AppController
{
    /**
     * Téléchargement des fichiers d'archives (description ou lifecycle)
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function download(string $id)
    {
        /** @var ArchiveFile $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('ArchiveFiles')
            ->find('seal')
            ->innerJoinWith('Archives')
            ->where(['ArchiveFiles.id' => $id])
            ->contain(
                [
                    'StoredFiles',
                    'Archives' => [
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                    ],
                ]
            )
            ->firstOrFail();

        $archive_id = $entity->get('archive_id');
        $this->Seal->hierarchicalView()
            ->table('Archives')
            ->find('seal')
            ->innerJoinWith('ArchivalAgencies')
            ->innerJoinWith('TransferringAgencies')
            ->innerJoinWith('OriginatingAgencies')
            ->where(['Archives.id' => $archive_id])
            ->firstOrFail();

        if ($sessionFile = $this->getRequest()->getQuery('sessionFile')) {
            Notify::emit(
                'download_file',
                [
                    'uuid' => $sessionFile,
                    'state' => 'downloading',
                    'last_update' => time(),
                ]
            );
        }

        $response = $this->getResponse()
            ->withHeader('Content-Description', 'File Transfer')
            ->withHeader('Content-Type', 'application/xml')
            ->withHeader(
                'Content-Length',
                Hash::get($entity, 'stored_file.size')
            )
            ->withHeader(
                'File-Size',
                Hash::get($entity, 'stored_file.size')
            )
            ->withHeader(
                'Content-Disposition',
                'attachment; filename="' . $entity->get('filename') . '"'
            )
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate')
            ->withHeader('Pragma', 'public')
            ->withBody(
                new CallbackStream(
                    function () use ($entity, $sessionFile) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        $success = $entity->readfile();
                        if ($sessionFile) {
                            Notify::emit(
                                'download_file',
                                [
                                    'uuid' => $sessionFile,
                                    'state' => 'finished',
                                    'last_update' => time(),
                                    'integrity' => $success,
                                ]
                            );
                        }
                    }
                )
            );
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        $entry = $EventLogs->newEntry(
            'download_' . Inflector::singularize($this->getName()),
            'success',
            __(
                "Téléchargement du fichier ''{0}'' par l'utilisateur ''{1}''",
                $entity->get('filename'),
                Hash::get($this->user, 'name')
                    ?: Hash::get(
                        $this->user,
                        'username'
                    )
            ),
            $this->userId,
            $entity
        );
        $entry->set('in_lifecycle', false);
        $EventLogs->saveOrFail($entry);
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        /** @var EntityInterface $archive */
        $storedFile = Hash::get(
            $entity,
            'archive.lifecycle_xml_archive_file.stored_file',
            []
        );
        try {
            $Archives->updateLifecycleXml($storedFile, $entry, true);
        } catch (Throwable $e) {
            throw new VolumeException(__("Echec lors de la mise à jour du cycle de vie"), $e->getCode(), $e);
        }

        return $response;
    }
}
