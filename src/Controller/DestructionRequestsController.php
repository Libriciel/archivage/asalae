<?php

/**
 * Asalae\Controller\DestructionRequestsController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Entity\DestructionRequest;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsDestructionRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\DestructionRequestsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationProcessesTable;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\CallbackStream;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use DateInvalidTimeZoneException;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Psr\Http\Message\ResponseInterface;
use Seda2Pdf\Seda2Pdf;
use ZMQSocketException;

/**
 * DestructionRequests
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveUnitsDestructionRequestsTable ArchiveUnitsDestructionRequests
 * @property ArchiveUnitsTable                    ArchiveUnits
 * @property ArchivesTable                        Archives
 * @property CountersTable                        Counters
 * @property DestructionRequestsTable             DestructionRequests
 * @property OrgEntitiesTable                     OrgEntities
 * @property ValidationChainsTable                ValidationChains
 * @property ValidationProcessesTable             ValidationProcesses
 */
class DestructionRequestsController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     * @var array
     */
    public array $paginate = [
        'order' => [
            'DestructionRequests.id' => 'desc',
        ],
    ];

    /**
     * Liste les demandes d'élimination en préparation
     */
    public function indexPreparating()
    {
        $query = $this->Seal->archivalAgency()
            ->table('DestructionRequests')
            ->find('seal')
            ->where(
                [
                    'DestructionRequests.created_user_id' => $this->userId,
                    'DestructionRequests.state' => DestructionRequestsTable::S_CREATING,
                ]
            );
        $this->indexCommons($query);
    }

    /**
     * Liste les enregistrements
     * @param Query $query
     * @return Response|void
     * @throws Exception
     */
    private function indexCommons(Query $query)
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');

        $query->contain(
            [
                'CreatedUsers' => ['OrgEntities'],
                'DestructionNotifications',
            ]
        );

        /** @var DestructionRequestsTable $DestructionRequests */
        $DestructionRequests = $this->fetchTable('DestructionRequests');
        $subquery = $DestructionRequests->selectQuery()
            ->select(['dr.id'])
            ->from(['dr' => 'destruction_requests'])
            ->innerJoin(
                ['lk' => 'archive_units_destruction_requests'],
                [
                    'lk.destruction_request_id' => new IdentifierExpression(
                        'dr.id'
                    ),
                ]
            )
            ->innerJoin(
                ['au' => 'archive_units'],
                ['au.id' => new IdentifierExpression('lk.archive_unit_id')]
            )
            ->where(
                ['dr.id' => new IdentifierExpression('DestructionRequests.id')]
            )
            ->limit(1);

        $IndexComponent->setQuery($query)
            ->filter(
                'archive_unit_identifier',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike(
                            'au.archival_agency_identifier',
                            $v
                        )
                    );
                    return $q->andWhere(['DestructionRequests.id IN' => $sq]);
                }
            )
            ->filter(
                'archive_unit_name',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike('au.name', $v)
                    );
                    return $q->andWhere(['DestructionRequests.id IN' => $sq]);
                }
            )
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter(
                'created_user',
                IndexComponent::FILTER_IN,
                'DestructionRequests.created_user_id'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('state', IndexComponent::FILTER_IN);

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/commons');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) use ($ArchiveUnits) {
                    // effectue un contain avec limite individuelle d'enregistrements
                    $aus = $ArchiveUnits->find()
                        ->innerJoinWith('ArchiveUnitsDestructionRequests')
                        ->where(['destruction_request_id' => $e->id])
                        ->orderBy(['archival_agency_identifier'])
                        ->limit(10)
                        ->toArray();
                    $e->set('archive_units', $aus);
                    return $e;
                }
            )
            ->toArray();
        $this->set('data', $data);

        // override $IndexComponent->init()
        $viewBuilder = $this->viewBuilder();
        if (
            $this->getRequest()->is('ajax') && $viewBuilder->getLayout() === null
        ) {
            $viewBuilder->setTemplate('ajax_index');
        }

        // options
        $this->set('states', $DestructionRequests->options('state'));

        $created_users = $DestructionRequests->find(
            'list',
            keyField: '_matchingData.CreatedUsers.id',
            valueField: '_matchingData.CreatedUsers.username',
            groupField: '_matchingData.OrgEntities.name',
        )
            ->select(
                ['CreatedUsers.id', 'CreatedUsers.username', 'OrgEntities.name']
            )
            ->innerJoinWith('CreatedUsers')
            ->innerJoinWith('CreatedUsers.OrgEntities')
            ->distinct(
                $distinct = [
                    'OrgEntities.name',
                    'CreatedUsers.username',
                ]
            )
            ->where(
                ['DestructionRequests.archival_agency_id' => $this->archivalAgencyId]
            )
            ->orderBy($distinct);
        $this->set('created_users', $created_users);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * Liste de mes demandes d'élimination
     */
    public function indexMy()
    {
        $query = $this->Seal->archivalAgency()
            ->table('DestructionRequests')
            ->find('seal')
            ->where(['DestructionRequests.created_user_id' => $this->userId]);
        $this->indexCommons($query);
    }

    /**
     * Liste de mes demandes d'élimination
     */
    public function indexAll()
    {
        $type = Hash::get($this->orgEntity, 'type_entity.code');
        $query = $this->Seal->archivalAgency()
            ->table('DestructionRequests')
            ->find('seal');
        if ($type === 'CST') {
            $query->where(
                ['DestructionRequests.control_authority_id' => $this->orgEntityId]
            );
        } elseif ($type !== 'SA' && $type !== 'SE') {
            $query->where(
                ['DestructionRequests.originating_agency_id' => $this->orgEntityId]
            );
        }
        $this->indexCommons($query);
    }

    /**
     * Action d'ajout
     */
    public function add()
    {
        $request = $this->getRequest();
        /** @var ArchiveUnitsDestructionRequestsTable $ArchiveUnitsDestructionRequests */
        $ArchiveUnitsDestructionRequests = $this->fetchTable('ArchiveUnitsDestructionRequests');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        // on créé l'entrée dans le get avec les paramètres des recherche des archive_units dans le query
        if ($request->is('get')) {
            /** @var CountersTable $Counters */
            $Counters = $this->fetchTable('Counters');
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = $this->fetchTable('OrgEntities');
            $control = $OrgEntities->cst($this->archivalAgencyId);

            $units = [];
            $viewBuilder = $this->viewBuilder();
            $viewBuilder->setLayout('ajax');
            $query = $this->queryArchiveUnit()
                ->andWhere(['AppraisalRules.final_action_code' => 'destroy']);
            $auCount = 0;
            $originalCount = 0;
            $originalSize = 0;
            $ids = [];
            foreach ($query as $archiveUnit) {
                if (count($units) < 100) {
                    $units[] = $archiveUnit['archival_agency_identifier'] . ' - ' . $archiveUnit['name'];
                }
                $auCount++;
                $originalCount += $archiveUnit['original_total_count'];
                $originalSize += $archiveUnit['original_total_size'];
                $ids[] = $archiveUnit['id'];
            }
            if (count($units) === 0) {
                throw new BadRequestException(
                    __("Aucune unité d'archives supprimables actuellement")
                );
            }
            /** @var DestructionRequestsTable $DestructionRequests */
            $DestructionRequests = $this->fetchTable('DestructionRequests');
            $entity = $DestructionRequests->newEntity(
                [
                    'identifier' => $Counters->next(
                        $this->archivalAgencyId,
                        'ArchiveDestructionRequest'
                    ),
                    'comment' => __n(
                        "Demande d'élimination de l'unité d'archive par le {0} :\n- {1}",
                        "Demande d'élimination des unités d'archives par le {0} :\n- {1}",
                        count($units),
                        Hash::get($this->orgEntity, 'name'),
                        implode("\n- ", $units)
                    ),
                    'archival_agency_id' => $this->archivalAgencyId,
                    'originating_agency_id' => $request->getQuery(
                        'originating_agency_id.0'
                    ),
                    'control_authority_id' => $control->id,
                    'state' => $DestructionRequests->initialState,
                    'archive_units_count' => $auCount,
                    'original_count' => $originalCount,
                    'original_size' => $originalSize,
                    'created_user_id' => $this->userId,
                ]
            );
            // note: il peut y avoir +65535 unités d'archives -> impossible d'insérer directement
            $DestructionRequests->saveOrFail($entity);
            foreach ($ids as $archive_unit_id) {
                $ArchiveUnitsDestructionRequests->insertQuery()
                    ->insert(['archive_unit_id', 'destruction_request_id'])
                    ->values(
                        [
                            'archive_unit_id' => $archive_unit_id,
                            'destruction_request_id' => $entity->id,
                        ]
                    )
                    ->execute();
            }
        } else {
            $entity = $this->Seal->archivalAgency()
                ->table('DestructionRequests')
                ->find('seal')
                ->where(['DestructionRequests.id' => $request->getData('id')])
                ->contain(
                    [
                        'ArchiveUnits' => function (Query $q) {
                            return $q->limit(10);
                        },
                    ]
                )
                ->firstOrFail();
            $data = [
                'comment' => $request->getData('comment'),
            ];
            /** @var DestructionRequestsTable $DestructionRequests */
            $DestructionRequests = $this->fetchTable('DestructionRequests');
            $DestructionRequests->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($DestructionRequests->save($entity)) {
                $query = $ArchiveUnits->find()
                    ->innerJoinWith('ArchiveUnitsDestructionRequests')
                    ->where(
                        ['ArchiveUnitsDestructionRequests.destruction_request_id' => $entity->id]
                    );
                /** @var ArchivesTable $Archives */
                $Archives = $this->fetchTable('Archives');
                /** @var EntityInterface $unit */
                foreach ($query as $unit) {
                    $ArchiveUnits->transitionAll(
                        ArchiveUnitsTable::T_REQUEST_DESTRUCTION,
                        [
                            'lft >=' => $unit->get('lft'),
                            'rght <=' => $unit->get('rght'),
                        ]
                    );
                    if ($unit->get('parent_id') === null) {
                        $Archives->transitionAll(
                            ArchivesTable::T_REQUEST_DESTRUCTION,
                            ['id' => $unit->get('archive_id')]
                        );
                    }
                }
                $this->Modal->success();
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAction($entity);
                return $this->renderDataToJson($entity->toArray());
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $links = $ArchiveUnitsDestructionRequests->find()
            ->where(['destruction_request_id' => $entity->id])
            ->contain(
                [
                    'ArchiveUnits' => [
                        'Archives' => ['DescriptionXmlArchiveFiles'],
                        'ArchiveDescriptions',
                    ],
                ]
            )
            ->orderBy(['ArchiveUnits.archival_agency_identifier']);
        $this->set('unitsCount', $unitsCount = $links->count());
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $entities = $links->limit($AjaxPaginatorComponent->getConfig('limit'))
            ->toArray();
        /** @var EntityInterface[] $archive_units */
        $archive_units = [];
        foreach ($entities as $link) {
            $archiveUnit = $link->get('archive_unit');
            $archiveUnit->set('not_public', true);
            $archiveUnit->setVirtual(['description', 'dates']);
            $archive_units[] = $archiveUnit;
        }
        $this->set('units', $archive_units);

        $this->set('entity', $entity);
        $AjaxPaginatorComponent->setViewPaginator($archive_units, $unitsCount);
    }

    /**
     * Donne le query avec ses filtres
     * @return Query
     * @throws Exception
     */
    private function queryArchiveUnit()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index', ['model' => 'ArchiveUnits']);
        $query = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal')
            ->where(
                [
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                    'Archives.state' => ArchivesTable::S_AVAILABLE,
                    'ArchiveUnits.state' => ArchiveUnitsTable::S_AVAILABLE,
                    'ArchiveUnits.expired_dua_root' => true,
                ]
            )
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'Agreements',
                        'Profiles',
                        'OriginatingAgencies',
                        'TransferringAgencies',
                        'Transfers',
                    ],
                    'AppraisalRules',
                    'AccessRules' => ['AccessRuleCodes'],
                    'ArchiveDescriptions',
                ]
            );
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)) {
            $targetEntity = $this->archivalAgency;
        } else {
            $targetEntity = $this->orgEntity;
        }
        if ($typeEntity === 'SV') {
            $query->andWhere(
                [
                    'OR' => [
                        'Archives.transferring_agency_id' => $targetEntity->id,
                        'Archives.originating_agency_id' => $targetEntity->id,
                    ],
                ]
            );
        } elseif ($typeEntity !== 'SA' && $typeEntity !== 'SE') {
            $query->andWhere(
                ['Archives.originating_agency_id' => $targetEntity->id]
            );
        }
        $IndexComponent->setQuery($query)
            ->filter('agreement_id', null, 'Archives.agreement_id')
            ->filter(
                'appraisal_rule_end',
                IndexComponent::FILTER_DATEOPERATOR,
                'AppraisalRules.end_date'
            )
            ->filter('archival_agency_identifier', IndexComponent::FILTER_ILIKE)
            ->filter(
                'created',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.created'
            )
            ->filter(
                'favoris',
                function () {
                    $request = $this->getRequest();
                    $cookies = $request->getCookieParams();
                    $sessionCookies = $this->cookies;
                    if (is_array($sessionCookies)) {
                        $cookies = array_merge($cookies, $sessionCookies);
                    }
                    $cookies = array_filter($cookies, fn ($v) => $v !== '0');
                    $favoritesIds = [];
                    $regex = "/^table-favorite-archive-units-eliminable-table-(\d+)$/";
                    foreach (array_keys($cookies) as $cookieName) {
                        if (preg_match($regex, $cookieName, $match)) {
                            $favoritesIds[] = $match[1];
                        }
                    }
                    if (empty($favoritesIds)) {
                        return [];
                    }
                    return ['ArchiveUnits.id IN' => $favoritesIds];
                }
            )
            ->filter(
                'final_action_code',
                null,
                'AppraisalRules.final_action_code'
            )
            ->filter('id', IndexComponent::FILTER_IN)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter(
                'original_total_count',
                IndexComponent::FILTER_COUNTOPERATOR
            )
            ->filter('original_total_size', IndexComponent::FILTER_SIZEOPERATOR)
            ->filter(
                'originating_agency_id',
                null,
                'Archives.originating_agency_id'
            )
            ->filter('profile_id', null, 'Archives.profile_id')
            ->filter(
                'transferring_agency_id',
                null,
                'Archives.transferring_agency_id'
            );
        return $query;
    }

    /**
     * Action modifier
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var ArchiveUnitsDestructionRequestsTable $ArchiveUnitsDestructionRequests */
        $ArchiveUnitsDestructionRequests = $this->fetchTable('ArchiveUnitsDestructionRequests');
        $request = $this->getRequest();
        $entity = $this->Seal->archivalAgency()
            ->table('DestructionRequests')
            ->find('seal')
            ->where(['DestructionRequests.id' => $id])
            ->contain(
                [
                    'ArchiveUnits' => function (Query $q) {
                        return $q->limit(10);
                    },
                ]
            )
            ->firstOrFail();
        if ($request->is('put')) {
            $data = [
                'comment' => $request->getData('comment'),
            ];
            /** @var DestructionRequestsTable $DestructionRequests */
            $DestructionRequests = $this->fetchTable('DestructionRequests');
            $DestructionRequests->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($DestructionRequests->save($entity)) {
                $this->Modal->success();
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAction($entity);
                return $this->renderDataToJson($entity->toArray());
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $links = $ArchiveUnitsDestructionRequests->find()
            ->where(['destruction_request_id' => $entity->id])
            ->contain(
                [
                    'ArchiveUnits' => [
                        'Archives' => ['DescriptionXmlArchiveFiles'],
                        'ArchiveDescriptions',
                    ],
                ]
            )
            ->orderBy(['ArchiveUnits.archival_agency_identifier']);
        $this->set('unitsCount', $unitsCount = $links->count());
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $entities = $links->limit($AjaxPaginatorComponent->getConfig('limit'))
            ->toArray();
        /** @var EntityInterface[] $archive_units */
        $archive_units = [];
        foreach ($entities as $link) {
            $archiveUnit = $link->get('archive_unit');
            $archiveUnit->set('not_public', true);
            $archiveUnit->setVirtual(['description', 'dates']);
            $archive_units[] = $archiveUnit;
        }
        $this->set('units', $archive_units);
        $this->set('entity', $entity);
        $AjaxPaginatorComponent->setViewPaginator($archive_units, $unitsCount);
    }

    /**
     * Pagination des archive units dans la visualisation
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function paginateArchiveUnits(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $query = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal')
            ->innerJoinWith('ArchiveUnitsDestructionRequests')
            ->innerJoinWith('Archives')
            ->where(
                ['ArchiveUnitsDestructionRequests.destruction_request_id' => $id]
            );
        return $AjaxPaginatorComponent->json(
            $query,
            (bool)$this->getRequest()->getQuery('count')
        );
    }

    /**
     * Retire une unité d'archives d'une demande d'élimination
     * @param string $id
     * @param string $archive_unit_id
     * @return Response
     * @throws Exception
     */
    public function removeArchiveUnit(string $id, string $archive_unit_id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var ArchiveUnitsDestructionRequestsTable $ArchiveUnitsDestructionRequests */
        $ArchiveUnitsDestructionRequests = $this->fetchTable('ArchiveUnitsDestructionRequests');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $link = $this->Seal->archivalAgency()
            ->table('ArchiveUnitsDestructionRequests')
            ->find('seal')
            ->where(
                [
                    'ArchiveUnitsDestructionRequests.archive_unit_id' => $archive_unit_id,
                    'ArchiveUnitsDestructionRequests.destruction_request_id' => $id,
                ]
            )
            ->contain(['ArchiveUnits'])
            ->firstOrFail();

        $success = $ArchiveUnitsDestructionRequests->delete($link);
        $report = $success
            ? 'done'
            : 'Erreur lors de la suppression';

        if ($success) {
            $unit = $link->get('archive_unit');
            $ArchiveUnits->transitionAll(
                ArchiveUnitsTable::T_CANCEL,
                ['lft >=' => $unit->get('lft'), 'rght <=' => $unit->get('rght')]
            );
            if ($unit->get('parent_id') === null) {
                $Archives->transitionAll(
                    ArchivesTable::T_CANCEL,
                    ['id' => $unit->get('archive_id')]
                );
            }

            /** @var DestructionRequestsTable $DestructionRequests */
            $DestructionRequests = $this->fetchTable('DestructionRequests');
            $destruction = $DestructionRequests->get($id);
            $destruction->set(
                'archive_units_count',
                $destruction->get('archive_units_count') - 1
            );
            $destruction->set(
                'original_count',
                $destruction->get('original_count') - $unit->get('original_total_count')
            );
            $destruction->set(
                'original_size',
                $destruction->get('original_size') - $unit->get('original_total_size')
            );
            $DestructionRequests->save($destruction);
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Action supprimer
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Seal->archivalAgency()
            ->table('DestructionRequests')
            ->find('seal')
            ->where(['DestructionRequests.id' => $id])
            ->firstOrFail();
        if (!$entity->get('deletable')) {
            throw new ForbiddenException();
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $ArchiveUnits->find()
            ->innerJoinWith('ArchiveUnitsDestructionRequests')
            ->where(
                ['ArchiveUnitsDestructionRequests.destruction_request_id' => $entity->id]
            );
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        /** @var EntityInterface $unit */
        foreach ($query as $unit) {
            $ArchiveUnits->transitionAll(
                ArchiveUnitsTable::T_CANCEL,
                ['lft >=' => $unit->get('lft'), 'rght <=' => $unit->get('rght')]
            );
            if ($unit->get('parent_id') === null) {
                $Archives->transitionAll(
                    ArchivesTable::T_CANCEL,
                    ['id' => $unit->get('archive_id')]
                );
            }
        }

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($entity);
        /** @var DestructionRequestsTable $DestructionRequests */
        $DestructionRequests = $this->fetchTable('DestructionRequests');
        $report = $DestructionRequests->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Envoi de la demande d'élimination
     * @param string $id
     * @throws ZMQSocketException
     * @throws DateInvalidTimeZoneException
     */
    public function send(string $id)
    {
        /** @var DestructionRequest $destructionRequest */
        $destructionRequest = $this->Seal->archivalAgency()
            ->table('DestructionRequests')
            ->find('seal')
            ->where(['DestructionRequests.id' => $id])
            ->contain(['ArchivalAgencies', 'OriginatingAgencies'])
            ->firstOrFail();
        /** @var DestructionRequestsTable $DestructionRequests */
        $DestructionRequests = $this->fetchTable('DestructionRequests');
        if (
            !$DestructionRequests->transition(
                $destructionRequest,
                DestructionRequestsTable::T_SEND
            )
        ) {
            throw new ForbiddenException();
        }
        /** @var Notify $Notify */
        $Notify = Utility::get('Notify');

        $request = $this->getRequest();
        $session = $request->getSession();

        $validationChain = $session->read(
            'ConfigArchivalAgency.destruction-request-chain'
        ) ?: 'default';
        if ($validationChain === 'default') {
            /** @var ValidationChainsTable $ValidationChains */
            $ValidationChains = $this->fetchTable('ValidationChains');
            $validationChain = $ValidationChains->find(
                'default',
                app_type: ValidationChainsTable::ARCHIVE_DESTRUCTION_REQUEST,
                org_entity_id: $this->archivalAgencyId,
            )->firstOrFail()->get('id');
        }

        $params = [
            'user_id' => $this->userId,
            'org_entity_id' => $this->archivalAgencyId,
            'validation_chain_id' => $validationChain,
        ];
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $process = $ValidationProcesses->createNewProcess(
            $destructionRequest,
            $params
        );
        $conn = $DestructionRequests->getConnection();
        $conn->begin();
        $success = $ValidationProcesses->save($process)
            && $DestructionRequests->save($destructionRequest);
        $xml = $destructionRequest->generateXml();
        Filesystem::dumpFile($destructionRequest->get('xml'), $xml);
        if ($success) {
            $ValidationProcesses->notifyProcess($process);
            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logValidationSend($destructionRequest);
            $conn->commit();
            $message = __(
                "La demande d'élimination {0} a bien été envoyée.",
                $destructionRequest->get('identifier')
            );
            $notifyClass = 'alert-warning';
        } else {
            $conn->rollback();
            $errors = FormatError::logEntityErrors($destructionRequest);
            $message = __(
                "Une erreur a eu lieu lors de l'envoi de la demande d'élimination {0} : {1}",
                $destructionRequest->get('identifier'),
                $errors
            );
            $notifyClass = 'alert-danger';
        }
        $Notify->send(
            $this->userId,
            [$session->read('Session.token')],
            $message,
            $notifyClass
        );
        if (!$success) {
            throw new BadRequestException();
        }
    }

    /**
     * Visualisation d'une demande d'élimination
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $destructionRequest = $this->Seal->archivalAgency()
            ->table('DestructionRequests')
            ->find('seal')
            ->where(['DestructionRequests.id' => $id])
            ->contain(
                [
                    'ArchivalAgencies',
                    'OriginatingAgencies',
                    'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q->orderBy(
                            ['ArchiveUnits.archival_agency_identifier']
                        )
                            ->limit($AjaxPaginatorComponent->getConfig('limit'))
                            ->contain(
                                [
                                    'Archives' => ['DescriptionXmlArchiveFiles'],
                                    'ArchiveDescriptions',
                                ]
                            );
                    },
                    'CreatedUsers',
                    'ValidationProcesses' => [
                        'ValidationChains',
                        'CurrentStages' => [
                            'ValidationActors',
                        ],
                        'ValidationHistories' => [
                            'sort' => [
                                'ValidationHistories.created' => 'asc',
                                'ValidationHistories.id' => 'asc',
                            ],
                            'ValidationActors' => [
                                'Users',
                                'ValidationStages',
                            ],
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        /** @var EntityInterface[] $archive_units */
        $archive_units = Hash::get($destructionRequest, 'archive_units');
        foreach ($archive_units as $archiveUnit) {
            $archiveUnit->set('not_public', true);
            $archiveUnit->setVirtual(['description', 'dates']);
        }
        $this->set('destructionRequest', $destructionRequest);
        /** @var ArchiveUnitsDestructionRequestsTable $ArchiveUnitsDestructionRequests */
        $ArchiveUnitsDestructionRequests = $this->fetchTable('ArchiveUnitsDestructionRequests');
        $unitsCount = $ArchiveUnitsDestructionRequests->find()
            ->where(['destruction_request_id' => $id])
            ->count();
        $this->set('unitsCount', $unitsCount);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($destructionRequest);
        $AjaxPaginatorComponent->setViewPaginator($destructionRequest->get('archive_units') ?: [], $unitsCount);
    }

    /**
     * Donne le fichier PDF lié à un transfert
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function downloadPdf(string $id): Response
    {
        /** @var DestructionRequest $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('DestructionRequests')
            ->find('seal')
            ->where(['DestructionRequests.id' => $id])
            ->firstOrFail();
        $xml = $entity->get('xml');
        $generator = new Seda2Pdf($xml);
        $pdf = tempnam(sys_get_temp_dir(), uniqid('seda2pdf-'));
        $generator->generate($pdf);

        if ($sessionFile = $this->getRequest()->getQuery('sessionFile')) {
            Notify::emit(
                'download_file',
                [
                    'uuid' => $sessionFile,
                    'state' => 'downloading',
                    'last_update' => time(),
                ]
            );
        }

        return $this->getResponse()
            ->withHeader('Content-Type', 'application/pdf')
            ->withHeader('Content-Length', filesize($pdf))
            ->withHeader('File-Size', filesize($pdf))
            ->withHeader(
                'Content-Disposition',
                'attachment; filename="' . $entity->get('pdf_basename') . '"'
            )
            ->withBody(
                new CallbackStream(
                    function () use ($pdf, $sessionFile) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        readfile($pdf);
                        unlink($pdf);
                        if ($sessionFile) {
                            Notify::emit(
                                'download_file',
                                [
                                    'uuid' => $sessionFile,
                                    'state' => 'finished',
                                    'last_update' => time(),
                                    'integrity' => true,
                                ]
                            );
                        }
                    }
                )
            );
    }
}
