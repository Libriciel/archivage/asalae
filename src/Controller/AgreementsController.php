<?php

/**
 * Asalae\Controller\AgreementsController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Table\AgreementsTable;
use Asalae\Model\Table\FileExtensionsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\ProfilesTable;
use Asalae\Model\Table\PronomsTable;
use Asalae\Model\Table\ServiceLevelsTable;
use Asalae\Model\Table\ValidationActorsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationStagesTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use Cake\Datasource\EntityInterface;
use Cake\Http\Response;
use Cake\ORM\Table;
use Exception;

/**
 * Accords de versement
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AgreementsTable       Agreements
 * @property FileExtensionsTable   FileExtensions
 * @property OrgEntitiesTable      OrgEntities
 * @property ProfilesTable         Profiles
 * @property PronomsTable          Pronoms
 * @property ServiceLevelsTable    ServiceLevels
 * @property ValidationActorsTable ValidationActors
 * @property ValidationChainsTable ValidationChains
 * @property ValidationStagesTable ValidationStages
 */
class AgreementsController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'agreements-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'Agreements.id' => 'asc',
        ],
    ];

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default' => [
                'post' => [
                    'function' => 'apiAdd',
                ],
            ],
        ];
    }

    /**
     * Liste des Profils
     *
     */
    public function index()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        $query = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('seal')
            ->contain(
                [
                    'ProperChains',
                    'ImproperChains',
                    'TransferringAgencies',
                    'OriginatingAgencies',
                    'ServiceLevels',
                    'Profiles',
                ]
            );
        $IndexComponent->setQuery($query)
            ->filter('active', IndexComponent::FILTER_IS)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('default_agreement', IndexComponent::FILTER_IS)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('modified', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('name', IndexComponent::FILTER_ILIKE);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) {
                    $e->setVirtual(['deletable']);
                    return $e;
                }
            )
            ->toArray();
        $this->set('data', $data);
        $this->setValidationChainsOptions();
        $sl = $this->Seal->archivalAgency()
            ->table('ServiceLevels')
            ->find('sealList')
            ->where(['ServiceLevels.active' => true])
            ->orderBy(['ServiceLevels.name' => 'asc']);
        $this->set('service_levels', $sl);
        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('sealList')
            ->where(['Profiles.active' => true])
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * set validation_chains_validate
     * set validation_chains_invalidate
     */
    private function setValidationChainsOptions()
    {
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $this->set(
            'validation_chains_validate',
            $ValidationChains->findListOptions(
                $this->archivalAgencyId,
                ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM
            )
        );
        $this->set(
            'validation_chains_invalidate',
            $ValidationChains->findListOptions(
                $this->archivalAgencyId,
                ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM
            )
        );
    }

    /**
     * Partie 1 de l'ajout d'un accord de versement
     * @throws Exception
     */
    public function add1()
    {
        /** @var AgreementsTable $Agreements */
        $Agreements = $this->fetchTable('Agreements');
        $countDefault = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('seal')
            ->where(['default_agreement' => true])
            ->count();
        $entity = $Agreements->newEntity(
            [
                'active' => true,
                'org_entity_id' => $this->archivalAgencyId,
                'default_agreement' => $countDefault === 0,
            ]
        );
        if ($this->getRequest()->is('post')) {
            $data = [
                'default_level' => $this->getRequest()->getData(
                    'default_level'
                ),
                'org_entity_id' => $entity->get('org_entity_id'),
                'identifier' => $this->getRequest()->getData('identifier'),
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'default_agreement' => $this->getRequest()->getData(
                    'default_agreement'
                ),
                'active' => $this->getRequest()->getData('active'),
                'auto_validate' => $this->getRequest()->getData(
                    'auto_validate'
                ),
                'proper_chain_id' => $this->getRequest()->getData(
                    'proper_chain_id'
                ),
                'improper_chain_id' => $this->getRequest()->getData(
                    'improper_chain_id'
                ),
                'date_begin' => $this->getRequest()->getData('date_begin'),
                'date_end' => $this->getRequest()->getData('date_end'),
                'mult_max_size_per_transfer' => pow(1024, 2), // Mo
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            /** @var AgreementsTable $Agreements */
            $Agreements = $this->fetchTable('Agreements');
            $Agreements->patchEntity($entity, $data);
            if (empty($entity->getErrors())) {
                $this->getRequest()->getSession()->write(
                    'Agreement.entity',
                    $entity
                );
                $this->Modal->success();
                $this->Modal->step('addAgreementStep2()');
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        } else {
            $default = [
                "auto_validate" => true,
            ];
            $Agreements->patchEntity($entity, $default);
        }
        $this->set('entity', $entity);

        // Options
        $this->setValidationChainsOptions();
    }

    /**
     * Partie 2 de l'ajout d'un accord de versement
     */
    public function add2()
    {
        /** @var AgreementsTable $Agreements */
        $Agreements = $this->fetchTable('Agreements');
        /** @var EntityInterface $entity */
        $entity = $this->getRequest()->getSession()->read('Agreement.entity');
        if (empty($entity)) { // en cas de deconnexion/reconnexion
            $entity = $Agreements->newEntity(
                [
                    'default_level' => $this->getRequest()->getData(
                        'default_level'
                    ),
                    'org_entity_id' => $this->archivalAgencyId,
                    'identifier' => $this->getRequest()->getData('identifier'),
                    'name' => $this->getRequest()->getData('name'),
                    'description' => $this->getRequest()->getData(
                        'description'
                    ),
                    'default_agreement' => $this->getRequest()->getData(
                        'default_agreement'
                    ),
                    'active' => $this->getRequest()->getData('active'),
                    'auto_validate' => $this->getRequest()->getData(
                        'auto_validate'
                    ),
                    'proper_chain_id' => $this->getRequest()->getData(
                        'proper_chain_id'
                    ),
                    'improper_chain_id' => $this->getRequest()->getData(
                        'improper_chain_id'
                    ),
                    'date_begin' => $this->getRequest()->getData('date_begin'),
                    'date_end' => $this->getRequest()->getData('date_end'),
                ]
            );
            $this->getRequest()->getSession()->write(
                'Agreement.entity',
                $entity
            );
        } else {
            $entity->isNew();
        }

        if ($this->getRequest()->is('post')) {
            $data = [
                'allow_all_transferring_agencies' => $this->getRequest()
                    ->getData(
                        'allow_all_transferring_agencies'
                    ),
                'allow_all_originating_agencies' => $this->getRequest()
                    ->getData(
                        'allow_all_originating_agencies'
                    ),
                'allow_all_service_levels' => $this->getRequest()->getData(
                    'allow_all_service_levels'
                ),
                'allow_all_profiles' => $this->getRequest()->getData(
                    'allow_all_profiles'
                ),
                'transferring_agencies' => $this->getRequest()->getData(
                    'transferring_agencies'
                ),
                'originating_agencies' => $this->getRequest()->getData(
                    'originating_agencies'
                ),
                'service_levels' => $this->getRequest()->getData(
                    'service_levels'
                ),
                'profiles' => $this->getRequest()->getData('profiles'),
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            /** @var AgreementsTable $Agreements */
            $Agreements = $this->fetchTable('Agreements');
            $Agreements->patchEntity(
                $entity,
                $data,
                [
                    'associated' => [
                        'TransferringAgencies' => ['fields' => ['_ids']],
                        'OriginatingAgencies' => ['fields' => ['_ids']],
                        'ServiceLevels' => ['fields' => ['_ids']],
                        'Profiles' => ['fields' => ['_ids']],
                    ],
                ]
            );
            if (empty($entity->getErrors())) {
                $this->getRequest()->getSession()->write(
                    'Agreement.entity',
                    $entity
                );
                $this->Modal->success();
                $this->Modal->step('addAgreementStep3()');
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        } else {
            $default = [
                "allow_all_transferring_agencies" => true,
                "allow_all_originating_agencies" => true,
                "allow_all_service_levels" => true,
                "allow_all_profiles" => true,
            ];
            $Agreements->patchEntity(
                $entity,
                $default,
                ['validate' => false]
            );
        }
        $this->set('entity', $entity);

        // Options
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $ta = $OrgEntities->listOptions([], $this->Seal->archivalAgencyChilds())
            ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES]);
        $this->set('transferring_agencies', $ta->all());
        $oa = $OrgEntities->listOptions([], $this->Seal->archivalAgencyChilds())
            ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES])
            ->orderBy(['OrgEntities.name' => 'asc']);
        $this->set('originating_agencies', $oa->all());
        $sl = $this->Seal->archivalAgency()
            ->table('ServiceLevels')
            ->find('sealList')
            ->where(['ServiceLevels.active' => true])
            ->orderBy(['ServiceLevels.name' => 'asc']);
        $this->set('service_levels', $sl);
        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('sealList')
            ->where(['Profiles.active' => true])
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles);
    }

    /**
     * Partie 3 de l'ajout d'un accord de versement
     */
    public function add3()
    {
        $request = $this->getRequest();
        /** @var AgreementsTable $Agreements */
        $Agreements = $this->fetchTable('Agreements');
        /** @var EntityInterface $entity */
        $entity = $request->getSession()->read('Agreement.entity');
        if (empty($entity)) { // en cas de deconnexion/reconnexion
            $entity = $Agreements->newEntity(
                [
                    'default_level' => $request->getData('default_level'),
                    'org_entity_id' => $this->archivalAgencyId,
                    'identifier' => $request->getData('identifier'),
                    'name' => $request->getData('name'),
                    'description' => $request->getData('description'),
                    'default_agreement' => $request->getData('default_agreement'),
                    'active' => $request->getData('active'),
                    'auto_validate' => $request->getData('auto_validate'),
                    'proper_chain_id' => $request->getData('proper_chain_id'),
                    'improper_chain_id' => $request->getData('improper_chain_id'),
                    'date_begin' => $request->getData('date_begin'),
                    'date_end' => $request->getData('date_end'),
                    'allow_all_transferring_agencies' => $request
                        ->getData('allow_all_transferring_agencies'),
                    'allow_all_originating_agencies' => $request
                        ->getData('allow_all_originating_agencies'),
                    'allow_all_service_levels' => $request
                        ->getData('allow_all_service_levels'),
                    'allow_all_profiles' => $request
                        ->getData('allow_all_profiles'),
                    'transferring_agencies' => [
                        '_ids' => $request->getData('transferring_agencies._ids', []),
                    ],
                    'originating_agencies' => [
                        '_ids' => $request->getData('originating_agencies._ids', []),
                    ],
                    'service_levels' => [
                        '_ids' => $request->getData('service_levels._ids', []),
                    ],
                    'profiles' => [
                        '_ids' => $request->getData('profiles._ids', []),
                    ],
                ]
            );
        } else {
            $entity->isNew();
        }
        if ($request->is('post')) {
            $data = [
                'default_agreement' => $entity->get('default_agreement'),
                'allowed_formats' => $request->getData('allowed_formats'),
                'require_files' => $request->getData('require_files'),
                'alert_on_invalid_files' => $request->getData('alert_on_invalid_files'),
                'max_transfers' => $request->getData('max_transfers'),
                'transfer_period' => $request->getData('transfer_period'),
                'mult_max_size_per_transfer' => $request
                    ->getData('mult_max_size_per_transfer'),
                'max_size_per_transfer_conv' => $request
                    ->getData('max_size_per_transfer_conv'),
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            /** @var AgreementsTable $Agreements */
            $Agreements = $this->fetchTable('Agreements');
            $session = $request->getSession();
            $Agreements->patchEntity($entity, $data);
            if ($Agreements->save($entity)) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAdd($entity);
                $session->delete('Agreement.entity');
                $entity = $Agreements->find()
                    ->where(['Agreements.id' => $entity->get('id')])
                    ->contain(
                        [
                            'ProperChains',
                            'ImproperChains',
                            'TransferringAgencies',
                            'OriginatingAgencies',
                            'ServiceLevels',
                        ]
                    )
                    ->firstOrFail();
                $this->Modal->success();
                $this->Modal->step('addAgreementStep4(' . $entity->id . ')');
            } else {
                $this->Modal->fail();
            }
        } else {
            $default = [
                "require_files" => true,
            ];
            $Agreements->patchEntity($entity, $default);
        }
        $this->set('entity', $entity);
        $this->getAllowedFormatsOptions();
        $this->set(
            'transfer_periods',
            $Agreements->options('transfer_period')
        );
    }

    /**
     * Suppression d'un accord de versement
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var AgreementsTable $Agreements */
        $Agreements = $this->fetchTable('Agreements');
        $entity = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('seal')
            ->where(['Agreements.id' => $id])
            ->firstOrFail();

        $conn = $Agreements->getConnection();
        $conn->begin();
        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logDelete($entity);
        if ($Agreements->delete($entity)) {
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Donne les options complexes du champ json allowed_formats
     */
    private function getAllowedFormatsOptions()
    {
        /** @var PronomsTable $Pronoms */
        $Pronoms = $this->fetchTable('Pronoms');
        /** @var FileExtensionsTable $FileExtensions */
        $FileExtensions = $this->fetchTable('FileExtensions');
        $pronoms = $Pronoms->find(
            'list',
            keyField: function (EntityInterface $pronom) {
                return 'pronom-' . $pronom->get('puid');
            },
            valueField: function (EntityInterface $pronom) {
                return $pronom->get('puid') . ' - ' . $pronom->get('name');
            },
        )->orderBy(['id'])->all()->toArray();
        $this->set('pronoms', $pronoms);

        $extensions = $FileExtensions->find(
            'list',
            keyField: function (EntityInterface $extension) {
                return 'ext-' . $extension->get('name');
            },
            valueField: function (EntityInterface $pronom) {
                return __("Extension") . ' - ' . $pronom->get('name');
            },
        )->orderBy(['name'])->all()->toArray();
        $this->set('extensions', $extensions);

        $mimes = $Pronoms->find(
            'list',
            valueField: function (EntityInterface $pronom) {
                return 'MIME - ' . $pronom->get('mime');
            },
            keyField: function (EntityInterface $extension) {
                return 'mime-' . $extension->get('mime');
            },
        )->orderBy(['mime'])->where(['mime is NOT' => null])->all()->toArray();
        $this->set('mimes', array_unique($mimes));

        $this->set(
            'allowed_formats',
            [
                __("Types MIME") => $mimes,
                __("PRONOM") => $pronoms,
                __("Extensions de nom de fichier") => $extensions,
            ]
        );
    }

    /**
     * Partie 4 de l'ajout d'un accord de versement
     * @param string $id
     * @return void
     * @throws Exception
     */
    public function add4(string $id)
    {
        $request = $this->getRequest();
        /** @var AgreementsTable $Agreements */
        $Agreements = $this->fetchTable('Agreements');
        $entity = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('seal')
            ->where(['Agreements.id' => $id])
            ->contain(
                [
                    'ProperChains',
                    'ImproperChains',
                    'TransferringAgencies',
                    'OriginatingAgencies',
                    'ServiceLevels',
                    'Profiles',
                ]
            )
            ->firstOrFail()
            ->setVirtual(['deletable']);
        if ($request->is('put')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save(
                $Agreements,
                $entity,
                ['notify_delay' => $request->getData('notify_delay')]
            );
        }
        $this->set('entity', $entity);
    }

    /**
     * Modification d'un accord de versement
     * @param string $id
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var AgreementsTable $Agreements */
        $Agreements = $this->fetchTable('Agreements');
        $entity = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('seal')
            ->where(['Agreements.id' => $id])
            ->contain(
                [
                    'ProperChains',
                    'ImproperChains',
                    'TransferringAgencies',
                    'OriginatingAgencies',
                    'ServiceLevels',
                    'Profiles',
                ]
            )
            ->firstOrFail();
        if ($this->getRequest()->is('put')) {
            $data = [
                'org_entity_id' => $entity->get('org_entity_id'),
            ] + $this->getRequest()->getData();
            $Agreements->patchEntity($entity, $data);
            $data['transferring_agencies']
                = [
                    '_ids' => $this->getRequest()->getData(
                        'transferring_agencies._ids'
                    ),
                ];
            $data['originating_agencies']
                = [
                    '_ids' => $this->getRequest()->getData(
                        'originating_agencies._ids'
                    ),
                ];
            $data['service_levels']
                = [
                    '_ids' => $this->getRequest()->getData(
                        'service_levels._ids'
                    ),
                ];
            $data['profiles']
                = ['_ids' => $this->getRequest()->getData('profiles._ids')];
            $entity->set('transfer_period', $data['transfer_period']);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save(
                $Agreements,
                $entity,
                $data,
                [
                    'fields' => [
                        'default_level',
                        'identifier',
                        'name',
                        'description',
                        'active',
                        'allow_all_transferring_agencies',
                        'allow_all_originating_agencies',
                        'allow_all_service_levels',
                        'allow_all_profiles',
                        'transferring_agencies',
                        'originating_agencies',
                        'service_levels',
                        'auto_validate',
                        'proper_chain_id',
                        'improper_chain_id',
                        'date_begin',
                        'date_end',
                        'default_agreement',
                        'allowed_formats',
                        'require_files',
                        'alert_on_invalid_files',
                        'max_transfers',
                        'max_size_per_transfer',
                        'max_size_per_transfer_conv',
                        'mult_max_size_per_transfer',
                        'notify_delay',
                    ],
                    'associated' => [
                        'TransferringAgencies',
                        'OriginatingAgencies',
                        'ServiceLevels',
                        'Profiles',
                    ],
                ],
                function (EntityInterface $entity, Table $model) {
                    /** @var EventLoggerComponent $EventLogger */
                    $EventLogger = $this->loadComponent('EventLogger');
                    $EventLogger->logEdit($entity);
                    return $model->find()
                        ->where(['Agreements.id' => $entity->get('id')])
                        ->contain(
                            [
                                'ProperChains',
                                'ImproperChains',
                                'TransferringAgencies',
                                'OriginatingAgencies',
                                'ServiceLevels',
                            ]
                        )
                        ->firstOrFail()
                        ->setVirtual(['deletable']);
                }
            );
        }
        $this->set('entity', $entity);

        // Options
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $ta = $OrgEntities->find(
            'list',
            valueField: fn(EntityInterface $e) => $e->get('name') . ($e->get('active')
                    ? ''
                    : (' (' . __(
                        "désactivé"
                    ) . ')')),
        )
            ->orderBy(['OrgEntities.name'])
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES,
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            );
        $this->set('transferring_agencies', $ta->all());
        $oa = $OrgEntities->find(
            'list',
            valueField: fn(EntityInterface $e) => $e->get('name') . ($e->get('active')
                    ? ''
                    : (' (' . __(
                        "désactivé"
                    ) . ')')),
        )
            ->orderBy(['OrgEntities.name'])
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES,
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            );
        $this->set('originating_agencies', $oa->all());
        $sl = $this->Seal->archivalAgency()
            ->table('ServiceLevels')
            ->find(
                'sealList',
                valueField: fn(EntityInterface $e) => $e->get('name') . ($e->get('active')
                    ? ''
                    : (' (' . __("désactivé") . ')')),
            )
            ->orderBy(['ServiceLevels.name' => 'asc']);
        $this->set('service_levels', $sl);
        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find(
                'sealList',
                valueField: fn(EntityInterface $e) => $e->get('name') . ($e->get('active')
                    ? ''
                    : (' (' . __("désactivé") . ')')),
            )
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles);
        $this->setValidationChainsOptions();
        $this->getAllowedFormatsOptions();
        /** @var ProfilesTable $Profiles */
        $Profiles = $this->fetchTable('Profiles');
        $profiles = $Profiles->find(
            'sealList',
            valueField: fn(EntityInterface $e) => $e->get('name') . ($e->get('active')
                ? ''
                : (' (' . __("désactivé") . ')')),
        )
            ->where(['Profiles.org_entity_id' => $this->archivalAgencyId])
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles->all());
        $this->set(
            'transfer_periods',
            $Agreements->options('transfer_period')
        );
    }

    /**
     * Visualisation d'un accord de versement
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $entity = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('seal')
            ->where(['Agreements.id' => $id])
            ->contain(
                [
                    'ProperChains',
                    'ImproperChains',
                    'TransferringAgencies',
                    'OriginatingAgencies',
                    'ServiceLevels',
                    'Profiles',
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($entity);
    }

    /**
     * Défini un accord par défaut
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function setDefault(string $id): Response
    {
        $entity = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('seal')
            ->where(
                [
                    'Agreements.id' => $id,
                    'Agreements.active IS' => true,
                ]
            )
            ->firstOrFail();
        $entity->set('default_agreement', true);
        /** @var AgreementsTable $Agreements */
        $Agreements = $this->fetchTable('Agreements');
        return $this->renderDataToJson(
            ['success' => $Agreements->save($entity)]
        );
    }

    /**
     * Création via API
     */
    protected function apiAdd()
    {
        $request = $this->getRequest();
        $active = null;
        if ($request->getData('active')) {
            $active = $request->getData('active') === 'true';
        }
        $default_agreement = null;
        if ($request->getData('default_agreement')) {
            $default_agreement = $request->getData(
                'default_agreement'
            ) === 'true';
        }
        $formats = '[]';
        if ($request->getData('allowed_formats')) {
            $formats = json_encode(
                explode(',', $request->getData('allowed_formats'))
            );
        }
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $sa = $OrgEntities->get($request->getData('org_entity_id'));

        $transferring_agencies = [];
        if ($request->getData('transferring_agencies')) {
            $transferring_agencies = ['_ids' => []];
            $exp = explode(
                ',',
                $request->getData('transferring_agencies')
            );
            foreach ($exp as $ta) {
                $transferring_agencies['_ids'][] = $OrgEntities->find()
                    ->innerJoinWith('TypeEntities')
                    ->innerJoinWith('ArchivalAgencies')
                    ->where(
                        [
                            'OrgEntities.id' => (int)trim($ta),
                            'ArchivalAgencies.id' => $sa->id,
                            'TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES,
                        ]
                    )
                    ->firstOrFail()
                    ->id;
            }
        }
        $originating_agencies = [];
        if ($request->getData('originating_agencies')) {
            $originating_agencies = ['_ids' => []];
            $exp = explode(
                ',',
                $request->getData('originating_agencies')
            );
            foreach ($exp as $oa) {
                $originating_agencies['_ids'][] = $OrgEntities->find()
                    ->innerJoinWith('TypeEntities')
                    ->innerJoinWith('ArchivalAgencies')
                    ->where(
                        [
                            'OrgEntities.id' => (int)trim($oa),
                            'ArchivalAgencies.id' => $sa->id,
                            'TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES,
                        ]
                    )
                    ->firstOrFail()
                    ->id;
            }
        }
        $service_levels = [];
        if ($request->getData('service_levels')) {
            $service_levels = ['_ids' => []];
            /** @var ServiceLevelsTable $ServiceLevels */
            foreach (explode(',', $request->getData('service_levels')) as $sl) {
                $service_levels['_ids'][] = $this->Seal->archivalAgency()
                    ->table('ServiceLevels')
                    ->find('seal')
                    ->where(['ServiceLevels.id' => (int)trim($sl)])
                    ->firstOrFail()
                    ->id;
            }
        }
        $profiles = [];
        if ($request->getData('profiles')) {
            $profiles = ['_ids' => []];
            /** @var ProfilesTable $Profiles */
            $Profiles = $this->fetchTable('Profiles');
            foreach (explode(',', $request->getData('profiles')) as $p) {
                $profiles['_ids'][] = $Profiles->find()
                    ->where(
                        [
                            'Profiles.id' => (int)trim($p),
                            'Profiles.org_entity_id' => $sa->id,
                        ]
                    )
                    ->firstOrFail()
                    ->id;
            }
        }

        $data = [
            'org_entity_id' => $request->getData('org_entity_id'),
            'identifier' => $request->getData('identifier'),
            'name' => $request->getData('name'),
            'description' => $request->getData('description'),
            'active' => $active,
            'allow_all_transferring_agencies' => empty($transferring_agencies),
            'transferring_agencies' => $transferring_agencies,
            'allow_all_originating_agencies' => empty($originating_agencies),
            'originating_agencies' => $originating_agencies,
            'allow_all_service_levels' => empty($service_levels),
            'service_levels' => $service_levels,
            'allow_all_profiles' => empty($profiles),
            'profiles' => $profiles,
            'auto_validate' => !$request->getData('proper_chain_id'),
            'proper_chain_id' => $request->getData('proper_chain_id'),
            'improper_chain_id' => $request->getData('improper_chain_id'),
            'date_begin' => $request->getData('date_begin'),
            'date_end' => $request->getData('date_end'),
            'default_agreement' => $default_agreement,
            'allowed_formats' => $formats,
            'max_tranfers' => $request->getData('max_tranfers'),
            'transfer_period' => $request->getData('transfer_period'),
            'max_size_per_transfer' => $request->getData('max_size_per_transfer'),
        ];
        /** @var AgreementsTable $Agreements */
        $Agreements = $this->fetchTable('Agreements');
        $entity = $Agreements->newEntity($data);
        $conn = $Agreements->getConnection();
        $conn->begin();
        try {
            if ($Agreements->save($entity)) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAdd($entity);
                $conn->commit();
                return $this->renderData(
                    ['success' => true, 'id' => $entity->id]
                );
            } else {
                $conn->rollback();
                return $this->renderData(
                    ['success' => false, 'errors' => $entity->getErrors()]
                );
            }
        } finally {
            while ($conn->inTransaction()) {
                $conn->rollback();
            }
        }
    }
}
