<?php

/**
 * Asalae\Controller\AdminTenantsController
 */

namespace Asalae\Controller;

use Asalae\Model\Entity\Eaccpf;
use Asalae\Model\Entity\Ldap;
use Asalae\Model\Entity\User;
use Asalae\Model\Table\AcosTable;
use Asalae\Model\Table\ArchivingSystemsTable;
use Asalae\Model\Table\ArosAcosTable;
use Asalae\Model\Table\ArosTable;
use Asalae\Model\Table\StoredFilesTable;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Factory;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Response as CoreResponse;
use AsalaeCore\I18n\Date as CoreDate;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\Ip;
use AsalaeCore\Utility\Notify;
use Asalae\Controller\Component\EmailsComponent;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Table\BeanstalkJobsTable;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\CronsTable;
use Asalae\Model\Table\EaccpfsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\LdapsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\RolesTable;
use Asalae\Model\Table\SecureDataSpacesTable;
use Asalae\Model\Table\SessionsTable;
use Asalae\Model\Table\TimestampersTable;
use Asalae\Model\Table\TypeEntitiesTable;
use Asalae\Model\Table\UsersTable;
use Asalae\Model\Table\VersionsTable;
use Asalae\Model\Table\VolumesTable;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Beanstalk\Model\Table\BeanstalkWorkersTable;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\Http\Response as CakeResponse;
use Cake\I18n\Date as CakeDate;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Exception;
use Pheanstalk\Exception\ServerException;
use Psr\Http\Message\ResponseInterface;
use ZMQSocketException;

/**
 * Administration technique pour les tenants
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AcosTable Acos
 * @property ArchivingSystemsTable ArchivingSystems
 * @property ArosTable Aros
 * @property BeanstalkJobsTable BeanstalkJobs
 * @property BeanstalkWorkersTable BeanstalkWorkers
 * @property ConfigurationsTable Configurations
 * @property CronsTable Crons
 * @property EaccpfsTable Eaccpfs
 * @property EmailsComponent $Emails
 * @property EventLogsTable EventLogs
 * @property FileuploadsTable Fileuploads
 * @property LdapsTable Ldaps
 * @property OrgEntitiesTable OrgEntities
 * @property ArosAcosTable Permissions
 * @property RolesTable Roles
 * @property SecureDataSpacesTable SecureDataSpaces
 * @property SessionsTable Sessions
 * @property StoredFilesTable StoredFiles
 * @property Table Phinxlog
 * @property TimestampersTable Timestampers
 * @property TypeEntitiesTable TypeEntities
 * @property UsersTable Users
 * @property VersionsTable Versions
 * @property VolumesTable Volumes
 */
class AdminTenantsController extends AdminsController
{
    /**
     * Neutralise le beforeRender pour permettre de se connecter a ce controller
     * sans base de donnée
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeRender(EventInterface $event)
    {
        $event->setData('do_not_override', true);
        return parent::beforeRender($event);
    }

    /**
     * Contrôle que l'utilisateur est connecté en administrateur technique
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse|null
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        if (
            Configure::read('Ip.enable_whitelist', false)
            && !Ip::inWhitelist($this->getRequest()->clientIp())
        ) {
            $this->Flash->error(
                __("Vous n'avez pas accès à cette partie de l'application")
            );
            return $this->redirect('/users/logout');
        }

        $event->setData('do_not_override', true);
        parent::beforeFilter($event);
    }

    /**
     * Page principale de l'administration technique
     */
    public function index()
    {
        $this->set('archivalAgency', $this->archivalAgency);
        parent::index();
        $this->viewBuilder()->setLayout('admin_tenant');
    }

    /**
     * Donne des infos sur l'état du system
     */
    public function probe()
    {
        $this->viewBuilder()->setTemplatePath('Admins');
        // memory usage
        exec('cat /proc/meminfo', $output);
        $parsed = [];
        foreach ($output as $str) {
            if (strpos($str, ':')) {
                [$key, $value] = explode(':', $str, 2);
                $parsed[$key] = (int)$value;
            }
        }
        $probe = [
            'memory' => [
                'total' => $parsed['MemTotal'] ?? 0,
                'available' => $parsed['MemAvailable'] ?? 0,
            ],
            'swap' => [
                'total' => $parsed['SwapTotal'] ?? 0,
                'available' => $parsed['SwapFree'] ?? 0,
            ],
            'loadavg' => sys_getloadavg(),
        ];
        $this->set('probe', $probe);
    }

    /**
     * Vérification asynchrone des volumes
     * @throws Exception
     */
    public function ajaxCheckVolumes()
    {
        parent::ajaxCheckVolumes();
    }

    /**
     * Vérification asynchrone des TreeBehavior
     * @throws Exception
     */
    public function ajaxCheckTreeSanity()
    {
        parent::ajaxCheckTreeSanity();
    }

    /**
     * Vérification du système
     */
    public function ajaxCheck()
    {
        parent::ajaxCheck();
    }

    /**
     * Visualisation des logs de l'application
     */
    public function ajaxLogs()
    {
        parent::ajaxLogs();
    }

    /**
     * Donne le fichier de log
     * @param string $filename
     * @return CoreResponse
     */
    public function downloadLog(string $filename)
    {
        return parent::downloadLog($filename);
    }

    /**
     * Envoi une notification à tous les utilisateurs
     */
    public function notifyAll()
    {
        $this->viewBuilder()->setTemplatePath('Admins');
        if ($this->getRequest()->is('post')) {
            $title = '<h4>' . __("Message de l'administrateur") . '</h4>';
            $notificationsSubmited = 0;
            /** @var Notify $Notify */
            $Notify = Factory\Utility::get('Notify');
            $innerBody = $this->getNotifyBody();
            $msg = $title . $innerBody;
            $class = h($this->getRequest()->getData('color'));

            if ($this->getRequest()->getData('activeusers')) {
                /** @var EntityInterface $session */
                $usersTokens = [];
                /** @var SessionsTable $Sessions */
                $Sessions = $this->fetchTable('Sessions');
                $query = $Sessions->find()
                    ->select(['user_id', 'token'])
                    ->innerJoinWith('Users')
                    ->innerJoinWith('Users.OrgEntities')
                    ->innerJoinWith('Users.OrgEntities.ArchivalAgencies')
                    ->leftJoinWith('Users.SuperArchivistsArchivalAgencies')
                    ->where(
                        [
                            'token IS NOT' => null,
                            'OR' => [
                                'ArchivalAgencies.id' => $this->archivalAgencyId,
                                'SuperArchivistsArchivalAgencies.id' => $this->archivalAgencyId,
                            ],
                        ]
                    );
                foreach ($query as $session) {
                    $usersTokens[$session->get('user_id')][] = $session->get(
                        'token'
                    );
                }
                foreach ($usersTokens as $user_id => $tokens) {
                    $notificationsSubmited++;
                    $Notify->send(
                        $user_id,
                        $tokens,
                        $msg,
                        $class
                    );
                }
            } else {
                /** @var UsersTable $Users */
                $Users = $this->fetchTable('Users');
                $users = $Users->find()
                    ->innerJoinWith('OrgEntities')
                    ->innerJoinWith('OrgEntities.ArchivalAgencies')
                    ->where(
                        [
                            'Users.active' => true,
                            'ArchivalAgencies.id' => $this->archivalAgencyId,
                        ]
                    )
                    ->all();
                /** @var EntityInterface $user */
                foreach ($users as $user) {
                    $notificationsSubmited++;
                    $Notify->send(
                        $user->get('id'),
                        [],
                        $msg,
                        $class
                    );
                }
            }
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();

            $this->logEvent(
                'admintech_notifyall',
                'success',
                __("Envoi de notification à tous les utilisateurs")
            );

            return $this->renderDataToJson(['count' => $notificationsSubmited]);
        }
    }

    /**
     * Etat des disques
     */
    public function ajaxCheckDisk()
    {
        $this->viewBuilder()->setTemplatePath('Admins');
        exec('df -h | grep -v tmpfs | grep -v /dev/loop', $disks);
        $this->set('disks', $disks);
    }

    /**
     * Effectue un tail -f sur un fichier log
     * @param string $logfile
     * @param string $timeout
     * @return CoreResponse
     * @throws Exception
     */
    public function tail(string $logfile, $timeout = '10')
    {
        return parent::tail($logfile, $timeout);
    }

    /**
     * Affiche un phpinfo()
     * @return CakeResponse
     */
    public function phpInfo()
    {
        return parent::phpInfo();
    }

    /**
     * Liste les utilisateurs connectés
     * @return void
     */
    public function indexSessions()
    {
        /** @var SessionsTable $Sessions */
        $Sessions = $this->fetchTable('Sessions');

        $this->set('tableIdSessions', self::TABLE_INDEX_SESSIONS);

        $sessions = $Sessions->find()
            ->contain(
                [
                    'Users' => [
                        'OrgEntities' => [
                            'ArchivalAgencies',
                        ],
                    ],
                ]
            )
            ->where(
                [
                    'Sessions.user_id IS NOT' => null,
                    'ArchivalAgencies.id' => $this->archivalAgencyId,
                ]
            )
            ->orderBy(['Users.username' => 'asc', 'Sessions.modified' => 'desc'])
            ->limit(100) // garde fou - envisager une pagination si besoin
            ->all()
            ->toArray();
        $this->set('sessions', $sessions);
    }

    /**
     * Déconnecte un utilisateur
     * @param string $session_id
     * @return CakeResponse
     */
    public function deleteSession(string $session_id)
    {
        return parent::deleteSession($session_id);
    }

    /**
     * Envoi une notification à un utilisateur spécifique
     * @param string $session_id
     * @return CakeResponse
     * @throws ZMQSocketException
     */
    public function notifyUser(string $session_id)
    {
        $this->viewBuilder()->setTemplatePath('Admins');
        return parent::notifyUser($session_id);
    }

    /**
     * Ping
     * @return CakeResponse
     */
    public function ping()
    {
        return parent::ping();
    }

    /**
     * Donne les roles disponibles pour un webservice
     * @param string $org_entity_id
     * @return CakeResponse
     */
    public function getWsRolesOptions(string $org_entity_id)
    {
        return parent::getWsRolesOptions($org_entity_id);
    }

    /**
     * ArchivalAgencyTrait
     */

    /**
     * Permet d'ajouter / retirer des Services d'archives
     */
    public function indexServicesArchives()
    {
        $this->viewBuilder()->setTemplatePath('AdminTenants/ArchivalAgency');
        $this->getRequest()->allowMethod('ajax');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        /** @var OrgEntity $entity */
        $data = $OrgEntities->find()
            ->where(['OrgEntities.id' => $this->archivalAgencyId])
            ->contain(['TypeEntities'])
            ->all()
            ->toArray();
        $this->set('data', $data);
        $this->set('tableIdServiceArchive', self::TABLE_SERVICE_ARCHIVE);
    }

    /**
     * Edition d'un service d'archives
     * @param string $id
     * @throws Exception
     */
    public function editServiceArchive(string $id)
    {
        $this->viewBuilder()->setTemplatePath('AdminTenants/ArchivalAgency');
        $request = $this->getRequest();
        $request->allowMethod('ajax');
        /** @var ArchivingSystemsTable $ArchivingSystems */
        $ArchivingSystems = $this->fetchTable('ArchivingSystems');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var OrgEntity $entity */
        $entity = $OrgEntities->find()
            ->where(['OrgEntities.id' => $id])
            ->andWhere(
                [
                    'TypeEntities.code' => 'SA',
                ]
            )
            ->contain(
                [
                    'ArchivingSystems',
                    'Eaccpfs' => [
                        'sort' => [
                            'from_date' => 'desc',
                            'to_date' => 'desc',
                            'created' => 'desc',
                        ],
                    ],
                    'Ldaps',
                    'SecureDataSpaces',
                    'Timestampers',
                    'TypeEntities',
                    'Users' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q
                            ->orderBy(['Users.username' => 'asc'])
                            ->contain(['Roles'])
                            ->limit($AjaxPaginatorComponent->getConfig('limit'));
                    },
                ]
            )
            ->firstOrFail();

        /** @var EntityInterface $user */
        foreach ($entity->get('users') as $user) {
            $user->setVirtual(['deletable', 'is_linked_to_ldap']);
        }

        $this->set('entity', $entity);
        $countUsers = $Users->find()
            ->where(['org_entity_id' => $entity->id])
            ->count();
        $this->set('countUsers', $countUsers);
        $this->set('tableIdEaccpf', self::TABLE_EDIT_EACCPF_SA);
        $this->set('tableIdUser', self::TABLE_EDIT_USER_SA);

        $editableIdentifier = $entity->get('editable_identifier');
        $this->set('editableIdentifier', $editableIdentifier);

        if ($request->is('put')) {
            $data = [
                'name' => $request->getData('name'),
                'identifier' => $editableIdentifier
                    ? $this->getRequest()->getData('identifier')
                    : $entity->get('identifier'),
                'default_secure_data_space_id' => $request->getData(
                    'default_secure_data_space_id'
                ),
            ];

            $originalDefaultEcsId = $entity->get('default_secure_data_space_id');

            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $conn = $OrgEntities->getConnection();
            $conn->begin();
            $OrgEntities->patchEntity($entity, $data);
            $success = (bool)$OrgEntities->save($entity, ['associated' => false]);
            $this->logEvent(
                'admintech_edit_archival_agency',
                $success ? 'success' : 'fail',
                __("Modification d'un service d'archives"),
                $entity
            );
            if ($success) {
                /** @var SecureDataSpacesTable $SecureDataSpaces */
                $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
                $defaultSecureDataSpace = $SecureDataSpaces->find()
                    ->innerJoinWith('Volumes')
                    ->where(
                        [
                            'SecureDataSpaces.id' => $request->getData('default_secure_data_space_id'),
                            'OR' => [
                                'SecureDataSpaces.org_entity_id' => $id,
                                'SecureDataSpaces.org_entity_id IS' => null,
                            ],
                        ]
                    )
                    ->firstOrFail();

                if ($request->getData('default_secure_data_space_id') !== $originalDefaultEcsId) {
                    $SecureDataSpaces->updateAll(
                        ['is_default' => false],
                        ['org_entity_id' => $id]
                    );

                    $SecureDataSpaces->updateAll(
                        ['is_default' => true],
                        ['id' => $defaultSecureDataSpace->get('id')]
                    );
                }
            }
            if ($success) {
                $this->Modal->success();
                $conn->commit();
            } else {
                $this->Modal->fail();
                $conn->rollback();
                FormatError::logEntityErrors($entity);
            }
        }

        /** @var TimestampersTable $Timestampers */
        $Timestampers = $this->fetchTable('Timestampers');
        $query = $Timestampers->find();
        $timestampers = [];
        foreach ($query as $timestamper) {
            $timestampers[] = [
                'value' => $timestamper->get('id'),
                'text' => $timestamper->get('name'),
                'locked' => $timestamper->get('removable') ? false : 'locked',
            ];
        }
        $this->set('timestampers', $timestampers);

        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
        $query = $SecureDataSpaces->find()
            ->innerJoinWith('Volumes')
            ->where(
                [
                    'OR' => [
                        'SecureDataSpaces.org_entity_id IS' => null,
                        'SecureDataSpaces.org_entity_id' => $id,
                    ],
                ]
            );
        $spaces = [];
        /** @var EntityInterface $space */
        foreach ($query as $space) {
            $spaces[$space->get('id')] = [
                'value' => $space->get('id'),
                'text' => $space->get('name'),
                'locked' => $space->get('removable') ? false : 'locked',
            ];
        }
        $this->set('secure_data_spaces', $spaces);

        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $query = $Ldaps->find()
            ->where(
                [
                    'OR' => [
                        'Ldaps.org_entity_id IS' => null,
                        'Ldaps.org_entity_id' => $id,
                    ],
                ]
            );
        $ldaps = [];
        /** @var EntityInterface $ldap */
        foreach ($query as $ldap) {
            $ldaps[$ldap->get('id')] = [
                'value' => $ldap->get('id'),
                'text' => $ldap->get('name'),
                'locked' => $ldap->get('removable') ? false : 'locked',
            ];
        }
        $this->set('ldaps', $ldaps);
        $query = $ArchivingSystems->find()
            ->where(
                [
                    'OR' => [
                        'ArchivingSystems.org_entity_id IS' => null,
                        'ArchivingSystems.org_entity_id' => $id,
                    ],
                ]
            );
        $archivingSystems = [];
        /** @var EntityInterface $archivingSystem */
        foreach ($query as $archivingSystem) {
            $archivingSystems[$archivingSystem->get('id')] = [
                'value' => $archivingSystem->get('id'),
                'text' => $archivingSystem->get('name'),
                'locked' => $archivingSystem->get('removable') ? false
                    : 'locked',
            ];
        }
        $this->set('archivingSystems', $archivingSystems);

        $rolesUsers = $OrgEntities->findByRoles($entity)
            ->select(['qexists' => 1], true)
            ->where(['OrgEntities.id' => $id, 'agent_type' => 'person'])
            ->disableHydration()
            ->first();
        $this->set('rolesUsers', (bool)$rolesUsers);
        $rolesWs = $OrgEntities->findByRoles($entity)
            ->select(['qexists' => 1], true)
            ->where(['OrgEntities.id' => $id, 'agent_type' => 'software'])
            ->disableHydration()
            ->first();
        $this->set('rolesWs', (bool)$rolesWs);
        $AjaxPaginatorComponent->setViewPaginator($entity->get('users'), $countUsers);
    }

    /**
     * Formulaire d'ajout d'un eaccpf
     * @param string $orgEntityId
     * @return CakeResponse|null
     * @throws Exception
     */
    public function addEaccpf(string $orgEntityId)
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        $this->getRequest()->allowMethod('ajax');

        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = $this->fetchTable('Eaccpfs');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $orgEntity = $OrgEntities->find()
            ->where(['OrgEntities.id' => $orgEntityId])
            ->contain(
                [
                    'ParentOrgEntities',
                    'Eaccpfs' => [
                        'sort' => [
                            'from_date' => 'desc',
                            'to_date' => 'desc',
                            'created' => 'desc',
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        /** @var null|Eaccpf $prevEntity */
        $prevEntity = null;
        if (isset($orgEntity->get('eaccpfs')[0])) {
            $prevEntity = end($orgEntity->get('eaccpfs'));
            $defaultRecordId = $prevEntity->get('record_id');
            $data = [
                'record_id' => $defaultRecordId,
                'name' => $prevEntity->get('name'),
                'entity_id' => $prevEntity->get('entity_id'),
                'entity_type' => $prevEntity->get('entity_type'),
                'from_date' => (string)(
                new CakeDate(
                    $prevEntity->get('to_date')
                        ?: $this->getRequest()->getData('to_date')
                )
                ),
                'to_date' => null,
                'agency_name' => $prevEntity->get('agency_name'),
            ];
        } else {
            $defaultRecordId = $orgEntity->get('identifier');
            $data = [
                'record_id' => $defaultRecordId,
                'name' => $orgEntity->get('name'),
                'agency_name' => Hash::get(
                    $orgEntity,
                    'parent_org_entity.name'
                ),
            ];
        }

        /** @var Eaccpf $entity */
        $entity = $Eaccpfs->newEntity($data);
        $entity->set(
            'from_date',
            (string)($entity->get('from_date') ?: new CoreDate()),
            ['setter' => false]
        );
        $this->set('defaultRecordId', $defaultRecordId);
        $this->set('prevEntity', $prevEntity);

        if ($prevEntity && !$prevEntity->get('to_date')) {
            if ($this->getRequest()->is('put')) {
                $Eaccpfs->patchEntity(
                    $prevEntity,
                    [
                        'record_id' => $prevEntity->get('record_id'),
                        'to_date' => $this->getRequest()->getData('to_date'),
                    ]
                );
                if (!$Eaccpfs->save($prevEntity)) {
                    $this->viewBuilder()->setTemplate('todate');
                }
                /** @use ModalComponent */
                $this->loadComponent('AsalaeCore.Modal');
                $this->Modal->fail();
            } else {
                $this->viewBuilder()->setTemplate('todate');
            }
        }

        if ($this->getRequest()->is('post')) {
            $entity = $Eaccpfs->newEntity([], ['validate' => false]);
            $data = [
                'record_id' => $orgEntity->get('identifier'),
                'name' => $orgEntity->get('name'),
                'agency_name' => $this->getRequest()->getData('agency_name'),
                'entity_id' => $this->getRequest()->getData('entity_id'),
                'entity_type' => $this->getRequest()->getData('entity_type'),
                'from_date' => $this->getRequest()->getData('from_date'),
                'to_date' => $this->getRequest()->getData('to_date'),
                'org_entity_id' => $orgEntityId,
            ];
            $Eaccpfs->patchEntity($entity, $data);
            $session = $this->getRequest()->getSession();
            $createur = $session->read("Admin.data.name") ?: $session->read("Admin.data.username");
            $Eaccpfs->initializeData($entity, ['name' => $createur]);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($Eaccpfs->save($entity)) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);
        $this->set('entity_type', $Eaccpfs->options('entity_type'));

        $uploadId = 'eaccpf-upload-table';
        $this->set('uploadId', $uploadId);
        $this->set('orgEntityId', $orgEntityId);
    }

    /**
     * Ajouter un utilisateur
     * @param string $orgEntityId
     * @return CakeResponse|null
     * @throws Exception
     */
    public function addUser(string $orgEntityId)
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        $this->getRequest()->allowMethod('ajax');

        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        /** @var RolesTable $Roles */
        $Roles = $this->fetchTable('Roles');
        $role = $Roles->find()
            ->select(['id'])
            ->where(['name' => 'Archiviste'])
            ->firstOrFail();
        /** @var User $entity */
        $entity = $Users->newEntity(
            [
                'org_entity_id' => $orgEntityId,
                'role_id' => $role->get('id'),
            ]
        );

        if ($this->getRequest()->is('post')) {
            $password = $Users->generatePassword();
            $Users->patchEntity(
                $entity,
                [
                    'username' => $this->getRequest()->getData('username'),
                    'password' => $password,
                    'confirm-password' => $password,
                    'name' => $this->getRequest()->getData('name'),
                    'email' => $this->getRequest()->getData('email'),
                    'high_contrast' => $this->getRequest()->getData(
                        'high_contrast'
                    ),
                    'active' => true,
                    'org_entity_id' => $orgEntityId,
                    // important pour la validation
                    'agent_type' => 'person',
                    'is_validator' => $this->getRequest()->getData(
                        'is_validator'
                    ) ?? true,
                    'use_cert' => $this->getRequest()->getData(
                        'use_cert'
                    ) ?? false,
                ]
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');

            $success = $Users->save($entity);
            $this->logEvent(
                'admintech_add_user',
                $success ? 'success' : 'fail',
                __(
                    "Ajout de l'utilisateur ''{0}'' sur le service d'archives id={1}",
                    $entity->get('username'),
                    $orgEntityId
                ),
                $entity
            );

            if ($success) {
                $this->Modal->success();
                $json = $entity->toArray();
                $json['role'] = $role->toArray();
                /** @use EmailsComponent */
                $this->loadComponent('Emails');
                $this->Emails->submitEmailNewUser($entity);
                return $this->renderJson(json_encode($json));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('user', $entity);
        $this->set('title', __("Ajouter un utilisateur"));
    }

    /**
     * Pagination des utilisateurs de l'entité
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function paginateUsers(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index', ['model' => 'Users']);
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $query = $Users->find()
            ->where(['Users.org_entity_id' => $id])
            ->contain(['Roles']);
        $IndexComponent->setQuery($query)
            ->filter('username', IndexComponent::FILTER_ILIKE);
        return $AjaxPaginatorComponent->json(
            $query,
            (bool)$this->getRequest()->getQuery('count')
        );
    }

    /**
     * LdapsTrait
     */

    /**
     * Liste des ldaps
     */
    public function indexLdaps()
    {
        $this->viewBuilder()->setTemplatePath('AdminTenants/Ldaps');
        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $this->set('tableId', 'index-ldaps-table');
        $ldaps = $Ldaps->find()
            ->innerJoinWith('OrgEntities')
            ->innerJoinWith('OrgEntities.ArchivalAgencies')
            ->where(['ArchivalAgencies.id' => $this->archivalAgencyId])
            ->orderBy(['Ldaps.name' => 'asc'])
            ->all();
        $this->set('data', $ldaps);
    }

    /**
     * Action d'ajout
     */
    public function addLdap()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Ldaps');
        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $entity = $Ldaps->newEntity(
            ['org_entity_id' => $this->archivalAgencyId],
            ['validate' => false]
        );

        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $data['org_entity_id'] = $this->archivalAgencyId;
            $Ldaps->patchEntity($entity, $data);
            $entity->set(
                'custom_options',
                $this->getRequest()->getData('custom_options') ?: []
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');

            $success = $Ldaps->save($entity);
            $this->logEvent(
                'admintech_add_ldap',
                $success ? 'success' : 'fail',
                __(
                    "Ajout du LDAP ''{0}''",
                    $entity->get('name')
                )
            );

            if ($success) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
        $this->set('schemas', $Ldaps->options('schema'));
        $this->set('versions', $Ldaps->options('version'));
        $this->set(
            'custom_options',
            array_keys($Ldaps->options('append_custom_option'))
        );
    }

    /**
     * Action modifier
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function editLdap(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Ldaps');
        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $entity = $Ldaps->get($id);
        $request = $this->getRequest();
        if ($request->is('put')) {
            $data = $this->getRequest()->getData();
            $opts = [];
            foreach (
                $request->getData('custom_options', []) as $option => $val
            ) {
                if (defined('LDAP_OPT_' . $option)) {
                    $opts[constant('LDAP_OPT_' . $option)]
                        = in_array($option, Ldap::LDAP_OPTS_INT) ? (int)$val
                        : $val;
                }
            }
            $data['custom_options'] = $opts;
            $data['org_entity_id'] = $this->archivalAgencyId;
            $Ldaps->patchEntity($entity, $data);
            $entity->set(
                'custom_options',
                $this->getRequest()->getData('custom_options', [])
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');

            $success = $Ldaps->save($entity);
            $this->logEvent(
                'admintech_edit_ldap',
                $success ? 'success' : 'fail',
                __(
                    "Modification du LDAP ''{0}''",
                    $entity->get('name')
                )
            );

            if ($success) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);
        $this->set('schemas', $Ldaps->options('schema'));
        $this->set('versions', $Ldaps->options('version'));
        $this->set(
            'custom_options',
            array_keys($Ldaps->options('append_custom_option'))
        );
    }

    /**
     * Action supprimer
     * @param string $id
     * @return CakeResponse
     * @throws \Exception
     */
    public function deleteLdap(string $id)
    {
        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $this->getRequest()->allowMethod('delete');
        $entity = $Ldaps->get($id);

        $report = $Ldaps->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        $identity = $this->getRequest()->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        $adminname = Hash::get($user, 'username');
        $this->logEvent(
            'admintech_delete_ldap',
            $report === 'done' ? 'success' : 'fail',
            __(
                "Suppression du LDAP ''{0}'' par l'administrateur technique ''{1}''",
                $entity->get('name'),
                $adminname
            )
        );

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Donne la liste des utilisateurs d'un ldap
     * @param string $id
     * @return CakeResponse|void
     * @throws Exception
     */
    public function getLdapUsers(string $id)
    {
        $this->fetchTable('Ldaps')->find()
            ->innerJoinWith('OrgEntities')
            ->innerJoinWith('OrgEntities.ArchivalAgencies')
            ->where(
                [
                    'Ldaps.id' => $id,
                    'ArchivalAgencies.id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        return parent::getLdapUsers($id);
    }

    /**
     * @param string $id Service d'archive
     * @return CakeResponse|void
     * @throws Exception
     */
    public function importLdapUser(string $id)
    {
        $reponse = parent::importLdapUser($this->archivalAgencyId);
        if ($reponse) {
            return $reponse;
        }
        $this->viewBuilder()->setTemplatePath('AdminTenants/Ldaps');
    }

    /**
     * TasksTrait
     */

    /**
     * Administration des tubes et des workers beanstalk
     */
    public function ajaxTasks()
    {
        $this->viewBuilder()->setTemplatePath('AdminTenants/Tasks');
        /** @var Beanstalk $Beanstalk */
        $Beanstalk = Factory\Utility::get('Beanstalk');
        if (!$Beanstalk->isConnected() || !$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données et le serveur de jobs "
                    . "doivent fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }

        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        /** @var BeanstalkWorkersTable $BeanstalkWorkers */
        $BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers');

        $workerParams = Configure::read('Beanstalk.workers');
        $workerTubes = array_keys($workerParams);
        sort($workerTubes);
        $tubes = [];
        foreach ($workerTubes as $tube) {
            $q = $BeanstalkJobs->query();
            $jobStates = $BeanstalkJobs->find()
                ->select(
                    [
                        'job_state',
                        'count' => $q->func()->count('*'),
                    ]
                )
                ->innerJoinWith('Users')
                ->innerJoinWith('Users.OrgEntities')
                ->innerJoinWith('Users.OrgEntities.ArchivalAgencies')
                ->where(
                    [
                        'ArchivalAgencies.id' => $this->archivalAgencyId,
                        'tube' => $tube,
                    ]
                )
                ->groupBy(['job_state'])
                ->toArray();
            $workerCount = $BeanstalkWorkers->find()
                ->where(['tube' => $tube])
                ->count();
            $tubes[$tube] = [
                'tube' => $tube,
                'workers' => $workerCount,
                BeanstalkJobsTable::S_WORKING => 0,
                BeanstalkJobsTable::S_PENDING => 0,
                BeanstalkJobsTable::S_DELAYED => 0,
                BeanstalkJobsTable::S_PAUSED => 0,
                BeanstalkJobsTable::S_FAILED => 0,
            ];
            foreach ($jobStates as $state) {
                $tubes[$tube][$state->get('job_state')] = $state->get('count');
            }
        }
        $workers = $BeanstalkWorkers->find()
            ->orderBy(['tube']);
        $this->set('workers', $workers);
        $this->set('tubes', $tubes);
        $this->set('states', $BeanstalkJobs->options('job_state'));

        $serviceName = Configure::read('Beanstalk.service');
        setlocale(LC_ALL, 'en_US.UTF-8');
        if (!$serviceName) {
            $command = sprintf(
                '[ -f %1$s ] && tail %1$s -n500',
                escapeshellarg(LOGS . 'beanstalk.log')
            );
        } else {
            $command = sprintf(
                "journalctl -u %s -b -n500",
                escapeshellarg($serviceName)
            );
        }
        exec($command, $output);
        setlocale(LC_ALL, null);
        $this->set('serviceLogs', implode("\n", $output));
    }

    /**
     * Liste les jobs d'un tube
     * @param string $tube
     * @return ResponseInterface|void
     * @throws Exception
     */
    public function indexJobs(string $tube)
    {
        $this->viewBuilder()->setTemplatePath('AdminTenants/Tasks');
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $query = $BeanstalkJobs->find()
            ->innerJoinWith('Users')
            ->innerJoinWith('Users.OrgEntities')
            ->innerJoinWith('Users.OrgEntities.ArchivalAgencies')
            ->where(['ArchivalAgencies.id' => $this->archivalAgencyId])
            ->where(['tube' => $tube])
            ->contain(['Users']);

        $IndexComponent->setConfig('model', 'BeanstalkJobs');
        $IndexComponent->setConfig('limit', 10);

        $IndexComponent->setQuery($query)
            ->filter('id')
            ->filter('job_state', IndexComponent::FILTER_IN)
            ->filter(
                'username',
                IndexComponent::FILTER_ILIKE,
                'Users.username'
            );

        if ($this->getRequest()->accepts('application/json')) {
            return $AjaxPaginatorComponent->json($query);
        }

        $data = $this->paginateToResultSet($query);

        $this->set('data', $data);
        $this->set('count', $query->count());
        $this->set('tube', $tube);
        $this->set('states', $BeanstalkJobs->options('job_state'));
        $this->set('connected', (bool)$this->userId);
    }

    /**
     * Permet d'obtenir les infos d'un job au format json
     * @param string $id
     * @return CakeResponse
     */
    public function jobInfo(string $id)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $job = $BeanstalkJobs->find()
            ->innerJoinWith('Users')
            ->innerJoinWith('Users.OrgEntities')
            ->innerJoinWith('Users.OrgEntities.ArchivalAgencies')
            ->where(['ArchivalAgencies.id' => $this->archivalAgencyId])
            ->where(['BeanstalkJobs.id' => $id])
            ->contain(['Users'])
            ->first();
        return $this->renderDataToJson($job);
    }

    /**
     * delete le job
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function jobCancel(string $id)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $BeanstalkJobs->find()
            ->innerJoinWith('Users')
            ->innerJoinWith('Users.OrgEntities')
            ->innerJoinWith('Users.OrgEntities.ArchivalAgencies')
            ->where(['ArchivalAgencies.id' => $this->archivalAgencyId])
            ->where(['id' => $id])
            ->firstOrFail();
        $Beanstalk = Utility::get('Beanstalk', $entity->get('tube'));
        try {
            $Beanstalk->selectJob($entity->get('jobid'))->reserve()->done();
        } catch (ServerException) {
            $BeanstalkJobs->deleteOrFail($entity);
        }
        $report = "done";
        $this->logEvent(
            'admintech_delete_job',
            'success',
            __(
                "Suppression du jobid={0} du tube ''{1}''",
                $entity->get('jobid'),
                $entity->get('tube')
            )
        );

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * bury le job
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function jobPause(string $id)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $BeanstalkJobs->find()
            ->innerJoinWith('Users')
            ->innerJoinWith('Users.OrgEntities')
            ->innerJoinWith('Users.OrgEntities.ArchivalAgencies')
            ->where(['ArchivalAgencies.id' => $this->archivalAgencyId])
            ->where(['BeanstalkJobs.id' => $id])
            ->contain(['Users'])
            ->firstOrFail();
        $BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_PAUSE);
        $BeanstalkJobs->saveOrFail($entity);
        $report = "done";

        $entity->set('errors', 'pause');
        $data = $entity->toArray();
        $this->logEvent(
            'admintech_pause_job',
            'success',
            __(
                "Mise en pause du id={0} sur le tube ''{1}''",
                $entity->get('id'),
                $entity->get('tube')
            )
        );

        return $this->renderDataToJson(['report' => $report, 'data' => $data]);
    }

    /**
     * kick le job
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function jobResume(string $id)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');

        $entity = $BeanstalkJobs->find()
            ->innerJoinWith('Users')
            ->innerJoinWith('Users.OrgEntities')
            ->innerJoinWith('Users.OrgEntities.ArchivalAgencies')
            ->where(['ArchivalAgencies.id' => $this->archivalAgencyId])
            ->where(['BeanstalkJobs.id' => $id])
            ->contain(['Users'])
            ->firstOrFail();
        $BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_RESUME);
        $BeanstalkJobs->saveOrFail($entity);
        $report = "done";

        $data = $entity->toArray();
        $this->logEvent(
            'admintech_resume_job',
            'success',
            __(
                "Reprise du id={0} du tube ''{1}''",
                $entity->get('id'),
                $entity->get('tube')
            )
        );

        return $this->renderDataToJson(['report' => $report, 'data' => $data]);
    }

    /**
     * Permet d'obtenir les logs du tube d'un worker
     * @param string $tube
     * @throws Exception
     */
    public function workerLog(string $tube)
    {
        parent::workerLog($tube);
    }

    /**
     * Relance les jobs en erreur/pause d'un tube
     * @param string $tube
     * @return CakeResponse
     * @throws Exception
     */
    public function resumeAllTube(string $tube)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $query = $BeanstalkJobs->find()
            ->innerJoinWith('Users')
            ->innerJoinWith('Users.OrgEntities')
            ->innerJoinWith('Users.OrgEntities.ArchivalAgencies')
            ->where(['ArchivalAgencies.id' => $this->archivalAgencyId])
            ->where(['tube' => $tube, 'job_state' => BeanstalkJobsTable::S_FAILED]);
        $Beanstalk = Factory\Utility::get('Beanstalk', $tube);
        $kicked = 0;
        /** @var EntityInterface $job */
        foreach ($query as $job) {
            try {
                $Beanstalk->selectJob($job->get('jobid'))->kick();
                $kicked++;
            } catch (Exception) {
            }
        }
        $this->logEvent(
            'admintech_resume_all_jobs',
            'success',
            __("Reprise de tous les jobs du tube ''{0}''", $tube)
        );
        return $this->renderDataToJson(
            ['success' => true, 'kicked' => $kicked]
        );
    }

    /**
     * VolumeTrait
     */

    /**
     * Liste les volumes et les espaces de conservation sécurisés
     */
    public function indexVolumes()
    {
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');

        $this->set('tableIdVolumes', self::TABLE_INDEX_VOLUMES);
        $this->set('tableIdSpaces', self::TABLE_INDEX_SPACES);

        $volumes = $Volumes->find()
            ->innerJoinWith('SecureDataSpaces')
            ->where(['org_entity_id' => $this->archivalAgencyId])
            ->orderBy('Volumes.name')
            ->all()
            ->toArray();
        /** @var Entity $volume */
        foreach ($volumes as $volume) {
            $configured = Configure::read(
                'Volumes.drivers.' . $volume->get('driver') . '.fields',
                []
            );
            $fields = $this->filterPasswordFromFields(
                $volume->get('fields'),
                $configured
            );
            $data = json_decode($fields, true);
            if (isset($data['path'])) {
                $data['path'] = '*****/' . basename($data['path']);
            }
            if (!empty($data['verify'])) {
                $data['verify'] = '*****/' . basename($data['verify']);
            }
            $volume->set('fields', json_encode($data));
        }
        $this->set('volumes', $volumes);

        $spaces = $SecureDataSpaces->find()
            ->where(['org_entity_id' => $this->archivalAgencyId])
            ->contain(['Volumes', 'OrgEntities'])
            ->orderBy(['SecureDataSpaces.name' => 'asc'])
            ->all();
        $this->set('spaces', $spaces);
        $this->viewBuilder()->setTemplatePath('AdminTenants/Volumes');
    }

    /**
     * Test du volume
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function testVolume(string $id = '')
    {
        return parent::testVolume($id);
    }

    /**
     * Visualisation d'un volume
     * @param string $id
     */
    public function viewVolume(string $id)
    {
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        $volume = $Volumes->find()
            ->innerJoinWith('SecureDataSpaces')
            ->where(['Volumes.id' => $id, 'org_entity_id' => $this->archivalAgencyId])
            ->first();
        $configured = Configure::read(
            'Volumes.drivers.' . $volume->get('driver') . '.fields',
            []
        );
        $fields = $this->filterPasswordFromFields(
            $volume->get('fields'),
            $configured
        );
        $data = json_decode($fields, true);
        if (isset($data['path'])) {
            $data['path'] = '*****/' . basename($data['path']);
        }
        $volume->set('fields', json_encode($data));
        $this->set('volume', $volume);
        $this->viewBuilder()->setTemplatePath('Admins/Volumes');
    }

    /**
     * Donne la liste des fichiers d'un volume
     * @param string $volume_id
     * @param string ...$storagePath
     * @throws VolumeException
     */
    public function volumeClient(string $volume_id, string ...$storagePath)
    {
        $this->fetchTable('Volumes')->find()
            ->innerJoinWith('SecureDataSpaces')
            ->where(['Volumes.id' => $volume_id, 'org_entity_id' => $this->archivalAgencyId])
            ->firstOrFail();
        parent::volumeClient($volume_id, ...$storagePath);
        $this->viewBuilder()->setTemplatePath('AdminTenants/Volumes');
    }

    /**
     * Contrôle d'integrité sur un fichier d'un volume
     * @param string $volume_id
     * @param string ...$storagePath
     * @return \Cake\Http\Response
     * @throws VolumeException
     */
    public function volumeCheckHash(string $volume_id, string ...$storagePath)
    {
        $this->fetchTable('Volumes')->find()
            ->innerJoinWith('SecureDataSpaces')
            ->where(['Volumes.id' => $volume_id, 'org_entity_id' => $this->archivalAgencyId])
            ->firstOrFail();
        return parent::volumeCheckHash($volume_id, ...$storagePath);
    }

    /**
     * Edition de l'utilisateur admin (soit-même)
     * @throws Exception
     */
    public function editAdmin()
    {
        $request = $this->getRequest();
        $request->allowMethod('ajax');
        $this->viewBuilder()->setLayout('ajax');

        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $query = $this->Seal->archivalAgency()
            ->table('Users')
            ->find('seal')
            ->contain(['OrgEntities' => ['TypeEntities'], 'Roles'])
            ->where(['Users.id' => $this->userId]);
        $Users->setGenericUserContain($query);
        $entity = $query->firstOrFail();
        $entity->unset('password');

        if ($request->is('put')) {
            $data = [
                'high_contrast' => $request->getData('high_contrast'),
                'org_entity_id' => $entity->get('org_entity_id'),
                // important pour la validation
                'is_validator' => $request->getData('is_validator') ?? true,
                'use_cert' => $request->getData('use_cert') ?? false,
            ];

            if (!$entity->get('ldap_id')) {
                $data['username'] = $request->getData('username');
                $data['name'] = $request->getData('name');
                $data['email'] = $request->getData('email');
            }

            $password = $request->getData('password');
            if (!$entity->get('ldap_id') && $password) {
                $data += [
                    'password' => $password,
                    'confirm-password' => $request->getData('confirm-password'),
                ];
            }
            $Users->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($Users->save($entity)) {
                if ($request->getParam('action') === 'editByAdmin') {
                    $identity = $this->getRequest()->getAttribute('identity');
                    $user = $identity ? $identity->getOriginalData() : null;
                    $adminname = Hash::get($user, 'username');
                    $this->logEvent(
                        'admintech_edit_User',
                        'success',
                        __(
                            "Ajouté ({0}) par l'utilisateur ({1})",
                            $entity->get('username'),
                            $adminname
                        )
                    );
                }
                $this->Modal->success();
                $entity = $Users->find()
                    ->contain(['OrgEntities' => ['TypeEntities'], 'Roles'])
                    ->where(['Users.id' => $this->userId])
                    ->firstOrFail();
                $entity->setVirtual(['deletable']);
                $json = $entity->toArray();
                unset($json['password'], $json['confirm-password']);
                $request->getSession()->write('Auth', $json);
                return $this->renderDataToJson($json);
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('user', $entity);
    }
}
