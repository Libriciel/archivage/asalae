<?php

/**
 * Asalae\Controller\CommandsController
 */

namespace Asalae\Controller;

use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Response;
use AsalaeCore\Utility\Exec;
use Cake\Core\App;
use Cake\Utility\Inflector;
use Exception;
use http\Exception\InvalidArgumentException;

/**
 * Permet de lancer des commandes cake par API
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CommandsController extends AppController implements ApiInterface
{
    use ApiTrait;

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default' => [
                'get' => ['function' => 'test'],
                'post' => ['function' => 'send'],
            ],
        ];
    }

    /**
     * Envoi une commande cake
     * @return Response|\Cake\Http\Response
     * @throws Exception
     */
    protected function send()
    {
        /** @var Response $response */
        $response = $this->getResponse()->withType('text/plain');
        $command = $this->getRequest()->getData('command');
        $classname = App::className(Inflector::camelize($command), 'Command', 'Command');
        if (!$classname) {
            throw new InvalidArgumentException(
                __("Commande invalide. La clé 'command' doit contenir une classe de Commande cake")
            );
        }
        $command = CAKE_SHELL . ' ' . $command;

        $options = $this->getRequest()->getData('options') ?: [];
        if (!empty($options) && !is_array($options)) {
            throw new InvalidArgumentException(
                __(
                    "Options invalide. 'options' doit contenir un array ex: {0}",
                    '["--verbose" => "", "--test" => "test"]'
                )
            );
        }
        foreach ($options as $key => $value) {
            if (!preg_match('/-(-[\w-]+|[a-zA-Z])/', $key)) {
                throw new InvalidArgumentException(
                    __(
                        "Options invalide. Une option commence par un tiret"
                        . " ou un double tiret et est limité en caractères",
                    )
                );
            }
            if ($value === '') {
                $command .= ' ' . $key;
            } else {
                $command .= ' ' . $key . ' ' . escapeshellarg($value);
            }
        }

        $args = $this->getRequest()->getData('args') ?: [];
        if (!empty($args) && !is_array($args)) {
            throw new InvalidArgumentException(
                __(
                    "Arguments invalide. 'args' doit contenir un array ex: {0}",
                    '["arg1", "arg2"]'
                )
            );
        }
        foreach ($args as $value) {
            $command .= ' ' . escapeshellarg($value);
        }

        $timeout = $this->getRequest()->getData('timeout', 30);

        $stdout = tempnam(sys_get_temp_dir(), 'exec-output-');
        /** @var Exec $Exec */
        $Exec = Utility::get('Exec');
        $Exec->defaultStdout = $stdout;
        $handle = fopen($stdout, 'r');
        $beginTime = microtime(true);
        ob_implicit_flush();
        try {
            $pid = $Exec->async($command);
            $i = 0;
            usleep(10000); // NOSONAR on laisse un minimum de temps à la commande (évite d'attendre 1s)
            do {
                $i++;
                $newText = '';
                while (!feof($handle)) {
                    $newText .= fread($handle, 8192);
                }
                echo $i;
                $response = $response->withStringBody($i . $newText . PHP_EOL);
                $response->partialEmit();
                if (microtime(true) > ($beginTime + $timeout)) {
                    exec("kill $pid");
                    $response = $response->withStringBody('timeout!' . PHP_EOL);
                    $response->partialEmit();
                    break;
                }
            } while (Exec::runningAsync());
        } finally {
            fclose($handle);
            unlink($stdout);
        }

        return $response;
    }
}
