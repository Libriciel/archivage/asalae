<?php

/**
 * Asalae\Controller\CountersController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Entity\Sequence;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\SequencesTable;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use Cake\Http\Exception\BadRequestException;
use Exception;

/**
 * Accords de versement
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property CountersTable    $Counters
 * @property SequencesTable   $Sequences
 * @property OrgEntitiesTable $OrgEntities
 */
class CountersController extends AppController
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'counters-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'limit' => 100,
        'order' => [
            'Counters.identifier' => 'asc',
        ],
    ];

    /**
     * Liste des Compteurs
     *
     */
    public function index()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        $query = $this->Seal->archivalAgency()
            ->table('Counters')
            ->find('seal')
            ->orderBy(['Counters.identifier' => 'asc']);
        $IndexComponent->setQuery($query)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('active', IndexComponent::FILTER_IS)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * Action d'initialisation des compteurs
     * @throws Exception
     */
    public function initCounters()
    {
        $id = $this->archivalAgencyId;
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $sa = $OrgEntities->find()
            ->where(['OrgEntities.id' => $id])
            ->contain(['Counters'])
            ->firstOrFail();
        if ($sa->get('counters')) {
            throw new BadRequestException();
        }
        $OrgEntities->initCounters($id);
        return $this->renderDataToJson(['success' => true]);
    }

    /**
     * Edition d'un compteur
     * @param string $id
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var CountersTable $Counters */
        $Counters = $this->fetchTable('Counters');
        /** @var SequencesTable $Sequences */
        $Sequences = $this->fetchTable('Sequences');
        $entity = $this->Seal->archivalAgency()
            ->table('Counters')
            ->find('seal')
            ->where(['Counters.id' => $id])
            ->firstOrFail();
        $sequences = $Sequences->find()
            ->select(['Sequences.id', 'Sequences.name', 'Sequences.value'])
            ->where(['Sequences.org_entity_id' => $this->archivalAgencyId])
            ->orderBy(['Sequences.name' => 'asc']);
        $sequencesOptions = [];
        $sequencesValues = [];
        /** @var Sequence $sequence */
        foreach ($sequences as $sequence) {
            $sequencesOptions[$sequence->get('id')] = $sequence->get(
                'name'
            ) . ' (' . $sequence->get('value') . ')';
            $sequencesValues[$sequence->get('id')] = $sequence->get('value');
        }
        $this->set('sequencesOptions', $sequencesOptions);
        $this->set('sequencesValues', $sequencesValues);
        $this->set('entity', $entity);
        if ($this->getRequest()->is('put')) {
            $data = [
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'definition_mask' => $this->getRequest()->getData(
                    'definition_mask'
                ),
                'sequence_reset_mask' => $this->getRequest()->getData(
                    'sequence_reset_mask'
                ),
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($Counters, $entity, $data);
            if ($this->Modal->lastSaveIsSuccess()) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logEdit($entity);
            }
        }
    }

    /**
     * Visualisation d'un compteur
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $entity = $this->Seal->archivalAgency()
            ->table('Counters')
            ->find('seal')
            ->where(['Counters.id' => $id])
            ->contain(['Sequences'])
            ->firstOrFail();
        $this->set('entity', $entity);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($entity);
    }
}
