<?php

/**
 * Asalae\Controller\TimestampersController
 */

namespace Asalae\Controller;

use Asalae\Model\Table\TimestampersTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;

/**
 * Timestampers
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property TimestampersTable Timestampers
 */
class TimestampersController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default',
        ];
    }
}
