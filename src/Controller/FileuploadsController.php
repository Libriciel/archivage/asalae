<?php

/**
 * Asalae\Controller\FileuploadsController
 */

namespace Asalae\Controller;

use Asalae\Model\Entity\Fileupload;
use Asalae\Model\Table\FileuploadsTable;
use AsalaeCore\Controller\RenderDataTrait;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Fichiers uploadés
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FileuploadsTable $Fileuploads
 */
class FileuploadsController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * Permet de supprimer un fichier uploadé
     *      Suppression dans le dossier temporaire si Disponible
     *      Suppression en base
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $report = [];
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = $this->fetchTable('Fileuploads');
        $user_id = $this->userId;
        /** @var Fileupload $file */
        $file = $Fileuploads->find()
            ->where(['id' => $id, 'user_id' => $user_id])
            ->firstOrFail();
        $report['entity_deleted'] = $Fileuploads->delete($file);
        if (!$report['entity_deleted']) {
            throw new BadRequestException(
                __("Impossible de supprimer cet enregistrement")
            );
        }
        if (is_file($file->get('path'))) {
            Filesystem::remove($file->get('path'));
        }
        $report['file_deleted'] = !is_file($file->get('path'));
        return $this->renderDataToJson($report);
    }
}
