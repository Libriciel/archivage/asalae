<?php

/**
 * Asalae\Controller\TasksController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Table\BeanstalkJobsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\UsersTable;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory;
use Beanstalk\Command\WorkerCommand;
use Beanstalk\Model\Table\BeanstalkWorkersTable;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\Http\Response as CakeResponse;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Exception;
use Pheanstalk\PheanstalkInterface;

/**
 * Jobs en cours de l'utilisateur
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property BeanstalkJobsTable BeanstalkJobs
 * @property BeanstalkWorkersTable BeanstalkWorkers
 * @property OrgEntitiesTable OrgEntities
 * @property UsersTable Users
 */
class TasksController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_ADMIN = 'all-jobs';
    public const string TABLE_INDEX = 'index-jobs-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            "job_state = 'pending'" => 'desc',
            'priority' => 'asc',
            'BeanstalkJobs.id' => 'asc',
        ],
    ];

    /**
     * Liste des taches en cours/à faire
     */
    public function index()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index', ['model' => 'BeanstalkJobs']);

        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $query = $BeanstalkJobs->find()
            ->where(['user_id' => $this->userId]);
        $IndexComponent->setQuery($query)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('job_state', IndexComponent::FILTER_IN)
            ->filter('tube', IndexComponent::FILTER_IN);

        try {
            /** @var EntityInterface[] $all */
            $all = $this->paginateToResultSet($query)->toArray();
        } catch (NotFoundException) {
            $this->Flash->error(
                __("La page demandé n'existe plus, retour à la page 1")
            );
            return $this->redirect(
                ['controller' => 'Tasks', 'action' => __FUNCTION__]
            );
        }

        $this->set('all', $all);

        // counts
        $countReady = $BeanstalkJobs->find()
            ->where(
                [
                    'user_id' => $this->userId,
                    'job_state' => BeanstalkJobsTable::S_PENDING,
                ]
            )
            ->count();
        $this->set('countReady', $countReady);
        $countBuried = $BeanstalkJobs->find()
            ->where(
                [
                    'user_id' => $this->userId,
                    'job_state' => BeanstalkJobsTable::S_FAILED,
                ]
            )
            ->count();
        $this->set('countBuried', $countBuried);

        // Options
        $this->set('states', $BeanstalkJobs->options('job_state'));
        $workerParams = Configure::read('Beanstalk.workers');
        $workerTubes = array_keys($workerParams);
        $this->set('tubes', array_combine($workerTubes, $workerTubes));

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * Liste des taches en cours/à faire
     */
    public function admin()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index', ['model' => 'BeanstalkJobs']);

        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');

        $query = $this->Seal->archivalAgencyChilds()
            ->table('BeanstalkJobs')
            ->find('seal')
            ->where(
                [
                    'BeanstalkJobs.tube IN' => array_keys(
                        Configure::read('Beanstalk.workers', ['false'])
                    ),
                ]
            )
            ->contain(['Users']);
        $IndexComponent->setQuery($query)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('job_state', IndexComponent::FILTER_IN)
            ->filter('tube', IndexComponent::FILTER_IN)
            ->filter('user_id', IndexComponent::FILTER_IN);

        try {
            /** @var EntityInterface[] $all */
            $all = $this->paginateToResultSet($query)->toArray();
        } catch (NotFoundException) {
            $this->Flash->error(
                __("La page demandé n'existe plus, retour à la page 1")
            );
            return $this->redirect(
                ['controller' => 'Tasks', 'action' => __FUNCTION__]
            );
        }

        $this->set('all', $all);

        // options
        $this->set('states', $BeanstalkJobs->options('state'));
        $tubes = array_keys(WorkerCommand::availablesWorkers());
        $this->set('tubes', array_combine($tubes, $tubes));
        $users = $this->Seal->archivalAgency()
            ->table('BeanstalkJobs')
            ->find(
                'sealList',
                keyField: '_matchingData.Users.id',
                valueField: '_matchingData.Users.username',
                groupField: fn($v) => Hash::get($v, 'OrgEntities.name', ''),
            )
            ->select(['OrgEntities.name', 'Users.id', 'Users.username'])
            ->innerJoinWith('Users')
            ->orderBy(['OrgEntities.name' => 'asc', 'Users.username' => 'asc'])
            ->all();
        $this->set('users', $users);
    }

    /**
     * kick le job
     *
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function resume(string $id)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $BeanstalkJobs->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        $BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_RESUME);
        $BeanstalkJobs->saveOrFail($entity);
        $this->Modal->success();
        return $this->renderDataToJson($BeanstalkJobs->get($id));
    }

    /**
     * bury le job
     *
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function pause(string $id)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $BeanstalkJobs->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        $BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_PAUSE);
        $BeanstalkJobs->saveOrFail($entity);
        $this->Modal->success();
        return $this->renderDataToJson($BeanstalkJobs->get($id));
    }

    /**
     * delete le job
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function cancel(string $id)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $BeanstalkJobs->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        $BeanstalkJobs->deleteOrFail($entity);
        $this->Modal->success();

        return $this->renderDataToJson(['report' => 'done']);
    }

    /**
     * delete le job
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function ajaxCancel(string $id)
    {
        if (!$this->getRequest()->is('ajax')) {
            throw new Exception(__("Accès direct interdit"));
        }
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $BeanstalkJobs->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        $BeanstalkJobs->deleteOrFail($entity);
        $report = "done";

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * bury le job
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function ajaxPause(string $id)
    {
        if (!$this->getRequest()->is('ajax')) {
            throw new Exception(__("Accès direct interdit"));
        }
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $BeanstalkJobs->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        $BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_PAUSE);
        $BeanstalkJobs->saveOrFail($entity);
        $report = "done";
        $data = json_encode($entity);

        return $this->renderDataToJson(['report' => $report, 'data' => $data]);
    }

    /**
     * kick le job
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function ajaxResume(string $id)
    {
        if (!$this->getRequest()->is('ajax')) {
            throw new Exception(__("Accès direct interdit"));
        }
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');

        $entity = $BeanstalkJobs->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        $BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_RESUME);
        $BeanstalkJobs->saveOrFail($entity);
        $report = "done";
        $data = json_encode($entity);

        return $this->renderDataToJson(['report' => $report, 'data' => $data]);
    }

    /**
     * Suppression du tube
     *
     * @param string $tubeName
     * @throws Exception
     */
    public function deleteTube(string $tubeName)
    {
        if (!Factory\Utility::get('Beanstalk')->isConnected()) {
            $this->Flash->error(__d('tasks', "Server Beanstalk down"));
            $this->redirect($this->referer());
        }

        $this->fetchTable('BeanstalkJobs')
            ->deleteAll(
                [
                    'tube' => $tubeName,
                    'job_state !=' => BeanstalkJobsTable::S_WORKING,
                ]
            );
        $this->redirect($this->referer());
    }

    /**
     * Ajout d'un job beanstalk
     * Appel addForm | addCheck | addSave selon le cas
     */
    public function add()
    {
        $this->getRequest()->allowMethod('ajax');
        $this->viewBuilder()->setLayout('ajax');

        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $BeanstalkJobs->newEntity([], ['validate' => false]);

        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData('data');
            foreach ($this->getRequest()->getData('mix', []) as $d) {
                $data[$d['key']] = $d['value'];
            }
            $Beanstalk = Factory\Utility::get(
                'Beanstalk',
                $this->getRequest()->getData('tube', 'default')
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $data = Hash::filter($data);
            try {
                $data['id'] = $Beanstalk->emit(
                    $data,
                    $this->getRequest()->getData(
                        'priority',
                        PheanstalkInterface::DEFAULT_PRIORITY
                    ),
                    $this->getRequest()->getData(
                        'delay',
                        PheanstalkInterface::DEFAULT_DELAY
                    ),
                    $this->getRequest()->getData(
                        'ttr',
                        PheanstalkInterface::DEFAULT_TTR
                    )
                );
                $this->Modal->success();
                $data = $BeanstalkJobs->find()
                    ->where(['id' => $data['id']])
                    ->first();
                return $this->renderDataToJson($data);
            } catch (\Exception) {
                $this->Modal->fail();
            }
        }

        $this->set('entity', $entity);
        $tubes = array_keys(WorkerCommand::availablesWorkers());
        $this->set('tubes', array_combine($tubes, $tubes));

        $users = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find(
                'sealList',
                keyField: '_matchingData.Users.id',
                valueField: '_matchingData.Users.username',
                groupField: 'name',
            )
            ->innerJoinWith('Users')
            ->select(['OrgEntities.name', 'Users.id', 'Users.username'])
            ->orderBy(['OrgEntities.name', 'Users.username'])
            ->all();
        $this->set('users', $users);
    }

    /**
     * modification d'un job beanstalk
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        $this->getRequest()->allowMethod('ajax');
        $this->viewBuilder()->setLayout('ajax');

        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $BeanstalkJobs->get($id);
        if ($entity->get('job_state') === BeanstalkJobsTable::S_WORKING) {
            throw new ForbiddenException();
        }

        if ($this->getRequest()->is('put')) {
            $data = $this->getRequest()->getData('data');
            foreach ($this->getRequest()->getData('mix', []) as $d) {
                $data[$d['key']] = $d['value'];
            }
            $data = Hash::filter($data);

            $model = $fk = null;
            foreach ($data as $key => $value) {
                if (
                    $key !== 'user_id'
                    && is_numeric($value)
                    && preg_match('/^(\w+)_id$/', $key, $m)
                ) {
                    $modelName = Inflector::camelize(Inflector::pluralize($m[1]));
                    if (get_class($this->fetchTable($modelName)) !== Table::class) {
                        $model = $modelName;
                        $fk = (int)$value;
                        break;
                    }
                }
            }

            $BeanstalkJobs->patchEntity(
                $entity,
                [
                    'user_id' => $data['user_id'] ?? null,
                    'data' => $data,
                    'object_model' => $model,
                    'object_foreign_key' => $fk,
                ] + $this->getRequest()->getData()
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($BeanstalkJobs->save($entity)) {
                $this->Modal->success();
                return $this->renderDataToJson($entity);
            } else {
                $this->Modal->fail();
            }
        }

        $this->set('entity', $entity);
        $workerParams = Configure::read('Beanstalk.workers');
        $workerTubes = array_keys($workerParams);
        $this->set('tubes', array_combine($workerTubes, $workerTubes));

        $users = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find(
                'sealList',
                keyField: '_matchingData.Users.id',
                valueField: '_matchingData.Users.username',
                groupField: 'name',
            )
            ->select(['OrgEntities.name', 'Users.id', 'Users.username'])
            ->innerJoinWith('Users')
            ->orderBy(['OrgEntities.name', 'Users.username'])
            ->all();
        $this->set('users', $users);
    }

    /**
     * Permet d'obtenir les infos d'un job au format json
     * @param string $id
     * @return Response
     */
    public function myJobInfo(string $id)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $job = $BeanstalkJobs->find()
            ->where(
                [
                    'id' => $id,
                    'user_id' => $this->userId,
                ]
            )
            ->first();
        return $this->renderDataToJson($job);
    }

    /**
     * Permet d'obtenir les infos d'un job au format json (pour les admins)
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function adminJobInfo(string $id)
    {
        $job = $this->Seal->archivalAgencyChilds()
            ->table('BeanstalkJobs')
            ->find('seal')
            ->innerJoinWith('Users')
            ->innerJoinWith('Users.OrgEntities')
            ->where(['BeanstalkJobs.id' => $id])
            ->first();
        return $this->renderDataToJson($job);
    }

    /**
     * Donne le contenu du tableau de jobs
     * @return CakeResponse
     * @throws Exception
     */
    public function getTubesInfo()
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        /** @var BeanstalkWorkersTable $BeanstalkWorkers */
        $BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers');

        $workerParams = Configure::read('Beanstalk.workers');
        $workerTubes = array_keys($workerParams);
        sort($workerTubes);
        $tubes = [];
        foreach ($workerTubes as $tube) {
            $q = $BeanstalkJobs->query();
            $jobStates = $BeanstalkJobs->find()
                ->select(
                    [
                        'job_state',
                        'count' => $q->func()->count('*'),
                    ]
                )
                ->where(['tube' => $tube])
                ->groupBy(['job_state'])
                ->toArray();
            $workerCount = $BeanstalkWorkers->find()
                ->where(['tube' => $tube])
                ->count();
            $tubes[$tube] = [
                'tube' => $tube,
                'workers' => $workerCount,
                BeanstalkJobsTable::S_WORKING => 0,
                BeanstalkJobsTable::S_PENDING => 0,
                BeanstalkJobsTable::S_DELAYED => 0,
                BeanstalkJobsTable::S_PAUSED => 0,
                BeanstalkJobsTable::S_FAILED => 0,
            ];
            foreach ($jobStates as $state) {
                $tubes[$tube][$state->get('job_state')] = $state->get('count');
            }
        }
        sort($tubes);
        return $this->renderDataToJson(array_values($tubes));
    }
}
