<?php

/**
 * Asalae\Controller\DeliveryRequestsController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Entity\DeliveryRequest;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsDeliveryRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\DeliveryRequestsTable;
use Asalae\Model\Table\KeywordsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationProcessesTable;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\CallbackStream;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\LimitBreak;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Pheanstalk\Pheanstalk;
use Psr\Http\Message\ResponseInterface;
use Seda2Pdf\Seda2Pdf;

/**
 * DeliveryRequests
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveUnitsDeliveryRequestsTable ArchiveUnitsDeliveryRequests
 * @property ArchiveUnitsTable                 ArchiveUnits
 * @property CountersTable                     Counters
 * @property DeliveryRequestsTable             DeliveryRequests
 * @property OrgEntitiesTable                  OrgEntities
 * @property ValidationChainsTable             ValidationChains
 * @property ValidationProcessesTable          ValidationProcesses
 */
class DeliveryRequestsController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'DeliveryRequests.id' => 'desc',
        ],
    ];

    /**
     * Liste de mes demandes de communication en préparation
     */
    public function indexPreparating()
    {
        $query = $this->Seal->archivalAgency()
            ->table('DeliveryRequests')
            ->find('seal')
            ->where(
                [
                    'DeliveryRequests.state' => DeliveryRequestsTable::S_CREATING,
                    'DeliveryRequests.created_user_id' => $this->userId,
                ]
            );
        $this->indexCommons($query);
    }

    /**
     * Liste les enregistrements
     * @param Query $query
     * @return Response|void
     * @throws Exception
     */
    private function indexCommons(Query $query)
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');

        $query->contain(['ArchiveUnits', 'CreatedUsers']);

        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        $subquery = $DeliveryRequests->selectQuery()
            ->select(['dr.id'])
            ->from(['dr' => 'delivery_requests'])
            ->innerJoin(
                ['lk' => 'archive_units_delivery_requests'],
                ['lk.delivery_request_id' => new IdentifierExpression('dr.id')]
            )
            ->innerJoin(
                ['au' => 'archive_units'],
                ['au.id' => new IdentifierExpression('lk.archive_unit_id')]
            )
            ->where(
                ['dr.id' => new IdentifierExpression('DeliveryRequests.id')]
            )
            ->limit(1);

        $IndexComponent->setQuery($query)
            ->filter(
                'archive_unit_identifier',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike(
                            'au.archival_agency_identifier',
                            $v
                        )
                    );
                    return $q->andWhere(['DeliveryRequests.id IN' => $sq]);
                }
            )
            ->filter(
                'archive_unit_name',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike('au.name', $v)
                    );
                    return $q->andWhere(['DeliveryRequests.id IN' => $sq]);
                }
            )
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter(
                'created_user',
                IndexComponent::FILTER_IN,
                'DeliveryRequests.created_user_id'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('state', IndexComponent::FILTER_IN);

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/commons');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) use ($ArchiveUnits) {
                    // effectue un contain avec limite individuelle d'enregistrements
                    $aus = $ArchiveUnits->find()
                        ->innerJoinWith('ArchiveUnitsDeliveryRequests')
                        ->where(['delivery_request_id' => $e->id])
                        ->orderBy(['archival_agency_identifier'])
                        ->limit(10)
                        ->toArray();
                    $e->set('archive_units', $aus);
                    return $e;
                }
            )
            ->toArray();
        $this->set('data', $data);

        // override $IndexComponent->init()
        $viewBuilder = $this->viewBuilder();
        if (
            $this->getRequest()->is('ajax') && $viewBuilder->getLayout() === null
        ) {
            $viewBuilder->setTemplate('ajax_index');
        }

        // options
        $this->set('states', $DeliveryRequests->options('state'));

        $created_users = $DeliveryRequests->find(
            'list',
            keyField: '_matchingData.CreatedUsers.id',
            valueField: '_matchingData.CreatedUsers.username',
            groupField: '_matchingData.OrgEntities.name',
        )
            ->select(
                ['CreatedUsers.id', 'CreatedUsers.username', 'OrgEntities.name']
            )
            ->innerJoinWith('CreatedUsers')
            ->innerJoinWith('CreatedUsers.OrgEntities')
            ->distinct(
                $distinct = [
                    'OrgEntities.name',
                    'CreatedUsers.username',
                ]
            )
            ->where(
                ['DeliveryRequests.archival_agency_id' => $this->archivalAgencyId]
            )
            ->orderBy($distinct);
        $this->set('created_users', $created_users);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * Liste de mes demandes de communication
     */
    public function indexMy()
    {
        $query = $this->Seal->archivalAgency()
            ->table('DeliveryRequests')
            ->find('seal')
            ->where(
                [
                    'DeliveryRequests.created_user_id' => $this->userId,
                ]
            );
        $this->indexCommons($query);
    }

    /**
     * Liste de toutes les demandes de communications
     */
    public function indexAll()
    {
        $conditions = ['DeliveryRequests.state !=' => 'creating'];
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (
            $this->orgEntityId != $this->archivalAgencyId
            && !in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)
        ) {
            $conditions['DeliveryRequests.requester_id'] = $this->orgEntityId;
        }
        $query = $this->Seal->archivalAgency()
            ->table('DeliveryRequests')
            ->find('seal')
            ->where($conditions);
        $this->indexCommons($query);
    }

    /**
     * Action d'ajout
     */
    public function add()
    {
        $request = $this->getRequest();
        if ($request->getQueryParams()) {
            $query = $this->queryArchiveUnit();
            $count = $query->count();
            if ($count === 0) {
                throw new BadRequestException(__("Panier vide"));
            }
            $basket = $query->select(['ArchiveUnits.id'], true);
            $limit = Configure::read('DeliveryRequests.max_basket_size', 1000);
            if ($basket->count() > $limit) {
                $this->Flash->error(
                    __(
                        "Le nombre d'unités d'archives correspondant " .
                        "aux filtres excède la limite de {0}. Veuillez " .
                        "affiner davantage votre sélection.",
                        $limit
                    )
                );
                $this->viewBuilder()->setTemplate('add');
                $this->set('fatalError', true);
                return;
            }
            $memoryRequired = $count * 100000; // 140k archive_units ~= 1.4Go de RAM
            LimitBreak::setMemoryLimit($memoryRequired);
        } elseif (!$this->cookies || empty($this->cookies['catalog_basket'])) {
            throw new BadRequestException(__("Panier vide"));
        } else {
            $basket = explode(',', $this->cookies['catalog_basket']);
        }
        $archiveUnits = $this->extractFilteredArchiveUnits($basket);
        $keys = $request->getData('archive_units._ids')
            ?: array_keys(
                $archiveUnits
            );

        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)) {
            $requester = $this->archivalAgency;
        } else {
            $requester = $this->orgEntity;
        }
        $this->set('requester', $requester);

        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        $entity = $DeliveryRequests->newEntity(
            [
                'archive_units' => [
                    '_ids' => $keys,
                ],
            ],
            ['validate' => false]
        );
        $derogation = $DeliveryRequests->derogationIsNeeded($keys);
        $chooseValidationChain = Hash::get(
            $requester,
            'id'
        ) === $this->archivalAgencyId;
        $this->set('chooseValidationChain', $chooseValidationChain);

        if ($request->is('post')) {
            /** @var CountersTable $Counters */
            $Counters = $this->fetchTable('Counters');
            $data = [
                'identifier' => $Counters->next(
                    $this->archivalAgencyId,
                    'ArchiveDeliveryRequest'
                ),
                'archival_agency_id' => $this->archivalAgencyId,
                'requester_id' => $this->orgEntityId === $this->archivalAgencyId
                    ? $request->getData('requester_id', $requester['id'])
                    : $requester['id'],
                'state' => $DeliveryRequests->initialState,
                'derogation' => $derogation,
                'comment' => $request->getData('comment'),
                'created_user_id' => $this->userId,
                'archive_units' => [
                    '_ids' => $request->getData('archive_units._ids'),
                ],
            ];
            if ($derogation && $chooseValidationChain) {
                $validationChainId = $request->getData('validation_chain_id');

                $chain = $this->Seal->archivalAgency()
                    ->table('ValidationChains')
                    ->find('seal')
                    ->where(
                        [
                            'ValidationChains.id' => $validationChainId,
                            'ValidationChains.app_type' => ValidationChainsTable::ARCHIVE_DELIVERY_REQUEST,
                            'ValidationChains.app_meta LIKE' => '%"active":true%',
                        ]
                    )
                    ->firstOrFail();

                $chain->setVirtual(['usable']);
                if ((!$chain->get('usable'))) {
                    throw new NotFoundException();
                }

                $data['validation_chain_id'] = $validationChainId;
            }
            $DeliveryRequests->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($DeliveryRequests->save($entity)) {
                $this->Modal->success();
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAction($entity);
                $send = $request->getData('send') ? 'true' : 'false';
                $cookies = json_decode(Hash::get($this->user, 'cookies'), true);
                unset($cookies['catalog_basket']);
                if ($this->user instanceof EntityInterface) {
                    $this->user->set('cookies', json_encode($cookies));
                } else {
                    $this->user['cookies'] = json_encode($cookies);
                }
                $this->fetchTable('Users')->updateAll(
                    ['cookies' => json_encode($cookies)],
                    ['id' => Hash::get($this->user, 'id')]
                );
                return $this->renderJson(json_encode($entity->toArray()))
                    ->withHeader('X-Asalae-Callback-Send', $send);
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
        $this->set('derogation', $derogation);
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $session = $request->getSession();
        $defaultChain = $session->read(
            'ConfigArchivalAgency.delivery-request-derogation-chain'
        ) ?: 'default';
        if ($defaultChain === 'default') {
            $defaultChain = $ValidationChains->find(
                'default',
                app_type: ValidationChainsTable::ARCHIVE_DELIVERY_REQUEST,
                org_entity_id: $this->archivalAgencyId,
            )->firstOrFail()->get('id');
        }
        $this->set('defaultChain', $defaultChain);

        // options
        $this->set('archiveUnits', $archiveUnits);
        $chains = $ValidationChains->findListOptions(
            $this->archivalAgencyId,
            $ValidationChains::ARCHIVE_DELIVERY_REQUEST
        );
        $this->set('chains', $chains);

        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $requesters = $OrgEntities->listOptions([], $this->Seal->archivalAgencyChilds())
            ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_REQUESTER_AGENCIES])
            ->orderBy(['OrgEntities.name' => 'asc']);
        $this->set('requesters', $requesters->all());
        $this->viewBuilder()->setTemplate('add');
    }

    /**
     * Donne le query avec ses filtres
     * @return Query
     * @throws Exception
     */
    private function queryArchiveUnit()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index', ['model' => 'ArchiveUnits']);
        $ConditionComponent = $IndexComponent->Condition;
        $query = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal');
        $ids = [];
        $request = $this->getRequest();
        if ($filenames = $request->getQuery('filename')) {
            /** @var ArchiveBinariesTable $ArchiveBinaries */
            $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
            $ids = $ArchiveBinaries->selectQuery()
                ->select(['id' => 'au.archive_id'])
                ->from(['ab' => 'archive_binaries'])
                ->innerJoin(
                    ['lnk' => 'archive_binaries_archive_units'],
                    ['lnk.archive_binary_id' => new IdentifierExpression('ab.id')]
                )
                ->innerJoin(
                    ['au' => 'archive_units'],
                    ['lnk.archive_unit_id' => new IdentifierExpression('au.id')]
                )
                ->limit(50000)
                ->disableHydration();
            foreach ((array)$filenames as $filename) {
                $ids->andWhere(
                    $ConditionComponent->ilike('ab.filename', $filename)
                );
            }
            $ids = $ids
                ->all()
                ->map(
                    function ($e) {
                        return $e['id'];
                    }
                )
                ->toArray();
        }
        $query = $query
            ->where(
                [
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                    'Archives.state IN' => [ArchivesTable::S_AVAILABLE, ArchivesTable::S_FREEZED],
                    'ArchiveUnits.state IN' => [ArchiveUnitsTable::S_AVAILABLE, ArchiveUnitsTable::S_FREEZED],
                ]
            )
            ->contain(
                [
                    'Archives' => [
                        'AccessRules' => ['AccessRuleCodes'],
                        'Agreements',
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'OriginatingAgencies',
                        'Profiles',
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                        'TransferringAgencies',
                    ],
                ]
            );
        if (!$request->getQuery('all') && !$request->getQuery('search')) {
            $query->where(['ArchiveUnits.parent_id IS' => null]);
        }
        $IndexComponent->setQuery($query)
            ->filter('agreement_id', null, 'Archives.agreement_id')
            ->filter('archival_agency_identifier', IndexComponent::FILTER_ILIKE)
            ->filter(
                'favoris',
                function () {
                    $request = $this->getRequest();
                    $cookies = $request->getCookieParams();
                    $sessionCookies = $this->cookies;
                    if (is_array($sessionCookies)) {
                        $cookies = array_merge($cookies, $sessionCookies);
                    }
                    $cookies = array_filter($cookies, fn ($v) => $v !== '0');
                    $favoritesIds = [];
                    $regex = "/^table-favorite-archive-units-catalog-table-(\d+)$/";
                    foreach (array_keys($cookies) as $cookieName) {
                        if (preg_match($regex, $cookieName, $match)) {
                            $favoritesIds[] = $match[1];
                        }
                    }
                    if (empty($favoritesIds)) {
                        return [];
                    }
                    return ['ArchiveUnits.id IN' => $favoritesIds];
                }
            )
            ->filter('id', IndexComponent::FILTER_IN)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter(
                'originating_agency_id',
                null,
                'Archives.originating_agency_id'
            )
            ->filter('profile_id', null, 'Archives.profile_id')
            ->filter(
                'transferring_agency_id',
                null,
                'Archives.transferring_agency_id'
            )
            ->filter(
                'created',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.created'
            )
            ->filter('original_total_size', IndexComponent::FILTER_SIZEOPERATOR)
            ->filter(
                'original_total_count',
                IndexComponent::FILTER_COUNTOPERATOR
            )
            ->filter(
                'keyword',
                function ($value, Query $q) use ($ConditionComponent) {
                    /** @var KeywordsTable $Keywords */
                    $Keywords = $this->fetchTable('Keywords');
                    $subquery = $Keywords->selectQuery()
                        ->select(['existing' => 'true'])
                        ->from(['k' => 'archive_keywords'])
                        ->innerJoin(
                            ['au' => 'archive_units'],
                            ['au.id' => new IdentifierExpression('k.archive_unit_id')]
                        )
                        ->where(
                            $ConditionComponent->ilike('k.content', $value)
                        )
                        ->andWhere(
                            [
                                'au.lft >=' => new IdentifierExpression('ArchiveUnits.lft'),
                                'au.rght <=' => new IdentifierExpression('ArchiveUnits.rght'),
                            ]
                        )
                        ->limit(1);
                    return $q->where([$subquery]);
                }
            )
            ->filter(
                'filename',
                function ($value, Query $q) use ($ids, $ConditionComponent) {
                    return $q->where(
                        $ConditionComponent->in('Archives.id', $ids)
                    );
                }
            )
            ->filter(
                'transferring_agency_identifier',
                IndexComponent::FILTER_ILIKE,
                'ArchiveUnits.transferring_agency_identifier'
            )
            ->filter(
                'originating_agency_identifier',
                IndexComponent::FILTER_ILIKE,
                'ArchiveUnits.originating_agency_identifier'
            )
            ->filter(
                'oldest_date',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.oldest_date'
            )
            ->filter(
                'latest_date',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.latest_date'
            )
            ->filter(
                'keyword_reference',
                function ($value, Query $q) use ($ConditionComponent) {
                    /** @var KeywordsTable $Keywords */
                    $Keywords = $this->fetchTable('Keywords');
                    $subquery = $Keywords->selectQuery()
                        ->select(['existing' => 'true'])
                        ->from(['k' => 'archive_keywords'])
                        ->innerJoin(
                            ['au' => 'archive_units'],
                            ['au.id' => new IdentifierExpression('k.archive_unit_id')]
                        )
                        ->where($ConditionComponent->ilike('k.reference', $value))
                        ->andWhere(
                            [
                                'au.lft >=' => new IdentifierExpression('ArchiveUnits.lft'),
                                'au.rght <=' => new IdentifierExpression('ArchiveUnits.rght'),
                            ]
                        )
                        ->limit(1);
                    return $q->where([$subquery]);
                }
            );

        if ($request->getQuery('search')) {
            $this->loadComponent('SearchEngine', ['model' => 'ArchiveUnits']);
            $this->SearchEngine->query($query);
            $query->orderBy([], true);
        }
        return $query;
    }

    /**
     * Donne la liste filtrée des archive units du panier
     * @param array|Query $basket
     * @return array
     * @throws Exception
     */
    private function extractFilteredArchiveUnits($basket): array
    {
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $ArchiveUnits->find()
            ->select(
                [
                    'ArchiveUnits.id',
                    'ArchiveUnits.name',
                    'ArchiveUnits.archival_agency_identifier',
                ]
            )
            ->innerJoinWith('Archives')
            ->where(
                [
                    'ArchiveUnits.id IN' => $basket,
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->orderBy(['ArchiveUnits.archival_agency_identifier'])
            ->disableHydration();
        $collection = $ArchiveUnits->filterChilds($query);
        $archiveUnits = [];
        $keys = [];
        foreach ($collection as $archiveUnit) {
            $archiveUnits[$archiveUnit['id']] = $archiveUnit['archival_agency_identifier']
                . ' - ' . $archiveUnit['name'];
            $keys[] = $archiveUnit['id'];
        }
        if (!$keys || (is_array($basket) && (count($basket) !== $query->count()))) {
            throw new BadRequestException(__("Panier invalide"));
        }
        return $archiveUnits;
    }

    /**
     * Action modifier
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        $request = $this->getRequest();
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)) {
            $requester = $this->archivalAgency;
        } else {
            $requester = $this->orgEntity;
        }
        $this->set('requester', $requester);

        $entity = $this->Seal->archivalAgency()
            ->table('DeliveryRequests')
            ->find('seal')
            ->where(
                [
                    'DeliveryRequests.id' => $id,
                    'DeliveryRequests.created_user_id' => $this->userId,
                ]
            )
            ->contain(
                ['ArchiveUnits']
            )
            ->firstOrFail();
        if ($entity->get('state') !== DeliveryRequestsTable::S_CREATING) {
            throw new ForbiddenException();
        }
        $keys = Hash::extract($entity, 'archive_units.{n}.id');
        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        $derogation = $DeliveryRequests->derogationIsNeeded($keys);
        $chooseValidationChain = Hash::get(
            $requester,
            'id'
        ) === $this->archivalAgencyId;
        $this->set('chooseValidationChain', $chooseValidationChain);

        if ($request->is('put')) {
            $data = [
                'archival_agency_id' => $this->archivalAgencyId,
                'requester_id' => $this->orgEntityId === $this->archivalAgencyId
                    ? $request->getData('requester_id', $requester['id'])
                    : $requester['id'],
                'derogation' => $derogation,
                'comment' => $request->getData('comment'),
                'archive_units' => [
                    '_ids' => $request->getData('archive_units._ids'),
                ],
            ];
            if ($derogation && $chooseValidationChain) {
                $validationChainId = $request->getData('validation_chain_id');

                $chain = $this->Seal->archivalAgency()
                    ->table('ValidationChains')
                    ->find('seal')
                    ->where(
                        [
                            'ValidationChains.id' => $validationChainId,
                            'ValidationChains.app_type' => ValidationChainsTable::ARCHIVE_DELIVERY_REQUEST,
                            'ValidationChains.app_meta LIKE' => '%"active":true%',
                        ]
                    )
                    ->firstOrFail();

                $chain->setVirtual(['usable']);
                if ((!$chain->get('usable'))) {
                    throw new NotFoundException();
                }

                $data['validation_chain_id'] = $validationChainId;
            }
            $DeliveryRequests->patchEntity(
                $entity,
                $data + $entity->toArray()
            );
            if (empty($request->getData('archive_units._ids'))) {
                $entity->setError(
                    'archive_units',
                    __(
                        "Une demande de communication doit avoir au moins une unité d'archive"
                    )
                );
            }
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($DeliveryRequests->save($entity)) {
                $this->Modal->success();
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAction($entity);
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
        $this->set('derogation', $derogation);
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $session = $request->getSession();
        $defaultChain = $session->read(
            'ConfigArchivalAgency.delivery-request-derogation-chain'
        ) ?: 'default';
        if ($defaultChain === 'default') {
            $defaultChain = $ValidationChains->find(
                'default',
                app_type: ValidationChainsTable::ARCHIVE_DELIVERY_REQUEST,
                org_entity_id: $this->archivalAgencyId,
            )->firstOrFail()->get('id');
        }
        $this->set('defaultChain', $defaultChain);

        // options
        $archiveUnits = [];
        /** @var EntityInterface $archiveUnit */
        foreach ($entity->get('archive_units') as $archiveUnit) {
            $archiveUnits[$archiveUnit->id] = $archiveUnit->get('name');
        }
        $this->set('archiveUnits', $archiveUnits);
        $chains = $ValidationChains->findListOptions(
            $this->archivalAgencyId,
            $ValidationChains::ARCHIVE_DELIVERY_REQUEST
        );
        $this->set('chains', $chains);

        if ($chooseValidationChain) {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = $this->fetchTable('OrgEntities');
            $requesters = $OrgEntities->listOptions([], $this->Seal->archivalAgencyChilds())
                ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_REQUESTER_AGENCIES])
                ->orderBy(['OrgEntities.name' => 'asc']);
            $this->set('requesters', $requesters->all());
        }
    }

    /**
     * Action supprimer
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Seal->archivalAgency()
            ->table('DeliveryRequests')
            ->find('seal')
            ->where(
                [
                    'DeliveryRequests.id' => $id,
                    'DeliveryRequests.created_user_id' => $this->userId,
                ]
            )
            ->firstOrFail();

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($entity);
        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        $report = $DeliveryRequests->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Donne en json selon une liste d'archive units si une dérogation est nécessaire
     * @return Response
     * @throws Exception
     */
    public function derogationNeeded()
    {
        $request = $this->getRequest();
        $request->allowMethod('post');

        $units = explode(',', $request->getData('archive_units_ids'));
        $archiveUnits = $this->extractFilteredArchiveUnits($units);
        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        return $this->renderDataToJson(
            [
                'derogation' => $DeliveryRequests->derogationIsNeeded(
                    array_keys($archiveUnits)
                ),
            ]
        );
    }

    /**
     * Action de visualisation
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $query = $this->Seal->archivalAgency()
            ->table('DeliveryRequests')
            ->find('seal')
            ->where(['DeliveryRequests.id' => $id]);

        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (
            $this->orgEntityId != $this->archivalAgencyId
            && !in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)
        ) {
            $query->where(
                ['DeliveryRequests.requester_id' => $this->orgEntityId]
            );
        }
        $entity = $query
            ->contain(
                [
                    'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q
                            ->orderBy(['ArchiveUnits.archival_agency_identifier'])
                            ->limit($AjaxPaginatorComponent->getConfig('limit'))
                            ->contain(
                                [
                                    'Archives' => ['DescriptionXmlArchiveFiles'],
                                    'ArchiveDescriptions',
                                ]
                            );
                    },
                    'CreatedUsers' => [
                        'OrgEntities',
                    ],
                    'Requesters',
                    'ValidationProcesses' => [
                        'ValidationChains',
                        'CurrentStages' => [
                            'ValidationActors',
                        ],
                        'ValidationHistories' => [
                            'sort' => [
                                'ValidationHistories.created' => 'asc',
                                'ValidationHistories.id' => 'asc',
                            ],
                            'ValidationActors' => [
                                'Users',
                                'ValidationStages',
                            ],
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        /** @var EntityInterface[] $archive_units */
        $archive_units = Hash::get($entity, 'archive_units');
        foreach ($archive_units as $archiveUnit) {
            $archiveUnit->setVirtual(['description', 'dates']);
        }
        $this->set('entity', $entity);

        /** @var ArchiveUnitsDeliveryRequestsTable $ArchiveUnitsDeliveryRequests */
        $ArchiveUnitsDeliveryRequests = $this->fetchTable('ArchiveUnitsDeliveryRequests');
        $unitsCount = $ArchiveUnitsDeliveryRequests->find()
            ->where(['delivery_request_id' => $id])
            ->count();
        $this->set('unitsCount', $unitsCount);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($entity);
        $AjaxPaginatorComponent->setViewPaginator($entity->get('archive_units') ?: [], $unitsCount);
    }

    /**
     * Pagination des archive units dans la visualisation
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function paginateArchiveUnits(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $ArchiveUnits->find()
            ->innerJoinWith('ArchiveUnitsDeliveryRequests')
            ->innerJoinWith('Archives')
            ->where(
                [
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                    'ArchiveUnitsDeliveryRequests.delivery_request_id' => $id,
                    'ArchiveUnitsDeliveryRequests.archive_unit_id'
                    => new IdentifierExpression('ArchiveUnits.id'),
                ]
            );
        return $AjaxPaginatorComponent->json(
            $query,
            (bool)$this->getRequest()->getQuery('count')
        );
    }

    /**
     * Envoi de la demande de communication
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function send(string $id)
    {
        /** @var DeliveryRequest $deliveryRequest */
        $deliveryRequest = $this->Seal->archivalAgency()
            ->table('DeliveryRequests')
            ->find('seal')
            ->where(
                [
                    'DeliveryRequests.id' => $id,
                    'DeliveryRequests.created_user_id' => $this->userId,
                ]
            )
            ->contain(
                [
                    'ArchiveUnits',
                    'ArchivalAgencies',
                    'Requesters',
                ]
            )
            ->firstOrFail();
        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        if (
            !$DeliveryRequests->transition(
                $deliveryRequest,
                DeliveryRequestsTable::T_SEND
            )
        ) {
            throw new ForbiddenException();
        }
        /** @var Notify $Notify */
        $Notify = Utility::get('Notify');

        $request = $this->getRequest();
        $session = $request->getSession();
        $validationChain = ($deliveryRequest->get('derogation')
            ? $session->read(
                'ConfigArchivalAgency.delivery-request-derogation-chain'
            )
            : $session->read(
                'ConfigArchivalAgency.delivery-request-no-derogation-chain'
            )
        ) ?: 'default';
        if ($deliveryRequest->get('validation_chain_id')) {
            $validationChain = $deliveryRequest->get('validation_chain_id');
        }

        $filename = sprintf(
            '%s_delivery_request.xml',
            $deliveryRequest->get('identifier')
        );
        $deliveryRequest->set('filename', $filename);

        if ($validationChain === 'auto') {
            $DeliveryRequests->transitionOrFail(
                $deliveryRequest,
                DeliveryRequestsTable::T_ACCEPT
            );
            $DeliveryRequests->saveOrFail($deliveryRequest);
            $xml = $deliveryRequest->generateXml();
            Filesystem::dumpFile($deliveryRequest->get('xml'), $xml);

            $message = __(
                "La demande de communication {0} a été validé automatiquement",
                $deliveryRequest->get('identifier')
            );
            $notifyClass = 'alert-success';
            $Notify->send(
                $this->userId,
                [$session->read('Session.token')],
                $message,
                $notifyClass
            );
            /** @var Beanstalk $Beanstalk */
            $Beanstalk = Utility::get('Beanstalk');
            $Beanstalk->setTube('delivery');
            $Beanstalk->emit(
                [
                    'user_id' => $this->userId,
                    'delivery_request_id' => $deliveryRequest->id,
                ],
                Pheanstalk::DEFAULT_PRIORITY,
                Pheanstalk::DEFAULT_DELAY,
                3600 // on part du principe qu'un fichier peut mettre 1h à se télécharger
            );
            return $this->renderDataToJson('done');
        }

        if ($validationChain === 'default') {
            /** @var ValidationChainsTable $ValidationChains */
            $ValidationChains = $this->fetchTable('ValidationChains');
            $validationChain = $ValidationChains->find(
                'default',
                app_type: ValidationChainsTable::ARCHIVE_DELIVERY_REQUEST,
                org_entity_id: $this->archivalAgencyId,
            )->firstOrFail()->get('id');
        }
        $params = [
            'user_id' => $this->userId,
            'org_entity_id' => $this->archivalAgencyId,
            'validation_chain_id' => $validationChain,
        ];
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $process = $ValidationProcesses->createNewProcess(
            $deliveryRequest,
            $params
        );
        $conn = $DeliveryRequests->getConnection();
        $conn->begin();
        $success = $ValidationProcesses->save($process)
            && $DeliveryRequests->save($deliveryRequest);
        $xml = $deliveryRequest->generateXml();
        Filesystem::dumpFile($deliveryRequest->get('xml'), $xml);
        if ($success) {
            $ValidationProcesses->notifyProcess($process);
            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logValidationSend($deliveryRequest);
            $conn->commit();
            $message = __(
                "La demande de communication {0} a bien été envoyée.",
                $deliveryRequest->get('identifier')
            );
            $notifyClass = 'alert-warning';
        } else {
            $conn->rollback();
            FormatError::logEntityErrors($process);
            FormatError::logEntityErrors($deliveryRequest);
            $message = __(
                "Une erreur a eu lieu lors de l'envoi de la demande de communication {0}",
                $deliveryRequest->get('identifier')
            );
            $notifyClass = 'alert-danger';
        }
        $Notify->send(
            $this->userId,
            [$session->read('Session.token')],
            $message,
            $notifyClass
        );
        if (!$success) {
            throw new BadRequestException();
        }
    }

    /**
     * Donne le fichier PDF lié à un transfert
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function downloadPdf(string $id): Response
    {
        /** @var DeliveryRequest $entity */
        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        $entity = $DeliveryRequests->find()
            ->where(
                [
                    'id' => $id,
                    'archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $xml = $entity->get('xml');
        $generator = new Seda2Pdf($xml);
        $pdf = tempnam(sys_get_temp_dir(), uniqid('seda2pdf-'));
        $generator->generate($pdf);

        if ($sessionFile = $this->getRequest()->getQuery('sessionFile')) {
            Notify::emit(
                'download_file',
                [
                    'uuid' => $sessionFile,
                    'state' => 'downloading',
                    'last_update' => time(),
                ]
            );
        }

        return $this->getResponse()
            ->withHeader('Content-Type', 'application/pdf')
            ->withHeader('Content-Length', filesize($pdf))
            ->withHeader('File-Size', filesize($pdf))
            ->withHeader(
                'Content-Disposition',
                'attachment; filename="' . $entity->get('pdf_basename') . '"'
            )
            ->withBody(
                new CallbackStream(
                    function () use ($pdf, $sessionFile) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        readfile($pdf);
                        unlink($pdf);
                        if ($sessionFile) {
                            Notify::emit(
                                'download_file',
                                [
                                    'uuid' => $sessionFile,
                                    'state' => 'finished',
                                    'last_update' => time(),
                                    'integrity' => true,
                                ]
                            );
                        }
                    }
                )
            );
    }
}
