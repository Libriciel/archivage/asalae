<?php

/**
 * Asalae\Controller\KeywordsController
 */

namespace Asalae\Controller;

use Asalae\Model\Entity\Fileupload;
use Asalae\Model\Entity\Keyword;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\KeywordListsTable;
use Asalae\Model\Table\KeywordsTable;
use Asalae\Model\Table\UsersTable;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\LimitBreak;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Exception;

/**
 * Mots-clés (thésaurus)
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property KeywordsTable $Keywords
 * @property KeywordListsTable $KeywordLists
 * @property UsersTable $Users
 * @property FileuploadsTable $Fileuploads
 */
class KeywordsController extends AppController
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'keywords-table';
    public const string TABLE_UPLOAD = 'import-keywords-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'Keywords.id' => 'asc',
        ],
    ];

    /**
     * Liste des mots-clés
     * @param string $keyword_list_id
     * @param string $version
     * @return Response|void
     * @throws Exception
     */
    public function index(string $keyword_list_id, string $version = '')
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        $list = $this->Seal->archivalAgency()
            ->table('KeywordLists')
            ->find('seal')
            ->where(['KeywordLists.id' => $keyword_list_id])
            ->firstOrFail();
        $this->set('list', $list);
        $existingWorkspace = $list->get('has_workspace');
        $this->set('existingWorkspace', $existingWorkspace);
        if ($version === '') {
            $version = $existingWorkspace
                ? 0
                : new IdentifierExpression('KeywordLists.version');
        }
        $this->set(
            'version',
            (int)(is_numeric($version) ? $version : $list->get('version'))
        );
        /** @var KeywordsTable $Keywords */
        $Keywords = $this->fetchTable('Keywords');
        $query = $Keywords->find()
            ->where(
                [
                    'Keywords.keyword_list_id' => $keyword_list_id,
                    'Keywords.version' => $version,
                ]
            )
            ->innerJoinWith('KeywordLists');
        $this->set('count', $query->count()); // avant application des filtres
        $IndexComponent->setQuery($query)
            ->filter('code', IndexComponent::FILTER_ILIKE)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('modified', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('name', IndexComponent::FILTER_ILIKE);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);

        $q = $Keywords->query();
        $duplicates = $Keywords->find()
            ->select(['name', 'count' => $q->func()->count('*')])
            ->where(['keyword_list_id' => $keyword_list_id, 'version' => 0])
            ->groupBy(['name'])
            ->having([new QueryExpression('count > 1')])
            ->orderBy(['name'])
            ->toArray();
        $this->set('duplicates', $duplicates);
    }

    /**
     * Ajout d'un mot clé
     * @param string $keyword_list_id
     * @throws Exception
     */
    public function add(string $keyword_list_id)
    {
        /** @var KeywordsTable $Keywords */
        $Keywords = $this->fetchTable('Keywords');
        $this->Seal->archivalAgency()
            ->table('KeywordLists')
            ->find('seal')
            ->where(['KeywordLists.id' => $keyword_list_id])
            ->firstOrFail();

        $entity = $Keywords->newEntity([], ['validate' => false]);
        $this->set('entity', $entity);
        if ($this->getRequest()->is('post')) {
            $data = [
                'keyword_list_id' => $keyword_list_id,
                'version' => 0,
                'code' => $this->getRequest()->getData('code'),
                'name' => $this->getRequest()->getData('name'),
                'created_user_id' => $this->userId,
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($Keywords, $entity, $data);
        }
    }

    /**
     * Donne les infos liés à un mot clé
     * @param string $id
     * @return Response
     */
    public function getData(string $id)
    {
        $this->viewBuilder()->setLayout('ajax');

        $keyword = $this->Seal->archivalAgency()
            ->table('Keywords')
            ->find('seal')
            ->where(['Keywords.id' => $id])
            ->contain(['KeywordLists'])
            ->firstOrFail();
        return $this->renderDataToJson($keyword);
    }

    /**
     * Ajout d'un ensemble de clés
     * @param string $keyword_list_id
     * @throws \Exception
     */
    public function addMultiple(string $keyword_list_id)
    {
        /** @var KeywordsTable $Keywords */
        $Keywords = $this->fetchTable('Keywords');
        $list = $this->Seal->archivalAgency()
            ->table('KeywordLists')
            ->find('seal')
            ->where(['KeywordLists.id' => $keyword_list_id])
            ->firstOrFail();
        $existingWorkspace = $list->get('has_workspace');
        if (!$existingWorkspace) {
            throw new BadRequestException(
                __(
                    "Impossible d'ajouter un mot clé, aucune version technique n'a été crée"
                )
            );
        }
        $requestData = [];
        foreach ($this->getRequest()->getData() as $values) {
            if (empty($values['name'])) {
                continue;
            }
            $requestData[] = [
                'keyword_list_id' => $keyword_list_id,
                'version' => 0,
                'name' => $values['name'],
                'code' => $values['code'],
            ];
        }
        $entities = $Keywords->newEntities($requestData);
        $this->set('entities', $entities);

        if ($this->getRequest()->is('post')) {
            $data = array_filter(
                array_map(
                    function ($v) use ($keyword_list_id) {
                        if (empty($v['name'])) {
                            return null;
                        }
                        return [
                            'keyword_list_id' => $keyword_list_id,
                            'version' => 0,
                            'code' => $v['code'],
                            'name' => $v['name'],
                            'created_user_id' => $this->userId,
                        ];
                    },
                    $this->getRequest()->getData()
                )
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');

            $response = $this->getResponse();
            /** @var EntityInterface $entity Model->patchEntities() fonctionne mal (n'applique pas app_meta) */
            foreach ($entities as $entity) {
                $shift = array_shift($data);
                $entity->set($shift);
            }
            if ($Keywords->saveMany($entities)) {
                $body = $response->getBody();
                $body->write(json_encode($entities));
                $response = $response->withBody($body)
                    ->withType('json')
                    ->withHeader('X-Asalae-Success', 'true');
                $this->disableAutoRender()->setResponse($response);
            } else {
                $this->Modal->fail();
            }
        }
    }

    /**
     * Edition d'un mot clé
     * @param string $id
     * @throws \Exception
     */
    public function edit(string $id)
    {
        /** @var KeywordsTable $Keywords */
        $Keywords = $this->fetchTable('Keywords');
        /** @var Keyword $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Keywords')
            ->find('seal')
            ->where(
                [
                    'Keywords.id' => $id,
                    'Keywords.version' => 0,
                ]
            )
            ->innerJoinWith('KeywordLists')
            ->firstOrFail();
        $this->set('entity', $entity);
        if ($this->getRequest()->is('put')) {
            $data = [
                'keyword_list_id' => $entity->get('keyword_list_id'),
                'version' => 0,
                'code' => $this->getRequest()->getData('code'),
                'name' => $this->getRequest()->getData('name'),
            ];
            $Keywords->patchEntity($entity, $data);
            $entity->appendModified($this->userId);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($Keywords, $entity);
        }
    }

    /**
     * Suppression d'un mot clé
     * @param string $id
     * @return Response
     * @throws \Exception
     */
    public function delete(string $id = null)
    {
        if ($id === null) {
            $ids = $this->getRequest()->getData('ids');
        } else {
            $ids = [$id];
        }
        /** @var KeywordsTable $Keywords */
        $Keywords = $this->fetchTable('Keywords');
        $entities = $this->Seal->archivalAgency()
            ->table('Keywords')
            ->find('seal')
            ->where(
                [
                    'Keywords.id IN' => $ids,
                    'Keywords.version' => 0,
                ]
            )
            ->innerJoinWith('KeywordLists')
            ->toArray();
        if (empty($entities)) {
            throw new RecordNotFoundException(
                sprintf(
                    'Record not found in table `%s`.',
                    $Keywords->getTable()
                )
            );
        }

        $report = $Keywords->deleteMany($entities)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Visualisation d'un mot clé
     * @param string $id
     * @throws \Exception
     */
    public function view(string $id)
    {
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $entity = $this->Seal->archivalAgency()
            ->table('Keywords')
            ->find('seal')
            ->where(['Keywords.id' => $id])
            ->innerJoinWith('KeywordLists')
            ->firstOrFail();
        $this->set('entity', $entity);

        $query = $Users->find()
            ->select(['Users.username', 'Users.name'])
            ->where(['Users.id' => $entity->get('created_user_id')]);
        $lastEditUserId = $entity->get('modified_user_id');
        if (!empty($lastEditUserId)) {
            $query->select(['EditUser.name', 'EditUser.username'])
                ->innerJoin(
                    ['EditUser' => 'users'],
                    [
                        'EditUser.id' => $lastEditUserId,
                    ]
                );
        }
        $user = $query->first();
        if (empty($user)) {
            $user = $Users->newEntity([], ['validate' => false]);
        }
        $this->set('users', $user);
    }

    /**
     * Import de mots-clés par skos
     * @param string $keyword_list_id
     * @return Response
     * @throws \Exception
     */
    public function import(string $keyword_list_id)
    {
        /** @var KeywordsTable $Keywords */
        $Keywords = $this->fetchTable('Keywords');
        $list = $this->Seal->archivalAgency()
            ->table('KeywordLists')
            ->find('seal')
            ->where(['KeywordLists.id' => $keyword_list_id])
            ->firstOrFail();
        $existingWorkspace = $list->get('has_workspace');
        if (!$existingWorkspace) {
            throw new BadRequestException(
                __(
                    "Impossible d'ajouter un mot clé, aucune version technique n'a été crée"
                )
            );
        }
        $this->set('list', $list);
        $this->set('uploadId', self::TABLE_UPLOAD);

        if ($this->getRequest()->is('post')) {
            // On récupère le fichier d'import et on définit son type
            /** @var FileuploadsTable $Fileuploads */
            $Fileuploads = $this->fetchTable('Fileuploads');
            $fileupload_id = $this->getRequest()->getData('fileupload_id');
            /** @var Fileupload $file */
            $file = $Fileuploads->get($fileupload_id);
            $ext = $file->get('ext');
            if (in_array($ext, ['rdf', 'csv'])) {
                $type = $ext;
            } else {
                $type = strpos($file->get('mime'), 'xml') ? 'rdf' : 'csv';
            }

            // On crée la liste d'entités selon le type d'import
            if ($type === 'rdf') {
                $imports = $Keywords->importSkosArray(
                    file_get_contents($file->get('path'))
                );
            } else {
                $imports = $Keywords->importCsvArray(
                    $file->get('content'),
                    $this->getRequest()->getData('Csv')
                );
                $file->fclose();
            }

            $conflicts = $this->findConflicts($keyword_list_id, $imports);

            $conn = $Keywords->getConnection();
            $conn->begin();

            $success = true;
            $deleted = 0;
            $saved = 0;

            // ~66 mots clés par seconde
            LimitBreak::setTimeLimit(ceil(count($imports) * 0.012));

            foreach ($imports as $import) {
                [$code, $name] = $import;
                // S'il y a des conflits (noms identiques), ont les résouds selon la stratégie choisie
                if (in_array($name, $conflicts) && !$this->solveConflict($name, $keyword_list_id, $deleted)) {
                    continue;
                }
                $entity = $Keywords->newEntity(
                    [
                        'code' => $code,
                        'name' => $name,
                        'keyword_list_id' => $keyword_list_id,
                        'version' => 0,
                        'created_user_id' => $this->userId,
                        'imported_from' => $type,
                    ]
                );
                if ($Keywords->save($entity)) {
                    $saved++;
                } else {
                    $success = false;
                    FormatError::logEntityErrors($entity);
                }
            }

            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                return $this->renderDataToJson(
                    ['success' => true, 'count' => count($imports), 'saved' => $saved, 'deleted' => $deleted]
                );
            } else {
                $conn->rollback();
                $this->Modal->fail();
            }
        }
    }

    /**
     * Trouve les doublons dans $imports par rapport à la liste/version
     * @param int   $keyword_list_id
     * @param array $imports
     * @return array
     */
    private function findConflicts($keyword_list_id, array $imports): array
    {
        $batchSize = 1000;
        $totalImports = count($imports);
        $offset = 0;
        $Keywords = $this->fetchTable('Keywords');
        $conflicts = [];
        $allNames = [];

        while ($offset < $totalImports) {
            $batch = array_slice($imports, $offset, $batchSize);
            $names = array_filter(array_column($batch, 1));
            if (!$names) {
                continue;
            }
            foreach ($names as $key => $name) {
                if (isset($allNames[$name])) {
                    $conflicts[] = $name;
                    unset($names[$key]);
                } else {
                    $allNames[$name] = true;
                }
            }
            $duplicateNames = $Keywords->find()
                ->select(['name'])
                ->where(
                    [
                        'name IN' => $names,
                        'keyword_list_id' => $keyword_list_id,
                        'version' => 0,
                    ]
                )
                ->disableHydration()
                ->disableAutoFields()
                ->toArray();
            $conflicts = array_merge($conflicts, array_column($duplicateNames, 'name'));

            $offset += $batchSize;
        }
        return array_unique($conflicts);
    }

    /**
     * Permet d'obtenir des informations sur un import avant d'effectuer le dit import
     * @param string $keyword_list_id
     * @param string $fileupload_id
     * @throws Exception
     */
    public function getImportFileInfo(
        string $keyword_list_id,
        string $fileupload_id
    ) {
        $this->viewBuilder()->setLayout('ajax');
        /** @var KeywordsTable $Keywords */
        $Keywords = $this->fetchTable('Keywords');
        /** @var Fileupload $file */
        $file = $this->Seal->archivalAgency()
            ->table('Fileuploads')
            ->find('seal')
            ->where(['Fileuploads.id' => $fileupload_id])
            ->firstOrFail();

        $ext = $file->get('ext');
        if (in_array($ext, ['rdf', 'csv'])) {
            $type = $ext;
        } else {
            $type = strpos($file->get('mime'), 'xml') ? 'rdf' : 'csv';
        }
        $this->set('type', $type);

        if ($type === 'rdf') {
            $imports = $Keywords->importSkosArray(file_get_contents($file->get('path')));
        } else {
            $imports = $Keywords->importCsvArray(
                $file->get('content'),
                $this->getRequest()->getData('Csv', [])
            );
            $file->fclose();
        }
        $count = count($imports);
        $conn = $Keywords->getConnection();
        $conn->begin();
        $conflicts = $this->findConflicts($keyword_list_id, $imports);

        $this->set('count', $count);
        $this->set('imports', array_slice($imports, 0, 5));

        $conn->rollback();
        $this->set('conflicts', $conflicts);
        $conflict_count = 0;
        foreach ($imports as $import) {
            if (in_array($import[1], $conflicts)) {
                $conflict_count++;
            }
        }
        $this->set('conflict_count', $conflict_count);

        if ($this->getRequest()->is('post')) {
            $this->viewBuilder()->setTemplate('test_import');
        }
    }

    /**
     * Effectue la recherche des options d'un select de mot clés
     * @throws Exception
     */
    public function populateSelect()
    {
        $this->viewBuilder()->setLayout('ajax');
        /** @var KeywordsTable $Keywords */
        $Keywords = $this->fetchTable('Keywords');
        $this->paginate['limit'] = Configure::read('Pagination.limit', 20);

        $query = $Keywords
            ->find(
                'list',
                keyField: $this->getRequest()->getQuery(
                    'key'
                ) === 'content' ? 'name' : 'id',
                valueField: function (EntityInterface $entity) {
                    return $entity->get('name') . ' - ' . $entity->get(
                        'code'
                    );
                },
                groupField: function (EntityInterface $entity) {
                    /** @var EntityInterface $join */
                    $join = $entity->get('_matchingData')['KeywordLists'];
                    return $join->get('name');
                },
            )->select(
                [
                    'Keywords.id',
                    'Keywords.code',
                    'Keywords.name',
                    'KeywordLists.name',
                ]
            )
            ->innerJoinWith('KeywordLists')
            ->where(
                [
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                    'KeywordLists.active' => true,
                    'KeywordLists.version' => new IdentifierExpression(
                        'Keywords.version'
                    ),
                    'KeywordLists.version >=' => 1,
                ]
            )
            ->limit($this->paginate['limit']);

        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        $value = $this->getRequest()->getData('term');
        if ($value) {
            $query->where(
                [
                    'OR' => array_merge(
                        $this->Condition->ilike(
                            'Keywords.name',
                            '%' . $value . '%'
                        ),
                        $this->Condition->ilike(
                            'Keywords.code',
                            '%' . $value . '%'
                        )
                    ),
                ]
            );
            $orderName = $this->Condition->ilike(
                'Keywords.name',
                pg_escape_string($value) . '%'
            );
            $orderCode = $this->Condition->ilike(
                'Keywords.code',
                pg_escape_string($value) . '%'
            );
            // Donne un <ORDER BY (Keywords.code ILIKE 'my_word%') desc>
            // -> Les mots qui commencent par 'my_word' en 1er
            $query->orderBy(
                new QueryExpression(
                    '(' . array_keys($orderName)[0] . ' \'' . current(
                        $orderName
                    ) . '\') desc'
                )
            );
            $query->orderBy(
                new QueryExpression(
                    '(' . array_keys($orderCode)[0] . ' \'' . current(
                        $orderCode
                    ) . '\') desc'
                )
            );
        }
        $query->orderBy(
            [
                'KeywordLists.name' => 'asc',
                'Keywords.code' => 'asc',
            ]
        );
        $page = $this->getRequest()->getData('page', 1);
        if ($page > 1) {
            $query->offset($this->paginate['limit'] * ($page - 1));
        }
        $data = $query->all()->toArray();
        $output = ['results' => [], 'pagination' => ['more' => false]];
        if ($this->getRequest()->getQuery('include_search') && $value) {
            $output['results'][] = [
                'id' => $value,
                'text' => h($value),
            ];
        }
        if ($query->count() > $this->paginate['limit'] * $page) {
            $output['pagination']['more'] = true;
        }
        foreach ($data as $group => $values) {
            $formatedValues = [];
            foreach ($values as $key => $value) {
                $formatedValues[] = [
                    'id' => $key,
                    'text' => h($value),
                ];
            }
            $output['results'][] = [
                'id' => 0,
                'text' => h($group),
                'children' => $formatedValues,
            ];
        }

        return $this->renderDataToJson($output);
    }

    /**
     * Résoud le conflit selon request->getData('conflicts')
     * @param string $name
     * @param string $keyword_list_id
     * @param int    $deleted
     * @return bool le doublon est-il résolu ? (enregistrement possible)
     * @throws Exception
     */
    private function solveConflict(string &$name, string $keyword_list_id, int &$deleted = 0): bool
    {
        $conditions = [
            'Keywords.name' => $name,
            'Keywords.keyword_list_id' => $keyword_list_id,
            'Keywords.version' => 0,
        ];
        $Keywords = $this->fetchTable('Keywords');
        // Si c'est un doublon au sein d'un fichier d'import, il faut tout de
        // même écrire le 1er mot clé avant de tenter le solve
        if (!$Keywords->exists($conditions)) {
            return true;
        }
        switch ($this->getRequest()->getData('conflicts') ?: 'ignore') {
            case 'overwrite':
                foreach ($Keywords->find()->where($conditions) as $keyword) {
                    $deleted += (int)$Keywords->delete($keyword);
                }
                break;
            case 'ignore':
                return false;
            case 'rename':
                $i = 1;
                $oldName = $name;
                do {
                    $name = $oldName . '_' . $i;
                    $exists = $Keywords->exists(
                        [
                            'name' => $name,
                            'keyword_list_id' => $keyword_list_id,
                            'version' => 0,
                        ]
                    );
                    if ($i > 100) {
                        throw new Exception(
                            __(
                                "Un problème a eu lieu, plus de 100 mots-clés ont le même nom"
                            )
                        );
                    }
                    $i++;
                } while ($exists);
                break;
        }
        return true;
    }
}
