<?php

/**
 * Asalae\Controller\RestitutionsController
 */

namespace Asalae\Controller;

use Asalae\Model\Entity\RestitutionRequest;
use Asalae\Model\Table\ArchiveUnitsRestitutionRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Table\RestitutionsTable;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use DateInvalidTimeZoneException;
use Exception;

/**
 * Restitutions
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveUnitsRestitutionRequestsTable ArchiveUnitsRestitutionRequests
 * @property RestitutionRequestsTable             RestitutionRequests
 * @property RestitutionsTable                    Restitutions
 */
class RestitutionsController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     * @var array
     */
    public array $paginate = [
        'order' => [
            'Restitutions.id' => 'desc',
        ],
    ];

    /**
     * Liste les enregistrements
     */
    public function retrieval()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        $conditions = [
            'Restitutions.state IN' => [
                RestitutionsTable::S_AVAILABLE,
                RestitutionsTable::S_DOWNLOADED,
            ],
        ];
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (
            $this->orgEntityId != $this->archivalAgencyId
            && !in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)
        ) {
            $conditions['RestitutionRequests.created_user_id'] = $this->userId;
        }

        $query = $this->Seal->archivalAgency()
            ->table('Restitutions')
            ->find('seal')
            ->innerJoinWith('RestitutionRequests')
            ->where($conditions)
            ->contain(['RestitutionRequests' => ['ArchiveUnits']]);

        $IndexComponent->setQuery($query)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('original_count', IndexComponent::FILTER_COUNTOPERATOR)
            ->filter('original_size', IndexComponent::FILTER_SIZEOPERATOR)
            ->filter('state', IndexComponent::FILTER_IN);

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/commons');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) use ($ArchiveUnits) {
                    // effectue un contain avec limite individuelle d'enregistrements
                    $aus = $ArchiveUnits->find()
                        ->innerJoinWith('ArchiveUnitsRestitutionRequests')
                        ->where(
                            [
                                'restitution_request_id' => $e->get(
                                    'restitution_request_id'
                                ),
                            ]
                        )
                        ->orderBy(['archival_agency_identifier'])
                        ->limit(10)
                        ->toArray();
                    $restitutionRequest = $e->get('restitution_request');
                    $restitutionRequest->set('archive_units', $aus);
                    return $e;
                }
            )
            ->toArray();
        $this->set('data', $data);

        // options
        $this->set('states', $this->Restitutions->options('state'));
    }

    /**
     * Télécharge la restitution
     * @param string $id
     * @return Response
     * @throws DateInvalidTimeZoneException
     */
    public function download(string $id)
    {
        $restitution = $this->Seal->archivalAgency()
            ->table('Restitutions')
            ->find('seal')
            ->where(['Restitutions.id' => $id])
            ->contain(['RestitutionRequests'])
            ->firstOrFail();
        $zip = $restitution->get('zip');
        if (!is_readable($zip)) {
            throw new NotFoundException("xml not found");
        }
        if (
            $this->Restitutions->transition(
                $restitution,
                RestitutionsTable::T_DOWNLOAD
            )
        ) {
            $restitutionRequest = $restitution->get('restitution_request');
            /** @var RestitutionRequestsTable $RestitutionRequests */
            $RestitutionRequests = $this->fetchTable('RestitutionRequests');
            $RestitutionRequests->transition(
                $restitutionRequest,
                RestitutionRequestsTable::T_DOWNLOAD_RESTITUTION
            );
            $RestitutionRequests->saveOrFail($restitutionRequest);
            $this->Restitutions->saveOrFail($restitution);
        }

        return $this->getCoreResponse()
            ->withFileStream($zip, ['mime' => 'application/zip']);
    }

    /**
     * Acquittement des restitutions
     * @throws Exception
     */
    public function acquittal()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        $conditions = [
            'Restitutions.state' => RestitutionsTable::S_DOWNLOADED,
        ];
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (
            $this->orgEntityId != $this->archivalAgencyId
            && !in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)
        ) {
            $conditions['RestitutionRequests.created_user_id'] = $this->userId;
        }
        $query = $this->Seal->archivalAgency()
            ->table('Restitutions')
            ->find('seal')
            ->innerJoinWith('RestitutionRequests')
            ->where($conditions)
            ->contain(['RestitutionRequests' => ['ArchiveUnits']]);

        $IndexComponent->setQuery($query)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('original_count', IndexComponent::FILTER_COUNTOPERATOR)
            ->filter('original_size', IndexComponent::FILTER_SIZEOPERATOR)
            ->filter('state', IndexComponent::FILTER_IN);

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/commons');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) use ($ArchiveUnits) {
                    // effectue un contain avec limite individuelle d'enregistrements
                    $aus = $ArchiveUnits->find()
                        ->innerJoinWith('ArchiveUnitsRestitutionRequests')
                        ->where(
                            [
                                'restitution_request_id' => $e->get(
                                    'restitution_request_id'
                                ),
                            ]
                        )
                        ->orderBy(['archival_agency_identifier'])
                        ->limit(10)
                        ->toArray();
                    $restitutionRequest = $e->get('restitution_request');
                    $restitutionRequest->set('archive_units', $aus);
                    return $e;
                }
            )
            ->toArray();
        $this->set('data', $data);

        // options
        $this->set('states', $this->Restitutions->options('state'));
    }

    /**
     * acquitter une restitution
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function acquit(string $id)
    {
        /** @var RestitutionRequestsTable $RestitutionRequests */
        $RestitutionRequests = $this->fetchTable('RestitutionRequests');
        $restitution = $this->Seal->archivalAgency()
            ->table('Restitutions')
            ->find('seal')
            ->where(['Restitutions.id' => $id])
            ->contain(
                [
                    'RestitutionRequests' => ['ArchivalAgencies', 'OriginatingAgencies'],
                ]
            )
            ->firstOrFail();
        /** @var RestitutionRequest $restitutionRequest */
        $restitutionRequest = $restitution->get('restitution_request');

        if (
            $RestitutionRequests->transition(
                $restitutionRequest,
                RestitutionRequestsTable::T_ACQUIT_RESTITUTION
            )
            && $this->Restitutions->transition(
                $restitution,
                RestitutionsTable::T_ACQUIT
            )
        ) {
            // génère l'attestation
            $restitutionRequest->get('certification');
            $RestitutionRequests->saveOrFail($restitutionRequest);
            $this->Restitutions->saveOrFail($restitution);
            $report = $this->Restitutions->delete($restitution)
                ? 'done'
                : 'Erreur lors de la suppression';
        } else {
            throw new ForbiddenException();
        }
        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Visualisation d'une demande de restitution
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $restitution = $this->Seal->archivalAgency()
            ->table('Restitutions')
            ->find('seal')
            ->where(['Restitutions.id' => $id])
            ->contain(
                [
                    'RestitutionRequests' => [
                        'ArchivalAgencies',
                        'OriginatingAgencies',
                        'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                            return $q->orderBy(
                                ['ArchiveUnits.archival_agency_identifier']
                            )
                                ->limit(
                                    $AjaxPaginatorComponent->getConfig('limit')
                                )
                                ->contain(
                                    [
                                        'Archives' => ['DescriptionXmlArchiveFiles'],
                                        'ArchiveDescriptions',
                                    ]
                                );
                        },
                        'CreatedUsers',
                    ],
                ]
            )
            ->firstOrFail();
        /** @var EntityInterface[] $archive_units */
        $archive_units = Hash::get(
            $restitution,
            'restitution_request.archive_units'
        );
        foreach ($archive_units as $archiveUnit) {
            $archiveUnit->set('not_public', true);
            $archiveUnit->setVirtual(['description', 'dates']);
        }
        $this->set('restitution', $restitution);
        /** @var ArchiveUnitsRestitutionRequestsTable $ArchiveUnitsRestitutionRequests */
        $ArchiveUnitsRestitutionRequests = $this->fetchTable('ArchiveUnitsRestitutionRequests');
        $unitsCount = $ArchiveUnitsRestitutionRequests->find()
            ->where(
                [
                    'restitution_request_id' => $restitution->get(
                        'restitution_request_id'
                    ),
                ]
            )
            ->count();
        $this->set('unitsCount', $unitsCount);
        $AjaxPaginatorComponent->setViewPaginator(
            Hash::get($restitution, 'restitution_request.archive_units', []),
            $unitsCount
        );
    }

    /**
     * Récupère les informations d'une restitution
     * @param string $id
     * @return Response
     */
    public function getInfo(string $id)
    {
        $restitution = $this->Seal->archivalAgency()
            ->table('Restitutions')
            ->find('seal')
            ->where(['Restitutions.id' => $id])
            ->contain(['RestitutionRequests'])
            ->firstOrFail();
        return $this->renderDataToJson($restitution);
    }
}
