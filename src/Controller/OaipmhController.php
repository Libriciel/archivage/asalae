<?php

/**
 * Asalae\Controller\OaipmhController
 */

namespace Asalae\Controller;

use Asalae\Exception\OaipmhException;
use Asalae\Model\Entity\Archive;
use Asalae\Model\Entity\OaipmhToken;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\OaipmhTokensTable;
use Asalae\Model\Table\RolesTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\VersionsTable;
use Asalae\View\Helper\TranslateHelper;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Http\CallbackStream;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Premis;
use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Core\Configure;
use Cake\Event\EventInterface;
use Cake\Http\Response as CakeResponse;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\View\View;
use DateTime;
use DOMDocument;
use DOMException;
use Exception;
use LibXMLError;

/**
 * OAI-PMH - Open Archives Initiative Protocol for Metadata Harvesting
 *
 * @category Controller
 *
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 *
 * @property ArchivesTable     Archives
 * @property TransfersTable    Transfers
 * @property OaipmhTokensTable OaipmhTokens
 */
class OaipmhController extends AppController implements ApiInterface
{
    use ApiTrait;

    /**
     * Constantes XML
     */
    public const string NS_OAIPMH = 'http://www.openarchives.org/OAI/2.0/'; // NOSONAR
    public const string OAIPMH_DATE_FORMAT = 'Y-m-d\TH:i:s\Z';

    /**
     * @var string
     */
    public const string GRANULARITY = 'YYYY-MM-DDThh:mm:ssZ';

    /**
     * Allowed verbs with a boolean for token usage or not
     *
     * @var array
     */
    private $allowedVerbs = [
        'Identify' => false,
        'ListMetadataFormats' => false,
        'ListSets' => true,
        'ListIdentifiers' => true,
        'ListRecords' => true,
        'GetRecord' => false,
    ];

    /**
     * @var int
     */
    private $version = 2;

    /**
     * @var string for xml saving before returning the response
     */
    private $fileName;

    /**
     * @var DOMDocument
     */
    private $dom;

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default',
            'v1',
            'v2',
        ];
    }

    /**
     * Api Controller, ServiceArchive Component needed
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse|void|null
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        $this->Rest->setApiActions(
            [
                'default' => [
                    'get' => ['function' => 'apiGetAction'],
                ],
                'v1',
                'v2',
            ]
        );
        return parent::beforeFilter($event);
    }

    /**
     * V1 version access
     *
     * @return CakeResponse|null
     * @throws Exception
     */
    public function v1(): ?CakeResponse
    {
        $this->version = 1;
        return $this->apiGetAction();
    }

    /**
     * Point d'entrée des requêtes OAI-PMH.
     *
     * @return CakeResponse
     * @throws Exception
     */
    protected function apiGetAction(): CakeResponse
    {
        $addEventLog = false;
        $this->request->allowMethod(['get']);
        $params = $this->request->getQueryParams();

        $this->dom = new DOMDocument();
        $this->dom->formatOutput = true;
        $this->dom->preserveWhiteSpace = false;

        $element = $this->dom->createElementNS(self::NS_OAIPMH, 'OAI-PMH');
        $element->setAttributeNS(
            'http://www.w3.org/2000/xmlns/',
            'xmlns:xsi',
            Premis::NS_XSI
        );
        $responseDate = $this->dom->createElementNS(
            self::NS_OAIPMH,
            'responseDate'
        );
        $responseDate->appendChild(
            DOMUtility::createDomTextNode(
                $this->dom,
                (new DateTime())->format(self::OAIPMH_DATE_FORMAT)
            )
        );
        $element->appendChild($responseDate);
        $this->dom->appendChild($element);

        try {
            $this->callActionForVerb($params);
            $addEventLog = true;
        } catch (OaipmhException $e) {
            if ($child = $element->getElementsByTagName($params['verb'] ?? '_')->item(0)) {
                $element->removeChild($child);
            }
            $error = $this->dom->createElementNS(self::NS_OAIPMH, 'error');
            $error->appendChild(DOMUtility::createDomTextNode($this->dom, $e->getValue()));
            $error->setAttribute('code', $e->getCodeAttribute());
            $element->appendChild($error);
        }

        // debug
        libxml_use_internal_errors(true);
        if (!$this->dom->schemaValidate(OAIPMH_V20_XSD_LAX)) {
            $error = implode(
                "\n",
                array_map(
                    [$this, 'libxmlDisplayError'],
                    libxml_get_errors()
                )
            );
            libxml_clear_errors();
            throw new Exception($error);
        }
        $path = Configure::read('App.paths.data') . DS . 'upload' . DS;
        if (!is_dir($path)) {
            mkdir($path);
        }
        $fileName = $path . uniqid('oaipmh_') . '.xml';

        if (!$this->dom->save($fileName)) {
            throw new Exception('Fail Xml Save for file ' . $fileName . ' in ' . __FUNCTION__);
        }

        if ($sessionFile = $this->getRequest()->getQuery('sessionFile')) {
            Notify::emit(
                'download_file',
                [
                    'uuid' => $sessionFile,
                    'state' => 'downloading',
                    'last_update' => time(),
                ]
            );
        }

        if ($addEventLog) {
            $this->addLogEvent();
        }

        return $this->getResponse()
            ->withHeader('Content-Type', 'application/xml')
            ->withHeader('Content-Length', $size = filesize($fileName))
            ->withHeader('File-Size', $size)
            ->withBody(
                new CallbackStream(
                    function () use ($fileName, $sessionFile) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        readfile($fileName);
                        unlink($fileName);
                        if ($sessionFile) {
                            Notify::emit(
                                'download_file',
                                [
                                    'uuid' => $sessionFile,
                                    'state' => 'finished',
                                    'last_update' => time(),
                                    'integrity' => true,
                                ]
                            );
                        }
                    }
                )
            );
    }

    /**
     * Appel de l'action correspondant au verb demandé, ou erreur
     *
     * @param array $params
     * @throws OaipmhException
     * @throws DOMException
     */
    protected function callActionForVerb(array $params)
    {
        $verb = $params['verb'] ?? '_';
        unset($params['verb']);

        $url = Configure::read('App.fullBaseUrl') . '/api/oaipmh';
        $request = $this->dom->createElementNS(self::NS_OAIPMH, 'request');
        $request->appendChild(DOMUtility::createDomTextNode($this->dom, $url));
        $this->dom->firstChild->appendChild($request);

        if (!isset($this->allowedVerbs[$verb])) {
            throw new OaipmhException(OaipmhException::BAD_VERB);
        }

        $request->setAttribute('verb', $verb);

        if (isset($params['resumptionToken']) && !$this->allowedVerbs[$verb]) {
            throw new OaipmhException(OaipmhException::VERB_NOT_SUPPORT_TOKEN);
        }

        /** @var OaipmhTokensTable $OaipmhTokens */
        $OaipmhTokens = $this->fetchTable('OaipmhTokens');
        $token = empty($params['resumptionToken'])
            ? null
            : $OaipmhTokens->getTokenForVerb(
                $params['resumptionToken'],
                lcfirst($verb),
                $this->version
            );

        unset($params['resumptionToken']);

        if ($token === false) { // resumptionToken non trouvé
            throw new OaipmhException(OaipmhException::TOKEN_INVALID);
        }

        if ($token !== null && count($params)) {
            throw new OaipmhException(
                OaipmhException::INCLUDE_ILLEGAL_ARGUMENTS
            );
        }

        // @see $this->allowedVerbs
        $method = lcfirst($verb);
        $this->$method($params, $token);

        foreach ($params as $param => $value) {
            $request->setAttribute($param, $value);
        }
    }

    /**
     * Add logevent for authorize verbs
     * @return void
     * @throws Exception
     */
    protected function addLogEvent()
    {
        $verb = $this->request->getQueryParams()['verb'];
        $translateHelper = new TranslateHelper(new View());
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        /** @var VersionsTable $Versions */
        $Versions = $this->fetchTable('Versions');
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $entry = $EventLogs->newEntry(
            Inflector::underscore($verb) . '_' . Inflector::singularize($this->getName()),
            'success',
            __(
                "Requête {0} ({1}) par l'accès de webservice ({2})",
                $translateHelper->permission($this->getName()),
                $verb,
                $AuthenticationComponent->getIdentityData('username')
            ),
            $AuthenticationComponent->getIdentityData('id'),
            $Versions->getAppVersion()
        );
        $EventLogs->saveOrFail($entry);
    }

    /**
     * V2 version access
     *
     * @return CakeResponse|null
     * @throws Exception
     */
    public function v2(): ?CakeResponse
    {
        return $this->apiGetAction();
    }

    /**
     * @param LibXMLError $error
     * @return string
     */
    protected function libxmlDisplayError(LibXMLError $error): string
    {
        $return = '';
        switch ($error->level) {
            case LIBXML_ERR_WARNING:
                $return .= "Warning $error->code: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "Error $error->code: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "Fatal Error $error->code: ";
                break;
        }
        $return .= trim($error->message);
        if ($error->file) {
            $return .= " in $error->file";
        }
        $return .= " on line $error->line";

        return $return;
    }

    /**
     * Identify verb
     *
     * @param array $params
     * @throws OaipmhException|Exception
     */
    protected function identify(array $params)
    {
        $this->checkAllowedParams($params);

        $data = [
            'repositoryName' => $this->getRepositoryName(),
            'baseURL' => Configure::read('App.fullBaseUrl') . '/api/oaipmh',
            'protocolVersion' => '2.0',
            'adminEmail' => $this->getAdminEmail(),
            'earliestDatestamp' => $this->getEarliestDateStamp(),
            'deletedRecord' => 'persistent',
            'granularity' => self::GRANULARITY,
        ];

        $element = $this->dom->createElementNS(self::NS_OAIPMH, 'Identify');
        $this->dom->firstChild->appendChild($element);
        foreach ($data as $name => $value) {
            $node = $this->dom->createElementNS(self::NS_OAIPMH, $name);
            $node->appendChild(DOMUtility::createDomTextNode($this->dom, $value));
            $element->appendChild($node);
        }
    }

    /**
     * Throw exception if the request contain an argument not authorized
     *
     * @param array $params
     * @param array $allowedParams
     * @throws OaipmhException
     */
    protected function checkAllowedParams(
        array $params,
        array $allowedParams = []
    ) {
        foreach ($params as $param => $value) {
            if (!in_array($param, $allowedParams)) {
                throw new OaipmhException(
                    OaipmhException::INCLUDE_ILLEGAL_ARGUMENTS
                );
            }
        }
    }

    /**
     * Nom du service
     *
     * @return string
     */
    protected function getRepositoryName(): string
    {
        return $this->archivalAgency->get('name');
    }

    /**
     * Email du premier admin actif du service,
     * échec si aucun user ne correspond
     *
     * @return string
     */
    protected function getAdminEmail(): string
    {
        $admin = $this->Seal->archivalAgency()
            ->table('Users')
            ->find('seal')
            ->select(['Users.email'])
            ->where(
                [
                    'Roles.code' => RolesTable::CODE_ARCHIVIST,
                    'Users.active' => true,
                    'Users.email IS NOT' => null,
                ]
            )
            ->orderBy(['Users.created' => 'asc'])
            ->contain(['Roles'])
            ->firstOrFail();

        return Hash::get($admin, 'email');
    }

    /**
     * Date de création de la première archive du service ou du service d'archive si aucune archive
     *
     * @return string
     * @throws Exception
     */
    protected function getEarliestDateStamp(): string
    {
        $earliestRecord = $this->Seal->archivalAgency()
            ->table('Archives')
            ->find('seal')
            ->select(['Archives.created'])
            ->orderBy(['Archives.created' => 'asc'])
            ->first();

        /** @var DateTime $date */
        $date = $earliestRecord !== null
            ? $earliestRecord->created
            : $this->archivalAgency->get('created');

        return $date->format(self::OAIPMH_DATE_FORMAT);
    }

    /**
     * listMetadataFormats verb
     *
     * @param array $params
     * @throws OaipmhException
     * @throws DOMException
     */
    protected function listMetadataFormats(array $params)
    {
        $this->checkAllowedParams($params);

        $element = $this->dom->createElementNS(
            self::NS_OAIPMH,
            'ListMetadataFormats'
        );
        $this->dom->firstChild->appendChild($element);

        $oaidc = $this->dom->createElementNS(self::NS_OAIPMH, 'metadataFormat');
        $oaidc->appendChild(
            $this->dom->createElementNS(
                self::NS_OAIPMH,
                'metadataPrefix',
                'oai_dc'
            )
        );
        $oaidc->appendChild(
            $this->dom->createElementNS(
                self::NS_OAIPMH,
                'schema',
                'http://www.openarchives.org/OAI/2.0/oai_dc.xsd' // NOSONAR
            )
        );
        $oaidc->appendChild(
            $this->dom->createElementNS(
                self::NS_OAIPMH,
                'metadataNamespace',
                'http://www.openarchives.org/OAI/2.0/oai_dc/' // NOSONAR
            )
        );
        $element->appendChild($oaidc);

        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $versions = $Transfers->getMessageVersions($this->archivalAgencyId);

        if (in_array('seda0.2', $versions)) {
            $seda02 = $this->dom->createElementNS(
                self::NS_OAIPMH,
                'metadataFormat'
            );
            $element->appendChild($seda02);
            $seda02->appendChild(
                $this->dom->createElementNS(
                    self::NS_OAIPMH,
                    'metadataPrefix',
                    'seda0.2'
                )
            );
            $seda02->appendChild(
                $this->dom->createElementNS(
                    self::NS_OAIPMH,
                    'schema',
                    'https://francearchives.fr/seda/seda_v0-2.xsd'
                )
            );
            $seda02->appendChild(
                $this->dom->createElementNS(
                    self::NS_OAIPMH,
                    'metadataNamespace',
                    NAMESPACE_SEDA_02
                )
            );
        }

        if (in_array('seda1.0', $versions)) {
            $seda10 = $this->dom->createElementNS(
                self::NS_OAIPMH,
                'metadataFormat'
            );
            $element->appendChild($seda10);
            $seda10->appendChild(
                $this->dom->createElementNS(
                    self::NS_OAIPMH,
                    'metadataPrefix',
                    'seda1.0'
                )
            );
            $seda10->appendChild(
                $this->dom->createElementNS(
                    self::NS_OAIPMH,
                    'schema',
                    'https://francearchives.fr/seda/seda_v1-0.xsd'
                )
            );
            $seda10->appendChild(
                $this->dom->createElementNS(
                    self::NS_OAIPMH,
                    'metadataNamespace',
                    NAMESPACE_SEDA_10
                )
            );
        }

        if (in_array('seda2.1', $versions)) {
            $seda21 = $this->dom->createElementNS(
                self::NS_OAIPMH,
                'metadataFormat'
            );
            $element->appendChild($seda21);
            $seda21->appendChild(
                $this->dom->createElementNS(
                    self::NS_OAIPMH,
                    'metadataPrefix',
                    'seda2.1'
                )
            );
            $seda21->appendChild(
                $this->dom->createElementNS(
                    self::NS_OAIPMH,
                    'schema',
                    'https://francearchives.fr/seda/seda_v2-1.xsd'
                )
            );
            $seda21->appendChild(
                $this->dom->createElementNS(
                    self::NS_OAIPMH,
                    'metadataNamespace',
                    NAMESPACE_SEDA_21
                )
            );
        }

        if (in_array('seda2.2', $versions)) {
            $seda22 = $this->dom->createElementNS(
                self::NS_OAIPMH,
                'metadataFormat'
            );
            $element->appendChild($seda22);
            $seda22->appendChild(
                $this->dom->createElementNS(
                    self::NS_OAIPMH,
                    'metadataPrefix',
                    'seda2.2'
                )
            );
            $seda22->appendChild(
                $this->dom->createElementNS(
                    self::NS_OAIPMH,
                    'schema',
                    'https://francearchives.fr/seda/seda_v2-2.xsd'
                )
            );
            $seda22->appendChild(
                $this->dom->createElementNS(
                    self::NS_OAIPMH,
                    'metadataNamespace',
                    NAMESPACE_SEDA_22
                )
            );
        }
    }

    /**
     * listSets verb, use resumptionToken.
     *
     * @param array            $params
     * @param OaipmhToken|null $token
     * @throws Exception
     */
    protected function listSets(array $params, $token)
    {
        $this->checkAllowedParams($params);

        $params['version'] = $this->version;
        $params['service_archive_id'] = $this->archivalAgencyId;

        /** @var OaipmhTokensTable $OaipmhTokens */
        $OaipmhTokens = $this->fetchTable('OaipmhTokens');
        $OaipmhTokens->listRequestForVerb(
            $params,
            $token,
            'listSets',
            $this->dom
        );
    }

    /**
     * listIdentifiers verb, use resumptionToken.
     *
     * @param array            $params
     * @param OaipmhToken|null $token
     * @throws Exception
     */
    protected function listIdentifiers(array $params, $token)
    {
        $this->checkAllowedParams(
            $params,
            ['from', 'until', 'metadataPrefix', 'set']
        );

        $params['version'] = $this->version;
        $params['service_archive_id'] = $this->archivalAgencyId;

        /** @var OaipmhTokensTable $OaipmhTokens */
        $OaipmhTokens = $this->fetchTable('OaipmhTokens');
        $OaipmhTokens->listRequestForVerb(
            $params,
            $token,
            'listIdentifiers',
            $this->dom
        );
    }

    /**
     * listRecords verb, use resumptionToken.
     *
     * @param array            $params
     * @param OaipmhToken|null $token
     * @throws Exception
     */
    protected function listRecords(array $params, $token)
    {
        $this->checkAllowedParams(
            $params,
            ['from', 'until', 'metadataPrefix', 'set']
        );

        $params['version'] = $this->version;
        $params['service_archive_id'] = $this->archivalAgencyId;

        /** @var OaipmhTokensTable $OaipmhTokens */
        $OaipmhTokens = $this->fetchTable('OaipmhTokens');
        $OaipmhTokens->listRequestForVerb(
            $params,
            $token,
            'listRecords',
            $this->dom
        );
    }

    /**
     * getRecord verb, no resumptionToken
     *
     * @param array $params
     * @throws OaipmhException
     * @throws VolumeException
     * @throws DOMException
     */
    protected function getRecord(array $params)
    {
        $this->checkAllowedParams($params, ['identifier', 'metadataPrefix']);

        /** @var OaipmhTokensTable $OaipmhTokens */
        $OaipmhTokens = $this->fetchTable('OaipmhTokens');
        $record = $OaipmhTokens->getRecordForService(
            $this->archivalAgencyId,
            $params
        );

        if ($record === null) {
            throw new OaipmhException(OaipmhException::ID_DOES_NOT_EXIST);
        }

        /** @var $record Archive */
        $element = $this->dom->createElementNS(self::NS_OAIPMH, 'GetRecord');
        $this->dom->firstChild->appendChild($element);

        $record->addOaipmhXmlToDom(
            $this->dom,
            'GetRecord',
            $params['metadataPrefix']
        );
    }
}
