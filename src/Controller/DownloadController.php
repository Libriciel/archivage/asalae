<?php

/**
 * Asalae\Controller\DownloadController
 */

namespace Asalae\Controller;

use Asalae\Model\Entity\Fileupload;
use Asalae\Model\Table\FileuploadsTable;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Http\CallbackStream;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Exception;
use Imagine\Gd\Imagine as ImagineGd;
use Imagine\Gmagick\Imagine as ImagineGmagick;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Imagick\Imagine as ImagineImagick;
use Throwable;
use ZipArchive;

/**
 * Download de fichiers
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FileuploadsTable $Fileuploads
 */
class DownloadController extends AppController
{
    /**
     * Traits
     */
    use CachableTrait;
    use RenderDataTrait;

    /**
     * Créer une image d'aperçu à partir d'une image uploadé
     *
     * @param string $id
     * @return Response
     * @throws Exception|Throwable
     */
    public function thumbnailImage(string $id)
    {
        $conditions = ['Fileuploads.id' => $id];
        /** @var Fileupload $file */
        $file = $this->Seal->archivalAgency()
            ->table('Fileuploads')
            ->find('seal')
            ->where($conditions)
            ->firstOrFail();

        if (!preg_match('/^image\/.*$/', $file->get('mime'))) {
            throw new BadRequestException(__("Fichier non compatible"));
        }

        $tmp = TMP . 'thumbnail' . DS . 'user_' . $this->userId;
        if (!is_dir($tmp)) {
            mkdir($tmp, 0777, true);
        }
        $ext = $this->extensionByMime($file->get('mime'));
        $originalExt = pathinfo($file->get('name'), PATHINFO_EXTENSION);
        $cached = $tmp . DS . basename(
            $file->get('name'),
            $originalExt ? '.' . $originalExt : null
        ) . $ext;

        if (!is_file($cached)) {
            switch (Configure::read('DriverImage')) {
                case 'imagick':
                    $imagine = new ImagineImagick();
                    break;
                case 'gmagick':
                    $imagine = new ImagineGmagick();
                    break;
                default:
                    $imagine = new ImagineGd();
                    break;
            }

            $mode = ImageInterface::THUMBNAIL_INSET;
            // Un png valide peut provoquer une erreur s'il n'a pas un EXIF readable
            set_error_handler(
                function () {
                }
            );
            try {
                $imagine->open($file->get('tmpFilename'))
                    ->thumbnail(new Box(150, 150), $mode)
                    ->save($cached);
            } catch (Throwable $e) {
                restore_error_handler();
                throw $e;
            }
            restore_error_handler();
        }

        return $this->renderCachable($cached);
    }

    /**
     * Permet d'obtenir une extension de fichier à partir d'un mime
     *
     * @param string $mime
     * @return string
     */
    private function extensionByMime(string $mime): string
    {
        switch ($mime) {
            case 'image/bmp':
                return '.bmp';
            case 'image/cis-cod':
                return '.cod';
            case 'image/gif':
                return '.gif';
            case 'image/ief':
                return '.ief';
            case 'image/pipeg':
                return '.jfif';
            case 'image/tiff':
                return '.tif';
            case 'image/x-cmu-raster':
                return '.ras';
            case 'image/x-cmx':
                return '.cmx';
            case 'image/x-icon':
                return '.ico';
            case 'image/x-portable-anymap':
                return '.pnm';
            case 'image/x-portable-bitmap':
                return '.pbm';
            case 'image/x-portable-graymap':
                return '.pgm';
            case 'image/x-portable-pixmap':
                return '.ppm';
            case 'image/x-rgb':
                return '.rgb';
            case 'image/x-xbitmap':
                return '.xbm';
            case 'image/x-xpixmap':
                return '.xpm';
            case 'image/x-xwindowdump':
                return '.xwd';
            case 'image/png':
                return '.png';
            case 'image/x-jps':
                return '.jps';
            case 'image/x-freehand':
                return '.fh';
            case 'image/jpeg':
            default:
                return '.jpg';
        }
    }

    /**
     * Permet de "lire" un fichier directement dans le navigateur
     *
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function open(string $id)
    {
        /** @var Fileupload $file */
        $entity = $this->Seal->archivalAgency()
            ->table('Fileuploads')
            ->find('seal')
            ->where(['Fileuploads.id' => $id])
            ->firstOrFail();
        $mime = $entity->get('mime');
        if (
            preg_match('/^image\/.*$/', $mime)
            || preg_match('/^application\/pdf$/', $mime)
            || preg_match('/^application\/vnd\.oasis\.opendocument\..*$/', $mime)
        ) {
            return $this->readfile($entity);
        }
        throw new BadRequestException(
            "Pas de lecteur trouvé pour le fichier. type mime: " . $mime
        );
    }

    /**
     * Permet de lire un fichier uploadé
     *
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function file(string $id)
    {
        /** @var Fileupload $file */
        $file = $this->Seal->archivalAgency()
            ->table('Fileuploads')
            ->find('seal')
            ->where(['Fileuploads.id' => $id])
            ->firstOrFail();
        return $this->getCoreResponse()
            ->withFileStream($file->get('path'))
            ->withHeader(
                'Content-Description',
                'File Transfer'
            )
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate')
            ->withHeader('Pragma', 'public');
    }

    /**
     * Permet de télécharger un zip comprennant les fichiers uploadé selectionné
     *
     * @return Response
     * @throws Exception
     */
    public function zip(): Response
    {
        $pIds = $this->getRequest()->getQuery('id');
        if (!$pIds || !preg_match('/^(\d,)*\d$/', $pIds)) {
            throw new NotFoundException(__("Fichier non trouvé"));
        }
        $ids = explode(',', $this->getRequest()->getQuery('id'));

        /** @var Fileupload $file */
        $files = $this->Seal->archivalAgency()
            ->table('Fileuploads')
            ->find('seal')
            ->where(['Fileuploads.id IN' => $ids]);
        if ($files->count() !== count($ids)) {
            throw new NotFoundException(__("Fichier non trouvé"));
        }

        $zip = new ZipArchive();
        $location = sys_get_temp_dir() . DS . uniqid('zip');

        if (!$zip->open($location, ZipArchive::CREATE)) {
            throw new Exception(__("Impossible de créer le zip"));
        }

        /** @var Fileupload $file */
        foreach ($files as $file) {
            if (!$zip->addFile($file->get('tmpFilename'), $file->get('name'))) {
                throw new Exception(
                    __(
                        "Impossible d'ajouter {0} au fichier zip",
                        $file->get('name')
                    )
                );
            }
        }
        $zip->close();

        return $this->getCoreResponse()
            ->withFileStream(
                $location,
                ['mime' => 'application/zip', 'delete' => true]
            );
    }

    /**
     * Donne une réponse pour un readfile (avec CallbackStream)
     * @param EntityInterface|Fileupload $file
     * @return Response
     */
    private function readfile(EntityInterface $file): Response
    {
        return $this->getResponse()
            ->withHeader(
                'Content-Type',
                $file->get('mime')
            )
            ->withHeader(
                'Content-Length',
                $file->get('size')
            )
            ->withHeader(
                'File-Size',
                $file->get('size')
            )
            ->withBody(
                new CallbackStream(
                    function () use ($file) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        readfile($file->get('path'));
                    }
                )
            );
    }
}
