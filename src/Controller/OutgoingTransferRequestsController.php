<?php

/**
 * Asalae\Controller\OutgoingTransferRequestsController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Entity\ArchivingSystem;
use Asalae\Model\Entity\OutgoingTransferRequest;
use Asalae\Model\Table\AgreementsTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsOutgoingTransferRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\ArchivingSystemsTable;
use Asalae\Model\Table\CountersTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\OutgoingTransferRequestsTable;
use Asalae\Model\Table\ProfilesTable;
use Asalae\Model\Table\ServiceLevelsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationProcessesTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Beanstalk\Utility\Beanstalk;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Client\Exception\NetworkException;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\HttpException;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use DateInvalidTimeZoneException;
use Exception;
use Pheanstalk\Pheanstalk;
use Psr\Http\Message\ResponseInterface;
use ZMQSocketException;

/**
 * OutgoingTransferRequests
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AgreementsTable Agreements
 * @property ArchiveUnitsOutgoingTransferRequestsTable ArchiveUnitsOutgoingTransferRequests
 * @property ArchiveUnitsTable ArchiveUnits
 * @property ArchivesTable Archives
 * @property ArchivingSystemsTable ArchivingSystems
 * @property CountersTable Counters
 * @property OrgEntitiesTable OrgEntities
 * @property OutgoingTransferRequestsTable OutgoingTransferRequests
 * @property ProfilesTable Profiles
 * @property ServiceLevelsTable ServiceLevels
 * @property ValidationProcessesTable ValidationProcesses
 * @property ValidationChainsTable ValidationChains
 */
class OutgoingTransferRequestsController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use RenderDataTrait;
    use ApiTrait;

    /**
     * Options de pagination par défaut
     * @var array
     */
    public array $paginate = [
        'order' => [
            'OutgoingTransferRequests.id' => 'desc',
        ],
    ];

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'options',
        ];
    }

    /**
     * Liste les demandes de transfert d'archives en préparation
     */
    public function indexPreparating()
    {
        $query = $this->Seal->archivalAgency()
            ->table('OutgoingTransferRequests')
            ->find('seal')
            ->where(
                [
                    'OutgoingTransferRequests.created_user_id' => $this->userId,
                    'OutgoingTransferRequests.state' => OutgoingTransferRequestsTable::S_CREATING,
                ]
            );
        $this->indexCommons($query);
    }

    /**
     * Liste les enregistrements
     * @param Query $query
     * @return Response|void
     * @throws Exception
     */
    private function indexCommons(Query $query)
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');

        $query->where(
            ['OutgoingTransferRequests.archival_agency_id' => $this->archivalAgencyId]
        )
            ->contain(['CreatedUsers' => ['OrgEntities']]);
        /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
        $OutgoingTransferRequests = $this->fetchTable('OutgoingTransferRequests');
        $subquery = $OutgoingTransferRequests->selectQuery()
            ->select(['dr.id'])
            ->from(['dr' => 'outgoing_transfer_requests'])
            ->innerJoin(
                ['lk' => 'archive_units_outgoing_transfer_requests'],
                ['lk.otr_id' => new IdentifierExpression('dr.id')]
            )
            ->innerJoin(
                ['au' => 'archive_units'],
                ['au.id' => new IdentifierExpression('lk.archive_unit_id')]
            )
            ->innerJoin(
                ['a' => 'archives'],
                ['a.id' => new IdentifierExpression('au.archive_id')]
            )
            ->where(
                [
                    'dr.id' => new IdentifierExpression(
                        'OutgoingTransferRequests.id'
                    ),
                ]
            )
            ->limit(1);

        $IndexComponent->setQuery($query)
            ->filter(
                'archive_unit_identifier',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike(
                            'au.archival_agency_identifier',
                            $v
                        )
                    );
                    return $q->andWhere(
                        ['OutgoingTransferRequests.id IN' => $sq]
                    );
                }
            )
            ->filter(
                'archive_unit_name',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike('au.name', $v)
                    );
                    return $q->andWhere(
                        ['OutgoingTransferRequests.id IN' => $sq]
                    );
                }
            )
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter(
                'created_user',
                IndexComponent::FILTER_IN,
                'OutgoingTransferRequests.created_user_id'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('state', IndexComponent::FILTER_IN)
            ->filter(
                'originating_agency_id',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        ['a.originating_agency_id' => $v]
                    );
                    return $q->andWhere(
                        ['OutgoingTransferRequests.id IN' => $sq]
                    );
                }
            )
            ->filter(
                'transferring_agency_id',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        ['a.transferring_agency_id' => $v]
                    );
                    return $q->andWhere(
                        ['OutgoingTransferRequests.id IN' => $sq]
                    );
                }
            )
            ->filter(
                'profile_id',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(['a.profile_id' => $v]);
                    return $q->andWhere(
                        ['OutgoingTransferRequests.id IN' => $sq]
                    );
                }
            )
            ->filter(
                'agreement_id',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(['a.agreement_id' => $v]);
                    return $q->andWhere(
                        ['OutgoingTransferRequests.id IN' => $sq]
                    );
                }
            );

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/commons');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) use ($ArchiveUnits) {
                    // effectue un contain avec limite individuelle d'enregistrements
                    $aus = $ArchiveUnits->find()
                        ->innerJoinWith('ArchiveUnitsOutgoingTransferRequests')
                        ->where(['otr_id' => $e->id])
                        ->orderBy(['archival_agency_identifier'])
                        ->limit(10)
                        ->toArray();
                    $e->set('archive_units', $aus);
                    return $e;
                }
            )
            ->toArray();
        $this->set('data', $data);

        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $oa = $OrgEntities->listOptions([], $this->Seal->archivalAgencyChilds())
            ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES])
            ->orderBy(['OrgEntities.name' => 'asc']);
        $this->set('originating_agencies', $oa->all());

        $ta = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES]
            )
            ->orderBy(['OrgEntities.name' => 'asc']);
        $this->set('transferring_agencies', $ta->all());

        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('sealList')
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles);

        $agreements = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('sealList')
            ->orderBy(['Agreements.name' => 'asc']);
        $this->set('agreements', $agreements);

        // override $IndexComponent->init()
        $viewBuilder = $this->viewBuilder();
        if (
            $this->getRequest()->is('ajax')
            && $viewBuilder->getLayout() === null
        ) {
            $viewBuilder->setTemplate('ajax_index');
        }

        // options
        $this->set('states', $OutgoingTransferRequests->options('state'));

        $created_users = $OutgoingTransferRequests->find(
            'list',
            keyField: '_matchingData.CreatedUsers.id',
            valueField: '_matchingData.CreatedUsers.username',
            groupField: '_matchingData.OrgEntities.name',
        )
            ->select(
                ['CreatedUsers.id', 'CreatedUsers.username', 'OrgEntities.name']
            )
            ->innerJoinWith('CreatedUsers')
            ->innerJoinWith('CreatedUsers.OrgEntities')
            ->distinct(
                $distinct = [
                    'OrgEntities.name',
                    'CreatedUsers.username',
                ]
            )
            ->where(
                ['OutgoingTransferRequests.archival_agency_id' => $this->archivalAgencyId]
            )
            ->orderBy($distinct);

        $this->set('created_users', $created_users);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * Donne les options pour une nouvelle demande de transfert sortant
     * @return Response
     * @throws Exception
     */
    protected function options()
    {
        /** @var AgreementsTable $Agreements */
        $Agreements = $this->fetchTable('Agreements');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        /** @var ProfilesTable $Profiles */
        $Profiles = $this->fetchTable('Profiles');
        /** @var ServiceLevelsTable $ServiceLevels */
        $ServiceLevels = $this->fetchTable('ServiceLevels');

        $transferringAgencies = $OrgEntities->listOptions(
            ['keyField' => 'identifier'],
            $this->Seal->archivalAgencyChilds()
        )
            ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES])
            ->orderBy(['OrgEntities.name' => 'asc'])
            ->toArray();
        $originatingAgencies = $OrgEntities->listOptions(
            ['keyField' => 'identifier'],
            $this->Seal->archivalAgencyChilds()
        )
            ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES])
            ->orderBy(['OrgEntities.name' => 'asc'])
            ->toArray();
        $profiles = $Profiles->find(
            'list',
            keyField: 'identifier',
            valueField: function (EntityInterface $entity) {
                return $entity->get('identifier') . ' - ' . $entity->get(
                    'name'
                );
            },
        )
            ->where(['org_entity_id' => $this->archivalAgencyId])
            ->orderBy(['name'])
            ->toArray();
        $agreements = $Agreements->find(
            'list',
            keyField: 'identifier',
            valueField: function (EntityInterface $entity) {
                return $entity->get('identifier') . ' - ' . $entity->get(
                    'name'
                );
            },
        )
            ->where(['org_entity_id' => $this->archivalAgencyId])
            ->orderBy(['name'])
            ->toArray();
        $services = $ServiceLevels->find(
            'list',
            keyField: 'identifier',
            valueField: function (EntityInterface $entity) {
                return $entity->get('identifier') . ' - ' . $entity->get('name');
            },
        )
            ->where(['org_entity_id' => $this->archivalAgencyId])
            ->orderBy(['name'])
            ->toArray();

        $options = [
            'archival_agency_identifier' => $this->archivalAgency->get(
                'identifier'
            ),
            'archival_agency_name' => $this->archivalAgency->get('name'),
            'transferring_agencies' => $transferringAgencies,
            'originating_agencies' => $originatingAgencies,
            'agreements' => $agreements,
            'profiles' => $profiles,
            'services' => $services,
        ];
        return $this->renderDataToJson($options);
    }

    /**
     * Liste de mes demandes de transfert d'archives
     */
    public function indexMy()
    {
        $query = $this->Seal->archivalAgency()
            ->table('OutgoingTransferRequests')
            ->find('seal')
            ->where(
                ['OutgoingTransferRequests.created_user_id' => $this->userId]
            );
        $this->indexCommons($query);
    }

    /**
     * Liste de toutes les demandes de transfert d'archives
     */
    public function indexAll()
    {
        $type = Hash::get($this->orgEntity, 'type_entity.code');
        $query = $this->Seal->archivalAgency()
            ->table('OutgoingTransferRequests')
            ->find('seal');
        if ($type !== 'SA' && $type !== 'SE') {
            $query->innerJoinWith('Users')
                ->where(['Users.org_entity_id' => $this->orgEntityId]);
        }
        $this->indexCommons($query);
    }

    /**
     * Liste de toutes les demandes de transfert d'archives à éliminer
     */
    public function indexProcessed()
    {
        $type = Hash::get($this->orgEntity, 'type_entity.code');
        $query = $this->Seal->archivalAgency()
            ->table('OutgoingTransferRequests')
            ->find('seal');
        if ($type !== 'SA' && $type !== 'SE') {
            $query->innerJoinWith('Users')
                ->where(['Users.org_entity_id' => $this->orgEntityId]);
        }
        $query->andWhere(
            [
                'OutgoingTransferRequests.state IN' => [
                    OutgoingTransferRequestsTable::S_TRANSFERS_PROCESSED,
                    OutgoingTransferRequestsTable::S_ARCHIVE_DESTRUCTION_SCHEDULED,
                    OutgoingTransferRequestsTable::S_ARCHIVE_FILES_DESTROYING,
                    OutgoingTransferRequestsTable::S_ARCHIVE_DESCRIPTION_DELETING,
                ],
            ]
        );
        $this->indexCommons($query);
    }

    /**
     * Action d'ajout - sélection du SAE
     */
    public function add1()
    {
        $request = $this->getRequest();
        if (
            $request->is('post')
            && ($archivingSystem = $request->getData('archiving_system_id'))
        ) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            $this->Modal->step(
                'addOutgoingTransferRequestStep2(' . $archivingSystem . ')'
            );
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');

        $subquery = $this->queryArchiveUnit()
            ->select(['ArchiveUnits.id'], true);
        $func = $ArchiveUnits->query()->func();
        $query = $ArchiveUnits->find()
            ->select(
                [
                    'au_count' => $func->count('*'),
                    'documents_count' => $func->sum('original_total_count'),
                    'documents_size' => $func->sum('original_total_size'),
                ]
            )
            ->where(['ArchiveUnits.id IN' => $subquery]);
        $this->set('sums', $query->all()->toArray()[0]);

        /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
        $OutgoingTransferRequests = $this->fetchTable('OutgoingTransferRequests');
        $entity = $OutgoingTransferRequests->newEmptyEntity();
        $this->set('entity', $entity);

        $archivingSystems = $this->Seal->archivalAgency()
            ->table('ArchivingSystems')
            ->find('sealList')
            ->orderBy('ArchivingSystems.name')
            ->toArray();
        $this->set('archivingSystemsList', $archivingSystems);

        $archivingSystems = $this->Seal->archivalAgency()
            ->table('ArchivingSystems')
            ->find('seal')
            ->orderBy('ArchivingSystems.name')
            ->toArray();
        $this->set('archivingSystems', $archivingSystems);

        $this->set('filters', json_encode($request->getQueryParams()));
    }

    /**
     * Donne le query avec ses filtres
     * @return Query
     * @throws Exception
     */
    private function queryArchiveUnit()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index', ['model' => 'ArchiveUnits']);
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $ArchiveUnits->findByIndexSealed(
            $this->Seal->hierarchicalView(),
            true
        )
            ->where(['ArchiveUnits.state IS NOT ' => ArchiveUnitsTable::S_FREEZED]);
        $query->contain(['ArchiveDescriptions']);
        $IndexComponent->setQuery($query)
            ->filter('agreement_id', null, 'Archives.agreement_id')
            ->filter(
                'appraisal_rule_end',
                IndexComponent::FILTER_DATECOMPARENOW,
                'AppraisalRules.end_date'
            )
            ->filter(
                'final_action_code',
                IndexComponent::FILTER_EQUAL,
                'AppraisalRules.final_action_code'
            )
            ->filter('archival_agency_identifier', IndexComponent::FILTER_ILIKE)
            ->filter(
                'created',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.created'
            )
            ->filter(
                'favoris',
                function () {
                    $request = $this->getRequest();
                    $cookies = $request->getCookieParams();
                    $sessionCookies = $this->cookies;
                    if (is_array($sessionCookies)) {
                        $cookies = array_merge($cookies, $sessionCookies);
                    }
                    $cookies = array_filter($cookies, fn ($v) => $v !== '0');
                    $favoritesIds = [];
                    $regex = "/^table-favorite-archive-units-transferable-table-(\d+)$/";
                    foreach (array_keys($cookies) as $cookieName) {
                        if (preg_match($regex, $cookieName, $match)) {
                            $favoritesIds[] = $match[1];
                        }
                    }
                    if (empty($favoritesIds)) {
                        return [];
                    }
                    return ['ArchiveUnits.id IN' => $favoritesIds];
                }
            )
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter(
                'original_total_count',
                IndexComponent::FILTER_COUNTOPERATOR
            )
            ->filter('original_total_size', IndexComponent::FILTER_SIZEOPERATOR)
            ->filter('profile_id', null, 'Archives.profile_id')
            ->filter(
                'transferring_agency_id',
                null,
                'Archives.transferring_agency_id'
            )->filter(
                'originating_agency_id',
                null,
                'Archives.originating_agency_id'
            );
        return $query;
    }

    /**
     * Action d'ajout - formulaire
     * @param string $archiving_system_id
     * @throws Exception
     */
    public function add2(string $archiving_system_id)
    {
        $request = $this->getRequest();

        /** @var ArchivingSystem $archivingSystem */
        $archivingSystem = $this->Seal->archivalAgency()
            ->table('ArchivingSystems')
            ->find('seal')
            ->where(['ArchivingSystems.id' => $archiving_system_id])
            ->firstOrFail();
        $this->set('archivingSystem', $archivingSystem);
        /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
        $OutgoingTransferRequests = $this->fetchTable('OutgoingTransferRequests');
        $entity = $OutgoingTransferRequests->newEmptyEntity();

        if ($request->is('post')) {
            $conn = $OutgoingTransferRequests->getConnection();
            $conn->begin();
            /** @var CountersTable $Counters */
            $Counters = $this->fetchTable('Counters');
            $data = [
                'identifier' => $Counters->next(
                    $this->archivalAgencyId,
                    'OutgoingTransferRequest'
                ),
                'comment' => $request->getData('comment'),
                'archival_agency_id' => $this->archivalAgencyId,
                'archiving_system_id' => $archivingSystem->id,
                'archival_agency_identifier' => $request->getData(
                    'archival_agency_identifier'
                ),
                'archival_agency_name' => $request->getData(
                    'archival_agency_name'
                ),
                'transferring_agency_identifier' => $request->getData(
                    'transferring_agency_identifier'
                ),
                'transferring_agency_name' => $request->getData(
                    'transferring_agency_name'
                ),
                'originating_agency_identifier' => $request->getData(
                    'originating_agency_identifier'
                ),
                'originating_agency_name' => $request->getData(
                    'originating_agency_name'
                ),
                'agreement_identifier' => $request->getData(
                    'agreement_identifier'
                ),
                'profile_identifier' => $request->getData(
                    'profile_identifier'
                ),
                'service_level_identifier' => $request->getData(
                    'service_level_identifier'
                ),
                'state' => $OutgoingTransferRequests->initialState,
                'archives_count' => 0,
                'archive_units_count' => 0,
                'original_count' => 0,
                'original_size' => 0,
                'created_user_id' => $this->userId,
            ];
            $OutgoingTransferRequests->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($OutgoingTransferRequests->save($entity)) {
                $query = $this->queryArchiveUnit()
                    ->select(
                        [
                            'ArchiveUnits.id',
                            'ArchiveUnits.lft',
                            'ArchiveUnits.rght',
                            'ArchiveUnits.archive_id',
                        ],
                        true
                    )
                    ->disableHydration();
                /** @var ArchivesTable $Archives */
                $Archives = $this->fetchTable('Archives');
                /** @var ArchiveUnitsTable $ArchiveUnits */
                $ArchiveUnits = $this->fetchTable('ArchiveUnits');
                /** @var ArchiveUnitsOutgoingTransferRequestsTable $ArchiveUnitsOutgoingTransferRequests */
                $ArchiveUnitsOutgoingTransferRequests = $this->fetchTable('ArchiveUnitsOutgoingTransferRequests');
                foreach ($query as $res) {
                    $archive_unit_id = $res['id'];
                    $ArchiveUnitsOutgoingTransferRequests->insertQuery()
                        ->insert(['archive_unit_id', 'otr_id'])
                        ->values(
                            [
                                'archive_unit_id' => $archive_unit_id,
                                'otr_id' => $entity->id,
                            ]
                        )
                        ->execute();
                    $Archives->transitionAll(
                        ArchivesTable::T_REQUEST_TRANSFER,
                        ['id' => $res['archive_id']]
                    );
                    $ArchiveUnits->transitionAll(
                        ArchiveUnitsTable::T_REQUEST_TRANSFER,
                        ['lft >=' => $res['lft'], 'rght <=' => $res['rght']]
                    );
                }

                $conn->commit();
                $this->Modal->success();
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAdd($entity);
                $this->Modal->step(
                    'addOutgoingTransferRequestStep3(' . $entity->id . ')'
                );
            } else {
                $this->Modal->fail();
                $conn->rollback();
            }
        }

        try {
            $options = $archivingSystem->requestOptions();
        } catch (HttpException | NetworkException $e) {
            $this->set('exception', $e);
            return;
        }

        $this->set('filters', json_encode($request->getQueryParams()));

        $entity->set('archival_agency_id', $this->archivalAgencyId);
        $entity->set('archiving_system_id', $archiving_system_id);
        $entity->set(
            'archival_agency_identifier',
            $options['archival_agency_identifier']
        );
        $entity->set('archival_agency_name', $options['archival_agency_name']);
        $this->set('entity', $entity);

        $this->set('transferring_agencies', $options['transferring_agencies']);
        $this->set(
            'originating_agencies',
            $options['originating_agencies'] ?? []
        );
        $this->set('profiles', $options['profiles']);
        $this->set('agreements', $options['agreements']);
        $this->set('services', $options['services'] ?? []);
    }

    /**
     * Action d'ajout - archive_units
     * @param string $id
     * @throws Exception
     */
    public function add3(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $outgoingTransferRequest = $this->Seal->archivalAgency()
            ->table('OutgoingTransferRequests')
            ->find('seal')
            ->where(['OutgoingTransferRequests.id' => $id])
            ->contain(
                [
                    'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q
                            ->orderBy(['ArchiveUnits.archival_agency_identifier'])
                            ->limit($AjaxPaginatorComponent->getConfig('limit'))
                            ->contain(
                                [
                                    'Archives' => ['DescriptionXmlArchiveFiles'],
                                    'ArchiveDescriptions',
                                ]
                            );
                    },
                ]
            )
            ->firstOrFail();
        /** @var ArchiveUnitsOutgoingTransferRequestsTable $ArchiveUnitsOutgoingTransferRequests */
        $ArchiveUnitsOutgoingTransferRequests = $this->fetchTable('ArchiveUnitsOutgoingTransferRequests');
        $unitsCount = $ArchiveUnitsOutgoingTransferRequests->find()
            ->where(['otr_id' => $id])
            ->count();
        $this->set('unitsCount', $unitsCount);
        $this->set('outgoingTransferRequest', $outgoingTransferRequest);
        $this->set('jsTableId', 'add3-outgoing-transfer-request');
        $this->set(
            'paginatorId',
            'paginate-add3-outgoing-transfer-request-archive-units'
        );

        $this->addEditCommon($outgoingTransferRequest);
        $AjaxPaginatorComponent->setViewPaginator($outgoingTransferRequest->get('archive_units'), $unitsCount);
    }

    /**
     * Code commun entre add3 et edit
     * @param EntityInterface $outgoingTransferRequest
     * @return bool|null
     * @throws Exception
     */
    private function addEditCommon(EntityInterface $outgoingTransferRequest): ?bool
    {
        if ($this->getRequest()->is('put')) {
            /** @var ArchiveUnitsOutgoingTransferRequestsTable $ArchiveUnitsOutgoingTransferRequests */
            $ArchiveUnitsOutgoingTransferRequests = $this->fetchTable('ArchiveUnitsOutgoingTransferRequests');
            $query = $ArchiveUnitsOutgoingTransferRequests->find();
            $query
                ->select(
                    [
                        'archives_count' => $query->func()->count('*'),
                        'original_count' => $query->func()->sum(
                            'original_total_count'
                        ),
                        'original_size' => $query->func()->sum(
                            'original_total_size'
                        ),
                    ],
                    true
                )
                ->innerJoinWith('ArchiveUnits')
                ->where(['otr_id' => $outgoingTransferRequest->id])
                ->disableHydration();
            $sums = $query->all()->toArray()[0];

            $query = $ArchiveUnitsOutgoingTransferRequests->find();
            $sums += $query->select(
                ['archive_units_count' => $query->func()->count('*')],
                true
            )
                ->innerJoin(
                    ['ArchiveUnits' => 'archive_units'],
                    [
                        'ArchiveUnits.id' => new IdentifierExpression(
                            'archive_unit_id'
                        ),
                    ]
                )
                ->innerJoin(
                    ['au' => 'archive_units'],
                    [
                        'au.lft >=' => new IdentifierExpression(
                            'ArchiveUnits.lft'
                        ),
                        'au.rght <=' => new IdentifierExpression(
                            'ArchiveUnits.rght'
                        ),
                    ]
                )
                ->where(['otr_id' => $outgoingTransferRequest->id])
                ->disableHydration()
                ->all()
                ->toArray()[0];

            /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
            $OutgoingTransferRequests = $this->fetchTable('OutgoingTransferRequests');
            $OutgoingTransferRequests->patchEntity(
                $outgoingTransferRequest,
                array_map('intval', $sums) + $outgoingTransferRequest->toArray()
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if (
                $OutgoingTransferRequests->save(
                    $outgoingTransferRequest,
                    ['associated' => false]
                )
            ) {
                $this->Modal->success();
                if ($this->getRequest()->getParam('action') === 'edit') {
                    /** @var EventLoggerComponent $EventLogger */
                    $EventLogger = $this->loadComponent('EventLogger');
                    $EventLogger->logEdit($outgoingTransferRequest);
                }
                return true;
            } else {
                $this->Modal->fail();
                return false;
            }
        }
        return null;
    }

    /**
     * Action modifier
     * @param string $id
     * @return Response|null|void
     * @throws Exception
     */
    public function edit(string $id)
    {
        $request = $this->getRequest();
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $entity = $this->Seal->archivalAgency()
            ->table('OutgoingTransferRequests')
            ->find('seal')
            ->where(['OutgoingTransferRequests.id' => $id])
            ->contain(
                [
                    'ArchivingSystems',
                    'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q->orderBy(
                            ['ArchiveUnits.archival_agency_identifier']
                        )
                            ->limit($AjaxPaginatorComponent->getConfig('limit'))
                            ->contain(
                                [
                                    'Archives' => ['DescriptionXmlArchiveFiles'],
                                    'ArchiveDescriptions',
                                ]
                            );
                    },
                ]
            )
            ->firstOrFail();

        /** @var ArchiveUnitsOutgoingTransferRequestsTable $ArchiveUnitsOutgoingTransferRequests */
        $ArchiveUnitsOutgoingTransferRequests = $this->fetchTable('ArchiveUnitsOutgoingTransferRequests');
        $unitsCount = $ArchiveUnitsOutgoingTransferRequests->find()
            ->where(['otr_id' => $id])
            ->count();
        $this->set('unitsCount', $unitsCount);
        $this->set('jsTableId', 'table-edit-outgoing-transfer-request');

        /** @var ArchivingSystem $archivingSystem */
        $archivingSystem = $entity->get('archiving_system');
        try {
            $options = $archivingSystem->requestOptions();
        } catch (HttpException $e) {
            $this->set('exception', $e);
            return;
        }

        $this->set('archivingSystem', $archivingSystem);
        $this->set('filters', json_encode($request->getQueryParams()));

        $entity->set('archival_agency_id', $this->archivalAgencyId);
        $entity->set('archiving_system_id', $archivingSystem->id);
        $entity->set(
            'archival_agency_identifier',
            $options['archival_agency_identifier']
        );
        $entity->set('archival_agency_name', $options['archival_agency_name']);
        $this->set('entity', $entity);

        $this->set('transferring_agencies', $options['transferring_agencies']);
        $this->set(
            'originating_agencies',
            $options['originating_agencies'] ?? []
        );
        $this->set('profiles', $options['profiles']);
        $this->set('agreements', $options['agreements']);

        if ($this->getRequest()->is('put')) {
            /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
            $OutgoingTransferRequests = $this->fetchTable('OutgoingTransferRequests');
            $data = [
                'comment' => $request->getData('comment'),
                'archival_agency_id' => $this->archivalAgencyId,
                'archiving_system_id' => $archivingSystem->id,
                'archival_agency_identifier' => $request->getData(
                    'archival_agency_identifier'
                ),
                'archival_agency_name' => $request->getData(
                    'archival_agency_name'
                ),
                'transferring_agency_identifier' => $request->getData(
                    'transferring_agency_identifier'
                ),
                'transferring_agency_name' => $request->getData(
                    'transferring_agency_name'
                ),
                'originating_agency_identifier' => $request->getData(
                    'originating_agency_identifier'
                ),
                'originating_agency_name' => $request->getData(
                    'originating_agency_name'
                ),
                'agreement_identifier' => $request->getData(
                    'agreement_identifier'
                ),
                'profile_identifier' => $request->getData(
                    'profile_identifier'
                ),
                'service_level_identifier' => $request->getData(
                    'service_level_identifier'
                ),
                'state' => $OutgoingTransferRequests->initialState,
                'created_user_id' => $this->userId,
            ] + $entity->toArray();
            $OutgoingTransferRequests->patchEntity($entity, $data);
            if ($this->addEditCommon($entity)) {
                return $this->renderDataToJson($entity->toArray());
            }
        }
        $AjaxPaginatorComponent->setViewPaginator($entity->get('archive_units') ?: [], $unitsCount);
    }

    /**
     * Pagination des archive units dans la visualisation
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function paginateArchiveUnits(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $query = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal')
            ->innerJoinWith('ArchiveUnitsOutgoingTransferRequests')
            ->innerJoinWith('Archives')
            ->where(
                ['ArchiveUnitsOutgoingTransferRequests.otr_id' => $id]
            )
            ->contain(
                [
                    'Archives' => ['DescriptionXmlArchiveFiles'],
                    'ArchiveDescriptions',
                ]
            );
        return $AjaxPaginatorComponent->json(
            $query,
            (bool)$this->getRequest()->getQuery('count')
        );
    }

    /**
     * Retire une unité d'archives d'une demande de transfert d'archives
     * @param string $id
     * @param string $archive_unit_id
     * @return Response
     */
    public function removeArchiveUnit(string $id, string $archive_unit_id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var ArchiveUnitsOutgoingTransferRequestsTable $ArchiveUnitsOutgoingTransferRequests */
        $ArchiveUnitsOutgoingTransferRequests = $this->fetchTable('ArchiveUnitsOutgoingTransferRequests');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $link = $this->Seal->archivalAgency()
            ->table('ArchiveUnitsOutgoingTransferRequests')
            ->find('seal')
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.Archives')
            ->where(
                [
                    'ArchiveUnitsOutgoingTransferRequests.archive_unit_id' => $archive_unit_id,
                    'ArchiveUnitsOutgoingTransferRequests.otr_id' => $id,
                ]
            )
            ->contain(['ArchiveUnits'])
            ->firstOrFail();
        $report = $ArchiveUnitsOutgoingTransferRequests->delete($link)
            ? 'done'
            : 'Erreur lors de la suppression';
        $unit = $link->get('archive_unit');
        $ArchiveUnits->transitionAll(
            ArchiveUnitsTable::T_CANCEL,
            ['lft >=' => $unit->get('lft'), 'rght <=' => $unit->get('rght')]
        );
        $Archives->transitionAll(
            ArchivesTable::T_CANCEL,
            ['id' => $unit->get('archive_id')]
        );

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Action supprimer
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Seal->archivalAgency()
            ->table('OutgoingTransferRequests')
            ->find('seal')
            ->where(['OutgoingTransferRequests.id' => $id])
            ->firstOrFail();
        if (!$entity->get('deletable')) {
            throw new ForbiddenException();
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $ArchiveUnits->find()
            ->innerJoinWith('ArchiveUnitsOutgoingTransferRequests')
            ->where(
                ['ArchiveUnitsOutgoingTransferRequests.otr_id' => $entity->id]
            );
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        /** @var EntityInterface $unit */
        foreach ($query as $unit) {
            $ArchiveUnits->transitionAll(
                ArchiveUnitsTable::T_CANCEL,
                ['lft >=' => $unit->get('lft'), 'rght <=' => $unit->get('rght')]
            );
            $Archives->transitionAll(
                ArchivesTable::T_CANCEL,
                ['id' => $unit->get('archive_id')]
            );
        }

        /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
        $OutgoingTransferRequests = $this->fetchTable('OutgoingTransferRequests');
        $report = $OutgoingTransferRequests->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        if ($report === 'done') {
            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logDelete($entity);
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Envoi pour validation de la demande de transferts sortants
     * @param string $id
     * @throws ZMQSocketException
     * @throws DateInvalidTimeZoneException
     */
    public function send(string $id)
    {
        /** @var OutgoingTransferRequest $outgoingTransferRequests */
        $outgoingTransferRequests = $this->Seal->archivalAgency()
            ->table('OutgoingTransferRequests')
            ->find('seal')
            ->where(
                [
                    'OutgoingTransferRequests.id' => $id,
                    'OutgoingTransferRequests.created_user_id' => $this->userId,
                ]
            )
            ->contain(['ArchivalAgencies'])
            ->firstOrFail();
        /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
        $OutgoingTransferRequests = $this->fetchTable('OutgoingTransferRequests');
        if (
            !$OutgoingTransferRequests->transition(
                $outgoingTransferRequests,
                OutgoingTransferRequestsTable::T_SEND
            )
        ) {
            throw new ForbiddenException();
        }
        /** @var Notify $Notify */
        $Notify = Utility::get('Notify');

        $request = $this->getRequest();
        $session = $request->getSession();

        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $validationChain = $session->read(
            'ConfigArchivalAgency.outgoing-transfer-requests-chain'
        ) ?: 'default';
        if ($validationChain === 'default') {
            $validationChain = $ValidationChains->find(
                'default',
                app_type: ValidationChainsTable::ARCHIVE_OUTGOING_TRANSFER_REQUEST,
                org_entity_id: $this->archivalAgencyId,
            )->firstOrFail()->get('id');
        }
        $params = [
            'user_id' => $this->userId,
            'org_entity_id' => $this->archivalAgencyId,
            'validation_chain_id' => $validationChain,
        ];
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $process = $ValidationProcesses->createNewProcess(
            $outgoingTransferRequests,
            $params
        );
        $conn = $OutgoingTransferRequests->getConnection();
        $conn->begin();
        $success = $ValidationProcesses->save($process)
            && $OutgoingTransferRequests->save($outgoingTransferRequests);
        if ($success) {
            $ValidationProcesses->notifyProcess($process);
            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logValidationSend($outgoingTransferRequests);
            $conn->commit();
            $message = __(
                "La demande de transfert sortant {0} a bien été envoyée.",
                $outgoingTransferRequests->get('identifier')
            );
            $notifyClass = 'alert-warning';
        } else {
            $conn->rollback();
            $errors = FormatError::logEntityErrors($outgoingTransferRequests);
            $message = __(
                "Une erreur a eu lieu lors de l'envoi de la demande de transfert sortant {0} : {1}",
                $outgoingTransferRequests->get('identifier'),
                $errors
            );
            $notifyClass = 'alert-danger';
        }
        $Notify->send(
            $this->userId,
            [$session->read('Session.token')],
            $message,
            $notifyClass
        );
        if (!$success) {
            throw new BadRequestException($message);
        }
    }

    /**
     * Visualisation d'une demande de transfert sortant
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $outgoingTransferRequests = $this->Seal->archivalAgency()
            ->table('OutgoingTransferRequests')
            ->find('seal')
            ->where(['OutgoingTransferRequests.id' => $id])
            ->contain(
                [
                    'ArchivalAgencies',
                    'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q->orderBy(
                            ['ArchiveUnits.archival_agency_identifier']
                        )
                            ->limit($AjaxPaginatorComponent->getConfig('limit'))
                            ->contain(
                                [
                                    'Archives' => ['DescriptionXmlArchiveFiles'],
                                    'ArchiveDescriptions',
                                ]
                            );
                    },
                    'ArchivingSystems',
                    'CreatedUsers',
                    'ValidationProcesses' => [
                        'ValidationChains',
                        'CurrentStages' => [
                            'ValidationActors',
                        ],
                        'ValidationHistories' => [
                            'sort' => [
                                'ValidationHistories.created' => 'asc',
                                'ValidationHistories.id' => 'asc',
                            ],
                            'ValidationActors' => [
                                'Users',
                                'ValidationStages',
                            ],
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        /** @var EntityInterface[] $archive_units */
        $archive_units = Hash::get($outgoingTransferRequests, 'archive_units');
        foreach ($archive_units as $archiveUnit) {
            $archiveUnit->set('not_public', true);
            $archiveUnit->setVirtual(['description', 'dates']);
        }
        $this->set('outgoingTransferRequests', $outgoingTransferRequests);
        /** @var ArchiveUnitsOutgoingTransferRequestsTable $ArchiveUnitsOutgoingTransferRequests */
        $ArchiveUnitsOutgoingTransferRequests = $this->fetchTable('ArchiveUnitsOutgoingTransferRequests');
        $unitsCount = $ArchiveUnitsOutgoingTransferRequests->find()
            ->where(['otr_id' => $id])
            ->count();
        $this->set('unitsCount', $unitsCount);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($outgoingTransferRequests);
        $AjaxPaginatorComponent->setViewPaginator($outgoingTransferRequests->get('archive_units') ?: [], $unitsCount);
    }

    /**
     * Marque une demande de transfert sortant pour élimination
     * @param string $id
     * @return Response
     * @throws DateInvalidTimeZoneException
     */
    public function scheduleArchiveDestruction(string $id)
    {
        $restitutionRequest = $this->Seal->archivalAgency()
            ->table('OutgoingTransferRequests')
            ->find('seal')
            ->where(['OutgoingTransferRequests.id' => $id])
            ->firstOrFail();
        /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
        $OutgoingTransferRequests = $this->fetchTable('OutgoingTransferRequests');
        $OutgoingTransferRequests->transitionOrFail(
            $restitutionRequest,
            OutgoingTransferRequestsTable::T_SCHEDULE_ARCHIVE_DESTRUCTION
        );
        $OutgoingTransferRequests->saveOrFail($restitutionRequest);
        return $this->renderDataToJson($restitutionRequest->toArray());
    }

    /**
     * Annule le marquage d'une demande de restitution pour élimination
     * @param string $id
     * @return Response
     * @throws DateInvalidTimeZoneException
     */
    public function cancelArchiveDestruction(string $id)
    {
        $restitutionRequest = $this->Seal->archivalAgency()
            ->table('OutgoingTransferRequests')
            ->find('seal')
            ->where(['OutgoingTransferRequests.id' => $id])
            ->firstOrFail();
        /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
        $OutgoingTransferRequests = $this->fetchTable('OutgoingTransferRequests');
        $OutgoingTransferRequests->transitionOrFail(
            $restitutionRequest,
            OutgoingTransferRequestsTable::T_CANCEL_ARCHIVE_DESTRUCTION
        );
        $OutgoingTransferRequests->saveOrFail($restitutionRequest);
        return $this->renderDataToJson($restitutionRequest->toArray());
    }

    /**
     * Renvoi la demande au worker suite à une erreur
     * @param string $id
     * @return \AsalaeCore\Http\Response|Response
     * @throws Exception
     */
    public function resend(string $id)
    {
        $outgoingTransferRequest = $this->Seal->archivalAgency()
            ->table('OutgoingTransferRequests')
            ->find('seal')
            ->where(
                [
                    'OutgoingTransferRequests.id' => $id,
                    'OutgoingTransferRequests.created_user_id' => $this->userId,
                ]
            )
            ->contain(['ArchivalAgencies'])
            ->firstOrFail();
        /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
        $OutgoingTransferRequests = $this->fetchTable('OutgoingTransferRequests');
        if (
            !$OutgoingTransferRequests->transition(
                $outgoingTransferRequest,
                OutgoingTransferRequestsTable::T_RESEND
            )
        ) {
            throw new ForbiddenException();
        }
        if ($OutgoingTransferRequests->save($outgoingTransferRequest)) {
            /** @var Beanstalk $Beanstalk */
            $Beanstalk = Utility::get('Beanstalk');
            $Beanstalk->setTube('outgoing-transfer');
            $Beanstalk->emit(
                [
                    'user_id' => $this->userId,
                    'outgoing_transfer_request_id' => $outgoingTransferRequest->id,
                ],
                Pheanstalk::DEFAULT_PRIORITY,
                Pheanstalk::DEFAULT_DELAY,
                3600 // on part du principe qu'un fichier peut mettre 1h à se télécharger
            );
            return $this->renderDataToJson($outgoingTransferRequest->toArray());
        } else {
            throw new BadRequestException(
                FormatError::logEntityErrors($outgoingTransferRequest)
            );
        }
    }
}
