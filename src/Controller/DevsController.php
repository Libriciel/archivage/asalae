<?php

/**
 * Asalae\Controller\DevsController
 */

namespace Asalae\Controller;

use Asalae\Exception\GenericException;
use Asalae\Model\Entity\DestructionRequest;
use Asalae\Model\Entity\RestitutionRequest;
use Asalae\Model\Table\AcosTable;
use Asalae\Model\Table\ArosAcosTable;
use Asalae\Model\Table\ArosTable;
use Asalae\Model\Table\RolesTable;
use Asalae\View\Helper\TranslateHelper;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Core\Configure;
use Cake\Database\Expression\QueryExpression;
use Cake\Http\Response;
use Cake\View\View;
use Exception;
use Sabberworm\CSS;
use Sabberworm\CSS\Parsing\SourceException;

/**
 * Controller uniquement destiné au développement de asalae
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AcosTable     Acos
 * @property ArosTable     Aros
 * @property ArosAcosTable Permissions
 * @property RolesTable    Roles
 */
class DevsController extends AppController
{
    use RenderDataTrait;

    /**
     * Nombre d'inputs par action
     * 3 hiddens
     * 1 lot de 3 radios (gestion des droits: null, 0, 1)
     * 1 lot de 2 (hidden 0 + checkbox 1 pour accès)
     * 1 select (comme droit)
     * 1 select (group)
     */
    public const int INPUTS_PER_ACTION = 7;
    /**
     * @var array [$controller => $id]
     */
    private $controllers = [];

    /**
     * Défini certaines règles supplémentaire pour les controlleurs et permet la
     * création des acos
     *
     * @throws Exception
     */
    public function index()
    {
        if (!is_writable(Configure::read('App.paths.controllers_rules'))) {
            throw new Exception(
                __(
                    "www-data n'a pas accès à controllers.json en écriture, 
                    impossible de modifier les controlleurs!"
                )
            );
        }
        $controllers = [];
        $appControllerMethods = get_class_methods(
            "Asalae\\Controller\\AppController"
        );
        $count = 0;
        foreach ($this->getControllers() as $controllerClass) {
            $methods = get_class_methods(
                "Asalae\\Controller\\$controllerClass"
            );
            $controllerName = substr(
                $controllerClass,
                0,
                strlen($controllerClass) - strlen('Controller')
            );

            // get_class_methods() envoi les methodes protégés dans ce contexte
            if ($controllerName === 'Devs') {
                $methods = ['index'];
            }

            $controllers[$controllerName] = array_diff(
                $methods,
                $appControllerMethods
            );
            sort($controllers[$controllerName]);
            $count += count($controllers[$controllerName]);
        }
        $this->set(compact('controllers'));
        if ($count * self::INPUTS_PER_ACTION > ini_get('max_input_vars')) {
            $this->Flash->error(
                __(
                    "La configuration de PHP permet {0} inputs, mais cette page en contient actuellement {1}.
                     Veuillez augmenter la valeur de {2} avant d'envoyer le formulaire",
                    ini_get('max_input_vars'),
                    $count * self::INPUTS_PER_ACTION,
                    'max_input_vars'
                )
            );
        }

        /** @var AcosTable $Acos */
        $Acos = $this->fetchTable('Acos');

        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            if (isset($data['new'])) {
                $this->newPermissions(array_filter($data['new']));
                unset($data['new']);
            }
            if (isset($data['update'])) {
                $this->updatePermissions(array_filter($data['update']));
                unset($data['update']);
            }
            $this->updateApiActions($data);
            $handle = fopen(
                Configure::read('App.paths.controllers_rules'),
                'w'
            );
            fwrite($handle, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
            fclose($handle);
        } else {
            $data = json_decode(
                file_get_contents(
                    Configure::read('App.paths.controllers_rules')
                ),
                true
            );
        }

        $root = $Acos->find()
            ->select(['lft', 'rght'])
            ->where(['alias' => 'controllers', 'model' => 'root'])
            ->first();
        if ($root) {
            $acos = $Acos->find()
                ->where(
                    function (QueryExpression $exp) use ($root) {
                        return $exp->between('lft', $root->lft, $root->rght);
                    }
                )
                ->orderBy(['lft' => 'asc'])
                ->all()
                ->toArray();
        } else {
            $acos = [];
        }
        $this->set('acos', $acos);
        $this->set('data', $data);
        $groups = json_decode(
            file_get_contents(Configure::read('App.paths.controllers_group'))
        );
        sort($groups);
        $this->set('groups', array_combine($groups, $groups));
    }

    /**
     * Permet de récupérer la liste des controlleurs de l'application
     *
     * @return array
     */
    private function getControllers(): array
    {
        $results = [];
        $files = glob(APP . 'Controller' . DS . '*Controller.php');
        foreach ($files as $file) {
            $info = pathinfo($file);
            if ($info['filename'] !== 'AppController') {
                $results[] = $info['filename'];
            }
        }
        return $results;
    }

    /**
     * Génère les acos à partir d'une liste de $controller::$action
     * Ajoute les droits en cas d'accès par défaut
     *
     * @param array $news
     * @throws Exception
     */
    private function newPermissions(array $news)
    {
        /** @var ArosTable $Aros */
        $Aros = $this->fetchTable('Aros');
        /** @var AcosTable $Acos */
        $Acos = $this->fetchTable('Acos');
        /** @var ArosAcosTable $Permissions */
        $Permissions = $this->fetchTable('ArosAcos');
        $aros = $Aros->find()->all()->toArray();
        foreach (array_keys($news) as $new) {
            [$controller, $action] = explode('::', $new);
            $aco = $Acos->newEntity([], ['validate' => false]);
            $aco->set('parent_id', $this->getAcoId($controller));
            $aco->set('alias', $action);
            $aco->set('model', $controller);
            $doublon = $Acos->exists(
                [
                    'parent_id' => $aco->parent_id,
                    'alias' => $aco->alias,
                    'model' => $aco->model,
                ]
            );
            if ($doublon) {
                trigger_error(
                    __(
                        "Tentative de création d'aco en doublon {0}::{1}",
                        $controller,
                        $action
                    ),
                    E_USER_WARNING
                );
                continue;
            }

            if (!$Acos->save($aco)) {
                throw new Exception(
                    __(
                        "Impossible de sauvegarder un aco pour {0}::{1} : {2}",
                        $controller,
                        $action,
                        var_export($aco->getErrors(), true)
                    )
                );
            }
            $data = $this->getRequest()->getData($controller . '.' . $action);
            if (!empty($data['accesParDefaut'])) {
                foreach ($aros as $aro) {
                    $perm = $Permissions->newEntity(
                        [
                            'aco_id' => $aco->id,
                            'aro_id' => $aro->id,
                            '_create' => 1,
                            '_read' => 1,
                            '_update' => 1,
                            '_delete' => 1,
                        ]
                    );
                    if (!$Permissions->save($perm)) {
                        throw new Exception(
                            __(
                                "Impossible de sauvegarder une permission pour {0}::{1} 
                                aco_id:{2}, aro_id:{3} : {4}",
                                $controller,
                                $action,
                                var_export($aco->getErrors(), true),
                                $aco->id,
                                $aro->id
                            )
                        );
                    }
                }
            }
        }
    }

    /**
     * Assure l'existance dans les acos du controlleur et renvoi son id
     *
     * @param string $controller
     * @return int id
     */
    private function getAcoId(string $controller): int
    {
        if (in_array($controller, $this->controllers)) {
            return $this->controllers[$controller];
        }
        /** @var AcosTable $Acos */
        $Acos = $this->fetchTable('Acos');
        switch ($controller) {
            case 'root':
                $conditions = ['alias' => 'root', 'model IS' => null];
                break;
            case 'controllers':
                $conditions = ['alias' => $controller, 'model' => 'root'];
                break;
            default:
                $conditions = [
                    'alias' => $controller,
                    'model' => 'controllers',
                ];
        }
        $entity = $Acos->find()
            ->where($conditions)
            ->first();
        if ($entity) {
            $this->controllers[$controller] = $entity->id;
            return $entity->id;
        }
        $entity = $Acos->newEntity([], ['validate' => false]);
        switch ($controller) {
            case 'root':
                $data = ['alias' => 'root'];
                break;
            case 'controllers':
                $data = [
                    'alias' => 'controllers',
                    'model' => 'root',
                    'parent_id' => $this->getAcoId('root'),
                ];
                break;
            default:
                $data = [
                    'alias' => $controller,
                    'model' => 'controllers',
                    'parent_id' => $this->getAcoId('controllers'),
                ];
        }
        $Acos->patchEntity($entity, $data);
        $Acos->save($entity);
        $this->controllers[$controller] = $entity->id;
        return $entity->id;
    }

    /**
     * Met à jour les acos à partir d'une liste de $controller::$action
     * Défini la logique du commeDroit et de aucunsDroits
     *
     * @param array $updates
     */
    private function updatePermissions(array $updates)
    {
        /** @var AcosTable $Acos */
        $Acos = $this->fetchTable('Acos');
        /** @var ArosAcosTable $Permissions */
        $Permissions = $this->fetchTable('ArosAcos');
        foreach (array_keys($updates) as $update) {
            [$controller, $action] = explode('::', $update);
            $entity = $Acos->find()
                ->where(['alias' => $action, 'model' => $controller])
                ->first();

            if (!$entity) {
                continue;
            }
            $query = $Permissions->updateQuery()
                ->where(['aco_id' => $entity->id]);

            // Gestion du comme droit
            $data = $this->getRequest()->getData($controller . '.' . $action);
            if (!empty($data['commeDroits'])) {
                [$pc, $pa] = explode('::', $data['commeDroits']);
                $parent = $Acos->find()
                    ->select(['id'])
                    ->where(['alias' => $pa, 'model' => $pc])
                    ->first();
                if ($parent) {
                    $entity->set('parent_id', $parent->id);
                    $Acos->save($entity);
                    $query->set(
                        [
                            '_read' => 0,
                            '_update' => 0,
                            '_create' => 0,
                            '_delete' => 0,
                        ]
                    )->execute();
                }
                continue;
            }

            // Attribu à nouveau le controller comme parent si il étais en commeDroits
            $entity->set('parent_id', $this->getAcoId($controller));
        }
    }

    /**
     * Permet de mettre à jour les aco pour api
     * @param array $data
     */
    private function updateApiActions($data)
    {
        /** @var AcosTable $Acos */
        $Acos = $this->fetchTable('Acos');
        $root = $Acos->find()
            ->where(['alias' => 'api', 'model' => 'root'])
            ->first();
        if (!$root) {
            $alpha = $Acos->find()
                ->where(['parent_id IS' => null, 'alias' => 'root'])
                ->first();
            $root = $Acos->newEntity(
                [
                    'alias' => 'api',
                    'model' => 'root',
                    'parent_id' => $alpha->get('id'),
                ]
            );
            $Acos->saveOrFail($root);
        }

        foreach (array_keys($data) as $model) {
            $conditions = [
                'parent_id' => $root->get('id'),
                'alias' => $model,
                'model' => 'api',
            ];
            $aco = $Acos->find()
                ->where($conditions)
                ->first();
            if (!$aco) {
                $aco = $Acos->newEntity($conditions);
                $Acos->saveOrFail($aco);
            }
        }
    }

    /**
     * liste les permissions des roles communs
     * @throws Exception
     */
    public function permissions()
    {
        $exportFile = Configure::read(
            'Devs.export_roles_path',
            RESOURCES . 'export_roles.json'
        );
        $exportJson = json_decode(file_get_contents($exportFile), true);
        $pathToControllersJson = Configure::read(
            'App.paths.controllers_rules',
            RESOURCES . 'controllers.json'
        );
        $controllerJson = json_decode(
            file_get_contents($pathToControllersJson),
            true
        );
        $data = [];
        $translator = new TranslateHelper(new View());
        foreach ($controllerJson as $controller => $actions) {
            foreach ($actions as $action => $params) {
                if (($params['invisible'] ?? '1') === '1' || ($params['commeDroits'] ?? false)) {
                    continue;
                }
                $path = "root/controllers/$controller/$action";
                $tr = [
                    'path' => $path,
                    'traduction' => $translator->permission(
                        $controller,
                        $action
                    ),
                    'controller' => $controller,
                    'action' => $action,
                    'type' => 'controllers',
                    'group' => $params['group'] ?? null,
                ];
                $defined = false;
                foreach ($exportJson as $role => $permissions) {
                    if (isset($permissions[$path])) {
                        $tr[$role] = $permissions[$path] === '1111';
                        $defined = true;
                    } else {
                        $tr[$role] = null;
                    }
                }
                $tr['defined'] = $defined;
                $data[] = $tr;
            }
        }

        $pathToApiJson = Configure::read(
            'App.paths.apis_rules',
            RESOURCES . 'apis.json'
        );
        $apiJson = json_decode(file_get_contents($pathToApiJson), true);
        foreach ($apiJson as $controller => $actions) {
            foreach ($actions as $action => $params) {
                if (($params['invisible'] ?? '1') === '1' || ($params['commeDroits'] ?? false)) {
                    continue;
                }
                $path = "root/api/$controller";
                if ($action !== 'default') {
                    $path .= '/' . $action;
                }
                $tr = [
                    'path' => $path,
                    'traduction' => $translator->permission(
                        $controller,
                        $action
                    ),
                    'controller' => $controller,
                    'action' => $action,
                    'type' => 'api',
                    'group' => $params['group'] ?? null,
                ];
                $defined = false;
                foreach ($exportJson as $role => $permissions) {
                    if (!isset($permissions[$path])) {
                        $tr[$role] = null;
                    } elseif ($permissions[$path] === '1111') {
                        $tr[$role] = true;
                        $defined = true;
                    } elseif ($permissions[$path] === '0000') {
                        $tr[$role] = false;
                        $defined = true;
                    } else {
                        $tr[$role] = 'mixed';
                        $defined = true;
                    }
                }
                $tr['defined'] = $defined;
                $data[] = $tr;
            }
        }
        // order by type, group, controller, action
        usort(
            $data,
            function ($a, $b) {
                $type = strcmp($b['type'], $a['type']);
                if ($type === 0) {
                    $group = strcmp($a['group'], $b['group']);
                    if ($group === 0) {
                        $controller = strcmp(
                            $a['controller'],
                            $b['controller']
                        );
                        if ($controller === 0) {
                            return strcmp($a['action'], $b['action']);
                        }
                        return $controller;
                    }
                    return $group;
                }
                return $type;
            }
        );
        $this->set('data', $data);

        // options
        $this->set('roles', array_keys($exportJson));
    }

    /**
     * Défini une permission pour un role
     */
    public function setPermission()
    {
        $request = $this->getRequest();
        $request->allowMethod('post');
        $role = $request->getData('role');
        $path = $request->getData('path');
        $value = $request->getData('value');

        /** @var RolesTable $Roles */
        $Roles = $this->fetchTable('Roles');
        /** @var ArosAcosTable $Permissions */
        $Permissions = $this->fetchTable('ArosAcos');
        $aro_id = $Roles->find()
            ->select(['aro_id' => 'Aros.foreign_key'])
            ->innerJoinWith('Aros')
            ->where(['Roles.name' => $role, 'org_entity_id IS' => null])
            ->firstOrFail()
            ->get('aro_id');
        $aro = [
            'foreign_key' => $aro_id,
            'model' => 'Roles',
        ];

        $exportFile = Configure::read(
            'Devs.export_roles_path',
            RESOURCES . 'export_roles.json'
        );
        $exportJson = json_decode(file_get_contents($exportFile), true);
        if ($value === 'null') {
            unset($exportJson[$role][$path]);
            $Permissions->allow($aro, $path, '*', 0);
        } else {
            $exportJson[$role][$path] = $value === 'true' ? '1111' : '0000';
            $Permissions->allow(
                $aro,
                $path,
                '*',
                $value === 'true' ? 1 : -1
            );
            ksort($exportJson[$role]);
        }
        if (
            !file_put_contents(
                $exportFile,
                json_encode($exportJson, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
            )
        ) {
            throw new GenericException('write error');
        }
        return $this->renderDataToJson('done');
    }

    /**
     * Affiche un email
     * @param string $b64Filename
     * @return Response
     */
    public function debugMail(string $b64Filename)
    {
        $basePath = Configure::read('debug_mails_basepath', TMP . 'debug_mails');
        $filename = base64_decode($b64Filename);
        if (!is_file($basePath . DS . $filename)) {
            return $this->getResponse()
                ->withStatus(404)
                ->withType('txt')
                ->withStringBody(__("Le fichier n'existe plus"));
        }
        return $this->getResponse()
            ->withType('html')
            ->withStringBody(file_get_contents($basePath . DS . $filename));
    }

    /**
     * Attestation d’élimination
     * @param string      $destruction_request_id
     * @param string|bool $definitive
     * @return Response
     * @throws VolumeException
     */
    public function destructionCertificate(string $destruction_request_id, $definitive = false)
    {
        /** @var DestructionRequest $destructionRequest */
        $destructionRequest = $this->fetchTable('DestructionRequests')
            ->get($destruction_request_id, ['contain' => ['ArchivalAgencies', 'OriginatingAgencies']]);
        return $this->getResponse()
            ->withStringBody($destructionRequest->getDestructionCertificateHtml($definitive));
    }

    /**
     * Attestation de restitution
     * @param string $restitution_request_id
     * @return Response
     * @throws VolumeException
     */
    public function restitutionCertificate(string $restitution_request_id)
    {
        /** @var RestitutionRequest $restitutionRequest */
        $restitutionRequest = $this->fetchTable('RestitutionRequests')
            ->get($restitution_request_id, ['contain' => ['ArchivalAgencies', 'OriginatingAgencies']]);
        return $this->getResponse()
            ->withStringBody($restitutionRequest->getRestitutionCertificateHtml());
    }

    /**
     * Attestation d'élimination d'une restitution
     * @param string      $restitution_request_id
     * @param string|bool $definitive
     * @return Response
     * @throws VolumeException
     */
    public function restitutionDestructionCertificate(string $restitution_request_id, $definitive = false)
    {
        /** @var RestitutionRequest $restitutionRequest */
        $restitutionRequest = $this->fetchTable('RestitutionRequests')
            ->get($restitution_request_id, ['contain' => ['ArchivalAgencies', 'OriginatingAgencies']]);
        return $this->getResponse()
            ->withStringBody($restitutionRequest->getDestructionCertificateHtml($definitive));
    }

    /**
     * Liste les icones disponnibles
     * @return void
     * @throws SourceException
     */
    public function icons()
    {
        $css = [
            'font-awesome5.min.css',
            'font-awesome.min.css',
        ];
        $icons = [];
        foreach ($css as $filename) {
            $path = WWW_ROOT . 'css' . DS . $filename;
            if (is_file($path)) {
                $parser = new CSS\Parser(file_get_contents($path));
                $cssDocument = $parser->parse();
                $icons[$filename] = $this->extractIconsFromCssDocument($cssDocument);
            }
        }
        $this->set('icons', $icons);
    }

    /**
     * Donne une liste d'icons
     * @param CSS\CSSList\Document $cssDocument
     * @return array ['fa-yin-yang' => bin, ...]
     */
    private function extractIconsFromCssDocument(CSS\CSSList\Document $cssDocument): array
    {
        $icons = [];
        foreach ($cssDocument->getAllDeclarationBlocks() as $block) {
            /** @var CSS\Rule\Rule[] $rules */
            $rules = $block->getRulesAssoc();
            if (!isset($rules['content'])) {
                continue;
            }
            /** @var CSS\Value\CSSString $value */
            $value = $rules['content']->getValue();
            foreach ($block->getSelectors() as $selector) {
                $str = $selector->getSelector();
                if (preg_match('/^\.(fa-[^:]+):before$/', $str, $m)) {
                    $icons[$m[1]] = $value->getString();
                }
            }
        }
        return $icons;
    }
}
