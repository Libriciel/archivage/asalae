<?php

/**
 * Asalae\Controller\InstallController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EmailsComponent;
use Asalae\Form\InstallForm;
use Asalae\Model\Table\OrgEntitiesTable;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use Cake\Controller\Component\FlashComponent;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\Mailer\Mailer;
use Cake\Mailer\TransportFactory;
use Cake\Utility\Hash;
use ErrorException;
use Exception;
use Throwable;

/**
 * Installation de l'application
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property OrgEntitiesTable $OrgEntities
 */
class InstallController extends Controller
{
    use RenderDataTrait;
    use Admins\TimestampersTrait {
        testTimestamper as private;
        timestampers as private;
        addTimestamper as private;
        editTimestamper as private;
        deleteTimestamper as private;
        addTimestamperCert as private;
        testTimestamperTSA as private;
    }

    /**
     * Action unique de l'installation
     * @throws Exception
     */
    public function index()
    {
        /** @use FlashComponent */
        $this->loadComponent('Flash');
        $this->viewBuilder()->setLayout('not_connected');
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        if (
            !is_file($pathToLocalConfig) && !is_writable(
                $dir = dirname($pathToLocalConfig)
            )
        ) {
            $this->Flash->error(
                __("Le dossier ''{0}'' n'est pas inscriptible", $dir)
            );
        }
        try {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = $this->fetchTable('OrgEntities');
            if (is_file($pathToLocalConfig)) {
                $pathToLocal = include $pathToLocalConfig;
                $config = is_file($pathToLocal)
                    ? json_decode(file_get_contents($pathToLocal), true)
                    : [];
            } else {
                $config = [];
            }
            if (
                Hash::get($config, 'Security.salt')
                && $OrgEntities->find()->count()
            ) {
                return $this->redirect('/');
            }
        } catch (Exception) {
        }

        $form = new InstallForm();

        if (
            $this->getRequest()->is('post')
            && $this->getRequest()->getData('initializeDatabase')
        ) {
            $Exec = Utility::get('Exec');
            $Exec->rawCommand(
                ROOT . DS . 'bin' . DS . 'cake migrations migrate 2>&1'
                . '&& ' . ROOT . DS . 'bin' . DS . 'cake migrations seed 2>&1'
                . '&& ' . ROOT . DS . 'bin' . DS . 'cake update 2>&1'
                . '&& ' . ROOT . DS . 'bin' . DS . 'cake roles_perms import '
                . RESOURCES . 'export_roles.json --keep 2>&1',
                $output,
                $code
            );
            $method = $code === 0 ? 'success' : 'error';
            $message = implode("<br>\n", $output);
            if (strpos($message, 'PDOException') !== false) {
                $method = 'error';
            }
            $this->Flash->$method(
                implode("<br>\n", $output),
                ['escape' => false]
            );
        } elseif (
            $this->getRequest()->is('post') && $form->execute(
                $this->getRequest()->getData()
            )
        ) {
            $admin = (json_decode(
                Configure::read('App.paths.administrators_json'),
                true
            ) ?: [null])[0];
            unset($admin['password']);
            $session = $this->getRequest()->getSession();
            $session->write('Admin.tech', true);
            $session->write('Admin.data', $admin);
            return $this->redirect('/admins');
        }

        $this->set('form', $form);
    }

    /**
     * Envoi des emails de test
     * @return Response|null
     * @throws Exception
     */
    public function sendTestMail()
    {
        $request = $this->getRequest();
        $request->allowMethod('post');
        /** @use FlashComponent */
        $this->loadComponent('Flash');
        $this->viewBuilder()->setLayout('not_connected');
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        if (
            !is_file($pathToLocalConfig) && !is_writable(
                $dir = dirname($pathToLocalConfig)
            )
        ) {
            $this->Flash->error(
                __("Le dossier ''{0}'' n'est pas inscriptible", $dir)
            );
        }
        try {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = $this->fetchTable('OrgEntities');
            if ($OrgEntities->find()->count()) {
                return $this->redirect('/');
            }
        } catch (Exception) {
        }

        try {
            $request = $this->getRequest();
            TransportFactory::drop('default');
            TransportFactory::setConfig(
                [
                    'default' => [
                        'className' => $request->getData(
                            'Config__EmailTransport__default__className'
                        ),
                        // The following keys are used in SMTP transports
                        'host' => $request->getData(
                            'Config__EmailTransport__default__host'
                        ),
                        'port' => $request->getData(
                            'Config__EmailTransport__default__port'
                        ),
                        'timeout' => 30,
                        'username' => $request->getData(
                            'Config__EmailTransport__default__username'
                        ),
                        'password' => $request->getData(
                            'Config__EmailTransport__default__password'
                        ),
                        'client' => null,
                        'tls' => null,
                        'url' => env('EMAIL_TRANSPORT_DEFAULT_URL'),
                    ],
                ]
            );

            $emailAdress = $this->getRequest()->getData('email');
            $success = !empty($emailAdress);
            $session = $this->getRequest()->getSession();
            /** @var Mailer $email */
            $email = clone Utility::get(Mailer::class);
            $sign = $session->read(
                'ConfigArchivalAgency.mail-html-signature'
            );
            $email->setViewVars(
                [
                    'htmlSignature' => $sign ?: null,
                ]
            );
            $email->viewBuilder()
                ->setTemplate('test')
                ->setLayout('default')
                ->addHelpers(['Html', 'Url']);
            $success = $success && $email->setEmailFormat('both')
                    ->setSubject('[asalae] ' . __("Email de test"))
                    ->setFrom(
                        $request->getData('Config__Email__default__from')
                    )
                    ->setTo($emailAdress)
                    ->send();

            /** @use EmailsComponent */
            $this->loadComponent('Emails');
            if ($response = $this->Emails->debug($email)) {
                return $response;
            }
        } catch (Exception) {
            $success = false;
        }

        return $this->renderDataToJson(['success' => $success]);
    }

    /**
     * Permet de tester l'horodatage en local
     * @return Response
     * @throws ErrorException|Throwable
     */
    public function testTimestamper()
    {
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        try {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = $this->fetchTable('OrgEntities');
            $config = is_file($pathToLocalConfig)
                ? json_decode(
                    file_get_contents(
                        include $pathToLocalConfig
                    ),
                    true
                )
                : [];
            if (
                Hash::get($config, 'Security.salt') && $OrgEntities->find()->count()
            ) {
                return $this->redirect('/');
            }
        } catch (Exception) {
        }
        return $this->testTimestamperTSA('Timestamper__');
    }
}
