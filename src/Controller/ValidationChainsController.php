<?php

/**
 * Asalae\Controller\ValidationChainsController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Entity\ValidationActor;
use Asalae\Model\Entity\ValidationChain;
use Asalae\Model\Table\ValidationActorsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationProcessesTable;
use Asalae\Model\Table\ValidationStagesTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use Exception;

/**
 * Circuits de validation
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ValidationActorsTable ValidationActors
 * @property ValidationChainsTable ValidationChains
 * @property ValidationProcessesTable ValidationProcesses
 * @property ValidationStagesTable ValidationStages
 */
class ValidationChainsController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'validation-chains-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'ValidationChains.app_type' => 'asc',
            'ValidationChains.app_meta LIKE \'%"default":true%\'' => 'desc',
            'ValidationChains.id' => 'asc',
        ],
    ];

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default' => [
                'post' => [
                    'function' => 'apiAdd',
                ],
            ],
        ];
    }

    /**
     * Liste des circuits de validation
     */
    public function index()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        $query = $this->Seal->archivalAgency()
            ->table('ValidationChains')
            ->find('seal')
            ->contain(
                [
                    'ValidationStages' => [
                        'sort' => 'ord',
                        'ValidationActors' => [
                            'sort' => 'ValidationActors.id',
                            'Users',
                        ],
                    ],
                ]
            );

        $IndexComponent->setQuery($query)
            ->filter(
                'active',
                function ($v) {
                    return [
                        'app_meta LIKE' => '%"active":' . ($v ? 'true'
                                : 'false') . '%',
                    ];
                }
            )
            ->filter('app_type', IndexComponent::FILTER_IN)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter(
                'default',
                function ($v) {
                    return [
                        'app_meta LIKE' => '%"default":' . ($v ? 'true'
                                : 'false') . '%',
                    ];
                }
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('modified', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('name', IndexComponent::FILTER_ILIKE);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->map(
            function (EntityInterface $e) {
                $e->setVirtual(['app_typetrad', 'deletable', 'has_mail_agent']);
                return $e;
            }
        )->toArray();
        $this->set('data', $data);
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $this->set('app_types', $ValidationChains->options('app_type'));

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * Ajout d'un circuit de validation
     * @param string|null $copy validation_chain_id -> circuit à copier
     * @return Response
     * @throws Exception
     */
    public function add(string $copy = null)
    {
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $entity = $ValidationChains->newEntity(
            ['active' => true],
            ['validate' => false]
        );
        if ($copy) {
            $prev = $this->Seal->archivalAgency()
                ->table('ValidationChains')
                ->find('seal')
                ->where(['ValidationChains.id' => $copy])
                ->contain(['ValidationStages' => ['ValidationActors']])
                ->firstOrFail();
            $dataPrev = $prev->toArray();
            unset($dataPrev['id'], $dataPrev['validation_stages']);
            $ValidationChains->patchEntity(
                $entity,
                $dataPrev,
                ['validate' => false]
            );
        }

        if ($this->getRequest()->is('post')) {
            $data = [
                'org_entity_id' => $this->archivalAgencyId,
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'app_type' => $this->getRequest()->getData('app_type'),
                'default' => (bool)$this->getRequest()->getData('default'),
                'active' => (bool)$this->getRequest()->getData('active'),
                'created_user_id' => $this->userId,
            ];
            $ValidationChains->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');

            $conn = $ValidationChains->getConnection();
            $conn->begin();
            $success = (bool)$ValidationChains->save($entity);
            if ($success && isset($prev)) {
                /** @var ValidationStagesTable $ValidationStages */
                $ValidationStages = $this->fetchTable('ValidationStages');
                /** @var ValidationActorsTable $ValidationActors */
                $ValidationActors = $this->fetchTable('ValidationActors');
                /** @var EntityInterface[] $stages */
                $stages = $prev->get('validation_stages');
                foreach ($stages as $prevstage) {
                    $stage = $ValidationStages->newEntity(
                        [
                            'validation_chain_id' => $entity->id,
                            'name' => $prevstage->get('name'),
                            'description' => $prevstage->get('description'),
                            'all_actors_to_complete' => $prevstage->get(
                                'all_actors_to_complete'
                            ),
                            'ord' => $prevstage->get('ord'),
                            'app_type' => $prevstage->get('app_type'),
                            'app_meta' => $prevstage->get('app_meta'),
                        ],
                        ['validate' => false, 'setter' => false]
                    );
                    $ValidationStages->saveOrFail($stage);
                    /** @var EntityInterface[] $actors */
                    $actors = $prevstage->get('validation_actors');
                    foreach ($actors as $prevactor) {
                        $actor = $ValidationStages->newEntity(
                            [
                                'validation_stage_id' => $stage->id,
                                'app_type' => $prevactor->get('app_type'),
                                'app_key' => $prevactor->get('app_key'),
                                'app_meta' => $prevactor->get('app_meta'),
                                'app_foreign_key' => $prevactor->get(
                                    'app_foreign_key'
                                ),
                            ],
                            ['validate' => false, 'setter' => false]
                        );
                        $ValidationActors->saveOrFail($actor);
                    }
                }
            }
            if ($success) {
                $conn->commit();
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAdd($entity);
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);
        $this->set('app_types', $ValidationChains->options('app_type'));
        $defaults = $this->Seal->archivalAgency()
            ->table('ValidationChains')
            ->find('seal')
            ->select(['ValidationChains.app_type'])
            ->where(
                [
                    ['ValidationChains.app_meta LIKE' => '%"default":true%'],
                    ['ValidationChains.app_meta LIKE' => '%"active":true%'],
                ]
            )
            ->disableHydration()
            ->all()
            ->map(
                function ($v) {
                    return $v['app_type'];
                }
            );
        $this->set('defaults', $defaults);
    }

    /**
     * Modification du circuit de validation
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var ValidationChain $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('ValidationChains')
            ->find('seal')
            ->where(['ValidationChains.id' => $id])
            ->firstOrFail();

        if ($this->getRequest()->is('put')) {
            $data = [
                'org_entity_id' => $this->archivalAgencyId,
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'app_type' => $this->getRequest()->getData('app_type'),
                'default' => (bool)$this->getRequest()->getData('default'),
                'active' => (bool)$this->getRequest()->getData('active'),
            ];
            /** @var ValidationChainsTable $ValidationChains */
            $ValidationChains = $this->fetchTable('ValidationChains');
            $ValidationChains->patchEntity($entity, $data);

            $newEntry = [
                'user_id' => $this->userId,
                'date' => date('Y-m-d H:i:s'),
                'fields' => $entity->getDirty(),
            ];
            $entity->appendModified($newEntry);

            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($ValidationChains->save($entity)) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logEdit($entity);
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);

        $isOnlyActiveOfThisType = $this->Seal->archivalAgency()
                ->table('ValidationChains')
                ->find('seal')
                ->where(
                    [
                        'ValidationChains.id !=' => $entity->get('id'),
                        'ValidationChains.app_type' => $entity->get('app_type') ?: '',
                        ['ValidationChains.app_meta LIKE' => '%"active":true%'],
                    ]
                )
                ->disableHydration()
                ->all()
                ->count() === 0;

        $this->set('isOnlyActiveOfThisType', $isOnlyActiveOfThisType);

        // Options
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $this->set('app_types', $ValidationChains->options('app_type'));
    }

    /**
     * Suppression d'un circuit de validation
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        /** @var ValidationChain $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('ValidationChains')
            ->find('seal')
            ->where(['ValidationChains.id' => $id])
            ->firstOrFail();

        if (!$entity->get('deletable')) {
            throw new ForbiddenException(
                __("Tentative de suppression d'une entité protégée")
            );
        }

        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $conn = $ValidationChains->getConnection();
        $conn->begin();
        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logDelete($entity);
        if ($ValidationChains->delete($entity)) {
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Visualisation des données
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        /** @var ValidationChain $entity */
        $chain = $this->Seal->archivalAgency()
            ->table('ValidationChains')
            ->find('seal')
            ->where(['ValidationChains.id' => $id])
            ->contain(
                [
                    'ValidationStages' => [
                        'sort' => 'ord',
                        'ValidationActors' => ['Users'],
                    ],
                ]
            )
            ->firstOrFail();
        $this->set('chain', $chain);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($chain);
    }

    /**
     * Défini un circuit par défaut
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function setDefault(string $id)
    {
        /** @var ValidationChain $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('ValidationChains')
            ->find('seal')
            ->where(['ValidationChains.id' => $id])
            ->firstOrFail();
        $entity->set('default', true);
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        return $this->renderDataToJson(
            ['success' => $ValidationChains->save($entity)]
        );
    }

    /**
     * Renvoi l'email pour la validation d'un transfert
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function resendMail(string $id)
    {
        $processes = $this->Seal->archivalAgencyChilds()
            ->table('ValidationProcesses')
            ->find('seal')
            ->innerJoinWith('ValidationChains')
            ->where(
                [
                    'ValidationChains.id' => $id,
                    'CurrentStages.id IS NOT' => null,
                ]
            )
            ->contain(
                [
                    'CurrentStages' => [
                        'ValidationActors' => function (Query $q) {
                            return $q->where(
                                ['ValidationActors.app_type' => 'MAIL']
                            );
                        },
                    ],
                ]
            );
        foreach ($processes as $process) {
            /** @var ValidationActor $actor */
            foreach (
                Hash::get(
                    $process,
                    'current_stage.validation_actors',
                    []
                ) as $actor
            ) {
                $actor->notify($process);
            }
        }
        return $this->renderJson('"done"');
    }

    /**
     * Création via API
     */
    protected function apiAdd()
    {
        $request = $this->getRequest();
        $default = null;
        if ($request->getData('default')) {
            $default = $request->getData('default') === 'true';
        }
        $active = null;
        if ($request->getData('active')) {
            $active = $request->getData('active') === 'true';
        }
        $data = [
            'org_entity_id' => $request->getData('org_entity_id'),
            'name' => $request->getData('name'),
            'description' => $request->getData('description'),
            'app_type' => $request->getData('app_type'),
            'default' => $default ?: false,
            'active' => $active,
            'created_user_id' => $this->userId,
        ];
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $entity = $ValidationChains->newEntity($data);
        $conn = $ValidationChains->getConnection();
        $conn->begin();
        if ($ValidationChains->save($entity)) {
            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logAdd($entity);
            $conn->commit();
            return $this->renderData(['success' => true, 'id' => $entity->id]);
        } else {
            $conn->rollback();
            return $this->renderData(
                ['success' => false, 'errors' => $entity->getErrors()]
            );
        }
    }
}
