<?php

/**
 * Asalae\Controller\WebservicesController
 */

namespace Asalae\Controller;

use Asalae\Model\Table\UsersTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\AclComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\FormatError;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Table\AcosTable;
use Asalae\Model\Table\ArosAcosTable;
use Asalae\Model\Table\ArosTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\RolesTable;
use Asalae\View\Helper\TranslateHelper;
use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\Http\Response;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\View\View;
use Exception;
use PDOException;

/**
 * Webservices
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AclComponent     Acl
 * @property AcosTable        Acos
 * @property ArosAcosTable    Permissions
 * @property ArosTable        Aros
 * @property EventLogsTable   EventLogs
 * @property OrgEntitiesTable OrgEntities
 * @property RolesTable       Roles
 */
class WebservicesController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * Permissions préconfigurés
     * options du formulaire add/edit pour préremplissage des permissions
     * @deprecated since 2.0.0a5
     */
    public const array OPTIONS_PERMS = [
        'versant' => ['Transfers' => ['create', 'read']],
        'producteur' => ['Transfers' => ['read']],
        'admin' => [
            'Users' => ['create', 'read', 'update', 'delete'],
            'Eaccpfs' => ['create', 'read', 'update', 'delete'],
        ],
    ];

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'webservices-table';
    /**
     * @var null|bool mémoire du retour de fonction de isConnected()
     */
    private $isConnected = null;

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'monitoring',
            'ping',
            'version',
            'whoami',
        ];
    }

    /**
     * Called before the controller action. You can use this method to configure and customize components
     * or perform logic that needs to happen before each controller action.
     *
     * @param EventInterface $event An Event instance
     * @return Response|void|null
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $AuthenticationComponent->addUnauthenticatedActions(
            ['authorize', 'accessToken', 'refreshToken', 'ping']
        );

        $this->Rest->setApiActions(
            [
                'default' => ['*' => ['function' => 'forbidden']],
                'monitoring',
                'ping',
                'version',
                'whoami',
            ]
        );

        return parent::beforeFilter($event);
    }

    /**
     * Liste des webservices
     */
    public function index()
    {
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }
        $this->getRequest()->allowMethod('ajax');
        $this->set('tableId', self::TABLE_INDEX);

        $saTypeId = $this->fetchTable('TypeEntities')->find()
            ->select(['id'])
            ->where(['code' => 'SA'])
            ->firstOrFail()
            ->get('id');
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $query = $Users->find()
            ->innerJoinWith('OrgEntities')
            ->innerJoinWith('OrgEntities.ArchivalAgencies')
            ->where(['ArchivalAgencies.type_entity_id' => $saTypeId])
            ->where(['agent_type' => 'software'])
            ->contain(['OrgEntities' => ['ArchivalAgencies']]);
        if ($this->archivalAgency) {
            $query->where(
                [
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            );
        }
        $data = $query->all()
            ->map(
                function (EntityInterface $entity) {
                    $entity->setVirtual(['deletable']);
                    return $entity;
                }
            )
            ->toArray();
        $this->set('webservices', $data);
    }

    /**
     * Vérifi la connexion à la base de donnée
     *
     * @return bool
     */
    private function isConnected(): bool
    {
        if ($this->isConnected !== null) {
            return $this->isConnected;
        }
        try {
            $conn = ConnectionManager::get('default');
            $conn->execute('select 1');
            $this->isConnected = true;
        } catch (PDOException | MissingConnectionException) {
            $this->isConnected = false;
        }
        return $this->isConnected;
    }

    /**
     * Ajout d'un webservice
     * @param string|null $org_entity_id
     * @return Response
     * @throws Exception
     */
    public function add(string $org_entity_id = null)
    {
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $entity = $Users->newEntity(
            ['agent_type' => 'software'],
            ['validate' => false]
        );

        $request = $this->getRequest();
        $org_entity_id = $org_entity_id ?: $request->getData('org_entity_id');
        if ($org_entity_id !== null) {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = $this->fetchTable('OrgEntities');
            $query = $OrgEntities->find()
                ->where(['id' => $org_entity_id]);
            if ($this->orgEntity) {
                $query->andWhere(
                    [
                        'OrgEntities.lft >=' => $this->orgEntity->get('lft'),
                        'OrgEntities.rght <=' => $this->orgEntity->get('rght'),
                    ]
                );
            }
            $orgEntity = $query->firstOrFail();
            $entity->set('org_entity', $orgEntity);
        }

        if ($request->is('post')) {
            $role_id = $request->getData('role_id');
            /** @var RolesTable $Roles */
            $Roles = $this->fetchTable('Roles');
            if ($request->getData('sha256') && $request->getData('password')) {
                $password = hash('sha256', $request->getData('password'));
                $confirm = hash(
                    'sha256',
                    $request->getData('confirm-password')
                );
            } else {
                $password = $request->getData('password');
                $confirm = $request->getData('confirm-password');
            }
            $data = [
                'org_entity_id' => $org_entity_id,
                'name' => $request->getData('name'),
                'email' => $request->getData('email'),
                'role_id' => $role_id,
                'active' => $request->getData('active'),
                'username' => $request->getData('username'),
                'password' => $password,
                'confirm-password' => $confirm,
                'agent_type' => 'software',
                'is_validator' => false,
                'use_cert' => false,
            ];
            $Users->patchEntity($entity, $data);
            $conn = $Users->getConnection();
            $conn->begin();
            $success = $Users->save($entity);
            if ($success) {
                $translateHelper = new TranslateHelper(new View());
                /** @var EventLogsTable $EventLogs */
                $EventLogs = $this->fetchTable('EventLogs');
                $entry = $EventLogs->newEntry(
                    'add_' . Inflector::singularize($this->getName()),
                    'success',
                    __(
                        "Ajouté ({0}) par l''{1} ({2})",
                        $translateHelper->permission($this->getName()),
                        $this->userId ? 'utilisateur' : 'administrateur',
                        $this->userId
                            ? Hash::get($this->user, 'username')
                            : $request->getSession()->read('Admin.data.username')
                    ),
                    $this->userId, // si utilisateur connecté, null sinon
                    $entity
                );
                $success = $EventLogs->save($entry);
                FormatError::logEntityErrors($entry);
            }
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                $json = $entity->toArray();
                $json['role'] = $Roles->get($json['role_id'])->toArray();
                unset($json['password'], $json['confirm-password']);
                $json['deletable'] = true;
                return $this->renderJson(json_encode($json));
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);

        // options
        $roles = [];
        if ($org_entity_id) {
            /** @var RolesTable $Roles */
            $Roles = $this->fetchTable('Roles');
            $roles = $Roles->find('list')
                ->innerJoinWith('TypeEntities')
                ->innerJoinWith('TypeEntities.OrgEntities')
                ->where(
                    [
                        'OrgEntities.id' => $org_entity_id,
                        'Roles.agent_type' => 'software',
                        'OR' => [
                            'Roles.code !=' => RolesTable::CODE_WS_ADMINISTRATOR,
                            'Roles.code IS' => null,
                        ],
                    ]
                )
                ->orderBy(['Roles.name'])
                ->toArray();
        } else {
            $this->setWsEntities();
        }
        $this->set('roles', $roles);
        $this->set('org_entity_id', $org_entity_id);
    }

    /**
     * Défini les entités qui peuvent avoir des webservices
     */
    private function setWsEntities()
    {
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $query = $OrgEntities->listOptions([], $this->Seal->orgEntityChilds())
            ->where(
                [
                    'OrgEntities.active' => true,
                ]
            )
            ->innerJoinWith('TypeEntities')
            ->innerJoinWith('TypeEntities.Roles')
            ->where(
                [
                    'Roles.agent_type' => 'software',
                    'OR' => [
                        'Roles.code !=' => RolesTable::CODE_WS_ADMINISTRATOR,
                        'Roles.code IS' => null,
                    ],
                ]
            )
            ->contain(['ArchivalAgencies'], true)
            ->disableHydration();
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)) {
            $targetEntity = $this->archivalAgency;
        } else {
            $targetEntity = $this->orgEntity;
        }
        if ($targetEntity) {
            $query->andWhere(
                [
                    'OrgEntities.lft >=' => $targetEntity->get('lft'),
                    'OrgEntities.rght <=' => $targetEntity->get('rght'),
                ]
            );
        }
        $entities = $query->toArray();
        $this->set('entities', $entities);
    }

    /**
     * Action d'édition d'un webservice
     * @param string $id
     * @return Response|null
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $query = $Users->find()
            ->where(
                [
                    'Users.id' => $id,
                    'Users.agent_type' => 'software',
                ]
            )
            ->contain(['OrgEntities' => ['ArchivalAgencies']]);
        if ($this->archivalAgency) {
            $query->where(['ArchivalAgencies.id' => $this->archivalAgencyId]);
        }
        $entity = $query
            ->firstOrFail();
        $entity->unset('password');
        $request = $this->getRequest();
        if ($request->is('put')) {
            $role_id = $request->getData('role_id');
            /** @var RolesTable $Roles */
            $Roles = $this->fetchTable('Roles');
            $data = [
                'org_entity_id' => $request->getData('org_entity_id'),
                'name' => $request->getData('name'),
                'email' => $request->getData('email'),
                'role_id' => $role_id,
                'role' => $role_id ? $Roles->get($role_id) : null,
                'active' => $request->getData('active'),
                'username' => $request->getData('username'),
                'agent_type' => 'software',
            ];
            $password = $request->getData('password');
            $confirm = $request->getData('confirm-password');
            if ($password) {
                if ($request->getData('sha256')) {
                    $password = hash('sha256', $password);
                    $confirm = hash(
                        'sha256',
                        $request->getData('confirm-password')
                    );
                }
                $data += [
                    'password' => $password,
                    'confirm-password' => $confirm,
                ];
            }
            $Users->patchEntity($entity, $data);
            $conn = $Users->getConnection();
            $conn->begin();
            $success = $Users->save($entity);
            if ($success) {
                $translateHelper = new TranslateHelper(new View());
                /** @var EventLogsTable $EventLogs */
                $EventLogs = $this->fetchTable('EventLogs');
                $entry = $EventLogs->newEntry(
                    'edit_' . Inflector::singularize($this->getName()),
                    'success',
                    __(
                        "Modifié ({0}) par l''{1} ({2})",
                        $translateHelper->permission($this->getName()),
                        $this->userId ? 'utilisateur' : 'administrateur',
                        $this->userId
                            ? Hash::get($this->user, 'username')
                            : $request->getSession()->read('Admin.data.username')
                    ),
                    $this->userId, // si utilisateur connecté, null sinon
                    $entity
                );
                $success = $EventLogs->save($entry);
                FormatError::logEntityErrors($entry);
            }
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                $query = $Users->find()
                    ->where(['Users.id' => $id])
                    ->disableHydration();
                $Users->setGenericUserContain($query);
                $json = $query->firstOrFail();
                unset($json['password'], $json['confirm-password']);
                return $this->renderJson(json_encode($json));
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);

        // options
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $sa = $entity->get('org_entity_id')
            ? $OrgEntities->getEntitySA($entity->get('org_entity_id'))
            : null;
        $condition = $entity->get('org_entity_id')
            ? ['OrgEntities.id' => $entity->get('org_entity_id')]
            : ['OrgEntities.id IS' => null];
        $roles = $OrgEntities->findByRoles($sa)
            ->where($condition)
            ->andWhere(
                [
                    'Roles.agent_type' => 'software',
                    'OR' => [
                        'Roles.code !=' => RolesTable::CODE_WS_ADMINISTRATOR,
                        'Roles.code IS' => null,
                    ],
                ]
            )
            ->all()
            ->toArray();
        $roles = array_reduce(
            $roles,
            function ($res, OrgEntity $v) {
                $res[$v->get('Roles')['id']] = $v->get('Roles')['name'];
                return $res;
            }
        );
        $this->set('roles', $roles);
        $this->setWsEntities();
    }

    /**
     * Suppression d'un webservice
     * @param string $id
     * @return Response
     */
    public function delete(string $id)
    {
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $query = $Users->find()
            ->where(
                [
                    'Users.id' => $id,
                    'Users.agent_type' => 'software',
                ]
            )
            ->contain(['OrgEntities' => ['ArchivalAgencies']]);
        if ($this->archivalAgency) {
            $query->where(['ArchivalAgencies.id' => $this->archivalAgencyId]);
        }
        $entity = $query
            ->firstOrFail();

        $success = $Users->delete($entity);
        if ($success) {
            $report = 'done';
            $translateHelper = new TranslateHelper(new View());
            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            $entry = $EventLogs->newEntry(
                'delete_' . Inflector::singularize($this->getName()),
                'success',
                __(
                    "Supprimé ({0}) par l'administrateur ({1})",
                    $translateHelper->permission($this->getName()),
                    $this->getRequest()->getSession()->read('Admin.data.username')
                ),
                $this->userId, // si utilisateur connecté, null sinon
                $entity
            );
            $EventLogs->saveOrFail($entry);
        } else {
            $report = 'Erreur lors de la suppression du webservice';
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Action api avec authentification
     * @return Response
     */
    protected function monitoring(): Response
    {
        $commands = [
            'check',
            'crons_probe',
            'job list',
        ];

        try {
            $Exec = Utility::get('Exec');
            $executions = array_reduce(
                $commands,
                fn($results, $command) => $results
                    + [$command => $Exec->command(CAKE_SHELL . ' ' . $command)->getArrayCopy()],
                []
            );

            return $this->renderJson(
                json_encode(['success' => true] + $executions)
            );
        } catch (Exception) {
            return $this->renderJson(json_encode(['success' => false]));
        }
    }

    /**
     * Action api sans authentification
     * @return Response|null
     */
    protected function ping(): ?Response
    {
        return $this->renderJson(json_encode("Pong!"));
    }

    /**
     * Action api avec authentification
     * @return Response|null
     */
    protected function version(): ?Response
    {
        return $this->renderJson(json_encode(ASALAE_VERSION_SHORT));
    }

    /**
     * Action api permettant d'obtenir son identité
     * Utile pour le développement
     * @return Response|null
     * @throws Exception
     */
    protected function whoami()
    {
        /** @var RolesTable $Roles */
        $Roles = $this->fetchTable('Roles');
        $rootRole = $Roles->find()
            ->where(
                [
                    'parent_id IS' => null,
                    'lft <=' => Hash::get($this->user, 'role.lft'),
                    'rght >=' => Hash::get($this->user, 'role.rght'),
                ]
            )
            ->orderByAsc('lft')
            ->firstOrFail();

        $data = [
            'username' => Hash::get($this->user, 'username'),
            'fullname' => Hash::get($this->user, 'name'),
            'entity_name' => $this->orgEntity->get('name'),
            'entity_identifier' => $this->orgEntity->get('identifier'),
            'archival_agency_name' => $this->archivalAgency->get('name'),
            'archival_agency_identifier' => $this->archivalAgency->get(
                'identifier'
            ),
            'role' => Hash::get($this->user, 'role.name'),
            'role_root' => $rootRole->get('name'),
        ];
        return $this->renderJson(json_encode($data));
    }

    /**
     * Action de visualisation
     * @param string $id
     */
    public function view(string $id)
    {
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $query = $Users->find()
            ->where(
                [
                    'Users.id' => $id,
                    'Users.agent_type' => 'software',
                ]
            )
            ->contain(['OrgEntities' => ['ArchivalAgencies']]);
        if ($this->archivalAgency) {
            $query->where(['ArchivalAgencies.id' => $this->archivalAgencyId]);
        }
        $user = $query
            ->firstOrFail();
        $this->set('user', $user);
    }
}
