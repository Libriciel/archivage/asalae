<?php

/**
 * Asalae\Controller\ArchiveUnitsController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Controller\Component\SearchEngineComponent;
use Asalae\Model\Entity\Archive;
use Asalae\Model\Entity\ArchiveBinary;
use Asalae\Model\Entity\ArchiveUnit;
use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Entity\PublicDescriptionArchiveFile;
use Asalae\Model\Entity\StoredFile;
use Asalae\Model\Table\AccessRuleCodesTable;
use Asalae\Model\Table\AccessRulesTable;
use Asalae\Model\Table\AgreementsTable;
use Asalae\Model\Table\AppraisalRuleCodesTable;
use Asalae\Model\Table\AppraisalRulesTable;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveDescriptionsTable;
use Asalae\Model\Table\ArchiveKeywordsTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\ArchivingSystemsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\KeywordsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\ProfilesTable;
use Asalae\Model\Table\PublicDescriptionArchiveFilesTable;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Table\ValidationActorsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationStagesTable;
use Asalae\Model\Volume\VolumeManager;
use Asalae\Utility\JstreeSeda;
use Asalae\View\Helper\TranslateHelper;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Http\CallbackStream;
use AsalaeCore\Http\Response as CoreResponse;
use AsalaeCore\I18n\Date as CoreDate;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\LimitBreak;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Translit;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\HttpException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response as CakeResponse;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\I18n\Date as CakeDate;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\ORM\ResultSet;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\Utility\Text;
use Cake\View\View;
use DateTime;
use DOMAttr;
use DOMDocument;
use DOMElement;
use DOMException;
use DOMXPath;
use Exception;
use Psr\Http\Message\ResponseInterface;
use ZMQSocketException;

/**
 * ArchiveUnits
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AccessRuleCodesTable               AccessRuleCodes
 * @property AccessRulesTable                   AccessRules
 * @property AgreementsTable                    Agreements
 * @property AppraisalRuleCodesTable            AppraisalRuleCodes
 * @property AppraisalRulesTable                AppraisalRules
 * @property ArchiveBinariesTable               ArchiveBinaries
 * @property ArchiveDescriptionsTable           ArchiveDescriptions
 * @property ArchiveKeywordsTable               ArchiveKeywords
 * @property ArchiveUnitsTable                  ArchiveUnits
 * @property ArchivesTable                      Archives
 * @property ArchivingSystemsTable              ArchivingSystems
 * @property EventLogsTable                     EventLogs
 * @property KeywordsTable                      Keywords
 * @property OrgEntitiesTable                   OrgEntities
 * @property ProfilesTable                      Profiles
 * @property PublicDescriptionArchiveFilesTable PublicDescriptionArchiveFiles
 * @property ValidationActorsTable              ValidationActors
 * @property ValidationChainsTable              ValidationChains
 * @property ValidationStagesTable              ValidationStages
 */
class ArchiveUnitsController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'Archives.id' => 'desc',
            'ArchiveUnits.lft' => 'asc',
        ],
        'sortableFields' => [
            'archival_agency_identifier' => 'Archives.archival_agency_identifier',
            'created' => 'Archives.created',
            'dua_end' => 'AppraisalRules.end_date',
        ],
    ];

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'binaries' => [
                'get' => [
                    'function' => 'apiBinaries',
                ],
            ],
            'binary' => [
                'get' => [
                    'function' => 'apiBinary',
                ],
            ],
        ];
    }

    /**
     * Archives consultables
     */
    public function index()
    {
        $request = $this->getRequest();
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        $IndexComponent->setConfig('csv_filename', __("ArchivesConsultables"));

        $filenameIds = [];
        if ($filenames = $this->getRequest()->getQuery('filename')) {
            /** @var ArchiveBinariesTable $ArchiveBinaries */
            $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
            $filenameIds = $ArchiveBinaries->selectQuery()
                ->select(['id' => 'au.archive_id'])
                ->from(['ab' => 'archive_binaries'])
                ->innerJoin(
                    ['lnk' => 'archive_binaries_archive_units'],
                    ['lnk.archive_binary_id' => new IdentifierExpression('ab.id')]
                )
                ->innerJoin(
                    ['au' => 'archive_units'],
                    ['lnk.archive_unit_id' => new IdentifierExpression('au.id')]
                )
                ->limit(50000)
                ->disableHydration();
            foreach ((array)$filenames as $filename) {
                $filenameIds->andWhere(
                    $IndexComponent->Condition->ilike('ab.filename', $filename)
                );
            }
            $filenameIds = $filenameIds
                ->all()
                ->map(
                    function ($e) {
                        return $e['id'];
                    }
                )
                ->toArray();
        }
        $mimeIds = [];
        if ($mimes = $this->getRequest()->getQuery('mime')) {
            /** @var ArchiveBinariesTable $ArchiveBinaries */
            $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
            $mimeIds = $ArchiveBinaries->selectQuery()
                ->select(['id' => 'au.archive_id'])
                ->from(['ab' => 'archive_binaries'])
                ->innerJoin(
                    ['lnk' => 'archive_binaries_archive_units'],
                    ['lnk.archive_binary_id' => new IdentifierExpression('ab.id')]
                )
                ->innerJoin(
                    ['au' => 'archive_units'],
                    ['lnk.archive_unit_id' => new IdentifierExpression('au.id')]
                )
                ->limit(50000)
                ->disableHydration();
            foreach ((array)$mimes as $mime) {
                $mimeIds->andWhere(
                    $IndexComponent->Condition->ilike('ab.mime', $mime)
                );
            }
            $mimeIds = $mimeIds
                ->all()
                ->map(
                    function ($e) {
                        return $e['id'];
                    }
                )
                ->toArray();
        }
        $formatIds = [];
        if ($formats = $this->getRequest()->getQuery('format')) {
            /** @var ArchiveBinariesTable $ArchiveBinaries */
            $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
            $formatIds = $ArchiveBinaries->selectQuery()
                ->select(['id' => 'au.archive_id'])
                ->from(['ab' => 'archive_binaries'])
                ->innerJoin(
                    ['lnk' => 'archive_binaries_archive_units'],
                    ['lnk.archive_binary_id' => new IdentifierExpression('ab.id')]
                )
                ->innerJoin(
                    ['au' => 'archive_units'],
                    ['lnk.archive_unit_id' => new IdentifierExpression('au.id')]
                )
                ->limit(50000)
                ->disableHydration();
            foreach ((array)$formats as $format) {
                $formatIds->andWhere(
                    $IndexComponent->Condition->ilike('ab.format', $format)
                );
            }
            $formatIds = $formatIds
                ->all()
                ->map(
                    function ($e) {
                        return $e['id'];
                    }
                )
                ->toArray();
        }
        $ids = array_slice(
            array_unique(array_merge($filenameIds, $mimeIds, $formatIds)),
            0,
            50000
        );

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $ArchiveUnits->findByIndexSealed(
            $this->Seal->hierarchicalView(),
            !$request->getQuery('all') && !$request->getQuery('search')
        );
        $loaded = false;
        $ConditionComponent = $IndexComponent->Condition;
        $IndexComponent->setQuery($query)
            ->filter('agreement_id', null, 'Archives.agreement_id')
            ->filter('archival_agency_identifier', IndexComponent::FILTER_ILIKE)
            ->filter(
                'created',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.created'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('id', IndexComponent::FILTER_IN)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('original_total_count', IndexComponent::FILTER_COUNTOPERATOR)
            ->filter('original_total_size', IndexComponent::FILTER_SIZEOPERATOR)
            ->filter(
                'originating_agency_id',
                null,
                'Archives.originating_agency_id'
            )
            ->filter('profile_id', null, 'Archives.profile_id')
            ->filter(
                'transferring_agency_id',
                null,
                'Archives.transferring_agency_id'
            )
            ->filter(
                'history',
                IndexComponent::FILTER_ILIKE,
                'ArchiveDescriptions.history'
            )
            ->filter(
                'description',
                IndexComponent::FILTER_ILIKE,
                'ArchiveDescriptions.description'
            )
            ->filter(
                'keyword',
                function ($value, Query $q) use ($ConditionComponent) {
                    /** @var ArchiveUnitsTable $ArchiveUnits */
                    $ArchiveUnits = $this->fetchTable('ArchiveUnits');
                    $subquery = $ArchiveUnits->find()
                        ->select(['ArchiveUnits.archive_id'])
                        ->innerJoinWith('ArchiveKeywords')
                        ->where($ConditionComponent->ilike('ArchiveKeywords.content', $value));
                    return $q->where(['Archives.id IN' => $subquery]);
                }
            )
            ->filter(
                'filename',
                function ($value, Query $q) use ($ids, &$loaded, $ConditionComponent) {
                    $req = $q->where(
                        $loaded ? ['1 = 1'] : $ConditionComponent->in('Archives.id', $ids)
                    );
                    $loaded = true;
                    return $req;
                }
            )
            ->filter(
                'mime',
                function ($value, Query $q) use ($ids, &$loaded, $ConditionComponent) {
                    $req = $q->where(
                        $loaded ? ['1 = 1'] : $ConditionComponent->in('Archives.id', $ids)
                    );
                    $loaded = true;
                    return $req;
                }
            )
            ->filter(
                'format',
                function ($value, Query $q) use ($ids, &$loaded, $ConditionComponent) {
                    $req = $q->where(
                        $loaded ? ['1 = 1'] : $ConditionComponent->in('Archives.id', $ids)
                    );
                    $loaded = true;
                    return $req;
                }
            )
            ->filter(
                'transferring_agency_identifier',
                IndexComponent::FILTER_ILIKE,
                'Archives.transferring_agency_identifier'
            )
            ->filter(
                'originating_agency_identifier',
                IndexComponent::FILTER_ILIKE,
                'Archives.originating_agency_identifier'
            )
            ->filter(
                'oldest_date',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.oldest_date'
            )
            ->filter(
                'latest_date',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.latest_date'
            )
            ->filter(
                'keyword_reference',
                function ($value, Query $q) use ($ConditionComponent) {
                    /** @var KeywordsTable $Keywords */
                    $Keywords = $this->fetchTable('Keywords');
                    $subquery = $Keywords->selectQuery()
                        ->select(['existing' => 'true'])
                        ->from(['k' => 'archive_keywords'])
                        ->innerJoin(
                            ['au' => 'archive_units'],
                            ['au.id' => new IdentifierExpression('k.archive_unit_id')]
                        )
                        ->where(
                            $ConditionComponent->ilike('k.reference', $value)
                        )
                        ->andWhere(
                            [
                                'au.lft >=' => new IdentifierExpression('ArchiveUnits.lft'),
                                'au.rght <=' => new IdentifierExpression('ArchiveUnits.rght'),
                            ]
                        )
                        ->limit(1);
                    return $q->where([$subquery]);
                }
            );
        if ($request->getQuery('search')) {
            $this->loadComponent('SearchEngine', ['field' => 'full_search']);
            $this->SearchEngine->query($query);
            $query->orderBy([], true);
        }

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $entity) {
                    $entity->setVirtual(
                        [
                            'description',
                            'dates',
                            'is_desc_communicable',
                            'editable',
                        ]
                    );
                    return $entity;
                }
            )
            ->toArray();
        $this->set('data', $data);

        // Options
        $this->setIndexOptions();
    }

    /**
     * Défini les options pour l'index / catalog
     */
    private function setIndexOptions()
    {
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $ta = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES]
            )
            ->orderBy(['OrgEntities.name' => 'asc']);
        $this->set('transferring_agencies', $ta->all());

        $oa = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES]
            )
            ->orderBy(['OrgEntities.name' => 'asc']);
        $this->set('originating_agencies', $oa->all());

        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('sealList')
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles);

        $agreements = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('sealList')
            ->where(['Agreements.active' => true])
            ->orderBy(['Agreements.name' => 'asc']);
        $this->set('agreements', $agreements);

        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $validation_delivery_request = $ValidationChains->findListOptions(
            $this->archivalAgencyId,
            $ValidationChains::ARCHIVE_DELIVERY_REQUEST
        );
        $this->set('validation_delivery_request', $validation_delivery_request);
    }

    /**
     * Index du catalogue
     */
    public function catalog()
    {
        $query = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal');

        $viewCookies = $this->viewBuilder()->getVar('cookies');
        $cookies = json_decode($viewCookies, true) ?: null;
        if (!empty($cookies['catalog_basket'])) {
            /** @var ArchiveUnitsTable $ArchiveUnits */
            $ArchiveUnits = $this->fetchTable('ArchiveUnits');
            $units = $ArchiveUnits->find(
                'list',
                valueField: function (EntityInterface $e) {
                    return $e->get('archival_agency_identifier') . ' - ' . $e->get('name');
                }
            )
                ->innerJoinWith('Archives')
                ->where(
                    [
                        'Archives.archival_agency_id' => $this->archivalAgencyId,
                        'ArchiveUnits.id IN' => explode(
                            ',',
                            $cookies['catalog_basket']
                        ),
                    ]
                );
            $this->set('selected_archive_units', $units->toArray());
        }

        return $this->catalogCommons($query);
    }

    /**
     * Methode générique pour catalog et catalogMyEntity
     * @param Query $query
     * @return CakeResponse|null|void
     * @throws Exception
     */
    private function catalogCommons(Query $query)
    {
        $request = $this->getRequest();
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        $ids = [];
        $ConditionComponent = $IndexComponent->Condition;
        if ($filenames = $this->getRequest()->getQuery('filename')) {
            /** @var ArchiveBinariesTable $ArchiveBinaries */
            $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
            $ids = $ArchiveBinaries->selectQuery()
                ->select(['id' => 'au.archive_id'])
                ->from(['ab' => 'archive_binaries'])
                ->innerJoin(
                    ['lnk' => 'archive_binaries_archive_units'],
                    ['lnk.archive_binary_id' => new IdentifierExpression('ab.id')]
                )
                ->innerJoin(
                    ['au' => 'archive_units'],
                    ['lnk.archive_unit_id' => new IdentifierExpression('au.id')]
                )
                ->limit(50000)
                ->disableHydration();
            foreach ((array)$filenames as $filename) {
                $ids->andWhere(
                    $ConditionComponent->ilike('ab.filename', $filename)
                );
            }
            $ids = $ids
                ->all()
                ->map(
                    function ($e) {
                        return $e['id'];
                    }
                )
                ->toArray();
        }
        $query = $query
            ->where(
                [
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                    'Archives.state IN' => [ArchivesTable::S_AVAILABLE, ArchivesTable::S_FREEZED],
                    'ArchiveUnits.state IN' => [ArchiveUnitsTable::S_AVAILABLE, ArchiveUnitsTable::S_FREEZED],
                ]
            )
            ->contain(
                [
                    'AccessRules' => ['AccessRuleCodes'],
                    'Archives' => [
                        'Agreements',
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'OriginatingAgencies',
                        'Profiles',
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                        'TransferringAgencies',
                        'Transfers',
                    ],
                ]
            );
        if (!$request->getQuery('all') && !$request->getQuery('search')) {
            $query->where(['ArchiveUnits.parent_id IS' => null]);
        }

        $IndexComponent->setQuery($query)
            ->filter('agreement_id', null, 'Archives.agreement_id')
            ->filter('archival_agency_identifier', IndexComponent::FILTER_ILIKE)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('id', IndexComponent::FILTER_IN)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter(
                'originating_agency_id',
                null,
                'Archives.originating_agency_id'
            )
            ->filter('profile_id', null, 'Archives.profile_id')
            ->filter(
                'transferring_agency_id',
                null,
                'Archives.transferring_agency_id'
            )
            ->filter(
                'created',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.created'
            )
            ->filter('original_total_size', IndexComponent::FILTER_SIZEOPERATOR)
            ->filter(
                'original_total_count',
                IndexComponent::FILTER_COUNTOPERATOR
            )
            ->filter(
                'keyword',
                function ($value, Query $q) use ($ConditionComponent) {
                    /** @var KeywordsTable $Keywords */
                    $Keywords = $this->fetchTable('Keywords');
                    $subquery = $Keywords->selectQuery()
                        ->select(['existing' => 'true'])
                        ->from(['k' => 'archive_keywords'])
                        ->innerJoin(
                            ['au' => 'archive_units'],
                            ['au.id' => new IdentifierExpression('k.archive_unit_id')]
                        )
                        ->where(
                            $ConditionComponent->ilike('k.content', $value)
                        )
                        ->andWhere(
                            [
                                'au.lft >=' => new IdentifierExpression('ArchiveUnits.lft'),
                                'au.rght <=' => new IdentifierExpression('ArchiveUnits.rght'),
                            ]
                        )
                        ->limit(1);
                    return $q->where([$subquery]);
                }
            )
            ->filter(
                'filename',
                function ($value, Query $q) use ($ids, $ConditionComponent) {
                    return $q->where(
                        $ConditionComponent->in('Archives.id', $ids)
                    );
                }
            )
            ->filter(
                'transferring_agency_identifier',
                IndexComponent::FILTER_ILIKE,
                'ArchiveUnits.transferring_agency_identifier'
            )
            ->filter(
                'originating_agency_identifier',
                IndexComponent::FILTER_ILIKE,
                'ArchiveUnits.originating_agency_identifier'
            )
            ->filter(
                'oldest_date',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.oldest_date'
            )
            ->filter(
                'latest_date',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.latest_date'
            )
            ->filter(
                'keyword_reference',
                function ($value, Query $q) use ($ConditionComponent) {
                    /** @var KeywordsTable $Keywords */
                    $Keywords = $this->fetchTable('Keywords');
                    $subquery = $Keywords->selectQuery()
                        ->select(['existing' => 'true'])
                        ->from(['k' => 'archive_keywords'])
                        ->innerJoin(
                            ['au' => 'archive_units'],
                            ['au.id' => new IdentifierExpression('k.archive_unit_id')]
                        )
                        ->where($ConditionComponent->ilike('k.reference', $value))
                        ->andWhere(
                            [
                                'au.lft >=' => new IdentifierExpression('ArchiveUnits.lft'),
                                'au.rght <=' => new IdentifierExpression('ArchiveUnits.rght'),
                            ]
                        )
                        ->limit(1);
                    return $q->where([$subquery]);
                }
            )
            ->filter(
                'max_aggregated_access_end_date',
                IndexComponent::FILTER_DATEOPERATOR,
                'ArchiveUnits.max_aggregated_access_end_date'
            );

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        if ($request->getQuery('search')) {
            /** @use SearchEngineComponent */
            $this->loadComponent('SearchEngine');
            $this->SearchEngine->query($query);
            $query->orderBy([], true);
        }
        $this->set('resultCount', $query->count());
        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $entity) {
                    $entity->setVirtual(
                        ['description', 'dates', 'is_desc_communicable']
                    );
                    return $entity;
                }
            )
            ->toArray();
        $this->set('data', $data);

        // Options
        $this->setIndexOptions();

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex(
            [
                'typeName' => "CatalogArchiveUnits",
                'indexName' => "Catalogue",
            ]
        );
    }

    /**
     * Index du catalogue de mon service
     */
    public function catalogMyEntity()
    {
        $query = $this->Seal->hierarchicalView()
            ->table('ArchiveUnits')
            ->find('seal');

        return $this->catalogCommons($query);
    }

    /**
     * Action du moteur de recherche
     * @throws Exception
     */
    public function searchBar()
    {
        $request = $this->getRequest();
        $request->allowMethod('post');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->Seal->archivalAgency()->table('ArchiveUnits');
        $query = $ArchiveUnits
            ->findByCatalog('seal')
            ->where(
                [
                    'Archives.state IN' => [
                        ArchivesTable::S_AVAILABLE,
                        ArchivesTable::S_FREEZED,
                        ArchivesTable::S_FREEZED_TRANSFERRING,
                        ArchivesTable::S_FREEZED_DESTROYING,
                        ArchivesTable::S_FREEZED_RESTITUTING,
                    ],
                    'ArchiveUnits.state IN' => [
                        ArchiveUnitsTable::S_AVAILABLE,
                        ArchiveUnitsTable::S_FREEZED,
                        ArchiveUnitsTable::S_FREEZED_TRANSFERRING,
                        ArchiveUnitsTable::S_FREEZED_DESTROYING,
                        ArchiveUnitsTable::S_FREEZED_RESTITUTING,
                    ],
                ]
            )
            ->contain(
                [
                    'Archives' => [
                        'Agreements',
                        'Profiles',
                        'ArchivalAgencies',
                        'TransferringAgencies',
                        'OriginatingAgencies',
                    ],
                    'ArchiveDescriptions' => [
                        'AccessRules',
                    ],
                ],
                true
            );
        /** @use SearchEngineComponent */
        $this->loadComponent('SearchEngine');
        $this->SearchEngine->query($query)
            ->orderBy(
                [
                    'ArchiveUnits.parent_id IS NULL' => 'desc',
                    'ArchiveUnits.archive_id' => 'desc',
                    'ArchiveUnits.lft' => 'asc',
                ],
                true
            );
        $checkFields = [
            'archival_agency_identifier',
            'name',
            'archive_description.description',
            'archive_description.history',
            'archive_keywords.{n}.content',
        ];
        /** @var ArchiveKeywordsTable $ArchiveKeywords */
        $ArchiveKeywords = $this->fetchTable('ArchiveKeywords');
        // le hasMany doit être traité dans la boucle pour éviter un plantage mémoire
        $fn = function (EntityInterface $entity) use ($ArchiveKeywords) {
            $entity->set(
                'archive_keywords',
                $ArchiveKeywords->find()->where(
                    ['archive_unit_id' => $entity->id]
                )->toArray()
            );
        };
        $results = $this->SearchEngine->results($checkFields, $fn);
        return $this->renderDataToJson(array_values($results));
    }

    /**
     * Action du moteur de recherche
     * @throws Exception
     */
    public function fullSearchBar()
    {
        /** @use IndexComponent */
        $this->loadComponent('AsalaeCore.Index');

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $ArchiveUnits->findByIndexSealed(
            $this->Seal->hierarchicalView(),
            false
        )
            ->contain(
                [
                    'Archives' => [
                        'Agreements',
                        'Profiles',
                        'ArchivalAgencies',
                        'TransferringAgencies',
                        'OriginatingAgencies',
                    ],
                    'ArchiveDescriptions' => [
                        'AccessRules',
                    ],
                ],
                true
            );
        $this->loadComponent('SearchEngine', ['field' => 'full_search']);
        $this->SearchEngine->query($query)
            ->orderBy(
                [
                    'ArchiveUnits.parent_id IS NULL' => 'desc',
                    'ArchiveUnits.archive_id' => 'desc',
                    'ArchiveUnits.lft' => 'asc',
                ],
                true
            );
        $checkFields = [
            'archival_agency_identifier',
            'name',
            'archive_description.description',
            'archive_description.history',
            'archive_keywords.{n}.content',
        ];
        /** @var ArchiveKeywordsTable $ArchiveKeywords */
        $ArchiveKeywords = $this->fetchTable('ArchiveKeywords');
        // le hasMany doit être traité dans la boucle pour éviter un plantage mémoire
        $fn = function (EntityInterface $entity) use ($ArchiveKeywords) {
            $entity->set(
                'archive_keywords',
                $ArchiveKeywords->find()->where(
                    ['archive_unit_id' => $entity->id]
                )->toArray()
            );
        };
        $results = $this->SearchEngine->results($checkFields, $fn);
        return $this->renderDataToJson(array_values($results));
    }

    /**
     * Explorateur du fichier de description publique d'une unité d'archive
     * @param string $id
     * @throws Exception
     */
    public function descriptionPublic(string $id)
    {
        $entity = $this->getPublicDescription($id);
        $this->set('id', $id);
        $this->set('entity', $entity);
    }

    /**
     * Récupère/défini la description publique
     * @param int|string $id
     * @return EntityInterface archive_unit
     * @throws VolumeException
     */
    private function getPublicDescription($id): EntityInterface
    {
        /** @var PublicDescriptionArchiveFilesTable $PublicDescriptionArchiveFiles */
        $PublicDescriptionArchiveFiles = $this->fetchTable('PublicDescriptionArchiveFiles');
        $entity = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal')
            ->where(
                [
                    'ArchiveUnits.id' => $id,
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                    ],
                    'ArchiveDescriptions' => ['AccessRules'],
                ]
            )
            ->firstOrFail();
        $nextPubdate = Hash::get($entity, 'archive.next_pubdesc');
        if ($nextPubdate === null) {
            $newPubdate = $PublicDescriptionArchiveFiles->nextRebuildDate(
                Hash::get($entity, 'archive.id')
            );
            /** @var EntityInterface $archive */
            $archive = Hash::get($entity, 'archive');
            $archive->set('next_pubdesc', $newPubdate);
        }
        /** @var PublicDescriptionArchiveFile $archiveFile */
        $archiveFile = Hash::get(
            $entity,
            'archive.public_description_archive_file'
        );
        $needRenew = $nextPubdate && $nextPubdate < new DateTime();
        if ($this->getRequest()->getQuery('renew')) {
            $needRenew = true;
        }
        if ($archiveFile && $needRenew) {
            $PublicDescriptionArchiveFiles->deleteOrFail($archiveFile);
            $manager = VolumeManager::createWithStoredFileId(
                $archiveFile->get('stored_file_id')
            );
            $manager->fileDelete(Hash::get($archiveFile, 'stored_file.name'));
            $PublicDescriptionArchiveFiles->deleteAll(
                ['id' => $archiveFile->id]
            );
            $archiveFile = null;
            $newPubdate = $PublicDescriptionArchiveFiles->nextRebuildDate(
                Hash::get($entity, 'archive.id')
            );
            /** @var EntityInterface $archive */
            $archive = Hash::get($entity, 'archive');
            $archive->set('next_pubdesc', $newPubdate);
        }
        /** @var DescriptionXmlArchiveFile $archiveDescription */
        $archiveDescription = Hash::get(
            $entity,
            'archive.description_xml_archive_file'
        );
        if (!$archiveFile) {
            $archiveFile = $PublicDescriptionArchiveFiles->createFromDescriptionXml(
                $archiveDescription
            );
            if (!isset($manager)) {
                $manager = VolumeManager::createWithStoredFileId(
                    $archiveDescription->get('stored_file_id')
                );
            }
            $basePath = Hash::get(
                $entity,
                'archive.storage_path'
            ) . '/management_data/';
            $storedFile = $manager->filePutContent(
                $basePath . $archiveFile->get('filename'),
                $archiveFile->get('content')
            );
            $archiveFile->set('stored_file_id', $storedFile->id);
            $archiveFile->unset('content');
            $PublicDescriptionArchiveFiles->saveOrFail($archiveFile);
        }
        if (isset($archive)) {
            /** @var ArchivesTable $Archives */
            $Archives = $this->fetchTable('Archives');
            $Archives->saveOrFail($archive);
        }
        return $entity;
    }

    /**
     * Description d'une unité d'archive
     * @param string $id
     * @throws Exception
     */
    public function description(string $id)
    {
        /** @var ArchiveUnit $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal')
            ->where(['ArchiveUnits.id' => $id])
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                    ],
                ]
            )
            ->firstOrFail();
        $this->set('id', $id);
        $this->set('entity', $entity);
    }

    /**
     * Affiche une unité d'archives en html
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function displayXmlPublic(string $id): CakeResponse
    {
        $entity = $this->getPublicDescription($id);
        return $this->getResponse()->withStringBody($entity->get('PublicHTML'));
    }

    /**
     * Permet le commeDroit sur catalog-my-entity
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function displayXmlPublicMyEntity(string $id): CakeResponse
    {
        return $this->displayXmlPublic($id);
    }

    /**
     * Affiche le bordereau xml en html
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function displayXml(string $id): CakeResponse
    {
        // TODO remplacer par seal (as2.2)
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $query = $Archives->find()
            ->innerJoinWith('ArchivalAgencies')
            ->innerJoinWith('TransferringAgencies')
            ->innerJoinWith('OriginatingAgencies')
            ->innerJoinWith('ArchiveUnits')
            ->where(['ArchiveUnits.id' => $id]);
        $sa = $this->archivalAgency;
        $lft = Hash::get($this->orgEntity, 'lft');
        $rght = Hash::get($this->orgEntity, 'rght');
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (in_array($typeEntity, ['SA', 'CST'])) {
            $lft = $sa->get('lft');
            $rght = $sa->get('rght');
        }
        if (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->andWhere(
                [
                    'OR' => [
                        [
                            'ArchivalAgencies.lft >=' => $lft,
                            'ArchivalAgencies.rght <=' => $rght,
                        ],
                        [
                            'TransferringAgencies.lft >=' => $lft,
                            'TransferringAgencies.rght <=' => $rght,
                        ],
                        [
                            'OriginatingAgencies.lft >=' => $lft,
                            'OriginatingAgencies.rght <=' => $rght,
                        ],
                    ],
                ]
            );
        } else {
            $query->andWhere(
                [
                    $this->orgEntityId . ' IN' => [
                        new IdentifierExpression('ArchivalAgencies.id'),
                        new IdentifierExpression('TransferringAgencies.id'),
                        new IdentifierExpression('OriginatingAgencies.id'),
                    ],
                ]
            );
        }
        $query->firstOrFail();
        // FIN TODO

        $entity = $this->getDescription($id);
        return $this->getResponse()->withStringBody($entity->get('HTML'));
    }

    /**
     * @param int|string $id
     * @return EntityInterface
     * @throws Exception
     */
    private function getDescription($id): EntityInterface
    {
        return $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal')
            ->where(['ArchiveUnits.id' => $id])
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                    ],
                ]
            )
            ->firstOrFail();
    }

    /**
     * Donne le data jstree pour la description d'archive publique
     * @param string $id
     * @return CakeResponse
     * @throws VolumeException
     */
    public function getPublicTree(string $id): CakeResponse
    {
        /** @var ArchiveUnit $entity */
        $entity = $this->getPublicDescription($id);
        return $this->getTreeCommons($entity->getPublicDom(), $entity);
    }

    /**
     * Donne le data jstree pour la description d'archive publique
     * @param DOMDocument $dom
     * @param ArchiveUnit $entity
     * @return CakeResponse
     * @throws Exception
     */
    private function getTreeCommons(DOMDocument $dom, ArchiveUnit $entity)
    {
        $this->set('id', $entity->id);
        $this->set('entity', $entity);

        $schemaPath = JstreeSeda::xpathToSchemaPath($entity->get('xpath'));
        $jstreeSeda = new JstreeSeda($dom, $schemaPath);

        $idNode = $this->getRequest()->getQuery('id');
        if ($idNode !== '#' && $idNode) {
            return $this->renderDataToJson($jstreeSeda->getByIdNode($idNode));
        } else {
            $path = array_filter(explode('/ns:', $entity->get('xpath')));
            return $this->renderDataToJson($jstreeSeda->getJsTree(...$path));
        }
    }

    /**
     * Donne le data jstree pour la description d'archive publique
     * @param string $id
     * @return CakeResponse
     * @throws VolumeException
     */
    public function getTree(string $id): CakeResponse
    {
        /** @var ArchiveUnit $entity */
        $entity = $this->getDescription($id);
        return $this->getTreeCommons($entity->getDom(), $entity);
    }

    /**
     * Affichage d'un noeud
     * @param string $id
     * @param mixed  ...$path
     * @throws Exception
     */
    public function viewPublicDescriptionNode(string $id, ...$path)
    {
        /** @var ArchiveUnit $entity */
        $entity = $this->getPublicDescription($id);
        $this->descriptionNode($entity->getPublicDom(), $path);
    }

    /**
     * Affichage d'un noeud
     * @param DOMDocument $dom
     * @param mixed       $path
     */
    private function descriptionNode(DOMDocument $dom, $path)
    {
        $xmlns = $dom->documentElement->getAttributeNode('xmlns');
        $namespace = $xmlns?->nodeValue;
        $xpath = new DOMXpath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $xpathExp = '/ns:' . implode('/ns:', $path);
        $node = $xpath->query($xpathExp)->item(0);
        $nodes = [];
        $attrs = [];
        /** @var DOMAttr $attr */
        foreach ($node->attributes as $attr) {
            $attrs[$attr->nodeName] = $attr->nodeValue;
        }
        if ($attrs) {
            $nodes['@'][] = ['attrs' => $attrs, 'value' => null];
        }
        foreach ($node->childNodes as $child) {
            if ($child instanceof DOMElement) {
                foreach ($child->childNodes as $subChild) {
                    if ($subChild instanceof DOMElement) {
                        continue 2;
                    }
                }
                if (!isset($nodes[$child->nodeName])) {
                    $nodes[$child->nodeName] = [];
                }
                $attrs = [];
                foreach ($child->attributes as $attr) {
                    $attrs[$attr->nodeName] = $attr->nodeValue;
                }
                $nodes[$child->nodeName][] = [
                    'attrs' => $attrs,
                    'value' => $child->nodeValue,
                ];
            }
        }
        $this->set('path', $path);
        $this->set('nodes', $nodes);
    }

    /**
     * Affichage d'un noeud
     * @param string $id
     * @param mixed  ...$path
     * @throws VolumeException
     */
    public function viewDescriptionNode(string $id, ...$path)
    {
        /** @var ArchiveUnit $entity */
        $entity = $this->getDescription($id);
        $this->descriptionNode($entity->getDom(), $path);
    }

    /**
     * Affiche l'arborescence liée à une archive-unit
     * @param string $id
     * @throws Exception
     */
    public function getNavTree(string $id)
    {
        $archiveUnit = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal')
            ->innerJoinWith('Archives')
            ->where(['ArchiveUnits.id' => $id])
            ->firstOrFail();

        // TODO remplacer par seal (as2.2)
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $archive_id = $archiveUnit->get('archive_id');
        $query = $Archives->find()
            ->innerJoinWith('ArchivalAgencies')
            ->innerJoinWith('TransferringAgencies')
            ->innerJoinWith('OriginatingAgencies')
            ->where(['Archives.id' => $archive_id]);
        $sa = $this->archivalAgency;
        $lft = Hash::get($this->orgEntity, 'lft');
        $rght = Hash::get($this->orgEntity, 'rght');
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (in_array($typeEntity, ['SA', 'CST'])) {
            $lft = $sa->get('lft');
            $rght = $sa->get('rght');
        }
        if (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->andWhere(
                [
                    'OR' => [
                        [
                            'ArchivalAgencies.lft >=' => $lft,
                            'ArchivalAgencies.rght <=' => $rght,
                        ],
                        [
                            'TransferringAgencies.lft >=' => $lft,
                            'TransferringAgencies.rght <=' => $rght,
                        ],
                        [
                            'OriginatingAgencies.lft >=' => $lft,
                            'OriginatingAgencies.rght <=' => $rght,
                        ],
                    ],
                ]
            );
        } else {
            $query->andWhere(
                [
                    $this->orgEntityId . ' IN' => [
                        new IdentifierExpression('ArchivalAgencies.id'),
                        new IdentifierExpression('TransferringAgencies.id'),
                        new IdentifierExpression('OriginatingAgencies.id'),
                    ],
                ]
            );
        }
        $query->firstOrFail();
        // FIN TODO

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $parents = Hash::extract(
            $ArchiveUnits->find()
                ->select(['id'])
                ->where(
                    [
                        'lft <=' => $archiveUnit->get('lft'),
                        'rght >=' => $archiveUnit->get('rght'),
                    ]
                )
                ->orderBy(['lft'])
                ->disableHydration()
                ->toArray(),
            '{n}.id'
        );
        $count = $ArchiveUnits->find()
            ->where(['archive_id' => $archiveUnit->get('archive_id')])
            ->count();
        LimitBreak::setMemoryLimit($count * 3000);
        LimitBreak::setTimeLimit($count);
        $archiveUnits = $ArchiveUnits->find('threaded')
            ->select(
                [
                    'id',
                    'archival_agency_identifier',
                    'name',
                    'parent_id',
                ]
            )
            ->where(['archive_id' => $archiveUnit->get('archive_id')])
            ->orderBy(
                [
                    'ArchiveUnits.id IN (' . implode(
                        ', ',
                        $parents
                    ) . ')' => 'desc',
                    'ArchiveUnits.lft > ' . $archiveUnit->get('lft') => 'desc',
                    'ArchiveUnits.lft',
                ]
            )
            ->disableHydration()
            ->toArray();
        $tree = $this->threadedToJsTree(
            $archiveUnits,
            [
                'id' => 'archival_agency_identifier',
                'text' => function ($values) {
                    if (mb_strlen($values['name']) > 30) {
                        $trimmed = mb_substr($values['name'], 0, 29) . '~';
                        $escaped = h(str_replace('"', '', $values['name']));
                        $append = '<span class="child-value trimmed" title="' . $escaped . '">' . h(
                            $trimmed
                        ) . '</span>';
                    } else {
                        $append = '<span class="child-value">' . h(
                            $values['name']
                        ) . '</span>';
                    }
                    return h($values['archival_agency_identifier']) . $append;
                },
                'url' => 'id',
                'identifier' => 'archival_agency_identifier',
                'search' => function ($values) {
                    return [
                        'name' => Translit::asciiToLower($values['name']),
                        'identifier' => Translit::asciiToLower(
                            $values['archival_agency_identifier']
                        ),
                    ];
                },
                'name' => 'name',
                'icon' => function () {
                    return 'fa fa-folder';
                },
                'state' => function ($values) use ($parents, $archiveUnit) {
                    return [
                        'opened' => in_array($values['id'], $parents),
                        'selected' => $values['id'] === $archiveUnit->id,
                    ];
                },
            ]
        );
        $this->set('tree', $tree);
        $this->set('id', $id);
    }

    /**
     * Transforme un find('threaded') en data pour jstree
     * @param array $nodes
     * @param array $attrs
     * @return array
     */
    private function threadedToJsTree(array $nodes, array $attrs): array
    {
        $output = [];
        foreach ($nodes as $key => $values) {
            $o = array_map(fn ($v) => is_callable($v) ? $v($values) : $values[$v], $attrs);
            $o['id'] = ($key + 1) . '_' . Text::slug($o['id']);
            $output[] = $o + [
                'children' => isset($values['children'])
                        ? $this->threadedToJsTree($values['children'], $attrs)
                        : null,
            ];
        }
        return $output;
    }

    /**
     * Visualisation d'une archive_unit
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $archiveUnit = $ArchiveUnits->findByIndexSealed(
            $this->Seal->hierarchicalView(),
            false
        )
            ->where(['ArchiveUnits.id' => $id])
            ->contain(
                [
                    'Archives' => [
                        'ArchivalAgencies',
                        'OriginatingAgencies',
                        'TransferringAgencies',
                        'Profiles',
                        'Agreements',
                    ],
                    'ArchiveDescriptions',
                    'AccessRules' => ['AccessRuleCodes'],
                    'AppraisalRules' => ['AppraisalRuleCodes'],
                ],
                true
            )
            ->firstOrFail();
        $this->set('archiveUnit', $archiveUnit);
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');

        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $subquery = $ArchiveUnits->find()
            ->select(['ArchiveBinariesArchiveUnits.archive_binary_id'])
            ->innerJoinWith('ArchiveBinariesArchiveUnits')
            ->where(
                [
                    'ArchiveUnits.lft >=' => $archiveUnit->get('lft'),
                    'ArchiveUnits.rght <=' => $archiveUnit->get('rght'),
                ]
            );
        $query = $ArchiveBinaries->find()
            ->where(
                [
                    'ArchiveBinaries.type' => 'original_data',
                    'ArchiveBinaries.id IN' => $subquery,
                ]
            )
            ->orderBy(['ArchiveBinaries.filename'])
            ->contain(
                [
                    'ArchiveUnits',
                ]
            );
        $this->set('countBinaries', $countBinaries = $query->count());
        $binaries = $query
            ->limit($AjaxPaginatorComponent->getConfig('limit'))
            ->all()
            ->map(
                function (EntityInterface $v) use ($ArchiveUnits) {
                    foreach ($v->get('archive_units') as $archiveUnit) {
                        $path = Hash::extract(
                            $ArchiveUnits->find()
                                ->select(['name'])
                                ->where(
                                    [
                                        'lft <=' => $archiveUnit->get('lft'),
                                        'rght >=' => $archiveUnit->get('rght'),
                                    ]
                                )
                                ->orderBy(['lft'])
                                ->disableHydration()
                                ->toArray(),
                            '{n}.name'
                        );
                        $path = array_map('h', $path);
                        $archiveUnit->set(
                            'relative_path',
                            '/&nbsp;' . implode(' /&nbsp;', $path)
                        );
                    }
                    return $v;
                }
            )
            ->toArray();
        $this->set('binaries', $binaries);
        $this->set('id', $id);
        $AjaxPaginatorComponent->setViewPaginator($binaries, $countBinaries);
    }

    /**
     * Pagination json de l'action view (binaries)
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function viewBinaries(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $archiveUnit = $ArchiveUnits->findByIndexSealed(
            $this->Seal->hierarchicalView(),
            false
        )
            ->where(['ArchiveUnits.id' => $id])
            ->firstOrFail();

        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $subquery = $ArchiveUnits->find()
            ->select(['ArchiveBinariesArchiveUnits.archive_binary_id'])
            ->innerJoinWith('ArchiveBinariesArchiveUnits')
            ->where(
                [
                    'ArchiveUnits.lft >=' => $archiveUnit->get('lft'),
                    'ArchiveUnits.rght <=' => $archiveUnit->get('rght'),
                ]
            );
        $query = $ArchiveBinaries->find()
            ->where(
                [
                    'ArchiveBinaries.type' => 'original_data',
                    'ArchiveBinaries.id IN' => $subquery,
                ]
            )
            ->orderBy(['ArchiveBinaries.filename'])
            ->contain(
                [
                    'ArchiveUnits',
                ]
            );

        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index', ['model' => 'ArchiveBinaries']);
        $IndexComponent->setQuery($query)
            ->filter(
                'filename',
                IndexComponent::FILTER_ILIKE,
                'ArchiveBinaries.filename'
            )
            ->filter(
                'format',
                IndexComponent::FILTER_ILIKE,
                'ArchiveBinaries.format'
            )
            ->filter(
                'mime',
                IndexComponent::FILTER_ILIKE,
                'ArchiveBinaries.mime'
            )
            ->filter(
                'size',
                function ($s) {
                    if (!$s['value']) {
                        return;
                    }
                    $value = $s['value'] * $s['mult'];
                    $operator = $s['operator'];
                    if (!in_array($operator, ['=', '<=', '>=', '<', '>'])) {
                        throw new BadRequestException(
                            __("L'opérateur indiqué n'est pas autorisé")
                        );
                    }
                    return ['StoredFiles.size ' . $operator => $value];
                }
            );
        return $AjaxPaginatorComponent->json(
            $query,
            false,
            function (ResultSet $data) use ($ArchiveUnits) {
                return $data->map(
                    function (EntityInterface $v) use ($ArchiveUnits) {
                        foreach ($v->get('archive_units') as $archiveUnit) {
                            $path = Hash::extract(
                                $ArchiveUnits->find()
                                    ->select(['name'])
                                    ->where(
                                        [
                                            'lft <=' => $archiveUnit->get('lft'),
                                            'rght >=' => $archiveUnit->get('rght'),
                                        ]
                                    )
                                    ->orderBy(['lft'])
                                    ->disableHydration()
                                    ->toArray(),
                                '{n}.name'
                            );
                            $path = array_map('h', $path);
                            $archiveUnit->set(
                                'relative_path',
                                '/&nbsp;' . implode(' /&nbsp;', $path)
                            );
                        }
                        return $v;
                    }
                );
            }
        );
    }

    /**
     * Edition d'une unité d'archives
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $archiveUnit = $ArchiveUnits->findByIndexSealed(
            $this->Seal->hierarchicalView(),
            false
        )
            ->where(['ArchiveUnits.id' => $id])
            ->contain(
                [
                    'Archives' => [
                        'Agreements',
                        'Profiles',
                        'OriginatingAgencies',
                        'Transfers',
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        'AccessRules' => ['AccessRuleCodes'],
                        'AppraisalRules' => ['AppraisalRuleCodes'],
                    ],
                    'ArchiveDescriptions' => [
                        'AccessRules' => ['AccessRuleCodes'],
                    ],
                    'ArchiveKeywords',
                    'AccessRules' => ['AccessRuleCodes'],
                    'AppraisalRules' => ['AppraisalRuleCodes'],
                    'ParentArchiveUnits' => [
                        'AccessRules' => ['AccessRuleCodes'],
                        'AppraisalRules' => ['AppraisalRuleCodes'],
                    ],
                ]
            )
            ->firstOrFail();
        if (!$archiveUnit->get('editable')) {
            throw new ForbiddenException();
        }
        /** @var ArchiveDescriptionsTable $ArchiveDescriptions */
        $ArchiveDescriptions = $this->fetchTable('ArchiveDescriptions');
        $parentDescription = $ArchiveDescriptions->find()
            ->innerJoinWith('ArchiveUnits')
            ->where(
                [
                    'ArchiveUnits.archive_id' => $archiveUnit->get(
                        'archive_id'
                    ),
                    'ArchiveUnits.lft <' => $archiveUnit->get('lft'),
                    'ArchiveUnits.rght >' => $archiveUnit->get('rght'),
                ]
            )
            ->orderBy(['ArchiveUnits.lft' => 'desc'])
            ->contain(['AccessRules' => ['AccessRuleCodes']])
            ->first();
        $archiveUnit->set('parent_description', $parentDescription);
        $this->set('entity', $archiveUnit);

        $sa = $this->archivalAgency;
        $lft = Hash::get($this->orgEntity, 'lft');
        $rght = Hash::get($this->orgEntity, 'rght');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');

        $oa = $OrgEntities->listOptions([], $this->Seal->archivalAgencyChilds())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES]
            )
            ->orderBy(['OrgEntities.name' => 'asc']);

        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (in_array($typeEntity, ['SA', 'CST'])) {
            $oa->andWhere(
                [
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                ]
            );
        } elseif (Hash::get($this->user, 'role.hierarchical_view')) {
            $oa->andWhere(
                [
                    'OrgEntities.lft >=' => $lft,
                    'OrgEntities.rght <=' => $rght,
                ]
            );
        } else {
            $oa->andWhere(['OrgEntities.id' => $this->orgEntityId]);
        }
        $oa = $oa->all();

        $agreements = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find(
                'sealList',
                valueField: fn(EntityInterface $e) => $e->get('name') . ($e->get('active')
                    ? ''
                    : (' (' . __("désactivé") . ')')),
            )
            ->orderBy(['Agreements.name' => 'asc']);

        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find(
                'sealList',
                valueField: fn(EntityInterface $e) => $e->get('name') . ($e->get('active')
                    ? ''
                    : (' (' . __("désactivé") . ')')),
            )
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles);

        /** @var AccessRuleCodesTable $AccessRuleCodes */
        $AccessRuleCodes = $this->fetchTable('AccessRuleCodes');
        $codes = $AccessRuleCodes->getRuleCodes(
            $this->archivalAgencyId,
            Hash::get($archiveUnit, 'archive.transfers.0.message_version'),
            'id'
        );

        $finals = Seda10Schema::getXsdOptions('appraisal_code');
        foreach ($finals as $key => $args) {
            $finals[$key]['value'] = $args['value'] === 'conserver' ? 'keep'
                : 'destroy';
        }

        /** @var AppraisalRuleCodesTable $AppraisalRuleCodes */
        $AppraisalRuleCodes = $this->fetchTable('AppraisalRuleCodes');
        $durations = $AppraisalRuleCodes->getRuleCodes(
            $this->archivalAgencyId,
            Hash::get($archiveUnit, 'archive.transfers.0.message_version'),
            'id'
        );

        $request = $this->getRequest();
        if ($request->is('put')) {
            $sedaVersion = Hash::get($archiveUnit, 'archive.transfers.0.message_version');
            if ($sedaVersion === 'seda1.0' || $sedaVersion === 'seda0.2') {
                $cond = ['org_entity_id IS' => null];
            } else {
                $cond = [
                    'OR' => [
                        'org_entity_id IS' => null,
                        'org_entity_id' => $sa->get('id'),
                    ],
                ];
            }
            $accessRuleCodeIds = $AccessRuleCodes
                ->find('list', keyField: 'id', valueField: 'id')
                ->where($cond)
                ->toArray();
            $appraisalRuleCodeIds = $AppraisalRuleCodes
                ->find('list', keyField: 'id', valueField: 'id')
                ->where($cond)
                ->toArray();
            // controls
            if (
                $this->cannotUseId('archive.originating_agency_id', self::unListOptions($oa->toArray()))
                || $this->cannotUseId('archive.profile_id', array_keys($profiles->toArray()))
                || $this->cannotUseId('access_rule.access_rule_code_id', $accessRuleCodeIds)
                || $this->cannotUseId('appraisal_rule.final_action_code', Hash::extract($finals, '{n}.value'))
                || $this->cannotUseId('appraisal_rule.appraisal_rule_code_id', $appraisalRuleCodeIds)
            ) {
                throw new ForbiddenException();
            }
            if ($response = $this->saveEdit($archiveUnit)) {
                return $response;
            }
        }

        // options
        $this->set('originating_agencies', $oa);
        $this->set('agreements', $agreements->all());
        $this->set('codes', $codes);
        $this->set('finals', $finals);
        $this->set('durations', $durations);
    }

    /**
     * Return true si l'id contenu dans la requête pour $dataPath n'est pas présent dans les id fournis
     * @param string $dataPath
     * @param int[]  $ids
     * @return bool
     */
    private function cannotUseId(string $dataPath, array $ids): bool
    {
        $data = $this->getRequest()->getData($dataPath);
        return $data && !in_array($data, $ids);
    }

    /**
     * Transforme une liste pour select (listOptions) en simple tableau d'id
     * @param array $list
     * @return array
     */
    public static function unListOptions(array $list): array
    {
        $fn = fn($carry, $item) => array_merge($carry, array_keys($item));
        return array_reduce(
            Hash::extract($list, '{s}'),
            $fn,
            []
        );
    }

    /**
     * Enregistrement des modifications de l'action edit()
     * @param EntityInterface $archiveUnit
     * @return CakeResponse|void
     * @throws DOMException
     * @throws VolumeException
     */
    private function saveEdit(EntityInterface $archiveUnit)
    {
        $request = $this->getRequest();

        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        $entities = [];

        /**
         * Archive
         */
        if ($archiveUnit->get('parent_id') === null) { // type archive
            $this->saveEditArchive($archiveUnit, $entities);
        }

        /**
         * ArchiveUnit
         */
        $dataArchiveUnit = [
            'name' => $request->getData('name')
                ?: $request->getData('archive.name'),
        ];
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $ArchiveUnits->patchEntity($archiveUnit, $dataArchiveUnit);
        $entities['archive_units'][] = $archiveUnit;

        /**
         * AccessRule - restriction d'accès
         */
        $accessRule = $this->saveEditAccessRule($archiveUnit, $entities);

        /**
         * AppraisalRule - DUA
         */
        $appraisalRule = $this->saveEditAppraisalRule($archiveUnit, $entities);
        $calcDuaExpired = $appraisalRule->isDirty();
        /** @var EntityInterface $appraisalRule */
        $appraisalRule = $archiveUnit->get('appraisal_rule');

        /**
         * ArchiveDescription
         */
        $accessRuleDescription = $this->saveEditArchiveDescription($archiveUnit, $entities);

        /**
         * Détection des modifications
         */
        $dirties = [];
        $loc = TableRegistry::getTableLocator();
        foreach ($entities as $path => $es) {
            /** @var EntityInterface $entity */
            foreach ($es as $entity) {
                $tableName = Inflector::underscore($entity->getSource());
                $table = $loc->get($tableName);
                $schema = $table->getSchema();
                $columns = $schema->columns();
                foreach ($entity->getDirty() as $dirty) {
                    if (!in_array($dirty, $columns)) {
                        continue;
                    }
                    $dirties[] = $path . '.' . $dirty;
                }
            }
        }
        $entries = $this->prepareEntries($archiveUnit, $dirties);

        /**
         * Si il n'y a pas de modification, on sort ici pour éviter la mise
         * à jour de la description et du cycle de vie
         */
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        if (empty($entries)) {
            $this->Modal->success();
            return $this->renderData($archiveUnit->toArray());
        }

        /**
         * Effectue les sauvegardes
         */
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $conn = $Archives->getConnection();
        $conn->begin();
        $success = true;
        foreach ($entities as $es) {
            /** @var EntityInterface $entity */
            foreach ($es as $entity) {
                $model = $this->fetchTable($entity->getSource());
                $model->save($entity);
                $error = FormatError::logEntityErrors($entity);
                if ($error) {
                    $this->Flash->error($error);
                    $success = false;
                    break;
                }
            }
        }

        // reconstruction des entités enfants après sauvegarde pour updateDescriptionXml
        /** @var AccessRuleCodesTable $AccessRuleCodes */
        $AccessRuleCodes = $this->fetchTable('AccessRuleCodes');
        $accessRule->set(
            'access_rule_code',
            $AccessRuleCodes->get(
                $accessRule->get('access_rule_code_id')
            )
        );
        /** @var AppraisalRuleCodesTable $AppraisalRuleCodes */
        $AppraisalRuleCodes = $this->fetchTable('AppraisalRuleCodes');
        $appraisalRule->set(
            'appraisal_rule_code',
            $AppraisalRuleCodes->get(
                $appraisalRule->get('appraisal_rule_code_id')
            )
        );
        if (isset($accessRuleDescription) && $accessRuleDescription->get('access_rule_code_id')) {
            $accessRuleCodeDesc = $AccessRuleCodes->get(
                $accessRuleDescription->get('access_rule_code_id')
            );
            $accessRuleDescription->set(
                'access_rule_code',
                $accessRuleCodeDesc
            );
        }

        /**
         * ================== Mise à jour de la description ================
         */
        $xmlSize = Hash::get(
            $archiveUnit,
            'archive.description_xml_archive_file.stored_file.size'
        );
        LimitBreak::setTimeLimit(
            (int)$xmlSize / 10000000 * 60 + 45
        ); // 1 min / 10Mo + 45 secondes
        LimitBreak::setMemoryLimit($xmlSize * 10);
        $success = $success
            && $ArchiveUnits->updateDescriptionXml($archiveUnit, true)
            && $EventLogs->saveMany($entries);
        if ($success) {
            /** @var EntityInterface $archive */
            $storedFile = Hash::get(
                $archiveUnit,
                'archive.lifecycle_xml_archive_file.stored_file',
                []
            );
            $success = $Archives->updateLifecycleXml(
                $storedFile,
                $entries,
                true
            );
        }
        if ($success) {
            /**
             * Mise à jour des champs search et expired_dua_root
             */
            $ArchiveUnits->updateSearchField(
                ['ArchiveUnits.id' => $archiveUnit->id]
            );
            $ArchiveUnits->updateFullSearchField(
                ['ArchiveUnits.id' => $archiveUnit->id]
            );
            if ($calcDuaExpired) {
                $ArchiveUnits->calcDuaExpiredRoot(
                    [
                        'ArchiveUnits.archive_id' => $archiveUnit->get(
                            'archive_id'
                        ),
                    ]
                );
            }
            if (!empty($entities['access_rules'])) {
                $ArchiveUnits->updateMaxAggregatedAccessEndDate($archiveUnit);
            }
            $conn->commit();
            $this->Modal->success();
            $archiveUnit = $ArchiveUnits->find()
                ->where(['ArchiveUnits.id' => $archiveUnit->id])
                ->contain(
                    [
                        'Archives' => [
                            'Transfers',
                            'Agreements',
                            'Profiles',
                            'OriginatingAgencies',
                            'TransferringAgencies',
                            'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                            'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        ],
                        'ArchiveDescriptions' => [
                            'AccessRules' => ['AccessRuleCodes'],
                        ],
                        'ArchiveKeywords',
                        'AccessRules' => ['AccessRuleCodes'],
                        'AppraisalRules' => ['AppraisalRuleCodes'],
                        'ParentArchiveUnits',
                    ]
                )
                ->firstOrFail();
            return $this->renderData($archiveUnit->toArray());
        } else {
            $conn->rollback();
            $this->Modal->fail();
        }
    }

    /**
     * Sauvegarde les modifications sur un archive_unit avec parent_id null
     * @param EntityInterface $archiveUnit
     * @param array           $entities
     * @return void
     */
    private function saveEditArchive(EntityInterface $archiveUnit, array &$entities)
    {
        $request = $this->getRequest();
        $originating_agency_id = $request->getData('archive.originating_agency_id')
            ?: Hash::get($archiveUnit, 'archive.originating_agency_id');
        $dataArchive = [
            'name' => $request->getData('archive.name')
                ?: $request->getData('name'),
            'description' => $request->getData(
                'archive_description.description'
            ) ?: null,
            'oldest_date' => $request->getData(
                'archive_description.oldest_date'
            ),
            'latest_date' => $request->getData(
                'archive_description.latest_date'
            ),
            'agreement_id' => $request->getData('archive.agreement_id'),
            'profile_id' => $request->getData('archive.profile_id'),
            'originating_agency_id' => $originating_agency_id,
        ];
        /** @var EntityInterface $archiveEntity */
        $archiveEntity = $archiveUnit->get('archive');
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $Archives->patchEntity($archiveEntity, $dataArchive);
        $entities['archives'][] = $archiveEntity;
        $archiveEntity->set(
            'originating_agency',
            $OrgEntities->get($originating_agency_id)
        );
    }

    /**
     * Sauvegarde les modifications sur un AccessRule
     * @param EntityInterface $archiveUnit
     * @param array           $entities
     * @return EntityInterface $accessRule
     * @throws VolumeException
     */
    private function saveEditAccessRule(EntityInterface $archiveUnit, array &$entities): EntityInterface
    {
        $request = $this->getRequest();
        /** @var EntityInterface $accessRule */
        $accessRule = $archiveUnit->get('access_rule');
        /** @var EntityInterface $parentAccessRule */
        $parentAccessRule = Hash::get(
            $archiveUnit,
            'parent_archive_unit.access_rule'
        );
        $archiveUnit->set(
            'use_parent_access_rule',
            $request->getData('use_parent_access_rule')
            && $archiveUnit->get('parent_archive_unit')
        );
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        /** @var AccessRulesTable $AccessRules */
        $AccessRules = $this->fetchTable('AccessRules');
        if (
            $request->getData('use_parent_access_rule')
            && $parentAccessRule
            && $parentAccessRule->id !== $accessRule->id
        ) {
            /** @var EntityInterface|null $oldAccessRule */
            $oldAccessRule = $archiveUnit->get('access_rule');
            $accessRule = $parentAccessRule;
            // propage la modifications aux enfants qui héritent du même access_rule
            $ArchiveUnits->updateAll(
                ['access_rule_id' => $parentAccessRule->id],
                [
                    'archive_id' => $archiveUnit->get('archive_id'),
                    'lft >=' => $archiveUnit->get('lft'),
                    'rght <=' => $archiveUnit->get('rght'),
                    'access_rule_id' => $oldAccessRule->id,
                ]
            );
            $ArchiveUnits->deletePublicDescription($archiveUnit);
            // Supprime l'access rule si il n'y y a plus de liens
            if ($oldAccessRule->get('has_many_count') === 0) {
                $AccessRules->delete($oldAccessRule);
            }
            $this->ArchiveUnits->updateMaxAggregatedAccessEndDate($archiveUnit);
        } elseif ($request->getData('access_rule')) {
            $dataAccessRule = [
                'access_rule_code_id' => $request->getData(
                    'access_rule.access_rule_code_id'
                ),
                'start_date' => $request->getData('access_rule.start_date'),
            ];
            if ($parentAccessRule && $parentAccessRule->id === $accessRule->id) {
                // si il a héritage et que use_parent_access_rule est à false, on créer un access_rule
                $accessRule = $AccessRules->newEntity(
                    $dataAccessRule,
                    ['validate' => false]
                );
                $AccessRules->saveOrFail($accessRule);
                $ArchiveUnits->updateAll(
                    ['access_rule_id' => $accessRule->id],
                    [
                        'access_rule_id' => $parentAccessRule->id,
                        'archive_id' => $archiveUnit->get('archive_id'),
                        'lft >=' => $archiveUnit->get('lft'),
                        'rght <=' => $archiveUnit->get('rght'),
                    ]
                );
                $ArchiveUnits->deletePublicDescription($archiveUnit);
                $this->ArchiveUnits->updateMaxAggregatedAccessEndDate($archiveUnit);
            } else {
                $AccessRules->patchEntity(
                    $accessRule,
                    $dataAccessRule
                );
            }
            $entities['access_rules'][] = $accessRule;
        }
        $ArchiveUnits->patchEntity(
            $archiveUnit,
            ['access_rule_id' => $accessRule->id]
        );
        $archiveUnit->set('access_rule', $accessRule);
        return $accessRule;
    }

    /**
     * Sauvegarde les modifications sur un AppraisalRule
     * @param EntityInterface $archiveUnit
     * @param array           $entities
     * @return EntityInterface
     * @throws VolumeException
     */
    private function saveEditAppraisalRule(EntityInterface $archiveUnit, array &$entities): EntityInterface
    {
        $request = $this->getRequest();
        /** @var EntityInterface $appraisalRule */
        $appraisalRule = $archiveUnit->get('appraisal_rule');
        /** @var EntityInterface $parentAppraisalRule */
        $parentAppraisalRule = Hash::get(
            $archiveUnit,
            'parent_archive_unit.appraisal_rule'
        );
        $archiveUnit->set(
            'use_parent_appraisal_rule',
            $request->getData('use_parent_appraisal_rule')
            && $archiveUnit->get('parent_archive_unit')
        );
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        /** @var AppraisalRulesTable $AppraisalRules */
        $AppraisalRules = $this->fetchTable('AppraisalRules');
        if (
            $request->getData('use_parent_appraisal_rule')
            && $parentAppraisalRule
            && $parentAppraisalRule->id !== $appraisalRule->id
        ) {
            /** @var EntityInterface|null $oldAppraisalRule */
            $oldAppraisalRule = $archiveUnit->get('appraisal_rule');
            $appraisalRule = $parentAppraisalRule;
            // propage la modifications aux enfants qui héritent du même appraisal_rule
            $ArchiveUnits->updateAll(
                ['appraisal_rule_id' => $parentAppraisalRule->id],
                [
                    'archive_id' => $archiveUnit->get('archive_id'),
                    'lft >=' => $archiveUnit->get('lft'),
                    'rght <=' => $archiveUnit->get('rght'),
                    'appraisal_rule_id' => $oldAppraisalRule->id,
                ]
            );
            $ArchiveUnits->deletePublicDescription($archiveUnit);
            // Supprime l'appraisal rule si il n'y y a plus de liens
            if ($oldAppraisalRule->get('has_many_count') === 0) {
                $AppraisalRules->delete($oldAppraisalRule);
            }
        } elseif ($request->getData('appraisal_rule')) {
            $dataAppraisalRule = [
                'final_action_code' => $request->getData(
                    'appraisal_rule.final_action_code'
                ),
                'appraisal_rule_code_id' => $request->getData(
                    'appraisal_rule.appraisal_rule_code_id'
                ),
                'start_date' => $request->getData(
                    'appraisal_rule.start_date'
                ),
            ];
            if ($parentAppraisalRule && $parentAppraisalRule->id === $appraisalRule->id) {
                // si il a héritage et que use_parent_appraisal_rule est à false, on créer un appraisal_rule
                $appraisalRule = $AppraisalRules->newEntity(
                    $dataAppraisalRule,
                    ['validate' => false]
                );
                $AppraisalRules->saveOrFail($appraisalRule);
                $ArchiveUnits->updateAll(
                    ['appraisal_rule_id' => $appraisalRule->id],
                    [
                        'appraisal_rule_id' => $parentAppraisalRule->id,
                        'archive_id' => $archiveUnit->get('archive_id'),
                        'lft >=' => $archiveUnit->get('lft'),
                        'rght <=' => $archiveUnit->get('rght'),
                    ]
                );
                $ArchiveUnits->deletePublicDescription($archiveUnit);
            } else {
                $AppraisalRules->patchEntity(
                    $appraisalRule,
                    $dataAppraisalRule
                );
            }
            $entities['appraisal_rules'][] = $appraisalRule;
        }
        $ArchiveUnits->patchEntity(
            $archiveUnit,
            ['appraisal_rule_id' => $appraisalRule->id]
        );
        $archiveUnit->set('appraisal_rule', $appraisalRule);
        return $appraisalRule;
    }

    /**
     * Sauvegarde les modifications sur un ArchiveDescription
     * @param EntityInterface $archiveUnit
     * @param array           $entities
     * @return EntityInterface|void $accessRuleDescription
     * @throws VolumeException
     */
    private function saveEditArchiveDescription(EntityInterface $archiveUnit, array &$entities): ?EntityInterface
    {
        $request = $this->getRequest();
        /** @var EntityInterface $description */
        $description = Hash::get($archiveUnit, 'archive_description');
        if ($description) {
            $baseDesc = 'archive_description.';
            $dataDescription = [
                'description' => $request->getData(
                    $baseDesc . 'description'
                ) ?: null,
                'oldest_date' => $request->getData(
                    $baseDesc . 'oldest_date'
                ),
                'latest_date' => $request->getData(
                    $baseDesc . 'latest_date'
                ),
                'history' => $request->getData($baseDesc . 'history')
                    ?: null,
            ];
            /** @var ArchiveDescriptionsTable $ArchiveDescriptions */
            $ArchiveDescriptions = $this->fetchTable('ArchiveDescriptions');
            $ArchiveDescriptions->patchEntity(
                $description,
                $dataDescription
            );
            $entities['archive_descriptions'][] = $description;
            /** @var EntityInterface|null $parentDescriptionAccessRule */
            $parentDescriptionAccessRule = Hash::get(
                $archiveUnit,
                'parent_description.access_rule'
            );

            /**
             * Hérite de la restriction d'accès d'une description d'une unité parente
             */
            $archiveUnit->set(
                'use_parent_description_access_rule',
                $request->getData('use_parent_description_access_rule')
                && $archiveUnit->get('parent_description')
            );
            /** @var ArchiveUnitsTable $ArchiveUnits */
            $ArchiveUnits = $this->fetchTable('ArchiveUnits');
            /** @var AccessRulesTable $AccessRules */
            $AccessRules = $this->fetchTable('AccessRules');
            if ($request->getData('use_parent_description_access_rule')) {
                /** @var EntityInterface|null $oldAccessRule */
                $oldAccessRule = $description->get('access_rule');
                if ($parentDescriptionAccessRule && $parentDescriptionAccessRule->id !== $oldAccessRule->id) {
                    $description->set(
                        'access_rule_id',
                        $parentDescriptionAccessRule->id
                    );
                    /** @var ArchiveKeywordsTable $ArchiveKeywords */
                    $ArchiveKeywords = $this->fetchTable('ArchiveKeywords');
                    $ArchiveKeywords->updateAll(
                        ['access_rule_id' => $parentDescriptionAccessRule->id],
                        [
                            'archive_unit_id' => $archiveUnit->id,
                            'access_rule_id' => $oldAccessRule->id,
                        ]
                    );
                    $this->ArchiveUnits->updateMaxAggregatedAccessEndDate($archiveUnit);
                    $ArchiveUnits->deletePublicDescription(
                        $archiveUnit
                    );
                    // Supprime l'access rule si il n'y y a plus de liens
                    if ($oldAccessRule->get('has_many_count') === 0) {
                        $AccessRules->delete($oldAccessRule);
                    }
                    $this->ArchiveUnits->updateMaxAggregatedAccessEndDate($archiveUnit);
                }
                $accessRuleDescription = $parentDescriptionAccessRule;
            } else {
                /** @var EntityInterface $accessRuleDescription */
                $accessRuleDescription = Hash::get(
                    $archiveUnit,
                    'archive_description.access_rule'
                );
                $baseAccess = 'archive_description.access_rule.';
                $dataDescriptionAccessRules = [
                    'access_rule_code_id' => $request->getData(
                        $baseAccess . 'access_rule_code_id'
                    ),
                    'start_date' => $request->getData(
                        $baseAccess . 'start_date'
                    ),
                ];
                if (
                    $parentDescriptionAccessRule
                    && $parentDescriptionAccessRule->id === $accessRuleDescription->id
                    && !empty($dataDescriptionAccessRules['access_rule_code_id'])
                ) {
                    // si il a héritage et que use_parent_access_rule est à false, on créer un access_rule
                    $accessRuleDescription = $AccessRules->newEntity(
                        $dataDescriptionAccessRules,
                        ['validate' => false]
                    );
                    $AccessRules->saveOrFail($accessRuleDescription);
                    $description->set(
                        'access_rule_id',
                        $accessRuleDescription->id
                    );
                    $description->set(
                        'access_rule',
                        $accessRuleDescription
                    );
                    $ArchiveUnits->updateAll(
                        ['access_rule_id' => $accessRuleDescription->id],
                        [
                            'access_rule_id' => $parentDescriptionAccessRule->id,
                            'archive_id' => $archiveUnit->get('archive_id'),
                            'lft >=' => $archiveUnit->get('lft'),
                            'rght <=' => $archiveUnit->get('rght'),
                        ]
                    );
                    $ArchiveUnits->deletePublicDescription(
                        $archiveUnit
                    );
                    $this->ArchiveUnits->updateMaxAggregatedAccessEndDate($archiveUnit);
                } elseif (!empty($dataDescriptionAccessRules['access_rule_code_id'])) {
                    $AccessRules->patchEntity(
                        $accessRuleDescription,
                        $dataDescriptionAccessRules
                    );
                } else { // seda 2.1 et 2.2, la règle de la description doit concorder
                    $accessRuleDescription->set(
                        'access_rule_code_id',
                        Hash::get($archiveUnit, 'access_rule.access_rule_code_id')
                    );
                    $accessRuleDescription->set(
                        'start_date',
                        Hash::get($archiveUnit, 'access_rule.start_date')
                    );
                }

                $entities['archive_descriptions.access_rules'][] = $accessRuleDescription;
            }

            // Si un pdf a été généré, on le supprime afin de permettre une nouvelle génération
            /** @var Archive $archive */
            $archive = $archiveUnit->get('archive');
            $pdf = $archive->get('pdf');
            /** @var StoredFilesTable $StoredFiles */
            $StoredFiles = $this->fetchTable('StoredFiles');
            $ecsId = $archive->get('secure_data_space_id');
            $storedFile = $StoredFiles->find()
                ->where(
                    [
                        'name' => $pdf,
                        'secure_data_space_id' => $ecsId,
                    ]
                )
                ->first();
            if ($storedFile) {
                $manager = new VolumeManager($ecsId);
                $manager->fileDelete($storedFile->get('name'));
            }

            // Suppression du zip de l'archive pour permettre une nouvelle génération
            $filename = $archive->get('zip');
            if (is_file($filename)) {
                unlink($filename);
            }
            // Suppression du zip des unités d'archives parentes
            $parents = $ArchiveUnits->find()
                ->where(
                    [
                        'archive_id' => $archiveUnit->get('archive_id'),
                        'lft <=' => $archiveUnit->get('lft'),
                        'rght >=' => $archiveUnit->get('rght'),
                    ]
                )
                ->all();
            foreach ($parents as $parent) {
                $filename = $parent->get('zip');
                if (is_file($filename)) {
                    unlink($filename);
                }
            }
            $this->ArchiveUnits->updateMaxAggregatedAccessEndDate($archiveUnit);
        }
        return $accessRuleDescription ?? null;
    }

    /**
     * Prépare une liste d'evenements
     * @param EntityInterface $archiveUnit
     * @param array           $dirties
     * @return array
     */
    private function prepareEntries(
        EntityInterface $archiveUnit,
        array $dirties
    ) {
        $translateHelper = new TranslateHelper(new View());
        $entries = [];
        $compares = [
            'archive_units.name' => [
                'path' => 'name',
                'translate' => __("Nom"),
            ],
            'archives.agreement_id' => [
                'path' => 'archive.agreement_id',
                'translate' => __("Accord de versement"),
            ],
            'archives.profile_id' => [
                'path' => 'archive.profile_id',
                'translate' => __("Profil d'archive"),
            ],
            'archives.originating_agency_id' => [
                'path' => 'archive.originating_agency_id',
                'translate' => __("Service producteur"),
            ],
            'access_rules.access_rule_code_id' => [
                'path' => 'access_rule.access_rule_code_id',
                'translate' => __("Délai de communicabilité"),
            ],
            'access_rules.start_date' => [
                'path' => 'access_rule.start_date',
                'translate' => __("Date de départ du calcul (communicabilité)"),
            ],
            'appraisal_rules.final_action_code' => [
                'path' => 'appraisal_rule.final_action_code',
                'translate' => __("Sort final à appliquer"),
            ],
            'appraisal_rules.appraisal_rule_code_id' => [
                'path' => 'appraisal_rule.appraisal_rule_code_id',
                'translate' => __("Durée d'utilité administrative"),
            ],
            'appraisal_rules.start_date' => [
                'path' => 'appraisal_rule.start_date',
                'translate' => __("Date de départ du calcul (sort final)"),
            ],
            'archive_descriptions.description' => [
                'path' => 'archive_description.description',
                'translate' => __("Description"),
            ],
            'archive_descriptions.oldest_date' => [
                'path' => 'archive_description.oldest_date',
                'translate' => __("Date la plus ancienne"),
            ],
            'archive_descriptions.latest_date' => [
                'path' => 'archive_description.latest_date',
                'translate' => __("Date la plus récente"),
            ],
            'archive_descriptions.history' => [
                'path' => 'archive_description.history',
                'translate' => __("Historique"),
            ],
            'archive_descriptions.access_rule_id' => [
                'path' => 'archive_description.access_rule_id',
                'translate' => __("Restriction d'accès (de la description)"),
            ],
            'archive_descriptions.access_rules.access_rule_code_id' => [
                'path' => 'archive_description.access_rule.access_rule_code_id',
                'translate' => __(
                    "Délai de communicabilité de la description"
                ),
            ],
            'archive_descriptions.access_rules.start_date' => [
                'path' => 'archive_description.access_rule.start_date',
                'translate' => __(
                    "Date de départ du calcul (communicabilité) de la description"
                ),
            ],
        ];
        $filtered = array_filter(
            $compares,
            function ($key) use ($dirties) {
                return in_array($key, $dirties);
            },
            ARRAY_FILTER_USE_KEY
        );
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        foreach ($filtered as $params) {
            $message = sprintf(
                ': %s: %s',
                $params['translate'],
                $this->compareOriginal($params['path'], $archiveUnit)
            );
            $entries[] = $EventLogs->newEntry(
                'edit_' . Inflector::singularize($this->getName()),
                'success',
                __(
                    "Modifié ({0}) par l'utilisateur ({1})",
                    $translateHelper->permission($this->getName()),
                    Hash::get($this->user, 'username')
                ) . $message,
                $this->userId,
                $archiveUnit
            );
        }
        return $entries;
    }

    /**
     * Compare la nouvelle valeur avec la valeur originale pour donner un message
     * @param string          $path
     * @param EntityInterface $archiveUnit
     * @return string
     */
    private function compareOriginal(
        string $path,
        EntityInterface $archiveUnit
    ): string {
        $parentPath = substr($path, 0, strrpos($path, '.'));
        $targetField = substr($path, ($a = strrpos($path, '.')) ? $a + 1 : 0);

        // sélectionne l'entité sur laquelle récupérer l'original et la valeur actuelle
        if ($parentPath) {
            $parent = Hash::get($archiveUnit, $parentPath);
        } else {
            $parent = $archiveUnit;
        }
        $original = $parent->getOriginal($targetField);
        $new = $parent->get($targetField);

        // formate les dates
        $original = $this->formatDate($original);
        $new = $this->formatDate($new);

        // remplace un id par 'identifier (name)'
        if (preg_match('/^(.*)_id$/', $targetField, $m)) {
            $loc = TableRegistry::getTableLocator();
            if ($m[1] === 'originating_agency') {
                $m[1] = 'org_entity';
            }
            $modelName = Inflector::camelize(Inflector::pluralize($m[1]));
            $model = $loc->get($modelName);
            $isRule = str_contains($targetField, 'rule_code');
            $identifier = $isRule ? 'code' : 'identifier';
            if ($original) {
                $originalEntity = $model->get($original);
                $original = sprintf(
                    '%s (%s)',
                    $originalEntity->get($identifier),
                    $originalEntity->get('name')
                );
            }
            if ($new) {
                $newEntity = $model->get($new);
                $new = sprintf(
                    '%s (%s)',
                    $newEntity->get($identifier),
                    $newEntity->get('name')
                );
            }
        }

        if ((!$original) && $new) {
            return __("Ajout de la valeur ''{0}''", $new);
        } elseif ($original && $new && $original !== $new) {
            return __("Valeur ''{0}'' modifiée en ''{1}''", $original, $new);
        } elseif ($original && (!$new)) {
            return __("Retrait de la valeur ''{0}''", $original);
        } else {
            return '';
        }
    }

    /**
     * Téléchargement du zip d'une archive
     * @param string $id
     * @return CoreResponse|CakeResponse
     * @throws VolumeException
     * @throws DOMException
     */
    public function downloadFiles(string $id): CakeResponse
    {
        /** @var ArchiveUnit $archiveUnit */
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $archiveUnit = $ArchiveUnits->findByIndexSealed(
            $this->Seal->hierarchicalView(),
            false
        )
            ->where(['ArchiveUnits.id' => $id])
            ->contain(
                [
                    'Archives' => [
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                    ],
                ]
            )
            ->firstOrFail();

        // regarder si le zip existe déjà
        $fileName = $archiveUnit->get('zip');
        if (!is_file($fileName)) {
            throw new NotFoundException();
        }

        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        $entry = $EventLogs->newEntry(
            'download_ArchiveUnitZip',
            'success',
            __(
                "Téléchargement des fichiers de l'unité d'archives ''{0}''",
                $archiveUnit->get('archival_agency_identifier')
            ),
            $this->userId,
            $archiveUnit
        );
        $EventLogs->saveOrFail($entry);

        $storedFile = Hash::get(
            $archiveUnit,
            'archive.lifecycle_xml_archive_file.stored_file',
            []
        );
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        if (!$Archives->updateLifecycleXml($storedFile, $entry, true)) {
            throw new Exception(
                __(
                    "Impossible de modifier le cycle de vie, une erreur a eu lieu lors de l'enregistrement"
                )
            );
        }

        return $this->getCoreResponse()
            ->withFileStream($fileName, ['mime' => 'application/zip'])
            ->withHeader('X-Asalae-ZipFileDownload', 'download');
    }

    /**
     * Création du zip des fichiers de l'archive (si besoin),
     * ou création asynchrone si on juge le temps trop long (avec envoit de mail à l'utilisateur)
     * @param string $id
     * @return CoreResponse|CakeResponse
     * @throws Exception
     */
    public function makeZip(string $id): CakeResponse
    {
        /** @var ArchiveUnit $archiveUnit */
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $archiveUnit = $ArchiveUnits->findByIndexSealed(
            $this->Seal->hierarchicalView(),
            false
        )
            ->where(['ArchiveUnits.id' => $id])
            ->contain(
                [
                    'Archives' => [
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                    ],
                ]
            )
            ->firstOrFail();
        $fileName = $archiveUnit->get('zip');
        if ($this->getRequest()->getQuery('renew') && is_file($fileName)) {
            unlink($fileName);
        }
        // le zip existe déjà
        if (is_file($fileName)) {
            return $this->getResponse()
                ->withHeader('X-Asalae-ZipFileDownload', 'download');
        }

        // le temps de création du zip est jugé trop long
        /** @var Exec $exec */
        $exec = Utility::get(Exec::class);
        $timelimit = Configure::read('Downloads.asyncFileCreationTimeLimit');
        if (
            $archiveUnit->zipCreationTimeEstimation() > $timelimit
            || $this->getRequest()->getQuery('forceAsync')
        ) {
            $exec->async(
                CAKE_SHELL,
                'zip_files',
                'archive_unit',
                $archiveUnit->id,
                $this->userId
            );

            return $this->getResponse()
                ->withHeader('X-Asalae-ZipFileDownload', 'email')
                ->withStringBody(
                    __(
                        "Le fichier est trop volumineux pour être téléchargé 
                        dans l'immédiat, un email vous sera envoyé dès qu'il sera disponible."
                    )
                );
        }

        // creation du fichier
        $result = $exec->command(
            CAKE_SHELL,
            'zip_files',
            'archive_unit',
            $archiveUnit->id,
            $this->userId,
            ['--no-mail' => null]
        );

        if (!$result->success) {
            if ($result->stderr) {
                Log::error($result->stderr);
            }
            return $this->getResponse()
                ->withHeader('X-Asalae-ZipFileDownload', 'fail')
                ->withStatus(500)
                ->withStringBody(
                    __(
                        "Une erreur est survenue lors de la création du fichier zip,"
                         . " veuillez ré-essayer ou contacter un administrateur."
                    )
                );
        }

        return $this->getResponse()
            ->withHeader('X-Asalae-ZipFileDownload', 'download');
    }

    /**
     * permet de télécharger un fichier de pièce jointe par son archive et son nom encodé en base 64
     * @param string $archive_unit_id
     * @param string $b64Filename     nom du fichier encodé en base 64
     * @return CakeResponse
     * @throws VolumeException
     * @throws DOMException|ZMQSocketException
     */
    public function downloadByName(string $archive_unit_id, string $b64Filename)
    {
        /** @var ArchiveBinary $binary */
        $binary = $this->Seal->archivalAgency()
            ->table('ArchiveBinaries')
            ->find('seal')
            ->where(
                [
                    'ArchiveUnits.id' => $archive_unit_id,
                    'ArchiveBinaries.filename' => base64_decode($b64Filename),
                ]
            )
            ->contain(
                [
                    'StoredFiles',
                    'ArchiveUnits' => [
                        'Archives' => [
                            'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        ],
                    ],
                    'OtherDatas' => function (Query $q) {
                        return $q->where(
                            ['OtherDatas.type' => 'dissemination_data']
                        )
                            ->limit(1)
                            ->contain(['StoredFiles']);
                    },
                ]
            )
            ->firstOrFail();

        // vérifi que le binary est communicable
        $today = (new DateTime())->format('Y-m-d');
        /** @var EntityInterface $archiveUnit */
        $archiveUnit = Hash::get($binary, 'archive_units.0');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $nonCommunicable = count(
            $ArchiveUnits->find()
                ->select(['existing' => 1])
                ->innerJoinWith('AccessRules')
                ->where(
                    [
                        'ArchiveUnits.lft <=' => $archiveUnit->get('lft'),
                        'ArchiveUnits.rght >=' => $archiveUnit->get('rght'),
                        'AccessRules.end_date >' => $today,
                    ]
                )
                ->limit(1)
                ->disableHydration()
                ->toArray()
        );
        if ($nonCommunicable) {
            throw new ForbiddenException(__("Fichier non communicable"));
        }

        /** @var EntityInterface $disseminationData */
        $disseminationData = Hash::get($binary, 'other_datas.0');
        if ($disseminationData) {
            $binary = $disseminationData;
        }

        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        $entry = $EventLogs->newEntry(
            'download_ArchiveBinary',
            'success',
            __(
                "Téléchargement du fichier ''{0}'' par l'utilisateur ''{1}''",
                $binary->get('filename'),
                Hash::get($this->user, 'name') ?: Hash::get($this->user, 'username')
            ),
            $this->userId,
            $binary
        );
        $entry->set('in_lifecycle', false);
        $EventLogs->saveOrFail($entry);
        $storedFile = Hash::get(
            $binary,
            'archive_units.0.archive.lifecycle_xml_archive_file.stored_file',
            []
        );
        if (!$Archives->updateLifecycleXml($storedFile, $entry, true)) {
            throw new HttpException(__("Echec lors de la mise à jour du cycle de vie"), 500);
        }

        if ($sessionFile = $this->getRequest()->getQuery('sessionFile')) {
            Notify::emit(
                'download_file',
                [
                    'uuid' => $sessionFile,
                    'state' => 'downloading',
                    'last_update' => time(),
                ]
            );
        }

        return $this->getResponse()
            ->withHeader(
                'Content-Disposition',
                'attachment; filename="' . basename(
                    $binary->get('filename')
                ) . '"'
            )
            ->withHeader('Content-Type', $binary->get('mime'))
            ->withHeader(
                'Content-Length',
                Hash::get($binary, 'stored_file.size')
            )
            ->withHeader(
                'File-Size',
                Hash::get($binary, 'stored_file.size')
            )
            ->withBody(
                new CallbackStream(
                    function () use ($binary, $sessionFile) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        $success = $binary->readfile();
                        if ($sessionFile) {
                            Notify::emit(
                                'download_file',
                                [
                                    'uuid' => $sessionFile,
                                    'state' => 'finished',
                                    'last_update' => time(),
                                    'integrity' => $success,
                                ]
                            );
                        }
                    }
                )
            );
    }

    /**
     * Liste des archives transferables
     */
    public function transferable()
    {
        $this->returnable(false);

        /** @var AppraisalRulesTable $AppraisalRules */
        $AppraisalRules = $this->fetchTable('AppraisalRules');
        $this->set(
            'final_action_codes',
            $AppraisalRules->options('final_action_code')
        );

        // options
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $chains = $ValidationChains->findListOptions(
            $this->archivalAgencyId,
            ValidationChainsTable::ARCHIVE_OUTGOING_TRANSFER_REQUEST
        );
        $this->set('validation_chains_outgoing_transfer', $chains);

        $archivingSystemsCount = $this->Seal->archivalAgency()
            ->table('ArchivingSystems')
            ->find('seal')
            ->count();
        $this->set('archivingSystemsCount', $archivingSystemsCount);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex(
            [
                'typeName' => "TransferableArchiveUnits",
                'indexName' => "Archives transférables",
            ]
        );
    }

    /**
     * Liste des archives restiuables
     * @param boolean $addEventLog ajoute l'événement dans le journal si true
     * @return void|CakeResponse
     * @throws Exception
     */
    public function returnable($addEventLog = true)
    {
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $ArchiveUnits->findByIndexSealed(
            $this->Seal->hierarchicalView(),
            true
        )
            ->where(['ArchiveUnits.state IS NOT ' => ArchiveUnitsTable::S_FREEZED]);
        $query->contain(['ArchiveDescriptions']);
        $IndexComponent->setQuery($query)
            ->filter('agreement_id', null, 'Archives.agreement_id')
            ->filter(
                'appraisal_rule_end',
                IndexComponent::FILTER_DATECOMPARENOW,
                'AppraisalRules.end_date'
            )
            ->filter(
                'final_action_code',
                IndexComponent::FILTER_EQUAL,
                'AppraisalRules.final_action_code'
            )
            ->filter('archival_agency_identifier', IndexComponent::FILTER_ILIKE)
            ->filter(
                'created',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.created'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter(
                'original_total_count',
                IndexComponent::FILTER_COUNTOPERATOR
            )
            ->filter('original_total_size', IndexComponent::FILTER_SIZEOPERATOR)
            ->filter(
                'originating_agency_id',
                null,
                'Archives.originating_agency_id'
            )
            ->filter('profile_id', null, 'Archives.profile_id')
            ->filter(
                'transferring_agency_id',
                null,
                'Archives.transferring_agency_id'
            );

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) {
                    return $e->setVirtual(['dates']);
                }
            )
            ->toArray();
        $this->set(
            'count',
            $query->where(
                ['ArchiveUnits.state' => ArchiveUnitsTable::S_AVAILABLE]
            )->count()
        );
        $this->set('data', $data);

        // Options
        /** @var AppraisalRulesTable $AppraisalRules */
        $AppraisalRules = $this->fetchTable('AppraisalRules');
        $this->set(
            'final_action_codes',
            $AppraisalRules->options('final_action_code')
        );

        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('sealList')
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles);

        $agreements = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('sealList')
            ->orderBy(['Agreements.name' => 'asc']);
        $this->set('agreements', $agreements);

        $ta = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES]
            )
            ->orderBy(['OrgEntities.name' => 'asc']);
        $this->set('transferring_agencies', $ta->all());

        $oa = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                [
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES,
                ]
            )
            ->orderBy(['OrgEntities.name' => 'asc']);
        $this->set('originating_agencies', $oa->all());

        $this->set('titleTable', __("Liste des entrées"));

        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $chains = $ValidationChains->findListOptions(
            $this->archivalAgencyId,
            ValidationChainsTable::ARCHIVE_RESTITUTION_REQUEST
        );
        $this->set('validation_chains_delivery', $chains);

        if ($addEventLog === true) {
            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logIndex(
                [
                    'typeName' => "ReturnableArchiveUnits",
                    'indexName' => "Archives restituables",
                ]
            );
        }
    }

    /**
     * Liste des archives éliminables
     */
    public function eliminable()
    {
        $request = $this->getRequest();
        $this->setRequest(
            $request->withQueryParams(
                ['final_action_code' => ['destroy']]
                + $request->getQueryParams()
            )
        );
        $this->duaExpired();

        // options
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $this->fetchTable('ValidationChains');
        $chains = $ValidationChains->findListOptions(
            $this->archivalAgencyId,
            ValidationChainsTable::ARCHIVE_DESTRUCTION_REQUEST
        );
        $this->set('validation_chains_destruction', $chains);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex(
            [
                'typeName' => "DeletableArchiveUnits",
                'indexName' => "Archives éliminables",
            ]
        );
    }

    /**
     * Affichage des unités d'archives disponibles pour lesquelles la DUA est échue.
     */
    public function duaExpired()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        $query = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal')
            ->where(
                [
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                    'Archives.state' => ArchivesTable::S_AVAILABLE,
                    'ArchiveUnits.state' => ArchiveUnitsTable::S_AVAILABLE,
                    'ArchiveUnits.expired_dua_root' => true,
                ]
            )
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'Agreements',
                        'Profiles',
                        'OriginatingAgencies',
                        'TransferringAgencies',
                        'Transfers',
                    ],
                    'AppraisalRules',
                    'AccessRules' => ['AccessRuleCodes'],
                    'ArchiveDescriptions',
                ]
            );
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)) {
            $targetEntity = $this->archivalAgency;
        } else {
            $targetEntity = $this->orgEntity;
        }
        if ($typeEntity === 'SV') {
            $query->andWhere(
                [
                    'OR' => [
                        'Archives.transferring_agency_id' => $targetEntity->id,
                        'Archives.originating_agency_id' => $targetEntity->id,
                    ],
                ]
            );
        } elseif (!in_array($typeEntity, ['SA', 'SE'])) {
            $query->andWhere(
                ['Archives.originating_agency_id' => $targetEntity->id]
            );
        }
        $IndexComponent->setQuery($query)
            ->filter('agreement_id', null, 'Archives.agreement_id')
            ->filter(
                'appraisal_rule_end',
                IndexComponent::FILTER_DATEOPERATOR,
                'AppraisalRules.end_date'
            )
            ->filter('archival_agency_identifier', IndexComponent::FILTER_ILIKE)
            ->filter(
                'created',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.created'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter(
                'final_action_code',
                null,
                'AppraisalRules.final_action_code'
            )
            ->filter('id', IndexComponent::FILTER_IN)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter(
                'original_total_count',
                IndexComponent::FILTER_COUNTOPERATOR
            )
            ->filter('original_total_size', IndexComponent::FILTER_SIZEOPERATOR)
            ->filter(
                'originating_agency_id',
                null,
                'Archives.originating_agency_id'
            )
            ->filter('profile_id', null, 'Archives.profile_id')
            ->filter(
                'transferring_agency_id',
                null,
                'Archives.transferring_agency_id'
            )
            ->filter(
                'transferring_agency_identifier',
                IndexComponent::FILTER_ILIKE,
                'Archives.transferring_agency_identifier'
            )
            ->filter(
                'originating_agency_identifier',
                IndexComponent::FILTER_ILIKE,
                'Archives.originating_agency_identifier'
            )
            ->filter(
                'oldest_date',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.oldest_date'
            )
            ->filter(
                'latest_date',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.latest_date'
            );

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) {
                    return $e->setVirtual(['dates']);
                }
            )
            ->toArray();
        $this->set(
            'count',
            $query->where(
                ['ArchiveUnits.state' => ArchiveUnitsTable::S_AVAILABLE]
            )->count()
        );
        $this->set('data', $data);

        // Options
        /** @var AppraisalRulesTable $AppraisalRules */
        $AppraisalRules = $this->fetchTable('AppraisalRules');
        $this->set(
            'final_action_codes',
            $AppraisalRules->options('final_action_code')
        );

        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('sealList')
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles);

        $agreements = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('sealList')
            ->orderBy(['Agreements.name' => 'asc']);
        $this->set('agreements', $agreements);

        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $ta = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES]
            );
        $this->set('transferring_agencies', $ta->all());

        $oa = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES]
            );
        $this->set('originating_agencies', $oa->all());

        $this->set('titleTable', __("Liste des entrées"));
    }

    /**
     * Lecture de la liste des fichiers binaires des archives accessibles
     * @param string $archivalAgencyIdentifier
     * @param string $type
     * @return CakeResponse
     * @throws Exception
     */
    protected function apiBinaries(string $archivalAgencyIdentifier, string $type = 'all'): CakeResponse
    {
        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');

        $types = array_keys($ArchiveBinaries->options('type'));
        if ($type !== 'all' && !in_array($type, $types)) {
            throw new BadRequestException();
        }

        $au = $this->getArchiveUnitByArchivalAgencyIdentifier($archivalAgencyIdentifier);
        $query = $ArchiveBinaries->find()
            ->select(
                [
                    'filename' => 'ArchiveBinaries.filename',
                    'type' => 'ArchiveBinaries.type',
                    'format' => 'ArchiveBinaries.format',
                    'mime' => 'ArchiveBinaries.mime',
                    'size' => 'StoredFiles.size',
                    'hash' => 'StoredFiles.hash',
                    'hash_algo' => 'StoredFiles.hash_algo',
                    'archive_unit_archival_agency_identifier' => 'ArchiveUnits.archival_agency_identifier',
                    'original_filename' => 'OriginalDatas.filename',
                ]
            )
            ->leftJoinWith('OriginalDatas')
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.Archives')
            ->innerJoinWith('StoredFiles')
            ->where(
                [
                    'ArchiveUnits.lft >=' => $au->get('lft'),
                    'ArchiveUnits.rght <=' => $au->get('rght'),
                    'ArchiveUnits.archive_id' => $au->get('archive_id'),
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                    'Archives.state IN' => [
                        ArchivesTable::S_AVAILABLE,
                        ArchivesTable::S_FREEZED,
                        ArchivesTable::S_FREEZED_TRANSFERRING,
                        ArchivesTable::S_FREEZED_DESTROYING,
                        ArchivesTable::S_FREEZED_RESTITUTING,
                    ],
                ]
                + ($type === 'all'
                    ? []
                    : ['ArchiveBinaries.type' => $type])
            );

        $request = $this->getRequest();
        $filters = $request->getQuery('filters');
        if ($filters) {
            $this->applyFilters($filters, $query);
        }

        $page = (int)$request->getQuery('page', 1);
        $limit = min(
            (int)$request->getQuery('limit', $this->paginate['limit'] ?? 100),
            1000
        );
        $count = $query->count();
        $offset = ($page - 1) * $limit;
        $sort = $request->getQuery('sort', 'id');
        $direction = $request->getQuery('direction', 'asc');

        if ($sort === 'id') {
            $sort = 'ArchiveBinaries.id';
        }

        $datas = $query
            ->orderBy([$sort => $direction])
            ->limit($limit)
            ->offset($offset)
            ->all()
            ->map(
                function (EntityInterface $e) use ($archivalAgencyIdentifier): array {
                    $urlFilename = implode(
                        '/',
                        array_map(
                            'urlencode',
                            explode(
                                '/',
                                $e->get('original_filename') ?: $e->get('filename')
                            )
                        )
                    );
                    $dlUrl = trim(Configure::read('App.fullBaseUrl'), '/ ') . '/api/archive-units/binary/'
                        . $archivalAgencyIdentifier . '/' . $e->get('type') . '/' . $urlFilename;

                    $e->setVirtual([]);
                    $e->set('download', $dlUrl);
                    $e->unset('original_filename');

                    return $e->toArray();
                }
            )
            ->toArray();

        if ($request->getHeader('Accept') === ['application/xml']) {
            $results = [
                'ArchiveBinary' => $datas,
            ];
        } else {
            $results = $datas;
        }

        if (!filter_var($request->getQuery('raw'), FILTER_VALIDATE_BOOL)) {
            $results['pagination'] = compact(
                'page',
                'limit',
                'count',
                'sort',
                'direction'
            );
        }

        return $this->renderData($results);
    }

    /**
     * Récupère une ArchiveUnit à partir de son archivalAgencyIdentifier
     * @param string $archivalAgencyIdentifier
     * @return EntityInterface
     */
    protected function getArchiveUnitByArchivalAgencyIdentifier(
        string $archivalAgencyIdentifier
    ): EntityInterface {
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');

        $query = $ArchiveUnits->find()
            ->innerJoinWith('Archives')
            ->innerJoinWith('Archives.TransferringAgencies')
            ->innerJoinWith('Archives.OriginatingAgencies')
            ->where(
                [
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                    'ArchiveUnits.archival_agency_identifier' => $archivalAgencyIdentifier,
                ]
            );

        return $this->whereSVCondition($query)
            ->firstOrFail();
    }

    /**
     * Ajout des conditions sur le service versant pour Archives.TransferringAgencies
     * @param Query $query
     * @return Query
     */
    protected function whereSVCondition(Query $query): Query
    {
        if (Hash::get($this->orgEntity, 'type_entity.code') === 'SV') {
            $query->andWhere(
                [
                    'OR' => [
                        [
                            'TransferringAgencies.lft >=' => Hash::get(
                                $this->orgEntity,
                                'lft'
                            ),
                            'TransferringAgencies.rght <=' => Hash::get(
                                $this->orgEntity,
                                'rght'
                            ),
                        ],
                        [
                            'OriginatingAgencies.lft >=' => Hash::get(
                                $this->orgEntity,
                                'lft'
                            ),
                            'OriginatingAgencies.rght <=' => Hash::get(
                                $this->orgEntity,
                                'rght'
                            ),
                        ],
                    ],
                ]
            );
        }

        return $query;
    }

    /**
     * Lecture du contenu des binaires des archives accessibles
     * @param string   $archivalAgencyIdentifier
     * @param string   $type
     * @param string[] ...$filename
     * @return CakeResponse
     * @throws Exception
     */
    protected function apiBinary(string $archivalAgencyIdentifier, $type, ...$filename): CakeResponse
    {
        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        /** @var StoredFilesTable $StoredFiles */
        $StoredFiles = $this->fetchTable('StoredFiles');

        $types = array_keys($ArchiveBinaries->options('type'));
        if (!in_array($type, $types)) {
            throw new BadRequestException();
        }

        $au = $this->getArchiveUnitByArchivalAgencyIdentifier($archivalAgencyIdentifier);
        $filename = implode('/', array_map('urldecode', $filename));

        $query = $StoredFiles->find()
            ->innerJoinWith('ArchiveBinaries')
            ->leftJoinWith('ArchiveBinaries.OriginalDatas')
            ->innerJoinWith('ArchiveBinaries.ArchiveUnits')
            ->innerJoinWith('ArchiveBinaries.ArchiveUnits.Archives')
            ->innerJoinWith('ArchiveBinaries.ArchiveUnits.Archives.TransferringAgencies')
            ->innerJoinWith('ArchiveBinaries.ArchiveUnits.Archives.OriginatingAgencies')
            ->where(
                [
                    'ArchiveUnits.lft >=' => $au->get('lft'),
                    'ArchiveUnits.rght <=' => $au->get('rght'),
                    'ArchiveUnits.archive_id' => $au->get('archive_id'),
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                    'ArchiveBinaries.type' => $type,
                ]
                + (
                $type === 'original_data'
                    ? [
                        'ArchiveBinaries.filename' => $filename,
                        'ArchiveBinaries.type' => 'original_data',
                    ]
                    : [
                        'OriginalDatas.filename' => $filename,
                    ]
                )
            );

        /** @var StoredFile $file */
        $file = $this->whereSVCondition($query)
            ->firstOrFail();

        if (!$file->fileExists()) {
            throw new NotFoundException();
        }

        $rawFilename = basename($file->get('name'));
        $primaryFilename = rawurlencode($rawFilename);
        $secondaryFilename = addcslashes(Translit::ascii($rawFilename), '"');

        if ($sessionFile = $this->getRequest()->getQuery('sessionFile')) {
            Notify::emit(
                'download_file',
                [
                    'uuid' => $sessionFile,
                    'state' => 'downloading',
                    'last_update' => time(),
                ]
            );
        }

        return $this->getResponse()
            ->withHeader(
                'Content-Type',
                Hash::get($file, 'archive_binary.mime') ?: 'application/octet-stream'
            )
            ->withHeader(
                'Content-Disposition',
                "attachment; filename*=UTF-8''$primaryFilename; filename=\"$secondaryFilename\""
            )
            ->withHeader(
                'Content-Length',
                $file->get('size')
            )
            ->withHeader(
                'File-Size',
                $file->get('size')
            )
            ->withBody(
                new CallbackStream(
                    function () use ($file, $sessionFile) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        $success = $file->readfile();
                        if ($sessionFile) {
                            Notify::emit(
                                'download_file',
                                [
                                    'uuid' => $sessionFile,
                                    'state' => 'finished',
                                    'last_update' => time(),
                                    'integrity' => $success,
                                ]
                            );
                        }
                    }
                )
            );
    }

    /**
     * Formate la date pour comparaison
     * @param mixed $date
     * @return mixed
     */
    private function formatDate(mixed $date): mixed
    {
        if ($date instanceof CakeDateTime || $date instanceof CakeDate) {
            return $date->i18nFormat();
        } elseif ($date instanceof CoreDate) {
            return $date->getCakeDate()->i18nFormat();
        }
        return $date;
    }
}
