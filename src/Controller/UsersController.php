<?php

/**
 * Asalae\Controller\UsersController
 */

namespace Asalae\Controller;

use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\CookieComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Form\ForgottenPasswordForm;
use AsalaeCore\Utility\FormatError;
use Asalae\Controller\Component\EmailsComponent;
use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Controller\Component\LoginComponent;
use Asalae\Form\ChangeEntityForm;
use Asalae\Model\Entity\Fileupload;
use Asalae\Model\Entity\User;
use Asalae\Model\Table\AuthUrlsTable;
use Asalae\Model\Table\ChangeEntityRequestsTable;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\LdapsTable;
use Asalae\Model\Table\MailsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\RolesTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\UsersTable;
use Asalae\Model\Table\VersionsTable;
use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Core\Configure;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response as CakeResponse;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Tout ce qui conserne l'utilisateur de l'application
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AuthUrlsTable AuthUrls
 * @property ConfigurationsTable Configurations
 * @property CookieComponent Cookie
 * @property EmailsComponent Emails
 * @property EventLogsTable EventLogs
 * @property FileuploadsTable Fileuploads
 * @property LdapsTable Ldaps
 * @property MailsTable Mails
 * @property OrgEntitiesTable OrgEntities
 * @property RolesTable Roles
 * @property UsersTable Users
 * @property VersionsTable Versions
 */
class UsersController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    public const string TABLE_INDEX = 'users-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'username' => 'asc',
        ],
        'sortableFields' => [
            'role' => 'Roles.name',
            'org_entity' => 'OrgEntities.name',
            'ldap' => 'Ldaps.name',
        ],
    ];

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default' => [
                'post' => [
                    'function' => 'apiAdd',
                ],
            ],
        ];
    }

    /**
     * Authorize les méthodes login et logout
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse|void|null
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $AuthenticationComponent->addUnauthenticatedActions(
            [
                'ajaxLogin',
                'forgottenPassword',
                'forgottenPasswordMail',
                'initializePassword',
                'login',
                'logout',
                'resetSession',
            ]
        );
        return parent::beforeFilter($event);
    }

    /**
     * Liste des utilisateurs accessibles par l'utilisateur connecté
     */
    public function index()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $query = $Users->find()
            ->select(
                [
                    'same_archival_agency' => new QueryExpression('ChangeEntityRequests.id IS NULL'),
                    'super_archivist' => new QueryExpression('SuperArchivistsArchivalAgencies.id IS NOT NULL'),
                ]
            )
            ->enableAutoFields()
            ->leftJoinWith('SuperArchivistsArchivalAgencies')
            ->leftJoinWith(
                'ChangeEntityRequests',
                function (Query $q) {
                    return $q->where(
                        ['ChangeEntityRequests.target_archival_agency_id' => $this->archivalAgencyId]
                    );
                }
            )
            ->where(
                [
                    [
                        'OR' => [
                            'ArchivalAgencies.id' => $this->archivalAgencyId,
                            'SuperArchivistsArchivalAgencies.archival_agency_id' => $this->archivalAgencyId,
                            'ChangeEntityRequests.id IS NOT' => null,
                        ],
                    ],
                    [
                        'OR' => [ // suppression des doublons
                            'SuperArchivistsArchivalAgencies.id IS' => null,
                            'SuperArchivistsArchivalAgencies.archival_agency_id' => $this->archivalAgencyId,
                        ],
                    ],
                ]
            )
            ->contain(
                [
                    'ChangeEntityRequests',
                    'Ldaps',
                    'OrgEntities' => ['ArchivalAgencies'],
                    'Roles',
                ]
            );
        $IndexComponent->setQuery($query)
            ->filter('active', IndexComponent::FILTER_IS)
            ->filter('is_validator', IndexComponent::FILTER_IS)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('email', IndexComponent::FILTER_ILIKE)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter(
                'is_linked_to_ldap',
                function ($v) {
                    return ['Users.ldap_id IS' . ($v ? ' NOT' : '') => null];
                }
            )
            ->filter('ldap_id')
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('org_entity_id', IndexComponent::FILTER_IN)
            ->filter('role_id', IndexComponent::FILTER_IN)
            ->filter('username', IndexComponent::FILTER_ILIKE);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        try {
            $data = $this->paginateToResultSet($query)
                ->map(
                    function (EntityInterface $entity) {
                        $entity->setVirtual(['deletable', 'is_linked_to_ldap']);
                        return $entity;
                    }
                )
                ->toArray();
        } catch (NotFoundException) {
            $this->Flash->error(
                __("La page demandée n'existe plus, retour à la page 1")
            );
            return $this->redirect('/users/index');
        }
        $this->set('data', $data);

        // Options
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $entities = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find(
                'sealList',
                valueField: function (EntityInterface $entity) {
                    $parent = Hash::get($entity, 'parent.name');
                    $name = $entity->get('name');
                    return $parent ? sprintf('%s (%s)', $name, $parent) : $name;
                },
            )
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.name',
                    'Parents.name',
                ]
            )
            ->contain(['Parents'])
            ->orderBy(['OrgEntities.name' => 'asc', 'OrgEntities.lft' => 'asc']);
        $this->set('org_entities', $entities->all());
        /** @var RolesTable $Roles */
        $Roles = $this->fetchTable('Roles');
        $roles = $Roles->find('list')
            ->where(
                [
                    'OR' => [
                        'Roles.org_entity_id IS' => null,
                        'Roles.org_entity_id' => $this->archivalAgencyId,
                    ],
                ]
            )
            ->orderBy(['Roles.name' => 'asc']);
        $this->set('roles', $roles->all());
        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $ldaps = $Ldaps->find('list')
            ->where(['Ldaps.org_entity_id' => $this->archivalAgencyId])
            ->orderBy(['Ldaps.name' => 'asc']);
        $this->set('ldaps', $ldaps->all());
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)) {
            $targetEntity = $this->archivalAgency;
        } else {
            $targetEntity = $this->orgEntity;
        }
        $rolesWs = $OrgEntities->findByRoles($targetEntity)
            ->select(['qexists' => 1], true)
            ->where(
                [
                    'OrgEntities.id' => $targetEntity->id,
                    'agent_type' => 'software',
                ]
            )
            ->disableHydration()
            ->first();
        $this->set('rolesWs', (bool)$rolesWs);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * Page d'identification
     *
     * @return Response|null
     * @throws Exception
     */
    public function login()
    {
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $result = $AuthenticationComponent->getResult();
        $request = $this->getRequest();
        if ($request->is('get') && $result->isValid()) { // déjà connecté
            $redirect = $this->redirect(
                ['controller' => 'Home', 'action' => 'index']
            );
            return $this->redirect($redirect);
        }
        if ($request->is('post')) {
            $app = Configure::read('App.name');
            $connectionMessage = __(
                "L'utilisateur ''{0}''",
                h($request->getData('username'))
            );
            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            /** @var VersionsTable $Versions */
            $Versions = $this->fetchTable('Versions');
            $version = $Versions->find()
                ->where(['subject' => $app])
                ->orderBy(['id' => 'desc'])
                ->firstOrFail();
            if ($result->isValid()) {
                $redirect = $this->redirect(
                    ['controller' => 'Home', 'action' => 'index']
                );
                return $this->redirect($redirect);
            }
            $this->Flash->error(__("Login ou mot de passe invalide"));
            $entry = $EventLogs->newEntry(
                'login',
                'fail',
                __("Echec de connexion de {0} à {1}", $connectionMessage, $app),
                null,
                $version
            );
            $EventLogs->save($entry);
        }
        $this->set(
            'entity',
            $Users->newEmptyEntity()
        );

        $this->set('pageTitle', 'Connexion');
        $this->viewBuilder()->setLayout('Libriciel/Login.login');
        $this->viewBuilder()->setTemplate('Libriciel/Login.login');
    }

    /**
     * Déconnexion
     *
     * @return Response|null
     * @throws Exception
     */
    public function logout()
    {
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $result = $AuthenticationComponent->getResult();
        if ($result->isValid() && $this->user && Hash::get($this->user, 'username')) {
            /** @var VersionsTable $Versions */
            $Versions = $this->fetchTable('Versions');
            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            $app = Configure::read('App.name');
            $msg = __("L'utilisateur ''{0}''", h(Hash::get($this->user, 'username')));
            $version = $Versions->find()
                ->where(['subject' => $app])
                ->orderBy(['id' => 'desc'])
                ->firstOrFail();
            $entry = $EventLogs->newEntry(
                'logout',
                'success',
                __("Déconnexion de {0} à {1}", $msg, $app),
                $this->userId,
                $version
            );
            $EventLogs->save($entry);
        }
        $AuthenticationComponent->logout();
        $this->destroySession();
        return $this->redirect('/');
    }

    /**
     * Détruit proprement la session
     */
    private function destroySession()
    {
        /** @use CookieComponent */
        $this->loadComponent('AsalaeCore.Cookie');
        $this->Cookie->setConfig('encryption', false);

        $cookies = $this->getRequest()->getCookieParams();
        unset($cookies['PHPSESSID'], $cookies['XDEBUG_SESSION']);
        foreach (array_keys($cookies) as $cookie) {
            $this->Cookie->delete($cookie);
        }

        if ($this->userId) {
            /** @var FileuploadsTable $Fileuploads */
            $Fileuploads = $this->fetchTable('Fileuploads');
            $query = $Fileuploads->find()
                ->select(['id', 'path'])
                ->where(['user_id' => $this->userId, 'locked' => false]);
            $tmpCount = strlen(TMP);
            /** @var Fileupload $file */
            foreach ($query as $file) {
                if (
                    substr($file->get('path'), 0, $tmpCount) === TMP
                    && is_writable($file->get('path'))
                ) {
                    Filesystem::remove($file->get('path'));
                }
                $Fileuploads->delete($file);
            }
        }

        $this->getRequest()->getSession()->destroy();
    }

    /**
     * Supprime un utilisateur
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function delete(string $id)
    {
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $entity = $Users->get($id);
        $this->checkAccess($entity->get('org_entity_id'));

        $conn = $Users->getConnection();
        $conn->begin();
        if ($entity->get('agent_type') === 'person') {
            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logDelete($entity);
        } else {
            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            $entry = $EventLogs->newEntry(
                'delete_Webservice',
                'success',
                __(
                    "Supprimé ({0}) par l''{1} ({2})",
                    'Webservices',
                    $this->userId ? 'utilisateur' : 'administrateur',
                    $this->userId
                        ? Hash::get($this->user, 'username')
                        : $this->getRequest()->getSession()->read('Admin.data.username')
                ),
                $this->userId, // si utilisateur connecté, null sinon
                $entity
            );
            $EventLogs->saveOrFail($entry);
        }
        if ($Users->delete($entity)) {
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Vérifie qu'un org_entity fait bien partie du service d'archive de l'utilisateur
     * Laisse l'accès à un admin technique
     * @param int $id
     * @throws NotFoundException|Exception
     */
    private function checkAccess($id)
    {
        $adminTech = $this->getRequest()->getSession()->read('Admin.tech');
        if ($adminTech || !$id) {
            return;
        }
        $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->where(['OrgEntities.id' => $id])
            ->firstOrFail();
    }

    /**
     * Modification du profil d'utilisateur
     */
    public function edit()
    {
        return $this->editByAdmin($this->userId);
    }

    /**
     * Modification d'un utilisateur
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function editByAdmin(string $id)
    {
        $request = $this->getRequest();
        $request->allowMethod('ajax');
        $this->viewBuilder()->setLayout('ajax');

        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $query = $this->Seal->archivalAgencyChilds()
            ->table('Users')
            ->find('seal')
            ->where(['Users.id' => $id]);
        $Users->setGenericUserContain($query);
        $entity = $query->firstOrFail();
        $entity->unset('password');

        if ($request->is('put')) {
            $data = [
                'high_contrast' => $request->getData(
                    'high_contrast'
                ),
                'org_entity_id' => $entity->get('org_entity_id'),
                // important pour la validation
                'is_validator' => $request->getData('is_validator') ?? true,
                'use_cert' => $request->getData('use_cert') ?? false,
            ];

            if (!$entity->get('ldap_id')) {
                $data['username'] = $request->getData('username');
                $data['name'] = $request->getData('name');
                $data['email'] = $request->getData('email');
            }

            $password = $request->getData('password');
            if (!$entity->get('ldap_id') && $password) {
                $data += [
                    'password' => $password,
                    'confirm-password' => $request->getData(
                        'confirm-password'
                    ),
                ];
            }
            if ($request->getParam('action') === 'editByAdmin') {
                $data += [
                    'role_id' => $request->getData('role_id'),
                    'active' => $request->getData('active'),
                ];
            } else {
                $data += [
                    'notify_download_pdf_frequency' => 1,
                    'notify_download_zip_frequency' => 1,
                    'notify_validation_frequency' => $request->getData('notify_validation')
                        ? $request->getData('notify_validation_frequency')
                        : null,
                    'notify_job_frequency' => $request->getData('notify_job')
                        ? $request->getData('notify_job_frequency')
                        : null,
                    'notify_agreement_frequency' => $request->getData('notify_agreement')
                        ? $request->getData('notify_agreement_frequency')
                        : null,
                    'notify_transfer_report_frequency' => $request->getData('notify_transfer_report')
                        ? $request->getData('notify_transfer_report_frequency')
                        : null,
                ];
            }
            $Users->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $dirtyPassword = $entity->isDirty('password');
            if ($Users->save($entity, ['renewSession' => true])) {
                if ($request->getParam('action') === 'editByAdmin') {
                    /** @var EventLoggerComponent $EventLogger */
                    $EventLogger = $this->loadComponent('EventLogger');
                    $EventLogger->logEdit($entity);
                }
                $this->Modal->success();
                $query = $Users->find()
                    ->contain(['OrgEntities' => ['TypeEntities'], 'Roles'])
                    ->where(['Users.id' => $id]);
                $Users->setGenericUserContain($query);
                $entity = $query->firstOrFail();
                $entity->setVirtual(['deletable']);
                $json = $entity->toArray();
                unset($json['password'], $json['confirm-password']);
                if ($dirtyPassword) {
                    /** @var EventLogsTable $EventLogs */
                    $EventLogs = $this->fetchTable('EventLogs');
                    $entry = $EventLogs->newEntry(
                        'changedPassword',
                        'success',
                        __(
                            "Modification du mot de passe de l'utilisateur ''{0}''",
                            h($entity->get('username'))
                        ),
                        $this->userId,
                        $entity
                    );
                    $EventLogs->save($entry);
                }
                return $this->renderDataToJson($json);
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('user', $entity);

        // pas de changement possible entre référent technique et les autres
        /** @var RolesTable $Roles */
        $Roles = $this->fetchTable('Roles');
        $refTech = $Roles->find()
            ->where(['code' => 'ADMIN'])
            ->first();
        if (Hash::get($entity, 'role.code') === 'ADMIN') {
            $this->set('roles', [$refTech->get('id') => $refTech->get('name')]);
            $this->set('lockedRole', true);
        } else {
            $roles = $this->setAvailablesRoles($entity->get('org_entity_id'));
            unset($roles[$refTech->get('id')]);
            $this->set('roles', $roles);
        }
    }

    /**
     * permet d'obtenir la liste des rôles Disponible pour un OrgEntity
     * @param int|string $orgEntityId
     * @param string     $type
     * @return array
     */
    private function setAvailablesRoles($orgEntityId, $type = 'person'): array
    {
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        if ($orgEntityId) {
            $sa = $OrgEntities->getEntitySA($orgEntityId);
            $query = $OrgEntities->findByRoles($sa)
                ->where(
                    [
                        'OrgEntities.id' => $orgEntityId,
                        'Roles.agent_type' => $type,
                        'OR' => [
                            'Roles.code !=' => RolesTable::CODE_WS_ADMINISTRATOR,
                            'Roles.code IS' => null,
                        ],
                    ]
                );
            if (!$this->adminTech && !$this->adminTenant) {
                $query->where(
                    [
                        'OR' => [
                            'Roles.code IS' => null,
                            'Roles.code NOT IN' => ['ADMIN', 'WSA'],
                        ],
                    ]
                );
            }
            $roles = $query->all()->toArray();
            $roles = $roles ? array_reduce(
                $roles,
                function ($res, EntityInterface $v) {
                    $res[$v->get('Roles')['id']] = $v->get('Roles')['name'];
                    return $res;
                }
            ) : [];
        } else {
            $roles = [];
        }
        $this->set('roles', $roles);
        return $roles;
    }

    /**
     * Ajouter un utilisateur
     * @param string|null $orgEntityId
     * @return Response
     * @throws Exception
     */
    public function add(string $orgEntityId = null)
    {
        $request = $this->getRequest();
        $request->allowMethod('ajax');
        $this->viewBuilder()->setLayout('ajax');
        $this->checkAccess($orgEntityId);

        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        /** @var RolesTable $Roles */
        $Roles = $this->fetchTable('Roles');

        /** @var User $entity */
        $entity = $Users->newEntity(
            [
                'org_entity_id' => $orgEntityId,
                'agent_type' => 'person',
            ]
        );

        if ($orgEntityId !== null) {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = $this->fetchTable('OrgEntities');
            $orgEntity = $OrgEntities->get($orgEntityId);
            $entity->set('org_entity', $orgEntity);
        }

        if ($request->is('post')) {
            $password = $Users->generatePassword();
            $Users->patchEntity(
                $entity,
                [
                    'role_id' => $request->getData('role_id'),
                    'username' => $request->getData('username'),
                    'password' => $password,
                    'confirm-password' => $password,
                    'name' => $request->getData('name'),
                    'email' => $request->getData('email'),
                    'high_contrast' => $request->getData('high_contrast'),
                    'active' => true,
                    'org_entity_id' => $orgEntityId
                        ?: $request->getData('org_entity_id'),
                    'agent_type' => 'person',
                    'is_validator' => $request->getData('is_validator') ?? true,
                    'use_cert' => $request->getData('use_cert') ?? false,
                ]
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($Users->save($entity)) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAdd($entity);
                $this->Modal->success();
                $entity = $Users->find()
                    ->where(['Users.id' => $entity->id])
                    ->contain(['OrgEntities'])
                    ->firstOrFail();
                $entity->setVirtual(['deletable', 'is_linked_to_ldap']);
                $json = $entity->toArray();
                $json['role'] = $Roles->get($json['role_id'])->toArray();
                unset($json['password'], $json['confirm-password']);
                /** @use EmailsComponent */
                $this->loadComponent('Emails');
                $this->Emails->submitEmailNewUser($entity);
                return $this->renderJson(json_encode($json));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('user', $entity);
        $this->set('title', __("Ajouter un utilisateur"));
        $this->set('orgEntityId', $orgEntityId);
        if ($orgEntityId) {
            $this->setAvailablesRoles($orgEntityId);
        } else {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = $this->fetchTable('OrgEntities');
            $query = $OrgEntities->listOptions([], $this->Seal->orgEntityChilds())
                ->where(
                    [
                        'OrgEntities.active' => true,
                    ]
                )
                ->disableHydration();
            $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
            if (in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)) {
                $targetEntity = $this->archivalAgency;
            } else {
                $targetEntity = $this->orgEntity;
            }
            if ($targetEntity) {
                $query->andWhere(
                    [
                        'OrgEntities.lft >=' => $targetEntity->get('lft'),
                        'OrgEntities.rght <=' => $targetEntity->get('rght'),
                    ]
                );
            }
            $this->set('entities', $query->toArray());
            if ($request->getData('org_entity_id')) {
                $this->setAvailablesRoles($request->getData('org_entity_id'));
            }
        }
    }

    /**
     * Ajout d'un webservice
     * @param string $org_entity_id
     * @return Response
     * @throws Exception
     */
    public function addWebservice(string $org_entity_id)
    {
        $this->checkAccess($org_entity_id);
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $entity = $Users->newEntity(
            ['agent_type' => 'software'],
            ['validate' => false]
        );
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $orgEntity = $OrgEntities->get($org_entity_id);
        $entity->set('org_entity', $orgEntity);
        $request = $this->getRequest();
        if ($request->is('post')) {
            $role_id = $request->getData('role_id');
            /** @var RolesTable $Roles */
            $Roles = $this->fetchTable('Roles');
            if ($request->getData('sha256') && $request->getData('password')) {
                $password = hash('sha256', $request->getData('password'));
                $confirm = hash(
                    'sha256',
                    $request->getData('confirm-password')
                );
            } else {
                $password = $request->getData('password');
                $confirm = $request->getData('confirm-password');
            }
            $data = [
                'org_entity_id' => $org_entity_id,
                'name' => $request->getData('name'),
                'email' => $request->getData('email'),
                'role_id' => $role_id,
                'active' => $request->getData('active'),
                'username' => $request->getData('username'),
                'password' => $password,
                'confirm-password' => $confirm,
                'agent_type' => 'software',
                'is_validator' => false,
                'use_cert' => false,
            ];
            $Users->patchEntity($entity, $data);
            $conn = $Users->getConnection();
            $conn->begin();
            $success = $Users->save($entity);
            if ($success) {
                /** @var EventLogsTable $EventLogs */
                $EventLogs = $this->fetchTable('EventLogs');
                $entry = $EventLogs->newEntry(
                    'add_Webservice',
                    'success',
                    __(
                        "Ajouté ({0}) par l'utilisateur ({1})",
                        'Webservices',
                        Hash::get($this->user, 'username')
                    ),
                    $this->userId, // si utilisateur connecté, null sinon
                    $entity
                );
                $success = $EventLogs->save($entry);
                FormatError::logEntityErrors($entry);
            }
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                $json = $entity->toArray();
                $json['role'] = $Roles->get($json['role_id'])->toArray();
                unset($json['password'], $json['confirm-password']);
                return $this->renderJson(json_encode($json));
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);

        // options
        /** @var RolesTable $Roles */
        $Roles = $this->fetchTable('Roles');
        $roles = $Roles->find('list')
            ->innerJoinWith('TypeEntities')
            ->innerJoinWith('TypeEntities.OrgEntities')
            ->where(
                [
                    'OrgEntities.id' => $org_entity_id,
                    'Roles.agent_type' => 'software',
                    'OR' => [
                        'Roles.code !=' => RolesTable::CODE_WS_ADMINISTRATOR,
                        'Roles.code IS' => null,
                    ],
                ]
            )
            ->orderBy(['Roles.name'])
            ->toArray();
        $this->set('roles', $roles);
    }

    /**
     * Permet la connexion par ajax
     */
    public function ajaxLogin()
    {
        $request = $this->getRequest();
        $this->viewBuilder()->setLayout('ajax');
        if (Configure::read('Keycloak.enabled') || Configure::read('OpenIDConnect.enabled')) {
            if ($request->is('post')) {
                /** @use ModalComponent */
                $this->loadComponent('AsalaeCore.Modal');
                if ($this->user) {
                    $this->Modal->success();
                } else {
                    $this->Modal->fail();
                }
            }
            return;
        }
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $user = $Users->newEmptyEntity();

        if ($request->is('post')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($request->getData('prev_username') !== $request->getData('username')) {
                $user->setErrors(['username' => __("Identifiant incorrect")]);
                $this->Modal->fail();
                $this->set('entity', $user);
                return;
            }

            /** @var AuthenticationComponent $AuthenticationComponent */
            $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
            // vérifi le login/mdp et connecte l'utilisateur
            $result = $AuthenticationComponent->getResult();
            if ($result->isValid()) {
                $this->Modal->success();
            } else {
                $this->Modal->fail();
                $user->setErrors(['name' => __("Login ou mot de passe invalide")]);
            }

            $prev = (int)$request->getData('prev_archival_agency_id');
            if ($prev !== $this->archivalAgencyId) {
                $this->changeLoggedArchivalAgency($prev);
            }
        }
        $this->set('entity', $user);
    }

    /**
     * Sélectionne le service d'archives de l'utilisateur (pour le super archiviste)
     * @param string $archivalAgencyId
     * @return CakeResponse
     */
    public function changeLoggedArchivalAgency(string $archivalAgencyId)
    {
        $this->changeAppLoggedArchivalAgency($archivalAgencyId);
        return $this->renderDataToJson('done');
    }

    /**
     * Permet de sauvegarder le menu utilisateur
     */
    public function ajaxSaveMenu()
    {
        $this->getRequest()->allowMethod('ajax');
        $this->viewBuilder()->setLayout('ajax');
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $report = [];
        if ($this->getRequest()->is('post')) {
            $entity = $Users->get($this->userId);
            $data = ['menu' => $this->getRequest()->getData('menu')];
            $Users->patchEntity($entity, $data);
            $report['success'] = (bool)$Users->save($entity);
            if ($report['success']) {
                $this->getRequest()->getSession()->write(
                    'Auth.menu',
                    $data['menu']
                );
            }
        }
        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Ajout par webservices
     * @return Response
     * @throws Exception
     */
    protected function apiAdd()
    {
        $request = $this->getRequest();
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $password = $request->getData('agent_type') === 'software'
            ? $request->getData('password')
            : $Users->generatePassword();
        $high_contrast = null;
        if ($request->getData('high_contrast')) {
            $high_contrast = $request->getData('high_contrast') === 'true';
        }
        $active = null;
        if ($request->getData('active')) {
            $active = $request->getData('active') === 'true';
        }
        $is_validator = null;
        if ($request->getData('is_validator')) {
            $is_validator = $request->getData('is_validator') === 'true';
        }
        $use_cert = null;
        if ($request->getData('use_cert')) {
            $use_cert = $request->getData('use_cert') === 'true';
        }
        $entity = $Users->newEntity(
            [
                'org_entity_id' => $request->getData('org_entity_id'),
                'agent_type' => 'person',
                'role_id' => $request->getData('role_id'),
                'username' => $request->getData('username'),
                'password' => $password,
                'confirm-password' => $password,
                'name' => $request->getData('name'),
                'email' => $request->getData('email'),
                'high_contrast' => $high_contrast,
                'active' => $active,
                'is_validator' => $is_validator,
                'use_cert' => $use_cert,
                'ldap_id' => $request->getData('ldap_id'),
                'ldap_login' => $request->getData('ldap_login'),
            ]
        );
        if ($Users->save($entity)) {
            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logAdd($entity);
            $json = $entity->toArray();
            /** @use EmailsComponent */
            $this->loadComponent('Emails');
            $this->Emails->submitEmailNewUser($entity);
            return $this->renderData(
                ['success' => true, 'id' => $entity->id]
                + (isset($json['code']) ? ['code' => $json['code']] : [])
            );
        } else {
            return $this->renderData(
                ['success' => false, 'errors' => $entity->getErrors()]
            );
        }
    }

    /**
     * Permet de reconstruire la session à la demande d'un administrateur
     * ex : Les permissions ont changés
     */
    public function resetSession()
    {
        $request = $this->getRequest();
        $request->allowMethod('ajax');
        $request->allowMethod('post');
        // la modification aura lieu à la prochaine requete
        $this->loadComponent('AsalaeCore.Modal');

        $data = [
            'user_id' => $this->userId,
            'message' => $request->getData('message'),
            'super_archivist' => $this->superArchivist,
            'last_archival_agency_id' => $request->getData('last_archival_agency_id'),
        ];

        $this->Modal->success();
        /** @var CookieComponent $Cookie */
        /** @use ModalComponent */
            $Cookie = $this->loadComponent('AsalaeCore.Cookie');
        $Cookie->configKey('resetSession', ['encryption' => 'aes']);
        $Cookie->write('resetSession', json_encode($data));
        return $this->renderData(
            [
                'css_class' => 'alert alert-info',
                'text' => __(
                    "Votre session a été réinitialisée par un administrateur. 
                    Vos permissions ont potentiellement changé."
                ),
                'id' => 'instant-message',
            ]
        );
    }

    /**
     * Réinitialisation du mot de passe par url
     * @param string $id
     * @return Response|null
     * @throws Exception
     */
    public function forgottenPasswordMail(string $id)
    {
        return $this->initializePassword($id);
    }

    /**
     * Initialisation du mot de passe par url
     * @param string $id
     * @return Response|null|void
     * @throws Exception
     */
    public function initializePassword(string $id)
    {
        $this->viewBuilder()->setLayout('not_connected');
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $this->fetchTable('AuthUrls');
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        if (!$AuthUrls->isAuthorized($this->getRequest())) {
            $this->set('notAuthorized', true);
            return;
        }
        $this->set('code', $AuthUrls->getCode($this->getRequest()));
        $user = $Users->find()
            ->contain(['OrgEntities' => ['TypeEntities'], 'Roles'])
            ->where(
                [
                    'Users.id' => $id,
                    'Users.active IS' => true,
                ]
            )
            ->contain(
                [
                    'OrgEntities' => [
                        'ArchivalAgencies' => ['Configurations'],
                        'TypeEntities',
                    ],
                    'Ldaps',
                    'Roles',
                ]
            )
            ->firstOrFail();
        $this->set('user', $user);

        if ($this->getRequest()->is('put')) {
            $data = [
                'password' => $this->getRequest()->getData('password'),
                'confirm-password' => $this->getRequest()->getData('confirm-password'),
            ];
            $Users->patchEntity($user, $data);
            if ($Users->save($user)) {
                $AuthUrls->consume($this->getRequest());
                $this->destroySession();
                /** @use LoginComponent */
                $this->loadComponent('Login');
                $this->Login->loadCookies($user);
                /** @var AuthenticationComponent $AuthenticationComponent */
                $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
                $AuthenticationComponent->setIdentity($user);
                $this->getRequest()->getSession()->write('Auth', $user);
                /** @var VersionsTable $Versions */
                $Versions = $this->fetchTable('Versions');
                /** @var EventLogsTable $EventLogs */
                $EventLogs = $this->fetchTable('EventLogs');
                $app = Configure::read('App.name');
                $msg = __(
                    "L'utilisateur ''{0}''",
                    h(Hash::get($user, 'username'))
                );
                $version = $Versions->find()
                    ->where(['subject' => $app])
                    ->orderBy(['id' => 'desc'])
                    ->firstOrFail();

                $entry = $EventLogs->newEntry(
                    'forgottenPassword',
                    'success',
                    __(
                        "Réinitialisation du mot de passe de {0} pour {1}",
                        $msg,
                        $app
                    ),
                    $this->userId,
                    $version
                );
                $EventLogs->save($entry);
                return $this->redirect('/');
            }
        }
    }

    /**
     * Permet de redéfinir un mot de passe oublié
     * @return Response|null
     */
    public function forgottenPassword()
    {
        if ($this->getRequest()->is('post')) {
            $form = new ForgottenPasswordForm();
            $result = $form->execute($this->getRequest()->getData());
            $code = $form->code;
            if ($code) {
                /** @var VersionsTable $Versions */
                $Versions = $this->fetchTable('Versions');
                /** @var EventLogsTable $EventLogs */
                $EventLogs = $this->fetchTable('EventLogs');
                /** @var UsersTable $Users */
                $Users = $this->fetchTable('Users');
                $app = Configure::read('App.name');

                $user = $Users->find()
                    ->where(
                        [
                            'username' => $this->getRequest()->getData('username'),
                            'email' => $this->getRequest()->getData('email'),
                            'agent_type' => 'person',
                            'active IS' => true,
                        ]
                    )
                    ->firstOrFail();

                $msg = __("L'utilisateur ''{0}''", h($user->get('username')));
                $version = $Versions->find()
                    ->where(['subject' => $app])
                    ->orderBy(['id' => 'desc'])
                    ->firstOrFail();
                $entry = $EventLogs->newEntry(
                    'forgottenPassword',
                    'success',
                    __(
                        "Réinitialisation du mot de passe de {0} pour {1}",
                        $msg,
                        $app
                    ),
                    $user->get('id'),
                    $version
                );
                $EventLogs->save($entry);
                return $this->redirect('/auth-urls/activate/' . $code->get('code'));
            }
            $errors = $form->getErrors();
            $mailer = $form->mailer;
            if (($result && empty($errors)) || isset($errors['user']['notFound'])) {
                if ($mailer) {
                    /** @var MailsTable $Mails */
                    $Mails = $this->fetchTable('Mails');
                    $Mails->asyncMail($mailer, null);
                }
                $this->Flash->success(__("Si vos informations sont correctes, un e-mail a été envoyé"));
                return $this->redirect('/users/login');
            } else {
                $this->Flash->error($errors['username']['_empty'] ?? __("Erreur"));
            }
        }
    }

    /**
     * Donne les roles pour un org_entity_id donné
     * @param string $org_entity_id
     * @return Response
     * @throws Exception
     */
    public function getRolesOptions(string $org_entity_id)
    {
        $this->checkAccess($org_entity_id);
        return $this->renderDataToJson(
            $this->setAvailablesRoles($org_entity_id)
        );
    }

    /**
     * Donne les roles pour un org_entity_id donné
     * @param string $org_entity_id
     * @return Response
     * @throws Exception
     */
    public function getWsRolesOptions(string $org_entity_id)
    {
        $this->checkAccess($org_entity_id);
        return $this->renderDataToJson(
            $this->setAvailablesRoles($org_entity_id, 'software')
        );
    }

    /**
     * Visualisation d'un utilisateur
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $entity = $this->Seal->archivalAgencyChilds()
            ->table('Users')
            ->find('seal')
            ->where(['Users.id' => $id])
            ->contain(
                [
                    'Roles',
                    'Ldaps',
                    'OrgEntities',
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);
        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        if ($entity->get('agent_type') === 'software') {
            $EventLogger->logAction(
                $entity,
                [
                    'objectName' => 'Webservice',
                    'objectLiteral' => __("Accès webservice"),
                ]
            );
        } else {
            $EventLogger->logAction($entity);
        }
    }

    /**
     * Interface pour modifier l'entité d'un utilisateur
     * @param string $user_id
     * @return CakeResponse|void
     * @throws Exception
     */
    public function changeEntity(string $user_id)
    {
        $user = $this->Seal->archivalAgencyChilds()
            ->table('Users')
            ->find('seal')
            ->where(['Users.id' => $user_id])
            ->contain(
                [
                    'Roles',
                    'Ldaps',
                    'OrgEntities',
                    'Transfers' => function (Query $q) {
                        return $q->where(
                            ['Transfers.state' => TransfersTable::S_PREPARATING]
                        );
                    },
                    'ValidationActors' => [
                        'ValidationStages' => [
                            'ValidationChains',
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        $this->set('user', $user);

        $form = new ChangeEntityForm();
        $form->setData(
            [
                'base_archival_agency_id' => $this->archivalAgencyId,
                'archival_agency_id' => $this->archivalAgencyId,
                'user_id' => $user_id,
            ]
        );
        /** @var ChangeEntityRequestsTable $ChangeEntityRequests */
        $ChangeEntityRequests = $this->fetchTable('ChangeEntityRequests');
        $existingLink = $ChangeEntityRequests->find()
            ->where(
                [
                    'user_id' => $user_id,
                    'base_archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->first();
        if ($existingLink) {
            $form->setData(
                [
                    'base_archival_agency_id' => $this->archivalAgencyId,
                    'user_id' => $user_id,
                    'archival_agency_id' => $existingLink->get('target_archival_agency_id'),
                    'reason' => $existingLink->get('reason'),
                ]
            );
        }
        $this->set('form', $form);

        $request = $this->getRequest();
        if ($request->is('post')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($request->getData() + $form->getData())) {
                /** @var UsersTable $Users */
                $Users = $this->fetchTable('Users');
                $user = $Users->find()
                    ->select(
                        [
                            'same_archival_agency' => new QueryExpression('ChangeEntityRequests.id IS NULL'),
                            'super_archivist' => new QueryExpression('SuperArchivistsArchivalAgencies.id IS NOT NULL'),
                        ]
                    )
                    ->enableAutoFields()
                    ->leftJoinWith('SuperArchivistsArchivalAgencies')
                    ->leftJoinWith(
                        'ChangeEntityRequests',
                        function (Query $q) {
                            return $q->where(
                                ['ChangeEntityRequests.target_archival_agency_id' => $this->archivalAgencyId]
                            );
                        }
                    )
                    ->where(['Users.id' => $user_id])
                    ->contain(
                        [
                            'Ldaps',
                            'OrgEntities',
                            'Roles',
                        ]
                    )
                    ->firstOrFail()
                    ->setVirtual(['deletable', 'is_linked_to_ldap']);
                $this->Modal->success();
                /** @var EventLogsTable $EventLogs */
                $EventLogs = $this->fetchTable('EventLogs');
                $entry = $EventLogs->newEntry(
                    'changeEntity_user',
                    'success',
                    __(
                        "Modification de l'entité de l'utilisateur ({0})",
                        $user->get('username')
                    ),
                    $this->userId,
                    $user
                );
                $EventLogs->saveOrFail($entry);
                return $this->renderDataToJson($user);
            } else {
                $this->Modal->fail();
                FormatError::logFormErrors($form);
            }
        }

        // options
        $vField = fn(EntityInterface $e) => sprintf('%s - %s', $e->get('identifier'), $e->get('name'));
        $archivalAgencies = $this->fetchTable('OrgEntities')
            ->find('list', valueField: $vField)
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'TypeEntities.code' => 'SA',
                    'OrgEntities.active' => true,
                ]
            )
            ->orderByAsc('OrgEntities.name')
            ->toArray();
        $this->set('archivalAgencies', $archivalAgencies);

        $orgEntities = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('sealList', valueField: $vField)
            ->where(['OrgEntities.id !=' => $user->get('org_entity_id')])
            ->orderByAsc('OrgEntities.name')
            ->toArray();
        $this->set('orgEntities', $orgEntities);

        $roles = $this->fetchTable('Roles')->find('list')
            ->where(
                [
                    'OR' => [
                        'org_entity_id IS' => null,
                        'org_entity_id' => $this->archivalAgencyId,
                    ],
                    'agent_type' => $user->get('agent_type'),
                ]
            )
            ->orderByAsc('name')
            ->toArray();
        $this->set('roles', $roles);

        $query = $this->fetchTable('OrgEntities')->find()
            ->where(
                [
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            )
            ->contain(['TypeEntities' => ['Roles' => ['sort' => 'name']]]);
        $availablesRolesByEntities = [];
        /** @var EntityInterface $entity */
        foreach ($query as $entity) {
            $availablesRolesByEntities[$entity->id] = [];
            /** @var EntityInterface $role */
            foreach (Hash::get($entity, 'type_entity.roles') as $role) {
                $availablesRolesByEntities[$entity->id][] = $role->id;
            }
        }
        $this->set('availablesRolesByEntities', $availablesRolesByEntities);
    }

    /**
     * Permet d'accepter un utilisateur dans son service d'archives et lui
     * attribuer une entité
     * @param string $user_id
     * @return CakeResponse|void
     * @throws Exception
     */
    public function acceptUser(string $user_id)
    {
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $user = $Users->find()
            ->innerJoinWith(
                'ChangeEntityRequests',
                function (Query $q) {
                    return $q->where(
                        ['ChangeEntityRequests.target_archival_agency_id' => $this->archivalAgencyId]
                    );
                }
            )
            ->where(['Users.id' => $user_id])
            ->contain(
                [
                    'ChangeEntityRequests' => ['BaseArchivalAgencies'],
                    'OrgEntities',
                    'Roles',
                ]
            )
            ->firstOrFail();
        $this->set('user', $user);
        $this->set('entity', $user->get('change_entity_request'));

        $form = new ChangeEntityForm();
        $form->setData(
            [
                'base_archival_agency_id' => $this->archivalAgencyId,
                'archival_agency_id' => $this->archivalAgencyId,
                'user_id' => $user_id,
            ]
        );
        $this->set('form', $form);

        $request = $this->getRequest();
        if ($request->is('post')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($request->getData() + $form->getData())) {
                /** @var EventLogsTable $EventLogs */
                $EventLogs = $this->fetchTable('EventLogs');
                $entry = $EventLogs->newEntry(
                    'acceptUser_User',
                    'success',
                    __(
                        "Acceptation du changement d'entité de l'utilisateur ({0})",
                        $user->get('username')
                    ),
                    $this->userId,
                    $user
                );
                $EventLogs->saveOrFail($entry);
                $user = $Users->find()
                    ->where(['Users.id' => $user_id])
                    ->contain(
                        [
                            'Ldaps',
                            'OrgEntities',
                            'Roles',
                        ]
                    )
                    ->firstOrFail()
                    ->setVirtual(['deletable', 'is_linked_to_ldap']);
                $this->Modal->success();
                return $this->renderDataToJson($user);
            } else {
                $this->Modal->fail();
                FormatError::logFormErrors($form);
            }
        }

        // options
        $vField = fn(EntityInterface $e) => sprintf('%s - %s', $e->get('identifier'), $e->get('name'));
        $orgEntities = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('sealList', valueField: $vField)
            ->orderByAsc('OrgEntities.name')
            ->toArray();
        $this->set('orgEntities', $orgEntities);

        $roles = $this->fetchTable('Roles')->find('list')
            ->where(
                [
                    'OR' => [
                        'org_entity_id IS' => null,
                        'org_entity_id' => $this->archivalAgencyId,
                    ],
                    'agent_type' => $user->get('agent_type'),
                    'code NOT IN' => RolesTable::SPECIAL_ROLE_CODES,
                ]
            )
            ->orderByAsc('name')
            ->toArray();
        $this->set('roles', $roles);

        $query = $this->fetchTable('OrgEntities')->find()
            ->where(
                [
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            )
            ->contain(['TypeEntities' => ['Roles' => ['sort' => 'name']]]);
        $availablesRolesByEntities = [];
        /** @var EntityInterface $entity */
        foreach ($query as $entity) {
            $availablesRolesByEntities[$entity->id] = [];
            /** @var EntityInterface $role */
            foreach (Hash::get($entity, 'type_entity.roles') as $role) {
                $availablesRolesByEntities[$entity->id][] = $role->id;
            }
        }
        $this->set('availablesRolesByEntities', $availablesRolesByEntities);
    }

    /**
     * Refus d'un utilisateur venant d'un autre service d'archive
     * @param string $user_id
     * @return \AsalaeCore\Http\Response|CakeResponse
     */
    public function refuseUser(string $user_id)
    {
        $this->getRequest()->allowMethod('delete');

        /** @var ChangeEntityRequestsTable $ChangeEntityRequests */
        $ChangeEntityRequests = $this->fetchTable('ChangeEntityRequests');
        $changeEntityRequest = $ChangeEntityRequests->find()
            ->where(
                [
                    'user_id' => $user_id,
                    'target_archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Users'])
            ->firstOrFail();
        $ChangeEntityRequests->deleteOrFail($changeEntityRequest);

        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        $entry = $EventLogs->newEntry(
            'refuseUser_User',
            'success',
            __(
                "Refus du changement d'entité de l'utilisateur ({0})",
                Hash::get($changeEntityRequest, 'user.username')
            ),
            $this->userId,
            $changeEntityRequest->get('user')
        );
        $EventLogs->saveOrFail($entry);

        return $this->renderDataToJson(['report' => "done"]);
    }

    /**
     * Se désabonner de notifications
     * @param string $id
     * @param string $type validation|job|agreement|transfer_report
     * @return void
     * @throws Exception
     */
    public function unsubscribe(string $id, string $type)
    {
        $this->viewBuilder()->setLayout('not_connected');
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $this->fetchTable('AuthUrls');
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        if (!$AuthUrls->isAuthorized($this->getRequest())) {
            $this->set('notAuthorized', true);
            return;
        }
        $user = $Users->find()
            ->where(['Users.id' => $id])
            ->firstOrFail();

        $user->set(sprintf('notify_%s_frequency', $type));
        $Users->saveOrFail($user);
        $AuthUrls->consume($this->getRequest());
        $this->set('type', $type);
    }
}
