<?php

/**
 * Asalae\Controller\ArchiveBinariesController
 */

namespace Asalae\Controller;

use Asalae\Model\Entity\ArchiveBinary;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\EventLogsTable;
use AsalaeCore\Http\CallbackStream;
use AsalaeCore\Utility\Notify;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\HttpException;
use Cake\Http\Response;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Exception;

/**
 * ArchiveBinaries
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveBinariesTable ArchiveBinaries
 * @property ArchivesTable        Archives
 * @property EventLogsTable       EventLogs
 */
class ArchiveBinariesController extends AppController
{
    /**
     * Télécharge le fichier derière un archive_binary
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function download(string $id)
    {
        /** @var ArchiveBinary $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('ArchiveBinaries')
            ->find('seal')
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.Archives')
            ->where(['ArchiveBinaries.id' => $id])
            ->contain(
                [
                    'StoredFiles',
                    'ArchiveUnits' => [
                        'Archives' => [
                            'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        ],
                    ],
                ]
            )
            ->firstOrFail();

        // TODO remplacer par seal (as2.2)
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $archive_id = Hash::get($entity, 'archive_units.0.archive_id');
        $query = $Archives->find()
            ->innerJoinWith('ArchivalAgencies')
            ->innerJoinWith('TransferringAgencies')
            ->innerJoinWith('OriginatingAgencies')
            ->where(['Archives.id' => $archive_id]);
        $sa = $this->archivalAgency;
        $lft = Hash::get($this->orgEntity, 'lft');
        $rght = Hash::get($this->orgEntity, 'rght');
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (in_array($typeEntity, ['SA', 'CST'])) {
            $lft = $sa->get('lft');
            $rght = $sa->get('rght');
        }
        if (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->andWhere(
                [
                    'OR' => [
                        [
                            'ArchivalAgencies.lft >=' => $lft,
                            'ArchivalAgencies.rght <=' => $rght,
                        ],
                        [
                            'TransferringAgencies.lft >=' => $lft,
                            'TransferringAgencies.rght <=' => $rght,
                        ],
                        [
                            'OriginatingAgencies.lft >=' => $lft,
                            'OriginatingAgencies.rght <=' => $rght,
                        ],
                    ],
                ]
            );
        } else {
            $query->andWhere(
                [
                    $this->orgEntityId . ' IN' => [
                        new IdentifierExpression('ArchivalAgencies.id'),
                        new IdentifierExpression('TransferringAgencies.id'),
                        new IdentifierExpression('OriginatingAgencies.id'),
                    ],
                ]
            );
        }
        $query->firstOrFail();
        // FIN TODO

        if ($this->getRequest()->is('head')) {
            return $this->getResponse()
                ->withHeader(
                    'Content-Type',
                    $entity->get('mime') ?: 'application/octet-stream'
                )
                ->withHeader(
                    'Content-Length',
                    Hash::get($entity, 'stored_file.size')
                )
                ->withHeader(
                    'File-Size',
                    Hash::get($entity, 'stored_file.size')
                );
        }
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');

        if ($this->getRequest()->getQuery('action') === 'open') {
            $action = 'open';
            $msg = __(
                "Visualisation du fichier ''{0}'' par l'utilisateur ''{1}''",
                $entity->get('filename'),
                Hash::get($this->user, 'name')
                    ?: Hash::get(
                        $this->user,
                        'username'
                    )
            );
        } else {
            $action = 'download';
            $msg = __(
                "Téléchargement du fichier ''{0}'' par l'utilisateur ''{1}''",
                $entity->get('filename'),
                Hash::get($this->user, 'name')
                    ?: Hash::get(
                        $this->user,
                        'username'
                    )
            );
        }
        $entry = $EventLogs->newEntry(
            $action . '_' . Inflector::singularize($this->getName()),
            'success',
            $msg,
            $this->userId,
            $entity
        );
        $entry->set('in_lifecycle', false);
        $EventLogs->saveOrFail($entry);
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        /** @var EntityInterface $archive */
        $storedFile = Hash::get(
            $entity,
            'archive_units.0.archive.lifecycle_xml_archive_file.stored_file',
            []
        );
        if (!$Archives->updateLifecycleXml($storedFile, $entry, true)) {
            throw new HttpException(__("Echec lors de la mise à jour du cycle de vie"), 500);
        }

        if ($sessionFile = $this->getRequest()->getQuery('sessionFile')) {
            Notify::emit(
                'download_file',
                [
                    'uuid' => $sessionFile,
                    'state' => 'downloading',
                    'last_update' => time(),
                ]
            );
        }

        return $this->getResponse()
            ->withHeader(
                'Content-Type',
                $entity->get('mime') ?: 'application/octet-stream'
            )
            ->withHeader(
                'Content-Length',
                Hash::get($entity, 'stored_file.size')
            )
            ->withHeader(
                'File-Size',
                Hash::get($entity, 'stored_file.size')
            )
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate')
            ->withHeader('Pragma', 'public')
            ->withBody(
                new CallbackStream(
                    function () use ($entity, $sessionFile) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        $success = $entity->readfile();
                        if ($sessionFile) {
                            Notify::emit(
                                'download_file',
                                [
                                    'uuid' => $sessionFile,
                                    'state' => 'finished',
                                    'last_update' => time(),
                                    'integrity' => $success,
                                ]
                            );
                        }
                    }
                )
            );
    }

    /**
     * Ouvre une visionneuse pour le document en question
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function open(string $id)
    {
        /** @uses ReaderComponent */
        $this->loadComponent('AsalaeCore.Reader');
        $file = $this->Seal->archivalAgency()
            ->table('ArchiveBinaries')
            ->find('seal')
            ->where(['ArchiveBinaries.id' => $id])
            ->firstOrFail();
        if (!$this->Reader->canRead($file->get('mime'))) {
            throw new BadRequestException();
        }

        $reader = $this->Reader->get($file->get('mime'));
        $reader->setDownloadUrl(
            '/archive-binaries/download/' . $file->id . '/' . $file->get('url_basename') . '?action=open'
        );
        return $reader->getResponse();
    }
}
