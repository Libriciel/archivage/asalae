<?php

/**
 * Asalae\Controller\MercureController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\MercureComponent;
use AsalaeCore\Controller\RenderDataTrait;
use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Event\EventInterface;
use Cake\Http\Response as CakeResponse;
use Exception;

/**
 * Permet de publier sur mercure par ajax
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2024, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MercureController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * Authorize publish (gestion des perms par mercure)
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse|void|null
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $AuthenticationComponent->addUnauthenticatedActions(
            [
                'publish',
            ]
        );
        return parent::beforeFilter($event);
    }

    /**
     * @return void
     */
    public function publish(): CakeResponse
    {
        $request = $this->getRequest();
        $request->allowMethod('post');

        /** @var MercureComponent $MercureComponent chargé dans AppController::initialize() */
        $MercureComponent = $this->components()->get('Mercure');
        $MercureComponent->publish(
            $request->getData('chanel'),
            $request->getData('data'),
        );
        return $this->renderDataToJson('done');
    }
}
