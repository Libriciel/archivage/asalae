<?php

/**
 * Asalae\Controller\DeliveriesController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Table\ArchiveUnitsDeliveryRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\DeliveriesTable;
use Asalae\Model\Table\DeliveryRequestsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Http\Response;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use Exception;
use Psr\Http\Message\ResponseInterface;

/**
 * Communications - Deliveries
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveUnitsDeliveryRequestsTable ArchiveUnitsDeliveryRequests
 * @property DeliveriesTable                   Deliveries
 * @property DeliveryRequestsTable             DeliveryRequests
 */
class DeliveriesController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     * @var array
     */
    public array $paginate = [
        'order' => [
            'Deliveries.id' => 'desc',
        ],
    ];

    /**
     * Récupération des communications
     * @throws Exception
     */
    public function retrieval()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        $conditions = [
            'Deliveries.state IN' => [
                DeliveriesTable::S_AVAILABLE,
                DeliveriesTable::S_DOWNLOADED,
            ],
        ];
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (
            $this->orgEntityId != $this->archivalAgencyId
            && !in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)
        ) {
            $conditions['DeliveryRequests.created_user_id'] = $this->userId;
        }
        $query = $this->Seal->archivalAgency()
            ->table('Deliveries')
            ->find('seal')
            ->innerJoinWith('DeliveryRequests')
            ->where($conditions)
            ->contain(['DeliveryRequests' => ['ArchiveUnits']]);

        $IndexComponent->setQuery($query)
            ->filter('name', IndexComponent::FILTER_ILIKE);

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/commons');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) use ($ArchiveUnits) {
                    // effectue un contain avec limite individuelle d'enregistrements
                    $aus = $ArchiveUnits->find()
                        ->innerJoinWith('ArchiveUnitsDeliveryRequests')
                        ->where(
                            [
                                'delivery_request_id' => $e->get(
                                    'delivery_request_id'
                                ),
                            ]
                        )
                        ->orderBy(['archival_agency_identifier'])
                        ->limit(10)
                        ->toArray();
                    /** @var EntityInterface $dRequests */
                    $dRequests = $e->get('delivery_request');
                    $dRequests->set('archive_units', $aus);
                    return $e;
                }
            )
            ->toArray();
        $this->set('data', $data);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex(
            [
                'typeName' => "RetrievableDeliveries",
                'indexName' => __("Récupération des communications"),
            ]
        );
    }

    /**
     * Acquittement des communications
     * @throws Exception
     */
    public function acquittal()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        $conditions = [
            'Deliveries.state' => DeliveriesTable::S_DOWNLOADED,
        ];
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (
            $this->orgEntityId != $this->archivalAgencyId
            && !in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT)
        ) {
            $conditions['DeliveryRequests.created_user_id'] = $this->userId;
        }
        $query = $this->Seal->archivalAgency()
            ->table('Deliveries')
            ->find('seal')
            ->innerJoinWith('DeliveryRequests')
            ->where($conditions)
            ->contain(['DeliveryRequests' => ['ArchiveUnits']]);

        $IndexComponent->setQuery($query)
            ->filter('name', IndexComponent::FILTER_ILIKE);

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/commons');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) use ($ArchiveUnits) {
                    // effectue un contain avec limite individuelle d'enregistrements
                    $aus = $ArchiveUnits->find()
                        ->innerJoinWith('ArchiveUnitsDeliveryRequests')
                        ->innerJoinWith(
                            'ArchiveUnitsDeliveryRequests.DeliveryRequests'
                        )
                        ->innerJoinWith(
                            'ArchiveUnitsDeliveryRequests.DeliveryRequests.Deliveries'
                        )
                        ->where(['Deliveries.id' => $e->id])
                        ->orderBy(['archival_agency_identifier'])
                        ->limit(10)
                        ->toArray();
                    /** @var EntityInterface $dRequests */
                    $dRequests = $e->get('delivery_request');
                    $dRequests->set('archive_units', $aus);
                    return $e;
                }
            )
            ->toArray();
        $this->set('data', $data);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex(
            [
                'typeName' => "AcquittableDeliveries",
                'indexName' => __("Acquittements des communications"),
            ]
        );
    }

    /**
     * Action visualiser
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $entity = $this->Seal->archivalAgency()
            ->table('Deliveries')
            ->find('seal')
            ->where(['Deliveries.id' => $id])
            ->contain(
                [
                    'DeliveryRequests' => [
                        'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                            return $q
                                ->orderBy(
                                    ['ArchiveUnits.archival_agency_identifier']
                                )
                                ->limit(
                                    $AjaxPaginatorComponent->getConfig('limit')
                                )
                                ->contain(
                                    [
                                        'Archives' => ['DescriptionXmlArchiveFiles'],
                                        'ArchiveDescriptions',
                                    ]
                                );
                        },
                        'CreatedUsers' => [
                            'OrgEntities',
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        /** @var EntityInterface[] $archive_units */
        $archive_units = Hash::get($entity, 'delivery_request.archive_units');
        foreach ($archive_units as $archiveUnit) {
            $archiveUnit->setVirtual(['description', 'dates']);
        }
        $this->set('entity', $entity);

        /** @var ArchiveUnitsDeliveryRequestsTable $ArchiveUnitsDeliveryRequests */
        $ArchiveUnitsDeliveryRequests = $this->fetchTable('ArchiveUnitsDeliveryRequests');
        $unitsCount = $ArchiveUnitsDeliveryRequests->find()
            ->where(
                ['delivery_request_id' => $entity->get('delivery_request_id')]
            )
            ->count();
        $this->set('unitsCount', $unitsCount);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($entity);
        $AjaxPaginatorComponent->setViewPaginator(
            Hash::get($entity, 'delivery_request.archive_units', []) ?: [],
            $unitsCount
        );
    }

    /**
     * Pagination des archive units dans la visualisation
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function paginateArchiveUnits(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $query = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal')
            ->where(['Deliveries.id' => $id])
            ->innerJoinWith('ArchiveUnitsDeliveryRequests')
            ->innerJoinWith('ArchiveUnitsDeliveryRequests.DeliveryRequests')
            ->innerJoinWith(
                'ArchiveUnitsDeliveryRequests.DeliveryRequests.Deliveries'
            )
            ->innerJoinWith('Archives')
            ->where(
                [
                    'Deliveries.id' => $id,
                    'ArchiveUnitsDeliveryRequests.archive_unit_id'
                    => new IdentifierExpression('ArchiveUnits.id'),
                ]
            )
            ->contain(['ArchiveDescriptions']);
        return $AjaxPaginatorComponent->json(
            $query,
            (bool)$this->getRequest()->getQuery('count')
        );
    }

    /**
     * Permet de télécharger le zip
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function download(string $id)
    {
        $delivery = $this->Seal->archivalAgency()
            ->table('Deliveries')
            ->find('seal')
            ->where(['Deliveries.id' => $id])
            ->contain(['DeliveryRequests'])
            ->firstOrFail();
        $zip = $delivery->get('zip');
        if (
            $this->Deliveries->transition(
                $delivery,
                DeliveriesTable::T_DOWNLOAD
            )
        ) {
            $deliveryRequest = $delivery->get('delivery_request');
            /** @var DeliveryRequestsTable $DeliveryRequests */
            $DeliveryRequests = $this->fetchTable('DeliveryRequests');
            $DeliveryRequests->transition(
                $deliveryRequest,
                DeliveryRequestsTable::T_DOWNLOAD_DELIVERY
            );
            $DeliveryRequests->saveOrFail($deliveryRequest);
            $this->Deliveries->saveOrFail($delivery);
        }

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($delivery);

        return $this->getCoreResponse()
            ->withFileStream($zip, ['mime' => 'application/zip']);
    }

    /**
     * acquitter une communication
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function acquit(string $id)
    {
        $delivery = $this->Seal->archivalAgency()
            ->table('Deliveries')
            ->find('seal')
            ->where(['Deliveries.id' => $id])
            ->contain(['DeliveryRequests'])
            ->firstOrFail();

        if ($delivery->get('state') === DeliveriesTable::S_DOWNLOADED) {
            $deliveryRequest = $delivery->get('delivery_request');
            /** @var DeliveryRequestsTable $DeliveryRequests */
            $DeliveryRequests = $this->fetchTable('DeliveryRequests');
            $DeliveryRequests->transition(
                $deliveryRequest,
                DeliveryRequestsTable::T_ACQUIT_DELIVERY
            );
            $DeliveryRequests->saveOrFail($deliveryRequest);
            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logAction($delivery);
            $EventLogger->logAction($delivery, ['actionName' => 'delete']);
            $report = $this->Deliveries->delete($delivery)
                ? 'done'
                : 'Erreur lors de la suppression';
        } else {
            throw new ForbiddenException();
        }
        return $this->renderDataToJson(['report' => $report]);
    }
}
