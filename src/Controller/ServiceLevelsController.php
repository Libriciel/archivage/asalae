<?php

/**
 * Asalae\Controller\ServiceLevelsController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Table\OrgEntitiesTimestampersTable;
use Asalae\Model\Table\SecureDataSpacesTable;
use Asalae\Model\Table\ServiceLevelsTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Response;
use Exception;

/**
 * Liste niveaux de service
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ServiceLevelsTable $ServiceLevels
 * @property OrgEntitiesTimestampersTable $OrgEntitiesTimestampers
 * @property SecureDataSpacesTable $SecureDataSpaces
 */
class ServiceLevelsController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'service-levels-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'ServiceLevels.id' => 'asc',
        ],
    ];

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default',
        ];
    }

    /**
     * Liste des niveaux de service
     */
    public function index()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        $query = $this->Seal->archivalAgency()
            ->table('ServiceLevels')
            ->find('seal')
            ->contain(
                [
                    'SecureDataSpaces',
                    'TsMsgs' => ['Timestampers'],
                    'TsPjss' => ['Timestampers'],
                    'TsConvs' => ['Timestampers'],
                ]
            );

        $IndexComponent->setQuery($query)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('active', IndexComponent::FILTER_IS)
            ->filter('default_level', IndexComponent::FILTER_IS)
            ->filter('secure_data_space_id')
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('modified', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();

        // options
        $secureDataSpaces = $this->Seal->archivalAgency()
            ->table('SecureDataSpaces')
            ->find('sealList')
            ->innerJoinWith('Volumes')
            ->orderBy('SecureDataSpaces.name')
            ->toArray();
        $this->set('secureDataSpaces', $secureDataSpaces);
    }

    /**
     * Partie 1 de l'ajout d'un niveau de service
     * @throws Exception
     */
    public function add1()
    {
        /** @var ServiceLevelsTable $ServiceLevels */
        $ServiceLevels = $this->fetchTable('ServiceLevels');
        $entity = $ServiceLevels->newEntity(
            [
                'active' => true,
                'org_entity_id' => $this->archivalAgencyId,
            ],
            ['validate' => false]
        );
        if ($this->getRequest()->is('post')) {
            $data = [
                'default_level' => $this->getRequest()->getData(
                    'default_level'
                ),
                'org_entity_id' => $entity->get('org_entity_id'),
                'identifier' => $this->getRequest()->getData('identifier'),
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'commitment' => $this->getRequest()->getData('commitment'),
                'secure_data_space_id' => $this->getRequest()->getData(
                    'secure_data_space_id'
                ),
                'active' => true,
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $ServiceLevels->patchEntity($entity, $data);
            $this->getRequest()->getSession()->write(
                'ServiceLevel.entity',
                $entity
            );
            $this->Modal->success(empty($entity->getErrors()));
            $this->Modal->step('addServiceLevelStep2()');
        }
        $this->set('entity', $entity);

        $secureDataSpaces = $this->Seal->archivalAgency()
            ->table('SecureDataSpaces')
            ->find('sealList')
            ->innerJoinWith('Volumes')
            ->orderBy('SecureDataSpaces.name')
            ->toArray();
        $this->set('secure_data_spaces', $secureDataSpaces);
    }

    /**
     * Partie 2 de l'ajout d'un niveau de service
     */
    public function add2()
    {
        /** @var EntityInterface $entity */
        $entity = $this->getRequest()->getSession()->read(
            'ServiceLevel.entity'
        );
        if (empty($entity)) { // en cas de deconnexion/reconnexion
            /** @var ServiceLevelsTable $ServiceLevels */
            $ServiceLevels = $this->fetchTable('ServiceLevels');
            $entity = $ServiceLevels->newEntity(
                [
                    'active' => true,
                    'default_level' => $this->getRequest()->getData(
                        'default_level'
                    ),
                    'org_entity_id' => $this->archivalAgencyId,
                    'identifier' => $this->getRequest()->getData('identifier'),
                    'name' => $this->getRequest()->getData('name'),
                    'description' => $this->getRequest()->getData(
                        'description'
                    ),
                ]
            );
        } else {
            $entity->setNew(true);
        }

        if ($this->getRequest()->is('post')) {
            $data = [
                'ts_msg_id' => $this->getRequest()->getData('ts_msg_id'),
                'ts_pjs_id' => $this->getRequest()->getData('ts_pjs_id'),
                'ts_conv_id' => $this->getRequest()->getData('ts_conv_id'),
            ] + $entity->toArray();
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            /** @var ServiceLevelsTable $ServiceLevels */
            $ServiceLevels = $this->fetchTable('ServiceLevels');
            $ServiceLevels->patchEntity($entity, $data);
            $this->getRequest()->getSession()->write(
                'ServiceLevel.entity',
                $entity
            );
            $this->Modal->success(empty($entity->getErrors()));
            $this->Modal->step('addServiceLevelStep3()');
        }
        $this->set('entity', $entity);
        $this->setTimestampers();
    }

    /**
     * Options pour les services d'horodatage (par lien avec la relation entité->horodateur)
     */
    private function setTimestampers()
    {
        $timestampers = [];
        $query = $this->Seal->archivalAgency()
            ->table('OrgEntitiesTimestampers')
            ->find('seal')
            ->select(
                [
                    'OrgEntitiesTimestampers.id',
                    'Timestampers.name',
                ]
            )
            ->innerJoinWith('Timestampers');
        /** @var EntityInterface $entityTs */
        foreach ($query as $entityTs) {
            /** @var EntityInterface $timestamper */
            $timestamper = $entityTs->get('_matchingData')['Timestampers'];
            $timestampers[$entityTs->get('id')] = $timestamper->get('name');
        }
        $this->set('timestampers', $timestampers);
    }

    /**
     * Partie 3 de l'ajout d'un niveau de service
     */
    public function add3()
    {
        $request = $this->getRequest();
        /** @var EntityInterface $entity */
        $entity = $request->getSession()->read('ServiceLevel.entity');
        if (empty($entity)) { // en cas de deconnexion/reconnexion
            /** @var ServiceLevelsTable $ServiceLevels */
            $ServiceLevels = $this->fetchTable('ServiceLevels');
            $entity = $ServiceLevels->newEntity(
                [
                    'active' => true,
                    'default_level' => $request->getData('default_level'),
                    'org_entity_id' => $this->archivalAgencyId,
                    'identifier' => $request->getData('identifier'),
                    'name' => $request->getData('name'),
                    'description' => $request->getData('description'),
                    'ts_msg_id' => $request->getData('ts_msg_id'),
                    'ts_pjs_id' => $request->getData('ts_pjs_id'),
                    'ts_conv_id' => $request->getData('ts_conv_id'),
                ]
            );
        } else {
            $entity->setNew(true);
        }

        if ($request->is('post')) {
            $entity->set(
                'convert_preservation',
                $request->getData('convert_preservation')
            );
            $entity->set(
                'convert_dissemination',
                $request->getData('convert_dissemination')
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            /** @var ServiceLevelsTable $ServiceLevels */
            $ServiceLevels = $this->fetchTable('ServiceLevels');
            if ($ServiceLevels->save($entity)) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAdd($entity);
                $this->getRequest()->getSession()->delete(
                    'ServiceLevel.entity'
                );
                $entity = $ServiceLevels->find()
                    ->where(['ServiceLevels.id' => $entity->get('id')])
                    ->contain(
                        [
                            'SecureDataSpaces',
                            'TsMsgs' => ['Timestampers'],
                            'TsPjss' => ['Timestampers'],
                            'TsConvs' => ['Timestampers'],
                        ]
                    )
                    ->firstOrFail();
                $this->getRequest()->getSession()->delete(
                    'ServiceLevel.entity'
                );
                $this->Modal->success();
                return $this->renderDataToJson($entity->toArray());
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
        /** @var ServiceLevelsTable $ServiceLevels */
        $ServiceLevels = $this->fetchTable('ServiceLevels');
        $this->set('formats', $ServiceLevels->options('formats'));
    }

    /**
     * Action de suppression d'un niveau de service
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Seal->archivalAgency()
            ->table('ServiceLevels')
            ->find('seal')
            ->where(['ServiceLevels.id' => $id])
            ->firstOrFail();

        if (!$entity->get('deletable')) {
            throw new ForbiddenException(
                __("Tentative de suppression d'un niveau de service protégée")
            );
        }

        /** @var ServiceLevelsTable $ServiceLevels */
        $ServiceLevels = $this->fetchTable('ServiceLevels');
        $conn = $ServiceLevels->getConnection();
        $conn->begin();
        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logDelete($entity);
        if ($ServiceLevels->delete($entity)) {
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Edition d'un niveau de service
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var EntityInterface $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('ServiceLevels')
            ->find('seal')
            ->where(['ServiceLevels.id' => $id])
            ->firstOrFail();

        if ($this->getRequest()->is('put')) {
            $data = [
                'active' => $this->getRequest()->getData('active'),
                'default_level' => $this->getRequest()->getData(
                    'default_level'
                ),
                'org_entity_id' => $this->archivalAgencyId,
                'identifier' => $this->getRequest()->getData('identifier'),
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'commitment' => $this->getRequest()->getData('commitment'),
                'ts_msg_id' => $this->getRequest()->getData('ts_msg_id'),
                'ts_pjs_id' => $this->getRequest()->getData('ts_pjs_id'),
                'ts_conv_id' => $this->getRequest()->getData('ts_conv_id'),
                'secure_data_space_id' => $this->getRequest()->getData(
                    'secure_data_space_id'
                ),
                'convert_preservation' => $this->getRequest()->getData(
                    'convert_preservation'
                ),
                'convert_dissemination' => $this->getRequest()->getData(
                    'convert_dissemination'
                ),
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            /** @var ServiceLevelsTable $ServiceLevels */
            $ServiceLevels = $this->fetchTable('ServiceLevels');
            $ServiceLevels->patchEntity($entity, $data);
            if ($ServiceLevels->save($entity)) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logEdit($entity);
                $entity = $ServiceLevels->find()
                    ->where(['ServiceLevels.id' => $entity->get('id')])
                    ->contain(
                        [
                            'SecureDataSpaces',
                            'TsMsgs' => ['Timestampers'],
                            'TsPjss' => ['Timestampers'],
                            'TsConvs' => ['Timestampers'],
                        ]
                    )
                    ->firstOrFail();
                $this->Modal->success();
                return $this->renderDataToJson($entity->toArray());
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
        $this->setTimestampers();

        $secureDataSpaces = $this->Seal->archivalAgency()
            ->table('SecureDataSpaces')
            ->find('sealList')
            ->innerJoinWith('Volumes')
            ->orderBy('SecureDataSpaces.name')
            ->toArray();
        $this->set('secure_data_spaces', $secureDataSpaces);
        /** @var ServiceLevelsTable $ServiceLevels */
        $ServiceLevels = $this->fetchTable('ServiceLevels');
        $this->set('formats', $ServiceLevels->options('formats'));
    }

    /**
     * Visualisation d'un niveau de service
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $entity = $this->Seal->archivalAgency()
            ->table('ServiceLevels')
            ->find('seal')
            ->where(['ServiceLevels.id' => $id])
            ->contain(
                [
                    'SecureDataSpaces',
                    'TsMsgs' => ['Timestampers'],
                    'TsPjss' => ['Timestampers'],
                    'TsConvs' => ['Timestampers'],
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($entity);
    }
}
