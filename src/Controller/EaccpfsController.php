<?php

/**
 * Asalae\Controller\EaccpfsController
 * @noinspection PhpDeprecationInspection pour XsdToArray - retirer après refonte
 */

namespace Asalae\Controller;

use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\FilterComponent;
use AsalaeCore\Controller\Component\JstableComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\I18n\Date as CoreDate;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\XmlUtility;
use AsalaeCore\Utility\XsdToArray;
use Asalae\Model\Entity\Eaccpf;
use Asalae\Model\Entity\Fileupload;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Table\EaccpfsTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response as CakeResponse;
use Cake\Http\Response;
use Cake\I18n\Date as CakeDate;
use Cake\Utility\Hash;
use Exception;
use Throwable;
use ZipArchive;

/**
 * Gestion des notices d'autorités (norme EAC-CPF)
 *
 * @link http://eac.staatsbibliothek-berlin.de/
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property EaccpfsTable $Eaccpfs
 * @property FileuploadsTable $Fileuploads
 * @property OrgEntitiesTable $OrgEntities
 */
class EaccpfsController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'eaccpfs-table';
    public const string TABLE_UPLOAD = 'eaccpfs-uploads-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'Eaccpfs.id' => 'desc',
        ],
    ];

    /**
     * Liste les notices d'autorités
     *
     * @throws BadRequestException
     * @throws Exception
     * @deprecated
     */
    public function index()
    {
        /** @use JstableComponent */
        $this->loadComponent('AsalaeCore.Jstable');
        $this->Jstable->configurablePagination(self::TABLE_INDEX);
        $this->set('tableId', self::TABLE_INDEX);
        $this->set('uploadId', self::TABLE_UPLOAD);

        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = $this->fetchTable('Eaccpfs');

        $query = $Eaccpfs
            ->find();

        /** @use FilterComponent */
        $this->loadComponent('AsalaeCore.Filter');
        $this->Filter->loadAndSave();
        $params = $this->getRequest()->getParam('?') ?: [];

        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');

        // Filtre identifiant
        foreach ((array)Hash::get($params, 'record_id') as $value) {
            $query->where($this->Condition->ilike('Eaccpfs.record_id', $value));
        }
        // Filtre entity_type
        foreach ((array)Hash::get($params, 'entity_type') as $values) {
            $query->where($this->Condition->in('Eaccpfs.entity_type', $values));
        }
        // Filtre date
        foreach ((array)Hash::get($params, 'date') as $value) {
            $query->where(
                $this->Condition->dateBetweenFields(
                    $value,
                    'Eaccpfs.from_date',
                    'Eaccpfs.to_date'
                )
            );
        }
        // Filtre created
        foreach ((array)Hash::get($params, 'created') as $key => $value) {
            $operator = $this->getRequest()->getParam(
                "?.dateoperator_created.$key"
            );
            $query->where(
                $this->Condition->dateOperator(
                    'Eaccpfs.created',
                    $operator,
                    $value
                )
            );
        }
        // Filtre favoris
        if (Hash::get($params, 'favoris')) {
            $query->where($this->Condition->inFavorites('Eaccpfs.id'));
        }

        // Order by favorite
        if ($this->getRequest()->getQuery('sort') === 'favorite') {
            $dir = $this->getRequest()->getQuery('direction') ?: 'asc';
            $query->orderBy($this->Condition->orderByFavorites($dir));
        }

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);
        $this->set('entity_type', $Eaccpfs->options('entity_type'));

        if (
            $this->getRequest()->is('ajax') && $this->viewBuilder()->getLayout() === null
        ) {
            $this->viewBuilder()->setTemplate('ajax_' . __FUNCTION__);
        }
    }

    /**
     * Formulaire d'ajout d'un eaccpf
     * @param string $orgEntityId
     * @return Response|null
     * @throws Exception
     */
    public function add(string $orgEntityId)
    {
        $this->getRequest()->allowMethod('ajax');
        $this->viewBuilder()->setLayout('ajax');

        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = $this->fetchTable('Eaccpfs');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $query = $OrgEntities->find();
        $adminTech = $this->getRequest()->getSession()->read('Admin.tech');
        if (!$adminTech) {
            // On s'assure d'avoir des lft/rght à jour
            $sa = $OrgEntities->get($this->archivalAgencyId);
            $query->where(
                [
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                ]
            );
        }
        $orgEntity = $query
            ->where(['OrgEntities.id' => $orgEntityId])
            ->contain(
                [
                    'Eaccpfs' => [
                        'sort' => $order = [
                            'from_date' => 'desc',
                            'to_date' => 'desc',
                            'created' => 'desc',
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        /** @var null|OrgEntity $prevEntity */
        $prevEntity = null;
        if (isset($orgEntity->get('eaccpfs')[0])) {
            $prevEntity = current($orgEntity->get('eaccpfs'));
            $from = null;
            if ($prevEntity->get('to_date')) {
                $from = (string)CakeDate::parseDate($prevEntity->get('to_date'));
            } elseif ($this->getRequest()->getData('to_date')) {
                $from = (string)CakeDate::parseDate(
                    $this->getRequest()->getData('to_date')
                );
            }
            $data = [
                'record_id' => $orgEntity->get('identifier'),
                'name' => $orgEntity->get('name'),
                'entity_id' => $prevEntity->get('entity_id'),
                'entity_type' => $prevEntity->get('entity_type'),
                'from_date' => $from,
                'to_date' => null,
                'agency_name' => $prevEntity->get('agency_name'),
                'description' => $orgEntity->get('description'),
            ];
        } else {
            $data = [
                'record_id' => $orgEntity->get('identifier'),
                'name' => $orgEntity->get('name'),
                'agency_name' => $this->archivalAgency->get('name'),
                'description' => $orgEntity->get('description'),
            ];
        }

        /** @var Eaccpf $entity */
        $entity = $Eaccpfs->newEntity($data);
        $entity->set(
            'from_date',
            (string)($entity->get('from_date') ?: new CoreDate()),
            ['setter' => false]
        );
        $this->set('prevEntity', $prevEntity);

        if ($prevEntity && !$prevEntity->get('to_date')) {
            if ($this->getRequest()->is('put')) {
                $Eaccpfs->patchEntity(
                    $prevEntity,
                    [
                        'name' => $orgEntity->get('name'),
                        'record_id' => $orgEntity->get('identifier'),
                        'to_date' => $this->getRequest()->getData('to_date'),
                    ]
                );
                if (!$Eaccpfs->save($prevEntity)) {
                    $this->viewBuilder()->setTemplate('todate');
                }
                /** @use ModalComponent */
                $this->loadComponent('AsalaeCore.Modal');
                $this->Modal->fail();
            } else {
                $this->viewBuilder()->setTemplate('todate');
            }
        }

        if ($this->getRequest()->is('post')) {
            $entity = $Eaccpfs->newEntity([], ['validate' => false]);
            $data = [
                'record_id' => $orgEntity->get('identifier'),
                'name' => $orgEntity->get('name'),
                'agency_name' => $this->getRequest()->getData('agency_name'),
                'entity_id' => $this->getRequest()->getData('entity_id'),
                'entity_type' => $this->getRequest()->getData('entity_type'),
                'from_date' => $this->getRequest()->getData('from_date'),
                'to_date' => $this->getRequest()->getData('to_date'),
                'description' => $orgEntity->get('description'),
                'org_entity_id' => $orgEntityId,
            ];
            $Eaccpfs->patchEntity($entity, $data);
            $Eaccpfs->initializeData($entity, $this->user->toArray());
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($Eaccpfs->save($entity)) {
                $this->Modal->success();
                $eaccpfs = $Eaccpfs->find()
                    ->where(['org_entity_id' => $orgEntityId])
                    ->orderBy($order)
                    ->all();
                return $this->renderDataToJson($eaccpfs->toArray());
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);
        $this->set('entity_type', $Eaccpfs->options('entity_type'));

        $uploadId = 'eaccpf-upload-table';
        $this->set('uploadId', $uploadId);
        $this->set('orgEntityId', $orgEntityId);
    }

    /**
     * Ajoute une noticie d'autorité par uload d'un xml
     *
     * @param string $orgEntityId
     * @param string $id          upload_id
     * @return Response|null
     * @throws Exception
     */
    public function addByUpload(string $orgEntityId, string $id)
    {
        $this->getRequest()->allowMethod('ajax');
        $this->getRequest()->allowMethod('post');
        $this->viewBuilder()->setLayout('ajax');

        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = $this->fetchTable('Eaccpfs');
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = $this->fetchTable('Fileuploads');
        $orgEntity = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->where(['OrgEntities.id' => $orgEntityId])
            ->firstOrFail();

        /** @var Fileupload $file */
        $file = $Fileuploads->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        $eaccpf = $Eaccpfs->newEntityFromXml($file->path);
        $Eaccpfs->patchEntity(
            $eaccpf,
            [
                'record_id' => $orgEntity->get('identifier'),
                'org_entity_id' => $orgEntityId,
            ]
        );

        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        if ($Eaccpfs->save($eaccpf)) {
            $this->Modal->success();
            $eaccpfs = $Eaccpfs->find()
                ->where(['org_entity_id' => $orgEntityId])
                ->orderBy(
                    [
                        'from_date' => 'desc',
                        'to_date' => 'desc',
                        'created' => 'desc',
                    ]
                )
                ->all();
            return $this->renderDataToJson($eaccpfs->toArray());
        } else {
            $this->Modal->fail();
            $flattenErrors = FormatError::logEntityErrors($eaccpf, false);
            $report = [
                'id' => $id,
                'message' => $flattenErrors
                    ?: __("Une erreur inattendue a eu lieu"),
            ];
        }
        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Supprime un eaccpf
     *
     * @param string $id
     * @return CakeResponse
     */
    public function delete(string $id)
    {
        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = $this->fetchTable('Eaccpfs');

        $entity = $this->Seal->archivalAgencyChilds()
            ->table('Eaccpfs')
            ->find('seal')
            ->where(['Eaccpfs.id' => $id])
            ->firstOrFail();

        $report = $Eaccpfs->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression du eaccpf';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Edition d'un eaccpf
     *
     * @param string $id
     * @return Response|null
     * @throws Exception|Throwable
     */
    public function edit(string $id)
    {
        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = $this->fetchTable('Eaccpfs');
        $entity = $this->Seal->archivalAgencyChilds()
            ->table('Eaccpfs')
            ->find('seal')
            ->where(['Eaccpfs.id' => $id])
            ->firstOrFail();
        $errors = [];

        if ($this->getRequest()->is(['post', 'put'])) {
            $initialData = $entity->get('data');
            $data = Hash::filter(
                json_decode($this->getRequest()->getData('data'), true)
            );
            // force control.recordId === record_id
            $recordId =& $data['eac-cpf']['control']['recordId'];
            if (is_null($recordId) || is_string($recordId)) {
                $recordId = [];
            }
            $recordId['@'] = $entity->get('record_id');
            // force nameEntry.part === name
            $cpfDescription = $data['eac-cpf']['cpfDescription']
                ?? $data['eac-cpf']['multipleIdentities']['cpfDescription'][0];
            $part =& $cpfDescription['identity']['nameEntry']['part'];
            if (is_array($part) && isset($part[0])) {
                $part =& $part[0];
            }
            if (is_null($part) || is_string($part)) {
                $part = [];
            }
            $part['@'] = $entity->get('name');
            $xml = XmlUtility::jsonToXmlString(json_encode($data));
            $newEntity = $Eaccpfs->newEntityFromXml($xml);
            $patch = !empty($newEntity->getErrors()) ? $this->getRequest()
                ->getData() : $newEntity->toArray();
            $patch = [
                'org_entity_id' => $entity->get('org_entity_id'),
                'record_id' => $entity->get('record_id'),
                'name' => $entity->get('name'),
            ] + $patch;
            $Eaccpfs->patchEntity($entity, $patch);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($Eaccpfs->save($entity)) {
                if ($initialData !== $entity->get('data')) {
                    $user = $this->user;
                    $ref =& $data['eac-cpf']['control']['maintenanceHistory']['maintenanceEvent'];
                    if (!isset($ref[0])) {
                        $ref = [0 => $ref];
                    }
                    $ref[] = [
                        "eventType" => "derived",
                        "eventDateTime" => [
                            "@standardDateTime" => $h = date("Y-m-d\TH:i:s"),
                            "@" => $h,
                        ],
                        "agentType" => "human",
                        "agent" => !empty($user['name']) ? $user['name']
                            : $user['username'],
                        "eventDescription" => "Modified in Asalae",
                    ];
                    $entity->set('data', json_encode($data));
                    $Eaccpfs->saveOrFail($entity);
                }
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $entity->set('data', $this->getRequest()->getData('data'));
                $this->Modal->fail();
                $errors = array_merge(
                    Hash::flatten($entity->getErrors()),
                    $Eaccpfs->extractXsdValidationErrors(
                        $entity->get('data')
                    )
                );
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('xsd', XsdToArray::build(EAC_CPF_XSD));
        $this->set('entity', $entity);
        $this->set('errors', $errors);
        $this->set('entity_type', $Eaccpfs->options('entity_type'));
    }

    /**
     * Edition d'un eaccpf
     *
     * @param string $id
     * @return Response
     */
    public function download(string $id)
    {
        $entity = $this->Seal->archivalAgencyChilds()
            ->table('Eaccpfs')
            ->find('seal')
            ->where(['Eaccpfs.id' => $id])
            ->firstOrFail();
        $xml = $entity->get('XML');
        $body = $this->getResponse()->getBody();
        $body->write($xml);
        return $this->getResponse()->withHeader(
            'Content-Description',
            'File Transfer'
        )
            ->withHeader('Content-Type', 'application/octet-stream')
            ->withHeader(
                'Content-Disposition',
                'attachment; filename="' . $entity->get('record_id') . '.xml"'
            )
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate')
            ->withHeader('Pragma', 'public')
            ->withHeader('Content-Length', $size = strlen($xml))
            ->withHeader('File-Size', $size)
            ->withBody($body);
    }

    /**
     * Permet de télécharger un zip comprennant les notices selectionnés
     *
     * @param string $zipname
     * @return Response
     * @throws Exception
     */
    public function zip($zipname = 'eac-cpfs.zip'): CakeResponse
    {
        $ids = $this->getRequest()->getQuery('id');
        if (!$ids) {
            throw new NotFoundException(__("Notice d'autorité non trouvé"));
        }
        $cpfs = $this->Seal->archivalAgencyChilds()
            ->table('Eaccpfs')
            ->find('seal')
            ->where(['Eaccpfs.id IN' => $ids])
            ->orderBy(['Eaccpfs.id' => 'asc']);

        if ($cpfs->count() !== count($ids)) {
            throw new NotFoundException(__("Notice d'autorité non trouvé"));
        }

        $zip = new ZipArchive();
        $location = sys_get_temp_dir() . DS . uniqid('zip');

        if (!$zip->open($location, ZipArchive::CREATE)) {
            throw new Exception(__("Impossible de créer le zip"));
        }

        $i = 0;
        /** @var Eaccpf $cpf */
        foreach ($cpfs as $cpf) {
            $i++;
            if (
                !$zip->addFile(
                    $cpf->get('tmpFilename'),
                    $cpf->get('record_id') . '_' . $i . '.xml'
                )
            ) {
                throw new Exception(
                    __(
                        "Impossible d'ajouter {0} au fichier zip",
                        $cpf->get('record_id')
                    )
                );
            }
        }
        $zip->close();

        return $this->getCoreResponse()
            ->withFileStream(
                $location,
                ['delete' => true, 'mime' => 'application/zip', 'name' => $zipname]
            );
    }
}
