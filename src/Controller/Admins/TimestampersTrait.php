<?php

/**
 * Asalae\Controller\Admins\TimestampersTrait
 */

namespace Asalae\Controller\Admins;

use Asalae\Controller\AdminsController;
use Asalae\Model\Entity\Timestamper as TimestamperEntity;
use Asalae\Model\Table\TimestampersTable;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Driver\Timestamping\TimestampingLocal;
use AsalaeCore\Error\ErrorTrap;
use AsalaeCore\Form\TSACertForm;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\Timestamper;
use Cake\Core\Configure;
use Cake\Http\Response as CakeResponse;
use Cake\Utility\Hash;
use ErrorException;
use Exception;
use Throwable;

/**
 * Trait TimestampersTrait
 *
 * @category Controller\Admins
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AdminsController
 */
trait TimestampersTrait
{
    /**
     * Effectu un (new $driver($url))->check() sur un TimestampingInterface
     * @param string|null $timestamper_id
     * @return CakeResponse
     */
    public function testTimestamper(string $timestamper_id = null)
    {
        $this->getRequest()->allowMethod(['post', 'put']);
        /** @var TimestampersTable $Timestampers */
        $Timestampers = $this->fetchTable('Timestampers');
        if ($timestamper_id !== null) {
            $timestamper = $Timestampers->get($timestamper_id);
        } else {
            $timestamper = $Timestampers->newEntity(
                $this->getRequest()->getData(),
                ['validate' => false]
            );
        }
        $responseData = [];
        try {
            $util = new Timestamper($timestamper);
            $responseData['success'] = $util->check();
            $responseData['errors'] = $util->errors;
        } catch (Exception $e) {
            $responseData['success'] = false;
            $responseData['exception'] = $e->getMessage();
            if (Configure::read('debug')) {
                $responseData['file'] = $e->getFile();
                $responseData['line'] = $e->getLine();
            }
        }

        return $this->renderDataToJson($responseData);
    }

    /**
     * Formulaire d'ajout/modification des horodateurs
     */
    public function timestampers()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Timestampers');
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }
        $this->getRequest()->allowMethod('ajax');
        $this->set('tableId', self::TABLE_INDEX_TIMESTAMPERS);

        /** @var TimestampersTable $Timestampers */
        $Timestampers = $this->fetchTable('Timestampers');
        $timestampers = $Timestampers->find()
            ->orderBy(['name' => 'asc'])
            ->all();
        $this->set('timestampers', $timestampers);
    }

    /**
     * Ajout d'un horodateur
     */
    public function addTimestamper()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Timestampers');
        $this->getRequest()->allowMethod('ajax');
        /** @var TimestampersTable $Timestampers */
        $Timestampers = $this->fetchTable('Timestampers');
        $entity = $Timestampers->newEntity([], ['validate' => false]);

        if ($this->getRequest()->is('post')) {
            $Timestampers->patchEntity(
                $entity,
                $this->getRequest()->getData()
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');

            $success = $Timestampers->save($entity);
            $this->logEvent(
                'admintech_add_timestamper',
                $success ? 'success' : 'fail',
                __("Ajout d'un horodateur"),
                $entity
            );
            if ($success) {
                $this->Modal->success();
                return $this->renderDataToJson($entity);
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
        $this->set('sas', $this->optionsSa()->toArray());
    }

    /**
     * modification d'un horodateur
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function editTimestamper(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Timestampers');
        $this->getRequest()->allowMethod('ajax');
        /** @var TimestampersTable $Timestampers */
        $Timestampers = $this->fetchTable('Timestampers');
        $entity = $Timestampers->find()
            ->where(['Timestampers.id' => $id])
            ->contain(['OrgEntities'])
            ->firstOrFail();

        if ($this->getRequest()->is('put')) {
            $Timestampers->patchEntity(
                $entity,
                $this->getRequest()->getData()
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');

            $success = $Timestampers->save($entity);
            $this->logEvent(
                'admintech_edit_timestamper',
                $success ? 'success' : 'fail',
                __("Modification d'un horodateur"),
                $entity
            );
            if ($success) {
                $this->Modal->success();
                return $this->renderDataToJson(
                    $Timestampers->get($entity->get('id'))
                );
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
        $this->set('sas', $this->optionsSa()->toArray());
    }

    /**
     * Action de suppression d'un service d'horodatage
     * @param string $id
     * @return CakeResponse
     */
    public function deleteTimestamper(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var TimestampersTable $Timestampers */
        $Timestampers = $this->fetchTable('Timestampers');
        /** @var TimestamperEntity $entity */
        $entity = $Timestampers->get($id);

        $report = $Timestampers->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        $identity = $this->getRequest()->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        $adminname = Hash::get($user, 'username');
        $this->logEvent(
            'admintech_delete_timestamper',
            $report === 'done' ? 'success' : 'fail',
            __(
                "Suppression de l'horodateur ''{0}'' par l'administrateur technique ''{1}''",
                $entity->get('name'),
                $adminname
            ),
            $entity->toPremisObject()
        );

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Formulaire de création d'un certificat TSA comme dans /install
     */
    public function addTimestamperCert()
    {
        $request = $this->getRequest();
        $this->viewBuilder()->setTemplatePath('Admins/Timestampers');
        $form = new TSACertForm();

        if ($request->is('post')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($request->getData())) {
                $this->Modal->success();
                return $this->renderDataToJson($form->entity);
            } else {
                $this->Modal->fail();
            }
        }

        $this->set('form', $form);
    }

    /**
     * Permet de tester avant création du TSA
     * @param string $prefix
     * @return CakeResponse
     * @throws ErrorException
     * @throws Throwable
     */
    public function testTimestamperTSA(string $prefix = '')
    {
        $this->getRequest()->allowMethod('post');
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        try {
            $responseData = [];
            try {
                $data = $this->getRequest()->getData();
                $dir = $data[$prefix . 'directory'];
                $cnf = str_replace('{{dir}}', $dir, $data[$prefix . 'cnf']);
                file_put_contents($dir . DS . 'asalae-tsa.cnf', $cnf);

                $tsaKey = openssl_pkey_new(
                    [
                        'private_key_bits' => 2048,
                        'private_key_type' => OPENSSL_KEYTYPE_RSA,
                    ]
                );
                $csr = openssl_csr_new(
                    [
                        "countryName" => $data[$prefix . 'countryName'],
                        "stateOrProvinceName" => $data[$prefix . 'stateOrProvinceName'],
                        "localityName" => $data[$prefix . 'localityName'],
                        "organizationName" => $data[$prefix . 'organizationName'],
                        "organizationalUnitName" => $data[$prefix . 'organizationalUnitName'],
                        "commonName" => $data[$prefix . 'commonName'],
                        "emailAddress" => $data[$prefix . 'emailAddress'],
                    ],
                    $tsaKey
                );
                openssl_pkey_export_to_file($tsaKey, $dir . DS . 'tsacert.pem');
                openssl_csr_export_to_file($csr, $dir . DS . 'tsacert.csr');

                file_put_contents(
                    $dir . DS . 'extfile',
                    $data[$prefix . 'extfile']
                );
                $in = Exec::escapeshellarg($dir . DS . 'tsacert.csr');
                $signkey = Exec::escapeshellarg($dir . DS . 'tsacert.pem');
                $out = Exec::escapeshellarg($dir . DS . 'tsacert.crt');
                $extfile = Exec::escapeshellarg($dir . DS . 'extfile');
                $cmd = "openssl x509 -req -days 3650 -in $in -signkey $signkey -out $out -extfile $extfile 2>&1";
                exec($cmd, $output, $code);
                if ($code !== 0) {
                    throw new Exception(implode(PHP_EOL, $output));
                }
                file_put_contents($dir . DS . 'serial', '00000001');

                $instance = new TimestampingLocal(
                    $dir . DS . 'tsacert.crt',
                    $dir . DS . 'tsacert.crt',
                    $dir . DS . 'tsacert.pem',
                    $dir . DS . 'asalae-tsa.cnf'
                );
                $errHandler = ErrorTrap::captureErrors(
                    [$instance, 'check']
                );
                $responseData['success'] = $errHandler['result'];
                if (!empty($errHandler['errors'])) {
                    $responseData['errors'] = $errHandler['errors'];
                }
            } catch (Throwable $e) {
                $responseData['success'] = false;
                $responseData['exception'] = $e->getMessage();
                if (Configure::read('debug')) {
                    $responseData['file'] = $e->getFile();
                    $responseData['line'] = $e->getLine();
                }
            }
        } catch (Throwable $e) {
            restore_error_handler();
            throw $e;
        }
        restore_error_handler();

        return $this->renderDataToJson($responseData);
    }
}
