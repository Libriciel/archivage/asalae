<?php

/**
 * Asalae\Controller\Admins\ArchivalAgencyTrait
 */

namespace Asalae\Controller\Admins;

use Asalae\Controller\AdminsController;
use Asalae\Controller\Component\EmailsComponent;
use Asalae\Model\Entity\Eaccpf;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Entity\User;
use Asalae\Model\Table\ArchivingSystemsTable;
use Asalae\Model\Table\EaccpfsTable;
use Asalae\Model\Table\LdapsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\RolesTable;
use Asalae\Model\Table\SecureDataSpacesTable;
use Asalae\Model\Table\TimestampersTable;
use Asalae\Model\Table\TypeEntitiesTable;
use Asalae\Model\Table\UsersTable;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\I18n\Date as CoreDate;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Cake\Datasource\EntityInterface;
use Cake\Http\Response as CakeResponse;
use Cake\I18n\Date as CakeDate;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use Exception;
use Psr\Http\Message\ResponseInterface;

/**
 * Trait ArchivalAgencyTrait
 *
 * @category Controller\Admins
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AdminsController
 */
trait ArchivalAgencyTrait
{
    /**
     * Permet d'ajouter / retirer des Services d'archives
     */
    public function indexServicesArchives()
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }
        $this->getRequest()->allowMethod('ajax');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        /** @var OrgEntity $entity */
        $data = $OrgEntities->find()
            ->where(['TypeEntities.code' => 'SA'])
            ->contain(['TypeEntities'])
            ->orderBy(['OrgEntities.name' => 'asc'])
            ->all()
            ->map(
                function (EntityInterface $e) {
                    $e->set('deletable', $e->get('deletable'));
                    return $e;
                }
            )
            ->toArray();
        $this->set('data', $data);
        $this->set('tableIdServiceArchive', self::TABLE_SERVICE_ARCHIVE);
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
        $count = $SecureDataSpaces->find()
            ->innerJoinWith('Volumes')
            ->where(['is_default' => false])
            ->count();
        $this->set('availableDataspaces', $count > 0);
    }

    /**
     * Ajout d'un service d'archives
     */
    public function addServiceArchive()
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        $request = $this->getRequest();
        $request->allowMethod('ajax');
        /** @var ArchivingSystemsTable $ArchivingSystems */
        $ArchivingSystems = $this->fetchTable('ArchivingSystems');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        /** @var TypeEntitiesTable $TypeEntities */
        $TypeEntities = $this->fetchTable('TypeEntities');
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');

        $se = $OrgEntities->find()
            ->select(['OrgEntities.id'])
            ->contain(['TypeEntities'])
            ->where(['TypeEntities.code' => 'SE'])
            ->firstOrFail();
        $type = $TypeEntities->find()
            ->select(['id', 'code'])
            ->where(['code' => 'SA'])
            ->firstOrFail();
        $entity = $OrgEntities->newEntity(
            [
                'parent_id' => $se->get('id'),
                'type_entity_id' => $type->get('id'),
                'type_entity' => $type,
            ],
            ['validate' => false]
        );
        $mainExists = $OrgEntities->exists(
            ['is_main_archival_agency' => true]
        );
        $this->set('mainExists', $mainExists);
        if ($request->is('post')) {
            $defaultSecureDataSpace = $SecureDataSpaces->find()
                ->innerJoinWith('Volumes')
                ->where(
                    [
                        'SecureDataSpaces.id' => (int)$request->getData('default_secure_data_space_id'),
                        'SecureDataSpaces.org_entity_id IS' => null,
                        'SecureDataSpaces.is_default' => false,
                    ]
                )
                ->firstOrFail();

            $data = [
                'name' => $request->getData('name'),
                'identifier' => $request->getData('identifier'),
                'parent_id' => $se->get('id'),
                'timestampers' => [
                    '_ids' => $request->getData('timestampers._ids'),
                ],
                'type_entity_id' => $type->get('id'),
                'type_entity' => $type,
                'secure_data_spaces' => [
                    '_ids' => [
                        $request->getData('secure_data_spaces._ids'),
                    ],
                ],
                'ldaps' => [
                    '_ids' => $request->getData('ldaps._ids'),
                ],
                'archiving_systems' => [
                    '_ids' => $request->getData('archiving_systems._ids'),
                ],
                'default_secure_data_space_id' => $request->getData('default_secure_data_space_id'),
                'archival_agency_id' => null,
            ];
            if (!$mainExists) {
                $data['timestamper_id'] = $request->getData('timestamper_id');
                $data['is_main_archival_agency'] = true;
            }
            $OrgEntities->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');

            $success = $OrgEntities->save($entity);
            $this->logEvent(
                'admintech_add_archival_agency',
                $success ? 'success' : 'fail',
                __("Ajout d'un service d'archives"),
                $entity
            );

            $SecureDataSpaces->patchEntity(
                $defaultSecureDataSpace,
                [
                    'org_entity_id' => $entity->get('id'),
                    'is_default' => true,
                ]
            );
            $success = $success && $SecureDataSpaces->save($defaultSecureDataSpace);

            if ($success) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);

        /** @var TimestampersTable $Timestampers */
        $Timestampers = $this->fetchTable('Timestampers');
        $timestampers = $Timestampers->find('list')
            ->orderBy(['name' => 'asc'])
            ->all()
            ->toArray();
        $this->set('timestampers', $timestampers);

        $query = $SecureDataSpaces->find()
            ->innerJoinWith('Volumes')
            ->where(
                [
                    'SecureDataSpaces.org_entity_id IS' => null,
                    'SecureDataSpaces.is_default' => false,
                ]
            );
        $spaces = [];
        /** @var EntityInterface $space */
        foreach ($query as $space) {
            $spaces[$space->get('id')] = [
                'value' => $space->get('id'),
                'text' => $space->get('name'),
                'locked' => $space->get('removable') ? false : 'locked',
            ];
        }
        $this->set('secure_data_spaces', $spaces);

        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $query = $Ldaps->find()
            ->where(['Ldaps.org_entity_id IS' => null]);
        $ldaps = [];
        /** @var EntityInterface $ldap */
        foreach ($query as $ldap) {
            $ldaps[$ldap->get('id')] = [
                'value' => $ldap->get('id'),
                'text' => $ldap->get('name'),
                'locked' => false,
            ];
        }
        $this->set('ldaps', $ldaps);
        $query = $ArchivingSystems->find()
            ->where(
                [
                    'ArchivingSystems.org_entity_id IS' => null,
                    'ArchivingSystems.active IS' => true,
                ]
            );
        $archivingSystems = [];
        /** @var EntityInterface $archivingSystem */
        foreach ($query as $archivingSystem) {
            $archivingSystems[$archivingSystem->get('id')] = [
                'value' => $archivingSystem->get('id'),
                'text' => $archivingSystem->get('name'),
                'locked' => false,
            ];
        }
        $this->set('archivingSystems', $archivingSystems);
    }

    /**
     * Edition d'un service d'archives
     * @param string $id
     * @throws Exception
     */
    public function editServiceArchive(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        $request = $this->getRequest();
        $request->allowMethod('ajax');
        /** @var ArchivingSystemsTable $ArchivingSystems */
        $ArchivingSystems = $this->fetchTable('ArchivingSystems');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var OrgEntity $entity */
        $entity = $OrgEntities->find()
            ->where(['OrgEntities.id' => $id])
            ->andWhere(
                [
                    'TypeEntities.code' => 'SA',
                ]
            )
            ->contain(
                [
                    'ArchivingSystems',
                    'Eaccpfs' => [
                        'sort' => [
                            'from_date' => 'desc',
                            'to_date' => 'desc',
                            'created' => 'desc',
                        ],
                    ],
                    'Ldaps',
                    'SecureDataSpaces',
                    'Timestampers',
                    'TypeEntities',
                    'Users' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q
                            ->orderBy(['Users.username' => 'asc'])
                            ->contain(['Roles'])
                            ->limit($AjaxPaginatorComponent->getConfig('limit'));
                    },
                ]
            )
            ->firstOrFail();

        /** @var EntityInterface $user */
        foreach ($entity->get('users') as $user) {
            $user->setVirtual(['deletable', 'is_linked_to_ldap']);
        }

        $this->set('entity', $entity);
        $countUsers = $Users->find()
            ->where(['org_entity_id' => $entity->id])
            ->count();
        $this->set('countUsers', $countUsers);
        $this->set('tableIdEaccpf', self::TABLE_EDIT_EACCPF_SA);
        $this->set('tableIdUser', self::TABLE_EDIT_USER_SA);

        $editableIdentifier = $entity->get('editable_identifier');
        $this->set('editableIdentifier', $editableIdentifier);

        if ($request->is('put')) {
            $data = [
                'name' => $request->getData('name'),
                'identifier' => $editableIdentifier
                    ? $this->getRequest()->getData('identifier')
                    : $entity->get('identifier'),
                'type_entity' => $entity->get('type_entity'),
                'type_entity_id' => $entity->get('type_entity_id'),
                'timestampers' => [
                    '_ids' => $request->getData('timestampers._ids'),
                ],
                'secure_data_spaces' => [
                    '_ids' => $request->getData('secure_data_spaces._ids'),
                ],
                'ldaps' => [
                    '_ids' => $request->getData('ldaps._ids'),
                ],
                'archiving_systems' => [
                    '_ids' => $request->getData('archiving_systems._ids'),
                ],
                'default_secure_data_space_id' => $request->getData(
                    'default_secure_data_space_id'
                ),
                'archival_agency_id' => $entity->get('archival_agency_id'),
                'parent_id' => $entity->get('parent_id'),
            ];

            if ($entity->get('is_main_archival_agency')) {
                $data['timestamper_id'] = $request->getData('timestamper_id');
            }

            $originalDefaultEcsId = $entity->get('default_secure_data_space_id');

            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $conn = $OrgEntities->getConnection();
            $conn->begin();
            $OrgEntities->patchEntity($entity, $data);
            $success = (bool)$OrgEntities->save($entity);
            $this->logEvent(
                'admintech_edit_archival_agency',
                $success ? 'success' : 'fail',
                __("Modification d'un service d'archives"),
                $entity
            );
            if ($success) {
                /** @var SecureDataSpacesTable $SecureDataSpaces */
                $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
                $defaultSecureDataSpace = $SecureDataSpaces->find()
                    ->innerJoinWith('Volumes')
                    ->where(
                        [
                            'SecureDataSpaces.id' => $request->getData('default_secure_data_space_id'),
                            'OR' => [
                                'SecureDataSpaces.org_entity_id' => $id,
                                'SecureDataSpaces.org_entity_id IS' => null,
                            ],
                        ]
                    )
                    ->firstOrFail();

                if ($request->getData('default_secure_data_space_id') !== $originalDefaultEcsId) {
                    $SecureDataSpaces->updateAll(
                        ['is_default' => false],
                        ['org_entity_id' => $id]
                    );

                    $SecureDataSpaces->updateAll(
                        ['is_default' => true],
                        ['id' => $defaultSecureDataSpace->get('id')]
                    );
                }

                $conditions = ['org_entity_id' => $id];
                if ($request->getData('secure_data_spaces._ids')) {
                    $conditions['id NOT IN'] = $request->getData(
                        'secure_data_spaces._ids'
                    );
                }
                $SecureDataSpaces->updateAll(
                    [
                        'org_entity_id' => null,
                        'is_default' => false,
                    ],
                    $conditions
                );
                /** @var LdapsTable $Ldaps */
                $Ldaps = $this->fetchTable('Ldaps');
                $conditions = ['org_entity_id' => $id];
                if ($request->getData('ldaps._ids')) {
                    $conditions['id NOT IN'] = $request->getData('ldaps._ids');
                }
                $Ldaps->updateAll(
                    ['org_entity_id' => null],
                    $conditions
                );
                /** @var ArchivingSystemsTable $ArchivingSystems */
                $ArchivingSystems = $this->fetchTable('ArchivingSystems');
                $conditions = ['org_entity_id' => $id];
                if ($request->getData('archiving_systems._ids')) {
                    $conditions['id NOT IN'] = $request->getData(
                        'archiving_systems._ids'
                    );
                }
                $ArchivingSystems->updateAll(
                    ['org_entity_id' => null],
                    $conditions
                );
            }
            if ($success) {
                $this->Modal->success();
                $conn->commit();
            } else {
                $this->Modal->fail();
                $conn->rollback();
                FormatError::logEntityErrors($entity);
            }
        }

        /** @var TimestampersTable $Timestampers */
        $Timestampers = $this->fetchTable('Timestampers');
        $query = $Timestampers->find();
        $timestampers = [];
        foreach ($query as $timestamper) {
            $timestampers[] = [
                'value' => $timestamper->get('id'),
                'text' => $timestamper->get('name'),
                'locked' => $timestamper->get('removable') ? false : 'locked',
            ];
        }
        $this->set('timestampers', $timestampers);

        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
        $query = $SecureDataSpaces->find()
            ->innerJoinWith('Volumes')
            ->where(
                [
                    'OR' => [
                        'SecureDataSpaces.org_entity_id IS' => null,
                        'SecureDataSpaces.org_entity_id' => $id,
                    ],
                ]
            );
        $spaces = [];
        /** @var EntityInterface $space */
        foreach ($query as $space) {
            $spaces[$space->get('id')] = [
                'value' => $space->get('id'),
                'text' => $space->get('name'),
                'locked' => $space->get('removable') ? false : 'locked',
            ];
        }
        $this->set('secure_data_spaces', $spaces);

        /** @var LdapsTable $Ldaps */
        $Ldaps = $this->fetchTable('Ldaps');
        $query = $Ldaps->find()
            ->where(
                [
                    'OR' => [
                        'Ldaps.org_entity_id IS' => null,
                        'Ldaps.org_entity_id' => $id,
                    ],
                ]
            );
        $ldaps = [];
        /** @var EntityInterface $ldap */
        foreach ($query as $ldap) {
            $ldaps[$ldap->get('id')] = [
                'value' => $ldap->get('id'),
                'text' => $ldap->get('name'),
                'locked' => $ldap->get('removable') ? false : 'locked',
            ];
        }
        $this->set('ldaps', $ldaps);
        $query = $ArchivingSystems->find()
            ->where(
                [
                    'OR' => [
                        'ArchivingSystems.org_entity_id IS' => null,
                        'ArchivingSystems.org_entity_id' => $id,
                    ],
                ]
            );
        $archivingSystems = [];
        /** @var EntityInterface $archivingSystem */
        foreach ($query as $archivingSystem) {
            $archivingSystems[$archivingSystem->get('id')] = [
                'value' => $archivingSystem->get('id'),
                'text' => $archivingSystem->get('name'),
                'locked' => $archivingSystem->get('removable') ? false
                    : 'locked',
            ];
        }
        $this->set('archivingSystems', $archivingSystems);

        $rolesUsers = $OrgEntities->findByRoles($entity)
            ->select(['qexists' => 1], true)
            ->where(['OrgEntities.id' => $id, 'agent_type' => 'person'])
            ->disableHydration()
            ->first();
        $this->set('rolesUsers', (bool)$rolesUsers);
        $rolesWs = $OrgEntities->findByRoles($entity)
            ->select(['qexists' => 1], true)
            ->where(['OrgEntities.id' => $id, 'agent_type' => 'software'])
            ->disableHydration()
            ->first();
        $this->set('rolesWs', (bool)$rolesWs);
        $AjaxPaginatorComponent->setViewPaginator($entity->get('users'), $countUsers);
    }

    /**
     * Formulaire d'ajout d'un eaccpf
     * @param string $orgEntityId
     * @return CakeResponse|null
     * @throws Exception
     */
    public function addEaccpf(string $orgEntityId)
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        $this->getRequest()->allowMethod('ajax');

        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = $this->fetchTable('Eaccpfs');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $orgEntity = $OrgEntities->find()
            ->where(['OrgEntities.id' => $orgEntityId])
            ->contain(
                [
                    'ParentOrgEntities',
                    'Eaccpfs' => [
                        'sort' => [
                            'from_date' => 'desc',
                            'to_date' => 'desc',
                            'created' => 'desc',
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        /** @var null|Eaccpf $prevEntity */
        $prevEntity = null;
        if (isset($orgEntity->get('eaccpfs')[0])) {
            $prevEntity = end($orgEntity->get('eaccpfs'));
            $defaultRecordId = $prevEntity->get('record_id');
            $fromDate = $prevEntity->get('to_date') ?: $this->getRequest()->getData('to_date');
            $data = [
                'record_id' => $defaultRecordId,
                'name' => $prevEntity->get('name'),
                'entity_id' => $prevEntity->get('entity_id'),
                'entity_type' => $prevEntity->get('entity_type'),
                'from_date' => $fromDate ? (string)(new CakeDate($fromDate)) : null,
                'to_date' => null,
                'agency_name' => $prevEntity->get('agency_name'),
            ];
        } else {
            $defaultRecordId = $orgEntity->get('identifier');
            $data = [
                'record_id' => $defaultRecordId,
                'name' => $orgEntity->get('name'),
                'agency_name' => Hash::get(
                    $orgEntity,
                    'parent_org_entity.name'
                ),
            ];
        }

        /** @var Eaccpf $entity */
        $entity = $Eaccpfs->newEntity($data);
        $entity->set(
            'from_date',
            (string)($entity->get('from_date') ?: new CoreDate()),
            ['setter' => false]
        );
        $this->set('defaultRecordId', $defaultRecordId);
        $this->set('prevEntity', $prevEntity);

        if ($prevEntity && !$prevEntity->get('to_date')) {
            if ($this->getRequest()->is('put')) {
                $Eaccpfs->patchEntity(
                    $prevEntity,
                    [
                        'record_id' => $prevEntity->get('record_id'),
                        'to_date' => $this->getRequest()->getData('to_date'),
                    ]
                );
                if (!$Eaccpfs->save($prevEntity)) {
                    $this->viewBuilder()->setTemplate('todate');
                }
                /** @use ModalComponent */
                $this->loadComponent('AsalaeCore.Modal');
                $this->Modal->fail();
            } else {
                $this->viewBuilder()->setTemplate('todate');
            }
        }
        if ($this->getRequest()->is('post')) {
            $entity = $Eaccpfs->newEntity([], ['validate' => false]);
            $data = [
                'record_id' => $orgEntity->get('identifier'),
                'name' => $orgEntity->get('name'),
                'agency_name' => $this->getRequest()->getData('agency_name'),
                'entity_id' => $this->getRequest()->getData('entity_id'),
                'entity_type' => $this->getRequest()->getData('entity_type'),
                'from_date' => $this->getRequest()->getData('from_date'),
                'to_date' => $this->getRequest()->getData('to_date'),
                'org_entity_id' => $orgEntityId,
            ];
            $Eaccpfs->patchEntity($entity, $data);
            $session = $this->getRequest()->getSession();
            $createur = $session->read("Admin.data.name") ?: $session->read("Admin.data.username");
            $Eaccpfs->initializeData($entity, ['name' => $createur]);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($Eaccpfs->save($entity)) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);
        $this->set('entity_type', $Eaccpfs->options('entity_type'));

        $uploadId = 'eaccpf-upload-table';
        $this->set('uploadId', $uploadId);
        $this->set('orgEntityId', $orgEntityId);
    }

    /**
     * Ajouter un utilisateur
     * @param string $orgEntityId
     * @return CakeResponse|null
     * @throws Exception
     */
    public function addUser(string $orgEntityId)
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        $this->getRequest()->allowMethod('ajax');

        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        /** @var RolesTable $Roles */
        $Roles = $this->fetchTable('Roles');
        $role = $Roles->find()
            ->select(['id'])
            ->where(['name' => 'Archiviste'])
            ->firstOrFail();
        /** @var User $entity */
        $entity = $Users->newEntity(
            [
                'org_entity_id' => $orgEntityId,
                'role_id' => $role->get('id'),
            ]
        );

        if ($this->getRequest()->is('post')) {
            $password = $Users->generatePassword();
            $Users->patchEntity(
                $entity,
                [
                    'username' => $this->getRequest()->getData('username'),
                    'password' => $password,
                    'confirm-password' => $password,
                    'name' => $this->getRequest()->getData('name'),
                    'email' => $this->getRequest()->getData('email'),
                    'high_contrast' => $this->getRequest()->getData(
                        'high_contrast'
                    ),
                    'active' => true,
                    'org_entity_id' => $orgEntityId,
                    // important pour la validation
                    'agent_type' => 'person',
                    'is_validator' => $this->getRequest()->getData(
                        'is_validator'
                    ) ?? true,
                    'use_cert' => $this->getRequest()->getData(
                        'use_cert'
                    ) ?? false,
                ]
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');

            $success = $Users->save($entity);
            $this->logEvent(
                'admintech_add_user',
                $success ? 'success' : 'fail',
                __(
                    "Ajout de l'utilisateur ''{0}'' sur le service d'archives id={1}",
                    $entity->get('username'),
                    $orgEntityId
                ),
                $entity
            );

            if ($success) {
                $this->Modal->success();
                $json = $entity->toArray();
                $json['role'] = $role->toArray();
                /** @use EmailsComponent */
                $this->loadComponent('Emails');
                $this->Emails->submitEmailNewUser($entity);
                return $this->renderJson(json_encode($json));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('user', $entity);
        $this->set('title', __("Ajouter un utilisateur"));
    }

    /**
     * Défini le service d'archives gestionnaire
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function setMainArchivalAgency(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $entity = $OrgEntities->get($id);
        if ($this->getRequest()->is('put')) {
            $OrgEntities->patchEntity(
                $entity,
                [
                    'is_main_archival_agency' => true,
                    'timestamper_id' => $this->getRequest()->getData(
                        'timestamper_id'
                    ),
                ]
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($OrgEntities->save($entity)) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        /** @var TimestampersTable $Timestampers */
        $Timestampers = $this->fetchTable('Timestampers');
        $timestampers = $Timestampers->find('list');
        $this->set('timestampers', $timestampers);
        $this->set('entity', $entity);
    }

    /**
     * Pagination des utilisateurs de l'entité
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function paginateUsers(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index', ['model' => 'Users']);
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $query = $Users->find()
            ->where(['Users.org_entity_id' => $id])
            ->contain(['Roles']);
        $IndexComponent->setQuery($query)
            ->filter('username', IndexComponent::FILTER_ILIKE);
        return $AjaxPaginatorComponent->json(
            $query,
            (bool)$this->getRequest()->getQuery('count')
        );
    }
}
