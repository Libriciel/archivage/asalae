<?php

/**
 * Asalae\Controller\Admins\ConfigurationsTrait
 */

namespace Asalae\Controller\Admins;

use Asalae\Controller\AdminsController;
use Asalae\Controller\Component\EmailsComponent;
use Asalae\Model\Entity\Configuration;
use Asalae\Model\Entity\Fileupload;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Factory\Utility;
use Cake\Mailer\Mailer;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use AsalaeCore\Form\ConfigurationForm;
use Cake\Core\Configure;

/**
 * Trait ConfigurationsTrait
 *
 * @category Controller\Admins
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AdminsController
 */
trait ConfigurationsTrait
{
    /**
     * Permet de modifier la configuration par editeur de texte
     */
    public function ajaxEditConf()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Configurations');
        $data = ['name' => 'logo-client'];
        /** @var ConfigurationsTable $Configurations */
        $Configurations = $this->fetchTable('Configurations');
        /** @var Configuration $configuration */
        if ($this->isConnected()) {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = $this->fetchTable('OrgEntities');
            $se = $OrgEntities->find()
                ->where(['TypeEntities.code' => 'SE'])
                ->contain(['TypeEntities'])
                ->first();
            if ($se) {
                $data += ['org_entity_id' => $se->get('id')];
                $configuration = $Configurations->find()
                    ->where($data)
                    ->first();
            } else {
                $configuration = null;
            }
            if (!$configuration) {
                $configuration = $Configurations->newEntity(
                    $data,
                    ['validate' => false]
                );
            }
        } else {
            $se = null;
            $configuration = null;
        }

        $this->set('se', $se);

        $form = new ConfigurationForm();
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            Filesystem::begin();
            if ($this->isConnected()) {
                $conn = $Configurations->getConnection();
                $conn->begin();
            }
            // Upload du background -> copy sur webroot
            if (
                $se
                && ($file_id = $this->getRequest()->getData('fileuploads.id'))
                && $file_id !== 'local'
            ) {
                /** @var FileuploadsTable $Fileuploads */
                $Fileuploads = $this->fetchTable('Fileuploads');
                /** @var Fileupload $file */
                $file = $Fileuploads->get($file_id);
                $file->copyInEntityDirectory($configuration);
            }
            if ($this->isConnected()) {
                $data['libriciel_login_logo'] = $configuration->get('setting');
                if (
                    !$data['libriciel_login_logo'] && $configuration->get(
                        'id'
                    )
                ) {
                    $Configurations->delete($configuration->get('id'));
                }
                $success = empty($data['libriciel_login_logo']) || $Configurations->save(
                    $configuration
                );
            } elseif (!isset($success)) {
                $success = true;
            }
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($success && $form->execute($data)) {
                if ($this->isConnected() && isset($conn)) {
                    $conn->commit();
                }
                Filesystem::commit();
                $this->Modal->success();
            } else {
                if ($this->isConnected() && isset($conn)) {
                    $conn->rollback();
                }
                Filesystem::rollback();
                $this->log(var_export($form->getErrors(), true));
                $this->Modal->fail();
                $success = false;
            }
            $identity = $this->getRequest()->getAttribute('identity');
            $user = $identity ? $identity->getOriginalData() : null;
            $adminname = Hash::get($user, 'username');
            $this->logEvent(
                'admintech_edit_config',
                $success ? 'success' : 'fail',
                __(
                    "Modification de la configuration de l'application par l'administrateur technique ''{0}''",
                    $adminname
                )
            );
        }
        $appLocalConfigFileUri = $this->getAppLocalConfigFileUri();
        if (empty($appLocalConfigFileUri)) {
            $this->Flash->error(
                __("Le fichier de configuration local n'a pas été trouvé")
            );
        }
        $this->set(
            'config',
            file_get_contents($appLocalConfigFileUri)
        );
        $this->set('default', include CONFIG . 'app_default.php');
        $this->set('form', $form);
        $this->set('hash_algo', ConfigurationForm::options('hash_algo'));
        $this->set(
            'password_complexity',
            ConfigurationForm::options('password_complexity')
        );
        $this->set(
            'datacompressor_encodefilename',
            ConfigurationForm::options('datacompressor_encodefilename')
        );
        $this->set(
            'filesystem_useshred',
            ConfigurationForm::options('filesystem_useshred')
        );
        $this->set('appLocalConfigFileUri', $this->getAppLocalConfigFileUri());

        $uploadFiles = [];
        if ($configuration && $url = $configuration->get('setting')) {
            $webrootDir = rtrim(
                Configure::read('OrgEntities.public_dir', WWW_ROOT),
                DS
            );
            $logo = $webrootDir . DS . ltrim($url, '/');
            if (is_file($logo)) {
                $uploadFiles[] = [
                    'id' => 'local',
                    'prev' => $url,
                    'name' => basename($url),
                    'size' => filesize($logo),
                    'webroot' => true,
                ];
            }
        }
        $this->set('uploadFiles', $uploadFiles);

        $defaultEmail = $this->getRequest()->getSession()->read('Admin.data.email');
        $this->set('defaultEmail', $defaultEmail);
    }

    /**
     * Supprime le logo configuré par l'entité
     * @throws Exception
     */
    public function deleteLogo()
    {
        $this->getRequest()->allowMethod('delete');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        /** @var ConfigurationsTable $Configurations */
        $Configurations = $this->fetchTable('Configurations');
        $se = $OrgEntities->find()
            ->where(['TypeEntities.code' => 'SE'])
            ->contain(['TypeEntities'])
            ->firstOrFail();
        $id = $se->get('id');
        $this->getRequest()->allowMethod('delete');
        $entity = $Configurations->find()
            ->where(
                [
                    'org_entity_id' => $id,
                    'name' => 'logo-client',
                ]
            )
            ->firstOrFail();

        Filesystem::begin();
        Filesystem::setSafemode(true);
        if ($Configurations->delete($entity)) {
            Filesystem::commit();
            $report = 'done';
        } else {
            Filesystem::rollback();
            $report = 'Erreur lors de la suppression';
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Envoi des emails de test
     */
    public function sendTestMail()
    {
        $emailAdress = $this->getRequest()->getData('email');
        $success = !empty($emailAdress);
        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $email->viewBuilder()
            ->setTemplate('test')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);
        $success = $success && $email->setEmailFormat('both')
                ->setSubject('[asalae] ' . __("Email de test"))
                ->setTo($emailAdress)
                ->send();
        $identity = $this->getRequest()->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        $adminname = Hash::get($user, 'username');
        $this->logEvent(
            'admintech_send_testmail',
            $success ? 'success' : 'fail',
            __(
                "Envoi d'un email de test par l'administrateur technique ''{0}''",
                $adminname
            )
        );
        /** @use EmailsComponent */
        $this->loadComponent('Emails');
        if ($response = $this->Emails->debug($email)) {
            return $response;
        }
        return $this->renderDataToJson(['success' => $success]);
    }
}
