<?php

/**
 * Asalae\Controller\Admins\CronsTrait
 */

namespace Asalae\Controller\Admins;

use Asalae\Controller\AdminsController;
use Asalae\Model\Table\CronsTable;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Factory;
use Cake\Http\Response as CakeResponse;
use Exception;

/**
 * Trait CronsTrait
 *
 * @category Controller\Admins
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AdminsController
 */
trait CronsTrait
{
    /**
     * Liste des crons
     */
    public function indexCrons()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Crons');
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }
        $this->set('tableId', self::TABLE_INDEX_CRONS);
        /** @var CronsTable $Crons */
        $Crons = $this->fetchTable('Crons');
        $crons = $Crons->find()
            ->orderBy(['Crons.name' => 'asc'])
            ->all()
            ->toArray();
        $this->set('crons', $crons);
    }

    /**
     * Modification d'un cron
     * @param string $id
     * @throws Exception
     */
    public function editCron(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Crons');
        /** @var CronsTable $Crons */
        $Crons = $this->fetchTable('Crons');
        $entity = $Crons->get($id);
        $request = $this->getRequest();
        if ($request->is('put')) {
            $data = [
                'next' => $request->getData('next'),
                'active' => $request->getData('active'),
            ];
            if ($entity->get('frequency') !== 'one_shot') {
                $data += [
                    // Important: "frequency_build_unit" doit être avant "frequency_build"
                    'frequency_build_unit' => $request->getData('frequency_build_unit'),
                    'frequency_build' => $request->getData('frequency_build'),
                ];
            }
            if ($request->getData('hidden_locked')) {
                $data['locked'] = $request->getData('locked');
            }
            /** @var string|CronInterface $className */
            $className = $entity->get('classname');
            foreach ($className::getVirtualFields() as $field => $options) {
                $data[$field] = $request->getData($field);
            }
            $Crons->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($Crons, $entity, $data);
            $this->logEvent(
                'admintech_edit_cron',
                $this->Modal->lastSaveIsSuccess() ? 'success' : 'fail',
                __(
                    "Modification du cron ''{0}''",
                    $entity->get('name')
                ),
                $entity
            );
        }
        $this->set('entity', $entity);
    }

    /**
     * Visualisation d'un cron
     * @param string $id
     */
    public function viewCron(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Crons');
        /** @var CronsTable $Crons */
        $Crons = $this->fetchTable('Crons');
        $entity = $Crons->get($id);
        $this->set('entity', $entity);
    }

    /**
     * Lance le cron en manuel
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function runCron(string $id)
    {
        /** @var CronsTable $Crons */
        $Crons = $this->fetchTable('Crons');
        $entity = $Crons->get($id);
        Factory\Utility::get('Exec')
            ->async(
                CAKE_SHELL,
                'cron',
                'run',
                (int)$id,
                ['--context' => 'user-action']
            );

        $this->logEvent(
            'admintech_exec_cron',
            'success',
            __(
                "Execution manuelle du cron ''{0}''",
                $entity->get('name')
            ),
            $entity
        );
        return $this->renderDataToJson("done");
    }

    /**
     * Permet d'obtenir des informations sur un cron
     * @param string $id
     * @return CakeResponse
     */
    public function getCronState(string $id)
    {
        /** @var CronsTable $Crons */
        $Crons = $this->fetchTable('Crons');
        $entity = $Crons->get($id);
        return $this->renderDataToJson($entity);
    }

    /**
     * Suppression des crons (one_shot en erreur)
     * @param string $id
     * @return CakeResponse
     */
    public function deleteCron(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var CronsTable $Crons */
        $Crons = $this->fetchTable('Crons');
        $entity = $Crons->get($id);

        $report = $Crons->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }
}
