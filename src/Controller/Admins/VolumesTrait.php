<?php

/**
 * Asalae\Controller\Admins\VolumesTrait
 */

namespace Asalae\Controller\Admins;

use Asalae\Controller\AdminsController;
use Asalae\Cron\ChangeVolumesInSpace;
use Asalae\Form\ChangeVolumesInSpaceForm;
use Asalae\Model\Entity\SecureDataSpace;
use Asalae\Model\Entity\Volume;
use Asalae\Model\Table\CronsTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\SecureDataSpacesTable;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Table\VolumesTable;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeInterface;
use AsalaeCore\Http\Response;
use AsalaeCore\Utility\LimitBreak;
use AsalaeCore\Utility\Translit;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Response as CakeResponse;
use Cake\Log\Log;
use Cake\ORM\Entity;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use Exception;

/**
 * Trait VolumesTrait
 *
 * @category Controller\Admins
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AdminsController
 */
trait VolumesTrait
{
    /**
     * Liste les volumes et les espaces de conservation sécurisés
     */
    public function indexVolumes()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Volumes');
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');

        $this->set('tableIdVolumes', self::TABLE_INDEX_VOLUMES);
        $this->set('tableIdSpaces', self::TABLE_INDEX_SPACES);

        $volumes = $Volumes->find()->orderBy('name')->all()->toArray();
        /** @var Entity $volume */
        foreach ($volumes as $volume) {
            $configured = Configure::read(
                'Volumes.drivers.' . $volume->get('driver') . '.fields',
                []
            );
            $volume->set(
                'fields',
                $this->filterPasswordFromFields(
                    $volume->get('fields'),
                    $configured
                )
            );
        }
        $this->set('volumes', $volumes);

        $spaces = $SecureDataSpaces->find()
            ->contain(['Volumes', 'OrgEntities'])
            ->orderBy(['SecureDataSpaces.name' => 'asc'])
            ->all();
        $this->set('spaces', $spaces);
    }

    /**
     * Retire les mots de passe du champ fields d'un driver
     * @param string $fields     json
     * @param array  $configured
     * @return string
     */
    protected function filterPasswordFromFields(string $fields, array $configured)
    {
        $data = json_decode($fields);
        $changed = false;
        foreach ($configured as $field => $params) {
            if (isset($params['type']) && $params['type'] === 'password') {
                $changed = true;
                unset($data->$field);
            }
        }
        return $changed ? json_encode($data) : $fields;
    }

    /**
     * Suppression d'un volume
     * @param string $id
     * @return CakeResponse
     */
    public function deleteVolume(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        /** @var Volume $entity */
        $entity = $Volumes->get($id);

        $report = $Volumes->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        $identity = $this->getRequest()->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        $adminname = Hash::get($user, 'username');
        $this->logEvent(
            'admintech_delete_volume',
            $report === 'done' ? 'success' : 'fail',
            __(
                "Suppression du volume ''{0}'' par l'administrateur technique ''{1}''",
                $entity->get('name'),
                $adminname
            ),
            $entity->toPremisObject()
        );

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Ajout d'un volume
     * @throws Exception
     */
    public function addVolume()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Volumes');
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        $entity = $Volumes->newEntity(
            [
                'active' => true,
                'disk_usage' => 0,
            ],
            ['validate' => false]
        );
        $this->addEditVolumeCommon($entity, 'post');
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
        $this->set(
            'secure_data_spaces',
            $SecureDataSpaces->find('list')->orderBy('name')->all()
        );
        if ($this->getRequest()->is('post')) {
            $success = $this->Modal->lastSaveIsSuccess();
            $this->logEvent(
                'admintech_add_volume',
                $success ? 'success' : 'fail',
                __("Ajout d'un volume"),
                $entity
            );
        }
    }

    /**
     * Logique commune entre add et edit d'un volume
     * @param EntityInterface $entity
     * @param string          $method
     * @throws Exception
     */
    private function addEditVolumeCommon(
        EntityInterface $entity,
        string $method
    ) {
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        $decryptKey = hash('sha256', self::DECRYPT_KEY);
        $this->set('decryptKey', $decryptKey);
        $diskUsage = $entity->get('max_disk_usage');
        if ($diskUsage && (int)$diskUsage % 1024 === 0) {
            $pow = 0;
            while ($diskUsage > 1024) {
                $diskUsage /= 1024;
                if (++$pow >= 5 || $diskUsage % 1024 !== 0) {
                    break;
                }
            }
            $entity->set('mult', pow(1024, $pow));
            $entity->setDirty('mult', false);
        }
        $entity->set('max_disk_usage_conv', $diskUsage);
        $entity->setDirty('max_disk_usage_conv', false);

        if ($this->getRequest()->is($method)) {
            $driver = $this->getRequest()->getData('driver');
            $fields = current($this->getRequest()->getData('fields', [[]]));
            $originalDecryptedFields = $entity->get('original_decrypted_fields');
            $originalCryptedFields = $entity->get('original_crypted_fields');
            $configured = Configure::read(
                'Volumes.drivers.' . $driver . '.fields',
                []
            );
            foreach ($configured as $field => $params) {
                $originalCryptedValue = $originalCryptedFields[$field] ?? null;
                $originalDecryptedValue = $originalDecryptedFields[$field] ?? null;
                if (isset($params['type']) && $params['type'] === 'password') {
                    $fields[$field] = $fields[$field] === $originalDecryptedValue
                        ? $originalCryptedValue
                        : base64_encode(Security::encrypt($fields[$field], $decryptKey));
                }
            }

            $data = [
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'location' => $this->getRequest()->getData('location'),
                'driver' => $driver,
                'active' => $this->getRequest()->getData('active'),
                'date_begin' => $this->getRequest()->getData('date_begin'),
                'date_end' => $this->getRequest()->getData('date_end'),
                'alert_date' => $this->getRequest()->getData('alert_date'),
                'alert_rate' => $this->getRequest()->getData('alert_rate'),
                'fields' => json_encode($fields, JSON_UNESCAPED_SLASHES),
            ];
            if ($max = $this->getRequest()->getData('max_disk_usage_conv')) {
                $data['max_disk_usage'] = $max * (int)$this->getRequest()
                        ->getData('mult', 1);
            }
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $callback = function (EntityInterface $entity) use ($configured) {
                $controller = $this;
                $output = $entity->toArray();
                if ($entity->isDirty('fields')) {
                    $output['fields'] = $controller->filterPasswordFromFields(
                        $output['fields'],
                        $configured
                    );
                }
                return $output;
            };
            if (
                $this->Modal->save(
                    $Volumes,
                    $entity,
                    $data,
                    ['update_read_duration' => true],
                    $callback
                )->success
            ) {
                return;
            }
        }

        $this->set('entity', $entity);

        $driversFields = [];
        $drivers = [];
        foreach (Configure::read('Volumes.drivers', []) as $id => $params) {
            $drivers[$id] = $params['name'];
            $driversFields[$id] = $params['fields'];
        }
        $this->set('drivers', $drivers);
        $this->set('driversFields', $driversFields);

        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = $this->fetchTable('Fileuploads');
        $path = Configure::read('App.paths.data') . DS . 'ssl' . DS . 'minio';
        $uploadData = $Fileuploads->find()
            ->where(['path LIKE' => $path . '%'])
            ->orderBy(['name'])
            ->toArray();
        $this->set('uploadData', $uploadData);
    }

    /**
     * Modification d'un volume
     * @param string $id
     * @throws Exception
     */
    public function editVolume(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Volumes');
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        $entity = $Volumes->get($id);
        $fields = json_decode($entity->get('fields'), true);
        $decryptKey = hash('sha256', self::DECRYPT_KEY);
        $configured = Configure::read(
            'Volumes.drivers.' . $entity->get('driver') . '.fields'
        );
        $entity->set('original_crypted_fields', $fields);
        foreach ($fields as $field => $value) {
            if (!empty($configured[$field]['type']) && $configured[$field]['type'] === 'password') {
                $fields[$field] = Security::decrypt(
                    base64_decode($value),
                    $decryptKey
                );
            }
        }
        $entity->set('original_decrypted_fields', $fields);
        $drivers = Configure::read('Volumes.drivers');
        $index = array_search($entity->get('driver'), array_keys($drivers));
        $entity->set(
            'fields',
            [$index => $fields]
        );// Pour un bon remplissage du formulaire
        $this->addEditVolumeCommon($entity, 'put');
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
        $this->set(
            'secure_data_spaces',
            $SecureDataSpaces->find('list')->orderBy('name')->all()
        );
        if ($this->getRequest()->is('put')) {
            $success = $this->Modal->lastSaveIsSuccess();
            $this->logEvent(
                'admintech_edit_volume',
                $success ? 'success' : 'fail',
                __("Modification d'un volume"),
                $entity
            );
        }
    }

    /**
     * Action de programation du changement de volumes d'un espace de stockage
     * @param string $space_id
     * @return Response|CakeResponse|void
     * @throws Exception
     */
    public function changeVolumesInSpace(string $space_id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Volumes');
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');

        $ecs = $SecureDataSpaces->find()
            ->contain(['Volumes', 'OrgEntities'])
            ->where(['SecureDataSpaces.id' => $space_id])
            ->firstOrFail();
        if (!$ecs->get('volumesEditable')) {
            throw new ForbiddenException();
        }

        // on peut ajouter 2 fois le même volume à un ecs si le cron est pas encore passé
        /** @var CronsTable $Crons */
        $Crons = $this->fetchTable('Crons');
        $mobilisedVolumes = [];
        $changeVolumeCrons = $Crons->find()
            ->select('app_meta')
            ->where(
                [
                    'classname' => ChangeVolumesInSpace::class,
                    'app_meta LIKE' => '%"add":["%',
                ]
            )
            ->disableHydration();

        foreach ($changeVolumeCrons as $cron) {
            $mobilisedVolumes = array_merge($mobilisedVolumes, json_decode($cron['app_meta'], true)['add']);
        }
        $mobilisedVolumes = array_unique($mobilisedVolumes);

        $availablesVolumes = $Volumes->find('list')
            ->where(
                [
                    'secure_data_space_id IS' => null,
                    'active IS' => true,
                ]
                + ($mobilisedVolumes ? ['id NOT IN' => $mobilisedVolumes] : [])
            )
            ->all();
        $this->set('availablesVolumes', $availablesVolumes);

        $currentVolumes = $Volumes->find('list')
            ->where(['secure_data_space_id' => $space_id])
            ->all();
        $this->set('currentVolumes', $currentVolumes);

        $form = new ChangeVolumesInSpaceForm();
        $form->currentVolumes = $currentVolumes;

        if ($this->getRequest()->is('post')) {
            $data = Hash::filter(
                $this->getRequest()->getData() + ['space_id' => $space_id]
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');

            $success = $form->execute($data);
            $this->logEvent(
                'admintech_change_volume_in_space',
                $success ? 'success' : 'fail',
                __(
                    "Modification des volumes de l'ECS ''{0}''",
                    $ecs->get('name')
                ),
                $ecs
            );

            if ($success) {
                $this->Modal->success();
                return $this->renderDataToJson($form->getData() + ['ecs' => $ecs]);
            } else {
                $this->Modal->fail();
            }
        }

        $this->set('form', $form);
    }

    /**
     * Test du volume
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function testVolume(string $id = '')
    {
        $this->getRequest()->allowMethod(['post', 'put']);
        if (empty($id)) {
            $driver = $this->getRequest()->getData('driver');
            $fields = current($this->getRequest()->getData('fields'));
        } else {
            /** @var VolumesTable $Volumes */
            $Volumes = $this->fetchTable('Volumes');
            $volume = $Volumes->get($id);
            if (empty($volume->get('read_duration'))) {
                $Volumes->calcReadDuration($volume);
            }
            $driver = $volume->get('driver');
            $fields = json_decode($volume->get('fields'), true);
        }

        $config = Configure::read('Volumes.drivers.' . $driver);
        if (empty($driver) || empty($config)) {
            throw new BadRequestException();
        }

        $decryptKey = hash('sha256', self::DECRYPT_KEY);
        $args = [];
        foreach ((array)Hash::get($config, 'fields', []) as $field => $params) {
            $value = null;
            if (!empty($params['read_config'])) {
                $value = Configure::read($params['read_config']);
            } elseif (isset($fields[$field])) {
                $value = $fields[$field];
            } elseif (!empty($params['default'])) {
                $value = $params['default'];
            }
            if ($id && isset($params['type']) && $params['type'] === 'password') {
                $value = Security::decrypt(
                    base64_decode($fields[$field]),
                    $decryptKey
                );
            }
            $args[$field] = $value ?: '';
        }
        if (!empty($volume)) {
            $args['volume_entity'] = $volume;
        }
        $responseData = ['errors' => []];
        $classname = Hash::get($config, 'class', '');
        try {
            /** @var VolumeInterface $driver */
            $driver = new $classname($args);
            $responseData = $driver->test();
        } catch (Exception $e) {
            Log::error((string)$e);
            $responseData['success'] = false;
            $responseData['exception'] = $e->getMessage();
            if (Configure::read('debug')) {
                $responseData['file'] = $e->getFile();
                $responseData['line'] = $e->getLine();
            }
        }

        return $this->renderDataToJson($responseData);
    }

    /**
     * Ajout d'un espace de conservation sécurisé
     * @throws Exception
     */
    public function addSpace()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Volumes');
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
        $entity = $SecureDataSpaces->newEntity([], ['validate' => false]);

        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');

        if ($this->getRequest()->is('post')) {
            $data = [
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'org_entity_id' => $this->getRequest()->getData('org_entity_id'),
                'volumes' => [
                    '_ids' => $this->getRequest()->getData('volumes._ids'),
                ],
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $callback = function (EntityInterface $entity) use ($SecureDataSpaces) {
                return $SecureDataSpaces->find()
                    ->where(['SecureDataSpaces.id' => $entity->get('id')])
                    ->contain(['Volumes', 'OrgEntities'])
                    ->orderBy(['SecureDataSpaces.name' => 'asc'])
                    ->first();
            };
            $this->Modal->save(
                $SecureDataSpaces,
                $entity,
                $data,
                [],
                $callback
            );

            $success = $this->Modal->lastSaveIsSuccess();
            $this->logEvent(
                'admintech_add_space',
                $success ? 'success' : 'fail',
                __("Ajout d'un ECS"),
                $entity
            );
            if ($success) {
                return;
            }
        }

        $this->set('entity', $entity);
        $this->set('sas', $this->optionsSa()->toArray());
        $volumes = $Volumes->find('list')
            ->where(
                [
                    'active' => true,
                    'secure_data_space_id IS' => null,
                ]
            )
            ->orderBy(['name' => 'asc'])
            ->all()
            ->toArray();
        $this->set('volumes', $volumes);
    }

    /**
     * Modification d'un espace de conservation sécurisé
     * @param string $id
     * @throws Exception
     */
    public function editSpace(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Volumes');
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
        $entity = $SecureDataSpaces->get($id);
        $sas = $this->optionsSa()->toArray();

        if ($this->getRequest()->is('put')) {
            $orgEntityId = (int)$this->getRequest()->getData('org_entity_id');
            $data = [
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'org_entity_id' => $orgEntityId ?: null,
            ];

            // on ne peut pas changer le SA ici, et il doit être dans les options
            if (
                $entity->get('org_entity_id')
                && ($entity->get('org_entity_id') !== $orgEntityId || !isset($sas[$orgEntityId]))
            ) {
                throw new ForbiddenException();
            }

            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $callback = function (EntityInterface $entity) use ($SecureDataSpaces) {
                return $SecureDataSpaces->find()
                    ->where(['SecureDataSpaces.id' => $entity->get('id')])
                    ->contain(['Volumes', 'OrgEntities'])
                    ->orderBy(['SecureDataSpaces.name' => 'asc'])
                    ->first();
            };
            $this->Modal->save(
                $SecureDataSpaces,
                $entity,
                $data,
                [],
                $callback
            );

            $success = $this->Modal->lastSaveIsSuccess();
            $this->logEvent(
                'admintech_edit_space',
                $success ? 'success' : 'fail',
                __("Modification d'un ECS"),
                $entity
            );
            if ($success) {
                return;
            }
        }

        $this->set('entity', $entity);
        $this->set('sas', $sas);
    }

    /**
     * Supprression d'un espace de conservation sécurisé
     * @param string $id
     * @return CakeResponse
     */
    public function deleteSpace(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
        /** @var SecureDataSpace $entity */
        $entity = $SecureDataSpaces->get($id);

        $report = $SecureDataSpaces->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        $identity = $this->getRequest()->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        $adminname = Hash::get($user, 'username');
        $this->logEvent(
            'admintech_delete_space',
            $report === 'done' ? 'success' : 'fail',
            __(
                "Suppression de l'ECS ''{0}'' par l'administrateur technique ''{1}''",
                $entity->get('name'),
                $adminname
            ),
            $entity->toPremisObject()
        );

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Visualisation d'un volume
     * @param string $id
     */
    public function viewVolume(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Volumes');
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        $this->set('volume', $Volumes->get($id));
    }

    /**
     * Donne la liste des fichiers d'un volume
     * @param string $volume_id
     * @param string ...$storagePath
     * @throws VolumeException
     */
    public function volumeClient(string $volume_id, string ...$storagePath)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Volumes');
        $storagePath = implode('/', array_map('urlencode', $storagePath));
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        /** @var StoredFilesTable $StoredFiles */
        $StoredFiles = $this->fetchTable('StoredFiles');
        $volume = $Volumes->find()
            ->where(['Volumes.id' => $volume_id])
            ->contain(['SecureDataSpaces'])
            ->firstOrFail();
        $this->set('volume', $volume);

        /** @var EntityInterface $sds */
        $sds = Hash::get($volume, 'secure_data_space');
        try {
            $instance = VolumeManager::getDriver($volume);
        } catch (VolumeException $e) {
            $this->set('exception', $e->getMessage());
            return;
        }

        // construction du data
        $dirs = [];
        $files = [];
        foreach ($instance->ls($storagePath) as $name) {
            $trimmed = substr($name, strpos($name, '>') + 1);
            $trimmed = substr($trimmed, 0, strrpos($trimmed, '<'));
            $adata = [];
            if (strpos($name, '<warning>') !== false) {
                // Si c'est un dossier...
                if ($sds) {
                    $q = $StoredFiles->find();
                    $adata = $q
                        ->select(
                            [
                                'count' => $q->func()->count('*'),
                                'size' => $q->func()->sum('size'),
                            ]
                        )
                        ->where(
                            [
                                'secure_data_space_id' => $sds->id,
                                'name LIKE' => trim(
                                    "$storagePath/$trimmed%",
                                    '/'
                                ),
                            ]
                        )
                        ->disableHydration()
                        ->all()
                        ->first();
                }
                $dirs[] = [
                    'filename' => $trimmed,
                    'directory' => true,
                    'fullpath' => "$storagePath/$trimmed",
                ] + $adata;
            } elseif (strpos($name, '<comment>') !== false) {
                // Si c'est un fichier...
                if ($sds) {
                    if (basename($storagePath) === $trimmed) {
                        $conditions = [
                            'OR' => [
                                [
                                    'name LIKE' => trim(
                                        "$storagePath/$trimmed",
                                        '/'
                                    ),
                                ],
                                ['name LIKE' => trim("$storagePath", '/')],
                            ],
                        ];
                    } else {
                        $conditions = [
                            'name LIKE' => trim(
                                "$storagePath/$trimmed",
                                '/'
                            ),
                        ];
                    }
                    $adata = $StoredFiles->find()
                        ->where(['secure_data_space_id' => $sds->id])
                        ->andWhere($conditions)
                        ->disableHydration()
                        ->first();
                    if (empty($adata['size'])) {
                        $adata = [];
                    }
                }
                $files[] = [
                    'filename' => $trimmed,
                    'directory' => false,
                    'fullpath' => $adata['name'] ?? "$storagePath/$trimmed",
                    'count' => !empty($adata) ? 1 : null,
                ] + $adata;
            }
        }
        $data = array_merge($dirs, $files);
        $this->set('data', $data);
        $this->set('storagePath', $storagePath);
    }

    /**
     * Supprime les fichiers lié à un dossier/un fichier sur volume
     * @param string $volume_id
     * @param string ...$storagePath
     * @return CakeResponse
     * @throws Exception
     */
    public function deleteVolumeFile(string $volume_id, string ...$storagePath)
    {
        $storagePath = implode('/', array_map('urlencode', $storagePath));
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        try {
            $volume = $Volumes->get($volume_id);
            $instance = VolumeManager::getDriver($volume);
            $files = $instance->ls($storagePath, true);
            array_shift($files);
            foreach ($files as $ref) {
                $instance->fileDelete($ref);
            }
            $identity = $this->getRequest()->getAttribute('identity');
            $user = $identity ? $identity->getOriginalData() : null;
            $adminname = Hash::get($user, 'username');
            $this->logEvent(
                'admintech_delete_volume_file',
                'success',
                __(
                    "Suppression du fichier ''{0}'' du volume ''{1}'' par l'administrateur technique ''{2}''",
                    $storagePath,
                    $volume->get('name'),
                    $adminname
                ),
                $volume
            );
        } catch (VolumeException $e) {
            $this->Modal->fail();
            return $this->renderDataToJson(['report' => $e->getMessage()]);
        }
        $this->Modal->success();
        return $this->renderDataToJson(['report' => 'done']);
    }

    /**
     * Contrôle d'integrité sur un fichier d'un volume
     * @param string $volume_id
     * @param string ...$storagePath
     * @return CakeResponse
     * @throws VolumeException
     */
    public function volumeCheckHash(string $volume_id, string ...$storagePath)
    {
        $storagePath = implode('/', array_map([Translit::class, 'safeUrlDecode'], $storagePath));
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        /** @var StoredFilesTable $StoredFiles */
        $StoredFiles = $this->fetchTable('StoredFiles');
        $volume = $Volumes->find()
            ->where(['Volumes.id' => $volume_id])
            ->contain(['SecureDataSpaces'])
            ->firstOrFail();
        /** @var EntityInterface $sds */
        $sds = Hash::get($volume, 'secure_data_space');
        $storedFile = $StoredFiles->find()
            ->select(['hash', 'hash_algo', 'size'])
            ->where(
                [
                    'secure_data_space_id' => $sds->id,
                    'name LIKE' => trim("$storagePath", '/'),
                ]
            )
            ->disableHydration()
            ->first();
        if (!$storedFile) {
            return $this->renderDataToJson(['match' => false]);
        }
        $instance = VolumeManager::getDriver($volume);

        // 20s / Go
        LimitBreak::setTimeLimit($storedFile['size'] * 0.00000002);
        $hash = $instance->hash($storagePath, $storedFile['hash_algo']);
        return $this->renderDataToJson(
            ['match' => $hash === $storedFile['hash']]
        );
    }
}
