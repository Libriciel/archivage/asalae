<?php

/**
 * Asalae\Controller\Admins\TasksTrait
 */

namespace Asalae\Controller\Admins;

use Asalae\Controller\AdminsController;
use Asalae\Form\AddWorkerForm;
use Asalae\Model\Entity\BeanstalkJob;
use Asalae\Model\Table\BeanstalkWorkersTable;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Factory;
use AsalaeCore\Http\Response as CoreResponse;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Beanstalk\Command\WorkerCommand;
use Beanstalk\Model\Table\BeanstalkJobsTable;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Http\Response as CakeResponse;
use Exception;
use Psr\Http\Message\ResponseInterface;

/**
 * Trait TasksTrait
 *
 * @category Controller\Admins
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AdminsController
 */
trait TasksTrait
{
    /**
     * Suppression du tube
     *
     * @param string $tube
     * @return CakeResponse
     * @throws Exception
     */
    public function deleteTube(string $tube)
    {
        $this->fetchTable('BeanstalkJobs')
            ->deleteAll(
                [
                    'tube' => $tube,
                    'job_state !=' => BeanstalkJobsTable::S_WORKING,
                ]
            );
        $this->logEvent(
            'admintech_delete_tube',
            'success',
            __("Vidange du tube ''{0}''", $tube)
        );
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        $this->Modal->success();
        return $this->renderDataToJson('');
    }

    /**
     * Administration des tubes et des workers beanstalk
     */
    public function ajaxTasks()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Tasks');
        /** @var Beanstalk $Beanstalk */
        $Beanstalk = Factory\Utility::get('Beanstalk');
        if (!$Beanstalk->isConnected() || !$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données et le serveur de jobs "
                    . "doivent fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }

        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        /** @var BeanstalkWorkersTable $BeanstalkWorkers */
        $BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers');

        $workerParams = Configure::read('Beanstalk.workers');
        $workerTubes = array_keys($workerParams);
        sort($workerTubes);
        $tubes = [];
        foreach ($workerTubes as $tube) {
            $q = $BeanstalkJobs->query();
            $jobStates = $BeanstalkJobs->find()
                ->select(
                    [
                        'job_state',
                        'count' => $q->func()->count('*'),
                    ]
                )
                ->where(['tube' => $tube])
                ->groupBy(['job_state'])
                ->toArray();
            $workerCount = $BeanstalkWorkers->find()
                ->where(['tube' => $tube])
                ->count();
            $tubes[$tube] = [
                'tube' => $tube,
                'workers' => $workerCount,
                BeanstalkJobsTable::S_WORKING => 0,
                BeanstalkJobsTable::S_PENDING => 0,
                BeanstalkJobsTable::S_DELAYED => 0,
                BeanstalkJobsTable::S_PAUSED => 0,
                BeanstalkJobsTable::S_FAILED => 0,
            ];
            foreach ($jobStates as $state) {
                $tubes[$tube][$state->get('job_state')] = $state->get('count');
            }
        }
        $q = $BeanstalkJobs->query();
        $jobCount = $BeanstalkJobs->find()
            ->select(['count' => $q->func()->count('*')])
            ->where(['tube' => $q->identifier('BeanstalkWorkers.tube')]);

        $workers = $BeanstalkWorkers->find()
            ->enableAutoFields()
            ->select(['jobs' => $jobCount])
            ->orderBy(['tube']);
        $this->set('workers', $workers);
        $this->set('tubes', $tubes);
        $this->set('states', $BeanstalkJobs->options('job_state'));

        $serviceName = Configure::read('Beanstalk.service');
        setlocale(LC_ALL, 'en_US.UTF-8');
        if (!$serviceName) {
            $command = sprintf(
                '[ -f %1$s ] && tail %1$s -n500',
                escapeshellarg(LOGS . 'beanstalk.log')
            );
        } else {
            $command = sprintf(
                "journalctl -u %s -b -n500",
                escapeshellarg($serviceName)
            );
        }
        exec($command, $output);
        setlocale(LC_ALL, null);
        $this->set('serviceLogs', implode("\n", $output));
    }

    /**
     * Arrete tout les workers selon le pid en base
     */
    public function stopAllWorkers()
    {
        $Beanstalk = Factory\Utility::get('Beanstalk');
        $Beanstalk->socketEmit('stop-all-workers');
        return $this->renderDataToJson([]);
    }

    /**
     * Demande l'arret d'un worker
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function stopWorker(string $id)
    {
        $worker = $this->fetchTable('BeanstalkWorkers')->get($id);
        $message = sprintf('stop:%s:%d', $worker->get('tube'), $id);
        Beanstalk::getInstance()->socketEmit($message);
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        $this->Modal->success();
        $this->logEvent(
            'admintech_stop_worker',
            'success',
            __("Arret du worker ''{0}''", $worker->get('name'))
        );
        return $this->renderDataToJson('done');
    }

    /**
     * Tue un worker
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function killWorker(string $id)
    {
        /** @var BeanstalkWorkersTable $BeanstalkWorkers */
        $BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers');
        $worker = $BeanstalkWorkers->get($id);
        $killCommand = Configure::read(
            'Beanstalk.workers.' . $worker->get('name') . '.kill'
        );
        if (!$killCommand) {
            throw new Exception('kill command not found');
        }
        $killCommand = str_replace(
            ['{{pid}}', '{{hostname}}'],
            [$worker->get('pid'), $worker->get('hostname')],
            $killCommand
        );
        Factory\Utility::get('Exec')->command($killCommand);
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        $this->Modal->success();
        $this->logEvent(
            'admintech_kill_worker',
            'success',
            __("Kill du worker ''{0}''", $worker->get('name'))
        );
        return $this->renderDataToJson('done');
    }

    /**
     * Liste les jobs d'un tube
     * @param string $tube
     * @return ResponseInterface|void
     * @throws Exception
     */
    public function indexJobs(string $tube)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Tasks');
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index', ['template' => false]);
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $query = $BeanstalkJobs->find()
            ->where(['tube' => $tube])
            ->contain(['Users']);

        $IndexComponent->setConfig('model', 'BeanstalkJobs');
        $IndexComponent->setConfig('limit', 10);

        $IndexComponent->setQuery($query)
            ->filter('id')
            ->filter('job_state', IndexComponent::FILTER_IN)
            ->filter(
                'username',
                IndexComponent::FILTER_ILIKE,
                'Users.username'
            );

        if ($this->getRequest()->accepts('application/json')) {
            return $AjaxPaginatorComponent->json($query);
        }

        $data = $this->paginateToResultSet($query);

        $this->set('data', $data);
        $this->set('count', $query->count());
        $this->set('tube', $tube);
        $this->set('states', $BeanstalkJobs->options('job_state'));
        $this->set('connected', (bool)$this->userId);
    }

    /**
     * Permet d'obtenir les infos d'un job au format json
     * @param string $id
     * @return CakeResponse
     */
    public function jobInfo(string $id)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        /** @var BeanstalkJob $job */
        $job = $BeanstalkJobs->find()
            ->where(['BeanstalkJobs.id' => $id])
            ->contain(['Users'])
            ->first();
        return $this->renderDataToJson($job);
    }

    /**
     * delete le job
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function jobCancel(string $id)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $BeanstalkJobs->find()
            ->where(['id' => $id])
            ->firstOrFail();
        $BeanstalkJobs->deleteOrFail($entity);
        $report = "done";
        $this->logEvent(
            'admintech_delete_job',
            'success',
            __(
                "Suppression du id={0} du tube ''{1}''",
                $entity->get('id'),
                $entity->get('tube')
            )
        );

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * bury le job
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function jobPause(string $id)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $BeanstalkJobs->find()
            ->where(['BeanstalkJobs.id' => $id])
            ->contain(['Users'])
            ->firstOrFail();
        $BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_PAUSE);
        $BeanstalkJobs->saveOrFail($entity);
        $report = "done";

        $entity->set('errors', 'pause');
        $data = $entity->toArray();
        $this->logEvent(
            'admintech_pause_job',
            'success',
            __(
                "Mise en pause du id={0} sur le tube ''{1}''",
                $entity->get('id'),
                $entity->get('tube')
            )
        );

        return $this->renderDataToJson(['report' => $report, 'data' => $data]);
    }

    /**
     * kick le job
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function jobResume(string $id)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');

        $entity = $BeanstalkJobs->find()
            ->where(['BeanstalkJobs.id' => $id])
            ->contain(['Users'])
            ->firstOrFail();
        $BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_RESUME);
        $BeanstalkJobs->saveOrFail($entity);
        $report = "done";

        $data = $entity->toArray();
        $this->logEvent(
            'admintech_resume_job',
            'success',
            __(
                "Reprise du id={0} du tube ''{1}''",
                $entity->get('id'),
                $entity->get('tube')
            )
        );

        return $this->renderDataToJson(['report' => $report, 'data' => $data]);
    }

    /**
     * Permet de lancer manuellement un worker
     */
    public function addWorker()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Tasks');
        $job = new AddWorkerForm();

        if ($this->getRequest()->is('post')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $data = $this->getRequest()->getData();

            $success = $job->execute($this->getRequest()->getData());
            $this->logEvent(
                'admintech_add_job',
                $success ? 'success' : 'fail',
                __("Ajout d'un job sur le tube ''{0}''", $data['tube'])
            );
            if ($success) {
                $this->Modal->success();
                return $this->renderDataToJson($data);
            } else {
                $this->Modal->fail();
            }
        }

        $workers = WorkerCommand::availablesWorkers();
        $availables = [];
        foreach (array_keys($workers) as $worker) {
            $availables[$worker] = $worker;
        }
        $this->set('availables', $availables);
        $this->set('job', $job);
    }

    /**
     * Permet d'obtenir les logs du tube d'un worker
     * @param string $tube
     * @throws Exception
     */
    public function workerLog(string $tube)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Tasks');
        $logfile = Configure::read(
            'App.paths.logs',
            LOGS
        ) . 'worker_' . $tube . '.log';
        $log = Factory\Utility::get('Exec')->command(
            'tail -n500',
            $logfile
        )->stdout;
        $this->set('log', $log);
    }

    /**
     * Relance les jobs en erreur/pause d'un tube
     * @param string $tube
     * @return CakeResponse
     * @throws Exception
     */
    public function resumeAllTube(string $tube)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $query = $BeanstalkJobs->find()
            ->where(['tube' => $tube, 'job_state' => BeanstalkJobsTable::S_FAILED]);
        $kicked = $query->count();
        foreach ($query as $job) {
            $BeanstalkJobs->transition($job, BeanstalkJobsTable::T_RETRY);
            $BeanstalkJobs->save($job);
        }
        $this->logEvent(
            'admintech_resume_all_jobs',
            'success',
            __("Reprise de tous les jobs du tube ''{0}''", $tube)
        );
        return $this->renderDataToJson(
            ['success' => true, 'kicked' => $kicked]
        );
    }

    /**
     * Effectue un tail -f sur un fichier log
     * @param string $timeout
     * @return CoreResponse
     * @throws Exception
     */
    public function tailService($timeout = '10')
    {
        $serviceName = Configure::read('Beanstalk.service');

        setlocale(LC_ALL, 'en_US.UTF-8');
        if (!$serviceName) {
            $command = sprintf(
                '[ -f %2$s ] && timeout %.1F tail %2$s -n500',
                (float)$timeout,
                escapeshellarg(LOGS . 'beanstalk.log')
            );
        } else {
            $command = sprintf(
                "timeout %.1F journalctl -u %s -b -f -n500",
                (float)$timeout,
                escapeshellarg($serviceName)
            );
        }

        /** @var CoreResponse $response */
        $response = $this->getResponse()->withType('text');
        $handle = popen($command, 'r');
        while (!feof($handle)) {
            $response->withStringBody(fgets($handle))->partialEmit();
        }
        pclose($handle);

        setlocale(LC_ALL, null);
        return $response;
    }

    /**
     * Ordre d'arret sur le service beanstalk server
     * @return CoreResponse|CakeResponse
     */
    public function stopService()
    {
        Beanstalk::getInstance()->socketEmit('exit');
        return $this->renderDataToJson('');
    }
}
