<?php

/**
 * Asalae\Controller\FiltersController
 */

namespace Asalae\Controller;

use Asalae\Model\Table\FiltersTable;
use Asalae\Model\Table\SavedFiltersTable;
use AsalaeCore\Controller\FiltersControllerTrait;

/**
 * Filtres de rechercher
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017-2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property SavedFiltersTable $SavedFilters
 * @property FiltersTable      $Filters
 */
class FiltersController extends AppController
{
    /**
     * Traits
     */
    use FiltersControllerTrait;
}
