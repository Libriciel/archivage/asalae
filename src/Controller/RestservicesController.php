<?php

/**
 * Asalae\Controller\RestservicesController
 */

namespace Asalae\Controller;

use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\ServiceLevelsTable;
use Asalae\Model\Table\TransferAttachmentsTable;
use Asalae\Model\Table\TransfersTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Utility\Antivirus\AntivirusInterface;
use AsalaeCore\Utility\Antivirus\Clamav;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\PreControlMessages;
use Authentication\Controller\Component\AuthenticationComponent;
use Beanstalk\Utility\Beanstalk;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\ServiceUnavailableException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Response;
use Cake\Log\Log;
use Cake\Utility\Hash;
use Datacompressor\Utility\DataCompressor;
use DateTime;
use ErrorException;
use Exception;
use FileValidator\Utility\FileValidator;
use Laminas\Diactoros\UploadedFile;
use Libriciel\Filesystem\Utility\Filesystem;
use Pheanstalk\Pheanstalk;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use Transliterator;

/**
 * Restservices webservices compatible asalae 1
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchivesTable            Archives
 * @property EventLogsTable           EventLogs
 * @property OrgEntitiesTable         OrgEntities
 * @property ServiceLevelsTable       ServiceLevels
 * @property TransferAttachmentsTable TransferAttachments
 * @property TransfersTable           Transfers
 */
class RestservicesController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * Called before the controller action. You can use this method to configure and customize components
     * or perform logic that needs to happen before each controller action.
     *
     * @param EventInterface $event An Event instance
     * @return Response|null|void
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $AuthenticationComponent->addUnauthenticatedActions(['ping']);
        $inUnauthorizedActions = in_array(
            $this->getRequest()->getParam('action'),
            $AuthenticationComponent->getUnauthenticatedActions()
        );
        if (!$inUnauthorizedActions && !$this->user) {
            throw new UnauthorizedException();
        }
        return parent::beforeFilter($event);
    }

    /**
     * Called after the controller action is run and rendered.
     *
     * @param EventInterface $event An Event instance
     * @link https://book.cakephp.org/3/en/controllers.html#request-life-cycle-callbacks
     */
    public function afterFilter(EventInterface $event)
    {
        $this->getRequest()->getSession()->destroy();
    }

    /**
     * action ping
     *
     */
    public function ping()
    {
        $this->viewBuilder()->disableAutoLayout();
    }

    /**
     * action versions
     *
     */
    public function versions()
    {
        $session = $this->getRequest()->getSession();
        $saName = h(
            $session->read('ConfigArchivalAgency.header-title')
                ?: Hash::get($this->archivalAgency, 'name')
        );
        $this->viewBuilder()->disableAutoLayout();
        $this->set('application', Configure::read('App.name'));
        $this->set('denomination', $saName);
        $this->set('version', ASALAE_VERSION_LONG);
    }

    /**
     * Envoi des messages SEDA
     *
     * @return Response
     * @throws Exception
     */
    public function sedaMessages()
    {
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        // vérification de la requète
        $this->viewBuilder()->disableAutoLayout();
        $request = $this->getRequest();
        $request->allowMethod(['get', 'post']);
        try {
            if ($request->is('get')) {
                return $this->getSedaMessages();
            } else {
                $this->postSedaMessages();
            }
        } catch (InternalErrorException $e) {
            $this->log($e->getMessage());
            return $this->renderData(
                $e->getMessage(),
                $this->getResponse()->withStatus(500)
            );
        } finally {
            restore_error_handler();
        }
    }

    /**
     * Method GET sur sedaMessages
     * @throws Exception
     */
    private function getSedaMessages()
    {
        $request = $this->getRequest();
        $queryParams = $request->getQueryParams();
        if (empty($queryParams)) {
            $queryParams = [];
            foreach ($request->getParam('pass') as $passParameter) {
                if (preg_match('/^(.*):(.*)$/', $passParameter, $m)) {
                    $queryParams[$m[1]] = $m[2];
                }
            }
        }
        $this->checkRequiredParams(
            $queryParams,
            [
                'sequence' => ['ArchiveTransfer'],
                'message' => [
                    'Acknowledgement',
                    'ArchiveTransferReply',
                    'Reply',
                ],
                'originOrganizationIdentification' => null,
                'originMessageIdentifier' => null,
            ]
        );

        // On récupère le service versant (on vérifie qu'il existe)
        $taIdentifier = $queryParams['originOrganizationIdentification'];
        $transferringAgency = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->where(['OrgEntities.identifier' => $taIdentifier])
            ->first();
        if (!$transferringAgency) {
            throw new InternalErrorException(
                sprintf(
                    "Service à l'origine de la séquence non trouvé : %s",
                    h($taIdentifier)
                )
            );
        }

        // On récupère le transfert, on vérifie qu'il existe
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        /** @var Transfer $transfer */
        $transfer = $Transfers->find()
            ->where(
                [
                    'transfer_identifier' => $queryParams['originMessageIdentifier'],
                    'transferring_agency_id' => $transferringAgency->get('id'),
                ]
            )
            ->first();
        if (!$transfer) {
            throw new InternalErrorException(
                sprintf(
                    "Transfert d'archive à l'origine de la séquence non trouvé. : %s",
                    $taIdentifier
                )
            );
        }
        $accepted = $transfer->get('is_accepted');
        if (
            in_array($queryParams['message'], ['ArchiveTransferReply', 'Reply'])
            && $accepted
        ) {
            /** @var ArchivesTable $Archives */
            $Archives = $this->fetchTable('Archives');
            $archive = $Archives->find()
                ->innerJoinWith('Transfers')
                ->where(
                    [
                        'Transfers.id' => $transfer->id,
                        'Archives.state' => ArchivesTable::S_AVAILABLE,
                    ]
                )
                ->first();
            if (!$archive) {
                throw new InternalErrorException(
                    'Transfert en cours de traitement'
                );
            }
        }
        return $this->renderData(
            $transfer->createResponse($queryParams['message'])
        );
    }

    /**
     * Vérifi la cohérence des données dans data
     * @param array $data
     * @param array $params
     * @throws Exception
     */
    private function checkRequiredParams(array $data, array $params)
    {
        foreach ($params as $param => $accepted) {
            $value = Hash::get($data, $param);
            if (!$value) {
                throw new BadRequestException(
                    sprintf('Erreur de paramètre : %s est manquant', $param)
                );
            }
            if (is_array($accepted) && !in_array($value, $accepted)) {
                throw new BadRequestException(
                    sprintf(
                        "Erreur de paramètre : la valeur %s n'est pas autorisée pour %s (valeurs autorisées : %s)",
                        h($value),
                        $param,
                        implode(', ', $accepted)
                    )
                );
            }
        }
    }

    /**
     * Method POST sur sedaMessages
     * @throws Exception
     */
    private function postSedaMessages()
    {
        $request = $this->getRequest();
        /** @var UploadedFile $sedaMessage */
        $sedaMessage = $request->getUploadedFile('seda_message');
        if (!$sedaMessage) {
            throw new BadRequestException('form-data seda_message not found');
        }
        /** @var UploadedFile $attachments */
        $attachments = $request->getUploadedFile('attachments');
        if (
            $sedaMessage->getError() !== UPLOAD_ERR_OK
            || ($attachments && $attachments->getError() !== UPLOAD_ERR_OK)
            || (!$attachments && !$request->getData('send_chunked_attachments'))
        ) {
            throw new BadRequestException('upload error');
        }
        $tmpName = $sedaMessage->getStream()->getMetadata('uri');
        $xml = Transliterator::create('NFC')
            ->transliterate(file_get_contents($tmpName));
        file_put_contents($tmpName, $xml);
        $puid_infos = FileValidator::getPuid($tmpName);
        if (
            !empty($puid_infos[0]['mime']) && strpos(
                $puid_infos[0]['mime'],
                'xml'
            ) === false
        ) {
            throw new BadRequestException(
                'form-data seda_message must be a xml file'
            );
        }
        $attachments_puid_infos = [];
        if ($attachments) {
            $tmpAttachments = $attachments->getStream()->getMetadata('uri');
            $attachments_puid_infos = FileValidator::getPuid($tmpAttachments);
            if (empty($attachments_puid_infos[0]['mime'])) {
                throw new BadRequestException(
                    'form-data attachments file format cannot be determined'
                );
            }
            if (
                strpos(
                    $attachments_puid_infos[0]['mime'],
                    'zip'
                ) === false // gzip match
                && strpos($attachments_puid_infos[0]['mime'], 'tar') === false
            ) {
                throw new BadRequestException(
                    sprintf(
                        'form-data attachments (%s) must be a zip/tar.gz file',
                        $attachments_puid_infos[0]['mime']
                    )
                );
            }
        }

        // PreControl avec augmentation des limites si besoin
        $size = $sedaMessage->getSize() ?: filesize($tmpName);
        $max_execution_time = (int)($size / 100000); // 46Mo = 460 seconds
        $memory_limit = (int)($size * 10); // 46Mo = 460Mo
        if ($memory_limit > 134217728) { // > 128Mo
            ini_set('memory_limit', $memory_limit);
        }
        if ($max_execution_time > 30 && PHP_SAPI !== 'cli') {
            ini_set('max_execution_time', $max_execution_time);
            set_time_limit($max_execution_time);
        }
        if (!PreControlMessages::isValid($tmpName)) {
            throw new InternalErrorException(
                PreControlMessages::getLastErrorMessage()
            );
        }

        // Création du transfert
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        /** @var Transfer $transfer */
        $transfer = $Transfers->newEntityFromXml($tmpName);
        $Transfers->patchEntity(
            $transfer,
            [
                'state' => $Transfers->initialState,
                'is_modified' => false,
                'filename' => basename($sedaMessage->getClientFilename()),
                'created_user_id' => $this->userId,
                'tmp_xml' => $tmpName,
                'files_deleted' => false,
            ] + $transfer->toArray()
        );
        $Transfers->transition($transfer, TransfersTable::T_PREPARE);
        $conn = $Transfers->getConnection();
        $conn->begin();
        if ($transfer->get('archival_agency_id') !== $this->archivalAgencyId) {
            $transfer->setError(
                'archival_agency_id',
                __("Service d'archive différent de l'utilisateur")
            );
        }
        if ($Transfers->save($transfer)) {
            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            $entry = $EventLogs->newEntry(
                'transfer_add',
                'success',
                __(
                    "Prise en compte du transfert ''{0}''",
                    $transfer->get('transfer_identifier')
                ),
                $this->userId,
                $transfer
            );
            $entry->set(
                'in_lifecycle',
                false
            ); // ajouté dans worker archive_management
            $EventLogs->saveOrFail($entry);
            $path = $transfer->get('xml');
            if (!is_dir($dir = dirname($path))) {
                Filesystem::mkdir($dir);
            }
            try {
                $sedaMessage->moveTo($path);
            } catch (Exception $e) {
                $conn->rollback();
                Log::error(
                    sprintf('unable to write in %s', $transfer->get('xml'))
                );
                throw new InternalErrorException('write error', 500, $e);
            }
            if ($request->getData('send_chunked_attachments')) {
                $identifier = uniqid((new DateTime())->format('Ymd') . '_');
                $securityPass = bin2hex(random_bytes(16));
                $this->set('chunk_session_identifier', $identifier);
                $this->set('chunk_security_identifier', $securityPass);
                $this->createChunkDir(
                    $transfer->get('id'),
                    $identifier,
                    $securityPass
                );
            }
            if ($attachments) {
                $targetUri = AbstractGenericMessageForm::getDataDirectory(
                    $transfer->get('id')
                ) . DS . 'attachments';
                Filesystem::begin();
                DataCompressor::useTransaction();

                $limit = ($attachments->getSize() ?? filesize(
                    $tmpAttachments
                )) / 100000; // 2.5Go ~= 7h30
                if (PHP_SAPI !== 'cli') {
                    set_time_limit(max($limit, 30));
                }

                if (!strpos($attachments_puid_infos[0]['mime'], 'gzip')) {
                    DataCompressor::sanitizeZip($tmpAttachments);
                }
                $files = DataCompressor::uncompress(
                    $tmpAttachments,
                    $targetUri
                );
                try {
                    /** @var Clamav|AntivirusInterface $Antivirus */
                    $Antivirus = Utility::get(
                        Configure::read('Antivirus.classname')
                    );
                    $viruses = $Antivirus->scan($targetUri);
                } catch (Exception $e) {
                    $this->log($e->getMessage());
                    $viruses = null;
                }

                $exec = Utility::get('Exec')->command('sf -json', $targetUri);
                $json = json_decode($exec->stdout, true);
                $sf = [];
                foreach (Hash::get($json, 'files', []) as $sfFile) {
                    $format = Hash::get($sfFile, 'matches.0.id') ?: 'UNKNOWN';
                    $sf[$sfFile['filename']] = [
                        'format' => $format === 'UNKNOWN' ? '' : $format,
                        'mime' => mime_content_type($sfFile['filename']),
                        'extension' => pathinfo(
                            $sfFile['filename'],
                            PATHINFO_EXTENSION
                        ),
                    ];
                }

                $len = strlen($targetUri) + 1;
                $algo = Configure::read('hash_algo', 'sha256');
                $util = DOMUtility::load($transfer->get('xml'));

                /** @var TransferAttachmentsTable $TransferAttachments */
                $TransferAttachments = $this->fetchTable('TransferAttachments');
                foreach ($files as $attachmentFile) {
                    $filename = substr($attachmentFile, $len);
                    $exists = $TransferAttachments->find()
                        ->where(
                            [
                                'transfer_id' => $transfer->get('id'),
                                'filename' => $filename,
                            ]
                        )
                        ->count();
                    if ($exists) {
                        continue;
                    }

                    if ($viruses === null) {
                        $virus = null;
                    } elseif (isset($viruses[$attachmentFile])) {
                        $virus = $viruses[$attachmentFile];
                    } else {
                        $virus = '';
                    }

                    $node = ArchiveBinariesTable::findBinaryByFilename($util, $filename);
                    $xpath = $node ? $util->getElementXpath($node, '') : null;

                    $entity = $TransferAttachments->newEntity(
                        [
                            'transfer_id' => $transfer->get('id'),
                            'filename' => $filename,
                            'size' => filesize($attachmentFile),
                            'hash' => hash_file($algo, $attachmentFile),
                            'hash_algo' => $algo,
                            'deletable' => true,
                            'mime' => mime_content_type($attachmentFile),
                            'extension' => $sf[$attachmentFile]['extension'],
                            'format' => $sf[$attachmentFile]['format'],
                            'virus_name' => $virus,
                            'xpath' => $xpath,
                            'valid' => Utility::get(FileValidator::class)
                                ->isValid($attachmentFile),
                        ]
                    );
                    if (!$TransferAttachments->save($entity)) {
                        Filesystem::rollback();
                        $conn->rollback();
                        Log::error(
                            sprintf(
                                "TransferAttachment save error: %s",
                                json_encode(
                                    $entity->getErrors(),
                                    JSON_PRETTY_PRINT
                                )
                            )
                        );
                        throw new InternalErrorException(
                            'unable to create a transfer attachment'
                        );
                    }
                }
                $this->sendTransfer($transfer->get('id'));
                Filesystem::commit();
            }
            $conn->commit();
        } else {
            $conn->rollback();
            Log::error(
                "Transfer save error: " . json_encode(
                    $transfer->getErrors(),
                    JSON_PRETTY_PRINT
                )
            );
            throw new InternalErrorException('unable to create a transfer');
        }
    }

    /**
     * Créer un dossier pour les morceaux de fichier d'une instance d'upload
     * @param int    $transfer_id
     * @param string $identifier
     * @param string $securityPass
     * @throws Exception
     */
    private function createChunkDir(
        int $transfer_id,
        string $identifier,
        string $securityPass
    ) {
        $security = new DefaultPasswordHasher();
        $data = [
            'created' => new DateTime(),
            'transfer_id' => $transfer_id,
            'security_pass' => $security->hash($securityPass),
        ];
        Filesystem::dumpFile(
            $this->getChunkDir($identifier) . DS . 'chunks.json',
            json_encode($data)
        );
    }

    /**
     * Donne le chemin vers le dossier des uploads par morceaux
     *
     * @param string $identifier
     * @return string
     */
    private function getChunkDir(string $identifier): string
    {
        $baseDir = Configure::read(
            'Restservices.chunk_dir',
            Configure::read('App.paths.data') . DS . 'tmp'
        );
        $userDir = $baseDir . DS . 'user_' . $this->userId;
        return $userDir . DS . $identifier;
    }

    /**
     * Comme Transfers::send
     * @param int|string $id
     * @throws Exception
     */
    private function sendTransfer($id)
    {
        /** @var Beanstalk $Beanstalk */
        $Beanstalk = Utility::get('Beanstalk');
        if (!$Beanstalk->isConnected()) {
            throw new ServiceUnavailableException();
        }

        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        /** @var Transfer $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.id' => $id,
                    'Transfers.state' => TransfersTable::S_PREPARATING,
                ]
            )
            ->contain(
                [
                    'ServiceLevels' => ['TsMsgs', 'TsPjss', 'TsConvs'],
                ]
            )
            ->firstOrFail();

        $serviceLevel = $entity->get('service_level');
        if (!$serviceLevel) {
            $serviceLevel = $this->Seal->archivalAgency()
                ->table('ServiceLevels')
                ->find('seal')
                ->where(['ServiceLevels.default_level' => true])
                ->contain(['TsMsgs', 'TsPjss', 'TsConvs'])
                ->first();
        }

        /** @var Beanstalk $Beanstalk */
        if (
            $serviceLevel && ($serviceLevel->get(
                'ts_msg_id'
            ) || $serviceLevel->get('ts_pjs_id'))
        ) {
            $job = [
                'transfer_id' => $entity->get('id'),
                'user_id' => $this->userId,
                'ts_msg_id' => Hash::get(
                    $serviceLevel,
                    'ts_msg.timestamper_id'
                ),
                'ts_pjs_id' => Hash::get(
                    $serviceLevel,
                    'ts_pjs.timestamper_id'
                ),
                'ts_conv_id' => Hash::get(
                    $serviceLevel,
                    'ts_conv.timestamper_id'
                ),
            ];
            $Transfers->transition($entity, TransfersTable::T_TIMESTAMP);
            $Transfers->saveOrFail($entity);
            $Beanstalk->setTube('timestamp');
        } else {
            $job = [
                'transfer_id' => $entity->get('id'),
                'user_id' => $this->userId,
            ];
            $Transfers->transition($entity, TransfersTable::T_ANALYSE);
            $Transfers->saveOrFail($entity);
            $Beanstalk->setTube('analyse');
        }
        $Beanstalk->emit($job, Pheanstalk::DEFAULT_PRIORITY, 1);
    }

    /**
     * Action pour l'envoi de fichiers morcelés
     *
     * @throws Exception
     * @throws NotFoundException
     * @throws UnauthorizedException
     */
    public function sedaAttachmentsChunkFiles()
    {
        $this->viewBuilder()->disableAutoLayout();
        $request = $this->getRequest();
        $request->allowMethod(['post', 'delete']);
        $dir = $this->getChunkDir($request->getData('session_identifier'));
        if (
            !is_readable(
                $info = $dir . DS . 'chunks.json'
            ) || !($info = json_decode(file_get_contents($info)))
        ) {
            throw new NotFoundException();
        }
        $security = new DefaultPasswordHasher();
        if (
            !$security->check(
                $request->getData('security_identifier'),
                $info->security_pass
            )
        ) {
            throw new UnauthorizedException();
        }
        if ($request->is('delete')) {
            if (is_dir($dir)) {
                Filesystem::remove($dir);
            }
            $this->viewBuilder()->setTemplate('deleteChunks');
            return;
        }

        $maxlen = strlen($request->getData('number_of_files', 1));
        $filePrefix = sprintf(
            "%0{$maxlen}d",
            $request->getData('file_index')
        ) . '_';

        $maxlen = strlen($request->getData('number_of_chunks'));
        $chunk = sprintf("%0{$maxlen}d", $request->getData('chunk_index'));

        $targetFilename = $dir . DS . 'chunks' . DS . $filePrefix . $chunk;
        if (is_file($targetFilename)) {
            throw new InternalErrorException('file already exists');
        }
        /** @var UploadedFile $file */
        $file = $request->getUploadedFile('chunk_content');
        if ($file) {
            if (!is_dir($targetdir = dirname($targetFilename))) {
                Filesystem::mkdir($targetdir);
            }
            try {
                $file->moveTo($targetFilename);
            } catch (Exception $e) {
                Log::error(sprintf('unable to write in %s', $targetFilename));
                throw new InternalErrorException('write error', 500, $e);
            }
        } else {
            $content = $request->getData('chunk_content');
            if (is_array($content)) {
                $content = file_get_contents($content['tmp_name']);
            }
            Filesystem::dumpFile($targetFilename, $content);
        }

        $completed = false;
        if (
            count(
                glob($dir . DS . 'chunks' . DS . $filePrefix . '*')
            ) === (int)$request->getData('number_of_chunks')
        ) {
            $completed = true;
            $filename = $request->getData('file_name');
            $this->regroupChunkedFile($dir, $filename, $filePrefix);
            if (
                ($request->getData(
                    'compression_algorithm',
                    'NONE'
                )) !== 'NONE'
            ) {
                $fh = fopen($dir . DS . 'to_uncompress.list', 'a');
                fwrite($fh, $dir . DS . 'files' . DS . $filename . PHP_EOL);
                fclose($fh);
            }
        }

        $all_files_uploaded = false;
        if (
            $completed
            && count(
                glob($dir . DS . 'files' . DS . '*')
            ) === (int)$request->getData('number_of_files', 1)
        ) {
            /** @var TransferAttachmentsTable $TransferAttachments */
            $TransferAttachments = $this->fetchTable('TransferAttachments');
            $conn = $TransferAttachments->getConnection();
            $conn->begin();
            $all_files_uploaded = true;
            $uncompressList = [];
            if (is_file($dir . DS . 'to_uncompress.list')) {
                $uncompressList = explode(
                    PHP_EOL,
                    trim(file_get_contents($dir . DS . 'to_uncompress.list'))
                );
            }
            Filesystem::begin();
            Filesystem::setSafemode(true);
            Filesystem::addToDeleteIfRollback($dir . DS . 'files');
            foreach ($uncompressList as $compressed) {
                $limit = filesize($compressed) / 100000; // 2.5Go ~= 7h30
                if (PHP_SAPI !== 'cli') {
                    set_time_limit(max($limit, 30));
                }
                if (preg_match('/\.zip$/i', $compressed)) {
                    DataCompressor::sanitizeZip($compressed);
                }
                $uncompressedSubDir = uniqid();
                $files = DataCompressor::uncompress(
                    $compressed,
                    $dir . DS . 'files' . DS . $uncompressedSubDir
                );
                $len = mb_strlen(
                    $dir . DS . 'files' . DS . $uncompressedSubDir . DS
                );
                if (!$files) {
                    throw new InternalErrorException(
                        'unable to uncompress file'
                    );
                }
                unlink($compressed);
                foreach ($files as $filePath) {
                    $npath = $dir . DS . 'files' . DS . mb_substr(
                        $filePath,
                        $len
                    );
                    if (!is_dir(dirname($npath))) {
                        mkdir(dirname($npath), 0777, true);
                    }
                    rename($filePath, $npath);
                }
            }
            $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($dir . DS . 'files')
            );
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $transfer = $Transfers->get($info->transfer_id);
            $util = DOMUtility::load($transfer->get('xml'));
            /** @var SplFileInfo $file */
            foreach ($iterator as $file) {
                if ($file->isDir()) {
                    continue;
                }
                $filename = $file->getPathname();
                $attachment = $this->addTransferAttachment(
                    $info->transfer_id,
                    $dir . DS . 'files',
                    $file,
                    $util
                );
                if (!$attachment) {
                    $conn->rollback();
                    throw new InternalErrorException(
                        'unable to create transfer_attachment'
                    );
                }
                Filesystem::rename($filename, $attachment->get('path'));
            }
            $this->sendTransfer($info->transfer_id);
            $conn->commit();
            Filesystem::remove($dir);
            Filesystem::commit();
        }
        $this->set('all_files_uploaded', $all_files_uploaded);
        $this->set('completed', $completed);
    }

    /**
     * Fusionne les fichiers morcelés
     *
     * @param string $dir
     * @param string $filename
     * @param string $filePrefix
     * @throws Exception
     */
    private function regroupChunkedFile(
        string $dir,
        string $filename,
        string $filePrefix
    ) {
        $targetDir = dirname($dir . DS . 'files' . DS . $filename);
        if (!is_dir($targetDir)) {
            Filesystem::mkdir($targetDir);
        }
        $file = fopen($dir . DS . 'files' . DS . $filename, 'w');
        if (!$file) {
            throw new InternalErrorException('unable to open file');
        }
        foreach (glob($dir . DS . 'chunks' . DS . $filePrefix . '*') as $part) {
            $chunk = fopen($part, 'r');
            while (!feof($chunk)) {
                $buffer = fread($chunk, 4096);
                if ($buffer === false) {
                    throw new InternalErrorException('unable to read file');
                }
                if (fwrite($file, $buffer) === false) {
                    throw new InternalErrorException('unable to write file');
                }
            }
            fclose($chunk);
            unlink($part);
        }
        fclose($file);
    }

    /**
     * Ajoute un fichier lié à un transfert
     *
     * @param int        $transfer_id
     * @param string     $baseDir
     * @param string     $file
     * @param DOMUtility $util
     * @return bool|EntityInterface
     * @throws Exception
     */
    private function addTransferAttachment(
        int $transfer_id,
        string $baseDir,
        string $file,
        DOMUtility $util
    ) {
        $len = strlen($baseDir) + 1;
        $algo = Configure::read('hash_algo', 'sha256');

        $filename = substr($file, $len);
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        $exists = $TransferAttachments->find()
            ->where(
                [
                    'transfer_id' => $transfer_id,
                    'filename' => $filename,
                ]
            )
            ->count();
        if ($exists) {
            return true;
        }
        $exec = Utility::get('Exec')->command('sf -json', $file);
        $json = json_decode($exec->stdout, true);
        $sf = Hash::get($json, 'files.0.matches.0', []);

        try {
            /** @var Clamav|AntivirusInterface $Antivirus */
            $Antivirus = Utility::get(Configure::read('Antivirus.classname'));
            $virus = current($Antivirus->scan($file));
            if ($virus === 'OK') {
                $virus = '';
            }
        } catch (\Exception $e) {
            $this->log($e->getMessage());
            $virus = null;
        }
        $format = Hash::get($sf, 'id');
        if ($format === 'UNKNOWN') {
            $format = '';
        }

        $node = ArchiveBinariesTable::findBinaryByFilename($util, $filename);
        $xpath = $node ? $util->getElementXpath($node, '') : null;

        $entity = $TransferAttachments->newEntity(
            [
                'transfer_id' => $transfer_id,
                'filename' => $filename,
                'size' => filesize($file),
                'hash' => hash_file($algo, $file),
                'hash_algo' => $algo,
                'deletable' => true,
                'mime' => mime_content_type($file),
                'extension' => pathinfo($file, PATHINFO_EXTENSION),
                'format' => $format,
                'virus_name' => $virus,
                'xpath' => $xpath,
                'valid' => Utility::get(FileValidator::class)->isValid($file),
            ]
        );
        return $TransferAttachments->save($entity);
    }

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return ['sedaAttachmentsChunkFiles', 'sedaMessages'];
    }
}
