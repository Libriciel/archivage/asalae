<?php

/**
 * Asalae\Controller\AuthUrlsController
 */

namespace Asalae\Controller;

use Asalae\Model\Table\AuthUrlsTable;
use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Event\EventInterface;
use Cake\Http\Response;
use Cake\Http\Response as CakeResponse;
use Exception;

/**
 * Permet d'accèder à certaines parties de l'application avec une simple url
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AuthUrlsTable $AuthUrls
 */
class AuthUrlsController extends AppController
{
    /**
     * Désactive l'autentification par défaut
     * @param EventInterface $event An Event instance
     * @return CakeResponse|void|null
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $AuthenticationComponent->allowUnauthenticated(
            [$this->getRequest()->getParam('action')]
        );
        return parent::beforeFilter($event);
    }

    /**
     * Utilise le code et redirige vers l'url correspondant
     * @param string $code
     * @return Response|null|void
     * @throws Exception
     * @fixme Déplacer dans un Middleware ou Authenticator/Identifier
     */
    public function activate(string $code)
    {
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $this->fetchTable('AuthUrls');
        $entity = $AuthUrls->find()
            ->where(['code' => $code])
            ->first();
        if (!$entity) {
            $this->setResponse($this->getResponse()->withStatus(410)); // Gone
            $this->viewBuilder()->setLayout('not_connected');
            $this->viewBuilder()->setTemplate('gone');
            $this->Flash->success(
                __("La ressource demandée n'est plus disponible")
            );
            return;
        }
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $AuthenticationComponent->logout();
        $auth = [
            'code' => $code,
            'url' => $entity->get('url'),
            'url_id' => $entity->id,
        ];
        $session = $this->getRequest()->getSession();
        $session->clear(true);
        $session->write('Auth', $auth);
        return $this->redirect($entity->get('url'))->withHeader('code', $code);
    }
}
