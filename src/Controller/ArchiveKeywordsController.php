<?php

/**
 * Asalae\Controller\ArchiveKeywordsController
 */

namespace Asalae\Controller;

use Asalae\Model\Entity\ArchiveKeyword;
use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Entity\PublicDescriptionArchiveFile;
use Asalae\Model\Table\AccessRuleCodesTable;
use Asalae\Model\Table\ArchiveKeywordsTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\PublicDescriptionArchiveFilesTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Volume\VolumeManager;
use Asalae\View\Helper\TranslateHelper;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Error\ErrorTrap;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Form\MessageSchema\MessageSchema;
use AsalaeCore\Form\MessageSchema\Seda02Schema;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Form\MessageSchema\Seda21Schema;
use AsalaeCore\Form\MessageSchema\Seda22Schema;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\FormatError;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\HttpException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\Log\Log;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\View\View;
use DateTime;
use DOMAttr;
use DOMElement;
use DOMException;
use Exception;

/**
 * ArchiveKeywords
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchivesTable        Archives
 * @property ArchiveKeywordsTable ArchiveKeywords
 * @property AccessRuleCodesTable AccessRuleCodes
 * @property EventLogsTable       EventLogs
 */
class ArchiveKeywordsController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * @var DOMUtility
     */
    private $util;

    /**
     * Ajoute un mot clé à une archive_unit
     * @param string $archive_unit_id
     * @return Response
     * @throws VolumeException
     */
    public function add(string $archive_unit_id)
    {
        $archiveUnit = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find('seal')
            ->innerJoinWith('Archives')
            ->where(['ArchiveUnits.id' => $archive_unit_id])
            ->contain(
                [
                    'ArchiveDescriptions' => [
                        'AccessRules' => ['AccessRuleCodes'],
                    ],
                    'Archives' => [
                        'Transfers',
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                    ],
                ]
            )
            ->firstOrFail();
        /** @var EntityInterface $archive */
        $archive = $archiveUnit->get('archive');
        $util = $this->getUtilFromArchive($archive);
        $archiveUnitNode = $util->xpath->query($archiveUnit->get('xpath'))
            ->item(0);
        $schemaClassname = MessageSchema::getSchemaClassname($util->namespace);
        /** @var Seda02Schema|Seda10Schema|Seda21Schema|Seda22Schema $schemaClassname */
        $keywordsTagname = $schemaClassname::getTagname('Keyword', 'Archive');
        $descriptionTagname = $schemaClassname::getTagname(
            'ContentDescription',
            'Archive'
        );
        $count = $util->xpath->query(
            "ns:$descriptionTagname/ns:$keywordsTagname",
            $archiveUnitNode
        )->count();
        /** @var ArchiveKeywordsTable $ArchiveKeywords */
        $ArchiveKeywords = $this->fetchTable('ArchiveKeywords');
        $entity = $ArchiveKeywords->newEntity(
            [
                'use_parent_access_rule' => true,
                'archive_unit_id' => $archiveUnit->id,
                'access_rule_id' => Hash::get(
                    $archiveUnit,
                    'archive_description.access_rule_id'
                ),
                'xml_node_tagname' => $keywordsTagname . '[' . ($count + 1) . ']',
            ],
            ['validate' => false]
        );
        $entity->set(
            'access_rule',
            Hash::get($archiveUnit, 'archive_description.access_rule')
        );
        if ($this->getRequest()->is('post')) {
            $conn = $ArchiveKeywords->getConnection();
            $conn->begin();
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            try {
                $this->saveNewKeyword($entity, $archiveUnit, $archiveUnitNode);
                $conn->commit();
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } catch (Exception $e) {
                Log::error((string)$e);
                $this->Flash->error($e->getMessage());
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
        $this->set('sedaVersion', Hash::get($archive, 'transfers.0.message_version'));

        // options
        $schema = $this->getSchemaClassname($archive);
        $this->set('types', $schema::getXsdOptions('keywordtype'));
        $this->setCodes($schema);
    }

    /**
     * Donne 'Seda02Schema' ou 'Seda10Schema'
     * @param EntityInterface $archive
     * @return Seda02Schema|Seda10Schema|string
     */
    private function getSchemaClassname(EntityInterface $archive): string
    {
        $norme = array_search(
            Hash::get($archive, 'transfers.0.message_version'),
            TransfersTable::NORMES_SEDA
        );
        switch ($norme) {
            case NAMESPACE_SEDA_02:
                return Seda02Schema::class;
            default:
            case NAMESPACE_SEDA_10:
                return Seda10Schema::class;
        }
    }

    /**
     * Met à jour la description avec un nouveau keyword
     * @param EntityInterface $entity
     * @param EntityInterface $archiveUnit
     * @param DOMElement      $archiveUnitNode
     * @throws DOMException
     * @throws VolumeException
     */
    private function saveNewKeyword(
        EntityInterface $entity,
        EntityInterface $archiveUnit,
        DOMElement $archiveUnitNode
    ) {
        /** @var EntityInterface $archive */
        $archive = $archiveUnit->get('archive');
        $data = $this->getRequest()->getData();
        /** @var ArchiveKeywordsTable $ArchiveKeywords */
        $ArchiveKeywords = $this->fetchTable('ArchiveKeywords');
        $ArchiveKeywords->patchEntity(
            $entity,
            $data + $entity->toArray()
        );
        /** @var EntityInterface $access */
        $access = $entity->get('access_rule');
        // nécessaire pour un bon appendKeywordNode
        if (!$entity->get('use_parent_access_rule') && $access->isDirty('access_rule_code_id')) {
            /** @var AccessRuleCodesTable $AccessRuleCodes */
            $AccessRuleCodes = $this->fetchTable('AccessRuleCodes');
            $access->set(
                'access_rule_code',
                $AccessRuleCodes->get($access->get('access_rule_code_id'))
            );
        }
        $ArchiveKeywords->saveOrFail($entity);
        $this->appendKeywordNode($entity, $archiveUnit, $archiveUnitNode);
        $util = $this->getUtilFromArchive($archive);
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        if (!@$util->dom->schemaValidate($Archives->schemas[$util->namespace])) {
            throw new Exception(__("Echec lors de la validation du schéma"));
        }
        $ArchiveUnits->saveAsync($archiveUnit, $util, false);
        $ArchiveUnits->updateSearchField(
            ['ArchiveUnits.id' => $archiveUnit->id]
        );
        $ArchiveUnits->updateFullSearchField(
            ['ArchiveUnits.id' => $archiveUnit->id]
        );
        if (!$entity->get('use_parent_access_rule')) {
            $ArchiveUnits->updateMaxKeywordsAccessEndDate($archiveUnit);
            $ArchiveUnits->updateMaxAggregatedAccessEndDate($archiveUnit);
        }
        /** @var PublicDescriptionArchiveFile $archiveFile */
        $archiveFile = Hash::get(
            $archive,
            'archive.public_description_archive_file'
        );
        if ($archiveFile) {
            /** @var PublicDescriptionArchiveFilesTable $PublicDescriptionArchiveFiles */
            $PublicDescriptionArchiveFiles = $this->fetchTable('PublicDescriptionArchiveFiles');
            $PublicDescriptionArchiveFiles->deleteOrFail($archiveFile);
            $manager = new VolumeManager($archive->get('secure_data_space_id'));
            $manager->fileDelete(Hash::get($archiveFile, 'stored_file.name'));
            $archive->set('next_pubdesc');
            $Archives->saveOrFail($archive);
        }
        $translateHelper = new TranslateHelper(new View());
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        $entry = $EventLogs->newEntry(
            'add_' . Inflector::singularize($this->getName()),
            'success',
            __(
                "Ajouté ({0}) par l'utilisateur ({1})",
                $translateHelper->permission($this->getName()),
                Hash::get($this->user, 'username')
            ),
            $this->userId,
            $entity
        );
        $entry->set('in_lifecycle', false);
        $EventLogs->saveOrFail($entry);
        /** @var EntityInterface $archive */
        $storedFile = Hash::get(
            $archive,
            'lifecycle_xml_archive_file.stored_file',
            []
        );
        if (!$Archives->updateLifecycleXml($storedFile, $entry, true)) {
            throw new Exception(
                __(
                    "Impossible de modifier le cycle de vie, une erreur a eu lieu lors de l'enregistrement"
                )
            );
        }
    }

    /**
     * Ajoute un mot clé selon $entity
     * @param EntityInterface $entity
     * @param EntityInterface $archiveUnit
     * @param DOMElement      $archiveUnitNode
     * @throws VolumeException
     * @throws DOMException
     */
    private function appendKeywordNode(
        EntityInterface $entity,
        EntityInterface $archiveUnit,
        DOMElement $archiveUnitNode
    ) {
        /** @var EntityInterface $archive */
        $archive = $archiveUnit->get('archive');
        $data = $this->getRequest()->getData();
        $util = $this->getUtilFromArchive($archive);
        /** @var DOMElement $parentNode */
        $parentNode = $util->xpath->query(
            ArchiveKeyword::DOM_REL_APPEND_CONTAINER[$util->namespace],
            $archiveUnitNode
        )->item(0);
        if (empty($parentNode)) {
            throw new Exception();
        }
        $tagName = ArchiveKeyword::DOM_TAG_NAME[$util->namespace];
        $keyword = AbstractGenericMessageForm::smartAppendNode(
            $parentNode,
            $tagName,
            $archive->get('archival_agency_id')
        );

        if (!$entity->get('use_parent_access_rule')) {
            $access = $util->dom->createElementNS(
                $util->namespace,
                ArchiveKeyword::DOM_ACCESS_TAG_NAME[$util->namespace]
            );
            $code = $util->dom->createElementNS(
                $util->namespace,
                'Code'
            );
            $code->appendChild($util->createTextNode(Hash::get($entity, 'access_rule.access_rule_code.code')));
            if ($util->namespace === NAMESPACE_SEDA_02) {
                $code->setAttribute('listVersionID', 'edition 2009');
            }
            $access->appendChild($code);
            /** @var DateTime $date */
            $date = Hash::get($entity, 'access_rule.start_date');
            $start = $util->dom->createElementNS(
                $util->namespace,
                'StartDate'
            );
            $start->appendChild($util->createTextNode($date->format('Y-m-d')));
            $access->appendChild($start);
            if ($util->namespace === NAMESPACE_SEDA_02) {
                $keyword->appendChild($access);
            }
        }

        $content = $util->dom->createElementNS(
            $util->namespace,
            'KeywordContent'
        );
        DOMUtility::setValue($content, $entity->get('content'));
        foreach ($data as $field => $value) {
            if (
                $value && substr(
                    $field,
                    0,
                    strlen('content_')
                ) === 'content_'
            ) {
                [, $attr] = explode('_', $field);
                $content->setAttribute($attr, $value);
            }
        }
        $keyword->appendChild($content);

        $reference = $util->dom->createElementNS(
            $util->namespace,
            'KeywordReference'
        );
        DOMUtility::setValue($reference, $entity->get('reference'));
        $empty = !$entity->get('reference');
        foreach ($data as $field => $value) {
            if (
                $value && substr(
                    $field,
                    0,
                    strlen('reference_')
                ) === 'reference_'
            ) {
                [, $attr] = explode('_', $field);
                $reference->setAttribute($attr, $value);
                $empty = false;
            }
        }
        if (!$empty) {
            $keyword->appendChild($reference);
        }

        $type = $util->dom->createElementNS($util->namespace, 'KeywordType');
        DOMUtility::setValue($type, $entity->get('type'));
        $empty = !$entity->get('type');
        foreach ($data as $field => $value) {
            if ($value && substr($field, 0, strlen('type_')) === 'type_') {
                [, $attr] = explode('_', $field);
                $type->setAttribute($attr, $value);
                $empty = false;
            }
        }
        if (!$empty) {
            if (
                $util->namespace === NAMESPACE_SEDA_02 && !$type->getAttribute(
                    'listVersionID'
                )
            ) {
                $type->setAttribute('listVersionID', 'edition 2009');
            }
            $keyword->appendChild($type);
        }
        if (isset($access) && $util->namespace !== NAMESPACE_SEDA_02) {
            $keyword->appendChild($access);
        }
    }

    /**
     * Donne un DOMUtility à partir d'une entité d'archive
     * @param EntityInterface $archive
     * @return DOMUtility
     * @throws VolumeException
     */
    private function getUtilFromArchive(EntityInterface $archive): DOMUtility
    {
        if (empty($this->util)) {
            /** @var EntityInterface $storedFile */
            $storedFile = Hash::get(
                $archive,
                'description_xml_archive_file.stored_file'
            );
            $stored_file_id = Hash::get(
                $archive,
                'description_xml_archive_file.stored_file.id'
            );
            $manager = VolumeManager::createWithStoredFileId($stored_file_id);
            $basedir = Configure::read(
                'Archives.updateDescription.path',
                TMP . 'update_description'
            );
            if (!is_dir($basedir)) {
                mkdir($basedir, 0700, true);
            }
            $tmpFile = $basedir . DS . $stored_file_id . '-description.xml';
            if (!is_file($tmpFile)) {
                $manager->fileDownload(
                    Hash::get($storedFile, 'name'),
                    $tmpFile
                );
            }
            $this->util = DOMUtility::load($tmpFile);
        }
        return $this->util;
    }

    /**
     * Envoi les codes à la vue
     * @param Seda02Schema|Seda10Schema|string $schema
     */
    private function setCodes(string $schema)
    {
        /** @var AccessRuleCodesTable $AccessRuleCodes */
        $AccessRuleCodes = $this->fetchTable('AccessRuleCodes');
        $list = $AccessRuleCodes->find(
            'list',
            keyField: 'code',
            valueField: 'id'
        )
            ->orderBy(['code' => 'asc'])
            ->all()
            ->toArray();
        $codes = [];
        // remplace le value(code) par un value(id) pour assignation automatique de la valeur de l'entité
        foreach ($schema::getXsdOptions('access_code') as $args) {
            if (isset($list[$args['value']])) {
                $codes[] = [
                    'value' => $list[$args['value']],
                    'title' => $args['title'],
                    'text' => $args['text'],
                ];
            }
        }
        $this->set('codes', $codes);
    }

    /**
     * Action modifier
     * @param string $id
     * @return Response
     * @throws VolumeException
     */
    public function edit(string $id)
    {
        $entity = $this->Seal->archivalAgency()
            ->table('ArchiveKeywords')
            ->find('seal')
            ->where(['ArchiveKeywords.id' => $id])
            ->contain(
                [
                    'AccessRules' => [
                        'AccessRuleCodes',
                    ],
                    'ArchiveUnits' => [
                        'Archives' => [
                            'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                            'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                            'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                            'Transfers',
                        ],
                        'ArchiveDescriptions',
                    ],
                ]
            )
            ->firstOrFail();
        /** @var EntityInterface $archive */
        $archive = Hash::get($entity, 'archive_unit.archive');
        $util = $this->getUtilFromArchive($archive);
        $node = $this->findKeywordNode(
            $entity,
            $util
        ); // @throw NotFoundException

        // Sauvegarde
        if ($this->getRequest()->is('put')) {
            /** @var ArchiveKeywordsTable $ArchiveKeywords */
            $ArchiveKeywords = $this->fetchTable('ArchiveKeywords');
            $conn = $ArchiveKeywords->getConnection();
            $conn->begin();
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            try {
                $this->saveEditKeyword($entity, $node);
                $conn->commit();
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } catch (Exception $e) {
                Log::error((string)$e);
                $this->Flash->error(
                    nl2br($e->getMessage()),
                    ['escape' => false]
                );
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
        $this->set('sedaVersion', Hash::get($archive, 'transfers.0.message_version'));

        // options
        $schema = $this->getSchemaClassname(
            Hash::get($entity, 'archive_unit.archive')
        );
        $this->set('types', $schema::getXsdOptions('keywordtype'));
        $this->setCodes($schema);
    }

    /**
     * Localise un mot clé (node) en fonction du content dans $entity
     * @param EntityInterface|ArchiveKeyword $entity
     * @param DOMUtility                     $util
     * @return DOMElement
     */
    private function findKeywordNode(
        EntityInterface $entity,
        DOMUtility $util
    ): DOMElement {
        $archiveUnit = $util->node(Hash::get($entity, 'archive_unit.xpath'));
        $query = $util->xpath->query(
            ArchiveKeyword::DOM_REL_APPEND_PATH[$util->namespace],
            $archiveUnit
        );
        $node = null;
        foreach ($query as $element) {
            if (!$element instanceof DOMElement) {
                continue;
            }
            if ($element->nodeValue === $entity->get('content')) {
                $node = $element->parentNode;
                $entity->loadDOMElement($node);
                break;
            }
        }
        if ($node === null) {
            throw new NotFoundException('keyword not found in xml');
        }
        return $node;
    }

    /**
     * Met à jour un mot clé
     * @param EntityInterface $entity
     * @param DOMElement      $node
     * @throws DOMException
     * @throws VolumeException
     */
    private function saveEditKeyword(EntityInterface $entity, DOMElement $node)
    {
        /**
         * On cherche le noeud qui correspond à l'entité
         * @var EntityInterface           $archive_unit
         * @var EntityInterface           $archive
         * @var DescriptionXmlArchiveFile $desc
         */
        $archive_unit = $entity->get('archive_unit');
        $archive = $archive_unit->get('archive');
        $util = $this->getUtilFromArchive($archive);

        $data = $this->getRequest()->getData();
        $initialUseParentAccessRule = $entity->get('use_parent_access_rule');
        /** @var ArchiveKeywordsTable $ArchiveKeywords */
        $ArchiveKeywords = $this->fetchTable('ArchiveKeywords');
        $ArchiveKeywords->patchEntity($entity, $data);
        if (
            $entity->get(
                'use_parent_access_rule'
            ) === $initialUseParentAccessRule
        ) {
            $entity->setDirty(
                'use_parent_access_rule',
                false
            );// dirty bug sur champs virtuel
        }
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        $ArchiveKeywords->saveOrFail($entity);
        $isDirty = $this->entityToXml($entity, $util, $node);
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        if ($isDirty) {
            $errHandler = ErrorTrap::captureErrors(
                function (DOMUtility $util) {
                    return MessageSchema::validate($util->dom);
                },
                $util
            );
            if (!$errHandler['result']) {
                $output = array_map(
                    function ($v) {
                        return $v['description'];
                    },
                    $errHandler['errors']
                );
                throw new Exception(
                    __(
                        "Impossible de modifier la description, une erreur inattendue a eu lieu"
                    )
                    . "\n - " . implode("\n - ", $output)
                );
            }
            /** @var EntityInterface $archiveUnit */
            $archiveUnit = $entity->get('archive_unit');
            /** @var ArchiveUnitsTable $ArchiveUnits */
            $ArchiveUnits = $this->fetchTable('ArchiveUnits');
            if (!$entity->get('use_parent_access_rule')) {
                $ArchiveUnits->updateMaxKeywordsAccessEndDate($archiveUnit);
                $ArchiveUnits->updateMaxAggregatedAccessEndDate($archiveUnit);
            }
            $ArchiveUnits->saveAsync($archiveUnit, $util, false);

            $translateHelper = new TranslateHelper(new View());
            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            $entry = $EventLogs->newEntry(
                'edit_' . Inflector::singularize($this->getName()),
                'success',
                __(
                    "Modifié ({0}) par l'utilisateur ({1})",
                    $translateHelper->permission($this->getName()),
                    Hash::get($this->user, 'username')
                ),
                $this->userId,
                $entity
            );
            $entry->set('in_lifecycle', false);
            $EventLogs->saveOrFail($entry);
            /** @var EntityInterface $archive */
            $storedFile = Hash::get(
                $archive,
                'lifecycle_xml_archive_file.stored_file',
                []
            );
            if (
                !$Archives->updateLifecycleXml(
                    $storedFile,
                    $entry,
                    true
                )
            ) {
                throw new Exception(
                    __(
                        "Impossible de modifier le cycle de vie, une erreur a eu lieu lors de l'enregistrement"
                    )
                );
            }
        }
    }

    /**
     * Met à jour le xml en fonction des valeurs dans entité
     * @param EntityInterface $entity
     * @param DOMUtility      $util
     * @param DOMElement      $node
     * @return bool dirty (vrai si xml modifié)
     * @throws Exception
     */
    private function entityToXml(
        EntityInterface $entity,
        DOMUtility $util,
        DOMElement $node
    ): bool {
        $dirty = false;
        $tagnames = ['content', 'reference', 'type'];
        foreach ($entity->toArray() as $field => $value) {
            if (strpos($field, '_')) {
                [$tagname, $attribute] = explode('_', $field);
            } else {
                $tagname = $field;
                $attribute = null;
            }
            if (!in_array($tagname, $tagnames)) {
                continue;
            }
            $value = (string)$value;
            $tagname = 'Keyword' . ucfirst($tagname);
            /** @var DOMElement $n */
            $n = $util->xpath->query('ns:' . $tagname, $node)->item(0);
            if (!$n) {
                if ($value) {
                    $dirty = true;
                }
                $n = $util->appendChild(
                    $node,
                    $util->createElement($tagname, $value)
                );
            }
            if ($attribute) {
                $old = $n->getAttribute($attribute);
                if ($old !== $value) {
                    $dirty = true;
                    $n->setAttribute($attribute, $value);
                }
                if (
                    $n->hasAttribute($attribute) && !$n->getAttribute(
                        $attribute
                    )
                ) {
                    $dirty = true;
                    $n->removeAttribute($attribute);
                }
            } else {
                $old = $n->nodeValue;
                if ($old !== $value) {
                    $dirty = true;
                    DOMUtility::setValue($n, $value);
                }
            }
            // force la valeur de KeywordType@listVersionID en seda 0.2
            if (
                $tagname === 'KeywordType'
                && $util->namespace === NAMESPACE_SEDA_02
                && !$n->getAttribute('listVersionID')
            ) {
                $n->setAttribute('listVersionID', 'edition 2009');
            }
        }
        // supprime les noeuds vides (sans valeur ni attributs)
        $emptyNodes = $util->xpath->query(
            '(ns:KeywordContent|ns:KeywordReference|ns:KeywordType)[not(normalize-space())]',
            $node
        );
        foreach ($emptyNodes as $n) {
            /** @var DOMAttr $attr */
            foreach ($n->attributes as $attr) {
                if (!$attr->nodeValue) {
                    $n->removeChild($attr);
                }
            }
            if (!$n->attributes->count()) {
                $n->parentNode->removeChild($n);
            }
        }
        $accessTagname = ArchiveKeyword::DOM_ACCESS_TAG_NAME[$util->namespace];
        $n = $util->xpath->query('ns:' . $accessTagname, $node)->item(0);
        if (!$entity->get('use_parent_access_rule')) {
            if (!$n) {
                $n = $util->dom->createElementNS(
                    $util->namespace,
                    $accessTagname
                );
                if ($accessTagname === 'AccessRestriction') { // seda 0.2
                    $node->insertBefore($n, $node->firstChild);
                } else {
                    $node->appendChild($n);
                }
                $dirty = true;
            }
            /** @var AccessRuleCodesTable $AccessRuleCodes */
            $AccessRuleCodes = $this->fetchTable('AccessRuleCodes');
            $bcode = $AccessRuleCodes->find()
                ->select(['code'])
                ->where(
                    [
                        'id' => Hash::get(
                            $entity,
                            'access_rule.access_rule_code_id'
                        ),
                    ]
                )
                ->disableHydration()
                ->firstOrFail()
            ['code'];
            $code = $util->xpath->query('ns:Code', $n)->item(0);
            if (!$code) {
                $code = $util->dom->createElementNS(
                    $util->namespace,
                    'Code'
                );
                $code->appendChild($util->createTextNode($bcode));
                if ($accessTagname === 'AccessRestriction') { // seda 0.2
                    $code->setAttribute('listVersionID', 'edition 2009');
                }
                $n->insertBefore($code, $n->firstChild);
                $dirty = true;
            } elseif ($code->nodeValue !== $bcode) {
                DOMUtility::setValue($code, $bcode);
                $dirty = true;
            }
            /** @var DateTime $date */
            $date = Hash::get($entity, 'access_rule.start_date');
            $bstart = $date->format('Y-m-d');
            $start = $util->xpath->query('ns:StartDate', $n)->item(0);
            if (!$start) {
                $start = $util->dom->createElementNS(
                    $util->namespace,
                    'StartDate'
                );
                $start->appendChild($util->createTextNode($bstart));
                $n->appendChild($start);
                $dirty = true;
            } elseif ($start->nodeValue !== $bstart) {
                DOMUtility::setValue($start, $bstart);
                $dirty = true;
            }
        } elseif ($n) {
            $n->parentNode->removeChild($n);
            $dirty = true;
        }
        return $dirty;
    }

    /**
     * Action supprimer
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Seal->archivalAgency()
            ->table('ArchiveKeywords')
            ->find('seal')
            ->where(['ArchiveKeywords.id' => $id])
            ->contain(
                [
                    'AccessRules',
                    'ArchiveUnits' => [
                        'Archives' => [
                            'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                            'PublicDescriptionArchiveFiles' => ['StoredFiles'],
                            'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                            'Transfers',
                        ],
                        'ArchiveDescriptions' => ['AccessRules'],
                    ],
                ]
            )
            ->firstOrFail();
        /**
         * On cherche le noeud qui correspond à l'entité
         * @var EntityInterface           $archiveUnit
         * @var EntityInterface           $archive
         * @var DescriptionXmlArchiveFile $desc
         */
        $archiveUnit = $entity->get('archive_unit');
        $archive = $archiveUnit->get('archive');
        $util = $this->getUtilFromArchive($archive);
        try {
            $node = $this->findKeywordNode($entity, $util);
            $node->parentNode->removeChild($node);
        } catch (HttpException) {
            $node = null;
        }

        /** @var ArchiveKeywordsTable $ArchiveKeywords */
        $ArchiveKeywords = $this->fetchTable('ArchiveKeywords');
        $conn = $ArchiveKeywords->getConnection();
        $conn->begin();

        try {
            $translateHelper = new TranslateHelper(new View());
            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            $entry = $EventLogs->newEntry(
                'delete_' . Inflector::singularize($this->getName()),
                'success',
                __(
                    "Supprimé ({0}) par l'utilisateur ({1})",
                    $translateHelper->permission($this->getName()),
                    Hash::get($this->user, 'username')
                ),
                $this->userId,
                $entity
            );
            $entry->set('in_lifecycle', false);
            $EventLogs->saveOrFail($entry);
            $ArchiveKeywords->deleteOrFail($entity);
            /** @var ArchiveUnitsTable $ArchiveUnits */
            $ArchiveUnits = $this->fetchTable('ArchiveUnits');
            if (!$entity->get('use_parent_access_rule')) {
                $ArchiveUnits->updateMaxKeywordsAccessEndDate($archiveUnit);
                $ArchiveUnits->updateMaxAggregatedAccessEndDate($archiveUnit);
            }
            $ArchiveUnits->saveAsync($archiveUnit, $util, false);
            $report = 'done';
            /** @var ArchivesTable $Archives */
            $Archives = $this->fetchTable('Archives');
            /** @var EntityInterface $archive */
            $storedFile = Hash::get(
                $archive,
                'lifecycle_xml_archive_file.stored_file',
                []
            );
            if (
                !$Archives->updateLifecycleXml(
                    $storedFile,
                    $EventLogs->get($entry->id),
                    true
                )
            ) {
                throw new Exception(
                    __(
                        "Impossible de modifier le cycle de vie, une erreur a eu lieu lors de l'enregistrement"
                    )
                );
            }
            $conn->commit();
        } catch (Exception) {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }
}
