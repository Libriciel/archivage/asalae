<?php

/**
 * Asalae\Controller\ValidationStagesController
 */

namespace Asalae\Controller;

use Asalae\Model\Entity\ValidationStage;
use Asalae\Model\Table\ValidationActorsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationStagesTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use Cake\Datasource\EntityInterface;
use Cake\Http\Response;
use Exception;

/**
 * Etapes d'un circuit de validation
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ValidationChainsTable $ValidationChains
 * @property ValidationStagesTable $ValidationStages
 * @property ValidationActorsTable $ValidationActors
 */
class ValidationStagesController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'validation-stages-table';

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default' => [
                'post' => [
                    'function' => 'apiAdd',
                ],
            ],
        ];
    }

    /**
     * Liste des étapes d'un circuit de validation
     * @param string $validation_chain_id
     * @throws Exception
     */
    public function index(string $validation_chain_id)
    {
        /** @use IndexComponent */
        $this->loadComponent('AsalaeCore.Index');

        $chain = $this->getValidationChain($validation_chain_id);
        $this->set('chain', $chain);

        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $query = $ValidationStages->find()
            ->where(['validation_chain_id' => $validation_chain_id])
            ->orderBy(['ord' => 'asc'])
            ->contain(
                [
                    'ValidationActors' => [
                        'sort' => 'ValidationActors.id',
                        'Users',
                    ],
                ]
            );
        $data = $this->paginateToResultSet($query);
        $this->set('data', $data);
    }

    /**
     * Envoi un 404 si le validation_chain_id n'existe pas ou ne fait pas parti
     * du même service d'archive.
     *
     * @param string|int $validation_chain_id
     * @return EntityInterface
     * @throws Exception
     */
    private function getValidationChain($validation_chain_id): EntityInterface
    {
        return $this->Seal->archivalAgency()
            ->table('ValidationChains')
            ->find('seal')
            ->where(['ValidationChains.id' => $validation_chain_id])
            ->firstOrFail();
    }

    /**
     * Ajout d'une étape de circuit de validation
     * @param string $validation_chain_id
     * @return Response|null
     * @throws Exception
     */
    public function add(string $validation_chain_id)
    {
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $chain = $this->getValidationChain($validation_chain_id);
        $this->set('chain', $chain);
        $entity = $ValidationStages->newEntity(
            compact('validation_chain_id')
        );

        if ($this->getRequest()->is('post')) {
            $data = [
                'validation_chain_id' => $validation_chain_id,
                // nécessaire pour la validation
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'all_actors_to_complete' => $this->getRequest()->getData(
                    'all_actors_to_complete',
                    0
                ),
                'created_user_id' => $this->userId,
            ];
            $ValidationStages->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($ValidationStages->save($entity)) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);
        $all_actors_to_completes = $ValidationStages->options(
            'all_actors_to_complete'
        );
        $this->set('all_actors_to_completes', $all_actors_to_completes);
    }

    /**
     * Modifier une étape de circuit de validation
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        /** @var ValidationStage $entity */
        $entity = $ValidationStages->find()
            ->where(['ValidationStages.id' => $id])
            ->contain(['ValidationChains', 'ValidationActors' => ['Users']])
            ->firstOrFail();
        $this->set(
            'chain',
            $this->getValidationChain($entity->get('validation_chain_id'))
        );
        $this->set('stage', $entity);// alias pour ValidationActors/index.ctp
        $this->set('data', $entity->get('validation_actors'));

        if ($this->getRequest()->is('put')) {
            $data = [
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'all_actors_to_complete' => $this->getRequest()->getData(
                    'all_actors_to_complete',
                    0
                ),
                'default' => (bool)$this->getRequest()->getData('default'),
                'active' => (bool)$this->getRequest()->getData('active'),
            ];
            $ValidationStages->patchEntity($entity, $data);
            $entity->appendModified($this->userId);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($ValidationStages->save($entity)) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);

        $all_actors_to_completes = $ValidationStages->options(
            'all_actors_to_complete'
        );
        $this->set('all_actors_to_completes', $all_actors_to_completes);

        $this->set('tableIdStages', self::TABLE_INDEX);

        // Tab index actors
        $this->set('tableIdActors', ValidationActorsController::TABLE_INDEX);
        /** @var ValidationActorsTable $ValidationActors */
        $ValidationActors = $this->fetchTable('ValidationActors');
        $this->set(
            'type_validations',
            $ValidationActors->options('type_validation')
        );
    }

    /**
     * Suppression d'une étape de circuit de validation
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $entity = $this->Seal->archivalAgency()
            ->table('ValidationStages')
            ->find('seal')
            ->where(['ValidationStages.id' => $id])
            ->firstOrFail();
        $this->getValidationChain($entity->get('validation_chain_id'));

        $report = $ValidationStages->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Action de modification de l'ordre de l'étape
     * @param string $id
     * @param string $order
     * @return Response
     * @throws Exception
     */
    public function changeOrder(string $id, string $order)
    {
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $entity = $this->Seal->archivalAgency()
            ->table('ValidationStages')
            ->find('seal')
            ->where(['ValidationStages.id' => $id])
            ->firstOrFail();
        $this->getValidationChain($entity->get('validation_chain_id'));
        $entity->set('ord', (int)$order);
        $report = $ValidationStages->save($entity)
            ? 'done'
            : "Erreur lors de l'enregistrement";

        if ($report === 'done') {
            $data = $ValidationStages->find()
                ->where(
                    [
                        'validation_chain_id' => $entity->get(
                            'validation_chain_id'
                        ),
                    ]
                )
                ->contain(['ValidationChains', 'ValidationActors' => ['Users']])
                ->orderBy(['ord' => 'asc'])
                ->all()
                ->toArray();
            return $this->renderData($data);
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Création via API
     */
    protected function apiAdd()
    {
        $request = $this->getRequest();
        $all_actors_to_complete = null;
        if ($request->getData('all_actors_to_complete')) {
            $all_actors_to_complete = $request->getData(
                'all_actors_to_complete'
            ) === 'true';
        }
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $count = $ValidationStages->find()
            ->where(
                [
                    'validation_chain_id' => $request->getData(
                        'validation_chain_id'
                    ),
                ]
            )
            ->count();
        $data = [
            'validation_chain_id' => $request->getData('validation_chain_id'),
            'name' => $request->getData('name'),
            'description' => $request->getData('description'),
            'ord' => $request->getData('ord') ?: $count + 1,
            'all_actors_to_complete' => $all_actors_to_complete ?: false,
        ];
        $entity = $ValidationStages->newEntity($data);
        if ($ValidationStages->save($entity)) {
            return $this->renderData(['success' => true, 'id' => $entity->id]);
        } else {
            return $this->renderData(
                ['success' => false, 'errors' => $entity->getErrors()]
            );
        }
    }
}
