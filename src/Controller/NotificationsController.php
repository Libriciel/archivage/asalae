<?php

/**
 * Asalae\Controller\NotificationsController
 */

namespace Asalae\Controller;

use Asalae\Model\Table\NotificationsTable;
use Asalae\Model\Table\UsersTable;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory;
use Cake\Http\Response;

/**
 * Notifications de messages beanstalkd à l'utilisateur d'asalae
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property NotificationsTable $Notifications
 * @property UsersTable         $Users
 */
class NotificationsController extends AppController
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * Envoi un message test à l'utilisateur qui l'a provoqué
     */
    public function test()
    {
        $result = Factory\Utility::get('Notify')->send(
            $this->userId,
            [$this->getRequest()->getSession()->read('Session.token')],
            $this->getRequest()->getData('msg')
        );
        return $this->renderDataToJson(['result' => $result]);
    }

    /**
     * Supprime une notification
     *
     * @param string $id
     * @return Response
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        if ($id === '0') {
            return $this->renderDataToJson(['report' => 'ok']);
        }

        /** @var NotificationsTable $Notifications */
        $Notifications = $this->fetchTable('Notifications');
        $entity = $Notifications
            ->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();

        $report = $Notifications->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }
}
