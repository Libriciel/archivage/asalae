<?php

/**
 * Asalae\Controller\TypeEntities
 */

namespace Asalae\Controller;

use Asalae\Model\Table\TypeEntitiesTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\RenderDataTrait;
use Cake\Http\Response;
use Exception;

/**
 * TypeEntities
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property TypeEntitiesTable TypeEntities
 */
class TypeEntitiesController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;
    use Traits\PermTrait;

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default' => [
                'get' => [
                    'function' => 'apiGet',
                ],
            ],
        ];
    }

    /**
     * Index api avec containe Roles
     * @return Response
     * @throws Exception
     */
    protected function apiGet(): Response
    {
        /** @var TypeEntitiesTable $TypeEntities */
        $TypeEntities = $this->fetchTable('TypeEntities');
        $query = $TypeEntities->find()
            ->contain(['Roles']);

        $filters = $this->getRequest()->getQuery('filters');
        if ($filters) {
            $this->applyFilters($filters, $query);
        }

        $data = $this->paginateToResultSet($query);

        return $this->renderDataToJson($data);
    }
}
