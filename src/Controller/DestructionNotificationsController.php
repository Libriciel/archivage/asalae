<?php

/**
 * Asalae\Controller\DestructionNotificationsController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Entity\DestructionRequest;
use Asalae\Model\Table\DestructionNotificationsTable;
use Asalae\Model\Table\DestructionNotificationXpathsTable;
use Asalae\Model\Table\DestructionRequestsTable;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Http\CallbackStream;
use AsalaeCore\Utility\Notify;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\Query;
use DateInterval;
use Exception;

/**
 * DestructionNotifications
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property DestructionNotificationXpathsTable DestructionNotificationXpaths
 * @property DestructionNotificationsTable      DestructionNotifications
 * @property DestructionRequestsTable           DestructionRequests
 */
class DestructionNotificationsController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     * @var array
     */
    public array $paginate = [
        'order' => [
            'DestructionNotifications.id' => 'desc',
        ],
        'sortableFields' => [
            'original_count' => 'DestructionRequests.original_count',
            'original_size' => 'DestructionRequests.original_size',
        ],
    ];

    /**
     * Liste les enregistrements
     */
    public function index()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');

        $query = $this->Seal->orgEntity()
            ->table('DestructionNotifications')
            ->find('seal')
            ->contain(['DestructionRequests' => ['ArchiveUnits']]);
        /** @var DestructionRequestsTable $DestructionRequests */
        $DestructionRequests = $this->fetchTable('DestructionRequests');
        $subquery = $DestructionRequests->selectQuery()
            ->select(['dr.id'])
            ->from(['dr' => 'destruction_requests'])
            ->innerJoin(
                ['lk' => 'destruction_notification_xpaths'],
                [
                    'lk.destruction_notification_id' => new IdentifierExpression(
                        'DestructionNotifications.id'
                    ),
                ]
            )
            ->where(
                ['dr.id' => new IdentifierExpression('DestructionRequests.id')]
            )
            ->limit(1);
        $IndexComponent->setQuery($query)
            ->filter(
                'archive_unit_identifier',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike(
                            'lk.archive_unit_identifier',
                            $v
                        )
                    );
                    return $q->andWhere(['DestructionRequests.id IN' => $sq]);
                }
            )
            ->filter(
                'archive_unit_name',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike('lk.archive_unit_name', $v)
                    );
                    return $q->andWhere(['DestructionRequests.id IN' => $sq]);
                }
            )
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter(
                'original_count',
                IndexComponent::FILTER_COUNTOPERATOR,
                'DestructionRequests.original_count'
            )
            ->filter(
                'original_size',
                IndexComponent::FILTER_SIZEOPERATOR,
                'DestructionRequests.original_size'
            );

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        /** @var DestructionNotificationXpathsTable $DestructionNotificationXpaths */
        $DestructionNotificationXpaths = $this->fetchTable('DestructionNotificationXpaths');
        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) use ($DestructionNotificationXpaths) {
                    // effectue un contain avec limite individuelle d'enregistrements
                    $xpaths = $DestructionNotificationXpaths->find()
                        ->where(['destruction_notification_id' => $e->id])
                        ->orderBy(['archive_unit_name'])
                        ->limit(10)
                        ->toArray();
                    $e->set('destruction_notification_xpaths', $xpaths);
                    return $e;
                }
            )
            ->toArray();
        $this->set('data', $data);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * Téléchargement d'une notification
     * @param string $id
     * @return Response|null
     * @throws Exception
     */
    public function download(string $id)
    {
        $notification = $this->Seal->orgEntity()
            ->table('DestructionNotifications')
            ->find('seal')
            ->where(['DestructionNotifications.id' => $id])
            ->firstOrFail();
        $xml = $notification->get('xml');
        if (!is_readable($xml)) {
            throw new NotFoundException("xml not found");
        }

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($notification);

        return $this->getCoreResponse()
            ->withFileStream($xml, ['mime' => 'application/xml']);
    }

    /**
     * Téléchargement de l'attestation d'élimination
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function destructionCertificate(string $id)
    {
        $conditions = [
            'DestructionRequests.archival_agency_id' => $this->orgEntityId,
            'DestructionRequests.originating_agency_id' => $this->orgEntityId,
        ];
        if ($this->superArchivist) {
            $conditions['DestructionRequests.archival_agency_id'] = $this->archivalAgencyId;
        }
        $destructionNotification = $this->DestructionNotifications->find()
            ->innerJoinWith('DestructionRequests')
            ->where(
                [
                    'DestructionNotifications.id' => $id,
                    'OR' => $conditions,
                ]
            )
            ->contain(['DestructionRequests'])
            ->firstOrFail();
        if ($sessionFile = $this->getRequest()->getQuery('sessionFile')) {
            Notify::emit(
                'download_file',
                [
                    'uuid' => $sessionFile,
                    'state' => 'downloading',
                    'last_update' => time(),
                ]
            );
        }

        /** @var DestructionRequest $destructionRequest */
        $destructionRequest = $destructionNotification->get('destruction_request');
        if ($destructionRequest->get('certified') === null) {
            $delay = new DateInterval(
                sprintf(
                    'P%dD',
                    Configure::read('Attestations.destruction_delay', 0)
                )
            );
            $created = new CakeDateTime($destructionRequest->get('created')->add($delay)->format('Y-m-d'));
            $days = $created->diffInDays(new CakeDateTime('00:00:00'));
            throw new NotFoundException(
                __("L'attestation n'a pas encore été généré") . PHP_EOL
                . __("Elle sera généré dans {0} jour(s)", $days)
            );
        }
        if ($this->getRequest()->getQuery('renew')) {
            $destructionRequest->set('needRenew', true);
        }
        $pdf = $destructionRequest->get('certification');
        $filename = sprintf(
            "%s_destruction_certificate.pdf",
            $destructionRequest->get('identifier')
        );
        // note met à jour le champ certified
        $this->DestructionNotifications->save($destructionRequest);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction(
            $destructionNotification,
            [
                'actionName' => 'download',
                'objectName' => 'DestructionCertificate',
                'objectLiteral' => __("Attestation d'élimination"),
            ]
        );

        return $this->getResponse()
            ->withHeader('Content-Type', 'application/pdf')
            ->withHeader('Content-Length', $size = strlen($pdf))
            ->withHeader('File-Size', $size)
            ->withHeader(
                'Content-Disposition',
                'attachment; filename="' . $filename . '"'
            )
            ->withBody(
                new CallbackStream(
                    function () use ($pdf, $sessionFile) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        echo $pdf;
                        if ($sessionFile) {
                            Notify::emit(
                                'download_file',
                                [
                                    'uuid' => $sessionFile,
                                    'state' => 'finished',
                                    'last_update' => time(),
                                    'integrity' => true,
                                ]
                            );
                        }
                    }
                )
            );
    }
}
