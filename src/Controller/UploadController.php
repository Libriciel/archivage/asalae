<?php

/**
 * Asalae\Controller\UploadController
 */

namespace Asalae\Controller;

use Asalae\Controller\Traits\TransfersAddCompressedTrait;
use Asalae\Model\Entity\Fileupload;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\TransferAttachmentsTable;
use Asalae\Model\Table\TransfersTable;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Http\Response;
use AsalaeCore\Utility\Antivirus\AntivirusInterface;
use AsalaeCore\Utility\Antivirus\Clamav;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\LimitBreak;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\PreControlMessages;
use AsalaeCore\View\Helper\PrevHelper;
use Cake\Core\Configure;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response as CakeResponse;
use Cake\Utility\Security;
use Cake\View\View;
use DOMDocument;
use Exception;
use FileConverters\Utility\FileConverters;
use FileValidator\Utility\FileValidator;
use Flow\Basic;
use Flow\Config;
use Flow\File;
use Flow\Request;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;
use Psr\Http\Message\MessageInterface;
use Transliterator;

/**
 * Upload de fichiers
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FileuploadsTable         Fileuploads
 * @property TransferAttachmentsTable TransferAttachments
 * @property TransfersTable           Transfers
 */
class UploadController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;
    use TransfersAddCompressedTrait;

    public const string FILE_ENCRYPT_KEY = 'upload';
    public const string HASH_ALGO = 'sha256';

    /**
     * @var FileUpload
     */
    private $entity;

    /**
     * Upload du fichier
     * (appelé par ajax via <b>new Flow({target: '/upload'})</b>)
     *
     * @param string $validation Fonction de validation du modèle
     * @return Response|CakeResponse
     * @throws Exception
     */
    public function index(string $validation = 'default')
    {
        $this->setResponse(
            $this->getResponse()->withHeader('Connection', 'close')
        );
        if ($validation === 'seda') {
            $this->setResponse(
                $this->getResponse()->withHeader('X-Asalae-No-Retry', 'true')
            );
        }

        $path = Configure::read(
            'Upload.dir',
            TMP . 'upload'
        ) . DS . 'user_' . $this->userId;
        if (!file_exists($path)) {
            Filesystem::mkdir($path);
        }
        return $this->uploadTo($path, $validation);
    }

    /**
     * Upload générique
     * @param string $path
     * @param string $validation
     * @param bool   $allowPath
     * @return CakeResponse|Response
     * @throws Exception
     */
    private function uploadTo(string $path, $validation = 'default', bool $allowPath = false)
    {
        $this->setResponse(
            $this->getResponse()->withHeader('Connection', 'close')
        );

        $config = new Config();
        $config->setTempDir($path);
        $request = new Request(
            $this->getRequest()->getData() + $this->getRequest()->getQuery()
        );

        $id = $request->getIdentifier();
        if (empty($id)) {
            throw new Exception(__("Une erreur inattendue a eu lieu"));
        }
        $finalPath = $path . DS . $request->getFileName();

        $file = (array)$request->getFile();
        if (!$this->getRequest()->is('post')) {
            $file['error'] = UPLOAD_ERR_OK;
        } elseif (!isset($file['error'])) {
            $file['error'] = 'not found';
        }
        $name = $request->getFileName();
        $report = [
            'uid' => $id,
            'filename' => $name,
            'name' => $name,
            'path' => $finalPath,
            'completion' => false,
            'error' => $file['error'],
        ];
        if ($this->getRequest()->is('post')) {
            switch ($report['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_INI_SIZE:
                    throw new Exception(
                        __(
                            "Problème de configuration serveur ({0}), veuillez contacter un administrateur",
                            'UPLOAD_ERR_INI_SIZE'
                        )
                    );
                case UPLOAD_ERR_PARTIAL:
                    throw new Exception(__("Connexion au serveur interrompue"));
                default:
                    throw new Exception(
                        __(
                            "Erreur code {0} lors de l'upload du fichier",
                            $report['error']
                        )
                    );
            }
        }

        $neededTime = (int)(300 / 1000000000 * $request->getTotalSize());// 1Go ~= 300s
        if (PHP_SAPI !== 'cli' && $neededTime > 30) {
            set_time_limit($neededTime);
        }
        if ($this->getRequest()->is('get')) {
            $file = new File($config, $request);
            if (!$file->checkChunk()) {
                return $this->getResponse()
                    ->withStatus(204, 'No Content')
                    ->withStringBody('');
            }
        }
        if (Basic::save($finalPath, $config, $request)) {
            return $this->saveFileinfo($report, $validation, $allowPath);
        }

        $report['path'] = urlencode(substr($report['path'], strlen($path) + 1));
        return $this->renderDataToJson(['report' => $report])->withHeader(
            'Connection',
            'close'
        );
    }

    /**
     * Sauvegarde les informations relative au fichier en base
     *
     * @param array  $report
     * @param string $validation Fonction de validation du modèle
     * @param bool   $allowPath
     * @return Response|CakeResponse
     * @throws Exception
     */
    private function saveFileinfo(
        array &$report,
        string $validation = 'default',
        bool $allowPath = false
    ) {
        if (!isset($report['path']) || !is_readable($report['path'])) {
            throw new Exception(__("Impossible de lire le fichier"));
        }
        $report['completion'] = date("Y-m-d H:i:s");
        $report['mime'] = mime_content_type($report['path']);

        // Envoi les informations à la vue avant de lancer la validation
        /** @var Response $response */
        $buffer = '{"report": {';
        foreach ($report as $key => $value) {
            if ($key !== 'path' || $allowPath) {
                $buffer .= '"' . $key . '": ' . json_encode($value) . ', ';
            }
        }
        $response = $this->getResponse()->withStringBody($buffer)->withType(
            'json'
        );
        $response->partialEmit();
        $buffer = '';

        $reportAfter = [];
        $errorCode = 0;
        try {
            if ($validation === 'seda') {
                $size = filesize($report['path']);
                $max_execution_time = (int)($size / 100000); // 46Mo = 460 seconds
                $memory_limit = (int)($size * 20); // 46Mo = 920Mo
                LimitBreak::setMemoryLimit($memory_limit);
                LimitBreak::setTimeLimit($max_execution_time);
                $rawXml = file_get_contents($report['path']);
                $xml = Transliterator::create('NFC')
                    ->transliterate($rawXml);
                if (!$xml) {
                    $xml = $rawXml;
                }
                $dom = new DOMDocument();
                $dom->formatOutput = true;
                $dom->preserveWhiteSpace = false;
                $dom->loadXML($xml);
                file_put_contents($report['path'], $dom->saveXML());
                $preControl = new PreControlMessages($report['path']);
                $archivalAgency = $preControl->getArchivalAgency();
                if (!$archivalAgency) {
                    throw new BadRequestException(
                        __(
                            "Le service d'archives ({0}) n'a pas été trouvé en base de données",
                            $preControl->getArchivalAgencyIdentifier()
                        )
                    );
                }
                if ($archivalAgency->id !== $this->archivalAgencyId) {
                    throw new BadRequestException(
                        __(
                            "Le service d'archives indiqué ({0}) est différent du votre",
                            h($archivalAgency->get('identifier'))
                        )
                    );
                }
                if (!$preControl->validate()) {
                    $errorCode = PreControlMessages::$lastError;
                    throw new BadRequestException(
                        PreControlMessages::getLastErrorMessage(),
                        400,
                        PreControlMessages::$lastException ?: null
                    );
                }
            }
            /** @var FileuploadsTable $Fileuploads */
            $Fileuploads = $this->fetchTable('Fileuploads');
            $entity = $Fileuploads->newUploadedEntity(
                $report['filename'],
                $report['path'],
                $this->userId ?: null,
                ['validate' => $validation]
            );
            $this->entity = $entity;

            if ($Fileuploads->save($entity)) {
                $reportAfter['id'] = $entity->id;
                $reportAfter['statetrad'] = $entity->statetrad;
                $reportAfter['pronom'] = isset($entity->siegfrieds[0])
                    ? $entity->siegfrieds[0]->pronom : null;
                $reportAfter['openable'] = $entity->openable;
                $reportAfter['hash'] = $entity->hash;
                $reportAfter['hash_algo'] = $entity->hash_algo;
                $Prev = new PrevHelper(new View());
                $reportAfter['prev'] = $Prev->create(
                    $reportAfter['id'],
                    $report['filename'],
                    $report['path']
                );
                $reportAfter['convertable']
                    = FileConverters::matchFormats($report['path']);
            } else {
                if (is_file($report['path'])) {
                    Filesystem::remove($report['path']);
                }
                $errors = Hash::flatten($entity->getErrors());
                $reportAfter['error'] = end($errors);
                throw new BadRequestException();
            }
            $output = [];
            foreach ($reportAfter as $key => $value) {
                $output[] = '"' . $key . '": ' . json_encode($value);
            }
            $buffer .= implode(', ', $output) . '}';
        } catch (Exception $e) {
            $output = [
                'error' => $reportAfter['error'] ?? $e->getMessage(),
                'message' => $e->getMessage(),
                'report' => $reportAfter,
                'file' => str_replace(
                    ['+', '/'],
                    ['-', '_'],
                    base64_encode(
                        Security::encrypt(
                            $report['path'],
                            hash(self::HASH_ALGO, self::FILE_ENCRYPT_KEY)
                        )
                    )
                ),
            ];
            if ($errorCode) {
                $output['code'] = $errorCode;
            }
            if (Configure::read('debug')) {
                $prev = $e->getPrevious();
                $output += [
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'previous' => $prev
                        ? [
                            'message' => $prev->getMessage(),
                            'file' => $prev->getFile(),
                            'line' => $prev->getLine(),
                            'trace' => explode(
                                "\n",
                                str_replace(
                                    ROOT,
                                    'ROOT',
                                    $prev->getTraceAsString()
                                )
                            ),
                        ]
                        : null,
                ];
            }
            return $response->withStringBody(
                substr(json_encode($output), 1) . '}'
            );
        }
        $buffer .= "}";
        return $response->withStringBody($buffer);
    }

    /**
     * Supprime un fichier uploadé
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function delete(string $id)
    {
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = $this->fetchTable('Fileuploads');

        $entity = $Fileuploads->find()
            ->where(['id' => $id])
            ->andWhere(
                $this->userId ? ['user_id' => $this->userId]
                    : ['user_id IS' => null]
            )
            ->firstOrFail();

        $delete = $Fileuploads->delete($entity);

        $unlink = true;
        if (is_writable($entity->path)) {
            $unlink = @unlink($entity->path);
        }

        $report = $delete && $unlink
            ? 'done'
            : (!$delete ? __(
                "Erreur lors de la suppression en base (id: {0}) ",
                $id
            ) : '')
            . (!$unlink ? __("Erreur lors de la suppression du fichier") : '');

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Upload d'un TransferAttachment
     * @param string      $transfer_id
     * @param bool|string $uncompress
     * @return Response|CakeResponse
     * @throws Exception
     */
    public function addTransferAttachment(string $transfer_id, $uncompress = false)
    {
        $this->setResponse(
            $this->getResponse()->withHeader('Connection', 'close')
        );
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->select(['Transfers.id'])
            ->where(['Transfers.id' => $transfer_id])
            ->firstOrFail();
        if ($uncompress) {
            $Transfers->lock($transfer_id, true);
        }
        $path = AbstractGenericMessageForm::getDataDirectory(
            $transfer_id
        ) . DS . 'attachments';
        $tmpDir = AbstractGenericMessageForm::getDataDirectory(
            $transfer_id
        ) . DS . 'tmp';
        if (!file_exists($path)) {
            Filesystem::mkdir($path);
        }
        if (!file_exists($tmpDir)) {
            Filesystem::mkdir($tmpDir);
        }

        $config = new Config();
        $config->setTempDir($tmpDir);
        $request = new Request(
            $this->getRequest()->getData() + $this->getRequest()->getQuery()
        );

        $id = $request->getIdentifier();
        if (empty($id)) {
            $debug = [
                'transfer_id' => $transfer_id,
                'path' => $path,
                'data' => $this->getRequest()->getData(),
                'headers' => $this->getRequest()->getHeaders(),
            ];
            file_put_contents(
                TMP . 'debug_UploadAddTransferAttachment.json',
                json_encode($debug, JSON_PRETTY_PRINT)
            );
            throw new Exception(__("Une erreur inattendue a eu lieu"));
        }
        $finalPath = $path . DS . $request->getFileName();

        $file = (array)$request->getFile();
        if (!$this->getRequest()->is('post')) {
            $file['error'] = UPLOAD_ERR_OK;
        } elseif (!isset($file['error'])) {
            $file['error'] = 'not found';
        }
        $report = [
            'uid' => $id,
            'filename' => $name = $request->getFileName(),
            'name' => $name,
            'path' => $finalPath,
            'completion' => false,
            'error' => $file['error'],
        ];
        if ($this->getRequest()->is('post')) {
            switch ($report['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_INI_SIZE:
                    throw new Exception(
                        __(
                            "Problème de configuration serveur ({0}), veuillez contacter un administrateur",
                            'UPLOAD_ERR_INI_SIZE'
                        )
                    );
                case UPLOAD_ERR_PARTIAL:
                    throw new Exception(__("Connexion au serveur interrompue"));
                default:
                    throw new Exception(
                        __(
                            "Erreur code {0} lors de l'upload du fichier",
                            $report['error']
                        )
                    );
            }
        }
        $neededTime = (int)(300 / 1000000000 * $request->getTotalSize());// 1Go ~= 300s
        if (PHP_SAPI !== 'cli' && $neededTime > 30) {
            set_time_limit($neededTime);
        }
        if ($this->getRequest()->is('get')) {
            $file = new File($config, $request);
            if (!$file->checkChunk()) {
                return $this->getResponse()
                    ->withStatus(204, 'No Content')
                    ->withStringBody('');
            }
        }
        if (Basic::save($finalPath, $config, $request)) {
            $this->setResponse(
                $this->getResponse()->withHeader('X-Asalae-No-Retry', 'true')
            );
            if (!isset($report['path']) || !is_readable($report['path'])) {
                throw new Exception(__("Impossible de lire le fichier"));
            }
            $report['completion'] = date("Y-m-d H:i:s");
            $report['mime'] = mime_content_type($report['path']);

            // Envoi les informations à la vue avant de lancer la validation
            /** @var Response $response */
            $buffer = '{"report": {';
            foreach ($report as $key => $value) {
                if ($key !== 'path') {
                    $buffer .= '"' . $key . '": ' . json_encode($value) . ', ';
                }
            }
            $response = $this->getResponse()->withType('json');
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $transfer = $Transfers->get($transfer_id);
            $util = DOMUtility::load($transfer->get('xml'));

            $reportAfter = [];
            try {
                /** @var TransferAttachmentsTable $TransferAttachments */
                $TransferAttachments = $this->fetchTable('TransferAttachments');
                $algo = Configure::read('hash_algo', 'sha256');

                $exec = Utility::get('Exec')->command('sf -json', $finalPath);
                $json = json_decode($exec->stdout, true);
                $sf = Hash::get($json, 'files.0.matches.0', []);

                try {
                    /** @var Clamav|AntivirusInterface $Antivirus */
                    $Antivirus = Utility::get(
                        Configure::read('Antivirus.classname')
                    );
                    $virus = current($Antivirus->scan($finalPath));
                    if ($virus === 'OK') {
                        $virus = '';
                    }
                } catch (\Exception $e) {
                    $this->log($e->getMessage());
                    $virus = null;
                }
                $format = Hash::get($sf, 'id');
                if ($format === 'UNKNOWN') {
                    $format = '';
                }

                $node = ArchiveBinariesTable::findBinaryByFilename($util, $report['filename']);
                $xpath = $node ? $util->getElementXpath($node, '') : null;

                $attachment = $TransferAttachments->newEntity(
                    [
                        'transfer_id' => $transfer_id,
                        'filename' => $report['filename'],
                        'size' => filesize($finalPath),
                        'hash' => hash_file($algo, $finalPath),
                        'hash_algo' => $algo,
                        'mime' => mime_content_type($finalPath),
                        'extension' => pathinfo($name, PATHINFO_EXTENSION),
                        'format' => $format,
                        'virus_name' => $virus,
                        'xpath' => $xpath,
                        'valid' => Utility::get(FileValidator::class)->isValid($path),
                    ]
                );
                if ($TransferAttachments->save($attachment)) {
                    $reportAfter['id'] = $attachment->get('id');
                    $reportAfter['hash'] = $attachment->get('hash');
                    $reportAfter['deletable'] = true;
                    Notify::emit("transfer_{$transfer_id}_attachment_saved", $id . ':' . $attachment->id);
                } else {
                    $reportAfter['error'] = $attachment->getErrors();
                }
                $output = [];
                foreach ($reportAfter as $key => $value) {
                    $output[] = '"' . $key . '": ' . json_encode($value);
                }
                if ($uncompress && empty($reportAfter['error'])) {
                    $Transfers->lock($transfer_id, false);
                    $addCompressedResponse = $this->_addCompressed($attachment->id);
                    $output[] = '"uncompress_response": ' . $addCompressedResponse->getBody()->getContents();
                    Notify::emit(
                        'addcompressed_' . $attachment->id,
                        __("Fichier décompressé avec succès, création de l'arborescence du bordereau...")
                    );
                    $oldRequest = $this->getRequest();
                    $request = $oldRequest
                        ->withMethod('PUT')
                        ->withData('generate_tree', 'true');
                    $this->setRequest($request);
                    $generateTree = empty($request->getQuery('without_tree'));
                    if ($generateTree) {
                        $this->_add2($transfer_id);
                    }
                    $this->setRequest($oldRequest);
                }
                $buffer .= implode(', ', $output) . '}';
            } catch (Exception $e) {
                $output = [
                    'error' => $reportAfter['error'] ?? $e->getMessage(),
                    'message' => $e->getMessage(),
                    'report' => $reportAfter,
                ];
                if (Configure::read('debug')) {
                    $output += [
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                    ];
                }
                return $response->withStringBody(
                    $buffer
                    . substr(
                        json_encode($output),
                        1
                    ) . "}"
                );
            }
            $buffer .= "}";
            $Transfers->unlock($transfer_id);
            $transfer->set('is_conform');
            $Transfers->saveOrFail($transfer);

            return $response->withStringBody($buffer);
        }
        $report['path'] = urlencode(substr($report['path'], strlen($path))); // FIXME vérifier le correctif

        return $this->renderDataToJson(['report' => $report])->withHeader(
            'Connection',
            'close'
        );
    }

    /**
     * Upload d'un certificat pour minio
     */
    public function minioCert()
    {
        return $this->ssl('minio');
    }

    /**
     * Upload d'un certificat pour archive
     * @param string $subdir
     * @return Response|CakeResponse|MessageInterface
     * @throws Exception
     */
    private function ssl(string $subdir)
    {
        $path = Configure::read('App.paths.data') . DS . 'ssl' . DS . $subdir;
        if (!file_exists($path)) {
            Filesystem::mkdir($path);
        }
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = $this->fetchTable('Fileuploads');
        $uid = $this->getRequest()->getQuery('flowIdentifier');
        $name = $this->getRequest()->getQuery('flowFilename');
        $filename = $path . DS . $name;
        if (is_file($filename)) {
            $report = [
                'uid' => $uid,
                'filename' => $name,
                'name' => $name,
                'path' => $filename,
                'completion' => true,
                'error' => UPLOAD_ERR_OK,
            ];
            return $this->renderDataToJson(['report' => $report])
                ->withHeader('Connection', 'close');
        }

        $response = $this->uploadTo($path, 'default', true);

        if ($this->entity) {
            $this->entity->set('locked', true);
            $Fileuploads->save($this->entity);
        }

        return $response;
    }

    /**
     * Upload d'un certificat pour SAE distant
     */
    public function archivingSystemCert()
    {
        return $this->ssl('archiving-system');
    }
}
