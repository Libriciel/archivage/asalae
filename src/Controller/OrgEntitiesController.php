<?php

/**
 * Asalae\Controller\OrgEntitiesController
 * @noinspection PhpDeprecationInspection
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Form\ArchivalAgencyConfigurationForm;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\ConversionPoliciesTable;
use Asalae\Model\Table\EaccpfsTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\RolesTable;
use Asalae\Model\Table\SecureDataSpacesTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\TypeEntitiesTable;
use Asalae\Model\Table\UsersTable;
use Asalae\Model\Table\ValidationActorsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationStagesTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\Component\IndexComponent;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Entity\TypeEntity;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\QueryInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\Http\Response as CakeResponse;
use Cake\ORM\Query;
use Cake\ORM\ResultSet;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

/**
 * Contrôleur des entités
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchivesTable           Archives
 * @property ConfigurationsTable     Configurations
 * @property ConversionPoliciesTable ConversionPolicies
 * @property EaccpfsTable            Eaccpfs
 * @property FileuploadsTable        Fileuploads
 * @property OrgEntitiesTable        OrgEntities
 * @property RolesTable              Roles
 * @property SecureDataSpacesTable   SecureDataSpaces
 * @property TransfersTable          Transfers
 * @property TypeEntitiesTable       TypeEntities
 * @property ValidationActorsTable   ValidationActors
 * @property ValidationChainsTable   ValidationChains
 * @property ValidationStagesTable   ValidationStages
 */
class OrgEntitiesController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'org-entities-table';
    public const string TABLE_EDIT_EACCPF = 'eaccpfs-entity-table';
    public const string TABLE_EDIT_USER = 'users-entity-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'OrgEntities.lft' => 'asc',
        ],
        'sortableFields' => [
            'type_entity' => 'TypeEntities.name',
        ],
    ];

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default' => [
                'post' => ['function' => 'apiCreate'],
            ],
        ];
    }

    /**
     * Liste des entités
     * @param string $type organigramme || table
     * @throws Exception
     */
    public function index(string $type = 'organigramme')
    {
        switch ($type) {
            case 'organigramme':
                $this->indexOrgchart();
                break;
            case 'table':
                $this->indexTable();
                break;
            default:
                throw new NotFoundException();
        }
        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * index sous forme d'organigramme
     * @throws Exception
     */
    private function indexOrgchart()
    {
        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = $this->fetchTable('Eaccpfs');
        $subQuery = $Eaccpfs->withAlias('a')->find()
            ->select(['a.id'])
            ->where(
                [
                    'a.org_entity_id' => new IdentifierExpression(
                        'OrgEntities.id'
                    ),
                ]
            );
        $orgEntities = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('sealThreaded')
            ->select(
                [
                    'has_eaccpf' => $Eaccpfs->query()->newExpr()->exists(
                        $subQuery
                    ),
                ]
            )
            ->enableAutoFields()
            ->contain(['TypeEntities']) // pour le champ virtuel "deletable"
            ->orderBy(['OrgEntities.name' => 'asc'])
            ->all()
            ->first();

        $f = function ($v) use (&$f) {
            if ($v instanceof EntityInterface) {
                $v->set('deletable', $v->get('deletable'));
                $v->set('html_id', 'OrgEntity_' . $v->get('id'));
                $children = $v->get('children');
                foreach ($children as $key => $child) {
                    $children[$key] = $f($child);
                }
                $v->set('children', $children);
                $v = $v->toArray();
            }
            $v['html_id'] = 'OrgEntity_' . ($v['id'] === null ? 'root'
                    : $v['id']);
            foreach ($v as $k => $val) {
                if (is_string($val)) {
                    $v[$k] = h($val);
                }
            }
            return $v;
        };
        $chartDatasource = $f(
            $orgEntities
        );// first() ne fonctionne pas en threaded
        $this->set('chartDatasource', $chartDatasource);

        $query = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.name',
                    'Parents.name',
                ]
            )
            ->contain(['Parents'])
            ->orderBy(['OrgEntities.name' => 'asc', 'OrgEntities.lft' => 'asc']);
        $allNames = [];
        /** @var OrgEntity $entity */
        foreach ($query as $entity) {
            $name = $entity->get('name');
            if ($parent = $entity->get('parent')) {
                $name .= ' (' . $parent->get('name') . ')';
            }
            $allNames[$entity->get('id')] = htmlspecialchars($name, ENT_COMPAT);
        }
        $this->set('allNames', $allNames);
        $this->viewBuilder()->setTemplate('index_orgchart');
    }

    /**
     * Index sous la forme d'un tableau de résultats classique
     * @throws Exception
     */
    private function indexTable()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = $this->fetchTable('Eaccpfs');
        $hasEac = $Eaccpfs->selectQuery()
            ->select(['a.id'])
            ->from(['a' => 'eaccpfs'])
            ->where(
                [
                    'a.org_entity_id' => new IdentifierExpression(
                        'OrgEntities.id'
                    ),
                ]
            );

        $query = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal');
        $query->select(['has_eaccpf' => $query->newExpr()->exists($hasEac)])
            ->enableAutoFields()
            ->contain(['TypeEntities']);

        $IndexComponent->setQuery($query)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('type_entity_id', IndexComponent::FILTER_IN);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $entity) {
                    $entity->setVirtual(['parents', 'deletable']);
                    return $entity;
                }
            )
            ->toArray();
        $this->set('data', $data);

        //options
        /** @var TypeEntitiesTable $TypeEntities */
        $TypeEntities = $this->fetchTable('TypeEntities');
        $types = $TypeEntities->find('list')->orderBy('name')->toArray();
        $this->set('types', $types);
    }

    /**
     * Ajouter une entité
     * @param string $parentId
     * @return Response|void
     * @throws Exception
     */
    public function add(string $parentId = '')
    {
        if ($parentId && !$this->adminTech) {
            $this->Seal->archivalAgencyChilds()
                ->table('OrgEntities')
                ->find('seal')
                ->where(['OrgEntities.id' => $parentId])
                ->firstOrFail();
        }
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $entity = $OrgEntities->newEntity([], ['validate' => false]);
        $disabledTypes = $this->setTypeEntities();

        if ($this->getRequest()->is('post')) {
            $type_id = $this->getRequest()->getData('type_entity_id');
            if (in_array($type_id, $disabledTypes)) {
                $entity->setError(
                    'type_entity_id',
                    __(
                        "Veuillez sélectionner une valeur parmi les valeurs disponibles"
                    )
                );
            } else {
                $data = [
                    'name' => $this->getRequest()->getData('name'),
                    'short_name' => $this->getRequest()->getData('short_name'),
                    'identifier' => $this->getRequest()->getData('identifier'),
                    'type_entity_id' => $type_id,
                    'parent_id' => $parentId
                        ?: $this->getRequest()->getData(
                            'parent_id'
                        ),
                    'description' => $this->getRequest()->getData(
                        'description'
                    ),
                    'archival_agency_id' => $this->archivalAgencyId,
                ];
                $OrgEntities->patchEntity($entity, $data);
            }
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($OrgEntities->save($entity)) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logAdd($entity);
                $entity->setVirtual(['parents', 'deletable']);
                $json = $entity->toArray();
                if (empty($json['parent_id'])) {
                    $json['parent_id'] = 'root';
                }
                $this->Modal->success();
                return $this->renderJson(json_encode($json));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);

        // options
        $query = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.name',
                    'Parents.name',
                ]
            )
            ->contain(['Parents'])
            ->orderBy(['OrgEntities.name' => 'asc', 'OrgEntities.lft' => 'asc']);
        $allNames = [];
        /** @var OrgEntity $entity */
        foreach ($query as $entity) {
            $name = $entity->get('name');
            if ($parent = $entity->get('parent')) {
                $name .= ' (' . $parent->get('name') . ')';
            }
            $allNames[$entity->get('id')] = $name;
        }
        $this->set('parents', $allNames);
        $this->set('parentId', $parentId);
    }

    /**
     * Donne les options pour les type_entities Disponibles
     * @param EntityInterface|null $entity
     * @return array
     */
    private function setTypeEntities(EntityInterface $entity = null): array
    {
        /** @var TypeEntitiesTable $TypeEntities */
        $TypeEntities = $this->fetchTable('TypeEntities');
        $typeEntitiesQuery = $TypeEntities->find()
            ->select(
                [
                    'TypeEntities.id',
                    'TypeEntities.name',
                    'TypeEntities.code',
                    'TypeEntities.active',
                ]
            )
            ->orderBy(['TypeEntities.name' => 'asc'])
            ->contain(
                [
                    'Roles' => function (QueryInterface $q) {
                        return $q->where(
                            [
                                'OR' => [
                                    'Roles.org_entity_id IS' => null,
                                    'Roles.org_entity_id' => $this->archivalAgencyId,
                                ],
                            ]
                        );
                    },
                ]
            );
        $disabledOptions = [];
        $typeEntities = [];

        $usedSV = false;
        $usedSP = false;
        if ($entity) {
            /** @var ArchivesTable $Archives */
            $Archives = $this->fetchTable('Archives');
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            if (
                $entity->get('code') === 'SV'
                && ($Transfers->exists(['transferring_agency_id' => $entity->id])
                || $Archives->exists(['transferring_agency_id' => $entity->id]))
            ) {
                $usedSV = true;
            } elseif (
                $entity->get('code') === 'SP'
                && $Archives->exists(['originating_agency_id' => $entity->id])
            ) {
                $usedSP = true;
            }
        }

        /** @var TypeEntity $type */
        foreach ($typeEntitiesQuery as $type) {
            $title = implode(PHP_EOL, Hash::extract($type, 'roles.{n}.name'));
            $typeEntities[$type->get('id')] = [
                'text' => $type->get('name'),
                'value' => $type->get('id'),
                'title' => sprintf(
                    "%s :\n%s",
                    __("Rôles utilisateurs associés"),
                    $title
                ),
            ];
            $code = $type->get('code');
            if (
                in_array($code, ['SA', 'SE'])
                || !$type->get('active')
                || ($usedSV && $code !== 'SV')
                || ($usedSP && $code !== 'SV' && $code !== 'SP')
            ) {
                $disabledOptions[$code] = $type->get('id');
            }
        }
        $this->set('typeEntitiesDisabled', $disabledOptions);
        $this->set('typeEntities', $typeEntities);
        return $disabledOptions;
    }

    /**
     * Affichage / édition(ajax) d'une entité
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = $this->fetchTable('Eaccpfs');
        /** @var RolesTable $Roles */
        $Roles = $this->fetchTable('Roles');
        $sa = $this->archivalAgency;

        $subQuery = $Eaccpfs->withAlias('a')->find()
            ->select(['a.id'])
            ->where(
                [
                    'a.org_entity_id' => new IdentifierExpression(
                        'OrgEntities.id'
                    ),
                ]
            );
        $entity = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->select(
                [
                    'has_eaccpf' => $Eaccpfs->query()->newExpr()->exists(
                        $subQuery
                    ),
                ]
            )
            ->enableAutoFields()
            ->where(['OrgEntities.id' => $id])
            ->contain(
                [
                    'TypeEntities',
                    'Eaccpfs' => [
                        'sort' => [
                            'from_date' => 'desc',
                            'to_date' => 'desc',
                            'created' => 'desc',
                        ],
                    ],
                    'Users' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q
                            ->orderBy(['Users.username' => 'asc'])
                            ->contain(['Roles'])
                            ->limit($AjaxPaginatorComponent->getConfig('limit'));
                    },
                ]
            )
            ->firstOrFail();
        $entity->setVirtual(['parents', 'deletable']);
        /** @var EntityInterface $user */
        foreach ($entity->get('users') as $user) {
            $user->setVirtual(['deletable']);
        }
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $countUsers = $Users->find()
            ->where(['org_entity_id' => $entity->id])
            ->count();
        $this->set('countUsers', $countUsers);

        $editableIdentifier = $entity->get('editable_identifier');
        $this->set('editableIdentifier', $editableIdentifier);

        if (
            $this->getRequest()->is('put')
            && $this->getRequest()->getData('name')
            && ($this->getRequest()->getData('parent_id') xor Hash::get($entity, 'type_entity.code') === 'SA')
        ) {
            /**
             * Sauvegarde Edition de l'entité
             */
            $code = $entity->get('code');
            $typeEntityId = in_array($code, ['SA', 'SE'])
                ? $entity->get('type_entity_id')
                : $this->getRequest()->getData('type_entity_id');
            $parentId = in_array($code, ['SA', 'SE'])
                ? $entity->get('parent_id')
                : $this->getRequest()->getData('parent_id');
            // assure de ne pas placer l'entité dans un autre service d'archives
            if (!in_array($code, ['SA', 'SE'])) {
                $this->Seal->archivalAgencyChilds()
                    ->table('OrgEntities')
                    ->find('seal')
                    ->where(['OrgEntities.id' => $parentId])
                    ->firstOrFail();
            }

            $data = [
                'name' => $this->getRequest()->getData('name'),
                'short_name' => $this->getRequest()->getData('short_name'),
                'identifier' => $editableIdentifier
                    ? $this->getRequest()->getData('identifier')
                    : $entity->get('identifier'),
                'type_entity_id' => $typeEntityId,
                'parent_id' => $parentId,
                'lft' => $entity->get('lft'),
                'rght' => $entity->get('rght'),
                'description' => $this->getRequest()->getData('description'),
                'archival_agency_id' => $this->archivalAgencyId,
                'active' => $this->getRequest()->getData('active'),
            ];
            $OrgEntities->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $dirty = $entity->isDirty();
            if ($OrgEntities->save($entity)) {
                /** @var EventLoggerComponent $EventLogger */
                $EventLogger = $this->loadComponent('EventLogger');
                $EventLogger->logEdit($entity);
                $this->Modal->success();
                if ($dirty) {
                    $this->setResponse(
                        $this->getResponse()->withHeader(
                            'X-Asalae-Reload',
                            'true'
                        )
                    );
                }
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        // FIXME vérifier si encore utile (table dépréciée)
        /** @var ConversionPoliciesTable $ConversionPolicies */
        $ConversionPolicies = $this->fetchTable('ConversionPolicies');
        $policies = $ConversionPolicies->find()
            ->where(['org_entity_id' => $id]);
        /** @var EntityInterface $policy */
        foreach ($policies as $policy) {
            $media = $entity->get($policy->get('media')) ?: [];
            $usage =& $media[$policy->get('usage')];
            $usage = [
                'method' => $policy->get('method'),
                'format' => $policy->get('format'),
                'params' => [
                    'codec' => $policy->get('codec'),
                    'audio' => $policy->get('audio'),
                ],
            ];
            $entity->set($policy->get('media'), $media);
        }
        $this->set('entity', $entity);

        $this->set('tableIdEaccpf', self::TABLE_EDIT_EACCPF);
        $this->set('tableIdUser', self::TABLE_EDIT_USER);
        $this->set('entity_type', $Eaccpfs->options('entity_type'));

        $roles = $Roles->find('list')
            ->where(['active' => true])
            ->orderBy(['name' => 'asc'])
            ->all()
            ->toArray();
        $this->set('roles', $roles);
        $this->setTypeEntities($entity);

        $parents = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.name',
                    'TypeEntities.code',
                ]
            )
            ->where(
                function (QueryExpression $exp) use ($entity) {
                    return $exp->or(
                        [
                            'OrgEntities.lft <' => $entity->get('lft'),
                            'OrgEntities.rght >' => $entity->get('rght'),
                        ]
                    );
                }
            )
            ->contain(['TypeEntities'])
            ->orderBy(['OrgEntities.name' => 'asc']);
        $parentsOptions = [];
        $disabledOptions = [];
        /** @var OrgEntity $entity */
        foreach ($parents as $entity) {
            $parentsOptions[$entity->get('id')] = $entity->get('name');
            if (
                $entity->get('type_entity')
                && ($code = $entity->get('type_entity')->get('code')) === 'SE'
            ) {
                $disabledOptions[$code] = $entity->get('id');
            }
        }
        $this->set('parents', $parentsOptions);
        $this->set('parentsDisabled', $disabledOptions);
        $this->set('id', $id);

        $rolesUsers = $OrgEntities->findByRoles($sa)
            ->select(['qexists' => 1], true)
            ->where(['OrgEntities.id' => $id, 'agent_type' => 'person'])
            ->disableHydration()
            ->first();
        $this->set('rolesUsers', (bool)$rolesUsers);
        $rolesWs = $OrgEntities->findByRoles($sa)
            ->select(['qexists' => 1], true)
            ->where(['OrgEntities.id' => $id, 'agent_type' => 'software'])
            ->disableHydration()
            ->first();
        $this->set('rolesWs', (bool)$rolesWs);
        $AjaxPaginatorComponent->setViewPaginator($entity->get('users') ?: [], $countUsers);
    }

    /**
     * Pagination des utilisateurs de l'entité
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function paginateUsers(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index', ['model' => 'Users']);
        $query = $this->Seal->archivalAgency()
            ->table('Users')
            ->find('seal')
            ->where(['Users.org_entity_id' => $id])
            ->innerJoinWith('OrgEntities')
            ->contain(['Roles']);
        $IndexComponent->setQuery($query)
            ->filter('username', IndexComponent::FILTER_ILIKE);
        return $AjaxPaginatorComponent->json(
            $query,
            (bool)$this->getRequest()->getQuery('count'),
            function (ResultSet $data) {
                return $data->map(
                    function (EntityInterface $user) {
                        return $user->setVirtual(['deletable']);
                    }
                );
            }
        );
    }

    /**
     * Action du formulaire (put) de modification de politique de conversion
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function editConversions(string $id)
    {
        $request = $this->getRequest();
        $request->allowMethod('put');
        if ((int)$id !== $this->archivalAgencyId) {
            throw new NotFoundException();
        }
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        /** @var ConversionPoliciesTable $ConversionPolicies */
        $ConversionPolicies = $this->fetchTable('ConversionPolicies');
        foreach ($request->getData() as $media => $values) {
            foreach ($values as $usage => $data) {
                $params = $data['params'] ?? [];
                unset($data['params']);
                $base = [
                    'org_entity_id' => $id,
                    'media' => $media,
                    'usage' => $usage,
                ];
                $entity = $ConversionPolicies->find()
                    ->where($base)
                    ->first();
                if (empty($data['method'])) {
                    if ($entity) {
                        $ConversionPolicies->deleteOrFail($entity);
                    }
                    continue;
                }
                if (!$entity) {
                    $entity = $ConversionPolicies->newEntity($base);
                }
                $ConversionPolicies->patchEntity($entity, $base + $data);
                // patch entity ne fonctionne pas avec un array dans params
                if ($params) {
                    $entity->set('params', $params);
                }
                if (!$ConversionPolicies->save($entity)) {
                    $this->Modal->fail();
                    FormatError::logEntityErrors($entity);
                    return $this->renderDataToJson(
                        ['errors' => $entity->getErrors()]
                    );
                }
            }
        }
        $this->Modal->success();
        return $this->renderDataToJson('done');
    }

    /**
     * Permet de modifier le parent_id via requète HTTP
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function editParentId(string $id)
    {
        $this->getRequest()->allowMethod('put');

        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $entity = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'OrgEntities.id' => $id,
                    'TypeEntities.code NOT IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES,
                ]
            )
            ->firstOrFail();

        $parentId = $this->getRequest()->getData('parent_id');
        $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->where(['OrgEntities.id' => $parentId])
            ->firstOrFail();

        $OrgEntities->patchEntity($entity, ['parent_id' => $parentId]);
        try {
            $success = $OrgEntities->save($entity);
        } catch (RuntimeException) { // Cannot use node "x" as parent for entity "y"
            $success = false;
        }
        if ($success) {
            $report = ['success' => true];
        } else {
            $report = [
                'success' => false,
                'errors' => $entity->getErrors(),
            ];
        }
        return $this->renderData($report);
    }

    /**
     * Supprime l'image du logo client et la retire de la configuration
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function deleteBackground(string $id)
    {
        $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->where(['OrgEntities.id' => $id])
            ->firstOrFail();
        $dataDir = rtrim(Configure::read('App.paths.data'), DS);
        $publicDir = rtrim(
            Configure::read('App.paths.config', $dataDir . DS . 'config'),
            DS
        );
        $webrootDir = rtrim(
            Configure::read('OrgEntities.public_dir', WWW_ROOT),
            DS
        );
        /** @var ConfigurationsTable $Configurations */
        $Configurations = $this->fetchTable('Configurations');
        $this->getRequest()->allowMethod('delete');
        $entity = $Configurations->find()
            ->where(
                [
                    'org_entity_id' => $id,
                    'name' => ConfigurationsTable::CONF_BACKGROUND_IMAGE,
                ]
            )
            ->firstOrFail();
        Filesystem::begin();
        Filesystem::setSafemode(true);
        $toDelete = [
            $dataDir . $entity->get('setting'),
            $publicDir . $entity->get('setting'),
            $webrootDir . $entity->get('setting'),
            $dataDir . DS . 'org-entity-data' . DS . $id . DS . 'background.css',
            $publicDir . DS . 'org-entity-data' . DS . $id . DS . 'background.css',
            $webrootDir . DS . 'org-entity-data' . DS . $id . DS . 'background.css',
        ];
        foreach ($toDelete as $path) {
            if (file_exists($path)) {
                Filesystem::remove($path);
            }
        }
        Filesystem::removeEmptySubFolders($dataDir . DS . 'org-entity-data' . DS . $id);
        Filesystem::removeEmptySubFolders($publicDir . DS . 'org-entity-data' . DS . $id);
        Filesystem::removeEmptySubFolders($webrootDir . DS . 'org-entity-data');
        if ($Configurations->delete($entity)) {
            Filesystem::commit();
            $report = 'done';
        } else {
            Filesystem::rollback();
            $report = 'Erreur lors de la suppression';
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Supprime une entité
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $entity = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->where(['OrgEntities.id' => $id])
            ->contain(['TypeEntities']) // pour le champ virtuel "deletable"
            ->firstOrFail();
        if (!$entity->get('deletable')) {
            throw new ForbiddenException(
                __("Tentative de suppression d'une entité protégée")
            );
        }

        $conn = $OrgEntities->getConnection();
        $conn->begin();
        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logDelete($entity);
        if ($OrgEntities->delete($entity)) {
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Donne la liste des roles disponnible pour les utilisateurs d'une entité
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function getAvailablesRoles(string $id)
    {
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $OrgEntities->getEntitySA($id);

        $agentType = $this->getRequest()->getQuery('agent_type');
        $agentTypeCondition = $agentType ? ['Roles.agent_type' => $agentType]
            : [];
        $query = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->select(
                [
                    'Roles.id',
                    'Roles.name',
                ]
            )
            ->where(
                [
                    'OrgEntities.id' => $id,
                ]
            )
            ->innerJoin(
                ['TypeEntities' => 'type_entities'],
                [
                    'TypeEntities.id' => new IdentifierExpression(
                        'OrgEntities.type_entity_id'
                    ),
                ]
            )
            ->innerJoin(
                ['TypesRoles' => 'roles_type_entities'],
                [
                    'TypesRoles.type_entity_id' => new QueryExpression(
                        'TypeEntities.id'
                    ),
                ]
            )
            ->innerJoin(
                ['Roles' => 'roles'],
                [
                    'Roles.id' => new QueryExpression('TypesRoles.role_id'),
                    'OR' => [
                        'Roles.org_entity_id IS' => null,
                        'Roles.org_entity_id' => $this->archivalAgencyId,
                    ],

                ] + $agentTypeCondition
            )
            ->orderByAsc('Roles.name');

        if (!$this->adminTech) {
            $query->where(
                [
                    'OR' => [
                        'Roles.code IS' => null,
                        'Roles.code !=' => RolesTable::CODE_ADMIN,
                    ],
                ]
            );
        }
        $roles = $query
            ->all()
            ->toArray();
        return $this->renderDataToJson(
            array_reduce(
                $roles,
                function ($res, OrgEntity $v) {
                    $res[$v->get('Roles')['id']] = $v->get('Roles')['name'];
                    return $res;
                }
            )
        );
    }

    /**
     * Configuration lié au service d'archives
     * @return Response
     * @throws Exception
     */
    public function editConfig()
    {
        /** @var ConfigurationsTable $Configurations */
        $Configurations = $this->fetchTable('Configurations');

        $entity = $this->archivalAgency;
        $this->set('entity', $entity);

        $form = ArchivalAgencyConfigurationForm::create($this->archivalAgencyId);

        // on charge le fichier logo si il existe
        $uploadFiles = [];
        $confBgImage = $form->entities[ConfigurationsTable::CONF_BACKGROUND_IMAGE] ?? null;
        $file = $confBgImage ? $confBgImage->get('webroot_filename') : null;
        if ($file && is_readable($file)) {
            $url = $confBgImage->get('setting');
            $uploadFiles[] = [
                'id' => 'local',
                'prev' => $url,
                'name' => basename($url),
                'size' => filesize($file),
                'webroot' => true,
            ];
        }
        $this->set('uploadFiles', $uploadFiles);

        if ($this->getRequest()->is('post')) {
            $conn = $Configurations->getConnection();
            $conn->begin();
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($this->getRequest()->getData())) {
                $conn->commit();
                $this->Modal->success();
                $configurations = $Configurations->find()
                    ->where(['org_entity_id' => $this->archivalAgencyId])
                    ->toArray();
                $this->archivalAgency->set('configurations', $configurations);
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $conn->rollback();
                $this->Modal->fail();
            }
        }
        $this->set($form->getOptions());
        $this->set('form', $form);
    }

    /**
     * Création via API
     */
    protected function apiCreate()
    {
        $request = $this->getRequest();
        $active = null;
        if ($request->getData('active')) {
            $active = $request->getData('active') === 'true';
        }
        $secureDataSpaces = [];
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
        if ($request->getData('secure_data_spaces')) {
            foreach (
                explode(
                    ',',
                    $request->getData('secure_data_spaces')
                ) as $id
            ) {
                $secureDataSpace = $SecureDataSpaces->get((int)trim($id));
                $secureDataSpaces[] = $secureDataSpace;
                if ($secureDataSpace->get('org_entity_id')) {
                    throw new BadRequestException(
                        'secure_data_space id=' . $id . ' is not available'
                    );
                }
            }
        }
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $entity = $OrgEntities->newEntity(
            [
                'name' => $request->getData('name'),
                'description' => $request->getData('description'),
                'identifier' => $request->getData('identifier'),
                'parent_id' => $request->getData('parent_id'),
                'type_entity_id' => $request->getData('type_entity_id'),
                'timestamper_id' => $request->getData('timestamper_id'),
                'active' => $active,
                'default_secure_data_space_id' => $request->getData(
                    'default_secure_data_space_id'
                ),
                'archival_agency_id' => $request->getData('archival_agency_id'),
                'is_main_archival_agency' => $request->getData(
                    'is_main_archival_agency'
                ),
            ]
        );

        $conn = $OrgEntities->getConnection();
        $conn->begin();
        if ($OrgEntities->save($entity)) {
            foreach ($secureDataSpaces as $secureDataSpace) {
                $secureDataSpace->set('org_entity_id', $entity->id);
                $SecureDataSpaces->saveOrFail($secureDataSpace);
            }
            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logAdd($entity);
            $conn->commit();
            return $this->renderData(['success' => true, 'id' => $entity->id]);
        } else {
            $conn->rollback();
            return $this->renderData(
                ['success' => false, 'errors' => $entity->getErrors()]
            );
        }
    }
}
