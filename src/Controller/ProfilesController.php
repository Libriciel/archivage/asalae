<?php

/**
 * Asalae\Controller\ProfilesController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Entity\Fileupload;
use Asalae\Model\Table\FileuploadsProfilesTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\ProfilesTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Response;
use Cake\ORM\Table;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

/**
 * profils d'archives
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FileuploadsProfilesTable FileuploadsProfiles
 * @property FileuploadsTable         Fileuploads
 * @property ProfilesTable            Profiles
 */
class ProfilesController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'profiles-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'Profiles.id' => 'asc',
        ],
    ];

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default' => [
                'post' => [
                    'function' => 'apiAdd',
                ],
            ],
        ];
    }

    /**
     * Liste des Profils
     */
    public function index()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        $query = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('seal')
            ->contain(['Fileuploads']);

        $IndexComponent->setQuery($query)
            ->filter('active', IndexComponent::FILTER_IS)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('modified', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('name', IndexComponent::FILTER_ILIKE);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logIndex();
    }

    /**
     * Action d'ajout d'un profil d'archives
     * @throws Exception
     */
    public function add()
    {
        /** @var ProfilesTable $Profiles */
        $Profiles = $this->fetchTable('Profiles');
        $entity = $Profiles->newEntity(
            [
                'org_entity_id' => $this->archivalAgencyId,
                'active' => true,
            ]
        );
        $this->set('entity', $entity);
        if ($this->getRequest()->is('post')) {
            $data = [
                'name' => $this->getRequest()->getData('name'),
                'identifier' => $this->getRequest()->getData('identifier'),
                'description' => $this->getRequest()->getData('description'),
                'active' => $this->getRequest()->getData('active'),
                'fileuploads' => $this->getRequest()->getData('fileuploads'),
                'org_entity_id' => $this->archivalAgencyId,
                // Important pour la validation
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save(
                $Profiles,
                $entity,
                $data,
                [],
                function (EntityInterface $e, Table $m) {
                    /** @var EventLoggerComponent $EventLogger */
                    $EventLogger = $this->loadComponent('EventLogger');
                    $EventLogger->logAdd($e);
                    $this->moveUploadedFile($e);
                    return $m->find()
                        ->where(['Profiles.id' => $e->get('id')])
                        ->contain(['Fileuploads'])
                        ->firstOrFail();
                }
            );
            $this->restoreFiles();
        }
    }

    /**
     * Déplace les fichiers profiles du dossier temporaire au dossier définitif
     * @param EntityInterface $profile
     * @throws Exception
     */
    private function moveUploadedFile(EntityInterface $profile)
    {
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = $this->fetchTable('Fileuploads');
        if (empty($profile->get('fileuploads'))) {
            $fileuploads = $Fileuploads->find()
                ->innerJoinWith('Profiles')
                ->where(['Profiles.id' => $profile->get('id')])
                ->all()
                ->toArray();
            $profile->set('fileuploads', $fileuploads);
        }
        $basedir = $profile->get('basepath');
        /** @var EntityInterface $fileupload */
        foreach ($profile->get('fileuploads') as $fileupload) {
            $path = $basedir . DS . $fileupload->get('name');
            if ($fileupload->get('path') !== $path) {
                try {
                    Filesystem::rename($fileupload->get('path'), $path);
                } catch (IOException) {
                    continue;
                }
                $fileupload->set('path', $path);
                if (!$Fileuploads->save($fileupload)) {
                    Filesystem::rename($path, $fileupload->get('path'));
                }
            }
        }
    }

    /**
     * En cas d'échec sur le formulaire, rempli à nouveau les fichiers uploadés
     */
    private function restoreFiles()
    {
        $uploadData = [];
        if (
            $this->getResponse()->getHeader('X-Asalae-Success')[0] === 'false'
            && $ids = $this->getRequest()->getData('fileuploads._ids')
        ) {
            /** @var FileuploadsTable $Fileuploads */
            $Fileuploads = $this->fetchTable('Fileuploads');
            $query = $Fileuploads->find()
                ->where(['id IN' => $ids]);
            /** @var Fileupload $file */
            foreach ($query as $file) {
                $uploadData[] = [
                    'id' => $file->get('id'),
                    'name' => $file->get('name'),
                    'size' => $file->get('size'),
                    'message' => $file->get('statetrad'),
                ];
            }
        }
        $this->set('uploadData', $uploadData);
    }

    /**
     * Modification d'un profil d'archives
     * @param string $id
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var ProfilesTable $Profiles */
        $Profiles = $this->fetchTable('Profiles');
        $entity = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('seal')
            ->where(['Profiles.id' => $id])
            ->contain(['Fileuploads'])
            ->firstOrFail();
        if ($files = $entity->get('fileuploads')) {
            $uploadData = [];
            /** @var Fileupload $file */
            foreach ($files as $file) {
                $uploadData[] = [
                    'id' => $file->get('id'),
                    'name' => $file->get('name'),
                    'size' => $file->get('size'),
                    'message' => $file->get('statetrad'),
                ];
            }
            $this->set('uploadData', $uploadData);
        }
        $this->set('entity', $entity);
        if ($this->getRequest()->is('put')) {
            $data = [
                'name' => $this->getRequest()->getData('name'),
                'identifier' => $this->getRequest()->getData('identifier'),
                'description' => $this->getRequest()->getData('description'),
                'active' => $this->getRequest()->getData('active'),
                'fileuploads' => $this->getRequest()->getData('fileuploads'),
                'org_entity_id' => $this->archivalAgencyId,
                // Important pour la validation
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save(
                $Profiles,
                $entity,
                $data,
                [],
                function (EntityInterface $e, Table $m) {
                    /** @var EventLoggerComponent $EventLogger */
                    $EventLogger = $this->loadComponent('EventLogger');
                    $EventLogger->logEdit($e);
                    $this->moveUploadedFile($e);
                    return $m->find()
                        ->where(['Profiles.id' => $e->get('id')])
                        ->contain(['Fileuploads'])
                        ->firstOrFail();
                }
            );
            $this->restoreFiles();
        }
    }

    /**
     * Suppression d'un profil d'archives
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        /** @var ProfilesTable $Profiles */
        $Profiles = $this->fetchTable('Profiles');
        $entity = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('seal')
            ->where(['Profiles.id' => $id])
            ->firstOrFail();

        $conn = $Profiles->getConnection();
        $conn->begin();
        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logDelete($entity);
        if ($Profiles->delete($entity, ['unlink' => true])) {
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Visualisation d'un profil
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $entity = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('seal')
            ->where(['Profiles.id' => $id])
            ->contain(['Fileuploads'])
            ->firstOrFail();
        $this->set('entity', $entity);

        /** @var EventLoggerComponent $EventLogger */
        $EventLogger = $this->loadComponent('EventLogger');
        $EventLogger->logAction($entity);
    }

    /**
     * Création via API
     */
    protected function apiAdd()
    {
        $request = $this->getRequest();
        $active = null;
        if ($request->getData('active')) {
            $active = $request->getData('active') === 'true';
        }
        $data = [
            'org_entity_id' => $request->getData('org_entity_id'),
            'name' => $request->getData('name'),
            'identifier' => $request->getData('identifier'),
            'description' => $request->getData('description'),
            'active' => $active,
        ];
        /** @var ProfilesTable $Profiles */
        $Profiles = $this->fetchTable('Profiles');
        $entity = $Profiles->newEntity($data);
        $conn = $Profiles->getConnection();
        $conn->begin();
        if ($Profiles->save($entity)) {
            $upload = $request->getUploadedFile('schema');
            if ($upload) {
                $filename = $upload->getClientFilename();
                $path = Configure::read('Upload.dir', TMP . 'upload')
                    . DS . 'user_' . $this->userId
                    . DS . $filename;
                $upload->moveTo($path);
                /** @var FileuploadsTable $Fileuploads */
                $Fileuploads = $this->fetchTable('Fileuploads');
                $file = $Fileuploads->newUploadedEntity(
                    $filename,
                    $path,
                    $this->userId
                );
                $file->set('locked', true);
                $Fileuploads->saveOrFail($file, ['silent' => true]);
                /** @var FileuploadsProfilesTable $FileuploadsProfiles */
                $FileuploadsProfiles = $this->fetchTable('FileuploadsProfiles');
                $link = $FileuploadsProfiles->newEntity(
                    [
                        'profile_id' => $entity->id,
                        'fileupload_id' => $file->id,
                    ]
                );
                $FileuploadsProfiles->saveOrFail($link);
            }

            /** @var EventLoggerComponent $EventLogger */
            $EventLogger = $this->loadComponent('EventLogger');
            $EventLogger->logAdd($entity);
            $conn->commit();
            return $this->renderData(['success' => true, 'id' => $entity->id]);
        } else {
            $conn->rollback();
            return $this->renderData(
                ['success' => false, 'errors' => $entity->getErrors()]
            );
        }
    }
}
