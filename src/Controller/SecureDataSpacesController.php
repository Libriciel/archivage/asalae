<?php

/**
 * Asalae\Controller\SecureDataSpacesController
 */

namespace Asalae\Controller;

use Asalae\Model\Table\SecureDataSpacesTable;
use Asalae\Model\Table\VolumesTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\RenderDataTrait;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;

/**
 * SecureDataSpaces
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property SecureDataSpacesTable SecureDataSpaces
 * @property VolumesTable          Volumes
 */
class SecureDataSpacesController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use RenderDataTrait;
    use ApiTrait;

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default' => [
                'post' => ['function' => 'apiCreate'],
            ],
        ];
    }

    /**
     * Ajout d'un ECS par api
     * @return Response
     */
    protected function apiCreate()
    {
        $request = $this->getRequest();
        /** @var VolumesTable $Volumes */
        $Volumes = $this->fetchTable('Volumes');
        foreach (explode(',', $request->getData('volumes')) as $volume_id) {
            $volume = $Volumes->get((int)trim($volume_id));
            if ($volume->get('secure_data_space_id')) {
                throw new BadRequestException(
                    'The volume id=' . $volume_id . ' is already used'
                );
            }
            $volumes[] = $volume;
        }
        if (empty($volumes)) {
            throw new BadRequestException('no volumes found');
        }

        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
        $sds = $SecureDataSpaces->newEntity(
            [
                'name' => $request->getData('name'),
                'description' => $request->getData('description'),
            ]
        );
        $conn = $SecureDataSpaces->getConnection();
        $conn->begin();
        if ($SecureDataSpaces->save($sds)) {
            foreach ($volumes as $volume) {
                $volume->set('secure_data_space_id', $sds->id);
                $Volumes->saveOrFail($volume);
            }
            $conn->commit();
            return $this->renderData(['success' => true, 'id' => $sds->id]);
        } else {
            $conn->rollback();
            return $this->renderData(
                ['success' => false, 'errors' => $sds->getErrors()]
            );
        }
    }
}
