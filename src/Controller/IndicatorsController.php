<?php

/**
 * Asalae\Controller\IndicateursController
 */

namespace Asalae\Controller;

use Asalae\Form\IndicatorsCsvForm;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Table\AgreementsTable;
use Asalae\Model\Table\ArchiveIndicatorsTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\MessageIndicatorsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\ProfilesTable;
use Asalae\Model\Table\TransfersTable;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\NotFoundException;
use Cake\I18n\Date as CakeDate;
use Cake\Utility\Hash;
use DateTime;
use Exception;

/**
 * Indicateurs
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AgreementsTable        $Agreements
 * @property ArchiveIndicatorsTable $ArchiveIndicators
 * @property ArchivesTable          $Archives
 * @property MessageIndicatorsTable $MessageIndicators
 * @property OrgEntitiesTable       OrgEntities
 * @property ProfilesTable          $Profiles
 * @property TransfersTable         $Transfers
 */
class IndicatorsController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    public const string TRANSFERRING = 'transferring';
    public const string ORIGINATING = 'originating';
    /**
     * Tableaux de résultats vides pour complétion des données
     */
    private const array EMPTY_ARCHIVES_RESULTS = [
        'archives_volume_total' => 0,
        'archives_volume_doc' => 0,
        'archives_volume_cons' => 0,
        'archives_volume_diff' => 0,
        'archives_volume_horo' => 0,
        'archives_count' => 0,
        'archives_unit_count' => 0,
        'archives_doc_count' => 0,
    ];
    private const array EMPTY_TRANSFER_RESULTS = [
        'transfers_volume' => 0,
        'transfers_pj_count' => 0,
        'transfers_count' => 0,
        'transfers_max_traitement' => 0,
    ];
    private const array EMPTY_REJECTED_TRANSFER_RESULTS = [
        'transfers_rejected_count' => 0,
    ];
    private const array EMPTY_COM_RESULTS = [
        'communications_volume_total' => 0,
        'communications_doc_count' => 0,
        'communications_count' => 0,
    ];
    private const array EMPTY_COM_REJECTED_RESULTS = [
        'communications_request_rejected' => 0,
    ];
    private const array EMPTY_DEROGATION_RESULTS = [
        'communications_derogation_intructed' => 0,
    ];
    private const array EMPTY_DEROGATION_REJECTED_RESULTS = [
        'communications_derogation_rejected' => 0,
    ];
    private const array EMPTY_ELIMINATION_RESULTS = [
        'eliminations_volume_total' => 0,
        'eliminations_doc_count' => 0,
        'eliminations_count' => 0,
    ];
    private const array EMPTY_ELIMINATION_REJECTED_RESULTS = [
        'eliminations_rejected' => 0,
    ];
    private const array EMPTY_REST_RESULTS = [
        'restitutions_volume_total' => 0,
        'restitutions_doc_count' => 0,
        'restitutions_count' => 0,
    ];
    private const array EMPTY_REST_REJECTED_RESULTS = [
        'restitutions_rejected' => 0,
    ];
    private const array EMPTY_OUTGOING_RESULTS = [
        'outgoing_volume_total' => 0,
        'outgoing_doc_count' => 0,
        'outgoing_count' => 0,
        'outgoing_max_traitement' => 0,
    ];
    private const array EMPTY_OUTGOING_REJECTED_RESULTS = [
        'outgoing_rejected' => 0,
    ];
    private const array EMPTY_MESSAGES_RESULTS = self::EMPTY_TRANSFER_RESULTS
    + self::EMPTY_REJECTED_TRANSFER_RESULTS
    + self::EMPTY_COM_RESULTS
    + self::EMPTY_COM_REJECTED_RESULTS
    + self::EMPTY_DEROGATION_RESULTS
    + self::EMPTY_DEROGATION_REJECTED_RESULTS
    + self::EMPTY_ELIMINATION_RESULTS
    + self::EMPTY_ELIMINATION_REJECTED_RESULTS
    + self::EMPTY_REST_RESULTS
    + self::EMPTY_REST_REJECTED_RESULTS
    + self::EMPTY_OUTGOING_RESULTS
    + self::EMPTY_OUTGOING_REJECTED_RESULTS;
    /**
     * @var array l'utilisation conditionne certains calculs
     */
    private $filters = [];

    /**
     * Liste les enregistrements par service versant
     */
    public function indexTransferringAgency()
    {
        /** @use IndexComponent */
        $this->loadComponent('AsalaeCore.Index');

        if (!$this->userCanSeeIndicators($this->user, $this->orgEntity)) {
            return;
        }

        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');

        $data = $this->getData(self::TRANSFERRING);
        $this->set('data', $data);

        // retrait des données des SO pour éviter de les compter 2 fois
        $data = array_filter(
            $data,
            fn(EntityInterface $e) => $e->get('type_entity')->get('code') !== 'SO'
        );

        $totalSA = $this->getTotalSA($data);
        $this->set('totalSA', $totalSA);

        // options
        $ta = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(['TypeEntities.code in' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES_WITH_ORGANIZATIONAL])
            ->orderBy(['OrgEntities.name' => 'asc']);
        $this->set('transferring_agencies', $ta);

        $agreements = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('sealList')
            ->orderBy(['Agreements.name' => 'asc']);
        $this->set('agreements', $agreements->all());

        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('sealList')
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles->all());
    }

    /**
     * L'entité de l'utilisateur ainsi que son rôle peuvent limiter son accès
     * @param array|EntityInterface|null $user
     * @param array|EntityInterface|null $orgEntity
     * @return bool
     */
    public static function userCanSeeIndicators($user = null, $orgEntity = null): bool
    {
        if (!$user || !$orgEntity) {
            return false;
        }

        $typeEntity = Hash::get($orgEntity, 'type_entity.code');
        // Type d'entités non concerné par les indicateurs
        if (!in_array($typeEntity, ['SA', 'SV', 'SO', 'SE'])) {
            return false;
        }

        // Service d'archive mais avec role ne permettant pas de visualiser les entités filles
        if ($typeEntity === 'SO' && !Hash::get($user, 'role.hierarchical_view')) {
            return false;
        }

        return true;
    }

    /**
     * Tableau d'entités avec leurs données
     *      (aucune donnée pour une entité = pas d'entrée pour l'entité)
     * @param string $by type de requete : transferring ou originating
     * @return OrgEntity[]
     * @throws Exception
     */
    protected function getData(string $by): array
    {
        $targetField = $by . '_agency_id';
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        $hierarchicalView = Hash::get($this->user, 'role.hierarchical_view');

        // si orgEntity = SE ; son lft est plus petit que celui du service d'archives
        $lft = max(Hash::get($this->orgEntity, 'lft'), Hash::get($this->archivalAgency, 'lft'));
        $rght = min(Hash::get($this->orgEntity, 'rght'), Hash::get($this->archivalAgency, 'rght'));
        $whereEntities = $hierarchicalView
            ? [ // entité de l'user et filles
                'OrgEntities.lft >=' => $lft,
                'OrgEntities.rght <=' => $rght,
                'TypeEntities.code in' => $by === self::TRANSFERRING
                    ? OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES_WITH_ORGANIZATIONAL
                    : OrgEntitiesTable::CODE_ORIGINATING_AGENCIES_WITH_ORGANIZATIONAL,
            ]
            : [ // seulement entité de l'user
                'OrgEntities.id' => $this->orgEntityId,
            ];

        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $query = $OrgEntities->find()
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.name',
                    'OrgEntities.lft',
                    'Parents.lft',
                    'OrgEntities.rght',
                    'Parents.rght',
                    'Parents.id',
                    'TypeEntities.code',
                ]
            )
            ->where($whereEntities)
            ->contain(['Parents', 'TypeEntities'])
            ->orderBy(['OrgEntities.lft' => 'asc'])
            ->formatResults(
                function ($results) {
                    return $results->combine(
                        'id',
                        fn($entity) => $entity
                    );
                },
                true
            );

        // filters
        $filterEntityId = $this->getRequest()->getQuery($targetField . '.0');
        if ($hierarchicalView && $filterEntityId !== null) {
            $filterEntity = $OrgEntities->find()
                ->select(
                    [
                        'OrgEntities.name',
                        'OrgEntities.id',
                        'OrgEntities.lft',
                        'OrgEntities.rght',
                    ]
                )
                ->where(
                    [
                        'OrgEntities.id' => $filterEntityId,
                        'OrgEntities.lft >=' => Hash::get($this->orgEntity, 'lft'),
                        'OrgEntities.rght <=' => Hash::get($this->orgEntity, 'rght'),
                        'TypeEntities.code in' => $by === self::TRANSFERRING
                            ? OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES_WITH_ORGANIZATIONAL
                            : OrgEntitiesTable::CODE_ORIGINATING_AGENCIES_WITH_ORGANIZATIONAL,
                    ]
                )
                ->contain(['TypeEntities'])
                ->firstOrFail();

            $this->filters[$by . '_agency'] = $filterEntity->get('name');
            $query->where(
                [
                    'OrgEntities.lft >=' => $filterEntity->get('lft'),
                    'OrgEntities.rght <=' => $filterEntity->get('rght'),
                ]
            );
        }

        $agreement_id = $this->getRequest()->getQuery('agreement_id.0');
        if ($agreement_id !== null) {
            /** @var AgreementsTable $Agreements */
            $Agreements = $this->fetchTable('Agreements');
            $agreement = $Agreements->find()
                ->select('name')
                ->where(['id' => $agreement_id])
                ->firstOrFail();
            $this->filters['agreement'] = $agreement->get('name');
            $this->filters['agreement_id'] = $agreement_id;
        }

        $profile_id = $this->getRequest()->getQuery('profile_id.0');
        if ($profile_id !== null) {
            /** @var ProfilesTable $Profiles */
            $Profiles = $this->fetchTable('Profiles');
            $profile = $Profiles->find()
                ->select('name')
                ->where(['id' => $profile_id])
                ->firstOrFail();
            $this->filters['profile'] = $profile->get('name');
            $this->filters['profile_id'] = $profile_id;
        }

        $date_begin = $this->getRequest()->getQuery('date_begin.0');
        $date_end = $this->getRequest()->getQuery('date_end.0');
        if ($date_begin !== null) {
            $this->filters['date_begin'] = CakeDate::parseDate($date_begin);
        }
        if ($date_end !== null) {
            $this->filters['date_end'] = CakeDate::parseDate($date_end);
        }

        /** @var OrgEntity[] $orgEntities */
        $orgEntities = $query->all()
            ->toArray();

        $indexedArchivesData = $this->getArchivesData($orgEntities, $by);
        $indexedMessagesData = $this->getMessagesData($orgEntities, $by);

        foreach ($orgEntities as $entity) {
            $entityArchivesData = isset($indexedArchivesData[$entity->get('id')])
                ? $indexedArchivesData[$entity->get('id')]->toArray()
                : self::EMPTY_ARCHIVES_RESULTS;
            $entityMessagesData = isset($indexedMessagesData[$entity->get('id')])
                ? $indexedMessagesData[$entity->get('id')]
                : self::EMPTY_MESSAGES_RESULTS;
            $entityData = $entityArchivesData + $entityMessagesData;
            unset($entityData[$targetField]); // unecessary entry

            // calcul du level pour l'indentation du tableau
            $parentId = $entity->get('parent')
                ? $entity->get('parent')->get('id')
                : null;
            if (isset($orgEntities[$parentId])) {
                $entity->set('lvl', $orgEntities[$parentId]->get('lvl') + 1);

                // si on est pas un SO, on va ajouter les données aux SO parents
                if ($entity->get('type_entity')->get('code') !== 'SO') {
                    $this->addDataToParent($orgEntities, $parentId, $entityData);
                }
            } else {
                $entity->set('lvl', 0);
            }
            $entity->set('indicators', $entityData);
        }

        return array_filter( // tri des lignes vides
            $orgEntities,
            function ($e) {
                return array_sum($e->get('indicators')) > 0;
            }
        );
    }

    /**
     * Récupération des données d'archives
     * @param array  $orgEntities
     * @param string $by          transferring ou originating
     * @return array
     * @throws Exception
     */
    protected function getArchivesData(array $orgEntities, string $by): array
    {
        $targetField = $by . '_agency_id';
        /** @var ArchiveIndicatorsTable $ArchiveIndicators */
        $ArchiveIndicators = $this->fetchTable('ArchiveIndicators');
        $q = $ArchiveIndicators->query();
        $query = $ArchiveIndicators->find()
            ->select(
                [
                    $targetField,
                    'archives_volume_total' => $q->func()->sum(
                        'original_size'
                        . ' + preservation_size'
                        . ' + dissemination_size'
                        . ' + timestamp_size'
                    ),
                    'archives_volume_doc' => $q->func()->sum('original_size'),
                    'archives_volume_cons' => $q->func()->sum('preservation_size'),
                    'archives_volume_diff' => $q->func()->sum('dissemination_size'),
                    'archives_volume_horo' => $q->func()->sum('timestamp_size'),
                    'archives_count' => $q->func()->sum('archive_count'),
                    'archives_unit_count' => $q->func()->sum('units_count'),
                    'archives_doc_count' => $q->func()->sum('original_count'),
                ]
            )
            ->where([$targetField . ' IN' => Hash::extract($orgEntities, '{n}.id')])
            ->groupBy([$targetField]);

        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        if (!empty($this->filters['date_begin']) && !empty($this->filters['date_end'])) {
            $query->andWhere(
                $this->Condition->dateBetween(
                    'archive_date',
                    $this->filters['date_begin'],
                    $this->filters['date_end']
                )
            );
        } elseif (!empty($this->filters['date_begin'])) {
            $query->andWhere(
                $this->Condition->dateOperator(
                    'archive_date',
                    '>=',
                    $this->filters['date_begin']
                )
            );
        } elseif (!empty($this->filters['date_end'])) {
            $query->andWhere(
                $this->Condition->dateOperator(
                    'archive_date',
                    '<=',
                    $this->filters['date_end']
                )
            );
        }
        if (!empty($this->filters['agreement_id'])) {
            $query->andWhere(['agreement_id' => $this->filters['agreement_id']]);
        }
        if (!empty($this->filters['profile_id'])) {
            $query->andWhere(['profile_id' => $this->filters['profile_id']]);
        }

        return Hash::combine(
            $query->all()->toArray(),
            '{n}.' . $targetField,
            '{n}'
        );
    }

    /**
     * Récupération des données de messages
     * @param array  $orgEntities
     * @param string $by          transferring ou originating
     * @return array
     */
    protected function getMessagesData(array $orgEntities, string $by): array
    {
        $targetField = $by . '_agency_id';
        /** @var MessageIndicatorsTable $MessageIndicators */
        $MessageIndicators = $this->fetchTable('MessageIndicators');
        $q = $MessageIndicators->query();

        $where = [
            $targetField . ' IN' => array_map(
                function ($e) {
                    return $e->get('id');
                },
                $orgEntities
            ),
            'message_date >=' => $this->filters['date_begin'] ?? 0,
            'message_date <=' => $this->filters['date_end'] ?? 'NOW',
        ]
            + (isset($this->filters['agreement_id'])
                ? ['agreement_id' => $this->filters['agreement_id']] : [])
            + (isset($this->filters['profile_id'])
                ? ['profile_id' => $this->filters['profile_id']] : []);

        $transfersData = $MessageIndicators->find()
            ->select(
                [
                    $targetField,
                    'transfers_volume' => $q->func()->sum('original_files_size'),
                    'transfers_pj_count' => $q->func()->sum('original_files_count'),
                    'transfers_count' => $q->func()->sum('message_count'),
                    'transfers_max_traitement' => $q->func()->max('validation_duration'),
                ]
            )
            ->where(
                $where + ['message_type' => MessageIndicatorsTable::IN_TRANSFER]
            )
            ->groupBy([$targetField])
            ->all()
            ->toArray();
        $transfersData = Hash::combine(
            $transfersData,
            '{n}.' . $targetField,
            '{n}'
        );

        $rejectedTransfersData = $MessageIndicators->find()
            ->select(
                [
                    $targetField,
                    'transfers_rejected_count' => $q->func()->sum('message_count'),
                ]
            )
            ->where(
                $where + [
                    'accepted' => false,
                    'message_type' => MessageIndicatorsTable::IN_TRANSFER,
                ]
            )
            ->groupBy([$targetField])
            ->all()
            ->toArray();
        $rejectedTransfersData = Hash::combine(
            $rejectedTransfersData,
            '{n}.' . $targetField,
            '{n}'
        );

        $acceptedDeliveryData = $MessageIndicators->find()
            ->select(
                [
                    $targetField,
                    'communications_volume_total' => $q->func()->sum('original_files_size'),
                    'communications_doc_count' => $q->func()->sum('original_files_count'),
                    'communications_count' => $q->func()->sum('message_count'),
                ]
            )
            ->where(
                $where + [
                    'accepted' => true,
                    'message_type' => MessageIndicatorsTable::DELIVERY,
                ]
            )
            ->groupBy([$targetField])
            ->all()
            ->toArray();
        $acceptedDeliveryData = Hash::combine(
            $acceptedDeliveryData,
            '{n}.' . $targetField,
            '{n}'
        );

        $rejectedDeliveryData = $MessageIndicators->find()
            ->select(
                [
                    $targetField,
                    'communications_request_rejected' => $q->func()->sum('message_count'),
                ]
            )
            ->where(
                $where + [
                    'accepted' => false,
                    'message_type' => MessageIndicatorsTable::DELIVERY,
                ]
            )
            ->groupBy([$targetField])
            ->all()
            ->toArray();
        $rejectedDeliveryData = Hash::combine(
            $rejectedDeliveryData,
            '{n}.' . $targetField,
            '{n}'
        );

        $derogationData = $MessageIndicators->find()
            ->select(
                [
                    $targetField,
                    'communications_derogation_intructed' => $q->func()->sum('message_count'),
                ]
            )
            ->where(
                $where + [
                    'derogation' => true,
                    'message_type' => MessageIndicatorsTable::DELIVERY,
                ]
            )
            ->groupBy([$targetField])
            ->all()
            ->toArray();
        $derogationData = Hash::combine(
            $derogationData,
            '{n}.' . $targetField,
            '{n}'
        );

        $rejectedDerogationData = $MessageIndicators->find()
            ->select(
                [
                    $targetField,
                    'communications_derogation_rejected' => $q->func()->sum('message_count'),
                ]
            )
            ->where(
                $where
                + [
                    'derogation' => true,
                    'accepted' => false,
                    'message_type' => MessageIndicatorsTable::DELIVERY,
                ]
            )
            ->groupBy([$targetField])
            ->all()
            ->toArray();
        $rejectedDerogationData = Hash::combine(
            $rejectedDerogationData,
            '{n}.' . $targetField,
            '{n}'
        );

        $eliminationData = $MessageIndicators->find()
            ->select(
                [
                    $targetField,
                    'eliminations_volume_total' => $q->func()->sum('original_files_size'),
                    'eliminations_doc_count' => $q->func()->sum('original_files_count'),
                    'eliminations_count' => $q->func()->sum('message_count'),
                ]
            )
            ->where(
                $where + [
                    'accepted' => true,
                    'message_type' => MessageIndicatorsTable::DESTRUCTION,
                ]
            )
            ->groupBy([$targetField])
            ->all()
            ->toArray();
        $eliminationData = Hash::combine(
            $eliminationData,
            '{n}.' . $targetField,
            '{n}'
        );

        $rejectedEliminationData = $MessageIndicators->find()
            ->select(
                [
                    $targetField,
                    'eliminations_rejected' => $q->func()->sum('message_count'),
                ]
            )
            ->where(
                $where + [
                    'accepted' => false,
                    'message_type' => MessageIndicatorsTable::DESTRUCTION,
                ]
            )
            ->groupBy([$targetField])
            ->all()
            ->toArray();
        $rejectedEliminationData = Hash::combine(
            $rejectedEliminationData,
            '{n}.' . $targetField,
            '{n}'
        );

        $restitutionData = $MessageIndicators->find()
            ->select(
                [
                    $targetField,
                    'restitutions_volume_total' => $q->func()->sum('original_files_size'),
                    'restitutions_doc_count' => $q->func()->sum('original_files_count'),
                    'restitutions_count' => $q->func()->sum('message_count'),
                ]
            )
            ->where(
                $where + [
                    'accepted' => true,
                    'message_type' => MessageIndicatorsTable::RESTITUTION,
                ]
            )
            ->groupBy([$targetField])
            ->all()
            ->toArray();
        $restitutionData = Hash::combine(
            $restitutionData,
            '{n}.' . $targetField,
            '{n}'
        );

        $rejectedRestitutionData = $MessageIndicators->find()
            ->select(
                [
                    $targetField,
                    'restitutions_rejected' => $q->func()->sum('message_count'),
                ]
            )
            ->where(
                $where + [
                    'accepted' => false,
                    'message_type' => MessageIndicatorsTable::RESTITUTION,
                ]
            )
            ->groupBy([$targetField])
            ->all()
            ->toArray();
        $rejectedRestitutionData = Hash::combine(
            $rejectedRestitutionData,
            '{n}.' . $targetField,
            '{n}'
        );

        $outgoingData = $MessageIndicators->find()
            ->select(
                [
                    $targetField,
                    'outgoing_volume_total' => $q->func()->sum('original_files_size'),
                    'outgoing_doc_count' => $q->func()->sum('original_files_count'),
                    'outgoing_count' => $q->func()->sum('message_count'),
                    'outgoing_max_traitement' => $q->func()->max('validation_duration'),
                ]
            )
            ->where(
                $where + [
                    'accepted' => true,
                    'message_type' => MessageIndicatorsTable::OUT_TRANSFER,
                ]
            )
            ->groupBy([$targetField])
            ->all()
            ->toArray();
        $outgoingData = Hash::combine(
            $outgoingData,
            '{n}.' . $targetField,
            '{n}'
        );

        $rejectedOutgoingData = $MessageIndicators->find()
            ->select(
                [
                    $targetField,
                    'outgoing_rejected' => $q->func()->sum('message_count'),
                ]
            )
            ->where(
                $where + [
                    'accepted' => false,
                    'message_type' => MessageIndicatorsTable::OUT_TRANSFER,
                ]
            )
            ->groupBy([$targetField])
            ->all()
            ->toArray();
        $rejectedOutgoingData = Hash::combine(
            $rejectedOutgoingData,
            '{n}.' . $targetField,
            '{n}'
        );

        $ids = array_keys(
            $transfersData
            + $rejectedTransfersData
            + $acceptedDeliveryData
            + $rejectedDeliveryData
            + $derogationData
            + $rejectedDerogationData
            + $eliminationData
            + $rejectedEliminationData
            + $restitutionData
            + $rejectedRestitutionData
            + $outgoingData
            + $rejectedOutgoingData
        );
        $results = [];
        foreach ($ids as $id) {
            $results[$id]
                = (isset($transfersData[$id])
                    ? $transfersData[$id]->toArray() : self::EMPTY_TRANSFER_RESULTS)
                + (isset($rejectedTransfersData[$id])
                    ? $rejectedTransfersData[$id]->toArray() : self::EMPTY_REJECTED_TRANSFER_RESULTS)
                + (isset($acceptedDeliveryData[$id])
                    ? $acceptedDeliveryData[$id]->toArray() : self::EMPTY_COM_RESULTS)
                + (isset($rejectedDeliveryData[$id])
                    ? $rejectedDeliveryData[$id]->toArray() : self::EMPTY_COM_REJECTED_RESULTS)
                + (isset($derogationData[$id])
                    ? $derogationData[$id]->toArray() : self::EMPTY_DEROGATION_RESULTS)
                + (isset($rejectedDerogationData[$id])
                    ? $rejectedDerogationData[$id]->toArray() : self::EMPTY_DEROGATION_REJECTED_RESULTS)
                + (isset($eliminationData[$id])
                    ? $eliminationData[$id]->toArray() : self::EMPTY_ELIMINATION_RESULTS)
                + (isset($rejectedEliminationData[$id])
                    ? $rejectedEliminationData[$id]->toArray() : self::EMPTY_ELIMINATION_REJECTED_RESULTS)
                + (isset($restitutionData[$id])
                    ? $restitutionData[$id]->toArray() : self::EMPTY_REST_RESULTS)
                + (isset($rejectedRestitutionData[$id])
                    ? $rejectedRestitutionData[$id]->toArray() : self::EMPTY_REST_REJECTED_RESULTS)
                + (isset($outgoingData[$id])
                    ? $outgoingData[$id]->toArray() : self::EMPTY_OUTGOING_RESULTS)
                + (isset($rejectedOutgoingData[$id])
                    ? $rejectedOutgoingData[$id]->toArray() : self::EMPTY_OUTGOING_REJECTED_RESULTS);
        }
        return $results;
    }

    /**
     * Ajoute les données aux SO parents
     * @param EntityInterface[] $orgEntities
     * @param int               $parentId
     * @param array             $entityData
     * @return void
     */
    private function addDataToParent(array $orgEntities, int $parentId, array $entityData): void
    {
        $parent = $orgEntities[$parentId] ?? false;
        while ($parent && $parent->get('type_entity')->get('code') === 'SO') {
            // ajouter les $entityData au parent,
            $addedValues = [];
            foreach (array_keys($entityData + $parent->get('indicators')) as $key) {
                $addedValues[$key] = $entityData[$key] + $parent->get('indicators')[$key];
            }
            $parent->set('indicators', $addedValues);
            $parent = $orgEntities[$parent->get('parent')->get('id')];
        }
    }

    /**
     * Effectue le total des données pour le Service d'Archive,
     *  retourne null si l'entité de l'user n'est pas un SA ou si filtres
     * @param array $data
     * @return array|null
     */
    protected function getTotalSA(array $data): ?array
    {
        if (Hash::get($this->orgEntity, 'type_entity.code') === 'SA' && empty($this->filters)) {
            $totalSA = [
                'archives_volume_total' => 0,
                'archives_volume_doc' => 0,
                'archives_volume_cons' => 0,
                'archives_volume_diff' => 0,
                'archives_volume_horo' => 0,
                'archives_count' => 0,
                'archives_unit_count' => 0,
                'archives_doc_count' => 0,
                'transfers_volume' => 0,
                'transfers_pj_count' => 0,
                'transfers_count' => 0,
                'transfers_max_traitement' => 0,
                'transfers_rejected_count' => 0,
                'communications_volume_total' => 0,
                'communications_doc_count' => 0,
                'communications_count' => 0,
                'communications_request_rejected' => 0,
                'communications_derogation_intructed' => 0,
                'communications_derogation_rejected' => 0,
                'eliminations_volume_total' => 0,
                'eliminations_doc_count' => 0,
                'eliminations_count' => 0,
                'eliminations_rejected' => 0,
                'restitutions_volume_total' => 0,
                'restitutions_doc_count' => 0,
                'restitutions_count' => 0,
                'restitutions_rejected' => 0,
                'outgoing_volume_total' => 0,
                'outgoing_doc_count' => 0,
                'outgoing_count' => 0,
                'outgoing_max_traitement' => 0,
                'outgoing_rejected' => 0,
            ];
            foreach ($totalSA as $key => $val) {
                $totalSA[$key] = array_reduce(
                    $data,
                    function ($acc, $e) use ($key) {
                        return strpos($key, '_max_traitement') !== false
                            ? max($acc, $e->get('indicators')[$key])
                            : $acc + $e->get('indicators')[$key];
                    },
                    0
                );
            }
        }
        return $totalSA ?? null;
    }

    /**
     * Liste les enregistrements par service producteur
     */
    public function indexOriginatingAgency()
    {
        /** @use IndexComponent */
        $this->loadComponent('AsalaeCore.Index');

        if (!$this->userCanSeeIndicators($this->user, $this->orgEntity)) {
            return;
        }

        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');

        $data = $this->getData(self::ORIGINATING);
        $this->set('data', $data);

        // retrait des données des SO pour éviter de les compter 2 fois
        $data = array_filter(
            $data,
            fn(EntityInterface $e) => $e->get('type_entity')->get('code') !== 'SO'
        );

        $totalSA = $this->getTotalSA($data);
        $this->set('totalSA', $totalSA);

        // options
        $ta = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(['TypeEntities.code in' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES_WITH_ORGANIZATIONAL])
            ->orderBy(['OrgEntities.name' => 'asc']);
        $this->set('originating_agencies', $ta);

        $agreements = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('sealList')
            ->orderBy(['Agreements.name' => 'asc']);
        $this->set('agreements', $agreements->all());

        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('sealList')
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles->all());
    }

    /**
     * Modale de l'export csv
     */
    public function csv()
    {
        if (!$this->userCanSeeIndicators($this->user, $this->orgEntity)) {
            return;
        }

        $form = new IndicatorsCsvForm();
        $this->set('form', $form);

        $by = $this->getRequest()->getQuery('by');
        if (!in_array($by, [self::TRANSFERRING, self::ORIGINATING])) {
            throw new NotFoundException();
        }

        if ($this->getRequest()->is('post')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $data = $this->getData($by);
            $filters = $this->filters;
            unset($filters['agreement_id']);
            unset($filters['profile_id']);
            $form->setIndicatorsData($data, $filters);
            if ($form->execute($this->getRequest()->getData() + ['by' => $by])) {
                $this->Modal->success();

                $filename = sprintf(
                    '%s_asalae_%s_%s.csv',
                    __("indicateurs"),
                    $by === self::TRANSFERRING ? __("service_versant") : __("service_producteur"),
                    (new DateTime())->format('Ymd')
                );

                return $this->getResponse()
                    ->withHeader('Content-Description', 'File Transfer')
                    ->withHeader('Content-Type', 'text/csv')
                    ->withHeader('Content-Length', $size = strlen($form->csv))
                    ->withHeader('File-Size', $size)
                    ->withHeader(
                        'Content-Disposition',
                        'attachment; filename="' . $filename . '"'
                    )
                    ->withHeader('Expires', '0')
                    ->withHeader('Cache-Control', 'must-revalidate')
                    ->withHeader('Pragma', 'public')
                    ->withStringBody($form->csv);
            } else {
                $this->Modal->fail();
            }
        }
    }
}
