<?php

/**
 * Asalae\Controller\BatchTreatmentsController
 */

namespace Asalae\Controller;

use Asalae\Form\BatchTreatmentForm;
use Asalae\Model\Table\AccessRuleCodesTable;
use Asalae\Model\Table\AppraisalRuleCodesTable;
use Asalae\Model\Table\ArchiveUnitsBatchTreatmentsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\BatchTreatmentsTable;
use Asalae\Model\Table\KeywordsTable;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Beanstalk\Utility\Beanstalk;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Http\Response;
use Cake\ORM\Query;
use Exception;
use Psr\Http\Message\ResponseInterface;

/**
 * BatchTreatments
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchiveUnitsBatchTreatmentsTable ArchiveUnitsBatchTreatments
 * @property BatchTreatmentsTable             BatchTreatments
 * @property KeywordsTable                    Keywords
 */
class BatchTreatmentsController extends AppController
{
    /**
     * Traits
     */
    use RenderDataTrait;

    /**
     * Formulaire pour effectuer le traitement par lot
     */
    public function add1()
    {
        $request = $this->getRequest();
        $form = new BatchTreatmentForm();
        $this->set('formEntity', $form);

        $subquery = $this->queryArchiveUnit();

        if ($request->is('post')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($request->getData())) {
                /** @var BatchTreatmentsTable $BatchTreatments */
                $BatchTreatments = $this->fetchTable('BatchTreatments');
                $entity = $BatchTreatments->newEntity(
                    [
                        'archival_agency_id' => $this->archivalAgencyId,
                        'user_id' => $this->userId,
                        'state' => $BatchTreatments->initialState,
                        'data' => $form->getData(),
                    ]
                );
                $BatchTreatments->saveOrFail($entity);

                /** @var ArchiveUnitsBatchTreatmentsTable $ArchiveUnitsBatchTreatments */
                $ArchiveUnitsBatchTreatments = $this->fetchTable('ArchiveUnitsBatchTreatments');
                $links = clone $subquery;
                $links->select(['ArchiveUnits.id', 'ArchiveUnits.archive_id'], true)
                    ->disableHydration();
                foreach ($links as $link) {
                    $ArchiveUnitsBatchTreatments->insertQuery()
                        ->insert(['archive_unit_id', 'batch_treatment_id'])
                        ->values(
                            [
                                'archive_unit_id' => $link['id'],
                                'batch_treatment_id' => $entity->id,
                            ]
                        )
                        ->execute();
                }

                $this->Modal->success();
                $this->Modal->step(
                    'addBatchTreatmentStep2(' . $entity->id . ')'
                );
            } else {
                FormatError::logFormErrors($form);
                $this->Modal->fail();
            }
        }

        $archivesUnitsSedaNotV2 = (clone $subquery)
            ->innerJoinWith('Archives.Transfers')
            ->where(['Transfers.message_version NOT LIKE' => 'seda2%'])
            ->count();

        /** @var AccessRuleCodesTable $AccessRuleCodes */
        $AccessRuleCodes = $this->fetchTable('AccessRuleCodes');
        /** @var AppraisalRuleCodesTable $AppraisalRuleCodes */
        $AppraisalRuleCodes = $this->fetchTable('AppraisalRuleCodes');

        $this->set('archivesUnitsSedaNotV2', $archivesUnitsSedaNotV2);
        $accessRuleCodes = $AccessRuleCodes->getRuleCodes($this->archivalAgencyId, 'seda2.1');
        $appraisalRuleCodes = $AppraisalRuleCodes->getRuleCodes($this->archivalAgencyId, 'seda2.1');

        $subquery->select(['ArchiveUnits.id'], true);
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $func = $ArchiveUnits->query()->func();
        $query = $ArchiveUnits->find()
            ->select(
                [
                    'au_count' => $func->count('*'),
                    'documents_count' => $func->sum('original_local_count'),
                    'documents_size' => $func->sum('original_local_size'),
                ]
            )
            ->where(['ArchiveUnits.id IN' => $subquery]);
        $this->set('sums', $query->all()->toArray()[0]);

        // options
        $this->set('codes', $accessRuleCodes);

        $finals = Seda10Schema::getXsdOptions('appraisal_code');
        foreach ($finals as $key => $args) {
            $finals[$key]['value'] = $args['value'] === 'conserver' ? 'keep'
                : 'destroy';
        }
        $this->set('finals', $finals);
        $this->set('durations', $appraisalRuleCodes);
    }

    /**
     * Donne le query avec ses filtres
     * @return Query
     * @throws Exception
     */
    private function queryArchiveUnit()
    {
        $request = $this->getRequest();
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index', ['model' => 'ArchiveUnits']);
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $query = $ArchiveUnits->findByIndexSealed(
            $this->Seal->hierarchicalView(),
            !$request->getQuery('all') && !$request->getQuery('search')
        );
        $query->contain(['ArchiveDescriptions']);
        $ConditionComponent = $IndexComponent->Condition;
        $IndexComponent->setQuery($query)
            ->filter('agreement_id', null, 'Archives.agreement_id')
            ->filter(
                'appraisal_rule_end',
                IndexComponent::FILTER_DATECOMPARENOW,
                'AppraisalRules.end_date'
            )
            ->filter(
                'final_action_code',
                IndexComponent::FILTER_EQUAL,
                'AppraisalRules.final_action_code'
            )
            ->filter('archival_agency_identifier', IndexComponent::FILTER_ILIKE)
            ->filter(
                'created',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.created'
            )
            ->filter(
                'favoris',
                function () {
                    $request = $this->getRequest();
                    $cookies = $request->getCookieParams();
                    $sessionCookies = $this->cookies;
                    if (is_array($sessionCookies)) {
                        $cookies = array_merge($cookies, $sessionCookies);
                    }
                    $cookies = array_filter($cookies, fn ($v) => $v !== '0');
                    $favoritesIds = [];
                    $regex = "/^table-favorite-archive-units-treatments-table-(\d+)$/";
                    foreach (array_keys($cookies) as $cookieName) {
                        if (preg_match($regex, $cookieName, $match)) {
                            $favoritesIds[] = $match[1];
                        }
                    }
                    if (empty($favoritesIds)) {
                        return [];
                    }
                    return ['ArchiveUnits.id IN' => $favoritesIds];
                }
            )
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter(
                'original_total_count',
                IndexComponent::FILTER_COUNTOPERATOR
            )
            ->filter('original_total_size', IndexComponent::FILTER_SIZEOPERATOR)
            ->filter(
                'originating_agency_id',
                null,
                'Archives.originating_agency_id'
            )
            ->filter('profile_id', null, 'Archives.profile_id')
            ->filter(
                'transferring_agency_id',
                null,
                'Archives.transferring_agency_id'
            )
            ->filter(
                'keyword',
                function ($value, Query $q) use ($ConditionComponent) {
                    /** @var KeywordsTable $Keywords */
                    $Keywords = $this->fetchTable('Keywords');
                    $subquery = $Keywords->selectQuery()
                        ->select(['existing' => 'true'])
                        ->from(['k' => 'archive_keywords'])
                        ->innerJoin(
                            ['au' => 'archive_units'],
                            [
                                'au.id' => new IdentifierExpression(
                                    'k.archive_unit_id'
                                ),
                            ]
                        )
                        ->where(
                            $ConditionComponent->ilike('k.content', $value)
                        )
                        ->andWhere(
                            [
                                'au.lft >=' => new IdentifierExpression(
                                    'ArchiveUnits.lft'
                                ),
                                'au.rght <=' => new IdentifierExpression(
                                    'ArchiveUnits.rght'
                                ),
                            ]
                        )
                        ->limit(1);
                    return $q->where([$subquery]);
                }
            );
        return $query;
    }

    /**
     * Formulaire pour effectuer le traitement par lot - étape 2
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function add2(string $id)
    {
        $request = $this->getRequest();
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $batchTreatment = $this->Seal->archivalAgency()
            ->table('BatchTreatments')
            ->find('seal')
            ->where(['BatchTreatments.id' => $id])
            ->contain(
                [
                    'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q->orderBy(
                            ['ArchiveUnits.archival_agency_identifier']
                        )
                            ->limit($AjaxPaginatorComponent->getConfig('limit'))
                            ->contain(
                                [
                                    'Archives' => ['DescriptionXmlArchiveFiles'],
                                    'ArchiveDescriptions',
                                ]
                            );
                    },
                ]
            )
            ->firstOrFail();
        $this->set('batchTreatment', $batchTreatment);
        /** @var ArchiveUnitsBatchTreatmentsTable $ArchiveUnitsBatchTreatments */
        $ArchiveUnitsBatchTreatments = $this->fetchTable('ArchiveUnitsBatchTreatments');
        $unitsCount = $ArchiveUnitsBatchTreatments->find()
            ->where(['batch_treatment_id' => $id])
            ->count();
        $this->set('unitsCount', $unitsCount);

        if ($request->is('delete')) {
            /** @var ArchiveUnitsBatchTreatmentsTable $ArchiveUnitsBatchTreatments */
            $ArchiveUnitsBatchTreatments = $this->fetchTable('ArchiveUnitsBatchTreatments');
            $ArchiveUnitsBatchTreatments->deleteAll(
                ['batch_treatment_id' => $id]
            );
            /** @var BatchTreatmentsTable $BatchTreatments */
            $BatchTreatments = $this->fetchTable('BatchTreatments');
            $BatchTreatments->deleteAll(['id' => $id]);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            return $this->renderDataToJson('done');
        } elseif ($request->is('put')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            $this->Modal->step('addBatchTreatmentStep3(' . $id . ')');
        }

        $this->set('jsTableId', 'add2-batch-treatment');
        $this->set(
            'paginatorId',
            'paginate-add3-batch-treatment-archive-units'
        );
        $AjaxPaginatorComponent->setViewPaginator($batchTreatment->get('archive_units'), $unitsCount);
    }

    /**
     * Formulaire pour effectuer le traitement par lot - étape 3
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function add3(string $id)
    {
        $request = $this->getRequest();
        $batchTreatment = $this->Seal->archivalAgency()
            ->table('BatchTreatments')
            ->find('seal')
            ->where(['BatchTreatments.id' => $id])
            ->firstOrFail();
        $this->set('batchTreatment', $batchTreatment);
        $form = new BatchTreatmentForm();
        $form->setData((array)$batchTreatment->get('data'));
        $this->set('formEntity', $form);

        /** @var BatchTreatmentsTable $BatchTreatments */
        $BatchTreatments = $this->fetchTable('BatchTreatments');
        if ($request->is('delete')) {
            /** @var ArchiveUnitsBatchTreatmentsTable $ArchiveUnitsBatchTreatments */
            $ArchiveUnitsBatchTreatments = $this->fetchTable('ArchiveUnitsBatchTreatments');
            $ArchiveUnitsBatchTreatments->deleteAll(
                ['batch_treatment_id' => $id]
            );
            $BatchTreatments->deleteAll(['id' => $id]);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            return $this->renderDataToJson('done');
        } elseif ($request->is('put')) {
            $BatchTreatments->transition(
                $batchTreatment,
                BatchTreatmentsTable::T_READY
            );
            $BatchTreatments->saveOrFail($batchTreatment);
            $session = $request->getSession();
            $message = '<h4>batch-treatment</h4>'
                . __("La demande de traitement va prochainement être traitée");
            /** @var Notify $Notify */
            $Notify = Utility::get('Notify');
            $Notify->send(
                $this->userId,
                [$session->read('Session.token')],
                $message,
                'alert-warning'
            );
            /** @var Beanstalk $Beanstalk */
            $Beanstalk = Utility::get('Beanstalk');
            $Beanstalk->setTube('batch-treatment');
            $Beanstalk->emit(
                [
                    'user_id' => $this->userId,
                    'batch_treatment_id' => $batchTreatment->id,
                ]
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            return $this->renderDataToJson('done');
        }

        $query = $BatchTreatments->find();
        $query
            ->select(
                [
                    'au_count' => $query->func()->count('*'),
                    'documents_count' => $query->func()->sum(
                        'original_local_count'
                    ),
                    'documents_size' => $query->func()->sum(
                        'original_local_size'
                    ),
                ]
            )
            ->innerJoinWith('ArchiveUnits')
            ->where(['BatchTreatments.id' => $id]);
        $this->set('sums', $query->all()->toArray()[0]);
    }

    /**
     * Pagination des archive units dans la visualisation
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function paginateArchiveUnits(string $id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $query = $this->Seal->archivalAgency()
            ->table('ArchiveUnits')
            ->find()
            ->innerJoinWith('ArchiveUnitsBatchTreatments')
            ->innerJoinWith('Archives')
            ->where(['ArchiveUnitsBatchTreatments.batch_treatment_id' => $id])
            ->contain(
                [
                    'Archives' => ['DescriptionXmlArchiveFiles'],
                    'ArchiveDescriptions',
                ]
            );
        return $AjaxPaginatorComponent->json(
            $query,
            (bool)$this->getRequest()->getQuery('count')
        );
    }

    /**
     * Retire une unité d'archives d'une demande de restitution
     * @param string $id
     * @param string $archive_unit_id
     * @return Response
     */
    public function removeArchiveUnit(string $id, string $archive_unit_id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var ArchiveUnitsBatchTreatmentsTable $ArchiveUnitsBatchTreatments */
        $ArchiveUnitsBatchTreatments = $this->fetchTable('ArchiveUnitsBatchTreatments');
        $link = $this->Seal->archivalAgency()
            ->table('ArchiveUnitsBatchTreatments')
            ->find('seal')
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('BatchTreatments')
            ->where(
                [
                    'ArchiveUnitsBatchTreatments.archive_unit_id' => $archive_unit_id,
                    'ArchiveUnitsBatchTreatments.batch_treatment_id' => $id,
                    'BatchTreatments.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $report = $ArchiveUnitsBatchTreatments->delete($link)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }
}
