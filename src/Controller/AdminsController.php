<?php

/**
 * Asalae\Controller\AdminsController
 */

namespace Asalae\Controller;

use Asalae\Form\ChangeEntityForm;
use Asalae\Form\PromoteSuperArchivistForm;
use Asalae\Model\Entity\User;
use Asalae\Model\Table\AcosTable;
use Asalae\Model\Table\ArchivingSystemsTable;
use Asalae\Model\Table\ArosAcosTable;
use Asalae\Model\Table\ArosTable;
use Asalae\Model\Table\StoredFilesTable;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AdminShellForm;
use AsalaeCore\Http\Response as CoreResponse;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\Ip;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Premis;
use Asalae\Controller\Component\EmailsComponent;
use Asalae\Form\InterruptForm;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Table\BeanstalkJobsTable;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\CronsTable;
use Asalae\Model\Table\EaccpfsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\LdapsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\RolesTable;
use Asalae\Model\Table\SecureDataSpacesTable;
use Asalae\Model\Table\SessionsTable;
use Asalae\Model\Table\TimestampersTable;
use Asalae\Model\Table\TypeEntitiesTable;
use Asalae\Model\Table\UsersTable;
use Asalae\Model\Table\VersionsTable;
use Asalae\Model\Table\VolumesTable;
use Asalae\Utility\Check;
use Authentication\Controller\Component\AuthenticationComponent;
use Beanstalk\Model\Table\BeanstalkWorkersTable;
use Cake\Core\Configure;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\Event\EventInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response as CakeResponse;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use DOMDocument;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use PDOException;
use Symfony\Component\Filesystem\Exception\IOException;
use ZMQSocketException;

/**
 * Administration technique de asalae2
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AcosTable             Acos
 * @property ArchivingSystemsTable ArchivingSystems
 * @property ArosTable             Aros
 * @property BeanstalkJobsTable    BeanstalkJobs
 * @property BeanstalkWorkersTable BeanstalkWorkers
 * @property ConfigurationsTable   Configurations
 * @property CronsTable            Crons
 * @property EaccpfsTable          Eaccpfs
 * @property EmailsComponent       $Emails
 * @property EventLogsTable        EventLogs
 * @property FileuploadsTable      Fileuploads
 * @property LdapsTable            Ldaps
 * @property OrgEntitiesTable      OrgEntities
 * @property ArosAcosTable         Permissions
 * @property RolesTable            Roles
 * @property SecureDataSpacesTable SecureDataSpaces
 * @property SessionsTable         Sessions
 * @property StoredFilesTable      StoredFiles
 * @property Table                 Phinxlog
 * @property TimestampersTable     Timestampers
 * @property TypeEntitiesTable     TypeEntities
 * @property UsersTable            Users
 * @property VersionsTable         Versions
 * @property VolumesTable          Volumes
 */
class AdminsController extends AppController
{
    /**
     * Traits
     */
    use Admins\ArchivalAgencyTrait;
    use Admins\ConfigurationsTrait;
    use Admins\CronsTrait;
    use Admins\LdapsTrait;
    use Admins\TasksTrait;
    use Admins\TimestampersTrait;
    use Admins\VolumesTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_EDIT_EACCPF_SE = 'eaccpfs-se-entity-table';
    public const string TABLE_EDIT_USER_SE = 'user-se-entity-table';
    public const string TABLE_EDIT_EACCPF_SA = 'eaccpfs-sa-entity-table';
    public const string TABLE_EDIT_USER_SA = 'users-sa-entity-table';
    public const string TABLE_SERVICE_ARCHIVE = 'entity-service-archive-table';

    /**
     * @var string clé de cryptage/décryptage (hashé en sha256)
     */
    public const string DECRYPT_KEY = 'timestamper';
    public const string TABLE_INDEX_TIMESTAMPERS = 'index-timestampers-table';

    public const string TABLE_INDEX_VOLUMES = 'index-volumes-table';
    public const string TABLE_INDEX_SPACES = 'index-spaces-table';

    public const string TEST_FILENAME = 'test_file.txt';

    public const string TABLE_INDEX_CRONS = 'index-crons-table';

    public const string TABLE_INDEX_SESSIONS = 'index-sessions-table';

    /**
     * @var null|bool connecté à la bdd (défini par isConnected())
     */
    protected $isConnected = null;

    /**
     * Neutralise le beforeRender pour permettre de se connecter a ce controller
     * sans base de donnée
     *
     * @param EventInterface $event An Event instance
     * @return CoreResponse|null|void
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeRender(EventInterface $event)
    {
        if ($event->getData('do_not_override')) {
            return parent::beforeRender($event);
        }
        $this->set('user_id');
        $this->set('notifications', json_encode([]));
        try {
            return parent::beforeRender($event);
        } catch (Exception) {
            return null;
        }
    }

    /**
     * Contrôle que l'utilisateur est connecté en administrateur technique
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse|null
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        if ($event->getData('do_not_override')) {
            return parent::beforeFilter($event);
        }
        $allowed = ['login', 'logout', 'ajaxLogin', 'probe', 'ping'];
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $AuthenticationComponent->addUnauthenticatedActions($allowed);

        $adminTech = $this->getRequest()->getSession()->read('Admin.tech');

        if (
            Configure::read('Ip.enable_whitelist', false)
            && !Ip::inWhitelist($this->getRequest()->clientIp())
        ) {
            $this->Flash->error(
                __("Vous n'avez pas accès à cette partie de l'application")
            );
            return $this->redirect('/');
        }
        if (
            !$AuthenticationComponent->getResult()
            && !in_array($this->getRequest()->getParam('action'), $allowed)
            && !$this->getRequest()->is('ajax')
        ) {
            return $this->redirect('/admins/login');
        }

        // pour le session timeout -> loginAjax
        $this->set('admin', $adminTech);
    }

    /**
     * Page principale de l'administration technique
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('admin_tech');
        if (empty($this->getAppLocalConfigFileUri())) {
            $this->Flash->error(
                __("Le fichier de configuration local n'a pas été trouvé")
            );
        }
        /** @var Check $Check */
        $Check = Factory\Utility::get(Check::class);
        $installation = !$Check->needInstall();
        $database = $Check->database();
        $beanstalkd = $Check->beanstalkd();
        $ratchet = $Check->ratchet();
        $clamav = $Check->clamav();
        $php = $Check->phpOk();
        $writable = $Check->writableOk();
        $config = !$Check->configurationErrors();
        $cron = $Check->cronOk();
        $system = $installation && $database && $ratchet && $clamav
            && $beanstalkd && $php && $writable && $config && $cron;
        $this->set('system', $system);
        if ($this->isConnected()) {
            try {
                /** @var Table $Phinxlog */
                $Phinxlog = $this->fetchTable('Phinxlog');
                $Phinxlog->find()->first();
                /** @var OrgEntitiesTable $OrgEntities */
                $OrgEntities = $this->fetchTable('OrgEntities');
                /** @var SecureDataSpacesTable $SecureDataSpaces */
                $SecureDataSpaces = $this->fetchTable('SecureDataSpaces');
                $se = $OrgEntities->find()
                    ->contain(['TypeEntities'])
                    ->where(['code' => 'SE'])
                    ->first();
                if (!$se) {
                    $this->Flash->error(
                        __("L'application n'a pas été installée")
                    );
                } elseif (!$SecureDataSpaces->find()->count()) {
                    $this->Flash->error(
                        __("Aucun espace de stockage n'a été configuré")
                    );
                }
            } catch (Exception) {
                $this->Flash->error(
                    __("La base de données n'a pas été initialisée")
                );
            }
        } else {
            $this->Flash->error(__("Base de données down"));
        }
    }

    /**
     * Retourne l'uri du fichier de configuration local
     */
    protected function getAppLocalConfigFileUri()
    {
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        if (is_readable($pathToLocalConfig)) {
            $appLocalConfigFileUri = include $pathToLocalConfig;
        } elseif (is_readable('/data/config/app_local.json')) {
            $appLocalConfigFileUri = '/data/config/app_local.json';
        } elseif (is_readable(CONFIG . 'app_local.json')) {
            $appLocalConfigFileUri = CONFIG . 'app_local.json';
        } else {
            $appLocalConfigFileUri = '';
        }
        return $appLocalConfigFileUri;
    }

    /**
     * Vérifi la connexion à la base de donnée
     *
     * @return bool
     */
    protected function isConnected(): bool
    {
        if ($this->isConnected !== null) {
            return $this->isConnected;
        }
        try {
            $conn = ConnectionManager::get('default');
            $conn->execute('select 1');
            $this->isConnected = true;
        } catch (PDOException | MissingConnectionException) {
            $this->isConnected = false;
        }
        return $this->isConnected;
    }

    /**
     * Donne des infos sur l'état du system
     */
    public function probe()
    {
        $this->viewBuilder()->setLayout('ajax');
        if (!$this->getRequest()->getSession()->read('Admin.tech')) {
            return $this->getResponse()->withStringBody('');
        }

        // memory usage
        exec('cat /proc/meminfo', $output);
        $parsed = [];
        foreach ($output as $str) {
            if (strpos($str, ':')) {
                [$key, $value] = explode(':', $str, 2);
                $parsed[$key] = (int)$value;
            }
        }
        $probe = [
            'memory' => [
                'total' => $parsed['MemTotal'] ?? 0,
                'available' => $parsed['MemAvailable'] ?? 0,
            ],
            'swap' => [
                'total' => $parsed['SwapTotal'] ?? 0,
                'available' => $parsed['SwapFree'] ?? 0,
            ],
            'loadavg' => sys_getloadavg(),
        ];
        $this->set('probe', $probe);
    }

    /**
     * Connection en administrateur technique
     *
     * @return CakeResponse|null
     * @throws Exception
     */
    public function login()
    {
        $this->viewBuilder()->setLayout('login');
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $result = $AuthenticationComponent->getResult();
        $request = $this->getRequest();
        if ($request->is('get') && $result->isValid()) { // déjà connecté
            $redirect = $this->redirect(
                ['controller' => 'Admins', 'action' => 'index']
            );
            return $this->redirect($redirect);
        }
        if ($request->is('post')) {
            $app = Configure::read('App.name');
            $connectionMessage = __(
                "L'utilisateur ''{0}''",
                h($request->getData('username'))
            );
            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            /** @var VersionsTable $Versions */
            $Versions = $this->fetchTable('Versions');
            $version = $Versions->find()
                ->where(['subject' => $app])
                ->orderBy(['id' => 'desc'])
                ->firstOrFail();
            if ($result->isValid()) {
                $user = $result->getData();
                if (is_array($user) && !empty($user['username'])) {
                    $this->logEvent(
                        'admintech_login',
                        'success',
                        __(
                            "Connexion à l'administration technique de {0} à {1}",
                            $connectionMessage,
                            $app
                        )
                    );
                }
                $redirect = $this->redirect(
                    ['controller' => 'Admins', 'action' => 'index']
                );
                return $this->redirect($redirect);
            }
            $this->Flash->error(__("Login ou mot de passe invalide"));
            $entry = $EventLogs->newEntry(
                'admintech_login',
                'fail',
                __(
                    "Echec de connexion à l'administration technique de {0} à {1}",
                    $connectionMessage,
                    $app
                ),
                null,
                $version
            );
            $EventLogs->save($entry);
        }

        $appLocalConfigFileUri = $this->getAppLocalConfigFileUri();
        if (empty($appLocalConfigFileUri)) {
            $this->Flash->error(
                __("Le fichier de configuration local n'a pas été trouvé")
            );
        }
    }

    /**
     * Ajoute un evenement d'administration technique
     * @param string               $type
     * @param string               $outcome
     * @param string               $detail
     * @param EntityInterface|null $object
     */
    protected function logEvent(
        string $type,
        string $outcome,
        string $detail,
        $object = null
    ) {
        if (!$this->isConnected()) {
            return;
        }
        if (empty($object)) {
            /** @var VersionsTable $Versions */
            $Versions = $this->fetchTable('Versions');
            $object = $Versions->find()
                ->where(['subject' => Configure::read('App.name')])
                ->orderBy(['id' => 'desc'])
                ->firstOrFail();
        }
        $identity = $this->getRequest()->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        $username = Hash::get($user, 'username');
        $agent = $this->user ?: new Premis\Agent('admin', $username);

        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        $entry = $EventLogs->newEntry(
            $type,
            $outcome,
            $detail,
            $agent,
            $object
        );
        $EventLogs->saveOrFail($entry);
    }

    /**
     * Permet la connexion par ajax
     */
    public function ajaxLogin()
    {
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $result = $AuthenticationComponent->getResult();
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $user = $Users->newEntity([], ['validate' => false]);
        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        if ($result->isValid()) {
            $this->Modal->success();
        } elseif ($this->getRequest()->is('post')) {
            $this->Modal->fail();
            $user->setErrors(['name' => __("Login ou mot de passe invalide")]);
        }
        $this->set('entity', $user);
        $this->viewBuilder()->setTemplatePath('Users');
    }

    /**
     * Déconnection de l'administration technique
     *
     * @return CakeResponse|null
     * @throws Exception
     */
    public function logout()
    {
        /** @var AuthenticationComponent $AuthenticationComponent */
        $AuthenticationComponent = $this->loadComponent('Authentication.Authentication');
        $result = $AuthenticationComponent->getResult();
        $request = $this->getRequest();
        if ($result->isValid()) {
            $identity = $request->getAttribute('identity');
            $user = $identity ? $identity->getOriginalData() : null;
            $username = Hash::get($user, 'username');
            $app = Configure::read('App.name');
            $msg = __("L'utilisateur ''{0}''", h($username));
            $this->logEvent(
                'admintech_logout',
                'success',
                __(
                    "Déconnexion de l'administration technique de {0} à {1}",
                    $msg,
                    $app
                )
            );
            $AuthenticationComponent->logout();
        }
        $request->getSession()->destroy();
        return $this->redirect(['controller' => 'admins', 'action' => 'login']);
    }

    /**
     * Vérification asynchrone des volumes
     * @throws Exception
     */
    public function ajaxCheckVolumes()
    {
        /** @var Check $Check */
        $Check = Factory\Utility::get(Check::class);
        $this->set('volume_ok', $Check->volumesOK());
    }

    /**
     * Vérification asynchrone des TreeBehavior
     * @throws Exception
     */
    public function ajaxCheckTreeSanity()
    {
        /** @var Check $Check */
        $Check = Factory\Utility::get(Check::class);
        $this->set('tree_ok', $Check->treeSanityOk());
    }

    /**
     * Vérification du système
     */
    public function ajaxCheck()
    {
    }

    /**
     * Visualisation des logs de l'application
     */
    public function ajaxLogs()
    {
        $logs = [];
        $logPath = Configure::read('App.paths.logs', LOGS) . '*.log';
        foreach (glob($logPath) as $filename) {
            $output = null;
            exec('tail -n500 "' . $filename . '"', $output);
            $logs[basename($filename)] = implode("\n", $output);
        }
        $this->set('logs', $logs);
    }

    /**
     * Donne le fichier de log
     * @param string $filename
     * @return CoreResponse
     */
    public function downloadLog(string $filename)
    {
        $logPath = Configure::read('App.paths.logs', LOGS) . $filename;
        return $this->getCoreResponse()
            ->withFileStream($logPath);
    }

    /**
     * Envoi une notification à tous les utilisateurs
     */
    public function notifyAll()
    {
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }

        if ($this->getRequest()->is('post')) {
            $title = '<h4>' . __("Message de l'administrateur") . '</h4>';
            $notificationsSubmited = 0;
            /** @var Notify $Notify */
            $Notify = Factory\Utility::get('Notify');
            $innerBody = $this->getNotifyBody();
            $msg = $title . $innerBody;
            $class = h($this->getRequest()->getData('color'));

            if ($this->getRequest()->getData('activeusers')) {
                /** @var EntityInterface $session */
                $usersTokens = [];
                /** @var SessionsTable $Sessions */
                $Sessions = $this->fetchTable('Sessions');
                $query = $Sessions->find()
                    ->select(['user_id', 'token'])
                    ->where(['token IS NOT' => null]);
                foreach ($query as $session) {
                    $usersTokens[$session->get('user_id')][] = $session->get(
                        'token'
                    );
                }
                foreach ($usersTokens as $user_id => $tokens) {
                    $notificationsSubmited++;
                    $Notify->send(
                        $user_id,
                        $tokens,
                        $msg,
                        $class
                    );
                }
            } else {
                /** @var UsersTable $Users */
                $Users = $this->fetchTable('Users');
                $users = $Users->find()->where(['active' => true])->all();
                /** @var EntityInterface $user */
                foreach ($users as $user) {
                    $notificationsSubmited++;
                    $Notify->send(
                        $user->get('id'),
                        [],
                        $msg,
                        $class
                    );
                }
            }
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();

            $this->logEvent(
                'admintech_notifyall',
                'success',
                __("Envoi de notification à tous les utilisateurs")
            );

            return $this->renderDataToJson(['count' => $notificationsSubmited]);
        }
    }

    /**
     * Récupère le formulaire sous forme utilisable par les notifications
     * @return string
     */
    protected function getNotifyBody(): string
    {
        // filtre les script et corrige les erreurs html
        $dom = new DOMDocument();
        @$dom->loadHTML($this->getRequest()->getData('msg'));
        if ($dom->getElementsByTagName('script')->count() > 0) {
            throw new BadRequestException('script in notification');
        }
        $body = $dom->getElementsByTagName('body')->item(0);
        $innerBody = '';
        foreach ($body->childNodes as $child) {
            $innerBody .= $dom->saveHTML($child);
        }
        return $innerBody;
    }

    /**
     * Etat des disques
     */
    public function ajaxCheckDisk()
    {
        exec('df -h | grep -v tmpfs | grep -v /dev/loop', $disks);
        $this->set('disks', $disks);
    }

    /**
     * Permet de définir l'état de debug par ajax
     * @return CakeResponse
     * @throws Exception
     */
    public function ajaxToggleDebug()
    {
        $appLocalConfigFileUri = $this->getAppLocalConfigFileUri();
        if (!is_writable($appLocalConfigFileUri)) {
            throw new ForbiddenException(
                sprintf("Can't write on %s", $appLocalConfigFileUri)
            );
        }
        if (empty($appLocalConfigFileUri)) {
            $this->Flash->error(
                __("Le fichier de configuration local n'a pas été trouvé")
            );
        }
        $confJson = file_get_contents($appLocalConfigFileUri);
        $conf = json_decode($confJson, true);
        $conf['debug'] = $this->getRequest()->getData('state') === 'true';
        ksort($conf);
        /** @var Filesystem $Filesystem */
        $Filesystem = Factory\Utility::get('Filesystem');
        try {
            $Filesystem->begin('toggle_debug');
            $Filesystem->setSafemode(true);
            $Filesystem->dumpFile(
                $appLocalConfigFileUri,
                json_encode($conf, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
            );
            $Filesystem->commit('toggle_debug');
            $this->logEvent(
                'admintech_toggle_debug',
                'success',
                $conf['debug']
                    ? __("Activation du mode debug")
                    : __("Désactivation du mode debug")
            );
            return $this->renderData(['success' => true]);
        } catch (IOException $e) {
            $Filesystem->rollback('toggle_debug');
            return $this->renderData(
                ['success' => false, 'error' => $e->getMessage()]
            );
        }
    }

    /**
     * Permet de modifier la configuration par editeur de texte
     */
    public function ajaxInterrupt()
    {
        $form = new InterruptForm();
        if ($this->getRequest()->is('post')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $success = $form->execute($this->getRequest()->getData());
            if ($success) {
                $this->Modal->success();
            } else {
                $this->Modal->fail();
            }
            $this->logEvent(
                'admintech_edit_interrupt',
                $success ? 'success' : 'fail',
                __("Modification des règles d'interruption de service")
            );
        }
        $this->set('form', $form);
    }

    /**
     * Edition de l'entité de type SE
     * Permet d'ajouter / retirer des Services d'archives / des utilisateurs
     * @return CakeResponse|null
     * @throws Exception
     */
    public function editServiceExploitation()
    {
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $this->getRequest()->allowMethod('ajax');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        /** @var OrgEntity $entity */
        $entity = $OrgEntities->find()
            ->where(['TypeEntities.code' => 'SE'])
            ->contain(
                [
                    'Eaccpfs' => [
                        'sort' => [
                            'from_date' => 'desc',
                            'to_date' => 'desc',
                            'created' => 'desc',
                        ],
                    ],
                    'Users' => ['ChangeEntityRequests', 'Roles'],
                    'TypeEntities',
                    'Children',
                ]
            )
            ->firstOrFail();
        if ($this->getRequest()->is('put')) {
            $data = [
                'name' => $this->getRequest()->getData('name'),
                'identifier' => $this->getRequest()->getData('identifier'),
            ];
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($OrgEntities, $entity, $data);
            $this->logEvent(
                'admintech_edit_exploitation',
                $this->Modal->lastSaveIsSuccess() ? 'success' : 'fail',
                __("Modification du service d'exploitation"),
                $entity
            );
            return null;
        }

        /** @var User $user */
        foreach ($entity->get('users') as $user) {
            $user->setVirtual(['deletable']);
        }

        $this->set('entity', $entity);
        $countUsers = $this->fetchTable('Users')->find()
            ->where(['org_entity_id' => $entity->id])
            ->count();
        $this->set('countUsers', $countUsers);

        $this->set('tableIdEaccpf', self::TABLE_EDIT_EACCPF_SE);
        $this->set('tableIdUser', self::TABLE_EDIT_USER_SE);
        $AjaxPaginatorComponent->setViewPaginator($entity->get('users'), $countUsers);
    }

    /**
     * Ajout d'un utilisateur super archiviste
     * @param string $se_id
     * @return CakeResponse|void
     * @throws Exception
     */
    public function addSuperArchivist(string $se_id)
    {
        /** @var RolesTable $Roles */
        $Roles = $this->fetchTable('Roles');
        $superArchivistRoleId = $Roles->find()
            ->where(['code' => RolesTable::CODE_SUPER_ARCHIVIST])
            ->firstOrFail()
            ->id;
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        /** @var User $entity */
        $entity = $Users->newEntity(
            [
                'org_entity_id' => $se_id,
                'agent_type' => 'person',
                'role_id' => $superArchivistRoleId,
                'is_validator' => true,
                'use_cert' => false,
            ]
        );
        $request = $this->getRequest();

        if ($request->is('post')) {
            $password = $Users->generatePassword();
            $Users->patchEntity(
                $entity,
                [
                    'username' => $request->getData('username'),
                    'password' => $password,
                    'confirm-password' => $password,
                    'name' => $request->getData('name'),
                    'email' => $request->getData('email'),
                    'high_contrast' => $request->getData('high_contrast'),
                    'active' => $request->getData('active'),
                    'is_validator' => $request->getData('is_validator'),
                    'user_cert' => $request->getData('use_cert'),
                    'archival_agencies' => ['_ids' => $request->getData('archival_agencies._ids')],
                ]
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($Users->save($entity)) {
                $identity = $this->getRequest()->getAttribute('identity');
                $user = $identity ? $identity->getOriginalData() : null;
                $adminname = Hash::get($user, 'username');
                $this->logEvent(
                    'admintech_add_User',
                    'success',
                    __(
                        "Ajouté ({0}) par l'utilisateur ({1})",
                        $entity->get('username'),
                        $adminname
                    )
                );
                $this->Modal->success();
                $entity->setVirtual(['deletable']);
                $json = $entity->toArray();
                $json['change_entity_request'] = null;
                $json['role'] = $Roles->get($json['role_id'])->toArray();
                unset($json['password'], $json['confirm-password']);
                /** @use EmailsComponent */
                $this->loadComponent('Emails');
                $this->Emails->submitEmailNewUser($entity);
                return $this->renderJson(json_encode($json));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('user', $entity);

        // options
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $archivalAgencies = $OrgEntities->find('list')
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SA'])
            ->orderBy(['OrgEntities.name'])
            ->all();
        $this->set('archivalAgencies', $archivalAgencies);
    }

    /**
     * Transforme un utilisateur en super archiviste
     * @param string $se_id
     * @return CoreResponse|CakeResponse|void
     * @throws Exception
     */
    public function promoteSuperArchivist(string $se_id)
    {
        $request = $this->getRequest();
        $form = new PromoteSuperArchivistForm();

        if ($request->is('post')) {
            $data = $request->getData();
            $data['org_entity_id'] = $se_id;
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($data)) {
                $user = $form->getData('user');
                $this->logEvent(
                    'admintech_promote_super_archivist',
                    'success',
                    __(
                        "Promotion d'un utilisateur en super archiviste ({0})",
                        $user->get('username')
                    ),
                    $user
                );
                $this->Modal->success();
                $user->setVirtual(['deletable']);
                $json = $user->toArray();
                $json['change_entity_request'] = null;
                $json['role'] = $this->fetchTable('Roles')
                    ->get($json['role_id'])
                    ->toArray();
                unset($json['password'], $json['confirm-password']);
                /** @use EmailsComponent */
                $this->loadComponent('Emails');
                return $this->renderJson(json_encode($json));
            } else {
                $this->Modal->fail();
                FormatError::logFormErrors($form);
            }
        }
        $this->set('form', $form);

        // options
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $archivalAgencies = $OrgEntities->find('list')
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SA'])
            ->orderBy(['OrgEntities.name'])
            ->all();
        $this->set('archivalAgencies', $archivalAgencies);

        $users = $this->fetchTable('Users')->find(
            'list',
            keyField: 'id',
            valueField: function (EntityInterface $e) {
                return $e->get('username') . ($e->get('name') ? ' - ' . $e->get('name') : '');
            },
            groupField: '_matchingData.ArchivalAgencies.name',
        )
            ->select(['ArchivalAgencies.name'])
            ->innerJoinWith('OrgEntities.ArchivalAgencies')
            ->innerJoinWith('Roles')
            ->where(['OrgEntities.parent_id IS NOT' => null])
            ->andWhere(['Roles.code' => RolesTable::CODE_ARCHIVIST])
            ->orderBy(['ArchivalAgencies.name', 'Users.username'])
            ->enableAutoFields()
            ->all();
        $this->set('users', $users);
    }

    /**
     * Interface pour modifier l'entité d'un utilisateur
     * @param string $user_id
     * @return CakeResponse|void
     * @throws Exception
     */
    public function revokeSuperArchivist(string $user_id)
    {
        $se = $this->fetchTable('OrgEntities')->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_OPERATING_DEPARTMENT])
            ->firstOrFail();
        $user = $this->fetchTable('Users')
            ->find()
            ->where(['Users.id' => $user_id])
            ->contain(
                [
                    'ArchivalAgencies',
                ]
            )
            ->firstOrFail();
        $this->set('user', $user);

        $form = new ChangeEntityForm();
        $form->setData(
            [
                'base_archival_agency_id' => $se->id,
                'user_id' => $user_id,
            ]
        );
        $this->set('form', $form);

        $request = $this->getRequest();
        if ($request->is('post')) {
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($request->getData() + $form->getData())) {
                $user = $this->fetchTable('Users')->find()
                    ->select(
                        [
                            'same_archival_agency' => new QueryExpression('ChangeEntityRequests.id IS NULL'),
                            'super_archivist' => new QueryExpression('SuperArchivistsArchivalAgencies.id IS NOT NULL'),
                        ]
                    )
                    ->enableAutoFields()
                    ->leftJoinWith('SuperArchivistsArchivalAgencies')
                    ->leftJoinWith(
                        'ChangeEntityRequests',
                        function (Query $q) use ($se) {
                            return $q->where(
                                ['ChangeEntityRequests.target_archival_agency_id' => $se->id]
                            );
                        }
                    )
                    ->where(['Users.id' => $user_id])
                    ->contain(
                        [
                            'Ldaps',
                            'OrgEntities',
                            'Roles',
                            'ChangeEntityRequests',
                        ]
                    )
                    ->firstOrFail()
                    ->setVirtual(['deletable', 'is_linked_to_ldap']);
                $this->Modal->success();
                $this->logEvent(
                    'admintech_revoke_super_archivist',
                    'success',
                    __("Révocation d'un super archiviste ({0})", $user->get('username')),
                    $user
                );
                return $this->renderDataToJson($user);
            } else {
                $this->Modal->fail();
                FormatError::logFormErrors($form);
            }
        }

        // options
        $vField = fn(EntityInterface $e) => sprintf('%s - %s', $e->get('identifier'), $e->get('name'));
        $archivalAgencies = $this->fetchTable('OrgEntities')
            ->find('list', valueField: $vField)
            ->innerJoinWith('TypeEntities')
            ->innerJoinWith('SuperArchivists')
            ->where(
                [
                    'TypeEntities.code' => 'SA',
                    'OrgEntities.active' => true,
                    'SuperArchivists.id' => $user_id,
                ]
            )
            ->orderByAsc('OrgEntities.name')
            ->toArray();
        $this->set('archivalAgencies', $archivalAgencies);

        $orgEntities = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('sealList', valueField: $vField)
            ->where(['OrgEntities.id !=' => $user->get('org_entity_id')])
            ->orderByAsc('OrgEntities.name')
            ->toArray();
        $this->set('orgEntities', $orgEntities);

        $roles = $this->fetchTable('Roles')->find('list')
            ->where(
                [
                    'org_entity_id IS' => null,
                    'agent_type' => $user->get('agent_type'),
                ]
            )
            ->orderByAsc('name')
            ->toArray();
        $this->set('roles', $roles);

        $query = $this->fetchTable('OrgEntities')->find()
            ->contain(['TypeEntities' => ['Roles' => ['sort' => 'name']]]);
        $availablesRolesByEntities = [];
        /** @var EntityInterface $entity */
        foreach ($query as $entity) {
            $availablesRolesByEntities[$entity->id] = [];
            /** @var EntityInterface $role */
            foreach (Hash::get($entity, 'type_entity.roles') as $role) {
                $availablesRolesByEntities[$entity->id][] = $role->id;
            }
        }
        $this->set('availablesRolesByEntities', $availablesRolesByEntities);
    }

    /**
     * Annulation de la demande de révocation
     * @param string $user_id
     * @return CoreResponse
     */
    public function deleteRevokation(string $user_id): CoreResponse
    {
        $this->getRequest()->allowMethod('delete');
        $ChangeEntityRequests = $this->fetchTable('ChangeEntityRequests');
        $entity = $ChangeEntityRequests->find()
            ->where(['user_id' => $user_id])
            ->firstOrFail();

        $report = $ChangeEntityRequests->delete($entity)
            ? 'done'
            : "Erreur lors de l'annulation";

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Edition d'un super archiviste
     * @param string $user_id
     * @return CakeResponse|void
     * @throws Exception
     */
    public function editSuperArchivist(string $user_id)
    {
        /** @var RolesTable $Roles */
        $Roles = $this->fetchTable('Roles');
        $superArchivistRoleId = $Roles->find()
            ->where(['code' => RolesTable::CODE_SUPER_ARCHIVIST])
            ->firstOrFail()
            ->id;
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        /** @var User $entity */
        $entity = $Users->find()
            ->innerJoinWith('Roles')
            ->where(
                [
                    'Users.id' => $user_id,
                    'Roles.id' => $superArchivistRoleId,
                ]
            )
            ->contain(
                [
                    'ChangeEntityRequests',
                    'ArchivalAgencies',
                    'ValidationActors',
                    'ValidationActors.ValidationStages',
                    'ValidationActors.ValidationStages.ValidationChains',
                ]
            )
            ->firstOrFail();
        $request = $this->getRequest();

        $validatorInAgencies = Hash::extract(
            $entity,
            'validation_actors.{n}.validation_stage.validation_chain.org_entity_id'
        );
        $runningRevokation = Hash::extract($entity, 'change_entity_request.target_archival_agency_id');
        $lockedAgencies = array_unique(array_merge($validatorInAgencies, $runningRevokation));

        if ($request->is('put')) {
            if (
                $lockedAgencies
                && count(array_diff($lockedAgencies, $request->getData('archival_agencies._ids')))
            ) {
                throw new BadRequestException();
            }

            $username = h($request->getData('username'));
            $name = h($request->getData('name'));
            $email = $request->getData('email');
            if ($entity->get('ldap_id')) {
                $username = $entity->get('username');
                $name = $entity->get('name');
                $email = $entity->get('email');
            }
            $Users->patchEntity(
                $entity,
                [
                    'username' => $username,
                    'name' => $name,
                    'email' => $email,
                    'high_contrast' => $request->getData('high_contrast'),
                    'active' => $request->getData('active'),
                    'is_validator' => $request->getData('is_validator'),
                    'user_cert' => $request->getData('use_cert'),
                    'archival_agencies' => ['_ids' => $request->getData('archival_agencies._ids')],
                ]
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($Users->save($entity)) {
                $identity = $this->getRequest()->getAttribute('identity');
                $user = $identity ? $identity->getOriginalData() : null;
                $adminname = Hash::get($user, 'username');
                $this->logEvent(
                    'admintech_edit_User',
                    'success',
                    __(
                        "Modifié ({0}) par l'utilisateur ({1})",
                        $entity->get('username'),
                        $adminname
                    )
                );
                $this->Modal->success();
                $json = $entity->toArray();
                $json['role'] = $Roles->get($json['role_id'])->toArray();
                unset($json['password'], $json['confirm-password']);
                return $this->renderJson(json_encode($json));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('user', $entity);

        // options
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $archivalAgencies = $OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SA'])
            ->orderBy(['OrgEntities.name'])
            ->all()
            ->map(
                function (EntityInterface $agency) use ($lockedAgencies): array {
                    return [
                        'value' => $agency->get('id'),
                        'text' => $agency->get('name'),
                        'locked' => in_array($agency->get('id'), $lockedAgencies) ? 'locked' : false,
                    ];
                }
            );
        $this->set('archivalAgencies', $archivalAgencies);
    }

    /**
     * Effectue un tail -f sur un fichier log
     * @param string $logfile
     * @param string $timeout
     * @return CoreResponse
     * @throws Exception
     */
    public function tail(string $logfile, $timeout = '10')
    {
        $filename = Configure::read('App.paths.logs', LOGS) . $logfile;
        if (!is_readable($filename)) {
            throw new NotFoundException(
                __("Impossible de lire le fichier {0}", $filename)
            );
        }
        /** @var CoreResponse $response */
        $response = $this->getResponse()->withType('text');

        setlocale(LC_ALL, 'en_US.UTF-8');
        $command = sprintf(
            "timeout %.1F tail -f -n500 %s 2>&1",
            (float)$timeout,
            Exec::escapeshellarg($filename)
        );
        setlocale(LC_ALL, null);
        $handle = popen($command, 'r');
        while (!feof($handle)) {
            $response->withStringBody(fgets($handle))->partialEmit();
        }
        pclose($handle);

        return $response;
    }

    /**
     * Affiche un phpinfo()
     * @return CakeResponse
     */
    public function phpInfo()
    {
        ob_start();
        phpinfo();
        $content = ob_get_contents();
        ob_end_clean();
        return $this->getResponse()->withStringBody($content);
    }

    /**
     * Liste les utilisateurs connectés
     * @return CakeResponse
     */
    public function indexSessions()
    {
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }
        /** @var SessionsTable $Sessions */
        $Sessions = $this->fetchTable('Sessions');

        $this->set('tableIdSessions', self::TABLE_INDEX_SESSIONS);

        $sessions = $Sessions->find()
            ->contain(
                [
                    'Users' => [
                        'OrgEntities' => [
                            'ArchivalAgencies',
                        ],
                    ],
                ]
            )
            ->where(['Sessions.user_id IS NOT' => null])
            ->orderBy(['Users.username' => 'asc', 'Sessions.modified' => 'desc'])
            ->limit(100) // garde fou - envisager une pagination si besoin
            ->all()
            ->toArray();
        $this->set('sessions', $sessions);
    }

    /**
     * Déconnecte un utilisateur
     * @param string $session_id
     * @return CakeResponse
     */
    public function deleteSession(string $session_id)
    {
        $this->getRequest()->allowMethod('delete');
        /** @var SessionsTable $Sessions */
        $Sessions = $this->fetchTable('Sessions');
        $entity = $Sessions->find()
            ->where(['Sessions.id' => $session_id])
            ->contain(['Users'])
            ->firstOrFail();
        $username = Hash::get($entity, 'user.username');

        $report = $Sessions->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        $identity = $this->getRequest()->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        $adminname = Hash::get($user, 'username');
        $this->logEvent(
            'admintech_delete_session',
            $report === 'done' ? 'success' : 'fail',
            __(
                "Déconnection de l'utilisateur ''{0}'' par l'administrateur technique ''{1}''",
                $username,
                $adminname
            )
        );

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Envoi une notification à un utilisateur spécifique
     * @param string $session_id
     * @return CakeResponse
     * @throws ZMQSocketException
     */
    public function notifyUser(string $session_id)
    {
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }
        /** @var SessionsTable $Sessions */
        $Sessions = $this->fetchTable('Sessions');
        /** @var EntityInterface $entity */
        $entity = $Sessions->find()
            ->select(['user_id', 'token', 'Users.username'])
            ->contain(['Users'])
            ->where(['Sessions.id' => $session_id])
            ->firstOrFail();

        if ($this->getRequest()->is('post')) {
            $innerBody = $this->getNotifyBody();
            $title = '<h4>' . __("Message privé de l'administrateur") . '</h4>';
            $msg = $title . $innerBody;
            /** @var Notify $Notify */
            $Notify = Factory\Utility::get('Notify');

            $class = h($this->getRequest()->getData('color'));
            $Notify->send(
                $entity->get('user_id'),
                [$entity->get('token')],
                $msg,
                $class
            );
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();

            $identity = $this->getRequest()->getAttribute('identity');
            $user = $identity ? $identity->getOriginalData() : null;
            $adminname = Hash::get($user, 'username');
            $this->logEvent(
                'admintech_notify_user',
                'success',
                __(
                    "Message pour l'utilisateur ''{0}'' de l'administrateur technique ''{1}''",
                    $entity->get('username'),
                    $adminname
                )
            );

            return $this->renderDataToJson(['success' => true]);
        }
        $this->set('entity', $entity);
    }

    /**
     * Ping
     * @return CakeResponse
     */
    public function ping()
    {
        return $this->getResponse()->withStringBody('pong');
    }

    /**
     * Donne les roles disponibles pour un webservice
     * @param string $org_entity_id
     * @return CakeResponse
     */
    public function getWsRolesOptions(string $org_entity_id)
    {
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $sa = $OrgEntities->getEntitySA($org_entity_id);
        $roles = $OrgEntities->findByRoles($sa)
            ->where(
                [
                    'OrgEntities.id' => $org_entity_id,
                    'Roles.agent_type' => 'software',
                ]
            )
            ->all()
            ->toArray();
        $roles = array_reduce(
            $roles,
            function ($res, OrgEntity $v) {
                $res[$v->get('Roles')['id']] = $v->get('Roles')['name'];
                return $res;
            }
        );
        return $this->renderDataToJson($roles);
    }

    /**
     * Donne la liste des services d'archives
     * @return ResultSetInterface
     * @see TimestampersTrait
     * @see VolumesTrait
     * @see LdapsTrait
     */
    protected function optionsSa(): ResultSetInterface
    {
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        return $OrgEntities->find('list')
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SA'])
            ->orderBy(['OrgEntities.name'])
            ->all();
    }

    /**
     * Edition de l'utilisateur admin (soit-même)
     * @throws Exception
     */
    public function editAdmin()
    {
        $request = $this->getRequest();
        $path = Configure::read(
            'App.paths.administrators_json',
            Configure::read('App.paths.data') . DS . 'administrateurs.json'
        );
        if (!is_writeable(dirname($path))) {
            throw new Exception(
                __(
                    "Le repertoire de configuration {0} n'est pas accessible en écriture",
                    $path
                )
            );
        }
        $admins = json_decode(file_get_contents($path));
        $identity = $this->getRequest()->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        if (Hash::get($user, 'id')) {
            throw new ForbiddenException();
        }
        $username = Hash::get($user, 'username');
        foreach ($admins as $admin) {
            if ($admin->username === $username) {
                break;
            }
        }
        $form = new AdminShellForm();
        $user = new Entity((array)($admin ?? []));
        $this->set('user', $user);
        $data = $user->toArray();
        unset($data['password'], $data['confirm-password']);

        if ($request->is('post')) {
            $data = [
                'action' => 'edit',
                'username' => $username,
            ] + $request->getData();
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($data)) {
                $this->Modal->success();
                return $this->renderDataToJson($data);
            } else {
                $this->Modal->fail();
            }
        }

        $form->setData($data);
        $this->set('form', $form);
    }

    /**
     * Permet de vérifier la connexion par websocket
     * @param string $uuid
     * @return CoreResponse|CakeResponse
     * @throws ZMQSocketException
     */
    public function ajaxCheckWebsocket(string $uuid)
    {
        $Notify = Utility::get('Notify');
        $Notify->emit('ping_' . $uuid, 'ok');
        return $this->renderDataToJson('ok');
    }
}
