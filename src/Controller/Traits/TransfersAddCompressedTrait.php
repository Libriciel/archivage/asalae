<?php

/**
 * Asalae\Controller\Traits\TransfersAddCompressedTrait
 */

namespace Asalae\Controller\Traits;

use Asalae\Controller\AppController;
use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\TransferAttachmentsTable;
use Asalae\Model\Table\TransfersTable;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\Utility\Antivirus\AntivirusInterface;
use AsalaeCore\Utility\Antivirus\Clamav;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\LimitBreak;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use Datacompressor\Utility\DataCompressor;
use DateInterval;
use DateTime;
use Exception;
use FileValidator\Utility\FileValidator;
use Libriciel\Filesystem\Utility\Filesystem;
use Psr\Http\Message\ResponseInterface;

/**
 * ArchiveKeywords
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AppController
 * @mixin RenderDataTrait
 */
trait TransfersAddCompressedTrait
{
    /**
     * Décompresse un fichier zip/tar/gz pour ajouter de nombreux fichiers à un
     * transfert
     * @param string $attachment_id
     * @return ResponseInterface
     * @throws Exception
     */
    private function _addCompressed(string $attachment_id)
    {
        $startTime = microtime(true);

        $this->getRequest()->allowMethod('post');
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        $upload = $TransferAttachments->find()
            ->where(['TransferAttachments.id' => $attachment_id])
            ->contain(['Transfers'])
            ->firstOrFail();
        $transfer = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(['Transfers.id' => $upload->get('transfer_id')])
            ->firstOrFail();

        $targetUri = AbstractGenericMessageForm::getDataDirectory(
            $upload->get('transfer_id')
        )
            . DS . 'tmp' . DS . uniqid();
        $transferSize = filesize(Hash::get($upload, 'transfer.xml'));
        LimitBreak::setTimeLimit(
            ($transferSize * 10 + $upload->get('size')) / 100000
        );// 2.5Go ~= 7h30
        LimitBreak::setMemoryLimit($transferSize * 10); // 65Mo = 650Mo

        if (preg_match('/\.zip$/i', $upload->get('path'))) {
            Notify::emit(
                'addcompressed_' . $attachment_id,
                __("Normalisation du fichier zip...")
            );
            DataCompressor::sanitizeZip($upload->get('path'));
        }

        $estimation = (int)(
            (DataCompressor::fileCount($upload->get('path')) / 30) // 1s pour 30 fichiers
            + ($upload->get('size') / 3000000)
        ); // 1s pour 3Mo
        Notify::emit(
            'addcompressed_' . $attachment_id,
            __(
                "Décompression... (durée estimée: {0})",
                (new DateTime('1970-01-01 00:00:00Z'))
                    ->add(new DateInterval('PT' . $estimation . 'S'))
                    ->format('H:i:s')
            )
        );
        $files = DataCompressor::uncompress($upload->get('path'), $targetUri);
        $len = mb_strlen($targetUri . DS);
        $targetUri = AbstractGenericMessageForm::getDataDirectory(
            $upload->get('transfer_id')
        ) . DS . 'attachments';
        $nfiles = [];
        foreach ($files as $filePath) {
            $npath = $targetUri . DS . mb_substr($filePath, $len);
            $nfiles[] = $npath;
            if (!is_dir(dirname($npath))) {
                mkdir(dirname($npath), 0777, true);
            }
            rename($filePath, $npath);
        }
        $files = $nfiles;
        Filesystem::removeEmptySubFolders(
            AbstractGenericMessageForm::getDataDirectory(
                $upload->get('transfer_id')
            )
        );

        Notify::emit(
            'addcompressed_' . $attachment_id,
            __("Analyse antivirus...")
        );
        try {
            /** @var Clamav|AntivirusInterface $Antivirus */
            $Antivirus = Utility::get(Configure::read('Antivirus.classname'));
            $viruses = $Antivirus->scan($targetUri);
        } catch (Exception $e) {
            $this->log($e->getMessage());
            $viruses = null;
        }

        $exec = Utility::get('Exec')->command('sf -json', $targetUri);
        file_put_contents(TMP . 'last_siegfried_stdout.json', $exec->stdout);

        $jsonDecodeMemoryNeed = strlen($exec->stdout) * 10;
        if ($jsonDecodeMemoryNeed > $transferSize * 10) {
            LimitBreak::setMemoryLimit($jsonDecodeMemoryNeed);
        }
        $timelimitNeed = round(strlen($exec->stdout) / 20000);
        if ($timelimitNeed > ($transferSize * 10 + $upload->get('size')) / 100000) {
            LimitBreak::setTimeLimit($timelimitNeed);
        }

        if ($exec->stderr) {
            file_put_contents(TMP . 'last_siegfried_stderr.log', $exec->stderr);
        } elseif (is_file(TMP . 'last_siegfried_stderr.log')) {
            unlink(TMP . 'last_siegfried_stderr.log');
        }
        $json = json_decode($exec->stdout, true);
        if (!$json) {
            Filesystem::rollback();
            throw new Exception('siegfried failed');
        }
        unlink(TMP . 'last_siegfried_stdout.json');
        if (is_file(TMP . 'last_siegfried_stderr.log')) {
            unlink(TMP . 'last_siegfried_stderr.log');
        }

        $sf = [];
        foreach (Hash::get($json, 'files', []) as $file) {
            $format = Hash::get($file, 'matches.0.id') ?: 'UNKNOWN';
            $sf[$file['filename']] = [
                'format' => $format === 'UNKNOWN' ? '' : $format,
                'mime' => mime_content_type($file['filename']),
                'extension' => pathinfo($file['filename'], PATHINFO_EXTENSION),
            ];
        }

        $len = strlen($targetUri) + 1;
        $algo = Configure::read('hash_algo', 'sha256');
        $success = true;
        $util = DOMUtility::load($transfer->get('xml'));
        $seda21 = $transfer->get('message_version') === 'seda2.1';
        $seda22 = $transfer->get('message_version') === 'seda2.2';

        $count = count($files);
        foreach ($files as $i => $file) {
            $filename = substr($file, $len);
            $nfile = $file;

            // Si le fichier est lié par le noeud Uri, on le remplace par un Attachment (avec le filename original)
            if ($seda21 || $seda22) {
                $q = sprintf(
                    '//ns:Uri[normalize-space(.)=%s]',
                    DOMUtility::normalizeSpace($filename, true)
                );
                $Uri = $util->xpath->query($q)->item(0);
                if (!$Uri) {
                    $q = sprintf(
                        '//ns:Filename[normalize-space(.)=%s]',
                        DOMUtility::normalizeSpace($filename, true)
                    );
                    $Filename = $util->xpath->query($q)->item(0);
                    if ($Filename) {
                        $Uri = $util->xpath->query(
                            'ns:Uri',
                            $Filename->parentNode->parentNode
                        )->item(0);
                    }
                } else {
                    $f = $util->xpath->query(
                        'ns:FileInfo/ns:Filename',
                        $Uri->parentNode
                    )->item(0);
                    if ($f) {
                        $filename = trim($f->nodeValue);
                        $nfile = $targetUri . DS . $filename;
                        rename($file, $nfile);
                    }
                }
                if ($Uri) {
                    $Attachment = $util->dom->createElementNS(
                        $util->namespace,
                        'Attachment'
                    );
                    $Attachment->setAttribute('filename', $filename);
                    $Uri->parentNode->insertBefore($Attachment, $Uri);
                    $Uri->parentNode->removeChild($Uri);
                }
            }

            $exists = $TransferAttachments->find()
                ->where(
                    [
                        'transfer_id' => $upload->get('transfer_id'),
                        'filename' => $filename,
                    ]
                )
                ->count();
            Notify::emit(
                'addcompressed_' . $attachment_id,
                sprintf('%d / %d - %s', $i + 1, $count, h($filename))
            );
            if ($exists) {
                continue;
            }

            if ($viruses === null) {
                $virus = null;
            } elseif (isset($viruses[$file])) {
                $virus = $viruses[$file];
            } else {
                $virus = '';
            }

            $node = ArchiveBinariesTable::findBinaryByFilename($util, $filename);
            $xpath = $node ? $util->getElementXpath($node, '') : null;

            $entity = $TransferAttachments->newEntity(
                [
                    'transfer_id' => $upload->get('transfer_id'),
                    'filename' => $filename,
                    'size' => filesize($nfile),
                    'hash' => hash_file($algo, $nfile),
                    'hash_algo' => $algo,
                    'deletable' => true,
                    'mime' => mime_content_type($nfile),
                    'extension' => $sf[$file]['extension'],
                    'format' => $sf[$file]['format'],
                    'virus_name' => $virus,
                    'xpath' => $xpath,
                    'valid' => Utility::get(FileValidator::class)->isValid($nfile),
                ]
            );
            if (!$TransferAttachments->save($entity)) {
                $success = false;
                break;
            }
        }
        $success = $success && $TransferAttachments->delete($upload);
        if ($success) {
            $util->dom->save($transfer->get('xml'));
        }

        /** @use ModalComponent */
        $this->loadComponent('AsalaeCore.Modal');
        $endTime = microtime(true);
        $diff = $endTime - $startTime;
        if ($diff > 300) { // 5 minutes
            Notify::send(
                $this->userId,
                [],
                __(
                    "La décompression du fichier {0} s'est terminée {1} en {2}",
                    $upload->get('filename'),
                    $success ? __("avec succès") : __("en échec"),
                    sprintf(
                        '%02d:%02d:%02d',
                        ($diff / 3600),
                        ($diff / 60 % 60),
                        $diff % 60
                    )
                )
            );
        }
        if ($success) {
            Filesystem::commit();
            $this->Modal->success();
            /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
            $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
            $query = $TransferAttachments->find()
                ->where(
                    [
                        'TransferAttachments.transfer_id' => $upload->get(
                            'transfer_id'
                        ),
                    ]
                )
                ->orderBy(['TransferAttachments.filename' => 'asc'])
                ->limit($AjaxPaginatorComponent->getConfig('limit'));
            return $AjaxPaginatorComponent->json($query, true);
        } else {
            $this->Modal->fail();
            Filesystem::rollback();
            $report = 'Une erreur a eu lieu';
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Formulaire d'ajout d'un transfert étape 2 (fichiers)
     * @param string $id
     * @param bool   $generateTree
     * @throws Exception
     */
    private function _add2(string $id, $generateTree = true)
    {
        if ($generateTree === 'true') {
            $generateTree = true;
        } elseif ($generateTree === 'false') {
            $generateTree = false;
        }
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var Transfer $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.id' => $id,
                    'Transfers.state' => TransfersTable::S_PREPARATING,
                    'Transfers.archival_agency_id' => $this->archivalAgencyId,
                    'Transfers.created_user_id' => $this->userId,
                ]
            )
            ->contain(
                [
                    'TransferLocks',
                    'TransferAttachments' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q
                            ->orderBy(['TransferAttachments.filename' => 'asc'])
                            ->limit($AjaxPaginatorComponent->getConfig('limit'));
                    },
                ]
            )
            ->firstOrFail();
        /** @var EntityInterface $lock */
        $lock = $entity->get('transfer_lock');
        if (!$lock) {
            /** @var TransfersTable $Transfers */
            $Transfers = $this->fetchTable('Transfers');
            $lock = $Transfers->lock($entity->id, true);
            // force le retour en étape 2 en cas d'annulation
            $lock->set('modified', '2000-01-01');
            $this->fetchTable('TransferLocks')->save($lock);
        }
        if ($this->getRequest()->getQuery('unlock')) {
            $this->fetchTable('TransferLocks')->deleteOrFail($lock);
        }
        $this->set('lock', $lock);

        if ($this->getRequest()->is('put')) {
            $generateTree = $this->getRequest()->getData(
                'generate_tree',
                false
            );
            /** @var TransferAttachmentsTable $TransferAttachments */
            $TransferAttachments = $this->fetchTable('TransferAttachments');
            $query = $TransferAttachments->find();
            $queryAttachments = $query
                ->where(['transfer_id' => $id])
                ->orderBy(['filename' => 'asc']);
            $count = $queryAttachments->count();
            $size = $TransferAttachments->find()
                ->select(
                    ['sum' => $queryAttachments->func()->sum('size')]
                )
                ->where(['transfer_id' => $id])
                ->first()
                ->get('sum');
            $timeout = (int)$size / 10000000 * 60 + $count * 2;
            LimitBreak::setTimeLimit(
                $timeout
            ); // 1 min / 10Mo + 2s par fichiers
            LimitBreak::setMemoryLimit(
                filesize($entity->get('xml')) * 10
            ); // 65Mo = 650Mo
            if ($generateTree) {
                $success = $entity->generateArchiveContain($queryAttachments);
            } else {
                $i = 0;
                $count = $queryAttachments->count();
                $msg = __(
                    "Vérification/ajout des blocs d'intégrités dans le bordereau"
                );
                $begin = microtime(true);
                $success = $entity->fillMissingHashes(
                    $queryAttachments,
                    function (string $filename) use (
                        $id,
                        $msg,
                        &$i,
                        $count,
                        $begin
                    ) {
                        $i++;
                        $remain = $count - $i;
                        $sec = (int)(microtime(true) - $begin);
                        $time = (new DateTime('@' . $sec));
                        $perSec = $i / ($sec ?: 1);
                        $remainTime = (new DateTime(
                            '@' . (int)($remain / $perSec)
                        ));
                        Utility::get('Notify')->emit(
                            'hashcheck_' . $id,
                            sprintf(
                                '%s - %d / %d - %s: %s (est: %s)',
                                $time->format('H:i:s'),
                                $i,
                                $count,
                                $msg,
                                h($filename),
                                $remainTime->format('H:i:s')
                            )
                        );
                    }
                );
            }
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($success) {
                /** @var TransfersTable $Transfers */
                $Transfers = $this->fetchTable('Transfers');
                $Transfers->unlock($entity->id);
                $tempXml = $entity->getTempXml($this->userId);
                if (is_file($tempXml)) {
                    unlink($tempXml);
                }
                $this->Modal->success();
                $this->Modal->step(
                    'actionEditTransfer(\'' . $entity->get('id') . '?add\')'
                );
            } else {
                $this->Modal->fail();
            }
        } else {
            $entity->set('generate_tree', (bool)$generateTree);
        }

        $this->set('entity', $entity);
        $this->set('id', $entity->get('id'));

        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        $countAttachments = $TransferAttachments->find()
            ->where(['TransferAttachments.transfer_id' => $id])
            ->count();
        $this->set('countAttachments', $countAttachments);
        $AjaxPaginatorComponent->setViewPaginator($entity->get('transfer_attachments') ?: [], $countAttachments);
    }
}
