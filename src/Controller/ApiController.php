<?php

/**
 * Asalae\Controller\ApiController
 */

namespace Asalae\Controller;

use Cake\Controller\Controller;
use Cake\Http\Response;

/**
 * Swagger
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ApiController extends Controller
{
    /**
     * Redirige sur swagger
     * @return Response|null
     */
    public function swagger()
    {
        return $this->redirect('/swagger/index.php');
    }
}
