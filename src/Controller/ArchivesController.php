<?php

/**
 * Asalae\Controller\ArchivesController
 */

namespace Asalae\Controller;

use Asalae\Cron\ArchivesIntegrityControl;
use Asalae\Exception\GenericException;
use Asalae\Form\SearchArchiveForm;
use Asalae\Model\Entity\Archive;
use Asalae\Model\Entity\ArchiveBinary;
use Asalae\Model\Entity\ArchiveUnit;
use Asalae\Model\Entity\AttestationPdfArchiveFile;
use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Entity\LifecycleXmlArchiveFile;
use Asalae\Model\Entity\Transfer;
use Asalae\Model\Entity\User;
use Asalae\Model\Table\AccessRuleCodesTable;
use Asalae\Model\Table\AccessRulesTable;
use Asalae\Model\Table\AgreementsTable;
use Asalae\Model\Table\AppraisalRuleCodesTable;
use Asalae\Model\Table\AppraisalRulesTable;
use Asalae\Model\Table\ArchiveBinariesArchiveUnitsTable;
use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchiveDescriptionsTable;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\KeywordsTable;
use Asalae\Model\Table\LifecycleXmlArchiveFilesTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\ProfilesTable;
use Asalae\Model\Table\SavedFiltersTable;
use Asalae\Model\Table\ServiceLevelsTable;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Table\StoredFilesVolumesTable;
use Asalae\Model\Table\TechnicalArchivesTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\UsersTable;
use Asalae\Model\Volume\VolumeManager;
use Asalae\Utility\JstreeSeda;
use Asalae\Utility\SedaToEad;
use Asalae\Worker\ArchiveUnitWorker;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeInterface;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\CallbackStream;
use AsalaeCore\Http\Response as CoreResponse;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\HashUtility;
use AsalaeCore\Utility\LimitBreak;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\PreControlMessages;
use AsalaeCore\Utility\Premis;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\HttpException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\NotImplementedException;
use Cake\Http\Response as CakeResponse;
use Cake\Http\Response;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\Log\Log;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\ResultSet;
use Cake\Utility\Hash;
use Cake\View\ViewBuilder;
use Datacompressor\Utility\DataCompressor;
use DateInterval;
use DateInvalidTimeZoneException;
use DateTime;
use DOMAttr;
use DOMDocument;
use DOMElement;
use DOMException;
use DOMXPath;
use Exception;
use FileConverters\Utility\DetectFileFormat;
use Laminas\Diactoros\UploadedFile;
use Libriciel\Filesystem\Utility\Filesystem;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Seda2Pdf\Seda2Pdf;
use SplFileInfo;
use Throwable;
use ZMQSocketException;

/**
 * Archives
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AccessRuleCodesTable             AccessRuleCodes
 * @property AccessRulesTable                 AccessRules
 * @property AgreementsTable                  Agreements
 * @property AppraisalRuleCodesTable          AppraisalRuleCodes
 * @property AppraisalRulesTable              AppraisalRules
 * @property ArchiveBinariesArchiveUnitsTable ArchiveBinariesArchiveUnits
 * @property ArchiveBinariesTable             ArchiveBinaries
 * @property ArchiveDescriptionsTable         ArchiveDescriptions
 * @property ArchiveFilesTable                ArchiveFiles
 * @property ArchiveUnitsTable                ArchiveUnits
 * @property ArchivesTable                    Archives
 * @property EventLogsTable                   EventLogs
 * @property KeywordsTable                    Keywords
 * @property LifecycleXmlArchiveFilesTable    LifecycleXmlArchiveFiles
 * @property OrgEntitiesTable                 OrgEntities
 * @property ProfilesTable                    Profiles
 * @property ServiceLevelsTable               ServiceLevels
 * @property StoredFilesTable                 StoredFiles
 * @property StoredFilesVolumesTable          StoredFilesVolumes
 * @property TransfersTable                   Transfers
 */
class ArchivesController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'archives-table';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'Archives.id' => 'desc',
        ],
        'sortableFields' => [
            'dua_end' => 'AppraisalRules.end_date',
            'access_end' => 'AccessRules.end_date',
        ],
    ];
    /**
     * @var VolumeInterface[]
     */
    private $volumes = [];
    /**
     * @var string dossier temporaire pour le fichier zip, et ses fichiers décompressés
     */
    private $baseImportDir;
    /**
     * @var DOMUtility
     */
    private $util;
    /**
     * @var array
     */
    private $mapping;

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default',
            'import',
        ];
    }

    /**
     * Called before the controller action. You can use this method to configure and customize components
     * or perform logic that needs to happen before each controller action.
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse|void|null
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        $this->setApiActions(
            [
                'import' => [
                    'get' => [
                        'function' => 'getImport',
                    ],
                    'post' => [
                        'function' => 'postImport',
                    ],
                ],
            ]
        );
        return parent::beforeFilter($event);
    }

    /**
     * Liste des archives non éliminées, non restituées, non transférées
     * accessibles par l'utilisateur connecté (SA, SP, CST)
     */
    public function index()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');

        $filenameIds = [];
        if ($filenames = $this->getRequest()->getQuery('filename')) {
            /** @var ArchiveBinariesTable $ArchiveBinaries */
            $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
            $filenameIds = $ArchiveBinaries->selectQuery()
                ->select(['id' => 'au.archive_id'])
                ->from(['ab' => 'archive_binaries'])
                ->innerJoin(
                    ['lnk' => 'archive_binaries_archive_units'],
                    ['lnk.archive_binary_id' => new IdentifierExpression('ab.id')]
                )
                ->innerJoin(
                    ['au' => 'archive_units'],
                    ['lnk.archive_unit_id' => new IdentifierExpression('au.id')]
                )
                ->limit(50000)
                ->disableHydration();
            foreach ((array)$filenames as $filename) {
                $filenameIds->andWhere(
                    $IndexComponent->Condition->ilike('ab.filename', $filename)
                );
            }
            $filenameIds = $filenameIds
                ->all()
                ->map(
                    function ($e) {
                        return $e['id'];
                    }
                )
                ->toArray();
        }
        $mimeIds = [];
        if ($mimes = $this->getRequest()->getQuery('mime')) {
            /** @var ArchiveBinariesTable $ArchiveBinaries */
            $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
            $mimeIds = $ArchiveBinaries->selectQuery()
                ->select(['id' => 'au.archive_id'])
                ->from(['ab' => 'archive_binaries'])
                ->innerJoin(
                    ['lnk' => 'archive_binaries_archive_units'],
                    ['lnk.archive_binary_id' => new IdentifierExpression('ab.id')]
                )
                ->innerJoin(
                    ['au' => 'archive_units'],
                    ['lnk.archive_unit_id' => new IdentifierExpression('au.id')]
                )
                ->limit(50000)
                ->disableHydration();
            foreach ((array)$mimes as $mime) {
                $mimeIds->andWhere(
                    $IndexComponent->Condition->ilike('ab.mime', $mime)
                );
            }
            $mimeIds = $mimeIds
                ->all()
                ->map(
                    function ($e) {
                        return $e['id'];
                    }
                )
                ->toArray();
        }
        $formatIds = [];
        if ($formats = $this->getRequest()->getQuery('format')) {
            /** @var ArchiveBinariesTable $ArchiveBinaries */
            $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
            $formatIds = $ArchiveBinaries->selectQuery()
                ->select(['id' => 'au.archive_id'])
                ->from(['ab' => 'archive_binaries'])
                ->innerJoin(
                    ['lnk' => 'archive_binaries_archive_units'],
                    ['lnk.archive_binary_id' => new IdentifierExpression('ab.id')]
                )
                ->innerJoin(
                    ['au' => 'archive_units'],
                    ['lnk.archive_unit_id' => new IdentifierExpression('au.id')]
                )
                ->limit(50000)
                ->disableHydration();
            foreach ((array)$formats as $format) {
                $formatIds->andWhere(
                    $IndexComponent->Condition->ilike('ab.format', $format)
                );
            }
            $formatIds = $formatIds
                ->all()
                ->map(
                    function ($e) {
                        return $e['id'];
                    }
                )
                ->toArray();
        }
        $ids = array_slice(
            array_unique(array_merge($filenameIds, $mimeIds, $formatIds)),
            0,
            50000
        );

        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $query = $Archives->findByIndexSealed($this->Seal->hierarchicalView());
        $loaded = false;
        $ConditionComponent = $IndexComponent->Condition;
        $IndexComponent->setQuery($query)
            ->filter(
                'access_end',
                IndexComponent::FILTER_DATEOPERATOR,
                'AccessRules.end_date'
            )
            ->filter(
                'access_rules_end_date_vs_now',
                IndexComponent::FILTER_DATECOMPARENOW,
                'AccessRules.end_date'
            )
            ->filter('agreement_id')
            ->filter(
                'appraisal_rules_end_date_vs_now',
                IndexComponent::FILTER_DATECOMPARENOW,
                'AppraisalRules.end_date'
            )
            ->filter('archival_agency_identifier', IndexComponent::FILTER_ILIKE)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('description', IndexComponent::FILTER_ILIKE)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('original_count', IndexComponent::FILTER_COUNTOPERATOR)
            ->filter(
                'final_action_code',
                null,
                'AppraisalRules.final_action_code'
            )
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('original_size', IndexComponent::FILTER_SIZEOPERATOR)
            ->filter('originating_agency_id')
            ->filter('profile_id')
            ->filter( // ajout des états freezed "intermédiaires"
                'state',
                function ($value, Query $q) {
                    if (is_string($value)) {
                        $value = [$value];
                    }
                    if (in_array(ArchivesTable::S_FREEZED, $value)) {
                        $value = array_merge(
                            $value,
                            [
                                ArchivesTable::S_FREEZED_DESTROYING,
                                ArchivesTable::S_FREEZED_RESTITUTING,
                                ArchivesTable::S_FREEZED_TRANSFERRING,
                            ]
                        );
                    }
                    return $q->andWhere(['Archives.state IN' => $value]);
                }
            )
            ->filter(
                'transferring_agency_identifier',
                IndexComponent::FILTER_ILIKE
            )
            ->filter(
                'originating_agency_identifier',
                IndexComponent::FILTER_ILIKE
            )
            ->filter('oldest_date', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('latest_date', IndexComponent::FILTER_DATEOPERATOR)
            ->filter(
                'transfer_id',
                function (int $value, Query $q) {
                    return $q->innerJoinWith('ArchivesTransfers')
                        ->where(['ArchivesTransfers.transfer_id' => $value]);
                }
            )
            ->filter('transferring_agency_id')
            ->filter('units_count', IndexComponent::FILTER_COUNTOPERATOR)
            ->filter(
                'keyword',
                function ($value, Query $q) use ($ConditionComponent) {
                    /** @var ArchiveUnitsTable $ArchiveUnits */
                    $ArchiveUnits = $this->fetchTable('ArchiveUnits');
                    $subquery = $ArchiveUnits->find()
                        ->select(['ArchiveUnits.archive_id'])
                        ->innerJoinWith('ArchiveKeywords')
                        ->where($ConditionComponent->ilike('ArchiveKeywords.content', $value));
                    return $q->where(['Archives.id IN' => $subquery]);
                }
            )
            ->filter(
                'filename',
                function ($value, Query $q) use ($ids, &$loaded, $ConditionComponent) {
                    $req = $q->where(
                        $loaded ? ['1 = 1'] : $ConditionComponent->in('Archives.id', $ids)
                    );
                    $loaded = true;
                    return $req;
                }
            )
            ->filter(
                'mime',
                function ($value, Query $q) use ($ids, &$loaded, $ConditionComponent) {
                    $req = $q->where(
                        $loaded ? ['1 = 1'] : $ConditionComponent->in('Archives.id', $ids)
                    );
                    $loaded = true;
                    return $req;
                }
            )
            ->filter(
                'format',
                function ($value, Query $q) use ($ids, &$loaded, $ConditionComponent) {
                    $req = $q->where(
                        $loaded ? ['1 = 1'] : $ConditionComponent->in('Archives.id', $ids)
                    );
                    $loaded = true;
                    return $req;
                }
            );

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        if ($ead = $this->exportEad($query)) {
            return $ead;
        }

        $data = $this->paginateToResultSet($query)
            ->map(fn(EntityInterface $archive) => $Archives->addFirstArchiveUnitToArchive($archive))
            ->toArray();
        $this->set('data', $data);

        // Options
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $ta = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES]
            );
        $this->set('transferring_agencies', $ta->all());

        $oa = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES]
            );
        $this->set('originating_agencies', $oa->all());

        /** @var AppraisalRulesTable $AppraisalRules */
        $AppraisalRules = $this->fetchTable('AppraisalRules');
        $this->set(
            'final_action_codes',
            $AppraisalRules->options('final_action_code')
        );

        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('sealList')
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles);
        $this->set('states', $Archives->options('state'));

        $agreements = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('sealList')
            ->where(['Agreements.active' => true])
            ->orderBy(['Agreements.name' => 'asc']);
        $this->set('agreements', $agreements);
        $this->set('titleTable', __("Liste des entrées"));
    }

    /**
     * Bouton Télécharger en EAD 2002 / rendu EAD 2002
     * @param Query $query
     * @return Response|void
     * @throws DOMException
     */
    public function exportEad(Query $query)
    {
        $filename = sprintf('%s_Archives_ead2002.xml', (new DateTime())->format('Y-m-d_His'));

        $request = $this->getRequest();
        if ($request->getQuery('export_ead')) {
            $viewBuilder = new ViewBuilder();
            $viewBuilder->setTemplate('/Archives/xml/ead')
                ->setLayout('ajax');

            $dom = new DOMDocument();
            $dom->formatOutput = true;
            $dom->preserveWhiteSpace = false;
            $dom->loadXML($viewBuilder->build()->render()); // fichier entête avec juste la base du xml

            $ead = new SedaToEad();
            /** @var Archive $archive */
            foreach ($query as $archive) {
                $node = $dom->importNode($ead->getEad($dom, $archive), true);
                $dom->documentElement->appendChild($node);
            }

            return $this->getResponse()
                ->withType('xml')
                ->withHeader(
                    'Content-Disposition',
                    'attachment; filename="' . $filename . '"'
                )
                ->withStringBody($dom->saveXML());
        } else {
            $uri = $request->getUri();
            $queryParams = $uri->getQuery();
            if ($queryParams) {
                $queryParams .= '&';
            }
            $queryParams .= 'export_ead=true';
            $downloadEad = [
                'href' => (string)$uri->withQuery($queryParams),
                'download' => $filename,
            ];
            $this->set('downloadEad', $downloadEad);
        }
    }

    /**
     * Donne un seul element de l'index (mise à jour)
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function indexOne(string $id)
    {
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $query = $Archives->findByIndexSealed($this->Seal->hierarchicalView());
        $archive = $query->where(['Archives.id' => $id])->firstOrFail();
        $Archives->addFirstArchiveUnitToArchive($archive);
        return $this->renderDataToJson($archive->toArray());
    }

    /**
     * Visualisation d'une archive
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $entity = $this->Seal->archivalAgency()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => $id])
            ->contain(
                [
                    'Transfers' => ['TransferringAgencies'],
                    'TransferringAgencies',
                    'OriginatingAgencies',
                    'Agreements',
                    'Profiles',
                    'FirstArchiveUnits',
                    'AccessRules' => ['AccessRuleCodes'],
                    'AppraisalRules' => ['AppraisalRuleCodes'],
                    'SecureDataSpaces',
                    'DescriptionXmlArchiveFiles',
                    'LifecycleXmlArchiveFiles',
                    'TransferXmlArchiveFiles',
                    'AttestationPdfArchiveFiles',
                ]
            )
            ->firstOrFail();

        // TODO remplacer par seal (as2.2)
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $archive_id = $id;
        $query = $Archives->find()
            ->innerJoinWith('ArchivalAgencies')
            ->innerJoinWith('TransferringAgencies')
            ->innerJoinWith('OriginatingAgencies')
            ->where(['Archives.id' => $archive_id]);
        $sa = $this->archivalAgency;
        $lft = Hash::get($this->orgEntity, 'lft');
        $rght = Hash::get($this->orgEntity, 'rght');
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (in_array($typeEntity, ['SA', 'CST'])) {
            $lft = $sa->get('lft');
            $rght = $sa->get('rght');
        }
        if (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->andWhere(
                [
                    'OR' => [
                        [
                            'ArchivalAgencies.lft >=' => $lft,
                            'ArchivalAgencies.rght <=' => $rght,
                        ],
                        [
                            'TransferringAgencies.lft >=' => $lft,
                            'TransferringAgencies.rght <=' => $rght,
                        ],
                        [
                            'OriginatingAgencies.lft >=' => $lft,
                            'OriginatingAgencies.rght <=' => $rght,
                        ],
                    ],
                ]
            );
        } else {
            $query->andWhere(
                [
                    $this->orgEntityId . ' IN' => [
                        new IdentifierExpression('ArchivalAgencies.id'),
                        new IdentifierExpression('TransferringAgencies.id'),
                        new IdentifierExpression('OriginatingAgencies.id'),
                    ],
                ]
            );
        }
        $query->firstOrFail();
        // FIN TODO

        $this->set('id', $id);
        $this->set('entity', $entity);
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $archiveUnit = $entity->get('first_archive_unit');
        if ($archiveUnit) {
            /** @var ArchiveBinariesTable $ArchiveBinaries */
            $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
            /** @var ArchiveUnitsTable $ArchiveUnits */
            $ArchiveUnits = $this->fetchTable('ArchiveUnits');
            $subquery = $ArchiveUnits->find()
                ->select(['ArchiveBinariesArchiveUnits.archive_binary_id'])
                ->innerJoinWith('ArchiveBinariesArchiveUnits')
                ->where(
                    [
                        'ArchiveUnits.lft >=' => $archiveUnit->get('lft'),
                        'ArchiveUnits.rght <=' => $archiveUnit->get('rght'),
                    ]
                );
            $query = $ArchiveBinaries->find()
                ->where(
                    [
                        'ArchiveBinaries.type' => 'original_data',
                        'ArchiveBinaries.id IN' => $subquery,
                    ]
                )
                ->orderBy(['ArchiveBinaries.filename'])
                ->contain(
                    [
                        'ArchiveUnits',
                        'StoredFiles',
                        'OtherDatas',
                    ]
                );
            $this->set('countBinaries', $countBinaries = $query->count());

            LimitBreak::setMemoryLimit(250000000); // 2Go
            $cache = [];
            $binaries = $query
                ->limit($AjaxPaginatorComponent->getConfig('limit'))
                ->all()
                ->map(
                    function (EntityInterface $v) use ($ArchiveUnits, &$cache) {
                        foreach ($v->get('archive_units') as $archiveUnit) {
                            if (isset($cache[$archiveUnit->get('id')])) {
                                $archiveUnit->set('relative_path', $cache[$archiveUnit->get('id')]);
                                continue;
                            }
                            if (isset($cache[$archiveUnit->get('parent_id')])) {
                                $relativePath = $cache[$archiveUnit->get('parent_id')]
                                    . ' /&nbsp;' . h($archiveUnit->get('name'));
                                $cache[$archiveUnit->get('id')] = $relativePath;
                                $archiveUnit->set('relative_path', $cache[$archiveUnit->get('id')]);
                                continue;
                            }

                            $path = Hash::extract(
                                $ArchiveUnits->find()
                                    ->select(['name'])
                                    ->where(
                                        [
                                            'lft <=' => $archiveUnit->get('lft'),
                                            'rght >=' => $archiveUnit->get('rght'),
                                        ]
                                    )
                                    ->orderBy(['lft'])
                                    ->disableHydration()
                                    ->toArray(),
                                '{n}.name'
                            );
                            $path = array_map('h', $path);
                            $name = array_pop($path);
                            $parentPath = '';
                            if ($archiveUnit->get('parent_id')) {
                                $parentPath = '/&nbsp;' . implode(' /&nbsp;', $path);
                                $cache[$archiveUnit->get('parent_id')] = $parentPath;
                            }
                            $relativePath = trim($parentPath . ' /&nbsp;' . $name);
                            $cache[$archiveUnit->get('parent_id')] = $relativePath;
                            $archiveUnit->set('relative_path', $relativePath);
                        }
                        return $v;
                    }
                )
                ->toArray();
            $this->set('binaries', $binaries);
            $AjaxPaginatorComponent->setViewPaginator($binaries, $countBinaries);
        } else {
            $this->set('binaries', []);
            $this->set('countBinaries', 0);
            $AjaxPaginatorComponent->setViewPaginator([], 0);
        }
    }

    /**
     * Explorateur du fichier de description d'une archive
     * @param string $id
     * @throws Exception
     */
    public function description(string $id)
    {
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $entity = $Archives->find()
            ->where(['Archives.id' => $id])
            ->andWhere(
                ['Archives.archival_agency_id' => $this->archivalAgencyId]
            )
            ->contain(['DescriptionXmlArchiveFiles', 'FirstArchiveUnits'])
            ->firstOrFail();

        $this->set('id', $id);
        $this->set('entity', $entity);
        $this->set('archive_unit', $entity->get('first_archive_unit'));
    }

    /**
     * Affiche le bordereau xml en html
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function displayXml(string $id)
    {
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $entity = $Archives->find()
            ->where(
                [
                    'Archives.id' => $id,
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['DescriptionXmlArchiveFiles'])
            ->firstOrFail();

        // TODO remplacer par seal (as2.2)
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $archive_id = $id;
        $query = $Archives->find()
            ->innerJoinWith('ArchivalAgencies')
            ->innerJoinWith('TransferringAgencies')
            ->innerJoinWith('OriginatingAgencies')
            ->where(['Archives.id' => $archive_id]);
        $sa = $this->archivalAgency;
        $lft = Hash::get($this->orgEntity, 'lft');
        $rght = Hash::get($this->orgEntity, 'rght');
        $typeEntity = Hash::get($this->orgEntity, 'type_entity.code');
        if (in_array($typeEntity, ['SA', 'CST'])) {
            $lft = $sa->get('lft');
            $rght = $sa->get('rght');
        }
        if (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->andWhere(
                [
                    'OR' => [
                        [
                            'ArchivalAgencies.lft >=' => $lft,
                            'ArchivalAgencies.rght <=' => $rght,
                        ],
                        [
                            'TransferringAgencies.lft >=' => $lft,
                            'TransferringAgencies.rght <=' => $rght,
                        ],
                        [
                            'OriginatingAgencies.lft >=' => $lft,
                            'OriginatingAgencies.rght <=' => $rght,
                        ],
                    ],
                ]
            );
        } else {
            $query->andWhere(
                [
                    $this->orgEntityId . ' IN' => [
                        new IdentifierExpression('ArchivalAgencies.id'),
                        new IdentifierExpression('TransferringAgencies.id'),
                        new IdentifierExpression('OriginatingAgencies.id'),
                    ],
                ]
            );
        }
        $query->firstOrFail();
        // FIN TODO

        /** @var EntityInterface $description */
        $description = $entity->get('description_xml_archive_file');
        $description->set(
            'archive',
            $entity
        ); // Pour les liens de téléchargements
        return $this->getResponse()->withStringBody(
            Hash::get($entity, 'description_xml_archive_file.HTML')
        );
    }

    /**
     * Donne le data jstree pour la description d'archive
     * @param string $id
     * @return CakeResponse
     * @throws VolumeException
     */
    public function getTree(string $id)
    {
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $entity = $Archives->find()
            ->where(
                [
                    'Archives.id' => $id,
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['DescriptionXmlArchiveFiles'])
            ->firstOrFail();

        $this->set('id', $id);
        $this->set('entity', $entity);

        /** @var DescriptionXmlArchiveFile $desc */
        $desc = $entity->get('description_xml_archive_file');
        if (empty($desc)) {
            return $this->renderDataToJson(
                __("Impossible d'obtenir le fichier de description")
            );
        }
        $dom = $desc->getDom();
        $jstreeSeda = new JstreeSeda($dom, 'Archive');

        $idNode = $this->getRequest()->getQuery('id');
        if ($idNode !== '#' && $idNode) {
            return $this->renderDataToJson($jstreeSeda->getByIdNode($idNode));
        } else {
            return $this->renderDataToJson($jstreeSeda->getJsTree(['Archive']));
        }
    }

    /**
     * Affichage d'un noeud
     * @param string $id
     * @param mixed  ...$path
     * @throws Exception
     */
    public function viewDescriptionNode(string $id, ...$path)
    {
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $entity = $Archives->find()
            ->where(
                [
                    'Archives.id' => $id,
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['DescriptionXmlArchiveFiles'])
            ->firstOrFail();

        /** @var DescriptionXmlArchiveFile $de */
        $de = $entity->get('description_xml_archive_file');
        if (empty($de)) {
            throw new NotFoundException(
                __("Impossible d'obtenir le fichier de description")
            );
        }
        $de->getDom();

        /** @var DOMDocument $dom */
        $dom = $de->get('dom');
        $xpath = new DOMXPath($dom);
        $namespace = $dom->documentElement->getAttributeNode(
            'xmlns'
        )->nodeValue;
        $xpath->registerNamespace('ns', $namespace);
        $xpathExp = '/ns:' . implode('/ns:', $path);
        $node = $xpath->query($xpathExp)->item(0);
        $nodes = [];
        $attrs = [];
        /** @var DOMAttr $attr */
        foreach ($node->attributes as $attr) {
            $attrs[$attr->nodeName] = $attr->nodeValue;
        }
        if ($attrs) {
            $nodes['@'][] = ['attrs' => $attrs, 'value' => null];
        }
        foreach ($node->childNodes as $child) {
            if ($child instanceof DOMElement) {
                foreach ($child->childNodes as $subChild) {
                    if ($subChild instanceof DOMElement) {
                        continue 2;
                    }
                }
                if (!isset($nodes[$child->nodeName])) {
                    $nodes[$child->nodeName] = [];
                }
                $attrs = [];
                foreach ($child->attributes as $attr) {
                    $attrs[$attr->nodeName] = $attr->nodeValue;
                }
                $nodes[$child->nodeName][] = [
                    'attrs' => $attrs,
                    'value' => $child->nodeValue,
                ];
            }
        }
        $this->set('path', $path);
        $this->set('nodes', $nodes);
    }

    /**
     * Pagination ajax de la vue des archive_binaries
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function viewBinaries(string $id)
    {
        $this->paginate['order'] = ['ArchiveBinaries.filename' => 'asc'];
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $query = $ArchiveUnits->findByIndexSealed(
            $this->Seal->hierarchicalView(),
            true
        );
        $archiveUnit = $query
            ->where(['ArchiveUnits.archive_id' => $id])
            ->firstOrFail();

        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $subquery = $ArchiveUnits->find()
            ->select(['ArchiveBinariesArchiveUnits.archive_binary_id'])
            ->innerJoinWith('ArchiveBinariesArchiveUnits')
            ->where(
                [
                    'ArchiveUnits.lft >=' => $archiveUnit->get('lft'),
                    'ArchiveUnits.rght <=' => $archiveUnit->get('rght'),
                ]
            );
        $query = $ArchiveBinaries->find()
            ->where(
                [
                    'ArchiveBinaries.type' => 'original_data',
                    'ArchiveBinaries.id IN' => $subquery,
                ]
            )
            ->orderBy(['ArchiveBinaries.filename'])
            ->contain(
                [
                    'ArchiveUnits',
                    'StoredFiles',
                    'OtherDatas',
                ]
            );

        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        $IndexComponent->setQuery($query)
            ->filter(
                'filename',
                IndexComponent::FILTER_ILIKE,
                'ArchiveBinaries.filename'
            )
            ->filter(
                'format',
                IndexComponent::FILTER_ILIKE,
                'ArchiveBinaries.format'
            )
            ->filter(
                'mime',
                IndexComponent::FILTER_ILIKE,
                'ArchiveBinaries.mime'
            )
            ->filter(
                'size',
                function ($s) {
                    if (!$s['value']) {
                        return;
                    }
                    $value = $s['value'] * $s['mult'];
                    $operator = $s['operator'];
                    if (!in_array($operator, ['=', '<=', '>=', '<', '>'])) {
                        throw new BadRequestException(
                            __("L'opérateur indiqué n'est pas autorisé")
                        );
                    }
                    return ['StoredFiles.size ' . $operator => $value];
                }
            );
        return $AjaxPaginatorComponent->json(
            $query,
            false,
            function (ResultSet $data) use ($ArchiveUnits) {
                return $data->map(
                    function (EntityInterface $v) use ($ArchiveUnits) {
                        foreach ($v->get('archive_units') as $archiveUnit) {
                            $path = Hash::extract(
                                $ArchiveUnits->find()
                                    ->select(['name'])
                                    ->where(
                                        [
                                            'lft <=' => $archiveUnit->get('lft'),
                                            'rght >=' => $archiveUnit->get('rght'),
                                        ]
                                    )
                                    ->orderBy(['lft'])
                                    ->disableHydration()
                                    ->toArray(),
                                '{n}.name'
                            );
                            $path = array_map('h', $path);
                            $archiveUnit->set(
                                'relative_path',
                                '/&nbsp;' . implode(' /&nbsp;', $path)
                            );
                        }
                        return $v;
                    }
                );
            }
        );
    }

    /**
     * Liste les fichiers liés à un archive_binary de type original_data
     * @param string $archive_binary_id
     * @throws Exception
     */
    public function viewBinary(string $archive_binary_id)
    {
        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        $entity = $ArchiveBinaries->find()
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('ArchiveUnits.Archives')
            ->where(
                [
                    'ArchiveBinaries.id' => $archive_binary_id,
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['OtherDatas' => ['StoredFiles']])
            ->firstOrFail();

        $this->set('entity', $entity);
    }

    /**
     * Recherche sur les entrées
     * @throws Exception
     * @deprecated n'est plus utilisé
     */
    public function search()
    {
        /** @use IndexComponent */
        $this->loadComponent('AsalaeCore.Index');

        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $query = $Archives->findByIndexSealed($this->Seal->hierarchicalView());
        $request = $this->getRequest();
        $queryParams = $request->getQueryParams();
        $form = SearchArchiveForm::create(
            $query,
            $queryParams,
            [
                'ArchivalAgency' => $this->archivalAgency,
                'UserOrgEntity' => $this->orgEntity,
                'UserRole' => Hash::get($this->user, 'role'),
            ]
        );
        // recherche lancée
        if ($queryParams) {
            if ($form->execute($form->getData())) {
                $this->set('data', $this->paginateToResultSet($form->query));
                if (!$this->getRequest()->is('ajax')) {
                    $this->viewBuilder()->setTemplate('active_search');
                }
            } else {
                $this->Flash->error(
                    FormatError::logEntityErrors($form->toEntity())
                );
            }
        }
        $this->set('form', $form);

        // options
        /** @var AppraisalRulesTable $AppraisalRules */
        $AppraisalRules = $this->fetchTable('AppraisalRules');
        $this->set(
            'final_action_codes',
            $AppraisalRules->options('final_action_code')
        );

        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        $mimes = $ArchiveBinaries->find(
            'list',
            keyField: 'mime',
            valueField: 'mime',
        )
            ->where(['mime !=' => ''])
            ->distinct(['mime'])
            ->all();
        $this->set('mimes', $mimes);
        $this->set('titleTable', __("Résultats de recherche"));
    }

    /**
     * Equivalent de Filters::getFilters() mais adapté pour plusieurs blocs de filtres
     * @param string $saved_filter_id
     * @throws Exception
     */
    public function getFilters(string $saved_filter_id)
    {
        /** @var SavedFiltersTable $SavedFilters */
        $SavedFilters = $this->fetchTable('SavedFilters');
        $save = $SavedFilters->find()
            ->where(
                [
                    'SavedFilters.id' => $saved_filter_id,
                    'SavedFilters.user_id' => $this->userId,
                ]
            )
            ->contain(['Filters'])
            ->firstOrFail();
        $this->set('savedFilters', $save);
        $this->set(
            'helperConfig',
            json_decode($this->request->getData('helper'), true)
        );

        /** @var AppraisalRulesTable $AppraisalRules */
        $AppraisalRules = $this->fetchTable('AppraisalRules');
        $this->set(
            'final_action_codes',
            $AppraisalRules->options('final_action_code')
        );

        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        $mimes = $ArchiveBinaries->find(
            'list',
            keyField: 'mime',
            valueField: 'mime',
        )
            ->where(['mime !=' => ''])
            ->all();
        $this->set('mimes', $mimes);

        $this->viewBuilder()->disableAutoLayout();
        $this->setResponse($this->getResponse()->withType('json'));
    }

    /**
     * Affichage du cycle de vie de l'archive
     * @param string $id
     * @return CakeResponse|MessageInterface|void
     * @throws Exception
     */
    public function lifecycle(string $id)
    {
        $request = $this->getRequest();

        $query = $this->Seal->hierarchicalView()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => $id]);
        $entity = $query->contain(['LifecycleXmlArchiveFiles'])->firstOrFail();
        $this->set('entity', $entity);
        $this->set('id', $entity->id);

        $page = (int)$this->getRequest()->getQuery('page', 1);
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $limit = $request->getParam(
            '?.limit',
            $AjaxPaginatorComponent->getConfig('limit') ?: 100
        );
        $type = $request->getQuery('type', 'all');
        $types = [
            'archive' => [
                'archive_add',
                'archiveunit_add',
                'download_ArchiveBinary',
                'download_ArchiveFile',
                'download_ArchiveZip',
                'edit_Archive',
                'open_ArchiveBinary',
                'transfer_add',
                'add_delivery',
                'archiveunit_delete',
                'batch_treatment_appraisal',
                'batch_treatment_restriction',
                'batch_treatment_name',
                'batch_treatment_description',
                'batch_treatment_keyword_replace',
                'batch_treatment_keyword_add',
                'batch_treatment_keyword_delete',
                'add_ArchiveKeyword',
                'edit_ArchiveKeyword',
                'delete_ArchiveKeyword',
                'restitution', // remplacé par restitution_build
                'restitution_build',
                'archive_restitute', // remplacé par restitution_destroy
                'restitution_destroy',
                'validate_transfer',
                'invalidate_transfer',
                'validation_stepback_transfer',
                'freeze_archive',
                'unfreeze_archive',
                'convert',
                'outgoing_transfer', // remplacé par outgoing_transfer_transfer
                'outgoing_transfer_transfer',
                'outgoing_transfer_destroy',
            ],
            'storage' => [
                'archivefile_add',
                'archivebinary_add',
                'batch_treatment_convert_do',
                'batch_treatment_convert_delete',
            ],
            'integrity' => [
                'check_archive_integrity',
            ],
        ];
        /** @var LifecycleXmlArchiveFile $lifecycle */
        $lifecycle = Hash::get($entity, 'lifecycle_xml_archive_file');
        /** @var LifecycleXmlArchiveFilesTable $LifecycleXmlArchiveFiles */
        $LifecycleXmlArchiveFiles = $this->fetchTable('LifecycleXmlArchiveFiles');
        $dom = $lifecycle->getDom();
        // Pagination ajax (json)
        if (isset($types[$type])) {
            $data = $LifecycleXmlArchiveFiles->paginate(
                $dom,
                $types[$type],
                $page,
                $limit
            );
            $count = $LifecycleXmlArchiveFiles->countTypes(
                $dom,
                $types[$type]
            );
            $body = $this->getResponse()->getBody();
            $body->write(json_encode($data));
            /** @var Response $response */
            $response = $AjaxPaginatorComponent->getResponseWithCountHeaders(
                $count,
                $page
            );
            return $response
                ->withBody($body)
                ->withType('json');
        }
        // 3 types = 3 onglets
        foreach ($types as $type => $values) {
            $data = $LifecycleXmlArchiveFiles->paginate(
                $dom,
                $values,
                $page,
                $limit
            );
            $count = $LifecycleXmlArchiveFiles->countTypes($dom, $values);
            $this->set('events' . ucfirst($type), $data);
            $this->set('count' . ucfirst($type), $count);
            $AjaxPaginatorComponent->setViewPaginator($data, $count, 'paginated_' . $type);
        }
    }

    /**
     * Création du zip des fichiers de l'archive (si besoin),
     * ou création asynchrone si on juge le temps trop long (avec envoit de mail à l'utilisateur)
     * @param string $id
     * @return CoreResponse|CakeResponse
     * @throws Exception
     */
    public function makeZip(string $id): CakeResponse
    {
        /** @var Archive $archive */
        $archive = $this->Seal->hierarchicalView()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => $id])
            ->contain(
                [
                    'ArchiveFiles' => ['StoredFiles'],
                    'DescriptionXmlArchiveFiles',
                ]
            )
            ->firstOrFail();
        $fileName = $archive->get('zip');
        if ($this->getRequest()->getQuery('renew') && is_file($fileName)) {
            unlink($fileName);
        }
        // le zip existe déjà
        if (is_file($fileName)) {
            return $this->getResponse()
                ->withHeader('X-Asalae-ZipFileDownload', 'download');
        }

        // le temps de création du zip est jugé trop long
        /** @var Exec $exec */
        $exec = Utility::get(Exec::class);
        $timelimit = Configure::read('Downloads.asyncFileCreationTimeLimit');
        if (
            $archive->zipCreationTimeEstimation() > $timelimit
            || $this->getRequest()->getQuery('forceAsync')
        ) {
            $exec->async(
                CAKE_SHELL,
                'zip_files',
                'archive',
                $archive->id,
                $this->userId
            );

            return $this->getResponse()
                ->withHeader('X-Asalae-ZipFileDownload', 'email')
                ->withStringBody(
                    __(
                        "Le fichier est trop volumineux pour être téléchargé 
                        dans l'immédiat, un email vous sera envoyé dès qu'il sera disponible."
                    )
                );
        }

        // creation du fichier
        $result = $exec->command(
            CAKE_SHELL,
            'zip_files',
            'archive',
            $archive->id,
            $this->userId,
            ['--no-mail' => null]
        );

        if (!$result->success) {
            if ($result->stderr) {
                Log::error($result->stderr);
            }
            return $this->getResponse()
                ->withHeader('X-Asalae-ZipFileDownload', 'fail')
                ->withStatus(500)
                ->withStringBody(
                    __(
                        "Une erreur est survenue lors de la création du fichier zip,"
                        . " veuillez ré-essayer ou contacter un administrateur."
                    )
                );
        }

        return $this->getResponse()
            ->withHeader('X-Asalae-ZipFileDownload', 'download');
    }

    /**
     * Téléchargement du zip d'une archive
     * @param string $id
     * @return CoreResponse|CakeResponse
     * @throws VolumeException
     * @throws DOMException
     */
    public function downloadFiles(string $id): CakeResponse
    {
        /** @var Archive $archive */
        $archive = $this->Seal->hierarchicalView()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => $id])
            ->contain(['LifecycleXmlArchiveFiles' => ['StoredFiles']])
            ->firstOrFail();

        // regarder si le zip existe déjà
        $zip = $archive->get('zip');
        if (!is_file($zip)) {
            throw new NotFoundException();
        }

        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        $entry = $EventLogs->newEntry(
            'download_ArchiveZip',
            'success',
            __(
                "Téléchargement des fichiers de l'archive ''{0}'' par l'utilisateur ''{1}''",
                $archive->get('archival_agency_identifier'),
                Hash::get($this->user, 'name')
                    ?: Hash::get($this->user, 'username')
            ),
            $this->userId,
            $archive
        );
        $entry->set('in_lifecycle', false);
        $EventLogs->saveOrFail($entry);

        $storedFile = Hash::get(
            $archive,
            'lifecycle_xml_archive_file.stored_file',
            []
        );
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        if (!$Archives->updateLifecycleXml($storedFile, $entry, true)) {
            throw new Exception(
                __(
                    "Impossible de modifier le cycle de vie, une erreur a eu lieu lors de l'enregistrement"
                )
            );
        }

        return $this->getCoreResponse()
            ->withFileStream($zip, ['mime' => 'application/zip'])
            ->withHeader('X-Asalae-ZipFileDownload', 'download');
    }

    /**
     * permet de télécharger un fichier de pièce jointe par son archive et son nom encodé en base 64
     * @param string $archive_id  identifiant de l'archive
     * @param string $b64Filename nom du fichier encodé en base 64
     * @return CakeResponse
     * @throws VolumeException
     * @throws DOMException|ZMQSocketException
     */
    public function downloadByName(string $archive_id, string $b64Filename)
    {
        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        /** @var Archive $archive */
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $archive = $Archives->findByIndexSealed($this->Seal->hierarchicalView())
            ->where(['Archives.id' => $archive_id])
            ->contain(['LifecycleXmlArchiveFiles' => ['StoredFiles']])
            ->firstOrFail();
        $Archives->addFirstArchiveUnitToArchive($archive);

        /** @var ArchiveBinary $binary */
        $binary = $ArchiveBinaries->find()
            ->where(
                [
                    'archive_id' => $archive_id,
                    'filename' => base64_decode(
                        str_replace(['-', '_'], ['+', '/'], $b64Filename)
                    ),
                ]
            )
            ->innerJoinWith('ArchiveUnits')
            ->contain(['StoredFiles'])
            ->firstOrFail();

        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        $entry = $EventLogs->newEntry(
            'download_ArchiveBinary',
            'success',
            __(
                "Téléchargement du fichier ''{0}'' par l'utilisateur ''{1}''",
                $binary->get('filename'),
                Hash::get($this->user, 'name') ?: Hash::get($this->user, 'username')
            ),
            $this->userId,
            $binary
        );
        $entry->set('in_lifecycle', false);
        $EventLogs->saveOrFail($entry);
        $storedFile = Hash::get(
            $archive,
            'lifecycle_xml_archive_file.stored_file',
            []
        );
        if (!$Archives->updateLifecycleXml($storedFile, $entry, true)) {
            throw new HttpException(__("Echec lors de la mise à jour du cycle de vie"), 500);
        }

        if ($sessionFile = $this->getRequest()->getQuery('sessionFile')) {
            Notify::emit(
                'download_file',
                [
                    'uuid' => $sessionFile,
                    'state' => 'downloading',
                    'last_update' => time(),
                ]
            );
        }

        return $this->getResponse()
            ->withHeader(
                'Content-Type',
                $binary->get('mime') ?: 'application/octet-stream'
            )
            ->withHeader(
                'Content-Length',
                Hash::get($binary, 'stored_file.size')
            )
            ->withHeader(
                'File-Size',
                Hash::get($binary, 'stored_file.size')
            )
            ->withBody(
                new CallbackStream(
                    function () use ($binary, $sessionFile) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        $success = $binary->readfile();
                        if ($sessionFile) {
                            Notify::emit(
                                'download_file',
                                [
                                    'uuid' => $sessionFile,
                                    'state' => 'finished',
                                    'last_update' => time(),
                                    'integrity' => $success,
                                ]
                            );
                        }
                    }
                )
            );
    }

    /**
     * Donne le fichier PDF lié à une archive
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function downloadPdf(string $id): Response
    {
        /** @var Archive $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => $id])
            ->contain(['DescriptionXmlArchiveFiles'])
            ->firstOrFail();
        $pdf = $entity->get('pdf');
        /** @var StoredFilesTable $StoredFiles */
        $StoredFiles = $this->fetchTable('StoredFiles');
        $storedFile = $StoredFiles->find()
            ->where(
                [
                    'name' => $pdf,
                    'secure_data_space_id' => $entity->get(
                        'secure_data_space_id'
                    ),
                ]
            )
            ->first();

        $volumeManager = new VolumeManager(
            $entity->get('secure_data_space_id')
        );
        $request = $this->getRequest();
        if ($request->getQuery('renew') && $storedFile) {
            $volumeManager->fileDelete($storedFile->get('name'));
            $storedFile = null;
        }
        if (!$storedFile) {
            /** @var DescriptionXmlArchiveFile $xml */
            $xml = $entity->get('description_xml_archive_file');
            $tmpname = tempnam(sys_get_temp_dir(), uniqid('download-pdf-'));
            try {
                $generator = new Seda2Pdf($xml->getDom());
                if ($request->getQuery('html')) {
                    return $this->renderPdfAsHtml($generator);
                }
                $generator->generate($tmpname);
                $storedFile = $volumeManager->fileUpload($tmpname, $pdf);
            } finally {
                if (is_file($tmpname)) {
                    unlink($tmpname);
                }
            }
        }

        if ($sessionFile = $this->getRequest()->getQuery('sessionFile')) {
            Notify::emit(
                'download_file',
                [
                    'uuid' => $sessionFile,
                    'state' => 'downloading',
                    'last_update' => time(),
                ]
            );
        }

        return $this->getResponse()
            ->withHeader('Content-Type', 'application/pdf')
            ->withHeader('Content-Length', $storedFile->get('size'))
            ->withHeader('File-Size', $storedFile->get('size'))
            ->withBody(
                new CallbackStream(
                    function () use ($volumeManager, $pdf, $sessionFile) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        $success = $volumeManager->readfile($pdf);
                        if ($sessionFile) {
                            Notify::emit(
                                'download_file',
                                [
                                    'uuid' => $sessionFile,
                                    'state' => 'finished',
                                    'last_update' => time(),
                                    'integrity' => $success,
                                ]
                            );
                        }
                    }
                )
            );
    }

    /**
     * debugging/dev, ajouter ?html=true à /downloadPdf/<id>
     * @param Seda2Pdf $pdf
     * @return CakeResponse
     */
    private function renderPdfAsHtml(Seda2Pdf $pdf): Response
    {
        $html = $pdf->renderHtml();
        $size = strlen($html);
        return $this->getResponse()
            ->withHeader('Content-Type', 'text/html; charset=UTF-8')
            ->withHeader('Content-Length', $size)
            ->withHeader('File-Size', $size)
            ->withBody(
                new CallbackStream(
                    function () use ($html) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        echo $html;
                    }
                )
            );
    }

    /**
     * Commande une génération de fichier PDF
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function makePdf(string $id): Response
    {
        /** @var Archive $entity */
        $entity = $this->Seal->archivalAgency()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => $id])
            ->contain(['DescriptionXmlArchiveFiles' => ['StoredFiles']])
            ->firstOrFail();
        $pdf = $entity->get('pdf');
        /** @var StoredFilesTable $StoredFiles */
        $StoredFiles = $this->fetchTable('StoredFiles');
        $storedFile = $StoredFiles->find()
            ->where(
                [
                    'name' => $pdf,
                    'secure_data_space_id' => $entity->get(
                        'secure_data_space_id'
                    ),
                ]
            )
            ->first();

        if ($storedFile) {
            return $this->getResponse()
                ->withHeader('X-Asalae-PdfFileDownload', 'download');
        }

        /** @var Exec $exec */
        $exec = Utility::get(Exec::class);
        $command = sprintf(
            '%s %s --force',
            CAKE_SHELL,
            'pdf_files'
        );
        $exec->async(
            $command,
            Hash::get($entity, 'description_xml_archive_file.stored_file.name'),
            $entity->get('pdf'),
            $this->userId,
            '/archives/download-pdf/' . $id,
            $entity->get('secure_data_space_id')
        );

        return $this->getResponse()
            ->withHeader('X-Asalae-PdfFileDownload', 'email')
            ->withStringBody(
                __(
                    "Le fichier est trop volumineux pour être téléchargé dans l'immédiat,"
                    . "un email vous sera envoyé dès qu'il sera disponible."
                )
            );
    }

    /**
     * Traitements par lot
     * @throws Exception
     */
    public function treatments()
    {
        $request = $this->getRequest();
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent(
            'AsalaeCore.Index',
            [
                'model' => 'ArchiveUnits',
                'paginate' => [
                    'order' => [
                        'Archives.id' => 'desc',
                    ],
                ]
            ]
        );

        $query = $ArchiveUnits->findByIndexSealed(
            $this->Seal->hierarchicalView(),
            !$request->getQuery('all') && !$request->getQuery('search')
        )
            ->where(['ArchiveUnits.state IS NOT ' => ArchiveUnitsTable::S_FREEZED]);
        $query->contain(['ArchiveDescriptions']);
        $ConditionComponent = $IndexComponent->Condition;
        $IndexComponent->setQuery($query)
            ->filter('agreement_id', null, 'Archives.agreement_id')
            ->filter(
                'appraisal_rule_end',
                IndexComponent::FILTER_DATECOMPARENOW,
                'AppraisalRules.end_date'
            )
            ->filter(
                'final_action_code',
                IndexComponent::FILTER_EQUAL,
                'AppraisalRules.final_action_code'
            )
            ->filter('archival_agency_identifier', IndexComponent::FILTER_ILIKE)
            ->filter(
                'created',
                IndexComponent::FILTER_DATEOPERATOR,
                'Archives.created'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter(
                'original_total_count',
                IndexComponent::FILTER_COUNTOPERATOR
            )
            ->filter('original_total_size', IndexComponent::FILTER_SIZEOPERATOR)
            ->filter(
                'originating_agency_id',
                null,
                'Archives.originating_agency_id'
            )
            ->filter('profile_id', null, 'Archives.profile_id')
            ->filter(
                'transferring_agency_id',
                null,
                'Archives.transferring_agency_id'
            )
            ->filter(
                'keyword',
                function ($value, Query $q) use ($ConditionComponent) {
                    /** @var KeywordsTable $Keywords */
                    $Keywords = $this->fetchTable('Keywords');
                    $subquery = $Keywords->selectQuery()
                        ->select(['existing' => 'true'])
                        ->from(['k' => 'archive_keywords'])
                        ->innerJoin(
                            ['au' => 'archive_units'],
                            [
                                'au.id' => new IdentifierExpression(
                                    'k.archive_unit_id'
                                ),
                            ]
                        )
                        ->where(
                            $ConditionComponent->ilike('k.content', $value)
                        )
                        ->andWhere(
                            [
                                'au.lft >=' => new IdentifierExpression(
                                    'ArchiveUnits.lft'
                                ),
                                'au.rght <=' => new IdentifierExpression(
                                    'ArchiveUnits.rght'
                                ),
                            ]
                        )
                        ->limit(1);
                    return $q->where([$subquery]);
                }
            );

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)
            ->map(
                function (EntityInterface $e) {
                    return $e->setVirtual(['dates']);
                }
            )
            ->toArray();
        $this->set(
            'count',
            $query->where(
                ['ArchiveUnits.state' => ArchiveUnitsTable::S_AVAILABLE]
            )->count()
        );
        $this->set('data', $data);

        // Options
        /** @var AppraisalRulesTable $AppraisalRules */
        $AppraisalRules = $this->fetchTable('AppraisalRules');
        $this->set(
            'final_action_codes',
            $AppraisalRules->options('final_action_code')
        );

        $profiles = $this->Seal->archivalAgency()
            ->table('Profiles')
            ->find('sealList')
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles);

        $agreements = $this->Seal->archivalAgency()
            ->table('Agreements')
            ->find('sealList')
            ->where(['Agreements.active' => true])
            ->orderBy(['Agreements.name' => 'asc']);
        $this->set('agreements', $agreements);

        $ta = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES]
            );
        $this->set('transferring_agencies', $ta->all());

        $oa = $OrgEntities->listOptions([], $this->Seal->hierarchicalView())
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES]
            )
            ->orderBy(['OrgEntities.name' => 'asc']);
        $this->set('originating_agencies', $oa->all());

        $this->set('titleTable', __("Liste des entrées"));
    }

    /**
     * Gèle l'archive
     * @param string $id
     * @return CakeResponse|void
     * @throws DOMException
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    public function freeze(string $id)
    {
        /** @var Archive $archive */
        $archive = $this->Seal->archivalAgency()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => $id])
            ->contain(['LifecycleXmlArchiveFiles' => ['StoredFiles']])
            ->firstOrFail();
        $request = $this->getRequest();
        if ($request->is('post') && $request->getData('reason')) {
            /** @var ArchivesTable $Archives */
            $Archives = $this->fetchTable('Archives');
            $conn = $Archives->getConnection();
            $conn->begin();
            try {
                $Archives->transitionOrFail($archive, ArchivesTable::T_FREEZE);
                $Archives->saveOrFail($archive);
                /** @var ArchiveUnitsTable $ArchiveUnits */
                $ArchiveUnits = $this->fetchTable('ArchiveUnits');
                $ArchiveUnits->transitionAll(
                    ArchiveUnitsTable::T_FREEZE,
                    ['archive_id' => $archive->id]
                );
            } catch (Exception $e) {
                $conn->rollback();
                throw $e;
            }
            $conn->commit();

            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            $entry = $EventLogs->newEntry(
                'freeze_archive',
                'success',
                __(
                    "Gelée par l'utilisateur ({0}) pour le motif : {1}",
                    Hash::get($this->user, 'username'),
                    $request->getData('reason')
                ),
                $this->userId,
                $archive
            );
            $entry->set('in_lifecycle', false);
            $EventLogs->saveOrFail($entry);
            $storedFile = Hash::get(
                $archive,
                'lifecycle_xml_archive_file.stored_file',
                []
            );
            $Archives->updateLifecycleXml($storedFile, $entry, true);

            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();

            $query = $Archives->findByIndexSealed($this->Seal->hierarchicalView())
                ->where(['Archives.id' => $archive->id]);
            $archive = $query->first();
            $Archives->addFirstArchiveUnitToArchive($archive);
            return $this->renderDataToJson($archive->toArray());
        }
    }

    /**
     * Dégèle l'archive
     * @param string $id
     * @return CakeResponse|void
     * @throws DOMException
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    public function unfreeze(string $id)
    {
        /** @var Archive $archive */
        $archive = $this->Seal->archivalAgency()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => $id])
            ->contain(['LifecycleXmlArchiveFiles' => ['StoredFiles']])
            ->firstOrFail();
        $request = $this->getRequest();
        if ($request->is('post') && $request->getData('reason')) {
            /** @var ArchivesTable $Archives */
            $Archives = $this->fetchTable('Archives');
            $conn = $Archives->getConnection();
            $conn->begin();
            try {
                $Archives->transitionOrFail($archive, ArchivesTable::T_UNFREEZE);
                $Archives->saveOrFail($archive);
                /** @var ArchiveUnitsTable $ArchiveUnits */
                $ArchiveUnits = $this->fetchTable('ArchiveUnits');
                $ArchiveUnits->transitionAll(
                    ArchiveUnitsTable::T_UNFREEZE,
                    ['archive_id' => $archive->id]
                );
            } catch (Exception $e) {
                $conn->rollback();
                throw $e;
            }
            $conn->commit();

            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            $entry = $EventLogs->newEntry(
                'unfreeze_archive',
                'success',
                __(
                    "Dégelée par l'utilisateur ({0}) pour le motif : {1}",
                    Hash::get($this->user, 'username'),
                    $request->getData('reason')
                ),
                $this->userId,
                $archive
            );
            $entry->set('in_lifecycle', false);
            $EventLogs->saveOrFail($entry);
            $storedFile = Hash::get(
                $archive,
                'lifecycle_xml_archive_file.stored_file',
                []
            );
            $Archives->updateLifecycleXml($storedFile, $entry, true);

            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();

            $query = $Archives->findByIndexSealed($this->Seal->hierarchicalView())
                ->where(['Archives.id' => $archive->id]);
            $archive = $query->firstOrFail();
            $Archives->addFirstArchiveUnitToArchive($archive);
            return $this->renderDataToJson($archive->toArray());
        }
    }

    /**
     * Attestation d'intégrité et d'authenticité des archives
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function integrityCertificate(string $id)
    {
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $archive = $Archives->find()
            ->where(
                [
                    'Archives.id' => $id,
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        /** @var null|CakeDateTime $integrityDate */
        $integrityDate = $archive->get('integrity_date');
        if (
            !$integrityDate
            || $integrityDate->add(new DateInterval('P1D')) < new CakeDateTime()
            || $this->getRequest()->getQuery('renew')
        ) {
            $this->checkIntegrity($id);
            $archive = $Archives->get($id);
        }
        if (!$archive->get('is_integrity_ok')) {
            throw new GenericException(__("Archive non intègre"), 400);
        }

        try {
            $htmlFile = tempnam(sys_get_temp_dir(), uniqid('seda2pdf-'));
            file_put_contents($htmlFile, $this->renderIntegrityCertificateHtml($id));
            $pdfFile = tempnam(sys_get_temp_dir(), uniqid('seda2pdf-'));
            exec(
                sprintf(
                    'cat %s | xvfb-run -a --server-args="-screen 0, 1024x768x24" wkhtmltopdf - %s 2>&1',
                    escapeshellarg($htmlFile),
                    escapeshellarg($pdfFile)
                ),
                $out,
                $code
            );
            if ($code !== 0) {
                throw new GenericException(var_export($out, true));
            }
            unlink($htmlFile);
            $pdf = file_get_contents($pdfFile);
        } finally {
            if (is_file($htmlFile)) {
                unlink($htmlFile);
            }
            if (isset($pdfFile) && is_file($pdfFile)) {
                unlink($pdfFile);
            }
        }

        return $this->getResponse()
            ->withHeader('Content-Type', 'application/pdf')
            ->withHeader('Content-Length', $size = strlen($pdf))
            ->withHeader('File-Size', $size)
            ->withStringBody($pdf);
    }

    /**
     * Lance la vérification de l'integrité d'une archive
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function checkIntegrity(string $id)
    {
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $Archives->findByIndexSealed($this->Seal->hierarchicalView())
            ->where(['Archives.id' => $id])
            ->firstOrFail();
        $size = $Archives->get($id)->get('transfered_size');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $count = $ArchiveUnits->find()
            ->innerJoinWith('ArchiveBinaries')
            ->where(['archive_id' => $id])
            ->count();
        LimitBreak::setTimeLimit(
            (int)$size / 10000000 * 60 + $count * 2
        ); // 1 min / 10Mo
        if ($this->checkArchiveIntegrity($id)) {
            return $this->getResponse()
                ->withHeader('X-Asalae-Success', 'true')
                ->withStringBody(
                    __(
                        "Tous les fichiers sont intègres (vérification effectuée le {0})",
                        (new CakeDateTime())->nice()
                    )
                );
        } else {
            return $this->getResponse()
                ->withHeader('X-Asalae-Success', 'false')
                ->withStringBody(
                    __(
                        "Des fichiers non intègres ont été détectés (vérification effectuée le {0})",
                        (new CakeDateTime())->nice()
                    )
                );
        }
    }

    /**
     * Récupération de la logique dans le cron ArchivesIntegrityControl
     * @param integer $id
     * @return bool|null
     * @throws VolumeException
     */
    private function checkArchiveIntegrity(int $id): ?bool
    {
        $model = 'Archives';
        /** @var ArchivesTable|TechnicalArchivesTable $Archives */
        $Archives = $this->fetchTable($model);
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        $StoredFilesVolumes = $this->fetchTable('StoredFilesVolumes');

        $query = $Archives->findByIntegrity()
            ->where([$model . '.id' => $id]);

        $archiveIntegrity = true;
        $conn = $Archives->getConnection();
        $conn->begin();

        /** @var EntityInterface $fetch */
        foreach ($query as $fetch) {
            $storedFile = new Entity(
                [
                    'id' => $fetch->get('stored_file_id'),
                    'name' => $fetch->get('stored_file_name'),
                ]
            );
            $fetch->set(
                'lifecycle_xml_archive_file',
                ['stored_file' => $storedFile]
            );
            $archive = $fetch->toArray();

            // si le volume ne fonctionne pas, on passe
            if (!$this->getVolume($archive['volume_id'])) {
                $conn->rollback();
                return null;
            }

            // on récupère le fichier
            $archiveBinaryIntegrity = $this->checkCurrentFile($archive);
            $archiveIntegrity = $archiveIntegrity && $archiveBinaryIntegrity;
            $ArchiveBinaries->updateAll(
                ['is_integrity_ok' => $archiveBinaryIntegrity],
                ['id' => $archive['archive_binary_id']]
            );
            $StoredFilesVolumes->updateAll(
                [
                    'is_integrity_ok' => $archiveBinaryIntegrity,
                    'integrity_date' => new DateTime(),
                ],
                [
                    'storage_ref' => $archive['storage_ref'],
                    'volume_id' => $archive['volume_id'],
                ]
            );
        }
        $ArchiveFiles = $this->fetchTable('ArchiveFiles');
        $query = $Archives->findByArchiveFilesIntegrity()
            ->where([$model . '.id' => $id]);
        /** @var EntityInterface $fetch */
        foreach ($query as $fetch) {
            $storedFile = new Entity(
                [
                    'id' => $fetch->get('stored_file_id'),
                    'name' => $fetch->get('stored_file_name'),
                ]
            );
            $fetch->set(
                'lifecycle_xml_archive_file',
                ['stored_file' => $storedFile]
            );
            $archive = $fetch->toArray();

            // on récupère le fichier
            $archiveFileIntegrity = true;
            try {
                $hash = $this->volumes[$archive['volume_id']]
                    ->hash($archive['storage_ref'], $archive['hash_algo']);
                if (!HashUtility::hashMatch($hash, $archive['hash'])) {
                    Log::error(
                        __(
                            "Le fichier {0} du volume {1} n'a pas le bon hash (volume: {2} / bdd: {3})",
                            $archive['storage_ref'],
                            $archive['volume_id'],
                            $hash,
                            $archive['hash']
                        )
                    );
                    $archiveFileIntegrity = false;
                    $archiveIntegrity = false;
                }
            } catch (VolumeException $e) {
                if ($e->volumeErrorCode === VolumeException::FILE_NOT_FOUND) {
                    // interruption du volume, on passe l'archive sans la marquer non intègre
                    if (!$this->volumes[$archive['volume_id']]->ping()) {
                        $conn->rollback();
                        $conn->begin();
                        $this->skipArchives[$archive['id']] = true;
                        $this->skipVolumes[$archive['volume_id']] = true;
                        return true;
                    }
                    Log::error(
                        __(
                            "Fichier {0} non trouvé sur le volume {1}",
                            $archive['storage_ref'],
                            $archive['volume_id']
                        )
                    );
                    $archiveFileIntegrity = false;
                    $archiveIntegrity = false;
                } else {
                    $conn->rollback();
                    throw $e;
                }
            }
            $ArchiveFiles->updateAll(
                ['is_integrity_ok' => $archiveFileIntegrity],
                ['id' => $archive['archive_file_id']]
            );
            $StoredFilesVolumes->updateAll(
                [
                    'is_integrity_ok' => $archiveFileIntegrity,
                    'integrity_date' => new DateTime(),
                ],
                [
                    'storage_ref' => $archive['storage_ref'],
                    'volume_id' => $archive['volume_id'],
                ]
            );
        }
        $Archives->updateAll(
            [
                'is_integrity_ok' => $archiveIntegrity,
                'integrity_date' => new DateTime(),
            ],
            ['id' => $id]
        );
        $conn->commit();
        ArchivesIntegrityControl::createLifecycleEvent(
            $archiveIntegrity,
            $Archives->find()
                ->where(['Archives.id' => $id])
                ->contain(['LifecycleXmlArchiveFiles' => ['StoredFiles']])
                ->firstOrFail(),
            $this->user
        );
        return $archiveIntegrity;
    }

    /**
     * Donne le volume par son id
     * @param int $volume_id
     * @return VolumeInterface|false
     */
    private function getVolume(int $volume_id)
    {
        if (!isset($this->volumes[$volume_id])) {
            try {
                $this->volumes[$volume_id] = VolumeManager::getDriverById(
                    $volume_id
                );
            } catch (VolumeException) {
                $this->volumes[$volume_id] = false;
            }
        }
        return $this->volumes[$volume_id];
    }

    /**
     * Vérifi l'intégrité d'un fichier
     * @param array $fetch
     * @return bool
     * @throws VolumeException
     */
    private function checkCurrentFile(array $fetch): bool
    {
        $isIntegrityOk = true;
        try {
            $hash = $this->volumes[$fetch['volume_id']]
                ->hash($fetch['storage_ref'], $fetch['hash_algo']);
            if (!HashUtility::hashMatch($hash, $fetch['hash'])) {
                Log::error(
                    __(
                        "Le fichier {0} du volume {1} n'a pas le bon hash (volume: {2} / bdd: {3})",
                        $fetch['storage_ref'],
                        $fetch['volume_id'],
                        $hash,
                        $fetch['hash']
                    )
                );
                $isIntegrityOk = false;
            }
        } catch (VolumeException $e) {
            if ($e->volumeErrorCode === VolumeException::FILE_NOT_FOUND) {
                // interruption du volume, on passe l'archive sans la marquer non intègre
                if ($this->volumes[$fetch['volume_id']]->ping() === false) {
                    $this->conn->rollback();
                    $this->conn->begin();
                    $this->skipArchives[$fetch['id']] = true;
                    $this->skipVolumes[$fetch['volume_id']] = true;
                    return true;
                }
                Log::error(
                    __(
                        "Fichier {0} non trouvé sur le volume {1}",
                        $fetch['storage_ref'],
                        $fetch['volume_id']
                    )
                );
                $isIntegrityOk = false;
            } else {
                $this->conn->rollback();
                throw $e;
            }
        }
        return $isIntegrityOk;
    }

    /**
     * Donne le HTML qui servira pour la transformation en PDF
     * @param int|string $id
     * @return string
     */
    private function renderIntegrityCertificateHtml($id): string
    {
        $viewBuilder = $this->viewBuilder()
            ->disableAutoLayout()
            ->addHelper('Html2Pdf')
            ->setTemplatePath('Archives')
            ->setTemplate('integrity_certificate')
            ->setVar('archival_agency', $this->archivalAgency);

        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        /** @var EntityInterface $entity */
        $entity = $Archives->find()
            ->where(
                [
                    'Archives.id' => $id,
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'OriginatingAgencies',
                    'Profiles',
                    'TransferringAgencies',
                    'Transfers',
                ]
            )
            ->firstOrFail();
        $viewBuilder->setVar('archive', $entity);
        $viewBuilder->setVar('transferring_agency', $entity->get('transferring_agency'));

        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        $transferReceipt = $EventLogs->find()
            ->where(
                [
                    'type' => 'transfer_add',
                    'object_model' => 'Transfers',
                    'object_foreign_key' => Hash::get($entity, 'transfers.0.id'),
                ]
            )
            ->firstOrFail();
        $viewBuilder->setVar('transferReceipt', $transferReceipt);
        $archiveCreate = $EventLogs->find()
            ->where(
                [
                    'type' => 'archive_add',
                    'object_model' => 'Archives',
                    'object_foreign_key' => $id,
                ]
            )
            ->firstOrFail();
        $viewBuilder->setVar('archiveCreate', $archiveCreate);
        $query = $EventLogs->find()
            ->where(
                [
                    'type' => 'check_archive_integrity',
                    'object_model' => 'Archives',
                    'object_foreign_key' => $id,
                ]
            )
            ->orderByAsc('id');
        $firstIntegrity = null;
        $integrities = [];
        foreach ($query as $integrityEvent) {
            if (!$firstIntegrity) {
                $firstIntegrity = $integrityEvent;
                $integrities = [$firstIntegrity, $firstIntegrity];
            }
            $integrities[] = $integrityEvent;
            if (count($integrities) === 3) {
                array_shift($integrities);
            }
        }
        $viewBuilder->setVar('firstIntegrity', $firstIntegrity);
        $viewBuilder->setVar('integrities', $integrities);

        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        $binaries = $ArchiveBinaries->find()
            ->innerJoinWith('ArchiveUnits')
            ->where(
                [
                    'ArchiveUnits.archive_id' => $id,
                    'ArchiveBinaries.type' => 'original_data',
                ]
            )
            ->contain(['StoredFiles'])
            ->orderByAsc('ArchiveBinaries.filename');
        $viewBuilder->setVar('binaries', $binaries);

        return $viewBuilder->build()->render();
    }

    /**
     * Attestation d'archivage
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function archivingCertificate(string $id)
    {
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $archive = $Archives->find()
            ->where(
                [
                    'Archives.id' => $id,
                    'Archives.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['AttestationPdfArchiveFiles' => ['StoredFiles']])
            ->firstOrFail();
        $filename = sprintf('%s_archiving_certificate.pdf', $archive->get('archival_agency_identifier'));
        /** @var AttestationPdfArchiveFile $archiveFile */
        if ($archiveFile = $archive->get('attestation_pdf_archive_file')) {
            $size = Hash::get($archiveFile, 'stored_file.size');
            return $this->getResponse()
                ->withHeader('Content-Type', 'application/pdf')
                ->withHeader('Content-Length', $size)
                ->withHeader('File-Size', $size)
                ->withHeader(
                    'Content-Disposition',
                    'attachment; filename="' . $filename . '"'
                )
                ->withBody(
                    new CallbackStream(
                        function () use ($archiveFile) {
                            ob_end_flush();
                            ob_implicit_flush();
                            set_time_limit(0);
                            $archiveFile->readfile();
                        }
                    )
                );
        }

        $pdf = $Archives->renderArchivingCertificatePdf($id);

        return $this->getResponse()
            ->withHeader('Content-Type', 'application/pdf')
            ->withHeader('Content-Length', $size = strlen($pdf))
            ->withHeader('File-Size', $size)
            ->withHeader(
                'Content-Disposition',
                'attachment; filename="' . $filename . '"'
            )
            ->withStringBody($pdf);
    }

    /**
     * Importe une archive via webservice
     * @throws Exception|Throwable
     */
    protected function postImport()
    {
        $this->baseImportDir = rtrim(
            Configure::read('Archives.import.tmp_dir', sys_get_temp_dir()),
            DS
        ) . DS . 'import-archive';
        if (!is_dir($this->baseImportDir)) {
            Filesystem::mkdir($this->baseImportDir);
        }
        $this->viewBuilder()->setLayout('ajax');
        $dry = filter_var(
            $this->getRequest()->getData('dryrun'),
            FILTER_VALIDATE_BOOLEAN,
            FILTER_NULL_ON_FAILURE
        );
        $this->set('dry', $dry);
        $request = $this->getRequest();
        $code = $this->checkImport($request->getData());
        if ($code >= 400 || $dry) {
            $this->setResponse(
                $this->getResponse()->withStatus($code)
            );
            return;
        }
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');

        $conn = $Archives->getConnection();
        $conn->begin();
        try {
            $zip = $this->getImportZip();
            $dir = $this->extractImportedZip($zip);
            $transfer = $this->importTransfer($dir);
            $archive = $this->importArchive($transfer, $dir);
            $this->importArchiveUnits($archive);
            $this->importArchiveBinaries($archive, $dir);
            $storedFileLifecycle = $this->importArchiveFiles($archive, $dir);
            $ArchiveUnits->updateCountSize(
                $ArchiveUnits->find()
                    ->where(['ArchiveUnits.archive_id' => $archive->id])
            );
            $Archives->updateCountSize([$archive]);
            $conn->commit();

            if ($storedFileLifecycle) {
                /** @var EventLogsTable $EventLogs */
                $EventLogs = $this->fetchTable('EventLogs');
                $entry = $EventLogs->newEntry(
                    'import_archive_with_lifecycle',
                    'success',
                    __(
                        "Import de l'archive ''{0}'' avec son lifecycle",
                        $archive->get('archival_agency_identifier')
                    ),
                    $this->userId,
                    $archive
                );
                $EventLogs->saveOrFail($entry);
                $Archives->updateLifecycleXml($storedFileLifecycle, $entry);
            }
        } catch (BadRequestException | NotImplementedException $e) {
            $errors[] = $e->getMessage();
            $this->setResponse(
                $this->getResponse()->withStatus($e->getCode() ?: 500)
            );
            $this->set('errors', $errors);
            if (Configure::read('debug')) {
                $this->set('file', $e->getFile());
                $this->set('line', $e->getLine());
                $this->set('trace', $e->getTraceAsString());
            }
            $conn->rollback();
            return;
        } catch (Throwable $e) {
            $conn->rollback();
            throw $e;
        }
    }

    /**
     * Vérifi la possibilité d'importer une archive
     * @param array $data
     * @return int
     * @throws Exception
     */
    private function checkImport(array $data): int
    {
        /** @var AgreementsTable $Agreements */
        $Agreements = $this->fetchTable('Agreements');
        /** @var ProfilesTable $Profiles */
        $Profiles = $this->fetchTable('Profiles');
        /** @var ServiceLevelsTable $ServiceLevels */
        $ServiceLevels = $this->fetchTable('ServiceLevels');
        $errors = [];
        $code = 200;

        if ($identifier = $data['archiveIdentifier'] ?? null) {
            /** @var ArchivesTable $Archives */
            $Archives = $this->fetchTable('Archives');
            $exists = $Archives->exists(
                [
                    'archival_agency_id' => $this->archivalAgencyId,
                    'archival_agency_identifier' => $identifier,
                ]
            );
            if ($exists) {
                $errors[] = __(
                    "L'identifiant d'archives ''{0}'' existe déjà en base de données",
                    $identifier
                );
                $code = 400;
            }
        }
        if ($transferring = $data['transferringAgencyIdentifier'] ?? null) {
            $t = $this->Seal->archivalAgency()
                ->table('OrgEntities')
                ->find('seal')
                ->where(['OrgEntities.identifier' => $transferring])
                ->contain(['TypeEntities'])
                ->first();
            if (!$t) {
                $errors[] = __("L'entité ''{0}'' n'existe pas dans l'application", $transferring);
                $code = 404;
            } elseif (!in_array(Hash::get($t, 'type_entity.code'), OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES)) {
                $errors[] = __("L'entité ''{0}'' n'est pas de type versant", $transferring);
                $code = 400;
            } elseif (!$t->get('active')) {
                $errors[] = __("L'entité ''{0}'' n'est pas active", $transferring);
                $code = 400;
            }
        }
        if ($originating = $data['originatingAgencyIdentifier'] ?? null) {
            $o = $this->Seal->archivalAgency()
                ->table('OrgEntities')
                ->find('seal')
                ->where(['OrgEntities.identifier' => $originating])
                ->contain(['TypeEntities'])
                ->first();

            if (!$o) {
                $errors[] = __("L'entité ''{0}'' n'existe pas dans l'application", $originating);
                $code = 404;
            } elseif (!in_array(Hash::get($o, 'type_entity.code'), OrgEntitiesTable::CODE_ORIGINATING_AGENCIES)) {
                $errors[] = __("L'entité ''{0}'' n'est pas de type producteur", $originating);
                $code = 400;
            } elseif (!$o->get('active')) {
                $errors[] = __("L'entité ''{0}'' n'est pas active", $originating);
                $code = 400;
            }
        }
        if ($agreement = $data['agreementIdentifier'] ?? null) {
            $a = $Agreements->find()
                ->where(
                    [
                        'org_entity_id' => $this->archivalAgencyId,
                        'identifier' => $agreement,
                    ]
                )
                ->first();
            if (!$a) {
                $errors[] = __("L'accord de versement ''{0}'' n'existe pas dans l'application", $agreement);
                $code = 404;
            } elseif (!$a->get('active')) {
                $errors[] = __("L'accord de versement ''{0}'' n'est pas actif", $agreement);
                $code = 400;
            }
        }
        if ($profile = $data['profileIdentifier'] ?? null) {
            $p = $Profiles->find()
                ->where(
                    [
                        'org_entity_id' => $this->archivalAgencyId,
                        'identifier' => $profile,
                    ]
                )
                ->first();
            if (!$p) {
                $errors[] = __("Le profil d'archives ''{0}'' n'existe pas dans l'application", $profile);
                $code = 404;
            } elseif (!$p->get('active')) {
                $errors[] = __("Le profil d'archives ''{0}'' n'est pas actif", $profile);
                $code = 400;
            }
        }
        if ($service = $data['serviceLevel'] ?? null) {
            $n = $ServiceLevels->find()
                ->where(
                    [
                        'org_entity_id' => $this->archivalAgencyId,
                        'identifier' => $service,
                    ]
                )
                ->first();
            if (!$n) {
                $errors[] = __("Le niveau de service ''{0}'' n'existe pas dans l'application", $service);
                $code = 404;
            } elseif (!$n->get('active')) {
                $errors[] = __("Le niveau de service ''{0}'' n'est pas actif", $service);
                $code = 400;
            }
        }

        $this->set('errors', $errors);
        return $code;
    }

    /**
     * Récupère le fichier zip
     * @return string fichier zip
     * @throws Exception
     */
    private function getImportZip(): string
    {
        $request = $this->getRequest();
        $targetFilename = $this->baseImportDir . DS . uniqid('import-archive-');
        $size = 0;
        /** @var UploadedFile $upload */
        if ($upload = $request->getUploadedFile('archive')) {
            $targetFilename .= '-' . $upload->getClientFilename();
            $size = $upload->getSize();
            $upload->moveTo($targetFilename);
        } elseif (
            ($url = $request->getData('archive'))
            && preg_match('/^(http|ftp)s?:\/\/.*/', $url)
        ) {
            $targetFilename .= '-' . basename($url);
            $h1 = fopen($url, 'r');
            $h2 = fopen($targetFilename, 'w');
            $size = stream_copy_to_stream($h1, $h2);
            fclose($h1);
            fclose($h2);
        }
        if (!$size || filesize($targetFilename) !== $size) {
            throw new BadRequestException('upload error');
        }
        $type = DataCompressor::detectType($targetFilename);
        if (!$type) {
            throw new BadRequestException(
                __("Le fichier envoyé n'est pas un fichier zip")
            );
        } elseif ($type === 'zip') {
            DataCompressor::sanitizeZip($targetFilename);
        }
        return $targetFilename;
    }

    /**
     * Extraction des fichiers du zip
     * @param string $zip
     * @return string dossier dezippé
     * @throws Exception
     */
    private function extractImportedZip(string $zip): string
    {
        $targetDir = $this->baseImportDir . DS . uniqid('import-archive-');
        Filesystem::mkdir($targetDir);
        DataCompressor::uncompress($zip, $targetDir);
        $documents = false;
        $description = false;
        $transfer = false;
        // Il peut y avoir des milliers de fichiers, on évite de tout parcourir
        if (is_dir($targetDir . DS . 'original_data')) {
            $h = opendir($targetDir . DS . 'original_data');
            while ($f = readdir($h)) {
                if ($f !== '.' && $f !== '..') {
                    $documents = true;
                    break;
                }
            }
            closedir($h);
        }
        if (is_dir($targetDir . DS . 'management_data')) {
            $files = glob($targetDir . DS . 'management_data' . DS . '*');
            foreach ($files as $filename) {
                if (preg_match('/description\.xml$/', $filename)) {
                    $description = true;
                } elseif (preg_match('/transfer\.xml$/', $filename)) {
                    $transfer = true;
                }
            }
        }
        if (!$documents) {
            throw new BadRequestException(
                __("Le dossier ''{0}'' n'a pas été trouvé", 'original_data/')
            );
        } elseif (!$description) {
            throw new BadRequestException(
                __("Le fichier ''{0}'' n'a pas été trouvé", 'description.xml')
            );
        } elseif (!$transfer) {
            throw new BadRequestException(
                __("Le fichier ''{0}'' n'a pas été trouvé", 'transfer.xml')
            );
        }
        return $targetDir;
    }

    /**
     * Créer le transfert à partir des fichiers importés
     * @param string $dir
     * @return EntityInterface
     * @throws Exception
     */
    private function importTransfer(string $dir): EntityInterface
    {
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');
        $transferFile = null;
        foreach (glob($dir . DS . 'management_data' . DS . '*') as $filename) {
            if (preg_match('/transfer\.xml$/', $filename)) {
                if (!PreControlMessages::isValid($filename)) {
                    throw new BadRequestException(
                        __(
                            "Erreur lors du pré-contrôle du transfert: {0}",
                            PreControlMessages::$lastErrorMessage
                        )
                    );
                }
                $transfer = $Transfers->newEntityFromXml($filename);
                $transferFile = $filename;
                break;
            }
        }
        if (empty($transfer) || $transfer->getErrors()) {
            throw new BadRequestException(
                __(
                    "Erreur lors de la création du transfert, celui-ci doit être au format SEDA 1.0 ou 2.1"
                )
            );
        }
        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir . DS . 'original_data')
        );
        $count = 0;
        $size = 0;
        /** @var SplFileInfo $file */
        foreach ($iterator as $file) {
            if ($file->isDir()) {
                continue;
            }
            $size += $file->getSize();
            $count++;
        }
        $Transfers->patchEntity(
            $transfer,
            [
                'created_user_id' => $this->userId,
                'is_conform' => true,
                'is_accepted' => true,
                'data_size' => $size,
                'file_deleted' => true,
                'data_count' => $count,
                'files_deleted' => false,
            ] + $transfer->toArray()
        );
        $Transfers->saveOrFail($transfer);
        Filesystem::dumpFile(
            $transfer->get('xml'),
            file_get_contents($transferFile)
        );
        $Transfers->transitionOrFail($transfer, TransfersTable::T_IMPORT);
        $Transfers->saveOrFail($transfer);

        return $transfer;
    }

    /**
     * Créer l'archive à partir des fichiers importés
     * @param EntityInterface $transfer
     * @param string          $dir
     * @return EntityInterface
     * @throws Exception
     */
    private function importArchive(
        EntityInterface $transfer,
        string $dir
    ): EntityInterface {
        $descriptionFile = null;
        foreach (glob($dir . DS . 'management_data' . DS . '*') as $filename) {
            if (preg_match('/description\.xml$/', $filename)) {
                $descriptionFile = $filename;
                break;
            }
        }
        if (empty($descriptionFile)) {
            throw new Exception();
        }
        $this->util = DOMUtility::load($descriptionFile);
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $this->mapping = $Archives::MAPPING[$this->util->namespace];
        $name = $this->mapValue('name');
        $description = $this->mapValue('description');
        $origIdentifier = $this->mapValue('originating_agency.identifier');
        $archiveIdentifier = $this->mapValue('archival_agency_identifier');
        $transferringIdentifier = $this->mapValue(
            'transferring_agency_identifier'
        );
        $originatingIdentifier = $this->mapValue(
            'originating_agency_identifier'
        );
        $oldest = $this->mapValue(
            'first_archive_unit.archive_description.oldest_date'
        );
        $latest = $this->mapValue(
            'first_archive_unit.archive_description.latest_date'
        );
        switch ($this->util->namespace) {
            case NAMESPACE_SEDA_02:
            case NAMESPACE_SEDA_10:
                $nodeArchive = 'Archive[1]';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $nodeArchive = 'ArchiveUnit[1]';
                break;
            default:
                throw new NotImplementedException(
                    sprintf(
                        'not implemented for namespace "%s"',
                        $this->util->namespace
                    )
                );
        }

        // Originating agency id
        $orig_id = null;
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        if ($origIdentifier) {
            $orig_id = $this->Seal->archivalAgency()
                ->table('OrgEntities')
                ->find('seal')
                ->select(['OrgEntities.id'])
                ->where(['OrgEntities.identifier' => $origIdentifier])
                ->firstOrFail()
                ->get('id');
        }

        // Access rule
        $code = $this->mapValue('access_rule.access_rule_code.code');
        $startDate = $this->mapValue('access_rule.start_date');
        /** @var AccessRulesTable $AccessRules */
        $AccessRules = $this->fetchTable('AccessRules');
        /** @var AccessRuleCodesTable $AccessRuleCodes */
        $AccessRuleCodes = $this->fetchTable('AccessRuleCodes');
        $rule = $AccessRuleCodes->find()
            ->select(['id', 'duration'])
            ->where(['code' => empty($code) ? 'AR062' : $code])
            ->firstOrFail();
        $startDate = new CakeDateTime(
            empty($startDate) ? date('Y-m-d') : $startDate
        );
        $accessRule = $AccessRules->newEntity(
            [
                'access_rule_code_id' => $rule->get('id'),
                'start_date' => clone $startDate,
                'end_date' => $startDate->add(
                    new DateInterval($rule->get('duration'))
                ),
            ]
        );
        $AccessRules->saveOrFail($accessRule);

        // appraisal rule
        /** @var AppraisalRulesTable $AppraisalRules */
        $AppraisalRules = $this->fetchTable('AppraisalRules');
        /** @var AppraisalRuleCodesTable $AppraisalRuleCodes */
        $AppraisalRuleCodes = $this->fetchTable('AppraisalRuleCodes');
        $finalAction = strtolower($this->mapValue('appraisal_rule.final'))
            ?: 'keep';
        $startDate = new CakeDateTime(
            $this->mapValue('appraisal_rule.start_date') ?: date('Y-m-d')
        );
        if (isset($this->mapping['appraisal_rule.appraisal_rule_code.rule'])) {
            $ruleCode = $this->mapValue(
                'appraisal_rule.appraisal_rule_code.rule'
            ) ?: 'APP0Y';
            $cond = ['code' => $ruleCode];
        } else {
            $duration = $this->mapValue(
                'appraisal_rule.appraisal_rule_code.duration'
            ) ?: 'P0Y';
            $cond = ['duration' => $duration];
        }
        $ruleCode = $AppraisalRuleCodes->find()
            ->select(['id', 'duration'])
            ->where($cond)
            ->firstOrFail();
        $appraisalRule = $AppraisalRules->newEntity(
            [
                'appraisal_rule_code_id' => $ruleCode->id,
                'final_action_code' => $finalAction,
                'start_date' => clone $startDate,
                'end_date' => $startDate->add(
                    new DateInterval($rule->get('duration'))
                ),
            ]
        );
        $AppraisalRules->saveOrFail($appraisalRule);

        // secure data space
        $serviceLevel = $this->Seal->archivalAgency()
            ->table('ServiceLevels')
            ->find('seal')
            ->where(
                $transfer->get('service_level_id')
                    ? ['ServiceLevels.id' => $transfer->get('service_level_id')]
                    : ['ServiceLevels.default_level IS' => true]
            )
            ->first();
        if (
            empty($serviceLevel)
            || empty($serviceLevel->get('secure_data_space_id'))
        ) {
            $secure_data_space_id
                = $this->archivalAgency->get('default_secure_data_space_id');
        } else {
            $secure_data_space_id = Hash::get(
                $serviceLevel,
                'secure_data_space_id'
            );
        }

        // originatingAgency
        $originatingAgency = $orig_id
            ? $OrgEntities->get($orig_id)
            : $OrgEntities->get($transfer->get('transferring_agency_id'));

        // profile
        /** @var ProfilesTable $Profiles */
        $Profiles = $this->fetchTable('Profiles');
        $profile = $transfer->get('profile_id')
            ? $Profiles->get($transfer->get('profile_id'))
            : null;

        $archive = $Archives->newEntity(
            [
                'state' => $Archives->initialState,
                'is_integrity_ok' => null,
                'archival_agency_id' => $this->archivalAgencyId,
                'transferring_agency_id' => $transfer->get(
                    'transferring_agency_id'
                ),
                'originating_agency_id' => $originatingAgency->id,
                'transferring_operator_id' => null,
                'agreement_id' => $transfer->get('agreement_id'),
                'profile_id' => $transfer->get('profile_id'),
                'transfer_id' => $transfer->id,
                'service_level_id' => $transfer->get('service_level_id'),
                'access_rule_id' => $accessRule->id,
                'appraisal_rule_id' => $appraisalRule->id,
                'archival_agency_identifier' => $archiveIdentifier,
                'transferring_agency_identifier' => $transferringIdentifier,
                'originating_agency_identifier' => $originatingIdentifier,
                'secure_data_space_id' => $secure_data_space_id,
                'storage_path' => implode(
                    '/',
                    [
                        urlencode($originatingAgency->get('identifier')),
                        $profile ? urlencode($profile->get('identifier'))
                            : 'no_profile',
                        date('Y-m'),
                        urlencode($archiveIdentifier),
                    ]
                ),
                'name' => $name,
                'description' => $description,
                'oldest_date' => $oldest ? new CakeDateTime($oldest) : null,
                'latest_date' => $latest ? new CakeDateTime($latest) : null,
                'transferred_size' => 0,
                'management_size' => 0,
                'original_size' => 0,
                'preservation_size' => 0,
                'dissemination_size' => 0,
                'timestamp_size' => 0,
                'transferred_count' => 0,
                'management_count' => 0,
                'original_count' => 0,
                'preservation_count' => 0,
                'dissemination_count' => 0,
                'timestamp_count' => 0,
                'xml_node_tagname' => $nodeArchive,
                'integrity_date' => null,
                'units_count' => 0,
                'filesexist_date' => null,
                'next_pubdesc' => null,
                'next_search' => null,
            ]
        );
        $Archives->saveOrFail($archive);
        $archive->set('transfers', [$transfer]);
        return $archive;
    }

    /**
     * Donne la valeur selon $path
     * @param string $path
     * @return string|null
     */
    private function mapValue(string $path)
    {
        $node = $this->util->xpath->query($this->mapping[$path])->item(0);
        return $node ? trim($node->nodeValue) : null;
    }

    /**
     * Créer les archive_units lié à l'archive importé
     * @param EntityInterface $archive
     * @return array
     * @throws Exception|Throwable
     */
    protected function importArchiveUnits(EntityInterface $archive): array
    {
        /** @var EntityInterface $transfer */
        $transfer = Hash::get($archive, 'transfers.0');
        $worker = new ArchiveUnitWorker(new Entity());
        $worker->setUtil(DOMUtility::load($transfer->get('xml')));
        $worker->Transfer = $transfer;
        ob_start();
        try {
            $worker->createArchiveUnitsForArchive($archive->id);
        } catch (Throwable $e) {
            ob_get_clean();
            throw $e;
        }
        ob_get_clean();
        return $worker->getXpaths();
    }

    /**
     * Créer les archive_binaries
     * @param EntityInterface $archive
     * @param string          $dir
     * @throws Exception
     */
    private function importArchiveBinaries(
        EntityInterface $archive,
        string $dir
    ) {
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        /** @var ArchiveBinariesArchiveUnitsTable $ArchiveBinariesArchiveUnits */
        $ArchiveBinariesArchiveUnits = $this->fetchTable('ArchiveBinariesArchiveUnits');
        /** @var EntityInterface $transfer */
        $transfer = Hash::get($archive, 'transfers.0');
        $targetUri = $dir . DS . 'original_data';

        // analyse des fichiers (format)
        $exec = Utility::get('Exec')->command('sf -json', $dir);
        $json = json_decode($exec->stdout, true);
        $sf = [];
        foreach (Hash::get($json, 'files', []) as $file) {
            $format = Hash::get($file, 'matches.0.id') ?: 'UNKNOWN';
            $sf[$file['filename']] = [
                'format' => $format === 'UNKNOWN' ? '' : $format,
                'mime' => mime_content_type($file['filename']),
                'extension' => pathinfo($file['filename'], PATHINFO_EXTENSION),
            ];
        }

        $uri = false;
        $util = null;
        if ($transfer->get('message_version') === 'seda2.1' || $transfer->get('message_version') === 'seda2.2') {
            $util = DOMUtility::load($transfer->get('xml'));
            $uri = true;
        }
        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($targetUri)
        );
        $len = strlen($targetUri . DS);
        $manager = new VolumeManager($archive->get('secure_data_space_id'));
        /** @var SplFileInfo $file */
        foreach ($iterator as $file) {
            if ($file->isDir()) {
                continue;
            }
            $file = $file->getPathname();
            $filename = substr($file, $len);

            // Si le fichier est lié par le noeud Uri, on le remplace par un Attachment (avec le filename original)
            if ($uri) {
                $q = sprintf(
                    '//ns:Uri[normalize-space(.)=%s]',
                    DOMUtility::normalizeSpace($filename, true)
                );
                $Uri = $util->xpath->query($q)->item(0);
                if (!$Uri) {
                    $q = sprintf(
                        '//ns:Filename[normalize-space(.)=%s]',
                        DOMUtility::normalizeSpace($filename, true)
                    );
                    $Filename = $util->xpath->query($q)->item(0);
                    if ($Filename) {
                        $Uri = $util->xpath->query(
                            'ns:Uri',
                            $Filename->parentNode->parentNode
                        )->item(0);
                    }
                } else {
                    $f = $util->xpath->query(
                        'ns:FileInfo/ns:Filename',
                        $Uri->parentNode
                    )->item(0);
                    if ($f) {
                        $filename = trim($f->nodeValue);
                        $nfile = $targetUri . DS . $filename;
                        rename($file, $nfile);
                    }
                }
                if ($Uri) {
                    $Attachment = $util->dom->createElementNS(
                        $util->namespace,
                        'Attachment'
                    );
                    $Attachment->setAttribute('filename', $filename);
                    $Uri->parentNode->insertBefore($Attachment, $Uri);
                    $Uri->parentNode->removeChild($Uri);
                }
            }

            $archiveUnit = $ArchiveUnits->find()
                ->where(
                    [
                        'archive_id' => $archive->id,
                        'name' => basename($filename),
                    ]
                )
                ->firstOrFail();
            $path = $archive->get('storage_path')
                . '/original_data/' . $filename;
            $storedFile = $manager->fileUpload($file, $path);
            $detector = new DetectFileFormat($file);
            $originalBinary = $ArchiveBinaries->newEntity(
                [
                    'is_integrity_ok' => null,
                    'type' => 'original_data',
                    'filename' => $filename,
                    'format' => $sf[$file]['format'],
                    'mime' => mime_content_type($file),
                    'extension' => $sf[$file]['extension'],
                    'stored_file_id' => $storedFile->get('id'),
                    'in_rgi' => $detector->rgiCompatible(),
                    'app_meta' => array_filter(
                        [
                            'category' => $detector->getCategory(),
                            'codecs' => $detector->getCodecs(),
                            'puid' => $detector->getPuid(),
                        ]
                    ),
                ]
            );
            $ArchiveBinaries->saveOrFail($originalBinary);
            $link = $ArchiveBinariesArchiveUnits->newEntity(
                [
                    'archive_binary_id' => $originalBinary->id,
                    'archive_unit_id' => $archiveUnit->id,
                ]
            );
            $ArchiveBinariesArchiveUnits->saveOrFail($link);

            if (is_file($dir . DS . 'horodatage' . DS . $filename . '.tsr')) {
                $storedFile = $manager->fileUpload($file, $path);
                $timestampBinary = $ArchiveBinaries->newEntity(
                    [
                        'is_integrity_ok' => null,
                        'type' => 'original_timestamp',
                        'filename' => $filename . '.tsr',
                        'format' => null,
                        'mime' => 'application/octet-stream',
                        'extension' => 'tsr',
                        'stored_file_id' => $storedFile->id,
                        'original_data_id' => $originalBinary->id,
                        'in_rgi' => false,
                        'app_meta' => [],
                    ]
                );
                $ArchiveBinaries->saveOrFail($timestampBinary);
                $link = $ArchiveBinariesArchiveUnits->newEntity(
                    [
                        'archive_binary_id' => $timestampBinary->id,
                        'archive_unit_id' => $archiveUnit->id,
                    ]
                );
                $ArchiveBinariesArchiveUnits->saveOrFail($link);
            }
        }
    }

    /**
     * Créer les fichiers d'archives (transfer, description et lifecycle)
     * @param Archive $archive
     * @param string  $dir
     * @return EntityInterface|null storedFile du lifecycle si il a été importé
     * @throws VolumeException
     * @throws DateInvalidTimeZoneException
     */
    private function importArchiveFiles(Archive $archive, string $dir)
    {
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $manager = new VolumeManager($archive->get('secure_data_space_id'));
        $descriptionFile = null;
        foreach (glob($dir . DS . 'management_data' . DS . '*') as $filename) {
            if (preg_match('/description\.xml$/', $filename)) {
                $descriptionFile = $filename;
            } elseif (preg_match('/transfer\.xml$/', $filename)) {
                $transferFile = $filename;
            } elseif (preg_match('/lifecycle\.xml$/', $filename)) {
                $lifecycleFile = $filename;
            }
        }
        if (empty($descriptionFile) || empty($transferFile)) {
            throw new Exception();
        }
        $basePath = $archive->get('storage_path') . '/management_data/';
        $identifier = $archive->get('archival_agency_identifier');

        /** @var ArchiveFilesTable $ArchiveFiles */
        $ArchiveFiles = $this->fetchTable('ArchiveFiles');
        $storedFile = $manager->fileUpload(
            $descriptionFile,
            $basePath . $identifier . '_description.xml'
        );
        $archiveFile = $ArchiveFiles->newEntity(
            [
                'archive_id' => $archive->get('id'),
                'stored_file_id' => $storedFile->get('id'),
                'is_integrity_ok' => null,
                'type' => ArchiveFilesTable::TYPE_DESCRIPTION,
                'filename' => $identifier . '_description.xml',
            ]
        );
        $ArchiveFiles->saveOrFail($archiveFile);

        $storedFile = $manager->fileUpload(
            $transferFile,
            $basePath . DS . $identifier . '_transfer.xml'
        );
        $archiveFile = $ArchiveFiles->newEntity(
            [
                'archive_id' => $archive->get('id'),
                'stored_file_id' => $storedFile->get('id'),
                'is_integrity_ok' => null,
                'type' => ArchiveFilesTable::TYPE_TRANSFER,
                'filename' => $identifier . '_transfer.xml',
            ]
        );
        $ArchiveFiles->saveOrFail($archiveFile);

        $lifecycle = null;
        if (!empty($lifecycleFile)) {
            $storedFile = $manager->fileUpload(
                $lifecycleFile,
                $basePath . DS . $identifier . '_lifecycle.xml'
            );
            $archiveFile = $ArchiveFiles->newEntity(
                [
                    'archive_id' => $archive->get('id'),
                    'stored_file_id' => $storedFile->get('id'),
                    'is_integrity_ok' => null,
                    'type' => ArchiveFilesTable::TYPE_LIFECYCLE,
                    'filename' => $identifier . '_lifecycle.xml',
                ]
            );
            $ArchiveFiles->saveOrFail($archiveFile);
            $lifecycle = $storedFile;
        } else {
            $storedFile = $manager->filePutContent(
                $basePath . DS . $identifier . '_lifecycle.xml',
                $this->createImportLifecycle($archive)
            );
            $archiveFile = $ArchiveFiles->newEntity(
                [
                    'archive_id' => $archive->get('id'),
                    'stored_file_id' => $storedFile->get('id'),
                    'is_integrity_ok' => null,
                    'type' => ArchiveFilesTable::TYPE_LIFECYCLE,
                    'filename' => $identifier . '_lifecycle.xml',
                ]
            );
            $ArchiveFiles->saveOrFail($archiveFile);
        }

        $Archives->transitionOrFail($archive, ArchivesTable::T_IMPORT);
        $Archives->saveOrFail($archive);
        return $lifecycle;
    }

    /**
     * Créer le fichier lifecycle de l'archive importé
     * @param Archive $archiveEntity
     * @return string
     * @throws Exception
     */
    private function createImportLifecycle(Archive $archiveEntity): string
    {
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $this->fetchTable('EventLogs');
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');
        $premis = new Premis();

        /** @var User $user */
        $user = $Users->get($this->userId);
        $agent = $user->toPremisAgent();
        /** @var Transfer $transferEntity */
        $transferEntity = Hash::get($archiveEntity, 'transfers.0');
        /** @var Premis\IntellectualEntity $transfer */
        $transfer = $transferEntity->toPremisObject();
        $now = (new DateTime())->format(DATE_RFC3339);

        $details = __(
            "Import du transfert ''{0}''",
            $transferEntity->get('transfer_identifier')
        );
        $entry = $EventLogs->newEntry(
            'transfer_import',
            'success',
            $details,
            $this->userId,
            $transferEntity
        );
        $EventLogs->saveOrFail($entry);
        $createTransferEvent = new Premis\Event(
            'transfer_import',
            'local',
            'EventLogs:' . $entry->id
        );
        $createTransferEvent->detail = __("import du transfert");
        $createTransferEvent->outcome = 'success';
        $createTransferEvent->outcomeDetail = $details;
        $createTransferEvent->dateTime = $now;
        $createTransferEvent->addAgent($agent);
        $createTransferEvent->addObject($transfer);
        $premis->add($createTransferEvent);

        /** @var Premis\IntellectualEntity $archive */
        $archive = $archiveEntity->toPremisObject();

        $entry = $EventLogs->newEntry(
            'archive_import',
            'success',
            $details = __(
                "Import de l'archive ''{0}''",
                $archiveEntity->get('archival_agency_identifier')
            ),
            $this->userId,
            $archiveEntity
        );
        $EventLogs->saveOrFail($entry);
        $createArchiveEvent = new Premis\Event(
            'archive_import',
            'local',
            'EventLogs:' . $entry->id
        );
        $createArchiveEvent->detail = __("import de l'archive");
        $createArchiveEvent->outcome = 'success';
        $createArchiveEvent->outcomeDetail = $details;
        $createArchiveEvent->dateTime = $now;
        $createArchiveEvent->addAgent($agent);
        $createArchiveEvent->addObject($archive);
        $premis->add($createArchiveEvent);

        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $units = $Archives->find()
            ->select(
                [
                    'archive_unit_id' => 'ArchiveUnits.id',
                    'archival_agency_identifier' => 'ArchiveUnits.archival_agency_identifier',
                    'name' => 'ArchiveUnits.name',
                    'archive_binary_id' => 'ArchiveBinaries.id',
                    'filename' => 'ArchiveBinaries.filename',
                    'format' => 'Pronoms.name',
                    'puid' => 'Pronoms.puid',
                    'hash' => 'StoredFiles.hash',
                    'hash_algo' => 'StoredFiles.hash_algo',
                    'size' => 'StoredFiles.size',
                    'space_id' => 'SecureDataSpaces.id',
                    'space' => 'SecureDataSpaces.name',
                ]
            )
            ->innerJoinWith('ArchiveUnits')
            ->innerJoinWith('SecureDataSpaces')
            ->leftJoinWith('ArchiveUnits.ArchiveBinaries')
            ->leftJoinWith('ArchiveUnits.ArchiveBinaries.Pronoms')
            ->leftJoinWith('ArchiveUnits.ArchiveBinaries.StoredFiles')
            ->where(
                [
                    'Archives.id' => $archiveEntity->id,
                    'OR' => [
                        'ArchiveBinaries.id IS' => null,
                        'ArchiveBinaries.type' => 'original_data',
                    ],
                ]
            )
            ->orderBy(['ArchiveUnits.lft' => 'asc']);
        $dom = $premis->generateDocument();
        $objects = '';
        $events = '';

        /** @var EntityInterface $entity */
        foreach ($units as $entity) {
            /**
             * Archive unit
             */
            $archiveUnit = new ArchiveUnit(
                [
                    'id' => $entity->get('archive_unit_id'),
                    'archival_agency_identifier' => $entity->get(
                        'archival_agency_identifier'
                    ),
                    'name' => $entity->get('name'),
                ]
            );
            $unit = $archiveUnit->toPremisObject();
            $objects .= $unit;

            $entry = $EventLogs->newEntry(
                'archiveunit_import',
                'success',
                $details = __(
                    "Import de l'unité d'archive ''{0}''",
                    $entity->get('name')
                ),
                $this->userId
            );
            $entry->set('object_model', 'ArchiveUnits');
            $entry->set('object_foreign_key', $entity->get('archive_unit_id'));
            $EventLogs->saveOrFail($entry);
            $createArchiveUnitEvent = new Premis\Event(
                'archiveunit_import',
                'local',
                'EventLogs:' . $entry->id
            );
            $createArchiveUnitEvent->detail = __(
                "import d'une unité d'archive"
            );
            $createArchiveUnitEvent->outcome = 'success';
            $createArchiveUnitEvent->outcomeDetail = $details;
            $createArchiveUnitEvent->dateTime = $now;
            $createArchiveUnitEvent->addAgent($agent);
            $createArchiveUnitEvent->addObject($unit);
            $events .= $createArchiveUnitEvent;

            /**
             * Archive binary
             */
            if ($entity->get('archive_binary_id')) {
                $binary = new Premis\File(
                    $entity->get('format') ?: 'unknown',
                    'local',
                    'ArchiveBinaries:' . $entity->get('archive_binary_id')
                );
                $binary->originalName = $entity->get('filename');
                $binary->messageDigest = $entity->get('hash');
                if ($entity->get('format')) {
                    $binary->formatRegistryName = 'pronom';
                    $binary->formatRegistryKey = $entity->get('puid');
                } else {
                    $binary->formatName = 'UNKNOWN';
                }
                if ($entity->get('hash_algo') !== 'sha256') {
                    $binary->messageDigestAlgorithm = [
                        '@' => $entity->get(
                            'hash_algo'
                        ),
                    ];
                }
                $binary->messageDigest = $entity->get('hash');
                $binary->size = $entity->get('size');
                $binary->relationship = [
                    'relatedObjectIdentifierType' => $unit->identifierType,
                    'relatedObjectIdentifierValue' => $unit->identifierValue,
                ];
                $binary->storages[] = [
                    'contentLocationType' => 'local',
                    'contentLocationValue' => 'SecureDataSpaces:'
                        . $entity->get('space_id'),
                    'storageMedium' => $entity->get('space'),
                ];
                $objects .= $binary;

                $entry = $EventLogs->newEntry(
                    'archivebinary_add',
                    'success',
                    $details = __(
                        "Stockage du fichier ''{0}'' sur l'espace de conservation ''{1}''",
                        $entity->get('name'),
                        $entity->get('space')
                    ),
                    $this->userId
                );
                $entry->set('object_model', 'ArchiveBinaries');
                $entry->set(
                    'object_foreign_key',
                    $entity->get('archive_binary_id')
                );
                $EventLogs->saveOrFail($entry);
                $createArchiveBinaryEvent = new Premis\Event(
                    'archivebinary_add',
                    'local',
                    'EventLogs:' . $entry->id
                );
                $createArchiveBinaryEvent->detail = __("stockage d'un fichier");
                $createArchiveBinaryEvent->outcome = 'success';
                $createArchiveBinaryEvent->outcomeDetail = $details;
                $createArchiveBinaryEvent->dateTime = $now;
                $createArchiveBinaryEvent->addAgent($agent);
                $createArchiveBinaryEvent->addObject($binary);
                $events .= $createArchiveBinaryEvent;
            }
        }
        $dom2 = new DOMDocument();
        $dom2->formatOutput = true;
        $dom2->preserveWhiteSpace = false;
        $xml = $dom->saveXML();
        $firstEvent = strpos($xml, '<event');
        $firstAgent = strpos($xml, '<agent');
        $oldEvents = substr($xml, $firstEvent, $firstAgent - $firstEvent);
        $oldAgents = substr($xml, $firstAgent);
        $dom2->loadXML(
            substr(
                $xml,
                0,
                $firstEvent
            ) . $objects . $oldEvents . $events . $oldAgents
        );
        if (!$dom2->schemaValidate(PREMIS_V3)) {
            throw new Exception('Invalid Premis');
        }
        return $dom2->saveXML();
    }

    /**
     * Récupère les informations lié à une archive (suite à un import)
     * @param string ...$identifier
     * @throws Exception
     */
    protected function getImport(...$identifier)
    {
        $identifier = rawurldecode(implode('/', $identifier));
        $this->viewBuilder()->setLayout('ajax')->setTemplate('info_import');
        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $archive = $Archives->find()
            ->where(
                [
                    'archival_agency_id' => $this->archivalAgencyId,
                    'archival_agency_identifier' => $identifier,
                ]
            )
            ->firstOrFail();
        $this->set('archive', $archive);
    }
}
