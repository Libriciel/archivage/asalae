<?php

/**
 * Asalae\Controller\ValidationProcessesController
 */

namespace Asalae\Controller;

use Asalae\Controller\Component\EventLoggerComponent;
use Asalae\Model\Entity\TransferError;
use Asalae\Model\Entity\ValidationActor;
use Asalae\Model\Entity\ValidationHistory;
use Asalae\Model\Table\AgreementsTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsDeliveryRequestsTable;
use Asalae\Model\Table\ArchiveUnitsDestructionRequestsTable;
use Asalae\Model\Table\ArchiveUnitsOutgoingTransferRequestsTable;
use Asalae\Model\Table\ArchiveUnitsRestitutionRequestsTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\AuthUrlsTable;
use Asalae\Model\Table\DeliveryRequestsTable;
use Asalae\Model\Table\DestructionRequestsTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\OutgoingTransferRequestsTable;
use Asalae\Model\Table\ProfilesTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Table\TransferAttachmentsTable;
use Asalae\Model\Table\TransferErrorsTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\ValidationActorsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationHistoriesTable;
use Asalae\Model\Table\ValidationProcessesTable;
use Asalae\Model\Table\ValidationStagesTable;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\Premis;
use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Beanstalk\Utility\Beanstalk;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use DateInvalidTimeZoneException;
use Exception;
use Pheanstalk\Pheanstalk;
use Psr\Http\Message\ResponseInterface;
use ZMQSocketException;

/**
 * Process de circuit de validation
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AgreementsTable                           Agreements
 * @property ArchiveUnitsDeliveryRequestsTable         ArchiveUnitsDeliveryRequests
 * @property ArchiveUnitsDestructionRequestsTable      ArchiveUnitsDestructionRequests
 * @property ArchiveUnitsOutgoingTransferRequestsTable ArchiveUnitsOutgoingTransferRequests
 * @property ArchiveUnitsRestitutionRequestsTable      ArchiveUnitsRestitutionRequests
 * @property ArchivesTable                             Archives
 * @property AuthUrlsTable                             AuthUrls
 * @property DeliveryRequestsTable                     DeliveryRequests
 * @property DestructionRequestsTable                  DestructionRequests
 * @property EventLogsTable                            EventLogs
 * @property OrgEntitiesTable                          OrgEntities
 * @property OutgoingTransferRequestsTable             OutgoingTransferRequests
 * @property ProfilesTable                             Profiles
 * @property RestitutionRequestsTable                  RestitutionRequests
 * @property TransferAttachmentsTable                  TransferAttachments
 * @property TransferErrorsTable                       TransferErrors
 * @property TransfersTable                            Transfers
 * @property ValidationActorsTable                     ValidationActors
 * @property ValidationChainsTable                     ValidationChains
 * @property ValidationHistoriesTable                  ValidationHistories
 * @property ValidationProcessesTable                  ValidationProcesses
 * @property ValidationStagesTable                     ValidationStages
 */
class ValidationProcessesController extends AppController
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_MY_TRANSFERS = 'validation-processes-my-transers-table';
    public const string TABLE_TRANSFERS = 'validation-processes-transfers-table';
    public const string MODAL_XML_TO_HTML = 'validation-processes-display-xml';
    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public array $paginate = [
        'order' => [
            'ValidationProcesses.id' => 'desc',
        ],
        'sortableFields' => [
            'transfer_data_size' => 'Transfers.data_size',
            'transfer_date' => 'Transfers.transfer_date',
            'transfer_identifier' => 'Transfers.transfer_identifier',
            'transferring_agency' => 'TransferringAgencies.name',
            'current_stage' => 'CurrentStages.name',
        ],
    ];

    /**
     * Liste de mes transferts à traiter
     * @param string|null $action
     * @throws Exception
     */
    public function myTransfers(string $action = null)
    {
        $request = $this->getRequest();
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        $this->set('modalXmlToHtml', self::MODAL_XML_TO_HTML);

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');

        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'Transfers',
            true
        )
            ->contain(['Transfers' => ['TransferringAgencies', 'OriginatingAgencies']]);
        switch ($action) {
            case 'validate':
                $query->andWhere(
                    ['ValidationChains.app_type' => ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM]
                );
                break;
            case 'invalidate':
                $query->andWhere(
                    ['ValidationChains.app_type' => ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM]
                );
                $this->viewBuilder()->setTemplate('myTransfersInvalidate');
                break;
        }
        $IndexComponent->setQuery($query)
            ->filter(
                'validation_stage_id',
                IndexComponent::FILTER_IN,
                'CurrentStages.id'
            )
            ->filter('validation_chain_id', IndexComponent::FILTER_IN)
            ->filter('app_meta', IndexComponent::FILTER_ILIKE, 'actor.app_meta')
            ->filter(
                'transfer_identifier',
                IndexComponent::FILTER_ILIKE,
                'Transfers.transfer_identifier'
            )
            ->filter(
                'transferring_agency_id',
                null,
                'Transfers.transferring_agency_id'
            )
            ->filter(
                'created',
                IndexComponent::FILTER_DATEOPERATOR,
                'Transfers.created'
            )
            ->filter(
                'transfer_date',
                IndexComponent::FILTER_DATEOPERATOR,
                'Transfers.transfer_date'
            )
            ->filter('agreement_id', null, 'Transfers.agreement_id')
            ->filter('profile_id', null, 'Transfers.profile_id')
            ->filter('favoris', IndexComponent::FILTER_FAVORITES);

        // champ additionnels
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $nextQuery = $ValidationStages->selectQuery()
            ->select(['s.id'])
            ->from(['s' => 'validation_stages'])
            ->where(
                [
                    's.validation_chain_id'
                    => new IdentifierExpression('ValidationProcesses.validation_chain_id'),
                    's.ord >' => new IdentifierExpression('CurrentStages.ord'),
                ]
            )
            ->limit(1);
        $errorQuery = $ValidationStages->selectQuery()
            ->select(['e.id'])
            ->from(['e' => 'transfer_errors'])
            ->where(
                [
                    'e.transfer_id' => new IdentifierExpression('ValidationProcesses.app_foreign_key'),
                    'e.code IN' => TransferError::INVALIDATE_CODES,
                ]
            )
            ->limit(1);
        $query->select(
            [
                'next' => $nextQuery,
                'error' => $errorQuery,
            ]
        )->enableAutoFields();

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/transfers-common');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->map(
            function (EntityInterface $e) {
                $e->setVirtual(['has_mail_agent', 'processable']);
                /** @var EntityInterface $transfer */
                $transfer = $e->get('transfer');
                $transfer->setVirtual(['hash', 'is_large']);
                return $e;
            }
        )->toArray();
        $this->set('data', $data);

        // formulaire traitement par lots
        if ($stage_id = $request->getQuery('validation_stage_id.0')) {
            /** @var ValidationActorsTable $ValidationActors */
            $ValidationActors = $this->fetchTable('ValidationActors');
            $actor = $ValidationActors->find()
                ->where(
                    [
                        'validation_stage_id' => $stage_id,
                        'OR' => [
                            [
                                'app_type' => 'USER',
                                'app_foreign_key' => $this->userId,
                            ],
                            [
                                'app_type' => 'SERVICE_ARCHIVES',
                            ],
                        ],
                    ]
                )->firstOrFail();
            $this->set('actor', $actor);
            $stage = $ValidationStages->find()
                ->where(
                    [
                        'ValidationStages.id' => $request->getQuery('validation_stage_id.0'),
                    ]
                )
                ->contain(['ValidationChains'])
                ->firstOrFail();
            $stageName = sprintf(
                "%s '%s' - %s '%s'",
                __("Circuit de validation"),
                Hash::get($stage, 'validation_chain.name'),
                __("Etape"),
                $stage->get('name')
            );
            $this->set('stageName', $stageName);

            /** @var ValidationProcessesTable $ValidationProcesses */
            $ValidationProcesses = $this->fetchTable('ValidationProcesses');
            $process = $ValidationProcesses->newEntity(
                [],
                ['validate' => false]
            );
            $this->set('process', $process);

            $prevs = $ValidationStages->find('list')
                ->where(
                    [
                        'validation_chain_id' => $stage->get(
                            'validation_chain_id'
                        ),
                        'ord <' => $stage->get('ord'),
                    ],
                )
                ->orderBy(['ord'])
                ->all()
                ->toArray();
            $this->set('prevs', $prevs);
            /** @var ValidationHistoriesTable $ValidationHistories */
            $ValidationHistories = $this->fetchTable('ValidationHistories');
            $actions = $ValidationHistories->optionsAction(!empty($prevs));
            $this->set('actions', $actions);
        }

        // Options
        $ta = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('sealList')
            ->innerJoinWith('TypeEntities')
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES]
            )
            ->orderByAsc('OrgEntities.name');
        $this->set('transferring_agencies', $ta->all());

        $oa = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('sealList')
            ->innerJoinWith('TypeEntities')
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES]
            )
            ->orderBy(['OrgEntities.name']);
        $this->set('originating_agencies', $oa->all());

        $stages = $ValidationStages->findListForUser(
            $this->archivalAgencyId,
            $this->user
        );
        $type = $action === 'validate'
            ? ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM
            : ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM;
        $stages->andWhere(['ValidationChains.app_type' => $type])
            ->orderBy(['ValidationChains.name' => 'asc', 'ValidationStages.ord']);

        $stageIds = [];
        foreach ($stages as $ids) {
            $stageIds = array_merge(array_keys($ids), $stageIds);
        }

        if ($chainsIds = $this->getRequest()->getQuery('validation_chain_id')) {
            $ids = [];
            foreach (array_filter(Hash::flatten($chainsIds)) as $chain_id) {
                $ids[] = (int)$chain_id;
            }
            if ($ids) {
                $stages->where(['ValidationChains.id IN' => $ids]);
            }
        }
        $stages = $stages->toArray();
        $this->set('stages', $stages);

        $chains = $stageIds
            ? $this->Seal->archivalAgency()
                ->table('ValidationChains')
                ->find('sealList')
                ->innerJoinWith('ValidationStages')
                ->where(
                    [
                        'ValidationChains.app_type' => $type,
                        'ValidationStages.id IN' => $stageIds,
                    ]
                )
                ->distinct(['ValidationChains.name'])
                ->orderBy(['ValidationChains.name'])
            : [];
        $this->set('chains', $chains);

        /** @var ProfilesTable $Profiles */
        $Profiles = $this->fetchTable('Profiles');
        $profiles = $Profiles->find('list')
            ->where(['Profiles.org_entity_id' => $this->archivalAgencyId])
            ->orderBy(['Profiles.name' => 'asc']);
        $this->set('profiles', $profiles);

        /** @var AgreementsTable $Agreements */
        $Agreements = $this->fetchTable('Agreements');
        $agreements = $Agreements->find('list')
            ->where(['org_entity_id' => $this->archivalAgencyId])
            ->orderBy(['name' => 'asc']);
        $this->set('agreements', $agreements);

        $viewBuilder = $this->viewBuilder();
        if ($request->is('ajax') && $viewBuilder->getLayout() === null && $action === 'invalidate') {
            $viewBuilder->setTemplate('ajax_my_transfers_invalidate');
        }
    }

    /**
     * Liste de tous les transferts en cours de validation
     */
    public function transfers()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->find()
            ->where(
                [
                    'current_stage_id IS NOT' => null,
                    'Transfers.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'Transfers' => ['TransferringAgencies', 'OriginatingAgencies'],
                    'CurrentStages' => ['ValidationActors'],
                    'ValidationChains',
                ]
            );
        $IndexComponent->setQuery($query)
            ->filter(
                'validation_stage_id',
                IndexComponent::FILTER_IN,
                'CurrentStages.id'
            )
            ->filter('validation_chain_id', IndexComponent::FILTER_IN)
            ->filter('app_meta', IndexComponent::FILTER_ILIKE, 'actor.app_meta')
            ->filter(
                'transfer_identifier',
                IndexComponent::FILTER_ILIKE,
                'Transfers.transfer_identifier'
            )
            ->filter(
                'transferring_agency_id',
                null,
                'Transfers.transferring_agency_id'
            )
            ->filter(
                'created',
                IndexComponent::FILTER_DATEOPERATOR,
                'Transfers.created'
            )
            ->filter(
                'transfer_date',
                IndexComponent::FILTER_DATEOPERATOR,
                'Transfers.transfer_date'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES);

        // champ additionnels
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $nextQuery = $ValidationStages->selectQuery()
            ->select(['s.id'])
            ->from(['s' => 'validation_stages'])
            ->where(
                [
                    's.validation_chain_id'
                    => new IdentifierExpression(
                        'ValidationProcesses.validation_chain_id'
                    ),
                    's.ord >' => new IdentifierExpression('CurrentStages.ord'),
                ]
            )
            ->limit(1);
        $errorQuery = $ValidationStages->selectQuery()
            ->select(['e.id'])
            ->from(['e' => 'transfer_errors'])
            ->where(
                [
                    'e.transfer_id' => new IdentifierExpression(
                        'ValidationProcesses.app_foreign_key'
                    ),
                    'e.code IN' => TransferError::INVALIDATE_CODES,
                ]
            )
            ->limit(1);
        $query->select(
            [
                'next' => $nextQuery,
                'error' => $errorQuery,
            ]
        )->enableAutoFields();

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/transfers-common');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->map(
            function (EntityInterface $e) {
                $e->set('validation_user', $this->user);
                $e->set('validation_org_entity', $this->orgEntity);
                $e->setVirtual(['has_mail_agent', 'processable']);
                /** @var EntityInterface $transfer */
                $transfer = $e->get('transfer');
                $transfer->setVirtual(['is_large', 'has_mail_agent']);
                return $e;
            }
        )->toArray();
        $this->set('data', $data);
        $this->Jstable->ajaxPagination();
        $this->set('modalXmlToHtml', self::MODAL_XML_TO_HTML);

        // formulaire traitement par lots
        if (
            !in_array(
                'section',
                $this->getRequest()->getHeader('X-Asalae-Update')
            )
            && $stage_id = $this->getRequest()->getQuery(
                'validation_stage_id.0'
            )
        ) {
            /** @var ValidationActorsTable $ValidationActors */
            $ValidationActors = $this->fetchTable('ValidationActors');
            $actor = $ValidationActors->findByUser(
                $stage_id,
                $this->archivalAgencyId,
                $this->user
            )
                ->orderBy(['ValidationActors.id'])
                ->firstOrFail();
            $this->set('actor', $actor);
            $stage = $ValidationStages->find()
                ->where(
                    [
                        'ValidationStages.id' => $this->getRequest()->getQuery(
                            'validation_stage_id.0'
                        ),
                    ]
                )
                ->contain(['ValidationChains'])
                ->firstOrFail();
            $stageName = sprintf(
                "%s '%s' - %s '%s'",
                __("Circuit de validation"),
                Hash::get($stage, 'validation_chain.name'),
                __("Etape"),
                $stage->get('name')
            );
            $this->set('stageName', $stageName);

            /** @var ValidationProcessesTable $ValidationProcesses */
            $ValidationProcesses = $this->fetchTable('ValidationProcesses');
            $process = $ValidationProcesses->newEntity(
                [],
                ['validate' => false]
            );
            $this->set('process', $process);

            $prevs = $ValidationStages->find('list')
                ->where(
                    [
                        'validation_chain_id' => $stage->get(
                            'validation_chain_id'
                        ),
                        'ord <' => $stage->get('ord'),
                    ],
                )
                ->orderBy(['ord'])
                ->all()
                ->toArray();
            $this->set('prevs', $prevs);
            /** @var ValidationHistoriesTable $ValidationHistories */
            $ValidationHistories = $this->fetchTable('ValidationHistories');
            $actions = $ValidationHistories->optionsAction(
                !empty($prevs)
            );
            $this->set('actions', $actions);
        }

        // Options
        $ta = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('sealList')
            ->innerJoinWith('TypeEntities')
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES]
            )
            ->orderByAsc('OrgEntities.name');
        $this->set('transferring_agencies', $ta->all());

        $oa = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('sealList')
            ->innerJoinWith('TypeEntities')
            ->where(
                ['TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES]
            )
            ->orderBy(['OrgEntities.name']);
        $this->set('originating_agencies', $oa->all());

        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $stages = $ValidationStages->findListForUser(
            $this->archivalAgencyId,
            $this->user
        );
        $stages->where(
            [
                'ValidationChains.app_type IN' => [
                    ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM,
                    ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM,
                ],
            ]
        )
            ->orderBy(['ValidationChains.name' => 'asc', 'ValidationStages.ord']);
        $stageIds = [];
        foreach ($stages as $ids) {
            $stageIds = array_merge(array_keys($ids), $stageIds);
        }

        if ($chainsIds = $this->getRequest()->getQuery('validation_chain_id')) {
            $ids = [];
            foreach (array_filter(Hash::flatten($chainsIds)) as $chain_id) {
                $ids[] = (int)$chain_id;
            }
            if ($ids) {
                $stages->where(['ValidationChains.id IN' => $ids]);
            }
        }
        $this->set('stages', $stages);

        $chains = $stageIds !== []
            ? $this->Seal->archivalAgency()
                ->table('ValidationChains')
                ->find('sealList')
                ->innerJoinWith('ValidationStages')
                ->where(
                    [
                        'ValidationChains.app_type IN' => [
                            ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM,
                            ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM,
                        ],
                        'ValidationStages.id IN' => $stageIds,
                    ]
                )
                ->distinct(['ValidationChains.name'])
                ->orderBy(['ValidationChains.name'])
            : [];
        $this->set('chains', $chains);
    }

    /**
     * Effectu une action sur une étape du circuit de validation
     * @param string $id
     * @return Response
     * @throws ZMQSocketException
     * @throws DateInvalidTimeZoneException
     */
    public function processMyTransfer(string $id)
    {
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        /** @var ValidationHistoriesTable $ValidationHistories */
        $ValidationHistories = $this->fetchTable('ValidationHistories');
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        /** @var TransferErrorsTable $TransferErrors */
        $TransferErrors = $this->fetchTable('TransferErrors');
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $request = $this->getRequest();
        $process = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'Transfers'
        )
            ->where(['ValidationProcesses.id' => $id])
            ->contain(['CurrentStages' => ['ValidationActors'], 'Transfers'])
            ->firstOrFail();
        $transfer = $this->Seal->archivalAgency()
            ->table('Transfers')
            ->find('seal')
            ->where(
                [
                    'Transfers.id' => Hash::get($process, 'transfer.id'),
                    'Transfers.is_conform IS NOT NULL',
                ]
            )
            ->contain(
                [
                    'ArchivalAgencies',
                    'TransferringAgencies',
                    'OriginatingAgencies',
                    'Agreements',
                    'Profiles',
                    'CreatedUsers',
                    'TransferAttachments' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q
                            ->orderBy(['TransferAttachments.filename' => 'asc'])
                            ->limit($AjaxPaginatorComponent->getConfig('limit'));
                    },
                    'ServiceLevels',
                    'TransferErrors' => function (Query $q) use ($AjaxPaginatorComponent) {
                        return $q
                            ->orderBy(
                                [
                                    'TransferErrors.level' => 'asc',
                                    'TransferErrors.id' => 'asc',
                                ]
                            )
                            ->limit($AjaxPaginatorComponent->getConfig('limit'));
                    },
                    'ValidationProcesses' => [
                        'ValidationChains',
                        'CurrentStages' => [
                            'ValidationActors',
                        ],
                        'ValidationHistories' => [
                            'sort' => [
                                'ValidationHistories.created' => 'asc',
                                'ValidationHistories.id' => 'asc',
                            ],
                            'ValidationActors' => [
                                'Users',
                                'ValidationStages',
                            ],
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        $this->set('transfer', $transfer);
        $countUploads = $TransferAttachments->find()
            ->where(['TransferAttachments.transfer_id' => $transfer->get('id')])
            ->count();
        $this->set('countUploads', $countUploads);
        $countErrors = $TransferErrors->find()
            ->where(['TransferErrors.transfer_id' => $transfer->get('id')])
            ->count();
        $this->set('countErrors', $countErrors);
        $actor = $process->get('processable');
        if (!$actor) {
            throw new NotFoundException('actor not found');
        }
        $prevs = $ValidationStages
            ->find('list')
            ->where(
                [
                    'validation_chain_id' => $process->get(
                        'validation_chain_id'
                    ),
                    'ord <' => $process->get('current_stage')->get('ord'),
                ],
            )
            ->orderBy(['ord'])
            ->all()
            ->toArray();
        $this->set('prevs', $prevs);
        $prev = !empty($prevs);
        $next = $ValidationStages->exists(
            [
                'validation_chain_id' => $process->get('validation_chain_id'),
                'ord >' => $process->get('current_stage')->get('ord'),
            ]
        );
        $typeValidation = Hash::get($actor, 'type_validation');
        if (
            $typeValidation === 'S'
            || (!$typeValidation && Hash::get($this->user, 'use_cert'))
        ) {
            $hash = hash_file('sha256', Hash::get($process, 'transfer.xml'));
            $this->set('hash', $hash);
        }
        $this->set('actor', $actor);

        /** @var ValidationHistory $history */
        $history = $ValidationHistories->newEntity(
            [],
            ['validate' => false]
        );
        if ($request->is('post')) {
            $data = [
                'validation_process_id' => $process->get('id'),
                'validation_actor_id' => $actor->get('id'),
                'current_step' => $process->get('current_step'),
                'action' => $request->getData('action'),
                'comment' => $request->getData('comment'),
                'prev' => $prev, // Nécessaire pour la validation
                'next' => $next,
                'created_user_id' => $this->userId,
                'type_validation' => $actor->get('type_validation'),
                'signature' => $request->getData('signature.' . $transfer->id),
            ];
            if ($data['action'] === 'stepback') {
                $prevStage = $ValidationStages->find()->select(['id'])
                    ->where(
                        [
                            'id' => $request->getData('prev_stage_id'),
                            'validation_chain_id' => $process->get(
                                'validation_chain_id'
                            ),
                        ]
                    )->firstOrFail();
                $data['stepback'] = $prevStage->get('id');
            }
            $ValidationHistories->patchEntity($history, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $process = $ValidationProcesses->proceed(
                $data['action'],
                $history
            );
            $conn = $ValidationProcesses->getConnection();
            $conn->begin();
            $success = $ValidationProcesses->save($process)
                && $ValidationHistories->save($history);

            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            switch ($request->getData('action')) {
                case 'validate':
                    $entry = $EventLogs->newEntry(
                        'validate_transfer',
                        $success ? 'success' : 'failed',
                        __(
                            "Validation du transfert ''{0}'' par l'utilisateur ''{1}''",
                            $transfer->get('transfer_identifier'),
                            Hash::get($this->user, 'name') ?: Hash::get($this->user, 'username')
                        ),
                        $this->userId,
                        $transfer
                    );
                    break;
                case 'invalidate':
                    $entry = $EventLogs->newEntry(
                        'invalidate_transfer',
                        $success ? 'success' : 'failed',
                        __(
                            "Refus lors de la validation du transfert ''{0}'' par l'utilisateur ''{1}''",
                            $transfer->get('transfer_identifier'),
                            Hash::get($this->user, 'name') ?: Hash::get($this->user, 'username')
                        ),
                        $this->userId,
                        $transfer
                    );
                    break;
                case 'stepback':
                default:
                    $entry = $EventLogs->newEntry(
                        'validation_stepback_transfer',
                        $success ? 'success' : 'failed',
                        __(
                            "Retour à l'étape précédente lors de la "
                            . "validation du transfert ''{0}'' par l'utilisateur ''{1}''",
                            $transfer->get('transfer_identifier'),
                            Hash::get($this->user, 'name') ?: Hash::get($this->user, 'username')
                        ),
                        $this->userId,
                        $transfer
                    );
                    break;
            }
            $entry->set('in_lifecycle', false);
            $EventLogs->saveOrFail($entry);

            if ($success && $process->get('current_stage_id') === null) {
                /** @var TransfersTable $Transfers */
                $Transfers = $this->fetchTable('Transfers');
                $transition = $process->get('validated')
                    ? TransfersTable::T_ARCHIVE : TransfersTable::T_REFUSE;
                $Transfers->patchEntity(
                    $transfer,
                    [
                        'is_accepted' => $process->get('validated'),
                    ]
                );
                $Transfers->transitionOrFail($transfer, $transition);
                if ($Transfers->save($transfer)) {
                    $conn->commit();
                    $Transfers->createCompositeIfNeeded($transfer);
                    $this->Modal->success();
                    $archiveWorker = Utility::get('Beanstalk', 'archive');
                    $archiveWorker->emit(
                        [
                            'transfer_id' => $transfer->get('id'),
                            'user_id' => $transfer->get('created_user_id'),
                        ]
                    );
                    return $this->renderJson(json_encode($history->toArray()));
                } else {
                    $success = false;
                    FormatError::logEntityErrors($transfer);
                }
            } elseif ($success) {
                $ValidationProcesses->notifyProcess($process);
            }
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                return $this->renderJson(json_encode($history->toArray()));
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($process);
                FormatError::logEntityErrors($history);
            }
        }
        if ($transfer->get('is_conform')) {
            $allowedValidate = true;
        } else {
            /** @var TransferErrorsTable $TransferErrors */
            $TransferErrors = $this->fetchTable('TransferErrors');
            $next = $ValidationStages->find()
                    ->select('id')
                    ->where(
                        [
                            'validation_chain_id' => $process->get('validation_chain_id'),
                            'ord >' => Hash::get($process, 'current_stage.ord'),
                        ]
                    )
                    ->disableHydration()
                    ->first()
                !== null;
            $allowedValidate = $next || $TransferErrors->find()
                    ->select('id')
                    ->where(
                        [
                            'transfer_id' => $transfer->get('id'),
                            'code IN' => TransferError::INVALIDATE_CODES,
                        ]
                    )
                    ->disableHydration()
                    ->first()
                === null;
        }

        $this->set('entity', $history);
        $this->set('process', $process);
        $this->set(
            'actions',
            $ValidationHistories->optionsAction($prev, $allowedValidate)
        );
        $AjaxPaginatorComponent->setViewPaginator(
            Hash::get($transfer, 'transfer_attachments', []),
            $countUploads,
        );
    }

    /**
     * Page de validation d'un transfert (par email)
     * @param string $transfer_id
     * @param string $actor_id
     * @param string $process_id
     * @return ResponseInterface|void
     * @throws ZMQSocketException
     * @throws DateInvalidTimeZoneException
     */
    public function processTransfer(
        string $transfer_id,
        string $actor_id,
        string $process_id
    ) {
        $request = $this->getRequest();
        $done = false;
        $this->viewBuilder()->setLayout('not_connected');
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $this->fetchTable('AuthUrls');
        if (!$AuthUrls->isAuthorized($request)) {
            $this->set('notAuthorized', true);
            return;
        }
        if ($request->getQuery('attachments')) {
            return $this->viewAttachments($transfer_id);
        }

        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $process = $ValidationProcesses->find()
            ->where(
                [
                    'ValidationProcesses.id' => $process_id,
                    'ValidationProcesses.app_subject_type' => 'Transfers',
                    'ValidationProcesses.app_foreign_key' => $transfer_id,
                ]
            )
            ->contain(
                [
                    'Transfers' => [
                        'ArchivalAgencies',
                        'TransferringAgencies',
                        'OriginatingAgencies',
                        'Agreements',
                        'Profiles',
                        'CreatedUsers',
                        'TransferAttachments' => function (Query $q) use ($AjaxPaginatorComponent) {
                            return $q
                                ->orderBy(
                                    ['TransferAttachments.filename' => 'asc']
                                )
                                ->limit(
                                    $AjaxPaginatorComponent->getConfig('limit')
                                );
                        },
                        'ServiceLevels',
                        'TransferErrors' => function (Query $q) use ($AjaxPaginatorComponent) {
                            return $q
                                ->orderBy(['TransferErrors.id' => 'asc'])
                                ->limit(
                                    $AjaxPaginatorComponent->getConfig('limit')
                                );
                        },
                        'ValidationProcesses' => [
                            'CurrentStages' => [
                                'ValidationActors',
                            ],
                            'ValidationHistories' => [
                                'sort' => [
                                    'ValidationHistories.created' => 'desc',
                                    'ValidationHistories.id' => 'desc',
                                ],
                                'ValidationActors' => [
                                    'Users',
                                    'ValidationStages',
                                ],
                            ],
                        ],
                    ],
                    'CurrentStages' => [
                        'ValidationActors' => [
                            'queryBuilder' => function (Query $q) use ($actor_id
                            ) {
                                return $q->where(['id' => $actor_id])->limit(1);
                            },
                        ],
                    ],
                ]
            )
            ->firstOrFail();
        $this->set('process', $process);
        if ($process->get('current_stage') === null) {
            $this->set('isOutdated', true);
            $this->set('done', false);
            return;
        }

        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $prevs = $ValidationStages
            ->find('list')
            ->where(
                [
                    'validation_chain_id' => $process->get(
                        'validation_chain_id'
                    ),
                    'ord <' => $process->get('current_stage')->get('ord'),
                ],
            )
            ->orderBy(['ord'])
            ->all()
            ->toArray();
        $this->set('prevs', $prevs);
        $prev = !empty($prevs);
        $next = $ValidationStages->exists(
            [
                'validation_chain_id' => $process->get('validation_chain_id'),
                'ord >' => $process->get('current_stage')->get('ord'),
            ]
        );

        $isOutdated = Hash::get($process, 'processed')
            || empty(Hash::get($process, 'current_stage.validation_actors'));
        $this->set('isOutdated', $isOutdated);

        /** @var EntityInterface $transfer */
        $transfer = Hash::get($process, 'transfer');
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        $countUploads = $TransferAttachments->find()
            ->where(['TransferAttachments.transfer_id' => $transfer->get('id')])
            ->count();
        $this->set('countUploads', $countUploads);
        /** @var TransferErrorsTable $TransferErrors */
        $TransferErrors = $this->fetchTable('TransferErrors');
        $countErrors = $TransferErrors->find()
            ->where(['TransferErrors.transfer_id' => $transfer->get('id')])
            ->count();
        $this->set('countErrors', $countErrors);

        /** @var EntityInterface $actor */
        $actor = Hash::get($process, 'current_stage.validation_actors.0');
        if (!$actor) {
            throw new NotFoundException(
                sprintf(
                    'actor %d not found in stage %d',
                    $actor_id,
                    $process->get('current_stage_id')
                )
            );
        }

        /** @var ValidationHistoriesTable $ValidationHistories */
        $ValidationHistories = $this->fetchTable('ValidationHistories');
        /** @var ValidationHistory $history */
        $history = $ValidationHistories->newEntity(
            [],
            ['validate' => false]
        );
        if ($this->getRequest()->is('post')) {
            $data = [
                'validation_process_id' => $process->get('id'),
                'validation_actor_id' => $actor->get('id'),
                'current_step' => $process->get('current_step'),
                'action' => $this->getRequest()->getData('action'),
                'comment' => $this->getRequest()->getData('comment'),
                'prev' => $prev, // Nécessaire pour la validation
                'next' => $next,
            ];
            if ($data['action'] === 'stepback') {
                $prevStage = $ValidationStages->find()->select(['id'])
                    ->where(
                        [
                            'id' => $this->getRequest()->getData(
                                'prev_stage_id'
                            ),
                            'validation_chain_id' => $process->get(
                                'validation_chain_id'
                            ),
                        ]
                    )->firstOrFail();
                $data['stepback'] = $prevStage->get('id');
            }
            $ValidationHistories->patchEntity($history, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $process = $ValidationProcesses->proceed(
                $data['action'],
                $history
            );
            $conn = $ValidationProcesses->getConnection();
            $conn->begin();
            $success = $ValidationProcesses->save($process)
                && $ValidationHistories->save($history);

            $agent = new Premis\Agent(
                'local',
                'ValidationActors:' . $actor->id
            );
            $agent->name = [$actor->get('app_key')];
            if ($actor->get('actor_name')) {
                $agent->name[] = $actor->get('actor_name');
            }

            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            switch ($request->getData('action')) {
                case 'validate':
                    $entry = $EventLogs->newEntry(
                        'validate_transfer',
                        $success ? 'success' : 'failed',
                        __(
                            "Validation du transfert ''{0}'' par e-mail ''{1}''",
                            $transfer->get('transfer_identifier'),
                            $actor->get('app_key')
                        ),
                        $agent,
                        $transfer
                    );
                    break;
                case 'invalidate':
                    $entry = $EventLogs->newEntry(
                        'invalidate_transfer',
                        $success ? 'success' : 'failed',
                        __(
                            "Refus lors de la validation du transfert ''{0}'' par e-mail ''{1}''",
                            $transfer->get('transfer_identifier'),
                            $actor->get('app_key')
                        ),
                        $agent,
                        $transfer
                    );
                    break;
                case 'stepback':
                default:
                    $entry = $EventLogs->newEntry(
                        'validation_stepback_transfer',
                        $success ? 'success' : 'failed',
                        __(
                            "Retour à l'étape précédente lors de la "
                            . "validation du transfert ''{0}'' par e-mail ''{1}''",
                            $transfer->get('transfer_identifier'),
                            $actor->get('app_key')
                        ),
                        $agent,
                        $transfer
                    );
                    break;
            }
            $entry->set('in_lifecycle', false);
            $EventLogs->saveOrFail($entry);

            if ($success && $process->get('current_stage_id') === null) {
                /** @var TransfersTable $Transfers */
                $Transfers = $this->fetchTable('Transfers');
                $transition = $process->get('validated')
                    ? TransfersTable::T_ARCHIVE : TransfersTable::T_REFUSE;
                $Transfers->patchEntity(
                    $transfer,
                    [
                        'is_accepted' => $process->get('validated'),
                    ]
                );
                $Transfers->transitionOrFail($transfer, $transition);
                if ($Transfers->save($transfer)) {
                    $conn->commit();
                    $Transfers->createCompositeIfNeeded($transfer);
                    $archiveWorker = Utility::get('Beanstalk', 'archive');
                    $archiveWorker->emit(
                        [
                            'transfer_id' => $transfer->get('id'),
                            'user_id' => $transfer->get('created_user_id'),
                        ]
                    );
                } else {
                    $success = false;
                }
            } elseif ($success) {
                $ValidationProcesses->notifyProcess($process);
            }
            if ($success) {
                $AuthUrls->consume($request);
                $conn->commit();
                $this->Flash->success(
                    __("Votre décision a bien été prise en compte.")
                );
                $done = true;
            } else {
                $conn->rollback();
                $this->Flash->success(__("Une erreur a eu lieu."));
            }
        }
        if ($transfer->get('is_conform')) {
            $allowedValidate = true;
        } else {
            /** @var TransferErrorsTable $TransferErrors */
            $TransferErrors = $this->fetchTable('TransferErrors');
            $next = $ValidationStages->find()
                    ->select('id')
                    ->where(
                        [
                            'validation_chain_id' => $process->get(
                                'validation_chain_id'
                            ),
                            'ord >' => $process->get('current_stage')->get(
                                'ord'
                            ),
                        ]
                    )
                    ->disableHydration()
                    ->first()
                !== null;
            $allowedValidate = $next || $TransferErrors->find()
                    ->select('id')
                    ->where(
                        [
                            'transfer_id' => $transfer->get('id'),
                            'code IN' => TransferError::INVALIDATE_CODES,
                        ]
                    )
                    ->disableHydration()
                    ->first()
                === null;
        }

        $this->set('entity', $history);
        $this->set('transfer', $transfer);
        $this->set(
            'actions',
            $ValidationHistories->optionsAction($prev, $allowedValidate)
        );
        $this->set('done', $done);
        $AjaxPaginatorComponent->setViewPaginator(
            Hash::get($transfer, 'transfer_attachments', []),
            $countUploads,
        );
    }

    /**
     * Pagination ajax des attachments dans l'email
     * @param string $transfer_id
     * @return ResponseInterface
     * @throws Exception
     */
    private function viewAttachments(string $transfer_id)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var TransferAttachmentsTable $TransferAttachments */
        $TransferAttachments = $this->fetchTable('TransferAttachments');
        $query = $TransferAttachments->find()
            ->innerJoinWith('Transfers')
            ->where(['TransferAttachments.transfer_id' => $transfer_id])
            ->orderBy(['TransferAttachments.filename' => 'asc']);

        $this->loadComponent(
            'AsalaeCore.Index',
            ['model' => 'TransferAttachments']
        );
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        $IndexComponent->setQuery($query)
            ->filter('filename', IndexComponent::FILTER_ILIKE)
            ->filter(
                'size',
                function ($s) {
                    if (!$s['value']) {
                        return;
                    }
                    $value = $s['value'] * $s['mult'];
                    $operator = $s['operator'];
                    if (!in_array($operator, ['=', '<=', '>=', '<', '>'])) {
                        throw new BadRequestException(
                            __("L'opérateur indiqué n'est pas autorisé")
                        );
                    }
                    return ['TransferAttachments.size ' . $operator => $value];
                }
            );
        return $AjaxPaginatorComponent->json(
            $query,
            (bool)$this->getRequest()->getQuery('count')
        );
    }

    /**
     * Traitement de masse pour la validation de transferts
     * @return Response
     * @throws Exception
     */
    public function processMultiTransfer()
    {
        $request = $this->getRequest();
        $request->allowMethod('post');

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        /** @var ValidationHistoriesTable $ValidationHistories */
        $ValidationHistories = $this->fetchTable('ValidationHistories');
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        /** @var ValidationActorsTable $ValidationActors */
        $ValidationActors = $this->fetchTable('ValidationActors');
        /** @var TransfersTable $Transfers */
        $Transfers = $this->fetchTable('Transfers');

        $ids = explode(',', $request->getData('ids'));
        if (!$ids) {
            throw new BadRequestException();
        }

        $isValidator = Hash::get($this->user, 'is_validator');
        $conditions = [
            'OR' => [
                [
                    'ValidationActors.app_foreign_key' => $this->userId,
                    'ValidationActors.app_type' => 'USER',
                ],
            ],
        ];
        $typeEntity = Hash::get($this->user, 'org_entity.type_entity.code');
        $isSuperArchivist = in_array($typeEntity, OrgEntitiesTable::CODE_OPERATING_DEPARTMENT);
        if ($isValidator && ($this->orgEntityId === $this->archivalAgencyId || $isSuperArchivist)) {
            $conditions['OR'][] = [
                'ValidationActors.app_type' => 'SERVICE_ARCHIVES',
            ];
        }

        $processes = $ValidationProcesses->find()
            ->where(['ValidationProcesses.id IN' => $ids])
            ->andWhere(['CurrentStages.id IS NOT' => null] + $conditions)
            ->andWhere(
                ['Transfers.archival_agency_id' => $this->archivalAgencyId]
            )
            ->innerJoinWith('CurrentStages')
            ->innerJoinWith('CurrentStages.ValidationActors')
            ->distinct(['ValidationProcesses.id'])
            ->contain(['CurrentStages', 'Transfers']);
        if (count($ids) !== $processes->count()) {
            throw new BadRequestException(
                __(
                    "Un des transferts ne semble pas pouvoir être traité par vous"
                )
            );
        }
        $conn = $ValidationProcesses->getConnection();
        $conn->begin();
        $notify = [];
        $emit = [];
        foreach ($processes as $process) {
            /** @var EntityInterface $transfer */
            $transfer = $process->get('transfer');
            // NOTE : devrait toujours être le même actor_id (même étape)
            $actor = $actor ?? $ValidationActors->find()
                ->where(
                    [
                        'validation_stage_id' => $process->get(
                            'current_stage_id'
                        ),
                    ]
                )
                ->andWhere($conditions)
                ->firstOrFail();
            $prevs = $prevs ?? $ValidationStages
                ->find('list')
                ->where(
                    [
                        'validation_chain_id' => $process->get(
                            'validation_chain_id'
                        ),
                        'ord <' => $process->get('current_stage')->get('ord'),
                    ],
                )
                ->orderBy(['ord'])
                ->all()
                ->toArray();
            $prev = !empty($prevs);
            $next = $next ?? $ValidationStages->exists(
                [
                    'validation_chain_id' => $process->get(
                        'validation_chain_id'
                    ),
                    'ord >' => $process->get('current_stage')->get('ord'),
                ]
            );
            $data = [
                'validation_process_id' => $process->get('id'),
                'validation_actor_id' => $actor->get('id'),
                'current_step' => $process->get('current_step'),
                'action' => $request->getData('action'),
                'comment' => $request->getData('comment'),
                'prev' => $prev, // Nécessaire pour la validation
                'next' => $next,
                'created_user_id' => $this->userId,
                'type_validation' => $actor->get('type_validation'),
                'signature' => $request->getData('signature.' . $process->id),
            ];
            if ($data['action'] === 'stepback') {
                $prevStage = $ValidationStages->find()->select(['id'])
                    ->where(
                        [
                            'id' => $request->getData('prev_stage_id'),
                            'validation_chain_id' => $process->get(
                                'validation_chain_id'
                            ),
                        ]
                    )->firstOrFail();
                $data['stepback'] = $prevStage->get('id');
            }
            /** @var ValidationHistory $history */
            $history = $ValidationHistories->newEntity($data);
            $process = $ValidationProcesses->proceed(
                $data['action'],
                $history
            );
            if (!$ValidationProcesses->save($process)) {
                $conn->rollback();
                throw new BadRequestException(
                    __(
                        "Echec lors de l'enregistrement: {0}",
                        var_export($process->getErrors(), true)
                    )
                );
            }
            if (!$ValidationHistories->save($history)) {
                $conn->rollback();
                throw new BadRequestException(
                    __(
                        "Echec lors de l'enregistrement: {0}",
                        var_export($history->getErrors(), true)
                    )
                );
            }
            if ($process->get('current_stage_id') === null) {
                /** @var TransfersTable $Transfers */
                $Transfers = $this->fetchTable('Transfers');
                $transition = $process->get('validated')
                    ? TransfersTable::T_ARCHIVE : TransfersTable::T_REFUSE;
                $Transfers->patchEntity(
                    $transfer,
                    [
                        'is_accepted' => $process->get('validated'),
                    ]
                );
                $Transfers->transitionOrFail($transfer, $transition);
                if (!$Transfers->save($transfer)) {
                    $conn->rollback();
                    throw new BadRequestException(
                        __(
                            "Echec lors de l'enregistrement: {0}",
                            var_export($transfer->getErrors(), true)
                        )
                    );
                }
                $emit[] = $transfer;
            } else {
                $notify[] = $process;
            }

            /** @var EventLogsTable $EventLogs */
            $EventLogs = $this->fetchTable('EventLogs');
            switch ($data['action']) {
                case 'validate':
                    $entry = $EventLogs->newEntry(
                        'validate_transfer',
                        'success',
                        __(
                            "Validation du transfert ''{0}'' par l'utilisateur ''{1}''",
                            $transfer->get('transfer_identifier'),
                            Hash::get($this->user, 'name') ?: Hash::get($this->user, 'username')
                        ),
                        $this->userId,
                        $transfer
                    );
                    break;
                case 'invalidate':
                    $entry = $EventLogs->newEntry(
                        'invalidate_transfer',
                        'success',
                        __(
                            "Refus lors de la validation du transfert ''{0}'' par l'utilisateur ''{1}''",
                            $transfer->get('transfer_identifier'),
                            Hash::get($this->user, 'name') ?: Hash::get($this->user, 'username')
                        ),
                        $this->userId,
                        $transfer
                    );
                    break;
                case 'stepback':
                default:
                    $entry = $EventLogs->newEntry(
                        'validation_stepback_transfer',
                        'success',
                        __(
                            "Retour à l'étape précédente lors de la "
                            . "validation du transfert ''{0}'' par l'utilisateur ''{1}''",
                            $transfer->get('transfer_identifier'),
                            Hash::get($this->user, 'name') ?: Hash::get($this->user, 'username')
                        ),
                        $this->userId,
                        $transfer
                    );
                    break;
            }
            $entry->set('in_lifecycle', false);
            $EventLogs->saveOrFail($entry);
        }
        $conn->commit();
        $archiveWorker = Utility::get('Beanstalk', 'archive');
        foreach ($notify as $process) {
            $ValidationProcesses->notifyProcess($process);
        }
        foreach ($emit as $transfer) {
            $Transfers->createCompositeIfNeeded($transfer);
            $archiveWorker->emit(
                [
                    'transfer_id' => $transfer->id,
                    'user_id' => $transfer->get('created_user_id'),
                ]
            );
        }

        return $this->renderJson('"done"');
    }

    /**
     * Renvoi l'email pour la validation d'un transfert
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function resendMail(string $id)
    {
        $sa = $this->archivalAgency;
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $subquery = $OrgEntities->find()
            ->select(['OrgEntities.id'])
            ->where(
                [
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                ]
            );
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $process = $ValidationProcesses->find()
            ->where(
                [
                    'ValidationProcesses.id' => $id,
                    'ValidationChains.org_entity_id IN' => $subquery,
                    'CurrentStages.id IS NOT' => null,
                ]
            )
            ->contain(
                [
                    'CurrentStages' => [
                        'ValidationActors' => function (Query $q) {
                            return $q->where(
                                ['ValidationActors.app_type' => 'MAIL']
                            );
                        },
                    ],
                    'Transfers',
                    'ValidationChains',
                ]
            )
            ->firstOrFail();
        /** @var ValidationActor $actor */
        foreach (
            Hash::get($process, 'current_stage.validation_actors', []) as $actor
        ) {
            $actor->notify($process);
        }
        return $this->renderJson('"done"');
    }

    /**
     * Index des validation de demande de communication
     * @throws Exception
     */
    public function deliveryRequests()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'DeliveryRequests',
            true
        )
            ->contain(
                [
                    'DeliveryRequests' => [
                        'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                            return $q
                                ->orderBy(
                                    ['ArchiveUnits.archival_agency_identifier']
                                )
                                ->limit(
                                    $AjaxPaginatorComponent->getConfig('limit')
                                );
                        },
                        'CreatedUsers' => [
                            'OrgEntities',
                        ],
                    ],
                ]
            );
        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        $subquery = $DeliveryRequests->selectQuery()
            ->select(['dr.id'])
            ->from(['dr' => 'delivery_requests'])
            ->innerJoin(
                ['lk' => 'archive_units_delivery_requests'],
                ['lk.delivery_request_id' => new IdentifierExpression('dr.id')]
            )
            ->innerJoin(
                ['au' => 'archive_units'],
                ['au.id' => new IdentifierExpression('lk.archive_unit_id')]
            )
            ->where(
                ['dr.id' => new IdentifierExpression('DeliveryRequests.id')]
            )
            ->limit(1);
        $IndexComponent->setQuery($query)
            ->filter(
                'archive_unit_identifier',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike(
                            'au.archival_agency_identifier',
                            $v
                        )
                    );
                    return $q->andWhere(['DeliveryRequests.id IN' => $sq]);
                }
            )
            ->filter(
                'archive_unit_name',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike('au.name', $v)
                    );
                    return $q->andWhere(['DeliveryRequests.id IN' => $sq]);
                }
            )
            ->filter(
                'created_user',
                IndexComponent::FILTER_IN,
                'DeliveryRequests.created_user_id'
            )
            ->filter(
                'delivery_request_created',
                IndexComponent::FILTER_DATEOPERATOR,
                'DeliveryRequests.created'
            )
            ->filter(
                'delivery_request_sent',
                IndexComponent::FILTER_DATEOPERATOR,
                'DeliveryRequests.last_state_update'
            )
            ->filter(
                'derogation',
                IndexComponent::FILTER_IS,
                'DeliveryRequests.derogation'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter(
                'identifier',
                IndexComponent::FILTER_ILIKE,
                'DeliveryRequests.identifier'
            )
            ->filter(
                'validation_stage_id',
                IndexComponent::FILTER_IN,
                'CurrentStages.id'
            )
            ->filter('validation_chain_id', IndexComponent::FILTER_IN);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);

        //options
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $stages = $ValidationStages->find(
            'list',
            keyField: 'id',
            valueField: 'name',
            groupField: '_matchingData.ValidationChains.name',
        )
            ->select(
                [
                    'ValidationStages.name',
                    'ValidationStages.id',
                    'ValidationChains.name',
                ]
            )
            ->innerJoinWith('ValidationChains')
            ->innerJoinWith(
                'ValidationProcesses'
            ) // filtre current_stage_id != null
            ->innerJoinWith('ValidationActors')
            ->where(
                [
                    'ValidationActors.app_type' => 'USER',
                    'ValidationActors.app_foreign_key' => $this->userId,
                    'ValidationChains.app_type' => ValidationChainsTable::ARCHIVE_DELIVERY_REQUEST,
                    'ValidationChains.org_entity_id' => $this->orgEntityId,
                ]
            )
            ->orderBy(['ValidationChains.name' => 'asc', 'ValidationStages.ord']);
        $this->set('stages', $stages);

        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        $created_users = $DeliveryRequests->find(
            'list',
            keyField: '_matchingData.CreatedUsers.id',
            valueField: '_matchingData.CreatedUsers.username',
            groupField: '_matchingData.OrgEntities.name',
        )
            ->select(
                ['CreatedUsers.id', 'CreatedUsers.username', 'OrgEntities.name']
            )
            ->innerJoinWith('CreatedUsers')
            ->innerJoinWith('CreatedUsers.OrgEntities')
            ->distinct(
                $distinct = [
                    'OrgEntities.name',
                    'CreatedUsers.username',
                ]
            )
            ->where(
                ['DeliveryRequests.archival_agency_id' => $this->archivalAgencyId]
            )
            ->orderBy($distinct);
        $this->set('created_users', $created_users);
    }

    /**
     * Validation d'une demande de communication par email
     * @param string $process_id
     * @param string $actor_id
     * @throws Exception
     */
    public function processDeliveryRequestMail(
        string $process_id,
        string $actor_id
    ) {
        $request = $this->getRequest();
        $this->viewBuilder()->setLayout('not_connected');
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $this->fetchTable('AuthUrls');
        if (!$AuthUrls->isAuthorized($request)) {
            $this->set('notAuthorized', true);
            return;
        }

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $process = $ValidationProcesses->find()
            ->innerJoinWith('CurrentStages')
            ->innerJoinWith('CurrentStages.ValidationActors')
            ->where(
                [
                    'ValidationProcesses.id' => $process_id,
                    'ValidationActors.id' => $actor_id,
                ]
            )
            ->contain(
                [
                    'CurrentStages' => ['ValidationActors'],
                    'ValidationChains' => ['OrgEntities'],
                ]
            )
            ->firstOrFail();
        if ($process->get('current_stage') === null) {
            $this->set('isOutdated', true);
            $this->set('done', false);
        } else {
            $this->set('isOutdated', false);
            $this->set('process', $process);
            $this->archivalAgency = Hash::get(
                $process,
                'validation_chain.org_entity'
            );
            $this->archivalAgencyId = Hash::get(
                $process,
                'validation_chain.org_entity_id'
            );
            $this->processDeliveryRequest($process_id, (int)$actor_id);
            $success = $this->getResponse()->getHeader('X-Asalae-Success');
            $success = $success ? (current($success) === 'true') : null;
            $done = false;
            if ($success) {
                $AuthUrls->consume($request);
                $this->Flash->success(
                    __("Votre décision a bien été prise en compte.")
                );
                $done = true;
            } elseif ($success === false) {
                $this->Flash->success(__("Une erreur a eu lieu."));
            }
            $this->set('done', $done);
        }
    }

    /**
     * Validation d'une demande de communication
     * @param string   $id
     * @param int|null $actor_id
     * @return Response
     * @throws Exception
     */
    public function processDeliveryRequest(string $id, int $actor_id = null)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $request = $this->getRequest();
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'DeliveryRequests',
            false,
            $actor_id
        )
            ->where(['ValidationProcesses.id' => $id])
            ->contain(
                [
                    'DeliveryRequests' => [
                        'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                            return $q
                                ->orderBy(
                                    ['ArchiveUnits.archival_agency_identifier']
                                )
                                ->limit(
                                    $AjaxPaginatorComponent->getConfig('limit')
                                )
                                ->contain(
                                    [
                                        'Archives' => ['DescriptionXmlArchiveFiles'],
                                        'ArchiveDescriptions',
                                    ]
                                );
                        },
                        'CreatedUsers' => [
                            'OrgEntities',
                        ],
                    ],
                ]
            );
        $process = $query->firstOrFail();
        /** @var EntityInterface[] $archive_units */
        $archive_units = Hash::get($process, 'delivery_request.archive_units');
        foreach ($archive_units as $archiveUnit) {
            $archiveUnit->setVirtual(['description', 'dates']);
        }
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        /** @var EntityInterface $deliveryRequest */
        $deliveryRequest = $process->get('delivery_request');
        $this->set('deliveryRequest', $deliveryRequest);
        /** @var ArchiveUnitsDeliveryRequestsTable $ArchiveUnitsDeliveryRequests */
        $ArchiveUnitsDeliveryRequests = $this->fetchTable('ArchiveUnitsDeliveryRequests');
        $unitsCount = $ArchiveUnitsDeliveryRequests->find()
            ->where(['delivery_request_id' => $deliveryRequest->id])
            ->count();
        $this->set('unitsCount', $unitsCount);
        $prevs = $ValidationStages->find('list')
            ->where(
                [
                    'validation_chain_id' => $process->get(
                        'validation_chain_id'
                    ),
                    'ord <' => Hash::get($process, 'current_stage.ord', 0),
                ]
            )
            ->orderBy(['ord'])
            ->toArray();
        $this->set('prevs', $prevs);
        $prev = !empty($prevs);
        $next = $ValidationStages->exists(
            [
                'validation_chain_id' => $process->get('validation_chain_id'),
                'ord >' => $process->get('current_stage')->get('ord'),
            ]
        );
        $actor = $process->get('processable');
        if (!$actor) {
            throw new NotFoundException('actor not found');
        }
        $this->set('actor', $actor);

        $typeValidation = Hash::get($actor, 'type_validation');
        if (
            $typeValidation === 'S'
            || (!$typeValidation && Hash::get($this->user, 'use_cert'))
        ) {
            $hash = hash_file(
                'sha256',
                Hash::get($process, 'delivery_request.xml')
            );
            $this->set('hash', $hash);
        }
        /** @var ValidationHistoriesTable $ValidationHistories */
        $ValidationHistories = $this->fetchTable('ValidationHistories');
        /** @var ValidationHistory $history */
        $history = $ValidationHistories->newEntity(
            [],
            ['validate' => false]
        );
        if ($request->is('post')) {
            $data = [
                'validation_process_id' => $process->get('id'),
                'validation_actor_id' => $actor->get('id'),
                'current_step' => $process->get('current_step'),
                'action' => $request->getData('action'),
                'comment' => $request->getData('comment'),
                'prev' => $prev,
                'next' => $next,
                'created_user_id' => $this->userId,
                'type_validation' => $actor->get('type_validation'),
                'signature' => $request->getData(
                    'signature.' . $deliveryRequest->id
                ),
            ];
            if ($data['action'] === 'stepback') {
                $prevStage = $ValidationStages->find()->select(['id'])
                    ->where(
                        [
                            'id' => $request->getData('prev_stage_id'),
                            'validation_chain_id' => $process->get(
                                'validation_chain_id'
                            ),
                        ]
                    )->firstOrFail();
                $data['stepback'] = $prevStage->get('id');
            }
            $ValidationHistories->patchEntity($history, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $process = $ValidationProcesses->proceed(
                $data['action'],
                $history
            );
            $conn = $ValidationProcesses->getConnection();
            $conn->begin();
            $success = $ValidationProcesses->save($process)
                && $ValidationHistories->save($history);
            if ($success && $process->get('current_stage_id') === null) {
                /** @var DeliveryRequestsTable $DeliveryRequests */
                $DeliveryRequests = $this->fetchTable('DeliveryRequests');
                $transition = $process->get('validated')
                    ? DeliveryRequestsTable::T_ACCEPT
                    : DeliveryRequestsTable::T_REFUSE;
                $DeliveryRequests->transitionOrFail(
                    $deliveryRequest,
                    $transition
                );
                if ($DeliveryRequests->save($deliveryRequest)) {
                    $conn->commit();
                    $this->Modal->success();
                    /** @var EventLoggerComponent $EventLogger */
                    $EventLogger = $this->loadComponent('EventLogger');
                    if ($transition === 'accept') {
                        $EventLogger->logValidationAccept($deliveryRequest, 'DeliveryRequests');
                        /** @var Beanstalk $Beanstalk */
                        $Beanstalk = Utility::get('Beanstalk');
                        $Beanstalk->setTube('delivery');
                        $Beanstalk->emit(
                            [
                                'user_id' => $this->userId,
                                'delivery_request_id' => $deliveryRequest->id,
                            ],
                            Pheanstalk::DEFAULT_PRIORITY,
                            Pheanstalk::DEFAULT_DELAY,
                            3600 // on part du principe qu'un fichier peut mettre 1h à se télécharger
                        );
                    } else {
                        $EventLogger->logValidationReject($deliveryRequest, 'DeliveryRequests');
                    }
                    return $this->renderJson(json_encode($history->toArray()));
                } else {
                    $success = false;
                    FormatError::logEntityErrors($deliveryRequest);
                }
            } elseif ($success) {
                $ValidationProcesses->notifyProcess($process);
            }
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                return $this->renderJson(json_encode($history->toArray()));
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($process);
                FormatError::logEntityErrors($history);
            }
        }

        $this->set('entity', $history);
        $this->set('process', $process);
        $this->set(
            'actions',
            $ValidationHistories->optionsAction($prev)
        );
        $AjaxPaginatorComponent->setViewPaginator(
            $deliveryRequest->get('archive_units') ?: [],
            $unitsCount,
        );
    }

    /**
     * Index des validation des demandes d'élimination
     * @throws Exception
     */
    public function destructionRequests()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'DestructionRequests',
            true
        )
            ->contain(
                [
                    'DestructionRequests' => [
                        'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                            return $q
                                ->orderBy(
                                    ['ArchiveUnits.archival_agency_identifier']
                                )
                                ->limit(
                                    $AjaxPaginatorComponent->getConfig('limit')
                                );
                        },
                        'CreatedUsers' => [
                            'OrgEntities',
                        ],
                    ],
                ]
            );

        /** @var DestructionRequestsTable $DestructionRequests */
        $DestructionRequests = $this->fetchTable('DestructionRequests');
        $subquery = $DestructionRequests->selectQuery()
            ->select(['dr.id'])
            ->from(['dr' => 'destruction_requests'])
            ->innerJoin(
                ['lk' => 'archive_units_destruction_requests'],
                [
                    'lk.destruction_request_id' => new IdentifierExpression(
                        'dr.id'
                    ),
                ]
            )
            ->innerJoin(
                ['au' => 'archive_units'],
                ['au.id' => new IdentifierExpression('lk.archive_unit_id')]
            )
            ->where(
                ['dr.id' => new IdentifierExpression('DestructionRequests.id')]
            )
            ->limit(1);
        $IndexComponent->setQuery($query)
            ->filter(
                'archive_unit_identifier',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike(
                            'au.archival_agency_identifier',
                            $v
                        )
                    );
                    return $q->andWhere(['DestructionRequests.id IN' => $sq]);
                }
            )
            ->filter(
                'archive_unit_name',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike('au.name', $v)
                    );
                    return $q->andWhere(['DestructionRequests.id IN' => $sq]);
                }
            )
            ->filter(
                'created_user',
                IndexComponent::FILTER_IN,
                'DestructionRequests.created_user_id'
            )
            ->filter(
                'destruction_request_created',
                IndexComponent::FILTER_DATEOPERATOR,
                'DestructionRequests.created'
            )
            ->filter(
                'destruction_request_sent',
                IndexComponent::FILTER_DATEOPERATOR,
                'DestructionRequests.last_state_update'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter(
                'identifier',
                IndexComponent::FILTER_ILIKE,
                'DestructionRequests.identifier'
            )
            ->filter(
                'validation_stage_id',
                IndexComponent::FILTER_IN,
                'CurrentStages.id'
            )
            ->filter('validation_chain_id', IndexComponent::FILTER_IN);
        $IndexComponent->setQuery($query);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);

        //options

        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $stages = $ValidationStages->find(
            'list',
            keyField: 'id',
            valueField: 'name',
            groupField: '_matchingData.ValidationChains.name',
        )
            ->select(
                [
                    'ValidationStages.name',
                    'ValidationStages.id',
                    'ValidationChains.name',
                ]
            )
            ->innerJoinWith('ValidationChains')
            ->innerJoinWith(
                'ValidationProcesses'
            ) // filtre current_stage_id != null
            ->innerJoinWith('ValidationActors')
            ->where(
                [
                    'ValidationActors.app_type' => 'USER',
                    'ValidationActors.app_foreign_key' => $this->userId,
                    'ValidationChains.app_type' => ValidationChainsTable::ARCHIVE_DESTRUCTION_REQUEST,
                    'ValidationChains.org_entity_id' => $this->orgEntityId,
                ]
            )
            ->orderBy(['ValidationChains.name' => 'asc', 'ValidationStages.ord']);
        $this->set('stages', $stages);

        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        $created_users = $DeliveryRequests->find(
            'list',
            keyField: '_matchingData.CreatedUsers.id',
            valueField: '_matchingData.CreatedUsers.username',
            groupField: '_matchingData.OrgEntities.name',
        )
            ->select(
                ['CreatedUsers.id', 'CreatedUsers.username', 'OrgEntities.name']
            )
            ->innerJoinWith('CreatedUsers')
            ->innerJoinWith('CreatedUsers.OrgEntities')
            ->distinct(
                $distinct = [
                    'OrgEntities.name',
                    'CreatedUsers.username',
                ]
            )
            ->where(
                ['DeliveryRequests.archival_agency_id' => $this->archivalAgencyId]
            )
            ->orderBy($distinct);
        $this->set('created_users', $created_users);
    }

    /**
     * Index de la validation des demandes de restitutions
     * @throws Exception
     */
    public function restitutionRequests()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'RestitutionRequests',
            true
        )
            ->contain(
                [
                    'RestitutionRequests' => [
                        'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                            return $q
                                ->orderBy(
                                    ['ArchiveUnits.archival_agency_identifier']
                                )
                                ->limit(
                                    $AjaxPaginatorComponent->getConfig('limit')
                                );
                        },
                        'CreatedUsers' => [
                            'OrgEntities',
                        ],
                    ],
                ]
            );

        /** @var RestitutionRequestsTable $RestitutionRequests */
        $RestitutionRequests = $this->fetchTable('RestitutionRequests');
        $subquery = $RestitutionRequests->selectQuery()
            ->select(['dr.id'])
            ->from(['dr' => 'restitution_requests'])
            ->innerJoin(
                ['lk' => 'archive_units_restitution_requests'],
                [
                    'lk.restitution_request_id' => new IdentifierExpression(
                        'dr.id'
                    ),
                ]
            )
            ->innerJoin(
                ['au' => 'archive_units'],
                ['au.id' => new IdentifierExpression('lk.archive_unit_id')]
            )
            ->where(
                ['dr.id' => new IdentifierExpression('RestitutionRequests.id')]
            )
            ->limit(1);
        $IndexComponent->setQuery($query)
            ->filter(
                'archive_unit_identifier',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike(
                            'au.archival_agency_identifier',
                            $v
                        )
                    );
                    return $q->andWhere(['RestitutionRequests.id IN' => $sq]);
                }
            )
            ->filter(
                'archive_unit_name',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike('au.name', $v)
                    );
                    return $q->andWhere(['RestitutionRequests.id IN' => $sq]);
                }
            )
            ->filter(
                'created_user',
                IndexComponent::FILTER_IN,
                'RestitutionRequests.created_user_id'
            )
            ->filter(
                'restitution_request_created',
                IndexComponent::FILTER_DATEOPERATOR,
                'RestitutionRequests.created'
            )
            ->filter(
                'restitution_request_sent',
                IndexComponent::FILTER_DATEOPERATOR,
                'RestitutionRequests.last_state_update'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter(
                'identifier',
                IndexComponent::FILTER_ILIKE,
                'RestitutionRequests.identifier'
            )
            ->filter(
                'validation_stage_id',
                IndexComponent::FILTER_IN,
                'CurrentStages.id'
            )
            ->filter('validation_chain_id', IndexComponent::FILTER_IN);
        $IndexComponent->setQuery($query);

        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);

        //options
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $stages = $ValidationStages->find(
            'list',
            keyField: 'id',
            valueField: 'name',
            groupField: '_matchingData.ValidationChains.name',
        )
            ->select(
                [
                    'ValidationStages.name',
                    'ValidationStages.id',
                    'ValidationChains.name',
                ]
            )
            ->innerJoinWith('ValidationChains')
            ->innerJoinWith(
                'ValidationProcesses'
            ) // filtre current_stage_id != null
            ->innerJoinWith('ValidationActors')
            ->where(
                [
                    'ValidationActors.app_type' => 'USER',
                    'ValidationActors.app_foreign_key' => $this->userId,
                    'ValidationChains.app_type' => ValidationChainsTable::ARCHIVE_RESTITUTION_REQUEST,
                    'ValidationChains.org_entity_id' => $this->orgEntityId,
                ]
            )
            ->orderBy(['ValidationChains.name' => 'asc', 'ValidationStages.ord']);
        $this->set('stages', $stages);

        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        $created_users = $DeliveryRequests->find(
            'list',
            keyField: '_matchingData.CreatedUsers.id',
            valueField: '_matchingData.CreatedUsers.username',
            groupField: '_matchingData.OrgEntities.name',
        )
            ->select(
                ['CreatedUsers.id', 'CreatedUsers.username', 'OrgEntities.name']
            )
            ->innerJoinWith('CreatedUsers')
            ->innerJoinWith('CreatedUsers.OrgEntities')
            ->distinct(
                $distinct = [
                    'OrgEntities.name',
                    'CreatedUsers.username',
                ]
            )
            ->where(
                ['DeliveryRequests.archival_agency_id' => $this->archivalAgencyId]
            )
            ->orderBy($distinct);
        $this->set('created_users', $created_users);
    }

    /**
     * Index de la validation des demandes de transfert sortant
     * @throws Exception
     */
    public function outgoingTransferRequests()
    {
        /** @var IndexComponent $IndexComponent */
        $IndexComponent = $this->loadComponent('AsalaeCore.Index');
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'OutgoingTransferRequests',
            true
        )
            ->contain(
                [
                    'OutgoingTransferRequests' => [
                        'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                            return $q
                                ->orderBy(
                                    ['ArchiveUnits.archival_agency_identifier']
                                )
                                ->limit(
                                    $AjaxPaginatorComponent->getConfig('limit')
                                );
                        },
                        'CreatedUsers' => [
                            'OrgEntities',
                        ],
                    ],
                ]
            );

        /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
        $OutgoingTransferRequests = $this->fetchTable('OutgoingTransferRequests');
        $subquery = $OutgoingTransferRequests->selectQuery()
            ->select(['dr.id'])
            ->from(['dr' => 'outgoing_transfer_requests'])
            ->innerJoin(
                ['lk' => 'archive_units_outgoing_transfer_requests'],
                ['lk.otr_id' => new IdentifierExpression('dr.id')]
            )
            ->innerJoin(
                ['au' => 'archive_units'],
                ['au.id' => new IdentifierExpression('lk.archive_unit_id')]
            )
            ->where(
                [
                    'dr.id' => new IdentifierExpression(
                        'OutgoingTransferRequests.id'
                    ),
                ]
            )
            ->limit(1);
        $IndexComponent->setQuery($query)
            ->filter(
                'archive_unit_identifier',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike(
                            'au.archival_agency_identifier',
                            $v
                        )
                    );
                    return $q->andWhere(
                        ['OutgoingTransferRequests.id IN' => $sq]
                    );
                }
            )
            ->filter(
                'archive_unit_name',
                function ($v, Query $q) use ($subquery) {
                    $sq = (clone $subquery)->andWhere(
                        $this->Condition->ilike('au.name', $v)
                    );
                    return $q->andWhere(
                        ['OutgoingTransferRequests.id IN' => $sq]
                    );
                }
            )
            ->filter(
                'created_user',
                IndexComponent::FILTER_IN,
                'OutgoingTransferRequests.created_user_id'
            )
            ->filter(
                'outgoing_transfer_request_created',
                IndexComponent::FILTER_DATEOPERATOR,
                'OutgoingTransferRequests.created'
            )
            ->filter(
                'outgoing_transfer_request_sent',
                IndexComponent::FILTER_DATEOPERATOR,
                'OutgoingTransferRequests.last_state_update'
            )
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter(
                'identifier',
                IndexComponent::FILTER_ILIKE,
                'OutgoingTransferRequests.identifier'
            )
            ->filter(
                'validation_stage_id',
                IndexComponent::FILTER_IN,
                'CurrentStages.id'
            )
            ->filter('validation_chain_id', IndexComponent::FILTER_IN);
        $IndexComponent->setQuery($query);

        if ($this->getRequest()->getQuery('export_csv')) {
            $this->viewBuilder()->setTemplate('csv/outgoing_transfer_requests');
        }
        if ($csv = $IndexComponent->exportCsv()) {
            return $csv;
        }

        $data = $this->paginateToResultSet($query)->toArray();
        $this->set('data', $data);

        //options
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        $stages = $ValidationStages->find(
            'list',
            keyField: 'id',
            valueField: 'name',
            groupField: '_matchingData.ValidationChains.name',
        )
            ->select(
                [
                    'ValidationStages.name',
                    'ValidationStages.id',
                    'ValidationChains.name',
                ]
            )
            ->innerJoinWith('ValidationChains')
            ->innerJoinWith(
                'ValidationProcesses'
            ) // filtre current_stage_id != null
            ->innerJoinWith('ValidationActors')
            ->where(
                [
                    'ValidationActors.app_type' => 'USER',
                    'ValidationActors.app_foreign_key' => $this->userId,
                    'ValidationChains.app_type' => ValidationChainsTable::ARCHIVE_OUTGOING_TRANSFER_REQUEST,
                    'ValidationChains.org_entity_id' => $this->orgEntityId,
                ]
            )
            ->orderBy(['ValidationChains.name' => 'asc', 'ValidationStages.ord']);
        $this->set('stages', $stages);

        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        $created_users = $DeliveryRequests->find(
            'list',
            keyField: '_matchingData.CreatedUsers.id',
            valueField: '_matchingData.CreatedUsers.username',
            groupField: '_matchingData.OrgEntities.name',
        )
            ->select(
                ['CreatedUsers.id', 'CreatedUsers.username', 'OrgEntities.name']
            )
            ->innerJoinWith('CreatedUsers')
            ->innerJoinWith('CreatedUsers.OrgEntities')
            ->distinct(
                $distinct = [
                    'OrgEntities.name',
                    'CreatedUsers.username',
                ]
            )
            ->where(
                ['DeliveryRequests.archival_agency_id' => $this->archivalAgencyId]
            )
            ->orderBy($distinct);
        $this->set('created_users', $created_users);
    }

    /**
     * Validation d'une demande d'élimination par email
     * @param string $process_id
     * @param string $actor_id
     * @throws Exception
     */
    public function processDestructionRequestMail(
        string $process_id,
        string $actor_id
    ) {
        $request = $this->getRequest();
        $this->viewBuilder()->setLayout('not_connected');
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $this->fetchTable('AuthUrls');
        if (!$AuthUrls->isAuthorized($request)) {
            $this->set('notAuthorized', true);
            return;
        }

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $process = $ValidationProcesses->find()
            ->innerJoinWith('CurrentStages')
            ->innerJoinWith('CurrentStages.ValidationActors')
            ->where(
                [
                    'ValidationProcesses.id' => $process_id,
                    'ValidationActors.id' => $actor_id,
                ]
            )
            ->contain(['CurrentStages' => ['ValidationActors']])
            ->firstOrFail();
        if ($process->get('current_stage') === null) {
            $this->set('isOutdated', true);
            $this->set('done', false);
        } else {
            $this->set('isOutdated', false);
            $this->set('process', $process);
            $this->processDestructionRequest($process_id, (int)$actor_id);
            $success = $this->getResponse()->getHeader('X-Asalae-Success');
            $success = $success ? (current($success) === 'true') : null;
            $done = false;
            if ($success) {
                $AuthUrls->consume($request);
                $this->Flash->success(
                    __("Votre décision a bien été prise en compte.")
                );
                $done = true;
            } elseif ($success === false) {
                $this->Flash->success(__("Une erreur a eu lieu."));
            }
            $this->set('done', $done);
        }
    }

    /**
     * Validation d'une demande d'élimination
     * @param string   $id
     * @param int|null $actor_id
     * @return Response
     * @throws Exception
     */
    public function processDestructionRequest(string $id, int $actor_id = null)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $request = $this->getRequest();
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'DestructionRequests',
            false,
            $actor_id
        )
            ->where(['ValidationProcesses.id' => $id])
            ->contain(
                [
                    'DestructionRequests' => [
                        'ArchivalAgencies',
                        'OriginatingAgencies',
                        'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                            return $q
                                ->orderBy(
                                    ['ArchiveUnits.archival_agency_identifier']
                                )
                                ->limit(
                                    $AjaxPaginatorComponent->getConfig('limit')
                                )
                                ->contain(
                                    [
                                        'Archives' => ['DescriptionXmlArchiveFiles'],
                                        'ArchiveDescriptions',
                                    ]
                                );
                        },
                        'CreatedUsers',
                        'ValidationProcesses' => [
                            'ValidationChains',
                            'CurrentStages' => [
                                'ValidationActors',
                            ],
                            'ValidationHistories' => [
                                'sort' => [
                                    'ValidationHistories.created' => 'asc',
                                    'ValidationHistories.id' => 'asc',
                                ],
                                'ValidationActors' => [
                                    'Users',
                                    'ValidationStages',
                                ],
                            ],
                        ],
                    ],
                ]
            );
        $process = $query->firstOrFail();
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        /** @var EntityInterface $destructionRequest */
        $destructionRequest = $process->get('destruction_request');
        $this->set('destructionRequest', $destructionRequest);
        /** @var ArchiveUnitsDestructionRequestsTable $ArchiveUnitsDestructionRequests */
        $ArchiveUnitsDestructionRequests = $this->fetchTable('ArchiveUnitsDestructionRequests');
        $unitsCount = $ArchiveUnitsDestructionRequests->find()
            ->where(['destruction_request_id' => $destructionRequest->id])
            ->count();
        $this->set('unitsCount', $unitsCount);
        $prevs = $ValidationStages->find('list')
            ->where(
                [
                    'validation_chain_id' => $process->get(
                        'validation_chain_id'
                    ),
                    'ord <' => Hash::get($process, 'current_stage.ord', 0),
                ]
            )
            ->orderBy(['ord'])
            ->toArray();
        $this->set('prevs', $prevs);
        $prev = !empty($prevs);
        $next = $ValidationStages->exists(
            [
                'validation_chain_id' => $process->get('validation_chain_id'),
                'ord >' => $process->get('current_stage')->get('ord'),
            ]
        );
        $actor = $process->get('processable');
        if (!$actor) {
            throw new NotFoundException('actor not found');
        }
        $this->set('actor', $actor);
        $typeValidation = Hash::get($actor, 'type_validation');
        if (
            $typeValidation === 'S'
            || (!$typeValidation && Hash::get($this->user, 'use_cert'))
        ) {
            $hash = hash_file(
                'sha256',
                Hash::get($process, 'destruction_request.xml')
            );
            $this->set('hash', $hash);
        }
        /** @var ValidationHistoriesTable $ValidationHistories */
        $ValidationHistories = $this->fetchTable('ValidationHistories');
        /** @var ValidationHistory $history */
        $history = $ValidationHistories->newEntity(
            [],
            ['validate' => false]
        );

        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        // Si au moins une archive freezed, validate n'est pas possible
        $freezedArchivesQuery = $Archives->getFreezedArchivesDestruction($destructionRequest)
            ->orderBy(['Archives.id']);
        $freezedArchivesCount = $freezedArchivesQuery->count();
        $freezedArchives = $freezedArchivesQuery->limit($AjaxPaginatorComponent->getConfig('limit'))
            ->all()
            ->toArray();
        $availableActions = $ValidationHistories->optionsAction($prev, !$freezedArchivesCount);

        if ($request->is('post')) {
            if (!isset($availableActions[$request->getData('action')])) {
                throw new ForbiddenException('Action not available');
            }

            $data = [
                'validation_process_id' => $process->get('id'),
                'validation_actor_id' => $actor->get('id'),
                'current_step' => $process->get('current_step'),
                'action' => $request->getData('action'),
                'comment' => $request->getData('comment'),
                'prev' => $prev,
                'next' => $next,
                'created_user_id' => $this->userId,
                'type_validation' => $actor->get('type_validation'),
                'signature' => $request->getData(
                    'signature.' . $destructionRequest->id
                ),
            ];
            if ($data['action'] === 'stepback') {
                $prevStage = $ValidationStages->find()->select(['id'])
                    ->where(
                        [
                            'id' => $request->getData('prev_stage_id', 0),
                            'validation_chain_id' => $process->get(
                                'validation_chain_id'
                            ),
                        ]
                    )->firstOrFail();
                $data['stepback'] = $prevStage->get('id');
            }
            $ValidationHistories->patchEntity($history, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $process = $ValidationProcesses->proceed(
                $data['action'],
                $history
            );
            $conn = $ValidationProcesses->getConnection();
            $conn->begin();
            $success = $ValidationProcesses->save($process)
                && $ValidationHistories->save($history);
            if ($success && $process->get('current_stage_id') === null) {
                /** @var DestructionRequestsTable $DestructionRequests */
                $DestructionRequests = $this->fetchTable('DestructionRequests');
                $transition = $process->get('validated')
                    ? DestructionRequestsTable::T_ACCEPT
                    : DestructionRequestsTable::T_REFUSE;
                $DestructionRequests->transitionOrFail(
                    $destructionRequest,
                    $transition
                );
                if ($DestructionRequests->save($destructionRequest)) {
                    $this->Modal->success();
                    /** @var ArchivesTable $Archives */
                    $Archives = $this->fetchTable('Archives');
                    /** @var EventLoggerComponent $EventLogger */
                    $EventLogger = $this->loadComponent('EventLogger');
                    if ($transition === 'accept') {
                        $EventLogger->logValidationAccept($destructionRequest, 'DestructionRequests');
                        $query = $Archives->find()
                            ->select(['Archives.id'])
                            ->innerJoinWith('ArchiveUnits')
                            ->innerJoinWith('ArchiveUnits.DestructionRequests')
                            ->where(['DestructionRequests.id' => $destructionRequest->id]);
                        $Archives->transitionAll(
                            ArchivesTable::T_VALIDATE,
                            ['Archives.id IN' => $query]
                        );
                        $conn->commit();
                        /** @var Beanstalk $Beanstalk */
                        $Beanstalk = Utility::get('Beanstalk');
                        $Beanstalk->setTube('destruction');
                        $Beanstalk->emit(
                            [
                                'user_id' => $this->userId,
                                'destruction_request_id' => $destructionRequest->id,
                            ],
                            Pheanstalk::DEFAULT_PRIORITY,
                            Pheanstalk::DEFAULT_DELAY,
                            3600 // on part du principe qu'un fichier peut mettre 1h à se télécharger
                        );
                    } else {
                        $EventLogger->logValidationReject($destructionRequest, 'DestructionRequests');
                        /** @var ArchiveUnitsTable $ArchiveUnits */
                        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
                        $query = $ArchiveUnits->find()
                            ->innerJoinWith('DestructionRequests')
                            ->where(
                                ['DestructionRequests.id' => $destructionRequest->id]
                            )
                            ->disableHydration();
                        foreach ($query as $archiveUnit) {
                            $ArchiveUnits->transitionAll(
                                ArchiveUnitsTable::T_CANCEL,
                                [
                                    'lft >=' => $archiveUnit['lft'],
                                    'rght <=' => $archiveUnit['rght'],
                                ]
                            );
                            $Archives->transitionAll(
                                ArchivesTable::T_CANCEL,
                                ['id' => $archiveUnit['archive_id']]
                            );
                        }
                        $conn->commit();
                    }
                    return $this->renderJson(json_encode($history->toArray()));
                } else {
                    $success = false;
                    FormatError::logEntityErrors($destructionRequest);
                }
            } elseif ($success) {
                $ValidationProcesses->notifyProcess($process);
            }
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                return $this->renderJson(json_encode($history->toArray()));
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($process);
                FormatError::logEntityErrors($history);
            }
        }

        $this->set('entity', $history);
        $this->set('process', $process);
        $this->set('actions', $availableActions);
        $this->set('freezedArchives', $freezedArchives);
        $this->set('freezedArchivesCount', $freezedArchivesCount);
        $AjaxPaginatorComponent->setViewPaginator(
            $destructionRequest->get('archive_units') ?: [],
            $unitsCount,
        );
    }

    /**
     * Pagination ajax pour la liste des archives gelées d'une demande de destruction
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function viewFreezedArchivesDestruction(string $id): ResponseInterface
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'DestructionRequests'
        )
            ->where(['ValidationProcesses.id' => $id])
            ->contain(['DestructionRequests'])
            ->firstOrFail();

        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $freezedArchivesQuery = $Archives->getFreezedArchivesDestruction($query->get('destruction_request'));

        return $AjaxPaginatorComponent->json(
            $freezedArchivesQuery,
            (bool)$this->getRequest()->getQuery('count')
        );
    }

    /**
     * Validation d'une demande de restitution par email
     * @param string $process_id
     * @param string $actor_id
     * @throws Exception
     */
    public function processRestitutionRequestMail(
        string $process_id,
        string $actor_id
    ) {
        $request = $this->getRequest();
        $this->viewBuilder()->setLayout('not_connected');
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $this->fetchTable('AuthUrls');
        if (!$AuthUrls->isAuthorized($request)) {
            $this->set('notAuthorized', true);
            return;
        }

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $process = $ValidationProcesses->find()
            ->innerJoinWith('CurrentStages')
            ->innerJoinWith('CurrentStages.ValidationActors')
            ->where(
                [
                    'ValidationProcesses.id' => $process_id,
                    'ValidationActors.id' => $actor_id,
                ]
            )
            ->contain(['CurrentStages' => ['ValidationActors']])
            ->firstOrFail();
        if ($process->get('current_stage') === null) {
            $this->set('isOutdated', true);
            $this->set('done', false);
        } else {
            $this->set('isOutdated', false);
            $this->set('process', $process);
            $this->processRestitutionRequest($process_id, (int)$actor_id);
            $success = $this->getResponse()->getHeader('X-Asalae-Success');
            $success = $success ? (current($success) === 'true') : null;
            $done = false;
            if ($success) {
                $AuthUrls->consume($request);
                $this->Flash->success(
                    __("Votre décision a bien été prise en compte.")
                );
                $done = true;
            } elseif ($success === false) {
                $this->Flash->success(__("Une erreur a eu lieu."));
            }
            $this->set('done', $done);
        }
    }

    /**
     * Validation d'une demande de restitution
     * @param string   $id
     * @param int|null $actor_id
     * @return Response
     * @throws Exception
     */
    public function processRestitutionRequest(string $id, int $actor_id = null)
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $request = $this->getRequest();
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'RestitutionRequests',
            false,
            $actor_id
        )
            ->where(['ValidationProcesses.id' => $id])
            ->contain(
                [
                    'RestitutionRequests' => [
                        'ArchivalAgencies',
                        'OriginatingAgencies',
                        'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                            return $q
                                ->orderBy(
                                    ['ArchiveUnits.archival_agency_identifier']
                                )
                                ->limit(
                                    $AjaxPaginatorComponent->getConfig('limit')
                                )
                                ->contain(
                                    [
                                        'Archives' => ['DescriptionXmlArchiveFiles'],
                                        'ArchiveDescriptions',
                                    ]
                                );
                        },
                        'CreatedUsers',
                        'ValidationProcesses' => [
                            'ValidationChains',
                            'CurrentStages' => [
                                'ValidationActors',
                            ],
                            'ValidationHistories' => [
                                'sort' => [
                                    'ValidationHistories.created' => 'asc',
                                    'ValidationHistories.id' => 'asc',
                                ],
                                'ValidationActors' => [
                                    'Users',
                                    'ValidationStages',
                                ],
                            ],
                        ],
                    ],
                ]
            );
        $process = $query->firstOrFail();
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        /** @var EntityInterface $restitutionRequest */
        $restitutionRequest = $process->get('restitution_request');
        $this->set('restitutionRequest', $restitutionRequest);
        /** @var ArchiveUnitsRestitutionRequestsTable $ArchiveUnitsRestitutionRequests */
        $ArchiveUnitsRestitutionRequests = $this->fetchTable('ArchiveUnitsRestitutionRequests');
        $unitsCount = $ArchiveUnitsRestitutionRequests->find()
            ->where(['restitution_request_id' => $restitutionRequest->id])
            ->count();
        $this->set('unitsCount', $unitsCount);
        $prevs = $ValidationStages->find('list')
            ->where(
                [
                    'validation_chain_id' => $process->get(
                        'validation_chain_id'
                    ),
                    'ord <' => Hash::get($process, 'current_stage.ord', 0),
                ]
            )
            ->orderBy(['ord'])
            ->toArray();
        $this->set('prevs', $prevs);
        $prev = !empty($prevs);
        $next = $ValidationStages->exists(
            [
                'validation_chain_id' => $process->get('validation_chain_id'),
                'ord >' => $process->get('current_stage')->get('ord'),
            ]
        );
        $actor = $process->get('processable');
        if (!$actor) {
            throw new NotFoundException('actor not found');
        }
        $this->set('actor', $actor);
        $typeValidation = Hash::get($actor, 'type_validation');
        if (
            $typeValidation === 'S'
            || (!$typeValidation && Hash::get($this->user, 'use_cert'))
        ) {
            $hash = hash_file(
                'sha256',
                Hash::get($process, 'restitution_request.xml')
            );
            $this->set('hash', $hash);
        }
        /** @var ValidationHistoriesTable $ValidationHistories */
        $ValidationHistories = $this->fetchTable('ValidationHistories');
        /** @var ValidationHistory $history */
        $history = $ValidationHistories->newEntity(
            [],
            ['validate' => false]
        );

        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        // Si au moins une archive freezed, validate n'est pas possible
        $freezedArchivesQuery = $Archives->getFreezedArchivesRestitution($restitutionRequest)
            ->orderBy(['Archives.id']);
        $freezedArchivesCount = $freezedArchivesQuery->count();
        $freezedArchives = $freezedArchivesQuery->limit($AjaxPaginatorComponent->getConfig('limit'))
            ->all()
            ->toArray();
        $availableActions = $ValidationHistories->optionsAction($prev, !$freezedArchivesCount);

        if ($request->is('post')) {
            if (!isset($availableActions[$request->getData('action')])) {
                throw new ForbiddenException('Action not available');
            }

            $data = [
                'validation_process_id' => $process->get('id'),
                'validation_actor_id' => $actor->get('id'),
                'current_step' => $process->get('current_step'),
                'action' => $request->getData('action'),
                'comment' => $request->getData('comment'),
                'prev' => $prev,
                'next' => $next,
                'created_user_id' => $this->userId,
                'type_validation' => $actor->get('type_validation'),
                'signature' => $request->getData(
                    'signature.' . $restitutionRequest->id
                ),
            ];
            if ($data['action'] === 'stepback') {
                $prevStage = $ValidationStages->find()->select(['id'])
                    ->where(
                        [
                            'id' => $request->getData('prev_stage_id', 0),
                            'validation_chain_id' => $process->get(
                                'validation_chain_id'
                            ),
                        ]
                    )->firstOrFail();
                $data['stepback'] = $prevStage->get('id');
            }
            $ValidationHistories->patchEntity($history, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $process = $ValidationProcesses->proceed(
                $data['action'],
                $history
            );
            $conn = $ValidationProcesses->getConnection();
            $conn->begin();
            $success = $ValidationProcesses->save($process)
                && $ValidationHistories->save($history);
            if ($success && $process->get('current_stage_id') === null) {
                /** @var RestitutionRequestsTable $RestitutionRequests */
                $RestitutionRequests = $this->fetchTable('RestitutionRequests');
                $transition = $process->get('validated')
                    ? RestitutionRequestsTable::T_ACCEPT
                    : RestitutionRequestsTable::T_REFUSE;
                $RestitutionRequests->transitionOrFail(
                    $restitutionRequest,
                    $transition
                );
                if ($RestitutionRequests->save($restitutionRequest)) {
                    $this->Modal->success();
                    /** @var ArchivesTable $Archives */
                    $Archives = $this->fetchTable('Archives');
                    if ($transition === 'accept') {
                        $query = $Archives->find()
                            ->select(['Archives.id'])
                            ->innerJoinWith('ArchiveUnits')
                            ->innerJoinWith('ArchiveUnits.RestitutionRequests')
                            ->where(['RestitutionRequests.id' => $restitutionRequest->id]);
                        $Archives->transitionAll(
                            ArchivesTable::T_VALIDATE,
                            ['Archives.id IN' => $query]
                        );
                        /** @var EventLoggerComponent $EventLogger */
                        $EventLogger = $this->loadComponent('EventLogger');
                        $EventLogger->logValidationAccept($restitutionRequest, 'RestitutionRequests');
                        $conn->commit();
                        /** @var Beanstalk $Beanstalk */
                        $Beanstalk = Utility::get('Beanstalk');
                        $Beanstalk->setTube('restitution-build');
                        $Beanstalk->emit(
                            [
                                'user_id' => $this->userId,
                                'restitution_request_id' => $restitutionRequest->id,
                            ],
                            Pheanstalk::DEFAULT_PRIORITY,
                            Pheanstalk::DEFAULT_DELAY,
                            3600 // on part du principe qu'un fichier peut mettre 1h à se télécharger
                        );
                    } else {
                        /** @var ArchiveUnitsTable $ArchiveUnits */
                        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
                        $query = $ArchiveUnits->find()
                            ->innerJoinWith('RestitutionRequests')
                            ->where(
                                ['RestitutionRequests.id' => $restitutionRequest->id]
                            )
                            ->disableHydration();
                        foreach ($query as $archiveUnit) {
                            $ArchiveUnits->transitionAll(
                                ArchiveUnitsTable::T_CANCEL,
                                [
                                    'lft >=' => $archiveUnit['lft'],
                                    'rght <=' => $archiveUnit['rght'],
                                ]
                            );
                            $Archives->transitionAll(
                                ArchivesTable::T_CANCEL,
                                ['id' => $archiveUnit['archive_id']]
                            );
                        }
                        /** @var EventLoggerComponent $EventLogger */
                        $EventLogger = $this->loadComponent('EventLogger');
                        $EventLogger->logValidationReject($restitutionRequest, 'RestitutionRequests');
                        $conn->commit();
                    }
                    return $this->renderJson(json_encode($history->toArray()));
                } else {
                    $success = false;
                    FormatError::logEntityErrors($restitutionRequest);
                }
            } elseif ($success) {
                $ValidationProcesses->notifyProcess($process);
            }
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                return $this->renderJson(json_encode($history->toArray()));
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($process);
                FormatError::logEntityErrors($history);
            }
        }

        $this->set('entity', $history);
        $this->set('process', $process);
        $this->set('actions', $availableActions);
        $this->set('freezedArchives', $freezedArchives);
        $this->set('freezedArchivesCount', $freezedArchivesCount);
        $AjaxPaginatorComponent->setViewPaginator(
            $restitutionRequest->get('archive_units') ?: [],
            $unitsCount,
        );
    }

    /**
     * Pagination ajax pour la liste des archives gelées d'une demande de restitution
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function viewFreezedArchivesRestitution(string $id): ResponseInterface
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'RestitutionRequests'
        )
            ->where(['ValidationProcesses.id' => $id])
            ->contain(['RestitutionRequests'])
            ->firstOrFail();

        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $freezedArchivesQuery = $Archives->getFreezedArchivesRestitution($query->get('restitution_request'));

        return $AjaxPaginatorComponent->json(
            $freezedArchivesQuery,
            (bool)$this->getRequest()->getQuery('count')
        );
    }

    /**
     * Validation d'une demande de outgoingTransfer par email
     * @param string $process_id
     * @param string $actor_id
     * @throws Exception
     */
    public function processOutgoingTransferRequestMail(
        string $process_id,
        string $actor_id
    ) {
        $request = $this->getRequest();
        $this->viewBuilder()->setLayout('not_connected');
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $this->fetchTable('AuthUrls');
        if (!$AuthUrls->isAuthorized($request)) {
            $this->set('notAuthorized', true);
            return;
        }

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $process = $ValidationProcesses->find()
            ->innerJoinWith('CurrentStages')
            ->innerJoinWith('CurrentStages.ValidationActors')
            ->where(
                [
                    'ValidationProcesses.id' => $process_id,
                    'ValidationActors.id' => $actor_id,
                ]
            )
            ->contain(['CurrentStages' => ['ValidationActors']])
            ->firstOrFail();
        if ($process->get('current_stage') === null) {
            $this->set('isOutdated', true);
            $this->set('done', false);
        } else {
            $this->set('isOutdated', false);
            $this->set('process', $process);
            $this->processOutgoingTransferRequest($process_id, (int)$actor_id);
            $success = $this->getResponse()->getHeader('X-Asalae-Success');
            $success = $success ? (current($success) === 'true') : null;
            $done = false;
            if ($success) {
                $AuthUrls->consume($request);
                $this->Flash->success(
                    __("Votre décision a bien été prise en compte.")
                );
                $done = true;
            } elseif ($success === false) {
                $this->Flash->success(__("Une erreur a eu lieu."));
            }
            $this->set('done', $done);
        }
    }

    /**
     * Validation d'une demande de transfert sortant
     * @param string   $id
     * @param int|null $actor_id
     * @return Response
     * @throws Exception
     */
    public function processOutgoingTransferRequest(
        string $id,
        int $actor_id = null
    ) {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');
        $request = $this->getRequest();
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'OutgoingTransferRequests',
            false,
            $actor_id
        )
            ->where(['ValidationProcesses.id' => $id])
            ->contain(
                [
                    'OutgoingTransferRequests' => [
                        'ArchivalAgencies',
                        'ArchiveUnits' => function (Query $q) use ($AjaxPaginatorComponent) {
                            return $q
                                ->orderBy(
                                    ['ArchiveUnits.archival_agency_identifier']
                                )
                                ->limit(
                                    $AjaxPaginatorComponent->getConfig('limit')
                                )
                                ->contain(
                                    [
                                        'Archives' => ['DescriptionXmlArchiveFiles'],
                                        'ArchiveDescriptions',
                                    ]
                                );
                        },
                        'ArchivingSystems',
                        'CreatedUsers',
                        'ValidationProcesses' => [
                            'ValidationChains',
                            'CurrentStages' => [
                                'ValidationActors',
                            ],
                            'ValidationHistories' => [
                                'sort' => [
                                    'ValidationHistories.created' => 'asc',
                                    'ValidationHistories.id' => 'asc',
                                ],
                                'ValidationActors' => [
                                    'Users',
                                    'ValidationStages',
                                ],
                            ],
                        ],
                    ],
                ]
            );
        $process = $query->firstOrFail();
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $this->fetchTable('ValidationStages');
        /** @var EntityInterface $outgoingTransferRequest */
        $outgoingTransferRequest = $process->get('outgoing_transfer_request');
        $this->set('outgoingTransferRequest', $outgoingTransferRequest);
        /** @var ArchiveUnitsOutgoingTransferRequestsTable $ArchiveUnitsOutgoingTransferRequests */
        $ArchiveUnitsOutgoingTransferRequests = $this->fetchTable('ArchiveUnitsOutgoingTransferRequests');
        $unitsCount = $ArchiveUnitsOutgoingTransferRequests->find()
            ->where(['otr_id' => $outgoingTransferRequest->id])
            ->count();
        $this->set('unitsCount', $unitsCount);
        $prevs = $ValidationStages->find('list')
            ->where(
                [
                    'validation_chain_id' => $process->get(
                        'validation_chain_id'
                    ),
                    'ord <' => Hash::get($process, 'current_stage.ord', 0),
                ]
            )
            ->orderBy(['ord'])
            ->toArray();
        $this->set('prevs', $prevs);
        $prev = !empty($prevs);
        $next = $ValidationStages->exists(
            [
                'validation_chain_id' => $process->get('validation_chain_id'),
                'ord >' => $process->get('current_stage')->get('ord'),
            ]
        );
        $actor = $process->get('processable');
        if (!$actor) {
            throw new NotFoundException('actor not found');
        }
        $this->set('actor', $actor);
        $typeValidation = Hash::get($actor, 'type_validation');
        if (
            $typeValidation === 'S'
            || (!$typeValidation && Hash::get($this->user, 'use_cert'))
        ) {
            $hash = hash(
                'sha256',
                Hash::get($process, 'outgoing_transfer_request.identifier')
            );
            $this->set('hash', $hash);
        }
        /** @var ValidationHistoriesTable $ValidationHistories */
        $ValidationHistories = $this->fetchTable('ValidationHistories');
        /** @var ValidationHistory $history */
        $history = $ValidationHistories->newEntity(
            [],
            ['validate' => false]
        );

        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        // Si au moins une archive freezed, validate n'est pas possible
        $freezedArchivesQuery = $Archives->getFreezedArchivesOutgoingTransfer($outgoingTransferRequest)
            ->orderBy(['Archives.id']);
        $freezedArchivesCount = $freezedArchivesQuery->count();
        $freezedArchives = $freezedArchivesQuery->limit($AjaxPaginatorComponent->getConfig('limit'))
            ->all()
            ->toArray();
        $availableActions = $ValidationHistories->optionsAction($prev, !$freezedArchivesCount);

        if ($request->is('post')) {
            if (!isset($availableActions[$request->getData('action')])) {
                throw new ForbiddenException('Action not available');
            }

            $data = [
                'validation_process_id' => $process->get('id'),
                'validation_actor_id' => $actor->get('id'),
                'current_step' => $process->get('current_step'),
                'action' => $request->getData('action'),
                'comment' => $request->getData('comment'),
                'prev' => $prev,
                'next' => $next,
                'created_user_id' => $this->userId,
                'type_validation' => $actor->get('type_validation'),
                'signature' => $request->getData(
                    'signature.' . $outgoingTransferRequest->id
                ),
            ];
            if ($data['action'] === 'stepback') {
                $prevStage = $ValidationStages->find()->select(['id'])
                    ->where(
                        [
                            'id' => $request->getData('prev_stage_id', 0),
                            'validation_chain_id' => $process->get(
                                'validation_chain_id'
                            ),
                        ]
                    )->firstOrFail();
                $data['stepback'] = $prevStage->get('id');
            }
            $ValidationHistories->patchEntity($history, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            $process = $ValidationProcesses->proceed(
                $data['action'],
                $history
            );
            $conn = $ValidationProcesses->getConnection();
            $conn->begin();
            $success = $ValidationProcesses->save($process)
                && $ValidationHistories->save($history);
            if ($success && $process->get('current_stage_id') === null) {
                /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
                $OutgoingTransferRequests = $this->fetchTable('OutgoingTransferRequests');
                $transition = $process->get('validated')
                    ? OutgoingTransferRequestsTable::T_ACCEPT
                    : OutgoingTransferRequestsTable::T_REFUSE;
                $OutgoingTransferRequests->transitionOrFail(
                    $outgoingTransferRequest,
                    $transition
                );
                if (
                    $OutgoingTransferRequests->save(
                        $outgoingTransferRequest
                    )
                ) {
                    $this->Modal->success();
                    /** @var ArchivesTable $Archives */
                    $Archives = $this->fetchTable('Archives');
                    if ($transition === 'accept') {
                        $query = $Archives->find()
                            ->select(['Archives.id'])
                            ->innerJoinWith('ArchiveUnits')
                            ->innerJoinWith('ArchiveUnits.OutgoingTransferRequests')
                            ->where(['OutgoingTransferRequests.id' => $outgoingTransferRequest->id]);
                        $Archives->transitionAll(
                            ArchivesTable::T_VALIDATE,
                            ['Archives.id IN' => $query]
                        );
                        /** @var EventLoggerComponent $EventLogger */
                        $EventLogger = $this->loadComponent('EventLogger');
                        $EventLogger->logValidationAccept($outgoingTransferRequest, 'OutgoingTransferRequests');
                        $conn->commit();
                        /** @var Beanstalk $Beanstalk */
                        $Beanstalk = Utility::get('Beanstalk');
                        $Beanstalk->setTube('outgoing-transfer');
                        $Beanstalk->emit(
                            [
                                'user_id' => $this->userId,
                                'outgoing_transfer_request_id' => $outgoingTransferRequest->id,
                            ],
                            Pheanstalk::DEFAULT_PRIORITY,
                            Pheanstalk::DEFAULT_DELAY,
                            3600 // on part du principe qu'un fichier peut mettre 1h à se télécharger
                        );
                    } else {
                        /** @var ArchiveUnitsTable $ArchiveUnits */
                        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
                        $query = $ArchiveUnits->find()
                            ->innerJoinWith('OutgoingTransferRequests')
                            ->where(
                                ['OutgoingTransferRequests.id' => $outgoingTransferRequest->id]
                            )
                            ->disableHydration();
                        foreach ($query as $archiveUnit) {
                            $ArchiveUnits->transitionAll(
                                ArchiveUnitsTable::T_CANCEL,
                                [
                                    'lft >=' => $archiveUnit['lft'],
                                    'rght <=' => $archiveUnit['rght'],
                                ]
                            );
                            $Archives->transitionAll(
                                ArchivesTable::T_CANCEL,
                                ['id' => $archiveUnit['archive_id']]
                            );
                        }
                        /** @var EventLoggerComponent $EventLogger */
                        $EventLogger = $this->loadComponent('EventLogger');
                        $EventLogger->logValidationReject($outgoingTransferRequest, 'OutgoingTransferRequests');
                        $conn->commit();
                    }
                    return $this->renderJson(json_encode($history->toArray()));
                } else {
                    $success = false;
                    FormatError::logEntityErrors($outgoingTransferRequest);
                }
            } elseif ($success) {
                $ValidationProcesses->notifyProcess($process);
            }
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                return $this->renderJson(json_encode($history->toArray()));
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($process);
                FormatError::logEntityErrors($history);
            }
        }

        $this->set('entity', $history);
        $this->set('process', $process);
        $this->set('actions', $availableActions);
        $this->set('freezedArchives', $freezedArchives);
        $this->set('freezedArchivesCount', $freezedArchivesCount);
        $AjaxPaginatorComponent->setViewPaginator(
            $outgoingTransferRequest->get('archive_units') ?: [],
            $unitsCount,
        );
    }

    /**
     * Pagination ajax pour la liste des archives gelées d'une demande de transfert sortant
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function viewFreezedArchivesOutgoingTransfer(string $id): ResponseInterface
    {
        /** @var AjaxPaginatorComponent $AjaxPaginatorComponent  */
        $AjaxPaginatorComponent = $this->loadComponent('AsalaeCore.AjaxPaginator');

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $query = $ValidationProcesses->findByValidations(
            $this->archivalAgencyId,
            $this->user,
            'OutgoingTransferRequests'
        )
            ->where(['ValidationProcesses.id' => $id])
            ->contain(['OutgoingTransferRequests'])
            ->firstOrFail();

        /** @var ArchivesTable $Archives */
        $Archives = $this->fetchTable('Archives');
        $freezedArchivesQuery = $Archives->getFreezedArchivesOutgoingTransfer(
            $query->get('outgoing_transfer_request')
        );

        return $AjaxPaginatorComponent->json(
            $freezedArchivesQuery,
            (bool)$this->getRequest()->getQuery('count')
        );
    }
}
