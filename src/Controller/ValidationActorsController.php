<?php

/**
 * Asalae\Controller\ValidationActorsController
 */

namespace Asalae\Controller;

use Asalae\Model\Entity\ValidationActor;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\RolesTable;
use Asalae\Model\Table\UsersTable;
use Asalae\Model\Table\ValidationActorsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationStagesTable;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use Cake\Datasource\EntityInterface;
use Cake\Http\Response;
use Cake\Utility\Hash;
use Exception;

/**
 * Acteurs d'une l'étape du circuit de validation
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property OrgEntitiesTable      OrgEntities
 * @property ValidationActorsTable ValidationActors
 * @property ValidationChainsTable ValidationChains
 * @property ValidationStagesTable ValidationStages
 */
class ValidationActorsController extends AppController implements ApiInterface
{
    /**
     * Traits
     */
    use ApiTrait;
    use RenderDataTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const string TABLE_INDEX = 'validation-actors-table';

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default' => [
                'post' => [
                    'function' => 'apiAdd',
                ],
            ],
        ];
    }

    /**
     * Liste des acteurs d'une étape de circuit de validation
     * @param string $validation_stage_id
     * @throws Exception
     */
    public function index(string $validation_stage_id)
    {
        /** @use IndexComponent */
        $this->loadComponent('AsalaeCore.Index');

        $stage = $this->getStage($validation_stage_id);
        $this->set('stage', $stage);
        $this->set('chain', $stage->get('validation_chain'));

        /** @var ValidationActorsTable $ValidationActors */
        $ValidationActors = $this->fetchTable('ValidationActors');
        $query = $ValidationActors->find()
            ->where(['validation_stage_id' => $validation_stage_id])
            ->contain(['Users'])
            ->orderBy(['ValidationActors.id' => 'asc']);
        $data = $this->paginateToResultSet($query);
        $this->set('data', $data);
        $this->set(
            'type_validations',
            $ValidationActors->options('type_validation')
        );
    }

    /**
     * Envoi un 404 si le validation_stage_id n'existe pas ou ne fait pas parti
     * du même service d'archive.
     *
     * @param string|int $validation_stage_id
     * @return EntityInterface
     * @throws Exception
     */
    private function getStage($validation_stage_id): EntityInterface
    {
        return $this->Seal->archivalAgency()
            ->table('ValidationStages')
            ->find('seal')
            ->where(['ValidationStages.id' => $validation_stage_id])
            ->contain(['ValidationChains'])
            ->firstOrFail();
    }

    /**
     * Ajout d'un acteur à une étape de circuit de validation
     * @param string $validation_stage_id
     * @return Response|null
     * @throws Exception
     */
    public function add(string $validation_stage_id)
    {
        /** @var ValidationActorsTable $ValidationActors */
        $ValidationActors = $this->fetchTable('ValidationActors');
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');

        $stage = $this->getStage($validation_stage_id);
        $this->set('stage', $stage);
        $entity = $ValidationActors->newEntity([], ['validate' => false]);

        if ($this->getRequest()->is('post')) {
            $user = null;
            $type = $this->getRequest()->getData('app_type');
            $data = [
                'validation_stage_id' => $validation_stage_id,
                'validation_chain_id' => $stage->get('validation_chain_id'),
                'app_type' => $type,
                'created_user_id' => $this->userId,
            ];
            switch ($type) {
                case 'USER':
                    $userSelected = $this->Seal->archivalAgencyChilds()
                        ->table('Users')
                        ->find('seal')
                        ->innerJoinWith('OrgEntities')
                        ->where(
                            [
                                'Users.id' => $this->getRequest()->getData('app_foreign_key', 0),
                                'Users.active IS' => true,
                            ]
                        )
                        ->first();

                    if (!$userSelected) {
                        $Users->find()
                            ->innerJoinWith('Roles')
                            ->innerJoinWith('ArchivalAgencies')
                            ->where(
                                [
                                    'Users.id' => $this->getRequest()->getData('app_foreign_key', 0),
                                    'Roles.code' => RolesTable::CODE_SUPER_ARCHIVIST,
                                    'ArchivalAgencies.id' => $this->archivalAgencyId,
                                ]
                            )
                            ->firstOrFail();
                    }

                    $data['app_foreign_key'] = $this->getRequest()->getData('app_foreign_key');
                    $data['type_validation'] = $this->getRequest()->getData('type_validation');
                    $user = $data['app_foreign_key']
                        ? $Users->get($data['app_foreign_key'])
                        : null;
                    break;
                case 'MAIL':
                    $data['app_key'] = $this->getRequest()->getData('app_key');
                    $data['actor_name'] = $this->getRequest()->getData(
                        'actor_name'
                    );
                    break;
            }
            $ValidationActors->patchEntity($entity, $data);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($ValidationActors->save($entity)) {
                if ($data['app_type'] === 'USER') {
                    $entity->set('user', $user);
                }
                $this->Modal->success();
                $entity = $ValidationActors->find()
                    ->where(['ValidationActors.id' => $entity->get('id')])
                    ->contain(['ValidationStages', 'Users'])
                    ->firstOrFail();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);
        $availables = ValidationActor::MAP_TYPE_ACTOR[Hash::get($stage, 'validation_chain.app_type')];
        $types = array_filter(
            $ValidationActors->options('app_type'),
            fn ($type) => in_array($type, $availables),
            ARRAY_FILTER_USE_KEY
        );
        $this->set('app_types', $types);
        $this->set('type_validations', $ValidationActors->options('type_validation'));
        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');

        $aclFilter = function ($user) use ($stage) {
            return $this->Authorization->Acl->check(
                $user,
                UsersTable::getAcoPath(Hash::get($stage, 'validation_chain.app_type'))
            );
        };

        $query = $this->Seal->archivalAgencyChilds()
            ->table('Users')
            ->find('seal')
            ->innerJoinWith('OrgEntities')
            ->where(
                [
                    'Users.agent_type' => 'person',
                    'Users.active IS' => true,
                ]
            )
            ->andWhere(
                $this->Condition->notIn('Users.id', $stage->get('actors_id_list'))
            )
            ->orderBy(['username' => 'asc'])
            ->all()
            ->filter($aclFilter);
        $users = [];
        foreach ($query as $user) {
            $key = $user->id;
            $name = $user->get('name');
            $users[$key] = $user->get('username') . ($name ? ' - ' . $name : '');
        }

        $querySuperA = $Users->find()
            ->innerJoinWith('Roles')
            ->innerJoinWith('ArchivalAgencies')
            ->where(
                [
                    'Roles.code' => RolesTable::CODE_SUPER_ARCHIVIST,
                    'ArchivalAgencies.id' => $this->archivalAgencyId,
                ]
            )
            ->andWhere(
                $this->Condition->notIn('Users.id', $stage->get('actors_id_list'))
            )
            ->all()
            ->filter($aclFilter);
        foreach ($querySuperA as $user) {
            $key = $user->id;
            $name = $user->get('name');
            $users[$key] = $user->get('username') . ($name ? ' - ' . $name : '');
        }

        $this->set('userCount', count($users));
        $this->set('users', $users);
    }

    /**
     * Edition d'un acteur à une étape de circuit de validation
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        /** @var ValidationActorsTable $ValidationActors */
        $ValidationActors = $this->fetchTable('ValidationActors');
        /** @var UsersTable $Users */
        $Users = $this->fetchTable('Users');

        /** @var ValidationActor $entity */
        $entity = $ValidationActors->find()
            ->where(['ValidationActors.id' => $id])
            ->contain(
                [
                    'ValidationStages' => ['ValidationActors'],
                    'Users',
                ]
            )
            ->firstOrFail();
        $stage = $this->getStage($entity->get('validation_stage_id'));
        $this->set('stage', $stage);

        if ($this->getRequest()->is('put')) {
            $data = $entity->toArray();
            switch ($data['app_type']) {
                case 'USER':
                    $userSelected = $this->Seal->archivalAgencyChilds()
                        ->table('Users')
                        ->find('seal')
                        ->innerJoinWith('OrgEntities')
                        ->where(
                            [
                                [
                                    'Users.id' => $this->getRequest()->getData(
                                        'app_foreign_key'
                                    ),
                                    'Users.active IS' => true,
                                ],
                            ]
                        )
                        ->first();

                    if (!$userSelected) {
                        $Users->find()
                            ->innerJoinWith('Roles')
                            ->innerJoinWith('ArchivalAgencies')
                            ->where(
                                [
                                    'Users.id' => $this->getRequest()->getData('app_foreign_key', 0),
                                    'Roles.code' => RolesTable::CODE_SUPER_ARCHIVIST,
                                    'ArchivalAgencies.id' => $this->archivalAgencyId,
                                ]
                            )
                            ->firstOrFail();
                    }

                    $data['app_foreign_key'] = $this->getRequest()->getData(
                        'app_foreign_key'
                    );
                    $data['type_validation'] = $this->getRequest()->getData(
                        'type_validation',
                        ''
                    );
                    break;
                case 'MAIL':
                    $data['app_key'] = $this->getRequest()->getData('app_key');
                    $data['actor_name'] = $this->getRequest()->getData(
                        'actor_name'
                    );
                    break;
                default:
                    $data['app_foreign_key'] = null;
                    $data['type_validation'] = null;
            }
            $ValidationActors->patchEntity($entity, $data);
            $entity->appendModified($this->userId);
            /** @use ModalComponent */
            $this->loadComponent('AsalaeCore.Modal');
            if ($ValidationActors->save($entity)) {
                $this->Modal->success();
                $entity = $ValidationActors->find()
                    ->where(['ValidationActors.id' => $id])
                    ->contain(['ValidationStages', 'Users'])
                    ->firstOrFail();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);
        $this->set('app_types', $ValidationActors->options('app_type'));
        $this->set(
            'type_validations',
            $ValidationActors->options('type_validation')
        );

        /** @use ConditionComponent */
        $this->loadComponent('AsalaeCore.Condition');
        $usersAllreadyAssigned = array_diff(
            Hash::get($entity, 'validation_stage.actors_id_list', []),
            // in case we store something else than users.id here
            $entity->get('app_type') === 'USER' ? [$entity->get('app_foreign_key')] : []
        );

        $querySuperA = $Users->find()
            ->innerJoinWith('Roles')
            ->innerJoinWith('ArchivalAgencies')
            ->where(
                [
                    'Roles.code' => RolesTable::CODE_SUPER_ARCHIVIST,
                    'ArchivalAgencies.id' => $this->archivalAgencyId,
                ]
            )
            ->andWhere(
                $this->Condition->notIn('Users.id', $usersAllreadyAssigned)
            );

        $query = $this->Seal->archivalAgencyChilds()
            ->table('Users')
            ->find('seal')
            ->innerJoinWith('OrgEntities')
            ->where(
                [
                    'Users.agent_type' => 'person',
                    'Users.active IS' => true,
                ]
            )
            ->andWhere(
                $this->Condition->notIn('Users.id', $usersAllreadyAssigned)
            )
            ->union($querySuperA)
            ->all()
            ->sortBy('username')
            ->filter(
                function ($user) use ($stage) {
                    return $this->Authorization->Acl->check(
                        $user,
                        UsersTable::getAcoPath(Hash::get($stage, 'validation_chain.app_type'))
                    );
                }
            );

        $users = [];
        foreach ($query as $user) {
            $key = $user->id;
            $name = $user->get('name');
            $users[$key] = $user->get('username') . ($name ? ' - ' . $name : '');
        }

        $this->set('userCount', count($users));
        $this->set('users', $users);
    }

    /**
     * Suppression d'un acteur d'une étape de circuit de validation
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        /** @var ValidationActorsTable $ValidationActors */
        $ValidationActors = $this->fetchTable('ValidationActors');
        $entity = $ValidationActors->get($id);
        $this->getStage($entity->get('validation_stage_id'));

        $report = $ValidationActors->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Création via API
     */
    protected function apiAdd()
    {
        $request = $this->getRequest();
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->fetchTable('OrgEntities');
        $sa = $OrgEntities->find()
            ->innerJoinWith('ValidationChains')
            ->innerJoinWith('ValidationChains.ValidationStages')
            ->where(
                [
                    'ValidationStages.id' => $request->getData(
                        'validation_stage_id'
                    ),
                ]
            )
            ->firstOrFail();
        $type = $request->getData('app_type');
        $data = [
            'validation_stage_id' => $request->getData('validation_stage_id'),
            'app_type' => $type,
            'created_user_id' => $this->userId,
        ];
        switch ($type) {
            case 'USER':
                /** @var UsersTable $Users */
                $Users = $this->fetchTable('Users');
                $Users->find()
                    ->innerJoinWith('OrgEntities')
                    ->where(
                        [
                            'Users.id' => $request->getData(
                                'app_foreign_key',
                                0
                            ),
                            'Users.active IS' => true,
                            'OrgEntities.lft >=' => $sa->get('lft'),
                            'OrgEntities.rght <=' => $sa->get('rght'),
                        ]
                    )
                    ->firstOrFail();
                $data['app_foreign_key'] = $request->getData('app_foreign_key');
                $data['type_validation'] = $request->getData('type_validation');
                break;
            case 'MAIL':
                $data['app_key'] = $request->getData('app_key');
                $data['actor_name'] = $request->getData('actor_name');
                break;
        }
        /** @var ValidationActorsTable $ValidationActors */
        $ValidationActors = $this->fetchTable('ValidationActors');
        $entity = $ValidationActors->newEntity($data);
        if ($ValidationActors->save($entity)) {
            return $this->renderData(['success' => true, 'id' => $entity->id]);
        } else {
            return $this->renderData(
                ['success' => false, 'errors' => $entity->getErrors()]
            );
        }
    }
}
