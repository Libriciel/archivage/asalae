<?php

/**
 * Asalae\Form\InstallForm
 */

namespace Asalae\Form;

use Asalae\Model\Entity\Eaccpf;
use Asalae\Model\Table\EaccpfsTable;
use AsalaeCore\Utility\Config;
use AsalaeCore\Utility\Exec;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\ConnectionManager;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use PDOException;

/**
 * Formulaire d'installation
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class InstallForm extends Form
{
    public const string DEFAULT_TIMESTAMPER_TSA_EXTFILE = <<<eot
extensions = extend
[extend]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer
extendedKeyUsage=critical,timeStamping
eot;

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('local_config_file', ['type' => 'string']);
        $schema->addField('Config__App__paths__data', ['type' => 'string']);
        $schema->addField(
            'Config__App__paths__administrators_json',
            ['type' => 'string']
        );
        $schema->addField('Config__App__defaultLocal', ['type' => 'string']);
        $schema->addField('Config__App__timezone', ['type' => 'string']);
        $schema->addField('Config__App__fullBaseUrl', ['type' => 'string']);
        $schema->addField('Config__Ratchet__connect', ['type' => 'string']);
        $schema->addField(
            'Config__Datasources__default__driver',
            ['type' => 'string']
        );
        $schema->addField(
            'Config__Datasources__default__host',
            ['type' => 'string']
        );
        $schema->addField(
            'Config__Datasources__default__username',
            ['type' => 'string']
        );
        $schema->addField(
            'Config__Datasources__default__password',
            ['type' => 'string']
        );
        $schema->addField(
            'Config__Datasources__default__database',
            ['type' => 'string']
        );
        $schema->addField('Config__Security__salt', ['type' => 'string']);
        $schema->addField('Config__Email__default__from', ['type' => 'string']);
        $schema->addField(
            'Config__EmailTransport__default__className',
            ['type' => 'string']
        );
        $schema->addField(
            'Config__EmailTransport__default__host',
            ['type' => 'string']
        );
        $schema->addField(
            'Config__EmailTransport__default__port',
            ['type' => 'integer']
        );
        $schema->addField(
            'Config__EmailTransport__default__username',
            ['type' => 'string']
        );
        $schema->addField(
            'Config__EmailTransport__default__password',
            ['type' => 'string']
        );
        $schema->addField('Admins__username', ['type' => 'string']);
        $schema->addField('Admins__email', ['type' => 'string']);
        $schema->addField('Admins__password', ['type' => 'string']);
        $schema->addField('Timestamper__countryName', ['type' => 'string']);
        $schema->addField(
            'Timestamper__stateOrProvinceName',
            ['type' => 'string']
        );
        $schema->addField('Timestamper__localityName', ['type' => 'string']);
        $schema->addField(
            'Timestamper__organizationName',
            ['type' => 'string']
        );
        $schema->addField(
            'Timestamper__organizationalUnitName',
            ['type' => 'string']
        );
        $schema->addField('Timestamper__commonName', ['type' => 'string']);
        $schema->addField('Timestamper__emailAddress', ['type' => 'string']);
        $schema->addField('Timestamper__extfile', ['type' => 'text']);
        $schema->addField('Timestamper__directory', ['type' => 'string']);
        $schema->addField('Timestamper__cnf', ['type' => 'text']);
        $schema->addField('Timestamper__name', ['type' => 'string']);
        $schema->addField('ServiceExploitation__name', ['type' => 'string']);
        $schema->addField(
            'ServiceExploitation__identifier',
            ['type' => 'string']
        );
        $schema->addField('Eaccpfs__agent', ['type' => 'string']);
        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->add(
            'local_config_file',
            'writable',
            [
                'rule' => function ($value) {
                    $dir = realpath(dirname($value));
                    return $dir && is_writable(dirname($value)) && (
                            !is_file($value) || is_writable($value)
                        );
                },
                'message' => __("Le chemin indiqué n'est pas inscriptible"),
            ]
        );
        $validator->add(
            'Config__App__paths__data',
            'writable',
            [
                'rule' => function ($value) {
                    return is_writable($value);
                },
                'message' => __("Le chemin indiqué n'est pas inscriptible"),
            ]
        );
        $validator->add(
            'Config__App__paths__administrators_json',
            'writable',
            [
                'rule' => function ($value) {
                    return is_file($value)
                        ? is_writable($value)
                        : is_writable(
                            dirname($value)
                        );
                },
                'message' => __("Le chemin indiqué n'est pas inscriptible"),
            ]
        );
        $validator->add(
            'Config__App__defaultLocale',
            'writable',
            [
                'rule' => ['custom', '/^[a-z]{2,3}(_[A-Z]{2})?/'],
                'message' => __("Mauvais pattern"),
            ]
        );
        $validator->add(
            'Config__App__timezone',
            'timezone',
            [
                'rule' => function ($value) {
                    try {
                        $date = new CakeDateTime();
                        $date->setTimezone($value);
                        return true;
                    } catch (\Exception) {
                        return false;
                    }
                },
                'message' => __("Mauvaise timezone"),
            ]
        );
        $validator->add(
            'Config__App__fullBaseUrl',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if ($context['data']['ignore_invalid_fullbaseurl']) {
                        return true;
                    }
                    stream_context_set_default(
                        [
                            'ssl' => [
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                            ],
                        ]
                    );
                    $headers = @get_headers($value);
                    return $headers && $headers[0] !== 'HTTP/1.1 404 Not Found';
                },
                'message' => __("N'est pas un dns valide"),
            ]
        );
        $validator->add(
            'Config__Datasources__default__host',
            'custom',
            [
                'rule' => function ($value, $context) {
                    try {
                        $default = include CONFIG . 'app_default.php';
                        ConnectionManager::drop('validate_datasource');
                        ConnectionManager::setConfig(
                            'validate_datasource',
                            [
                                'host' => $value,
                                'driver' => $context['data']['Config__Datasources__default__driver']
                                    ?? $default['Datasources']['default']['driver'],
                                'username' => $context['data']['Config__Datasources__default__username'],
                                'password' => $context['data']['Config__Datasources__default__password'],
                                'database' => $context['data']['Config__Datasources__default__database'],
                            ] + $default['Datasources']['default']
                        );
                        $conn = ConnectionManager::get('validate_datasource');
                        $conn->execute('select 1');
                        return true;
                    } catch (PDOException | MissingConnectionException $e) {
                        return $e->getMessage();
                    }
                },
            ]
        );
        $validator->sameAs(
            'confirm_database_password',
            'Config__Datasources__default__password'
        );
        $validator->email('Config__Email__default__from');
        $validator->notEmptyString('Config__Email__default__from');
        $validator->numeric('Config__EmailTransport__default__port');
        $validator->notEmptyString(
            'Config__EmailTransport__default__className'
        );
        $validator->notEmptyString('Admins__username');
        $validator->notEmptyString('Admins__email');
        $validator->notEmptyString('Admins__password');
        $validator->sameAs('Admins__confirm-password', 'Admins__password');
        $validator->notEmptyString('Timestamper__countryName');
        $validator->notEmptyString('Timestamper__stateOrProvinceName');
        $validator->notEmptyString('Timestamper__localityName');
        $validator->notEmptyString('Timestamper__organizationName');
        $validator->notEmptyString('Timestamper__organizationalUnitName');
        $validator->notEmptyString('Timestamper__commonName');
        $validator->notEmptyString('Timestamper__emailAddress');
        $validator->notEmptyString('Timestamper__extfile');
        $validator->notEmptyString('Timestamper__directory');
        $validator->add(
            'Timestamper__directory',
            'writable',
            [
                'rule' => function ($value) {
                    return is_writable($value);
                },
                'message' => __("Le dossier indiqué n'est pas inscriptible"),
            ]
        );
        $validator->notEmptyString('Timestamper__cnf');
        $validator->notEmptyString('Timestamper__name');
        $validator->notEmptyString('ServiceExploitation__name');
        $validator->notEmptyString('ServiceExploitation__identifier');
        $validator->notEmptyString('Eaccpfs__agent');

        return $validator;
    }

    /**
     * Execute the form if it is valid.
     *
     * First validates the form, then calls the `_execute()` hook method.
     * This hook method can be implemented in subclasses to perform
     * the action of the form. This may be sending email, interacting
     * with a remote API, or anything else you may need.
     *
     * @param array $data    Form data.
     * @param array $options List of options.
     * @return bool False on validation failure, otherwise returns the
     *   result of the `_execute()` method.
     */
    public function execute(array $data, array $options = []): bool
    {
        $dataPath = Config::getPathToLocal();
        if (
            $dataPath && !is_dir(
                $dir = dirname($dataPath) . DS . 'timestamping'
            )
        ) {
            mkdir($dir, 0700);
        }
        return parent::execute($data);
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $this->configurate($data);
        $this->createAdmin($data);
        $this->createTimestamper($data);
        return (bool)$this->createSE($data);
    }

    /**
     * Configuration
     * @param array $data
     */
    private function configurate(array $data)
    {
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        if (isset($data['local_config_file'])) {
            if (!is_file($data['local_config_file'])) {
                file_put_contents($data['local_config_file'], '{}');
            }
            $path = addcslashes($data['local_config_file'], "'\\");
            file_put_contents($pathToLocalConfig, "<?php return '$path';?>");
        }
        switch ($data['security_salt_method'] ?? '') {
            case 'apply':
                $data['Config__Security__salt'] = $data['security_salt_method_apply'];
                break;
            case 'generate':
                $data['Config__Security__salt'] = $data['security_salt_method_generate'];
                break;
            case 'hash':
                $data['Config__Security__salt'] = hash(
                    'sha256',
                    'Libriciel-' . $data['security_salt_method_hash']
                );
                break;
        }
        $pathToLocal = include $pathToLocalConfig;
        $config = is_readable($pathToLocal)
            ? (array)json_decode(file_get_contents($pathToLocal), true)
            : [];
        $configChanged = false;
        $configKeys = [
            'App__paths__data',
            'App__paths__administrators_json',
            'App__defaultLocale',
            'App__timezone',
            'App__fullBaseUrl',
            'Ratchet__connect',
            'Datasources__default__driver',
            'Datasources__default__host',
            'Datasources__default__username',
            'Datasources__default__password',
            'Datasources__default__database',
            'Security__salt',
            'Email__default__from',
            'EmailTransport__default__host',
            'EmailTransport__default__port',
            'EmailTransport__default__username',
            'EmailTransport__default__password',
        ];
        foreach ($configKeys as $key) {
            $dataKey = 'Config__' . $key;
            if (isset($data[$dataKey])) {
                $config = Hash::insert(
                    $config,
                    str_replace('__', '.', $key),
                    $data[$dataKey]
                );
                $configChanged = true;
            }
        }

        if ($configChanged) {
            ksort($config);
            file_put_contents(
                $pathToLocal,
                json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
            );
        }
    }

    /**
     * Création du premier administrateur technique
     * @param array $data
     */
    private function createAdmin(array $data)
    {
        $targetFilename = Configure::read('App.paths.administrators_json');
        if (empty($data['Admins__username']) || is_file($targetFilename)) {
            return;
        }
        $hasher = new DefaultPasswordHasher();
        file_put_contents(
            $targetFilename,
            json_encode(
                [
                    [
                        'username' => $data['Admins__username'],
                        'email' => $data['Admins__email'] ?? '',
                        'password' => $hasher->hash(
                            $data['Admins__password'] ?? ''
                        ),
                    ],
                ],
                JSON_UNESCAPED_SLASHES
            )
        );
    }

    /**
     * Création d'un service d'horodatage local
     * @param array $data
     * @throws Exception
     */
    private function createTimestamper(array $data)
    {
        // Création d'un CA
        if (empty($data['Timestamper__directory'])) {
            return;
        }
        $dir = $data['Timestamper__directory'];
        $cnf = str_replace('{{dir}}', $dir, $data['Timestamper__cnf']);
        file_put_contents($dir . DS . 'asalae-tsa.cnf', $cnf);

        $tsaKey = openssl_pkey_new(
            [
                'private_key_bits' => 2048,
                'private_key_type' => OPENSSL_KEYTYPE_RSA,
            ]
        );
        $csr = openssl_csr_new(
            [
                "countryName" => $data['Timestamper__countryName'],
                "stateOrProvinceName" => $data['Timestamper__stateOrProvinceName'],
                "localityName" => $data['Timestamper__localityName'],
                "organizationName" => $data['Timestamper__organizationName'],
                "organizationalUnitName" => $data['Timestamper__organizationalUnitName'],
                "commonName" => $data['Timestamper__commonName'],
                "emailAddress" => $data['Timestamper__emailAddress'],
            ],
            $tsaKey
        );
        openssl_pkey_export_to_file($tsaKey, $dir . DS . 'tsacert.pem');
        openssl_csr_export_to_file($csr, $dir . DS . 'tsacert.csr');

        file_put_contents($dir . DS . 'extfile', $data['Timestamper__extfile']);
        $in = Exec::escapeshellarg($dir . DS . 'tsacert.csr');
        $signkey = Exec::escapeshellarg($dir . DS . 'tsacert.pem');
        $out = Exec::escapeshellarg($dir . DS . 'tsacert.crt');
        $extfile = Exec::escapeshellarg($dir . DS . 'extfile');
        $cmd = "openssl x509 -req -days 3650 -in $in -signkey $signkey -out $out -extfile $extfile 2>&1";
        exec($cmd, $output, $code);
        if ($code !== 0) {
            throw new Exception(implode(PHP_EOL, $output));
        }

        // Ajout de l'horodateur en base
        file_put_contents($dir . DS . 'serial', '00000001');
        $Timestampers = TableRegistry::getTableLocator()->get('Timestampers');
        $entity = $Timestampers->newEntity(
            [
                'name' => $data['Timestamper__name'],
                'fields' => json_encode(
                    [
                        'name' => $data['Timestamper__name'],
                        'driver' => 'OPENSSL',
                        'ca' => $dir . DS . 'tsacert.crt',
                        'crt' => $dir . DS . 'tsacert.crt',
                        'pem' => $dir . DS . 'tsacert.pem',
                        'cnf' => $dir . DS . 'asalae-tsa.cnf',
                    ],
                    JSON_UNESCAPED_SLASHES
                ),
            ]
        );
        $Timestampers->saveOrFail($entity);
    }

    /**
     * Création du service d'exploitation
     * @param array $data
     * @return bool|void
     */
    private function createSE(array $data)
    {
        if (empty($data['ServiceExploitation__name'])) {
            return;
        }
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $seCode = $OrgEntities->getAssociation('TypeEntities')->find()
            ->select(['id'])
            ->where(['code' => 'SE'])
            ->firstOrFail()
            ->get('id');
        $entity = $OrgEntities->newEntity(
            [
                'name' => $data['ServiceExploitation__name'],
                'identifier' => $data['ServiceExploitation__identifier'],
                'type_entity_id' => $seCode,
            ]
        );
        $OrgEntities->saveOrFail($entity);

        $from_date = new CakeDateTime();
        $from_date->setTimezone(Configure::read('App.timezone', 'UTC'));
        $initialData = [
            'name' => $entity->get('name'),
            'record_id' => $entity->get('identifier'),
            'agency_name' => $entity->get('name'),
            'org_entity_id' => $entity->get('id'),
            'entity_type' => 'corporateBody',
            'from_date' => $from_date,
        ];
        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs');
        /** @var Eaccpf $eac */
        $eac = $Eaccpfs->newEntity($initialData);
        $Eaccpfs->initializeData($eac, ['name' => $data['Eaccpfs__agent']]);

        return (bool)$Eaccpfs->save($eac);
    }
}
