<?php

/**
 * Asalae\Form\SearchArchiveForm
 */

namespace Asalae\Form;

use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\I18n\Date as CoreDate;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Http\ServerRequest;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateMalformedStringException;
use DateTime;
use DateTimeInterface;
use Exception;
use ReflectionException;

/**
 * Formulaire de recherche d'archives
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SearchArchiveForm extends Form
{
    use EntityFormTrait;

    /**
     * @var Query sera modifiée dans _execute()
     */
    public $query;
    /**
     * @var array
     */
    public $data;
    /**
     * @var array query du GET (créé les règles de validation à la volée)
     */
    private $queryData = [];
    /**
     * @var EntityInterface OrgEntity du service d'archives
     */
    private $sa;
    /**
     * @var EntityInterface OrgEntity de l'utilisateur
     */
    private $orgEntity;
    /**
     * @var EntityInterface Role de l'utilisateur
     */
    private $role;
    /**
     * @var ConditionComponent
     */
    private $Condition;

    /**
     * Fait appel au constructeur et initialise les attributs important
     * @param Query             $query
     * @param array             $queryData
     * @param EntityInterface[] $entities  ArchivalAgency, UserOrgEntity, UserRole
     * @return SearchArchiveForm
     * @throws ReflectionException
     */
    public static function create(
        Query $query,
        array $queryData,
        array $entities
    ): SearchArchiveForm {
        $sa = $entities['ArchivalAgency'] ?? null;
        $orgEntity = $entities['UserOrgEntity'] ?? null;
        $role = $entities['UserRole'] ?? null;
        $form = new self();
        $form->query = $query;
        $form->queryData = $queryData;
        $form->_data = $form->formatData($queryData);
        $form->data = Hash::expand($form->_data);
        $form->sa = $sa;
        $form->orgEntity = $orgEntity;
        $form->role = $role;
        $form->Condition = new ConditionComponent(new ComponentRegistry(new Controller(new ServerRequest())));
        return $form;
    }

    /**
     * Formate les données pour les rendre exploitable
     * @param array $queryData
     * @return array
     */
    private function formatData(array $queryData): array
    {
        $data = Hash::flatten($queryData);
        $schema = $this->getSchema();

        foreach ($data as $field => $value) {
            $schemaField = $schema->field($field);
            $type = $schemaField['type'] ?? 'string';
            if (method_exists($this, 'setter' . ucfirst($type))) {
                $data[$field] = $this->{'setter' . ucfirst($type)}($value);
            }
        }
        return $data;
    }

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $types = [
            'string' => [
                'Archives.{n}.name',
                'Archives.{n}.description',
                'Archives.{n}.keyword',
                'Archives.{n}.dua',
                'Archives.{n}.dua_duration',
                'Archives.{n}.appraisal_rules_final_action_code',
                'Archives.{n}.access_rules_end_date_vs_now',
                'Archives.{n}.archival_agency_identifier',
                'Archives.{n}.transferring_agency_identifier',
                'Archives.{n}.originating_agency_identifier',
            ],
            'date' => [
                'Archives.{n}.dua_end',
                'Archives.{n}.created',
            ],
            'integer' => [
                'Archives.{n}.keyword_list',
            ],
        ];// TODO ajouter les autres champs
        foreach ($types as $type => $paths) {
            foreach ($paths as $path) {
                $values = Hash::extract($this->queryData, $path);
                foreach (array_keys($values) as $index) {
                    $schema->addField(
                        str_replace('{n}', $index, $path),
                        ['type' => $type]
                    );
                }
            }
        }
        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $rules = [
            'notEmptyString' => [
                'Archives.{n}.name',
                'Archives.{n}.description',
                'Archives.{n}.keyword',
                'Archives.{n}.dua',
                'Archives.{n}.dua_duration',
                'Archives.{n}.appraisal_rules_final_action_code',
                'Archives.{n}.access_rules_end_date_vs_now',
                'Archives.{n}.archival_agency_identifier',
                'Archives.{n}.transferring_agency_identifier',
                'Archives.{n}.originating_agency_identifier',
            ],
            'notEmptyDate' => [
                'Archives.{n}.dua_end',
                'Archives.{n}.created',
            ],
            'date' => [
                'Archives.{n}.dua_end',
                'Archives.{n}.created',
            ],
        ];// TODO ajouter un maximum de validations
        foreach ($rules as $rule => $paths) {
            foreach ($paths as $path) {
                $values = Hash::extract($this->queryData, $path);
                foreach (array_keys($values) as $index) {
                    $validator->$rule(str_replace('{n}', $index, $path));
                }
            }
        }
        return $validator;
    }

    /**
     * Execute the form if it is valid.
     *
     * First validates the form, then calls the `_execute()` hook method.
     * This hook method can be implemented in subclasses to perform
     * the action of the form. This may be sending email, interacting
     * with a remote API, or anything else you may need.
     *
     * @param array $data    Form data.
     * @param array $options
     * @return bool False on validation failure, otherwise returns the
     *   result of the `_execute()` method.
     * @throws Exception
     */
    public function execute(array $data, array $options = []): bool
    {
        foreach ($data as $key => $value) {
            if (
                preg_match('/start|end|begin|created|modified|date/', $key)
                && preg_match(
                    '/^((\d{2}|\d{4})([\/-])\d{2}([\/-])\d{2})|(\d{2}([\/-])\d{2}([\/-])(\d{2}|\d{4}))$/',
                    $value
                )
            ) {
                $data[$key] = $this->setterDate($value);
            }
        }
        $this->data = Hash::expand($data);
        if (!$this->validate($data)) {
            return false;
        }

        return $this->_execute($data);
    }

    /**
     * Setter des types date
     * @param string|DateTimeInterface $value
     * @return CakeDateTime|string 'false' si pas réussi à parser la date
     * @throws DateMalformedStringException
     */
    public function setterDate($value)
    {
        if (is_string($value)) {
            $value = new CakeDateTime(CoreDate::stringToDateTime($value));
        } elseif ($value instanceof CakeDate || $value instanceof CoreDate) {
            $value = new CakeDateTime($value->format('Y-m-d'));
        } elseif ($value instanceof DateTimeInterface) {
            $value = new CakeDateTime($value);
        }

        return $value ?: 'false';
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $this->appendArchivesConditions();
        $this->appendArchiveUnitsConditions();
        $this->appendDocumentsConditions();

        return true;
    }

    /**
     * Conditions sur Archives
     */
    private function appendArchivesConditions()
    {
        $types = [
            'ilike' => [
                'name',
                'description',
                'archival_agency_identifier',
                'transferring_agency_identifier',
                'originating_agency_identifier',
            ],
            'date' => [
                'created',
            ],
        ];
        $this->query->where(
            $this->getGenericConditions('Archives', 'Archives', $types)
        );

        foreach (
            Hash::extract(
                $this->data,
                'Archives.{n}.keyword'
            ) as $key => $value
        ) {
            $this->addKeywordCondition('aauk' . $key, $value);
        }
        foreach (
            Hash::extract(
                $this->data,
                'Archives.{n}.keyword_list'
            ) as $key => $value
        ) {
            $this->addKeywordListCondition('aaukl' . $key, $value);
        }
        foreach (Hash::extract($this->data, 'Archives.{n}.dua') as $value) {
            $operator = $this->allowedOperator($value);
            if (!$operator) {
                $this->query->where(['1 = 0']);
                return;
            }
            $this->query->where(
                $this->Condition->dateOperator(
                    'AppraisalRules.end_date',
                    $operator,
                    new DateTime()
                )
            );
        }
        foreach (
            Hash::extract(
                $this->data,
                'Archives.{n}.dua_duration'
            ) as $key => $value
        ) {
            $operator = $this->allowedOperator(
                Hash::get($this->data, "Archives.$key.dua_duration_operator")
            );
            if (!$operator || !preg_match('/^P(\d+)Y$/', $value, $match)) {
                $this->query->where(['1 = 0']);
                return;
            }
            $diff = $this->Condition->dateDiff(
                'AppraisalRules.end_date',
                'AppraisalRules.start_date'
            );
            $this->query->where(["$diff $operator $match[1]"]);
        }
        foreach (
            Hash::extract(
                $this->data,
                'Archives.{n}.dua_end'
            ) as $key => $value
        ) {
            $operator = $this->allowedOperator(
                Hash::get($this->data, "Archives.$key.dateoperator_dua_end")
            );
            if (!$operator) {
                $this->query->where(['1 = 0']);
                return;
            }
            $this->query->where(
                $this->Condition->dateOperator(
                    'AppraisalRules.end_date',
                    $operator,
                    $value
                )
            );
        }
        foreach (
            Hash::extract(
                $this->data,
                'Archives.{n}.appraisal_rules_final_action_code'
            ) as $value
        ) {
            $this->query->where(['AppraisalRules.final_action_code' => $value]);
        }
        foreach (
            Hash::extract(
                $this->data,
                'Archives.{n}.access_rules_end_date_vs_now'
            ) as $value
        ) {
            $operator = $this->allowedOperator($value);
            if (!$operator) {
                $this->query->where(['1 = 0']);
                return;
            }
            $date = new DateTime();
            $this->query->where(['AccessRules.end_date ' . $operator => $date]);
        }
    }

    /**
     * Conditions génériques
     * @param string $model
     * @param string $alias
     * @param array  $types ex: ['ilike' => [...$fields]]
     * @return array
     * @throws Exception
     */
    private function getGenericConditions(
        string $model,
        string $alias,
        array $types
    ): array {
        $conditions = [];
        foreach ($types as $type => $fields) {
            foreach ($fields as $field) {
                $condition = $this->getGenericCondition($type, $model, $alias, $field);
                if ($condition) {
                    $conditions[] = $condition;
                }
            }
        }
        return $conditions;
    }

    /**
     * Donne la condition pour un $type
     * @param string $type
     * @param string $model
     * @param string $alias
     * @param string $field
     * @return array|QueryExpression|DateTime[]|string[]|null
     * @throws Exception
     */
    private function getGenericCondition(
        string $type,
        string $model,
        string $alias,
        string $field
    ) {
        switch ($type) {
            case 'ilike':
                foreach (
                    Hash::extract(
                        $this->data,
                        "$model.{n}.$field"
                    ) as $value
                ) {
                    return $this->Condition->ilike(
                        "$alias.$field",
                        $value
                    );
                }
                break;
            case 'date':
                foreach (
                    Hash::extract(
                        $this->data,
                        "$model.{n}.$field"
                    ) as $key => $value
                ) {
                    $operator = Hash::get(
                        $this->data,
                        "$model.$key.dateoperator_$field"
                    );
                    if (!$operator) {
                        return ['1 = 0'];
                    }
                    return $this->Condition->dateOperator(
                        "$alias.$field",
                        $operator,
                        $value
                    );
                }
                break;
        }
        return null;
    }

    /**
     * Condition sur keyword
     * @param string $suffix
     * @param string $value
     */
    private function addKeywordCondition(string $suffix, string $value)
    {
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $ArchiveKeywords = TableRegistry::getTableLocator()->get(
            'ArchiveKeywords'
        );

        $akIds = $ArchiveKeywords->find()
            ->select(['id'])
            ->where($this->Condition->ilike('content', $value))
            ->all()
            ->map(
                function (EntityInterface $v) {
                    return $v->get('id');
                }
            )
            ->toArray();

        if (empty($akIds)) {
            $this->query->where(['1 = 0']);
            return;
        }
        $archiveUnitsAlias = 'au' . $suffix;
        $archiveKeywordsAlias = 'ak' . $suffix;
        $subquery = $ArchiveUnits->selectQuery()
            ->select($archiveUnitsAlias . '.archive_id')
            ->from([$archiveUnitsAlias => 'archive_units'])
            ->innerJoin(
                [$archiveKeywordsAlias => 'archive_keywords'],
                [
                    $archiveKeywordsAlias . '.archive_unit_id'
                    => new IdentifierExpression($archiveUnitsAlias . '.id'),
                ]
            )
            ->where(
                [
                    $archiveUnitsAlias . '.archive_id' => new IdentifierExpression(
                        'Archives.id'
                    ),
                    $archiveKeywordsAlias . '.id IN' => $akIds,
                ]
            );
        $this->query->where(['Archives.id IN' => $subquery]);
    }

    /**
     * Conditions sur des archive_keyword_id
     * @param string $suffix
     * @param array  $values
     */
    private function addKeywordListCondition(string $suffix, array $values)
    {
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $ArchiveKeywords = TableRegistry::getTableLocator()->get(
            'ArchiveKeywords'
        );

        /**
         * NOTES:
         * keyword_list contient les ids de la table Keywords
         * on récupère le name liés à ces ids
         * on recherche les ArchiveKeywords qui ont un "content" = "Keyword.name"
         * ils doivent bien entendu avoir un archive_unit_id qui correspond à l'archive
         */
        $names = $Keywords->find()
            ->select(['name'])
            ->where(['id IN' => $values])
            ->all()
            ->map(
                function (EntityInterface $v) {
                    return $v->get('name');
                }
            )
            ->toArray();
        if (empty($names)) {
            $this->query->where(['1 = 0']);
            return;
        }
        $akIds = $ArchiveKeywords->find()
            ->select(['id'])
            ->where(['content IN' => $names])
            ->all()
            ->map(
                function (EntityInterface $v) {
                    return $v->get('id');
                }
            )
            ->toArray();
        if (empty($akIds)) {
            $this->query->where(['1 = 0']);
            return;
        }
        $archiveUnitsAlias = 'au' . $suffix;
        $archiveKeywordsAlias = 'ak' . $suffix;
        $subquery = $ArchiveUnits->selectQuery()
            ->select($archiveUnitsAlias . '.archive_id')
            ->from([$archiveUnitsAlias => 'archive_units'])
            ->innerJoin(
                [$archiveKeywordsAlias => 'archive_keywords'],
                [
                    $archiveKeywordsAlias . '.archive_unit_id'
                    => new IdentifierExpression($archiveUnitsAlias . '.id'),
                ]
            )
            ->where(
                [
                    $archiveUnitsAlias . '.archive_id' => new IdentifierExpression(
                        'Archives.id'
                    ),
                    $archiveKeywordsAlias . '.id IN' => $akIds,
                ]
            );
        $this->query->where(['Archives.id IN' => $subquery]);
    }

    /**
     * Renvoi l'operateur s'il est valide, false sinon
     * @param string $operator
     * @return bool|string
     */
    private function allowedOperator(string $operator)
    {
        $operator = strtolower($operator);
        $allowed = [
            '=',
            '<',
            '>',
            '<=',
            '>=',
            '<>',
            '!=',
            'like',
            'not like',
            'is',
            'is not',
        ];
        return in_array($operator, $allowed) ? $operator : false;
    }

    /**
     * Conditions sur ArchiveUnits
     * NOTE sur les alias:
     * le lien [A]rchive[U]nits -> [A]ppraisal[R]ules -> [d]ua_[d]uration donne [auardd]
     * Une à deux lettres par liens, de préférence le debut de chaques mots
     */
    private function appendArchiveUnitsConditions()
    {
        $types = [
            'ilike' => [
                'name',
                'archival_agency_identifier',
                'transferring_agency_identifier',
                'originating_agency_identifier',
            ],
        ];
        if (
            $conditions = $this->getGenericConditions(
                'ArchiveUnits',
                'au',
                $types
            )
        ) {
            $ArchiveUnits = TableRegistry::getTableLocator()->get(
                'ArchiveUnits'
            );
            $subquery = $ArchiveUnits->selectQuery()
                ->select('au.archive_id')
                ->from(['au' => 'archive_units'])
                ->where(
                    ['au.archive_id' => new IdentifierExpression('Archives.id')]
                )
                ->andWhere($conditions);
            $this->query->where(['Archives.id IN' => $subquery]);
        }
        foreach (
            Hash::extract(
                $this->data,
                'ArchiveUnits.{n}.keyword'
            ) as $key => $value
        ) {
            $this->addKeywordCondition('auauk' . $key, $value);
        }
        foreach (
            Hash::extract(
                $this->data,
                'ArchiveUnits.{n}.keyword_list'
            ) as $key => $values
        ) {
            $this->addKeywordListCondition('auaukl' . $key, $values);
        }
        foreach (
            Hash::extract(
                $this->data,
                'ArchiveUnits.{n}.dua'
            ) as $key => $value
        ) {
            $operator = $this->allowedOperator($value);
            if (!$operator) {
                $this->query->where(['1 = 0']);
                return;
            }
            $archiveUnitsAlias = 'aud' . $key;
            $appraisalRulesAlias = 'auard' . $key;
            $subquery = $this->getArchiveUnitAppraisalRulesQuery(
                $archiveUnitsAlias,
                $appraisalRulesAlias
            )
                ->where(
                    $this->Condition->dateOperator(
                        $appraisalRulesAlias . '.end_date',
                        $operator,
                        new DateTime()
                    )
                );
            $this->query->where(['Archives.id IN' => $subquery]);
        }
        foreach (
            Hash::extract(
                $this->data,
                'ArchiveUnits.{n}.dua_duration'
            ) as $key => $value
        ) {
            $operator = $this->allowedOperator(
                Hash::get(
                    $this->data,
                    "ArchiveUnits.$key.dua_duration_operator"
                )
            );
            if (!$operator || !preg_match('/^P(\d+)Y$/', $value, $match)) {
                $this->query->where(['1 = 0']);
                return;
            }
            $archiveUnitsAlias = 'audd' . $key;
            $appraisalRulesAlias = 'auardd' . $key;
            $diff = $this->Condition->dateDiff(
                $appraisalRulesAlias . '.end_date',
                $appraisalRulesAlias . '.start_date'
            );
            $subquery = $this->getArchiveUnitAppraisalRulesQuery(
                $archiveUnitsAlias,
                $appraisalRulesAlias
            )
                ->where(["$diff $operator $match[1]"]);
            $this->query->where(['Archives.id IN' => $subquery]);
        }
        foreach (
            Hash::extract(
                $this->data,
                'ArchiveUnits.{n}.dua_end'
            ) as $key => $value
        ) {
            $operator = $this->allowedOperator(
                Hash::get($this->data, "ArchiveUnits.$key.dateoperator_dua_end")
            );
            if (!$operator) {
                $this->query->where(['1 = 0']);
                return;
            }
            $archiveUnitsAlias = 'aude' . $key;
            $appraisalRulesAlias = 'auarde' . $key;
            if ($value instanceof DateTimeInterface || $value instanceof CakeDate) {
                $value = $value->format('Y-m-d');
            }
            $subquery = $this->getArchiveUnitAppraisalRulesQuery(
                $archiveUnitsAlias,
                $appraisalRulesAlias
            )
                ->where(
                    [$appraisalRulesAlias . '.end_date ' . $operator => $value]
                );
            $this->query->where(['Archives.id IN' => $subquery]);
        }
        foreach (
            Hash::extract(
                $this->data,
                'ArchiveUnits.{n}.appraisal_rules_final_action_code'
            ) as $key => $value
        ) {
            $archiveUnitsAlias = 'auac' . $key;
            $appraisalRulesAlias = 'auarac' . $key;
            $subquery = $this->getArchiveUnitAppraisalRulesQuery(
                $archiveUnitsAlias,
                $appraisalRulesAlias
            )
                ->where(
                    [$appraisalRulesAlias . '.final_action_code' => $value]
                );
            $this->query->where(['Archives.id IN' => $subquery]);
        }
        foreach (
            Hash::extract(
                $this->data,
                'ArchiveUnits.{n}.access_rules_end_date_vs_now'
            ) as $key => $value
        ) {
            $operator = $this->allowedOperator($value);
            if (!$operator) {
                $this->query->where(['1 = 0']);
                return;
            }
            $date = new DateTime();
            $archiveUnitsAlias = 'auvs' . $key;
            $accessRulesAlias = 'aualvs' . $key;
            $subquery = $this->getArchiveUnitAccessRulesQuery(
                $archiveUnitsAlias,
                $accessRulesAlias
            )
                ->where(
                    [$accessRulesAlias . '.end_date ' . $operator => $date],
                    [$accessRulesAlias . '.end_date' => 'date']
                );
            $this->query->where(['Archives.id IN' => $subquery]);
        }
        foreach (
            Hash::extract(
                $this->data,
                'ArchiveUnits.{n}.description'
            ) as $key => $value
        ) {
            $archiveUnitsAlias = 'auds' . $key;
            $archiveDescriptionAlias = 'auadds' . $key;
            $subquery = $this->getArchiveUnitArchiveDescriptionQuery(
                $archiveUnitsAlias,
                $archiveDescriptionAlias
            )
                ->where(
                    $this->Condition->ilike(
                        $archiveDescriptionAlias . '.description',
                        $value
                    )
                );
            $this->query->where(['Archives.id IN' => $subquery]);
        }
    }

    /**
     * Donne un subquery pour un appraisal_rule d'un archive_unit
     * @param string $archiveUnitsAlias
     * @param string $appraisalRulesAlias
     * @return Query
     */
    private function getArchiveUnitAppraisalRulesQuery(
        string $archiveUnitsAlias,
        string $appraisalRulesAlias
    ): Query {
        return TableRegistry::getTableLocator()->get('ArchiveUnits')->selectQuery()
            ->select($archiveUnitsAlias . '.archive_id')
            ->from([$archiveUnitsAlias => 'archive_units'])
            ->innerJoin(
                [$appraisalRulesAlias => 'appraisal_rules'],
                [
                    $appraisalRulesAlias . '.id'
                    => new IdentifierExpression(
                        $archiveUnitsAlias . '.appraisal_rule_id'
                    ),
                ]
            )
            ->where(
                [
                    $archiveUnitsAlias . '.archive_id' => new IdentifierExpression(
                        'Archives.id'
                    ),
                ]
            );
    }

    /**
     * Donne un subquery pour un access_rule d'un archive_unit
     * @param string $archiveUnitsAlias
     * @param string $accessRulesAlias
     * @return Query
     */
    private function getArchiveUnitAccessRulesQuery(
        string $archiveUnitsAlias,
        string $accessRulesAlias
    ): Query {
        return TableRegistry::getTableLocator()->get('ArchiveUnits')->selectQuery()
            ->select($archiveUnitsAlias . '.archive_id')
            ->from([$archiveUnitsAlias => 'archive_units'])
            ->innerJoin(
                [$accessRulesAlias => 'access_rules'],
                [
                    $accessRulesAlias . '.id' => new IdentifierExpression(
                        $archiveUnitsAlias . '.access_rule_id'
                    ),
                ]
            )
            ->where(
                [
                    $archiveUnitsAlias . '.archive_id' => new IdentifierExpression(
                        'Archives.id'
                    ),
                ]
            );
    }

    /**
     * Donne un subquery pour un access_rule d'un archive_unit
     * @param string $archiveUnitsAlias
     * @param string $archiveDescriptionAlias
     * @return Query
     */
    private function getArchiveUnitArchiveDescriptionQuery(
        string $archiveUnitsAlias,
        string $archiveDescriptionAlias
    ): Query {
        return TableRegistry::getTableLocator()->get('ArchiveUnits')->selectQuery()
            ->select($archiveUnitsAlias . '.archive_id')
            ->from([$archiveUnitsAlias => 'archive_units'])
            ->innerJoin(
                [$archiveDescriptionAlias => 'archive_descriptions'],
                [
                    $archiveDescriptionAlias . '.archive_unit_id'
                    => new IdentifierExpression($archiveUnitsAlias . '.id'),
                ]
            )
            ->where(
                [
                    $archiveUnitsAlias . '.archive_id' => new IdentifierExpression(
                        'Archives.id'
                    ),
                ]
            );
    }

    /**
     * Conditions sur Documents
     */
    private function appendDocumentsConditions()
    {
        foreach (
            Hash::extract(
                $this->data,
                'Documents.{n}.name'
            ) as $key => $value
        ) {
            $prefix = 'dn' . $key;
            $archiveBinaryAlias = $prefix . 'ab';
            $subquery = $this->getDocumentQuery($prefix, $archiveBinaryAlias)
                ->where(
                    $this->Condition->ilike(
                        $archiveBinaryAlias . '.filename',
                        $value
                    )
                );
            $this->query->where(['Archives.id IN' => $subquery]);
        }
        foreach (
            Hash::extract(
                $this->data,
                'Documents.{n}.mime'
            ) as $key => $values
        ) {
            $prefix = 'dm' . $key;
            $archiveBinaryAlias = $prefix . 'abm';
            $subquery = $this->getDocumentQuery($prefix, $archiveBinaryAlias)
                ->where([$archiveBinaryAlias . '.mime IN' => $values]);
            $this->query->where(['Archives.id IN' => $subquery]);
        }
    }

    /**
     * Donne un subquery pour des archive_binaries d'un archive_unit
     * @param string $prefix
     * @param string $ArchiveBinaryAlias
     * @return Query
     */
    private function getDocumentQuery(
        string $prefix,
        string $ArchiveBinaryAlias
    ): Query {
        $archiveUnitsAlias = $prefix . 'docau';
        $archiveUnitArchiveBinaryAlias = $prefix . 'docauaaab';
        return TableRegistry::getTableLocator()->get('ArchiveUnits')->selectQuery()
            ->select($archiveUnitsAlias . '.archive_id')
            ->from([$archiveUnitsAlias => 'archive_units'])
            ->innerJoin(
                [$archiveUnitArchiveBinaryAlias => 'archive_binaries_archive_units'],
                [
                    $archiveUnitArchiveBinaryAlias . '.archive_unit_id'
                    => new IdentifierExpression($archiveUnitsAlias . '.id'),
                ]
            )
            ->innerJoin(
                [$ArchiveBinaryAlias => 'archive_binaries'],
                [
                    $ArchiveBinaryAlias . '.id'
                    => new IdentifierExpression(
                        $archiveUnitArchiveBinaryAlias . '.archive_binary_id'
                    ),
                ]
            );
    }
}
