<?php

/**
 * Asalae\Form\PromoteSuperArchivistForm
 */

namespace Asalae\Form;

use Asalae\Model\Table\RolesTable;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;

/**
 * Formulaire de job Beanstlalk
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class PromoteSuperArchivistForm extends Form
{
    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('user_id', ['type' => 'int']);
        $schema->addField('archival_agencies', ['type' => 'array']);

        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence('user_id')
            ->notEmptyString('user_id')
            ->add(
                'user_id',
                'exists',
                [
                    'rule' => function ($value) {
                        $loc = TableRegistry::getTableLocator();
                        $Users = $loc->get('Users');
                        return $Users->exists(['id' => $value]);
                    },
                    'message' => __("Cet utilisateur n'existe pas"),
                ]
            );

        $validator
            ->requirePresence('archival_agencies')
            ->notEmptyString('archival_agencies');

        return $validator;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $loc = TableRegistry::getTableLocator();
        $Users = $loc->get('Users');
        $Roles = $loc->get('Roles');
        $role = $Roles->find()
            ->where(['code' => RolesTable::CODE_SUPER_ARCHIVIST])
            ->firstOrFail();
        $ArosAcos = $loc->get('ArosAcos');
        $user = $Users->find()
            ->where(['Users.id' => $data['user_id']])
            ->contain(['Aros'])
            ->firstOrFail();

        $conn = $Users->getConnection();
        $conn->begin();
        $ArosAcos->deleteAll(['aro_id' => Hash::get($user, 'aro.id')]);
        $userData = [
            'role_id' => $role->id,
            'org_entity_id' => $data['org_entity_id'],
            'archival_agencies' => [
                '_ids' => $data['archival_agencies'],
            ],
        ];
        $Users->patchEntity($user, $userData);
        $Users->saveOrFail($user);
        $conn->commit();
        $this->set('user', $user);
        return true;
    }
}
