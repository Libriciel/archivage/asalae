<?php

/**
 * Asalae\Form\IndicatorsCsvForm
 */

namespace Asalae\Form;

use Asalae\Controller\IndicatorsController;
use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\I18n\Number;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Cake\View\ViewBuilder;
use DateTime;
use Exception;

/**
 * Indicatorscsv Form.
 * @property array|string|string[] $csv
 */
class IndicatorsCsvForm extends Form
{
    /**
     * @var array|string|string[]
     */
    public $csv;
    /**
     * @var null|array
     */
    protected $_indicatorsData = null;
    /**
     * @var array
     */
    protected $_filters = [];

    /**
     * Builds the schema for the modelless form
     *
     * @param \Cake\Form\Schema $schema From schema
     * @return \Cake\Form\Schema
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('delimiter', ['type' => 'string']);
        $schema->addField('enclosure', ['type' => 'string']);
        $schema->addField('unit', ['type' => 'string']);

        return $schema;
    }

    /**
     * Form validation builder
     *
     * @param \Cake\Validation\Validator $validator to use against the form
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence('delimiter')
            ->inList('delimiter', array_keys($this->delimiterOptions()));

        $validator
            ->requirePresence('enclosure')
            ->inList('enclosure', array_keys($this->enclosureOptions()));

        $validator
            ->requirePresence('unit')
            ->inList('unit', array_keys($this->unitOptions()));

        return $validator;
    }

    /**
     * @return array
     */
    public function delimiterOptions(): array
    {
        return [
            chr(59) => __("Point-virgule"),
            chr(44) => __("Virgule"),
            chr(8) => __("Espace"),
            chr(9) => __("Tabulation"),
        ];
    }

    /**
     * @return array
     */
    public function enclosureOptions(): array
    {
        return [
            '"' => __("Guillemet (\")"),
            "'" => __("Apostrophe (')"),
        ];
    }

    /**
     * @return array
     */
    public function unitOptions(): array
    {
        return [
            0 => __("Octets"),
            1 => __("Kilo Octets"),
            2 => __("Mega Octets"),
            3 => __("Giga Octets"),
        ];
    }

    /**
     * Defines what to execute once the Form is processed
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        if ($this->_indicatorsData === null) {
            throw new Exception('use setIndicatorsData($data) before executing the form');
        }

        $pow = (int)$data['unit'];
        $divider = pow(1024, $pow);
        $unit = $this->getUnit($pow);
        $header = [
            $data['by'] === IndicatorsController::TRANSFERRING ? __("Service versant") : __("Service producteur"),
            __("Transferts Volume total ({0})", $unit),
            __("Transferts Nb PJs"),
            __("Transferts Nb"),
            __("Transferts Délai max de traitement"),
            __("Transferts Nb rejeté"),
            __("Archives volume total ({0})", $unit),
            __("Archives volume doc. ({0})", $unit),
            __("Archives volume conservation ({0})", $unit),
            __("Archives volume diffusion ({0})", $unit),
            __("Archives volume horodatage ({0})", $unit),
            __("Archives Nb Archives"),
            __("Archives Nb Unités Archives"),
            __("Archives Nb doc."),
            __("Communications Volume total ({0})", $unit),
            __("Communications Nb de documents"),
            __("Communications Nb"),
            __("Communications Nb de demandes rejetées"),
            __("Communications Nb de dérogations instruites"),
            __("Communications Nb de dérogations rejetées"),
            __("Éliminations Volume total ({0})", $unit),
            __("Éliminations Nb doc."),
            __("Éliminations Nb"),
            __("Éliminations Nb de demande rejetées"),
            __("Restitutions Volume total ({0})", $unit),
            __("Restitutions Nb doc."),
            __("Restitutions Nb"),
            __("Restitutions Nb de demande rejetées"),
            __("Transferts sortants Volume total ({0})", $unit),
            __("Transferts sortants Nb PJs"),
            __("Transferts sortants Nb"),
            __("Transferts sortants Délai max de traitement"),
            __("Transferts sortants Nb de rejetées"),
        ];

        $this->_indicatorsData = array_map(
            function (EntityInterface $e) use ($divider) {
                if (Hash::get($e, 'type_entity.code') === 'SO') {
                    return [
                        $e->get('name'),
                    ];
                }
                $count = function (string $path) use ($e) {
                    return (string)((int)Hash::get($e, $path));
                };
                $size = function (string $path) use ($e, $divider) {
                    return
                        Number::format(((int)Hash::get($e, $path)) / $divider, ['precision' => 2, 'pattern' => '#.##'])
                        . ' {escape}'; // force l'échapement des floats
                };
                $date = function (string $path) use ($e) {
                    return (new CakeDateTime(Hash::get($e, $path) ?? 0))->format(
                        'H:i:s'
                    );
                };
                return [
                    $e->get('name'),
                    $size('indicators.transfers_volume'),
                    // original_files_size
                    $count('indicators.transfers_pj_count'),
                    // original_files_count
                    $count('indicators.transfers_count'),
                    // message_count
                    $date('indicators.transfers_max_traitement'),
                    // validation_duration
                    $count('indicators.transfers_rejected_count'),
                    // message_count
                    $size('indicators.archives_volume_total'),
                    // original_size + timestamp_size
                    $size('indicators.archives_volume_doc'),
                    // original_size
                    $size('indicators.archives_volume_cons'),
                    // preservation_size
                    $size('indicators.archives_volume_diff'),
                    // dissemination_size
                    $size('indicators.archives_volume_horo'),
                    // timestamp_size
                    $count('indicators.archives_count'),
                    // archive_count
                    $count('indicators.archives_unit_count'),
                    // units_count
                    $count('indicators.archives_doc_count'),
                    // original_count
                    $size('indicators.communications_volume_total'),
                    // original_files_size
                    $count('indicators.communications_doc_count'),
                    // original_files_count
                    $count('indicators.communications_count'),
                    // message_count
                    $count('indicators.communications_request_rejected'),
                    // message_count
                    $count('indicators.communications_derogation_intructed'),
                    // message_count
                    $count('indicators.communications_derogation_rejected'),
                    // message_count
                    $size('indicators.eliminations_volume_total'),
                    // original_files_size
                    $count('indicators.eliminations_doc_count'),
                    // original_files_count
                    $count('indicators.eliminations_count'),
                    // message_count
                    $count('indicators.eliminations_rejected'),
                    // message_count
                    $size('indicators.restitutions_volume_total'),
                    // original_files_size
                    $count('indicators.restitutions_doc_count'),
                    // original_files_count
                    $count('indicators.restitutions_count'),
                    // message_count
                    $count('indicators.restitutions_rejected'),
                    // message_count
                    $size('indicators.outgoing_volume_total'),
                    // original_files_size
                    $count('indicators.outgoing_doc_count'),
                    // original_files_count
                    $count('indicators.outgoing_count'),
                    // message_count
                    $date('indicators.outgoing_max_traitement'),
                    // validation_duration
                    $count('indicators.outgoing_rejected'),
                    // message_count
                ];
            },
            $this->_indicatorsData
        );

        $date = (new DateTime());
        $title = __(
            "Indicateurs volumétriques de asalae par service {0} produits le {1} à {2}",
            $data['by'] === IndicatorsController::TRANSFERRING ? __("versant") : __("producteur"),
            $date->format('d-m-Y'),
            $date->format('H:i:s')
        );

        $filter = empty($this->_filters)
            ? __("Aucun filtre activé")
            : __("Filtres : ") . $this->translateFilters();

        array_unshift(
            $this->_indicatorsData,
            [$title],
            [$filter], // résumé des filtres
            [Router::url(null, true)], // url de la requete avec les filtres
            [''], // empty line
            $header // nom des colonnes
        );

        $viewBuilder = new ViewBuilder();
        $this->csv = $viewBuilder
            ->setClassName('CsvView.Csv')
            ->setVar('data', $this->_indicatorsData)
            ->setOptions(
                [
                    'serialize' => 'data',
                    'delimiter' => $data['delimiter'],
                    'enclosure' => $data['enclosure'],
                ]
            )
            ->build()
            ->render();

        // force l'échapement des floats
        $this->csv = str_replace(' {escape}', '', $this->csv);

        return true;
    }

    /**
     * Conversion de l'unité
     * @param int $param
     * @return string
     */
    public function getUnit(int $param): string
    {
        return [
            0 => 'Octets',
            1 => 'Ko',
            2 => 'Mo',
            3 => 'Go',
        ][$param];
    }

    /**
     * Filtres lisibles pour l'affichage dans le csv
     * @return string
     */
    protected function translateFilters(): string
    {
        return implode(
            ', ',
            array_map(
                function ($v, $f) {
                    return [
                        'transferring_agency' => __("Service versant"),
                        'agreement' => __("Accord de versement"),
                        'profile' => __("Profil d'archives"),
                        'date_begin' => __("Date de départ"),
                        'date_end' => __("Date de fin"),
                    ][$f] . '=' . $v;
                },
                $this->_filters,
                array_keys($this->_filters)
            )
        );
    }

    /**
     * @param array $indicatorsData
     * @param array $filters
     */
    public function setIndicatorsData(
        array $indicatorsData,
        array $filters = []
    ) {
        $this->_indicatorsData = $indicatorsData;
        $this->_filters = $filters;
    }
}
