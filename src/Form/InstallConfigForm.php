<?php

/**
 * Asalae\Form\InstallConfigForm
 */

namespace Asalae\Form;

use Asalae\Controller\Component\EmailsComponent;
use Asalae\Model\Entity\Eaccpf;
use Asalae\Model\Table\AgreementsTable;
use Asalae\Model\Table\ArchivingSystemsTable;
use Asalae\Model\Table\ArosAcosTable;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\CronsTable;
use Asalae\Model\Table\EaccpfsTable;
use Asalae\Model\Table\KeywordListsTable;
use Asalae\Model\Table\KeywordsTable;
use Asalae\Model\Table\LdapsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Model\Table\OrgEntitiesTimestampersTable;
use Asalae\Model\Table\ProfilesTable;
use Asalae\Model\Table\RolesTable;
use Asalae\Model\Table\RolesTypeEntitiesTable;
use Asalae\Model\Table\SecureDataSpacesTable;
use Asalae\Model\Table\ServiceLevelsTable;
use Asalae\Model\Table\TimestampersTable;
use Asalae\Model\Table\TypeEntitiesTable;
use Asalae\Model\Table\UsersTable;
use Asalae\Model\Table\ValidationActorsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationStagesTable;
use Asalae\Model\Table\VolumesTable;
use AsalaeCore\Cron\Pronom;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\Config;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\Http\ServerRequest;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\Mailer\Transport\MailTransport;
use Cake\Mailer\Transport\SmtpTransport;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use Cake\Validation\Validator;
use ErrorException;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Throwable;

/**
 * Formulaire d'installation par fichier ini
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class InstallConfigForm extends Form
{
    /**
     * @var callable permet le feedback
     */
    public $stdOut;

    /**
     * @var bool Permet d'ajouter des éléments suite à une installation
     */
    public $postInstall = false;

    /**
     * overwrite mode for import
     * @var bool
     */
    public $overwrite;

    /**
     * headless mode
     * @var bool
     */
    public $headless;

    /**
     * @var array représentation flatten du data du ini
     */
    private $iniData = [];

    /**
     * Surchage du execute
     * @param array $data
     * @param array $options
     * @return bool
     * @throws Exception
     */
    public function execute(array $data, array $options = []): bool
    {
        $this->setData($data);
        return parent::execute($this->_data, $options);
    }

    /**
     * Set form data.
     *
     * @param array $data Data array.
     * @return $this
     */
    public function setData(array $data)
    {
        // retrocompatibilité
        if (isset($data['email__method'])) {
            if ($data['email__method'] === 'mail') {
                $data['email__method'] = MailTransport::class;
            } elseif ($data['email__method'] === 'smtp') {
                $data['email__method'] = SmtpTransport::class;
            }
        }
        $this->_data = $data;

        return $this;
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        if ($this->postInstall) {
            return $validator;
        }
        $this->validationExploitation($validator);
        $this->validationConfig($validator);
        $this->validationProxy($validator);
        $this->validationApp($validator);
        $this->validationRatchet($validator);
        $this->validationDatabase($validator);
        $this->validationSecurity($validator);
        $this->validationEmail($validator);
        $this->validationAdmins($validator);
        $this->validationTimestamp($validator);
        $this->validationTimestampers($validator);
        $this->validationFiles($validator);
        return $validator;
    }

    /**
     * Validation pour la section exploitation_service
     * @param Validator $validator
     * @return void
     */
    private function validationExploitation(Validator $validator): void
    {
        $prefix = 'exploitation_service__';
        $validator
            ->requirePresence($prefix . 'name')
            ->notEmptyString($prefix . 'name');
        $validator
            ->requirePresence($prefix . 'identifier')
            ->notEmptyString($prefix . 'identifier');
        $validator
            ->requirePresence($prefix . 'installer_name')
            ->notEmptyString($prefix . 'installer_name');
    }

    /**
     * Validation pour la section config
     * @param Validator $validator
     * @return void
     */
    private function validationConfig(Validator $validator): void
    {
        $prefix = 'config__';
        $validator
            ->requirePresence($prefix . 'config_file')
            ->notEmptyString($prefix . 'config_file')
            ->add(
                'config_file',
                'is_writable',
                $this->ruleIsWritable()
            );
        $validator
            ->requirePresence($prefix . 'data_dir')
            ->notEmptyString($prefix . 'data_dir')
            ->add(
                $prefix . 'data_dir',
                'is_writable',
                $this->ruleIsWritable()
            );
        $validator
            ->requirePresence($prefix . 'admin_file')
            ->notEmptyString($prefix . 'admin_file')
            ->add(
                $prefix . 'admin_file',
                'is_writable',
                $this->ruleIsWritable()
            );
    }

    /**
     * Vérifie qu'un fichier/son dossier soit inscriptible
     * @return array
     */
    public function ruleIsWritable(): array
    {
        return [
            'rule' => function ($v) {
                if (is_file($v)) {
                    return is_writable($v)
                        ?: __(
                            "Le fichier n'est pas inscriptible"
                        );
                }
                $dir = $v;
                while (!is_dir($dir)) {
                    $lastdir = $dir;
                    $dir = dirname($dir);
                    if ($dir === $lastdir || $dir === '/' || $dir === '.') {
                        return false;
                    }
                }
                return is_writable($dir);
            },
            'message' => __("Le dossier n'est pas inscriptible"),
        ];
    }

    /**
     * Validation pour la section proxy
     * @param Validator $validator
     * @return void
     */
    private function validationProxy(Validator $validator): void
    {
        $prefix = 'proxy__';
        $validator
            ->allowEmptyString($prefix . 'host');
        $validator
            ->allowEmptyString($prefix . 'port')
            ->nonNegativeInteger($prefix . 'port');
        $validator
            ->allowEmptyString($prefix . 'username');
        $validator
            ->allowEmptyString($prefix . 'password');
    }

    /**
     * Validation pour la section app
     * @param Validator $validator
     * @return void
     */
    private function validationApp(Validator $validator): void
    {
        $prefix = 'app__';
        $validator
            ->requirePresence($prefix . 'locale')
            ->notEmptyString($prefix . 'locale');
        $validator
            ->requirePresence($prefix . 'timezone')
            ->notEmptyString($prefix . 'timezone');
        $validator
            ->requirePresence($prefix . 'url')
            ->notEmptyString($prefix . 'url');
    }

    /**
     * Validation pour la section ratchet
     * @param Validator $validator
     * @return void
     */
    private function validationRatchet(Validator $validator): void
    {
        $prefix = 'ratchet__';
        $validator
            ->requirePresence($prefix . 'url_ws')
            ->notEmptyString($prefix . 'url_ws');
    }

    /**
     * Validation pour la section database
     * @param Validator $validator
     * @return void
     */
    private function validationDatabase(Validator $validator): void
    {
        $prefix = 'database__';
        $validator
            ->requirePresence($prefix . 'host')
            ->notEmptyString($prefix . 'host');
        $validator
            ->requirePresence($prefix . 'database')
            ->notEmptyString($prefix . 'database');
    }

    /**
     * Validation pour la section security
     * @param Validator $validator
     * @return void
     */
    private function validationSecurity(Validator $validator): void
    {
        $prefix = 'security__';
        $validator
            ->requirePresence($prefix . 'type')
            ->notEmptyString($prefix . 'type')
            ->inList($prefix . 'type', ['set', 'random', 'hash']);
    }

    /**
     * Validation pour la section email
     * @param Validator $validator
     * @return void
     */
    private function validationEmail(Validator $validator): void
    {
        $prefix = 'email__';
        $validator
            ->requirePresence($prefix . 'from')
            ->notEmptyString($prefix . 'from')
            ->email($prefix . 'from');
        $validator
            ->requirePresence($prefix . 'method')
            ->notEmptyString($prefix . 'method')
            ->inList($prefix . 'method', [MailTransport::class, SmtpTransport::class]);
        $validator
            ->requirePresence($prefix . 'host')
            ->notEmptyString($prefix . 'host');
        $validator
            ->requirePresence($prefix . 'port')
            ->notEmptyString($prefix . 'port')
            ->nonNegativeInteger($prefix . 'port');
    }

    /**
     * Validation pour la section admins
     * @param Validator $validator
     * @return void
     */
    private function validationAdmins(Validator $validator): void
    {
        $prefix = 'admins__';
        $expanded = Hash::expand($this->_data, '__');
        foreach (array_keys(Hash::get($expanded, 'admins', [])) as $key) {
            $validator
                ->requirePresence($prefix . $key . '__username')
                ->notEmptyString($prefix . $key . '__username');
            $validator->add(
                $prefix . $key . '__password',
                'custom',
                [
                    'rule' => function (string $name, array $context) use ($prefix, $key) {
                        return !isset($context['data'][$prefix . $key . '__hashed_password']);
                    },
                ]
            );
            $validator->add(
                $prefix . $key . '__hashed_password',
                'custom',
                [
                    'rule' => function (string $name, array $context) use ($prefix, $key) {
                        return !isset($context['data'][$prefix . $key . '__password']);
                    },
                ]
            );
        }
    }

    /**
     * Validation pour la section timestamp
     * @param Validator $validator
     * @return void
     */
    private function validationTimestamp(Validator $validator): void
    {
        $prefix = 'timestamp__';
        $validator->add(
            $prefix . 'name',
            'custom',
            [
                'rule' => function (string $name, array $context) {
                    foreach (array_keys($context['data']) as $key) {
                        if (substr($key, 0, strlen('timestamps__')) === 'timestamps__') {
                            return false;
                        }
                    }
                    return true;
                },
            ]
        );

        $isOneTimestamp = fn($context) => (bool)Hash::get($context, 'data.timestamp');

        $validator
            ->requirePresence($prefix . 'name', $isOneTimestamp)
            ->notEmptyString($prefix . 'name');
        $validator
            ->requirePresence($prefix . 'countryName', $isOneTimestamp)
            ->notEmptyString($prefix . 'countryName');
        $validator
            ->requirePresence($prefix . 'stateOrProvinceName', $isOneTimestamp)
            ->notEmptyString($prefix . 'stateOrProvinceName');
        $validator
            ->requirePresence($prefix . 'localityName', $isOneTimestamp)
            ->notEmptyString($prefix . 'localityName');
        $validator
            ->requirePresence($prefix . 'organizationName', $isOneTimestamp)
            ->notEmptyString($prefix . 'organizationName');
        $validator
            ->requirePresence($prefix . 'organizationalUnitName', $isOneTimestamp)
            ->notEmptyString($prefix . 'organizationalUnitName');
        $validator
            ->requirePresence($prefix . 'commonName', $isOneTimestamp)
            ->notEmptyString($prefix . 'commonName');
        $validator
            ->requirePresence($prefix . 'emailAddress', $isOneTimestamp)
            ->notEmptyString($prefix . 'emailAddress');
        $validator
            ->requirePresence($prefix . 'extfile', $isOneTimestamp)
            ->notEmptyString($prefix . 'extfile');
        $validator
            ->requirePresence($prefix . 'directory', $isOneTimestamp)
            ->notEmptyString($prefix . 'directory')
            ->add(
                $prefix . 'directory',
                'is_writable',
                $this->ruleIsWritable()
            );
        $validator
            ->requirePresence($prefix . 'cnf', $isOneTimestamp)
            ->notEmptyString($prefix . 'cnf');
    }

    /**
     * @param Validator $validator
     * @return void
     */
    private function validationTimestampers(Validator $validator): void
    {
        $prefix = 'timestampers__';

        $expanded = Hash::expand($this->_data, '__');
        foreach (array_keys(Hash::get($expanded, 'timestampers', [])) as $key) {
            $validator->add(
                $prefix . $key . '__name',
                'custom',
                [
                    'rule' => function (string $name, array $context) {
                        return !isset($context['data']['timestamp__name']);
                    },
                ]
            );
            $validator
                ->requirePresence($prefix . $key . '__name')
                ->notEmptyString($prefix . $key . '__name');
            $validator->notEmptyString($prefix . $key . '__fields');
            $validator->add(
                $prefix . $key . '__fields',
                'custom',
                [
                    'rule' => function ($value) {
                        if (empty($value)) {
                            return false;
                        }
                        try {
                            json_decode($value);
                            return true;
                        } catch (Exception) {
                            return false;
                        }
                    },
                    'message' => __("N'est pas un JSON valide"),
                ]
            );
        }
    }

    /**
     * Validation pour les fichier en base64 dans le fichier
     * @param Validator $validator
     * @return void
     */
    private function validationFiles(Validator $validator): void
    {
        $prefix = 'files__';
        $expanded = Hash::expand($this->_data, '__');
        foreach (array_keys(Hash::get($expanded, 'files', [])) as $file) {
            $validator
                ->requirePresence($prefix . $file . '__path')
                ->notEmptyString($prefix . $file . '__path')
                ->requirePresence($prefix . $file . '__content')
                ->notEmptyString($prefix . $file . '__content')
                ->add(
                    $prefix . $file . '__path',
                    'is_writable',
                    $this->ruleIsWritable()
                );
        }
    }

    /**
     * Ne passe pas par la validation
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public function force(array $data): bool
    {
        return $this->_execute($data);
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $this->iniData = Hash::expand($data, '__');
        if (!$this->postInstall) {
            $this->buildConfigFile();
            $this->loadConfig();
            $this->buildAdministratorsJson();
            $this->buildDatabase();
            $this->loadDatabase();
            $this->buildTimestamper();
            $this->buildTimestampers();
            $this->buildExploitation();
            $this->updatePronoms();
        }

        if (isset($this->iniData['volumes'])) {
            $this->buildVolumes();
        }
        if (isset($this->iniData['secure_data_spaces'])) {
            $this->buildSecureDataSpaces();
        }
        if (isset($this->iniData['archival_agencies'])) {
            $this->buildArchivalAgencies();
        }
        if (isset($this->iniData['ldaps'])) {
            $this->buildLdaps();
        }
        if (isset($this->iniData['archiving_systems'])) {
            $this->buildArchivingSystems();
        }
        if (isset($this->iniData['org_entities'])) {
            $this->buildOrgEntities();
        }
        if (isset($this->iniData['roles'])) {
            $this->buildRoles();
        }
        if (isset($this->iniData['users'])) {
            $this->buildUsers();
        }
        if (isset($this->iniData['validations'])) {
            $this->buildValidations();
        }
        if (isset($this->iniData['profiles'])) {
            $this->buildProfiles();
        }
        if (isset($this->iniData['service_levels'])) {
            $this->buildServiceLevels();
        }
        if (isset($this->iniData['agreements'])) {
            $this->buildAgreements();
        }
        if (isset($this->iniData['configurations'])) {
            $this->buildConfigurations();
        }
        if (isset($this->iniData['keyword_lists'])) {
            $this->buildKeywordLists();
        }

        return true;
    }

    /**
     * Génère le fichier app_local.json
     * @throws Exception
     */
    private function buildConfigFile()
    {
        $configJson = [
            'debug' => (bool)$this->iniData['config']['debug'],
            'App.paths.data' => $this->iniData['config']['data_dir'],
            'App.paths.administrators_json' => $this->iniData['config']['admin_file'],
            'App.defaultLocale' => $this->iniData['app']['locale'],
            'App.timezone' => $this->iniData['app']['timezone'],
            'App.fullBaseUrl' => $this->iniData['app']['url'],
            'Ratchet.connect' => $this->iniData['ratchet']['url_ws'],
            'Datasources.default.host' => $this->iniData['database']['host'],
            'Datasources.default.port' => $this->iniData['database']['port'] ?? '5432',
            'Datasources.default.username' => $this->iniData['database']['username'],
            'Datasources.default.password' => $this->iniData['database']['password'],
            'Datasources.default.database' => $this->iniData['database']['database'],
            'Email.default.from' => $this->iniData['email']['from'],
            'EmailTransport.default.className' => ucfirst($this->iniData['email']['method']),
            'EmailTransport.default.host' => $this->iniData['email']['host'],
            'EmailTransport.default.port' => $this->iniData['email']['port'],
            'EmailTransport.default.username' => $this->iniData['email']['username'],
            'EmailTransport.default.password' => $this->iniData['email']['password'],
            'Proxy.host' => $this->iniData['proxy']['host'],
            'Proxy.port' => $this->iniData['proxy']['port'],
            'Proxy.username' => $this->iniData['proxy']['username'],
            'Proxy.password' => $this->iniData['proxy']['password'],
        ];
        switch ($this->iniData['security']['type']) {
            case 'set':
                $configJson['Security.salt'] = Hash::get($this->iniData, 'security.value');
                break;
            case 'random':
                $configJson['Security.salt'] = hash('sha256', uniqid('libriciel'));
                break;
            case 'hash':
                $configJson['Security.salt'] = hash(
                    'sha256',
                    'Libriciel-' . Hash::get($this->iniData, 'security.value')
                );
                break;
        }
        Security::setSalt($configJson['Security.salt']);
        ksort($configJson);
        $filename = Hash::get($this->iniData, 'config.config_file');
        $initialJson = file_exists($filename)
            ? (json_decode(file_get_contents($filename), true) ?: [])
            : [];
        Filesystem::dumpFile(
            $filename,
            json_encode(
                Hash::expand($configJson) + $initialJson,
                JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
            )
        );
        $this->out('written ' . $filename);
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        $escaped = addcslashes($filename, "'");
        Filesystem::dumpFile(
            $pathToLocalConfig,
            "<?php return '$escaped';?>"
        );
        $this->out('written ' . $pathToLocalConfig);
    }

    /**
     * Ecrit un message dans $this->output
     * @param string $output
     * @return void
     */
    public function out(string $output): void
    {
        if (is_callable($this->stdOut)) {
            call_user_func($this->stdOut, $output);
        }
    }

    /**
     * Recharge la config
     * @return void
     * @throws Exception
     */
    private function loadConfig(): void
    {
        Config::reset();
        Config::write(Config::readAll());
    }

    /**
     * Génère le fichier administrators.json
     * @return void
     * @throws Exception
     */
    private function buildAdministratorsJson(): void
    {
        $admins = array_map(
            function ($a) {
                $hasher = new DefaultPasswordHasher();
                $a['password'] = isset($a['password']) ? $hasher->hash($a['password']) : $a['hashed_password'];
                unset($a['hashed_password']);
                return $a;
            },
            Hash::get($this->iniData, 'admins', [])
        );
        $filename = Hash::get($this->iniData, 'config.admin_file');
        Filesystem::dumpFile($filename, json_encode(array_values($admins), JSON_PRETTY_PRINT));
        $this->out('written ' . $filename);
    }

    /**
     * Initialise la base de données
     * @return void
     * @throws Exception
     */
    private function buildDatabase(): void
    {
        $this->out('installing database...');
        $Exec = Utility::get('Exec');
        $Exec->rawCommand(
            ROOT . DS . 'bin' . DS . 'cake migrations migrate 2>&1'
            . '&& ' . ROOT . DS . 'bin' . DS . 'cake migrations seed 2>&1'
            . '&& ' . ROOT . DS . 'bin' . DS . 'cake update 2>&1'
            . '&& ' . ROOT . DS . 'bin' . DS . 'cake roles_perms import ' . RESOURCES . 'export_roles.json --keep 2>&1',
            $output,
            $code
        );
        $message = implode("<br>\n", $output);
        if ($code !== 0) {
            throw new Exception($message);
        }
        $this->out($message);
    }

    /**
     * Charge la base de donnée fraichement créée dans ConnectionManager
     * @return void
     * @throws Exception
     */
    private function loadDatabase(): void
    {
        /** @var ConnectionManager $ConnectionManager */
        $ConnectionManager = Utility::get(ConnectionManager::class);
        $ConnectionManager::drop('default');
        $ConnectionManager::setConfig(
            'default',
            Config::readAll('Datasources.default')
        );
    }

    /**
     * Horodatage local (ajout simple pour fichier .ini écrit à la main)
     * @return void
     * @throws Exception
     */
    private function buildTimestamper(): void
    {
        if (empty($this->iniData['timestamp'])) {
            return;
        }

        $this->out('installing timestamper...');
        $timestamper =& $this->iniData['timestamp'];
        /** @var TimestampersTable $Timestampers */
        $Timestampers = TableRegistry::getTableLocator()->get('Timestampers');
        if ($Timestampers->exists(['name' => $timestamper['name']])) {
            return;
        }
        $dir = $timestamper['directory'];
        $cnf = str_replace('{{dir}}', $dir, $timestamper['cnf']);
        Filesystem::dumpFile($dir . DS . 'asalae-tsa.cnf', $cnf);

        $tsaKey = openssl_pkey_new(
            [
                'private_key_bits' => 2048,
                'private_key_type' => OPENSSL_KEYTYPE_RSA,
            ]
        );
        $csr = openssl_csr_new(
            [
                "countryName" => $timestamper['countryName'],
                "stateOrProvinceName" => $timestamper['stateOrProvinceName'],
                "localityName" => $timestamper['localityName'],
                "organizationName" => $timestamper['organizationName'],
                "organizationalUnitName" => $timestamper['organizationalUnitName'],
                "commonName" => $timestamper['commonName'],
                "emailAddress" => $timestamper['emailAddress'],
            ],
            $tsaKey
        );
        openssl_pkey_export_to_file($tsaKey, $dir . DS . 'tsacert.pem');
        openssl_csr_export_to_file($csr, $dir . DS . 'tsacert.csr');

        Filesystem::dumpFile($dir . DS . 'extfile', $timestamper['extfile']);
        $Exec = Utility::get('Exec');
        $in = $Exec->escapeshellarg($dir . DS . 'tsacert.csr');
        $signkey = $Exec->escapeshellarg($dir . DS . 'tsacert.pem');
        $out = $Exec->escapeshellarg($dir . DS . 'tsacert.crt');
        $extfile = $Exec->escapeshellarg($dir . DS . 'extfile');
        $cmd = "openssl x509 -req -days 3650 -in $in -signkey $signkey -out $out -extfile $extfile 2>&1";
        $Exec->rawCommand($cmd, $output, $code);
        if ($code !== 0) {
            throw new Exception(implode(PHP_EOL, $output));
        }

        // Ajout de l'horodateur en base
        file_put_contents($dir . DS . 'serial', '00000001');
        $this->out('written ' . $in);
        $this->out('written ' . $signkey);
        $this->out('written ' . $out);
        $this->out('written ' . $extfile);
        $this->out('written ' . $dir . DS . 'serial');
        $entity = $Timestampers->newEntity(
            [
                'name' => $timestamper['name'],
                'fields' => json_encode(
                    [
                        'name' => $timestamper['name'],
                        'driver' => 'OPENSSL',
                        'ca' => $dir . DS . 'tsacert.crt',
                        'crt' => $dir . DS . 'tsacert.crt',
                        'pem' => $dir . DS . 'tsacert.pem',
                        'cnf' => $dir . DS . 'asalae-tsa.cnf',
                    ],
                    JSON_UNESCAPED_SLASHES
                ),
            ]
        );
        $Timestampers->saveOrFail($entity);
    }

    /**
     * Horodatages (si fichier .ini généré par export)
     * @return void
     * @throws Exception
     */
    private function buildTimestampers(): void
    {
        if (empty($this->iniData['timestampers'])) {
            return;
        }

        $this->out('installing timestampers...');
        $timestampers =& $this->iniData['timestampers'];
        /** @var TimestampersTable $Timestampers */
        $Timestampers = TableRegistry::getTableLocator()->get('Timestampers');
        foreach ($timestampers as $key => $timestamper) {
            $timestamperEntity = $Timestampers->find()
                ->where(['name' => $timestamper['name']])
                ->first();
            if (!$timestamperEntity) {
                $timestamperEntity = $Timestampers->saveOrFail(
                    $Timestampers->newEntity($timestamper)
                );
            }
            $timestampers[$key] = $timestamperEntity;
        }
        $this->buildFiles();
    }

    /**
     * Sauvegarde des fichiers encodés en prenant en compte l'option overwrite
     * @return void
     * @throws Exception
     */
    private function buildFiles(): void
    {
        foreach ($this->iniData['files'] as $file) {
            $this->buildOneFile($file);
        }
    }

    /**
     * Sauvegarde d'un fichier encodé en prenant en compte l'option overwrite
     * @param array $file
     * @return void
     * @throws Exception
     */
    protected function buildOneFile(array $file): void
    {
        $path = $file['path'];
        $content = base64_decode($file['content']);
        if (is_file($path) && md5_file($path) !== md5($content)) { // NOSONAR
            // si le fichier existe et qu'il est différent
            if ($this->overwrite) {
                Filesystem::remove($path);
                Filesystem::dumpFile($path, $content);
            } elseif (!$this->headless) {
                // prompt
                $display = __(
                    "Le fichier {0} existe et est différent, écraser ?",
                    $path
                );
                $in = $this->stdOut[0]->askChoice($display, ['yes', 'no']);
                if ($in === 'yes') {
                    Filesystem::remove($path);
                    Filesystem::dumpFile($path, $content);
                }
            }
        } elseif (!is_file($path)) { // s'il n'existe pas on le crée
            Filesystem::dumpFile($path, $content);
        }
    }

    /**
     * Ajout du service d'exploitation
     * @return void
     */
    private function buildExploitation(): void
    {
        $this->out('installing exploitation service...');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $seCode = $OrgEntities->getAssociation('TypeEntities')->find()
            ->select(['id'])
            ->where(['code IN' => OrgEntitiesTable::CODE_OPERATING_DEPARTMENT])
            ->firstOrFail()
            ->get('id');
        if ($OrgEntities->exists(['type_entity_id' => $seCode])) {
            return;
        }
        $entity = $OrgEntities->newEntity(
            [
                'name' => $this->iniData['exploitation_service']['name'],
                'identifier' => $this->iniData['exploitation_service']['identifier'],
                'type_entity_id' => $seCode,
            ]
        );
        $OrgEntities->saveOrFail($entity);

        $from_date = new CakeDateTime();
        $from_date->setTimezone(Configure::read('App.timezone', 'UTC'));
        $initialData = [
            'name' => $entity->get('name'),
            'record_id' => $entity->get('identifier'),
            'agency_name' => $entity->get('name'),
            'org_entity_id' => $entity->get('id'),
            'entity_type' => 'corporateBody',
            'from_date' => $from_date,
        ];
        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs');
        /** @var Eaccpf $eac */
        $eac = $Eaccpfs->newEntity($initialData);
        $Eaccpfs->initializeData(
            $eac,
            ['name' => $this->iniData['exploitation_service']['installer_name']]
        );
        $Eaccpfs->save($eac);
    }

    /**
     * Initialise la table pronoms
     * @return void
     * @throws Exception
     */
    private function updatePronoms(): void
    {
        $this->out('installing pronom table...');
        $loc = TableRegistry::getTableLocator();
        /** @var CronsTable $Crons */
        $Crons = $loc->get('Crons');
        $cron = $Crons->find()
            ->where(['classname' => Pronom::class])
            ->firstOrFail();
        if (Configure::read('Proxy.host')) {
            $cron->set('use_proxy', true);
            $Crons->saveOrFail($cron);
        }
        $Exec = Utility::get('Exec');
        $Exec->async(CAKE_SHELL, 'cron', 'run', $cron->id);
    }

    /**
     * Ajoute des volumes
     * @return void
     */
    private function buildVolumes(): void
    {
        $this->out('installing volumes...');
        $volumes =& $this->iniData['volumes'];
        /** @var VolumesTable $Volumes */
        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        foreach ($volumes as $key => $volume) {
            $volumeEntity = $Volumes->find()
                ->where(['name' => $volume['name']])
                ->first();
            if (!$volumeEntity) {
                $volume['max_disk_usage'] = (int)str_replace(' ', '', $volume['max_disk_usage'] ?? '');
                $volumeEntity = $Volumes->saveOrFail(
                    $Volumes->newEntity($volume)
                );
            }
            $volumes[$key] = $volumeEntity;
        }
    }

    /**
     * Ajoute des ECS
     * @return void
     */
    private function buildSecureDataSpaces(): void
    {
        $this->out('installing secure data spaces...');
        /** @var EntityInterface[] $volumes */
        $volumes =& $this->iniData['volumes'];
        $sds =& $this->iniData['secure_data_spaces'];
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = TableRegistry::getTableLocator()->get('SecureDataSpaces');
        foreach ($sds as $key => $secureDataSpace) {
            $sdsEntity = $SecureDataSpaces->find()
                ->where(['name' => $secureDataSpace['name']])
                ->first();
            if (!$sdsEntity) {
                $ids = [];
                foreach (array_filter(explode(',', $secureDataSpace['volumes'])) as $var) {
                    if (!isset($volumes[$var])) {
                        trigger_error('volume ' . $var . ' is not defined');
                        break 2;
                    }
                    $ids[] = $volumes[$var]->id;
                }
                $secureDataSpace['volumes'] = ['_ids' => $ids];
                $sdsEntity = $SecureDataSpaces->saveOrFail(
                    $SecureDataSpaces->newEntity($secureDataSpace)
                );
            }
            $sds[$key] = $sdsEntity;
        }
    }

    /**
     * Ajoute les service d'archives
     * @return void
     * @throws Exception
     */
    private function buildArchivalAgencies(): void
    {
        $this->out('installing archival agencies...');

        $aas =& $this->iniData['archival_agencies'];
        /** @var EntityInterface[] $sds */
        $sds =& $this->iniData['secure_data_spaces'];
        $loc = TableRegistry::getTableLocator();
        /** @var OrgEntitiesTable $ArchivalAgencies */
        $ArchivalAgencies = $loc->get('OrgEntities');
        /** @var SecureDataSpacesTable $SecureDataSpaces */
        $SecureDataSpaces = $loc->get('SecureDataSpaces');
        /** @var TimestampersTable $TypeEntities */
        $Timestampers = $ArchivalAgencies->getAssociation('Timestampers');
        /** @var OrgEntitiesTimestampersTable $OrgEntitiesTimestampers */
        $OrgEntitiesTimestampers = $loc->get('OrgEntitiesTimestampers');
        /** @var EaccpfsTable $Eaccpfs */
        $Eaccpfs = $loc->get('Eaccpfs');
        /** @var TypeEntitiesTable $TypeEntities */
        $TypeEntities = $ArchivalAgencies->getAssociation('TypeEntities');

        $saCode = $TypeEntities->find()
            ->select(['id'])
            ->where(['code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES])
            ->firstOrFail()
            ->get('id');

        // récupération de l'id du SE
        $seCode = $TypeEntities->find()
            ->select(['id'])
            ->where(['code IN' => OrgEntitiesTable::CODE_OPERATING_DEPARTMENT])
            ->firstOrFail()
            ->get('id');
        $seId = $ArchivalAgencies->find()
            ->select(['id'])
            ->where(['type_entity_id' => $seCode])
            ->firstOrFail()
            ->get('id');

        foreach ($aas as $aakey => $archivalAgency) {
            $archivalAgencyEntity = $ArchivalAgencies->find()
                ->where(['identifier' => $archivalAgency['identifier']])
                ->first();
            if (!$archivalAgencyEntity) {
                $timestamper = $Timestampers->find()
                    ->select(['id'])
                    ->where(
                        !empty($archivalAgency['timestamper'])
                            ? ['name' => $archivalAgency['timestamper']]
                            : []
                    )
                    // TODO: dans le cas d'une install via export,
                    // ou alors mettre une property dès le début pour déterminer dans quel type d'install on est ?
                    // pas mal de refacto du coup ...
                    // une archival_agency sans timestamper_id va avoir le premier par défaut
                    // pas très grave mais pas iso
                    ->orderBy(['id' => 'asc'])
                    ->firstOrFail()
                    ->get('id');

                $archivalAgency += [
                    'parent_id' => $seId,
                    'type_entity_id' => $saCode,
                    'timestamper_id' => $timestamper,
                    'active' => (bool)($archivalAgency['active'] ?? true),
                    'is_main_archival_agency' => (bool)$archivalAgency['is_main_archival_agency'],
                ];
                $archivalAgencyEntity = $ArchivalAgencies->saveOrFail(
                    $ArchivalAgencies->newEntity($archivalAgency)
                );

                if (!empty($archivalAgency['timestampers'])) {
                    foreach (array_filter(explode(',', $archivalAgency['timestampers'])) as $timestampKey) {
                        $timestamper = $Timestampers->find()
                            ->select(['id'])
                            ->where(['name' => $this->iniData['timestampers'][$timestampKey]['name']])
                            ->orderBy(['id' => 'asc'])
                            ->firstOrFail()
                            ->get('id');
                        $OrgEntitiesTimestampers
                            ->saveOrFail(
                                $OrgEntitiesTimestampers->newEntity(
                                    [
                                        'org_entity_id' => $archivalAgencyEntity->id,
                                        'timestamper_id' => $timestamper,
                                    ]
                                )
                            );
                    }
                } else {
                    $OrgEntitiesTimestampers
                        ->saveOrFail(
                            $OrgEntitiesTimestampers->newEntity(
                                [
                                    'org_entity_id' => $archivalAgencyEntity->id,
                                    'timestamper_id' => $timestamper,
                                ]
                            )
                        );
                }

                $ds = [];
                foreach (array_filter(explode(',', $archivalAgency['secure_data_spaces'])) as $var) {
                    if (!isset($sds[$var])) {
                        throw new Exception('secure_data_space ' . $var . ' is not defined');
                    }
                    $sds[$var]->set('org_entity_id', $archivalAgencyEntity->id);
                    $SecureDataSpaces->saveOrFail($sds[$var]);
                    $ds[] = $sds[$var];
                }
                $archivalAgencyEntity->set('secure_data_spaces', $ds);

                $from_date = new CakeDateTime();
                $from_date->setTimezone(Configure::read('App.timezone', 'UTC'));
                $initialData = [
                    'name' => $archivalAgencyEntity->get('name'),
                    'record_id' => $archivalAgencyEntity->get('identifier'),
                    'agency_name' => $archivalAgencyEntity->get('name'),
                    'org_entity_id' => $archivalAgencyEntity->get('id'),
                    'entity_type' => 'corporateBody',
                    'from_date' => $from_date,
                ];
                /** @var Eaccpf $eac */
                $eac = $Eaccpfs->newEntity($initialData);
                $Eaccpfs->initializeData(
                    $eac,
                    [
                        'name' => Hash::get($this->iniData, 'exploitation_service.installer_name'),
                    ]
                );
                $Eaccpfs->save($eac);
                $archivalAgencyEntity->set('eaccpf', $eac);
            }
            $aas[$aakey] = $archivalAgencyEntity;
        }
    }

    /**
     * Ajoute les ldaps
     * @return void
     */
    private function buildLdaps(): void
    {
        $this->out('installing ldaps...');
        $ldaps =& $this->iniData['ldaps'];
        $loc = TableRegistry::getTableLocator();
        /** @var LdapsTable $Ldaps */
        $Ldaps = $loc->get('Ldaps');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        foreach ($ldaps as $ldap) {
            $oe = $OrgEntities->find()
                ->where(['identifier' => $ldap['org_entity']])
                ->first();
            if (!$Ldaps->exists(['name' => $ldap['name']])) {
                $l = [
                    'org_entity_id' => $oe ? $oe->id : null,
                    'follow_referrals' => (bool)$ldap['follow_referrals'],
                ] + $ldap;
                $Ldaps->saveOrFail($Ldaps->newEntity($l));
            }
        }
    }

    /**
     * Ajoute les Systemes d'Archivage
     * @return void
     */
    private function buildArchivingSystems(): void
    {
        $this->out('installing archiving systems...');
        $archivingSystems =& $this->iniData['archiving_systems'];
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivingSystemsTable $ArchivingSystems */
        $ArchivingSystems = $loc->get('ArchivingSystems');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        foreach ($archivingSystems as $archivingSystem) {
            $oe = $OrgEntities->find()
                ->where(['identifier' => $archivingSystem['org_entity']])
                ->firstOrFail();
            if (
                !$ArchivingSystems->exists(
                    [
                        'org_entity_id' => $oe->id,
                        'name' => $archivingSystem['name'],
                    ]
                )
            ) {
                $as = [
                    'org_entity_id' => $oe->id,
                ] + $archivingSystem;
                $ArchivingSystems->saveOrFail($ArchivingSystems->newEntity($as));
            }
        }
    }

    /**
     * Ajoute les entités
     * @return void
     */
    private function buildOrgEntities(): void
    {
        $this->out('installing entities...');
        $oes =& $this->iniData['org_entities'];
        $loc = TableRegistry::getTableLocator();
        /** @var TypeEntitiesTable $TypeEntities */
        $TypeEntities = $loc->get('TypeEntities');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        foreach ($oes as $oe) {
            $sa = $OrgEntities->find()
                ->innerJoinWith('TypeEntities')
                ->where(['identifier' => $oe['archival_agency']])
                ->andWhere(['code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES])
                ->firstOrFail();
            $existingEntity = (bool)count(
                $OrgEntities->find()
                    ->select(['existing' => 1])
                    ->innerJoinWith('ArchivalAgencies')
                    ->where(
                        [
                            'ArchivalAgencies.id' => $sa->id,
                            'OrgEntities.identifier' => $oe['identifier'],
                        ]
                    )
                    ->limit(1)
                    ->disableHydration()
                    ->toArray()
            );
            if (!$existingEntity) {
                $org_entity_id = $OrgEntities->find()
                    ->select(['id'])
                    ->where(['identifier' => $oe['parent']])
                    ->andWhere(
                        [
                            'lft >=' => $sa->get('lft'),
                            'rght <=' => $sa->get('rght'),
                        ]
                    )
                    ->firstOrFail()
                    ->get('id');
                $type_entity_id = $TypeEntities->find()
                    ->select(['id'])
                    ->where(['code' => $oe['type_entity']])
                    ->firstOrFail()
                    ->get('id');
                $d = [
                    'parent_id' => $org_entity_id,
                    'type_entity_id' => $type_entity_id,
                ] + $oe;
                $newOrgEntity = $OrgEntities->saveOrFail($OrgEntities->newEntity($d));

                // création de l'EAC-CPF
                $seName = $OrgEntities->find()
                    ->contain(['TypeEntities'])
                    ->select(['OrgEntities.name'])
                    ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_OPERATING_DEPARTMENT])
                    ->firstOrFail()
                    ->get('name');
                $from_date = new CakeDateTime();
                $from_date->setTimezone(Configure::read('App.timezone', 'UTC'));
                $initialData = [
                    'name' => $newOrgEntity->get('name'),
                    'record_id' => $newOrgEntity->get('identifier'),
                    'agency_name' => $seName,
                    'org_entity_id' => $newOrgEntity->get('id'),
                    'entity_type' => 'corporateBody',
                    'from_date' => $from_date,
                ];
                /** @var EaccpfsTable $Eaccpfs */
                $Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs');
                /** @var Eaccpf $eac */
                $eac = $Eaccpfs->newEntity($initialData);
                $Eaccpfs->initializeData(
                    $eac,
                    ['name' => $seName]
                );
                $Eaccpfs->save($eac);
            }
        }
    }

    /**
     * Ajoute les roles
     * @return void
     * @throws ErrorException
     */
    protected function buildRoles(): void
    {
        $this->out('installing roles...');
        $roles =& $this->iniData['roles'];
        $loc = TableRegistry::getTableLocator();
        /** @var RolesTable $Roles */
        $Roles = $loc->get('Roles');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        /** @var TypeEntitiesTable $TypeEntities */
        $TypeEntities = $loc->get('TypeEntities');
        $roleAdded = false;
        foreach ($roles as &$role) {
            $oe = $OrgEntities->find()
                ->where(['identifier' => $role['org_entity']])
                ->firstOrFail();
            if (!$Roles->exists(['name' => $role['name'], 'org_entity_id' => $oe->id])) {
                $parent = $role['parent_role']
                    ? $Roles->find()
                        ->select(['id'])
                        ->where(['name' => $role['parent_role']])
                        ->andWhere(
                            [
                                'OR' => [
                                    'org_entity_id IS' => null,
                                    'org_entity_id' => $oe->id,
                                ],
                            ]
                        )
                        ->firstOrFail()
                        ->get('id')
                    : null;
                $typeEntities = $TypeEntities->find()
                    ->select(['id'])
                    ->where(['code IN' => array_filter(explode(',', $role['type_entities']))])
                    ->all()
                    ->extract('id')
                    ->toArray();
                $r = [
                    'org_entity_id' => $oe->id,
                    'parent_id' => $parent,
                    'hierarchical_view' => (bool)$role['hierarchical_view'],
                    'type_entities' => ['_ids' => $typeEntities],
                ] + $role;
                unset($r['type_entities']);
                $Roles->saveOrFail($r = $Roles->newEntity($r));
                foreach ($typeEntities as $typeEntityId) {
                    $loc->get('RolesTypeEntities')->findOrCreate(
                        [
                            'role_id' => $r->id,
                            'type_entity_id' => $typeEntityId,
                        ]
                    );
                }
                $role['id'] = $r->id;
                $roleAdded = true;
            }
        }
        if ($roleAdded) {
            $this->out('update permissions...');
            // update des permissions
            $Exec = Utility::get('Exec');
            $Exec->rawCommand(
                ROOT . DS . 'bin' . DS . 'cake update 2>&1',
                $output,
                $code
            );
            $message = implode("<br>\n", $output);
            if ($code !== 0) {
                throw new Exception($message);
            }
            if (isset($this->iniData['permissions'])) {
                $this->importPerms();
            }
        }
    }

    /**
     * Import des permissions de roles
     * @return void
     * @throws ErrorException
     */
    private function importPerms(): void
    {
        $import = $this->iniData['permissions'];
        $loc = TableRegistry::getTableLocator();
        /** @var RolesTable $Roles */
        $Roles = $loc->get('Roles');
        /** @var ArosAcosTable $Permissions */
        $Permissions = $loc->get('ArosAcos');
        $conn = $Roles->getConnection();
        $conn->begin();

        $error = false;
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        foreach ($import as $role => $accesses) {
            $roleEntity = $Roles->find()
                ->where(
                    [
                        'Roles.name' => $this->iniData['roles'][$role]['name'],
                        'org_entity_id IS NOT' => null,
                    ]
                )
                ->contain(['Aros'])
                ->firstOrFail();
            foreach ($accesses as $acosPath => $access) {
                /** @var string|array $aro Permissions->allow() la doc indique un string
                 *                         mais la fonction autorise les array
                 */
                $aro = [
                    'foreign_key' => $roleEntity->get('id'),
                    'model' => 'Roles',
                ];
                try {
                    $subAccess = array_filter(explode(',', $access));
                    foreach (['_create', '_read', '_update', '_delete'] as $id => $crud) {
                        $Permissions->allow(
                            $aro,
                            $acosPath,
                            $crud,
                            $subAccess[$id]
                        );
                    }
                } catch (Throwable) {
                    $error = true;
                    $this->out(
                        __(
                            "Erreur lors de l'import de {0}:{1} l'aco n'existe pas.",
                            $role,
                            $acosPath,
                            $subAccess[$id]
                        )
                    );
                }
                $this->out($role . ' : ' . $acosPath . ' : ' . $subAccess[$id]);
            }
        }
        restore_error_handler();

        if ($error) {
            $conn->rollback();
            $this->out(__("Une erreur a été détectée, rollback..."));
            throw new Exception();
        }

        $conn->commit();
        $this->out(__("L'import s'est effectué avec succès"));
    }

    /**
     * Ajoute des utilisateurs aux service d'archives
     * @return void
     * @throws Exception
     */
    private function buildUsers(): void
    {
        $this->out('installing users...');
        $users =& $this->iniData['users'];
        $loc = TableRegistry::getTableLocator();
        /** @var UsersTable $Users */
        $Users = $loc->get('Users');
        /** @var RolesTable $Roles */
        $Roles = $loc->get('Roles');
        $defaultRoles = $Roles->find(
            'list',
            keyField: 'code',
            valueField: 'id'
        )
            ->where(['code IS NOT' => null])
            ->all()
            ->toArray();
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        $registry = new ComponentRegistry(new Controller(new ServerRequest()));
        $Emails = new EmailsComponent($registry);
        $hasher = new DefaultPasswordHasher();
        $seId = $OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_OPERATING_DEPARTMENT])
            ->firstOrFail()
            ->get('id');

        foreach ($users as $key => $user) {
            if ($Users->exists(['username' => $user['username']])) {
                $this->out(__("L'utilisateur ''{0}'' existe déjà dans l'application", $user['username']));
                // 'super_archivist_of' = liste des "org_entity_$identifier", comma separated
                if ($user['super_archivist_of'] ?? '' && $this->postInstall) {
                    $aas = array_filter(explode(',', $user['super_archivist_of'] ?? ''));
                    $aaIdentifiers = array_map(fn($aa) => substr($aa, strlen('org_entity_')), $aas);
                    $super_archivist_of = implode(', ', $aaIdentifiers);
                    $this->out(
                        "\t" . __(
                            "(il est listé comme super archiviste des Services d'archives suivants : {0})",
                            $super_archivist_of
                        )
                    );
                }
                continue;
            }
            $conditions = ['OrgEntities.identifier' => $user['org_entity']];
            if (!empty($user['archival_agency'])) {
                $conditions['ArchivalAgencies.identifier'] = $user['archival_agency'];
            }
            $userOrgEntity = $OrgEntities->find()
                ->contain(['ArchivalAgencies'])
                ->where($conditions)
                ->firstOrFail();
            $user['org_entity_id'] = $userOrgEntity->get('id');
            if (array_key_exists($user['role'], $defaultRoles)) {
                $user['role_id'] = $defaultRoles[$user['role']];
            } else {
                $userRole = $Roles->find()
                    ->where(
                        [
                            'org_entity_id' => $userOrgEntity->get('archival_agency')->get('id'),
                            'name' => $user['role'],
                        ]
                    )->firstOrFail();
                $user['role_id'] = $userRole->get('id');
            }
            /** @var RolesTypeEntitiesTable $RolesTypeEntities */
            $RolesTypeEntities = $loc->get('RolesTypeEntities');
            if (
                !$RolesTypeEntities->exists(
                    [
                        'role_id' => $user['role_id'],
                        'type_entity_id' => $userOrgEntity->get('type_entity_id'),
                    ]
                )
            ) {
                throw new Exception(
                    __(
                        "le rôle {0} n'est pas autorisé pour l'utilisateur {1}",
                        $user['role'],
                        $user['username']
                    )
                );
            }
            $user['active'] = (bool)($user['org_entity'] ?? true);
            $sendMail = false;
            if (empty($user['password']) && empty($user['hashed_password'])) {
                $user['password'] = $Users->generatePassword();
                $sendMail = true;
            } else {
                $user['password'] = isset($user['password'])
                    ? $hasher->hash($user['password'])
                    : $user['hashed_password'];
            }

            // super archivist
            if ($aas = array_filter(explode(',', $user['super_archivist_of'] ?? ''))) {
                $aaIdentifiers = array_map(
                    fn($aa) => $this->iniData['archival_agencies'][$aa]['identifier'],
                    $aas
                );
                $user['archival_agencies']['_ids'] = $OrgEntities->find()
                    ->select(['id'])
                    ->where(['identifier IN' => $aaIdentifiers ?: [null]])
                    ->all()
                    ->map(fn(EntityInterface $e) => $e->id)
                    ->toArray();
                $user['org_entity_id'] = $seId;
            }

            $u = $Users->newEntity($user);
            $u->set('password', $user['password'], ['setter' => false]);
            $users[$key] = $Users->saveOrFail($u);

            if ($sendMail) {
                $Emails->submitEmailNewUser($users[$key]);
            }
        }
    }

    /**
     * Ajoute les circuits de validation
     * @return void
     * @throws Exception
     */
    private function buildValidations(): void
    {
        $this->out('installing validations...');
        $validations =& $this->iniData['validations'];
        $loc = TableRegistry::getTableLocator();
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $loc->get('ValidationChains');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        foreach ($validations as $key => $validation) {
            $org_entity_id = $OrgEntities->find()
                ->innerJoinWith('TypeEntities')
                ->where(['identifier' => $validation['archival_agency']])
                ->andWhere(['code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES])
                ->firstOrFail()
                ->id;
            $chain = $ValidationChains->find()
                ->where(
                    [
                        'org_entity_id' => $org_entity_id,
                        'name' => $validation['name'],
                    ]
                )
                ->first();
            if (!$chain) {
                $chain = $ValidationChains->newEntity(
                    [
                        'org_entity_id' => $org_entity_id,
                        'name' => $validation['name'],
                        'description' => $validation['description'],
                        'app_type' => $validation['app_type'],
                        'default' => (bool)$validation['is_default'],
                        'active' => (bool)($validation['is_active'] ?? true),
                    ]
                );
                $ValidationChains->saveOrFail($chain);
                $this->buildValidationStages($validation, $chain);
            }
            $validations[$key] = $chain;
        }
    }

    /**
     * Ajoute les étapes de validation à un circuit
     * @param array           $validation
     * @param EntityInterface $chain
     * @return void
     * @throws Exception
     */
    private function buildValidationStages(array $validation, EntityInterface $chain): void
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ValidationStagesTable $ValidationStages */
        $ValidationStages = $loc->get('ValidationStages');
        $stages = array_filter(explode(',', $validation['stages']));
        $arr = [];
        foreach ($stages as $i => $stage) {
            $stageParams = $this->iniData['validation_stages'][$stage];
            $stage = $ValidationStages->newEntity(
                [
                    'validation_chain_id' => $chain->id,
                    'name' => $stageParams['name'],
                    'description' => $stageParams['description'],
                    'all_actors_to_complete' => $stageParams['all_actors_to_complete'],
                    'ord' => $i + 1,
                ]
            );
            $ValidationStages->saveOrFail($stage);
            $this->buildValidationActors($stageParams, $stage);
            $arr[] = $stage;
        }
        $chain->set('validation_stages', $arr);
    }

    /**
     * Ajoute les acteurs de validation à une étape de validation
     * @param array           $stageParams
     * @param EntityInterface $stage
     * @return void
     * @throws Exception
     */
    private function buildValidationActors(array $stageParams, EntityInterface $stage): void
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ValidationActorsTable $ValidationActors */
        $ValidationActors = $loc->get('ValidationActors');
        /** @var UsersTable $Users */
        $Users = $loc->get('Users');
        $actors = array_filter(explode(',', $stageParams['actors']));
        $arr = [];
        foreach ($actors as $actor) {
            $actorParams = $this->iniData['validation_actors'][$actor];
            $data = [
                'validation_stage_id' => $stage->id,
                'app_type' => $actorParams['type'],
            ];
            switch ($actorParams['type']) {
                case 'USER':
                    $user = $Users->find()
                        ->where(['Users.username' => $actorParams['user_username']])
                        ->firstOrFail();
                    $data['app_foreign_key'] = $user->id;
                    $data['type_validation'] = $actorParams['user_type_validation'];
                    break;
                case 'MAIL':
                    $data['app_key'] = $actorParams['mail_mail'];
                    $data['actor_name'] = $actorParams['mail_actor_name'];
                    break;
                case 'SERVICE_ARCHIVES':
                case 'SERVICE_DEMANDEUR':
                case 'SERVICE_PRODUCTEUR':
                    break;
                default:
                    throw new Exception("Unknown validation_actor type : " . $actorParams['app_type']);
            }
            $actor = $ValidationActors->newEntity($data);
            $ValidationActors->saveOrFail($actor);
            $arr[] = $actor;
        }
        $stage->set('validation_actors', $arr);
    }

    /**
     * Ajoute les profils
     * @return void
     */
    private function buildProfiles(): void
    {
        $this->out('installing profiles...');
        $profiles =& $this->iniData['profiles'];
        $loc = TableRegistry::getTableLocator();
        /** @var ProfilesTable $Profiles */
        $Profiles = $loc->get('Profiles');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        foreach ($profiles as $key => $profile) {
            $org_entity_id = $OrgEntities->find()
                ->innerJoinWith('TypeEntities')
                ->where(['identifier' => $profile['archival_agency']])
                ->andWhere(['code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES])
                ->firstOrFail()
                ->id;
            $profileEntity = $Profiles->find()
                ->where(
                    [
                        'org_entity_id' => $org_entity_id,
                        'identifier' => $profile['identifier'],
                    ]
                )
                ->first();
            if (!$profileEntity) {
                $profileEntity = $Profiles->newEntity(
                    [
                        'org_entity_id' => $org_entity_id,
                        'identifier' => $profile['identifier'],
                        'name' => $profile['name'],
                        'description' => $profile['description'],
                        'active' => (bool)($profile['active'] ?? true),
                    ]
                );
                $Profiles->saveOrFail($profileEntity);
            }
            $profiles[$key] = $profileEntity;
        }
    }

    /**
     * Ajoute les niveaux de service
     * @return void
     */
    private function buildServiceLevels(): void
    {
        $this->out('installing service levels...');
        $levels =& $this->iniData['service_levels'];
        $loc = TableRegistry::getTableLocator();
        /** @var ServiceLevelsTable $ServiceLevels */
        $ServiceLevels = $loc->get('ServiceLevels');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        /** @var TimestampersTable $Timestampers */
        $Timestampers = $loc->get('Timestampers');
        /** @var OrgEntitiesTimestampersTable $OT */
        $OT = $loc->get('OrgEntitiesTimestampers');
        $timestamper_id = $Timestampers->find()->orderBy(['id'])->first()->id;
        foreach ($levels as $key => $level) {
            $org_entity_id = $OrgEntities->find()
                ->innerJoinWith('TypeEntities')
                ->where(['identifier' => $level['archival_agency']])
                ->andWhere(['code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES])
                ->firstOrFail()
                ->id;
            $levelEntity = $ServiceLevels->find()
                ->where(
                    [
                        'org_entity_id' => $org_entity_id,
                        'identifier' => $level['identifier'],
                    ]
                )
                ->first();
            if (!$levelEntity) {
                $link_id = $OT->find()
                    ->where(
                        [
                            'org_entity_id' => $org_entity_id,
                            'timestamper_id' => $timestamper_id,
                        ]
                    )
                    ->first();
                $secure_data_space_id = $this->iniData['secure_data_spaces'][$level['secure_data_space']]->id;
                $getSLPolicies = function (array $level, string $type) use ($link_id, $OT) {
                    $data = array_filter(explode(',', $level[$type]));
                    return $level[$type]
                        ? (
                        $level[$type] === '1'
                            ? $link_id
                            : $OT->find()
                            ->contain(['OrgEntities', 'Timestampers'])
                            ->where(
                                [
                                    'OrgEntities.identifier' => $data[0],
                                    'Timestampers.name' => $data[1],
                                ]
                            )
                            ->firstOrFail()
                            ->id
                        )
                        : null;
                };
                $levelEntity = $ServiceLevels->newEntity(
                    [
                        'org_entity_id' => $org_entity_id,
                        'identifier' => $level['identifier'],
                        'name' => $level['name'],
                        'description' => $level['description'],
                        'default_level' => (bool)$level['default'],
                        'secure_data_space_id' => $secure_data_space_id,
                        'active' => (int)($level['active'] ?? true),
                        'ts_msg_id' => $getSLPolicies($level, 'timestamp_messages'),
                        'ts_pjs_id' => $getSLPolicies($level, 'timestamp_attachments'),
                        'ts_conv_id' => $getSLPolicies($level, 'timestamp_conversions'),
                        'convert_preservation' => $level['convert_preservation'],
                        'convert_diffusion' => $level['convert_diffusion'],
                    ]
                );
                $ServiceLevels->saveOrFail($levelEntity);
            }
            $levels[$key] = $levelEntity;
        }
    }

    /**
     * Ajoute les accords de versement
     * @return void
     */
    private function buildAgreements(): void
    {
        $this->out('installing agreements...');
        $agreements =& $this->iniData['agreements'];
        $loc = TableRegistry::getTableLocator();
        /** @var AgreementsTable $Agreements */
        $Agreements = $loc->get('Agreements');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        /** @var ServiceLevelsTable $ServiceLevels */
        $ServiceLevels = $loc->get('ServiceLevels');
        /** @var ProfilesTable $Profiles */
        $Profiles = $loc->get('Profiles');
        foreach ($agreements as $key => $agreement) {
            $sa = $OrgEntities->find()
                ->innerJoinWith('TypeEntities')
                ->where(['identifier' => $agreement['archival_agency']])
                ->andWhere(['code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES])
                ->firstOrFail();
            $org_entity_id = $sa->id;
            $agreementEntity = $Agreements->find()
                ->where(
                    [
                        'org_entity_id' => $org_entity_id,
                        'identifier' => $agreement['identifier'],
                    ]
                )
                ->first();
            if (!$agreementEntity) {
                $proper_chain_id = null;
                if ($agreement['validation_conform'] && $agreement['validation_conform'] !== 'auto') {
                    $proper_chain_id = $this->iniData['validations'][$agreement['validation_conform']]->id;
                }
                $improper_chain_id = $this->iniData['validations'][$agreement['validation_not_conform']]->id;
                $additionnals = [];
                $allOrgEntities = $this->iniData['org_entities'] + $this->iniData['archival_agencies'];
                if ($tas = array_filter(explode(',', $agreement['allowed_transferring_agencies']))) {
                    $taIdentifiers = array_map(
                        fn($ta) => $allOrgEntities[$ta]['identifier'],
                        $tas
                    );
                    $additionnals['transferring_agencies']['_ids'] = $OrgEntities->find()
                        ->select(['id'])
                        ->where(['identifier IN' => $taIdentifiers ?: [null]])
                        ->andWhere(
                            [
                                'lft >=' => $sa->get('lft'),
                                'rght <=' => $sa->get('rght'),
                            ]
                        )
                        ->all()
                        ->map(fn(EntityInterface $e) => $e->id)
                        ->toArray();
                }
                if ($oas = array_filter(explode(',', $agreement['allowed_originating_agencies']))) {
                    $oaIdentifiers = array_map(
                        fn($oa) => $allOrgEntities[$oa]['identifier'],
                        $oas
                    );
                    $additionnals['originating_agencies']['_ids'] = $OrgEntities->find()
                        ->select(['id'])
                        ->where(['identifier IN' => $oaIdentifiers ?: [null]])
                        ->andWhere(
                            [
                                'lft >=' => $sa->get('lft'),
                                'rght <=' => $sa->get('rght'),
                            ]
                        )
                        ->all()
                        ->map(fn(EntityInterface $e) => $e->id)
                        ->toArray();
                }
                if ($sls = array_filter(explode(',', $agreement['allowed_service_levels']))) {
                    $slIdentifiers = array_map(
                        fn($sl) => $this->iniData['service_levels'][$sl]['identifier'],
                        $sls
                    );
                    $additionnals['service_levels']['_ids'] = $ServiceLevels->find()
                        ->select(['id'])
                        ->where(['identifier IN' => $slIdentifiers ?: [null]])
                        ->andWhere(['org_entity_id' => $org_entity_id])
                        ->all()
                        ->map(fn(EntityInterface $e) => $e->id)
                        ->toArray();
                }
                if ($prs = array_filter(explode(',', $agreement['allowed_profiles']))) {
                    $prIdentifiers = array_map(
                        fn($pr) => $this->iniData['profiles'][$pr]['identifier'],
                        $prs
                    );
                    $additionnals['profiles']['_ids'] = $Profiles->find()
                        ->select(['id'])
                        ->where(['identifier IN' => $prIdentifiers ?: [null]])
                        ->andWhere(['org_entity_id' => $org_entity_id])
                        ->all()
                        ->map(fn(EntityInterface $e) => $e->id)
                        ->toArray();
                }
                $agreementEntity = $Agreements->newEntity(
                    [
                        'org_entity_id' => $org_entity_id,
                        'identifier' => $agreement['identifier'],
                        'name' => $agreement['name'],
                        'description' => $agreement['description'],
                        'active' => (bool)($agreement['active'] ?? true),
                        'default_agreement' => (bool)$agreement['default'],
                        'allow_all_transferring_agencies' => (bool)(
                            $agreement['allow_all_transferring_agencies']
                            ?? empty($agreement['allowed_transferring_agencies'])
                        ),
                        'allow_all_originating_agencies' => (bool)(
                            $agreement['allow_all_originating_agencies']
                            ?? empty($agreement['allowed_originating_agencies'])
                        ),
                        'allow_all_service_levels' => (bool)(
                            $agreement['allow_all_service_levels'] ?? empty($agreement['allowed_service_levels'])
                        ),
                        'allow_all_profiles' => (bool)(
                            $agreement['allow_all_profiles'] ?? empty($agreement['allowed_profiles'])
                        ),
                        'auto_validate' => (bool)(
                            $agreement['auto_validate'] ?? $agreement['validation_conform'] === 'auto'
                        ),
                        'proper_chain_id' => $proper_chain_id,
                        'improper_chain_id' => $improper_chain_id,
                        'max_tranfers' => $agreement['max_tranfers'],
                        'transfer_period' => $agreement['transfer_period'],
                        'max_size_per_transfer' => $agreement['max_size_per_transfer'],
                        'allowed_formats' => $agreement['allowed_formats'],
                    ] + Hash::filter($additionnals)
                );
                $Agreements->saveOrFail($agreementEntity);
            }
            $agreements[$key] = $agreementEntity;
        }
    }

    /**
     * Ajoute les configurations
     * @return void
     */
    private function buildConfigurations(): void
    {
        $this->out('installing configurations...');
        $configurations =& $this->iniData['configurations'];
        $loc = TableRegistry::getTableLocator();
        /** @var ConfigurationsTable $Configurations */
        $Configurations = $loc->get('Configurations');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        foreach ($configurations as $key => $configuration) {
            $org_entity_id = $OrgEntities->find()
                ->innerJoinWith('TypeEntities')
                ->where(['identifier' => $configuration['org_entity']])
                ->andWhere(['code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES])
                ->firstOrFail()
                ->id;
            $configurationEntity = $Configurations->find()
                ->where(
                    [
                        'org_entity_id' => $org_entity_id,
                        'name' => $configuration['name'],
                    ]
                )
                ->first();
            if (!$configurationEntity) {
                $configurationEntity = $Configurations->newEntity(
                    [
                        'org_entity_id' => $org_entity_id,
                        'name' => $configuration['name'],
                        'setting' => $configuration['setting'],
                    ]
                );
                $Configurations->saveOrFail($configurationEntity);
            }
            $configurations[$key] = $configurationEntity;
        }
    }

    /**
     * Ajoute les listes de mots clefs
     * @return void
     */
    private function buildKeywordLists(): void
    {
        $this->out('installing keywords...');
        $keywordLists =& $this->iniData['keyword_lists'];
        $loc = TableRegistry::getTableLocator();
        /** @var KeywordListsTable $KeywordLists */
        $KeywordLists = $loc->get('KeywordLists');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        foreach ($keywordLists as $key => $keywordList) {
            $org_entity_id = $OrgEntities->find()
                ->innerJoinWith('TypeEntities')
                ->where(['identifier' => $keywordList['org_entity']])
                ->firstOrFail()
                ->id;
            $list = $KeywordLists->find()
                ->where(
                    [
                        'org_entity_id' => $org_entity_id,
                        'identifier' => $keywordList['identifier'],
                    ]
                )
                ->first();
            if (!$list) {
                $list = $KeywordLists->newEntity(
                    [
                        'org_entity_id' => $org_entity_id,
                        'identifier' => $keywordList['identifier'],
                        'name' => $keywordList['name'],
                        'description' => $keywordList['description'],
                        'active' => (bool)($keywordList['active'] ?? true),
                        'version' => $keywordList['version'],
                    ]
                );
                $KeywordLists->saveOrFail($list);
            }
            $keywordLists[$key] = $list;
        }
        $this->buildKeywords();
    }

    /**
     * Ajoute les mots-clefs à une liste
     * @return void
     */
    private function buildKeywords(): void
    {
        $keywords =& $this->iniData['keywords'];
        $loc = TableRegistry::getTableLocator();
        /** @var KeywordsTable $Keywords */
        $Keywords = $loc->get('Keywords');
        foreach ($keywords as $keyword) {
            $keyword_list_id = $this->iniData['keyword_lists'][$keyword['keyword_list']]->id;
            $kw = $Keywords->find()
                ->where(
                    [
                        'name' => $keyword['name'],
                        'keyword_list_id' => $keyword_list_id,
                        'version' => $keyword['version'],
                    ]
                )
                ->first();
            if (!$kw) {
                $parent = isset($keyword['parent_keyword'])
                    ? $Keywords->find()
                        ->select(['id'])
                        ->where(
                            [
                                'keyword_list_id' => $keyword_list_id,
                                'name' => $keyword['parent_keyword_name'],
                                'version' => $keyword['parent_keyword_version'],
                            ]
                        ) // aie soucis là, unicité sur (name, keyword_list_id, version)
                        ->firstOrFail()
                        ->get('id')
                    : null;
                $kw = $Keywords->newEntity(
                    [
                        'keyword_list_id' => $keyword_list_id,
                        'version' => $keyword['version'],
                        'code' => $keyword['code'],
                        'name' => $keyword['name'],
                        // app_meta // ?
                        'parent_id' => $parent,
                        'exact_match' => $keyword['exact_match'],
                        'change_note' => $keyword['change_note'],
                    ] + (isset($keyword['imported_from'])
                        ? ['imported_from' => $keyword['imported_from']]
                        : [])
                );
                $Keywords->saveOrFail($kw);
            }
        }
    }
}
