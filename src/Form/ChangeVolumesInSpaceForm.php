<?php

/**
 * Asalae\Form\ChangeVolumesInSpaceForm
 */

namespace Asalae\Form;

use AsalaeCore\I18n\Date as CoreDate;
use Asalae\Cron\ChangeVolumesInSpace;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\I18n\Date as CakeDate;
use Cake\I18n\DateTime as CakeDateTime;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateMalformedStringException;
use DateTimeInterface;
use Exception;

/**
 * Formulaire de changement de volumes d'un espace de stockage
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ChangeVolumesInSpaceForm extends Form
{
    /**
     * @var array Liste les volumes actuels
     */
    public $currentVolumes = [];

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('add', ['type' => 'array']);
        $schema->addField('remove', ['type' => 'array']);
        $schema->addField('cron', ['type' => 'datetime']);
        $schema->addField('space_id', ['type' => 'integer']);

        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $currentVolumes = $this->currentVolumes;
        $validator->allowEmptyArray(
            'add',
            __(
                "Au moins une action doit être sélectionnée (ajouter ou retirer un volume)"
            ),
            function ($context) {
                return !empty($context['data']['remove']);
            }
        );
        $validator->requirePresence(
            'add',
            function ($context) {
                return empty($context['data']['remove']);
            },
            __(
                "Au moins une action doit être sélectionnée (ajouter ou retirer un volume)"
            )
        );
        $validator->add(
            'remove',
            'checkMinAmountOfVolumes',
            [
                'rule' => function ($value, $context) use ($currentVolumes) {
                    if (isset($context['data']['add']) && $context['data']['add'] !== '') {
                        return true;
                    }
                    return count(Hash::filter($value)) < count($currentVolumes);
                },
                'message' => __("Vous devez laisser au moins un volume"),
            ]
        );
        $validator->allowEmptyArray('remove');
        $validator
            ->requirePresence('cron')
            ->notEmptyDateTime('cron');
        $validator
            ->integer('space_id')
            ->requirePresence('space_id')
            ->notEmptyString('space_id');

        return $validator;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $loc = TableRegistry::getTableLocator();
        $data = Hash::filter($data);
        $data['cron'] = $this->setterDate($data['cron']);
        $SecureDataSpaces = $loc->get('SecureDataSpaces');
        $space = $SecureDataSpaces->get($data['space_id']);

        $Volumes = $loc->get('Volumes');
        $add = [];
        $remove = [];
        if (isset($data['add'])) {
            $add = $Volumes->find('list')
                ->where(['id IN' => $data['add']])
                ->orderBy('name')
                ->all()
                ->map([$this, 'escapeName'])
                ->toArray();
        }
        if (isset($data['remove'])) {
            $remove = $Volumes->find('list')
                ->where(['id IN' => $data['remove']])
                ->orderBy('name')
                ->all()
                ->map([$this, 'escapeName'])
                ->toArray();
        }
        $addMessage = __n(
            "Ajout du volume {0}",
            "Ajout des volumes {0}",
            count($data['add'] ?? []),
            '"' . implode('", "', $add) . '"'
        );
        $removeMessage = __n(
            "Retrait du volume {0}",
            "Retrait des volumes {0}",
            count($data['remove'] ?? []),
            '"' . implode('", "', $remove) . '"'
        );
        $data['uid'] = uniqid(bin2hex(random_bytes(10)));

        $Crons = $loc->get('Crons');
        $cron = $Crons->newEntity(
            [
                'name' => __(
                    "Modification des volumes de l'espace {0}",
                    h($space->get('name'))
                ),
                'description' => $add && $remove
                    ? __("{0} et {1}", $addMessage, $removeMessage)
                    : ($add ? $addMessage : $removeMessage),
                'classname' => ChangeVolumesInSpace::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'one_shot',
                'next' => $data['cron'],
                'app_meta' => json_encode($data, JSON_UNESCAPED_SLASHES),
                'parallelisable' => false,
            ]
        );
        $this->set('cron', $Crons->save($cron));
        return (bool)$this->getData('cron');
    }

    /**
     * Setter des types date
     * @param string|DateTimeInterface $value
     * @return CakeDateTime|string 'false' si pas réussi à parser la date
     * @throws DateMalformedStringException
     */
    public function setterDate($value)
    {
        if (is_string($value)) {
            $value = new CakeDateTime(CoreDate::stringToDateTime($value));
        } elseif ($value instanceof CakeDate || $value instanceof CoreDate) {
            $value = new CakeDateTime($value->format('Y-m-d'));
        } elseif ($value instanceof DateTimeInterface) {
            $value = new CakeDateTime($value);
        }

        return $value ?: 'false';
    }

    /**
     * Echape le name du volume
     * NOTE: 2e param = id, 3e param = ResultSet
     * (provoque une erreur si h() direct)
     * @param string $value
     * @return string
     */
    public function escapeName(string $value): string
    {
        return h($value);
    }
}
