<?php

/**
 * Asalae\Form\EntityFormTrait
 */

namespace Asalae\Form;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Entity;

/**
 * Permet de transformer un form en entity
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait EntityFormTrait
{
    /**
     * Form -> Entity
     * @return EntityInterface
     */
    public function toEntity(): EntityInterface
    {
        $entity = new Entity($this->getData());
        $entity->setErrors($this->getValidator()->validate($this->getData()));
        return $entity;
    }
}
