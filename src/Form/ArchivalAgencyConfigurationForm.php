<?php

/**
 * Asalae\Form\ArchivalAgencyConfigurationForm
 */

namespace Asalae\Form;

use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\FileuploadsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\Validation\Validator;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Formulaire d'édition de la configuration d'un service d'archives
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchivalAgencyConfigurationForm extends Form
{
    /**
     * @var EntityInterface[] entités configurés
     */
    public array $entities = [];
    /**
     * @var ConfigurationsTable
     */
    private Table $Configurations;
    /**
     * @var FileuploadsTable
     */
    private Table $Fileuploads;
    /**
     * @var ValidationChainsTable
     */
    private Table $ValidationChains;
    /**
     * @var int service d'archives lié au formulaire
     */
    private int $archivalAgencyId;
    /**
     * @var array options à donner au controlleur
     */
    private array $vars = [];

    /**
     * Donne une instance avec service d'archives lié
     * @param int $archivalAgencyId
     * @return static
     */
    public static function create(int $archivalAgencyId): self
    {
        $instance = new self();
        $instance->archivalAgencyId = $archivalAgencyId;
        $loc = TableRegistry::getTableLocator();
        $instance->Configurations = $loc->get('Configurations');
        $instance->Fileuploads = $loc->get('Fileuploads');
        $instance->ValidationChains = $loc->get('ValidationChains');

        $query = $instance->Configurations->find()
            ->where(['org_entity_id' => $archivalAgencyId]);
        /** @var EntityInterface $entity */
        foreach ($query as $entity) {
            $instance->entities[$entity->get('name')] = $entity;
            $instance->_data[$entity->get('name')] = $entity->get('setting');
        }

        return $instance;
    }

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        // champs génériques (sera écrasé par les appels qui suivent)
        foreach (ConfigurationsTable::KEYS as $name) {
            $schema->addField($name, ['type' => 'string']);
        }

        $schema->addField(
            ConfigurationsTable::CONF_DESTR_REQUEST_CHAIN,
            ['type' => 'string', 'default' => 'default', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_DR_NO_DEROGATION_CHAIN,
            ['type' => 'string', 'default' => 'default', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_DR_DEROGATION_CHAIN,
            ['type' => 'string', 'default' => 'default', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_RR_CHAIN,
            ['type' => 'string', 'default' => 'default', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_OTR_CHAIN,
            ['type' => 'string', 'default' => 'default', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_MAIL_HTML_SIGNATURE,
            ['type' => 'text']
        );
        $schema->addField(
            ConfigurationsTable::CONF_MAIL_TEXT_SIGNATURE,
            ['type' => 'text']
        );
        $schema->addField(
            ConfigurationsTable::CONF_DELIVERY_RETENTION_PERIOD,
            ['type' => 'integer', 'default' => 7]
        );
        $schema->addField(
            ConfigurationsTable::CONF_RR_DESTR_DELAY,
            ['type' => 'integer', 'default' => 15]
        );
        $schema->addField(
            ConfigurationsTable::CONF_OTR_DESTR_DELAY,
            ['type' => 'integer', 'default' => 15]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_TEXT,
            ['type' => 'string', 'default' => 'otr', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_TEXT,
            ['type' => 'string', 'default' => 'pdf', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_PDF,
            ['type' => 'string', 'default' => 'otr', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_PDF,
            ['type' => 'string', 'default' => 'pdf', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_CALC,
            ['type' => 'string', 'default' => 'ods', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_CALC,
            ['type' => 'string', 'default' => 'pdf', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_PRES,
            ['type' => 'string', 'default' => 'odp', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_PRES,
            ['type' => 'string', 'default' => 'pdf', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_PRESERV_IMAGE,
            ['type' => 'string', 'default' => 'png', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_DISSEM_IMAGE,
            ['type' => 'string', 'default' => 'jpg', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_PRESERV_AUDIO,
            ['type' => 'string', 'default' => 'flac', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_DISSEM_AUDIO,
            ['type' => 'string', 'default' => 'ogg', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CONTAINER,
            ['type' => 'string', 'default' => 'mp4', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CODEC_VIDEO,
            ['type' => 'string', 'default' => 'h264', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CODEC_AUDIO,
            ['type' => 'string', 'default' => 'aac', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CONTAINER,
            ['type' => 'string', 'default' => 'mp4', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CODEC_VIDEO,
            ['type' => 'string', 'default' => 'h264', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CODEC_AUDIO,
            ['type' => 'string', 'default' => 'aac', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_ATTEST_ARCHIVING_CERT,
            ['type' => 'string', 'default' => 'on_demand', 'required' => true]
        );
        $schema->addField(
            ConfigurationsTable::CONF_ATTEST_ARCHIVING_CERT_COMMITMENT,
            ['type' => 'text']
        );
        $schema->addField(
            ConfigurationsTable::CONF_ATTEST_DESTR_CERT_INTERMEDIATE,
            ['type' => 'boolean', 'default' => false]
        );

        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $required = [
            ConfigurationsTable::CONF_DESTR_REQUEST_CHAIN,
            ConfigurationsTable::CONF_DR_NO_DEROGATION_CHAIN,
            ConfigurationsTable::CONF_DR_DEROGATION_CHAIN,
            ConfigurationsTable::CONF_RR_CHAIN,
            ConfigurationsTable::CONF_OTR_CHAIN,
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_TEXT,
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_TEXT,
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_PDF,
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_PDF,
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_CALC,
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_CALC,
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_PRES,
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_PRES,
            ConfigurationsTable::CONF_CONV_PRESERV_IMAGE,
            ConfigurationsTable::CONF_CONV_DISSEM_IMAGE,
            ConfigurationsTable::CONF_CONV_PRESERV_AUDIO,
            ConfigurationsTable::CONF_CONV_DISSEM_AUDIO,
            ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CONTAINER,
            ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CODEC_VIDEO,
            ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CODEC_AUDIO,
            ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CONTAINER,
            ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CODEC_VIDEO,
            ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CODEC_AUDIO,
            ConfigurationsTable::CONF_ATTEST_ARCHIVING_CERT,
        ];
        foreach (ConfigurationsTable::KEYS as $name) {
            $validator->scalar($name);
            if (!in_array($name, $required)) {
                $validator->allowEmptyString($name);
            }
        }
        $validator->add(
            ConfigurationsTable::CONF_BACKGROUND_POSITION,
            'no_semicolon',
            [
                'rule' => fn($value) => strpos($value, ';') === false,
                'message' => __("Le caractère ''{0}'' est interdit", ";"),
            ]
        );
        $validator->integer(ConfigurationsTable::CONF_DELIVERY_RETENTION_PERIOD)
            ->nonNegativeInteger(ConfigurationsTable::CONF_DELIVERY_RETENTION_PERIOD);

        $validator->integer(ConfigurationsTable::CONF_RR_DESTR_DELAY)
            ->nonNegativeInteger(ConfigurationsTable::CONF_RR_DESTR_DELAY);

        $validator->integer(ConfigurationsTable::CONF_OTR_DESTR_DELAY)
            ->nonNegativeInteger(ConfigurationsTable::CONF_OTR_DESTR_DELAY);

        // ajoute un control d'enum sur les fieldset
        $opts = $this->getOptions();
        foreach ($required as $key) {
            $options = $opts[$this->optionKey($key)] ?? [];
            // retire les groupes d'options (utilisés comme séparateur ‾‾)
            $options = array_filter($options, fn($v) => !is_array($v));
            $validator->inList($key, array_keys($options));
        }

        $validator->add(
            'fileuploads.id',
            'valid',
            [
                'rule' => function ($value) {
                    if ($value === 'local') {
                        return true;
                    }
                    $fileupload = $this->Fileuploads->find()
                        ->innerJoinWith('Users')
                        ->innerJoinWith('Users.OrgEntities')
                        ->innerJoinWith('Users.OrgEntities.ArchivalAgencies')
                        ->where(
                            [
                                'Users.id' => $value,
                                'ArchivalAgencies.id' => $this->archivalAgencyId,
                            ]
                        )
                        ->first();
                    return $fileupload
                        && is_readable($fileupload->get('path'))
                        && strpos(
                            mime_content_type($fileupload->get('path')),
                            'image/'
                        ) !== false;
                },
            ]
        );

        return $validator;
    }

    /**
     * Donne les options calculés
     * @return array
     */
    public function getOptions(): array
    {
        if (empty($this->vars)) {
            $this->loadOptions();
        }
        return $this->vars;
    }

    /**
     * Charge $this->vars pour offrir à la vue, les variables d'options
     * @return void
     */
    private function loadOptions()
    {
        $auto = ['auto' => __("Validation automatique")];
        $default = [
            'default' => __("Circuit de validation par défaut"),
            '‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾' => [],
        ];
        $key = $this->optionKey(ConfigurationsTable::CONF_DESTR_REQUEST_CHAIN);
        $this->vars[$key] = $default
            + $this->chainOptions(ValidationChainsTable::ARCHIVE_DESTRUCTION_REQUEST);

        $key = $this->optionKey(ConfigurationsTable::CONF_DR_NO_DEROGATION_CHAIN);
        $deliveryChains = $this->chainOptions(ValidationChainsTable::ARCHIVE_DELIVERY_REQUEST);
        $this->vars[$key] = $auto + $default + $deliveryChains;

        $key = $this->optionKey(ConfigurationsTable::CONF_DR_DEROGATION_CHAIN);
        $this->vars[$key] = $default + $deliveryChains;

        $key = $this->optionKey(ConfigurationsTable::CONF_RR_CHAIN);
        $this->vars[$key] = $default
            + $this->chainOptions(ValidationChainsTable::ARCHIVE_RESTITUTION_REQUEST);

        $key = $this->optionKey(ConfigurationsTable::CONF_OTR_CHAIN);
        $this->vars[$key] = $default
            + $this->chainOptions(ValidationChainsTable::ARCHIVE_OUTGOING_TRANSFER_REQUEST);

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_PRESERV_DOC_TEXT);
        $this->vars[$key] = [
            'odt' => 'ODT - OpenDocument Text',
            'pdf' => 'PDF/A - Portable Document Format',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_DISSEM_DOC_TEXT);
        $this->vars[$key] = [
            'pdf' => 'PDF/A - Portable Document Format',
            'odt' => 'ODT - OpenDocument Text',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_PRESERV_DOC_PDF);
        $this->vars[$key] = [
            'pdf' => 'PDF/A - Portable Document Format',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_DISSEM_DOC_PDF);
        $this->vars[$key] = [
            'pdf' => 'PDF/A - Portable Document Format',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_PRESERV_DOC_CALC);
        $this->vars[$key] = [
            'ods' => 'ODS - OpenDocument Spreadsheet',
            'pdf' => 'PDF/A - Portable Document Format',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_DISSEM_DOC_CALC);
        $this->vars[$key] = [
            'pdf' => 'PDF/A - Portable Document Format',
            'ods' => 'ODS - OpenDocument Spreadsheet',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_PRESERV_DOC_PRES);
        $this->vars[$key] = [
            'odp' => 'ODP - OpenDocument Presentation',
            'pdf' => 'PDF/A - Portable Document Format',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_DISSEM_DOC_PRES);
        $this->vars[$key] = [
            'pdf' => 'PDF/A - Portable Document Format',
            'odp' => 'ODP - OpenDocument Presentation',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_PRESERV_IMAGE);
        $this->vars[$key] = [
            'png' => 'PNG - Portable Network Graphics',
            'jpg' => 'JPG - Joint Photographic Experts Group',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_DISSEM_IMAGE);
        $this->vars[$key] = [
            'jpg' => 'JPG - Joint Photographic Experts Group',
            'png' => 'PNG - Portable Network Graphics',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_PRESERV_AUDIO);
        $this->vars[$key] = [
            'flac' => 'FLAC - Free Lossless Audio Codec',
            'ogg' => 'OGG - OGG Vorbis',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_DISSEM_AUDIO);
        $this->vars[$key] = [
            'ogg' => 'OGG - OGG Vorbis',
            'flac' => 'FLAC - Free Lossless Audio Codec',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CONTAINER);
        $this->vars[$key] = [
            'mp4' => 'MP4 - MPEG-4 Part 14',
            'mkv' => 'MKV - Matroska',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CODEC_VIDEO);
        $this->vars[$key] = [
            'h264' => 'H.264 / MPEG-4 AVC - Advanced Video Coding',
            'vp8' => 'VP8 - Open VP8',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CODEC_AUDIO);
        $this->vars[$key] = [
            'aac' => 'AAC - Advanced Audio Coding',
            'vorbis' => 'Vorbis - Vorbis',
            'flac' => 'FLAC - Free Lossless Audio Codec',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CONTAINER);
        $this->vars[$key] = [
            'mp4' => 'MP4 - MPEG-4 Part 14',
            'mkv' => 'MKV - Matroska',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CODEC_VIDEO);
        $this->vars[$key] = [
            'h264' => 'H.264 / MPEG-4 AVC - Advanced Video Coding',
            'vp8' => 'VP8 - Open VP8',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CODEC_AUDIO);
        $this->vars[$key] = [
            'aac' => 'AAC - Advanced Audio Coding',
            'vorbis' => 'Vorbis - Vorbis',
            'flac' => 'FLAC - Free Lossless Audio Codec',
        ];

        $key = $this->optionKey(ConfigurationsTable::CONF_ATTEST_ARCHIVING_CERT);
        $this->vars[$key] = [
            'on_demand' => __("Production à la demande"),
            'always' => __(
                "Production et stockage à la création des archives"
            ),
        ];
    }

    /**
     * Donne le nom de la variable d'options correspondant à un name
     * @param string $name
     * @return string
     */
    private function optionKey(string $name): string
    {
        return Inflector::variable(
            Inflector::pluralize($name)
        );
    }

    /**
     * Donne les options liés à un champ
     * @param string $name
     * @return array
     */
    private function chainOptions(string $name): array
    {
        return $this->ValidationChains
            ->findListOptions($this->archivalAgencyId, $name)
            ->toArray();
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        foreach ($data as $key => $value) {
            if (!in_array($key, ConfigurationsTable::KEYS)) {
                continue;
            }
            $entity = $this->Configurations->find()
                ->where(
                    $confData = [
                        'org_entity_id' => $this->archivalAgencyId,
                        'name' => $key,
                    ]
                )
                ->first();
            $hasValue = $value || $value === '0';
            if ($entity) {
                if ($hasValue) {
                    $this->Configurations->patchEntity($entity, ['setting' => $value]);
                    $success = $this->Configurations->save($entity);
                } else {
                    $success = $this->Configurations->delete($entity);
                }
            } elseif ($hasValue) {
                $entity = $this->Configurations->newEntity(
                    $confData + ['setting' => $value]
                );
                $success = $this->Configurations->save($entity);
            } else {
                $success = true;
            }
            if (!$success) {
                return false;
            }
        }

        $this->handleLogo($data);
        return true;
    }

    /**
     * Gestion du logo
     * @param array $data du formulaire
     * @return false|void
     * @throws Exception
     */
    private function handleLogo(array $data)
    {
        $dataDir = rtrim(Configure::read('App.paths.data'), DS);
        $publicDir = rtrim(
            Configure::read('App.paths.config', $dataDir . DS . 'config'),
            DS
        );
        $webrootDir = rtrim(
            Configure::read('OrgEntities.public_dir', WWW_ROOT),
            DS
        );

        $fileupload_id = Hash::get($data, 'fileuploads.id');
        $entity = $this->Configurations->find()
            ->where(
                $logoData = [
                    'org_entity_id' => $this->archivalAgencyId,
                    'name' => ConfigurationsTable::CONF_BACKGROUND_IMAGE,
                ]
            )
            ->first();
        // si fileupload_id est un integer, il sagit d'un nouveau logo
        if (isset($fileupload_id) && is_numeric($fileupload_id)) {
            $fileupload = $this->Fileuploads->get($fileupload_id);
            // Génère un nouveau répertoire unique
            do {
                $randdir = bin2hex(random_bytes(16));
                $setting = sprintf(
                    '/org-entity-data/%d/%s/%s',
                    $this->archivalAgencyId,
                    $randdir,
                    $fileupload->get('name')
                );
                $targetPath = $publicDir . $setting;
            } while (is_file($targetPath));

            // copy du fichier
            Filesystem::copy($fileupload->get('path'), $targetPath);
            Filesystem::copy($fileupload->get('path'), $webrootDir . $setting);

            // Patch de l'entité
            if (!$entity) {
                $entity = $this->Configurations->newEmptyEntity();
            } else {
                $oldTargetPath = $entity->get('setting');
                if (is_file($dataDir . $oldTargetPath)) {
                    Filesystem::remove($dataDir . $oldTargetPath);
                }
                if (is_file($publicDir . $oldTargetPath)) {
                    Filesystem::remove($publicDir . $oldTargetPath);
                }
                if (is_file($webrootDir . $oldTargetPath)) {
                    Filesystem::remove($webrootDir . $oldTargetPath);
                }
            }
            $this->Configurations->patchEntity($entity, $logoData + ['setting' => $setting]);
            if (!$this->Configurations->save($entity)) {
                return false;
            }
            $this->Fileuploads->delete($fileupload);
        } elseif (!isset($fileupload_id) && $entity) {
            $this->Configurations->delete($entity);
            unset($entity);
        }
        if (empty($entity)) {
            $target = sprintf('/org-entity-data/%d/background.css', $this->archivalAgencyId);
            if (is_file($dataDir . $target)) {
                Filesystem::remove($dataDir . $target);
            }
            if (is_file($publicDir . $target)) {
                Filesystem::remove($publicDir . $target);
            }
            if (is_file($webrootDir . $target)) {
                Filesystem::remove($webrootDir . $target);
            }
        } else {
            $position = $data[ConfigurationsTable::CONF_BACKGROUND_POSITION] ?? '';
            $position = $position ?: '50px 50px';
            $this->generateBackgroundCss($entity->get('setting'), $position);
        }
    }

    /**
     * Génère le css pour le background-image du logo client
     * @param string $imageUrl
     * @param string $position
     * @throws Exception
     */
    private function generateBackgroundCss(string $imageUrl, string $position)
    {
        $fullUrl = addcslashes(Router::url($imageUrl, true), "'");

        $posX = preg_match('/^(\d+)px/', $position, $matches)
            ? (int)$matches[1] : 0;

        $containerSize = 1170;
        $dataDir = rtrim(Configure::read('App.paths.data'), DS);
        $publicDir = rtrim(
            Configure::read('App.paths.config', $dataDir . DS . 'config'),
            DS
        );
        $prevImage = is_file($publicDir . DS . ltrim($imageUrl, DS))
            ? $publicDir . DS . ltrim($imageUrl, DS)
            : $dataDir . DS . ltrim($imageUrl, DS);
        [$width, $height] = getimagesize($prevImage);
        $maxSize = $containerSize + (($width + $posX + 10) * 2);
        $height20 = $height + 20;
        $css = <<<EOT
body:not(.bg-login) div#body-content {
    background-image: url('$fullUrl');
    background-position: $position;
    background-repeat: no-repeat;
}
@media (max-width: {$maxSize}px) {
    body:not(.bg-login) div#body-content {
        background-image: none;
        padding-top: {$height20}px;
    }

    body:not(.bg-login) div#body-content:before {
        content: ' ';
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        width: 100%;
        height: {$height20}px;
        background-color: #fff;
        background-image: url('$fullUrl');
        background-position: center center;
        background-repeat: no-repeat;
        box-shadow: 0 0 1px rgba(0, 0, 0, 0.6);
    }
}
EOT;
        $webrootDir = rtrim(
            Configure::read('OrgEntities.public_dir', WWW_ROOT),
            DS
        );
        $target = sprintf('/org-entity-data/%d/background.css', $this->archivalAgencyId);
        Filesystem::dumpFile($publicDir . $target, $css);
        Filesystem::dumpFile($webrootDir . $target, $css);
    }
}
