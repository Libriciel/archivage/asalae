<?php

/**
 * Asalae\Form\BatchTreatmentForm
 */

namespace Asalae\Form;

use AsalaeCore\Model\Entity\DateTrait;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;

/**
 * Formulaire pour l'ajout d'un traitement par lot
 */
class BatchTreatmentForm extends Form
{
    use DateTrait;

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('type', ['type' => 'string']);

        $schema->addField('appraisal__final', ['type' => 'string']);
        $schema->addField('appraisal__duration', ['type' => 'string']);
        $schema->addField('appraisal__final_start_date', ['type' => 'date']);

        $schema->addField('restriction__code', ['type' => 'string']);
        $schema->addField('restriction__start_date', ['type' => 'date']);

        $schema->addField('name__research', ['type' => 'string']);
        $schema->addField('name__replace', ['type' => 'string']);

        $schema->addField('description__research', ['type' => 'string']);
        $schema->addField('description__replace', ['type' => 'string']);

        $schema->addField('keyword__type', ['type' => 'string']);
        $schema->addField('keyword__research', ['type' => 'string']);
        $schema->addField('keyword__replace', ['type' => 'string']);
        $schema->addField('keyword__delete', ['type' => 'string']);
        $schema->addField('keyword__add', ['type' => 'string']);

        $schema->addField('convert', ['type' => 'string']);

        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence('type', true, __("Formulaire incomplet"))
            ->notEmptyString('type');

        $validator
            ->requirePresence('appraisal__final', $this->requireAppraisal(...))
            ->notEmptyString(
                'appraisal__final',
                null,
                $this->requireAppraisal(...)
            );
        $validator
            ->requirePresence(
                'appraisal__duration',
                $this->requireAppraisal(...)
            )
            ->notEmptyString(
                'appraisal__duration',
                null,
                $this->requireAppraisal(...)
            );
        $validator
            ->requirePresence(
                'appraisal__final_start_date',
                $this->requireAppraisal(...)
            )
            ->notEmptyDate(
                'appraisal__final_start_date',
                null,
                $this->requireAppraisal(...)
            );

        $validator
            ->requirePresence(
                'restriction__code',
                $this->requireRestriction(...)
            )
            ->notEmptyString(
                'restriction__code',
                null,
                $this->requireRestriction(...)
            );
        $validator
            ->requirePresence(
                'restriction__start_date',
                $this->requireRestriction(...)
            )
            ->notEmptyDate(
                'restriction__start_date',
                null,
                $this->requireRestriction(...)
            );

        $validator
            ->requirePresence('name__research', $this->requireName(...))
            ->notEmptyDate('name__research', null, $this->requireName(...));
        $validator
            ->requirePresence('name__replace', $this->requireName(...))
            ->notEmptyDate('name__replace', null, $this->requireName(...));

        $validator
            ->requirePresence(
                'description__research',
                $this->requireDescription(...)
            )
            ->notEmptyDate(
                'description__research',
                null,
                $this->requireDescription(...)
            );
        $validator
            ->requirePresence(
                'description__replace',
                $this->requireDescription(...)
            )
            ->notEmptyDate(
                'description__replace',
                null,
                $this->requireDescription(...)
            );

        $validator
            ->requirePresence(
                'keyword__research',
                $this->requireKeywordReplace(...)
            )
            ->notEmptyDate(
                'keyword__research',
                null,
                $this->requireKeywordReplace(...)
            );
        $validator
            ->requirePresence(
                'keyword__replace',
                $this->requireKeywordReplace(...)
            )
            ->notEmptyDate(
                'keyword__replace',
                null,
                $this->requireKeywordReplace(...)
            );
        $validator
            ->requirePresence(
                'keyword__delete',
                $this->requireKeywordDelete(...)
            )
            ->notEmptyDate(
                'keyword__delete',
                null,
                $this->requireKeywordDelete(...)
            );
        $validator
            ->requirePresence('keyword__add', $this->requireKeywordAdd(...))
            ->notEmptyDate('keyword__add', null, $this->requireKeywordAdd(...));

        $validator
            ->requirePresence('convert', $this->requireConvert(...))
            ->notEmptyDate('convert', null, $this->requireConvert(...));

        return $validator;
    }

    /**
     * Nécessite un element de l'appraisal
     * @param array $context
     * @return bool
     */
    public function requireAppraisal($context)
    {
        if (Hash::get($context, 'data.type') !== 'appraisal') {
            return false;
        }
        $fields = [
            'appraisal__final',
            'appraisal__duration',
            'appraisal__final_start_date',
        ];
        foreach ($fields as $field) {
            if (Hash::get($context, "data.$field")) {
                return false;
            }
        }
        return true;
    }

    /**
     * Nécessite un element de restriction
     * @param array $context
     * @return bool
     */
    public function requireRestriction($context)
    {
        if (Hash::get($context, 'data.type') !== 'restriction') {
            return false;
        }
        $fields = ['restriction__code', 'restriction__start_date'];
        foreach ($fields as $field) {
            if (Hash::get($context, "data.$field")) {
                return false;
            }
        }
        return true;
    }

    /**
     * Type name -> obligatoire
     * @param array $context
     * @return bool
     */
    public function requireName($context)
    {
        return Hash::get($context, 'data.type') === 'name';
    }

    /**
     * Type description -> obligatoire
     * @param array $context
     * @return bool
     */
    public function requireDescription($context)
    {
        return Hash::get($context, 'data.type') === 'description';
    }

    /**
     * Nécessaire si remplacement de mots-clés
     * @param array $context
     * @return bool
     */
    public function requireKeywordReplace($context)
    {
        return Hash::get($context, 'data.keyword__type') === 'replace';
    }

    /**
     * Nécessaire si suppression de mots-clés
     * @param array $context
     * @return bool
     */
    public function requireKeywordDelete($context)
    {
        return Hash::get($context, 'data.keyword__type') === 'delete';
    }

    /**
     * Nécessaire si ajout de mots-clés
     * @param array $context
     * @return bool
     */
    public function requireKeywordAdd($context)
    {
        return Hash::get($context, 'data.keyword__type') === 'add';
    }

    /**
     * Nécessaire si conversions
     * @param array $context
     * @return bool
     */
    public function requireConvert($context)
    {
        return Hash::get($context, 'data.type') === 'convert';
    }

    /**
     * Prépare le data à la validation
     * @param array $data
     * @param array $options
     * @return bool
     * @throws Exception
     */
    public function execute(array $data, array $options = []): bool
    {
        $data = array_filter($data);
        $type = Hash::get($data, 'type');
        if (!$type) {
            return false;
        }
        // nettoie le formulaire
        $len = strlen($type);
        foreach (array_keys($data) as $field) {
            if (
                strpos($field, '__') !== false && substr(
                    $field,
                    0,
                    $len
                ) !== $type
            ) {
                unset($data[$field]);
            }
        }

        // champs date
        $schema = $this->getSchema();
        $fields = array_intersect($schema->fields(), array_keys($data));
        foreach ($fields as $field) {
            $params = $schema->field($field);
            if ($params['type'] === 'date') {
                $data[$field] = $this->getDatetime($data[$field])->format(
                    'Y-m-d'
                );
            }
        }

        if ($type === 'keyword') {
            $keywordType = Hash::get($data, 'keyword__type');
            switch ($keywordType) {
                case 'replace':
                    unset($data['keyword__delete'], $data['keyword__add']);
                    break;
                case 'delete':
                    unset($data['keyword__research'], $data['keyword__replace'], $data['keyword__add']);
                    break;
                case 'add':
                    unset($data['keyword__research'], $data['keyword__replace'], $data['keyword__delete']);
                    break;
            }
        }
        return parent::execute($data);
    }

    /**
     * Options du type de traitement
     * @return array
     */
    public function optionsTypes(): array
    {
        return [
            'appraisal' => __("DUA, sort final"),
            'restriction' => __("Communicabilité"),
            'name' => __("Nom"),
            'description' => __("Description"),
            'keyword' => __("Mots-clés"),
            'convert' => __("Conversions"),
        ];
    }

    /**
     * Options des traitements sur les mots-clés
     * @return array
     */
    public function optionsKeywordTypes(): array
    {
        return [
            'add' => __("Ajouter un mot clé"),
            'delete' => __("Supprimer un mot clé"),
            'replace' => __("Remplacer un mot clé par un autre"),
        ];
    }

    /**
     * Options des traitements sur les conversions
     * @return array
     */
    public function optionsConvert(): array
    {
        return [
            'do' => __("Effectue les conversions"),
            'delete' => __("Supprime les conversions"),
        ];
    }
}
