#!/bin/bash

declare -A OPTIONS
declare -a ARGS
declare -a ARGV

SCRIPT_FILE=$(basename "$0")
ACTION="$1"
shift 1
#
ARGV=("$@")

# Sépare les arguments des options (options sous la forme --<key> <value>)
while [[ $# -gt 0 ]]; do
    key="$1"
    if [[ $key == --* ]]; then
        option="${key#--}"
        value="$2"
        # shellcheck disable=SC2034
        OPTIONS[$option]="$value"
        shift
    else
        ARGS+=("$1")
    fi
    shift
done

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR" || exit

if [ -f .env.docker.local ]; then
  source .env.docker.local
elif [ -f .env ]; then
  source .env
fi

################################################################################
# Définition des commandes (appelé tout en bas dans select_function_asalae())
################################################################################

 ## bash sur le docker web
function bash() {
  if [ "${#ARGV[@]}" -eq 0 ]; then
      docker compose exec -it asalae_web bash
  else
      docker compose exec -it asalae_web bash -c "${ARGV[*]}"
  fi
}

function bash-root() {
  if [ "${#ARGV[@]}" -eq 0 ]; then
      docker compose exec --user=root -it asalae_web bash
  else
      docker compose exec --user=root -it asalae_web bash -c "${ARGV[*]}"
  fi
}

function certbot-hook() {
  if [ ! -d "/etc/letsencrypt/live/" ] || [ -z "$(ls -A /etc/letsencrypt/live/)" ]; then
      echo "Vous devez créer le certificat avant de faire le hook: certbot certonly --standalone"
      exit 1
  fi

  if [ -z "$DATA_DIR" ]; then
    DATA_DIR="./data"
  fi
  DATA_DIR=$(realpath "$DATA_DIR")

    echo "
#!/bin/bash

cd \"$DIR\"
docker compose down

" > /etc/letsencrypt/renewal-hooks/pre/pre-asalae.sh

    echo "
#!/bin/bash

cp \"\$RENEWED_LINEAGE/cert.pem\" \"$DATA_DIR/ssl/server.crt\"
cp \"\$RENEWED_LINEAGE/privkey.pem\" \"$DATA_DIR/ssl/server.key\"

cd \"$DIR\"
docker compose down
docker compose up -d

" > /etc/letsencrypt/renewal-hooks/deploy/renew-asalae.sh

  sudo chmod +x /etc/letsencrypt/renewal-hooks/deploy/renew-asalae.sh
  echo "written /etc/letsencrypt/renewal-hooks/deploy/renew-asalae.sh"
}

function cake() {
  docker compose exec -it asalae_web bin/cake "${ARGV[@]}"
}

## prefixe les services dans les commandes docker compose
function compose() {
    # Récupérer la liste des services définis dans docker-compose.yml
    local services
    services=$(docker compose config --services)

    local NEW_ARGS=()
    local SERVICE_FOUND=false

    for arg in "${ARGV[@]}"; do
        # Boucle pour vérifier si l'argument correspond à un service
        local is_service=false
        for service in $services; do
            if [[ "asalae_$arg" == "$service" ]]; then
                is_service=true
                break
            fi
        done

        # Si l'argument correspond à un service, ajouter le préfixe "asalae_"
        if [[ "$is_service" == true ]] && [ "$SERVICE_FOUND" = false ]; then
            NEW_ARGS+=("asalae_$arg")
            SERVICE_FOUND=true
        else
            # Sinon, ajouter l'argument tel quel
            NEW_ARGS+=("$arg")
        fi
    done

    # Exécuter la commande docker compose avec les nouveaux arguments
    docker compose "${NEW_ARGS[@]}"
}

function copy-certificate() {
  if [ "${#ARGV[@]}" -lt 1 ]; then
    echo "Usage: ./asalae copy-certificate /etc/letsencrypt/live/<domain.asalae.fr>/" >&2
    exit 1
  fi

  if [ ! -d "${ARGV[0]}" ]; then
    echo "Le dossier ${ARGV[0]} n'existe pas"
    exit 1
  fi

  LETSENCRYPT_DIR=$(realpath "${ARGV[0]}")
  if [ ! -f "${LETSENCRYPT_DIR}/fullchain.pem" ] || [ ! -f "${LETSENCRYPT_DIR}/privkey.pem" ]; then
    echo "Certificat ou clé privée manquant."
    exit 1
  fi

  down

  cp "${LETSENCRYPT_DIR}/fullchain.pem" "$DATA_DIR/ssl/server.crt" || exit 1
  cp "${LETSENCRYPT_DIR}/privkey.pem" "$DATA_DIR/ssl/server.key" || exit 1

  up
}

 ## docker compose down
function down() {
	docker compose down
}

function dump() {
  POSTGRES_USER=${POSTGRES_USER:-asalae}
  POSTGRES_DB=${POSTGRES_DB:-asalae}
  POSTGRES_DBPORT=${POSTGRES_DBPORT:-5432}
  dumpname=dbdump_"$(date +%Y-%m-%d_%H_%M_%S)".sql

  if [ -f .env.docker.local ]; then
    docker compose --env-file .env.docker.local up -d asalae_db
  else
    docker compose up -d asalae_db
  fi

  docker compose exec -t asalae_db pg_dump --blobs \
    -U "$POSTGRES_USER" \
    -d "$POSTGRES_DB" \
    -p "$POSTGRES_DBPORT" \
     | tee "$dumpname" > /dev/null
  if [ -s "$dumpname" ]; then
    echo "$dumpname"
  else
    rm -f "$dumpname"
  fi
}

function help() {
  CURFILE=$(basename "$0")
  echo Liste des commandes:
  linenum=$(grep -n "^# BEGIN COMMAND EXTRACTOR$" "$CURFILE" | head -n 1 | cut -d ':' -f 1)
  sed -n "$linenum,\$p" "$CURFILE" | grep -B1 ";;" | grep -v ";;" | grep -v '\-\-' | grep -v echo | sort
}

function install-autocomplete() {
  AUTOCOMPLETE_SCRIPT=$(realpath "_autocomplete_asalae.sh")
  BASHRC_FILE="$HOME/.bashrc"

  # Vérification de l'existence du script d'autocomplétion
  if [ ! -f "$AUTOCOMPLETE_SCRIPT" ]; then
      echo "Le script d'autocomplétion n'est pas trouvé : $AUTOCOMPLETE_SCRIPT"
      exit 1
  fi

  # Vérification si l'autocomplétion est déjà configurée dans ~/.bashrc
  if ! grep -q "$AUTOCOMPLETE_SCRIPT" ~/.bashrc; then
      last_char=$(tail -c 1 "$BASHRC_FILE")
      if [[ "$last_char" != $'\n' ]]; then
          # Ajout d'un retour à la ligne si nécessaire
          echo "" >> "$BASHRC_FILE"
      fi
      # Ajout de la commande pour charger l'autocomplétion dans ~/.bashrc
      echo -e "source $AUTOCOMPLETE_SCRIPT\n" >> ~/.bashrc
      echo "L'autocomplétion a été ajoutée à ~/.bashrc avec succès !"
      echo "veuillez taper la commande suivante pour activer immédiatement l'autocompletion:"
      echo "source ~/.bashrc"
  else
      echo "L'autocomplétion est déjà configurée dans ~/.bashrc."
  fi
}

 ## docker compose logs
function log() {
	docker compose logs -f "${ARGV[@]}"
}

function psql() {
  POSTGRES_USER=${POSTGRES_USER:-asalae}
  POSTGRES_DB=${POSTGRES_DB:-asalae}
  POSTGRES_DBPORT=${POSTGRES_DBPORT:-5432}
  dumpname=dbdump_"$(date +%Y-%m-%d_%H_%M_%S)".sql

  docker compose exec -ti asalae_db psql \
    -U "$POSTGRES_USER" \
    -d "$POSTGRES_DB" \
    -p "$POSTGRES_DBPORT" \
    "${ARGV[@]}"
}

function realpath() {
  SERVICE_NAME="asalae_web"
  FILE_URI=${ARGV[0]}
  CONTAINER_ID=$(docker compose ps -q "$SERVICE_NAME")
  if [ -z "$FILE_URI" ]; then
    echo "Erreur : Vous devez fournir un URI de fichier."
    exit 1
  fi
  if [ -z "$CONTAINER_ID" ]; then
    echo "Le conteneur pour le service $SERVICE_NAME n'a pas été trouvé."
    exit 1
  fi
  MOUNTS=$(docker inspect --format '{{json .Mounts}}' "$CONTAINER_ID")

  if [ -z "$MOUNTS" ]; then
    echo "Aucun montage trouvé pour le conteneur $CONTAINER_ID ($SERVICE_NAME)."
    exit 1
  fi

  # Boucler sur les montages pour afficher Source et Destination
  while IFS=$'\t' read -r SOURCE DESTINATION; do
    # Vérifier si le chemin fourni commence par la destination
    if [[ "$FILE_URI" == "$DESTINATION"* ]]; then
      # Remplacer la partie Destination par Source
      REAL_PATH="${FILE_URI/$DESTINATION/$SOURCE}"
      echo "$REAL_PATH"
      return 0
    fi
  done < <(echo "$MOUNTS" | jq -r '.[] | "\(.Source)\t\(.Destination)"')

  echo "Aucun montage ne correspond à l'URI fourni : $FILE_URI"
  exit 1
}

function reload() {
	docker compose exec -it asalae_web /usr/sbin/apachectl -k graceful
}

function restart() {
	  docker compose restart "${ARGV[@]}"
}

function restore() {
  if [ "${#ARGV[@]}" -lt 1 ]; then
    echo "Usage: ./asalae restore dbdump_<datetime>.sql [delete]" >&2
    exit 1
  fi
  if [ ! -s "${ARGV[0]}" ]; then
    echo "Erreur : le fichier '${ARGV[0]}' n'est pas valide." >&2
    exit 1
  fi

  sudo docker compose down --remove-orphans || exit 1

  if docker compose ps asalae_db | grep -q asalae_db; then
    echo "failed to docker compose down"
    exit 1
  fi

  if [ -z "$DATA_DIR" ]; then
      DATA_DIR="./data"
  fi
  DATA_DIR=$(realpath "$DATA_DIR")
  SUFFIX=$(date +%Y-%m-%d_%H_%M_%S)
  POSTGRES_DIR="${DATA_DIR}/postgres"
  POSTGRES_USER=${POSTGRES_USER:-asalae}
  POSTGRES_DB=${POSTGRES_DB:-asalae}
  POSTGRES_DBPORT=${POSTGRES_DBPORT:-5432}

  echo "${ARGV[1]}"
  if [ -d "$POSTGRES_DIR" ]
  then
    if [ "${ARGV[1]}" = "delete" ]
    then
      sudo rm -r "$POSTGRES_DIR" || exit 1
    else
      sudo mv "$POSTGRES_DIR" "${POSTGRES_DIR}_$SUFFIX" || exit 1
      echo "backup created ${POSTGRES_DIR}_$SUFFIX"
    fi
  fi

  docker compose up -d --wait asalae_db || exit 1
  docker compose cp "${ARGV[0]}" asalae_db:/db-dump/dump.sql || exit 1
  docker compose exec -i asalae_db psql -v ON_ERROR_STOP=1 \
      -U "$POSTGRES_USER" \
      -p "$POSTGRES_DBPORT" \
      -f /db-dump/dump.sql || exit 1
  docker compose exec -i asalae_db rm /db-dump/dump.sql || exit 1
  docker compose up -d --wait

  echo "dump appliqué avec succès"
  if [ "${ARGV[1]}" != "delete" ]
  then
    echo "vous pouvez vérifier le bon fonctionnement de l'application puis supprimer ${POSTGRES_DIR}_$SUFFIX"
  fi
}

function set-env-passwords() {
  COMPOSE_FILES=("docker-compose.yml")
  if [ -f "docker-compose.override.yml" ]; then
    COMPOSE_FILES+=("docker-compose.override.yml")
  fi
  ENV_FILE=".env"
  if [ ! -f "$ENV_FILE" ]; then
    touch "$ENV_FILE"
  fi

  REQUIRED_VARS=
  for file in "${COMPOSE_FILES[@]}"; do
    # Rechercher les variables avec :?
    VARNAMES=$(grep -oP '\$\{\w+:\?' "$file" | sed -E 's/\$\{(\w+):\?/\1/')
    REQUIRED_VARS+=("$VARNAMES")
  done

  # shellcheck disable=SC2207 # le code de remplaçement ajoute une valeur vide
  REQUIRED_VARS=($(printf "%s\n" "${REQUIRED_VARS[@]}" | sort -u))
  PASSWORD_ADDED=false
  for var in "${REQUIRED_VARS[@]}"; do
    if ! grep -q "^$var=" "$ENV_FILE" || grep -q "^$var=$" "$ENV_FILE"; then
      echo "Ajout ou remplacement de la variable manquante : $var"
      sed -i "/^$var=/d" "$ENV_FILE" # Supprimer la ligne existante (si présente)
      echo "$var=$(openssl rand -hex 32)" >> "$ENV_FILE" # Ajouter avec une valeur aléatoire
      PASSWORD_ADDED=true
    fi
  done
  if [ "$PASSWORD_ADDED" = false ]; then
    echo "Aucun mot de passe n'a été ajouté"
  fi
}

 ## docker compose up
function up() {
  docker compose up -d "${ARGV[@]}"
}

function version() {
  docker compose exec -it asalae_web php -r '$v=file("VERSION.txt");echo trim(array_pop($v)).PHP_EOL;'
}


################################################################################
# Définition des commandes
################################################################################

# Définir ici les nouvelles commandes
# BEGIN COMMAND EXTRACTOR
function select_function_asalae() {
  case $1 in
    bash)
      bash
      ;;
    bash-root)
      bash-root
      ;;
    certbot-hook)
      certbot-hook
      ;;
    cake)
      cake
      ;;
    compose)
      compose
      ;;
    copy-certificate)
      copy-certificate
      ;;
    down)
      down
      ;;
    dump)
      dump
      ;;
    help)
      help
      ;;
    install-autocomplete)
      install-autocomplete
      ;;
    log)
      log
      ;;
    psql)
      psql
      ;;
    realpath)
      realpath
      ;;
    reload)
      reload
      ;;
    restart)
      restart
      ;;
    restore)
      restore
      ;;
    set-env-passwords)
      set-env-passwords
      ;;
    up)
      up
      ;;
    version)
      version
      ;;
    *)
      echo "Fonction non définie pour l'argument passé : $1"
      ;;
  esac
}

# Vérification si au moins un argument est passé
if [[ -z "$ACTION" ]]; then
  help
  exit 1
fi

if [ "$SCRIPT_FILE" = "asalae" ]; then
    select_function_asalae "$ACTION"
fi
