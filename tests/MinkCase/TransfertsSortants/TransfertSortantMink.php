<?php

namespace Asalae\Test\MinkCase\TransfertsSortants;

use AsalaeCore\MinkSuite\MinkCase;
use Asalae\Cron\OutgoingTransferRequestArchiveDestruction;
use Asalae\Cron\OutgoingTransferTracking;
use Asalae\Cron\TransfersRetention;
use Asalae\MinkSuite\MinkArchive;
use Asalae\Model\Table\ArchivesTable;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;

class TransfertSortantMink extends MinkCase
{
    private EntityInterface $archive;

    public function setUp(): void
    {
        parent::setUp();
        $this->archive = MinkArchive::create();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        MinkArchive::delete($this->archive);
    }

    public function minkAjouterUneDemande()
    {
        $mink = $this;

        $Archives = $mink->databaseGetModel('Archives');
        $mink->databaseDeleteAll('OutgoingTransfers');
        $mink->databaseDeleteAll('ArchiveUnitsOutgoingTransferRequests');
        $mink->databaseDeleteAll('OutgoingTransferRequests');
        $ArchivingSystems = $mink->databaseGetModel('ArchivingSystems');
        $archivingSystem = $ArchivingSystems->get(1);
        $archivingSystem->set('url', 'https://localhost');
        $archivingSystem->set('password', 'sa2'); // corrige en cas de modification du Security.salt
        $archivingSystem->set('chunk_size', 2097152);
        $ArchivingSystems->saveOrFail($archivingSystem);
        $Volumes = $mink->databaseGetModel('Volumes');
        $baseVolumeFields = '{"path":"' . Configure::read('App.paths.data');
        $Volumes->updateAll(['fields' => $baseVolumeFields . '/volume01"}'], ['id' => 1]);
        $Volumes->updateAll(['fields' => $baseVolumeFields . '/volume02"}'], ['id' => 2]);
        $Volumes->updateAll(['fields' => $baseVolumeFields . '/volume03"}'], ['id' => 3]);
        $archive = $mink->archive ?? MinkArchive::create();
        $mink->doLogin();
        $mink->doClickMenu(__("Transferts sortants"), __("Archives transférables"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Archives transférables"));

        // filtres de recherche
        $mink->setFormFieldValue(__("Service producteur"), '2');
        $mink->setNamelessFormFieldValue(__("Ajouter un filtre"), 'archival_agency_identifier');
        $mink->setFormFieldValue(__("Cote"), $archive->get('archival_agency_identifier'));
        $mink->doClickButton(__("Filtrer"));
        $mink->waitAjaxComplete();
        $mink->doClickButton(__("Ajouter une demande de transfert sortant"));
        $mink->waitModalOpen();

        // ajout étape 1
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->setFormFieldValue(__("Système d'archivage électronique"), '1');
        $mink->submitModal();
        $mink->pausable();

        // ajout étape 2
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Service versant"), 'sa2');
        $mink->setFormFieldValue(__("Service producteur"), 'sa2');
        $mink->pausable();
        $mink->submitModal();
        $mink->pausable();

        // ajout étape 3
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(3);
        $mink->pausable();
        $mink->submitModal();
        $mink->pausable();

        $outgoingTransferRequest = $mink->databaseFind('OutgoingTransferRequests')->orderByDesc('id')->first();

        // envoi
        $mink->doClickAction(__("Envoyer {0}", $outgoingTransferRequest->get('identifier')));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();

        // validation
        $mink->doClickMenu(__("Transferts sortants"), __("Demandes de transferts sortants à valider"));
        $mink->assertPageTitleIs(__("Demandes de transferts sortants à valider"));
        $mink->pausable();
        $mink->doClickAction(__("Valider {0}", $outgoingTransferRequest->get('identifier')));
        $mink->waitModalOpen();

        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Action"), 'validate');
        $mink->setFormFieldValue(__("Commentaire"), 'test mink');
        $mink->submitModal();

        // worker
        $mink->assertNextJobSuccess('outgoing-transfer');
        $cond = ['outgoing_transfer_request_id' => $outgoingTransferRequest->id];
        $mink->databaseFindFirstOrFail('OutgoingTransfers', $cond);
        $newTransfer = $mink->databaseFind('Transfers')->orderByDesc('id')->firstOrFail();

        // nouveau transfert, stack d'archivage
        $mink->assertNextJobSuccess('analyse');
        $mink->assertNextJobSuccess('control');
        $mink->assertNextJobSuccess('archive');
        $mink->assertNextJobSuccess('archive-unit');
        $mink->assertNextJobSuccess('archive-binary');
        $mink->assertNextJobSuccess('archive-management');

        $queryTransferredArchive = $Archives->find()->innerJoinWith('Transfers');
        $queryTransferredArchive->where(['Transfers.id' => $newTransfer->id]);
        $transferedArchive = $queryTransferredArchive->firstOrFail();
        $mink->assertEquals(ArchivesTable::S_AVAILABLE, $transferedArchive->get('state'));

        // lancement du cron tracking
        $this->assertTrue($mink->runCron(OutgoingTransferTracking::class)->success);

        // Programmer la destruction
        $mink->doClickMenu(__("Transferts sortants"), __("Archives transférées à éliminer"));
        $mink->assertPageTitleIs(__("Archives transférées à éliminer"));
        $mink->pausable();
        $mink->doClickAction(__("Programmer la destruction des archives liées à {0}", $outgoingTransferRequest->get('identifier')));
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();

        // lancement du cron destruction
        $this->assertTrue($mink->runCron(OutgoingTransferRequestArchiveDestruction::class)->success);

        // lancement du cron rétention
        $this->assertTrue($mink->runCron(TransfersRetention::class)->success);
        $archive = $Archives->get($archive->id);
        $mink->assertEquals(ArchivesTable::S_TRANSFERRED, $archive->get('state'));
    }
}
