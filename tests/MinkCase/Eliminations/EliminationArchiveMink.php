<?php

namespace Asalae\Test\MinkCase\Eliminations;

use Asalae\MinkSuite\MinkArchive;
use Asalae\Model\Table\ConfigurationsTable;
use AsalaeCore\MinkSuite\MinkCase;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;

class EliminationArchiveMink extends MinkCase
{
    private EntityInterface $archive;

    public function setUp(): void
    {
        parent::setUp();
        $this->archive = MinkArchive::create();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        MinkArchive::delete($this->archive);
    }

    public function minkAjouterUneDemande()
    {
        $mink = $this;

        $mink->setAppConfig('Attestations.destruction_delay', 7);
        $loc = TableRegistry::getTableLocator();
        $Configurations = $loc->get('Configurations');
        $Configurations->updateAll(
            ['setting' => '1'],
            ['name' => ConfigurationsTable::CONF_ATTEST_DESTR_CERT_INTERMEDIATE]
        );
        $archive = $mink->archive ?? MinkArchive::create();
        $mink->doLogin();
        $mink->doClickMenu(__("Eliminations"), __("Archives éliminables"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Archives éliminables"));

        // filtres de recherche
        $mink->setFormFieldValue(__("Service producteur"), '2');
        $mink->setNamelessFormFieldValue(__("Ajouter un filtre"), 'archival_agency_identifier');
        $mink->setFormFieldValue(__("Cote"), $archive->get('archival_agency_identifier'));
        $mink->doClickButton(__("Filtrer"));
        $mink->waitAjaxComplete();
        $mink->doClickButton(__("Ajouter une demande d'élimination"));
        $mink->waitModalOpen();

        // ajout
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->submitModal();
        $mink->pausable();

        $DestructionRequests = $loc->get('DestructionRequests');
        $destructionRequest = $DestructionRequests->find()->orderByDesc('id')->first();
        $mink->out(__("Destruction {0}", $destructionRequest->get('identifier')));

        // envoi
        $mink->doClickAction(__("Envoyer {0}", $destructionRequest->get('identifier')));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();

        // validation
        $mink->doClickMenu(__("Eliminations"), __("Demandes d'élimination à valider"));
        $mink->assertPageTitleIs(__("Demandes d'élimination à valider"));
        $mink->pausable();
        $mink->doClickAction(__("Valider {0}", $destructionRequest->get('identifier')));
        $mink->waitModalOpen();

        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Action"), 'validate');
        $mink->setFormFieldValue(__("Commentaire"), 'test mink');
        $mink->submitModal();

        // worker
        $mink->assertNextJobSuccess('destruction');
        $DestructionNotifications = $loc->get('DestructionNotifications');
        $destructionNotification = $DestructionNotifications->find()->orderByDesc('id')->first();

        // notification
        $mink->doClickMenu(__("Eliminations"), __("Notifications d'élimination"));
        $mink->assertPageTitleIs(__("Notifications d'élimination"));
        $mink->out(__("Téléchargement de la notification {0}", $destructionNotification->get('identifier')));
        $file1 = $mink->doDownloadFromLink(__("Télécharger {0}", $destructionNotification->get('identifier')));
        $mink->assertTrue(file_exists($file1));
        $mink->out(__("Téléchargement de l'attestation d'élimination {0}", $destructionRequest->get('identifier')));
        $file2 = $mink->doDownloadFromLink(__("Attestation d'élimination de {0}", $destructionRequest->get('identifier')));
        $mink->assertTrue(file_exists($file2));
    }
}
