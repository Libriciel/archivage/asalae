<?php

namespace Asalae\Test\MinkCase\Administration;

use Asalae\Model\Table\ValidationChainsTable;
use AsalaeCore\MinkSuite\MinkCase;

class CircuitsDeValidationMink extends MinkCase
{
    public function minkAddValidationChain()
    {
        $mink = $this;
        $mink->databaseDeleteIfExists('ValidationChains', ['name' => 'mink']);
        $mink->databaseDeleteIfExists('ValidationChains', ['name' => 'mink2']);
        $mink->databaseDeleteIfExists('Users', ['username' => 'mink_user_2']);

        $mink->doLogin();
        $mink->doClickMenu(__("Administration"), __("Circuits de validation"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Circuits de validation"));
        $mink->doClickButton(__("Ajouter un circuit de validation"));
        $mink->waitModalOpen();

        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->setFormFieldValue(__("Type de circuit de validation"), ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM);
        $mink->setFormFieldValue(__("Nom"), 'mink');
        $mink->setFormFieldValue(__("Description"), 'Ceci est un test mink');
        $mink->submitModal();
        // NOTE: pas de assertResponseSuccess() car rechargement de la page

        $mink->assertEquals(1, $mink->databaseFind('ValidationChains', ['name' => 'mink'])->count());
        $mink->assertTableContains('#validation-chains-index-table', 'mink');
        $mink->pausable();
    }

    public function minkEditValidationChain()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Circuits de validation"));
        $mink->pausable();
        $mink->doClickAction(__("Modifier {0}", 'mink'));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Nom"), 'mink2');
        $mink->submitModal();
        // NOTE: pas de assertResponseSuccess() car rechargement de la page
        $mink->waitModalClose();
        $mink->pausable();

        $mink->assertEquals(0, $mink->databaseFind('ValidationChains', ['name' => 'mink'])->count());
        $mink->assertEquals(1, $mink->databaseFind('ValidationChains', ['name' => 'mink2'])->count());
        $mink->assertTableDoesNotContains('#validation-chains-index-table', 'mink');
        $mink->assertTableContains('#validation-chains-index-table', 'mink2');
    }

    public function minkAddValidationStage()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Circuits de validation"));
        $mink->pausable();
        $mink->doClickAction(__("Etapes de {0}", 'mink2'));
        $mink->pausable();
        $mink->waitAjaxComplete();
        $mink->assertPageTitleIs(__("Etapes de validation"));

        $mink->mountAjaxEvents();
        $mink->doClickButton(__("Ajouter une étape"));
        $mink->waitModalOpen();

        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->setFormFieldValue(__("Nom"), 'mink_stage_1');
        $mink->submitModal();
        // NOTE: pas de assertResponseSuccess() car rechargement de la page
        $mink->waitModalClose();
        $mink->pausable();

        $chainId = $mink->databaseFind('ValidationChains', ['name' => 'mink2'])->firstOrFail()->get('id');
        $mink->assertEquals(1, $mink->databaseFind('ValidationStages', ['name' => 'mink_stage_1'])->count());
        $mink->assertEquals(1, $mink->databaseFind('ValidationStages', ['validation_chain_id' => $chainId])->count());
    }

    public function minkEditValidationStage()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Etapes de validation"));
        $mink->pausable();
        $mink->doClickAction(__("Modifier {0}", 'mink_stage_1'));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Nom"), 'mink_stage_2');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->pausable();

        $mink->assertEquals(0, $mink->databaseFind('ValidationStages', ['name' => 'mink_stage_1'])->count());
        $mink->assertEquals(1, $mink->databaseFind('ValidationStages', ['name' => 'mink_stage_2'])->count());
        $mink->assertTableDoesNotContains('#validation-stages-index-table', 'mink_stage_1');
        $mink->assertTableContains('#validation-stages-index-table', 'mink_stage_2');
    }

    public function minkAddValidationActor()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Etapes de validation"));
        $mink->pausable();
        $mink->doClickAction(__("Acteurs de {0}", 'mink_stage_2'));
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertPageTitleIs(__("Acteurs de l'étape de validation"));

        $mink->doClickButton(__("Ajouter un acteur"));
        $mink->waitModalOpen();

        $mink->setFormFieldValue(__("Type d'acteur d'étape de circuit de validation"), 'USER');
        $mink->pausable();

        $mink->setFormFieldValue(__("Utilisateur"), 1);
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->pausable();

        $stageId = $mink->databaseFind('ValidationStages', ['name' => 'mink_stage_2'])->firstOrFail()->get('id');
        $mink->assertEquals(1, $mink->databaseFind('ValidationActors', ['validation_stage_id' => $stageId])->count());
        $name = $mink->databaseGetModel('Users')->get(1)->get('name');
        $mink->assertTableContains(
            '#validation-actors-index-table',
            "Utilisateur : $name, validation par Visa"
        );
    }

    public function minkEditValidationActor()
    {
        $mink = $this;

        $Users =  $mink->databaseGetModel('Users');
        $Users->saveOrFail(
            $user = $Users->newEntity(
                [
                    'username' => 'mink_user_2',
                    'email' => 'mink2@test.fr',
                    'org_entity_id' => 2,
                    'role_id' => 1,
                    'active' => true,
                ]
            )
        );
        $mink->assertPageTitleIs(__("Acteurs de l'étape de validation"));
        $mink->pausable();
        $mink->doClickAction(__("Modifier cet acteur"));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();

        $mink->setFormFieldValue(__("Utilisateur"), $user->get('id'));
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->pausable();

        $stageId = $mink->databaseFind('ValidationStages', ['name' => 'mink_stage_2'])->firstOrFail()->get('id');
        $mink->assertEquals(
            0,
            $mink->databaseFind('ValidationActors',
                [
                    'validation_stage_id' => $stageId,
                    'app_foreign_key' => '1'
                ]
            )->count()
        );
        $mink->assertEquals(
            1,
            $mink->databaseFind('ValidationActors', ['app_foreign_key' => $user->get('id')])->count()
        );
        $mink->assertTableDoesNotContains('#validation-actors-index-table', 'Utilisateur : Mink Man, validation par Visa');
        $mink->assertTableContains('#validation-actors-index-table', 'Utilisateur : mink_user_2, validation par Visa');
    }

    public function minkDeleteValidationActor()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Acteurs de l'étape de validation"));
        $mink->pausable();
        $mink->doClickAction(__("Supprimer cet acteur"));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();

        $stageId = $mink->databaseFind('ValidationStages', ['name' => 'mink_stage_2'])->firstOrFail()->get('id');
        $mink->assertEquals(
            0,
            $mink->databaseFind('ValidationActors', ['validation_stage_id' => $stageId])->count()
        );
    }

    public function mminkDeleteValidationStage()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Acteurs de l'étape de validation"));
        $mink->pausable();
        $mink->doClickBreadcrumb('Etape mink_stage_2');
        $mink->pausable();
        $mink->assertPageTitleIs(__("Etapes de validation"));
        $mink->pausable();

        $mink->assertEquals(1, $mink->databaseFind('ValidationStages', ['name' => 'mink2'])->count());

        $mink->doClickAction(__("Supprimer {0}", 'mink_stage_1'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertEquals(0, $mink->databaseFind('ValidationStages', ['name' => 'mink_stage_1'])->count());
        $mink->assertEquals(0, $mink->databaseFind('ValidationStages', ['name' => 'mink_stage_2'])->count());
        $mink->assertTableDoesNotContains('#validation-stages-index-table', 'mink_stage_1');
        $mink->assertTableDoesNotContains('#validation-stages-index-table', 'mink_stage_2');
    }

    public function minkDeleteValidationChain()
    {
        $mink = $this;
        $mink->doClickMenu(__("Administration"), __("Circuits de validation"));
        $mink->assertPageTitleIs(__("Circuits de validation"));
        $mink->assertEquals(1, $mink->databaseFind('ValidationChains', ['name' => 'mink2'])->count());

        $mink->doClickAction(__("Supprimer {0}", 'mink2'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertEquals(0, $mink->databaseFind('ValidationChains', ['name' => 'mink'])->count());
        $mink->assertEquals(0, $mink->databaseFind('ValidationChains', ['name' => 'mink2'])->count());
        $mink->assertTableDoesNotContains('#validation-chains-index-table', 'mink');
        $mink->assertTableDoesNotContains('#validation-chains-index-table', 'mink2');
    }
}
