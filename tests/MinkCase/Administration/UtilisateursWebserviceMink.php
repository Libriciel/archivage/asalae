<?php

namespace Asalae\Test\MinkCase\Administration;

use AsalaeCore\MinkSuite\MinkCase;
use Cake\ORM\TableRegistry;

class UtilisateursWebserviceMink extends MinkCase
{
    public function minkAddUserWebservice()
    {
        $mink = $this;
        $Users = TableRegistry::getTableLocator()->get('Users');
        $Users->deleteAll(['username' => 'mink_username']);
        $Users->deleteAll(['username' => 'mink_username2']);

        $mink->doLogin();
        $mink->doClickMenu(__("Administration"), __("Utilisateurs"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Utilisateurs"));
        $mink->doClickButton(__("Ajouter un accès de webservice"));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Entité de l'accès webservice"), 2);
        $mink->waitAjaxComplete();
        $mink->setFormFieldValue(__("Nom du webservice"), 'mink_webservice');
        $mink->setFormFieldValue(__("Rôle"), 8);

        $mink->setFormFieldValue(__("Identifiant de connexion"), 'mink_username');
        $mink->setFormFieldValue(__("Mot de passe de l'utilisateur"), 'm1nk');
        $mink->setFormFieldValue(__("Confirmer le mot de passe"), 'm1nk');

        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();

        $mink->assertEquals(1, $Users->find()->where(['username' => 'mink_username'])->count());
        $mink->assertTableContains('#users-index-table', 'mink_username');
        $mink->pausable();
    }


    public function minkViewUser()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Utilisateurs"));

        $mink->doClickAction(__("Visualiser {0}", 'mink_username'));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->assertTableContains('#users-index-table', 'mink_username');
        $mink->closeModal();
    }

    public function minkEditUserWebservice()
    {
        $mink = $this;
        $Users = TableRegistry::getTableLocator()->get('Users');

        $mink->assertPageTitleIs(__("Utilisateurs"));
        $mink->doClickAction(__("Modifier {0}", 'mink_username'));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Identifiant de connexion"), 'mink_username2');

        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->assertEquals(0, $Users->find()->where(['username' => 'mink_username'])->count());
        $mink->assertEquals(1, $Users->find()->where(['username' => 'mink_username2'])->count());
        $mink->assertTableContains('#users-index-table', 'mink_username2');
        $mink->pausable();
    }

    public function minkDeleteUserWebservice()
    {
        $mink = $this;
        $Users = TableRegistry::getTableLocator()->get('Users');

        $mink->doClickMenu(__("Administration"), __("Utilisateurs"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Utilisateurs"));

        $mink->doClickAction(__("Supprimer {0}", 'mink_username2'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertEquals(0, $Users->find()->where(['username' => 'mink_username'])->count());
        $mink->assertEquals(0, $Users->find()->where(['username' => 'mink_username2'])->count());
        $mink->assertTableDoesNotContains('#users-index-table', 'mink_username');
        $mink->assertTableDoesNotContains('#users-index-table', 'mink_username2');
    }
}
