<?php

namespace Asalae\Test\MinkCase\Administration;

use AsalaeCore\MinkSuite\MinkCase;
use Cake\ORM\TableRegistry;

class CodesDUAMink extends MinkCase
{
    public function minkAddAppraisalRuleCode()
    {
        $mink = $this;
        $AppraisalRuleCodes = TableRegistry::getTableLocator()->get('AppraisalRuleCodes');
        $AppraisalRuleCodes->deleteAll(['name' => 'mink']);
        $AppraisalRuleCodes->deleteAll(['name' => 'mink2']);

        $mink->doLogin();
        $mink->doClickMenu(__("Administration"), __("Codes des durées d'utilité administrative seda v2"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Codes des durées d'utilité administrative seda v2"));
        $mink->doClickButton(__("Ajouter un code de durée d'utilité administrative"));
        $mink->waitModalOpen();

        // création du code de DUA
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->setFormFieldValue(__("Code"), 'Code de DUA mink');
        $mink->setFormFieldValue(__("Nom"), 'mink');
        $mink->setFormFieldValue(__("Description"), 'Ceci est un test mink');
        $mink->setFormFieldValue(__("Durée"), 'P1Y6M15D');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->assertTableContains('#appraisal-rule-codes-index-table', 'mink');
    }

    public function minkVisualizeAppraisalRuleCode()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Codes des durées d'utilité administrative seda v2"));
        $mink->pausable();
        $mink->doClickAction(__("Visualiser {0}", "Code de DUA mink"));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->assertViewTableValueContains(__("Code"), "Code de DUA mink");
        $mink->closeModal();
    }

    public function minkEditAppraisalRuleCode()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Codes des durées d'utilité administrative seda v2"));
        $mink->pausable();
        $mink->doClickAction(__("Modifier {0}", 'Code de DUA mink'));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();

        //Modification des valeurs
        $mink->setFormFieldValue(__("Code"), 'Code de DUA mink2');
        $mink->setFormFieldValue(__("Nom"), 'mink2');
        $mink->setFormFieldValue(__("Description"), 'Ceci est un test mink2');
        $mink->setFormFieldValue(__("Durée"), 'P0Y');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->pausable();

        $AppraisalRuleCodes = TableRegistry::getTableLocator()->get('AppraisalRuleCodes');
        $mink->assertEquals(1, $AppraisalRuleCodes->find()->where(['name' => 'mink2'])->count());
        $mink->assertTableContains('#appraisal-rule-codes-index-table', 'mink2');
    }

    public function minkDeleteAppraisalRuleCode()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Codes des durées d'utilité administrative seda v2"));
        $AppraisalRuleCodes = TableRegistry::getTableLocator()->get('AppraisalRuleCodes');
        $mink->assertEquals(1, $AppraisalRuleCodes->find()->where(['name' => 'mink2'])->count());
        $mink->doClickAction(__("Supprimer {0}", 'Code de DUA mink2'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertEquals(0, $AppraisalRuleCodes->find()->where(['name' => 'mink2'])->count());
        $mink->assertTableDoesNotContains('#appraisal-rule-codes-index-table', 'mink');
        $mink->assertTableDoesNotContains('#appraisal-rule-codes-index-table', 'mink2');
    }
}
