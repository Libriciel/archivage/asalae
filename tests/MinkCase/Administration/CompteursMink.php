<?php

namespace Asalae\Test\MinkCase\Administration;

use AsalaeCore\MinkSuite\MinkCase;
use Cake\ORM\TableRegistry;

class CompteursMink extends MinkCase
{
    public function minkEditCompteur()
    {
        $mink = $this;

        $Counters = TableRegistry::getTableLocator()->get('Counters');
        $Counters->updateAll(
            ['name' => 'ArchiveTransfer'],
            ['identifier' => 'ArchiveTransfer']
        );

        $mink->doLogin();
        $mink->doClickMenu(__("Administration"), __("Compteurs"));

        $mink->assertPageTitleIs(__("Compteurs"));
        $mink->pausable();
        $mink->doClickAction(__("Modifier {0}", 'ArchiveTransfer'));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Nom du compteur"), 'mink');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->pausable();

        $counter = $Counters->find()->where(['identifier' => 'ArchiveTransfer', 'org_entity_id' => 2])->first();
        $mink->assertEquals('mink', $counter->get('name'));
        $mink->assertTableContains('#counters-index-table', 'mink');

        $Counters->updateAll(
            ['name' => 'ArchiveTransfer'],
            ['identifier' => 'ArchiveTransfer']
        );
    }
}
