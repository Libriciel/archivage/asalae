<?php

namespace Asalae\Test\MinkCase\Administration;

use AsalaeCore\MinkSuite\MinkCase;
use Cake\ORM\TableRegistry;

class AccordsDeVersementMink extends MinkCase
{
    public function minkAddAgreement()
    {
        $mink = $this;
        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $Agreements->deleteAll(['identifier' => 'mink']);
        $Agreements->deleteAll(['identifier' => 'mink2']);

        $mink->doLogin();
        $mink->doClickMenu(__("Administration"), __("Accords de versement"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Accords de versement"));
        $mink->doClickButton(__("Ajouter un accord de versement"));
        $mink->waitModalOpen();

        // étape 1
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->setFormFieldValue(__("Nom"), 'Agreement mink');
        $mink->setFormFieldValue(__("Identifiant unique"), 'mink');
        $mink->setFormFieldValue(__("Description"), 'Ceci est un test mink');
        $mink->setFormFieldValue(
            __("Circuit de validation des transferts non conformes"),
            '2'
        );
        $mink->submitModal();

        // étape 2
        $mink->assertModalStep(2);
        $mink->pausable();
        $mink->submitModal();

        // étape 3
        $mink->assertModalStep(3);
        $mink->pausable();
        $mink->submitModal();

        // étape 4
        $mink->assertModalStep(4);
        $mink->pausable();
        $mink->submitModal();
        $mink->assertResponseSuccess();

        $mink->assertEquals(1, $Agreements->find()->where(['identifier' => 'mink'])->count());
        $mink->assertTableContains('#agreements-index-table', 'mink');
    }

    public function minkEditAgreement()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Accords de versement"));
        $mink->pausable();
        $mink->doClickAction(__("Modifier {0}", 'Agreement mink'));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Identifiant unique"), 'mink2');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->pausable();

        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $mink->assertEquals(1, $Agreements->find()->where(['identifier' => 'mink2'])->count());
        $mink->assertTableContains('#agreements-index-table', 'mink2');
    }

    public function minkDeleteAgreement()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Accords de versement"));
        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $mink->assertEquals(1, $Agreements->find()->where(['identifier' => 'mink2'])->count());
        $mink->doClickAction(__("Supprimer {0}", 'Agreement mink'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertEquals(0, $Agreements->find()->where(['identifier' => 'mink'])->count());
        $mink->assertEquals(0, $Agreements->find()->where(['identifier' => 'mink2'])->count());
        $mink->assertTableDoesNotContains('#agreements-index-table', 'mink');
        $mink->assertTableDoesNotContains('#agreements-index-table', 'mink2');
    }

    public function minkDownloadCsv()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Accords de versement"));
        $file = $mink->doDownloadFromLink(__("Télécharger au format CSV"));
        $mink->assertTrue(file_exists($file));
        $mink->assertStringContainsString('Mon accord de versement', file_get_contents($file));
    }
}
