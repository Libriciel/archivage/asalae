<?php

namespace Asalae\Test\MinkCase\Administration;

use AsalaeCore\MinkSuite\MinkCase;
use Cake\ORM\TableRegistry;

class EntitesMink extends MinkCase
{
    public function minkAddOrgEntity()
    {
        $mink = $this;
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $OrgEntities->deleteAll(['identifier' => 'mink']);
        $OrgEntities->deleteAll(['identifier' => 'mink2']);
        $OrgEntities->updateAll(['description' => null], ['id' => 2]);

        $mink->doLogin();
        $mink->doClickMenu(__("Administration"), __("Entités"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Entités"));
        $mink->doClickButton(__("Tableau"));
        $mink->waitAjaxComplete();
        $mink->pausable();

        $mink->doClickButton(__("Ajouter une entité"));
        $mink->waitModalOpen();
        $mink->pausable();

        // étape 1
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->setFormFieldValue(__("Nom"), 'Entité mink');
        $mink->setFormFieldValue(__("Nom abrégé"), 'Mink');
        $mink->setFormFieldValue(__("Identifiant unique"), 'mink');
        $mink->setFormFieldValue(__("Description"), 'Ceci est un test mink');
        $mink->setFormFieldValue(__("Type d'entité"), '4');
        $mink->setFormFieldValue(__("Entité parente"), '2');
        $mink->submitModal();
        $mink->assertResponseSuccess();

        // étape 2
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->pausable();
        $mink->submitModal();
        $mink->assertResponseSuccess();

        $mink->wait(1000); // laisse le temps à la mise à jour de la session
        $mink->assertEquals(1, $OrgEntities->find()->where(['identifier' => 'mink'])->count());
        $mink->assertTableContains('#org-entities-index-table', 'mink');
    }

    public function minkEditOrgEntities()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Entités"));
        $mink->pausable();
        $mink->doClickAction(__("Modifier {0}", 'Entité mink'));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Identifiant unique"), 'mink2');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->pausable();

        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $mink->assertEquals(1, $OrgEntities->find()->where(['identifier' => 'mink2'])->count());
        $mink->assertTableContains('#org-entities-index-table', 'mink2');
    }

    public function minkEditArchivalAgency()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Entités"));
        $mink->pausable();
        $mink->doClickAction(__("Modifier {0}", "Mon Service d'archive"));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Description"), 'Ceci est un test mink');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();

        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $mink->assertEquals("Ceci est un test mink", $OrgEntities->get(2)->get('description'));
    }

    public function minkDeleteAgreement()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Entités"));
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $mink->assertEquals(1, $OrgEntities->find()->where(['identifier' => 'mink2'])->count());
        $mink->doClickAction(__("Supprimer {0}", 'Entité mink'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertEquals(0, $OrgEntities->find()->where(['identifier' => 'mink'])->count());
        $mink->assertEquals(0, $OrgEntities->find()->where(['identifier' => 'mink2'])->count());
        $mink->assertTableDoesNotContains('#org-entities-index-table', 'mink');
        $mink->assertTableDoesNotContains('#org-entities-index-table', 'mink2');
    }
}
