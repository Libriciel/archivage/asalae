<?php

namespace Asalae\Test\MinkCase\Administration;

use AsalaeCore\MinkSuite\MinkCase;
use Cake\ORM\TableRegistry;

class CodesDeRestrictionMink extends MinkCase
{
    public function minkAddRestrictionCode()
    {
        $mink = $this;
        $AccessRuleCodes = TableRegistry::getTableLocator()->get('AccessRuleCodes');
        $AccessRuleCodes->deleteAll(['name' => 'mink']);
        $AccessRuleCodes->deleteAll(['name' => 'mink2']);

        $mink->doLogin();
        $mink->doClickMenu(__("Administration"), __("Codes des restrictions d'accès seda v2"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Codes des restrictions d'accès seda v2"));
        $mink->doClickButton(__("Ajouter un code de restriction d'accès"));
        $mink->waitModalOpen();

        // création du code de restriction
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->setFormFieldValue(__("Code"), 'Code de restriction mink');
        $mink->setFormFieldValue(__("Nom"), 'mink');
        $mink->setFormFieldValue(__("Description"), 'Ceci est un test mink');
        $mink->setFormFieldValue(__("Durée"), 'P1Y6M15D');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->assertTableContains('#access-rule-codes-index-table', 'mink');
    }

    public function minkVisualizeAccessRuleCode()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Codes des restrictions d'accès seda v2"));
        $mink->pausable();
        $mink->doClickAction(__("Visualiser {0}", "Code de restriction mink"));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->assertViewTableValueContains(__("Code"), "Code de restriction mink");
        $mink->closeModal();
    }

    public function minkEditAccessRuleCode()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Codes des restrictions d'accès seda v2"));
        $mink->pausable();
        $mink->doClickAction(__("Modifier {0}", 'Code de restriction mink'));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();

        //Modification des valeurs
        $mink->setFormFieldValue(__("Code"), 'Code de restriction mink2');
        $mink->setFormFieldValue(__("Nom"), 'mink2');
        $mink->setFormFieldValue(__("Description"), 'Ceci est un test mink2');
        $mink->setFormFieldValue(__("Durée"), 'P0Y');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->pausable();

        $AccessRuleCodes = TableRegistry::getTableLocator()->get('AccessRuleCodes');
        $mink->assertEquals(1, $AccessRuleCodes->find()->where(['name' => 'mink2'])->count());
        $mink->assertTableContains('#access-rule-codes-index-table', 'mink2');
    }

    public function minkDeleteAccessRuleCode()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Codes des restrictions d'accès seda v2"));
        $AccessRuleCodes = TableRegistry::getTableLocator()->get('AccessRuleCodes');
        $mink->assertEquals(1, $AccessRuleCodes->find()->where(['name' => 'mink2'])->count());
        $mink->doClickAction(__("Supprimer {0}", 'Code de restriction mink2'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertEquals(0, $AccessRuleCodes->find()->where(['name' => 'mink2'])->count());
        $mink->assertTableDoesNotContains('#access-rule-codes-index-table', 'mink');
        $mink->assertTableDoesNotContains('#access-rule-codes-index-table', 'mink2');
    }
}
