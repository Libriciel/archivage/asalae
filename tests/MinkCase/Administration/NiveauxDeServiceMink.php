<?php

namespace Asalae\Test\MinkCase\Administration;

use AsalaeCore\MinkSuite\MinkCase;
use Cake\ORM\TableRegistry;

class NiveauxDeServiceMink extends MinkCase
{
    public function minkAddServiceLevel()
    {
        $mink = $this;
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $ServiceLevels->deleteAll(['identifier' => 'mink']);
        $ServiceLevels->deleteAll(['identifier' => 'mink2']);

        $mink->doLogin();
        $mink->doClickMenu(__("Administration"), __("Niveaux de service"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Niveaux de service"));
        $mink->doClickButton(__("Ajouter un niveau de service"));
        $mink->waitModalOpen();

        // étape 1
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->setFormFieldValue(__("Nom"), 'Niveau de Service mink');
        $mink->setFormFieldValue(__("Identifiant unique"), 'mink');
        $mink->setFormFieldValue(__("Description"), 'Ceci est un test mink');
        $mink->setFormFieldValue(__("Espace de stockage sécurisé"), 'ECS-001');
        $mink->submitModal();

        // étape 2
        $mink->assertModalStep(2);
        $mink->pausable();
        $mink->submitModal();

        // étape 3
        $mink->assertModalStep(3);
        $mink->pausable();
        $mink->submitModal();
        $mink->assertResponseSuccess();

        $mink->assertEquals(1, $ServiceLevels->find()->where(['identifier' => 'mink'])->count());
        $mink->assertTableContains('#service-levels-index-table', 'mink');
        $mink->pausable();
    }

    public function minkEditProfile()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Niveaux de service"));
        $mink->pausable();
        $mink->doClickAction(__("Modifier {0}", 'Niveau de Service mink'));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Identifiant unique"), 'mink2');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->pausable();

        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $mink->assertEquals(1, $ServiceLevels->find()->where(['identifier' => 'mink2'])->count());
        $mink->assertTableContains('#service-levels-index-table', 'mink2');
    }

    public function minkDeleteServiceLevel()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Niveaux de service"));
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $mink->assertEquals(1, $ServiceLevels->find()->where(['identifier' => 'mink2'])->count());
        $mink->doClickAction(__("Supprimer {0}", 'Niveau de Service mink'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertEquals(0, $ServiceLevels->find()->where(['identifier' => 'mink'])->count());
        $mink->assertEquals(0, $ServiceLevels->find()->where(['identifier' => 'mink2'])->count());
        $mink->assertTableDoesNotContains('#service-levels-index-table', 'mink');
        $mink->assertTableDoesNotContains('#service-levels-index-table', 'mink2');
    }
}
