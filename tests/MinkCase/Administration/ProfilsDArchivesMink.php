<?php

namespace Asalae\Test\MinkCase\Administration;

use AsalaeCore\MinkSuite\MinkCase;
use Cake\ORM\TableRegistry;

class ProfilsDArchivesMink extends MinkCase
{
    public function minkAddProfile()
    {
        $mink = $this;
        $Profiles = TableRegistry::getTableLocator()->get('Profiles');
        $Profiles->deleteAll(['identifier' => 'mink']);
        $Profiles->deleteAll(['identifier' => 'mink2']);

        $mink->doLogin();
        $mink->doClickMenu(__("Administration"), __("Profils d'archives"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Profils d'archives"));
        $mink->doClickButton(__("Ajouter un profil d'archives"));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Nom"), 'Profil mink');
        $mink->setFormFieldValue(__("Identifiant"), 'mink');
        $mink->setFormFieldValue(__("Description"), 'Ceci est un test mink');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->pausable();

        $mink->assertEquals(1, $Profiles->find()->where(['identifier' => 'mink'])->count());
        $mink->assertTableContains('#profiles-index-table', 'mink');
        $mink->pausable();
    }

    public function minkEditProfile()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Profils d'archives"));
        $mink->pausable();
        $mink->doClickAction(__("Modifier {0}", 'Profil mink'));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Identifiant"), 'mink2');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->pausable();

        $Profiles = TableRegistry::getTableLocator()->get('Profiles');
        $mink->assertEquals(1, $Profiles->find()->where(['identifier' => 'mink2'])->count());
        $mink->assertTableContains('#profiles-index-table', 'mink2');
    }

    public function minkDeleteProfile()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Profils d'archives"));
        $Profiles = TableRegistry::getTableLocator()->get('Profiles');
        $mink->assertEquals(1, $Profiles->find()->where(['identifier' => 'mink2'])->count());
        $mink->doClickAction(__("Supprimer {0}", 'Profil mink'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertEquals(0, $Profiles->find()->where(['identifier' => 'mink'])->count());
        $mink->assertEquals(0, $Profiles->find()->where(['identifier' => 'mink2'])->count());
        $mink->assertTableDoesNotContains('#profiles-index-table', 'mink');
        $mink->assertTableDoesNotContains('#profiles-index-table', 'mink2');
    }

    public function minkDownloadCsv()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Profils d'archives"));
        $file = $mink->doDownloadFromLink(__("Télécharger au format CSV"));
        $mink->assertTrue(file_exists($file));
        $mink->assertStringContainsString('profile1', file_get_contents($file));
    }
}
