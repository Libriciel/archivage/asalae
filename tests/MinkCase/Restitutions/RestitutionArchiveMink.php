<?php

namespace Asalae\Test\MinkCase\Restitutions;

use Asalae\Cron\DestructionCertificateCreation;
use Asalae\Cron\RestitutionRequestArchiveDestruction;
use Asalae\MinkSuite\MinkArchive;
use Asalae\Model\Table\ConfigurationsTable;
use AsalaeCore\MinkSuite\MinkCase;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;

class RestitutionArchiveMink extends MinkCase
{
    private EntityInterface $archive;

    public function setUp(): void
    {
        parent::setUp();
        $this->archive = MinkArchive::create();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        MinkArchive::delete($this->archive);
    }

    public function minkAjouterUneDemande()
    {
        $mink = $this;

        $mink->setAppConfig('Attestations.destruction_delay', 7);
        $loc = TableRegistry::getTableLocator();
        $Configurations = $loc->get('Configurations');
        $Configurations->updateAll(
            ['setting' => '1'],
            ['name' => ConfigurationsTable::CONF_ATTEST_DESTR_CERT_INTERMEDIATE]
        );
        $archive = $mink->archive ?? MinkArchive::create();
        $mink->doLogin();
        $mink->doClickMenu(__("Restitutions"), __("Archives restituables"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Archives restituables"));

        // filtres de recherche
        $mink->setFormFieldValue(__("Service producteur"), '2');
        $mink->setNamelessFormFieldValue(__("Ajouter un filtre"), 'archival_agency_identifier');
        $mink->setFormFieldValue(__("Cote"), $archive->get('archival_agency_identifier'));
        $mink->doClickButton(__("Filtrer"));
        $mink->waitAjaxComplete();
        $mink->doClickButton(__("Ajouter une demande de restitution"));
        $mink->waitModalOpen();

        // ajout
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->submitModal();
        $mink->pausable();

        $RestitutionRequests = $loc->get('RestitutionRequests');
        $restitutionRequest = $RestitutionRequests->find()->orderByDesc('id')->first();
        $mink->out(__("Restitution {0}", $restitutionRequest->get('identifier')));

        // envoi
        $mink->doClickAction(__("Envoyer {0}", $restitutionRequest->get('identifier')));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();

        // validation
        $mink->doClickMenu(__("Restitutions"), __("Demandes de restitution à valider"));
        $mink->assertPageTitleIs(__("Demandes de restitution à valider"));
        $mink->pausable();
        $mink->doClickAction(__("Valider {0}", $restitutionRequest->get('identifier')));
        $mink->waitModalOpen();

        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Action"), 'validate');
        $mink->setFormFieldValue(__("Commentaire"), 'test mink');
        $mink->submitModal();

        // worker
        $mink->assertNextJobSuccess('restitution-build');
        $Restitutions = $loc->get('Restitutions');
        $cond = ['restitution_request_id' => $restitutionRequest->id];
        $restitution = $Restitutions->find()->where($cond)->firstOrFail();

        // download
        $mink->doClickMenu(__("Restitutions"), __("Récupération des restitutions"));
        $mink->assertPageTitleIs(__("Récupération des restitutions"));
        $mink->pausable();
        $mink->out(__("Téléchargement de {0}", $restitution->get('basename')));
        $file1 = $mink->doDownloadFromLink(__("Télécharger {0}", $restitution->get('basename')));
        $mink->assertTrue(file_exists($file1));

        // acquittement
        $mink->doClickMenu(__("Restitutions"), __("Acquittement des restitutions"));
        $mink->assertPageTitleIs(__("Acquittement des restitutions"));
        $mink->pausable();
        $mink->doClickAction(__("Acquitter {0}", $restitution->get('identifier')));
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();

        // élimination
        $mink->doClickMenu(__("Restitutions"), __("Archives restituées et acquittées à éliminer"));
        $mink->assertPageTitleIs(__("Archives restituées et acquittées à éliminer"));
        $mink->out(__("Téléchargement de {0}", $restitutionRequest->get('identifier')));
        $file2 = $mink->doDownloadFromLink(__("Attestation de restitution de {0}", $restitutionRequest->get('identifier')));
        $mink->assertTrue(file_exists($file2));
        $mink->pausable();

        // état restitution créé, destruction null
        $restitutionRequest = $RestitutionRequests->get($restitutionRequest->id);
        $mink->assertTrue($restitutionRequest->get('restitution_certified'));
        $mink->assertNull($restitutionRequest->get('destruction_certified'));

        // désigner pour élimination
        $mink->doClickAction(__("Programmer la destruction des archives liées à {0}", $restitutionRequest->get('identifier')));
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();

        // lancement du cron
        $this->assertTrue($mink->runCron(RestitutionRequestArchiveDestruction::class)->success);

        // état destruction false (intermédiaire)
        $restitutionRequest = $RestitutionRequests->get($restitutionRequest->id);
        $mink->assertFalse($restitutionRequest->get('destruction_certified'));

        // lancement du cron avec suppression du délai
        $mink->setAppConfig('Attestations.destruction_delay', 0);
        $this->assertTrue($mink->runCron(DestructionCertificateCreation::class)->success);

        // état destruction true (définitif)
        $restitutionRequest = $RestitutionRequests->get($restitutionRequest->id);
        $mink->assertTrue($restitutionRequest->get('destruction_certified'));
    }
}
