<?php

namespace Asalae\Test\MinkCase\Transferts;

use AsalaeCore\MinkSuite\MinkCase;

class CreerUnTransfertMink extends MinkCase
{
    public function minkCreerUnTransfert()
    {
        $mink = $this;

        $subquery = $mink->databaseFind('TransferAttachments', ['transfer_identifier' => 'mink']);
        $subquery->select(['TransferAttachments.id'], true);
        $subquery->innerJoinWith('Transfers');
        $mink->databaseDeleteIfExists('TransferAttachments', ['id IN' => $subquery]);
        $mink->databaseDeleteIfExists('Transfers', ['transfer_identifier' => 'mink']);

        $mink->doLogin();
        $mink->doClickMenu(__("Transferts"), __("Créer un transfert"));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();

        // étape 1
        $mink->assertModalStep(1);
        $mink->setFormFieldValue(__("Version du SEDA"), 'seda1.0');
        $mink->setFormFieldValue(__("TransferIdentifier : Identifiant du transfert"), 'mink');
        $mink->setFormFieldValue(__("Date : Date de l'émission du message"), '24/05/2022 16:16');
        $mink->setFormFieldValue(__("Comment : Commentaires"), 'fait avec mink');
        $mink->setFormFieldValue(__("TransferringAgency : Service versant"), 4);
        $mink->setFormFieldValue(__("OriginatingAgency : Service producteur"), 5);
        $mink->setFormFieldValue(__("ArchivalAgreement : Accord de versement"), 1);
        $mink->setFormFieldValue(__("ArchivalProfile : Profil d'archive"), 1);
        $mink->setFormFieldValue(
            __("ServiceLevel : Niveau de service demandé (disponibilité, sécurité...)."),
            1
        );
        $mink->setFormFieldValue(__("Name : Intitulé de l'<Archive>."), "archive mink");
        $mink->setFormFieldValue(__("DescriptionLevel : Niveau de description de l'archive"), "file");
        $mink->setFormFieldValue(__("Code : Règle du délai de communicabilité"), "AR038");
        $mink->setFormFieldValue(__("StartDate : Date de départ du calcul (communicabilité)"), "24/05/2022");
        $mink->setFormFieldValue(__("Code : Sort final à appliquer"), "conserver");
        $mink->setFormFieldValue(__("Duration : Durée d'utilité administrative"), "P0Y");
        $mink->setFormFieldValue(__("StartDate : Date de départ du calcul (sort final)"), "24/05/2022");
        $mink->submitModal();

        // étape 2
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(2);
        $mink->doClickButton(__("Créer l'arborescence de transfert à partir d'un fichier zip/tar.gz"));
        $file = $mink->fileCreate('mink.txt', 'test');
        $zip = $mink->fileCreateZip('data.zip', [$file]);
        $mink->setFormFieldValue("Téléversement de fichiers", $zip);
        $mink->waitUploadComplete();
        $mink->wait();

        // étape 3
        $mink->wait(100);
        $mink->waitAjaxComplete();
// FIXME plantage du navigateur (uniquement dans la CI) - raisons inconnu # note: potentielement réparé
        $mink->doJstreeOpen('ArchiveTransfer', 'Archive archive mink', 'ContentDescription');
        $mink->waitAjaxComplete();
        $mink->wait();
        $mink->doJstreeClick('ArchiveTransfer', 'Archive archive mink', 'ContentDescription', 'Keyword');
        $mink->waitAjaxComplete();
        $mink->setInputValue('css', '#keywordcontent-0', "mink");
        $mink->submitModal();
        $mink->waitAjaxComplete();
        $mink->submitModal();
        $mink->waitAjaxComplete();
        $mink->assertModalIsClosed();
    }
}
