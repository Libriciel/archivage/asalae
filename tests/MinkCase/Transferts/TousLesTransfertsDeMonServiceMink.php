<?php

namespace Asalae\Test\MinkCase\Transferts;

use AsalaeCore\MinkSuite\MinkCase;

class TousLesTransfertsDeMonServiceMink extends MinkCase
{
    public function minkVisualiser()
    {
        $mink = $this;

        $mink->doLogin();
        $mink->doClickMenu(__("Transferts"), __("Tous les transferts de mon service"));
        $mink->waitAjaxComplete();
        $mink->pausable();

        $mink->assertPageTitleIs(__("Tous les transferts de mon service"));
        $mink->pausable();
        $mink->doClick('#transfers-index-my-entity-table .btn-filter[data-fields*="transfer_identifier[0]"]');
        $mink->setFormFieldValueById('filter-transfer-identifier-0', 'atr_alea_70f90eacdb2d3b53ad7e87f808a82430');
        $mink->doClick('#transfers-index-my-entity-table .filter-popup button[type=submit]');
        $mink->waitAjaxComplete();

        $mink->doClickAction(__("Visualiser {0}", 'atr_alea_70f90eacdb2d3b53ad7e87f808a82430'));
        $mink->waitAjaxComplete();

        $mink->assertModalIsOpenned();
        $mink->pausable();
        $file = $mink->doDownloadFromLink("ArchiveTransferReply_atr_alea_70f90eacdb2d3b53ad7e87f808a82430.xml");
        $mink->assertTrue(file_exists($file));

        $mink->doClickTab(__("Validation"));
        $value = __("E-mail: {0} ({1})", 'M. Dupont Martin', 'dupont.martin@test.fr');
        $mink->assertViewTableValueContains(__("Doit être validé par"), $value);

        $mink->doClickTab(__("Fichiers des pièces jointes"));
        $file = $mink->doDownloadFromLink(__("Télécharger {0}", 'sample.odt'));
        $mink->assertTrue(file_exists($file));
        $data = $mink->extractDataFromTable('#view-transfer-attachments', 7);
        $mink->assertTrue(isset($data['hash']));
        $mink->assertEquals($data['hash'], hash_file('sha256', $file));
    }
}
