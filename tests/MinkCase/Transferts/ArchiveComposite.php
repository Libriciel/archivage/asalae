<?php

namespace Asalae\Test\MinkCase\Transferts;

use Asalae\MinkSuite\MinkArchive;
use Asalae\Model\Table\ArchivesTable;
use AsalaeCore\MinkSuite\MinkCase;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Datacompressor\Utility\DataCompressor;

class ArchiveComposite extends MinkCase
{
    private EntityInterface $archive;

    public function setUp(): void
    {
        parent::setUp();
        $this->archive = MinkArchive::create();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        MinkArchive::delete($this->archive);
    }

    public function minkAjouterUnTransferComposite()
    {
        $mink = $this;

        $loc = TableRegistry::getTableLocator();
        $archive = $mink->archive ?? MinkArchive::create();

        /**
         * Création du transfert
         */
        $mink->doLogin();
        $mink->doClickMenu(__("Transferts"), __("Créer un transfert"));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();

        // étape 1
        $mink->assertModalStep(1);
        $mink->setFormFieldValue(__("Version du SEDA"), 'seda2.1');
        $mink->setFormFieldValue(__("TransferIdentifier : Identifiant du transfert"), '');
        $mink->setFormFieldValue(__("Date : Date de l'émission du message"), '24/05/2022 16:16');
        $mink->setFormFieldValue(__("Comment : Commentaires"), 'fait avec mink');
        $mink->setFormFieldValue(__("TransferringAgency : Service versant"), 2);
        $mink->setFormFieldValue(__("OriginatingAgency : Service producteur"), 5);
        $mink->setFormFieldValue(__("ArchivalAgreement : Accord de versement"), 1);
        $mink->setFormFieldValue(__("ArchivalProfile : Profil d'archive"), 1);
        $mink->setFormFieldValue(
            __("ServiceLevel : Niveau de service demandé (disponibilité, sécurité...)."),
            1
        );
        $mink->setFormFieldValue(__("Name : Intitulé de l'<Archive>."), "archive composite mink");
        $mink->setFormFieldValue(__("DescriptionLevel : Niveau de description de l'archive"), "file");
        $mink->setFormFieldValue(__("Code : Règle du délai de communicabilité"), "AR038");
        $mink->setFormFieldValue(__("StartDate : Date de départ du calcul (communicabilité)"), "24/05/2022");
        $mink->setFormFieldValue(__("Code : Sort final à appliquer"), "conserver");
        $mink->setFormFieldValue(__("Duration : Durée d'utilité administrative"), "P0Y");
        $mink->setFormFieldValue(__("StartDate : Date de départ du calcul (sort final)"), "24/05/2022");
        $mink->submitModal();

        // étape 2
        $mink->assertModalStep(2);
        $mink->doClickButton(__("Créer l'arborescence de transfert à partir d'un fichier zip/tar.gz"));
        file_put_contents($file = MinkCase::getTmpDir() . DS . 'mink.txt', 'test');
        DataCompressor::compress($file, $zip = MinkCase::getTmpDir() . DS . 'data.zip');
        $mink->setFormFieldValue("Téléversement de fichiers", $zip);
        $mink->waitUploadComplete();
        $mink->wait();
        $mink->submitModal();

        // étape 3
        $mink->wait(100);
        $mink->waitAjaxComplete();
        $mink->doJstreeOpen(
            'ArchiveTransfer',
            'DataObjectPackage',
            'DescriptiveMetadata',
            'ArchiveUnit archive composite mink',
            'Content'
        );
        $mink->waitAjaxComplete();
        $mink->doJstreeClick(
            'ArchiveTransfer',
            'DataObjectPackage',
            'DescriptiveMetadata',
            'ArchiveUnit archive composite mink',
            'Content',
            'RelatedObjectReference'
        );
        $mink->waitAjaxComplete();
        $mink->submitModal();
        $mink->doJstreeClick(
            'ArchiveTransfer',
            'DataObjectPackage',
            'DescriptiveMetadata',
            'ArchiveUnit archive composite mink',
            'Content',
            'RelatedObjectReference',
            'IsPartOf'
        );
        $mink->waitAjaxComplete();
        $mink->setFormFieldValue("RepositoryArchiveUnitPID", $archive->get('archival_agency_identifier'));
        $mink->submitModal();
        $mink->waitAjaxComplete();
        $mink->submitModal();
        $mink->waitAjaxComplete();
        $mink->assertModalIsClosed();

        /**
         * analyse
         */
        $Transfers = $loc->get('Transfers');
        $transfer = $Transfers->find()->orderByDesc('id')->first();
        $mink->doClickAction(__("Analyser {0}", $transfer->get('transfer_identifier')));
        $mink->waitAjaxComplete();
        $mink->doClickButton(__("Retour"));
        $mink->waitAjaxComplete();

        /**
         * envoyer
         */
        $mink->doClickAction(__("Envoyer {0}", $transfer->get('transfer_identifier')));
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();

        /**
         * workers
         */
        $mink->assertNextJobSuccess('analyse');
        $mink->assertNextJobSuccess('control');
        $mink->assertNextJobSuccess('archive');
        $mink->assertNextJobSuccess('archive-unit');
        $mink->assertNextJobSuccess('archive-binary');
        $mink->assertNextJobSuccess('archive-management');

        $Archives = $loc->get('Archives');
        $archive = $Archives->find()->innerJoinWith('Transfers')->where(['transfer_id' => $transfer->id])->firstOrFail();
        $mink->assertEquals(ArchivesTable::S_AVAILABLE, $archive->get('state'));
    }
}
