<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * VersionsFixture
 */
class VersionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'subject' => 'asalae',
                'version' => '2.0.8',
                'created' => '2021-12-09T13:53:55',
            ],
            [// id=2
                'subject' => 'asalae',
                'version' => '2.0.0',
                'created' => '2021-12-09T13:53:53',
            ],
            [// id=3
                'subject' => 'asalae',
                'version' => '2.1.0',
                'created' => '2021-12-09T13:53:53',
            ],
            [// id=4
                'subject' => 'asalae',
                'version' => '2.0.1',
                'created' => '2021-12-09T13:53:53',
            ],
            [// id=5
                'subject' => 'asalae',
                'version' => '2.0.3',
                'created' => '2021-12-09T13:53:53',
            ],
            [// id=6
                'subject' => 'asalae',
                'version' => '2.0.4',
                'created' => '2021-12-09T13:53:53',
            ],
            [// id=7
                'subject' => 'asalae',
                'version' => '2.0.5',
                'created' => '2021-12-09T13:53:53',
            ],
            [// id=8
                'subject' => 'asalae',
                'version' => '2.0.6',
                'created' => '2021-12-09T13:53:53',
            ],
            [// id=9
                'subject' => 'asalae',
                'version' => '2.0.7',
                'created' => '2021-12-09T13:53:53',
            ],
            [// id=10
                'subject' => 'asalae',
                'version' => '2.0.9',
                'created' => '2021-12-09T13:53:53',
            ],
            [// id=11
                'subject' => 'asalae',
                'version' => '2.1.4',
                'created' => '2021-12-09T13:53:53',
            ],
            [// id=12
                'subject' => 'asalae',
                'version' => '2.1.1',
                'created' => '2021-12-09T13:53:55',
            ],
            [// id=13
                'subject' => 'asalae',
                'version' => '2.1.2',
                'created' => '2021-12-09T13:53:55',
            ],
            [// id=14
                'subject' => 'asalae',
                'version' => '2.0.10',
                'created' => '2021-12-09T13:53:55',
            ],
            [// id=15
                'subject' => 'asalae',
                'version' => '2.1.3',
                'created' => '2021-12-09T13:53:55',
            ],
            [// id=16
                'subject' => 'asalae',
                'version' => '2.0.11',
                'created' => '2021-12-09T13:53:55',
            ],
            [// id=17
                'subject' => 'PronomTable',
                'version' => '99',
                'created' => '2021-12-09T13:54:24',
            ],
            [// id=18
                'subject' => 'asalae',
                'version' => '2.1.5',
                'created' => '2021-12-09T00:00:00',
            ],
            [// id=19
                'subject' => 'asalae',
                'version' => '2.1.6',
                'created' => '2022-06-13T00:00:00',
            ],
            [// id=20
                'subject' => 'asalae',
                'version' => '2.1.7',
                'created' => '2022-09-07T10:44:43',
            ],
            [// id=21
                'subject' => 'asalae',
                'version' => '2.1.8',
                'created' => '2022-09-07T10:44:43',
            ],
            [// id=22
                'subject' => 'asalae',
                'version' => '2.1.9',
                'created' => '2022-09-07T10:44:43',
            ],
            [// id=23
                'subject' => 'asalae',
                'version' => '2.2.0',
                'created' => '2022-06-13T00:00:00',
            ],
            [// id=24
                'subject' => 'asalae',
                'version' => '2.1.10',
                'created' => '2023-10-02T10:27:51',
            ],
            [// id=25
                'subject' => 'asalae',
                'version' => '2.1.11',
                'created' => '2023-10-02T10:27:51',
            ],
        ];
        parent::init();
    }
}
