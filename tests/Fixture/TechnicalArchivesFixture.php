<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TechnicalArchivesFixture
 */
class TechnicalArchivesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'state' => 'creating',
                'type' => 'EVENTS_LOGS',
                'is_integrity_ok' => null,
                'archival_agency_id' => 2,
                'archival_agency_identifier' => 'c1037259-43b8-4b90-988a-4239839ac8f3',
                'secure_data_space_id' => 1,
                'storage_path' => 'sa/technical_archives/c1037259-43b8-4b90-988a-4239839ac8f3',
                'name' => 'Journaux des événements de asalae : 2021',
                'created' => '2021-12-09T01:00:00',
                'original_size' => 194682,
                'original_count' => 5,
                'timestamp_size' => 9909,
                'timestamp_count' => 5,
                'management_size' => 0,
                'management_count' => 0,
                'last_state_update' => null,
                'integrity_date' => null,
                'filesexist_date' => null,
            ],
            [// id=2
                'state' => 'creating',
                'type' => 'EVENTS_LOGS',
                'is_integrity_ok' => null,
                'archival_agency_id' => 2,
                'archival_agency_identifier' => '6621dae4-ff82-4429-bd2d-73f514c1095d',
                'secure_data_space_id' => 1,
                'storage_path' => 'sa/technical_archives/6621dae4-ff82-4429-bd2d-73f514c1095d',
                'name' => 'Journaux des événements de asalae : 2022',
                'created' => '2022-01-06T01:00:00',
                'original_size' => 12028,
                'original_count' => 3,
                'timestamp_size' => 5943,
                'timestamp_count' => 3,
                'management_size' => 0,
                'management_count' => 0,
                'last_state_update' => null,
                'integrity_date' => null,
                'filesexist_date' => null,
            ],
        ];
        parent::init();
    }
}
