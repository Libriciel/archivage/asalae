<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * NotificationsFixture
 */
class NotificationsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'user_id' => 4,
                'text' => '<h4>control</h4>Le transfert sa_OAT_1 est non conforme.',
                'created' => '2021-12-09T17:30:56',
                'css_class' => 'alert-warning',
            ],
            [// id=2
                'user_id' => 1,
                'text' => '<h4>control</h4>Le transfert atr_alea_70f90eacdb2d3b53ad7e87f808a82430 est conforme.',
                'created' => '2021-12-10T09:28:21',
                'css_class' => 'alert-info',
            ],
            [// id=3
                'user_id' => 1,
                'text' => '<h4>batch-treatment</h4>La demande de traitement va prochainement être traitée',
                'created' => '2022-09-16T14:01:27',
                'css_class' => 'alert-warning',
            ],
        ];
        parent::init();
    }
}
