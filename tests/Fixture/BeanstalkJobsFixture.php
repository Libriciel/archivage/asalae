<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BeanstalkJobsFixture
 */
class BeanstalkJobsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'tube' => 'test',
                'priority' => 1024,
                'created' => '2021-12-10T10:33:31',
                'user_id' => 1,
                'delay' => 0,
                'ttr' => 60,
                'errors' => null,
                'data' => null,
                'beanstalk_worker_id' => null,
                'object_model' => null,
                'object_foreign_key' => null,
                'job_state' => 'pending',
                'last_state_update' => null,
                'states_history' => null,
            ],
            [// id=2
                'tube' => 'batch-treatment',
                'priority' => 1024,
                'created' => '2022-09-16T14:01:27',
                'user_id' => 1,
                'delay' => 0,
                'ttr' => 60,
                'errors' => null,
                'data' => null,
                'beanstalk_worker_id' => null,
                'object_model' => 'BatchTreatments',
                'object_foreign_key' => 1,
                'job_state' => 'failed',
                'last_state_update' => null,
                'states_history' => null,
            ],
            [// id=3
                'tube' => 'conversion',
                'priority' => 1024,
                'created' => '2022-09-16T14:01:27',
                'user_id' => 1,
                'delay' => 0,
                'ttr' => 60,
                'errors' => null,
                'data' => null,
                'beanstalk_worker_id' => null,
                'object_model' => 'Archives',
                'object_foreign_key' => 1,
                'job_state' => 'working',
                'last_state_update' => null,
                'states_history' => null,
            ],
            [// id=4
                'tube' => 'batch-treatment',
                'priority' => 1024,
                'created' => '2022-09-16T14:01:27',
                'user_id' => 1,
                'delay' => 0,
                'ttr' => 60,
                'errors' => null,
                'data' => null,
                'beanstalk_worker_id' => null,
                'object_model' => null,
                'object_foreign_key' => null,
                'job_state' => 'pending',
                'last_state_update' => null,
                'states_history' => null,
            ],
        ];
        parent::init();
    }
}
