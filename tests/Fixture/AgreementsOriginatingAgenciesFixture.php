<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AgreementsOriginatingAgenciesFixture
 */
class AgreementsOriginatingAgenciesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'agreement_id' => 2,
                'org_entity_id' => 5,
            ],
        ];
        parent::init();
    }
}
