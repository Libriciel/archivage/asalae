<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OutgoingTransferRequestsFixture
 */
class OutgoingTransferRequestsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'identifier' => 'sa_OATR_1',
                'comment' => 'transfert sur un autre SAE (même instance, autre service d\'archives)',
                'archival_agency_id' => 2,
                'archiving_system_id' => 1,
                'archival_agency_identifier' => 'sa2',
                'archival_agency_name' => 'Mon autre service d\'archive',
                'transferring_agency_identifier' => 'sa2',
                'transferring_agency_name' => 'Mon autre service d\'archive',
                'agreement_identifier' => 'auto',
                'profile_identifier' => 'profil',
                'state' => 'transfers_sent',
                'states_history' => '[{"date":"2021-12-09T17:30:42.869669+0100","state":"validating"},{"date":"2021-12-09T17:30:52.133756+0100","state":"accepted"},{"date":"2021-12-09T17:30:55.459314+0100","state":"transfers_sent"}]',
                'archives_count' => 1,
                'archive_units_count' => 1,
                'original_count' => 1,
                'original_size' => 5,
                'created_user_id' => 1,
                'created' => '2021-12-09T17:30:34',
                'modified' => '2021-12-09T17:30:55',
                'last_state_update' => '2021-12-09T17:30:55',
                'originating_agency_identifier' => 'sa2',
                'originating_agency_name' => 'Mon autre service d\'archive',
                'service_level_identifier' => 'service_level',
            ],
        ];
        parent::init();
    }
}
