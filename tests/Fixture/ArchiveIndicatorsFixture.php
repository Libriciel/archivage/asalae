<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchiveIndicatorsFixture
 */
class ArchiveIndicatorsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archive_date' => '2021-12-09',
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'originating_agency_id' => 2,
                'archive_count' => 1,
                'units_count' => 2,
                'original_count' => 1,
                'original_size' => 554297,
                'timestamp_count' => 0,
                'timestamp_size' => 0,
                'preservation_count' => 0,
                'preservation_size' => 0,
                'dissemination_count' => 0,
                'dissemination_size' => 0,
                'agreement_id' => null,
                'profile_id' => null,
            ],
            [// id=2
                'archive_date' => '2021-12-09',
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'originating_agency_id' => 5,
                'archive_count' => 1,
                'units_count' => 1,
                'original_count' => 1,
                'original_size' => 5,
                'timestamp_count' => 0,
                'timestamp_size' => 0,
                'preservation_count' => 0,
                'preservation_size' => 0,
                'dissemination_count' => 0,
                'dissemination_size' => 0,
                'agreement_id' => 2,
                'profile_id' => 1,
            ],
            [// id=3
                'archive_date' => '2021-12-09',
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'originating_agency_id' => 5,
                'archive_count' => 1,
                'units_count' => 1,
                'original_count' => 1,
                'original_size' => 5,
                'timestamp_count' => 0,
                'timestamp_size' => 0,
                'preservation_count' => 0,
                'preservation_size' => 0,
                'dissemination_count' => 0,
                'dissemination_size' => 0,
                'agreement_id' => 2,
                'profile_id' => 1,
            ],
            [// id=4
                'archive_date' => '2021-12-09',
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'originating_agency_id' => 5,
                'archive_count' => -1,
                'units_count' => 0,
                'original_count' => 0,
                'original_size' => 0,
                'timestamp_count' => 0,
                'timestamp_size' => 0,
                'preservation_count' => 0,
                'preservation_size' => 0,
                'dissemination_count' => 0,
                'dissemination_size' => 0,
                'agreement_id' => 2,
                'profile_id' => 1,
            ],
            [// id=5
                'archive_date' => '2021-12-09',
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'originating_agency_id' => 5,
                'archive_count' => 1,
                'units_count' => 1,
                'original_count' => 1,
                'original_size' => 5,
                'timestamp_count' => 0,
                'timestamp_size' => 0,
                'preservation_count' => 0,
                'preservation_size' => 0,
                'dissemination_count' => 0,
                'dissemination_size' => 0,
                'agreement_id' => 2,
                'profile_id' => 1,
            ],
            [// id=6
                'archive_date' => '2021-12-09',
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'originating_agency_id' => 5,
                'archive_count' => 1,
                'units_count' => 1,
                'original_count' => 1,
                'original_size' => 5,
                'timestamp_count' => 0,
                'timestamp_size' => 0,
                'preservation_count' => 0,
                'preservation_size' => 0,
                'dissemination_count' => 0,
                'dissemination_size' => 0,
                'agreement_id' => 2,
                'profile_id' => 1,
            ],
        ];
        parent::init();
    }
}
