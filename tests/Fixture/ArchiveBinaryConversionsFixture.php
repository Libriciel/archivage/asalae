<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchiveBinaryConversionsFixture
 */
class ArchiveBinaryConversionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archive_id' => 1,
                'archive_binary_id' => 1,
                'type' => 'preservation',
                'format' => 'mp4',
                'audio_codec' => 'aac',
                'video_codec' => 'h264',
                'created' => '2021-12-09T15:19:13',
            ],
        ];
        parent::init();
    }
}
