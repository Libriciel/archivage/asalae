<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RolesTypeEntitiesFixture
 */
class RolesTypeEntitiesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'role_id' => 1,
                'type_entity_id' => 2,
            ],
            [// id=2
                'role_id' => 2,
                'type_entity_id' => 2,
            ],
            [// id=3
                'role_id' => 4,
                'type_entity_id' => 2,
            ],
            [// id=4
                'role_id' => 4,
                'type_entity_id' => 3,
            ],
            [// id=5
                'role_id' => 5,
                'type_entity_id' => 4,
            ],
            [// id=6
                'role_id' => 6,
                'type_entity_id' => 4,
            ],
            [// id=7
                'role_id' => 7,
                'type_entity_id' => 4,
            ],
            [// id=8
                'role_id' => 10,
                'type_entity_id' => 4,
            ],
            [// id=9
                'role_id' => 8,
                'type_entity_id' => 4,
            ],
            [// id=10
                'role_id' => 7,
                'type_entity_id' => 5,
            ],
            [// id=11
                'role_id' => 6,
                'type_entity_id' => 5,
            ],
            [// id=12
                'role_id' => 7,
                'type_entity_id' => 6,
            ],
            [// id=13
                'role_id' => 8,
                'type_entity_id' => 7,
            ],
            [// id=14
                'role_id' => 3,
                'type_entity_id' => 8,
            ],
            [// id=15
                'role_id' => 8,
                'type_entity_id' => 2,
            ],
            [// id=16
                'role_id' => 9,
                'type_entity_id' => 2,
            ],
            [// id=17
                'role_id' => 10,
                'type_entity_id' => 2,
            ],
            [// id=18
                'role_id' => 11,
                'type_entity_id' => 1,
            ],
            [// id=19
                'role_id' => 12,
                'type_entity_id' => 2,
            ],
        ];
        parent::init();
    }
}
