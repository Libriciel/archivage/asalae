<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SecureDataSpacesFixture
 */
class SecureDataSpacesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'name' => 'ECS-001',
                'description' => '',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'org_entity_id' => 2,
                'is_default' => true,
            ],
            [// id=2
                'name' => 'ECS-002',
                'description' => '',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'org_entity_id' => 3,
                'is_default' => true,
            ],
        ];
        parent::init();
    }
}
