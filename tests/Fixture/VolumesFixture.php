<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * VolumesFixture
 */
class VolumesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'name' => 'volume01',
                'fields' => json_encode(['path' => TMP_VOL1]),
                'created' => '2021-12-09T13:54:14',
                'modified' => '2021-12-09T17:19:06',
                'driver' => 'FILESYSTEM',
                'description' => '',
                'active' => true,
                'alert_rate' => 95,
                'disk_usage' => 845558,
                'max_disk_usage' => 10000000000,
                'secure_data_space_id' => 1,
                'read_duration' => 0.00856709,
            ],
            [// id=2
                'name' => 'volume02',
                'fields' => json_encode(['path' => TMP_VOL2]),
                'created' => '2021-12-09T13:54:14',
                'modified' => '2021-12-09T17:19:06',
                'driver' => 'FILESYSTEM',
                'description' => '',
                'active' => true,
                'alert_rate' => 95,
                'disk_usage' => 845558,
                'max_disk_usage' => 10000000000,
                'secure_data_space_id' => 1,
                'read_duration' => 0.00911498,
            ],
            [// id=3
                'name' => 'volume03',
                'fields' => json_encode(['path' => TMP_VOL3]),
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'driver' => 'FILESYSTEM',
                'description' => '',
                'active' => true,
                'alert_rate' => 95,
                'disk_usage' => null,
                'max_disk_usage' => 10000000000,
                'secure_data_space_id' => 2,
                'read_duration' => 0.00856686,
            ],
        ];
        parent::init();
    }
}
