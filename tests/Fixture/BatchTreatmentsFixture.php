<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BatchTreatmentsFixture
 */
class BatchTreatmentsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archival_agency_id' => 2,
                'user_id' => 1,
                'state' => 'ready',
                'last_state_update' => '2022-09-16T14:01:27',
                'states_history' => '[{"date":"2022-09-16T14:01:27.696500+0200","state":"ready"}]',
                'data' => '{"type":"name","name__research":"Foo","name__replace":"Bar"}',
                'created' => '2022-09-16T14:00:58',
                'modified' => '2022-09-16T14:01:27',
            ],
        ];
        parent::init();
    }
}
