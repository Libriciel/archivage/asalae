<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AppraisalRulesFixture
 */
class AppraisalRulesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'appraisal_rule_code_id' => 1,
                'final_action_code' => 'destroy',
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=2
                'appraisal_rule_code_id' => 1,
                'final_action_code' => 'destroy',
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=3
                'appraisal_rule_code_id' => 1,
                'final_action_code' => 'destroy',
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=4
                'appraisal_rule_code_id' => 1,
                'final_action_code' => 'destroy',
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=5
                'appraisal_rule_code_id' => 1,
                'final_action_code' => 'destroy',
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=6
                'appraisal_rule_code_id' => 153,
                'final_action_code' => 'destroy',
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
        ];
        parent::init();
    }
}
