<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AgreementsTransferringAgenciesFixture
 */
class AgreementsTransferringAgenciesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'agreement_id' => 2,
                'org_entity_id' => 2,
            ],
            [// id=2
                'agreement_id' => 2,
                'org_entity_id' => 4,
            ],
        ];
        parent::init();
    }
}
