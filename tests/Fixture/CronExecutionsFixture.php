<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CronExecutionsFixture
 */
class CronExecutionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'cron_id' => 4,
                'date_begin' => '2021-12-09T13:54:15',
                'date_end' => '2021-12-09T13:54:24',
                'state' => 'success',
                'report' => 'Mise à jour de la table pronoms...
Une mise à jour a été trouvée vers la version 99. Lancement du téléchargement...
Enregistrement en base de données...
Le répertoire de pronoms a été mis à jour avec succès.
',
                'last_update' => null,
                'pid' => 21161,
            ],
            [// id=2
                'cron_id' => 4,
                'date_begin' => '2021-12-09T14:03:01',
                'date_end' => '2021-12-09T14:03:02',
                'state' => 'success',
                'report' => 'Mise à jour de la table pronoms...
La table de pronom est déjà à jour (version 99)
',
                'last_update' => null,
                'pid' => 21746,
            ],
            [// id=3
                'cron_id' => 4,
                'date_begin' => '2021-12-09T14:18:40',
                'date_end' => '2021-12-09T14:18:40',
                'state' => 'success',
                'report' => 'Mise à jour de la table pronoms...
La table de pronom est déjà à jour (version 99)
',
                'last_update' => null,
                'pid' => 22409,
            ],
            [// id=4
                'cron_id' => 4,
                'date_begin' => '2021-12-09T14:20:47',
                'date_end' => '2021-12-09T14:20:47',
                'state' => 'success',
                'report' => 'Mise à jour de la table pronoms...
La table de pronom est déjà à jour (version 99)
',
                'last_update' => null,
                'pid' => 22538,
            ],
            [// id=5
                'cron_id' => 4,
                'date_begin' => '2021-12-09T14:40:41',
                'date_end' => '2021-12-09T14:40:41',
                'state' => 'success',
                'report' => 'Mise à jour de la table pronoms...
La table de pronom est déjà à jour (version 99)
',
                'last_update' => null,
                'pid' => 23450,
            ],
        ];
        parent::init();
    }
}
