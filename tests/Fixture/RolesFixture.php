<?php
namespace Asalae\Test\Fixture;

use Asalae\Model\Table\RolesTable;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * RolesFixture
 */
class RolesFixture extends TestFixture
{
    /**
     * Valeurs par défaut
     * @return array[]
     */
    public static function defaultValues()
    {
        return [
            [// id=1
                'name' => 'Archiviste',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'org_entity_id' => null,
                'code' => RolesTable::CODE_ARCHIVIST,
                'parent_id' => null,
                'lft' => 1,
                'rght' => 2,
                'hierarchical_view' => true,
                'description' => null,
                'agent_type' => 'person',
            ],
            [// id=2
                'name' => 'Opérateur d\'archivage',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'org_entity_id' => null,
                'code' => RolesTable::CODE_ARCHIVING_OPERATOR,
                'parent_id' => null,
                'lft' => 3,
                'rght' => 4,
                'hierarchical_view' => false,
                'description' => null,
                'agent_type' => 'person',
            ],
            [// id=3
                'name' => 'Référent Archives',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'org_entity_id' => null,
                'code' => RolesTable::CODE_ARCHIVES_REFERENT,
                'parent_id' => null,
                'lft' => 5,
                'rght' => 6,
                'hierarchical_view' => true,
                'description' => null,
                'agent_type' => 'person',
            ],
            [// id=4
                'name' => 'Contrôleur',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'org_entity_id' => null,
                'code' => RolesTable::CODE_CONTROLLER,
                'parent_id' => null,
                'lft' => 7,
                'rght' => 8,
                'hierarchical_view' => true,
                'description' => null,
                'agent_type' => 'person',
            ],
            [// id=5
                'name' => 'Versant',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'org_entity_id' => null,
                'code' => RolesTable::CODE_SUBMITTER,
                'parent_id' => null,
                'lft' => 9,
                'rght' => 10,
                'hierarchical_view' => false,
                'description' => null,
                'agent_type' => 'person',
            ],
            [// id=6
                'name' => 'Producteur',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'org_entity_id' => null,
                'code' => RolesTable::CODE_PRODUCER,
                'parent_id' => null,
                'lft' => 11,
                'rght' => 12,
                'hierarchical_view' => false,
                'description' => null,
                'agent_type' => 'person',
            ],
            [// id=7
                'name' => 'Demandeur',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'org_entity_id' => null,
                'code' => RolesTable::CODE_APPLICANT,
                'parent_id' => null,
                'lft' => 13,
                'rght' => 14,
                'hierarchical_view' => false,
                'description' => null,
                'agent_type' => 'person',
            ],
            [// id=8
                'name' => 'ws versant',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'org_entity_id' => null,
                'code' => RolesTable::CODE_WS_SUBMITTER,
                'parent_id' => null,
                'lft' => 15,
                'rght' => 16,
                'hierarchical_view' => false,
                'description' => null,
                'agent_type' => 'software',
            ],
            [// id=9
                'name' => 'ws administrateur',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'org_entity_id' => null,
                'code' => RolesTable::CODE_WS_ADMINISTRATOR,
                'parent_id' => null,
                'lft' => 17,
                'rght' => 18,
                'hierarchical_view' => false,
                'description' => null,
                'agent_type' => 'software',
            ],
            [// id=10
                'name' => 'ws requêteur',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'org_entity_id' => null,
                'code' => RolesTable::CODE_WS_REQUESTER,
                'parent_id' => null,
                'lft' => 19,
                'rght' => 20,
                'hierarchical_view' => false,
                'description' => null,
                'agent_type' => 'software',
            ],
            [// id=11
                'name' => 'Super archiviste',
                'active' => true,
                'created' => '2022-06-13T00:00:00',
                'modified' => '2022-06-13T00:00:00',
                'org_entity_id' => null,
                'code' => RolesTable::CODE_SUPER_ARCHIVIST,
                'parent_id' => null,
                'lft' => 21,
                'rght' => 22,
                'hierarchical_view' => true,
                'description' => null,
                'agent_type' => 'person',
            ],
            [// id=12
                'name' => 'Référent technique',
                'active' => true,
                'created' => '2022-09-07T10:44:43',
                'modified' => '2022-09-07T10:44:43',
                'org_entity_id' => null,
                'code' => RolesTable::CODE_ADMIN,
                'parent_id' => null,
                'lft' => 23,
                'rght' => 24,
                'hierarchical_view' => false,
                'description' => null,
                'agent_type' => 'person',
            ],
        ];
    }

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = self::defaultValues();
        parent::init();
    }
}
