<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ValidationChainsFixture
 */
class ValidationChainsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'name' => 'Validation des transferts conformes',
                'description' => '',
                'app_type' => 'ARCHIVE_TRANSFER_CONFORM',
                'app_meta' => '{"default":true,"active":true}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'org_entity_id' => 2,
            ],
            [// id=2
                'name' => 'Validation des transferts non conformes',
                'description' => '',
                'app_type' => 'ARCHIVE_TRANSFER_NOT_CONFORM',
                'app_meta' => '{"default":true,"active":true}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'org_entity_id' => 2,
            ],
            [// id=3
                'name' => 'Validation des comms',
                'description' => '',
                'app_type' => 'ARCHIVE_DELIVERY_REQUEST',
                'app_meta' => '{"default":true,"active":true}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'org_entity_id' => 2,
            ],
            [// id=4
                'name' => 'Validation des elims',
                'description' => '',
                'app_type' => 'ARCHIVE_DESTRUCTION_REQUEST',
                'app_meta' => '{"default":true,"active":true}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'org_entity_id' => 2,
            ],
            [// id=5
                'name' => 'Validation des restitutions',
                'description' => '',
                'app_type' => 'ARCHIVE_RESTITUTION_REQUEST',
                'app_meta' => '{"default":true,"active":true}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'org_entity_id' => 2,
            ],
            [// id=6
                'name' => 'Validation des transferts sortants',
                'description' => '',
                'app_type' => 'ARCHIVE_OUTGOING_TRANSFER_REQUEST',
                'app_meta' => '{"default":true,"active":true}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'org_entity_id' => 2,
            ],
            [// id=7
                'name' => 'Non conformes sa2',
                'description' => '',
                'app_type' => 'ARCHIVE_TRANSFER_NOT_CONFORM',
                'app_meta' => '{"active":true,"default":true,"created_user_id":5}',
                'created' => '2021-12-09T17:27:48',
                'modified' => '2021-12-09T17:27:48',
                'org_entity_id' => 3,
            ],
            [// id=8
                'name' => 'Unused',
                'description' => '',
                'app_type' => 'ARCHIVE_TRANSFER_CONFORM',
                'app_meta' => '{"active":false,"default":false}',
                'created' => '2021-12-09T17:27:48',
                'modified' => '2021-12-09T17:27:48',
                'org_entity_id' => 2,
            ],
        ];
        parent::init();
    }
}
