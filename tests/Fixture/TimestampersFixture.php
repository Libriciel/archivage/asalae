<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TimestampersFixture
 */
class TimestampersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $timestampingDir = TMP_TESTDIR . DS . 'timestamping' . DS;

        $this->records = [
            [// id=1
                'name' => 'Openssl local',
                'fields' => json_encode(
                    [
                        'name' => 'Openssl local',
                        'driver' => 'OPENSSL',
                        'ca' => $timestampingDir . 'tsacert.crt',
                        'crt' => $timestampingDir . 'tsacert.crt',
                        'pem' => $timestampingDir . 'tsacert.pem',
                        'cnf' => $timestampingDir . 'asalae-tsa.cnf',
                    ]
                ),
                'created' => '2021-12-09T13:54:14',
                'modified' => '2021-12-09T13:54:14',
            ],
        ];
        parent::init();
    }
}
