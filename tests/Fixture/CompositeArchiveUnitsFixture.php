<?php
declare(strict_types=1);

namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CompositeArchiveUnitsFixture
 */
class CompositeArchiveUnitsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [];
        parent::init();
    }
}
