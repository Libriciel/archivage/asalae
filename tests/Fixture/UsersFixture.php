<?php
namespace Asalae\Test\Fixture;

use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 */
class UsersFixture extends TestFixture
{
    /**
     * Valeurs par défaut
     * @return array[]
     */
    public static function defaultValues()
    {
        $hasher = new DefaultPasswordHasher;
        return [
            [// id=1
                'username' => 'admin',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-10T09:26:40',
                'password' => $hasher->hash('admin'),
                'cookies' => '{"table-generator-config-table-validation-processes-my-transfers-table-tr-active":"table#validation-processes-my-transfers-table tr[data-id=\"2\"]","table-generator-config-table-transfers-index-preparating-table-tr-active":"table#transfers-index-preparating-table tr[data-id=\"8\"]","table-generator-config-table-agreements-index-table-tr-active":"table#agreements-index-table tr[data-id=\"2\"]","table-generator-config-table-delivery-requests-index-preparating-table-tr-active":"table#delivery-requests-index-preparating-table tr[data-id=\"1\"]","table-generator-config-table-delivery-requests-index-all-table-tr-active":"table#delivery-requests-index-all-table tr[data-id=\"1\"]","table-generator-config-table-deliveries-retrieval-table-tr-active":"table#deliveries-retrieval-table tr[data-id=\"1\"]","table-generator-config-table-transfers-index-my-entity-table-tr-active":"table#transfers-index-my-entity-table tr[data-id=\"6\"]","table-generator-config-table-deliveries-acquittal-table-tr-active":"table#deliveries-acquittal-table tr[data-id=\"1\"]","table-generator-config-table-destruction-requests-index-my-table-tr-active":"table#destruction-requests-index-my-table tr[data-id=\"1\"]","table-generator-config-table-destruction-requests-index-all-table-tr-active":"table#destruction-requests-index-all-table tr[data-id=\"1\"]","table-generator-config-table-destruction-notifications-index-table-tr-active":"table#destruction-notifications-index-table tr[data-id=\"1\"]","table-generator-config-table-restitution-requests-index-preparating-table-tr-active":"table#restitution-requests-index-preparating-table tr[data-id=\"1\"]","table-generator-config-table-validation-chains-index-table-tr-active":"table#validation-chains-index-table tr[data-id=\"5\"]","table-generator-config-table-validation-stages-index-table-tr-active":"table#validation-stages-index-table tr[data-id=\"6\"]","table-generator-config-table-validation-actors-index-table-tr-active":"table#validation-actors-index-table tr[data-id=\"8\"]","table-generator-config-table-validation-processes-restitution-requests-table-tr-active":"table#validation-processes-restitution-requests-table tr[data-id=\"5\"]","table-generator-config-table-outgoing-transfer-requests-index-preparating-table-tr-active":"table#outgoing-transfer-requests-index-preparating-table tr[data-id=\"1\"]","table-generator-config-table-validation-processes-outgoing-transfer-requests-table-tr-active":"table#validation-processes-outgoing-transfer-requests-table tr[data-id=\"6\"]"}',
                'menu' => null,
                'high_contrast' => false,
                'active' => true,
                'role_id' => 1,
                'org_entity_id' => 2,
                'email' => 'admin@libriciel.fr',
                'name' => 'M. Martin Dupont',
                'ldap_id' => null,
                'agent_type' => 'person',
                'is_validator' => true,
                'use_cert' => false,
                'ldap_login' => null,
                'notify_download_pdf_frequency' => 1,
                'notify_download_zip_frequency' => 1,
                'notify_validation_frequency' => null,
                'notify_job_frequency' => null,
                'notify_agreement_frequency' => null,
                'notify_transfer_report_frequency' => null,
            ],
            [// id=2
                'username' => 'pastell',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T16:38:35',
                'password' => $hasher->hash('pastell'),
                'cookies' => '[]',
                'menu' => null,
                'high_contrast' => false,
                'active' => true,
                'role_id' => 8,
                'org_entity_id' => 2,
                'email' => 'pastell@libriciel.fr',
                'name' => 'pastell',
                'ldap_id' => null,
                'agent_type' => 'software',
                'is_validator' => false,
                'use_cert' => false,
                'ldap_login' => null,
                'notify_download_pdf_frequency' => 1,
                'notify_download_zip_frequency' => 1,
                'notify_validation_frequency' => null,
                'notify_job_frequency' => null,
                'notify_agreement_frequency' => null,
                'notify_transfer_report_frequency' => null,
            ],
            [// id=3
                'username' => 'prod',
                'created' => '2021-12-09T15:34:37',
                'modified' => '2021-12-09T16:22:18',
                'password' => $hasher->hash('prod'),
                'cookies' => '{"table-generator-config-table-validation-processes-delivery-requests-table-tr-active":"table#validation-processes-delivery-requests-table tr[data-id=\"3\"]","table-generator-config-table-validation-processes-destruction-requests-table-tr-active":"table#validation-processes-destruction-requests-table tr[data-id=\"4\"]"}',
                'menu' => null,
                'high_contrast' => false,
                'active' => true,
                'role_id' => 6,
                'org_entity_id' => 5,
                'email' => 'prod@test.fr',
                'name' => 'prod',
                'ldap_id' => null,
                'agent_type' => 'person',
                'is_validator' => true,
                'use_cert' => false,
                'ldap_login' => null,
                'notify_download_pdf_frequency' => 1,
                'notify_download_zip_frequency' => 1,
                'notify_validation_frequency' => null,
                'notify_job_frequency' => null,
                'notify_agreement_frequency' => null,
                'notify_transfer_report_frequency' => null,
            ],
            [// id=4
                'username' => 'sa2',
                'created' => '2021-12-09T16:24:16',
                'modified' => '2021-12-09T16:52:03',
                'password' => $hasher->hash('sa2'),
                'cookies' => '[]',
                'menu' => null,
                'high_contrast' => false,
                'active' => true,
                'role_id' => 8,
                'org_entity_id' => 3,
                'email' => 'as2@test.fr',
                'name' => 'sa2',
                'ldap_id' => null,
                'agent_type' => 'software',
                'is_validator' => false,
                'use_cert' => false,
                'ldap_login' => null,
                'notify_download_pdf_frequency' => 1,
                'notify_download_zip_frequency' => 1,
                'notify_validation_frequency' => null,
                'notify_job_frequency' => null,
                'notify_agreement_frequency' => null,
                'notify_transfer_report_frequency' => null,
            ],
            [// id=5
                'username' => 'admin2',
                'created' => '2021-12-09T17:21:26',
                'modified' => '2021-12-09T17:45:29',
                'password' => $hasher->hash('admin2'),
                'cookies' => '{"table-generator-config-table-validation-chains-index-table-tr-active":"table#validation-chains-index-table tr[data-id=\"7\"]","table-generator-config-table-validation-stages-index-table-tr-active":"table#validation-stages-index-table tr[data-id=\"8\"]","table-generator-config-table-validation-processes-my-transfers-table-tr-active":"table#validation-processes-my-transfers-table tr[data-id=\"7\"]","table-generator-config-table-view-transfer-errors-tr-active":"table#view-transfer-errors tr[data-id=\"3\"]"}',
                'menu' => null,
                'high_contrast' => false,
                'active' => true,
                'role_id' => 1,
                'org_entity_id' => 3,
                'email' => 'admin2@test.fr',
                'name' => 'admin2',
                'ldap_id' => null,
                'agent_type' => 'person',
                'is_validator' => true,
                'use_cert' => false,
                'ldap_login' => null,
                'notify_download_pdf_frequency' => 1,
                'notify_download_zip_frequency' => 1,
                'notify_validation_frequency' => null,
                'notify_job_frequency' => null,
                'notify_agreement_frequency' => null,
                'notify_transfer_report_frequency' => null,
            ],
            [// id=6
                'username' => 'tech',
                'created' => '2021-12-09T17:21:26',
                'modified' => '2021-12-09T17:45:29',
                'password' => $hasher->hash('tech'),
                'cookies' => '[]',
                'menu' => null,
                'high_contrast' => false,
                'active' => true,
                'role_id' => 12,
                'org_entity_id' => 2,
                'email' => 'tech@test.fr',
                'name' => 'tech',
                'ldap_id' => null,
                'agent_type' => 'person',
                'is_validator' => false,
                'use_cert' => false,
                'ldap_login' => null,
                'notify_download_pdf_frequency' => 1,
                'notify_download_zip_frequency' => 1,
                'notify_validation_frequency' => null,
                'notify_job_frequency' => null,
                'notify_agreement_frequency' => null,
                'notify_transfer_report_frequency' => null,
            ],
        ];
    }

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = self::defaultValues();
        parent::init();
    }
}
