<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EaccpfsFixture
 */
class EaccpfsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'record_id' => 'se',
                'entity_id' => null,
                'entity_type' => 'corporateBody',
                'name' => 'Service d\'exploitation',
                'data' => '{"eac-cpf":{"control":{"recordId":"se","maintenanceStatus":"new","maintenanceAgency":{"agencyName":"Service d\'exploitation"},"maintenanceHistory":{"maintenanceEvent":[{"eventType":"created","eventDateTime":{"@standardDateTime":"2021-12-09T13:54:14","@":"2021-12-09T13:54:14"},"agentType":"human","agent":"Libriciel SCOP","eventDescription":"Created in asalae"}]}},"cpfDescription":{"identity":{"entityId":null,"entityType":"corporateBody","nameEntry":{"part":"Service d\'exploitation"}},"description":{"existDates":{"date":"2021-12-09T13:54:14+01:00"}}},"@xmlns":"urn:isbn:1-931666-33-4","@xmlns:xlink":"http:\/\/www.w3.org\/1999\/xlink"}}',
                'created' => '2021-12-09T13:54:14',
                'modified' => '2021-12-09T13:54:14',
                'agency_name' => 'Service d\'exploitation',
                'org_entity_id' => 1,
                'from_date' => '2021-12-09',
                'to_date' => null,
            ],
            [// id=2
                'record_id' => 'sa',
                'entity_id' => null,
                'entity_type' => 'corporateBody',
                'name' => 'Mon Service d\'archive',
                'data' => '{"eac-cpf":{"control":{"recordId":"sa","maintenanceStatus":"new","maintenanceAgency":{"agencyName":"Mon Service d\'archive"},"maintenanceHistory":{"maintenanceEvent":[{"eventType":"created","eventDateTime":{"@standardDateTime":"2021-12-09T13:54:15","@":"2021-12-09T13:54:15"},"agentType":"human","agent":"Libriciel SCOP","eventDescription":"Created in asalae"}]}},"cpfDescription":{"identity":{"entityId":null,"entityType":"corporateBody","nameEntry":{"part":"Mon Service d\'archive"}},"description":{"existDates":{"date":"2021-12-09T13:54:15+01:00"}}},"@xmlns":"urn:isbn:1-931666-33-4","@xmlns:xlink":"http:\/\/www.w3.org\/1999\/xlink"}}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'agency_name' => 'Mon Service d\'archive',
                'org_entity_id' => 2,
                'from_date' => '2021-12-09',
                'to_date' => null,
            ],
            [// id=3
                'record_id' => 'sa2',
                'entity_id' => null,
                'entity_type' => 'corporateBody',
                'name' => 'Mon autre service d\'archive',
                'data' => '{"eac-cpf":{"control":{"recordId":"sa2","maintenanceStatus":"new","maintenanceAgency":{"agencyName":"Mon autre service d\'archive"},"maintenanceHistory":{"maintenanceEvent":[{"eventType":"created","eventDateTime":{"@standardDateTime":"2021-12-09T13:54:15","@":"2021-12-09T13:54:15"},"agentType":"human","agent":"Libriciel SCOP","eventDescription":"Created in asalae"}]}},"cpfDescription":{"identity":{"entityId":null,"entityType":"corporateBody","nameEntry":{"part":"Mon autre service d\'archive"}},"description":{"existDates":{"date":"2021-12-09T13:54:15+01:00"}}},"@xmlns":"urn:isbn:1-931666-33-4","@xmlns:xlink":"http:\/\/www.w3.org\/1999\/xlink"}}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'agency_name' => 'Mon autre service d\'archive',
                'org_entity_id' => 3,
                'from_date' => '2021-12-09',
                'to_date' => null,
            ],
            [// id=4
                'record_id' => 'sv',
                'entity_id' => null,
                'entity_type' => 'corporateBody',
                'name' => 'Mon service versant',
                'data' => '{"eac-cpf":{"control":{"recordId":"sv","maintenanceStatus":"new","maintenanceAgency":{"agencyName":"Mon service versant"},"maintenanceHistory":{"maintenanceEvent":[{"eventType":"created","eventDateTime":{"@standardDateTime":"2021-12-09T13:54:15","@":"2021-12-09T13:54:15"},"agentType":"human","agent":"Libriciel SCOP","eventDescription":"Created in as@lae"}]}},"cpfDescription":{"identity":{"entityId":null,"entityType":"corporateBody","nameEntry":{"part":"Mon service versant"}},"description":{"existDates":{"date":"2021-12-09T13:54:15+01:00"}}},"@xmlns":"urn:isbn:1-931666-33-4","@xmlns:xlink":"http:\/\/www.w3.org\/1999\/xlink"}}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'agency_name' => 'Mon service versant',
                'org_entity_id' => 4,
                'from_date' => '2021-12-09',
                'to_date' => null,
            ],
            [// id=5
                'record_id' => 'sp',
                'entity_id' => null,
                'entity_type' => 'corporateBody',
                'name' => 'Mon service producteur',
                'data' => '{"eac-cpf":{"control":{"recordId":"sp","maintenanceStatus":"new","maintenanceAgency":{"agencyName":"Mon service producteur"},"maintenanceHistory":{"maintenanceEvent":[{"eventType":"created","eventDateTime":{"@standardDateTime":"2021-12-09T13:54:15","@":"2021-12-09T13:54:15"},"agentType":"human","agent":"Libriciel SCOP","eventDescription":"Created in as@lae"}]}},"cpfDescription":{"identity":{"entityId":null,"entityType":"corporateBody","nameEntry":{"part":"Mon service producteur"}},"description":{"existDates":{"date":"2021-12-09T13:54:15+01:00"}}},"@xmlns":"urn:isbn:1-931666-33-4","@xmlns:xlink":"http:\/\/www.w3.org\/1999\/xlink"}}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'agency_name' => 'Mon service producteur',
                'org_entity_id' => 5,
                'from_date' => '2021-12-09',
                'to_date' => null,
            ],
        ];
        parent::init();
    }
}
