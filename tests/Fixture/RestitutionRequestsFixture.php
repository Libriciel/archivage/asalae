<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RestitutionRequestsFixture
 */
class RestitutionRequestsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'identifier' => 'sa_ARR_1',
                'comment' => 'Demande de restitution de l\'unité d\'archive par le Mon Service d\'archive :
- sa_4 - seda2.1',
                'archival_agency_id' => 2,
                'originating_agency_id' => 5,
                'state' => 'restitution_available',
                'last_state_update' => '2021-12-09T17:14:44',
                'states_history' => '[{"date":"2021-12-09T16:56:54.315871+0100","state":"validating"},{"date":"2021-12-09T16:58:38.413665+0100","state":"accepted"},{"date":"2021-12-09T17:14:44.764205+0100","state":"restitution_available"}]',
                'archive_units_count' => 1,
                'original_count' => 1,
                'original_size' => 5,
                'created_user_id' => 1,
                'created' => '2021-12-09T16:56:42',
                'modified' => '2021-12-09T17:14:44',
                'restitution_certified' => false,
                'destruction_certified' => null,
            ],
        ];
        parent::init();
    }
}
