<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchivesTransfersFixture
 */
class ArchivesTransfersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archive_id' => 1,
                'transfer_id' => 1,
            ],
            [// id=2
                'archive_id' => 2,
                'transfer_id' => 3,
            ],
            [// id=3
                'archive_id' => 3,
                'transfer_id' => 4,
            ],
            [// id=4
                'archive_id' => 4,
                'transfer_id' => 5,
            ],
            [// id=5
                'archive_id' => 5,
                'transfer_id' => 6,
            ],
        ];
        parent::init();
    }
}
