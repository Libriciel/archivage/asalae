<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TransferAttachmentsFixture
 */
class TransferAttachmentsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'transfer_id' => 1,
                'filename' => 'sample.wmv',
                'size' => 554297,
                'hash' => '60084bf1d83c48fa33c46a0d51346b11a62a3a8112fa7cf1115479cd96f42353',
                'hash_algo' => 'sha256',
                'virus_name' => '',
                'valid' => true,
                'mime' => 'video/x-ms-wmv',
                'extension' => 'wmv',
                'format' => 'fmt/133',
                'xpath' => '/ArchiveTransfer/Archive/Document/Attachment',
            ],
            [// id=2
                'transfer_id' => 3,
                'filename' => 'file1.txt',
                'size' => 5,
                'hash' => 'f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2',
                'hash_algo' => 'sha256',
                'virus_name' => '',
                'valid' => true,
                'mime' => 'text/plain',
                'extension' => 'txt',
                'format' => 'x-fmt/111',
                'xpath' => '/ArchiveTransfer/DataObjectPackage/BinaryDataObject/Attachment',
            ],
            [// id=3
                'transfer_id' => 4,
                'filename' => 'file1.txt',
                'size' => 5,
                'hash' => 'f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2',
                'hash_algo' => 'sha256',
                'virus_name' => '',
                'valid' => true,
                'mime' => 'text/plain',
                'extension' => 'txt',
                'format' => 'x-fmt/111',
                'xpath' => '/ArchiveTransfer/DataObjectPackage/BinaryDataObject/Attachment',
            ],
            [// id=4
                'transfer_id' => 5,
                'filename' => 'file1.txt',
                'size' => 5,
                'hash' => 'f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2',
                'hash_algo' => 'sha256',
                'virus_name' => '',
                'valid' => true,
                'mime' => 'text/plain',
                'extension' => 'txt',
                'format' => 'x-fmt/111',
                'xpath' => '/ArchiveTransfer/DataObjectPackage/BinaryDataObject/Attachment',
            ],
            [// id=5
                'transfer_id' => 6,
                'filename' => 'file1.txt',
                'size' => 5,
                'hash' => 'f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2',
                'hash_algo' => 'sha256',
                'virus_name' => '',
                'valid' => true,
                'mime' => 'text/plain',
                'extension' => 'txt',
                'format' => 'x-fmt/111',
                'xpath' => '/ArchiveTransfer/DataObjectPackage/BinaryDataObject/Attachment',
            ],
            [// id=6
                'transfer_id' => 7,
                'filename' => 'file1.txt',
                'size' => 5,
                'hash' => 'f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2',
                'hash_algo' => 'sha256',
                'virus_name' => '',
                'valid' => true,
                'mime' => 'text/plain',
                'extension' => 'txt',
                'format' => 'x-fmt/111',
                'xpath' => '/ArchiveTransfer/DataObjectPackage/BinaryDataObject/Attachment',
            ],
            [// id=7
                'transfer_id' => 8,
                'filename' => 'sample.odt',
                'size' => 8077,
                'hash' => '95072adb607708af039d1e7a4368ad3a9ede8280e367991ec1206405c81eab97',
                'hash_algo' => 'sha256',
                'virus_name' => '',
                'valid' => true,
                'mime' => 'application/vnd.oasis.opendocument.text',
                'extension' => 'odt',
                'format' => 'fmt/291',
                'xpath' => '/ArchiveTransfer/Contains/Document/Attachment',
            ],
            [// id=8
                'transfer_id' => 8,
                'filename' => 'sample.rng',
                'size' => 555,
                'hash' => '97ea8588fc1b99dd777c95008dc489b3058706acfc7c1e75c946f31420882a54',
                'hash_algo' => 'sha256',
                'virus_name' => '',
                'valid' => true,
                'mime' => 'application/xml',
                'extension' => 'rng',
                'format' => 'fmt/101',
                'xpath' => '/ArchiveTransfer/Contains/Contains/Contains/Document/Attachment',
            ],
            [// id=9
                'transfer_id' => 8,
                'filename' => 'sample.pdf',
                'size' => 10018,
                'hash' => '51f2bb998d8ec51880c06edd36982ca4f2dce068d01c868bda1ab8c94e8b8653',
                'hash_algo' => 'sha256',
                'virus_name' => '',
                'valid' => true,
                'mime' => 'application/pdf',
                'extension' => 'pdf',
                'format' => 'fmt/18',
                'xpath' => '/ArchiveTransfer/Contains/Contains/Document/Attachment',
            ],
        ];
        parent::init();
    }
}
