<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RestitutionsFixture
 */
class RestitutionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'identifier' => 'sa_AR_1',
                'restitution_request_id' => 1,
                'state' => 'available',
                'comment' => 'Restitution de l\'unité d\'archives demandée par M. Martin Dupont :
- sa_4 - seda2.1',
                'archive_units_count' => 1,
                'original_count' => 1,
                'original_size' => 5,
                'created' => '2021-12-09T17:14:44',
                'modified' => '2021-12-09T17:14:44',
            ],
        ];
        parent::init();
    }
}
