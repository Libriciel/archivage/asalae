<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchiveUnitsDeliveryRequestsFixture
 */
class ArchiveUnitsDeliveryRequestsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archive_unit_id' => 1,
                'delivery_request_id' => 1,
            ],
        ];
        parent::init();
    }
}
