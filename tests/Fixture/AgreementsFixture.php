<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AgreementsFixture
 */
class AgreementsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'org_entity_id' => 2,
                'identifier' => 'agreement1',
                'name' => 'Mon accord de versement',
                'description' => '',
                'active' => true,
                'created' => '2021-12-09T14:40:40',
                'modified' => '2021-12-09T14:40:40',
                'allow_all_transferring_agencies' => true,
                'allow_all_originating_agencies' => true,
                'allow_all_service_levels' => true,
                'auto_validate' => false,
                'proper_chain_id' => 1,
                'improper_chain_id' => 2,
                'date_begin' => null,
                'date_end' => null,
                'default_agreement' => true,
                'allowed_formats' => '',
                'max_transfers' => null,
                'transfer_period' => '',
                'max_size_per_transfer' => null,
                'allow_all_profiles' => true,
                'notify_delay' => 0,
            ],
            [// id=2
                'org_entity_id' => 2,
                'identifier' => 'agreement2',
                'name' => 'Mon accord de versement 2',
                'description' => '',
                'active' => true,
                'created' => '2021-12-09T14:40:41',
                'modified' => '2021-12-09T14:40:41',
                'allow_all_transferring_agencies' => false,
                'allow_all_originating_agencies' => false,
                'allow_all_service_levels' => false,
                'auto_validate' => true,
                'proper_chain_id' => null,
                'improper_chain_id' => 1,
                'date_begin' => null,
                'date_end' => null,
                'default_agreement' => false,
                'allowed_formats' => 'mime-application/vnd.oasis.opendocument.text,pronom-fmt/18,ext-rng',
                'max_transfers' => null,
                'transfer_period' => '',
                'max_size_per_transfer' => null,
                'allow_all_profiles' => false,
                'notify_delay' => 0,
            ],
            [// id=3
                'org_entity_id' => 3,
                'identifier' => 'auto',
                'name' => 'Auto',
                'description' => '',
                'active' => true,
                'created' => '2021-12-09T17:28:43',
                'modified' => '2021-12-09T17:28:43',
                'allow_all_transferring_agencies' => true,
                'allow_all_originating_agencies' => true,
                'allow_all_service_levels' => true,
                'auto_validate' => true,
                'proper_chain_id' => null,
                'improper_chain_id' => 7,
                'date_begin' => null,
                'date_end' => null,
                'default_agreement' => true,
                'allowed_formats' => '',
                'max_transfers' => null,
                'transfer_period' => '',
                'max_size_per_transfer' => null,
                'allow_all_profiles' => true,
                'notify_delay' => 0,
            ],
        ];
        parent::init();
    }
}
