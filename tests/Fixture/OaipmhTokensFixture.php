<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;
use DateInterval;
use DateTime;

/**
 * OaipmhTokensFixture
 */
class OaipmhTokensFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $twoHours = new DateInterval('PT2H');
        $expired = new DateTime;
        $expireIn2H = new DateTime;
        $expired->sub($twoHours);
        $expireIn2H->add($twoHours);

        $this->records = [
            [// id=1
                'value' => 'expired_listsets_token',
                'verb' => 'listSets',
                'metadata_prefix' => null,
                'expiration_date' => $expired,
                'complete_list_size' => 5,
                'cursor' => 0,
                'prev_id' => null,
                'version' => 2,
            ],
            [// id=2
                'value' => 'valid_listsets_token',
                'verb' => 'listSets',
                'metadata_prefix' => null,
                'expiration_date' => $expireIn2H,
                'complete_list_size' => 5,
                'cursor' => 0,
                'prev_id' => null,
                'version' => 2,
            ],
            [// id=3
                'value' => 'valid_listidentifiers_token',
                'verb' => 'listIdentifiers',
                'metadata_prefix' => null,
                'expiration_date' => $expireIn2H,
                'complete_list_size' => 5,
                'cursor' => 0,
                'prev_id' => null,
                'version' => 2,
            ],
            [// id=4
                'value' => 'expired_listrecords_token',
                'verb' => 'listRecords',
                'metadata_prefix' => null,
                'expiration_date' => $expired,
                'complete_list_size' => 5,
                'cursor' => 0,
                'prev_id' => null,
                'version' => 2,
            ],
        ];
        parent::init();
    }
}
