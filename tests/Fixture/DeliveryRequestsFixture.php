<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DeliveryRequestsFixture
 */
class DeliveryRequestsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'identifier' => 'sa_ADLR_1',
                'archival_agency_id' => 2,
                'requester_id' => 5,
                'state' => 'delivery_acquitted',
                'comment' => 'Demande de communication de l\'unité d\'archive par le Mon Service d\'archive :
- sa_1 - seda1.0',
                'derogation' => false,
                'created_user_id' => 1,
                'created' => '2021-12-09T15:33:38',
                'modified' => '2021-12-09T16:20:46',
                'last_state_update' => '2021-12-09T16:20:46',
                'states_history' => '[{"date":"2021-12-09T15:33:47.997837+0100","state":"validating"},{"date":"2021-12-09T15:36:14.318925+0100","state":"accepted"},{"date":"2021-12-09T15:36:15.076976+0100","state":"delivery_available"},{"date":"2021-12-09T15:37:57.084848+0100","state":"delivery_downloaded"},{"date":"2021-12-09T16:20:46.544182+0100","state":"delivery_acquitted"}]',
                'filename' => 'sa_ADLR_1_delivery_request.xml',
                'validation_chain_id' => null,
                'archive_units_count' => 1,
                'original_count' => 1,
                'original_size' => 554297,
            ],
        ];
        parent::init();
    }
}
