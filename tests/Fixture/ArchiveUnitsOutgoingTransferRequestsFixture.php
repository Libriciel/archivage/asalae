<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchiveUnitsOutgoingTransferRequestsFixture
 */
class ArchiveUnitsOutgoingTransferRequestsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archive_unit_id' => 5,
                'otr_id' => 1,
            ],
        ];
        parent::init();
    }
}
