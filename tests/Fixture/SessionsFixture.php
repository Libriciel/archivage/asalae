<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SessionsFixture
 */
class SessionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => '77ovlobsthtb5ngll7lts2fpcl',
                'created' => '2021-12-10T09:28:07',
                'modified' => '2021-12-10T11:25:48',
                'data' => 'Config|a:1:{s:4:"time";i:1639131948;}Admin|a:2:{s:4:"tech";b:1;s:4:"data";a:4:{s:8:"username";s:5:"admin";s:5:"email";s:13:"admin@test.fr";s:7:"cookies";a:0:{}s:5:"admin";b:1;}}Auth|a:4:{s:8:"username";s:5:"admin";s:5:"email";s:13:"admin@test.fr";s:7:"cookies";a:0:{}s:5:"admin";b:1;}',
                'expires' => 1639133388,
                'user_id' => null,
                'token' => 'testunit',
            ],
        ];
        parent::init();
    }
}
