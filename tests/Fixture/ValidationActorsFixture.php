<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ValidationActorsFixture
 */
class ValidationActorsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'validation_stage_id' => 1,
                'app_type' => 'USER',
                'app_key' => null,
                'app_meta' => '{"type_validation":"V"}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'app_foreign_key' => 1,
            ],
            [// id=2
                'validation_stage_id' => 1,
                'app_type' => 'MAIL',
                'app_key' => 'dupont.martin@test.fr',
                'app_meta' => '{"actor_name":"M. Dupont Martin"}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'app_foreign_key' => null,
            ],
            [// id=3
                'validation_stage_id' => 1,
                'app_type' => 'SERVICE_ARCHIVES',
                'app_key' => null,
                'app_meta' => null,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'app_foreign_key' => null,
            ],
            [// id=4
                'validation_stage_id' => 2,
                'app_type' => 'MAIL',
                'app_key' => 'test@test.fr',
                'app_meta' => null,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'app_foreign_key' => null,
            ],
            [// id=5
                'validation_stage_id' => 3,
                'app_type' => 'USER',
                'app_key' => null,
                'app_meta' => '{"type_validation":"V"}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'app_foreign_key' => 1,
            ],
            [// id=6
                'validation_stage_id' => 4,
                'app_type' => 'SERVICE_DEMANDEUR',
                'app_key' => null,
                'app_meta' => null,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'app_foreign_key' => null,
            ],
            [// id=7
                'validation_stage_id' => 5,
                'app_type' => 'SERVICE_PRODUCTEUR',
                'app_key' => null,
                'app_meta' => null,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'app_foreign_key' => null,
            ],
            [// id=8
                'validation_stage_id' => 6,
                'app_type' => 'USER',
                'app_key' => null,
                'app_meta' => '{"type_validation":"V"}',
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'app_foreign_key' => 1,
            ],
            [// id=9
                'validation_stage_id' => 2,
                'app_type' => 'USER',
                'app_key' => null,
                'app_meta' => '{"created_user_id":1,"type_validation":"V"}',
                'created' => '2021-12-09T15:13:37',
                'modified' => '2021-12-09T15:13:37',
                'app_foreign_key' => 1,
            ],
            [// id=10
                'validation_stage_id' => 7,
                'app_type' => 'USER',
                'app_key' => null,
                'app_meta' => '{"created_user_id":1,"type_validation":"V"}',
                'created' => '2021-12-09T15:14:17',
                'modified' => '2021-12-09T15:14:17',
                'app_foreign_key' => 1,
            ],
            [// id=11
                'validation_stage_id' => 8,
                'app_type' => 'USER',
                'app_key' => null,
                'app_meta' => '{"created_user_id":5,"type_validation":"V"}',
                'created' => '2021-12-09T17:28:16',
                'modified' => '2021-12-09T17:28:16',
                'app_foreign_key' => 5,
            ],
        ];
        parent::init();
    }
}
