<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TypeEntitiesFixture
 */
class TypeEntitiesFixture extends TestFixture
{
    /**
     * Valeurs par défaut
     * @return array[]
     */
    public static function defaultValues()
    {
        return [
            [// id=1
                'name' => 'Service d\'Exploitation',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'code' => 'SE',
            ],
            [// id=2
                'name' => 'Service d\'Archives',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'code' => 'SA',
            ],
            [// id=3
                'name' => 'Service de Contrôle Scientifique et Technique',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'code' => 'CST',
            ],
            [// id=4
                'name' => 'Service Versant',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'code' => 'SV',
            ],
            [// id=5
                'name' => 'Service Producteur',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'code' => 'SP',
            ],
            [// id=6
                'name' => 'Service Demandeur',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'code' => 'SD',
            ],
            [// id=7
                'name' => 'Opérateur de versement',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'code' => 'OV',
            ],
            [// id=8
                'name' => 'Organisationnel',
                'active' => true,
                'created' => '2021-12-09T13:53:54',
                'modified' => '2021-12-09T13:53:54',
                'code' => 'SO',
            ],
        ];
    }

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = self::defaultValues();
        parent::init();
    }
}
