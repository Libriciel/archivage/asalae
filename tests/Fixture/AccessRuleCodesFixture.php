<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AccessRuleCodesFixture
 */
class AccessRuleCodesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'org_entity_id' => null,
                'code' => 'AR038',
                'name' => '0 an',
                'description' => 'Documents administratifs librement communicables. (Code du Patrimoine, art. L. 213-1)',
                'duration' => 'P0Y',
            ],
            [// id=2
                'org_entity_id' => null,
                'code' => 'AR039',
                'name' => '25 ans',
                'description' => 'Documents dont la communication porte atteinte au secret des délibérations du Gouvernement et des autorités responsables relevant du pouvoir exécutif, à la conduite des relations extérieures, à la monnaie et au crédit public, au secret en matière commerciale et industrielle, à la recherche par les services compétents des infractions fiscales et douanières. (Code du Patrimoine, art. L. 213-2, I, 1, a)',
                'duration' => 'P25Y',
            ],
            [// id=3
                'org_entity_id' => null,
                'code' => 'AR040',
                'name' => '25 ans',
                'description' => 'Documents qui ne sont pas considérés comme administratifs et mentionnés au dernier alinéa de l\'article 1er de la loi n° 78-753 du 17 juillet 1978 : actes et documents élaborés ou détenus par les assemblées parlementaires, avis du Conseil d\'Etat et des juridictions administratives, documents de la Cour des comptes mentionnés à l\'article L. 140-9 du code des juridictions financières et les documents des chambres régionales des comptes mentionnés à l\'article L. 241-6 du même code, documents d\'instruction des réclamations adressées au Médiateur de la République, documents préalables à l\'élaboration du rapport d\'accréditation des établissements de santé prévu à l\'article L. 6113-6 du code de la santé publique, rapports d\'audit des établissements de santé mentionnés à l\'article 40 de la loi de financement de la sécurité sociale pour 2001 (n° 2000-1257 du 23 décembre 2000). (Code du Patrimoine, art. L. 213-2, I, 1, b)',
                'duration' => 'P25Y',
            ],
            [// id=4
                'org_entity_id' => null,
                'code' => 'AR041',
                'name' => '25 ans',
                'description' => 'Secret statistique. (Code du Patrimoine, art. L. 213-2, I, 1, a)',
                'duration' => 'P25Y',
            ],
            [// id=5
                'org_entity_id' => null,
                'code' => 'AR042',
                'name' => '25 ans',
                'description' => 'Documents élaborés dans le cadre d\'un contrat de prestation de services exécuté pour le compte d\'une ou de plusieurs personnes déterminées. (Code du Patrimoine, art. L. 213-2, I, 1, c)',
                'duration' => 'P25Y',
            ],
            [// id=6
                'org_entity_id' => null,
                'code' => 'AR043',
                'name' => '25 ans à compter de la date de décès de l\'intéressé',
                'description' => 'Documents dont la communication est susceptible de porter atteinte au secret médical. (Code du Patrimoine, art. L. 213-2, I, 2)',
                'duration' => 'P25Y',
            ],
            [// id=7
                'org_entity_id' => null,
                'code' => 'AR044',
                'name' => '25 ans à compter de la date de décès de l\'intéressé si ce délai est plus court que le délai ordinaire de 75 ans',
                'description' => 'Etat civil (actes de naissance ou de mariage). (Code du Patrimoine, art. L. 213-2, I, 4, e)',
                'duration' => 'P25Y',
            ],
            [// id=8
                'org_entity_id' => null,
                'code' => 'AR045',
                'name' => '25 ans à compter de la date de décès de l\'intéressé si ce délai est plus court que le délai ordinaire de 75 ans',
                'description' => 'Minutes et répertoires des officiers publics et ministériels. (Code du Patrimoine, art. L. 213-2, I, 4, d)',
                'duration' => 'P25Y',
            ],
            [// id=9
                'org_entity_id' => null,
                'code' => 'AR046',
                'name' => '25 ans à compter de la date de décès de l\'intéressé si ce délai est plus court que le délai ordinaire de 75 ans',
                'description' => 'Documents relatifs aux enquêtes réalisées par les services de la police judiciaire. (Code du Patrimoine, art. L. 213-2, I, 4, b)',
                'duration' => 'P25Y',
            ],
            [// id=10
                'org_entity_id' => null,
                'code' => 'AR047',
                'name' => '25 ans à compter de la date de décès de l\'intéressé si ce délai est plus court que le délai ordinaire de 75 ans',
                'description' => 'Documents relatifs aux affaires portées devant les juridictions, sous réserve des dispositions particulières relatives aux jugements, et à l\'exécution des décisions de justice. (Code du Patrimoine, art. L. 213-2, I, 4, c)',
                'duration' => 'P25Y',
            ],
            [// id=11
                'org_entity_id' => null,
                'code' => 'AR048',
                'name' => '50 ans',
                'description' => 'Documents dont la communication porte atteinte à la protection de la vie privée ou portant appréciation ou jugement de valeur sur une personne physique nommément désignée, ou facilement identifiable, ou qui font apparaître le comportement d\'une personne dans des conditions susceptibles de lui porter préjudice. (Code du Patrimoine, art. L. 213-2, I, 3)',
                'duration' => 'P50Y',
            ],
            [// id=12
                'org_entity_id' => null,
                'code' => 'AR049',
                'name' => '50 ans',
                'description' => 'Documents dont la communication porte atteinte au secret de la défense nationale, aux intérêts fondamentaux de l\'État dans la conduite de la politique extérieure, à la sûreté de l\'État, à la sécurité publique. (Code du Patrimoine, art. L. 213-2, I, 3)',
                'duration' => 'P50Y',
            ],
            [// id=13
                'org_entity_id' => null,
                'code' => 'AR050',
                'name' => '50 ans',
                'description' => 'Documents relatifs à la construction, à l\'équipement et au fonctionnement des ouvrages, bâtiments ou parties de bâtiments utilisés pour la détention de personnes ou recevant habituellement des personnes détenues. (Code du Patrimoine, art. L. 213-2, I, 3)',
                'duration' => 'P50Y',
            ],
            [// id=14
                'org_entity_id' => null,
                'code' => 'AR051',
                'name' => '50 ans',
                'description' => 'Documents élaborés dans le cadre d\'un contrat de prestation de services dont la communication porte atteinte au secret de la défense nationale, aux intérêts fondamentaux de l\'État dans la conduite de la politique extérieure, à la sûreté de l\'État, à la sécurité publique, à la protection de la vie privée, ou concernant des bâtiments employés pour la détention de personnes ou recevant habituellement des personnes détenues. (Code du Patrimoine, art. L. 213-2, I, 1, c)',
                'duration' => 'P50Y',
            ],
            [// id=15
                'org_entity_id' => null,
                'code' => 'AR052',
                'name' => '75 ans',
                'description' => 'Secret statistique : données collectées au moyen de questionnaires ayant trait aux faits et aux comportements d\'ordre privé. (Code du Patrimoine, art. L. 213-2, I, 4, a)',
                'duration' => 'P75Y',
            ],
            [// id=16
                'org_entity_id' => null,
                'code' => 'AR053',
                'name' => '75 ans',
                'description' => 'Documents élaborés dans le cadre d\'un contrat de prestation de services et pouvant être liés aux services de la police judiciaire ou aux affaires portées devant les juridictions. (Code du Patrimoine, art. L 213-2, I, 1, b)',
                'duration' => 'P75Y',
            ],
            [// id=17
                'org_entity_id' => null,
                'code' => 'AR054',
                'name' => '75 ans',
                'description' => 'Etat civil (actes de naissance ou de mariage). (Code du Patrimoine, art. L. 213-2, I, 4, e)',
                'duration' => 'P75Y',
            ],
            [// id=18
                'org_entity_id' => null,
                'code' => 'AR055',
                'name' => '75 ans',
                'description' => 'Minutes et répertoires des officiers publics et ministériels. (Code du Patrimoine, art. L. 213-2, I, 4, d)',
                'duration' => 'P75Y',
            ],
            [// id=19
                'org_entity_id' => null,
                'code' => 'AR056',
                'name' => '75 ans',
                'description' => 'Documents relatifs aux enquêtes réalisées par les services de la police judiciaire. (Code du Patrimoine, art. L. 213-2, I, 4, b)',
                'duration' => 'P75Y',
            ],
            [// id=20
                'org_entity_id' => null,
                'code' => 'AR057',
                'name' => '75 ans',
                'description' => 'Documents relatifs aux affaires portées devant les juridictions. (Code du Patrimoine, art. L. 213-2, I, 4, c)',
                'duration' => 'P75Y',
            ],
            [// id=21
                'org_entity_id' => null,
                'code' => 'AR058',
                'name' => '100 ans',
                'description' => 'Documents évoquant des personnes mineures : statistiques, enquêtes de la police judiciaire, documents relatifs aux affaires portées devant les juridictions et à l\'exécution des décisions de justice. (Code du Patrimoine, art. L. 213-2, I, 5)',
                'duration' => 'P100Y',
            ],
            [// id=22
                'org_entity_id' => null,
                'code' => 'AR059',
                'name' => '100 ans',
                'description' => 'Secret de la Défense nationale : documents couverts ou ayant été couverts par le secret de la défense nationale dont la communication est de nature à porter atteinte à la sécurité de personnes nommément désignées ou facilement identifiables (agents spéciaux, agents de renseignements...). (Code du Patrimoine, art. L. 213-2, I, 3)',
                'duration' => 'P100Y',
            ],
            [// id=23
                'org_entity_id' => null,
                'code' => 'AR060',
                'name' => '100 ans',
                'description' => 'Documents dont la communication est de nature à porter atteinte à l\'intimité de la vie sexuelle des personnes : enquêtes de la police judiciaire, documents relatifs aux affaires portées devant les juridictions. (Code du Patrimoine, art. L. 213-2, I, 5)',
                'duration' => 'P100Y',
            ],
            [// id=24
                'org_entity_id' => null,
                'code' => 'AR061',
                'name' => '120 ans à compter de la date de naissance si la date de décès de l\'intéressé n\'est pas connue',
                'description' => 'Documents dont la communication est susceptible de porter atteinte au secret médical. (Code du Patrimoine, art. L. 213-2, I, 2)',
                'duration' => 'P120Y',
            ],
            [// id=25
                'org_entity_id' => null,
                'code' => 'AR062',
                'name' => 'Illimitée',
                'description' => 'Archives publiques dont la communication est susceptible d\'entraîner la diffusion d\'informations permettant de concevoir, fabriquer, utiliser ou localiser des armes nucléaires, biologiques, chimiques ou toutes autres armes ayant des effets directs ou indirects de destruction d\'un niveau analogue. (Code du Patrimoine, art. L. 213-2, II)',
                'duration' => 'P1000Y',
            ],
            [// id=26
                'org_entity_id' => 2,
                'code' => 'AR_001',
                'name' => 'Règle supprimable',
                'description' => 'Règle non utilisée',
                'duration' => 'P1Y6M',
            ],
            [// id=27
                'org_entity_id' => 2,
                'code' => 'AR_002',
                'name' => 'Règle non supprimable',
                'description' => 'Règle utilisée dans AccessRule',
                'duration' => 'P1Y',
            ],
        ];
        parent::init();
    }
}
