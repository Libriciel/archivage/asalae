<?php
declare(strict_types=1);

namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchivesRestitutionRequestsFixture
 */
class ArchivesRestitutionRequestsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archive_id' => 3,
                'restitution_request_id' => 1,
            ],
        ];
        parent::init();
    }
}
