<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DestructionNotificationXpathsFixture
 */
class DestructionNotificationXpathsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'destruction_notification_id' => 1,
                'archive_id' => 3,
                'archive_unit_identifier' => 'sa_3',
                'archive_unit_name' => 'seda2.1',
                'description_xpath' => 'ArchiveUnit[1]',
            ],
        ];
        parent::init();
    }
}
