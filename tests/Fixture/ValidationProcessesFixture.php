<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ValidationProcessesFixture
 */
class ValidationProcessesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'validation_chain_id' => 1,
                'current_stage_id' => null,
                'app_subject_type' => 'Transfers',
                'app_subject_key' => null,
                'processed' => true,
                'validated' => true,
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-09T15:11:49',
                'modified' => '2021-12-09T15:15:26',
                'app_foreign_key' => 1,
                'current_step' => 3,
            ],
            [// id=2
                'validation_chain_id' => 2,
                'current_stage_id' => null,
                'app_subject_type' => 'Transfers',
                'app_subject_key' => null,
                'processed' => true,
                'validated' => false,
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-09T15:11:49',
                'modified' => '2021-12-09T15:26:18',
                'app_foreign_key' => 2,
                'current_step' => 2,
            ],
            [// id=3
                'validation_chain_id' => 3,
                'current_stage_id' => null,
                'app_subject_type' => 'DeliveryRequests',
                'app_subject_key' => null,
                'processed' => true,
                'validated' => true,
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-09T15:33:48',
                'modified' => '2021-12-09T15:36:14',
                'app_foreign_key' => 1,
                'current_step' => 2,
            ],
            [// id=4
                'validation_chain_id' => 4,
                'current_stage_id' => null,
                'app_subject_type' => 'DestructionRequests',
                'app_subject_key' => null,
                'processed' => true,
                'validated' => true,
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-09T16:21:45',
                'modified' => '2021-12-09T16:22:19',
                'app_foreign_key' => 1,
                'current_step' => 2,
            ],
            [// id=5
                'validation_chain_id' => 5,
                'current_stage_id' => null,
                'app_subject_type' => 'RestitutionRequests',
                'app_subject_key' => null,
                'processed' => true,
                'validated' => true,
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-09T16:56:54',
                'modified' => '2021-12-09T16:58:38',
                'app_foreign_key' => 1,
                'current_step' => 2,
            ],
            [// id=6
                'validation_chain_id' => 6,
                'current_stage_id' => null,
                'app_subject_type' => 'OutgoingTransferRequests',
                'app_subject_key' => null,
                'processed' => true,
                'validated' => true,
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-09T17:30:42',
                'modified' => '2021-12-09T17:30:52',
                'app_foreign_key' => 1,
                'current_step' => 2,
            ],
            [// id=7
                'validation_chain_id' => 7,
                'current_stage_id' => 8,
                'app_subject_type' => 'Transfers',
                'app_subject_key' => null,
                'processed' => false,
                'validated' => null,
                'app_meta' => '{"created_user_id":4}',
                'created' => '2021-12-09T17:30:56',
                'modified' => '2021-12-09T17:30:56',
                'app_foreign_key' => 7,
                'current_step' => 1,
            ],
            [// id=8
                'validation_chain_id' => 1,
                'current_stage_id' => 1,
                'app_subject_type' => 'Transfers',
                'app_subject_key' => null,
                'processed' => false,
                'validated' => null,
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-10T09:28:21',
                'modified' => '2021-12-10T09:28:21',
                'app_foreign_key' => 8,
                'current_step' => 1,
            ],
        ];
        parent::init();
    }
}
