<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OaipmhArchivesFixture
 */
class OaipmhArchivesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'oaipmh_token_id' => 3,
                'archive_id' => 1,
                'datetime' => '2022-01-01T12:00:00',
                'deleted' => false,
            ],
            [// id=2
                'oaipmh_token_id' => 3,
                'archive_id' => 2,
                'datetime' => '2022-01-01T12:00:00',
                'deleted' => false,
            ],
            [// id=3
                'oaipmh_token_id' => 3,
                'archive_id' => 3,
                'datetime' => '2022-01-01T12:00:00',
                'deleted' => false,
            ],
            [// id=4
                'oaipmh_token_id' => 4,
                'archive_id' => 1,
                'datetime' => '2022-01-01T12:00:00',
                'deleted' => false,
            ],
            [// id=5
                'oaipmh_token_id' => 4,
                'archive_id' => 2,
                'datetime' => '2022-01-01T12:00:00',
                'deleted' => false,
            ],
            [// id=6
                'oaipmh_token_id' => 4,
                'archive_id' => 3,
                'datetime' => '2022-01-01T12:00:00',
                'deleted' => false,
            ],
        ];
        parent::init();
    }
}
