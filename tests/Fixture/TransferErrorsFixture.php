<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TransferErrorsFixture
 */
class TransferErrorsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'transfer_id' => 2,
                'level' => 'error',
                'code' => 211,
                'message' => 'Aucune pièce jointe référencée dans le bordereau',
            ],
            [// id=2
                'transfer_id' => 7,
                'level' => 'error',
                'code' => 203,
                'message' => 'Niveau de service non trouvé : servicelevel1',
            ],
        ];
        parent::init();
    }
}
