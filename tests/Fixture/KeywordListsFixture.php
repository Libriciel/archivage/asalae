<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * KeywordListsFixture
 */
class KeywordListsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'org_entity_id' => 2,
                'identifier' => 'keyword_list',
                'name' => 'mes mots-clés',
                'description' => '',
                'active' => true,
                'created' => '2021-12-09T15:02:53',
                'modified' => '2021-12-09T15:03:14',
                'version' => 1,
                'has_workspace' => false,
            ],
        ];
        parent::init();
    }
}
