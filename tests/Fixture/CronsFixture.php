<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CronsFixture
 */
class CronsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'name' => 'Vérification du fonctionnement de asalae',
                'description' => '<p>La tâche vérifie que asalae fonctionne dans les conditions nominales et qu\'il
est en mesure d\'assurer le service demandé à un SAE.</p>
<p>Les vérifications portent sur :
<ul>
  <li>l\'installation du SAE : vérification du serveur, de php
  (version et modules supplémentaires), de postgres,
  des sources de l\'application (version, plugin, ...),
  de la version du schéma de la base de données</li>

  <li>les outils et services externes : vérification des outils de conversion,
  de détection du format des fichiers, de l\'antivirus, du service d\'horodatage, ...</li>

  <li>les volumes de stockage : accessible, alerte taux d\'occupation et date de fin programmée</li>
</ul>
</p>',
                'classname' => 'Asalae\Cron\Check',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T02:00:00',
                'app_meta' => null,
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=2
                'name' => 'Mise à jour Libersign',
                'description' => 'Met à jour le logiciel de signature',
                'classname' => 'AsalaeCore\Cron\LiberSign',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T02:00:00',
                'app_meta' => null,
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=3
                'name' => 'Déverrouillage des crons KO',
                'description' => 'En cas de plantage d\'un cron, celui-ci est déverrouillé après une certaine période (1h par défaut)',
                'classname' => 'AsalaeCore\Cron\Unlocker',
                'active' => true,
                'locked' => false,
                'frequency' => 'PT1H',
                'next' => '2021-12-09T01:00:00',
                'app_meta' => null,
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=4
                'name' => 'Mise à jour de la liste des pronoms',
                'description' => 'Fait appel à un webservice pour mettre à jour à table des pronoms (fmt)',
                'classname' => 'AsalaeCore\Cron\Pronom',
                'active' => true,
                'locked' => true,
                'frequency' => 'P1D',
                'next' => '2021-12-08T02:00:00',
                'app_meta' => '{"wsdl":"https:\/\/www.nationalarchives.gov.uk\/PRONOM\/Services\/Contract\/PRONOM.wsdl","endpoint":"https:\/\/www.nationalarchives.gov.uk\/PRONOM\/service.asmx","action_version":"getSignatureFileVersionV1","action_data":"getSignatureFileV1","extract_version":"Version.Version","extract_data":"SignatureFile.FFSignatureFile.FileFormatCollection.FileFormat","use_proxy":false}',
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=5
                'name' => 'Créateur de jobs',
                'description' => 'Recherche des cas bloqués dans un état intermédiaire afin de lancer les jobs qui pourraient le débloquer',
                'classname' => 'Asalae\Cron\JobMaker',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T02:00:00',
                'app_meta' => '{"freezedInterval":"P1D"}',
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=6
                'name' => 'Création des journaux des événements',
                'description' =>
                    "Création des fichiers des journaux des événements sous forme de fichiers xml PREMIS"
                    ." et stockage dans les archives techniques des journaux des événements. "
                    ."Le service d'Archives gestionnaire conserve les journaux d'exploitation"
                    ." ainsi que les fichiers sceau des journaux",
                'classname' => 'Asalae\Cron\EventsLogsExport',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T01:00:00',
                'app_meta' => null,
                'parallelisable' => true,
                'max_duration' => 'PT6H',
                'last_email' => null,
            ],
            [// id=7
                'name' => 'Vérification de l\'intégrité des fichiers des archives',
                'description' => '<p>Effectue la lecture des fichiers des archives et vérifie que les tailles 
                            et empreintes recalculées correspondent à celles stockées en base de données.</p>',
                'classname' => 'Asalae\Cron\ArchivesIntegrityControl',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T02:00:00',
                'app_meta' => '{"max_execution_time":1,"min_delay":7}',
                'parallelisable' => false,
                'max_duration' => 'PT6H',
                'last_email' => null,
            ],
            [// id=8
                'name' => 'Nettoyage des entrées expirées en base de donnée et des fichiers temporaires',
                'description' => '<p>Effectue une suppression des entrées expirées dans les tables liées aux Tokens Oai-Pmh, Access tokens et Auth urls, ainsi que des fichiers temporaires liés aux zip d\'archives.</p>',
                'classname' => 'Asalae\Cron\GarbageCollector',
                'active' => true,
                'locked' => false,
                'frequency' => 'PT1H',
                'next' => '2021-12-09T01:00:00',
                'app_meta' => null,
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=9
                'name' => 'Rétention des transferts acceptés : suppression des fichiers',
                'description' => '<p>Suppression des fichiers des transferts acceptés une fois le délai de rétention passé.
                            Il faut également que les archives issues des transferts soient intègres.</p>',
                'classname' => 'Asalae\Cron\TransfersRetention',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T02:00:00',
                'app_meta' => '{"retention_delay":1}',
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=10
                'name' => 'Mise à jour des informations des utilisateurs LDAP/AD',
                'description' => 'Mise à jour du nom et du mail des utilisateurs issus de LDAP/AD.
                        Les utilisateurs ne faisant plus partie des LDAP/AD liés sont listés 
                        dans le rapport d\'exécution et doivront être désactivés par 
                        l\'admistrateur fonctionnel de asalae.',
                'classname' => 'Asalae\Cron\UpdateLDAPUsers',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T02:00:00',
                'app_meta' => null,
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=11
                'name' => 'Vérification de la présence des fichiers des archives',
                'description' => 'Effectue la vérification de la présence des fichiers des archives. 
                        Paramètre 1, optionnel, défaut = 1, temps maximum d\'exécution de la tâche en heures. 
                        Paramètre 2, optionnel, défaut = 1, délai minimmum entre deux vérifications en jours.',
                'classname' => 'Asalae\Cron\ArchivesFilesExistControl',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T02:00:00',
                'app_meta' => '{"max_execution_time":1,"min_delay":1}',
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=12
                'name' => 'Recalcul du champ de recherche du catalogue',
                'description' => 'Recalcul du champ de recherche du catalogue lorsque 
                        les règles de restriction d\'accès des descriptions arrivent à leur terme.',
                'classname' => 'Asalae\Cron\ArchiveUnitCalcSearch',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T02:00:00',
                'app_meta' => null,
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=13
                'name' => 'Recalcul du champ de dua échue racine des unités d\'archives',
                'description' => 'Recalcul du champ de dua échue racine qui sert à
                         la sélection des unités d\'archives dans la vue DUA échue.',
                'classname' => 'Asalae\Cron\ArchiveUnitCalcExpiredDUARoot',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T02:00:00',
                'app_meta' => null,
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=14
                'name' => 'Restitutions : élimination des archives restituées',
                'description' => 'Elimination des archives liées aux restitutions aquittées. Sans aucune actions, les archives seront automatiquement éliminées après le délai spécifié dans la configuration des restitutions après l\'acquittement',
                'classname' => 'Asalae\Cron\RestitutionRequestArchiveDestruction',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T02:00:00',
                'app_meta' => null,
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=15
                'name' => 'Transferts sortants : suivi',
                'description' => 'Lecture des accusés de réception et des réponses aux transferts des transferts sortants en cours',
                'classname' => 'Asalae\Cron\OutgoingTransferTracking',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T00:00:00',
                'app_meta' => null,
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=16
                'name' => 'Transferts sortants : élimination des archives transférées',
                'description' => 'Elimination des archives liées aux transferts sortants acceptés. Sans aucune actions, les archives seront automatiquement éliminées après le délai spécifié dans la configuration des transferts sortants',
                'classname' => 'Asalae\Cron\OutgoingTransferRequestArchiveDestruction',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2021-12-08T00:00:00',
                'app_meta' => null,
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=17
                'name' => 'Suppression du cache expiré',
                'description' => 'Suppression des fichiers de cache sur le serveur en fonction de leur date d\'expiration.',
                'classname' => 'Asalae\Cron\CacheClear',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2022-09-06T00:15:00',
                'app_meta' => null,
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=18
                'name' => 'Attestations d\'élimination',
                'description' => 'Génération des attestations d\'élimination après le délai de destruction des sauvegardes.',
                'classname' => 'Asalae\Cron\DestructionCertificateCreation',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2022-09-26T02:00:00',
                'app_meta' => null,
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
            [// id=19
                'name' => 'Notifications par mail',
                'description' => 'Envoi des mails de notification à destination des utilisateurs pour les validations à effectuer, les alertes de jobs en erreur et d\'absence de transfert et le récapitulatif des transfers.',
                'classname' => 'Asalae\Cron\MailNotifications',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2023-10-01T02:00:00',
                'app_meta' => null,
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ],
        ];
        parent::init();
    }
}
