<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchiveDescriptionsFixture
 */
class ArchiveDescriptionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archive_unit_id' => 1,
                'access_rule_id' => 2,
                'description' => null,
                'history' => null,
                'file_plan_position' => null,
                'oldest_date' => null,
                'latest_date' => null,
            ],
            [// id=2
                'archive_unit_id' => 3,
                'access_rule_id' => 4,
                'description' => null,
                'history' => null,
                'file_plan_position' => null,
                'oldest_date' => null,
                'latest_date' => null,
            ],
            [// id=3
                'archive_unit_id' => 4,
                'access_rule_id' => 8,
                'description' => null,
                'history' => null,
                'file_plan_position' => null,
                'oldest_date' => null,
                'latest_date' => null,
            ],
            [// id=4
                'archive_unit_id' => 5,
                'access_rule_id' => 10,
                'description' => null,
                'history' => null,
                'file_plan_position' => null,
                'oldest_date' => null,
                'latest_date' => null,
            ],
        ];
        parent::init();
    }
}
