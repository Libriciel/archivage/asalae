<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ValidationHistoriesFixture
 */
class ValidationHistoriesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'validation_process_id' => 1,
                'validation_actor_id' => 3,
                'action' => 'validate',
                'comment' => '',
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-09T15:15:18',
                'current_step' => 2,
            ],
            [// id=2
                'validation_process_id' => 1,
                'validation_actor_id' => 9,
                'action' => 'validate',
                'comment' => '',
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-09T15:15:26',
                'current_step' => 3,
            ],
            [// id=3
                'validation_process_id' => 2,
                'validation_actor_id' => 5,
                'action' => 'invalidate',
                'comment' => '',
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-09T15:26:18',
                'current_step' => 2,
            ],
            [// id=4
                'validation_process_id' => 3,
                'validation_actor_id' => 6,
                'action' => 'validate',
                'comment' => '',
                'app_meta' => '{"created_user_id":3}',
                'created' => '2021-12-09T15:36:14',
                'current_step' => 2,
            ],
            [// id=5
                'validation_process_id' => 4,
                'validation_actor_id' => 7,
                'action' => 'validate',
                'comment' => '',
                'app_meta' => '{"created_user_id":3}',
                'created' => '2021-12-09T16:22:19',
                'current_step' => 2,
            ],
            [// id=6
                'validation_process_id' => 5,
                'validation_actor_id' => 8,
                'action' => 'validate',
                'comment' => '',
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-09T16:58:38',
                'current_step' => 2,
            ],
            [// id=7
                'validation_process_id' => 6,
                'validation_actor_id' => 10,
                'action' => 'validate',
                'comment' => '',
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-09T17:30:52',
                'current_step' => 2,
            ],
        ];
        parent::init();
    }
}
