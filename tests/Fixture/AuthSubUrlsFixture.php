<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AuthSubUrlsFixture
 */
class AuthSubUrlsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'auth_url_id' => 1,
                'url' => '/transfers/display-xml/1',
            ],
            [// id=2
                'auth_url_id' => 3,
                'url' => '/transfers/display-xml/8',
            ],
        ];
        parent::init();
    }
}
