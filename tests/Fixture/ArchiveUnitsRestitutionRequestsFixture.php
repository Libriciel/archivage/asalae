<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchiveUnitsRestitutionRequestsFixture
 */
class ArchiveUnitsRestitutionRequestsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archive_unit_id' => 4,
                'restitution_request_id' => 1,
            ],
        ];
        parent::init();
    }
}
