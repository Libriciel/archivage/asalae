<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DestructionRequestsFixture
 */
class DestructionRequestsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'identifier' => 'sa_ADR_1',
                'comment' => 'Demande d\'élimination de l\'unité d\'archive par le Mon Service d\'archive :
- sa_3 - seda2.1',
                'archival_agency_id' => 2,
                'originating_agency_id' => 5,
                'control_authority_id' => 2,
                'state' => 'destroyed',
                'last_state_update' => '2021-12-09T16:22:20',
                'states_history' => '[{"date":"2021-12-09T16:21:45.336160+0100","state":"validating"},{"date":"2021-12-09T16:22:19.065281+0100","state":"accepted"},{"date":"2021-12-09T16:22:19.278410+0100","state":"files_destroying"},{"date":"2021-12-09T16:22:19.589070+0100","state":"description_deleting"},{"date":"2021-12-09T16:22:20.091806+0100","state":"destroyed"}]',
                'archive_units_count' => 1,
                'original_count' => 1,
                'original_size' => 5,
                'created_user_id' => 1,
                'created' => '2021-12-09T16:21:38',
                'modified' => '2021-12-09T16:22:20',
                'validation_chain_id' => null,
                'certified' => null,
            ],
        ];
        parent::init();
    }
}
