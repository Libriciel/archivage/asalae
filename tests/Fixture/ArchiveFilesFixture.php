<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchiveFilesFixture
 */
class ArchiveFilesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archive_id' => 1,
                'stored_file_id' => 2,
                'is_integrity_ok' => null,
                'type' => 'lifecycle',
                'filename' => 'sa_1_lifecycle.xml',
            ],
            [// id=2
                'archive_id' => 1,
                'stored_file_id' => 3,
                'is_integrity_ok' => null,
                'type' => 'description',
                'filename' => 'sa_1_description.xml',
            ],
            [// id=3
                'archive_id' => 1,
                'stored_file_id' => 4,
                'is_integrity_ok' => null,
                'type' => 'transfer',
                'filename' => 'sa_1_transfer.xml',
            ],
            [// id=4
                'archive_id' => 2,
                'stored_file_id' => 7,
                'is_integrity_ok' => null,
                'type' => 'lifecycle',
                'filename' => 'sa_2_lifecycle.xml',
            ],
            [// id=5
                'archive_id' => 2,
                'stored_file_id' => 8,
                'is_integrity_ok' => null,
                'type' => 'description',
                'filename' => 'sa_2_description.xml',
            ],
            [// id=6
                'archive_id' => 2,
                'stored_file_id' => 9,
                'is_integrity_ok' => null,
                'type' => 'transfer',
                'filename' => 'sa_2_transfer.xml',
            ],
            [// id=7
                'archive_id' => 3,
                'stored_file_id' => 10,
                'is_integrity_ok' => null,
                'type' => 'lifecycle',
                'filename' => 'sa_3_lifecycle.xml',
            ],
            [// id=8
                'archive_id' => 3,
                'stored_file_id' => 11,
                'is_integrity_ok' => null,
                'type' => 'description',
                'filename' => 'sa_3_description.xml',
            ],
            [// id=9
                'archive_id' => 3,
                'stored_file_id' => 12,
                'is_integrity_ok' => null,
                'type' => 'transfer',
                'filename' => 'sa_3_transfer.xml',
            ],
            [// id=10
                'archive_id' => 3,
                'stored_file_id' => 13,
                'is_integrity_ok' => null,
                'type' => 'deleted',
                'filename' => 'sa_3_deleted.json',
            ],
            [// id=11
                'archive_id' => 4,
                'stored_file_id' => 15,
                'is_integrity_ok' => null,
                'type' => 'lifecycle',
                'filename' => 'sa_4_lifecycle.xml',
            ],
            [// id=12
                'archive_id' => 4,
                'stored_file_id' => 16,
                'is_integrity_ok' => null,
                'type' => 'description',
                'filename' => 'sa_4_description.xml',
            ],
            [// id=13
                'archive_id' => 4,
                'stored_file_id' => 17,
                'is_integrity_ok' => null,
                'type' => 'transfer',
                'filename' => 'sa_4_transfer.xml',
            ],
            [// id=14
                'archive_id' => 5,
                'stored_file_id' => 19,
                'is_integrity_ok' => null,
                'type' => 'lifecycle',
                'filename' => 'sa_5_lifecycle.xml',
            ],
            [// id=15
                'archive_id' => 5,
                'stored_file_id' => 20,
                'is_integrity_ok' => null,
                'type' => 'description',
                'filename' => 'sa_5_description.xml',
            ],
            [// id=16
                'archive_id' => 5,
                'stored_file_id' => 21,
                'is_integrity_ok' => null,
                'type' => 'transfer',
                'filename' => 'sa_5_transfer.xml',
            ],
        ];
        parent::init();
    }
}
