<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * KeywordsFixture
 */
class KeywordsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'keyword_list_id' => 1,
                'version' => 1,
                'code' => 'foo',
                'name' => 'foo',
                'app_meta' => '{"created_user_id":1}',
                'parent_id' => null,
                'lft' => 1,
                'rght' => 2,
                'exact_match' => null,
                'change_note' => null,
                'created' => '2021-12-09T15:03:09',
                'modified' => '2021-12-09T15:03:09',
            ],
            [// id=2
                'keyword_list_id' => 1,
                'version' => 1,
                'code' => 'bar',
                'name' => 'bar',
                'app_meta' => '{"created_user_id":1}',
                'parent_id' => null,
                'lft' => 3,
                'rght' => 4,
                'exact_match' => null,
                'change_note' => null,
                'created' => '2021-12-09T15:03:09',
                'modified' => '2021-12-09T15:03:09',
            ],
        ];
        parent::init();
    }
}
