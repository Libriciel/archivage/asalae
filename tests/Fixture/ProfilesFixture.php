<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProfilesFixture
 */
class ProfilesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'org_entity_id' => 2,
                'identifier' => 'profile1',
                'name' => 'Mon profil d\'archives',
                'description' => '',
                'active' => true,
                'created' => '2021-12-09T14:18:39',
                'modified' => '2021-12-09T14:18:39',
            ],
            [// id=2
                'org_entity_id' => 3,
                'identifier' => 'profil',
                'name' => 'profil',
                'description' => '',
                'active' => true,
                'created' => '2021-12-09T17:30:17',
                'modified' => '2021-12-09T17:30:17',
            ],
        ];
        parent::init();
    }
}
