<?php
namespace Asalae\Test\Fixture;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * AcosFixture
 */
class AcosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'parent_id' => null,
                'model' => '',
                'foreign_key' => null,
                'alias' => 'root',
                'lft' => 1,
                'rght' => 1388,
            ],
            [// id=2
                'parent_id' => 1, // root
                'model' => 'root',
                'foreign_key' => null,
                'alias' => 'controllers',
                'lft' => 2,
                'rght' => 1305,
            ],
            [// id=3
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'AccessRuleCodes',
                'lft' => 3,
                'rght' => 18,
            ],
            [// id=4
                'parent_id' => 3, // root/controllers/AccessRuleCodes
                'model' => 'AccessRuleCodes',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 4,
                'rght' => 5,
            ],
            [// id=5
                'parent_id' => 3, // root/controllers/AccessRuleCodes
                'model' => 'AccessRuleCodes',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 6,
                'rght' => 7,
            ],
            [// id=6
                'parent_id' => 3, // root/controllers/AccessRuleCodes
                'model' => 'AccessRuleCodes',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 8,
                'rght' => 9,
            ],
            [// id=7
                'parent_id' => 3, // root/controllers/AccessRuleCodes
                'model' => 'AccessRuleCodes',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 10,
                'rght' => 11,
            ],
            [// id=8
                'parent_id' => 3, // root/controllers/AccessRuleCodes
                'model' => 'AccessRuleCodes',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 12,
                'rght' => 13,
            ],
            [// id=9
                'parent_id' => 3, // root/controllers/AccessRuleCodes
                'model' => 'AccessRuleCodes',
                'foreign_key' => null,
                'alias' => 'populateSelect',
                'lft' => 14,
                'rght' => 15,
            ],
            [// id=10
                'parent_id' => 3, // root/controllers/AccessRuleCodes
                'model' => 'AccessRuleCodes',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 16,
                'rght' => 17,
            ],
            [// id=11
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'AdminTenants',
                'lft' => 19,
                'rght' => 102,
            ],
            [// id=12
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'addEaccpf',
                'lft' => 20,
                'rght' => 21,
            ],
            [// id=13
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'addLdap',
                'lft' => 22,
                'rght' => 23,
            ],
            [// id=14
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'addUser',
                'lft' => 24,
                'rght' => 25,
            ],
            [// id=15
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'ajaxCheck',
                'lft' => 26,
                'rght' => 27,
            ],
            [// id=16
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'ajaxCheckDisk',
                'lft' => 28,
                'rght' => 29,
            ],
            [// id=17
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'ajaxCheckTreeSanity',
                'lft' => 30,
                'rght' => 31,
            ],
            [// id=18
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'ajaxCheckVolumes',
                'lft' => 32,
                'rght' => 33,
            ],
            [// id=19
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'ajaxLogs',
                'lft' => 34,
                'rght' => 35,
            ],
            [// id=20
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'ajaxTasks',
                'lft' => 36,
                'rght' => 37,
            ],
            [// id=21
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'deleteLdap',
                'lft' => 38,
                'rght' => 39,
            ],
            [// id=22
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'deleteSession',
                'lft' => 40,
                'rght' => 41,
            ],
            [// id=23
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'downloadLog',
                'lft' => 42,
                'rght' => 43,
            ],
            [// id=24
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'editAdmin',
                'lft' => 44,
                'rght' => 45,
            ],
            [// id=25
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'editLdap',
                'lft' => 46,
                'rght' => 47,
            ],
            [// id=26
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'editServiceArchive',
                'lft' => 48,
                'rght' => 49,
            ],
            [// id=27
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'getLdapUsers',
                'lft' => 50,
                'rght' => 51,
            ],
            [// id=28
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'getWsRolesOptions',
                'lft' => 52,
                'rght' => 53,
            ],
            [// id=29
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'importLdapUser',
                'lft' => 54,
                'rght' => 55,
            ],
            [// id=30
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 56,
                'rght' => 57,
            ],
            [// id=31
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'indexJobs',
                'lft' => 58,
                'rght' => 59,
            ],
            [// id=32
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'indexLdaps',
                'lft' => 60,
                'rght' => 61,
            ],
            [// id=33
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'indexServicesArchives',
                'lft' => 62,
                'rght' => 63,
            ],
            [// id=34
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'indexSessions',
                'lft' => 64,
                'rght' => 65,
            ],
            [// id=35
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'indexVolumes',
                'lft' => 66,
                'rght' => 67,
            ],
            [// id=36
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'jobCancel',
                'lft' => 68,
                'rght' => 69,
            ],
            [// id=37
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'jobInfo',
                'lft' => 70,
                'rght' => 71,
            ],
            [// id=38
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'jobPause',
                'lft' => 72,
                'rght' => 73,
            ],
            [// id=39
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'jobResume',
                'lft' => 74,
                'rght' => 75,
            ],
            [// id=40
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'notifyAll',
                'lft' => 76,
                'rght' => 77,
            ],
            [// id=41
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'notifyUser',
                'lft' => 78,
                'rght' => 79,
            ],
            [// id=42
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'paginateUsers',
                'lft' => 80,
                'rght' => 81,
            ],
            [// id=43
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'phpInfo',
                'lft' => 82,
                'rght' => 83,
            ],
            [// id=44
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'ping',
                'lft' => 84,
                'rght' => 85,
            ],
            [// id=45
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'probe',
                'lft' => 86,
                'rght' => 87,
            ],
            [// id=46
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'resumeAllTube',
                'lft' => 88,
                'rght' => 89,
            ],
            [// id=47
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'tail',
                'lft' => 90,
                'rght' => 91,
            ],
            [// id=48
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'testVolume',
                'lft' => 92,
                'rght' => 93,
            ],
            [// id=49
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'viewVolume',
                'lft' => 94,
                'rght' => 95,
            ],
            [// id=50
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'volumeCheckHash',
                'lft' => 96,
                'rght' => 97,
            ],
            [// id=51
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'volumeClient',
                'lft' => 98,
                'rght' => 99,
            ],
            [// id=52
                'parent_id' => 11, // root/controllers/AdminTenants
                'model' => 'AdminTenants',
                'foreign_key' => null,
                'alias' => 'workerLog',
                'lft' => 100,
                'rght' => 101,
            ],
            [// id=53
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Admins',
                'lft' => 103,
                'rght' => 280,
            ],
            [// id=54
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addEaccpf',
                'lft' => 104,
                'rght' => 105,
            ],
            [// id=55
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addLdap',
                'lft' => 106,
                'rght' => 107,
            ],
            [// id=56
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addServiceArchive',
                'lft' => 108,
                'rght' => 109,
            ],
            [// id=57
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addSpace',
                'lft' => 110,
                'rght' => 111,
            ],
            [// id=58
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addSuperArchivist',
                'lft' => 112,
                'rght' => 113,
            ],
            [// id=59
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addTimestamper',
                'lft' => 114,
                'rght' => 115,
            ],
            [// id=60
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addTimestamperCert',
                'lft' => 116,
                'rght' => 117,
            ],
            [// id=61
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addUser',
                'lft' => 118,
                'rght' => 119,
            ],
            [// id=62
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addVolume',
                'lft' => 120,
                'rght' => 121,
            ],
            [// id=63
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addWorker',
                'lft' => 122,
                'rght' => 123,
            ],
            [// id=64
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxCheck',
                'lft' => 124,
                'rght' => 125,
            ],
            [// id=65
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxCheckDisk',
                'lft' => 126,
                'rght' => 127,
            ],
            [// id=66
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxCheckTreeSanity',
                'lft' => 128,
                'rght' => 129,
            ],
            [// id=67
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxCheckVolumes',
                'lft' => 130,
                'rght' => 131,
            ],
            [// id=68
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxCheckWebsocket',
                'lft' => 132,
                'rght' => 133,
            ],
            [// id=69
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxCheckWorkers',
                'lft' => 134,
                'rght' => 135,
            ],
            [// id=70
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxEditConf',
                'lft' => 136,
                'rght' => 137,
            ],
            [// id=71
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxInterrupt',
                'lft' => 138,
                'rght' => 139,
            ],
            [// id=72
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxLogin',
                'lft' => 140,
                'rght' => 141,
            ],
            [// id=73
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxLogs',
                'lft' => 142,
                'rght' => 143,
            ],
            [// id=74
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxTasks',
                'lft' => 144,
                'rght' => 145,
            ],
            [// id=75
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxToggleDebug',
                'lft' => 146,
                'rght' => 147,
            ],
            [// id=76
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'changeVolumesInSpace',
                'lft' => 148,
                'rght' => 149,
            ],
            [// id=77
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteCron',
                'lft' => 150,
                'rght' => 151,
            ],
            [// id=78
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteLdap',
                'lft' => 152,
                'rght' => 153,
            ],
            [// id=79
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteLogo',
                'lft' => 154,
                'rght' => 155,
            ],
            [// id=80
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteSession',
                'lft' => 156,
                'rght' => 157,
            ],
            [// id=81
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteSpace',
                'lft' => 158,
                'rght' => 159,
            ],
            [// id=82
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteTimestamper',
                'lft' => 160,
                'rght' => 161,
            ],
            [// id=83
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteTube',
                'lft' => 162,
                'rght' => 163,
            ],
            [// id=84
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteVolume',
                'lft' => 164,
                'rght' => 165,
            ],
            [// id=85
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteVolumeFile',
                'lft' => 166,
                'rght' => 167,
            ],
            [// id=86
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'downloadLog',
                'lft' => 168,
                'rght' => 169,
            ],
            [// id=87
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editAdmin',
                'lft' => 170,
                'rght' => 171,
            ],
            [// id=88
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editCron',
                'lft' => 172,
                'rght' => 173,
            ],
            [// id=89
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editLdap',
                'lft' => 174,
                'rght' => 175,
            ],
            [// id=90
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editServiceArchive',
                'lft' => 176,
                'rght' => 177,
            ],
            [// id=91
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editServiceExploitation',
                'lft' => 178,
                'rght' => 179,
            ],
            [// id=92
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editSpace',
                'lft' => 180,
                'rght' => 181,
            ],
            [// id=93
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editSuperArchivist',
                'lft' => 182,
                'rght' => 183,
            ],
            [// id=94
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editTimestamper',
                'lft' => 184,
                'rght' => 185,
            ],
            [// id=95
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editVolume',
                'lft' => 186,
                'rght' => 187,
            ],
            [// id=96
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'getCronState',
                'lft' => 188,
                'rght' => 189,
            ],
            [// id=97
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'getLdapUsers',
                'lft' => 190,
                'rght' => 191,
            ],
            [// id=98
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'getWsRolesOptions',
                'lft' => 192,
                'rght' => 193,
            ],
            [// id=99
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'importLdapUser',
                'lft' => 194,
                'rght' => 195,
            ],
            [// id=100
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 196,
                'rght' => 197,
            ],
            [// id=101
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexCrons',
                'lft' => 198,
                'rght' => 199,
            ],
            [// id=102
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexJobs',
                'lft' => 200,
                'rght' => 201,
            ],
            [// id=103
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexLdaps',
                'lft' => 202,
                'rght' => 203,
            ],
            [// id=104
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexServicesArchives',
                'lft' => 204,
                'rght' => 205,
            ],
            [// id=105
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexSessions',
                'lft' => 206,
                'rght' => 207,
            ],
            [// id=106
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexVolumes',
                'lft' => 208,
                'rght' => 209,
            ],
            [// id=107
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'jobCancel',
                'lft' => 210,
                'rght' => 211,
            ],
            [// id=108
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'jobInfo',
                'lft' => 212,
                'rght' => 213,
            ],
            [// id=109
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'jobPause',
                'lft' => 214,
                'rght' => 215,
            ],
            [// id=110
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'jobResume',
                'lft' => 216,
                'rght' => 217,
            ],
            [// id=111
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'killWorker',
                'lft' => 218,
                'rght' => 219,
            ],
            [// id=112
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'login',
                'lft' => 220,
                'rght' => 221,
            ],
            [// id=113
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'logout',
                'lft' => 222,
                'rght' => 223,
            ],
            [// id=114
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'notifyAll',
                'lft' => 224,
                'rght' => 225,
            ],
            [// id=115
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'notifyUser',
                'lft' => 226,
                'rght' => 227,
            ],
            [// id=116
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'paginateUsers',
                'lft' => 228,
                'rght' => 229,
            ],
            [// id=117
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'phpInfo',
                'lft' => 230,
                'rght' => 231,
            ],
            [// id=118
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ping',
                'lft' => 232,
                'rght' => 233,
            ],
            [// id=119
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'probe',
                'lft' => 234,
                'rght' => 235,
            ],
            [// id=120
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'promoteSuperArchivist',
                'lft' => 236,
                'rght' => 237,
            ],
            [// id=121
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'restartAllWorkers',
                'lft' => 238,
                'rght' => 239,
            ],
            [// id=122
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'resumeAllTube',
                'lft' => 240,
                'rght' => 241,
            ],
            [// id=123
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'revokeSuperArchivist',
                'lft' => 242,
                'rght' => 243,
            ],
            [// id=124
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'runCron',
                'lft' => 244,
                'rght' => 245,
            ],
            [// id=125
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'runWorkerManager',
                'lft' => 246,
                'rght' => 247,
            ],
            [// id=126
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'sendTestMail',
                'lft' => 248,
                'rght' => 249,
            ],
            [// id=127
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'setMainArchivalAgency',
                'lft' => 250,
                'rght' => 251,
            ],
            [// id=128
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'stopAllWorkers',
                'lft' => 252,
                'rght' => 253,
            ],
            [// id=129
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'stopService',
                'lft' => 254,
                'rght' => 255,
            ],
            [// id=130
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'stopWorker',
                'lft' => 256,
                'rght' => 257,
            ],
            [// id=131
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'tail',
                'lft' => 258,
                'rght' => 259,
            ],
            [// id=132
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'tailService',
                'lft' => 260,
                'rght' => 261,
            ],
            [// id=133
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'testTimestamper',
                'lft' => 262,
                'rght' => 263,
            ],
            [// id=134
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'testTimestamperTSA',
                'lft' => 264,
                'rght' => 265,
            ],
            [// id=135
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'testVolume',
                'lft' => 266,
                'rght' => 267,
            ],
            [// id=136
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'timestampers',
                'lft' => 268,
                'rght' => 269,
            ],
            [// id=137
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'viewCron',
                'lft' => 270,
                'rght' => 271,
            ],
            [// id=138
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'viewVolume',
                'lft' => 272,
                'rght' => 273,
            ],
            [// id=139
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'volumeCheckHash',
                'lft' => 274,
                'rght' => 275,
            ],
            [// id=140
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'volumeClient',
                'lft' => 276,
                'rght' => 277,
            ],
            [// id=141
                'parent_id' => 53, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'workerLog',
                'lft' => 278,
                'rght' => 279,
            ],
            [// id=142
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Agreements',
                'lft' => 281,
                'rght' => 302,
            ],
            [// id=143
                'parent_id' => 142, // root/controllers/Agreements
                'model' => 'Agreements',
                'foreign_key' => null,
                'alias' => 'add1',
                'lft' => 282,
                'rght' => 289,
            ],
            [// id=144
                'parent_id' => 143, // root/controllers/Agreements/add1
                'model' => 'Agreements',
                'foreign_key' => null,
                'alias' => 'add2',
                'lft' => 283,
                'rght' => 284,
            ],
            [// id=145
                'parent_id' => 143, // root/controllers/Agreements/add1
                'model' => 'Agreements',
                'foreign_key' => null,
                'alias' => 'add3',
                'lft' => 285,
                'rght' => 286,
            ],
            [// id=146
                'parent_id' => 143, // root/controllers/Agreements/add1
                'model' => 'Agreements',
                'foreign_key' => null,
                'alias' => 'add4',
                'lft' => 287,
                'rght' => 288,
            ],
            [// id=147
                'parent_id' => 142, // root/controllers/Agreements
                'model' => 'Agreements',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 290,
                'rght' => 291,
            ],
            [// id=148
                'parent_id' => 142, // root/controllers/Agreements
                'model' => 'Agreements',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 292,
                'rght' => 293,
            ],
            [// id=149
                'parent_id' => 142, // root/controllers/Agreements
                'model' => 'Agreements',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 294,
                'rght' => 297,
            ],
            [// id=150
                'parent_id' => 142, // root/controllers/Agreements
                'model' => 'Agreements',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 298,
                'rght' => 299,
            ],
            [// id=151
                'parent_id' => 149, // root/controllers/Agreements/edit
                'model' => 'Agreements',
                'foreign_key' => null,
                'alias' => 'setDefault',
                'lft' => 295,
                'rght' => 296,
            ],
            [// id=152
                'parent_id' => 142, // root/controllers/Agreements
                'model' => 'Agreements',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 300,
                'rght' => 301,
            ],
            [// id=153
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Api',
                'lft' => 303,
                'rght' => 306,
            ],
            [// id=154
                'parent_id' => 153, // root/controllers/Api
                'model' => 'Api',
                'foreign_key' => null,
                'alias' => 'swagger',
                'lft' => 304,
                'rght' => 305,
            ],
            [// id=155
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'AppraisalRuleCodes',
                'lft' => 307,
                'rght' => 322,
            ],
            [// id=156
                'parent_id' => 155, // root/controllers/AppraisalRuleCodes
                'model' => 'AppraisalRuleCodes',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 308,
                'rght' => 309,
            ],
            [// id=157
                'parent_id' => 155, // root/controllers/AppraisalRuleCodes
                'model' => 'AppraisalRuleCodes',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 310,
                'rght' => 311,
            ],
            [// id=158
                'parent_id' => 155, // root/controllers/AppraisalRuleCodes
                'model' => 'AppraisalRuleCodes',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 312,
                'rght' => 313,
            ],
            [// id=159
                'parent_id' => 155, // root/controllers/AppraisalRuleCodes
                'model' => 'AppraisalRuleCodes',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 314,
                'rght' => 315,
            ],
            [// id=160
                'parent_id' => 155, // root/controllers/AppraisalRuleCodes
                'model' => 'AppraisalRuleCodes',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 316,
                'rght' => 317,
            ],
            [// id=161
                'parent_id' => 155, // root/controllers/AppraisalRuleCodes
                'model' => 'AppraisalRuleCodes',
                'foreign_key' => null,
                'alias' => 'populateSelect',
                'lft' => 318,
                'rght' => 319,
            ],
            [// id=162
                'parent_id' => 155, // root/controllers/AppraisalRuleCodes
                'model' => 'AppraisalRuleCodes',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 320,
                'rght' => 321,
            ],
            [// id=163
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'ArchiveBinaries',
                'lft' => 323,
                'rght' => 336,
            ],
            [// id=164
                'parent_id' => 163, // root/controllers/ArchiveBinaries
                'model' => 'ArchiveBinaries',
                'foreign_key' => null,
                'alias' => 'download',
                'lft' => 324,
                'rght' => 335,
            ],
            [// id=165
                'parent_id' => 164, // root/controllers/ArchiveBinaries/download
                'model' => 'ArchiveBinaries',
                'foreign_key' => null,
                'alias' => 'open',
                'lft' => 325,
                'rght' => 326,
            ],
            [// id=166
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'ArchiveFiles',
                'lft' => 337,
                'rght' => 338,
            ],
            [// id=167
                'parent_id' => 164, // root/controllers/ArchiveBinaries/download
                'model' => 'ArchiveFiles',
                'foreign_key' => null,
                'alias' => 'download',
                'lft' => 327,
                'rght' => 328,
            ],
            [// id=168
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'ArchiveKeywords',
                'lft' => 339,
                'rght' => 340,
            ],
            [// id=169
                'parent_id' => 183, // root/controllers/ArchiveUnits/edit
                'model' => 'ArchiveKeywords',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 373,
                'rght' => 374,
            ],
            [// id=170
                'parent_id' => 183, // root/controllers/ArchiveUnits/edit
                'model' => 'ArchiveKeywords',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 375,
                'rght' => 376,
            ],
            [// id=171
                'parent_id' => 183, // root/controllers/ArchiveUnits/edit
                'model' => 'ArchiveKeywords',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 377,
                'rght' => 378,
            ],
            [// id=172
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'ArchiveUnits',
                'lft' => 341,
                'rght' => 400,
            ],
            [// id=173
                'parent_id' => 172, // root/controllers/ArchiveUnits
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 342,
                'rght' => 343,
            ],
            [// id=174
                'parent_id' => 172, // root/controllers/ArchiveUnits
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'catalog',
                'lft' => 344,
                'rght' => 359,
            ],
            [// id=175
                'parent_id' => 172, // root/controllers/ArchiveUnits
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'catalogMyEntity',
                'lft' => 360,
                'rght' => 363,
            ],
            [// id=176
                'parent_id' => 182, // root/controllers/ArchiveUnits/duaExpired
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'description',
                'lft' => 365,
                'rght' => 366,
            ],
            [// id=177
                'parent_id' => 174, // root/controllers/ArchiveUnits/catalog
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'descriptionPublic',
                'lft' => 345,
                'rght' => 346,
            ],
            [// id=178
                'parent_id' => 182, // root/controllers/ArchiveUnits/duaExpired
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'displayXml',
                'lft' => 367,
                'rght' => 368,
            ],
            [// id=179
                'parent_id' => 174, // root/controllers/ArchiveUnits/catalog
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'displayXmlPublic',
                'lft' => 347,
                'rght' => 348,
            ],
            [// id=180
                'parent_id' => 174, // root/controllers/ArchiveUnits/catalog
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'downloadByName',
                'lft' => 349,
                'rght' => 350,
            ],
            [// id=181
                'parent_id' => 194, // root/controllers/ArchiveUnits/view
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'downloadFiles',
                'lft' => 393,
                'rght' => 394,
            ],
            [// id=182
                'parent_id' => 172, // root/controllers/ArchiveUnits
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'duaExpired',
                'lft' => 364,
                'rght' => 371,
            ],
            [// id=183
                'parent_id' => 172, // root/controllers/ArchiveUnits
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 372,
                'rght' => 379,
            ],
            [// id=184
                'parent_id' => 172, // root/controllers/ArchiveUnits
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'eliminable',
                'lft' => 380,
                'rght' => 381,
            ],
            [// id=185
                'parent_id' => 189, // root/controllers/ArchiveUnits/index
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'fullSearchBar',
                'lft' => 383,
                'rght' => 384,
            ],
            [// id=186
                'parent_id' => 189, // root/controllers/ArchiveUnits/index
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'getNavTree',
                'lft' => 385,
                'rght' => 386,
            ],
            [// id=187
                'parent_id' => 174, // root/controllers/ArchiveUnits/catalog
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'getPublicTree',
                'lft' => 351,
                'rght' => 352,
            ],
            [// id=188
                'parent_id' => 174, // root/controllers/ArchiveUnits/catalog
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'getTree',
                'lft' => 353,
                'rght' => 354,
            ],
            [// id=189
                'parent_id' => 172, // root/controllers/ArchiveUnits
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 382,
                'rght' => 387,
            ],
            [// id=190
                'parent_id' => 194, // root/controllers/ArchiveUnits/view
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'makeZip',
                'lft' => 395,
                'rght' => 396,
            ],
            [// id=191
                'parent_id' => 172, // root/controllers/ArchiveUnits
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'returnable',
                'lft' => 388,
                'rght' => 389,
            ],
            [// id=192
                'parent_id' => 174, // root/controllers/ArchiveUnits/catalog
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'searchBar',
                'lft' => 355,
                'rght' => 356,
            ],
            [// id=193
                'parent_id' => 172, // root/controllers/ArchiveUnits
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'transferable',
                'lft' => 390,
                'rght' => 391,
            ],
            [// id=194
                'parent_id' => 172, // root/controllers/ArchiveUnits
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 392,
                'rght' => 399,
            ],
            [// id=195
                'parent_id' => 194, // root/controllers/ArchiveUnits/view
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'viewBinaries',
                'lft' => 397,
                'rght' => 398,
            ],
            [// id=196
                'parent_id' => 182, // root/controllers/ArchiveUnits/duaExpired
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'viewDescriptionNode',
                'lft' => 369,
                'rght' => 370,
            ],
            [// id=197
                'parent_id' => 174, // root/controllers/ArchiveUnits/catalog
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'viewPublicDescriptionNode',
                'lft' => 357,
                'rght' => 358,
            ],
            [// id=198
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Archives',
                'lft' => 401,
                'rght' => 446,
            ],
            [// id=199
                'parent_id' => 198, // root/controllers/Archives
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 402,
                'rght' => 403,
            ],
            [// id=200
                'parent_id' => 198, // root/controllers/Archives
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'archivingCertificate',
                'lft' => 404,
                'rght' => 405,
            ],
            [// id=201
                'parent_id' => 198, // root/controllers/Archives
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'checkIntegrity',
                'lft' => 406,
                'rght' => 407,
            ],
            [// id=202
                'parent_id' => 198, // root/controllers/Archives
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'description',
                'lft' => 408,
                'rght' => 411,
            ],
            [// id=203
                'parent_id' => 220, // root/controllers/Archives/view
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'displayXml',
                'lft' => 433,
                'rght' => 434,
            ],
            [// id=204
                'parent_id' => 164, // root/controllers/ArchiveBinaries/download
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'downloadByName',
                'lft' => 329,
                'rght' => 330,
            ],
            [// id=205
                'parent_id' => 164, // root/controllers/ArchiveBinaries/download
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'downloadFiles',
                'lft' => 331,
                'rght' => 332,
            ],
            [// id=206
                'parent_id' => 220, // root/controllers/Archives/view
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'downloadPdf',
                'lft' => 435,
                'rght' => 436,
            ],
            [// id=207
                'parent_id' => 211, // root/controllers/Archives/index
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'exportEad',
                'lft' => 417,
                'rght' => 418,
            ],
            [// id=208
                'parent_id' => 198, // root/controllers/Archives
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'freeze',
                'lft' => 412,
                'rght' => 415,
            ],
            [// id=209
                'parent_id' => 217, // root/controllers/Archives/search
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'getFilters',
                'lft' => 427,
                'rght' => 428,
            ],
            [// id=210
                'parent_id' => 220, // root/controllers/Archives/view
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'getTree',
                'lft' => 437,
                'rght' => 438,
            ],
            [// id=211
                'parent_id' => 198, // root/controllers/Archives
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 416,
                'rght' => 421,
            ],
            [// id=212
                'parent_id' => 211, // root/controllers/Archives/index
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'indexOne',
                'lft' => 419,
                'rght' => 420,
            ],
            [// id=213
                'parent_id' => 198, // root/controllers/Archives
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'integrityCertificate',
                'lft' => 422,
                'rght' => 423,
            ],
            [// id=214
                'parent_id' => 198, // root/controllers/Archives
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'lifecycle',
                'lft' => 424,
                'rght' => 425,
            ],
            [// id=215
                'parent_id' => 220, // root/controllers/Archives/view
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'makePdf',
                'lft' => 439,
                'rght' => 440,
            ],
            [// id=216
                'parent_id' => 164, // root/controllers/ArchiveBinaries/download
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'makeZip',
                'lft' => 333,
                'rght' => 334,
            ],
            [// id=217
                'parent_id' => 198, // root/controllers/Archives
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'search',
                'lft' => 426,
                'rght' => 429,
            ],
            [// id=218
                'parent_id' => 198, // root/controllers/Archives
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'treatments',
                'lft' => 430,
                'rght' => 431,
            ],
            [// id=219
                'parent_id' => 208, // root/controllers/Archives/freeze
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'unfreeze',
                'lft' => 413,
                'rght' => 414,
            ],
            [// id=220
                'parent_id' => 198, // root/controllers/Archives
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 432,
                'rght' => 445,
            ],
            [// id=221
                'parent_id' => 220, // root/controllers/Archives/view
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'viewBinaries',
                'lft' => 441,
                'rght' => 442,
            ],
            [// id=222
                'parent_id' => 220, // root/controllers/Archives/view
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'viewBinary',
                'lft' => 443,
                'rght' => 444,
            ],
            [// id=223
                'parent_id' => 202, // root/controllers/Archives/description
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'viewDescriptionNode',
                'lft' => 409,
                'rght' => 410,
            ],
            [// id=224
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'ArchivingSystems',
                'lft' => 447,
                'rght' => 462,
            ],
            [// id=225
                'parent_id' => 224, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 448,
                'rght' => 449,
            ],
            [// id=226
                'parent_id' => 224, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 450,
                'rght' => 451,
            ],
            [// id=227
                'parent_id' => 224, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 452,
                'rght' => 453,
            ],
            [// id=228
                'parent_id' => 224, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 454,
                'rght' => 455,
            ],
            [// id=229
                'parent_id' => 224, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 456,
                'rght' => 457,
            ],
            [// id=230
                'parent_id' => 224, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'testConnection',
                'lft' => 458,
                'rght' => 459,
            ],
            [// id=231
                'parent_id' => 224, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 460,
                'rght' => 461,
            ],
            [// id=232
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Assets',
                'lft' => 463,
                'rght' => 470,
            ],
            [// id=233
                'parent_id' => 232, // root/controllers/Assets
                'model' => 'Assets',
                'foreign_key' => null,
                'alias' => 'configuredBackground',
                'lft' => 464,
                'rght' => 465,
            ],
            [// id=234
                'parent_id' => 232, // root/controllers/Assets
                'model' => 'Assets',
                'foreign_key' => null,
                'alias' => 'configuredLogoClient',
                'lft' => 466,
                'rght' => 467,
            ],
            [// id=235
                'parent_id' => 232, // root/controllers/Assets
                'model' => 'Assets',
                'foreign_key' => null,
                'alias' => 'css',
                'lft' => 468,
                'rght' => 469,
            ],
            [// id=236
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'AuthUrls',
                'lft' => 471,
                'rght' => 474,
            ],
            [// id=237
                'parent_id' => 236, // root/controllers/AuthUrls
                'model' => 'AuthUrls',
                'foreign_key' => null,
                'alias' => 'activate',
                'lft' => 472,
                'rght' => 473,
            ],
            [// id=238
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'BatchTreatments',
                'lft' => 475,
                'rght' => 486,
            ],
            [// id=239
                'parent_id' => 238, // root/controllers/BatchTreatments
                'model' => 'BatchTreatments',
                'foreign_key' => null,
                'alias' => 'add1',
                'lft' => 476,
                'rght' => 485,
            ],
            [// id=240
                'parent_id' => 239, // root/controllers/BatchTreatments/add1
                'model' => 'BatchTreatments',
                'foreign_key' => null,
                'alias' => 'add2',
                'lft' => 477,
                'rght' => 478,
            ],
            [// id=241
                'parent_id' => 239, // root/controllers/BatchTreatments/add1
                'model' => 'BatchTreatments',
                'foreign_key' => null,
                'alias' => 'add3',
                'lft' => 479,
                'rght' => 480,
            ],
            [// id=242
                'parent_id' => 239, // root/controllers/BatchTreatments/add1
                'model' => 'BatchTreatments',
                'foreign_key' => null,
                'alias' => 'paginateArchiveUnits',
                'lft' => 481,
                'rght' => 482,
            ],
            [// id=243
                'parent_id' => 239, // root/controllers/BatchTreatments/add1
                'model' => 'BatchTreatments',
                'foreign_key' => null,
                'alias' => 'removeArchiveUnit',
                'lft' => 483,
                'rght' => 484,
            ],
            [// id=244
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Commands',
                'lft' => 487,
                'rght' => 490,
            ],
            [// id=245
                'parent_id' => 244, // root/controllers/Commands
                'model' => 'Commands',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 488,
                'rght' => 489,
            ],
            [// id=246
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Counters',
                'lft' => 491,
                'rght' => 502,
            ],
            [// id=247
                'parent_id' => 246, // root/controllers/Counters
                'model' => 'Counters',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 492,
                'rght' => 493,
            ],
            [// id=248
                'parent_id' => 246, // root/controllers/Counters
                'model' => 'Counters',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 494,
                'rght' => 495,
            ],
            [// id=249
                'parent_id' => 246, // root/controllers/Counters
                'model' => 'Counters',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 496,
                'rght' => 497,
            ],
            [// id=250
                'parent_id' => 246, // root/controllers/Counters
                'model' => 'Counters',
                'foreign_key' => null,
                'alias' => 'initCounters',
                'lft' => 498,
                'rght' => 499,
            ],
            [// id=251
                'parent_id' => 246, // root/controllers/Counters
                'model' => 'Counters',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 500,
                'rght' => 501,
            ],
            [// id=252
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Deliveries',
                'lft' => 503,
                'rght' => 516,
            ],
            [// id=253
                'parent_id' => 254, // root/controllers/Deliveries/acquittal
                'model' => 'Deliveries',
                'foreign_key' => null,
                'alias' => 'acquit',
                'lft' => 505,
                'rght' => 506,
            ],
            [// id=254
                'parent_id' => 252, // root/controllers/Deliveries
                'model' => 'Deliveries',
                'foreign_key' => null,
                'alias' => 'acquittal',
                'lft' => 504,
                'rght' => 507,
            ],
            [// id=255
                'parent_id' => 252, // root/controllers/Deliveries
                'model' => 'Deliveries',
                'foreign_key' => null,
                'alias' => 'download',
                'lft' => 508,
                'rght' => 509,
            ],
            [// id=256
                'parent_id' => 258, // root/controllers/Deliveries/view
                'model' => 'Deliveries',
                'foreign_key' => null,
                'alias' => 'paginateArchiveUnits',
                'lft' => 513,
                'rght' => 514,
            ],
            [// id=257
                'parent_id' => 252, // root/controllers/Deliveries
                'model' => 'Deliveries',
                'foreign_key' => null,
                'alias' => 'retrieval',
                'lft' => 510,
                'rght' => 511,
            ],
            [// id=258
                'parent_id' => 252, // root/controllers/Deliveries
                'model' => 'Deliveries',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 512,
                'rght' => 515,
            ],
            [// id=259
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'DeliveryRequests',
                'lft' => 517,
                'rght' => 540,
            ],
            [// id=260
                'parent_id' => 259, // root/controllers/DeliveryRequests
                'model' => 'DeliveryRequests',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 518,
                'rght' => 519,
            ],
            [// id=261
                'parent_id' => 259, // root/controllers/DeliveryRequests
                'model' => 'DeliveryRequests',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 520,
                'rght' => 521,
            ],
            [// id=262
                'parent_id' => 259, // root/controllers/DeliveryRequests
                'model' => 'DeliveryRequests',
                'foreign_key' => null,
                'alias' => 'derogationNeeded',
                'lft' => 522,
                'rght' => 523,
            ],
            [// id=263
                'parent_id' => 270, // root/controllers/DeliveryRequests/view
                'model' => 'DeliveryRequests',
                'foreign_key' => null,
                'alias' => 'downloadPdf',
                'lft' => 535,
                'rght' => 536,
            ],
            [// id=264
                'parent_id' => 259, // root/controllers/DeliveryRequests
                'model' => 'DeliveryRequests',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 524,
                'rght' => 525,
            ],
            [// id=265
                'parent_id' => 259, // root/controllers/DeliveryRequests
                'model' => 'DeliveryRequests',
                'foreign_key' => null,
                'alias' => 'indexAll',
                'lft' => 526,
                'rght' => 527,
            ],
            [// id=266
                'parent_id' => 259, // root/controllers/DeliveryRequests
                'model' => 'DeliveryRequests',
                'foreign_key' => null,
                'alias' => 'indexMy',
                'lft' => 528,
                'rght' => 529,
            ],
            [// id=267
                'parent_id' => 259, // root/controllers/DeliveryRequests
                'model' => 'DeliveryRequests',
                'foreign_key' => null,
                'alias' => 'indexPreparating',
                'lft' => 530,
                'rght' => 531,
            ],
            [// id=268
                'parent_id' => 270, // root/controllers/DeliveryRequests/view
                'model' => 'DeliveryRequests',
                'foreign_key' => null,
                'alias' => 'paginateArchiveUnits',
                'lft' => 537,
                'rght' => 538,
            ],
            [// id=269
                'parent_id' => 259, // root/controllers/DeliveryRequests
                'model' => 'DeliveryRequests',
                'foreign_key' => null,
                'alias' => 'send',
                'lft' => 532,
                'rght' => 533,
            ],
            [// id=270
                'parent_id' => 259, // root/controllers/DeliveryRequests
                'model' => 'DeliveryRequests',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 534,
                'rght' => 539,
            ],
            [// id=271
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'DestructionNotifications',
                'lft' => 541,
                'rght' => 548,
            ],
            [// id=272
                'parent_id' => 271, // root/controllers/DestructionNotifications
                'model' => 'DestructionNotifications',
                'foreign_key' => null,
                'alias' => 'destructionCertificate',
                'lft' => 542,
                'rght' => 543,
            ],
            [// id=273
                'parent_id' => 271, // root/controllers/DestructionNotifications
                'model' => 'DestructionNotifications',
                'foreign_key' => null,
                'alias' => 'download',
                'lft' => 544,
                'rght' => 545,
            ],
            [// id=274
                'parent_id' => 271, // root/controllers/DestructionNotifications
                'model' => 'DestructionNotifications',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 546,
                'rght' => 547,
            ],
            [// id=275
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'DestructionRequests',
                'lft' => 549,
                'rght' => 572,
            ],
            [// id=276
                'parent_id' => 275, // root/controllers/DestructionRequests
                'model' => 'DestructionRequests',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 550,
                'rght' => 555,
            ],
            [// id=277
                'parent_id' => 275, // root/controllers/DestructionRequests
                'model' => 'DestructionRequests',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 556,
                'rght' => 557,
            ],
            [// id=278
                'parent_id' => 286, // root/controllers/DestructionRequests/view
                'model' => 'DestructionRequests',
                'foreign_key' => null,
                'alias' => 'downloadPdf',
                'lft' => 569,
                'rght' => 570,
            ],
            [// id=279
                'parent_id' => 275, // root/controllers/DestructionRequests
                'model' => 'DestructionRequests',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 558,
                'rght' => 559,
            ],
            [// id=280
                'parent_id' => 275, // root/controllers/DestructionRequests
                'model' => 'DestructionRequests',
                'foreign_key' => null,
                'alias' => 'indexAll',
                'lft' => 560,
                'rght' => 561,
            ],
            [// id=281
                'parent_id' => 275, // root/controllers/DestructionRequests
                'model' => 'DestructionRequests',
                'foreign_key' => null,
                'alias' => 'indexMy',
                'lft' => 562,
                'rght' => 563,
            ],
            [// id=282
                'parent_id' => 275, // root/controllers/DestructionRequests
                'model' => 'DestructionRequests',
                'foreign_key' => null,
                'alias' => 'indexPreparating',
                'lft' => 564,
                'rght' => 565,
            ],
            [// id=283
                'parent_id' => 276, // root/controllers/DestructionRequests/add
                'model' => 'DestructionRequests',
                'foreign_key' => null,
                'alias' => 'paginateArchiveUnits',
                'lft' => 551,
                'rght' => 552,
            ],
            [// id=284
                'parent_id' => 276, // root/controllers/DestructionRequests/add
                'model' => 'DestructionRequests',
                'foreign_key' => null,
                'alias' => 'removeArchiveUnit',
                'lft' => 553,
                'rght' => 554,
            ],
            [// id=285
                'parent_id' => 275, // root/controllers/DestructionRequests
                'model' => 'DestructionRequests',
                'foreign_key' => null,
                'alias' => 'send',
                'lft' => 566,
                'rght' => 567,
            ],
            [// id=286
                'parent_id' => 275, // root/controllers/DestructionRequests
                'model' => 'DestructionRequests',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 568,
                'rght' => 571,
            ],
            [// id=287
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Devs',
                'lft' => 573,
                'rght' => 590,
            ],
            [// id=288
                'parent_id' => 287, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'debugMail',
                'lft' => 574,
                'rght' => 575,
            ],
            [// id=289
                'parent_id' => 287, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'destructionCertificate',
                'lft' => 576,
                'rght' => 577,
            ],
            [// id=290
                'parent_id' => 287, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'icons',
                'lft' => 578,
                'rght' => 579,
            ],
            [// id=291
                'parent_id' => 287, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 580,
                'rght' => 581,
            ],
            [// id=292
                'parent_id' => 287, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'permissions',
                'lft' => 582,
                'rght' => 583,
            ],
            [// id=293
                'parent_id' => 287, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'restitutionCertificate',
                'lft' => 584,
                'rght' => 585,
            ],
            [// id=294
                'parent_id' => 287, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'restitutionDestructionCertificate',
                'lft' => 586,
                'rght' => 587,
            ],
            [// id=295
                'parent_id' => 287, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'setPermission',
                'lft' => 588,
                'rght' => 589,
            ],
            [// id=296
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Download',
                'lft' => 591,
                'rght' => 600,
            ],
            [// id=297
                'parent_id' => 296, // root/controllers/Download
                'model' => 'Download',
                'foreign_key' => null,
                'alias' => 'file',
                'lft' => 592,
                'rght' => 593,
            ],
            [// id=298
                'parent_id' => 296, // root/controllers/Download
                'model' => 'Download',
                'foreign_key' => null,
                'alias' => 'open',
                'lft' => 594,
                'rght' => 595,
            ],
            [// id=299
                'parent_id' => 296, // root/controllers/Download
                'model' => 'Download',
                'foreign_key' => null,
                'alias' => 'thumbnailImage',
                'lft' => 596,
                'rght' => 597,
            ],
            [// id=300
                'parent_id' => 296, // root/controllers/Download
                'model' => 'Download',
                'foreign_key' => null,
                'alias' => 'zip',
                'lft' => 598,
                'rght' => 599,
            ],
            [// id=301
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Eaccpfs',
                'lft' => 601,
                'rght' => 604,
            ],
            [// id=302
                'parent_id' => 375, // root/controllers/OrgEntities/add
                'model' => 'Eaccpfs',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 735,
                'rght' => 736,
            ],
            [// id=303
                'parent_id' => 375, // root/controllers/OrgEntities/add
                'model' => 'Eaccpfs',
                'foreign_key' => null,
                'alias' => 'addByUpload',
                'lft' => 737,
                'rght' => 738,
            ],
            [// id=304
                'parent_id' => 301, // root/controllers/Eaccpfs
                'model' => 'Eaccpfs',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 602,
                'rght' => 603,
            ],
            [// id=305
                'parent_id' => 377, // root/controllers/OrgEntities/delete
                'model' => 'Eaccpfs',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 743,
                'rght' => 744,
            ],
            [// id=306
                'parent_id' => 384, // root/controllers/OrgEntities/index
                'model' => 'Eaccpfs',
                'foreign_key' => null,
                'alias' => 'download',
                'lft' => 761,
                'rght' => 762,
            ],
            [// id=307
                'parent_id' => 379, // root/controllers/OrgEntities/edit
                'model' => 'Eaccpfs',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 747,
                'rght' => 748,
            ],
            [// id=308
                'parent_id' => 384, // root/controllers/OrgEntities/index
                'model' => 'Eaccpfs',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 763,
                'rght' => 764,
            ],
            [// id=309
                'parent_id' => 384, // root/controllers/OrgEntities/index
                'model' => 'Eaccpfs',
                'foreign_key' => null,
                'alias' => 'zip',
                'lft' => 765,
                'rght' => 766,
            ],
            [// id=310
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Fileuploads',
                'lft' => 605,
                'rght' => 608,
            ],
            [// id=311
                'parent_id' => 310, // root/controllers/Fileuploads
                'model' => 'Fileuploads',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 606,
                'rght' => 607,
            ],
            [// id=312
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Filters',
                'lft' => 609,
                'rght' => 620,
            ],
            [// id=313
                'parent_id' => 312, // root/controllers/Filters
                'model' => 'Filters',
                'foreign_key' => null,
                'alias' => 'ajaxDeleteSave',
                'lft' => 610,
                'rght' => 611,
            ],
            [// id=314
                'parent_id' => 312, // root/controllers/Filters
                'model' => 'Filters',
                'foreign_key' => null,
                'alias' => 'ajaxGetFilters',
                'lft' => 612,
                'rght' => 613,
            ],
            [// id=315
                'parent_id' => 312, // root/controllers/Filters
                'model' => 'Filters',
                'foreign_key' => null,
                'alias' => 'ajaxNewSave',
                'lft' => 614,
                'rght' => 615,
            ],
            [// id=316
                'parent_id' => 312, // root/controllers/Filters
                'model' => 'Filters',
                'foreign_key' => null,
                'alias' => 'ajaxOverwrite',
                'lft' => 616,
                'rght' => 617,
            ],
            [// id=317
                'parent_id' => 312, // root/controllers/Filters
                'model' => 'Filters',
                'foreign_key' => null,
                'alias' => 'ajaxRedirectUrl',
                'lft' => 618,
                'rght' => 619,
            ],
            [// id=318
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Home',
                'lft' => 621,
                'rght' => 624,
            ],
            [// id=319
                'parent_id' => 318, // root/controllers/Home
                'model' => 'Home',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 622,
                'rght' => 623,
            ],
            [// id=320
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Indicators',
                'lft' => 625,
                'rght' => 632,
            ],
            [// id=321
                'parent_id' => 323, // root/controllers/Indicators/indexTransferringAgency
                'model' => 'Indicators',
                'foreign_key' => null,
                'alias' => 'csv',
                'lft' => 627,
                'rght' => 628,
            ],
            [// id=322
                'parent_id' => 323, // root/controllers/Indicators/indexTransferringAgency
                'model' => 'Indicators',
                'foreign_key' => null,
                'alias' => 'indexOriginatingAgency',
                'lft' => 629,
                'rght' => 630,
            ],
            [// id=323
                'parent_id' => 320, // root/controllers/Indicators
                'model' => 'Indicators',
                'foreign_key' => null,
                'alias' => 'indexTransferringAgency',
                'lft' => 626,
                'rght' => 631,
            ],
            [// id=324
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Install',
                'lft' => 633,
                'rght' => 640,
            ],
            [// id=325
                'parent_id' => 324, // root/controllers/Install
                'model' => 'Install',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 634,
                'rght' => 635,
            ],
            [// id=326
                'parent_id' => 324, // root/controllers/Install
                'model' => 'Install',
                'foreign_key' => null,
                'alias' => 'sendTestMail',
                'lft' => 636,
                'rght' => 637,
            ],
            [// id=327
                'parent_id' => 324, // root/controllers/Install
                'model' => 'Install',
                'foreign_key' => null,
                'alias' => 'testTimestamper',
                'lft' => 638,
                'rght' => 639,
            ],
            [// id=328
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'KeywordLists',
                'lft' => 641,
                'rght' => 676,
            ],
            [// id=329
                'parent_id' => 328, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 642,
                'rght' => 643,
            ],
            [// id=330
                'parent_id' => 328, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 644,
                'rght' => 645,
            ],
            [// id=331
                'parent_id' => 328, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 646,
                'rght' => 647,
            ],
            [// id=332
                'parent_id' => 328, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 648,
                'rght' => 649,
            ],
            [// id=333
                'parent_id' => 328, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 650,
                'rght' => 653,
            ],
            [// id=334
                'parent_id' => 328, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'newVersion',
                'lft' => 654,
                'rght' => 671,
            ],
            [// id=335
                'parent_id' => 334, // root/controllers/KeywordLists/newVersion
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'publishVersion',
                'lft' => 655,
                'rght' => 656,
            ],
            [// id=336
                'parent_id' => 334, // root/controllers/KeywordLists/newVersion
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'removeVersion',
                'lft' => 657,
                'rght' => 658,
            ],
            [// id=337
                'parent_id' => 328, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 672,
                'rght' => 675,
            ],
            [// id=338
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Keywords',
                'lft' => 677,
                'rght' => 682,
            ],
            [// id=339
                'parent_id' => 334, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 659,
                'rght' => 660,
            ],
            [// id=340
                'parent_id' => 334, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'addMultiple',
                'lft' => 661,
                'rght' => 662,
            ],
            [// id=341
                'parent_id' => 338, // root/controllers/Keywords
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 678,
                'rght' => 679,
            ],
            [// id=342
                'parent_id' => 334, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 663,
                'rght' => 664,
            ],
            [// id=343
                'parent_id' => 334, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 665,
                'rght' => 666,
            ],
            [// id=344
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'getData',
                'lft' => 1033,
                'rght' => 1034,
            ],
            [// id=345
                'parent_id' => 334, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'getImportFileInfo',
                'lft' => 667,
                'rght' => 668,
            ],
            [// id=346
                'parent_id' => 334, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'import',
                'lft' => 669,
                'rght' => 670,
            ],
            [// id=347
                'parent_id' => 333, // root/controllers/KeywordLists/index
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 651,
                'rght' => 652,
            ],
            [// id=348
                'parent_id' => 338, // root/controllers/Keywords
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'populateSelect',
                'lft' => 680,
                'rght' => 681,
            ],
            [// id=349
                'parent_id' => 337, // root/controllers/KeywordLists/view
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 673,
                'rght' => 674,
            ],
            [// id=350
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Ldaps',
                'lft' => 683,
                'rght' => 714,
            ],
            [// id=351
                'parent_id' => 350, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 684,
                'rght' => 695,
            ],
            [// id=352
                'parent_id' => 350, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 696,
                'rght' => 697,
            ],
            [// id=353
                'parent_id' => 350, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 698,
                'rght' => 699,
            ],
            [// id=354
                'parent_id' => 351, // root/controllers/Ldaps/add
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'getCount',
                'lft' => 685,
                'rght' => 686,
            ],
            [// id=355
                'parent_id' => 351, // root/controllers/Ldaps/add
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'getFiltered',
                'lft' => 687,
                'rght' => 688,
            ],
            [// id=356
                'parent_id' => 351, // root/controllers/Ldaps/add
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'getRandomEntry',
                'lft' => 689,
                'rght' => 690,
            ],
            [// id=357
                'parent_id' => 350, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'importUsers',
                'lft' => 700,
                'rght' => 703,
            ],
            [// id=358
                'parent_id' => 350, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 704,
                'rght' => 705,
            ],
            [// id=359
                'parent_id' => 351, // root/controllers/Ldaps/add
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'listAvailableAttributes',
                'lft' => 691,
                'rght' => 692,
            ],
            [// id=360
                'parent_id' => 350, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'listLdapEntries',
                'lft' => 706,
                'rght' => 707,
            ],
            [// id=361
                'parent_id' => 351, // root/controllers/Ldaps/add
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'ping',
                'lft' => 693,
                'rght' => 694,
            ],
            [// id=362
                'parent_id' => 350, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'testConnection',
                'lft' => 708,
                'rght' => 709,
            ],
            [// id=363
                'parent_id' => 350, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'testLogin',
                'lft' => 710,
                'rght' => 711,
            ],
            [// id=364
                'parent_id' => 350, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'viewEntry',
                'lft' => 712,
                'rght' => 713,
            ],
            [// id=365
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Notifications',
                'lft' => 715,
                'rght' => 724,
            ],
            [// id=366
                'parent_id' => 365, // root/controllers/Notifications
                'model' => 'Notifications',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 716,
                'rght' => 717,
            ],
            [// id=367
                'parent_id' => 365, // root/controllers/Notifications
                'model' => 'Notifications',
                'foreign_key' => null,
                'alias' => 'apiPostAction',
                'lft' => 718,
                'rght' => 719,
            ],
            [// id=368
                'parent_id' => 365, // root/controllers/Notifications
                'model' => 'Notifications',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 720,
                'rght' => 721,
            ],
            [// id=369
                'parent_id' => 365, // root/controllers/Notifications
                'model' => 'Notifications',
                'foreign_key' => null,
                'alias' => 'test',
                'lft' => 722,
                'rght' => 723,
            ],
            [// id=370
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Oaipmh',
                'lft' => 725,
                'rght' => 732,
            ],
            [// id=371
                'parent_id' => 370, // root/controllers/Oaipmh
                'model' => 'Oaipmh',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 726,
                'rght' => 731,
            ],
            [// id=372
                'parent_id' => 371, // root/controllers/Oaipmh/api
                'model' => 'Oaipmh',
                'foreign_key' => null,
                'alias' => 'v1',
                'lft' => 727,
                'rght' => 728,
            ],
            [// id=373
                'parent_id' => 371, // root/controllers/Oaipmh/api
                'model' => 'Oaipmh',
                'foreign_key' => null,
                'alias' => 'v2',
                'lft' => 729,
                'rght' => 730,
            ],
            [// id=374
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'OrgEntities',
                'lft' => 733,
                'rght' => 768,
            ],
            [// id=375
                'parent_id' => 374, // root/controllers/OrgEntities
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 734,
                'rght' => 739,
            ],
            [// id=376
                'parent_id' => 374, // root/controllers/OrgEntities
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 740,
                'rght' => 741,
            ],
            [// id=377
                'parent_id' => 374, // root/controllers/OrgEntities
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 742,
                'rght' => 745,
            ],
            [// id=378
                'parent_id' => 379, // root/controllers/OrgEntities/edit
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'deleteBackground',
                'lft' => 749,
                'rght' => 750,
            ],
            [// id=379
                'parent_id' => 374, // root/controllers/OrgEntities
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 746,
                'rght' => 759,
            ],
            [// id=380
                'parent_id' => 379, // root/controllers/OrgEntities/edit
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'editConfig',
                'lft' => 751,
                'rght' => 752,
            ],
            [// id=381
                'parent_id' => 379, // root/controllers/OrgEntities/edit
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'editConversions',
                'lft' => 753,
                'rght' => 754,
            ],
            [// id=382
                'parent_id' => 379, // root/controllers/OrgEntities/edit
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'editParentId',
                'lft' => 755,
                'rght' => 756,
            ],
            [// id=383
                'parent_id' => 357, // root/controllers/Ldaps/importUsers
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'getAvailablesRoles',
                'lft' => 701,
                'rght' => 702,
            ],
            [// id=384
                'parent_id' => 374, // root/controllers/OrgEntities
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 760,
                'rght' => 767,
            ],
            [// id=385
                'parent_id' => 379, // root/controllers/OrgEntities/edit
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'paginateUsers',
                'lft' => 757,
                'rght' => 758,
            ],
            [// id=386
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'OutgoingTransferRequests',
                'lft' => 769,
                'rght' => 806,
            ],
            [// id=387
                'parent_id' => 386, // root/controllers/OutgoingTransferRequests
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'add1',
                'lft' => 770,
                'rght' => 777,
            ],
            [// id=388
                'parent_id' => 387, // root/controllers/OutgoingTransferRequests/add1
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'add2',
                'lft' => 771,
                'rght' => 772,
            ],
            [// id=389
                'parent_id' => 387, // root/controllers/OutgoingTransferRequests/add1
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'add3',
                'lft' => 773,
                'rght' => 774,
            ],
            [// id=390
                'parent_id' => 386, // root/controllers/OutgoingTransferRequests
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 778,
                'rght' => 779,
            ],
            [// id=391
                'parent_id' => 397, // root/controllers/OutgoingTransferRequests/indexProcessed
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'cancelArchiveDestruction',
                'lft' => 791,
                'rght' => 792,
            ],
            [// id=392
                'parent_id' => 386, // root/controllers/OutgoingTransferRequests
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 780,
                'rght' => 781,
            ],
            [// id=393
                'parent_id' => 386, // root/controllers/OutgoingTransferRequests
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 782,
                'rght' => 783,
            ],
            [// id=394
                'parent_id' => 386, // root/controllers/OutgoingTransferRequests
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'indexAll',
                'lft' => 784,
                'rght' => 785,
            ],
            [// id=395
                'parent_id' => 386, // root/controllers/OutgoingTransferRequests
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'indexMy',
                'lft' => 786,
                'rght' => 787,
            ],
            [// id=396
                'parent_id' => 386, // root/controllers/OutgoingTransferRequests
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'indexPreparating',
                'lft' => 788,
                'rght' => 789,
            ],
            [// id=397
                'parent_id' => 386, // root/controllers/OutgoingTransferRequests
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'indexProcessed',
                'lft' => 790,
                'rght' => 795,
            ],
            [// id=398
                'parent_id' => 403, // root/controllers/OutgoingTransferRequests/view
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'paginateArchiveUnits',
                'lft' => 803,
                'rght' => 804,
            ],
            [// id=399
                'parent_id' => 387, // root/controllers/OutgoingTransferRequests/add1
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'removeArchiveUnit',
                'lft' => 775,
                'rght' => 776,
            ],
            [// id=400
                'parent_id' => 402, // root/controllers/OutgoingTransferRequests/send
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'resend',
                'lft' => 797,
                'rght' => 798,
            ],
            [// id=401
                'parent_id' => 397, // root/controllers/OutgoingTransferRequests/indexProcessed
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'scheduleArchiveDestruction',
                'lft' => 793,
                'rght' => 794,
            ],
            [// id=402
                'parent_id' => 386, // root/controllers/OutgoingTransferRequests
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'send',
                'lft' => 796,
                'rght' => 801,
            ],
            [// id=403
                'parent_id' => 386, // root/controllers/OutgoingTransferRequests
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 802,
                'rght' => 805,
            ],
            [// id=404
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'OutgoingTransfers',
                'lft' => 807,
                'rght' => 820,
            ],
            [// id=405
                'parent_id' => 404, // root/controllers/OutgoingTransfers
                'model' => 'OutgoingTransfers',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 808,
                'rght' => 809,
            ],
            [// id=406
                'parent_id' => 402, // root/controllers/OutgoingTransferRequests/send
                'model' => 'OutgoingTransfers',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 799,
                'rght' => 800,
            ],
            [// id=407
                'parent_id' => 404, // root/controllers/OutgoingTransfers
                'model' => 'OutgoingTransfers',
                'foreign_key' => null,
                'alias' => 'indexAccepted',
                'lft' => 810,
                'rght' => 811,
            ],
            [// id=408
                'parent_id' => 404, // root/controllers/OutgoingTransfers
                'model' => 'OutgoingTransfers',
                'foreign_key' => null,
                'alias' => 'indexAll',
                'lft' => 812,
                'rght' => 813,
            ],
            [// id=409
                'parent_id' => 404, // root/controllers/OutgoingTransfers
                'model' => 'OutgoingTransfers',
                'foreign_key' => null,
                'alias' => 'indexPreparating',
                'lft' => 814,
                'rght' => 815,
            ],
            [// id=410
                'parent_id' => 404, // root/controllers/OutgoingTransfers
                'model' => 'OutgoingTransfers',
                'foreign_key' => null,
                'alias' => 'indexRejected',
                'lft' => 816,
                'rght' => 817,
            ],
            [// id=411
                'parent_id' => 404, // root/controllers/OutgoingTransfers
                'model' => 'OutgoingTransfers',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 818,
                'rght' => 819,
            ],
            [// id=412
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Permissions',
                'lft' => 821,
                'rght' => 822,
            ],
            [// id=413
                'parent_id' => 458, // root/controllers/Roles/index
                'model' => 'Permissions',
                'foreign_key' => null,
                'alias' => 'ajaxGetPermission',
                'lft' => 905,
                'rght' => 906,
            ],
            [// id=414
                'parent_id' => 458, // root/controllers/Roles/index
                'model' => 'Permissions',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 907,
                'rght' => 908,
            ],
            [// id=415
                'parent_id' => 458, // root/controllers/Roles/index
                'model' => 'Permissions',
                'foreign_key' => null,
                'alias' => 'editWebservice',
                'lft' => 909,
                'rght' => 910,
            ],
            [// id=416
                'parent_id' => 458, // root/controllers/Roles/index
                'model' => 'Permissions',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 911,
                'rght' => 912,
            ],
            [// id=417
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Profiles',
                'lft' => 823,
                'rght' => 836,
            ],
            [// id=418
                'parent_id' => 417, // root/controllers/Profiles
                'model' => 'Profiles',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 824,
                'rght' => 825,
            ],
            [// id=419
                'parent_id' => 417, // root/controllers/Profiles
                'model' => 'Profiles',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 826,
                'rght' => 827,
            ],
            [// id=420
                'parent_id' => 417, // root/controllers/Profiles
                'model' => 'Profiles',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 828,
                'rght' => 829,
            ],
            [// id=421
                'parent_id' => 417, // root/controllers/Profiles
                'model' => 'Profiles',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 830,
                'rght' => 831,
            ],
            [// id=422
                'parent_id' => 417, // root/controllers/Profiles
                'model' => 'Profiles',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 832,
                'rght' => 833,
            ],
            [// id=423
                'parent_id' => 417, // root/controllers/Profiles
                'model' => 'Profiles',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 834,
                'rght' => 835,
            ],
            [// id=424
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'RestitutionRequests',
                'lft' => 837,
                'rght' => 868,
            ],
            [// id=425
                'parent_id' => 424, // root/controllers/RestitutionRequests
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 838,
                'rght' => 843,
            ],
            [// id=426
                'parent_id' => 430, // root/controllers/RestitutionRequests/indexAcquitted
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'cancelArchiveDestruction',
                'lft' => 849,
                'rght' => 850,
            ],
            [// id=427
                'parent_id' => 424, // root/controllers/RestitutionRequests
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 844,
                'rght' => 845,
            ],
            [// id=428
                'parent_id' => 436, // root/controllers/RestitutionRequests/restitutionCertificate
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'destructionCertificate',
                'lft' => 861,
                'rght' => 862,
            ],
            [// id=429
                'parent_id' => 424, // root/controllers/RestitutionRequests
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 846,
                'rght' => 847,
            ],
            [// id=430
                'parent_id' => 424, // root/controllers/RestitutionRequests
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'indexAcquitted',
                'lft' => 848,
                'rght' => 853,
            ],
            [// id=431
                'parent_id' => 424, // root/controllers/RestitutionRequests
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'indexAll',
                'lft' => 854,
                'rght' => 855,
            ],
            [// id=432
                'parent_id' => 424, // root/controllers/RestitutionRequests
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'indexMy',
                'lft' => 856,
                'rght' => 857,
            ],
            [// id=433
                'parent_id' => 424, // root/controllers/RestitutionRequests
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'indexPreparating',
                'lft' => 858,
                'rght' => 859,
            ],
            [// id=434
                'parent_id' => 425, // root/controllers/RestitutionRequests/add
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'paginateArchiveUnits',
                'lft' => 839,
                'rght' => 840,
            ],
            [// id=435
                'parent_id' => 425, // root/controllers/RestitutionRequests/add
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'removeArchiveUnit',
                'lft' => 841,
                'rght' => 842,
            ],
            [// id=436
                'parent_id' => 424, // root/controllers/RestitutionRequests
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'restitutionCertificate',
                'lft' => 860,
                'rght' => 863,
            ],
            [// id=437
                'parent_id' => 430, // root/controllers/RestitutionRequests/indexAcquitted
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'scheduleArchiveDestruction',
                'lft' => 851,
                'rght' => 852,
            ],
            [// id=438
                'parent_id' => 424, // root/controllers/RestitutionRequests
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'send',
                'lft' => 864,
                'rght' => 865,
            ],
            [// id=439
                'parent_id' => 424, // root/controllers/RestitutionRequests
                'model' => 'RestitutionRequests',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 866,
                'rght' => 867,
            ],
            [// id=440
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Restitutions',
                'lft' => 869,
                'rght' => 882,
            ],
            [// id=441
                'parent_id' => 442, // root/controllers/Restitutions/acquittal
                'model' => 'Restitutions',
                'foreign_key' => null,
                'alias' => 'acquit',
                'lft' => 871,
                'rght' => 872,
            ],
            [// id=442
                'parent_id' => 440, // root/controllers/Restitutions
                'model' => 'Restitutions',
                'foreign_key' => null,
                'alias' => 'acquittal',
                'lft' => 870,
                'rght' => 873,
            ],
            [// id=443
                'parent_id' => 440, // root/controllers/Restitutions
                'model' => 'Restitutions',
                'foreign_key' => null,
                'alias' => 'download',
                'lft' => 874,
                'rght' => 877,
            ],
            [// id=444
                'parent_id' => 443, // root/controllers/Restitutions/download
                'model' => 'Restitutions',
                'foreign_key' => null,
                'alias' => 'getInfo',
                'lft' => 875,
                'rght' => 876,
            ],
            [// id=445
                'parent_id' => 440, // root/controllers/Restitutions
                'model' => 'Restitutions',
                'foreign_key' => null,
                'alias' => 'retrieval',
                'lft' => 878,
                'rght' => 879,
            ],
            [// id=446
                'parent_id' => 440, // root/controllers/Restitutions
                'model' => 'Restitutions',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 880,
                'rght' => 881,
            ],
            [// id=447
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Restservices',
                'lft' => 883,
                'rght' => 894,
            ],
            [// id=448
                'parent_id' => 447, // root/controllers/Restservices
                'model' => 'Restservices',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 884,
                'rght' => 885,
            ],
            [// id=449
                'parent_id' => 447, // root/controllers/Restservices
                'model' => 'Restservices',
                'foreign_key' => null,
                'alias' => 'ping',
                'lft' => 886,
                'rght' => 887,
            ],
            [// id=450
                'parent_id' => 451, // root/controllers/Restservices/sedaMessages
                'model' => 'Restservices',
                'foreign_key' => null,
                'alias' => 'sedaAttachmentsChunkFiles',
                'lft' => 889,
                'rght' => 890,
            ],
            [// id=451
                'parent_id' => 447, // root/controllers/Restservices
                'model' => 'Restservices',
                'foreign_key' => null,
                'alias' => 'sedaMessages',
                'lft' => 888,
                'rght' => 891,
            ],
            [// id=452
                'parent_id' => 447, // root/controllers/Restservices
                'model' => 'Restservices',
                'foreign_key' => null,
                'alias' => 'versions',
                'lft' => 892,
                'rght' => 893,
            ],
            [// id=453
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Roles',
                'lft' => 895,
                'rght' => 920,
            ],
            [// id=454
                'parent_id' => 453, // root/controllers/Roles
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 896,
                'rght' => 897,
            ],
            [// id=455
                'parent_id' => 453, // root/controllers/Roles
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 898,
                'rght' => 899,
            ],
            [// id=456
                'parent_id' => 453, // root/controllers/Roles
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 900,
                'rght' => 901,
            ],
            [// id=457
                'parent_id' => 453, // root/controllers/Roles
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 902,
                'rght' => 903,
            ],
            [// id=458
                'parent_id' => 453, // root/controllers/Roles
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 904,
                'rght' => 917,
            ],
            [// id=459
                'parent_id' => 453, // root/controllers/Roles
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 918,
                'rght' => 919,
            ],
            [// id=460
                'parent_id' => 458, // root/controllers/Roles/index
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'viewGlobal',
                'lft' => 913,
                'rght' => 914,
            ],
            [// id=461
                'parent_id' => 458, // root/controllers/Roles/index
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'viewWebservice',
                'lft' => 915,
                'rght' => 916,
            ],
            [// id=462
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'SecureDataSpaces',
                'lft' => 921,
                'rght' => 924,
            ],
            [// id=463
                'parent_id' => 462, // root/controllers/SecureDataSpaces
                'model' => 'SecureDataSpaces',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 922,
                'rght' => 923,
            ],
            [// id=464
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'ServiceLevels',
                'lft' => 925,
                'rght' => 942,
            ],
            [// id=465
                'parent_id' => 464, // root/controllers/ServiceLevels
                'model' => 'ServiceLevels',
                'foreign_key' => null,
                'alias' => 'add1',
                'lft' => 926,
                'rght' => 931,
            ],
            [// id=466
                'parent_id' => 465, // root/controllers/ServiceLevels/add1
                'model' => 'ServiceLevels',
                'foreign_key' => null,
                'alias' => 'add2',
                'lft' => 927,
                'rght' => 928,
            ],
            [// id=467
                'parent_id' => 465, // root/controllers/ServiceLevels/add1
                'model' => 'ServiceLevels',
                'foreign_key' => null,
                'alias' => 'add3',
                'lft' => 929,
                'rght' => 930,
            ],
            [// id=468
                'parent_id' => 464, // root/controllers/ServiceLevels
                'model' => 'ServiceLevels',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 932,
                'rght' => 933,
            ],
            [// id=469
                'parent_id' => 464, // root/controllers/ServiceLevels
                'model' => 'ServiceLevels',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 934,
                'rght' => 935,
            ],
            [// id=470
                'parent_id' => 464, // root/controllers/ServiceLevels
                'model' => 'ServiceLevels',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 936,
                'rght' => 937,
            ],
            [// id=471
                'parent_id' => 464, // root/controllers/ServiceLevels
                'model' => 'ServiceLevels',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 938,
                'rght' => 939,
            ],
            [// id=472
                'parent_id' => 464, // root/controllers/ServiceLevels
                'model' => 'ServiceLevels',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 940,
                'rght' => 941,
            ],
            [// id=473
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Tasks',
                'lft' => 943,
                'rght' => 972,
            ],
            [// id=474
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 944,
                'rght' => 945,
            ],
            [// id=475
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'admin',
                'lft' => 946,
                'rght' => 947,
            ],
            [// id=476
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'adminJobInfo',
                'lft' => 948,
                'rght' => 949,
            ],
            [// id=477
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'ajaxCancel',
                'lft' => 950,
                'rght' => 951,
            ],
            [// id=478
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'ajaxPause',
                'lft' => 952,
                'rght' => 953,
            ],
            [// id=479
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'ajaxResume',
                'lft' => 954,
                'rght' => 955,
            ],
            [// id=480
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'cancel',
                'lft' => 956,
                'rght' => 957,
            ],
            [// id=481
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'deleteTube',
                'lft' => 958,
                'rght' => 959,
            ],
            [// id=482
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 960,
                'rght' => 961,
            ],
            [// id=483
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'getTubesInfo',
                'lft' => 962,
                'rght' => 963,
            ],
            [// id=484
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 964,
                'rght' => 965,
            ],
            [// id=485
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'myJobInfo',
                'lft' => 966,
                'rght' => 967,
            ],
            [// id=486
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'pause',
                'lft' => 968,
                'rght' => 969,
            ],
            [// id=487
                'parent_id' => 473, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'resume',
                'lft' => 970,
                'rght' => 971,
            ],
            [// id=488
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'TechnicalArchives',
                'lft' => 973,
                'rght' => 990,
            ],
            [// id=489
                'parent_id' => 488, // root/controllers/TechnicalArchives
                'model' => 'TechnicalArchives',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 974,
                'rght' => 975,
            ],
            [// id=490
                'parent_id' => 488, // root/controllers/TechnicalArchives
                'model' => 'TechnicalArchives',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 976,
                'rght' => 977,
            ],
            [// id=491
                'parent_id' => 493, // root/controllers/TechnicalArchives/edit
                'model' => 'TechnicalArchives',
                'foreign_key' => null,
                'alias' => 'deleteFile',
                'lft' => 979,
                'rght' => 980,
            ],
            [// id=492
                'parent_id' => 496, // root/controllers/TechnicalArchives/view
                'model' => 'TechnicalArchives',
                'foreign_key' => null,
                'alias' => 'downloadFile',
                'lft' => 985,
                'rght' => 986,
            ],
            [// id=493
                'parent_id' => 488, // root/controllers/TechnicalArchives
                'model' => 'TechnicalArchives',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 978,
                'rght' => 981,
            ],
            [// id=494
                'parent_id' => 488, // root/controllers/TechnicalArchives
                'model' => 'TechnicalArchives',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 982,
                'rght' => 983,
            ],
            [// id=495
                'parent_id' => 496, // root/controllers/TechnicalArchives/view
                'model' => 'TechnicalArchives',
                'foreign_key' => null,
                'alias' => 'paginateFiles',
                'lft' => 987,
                'rght' => 988,
            ],
            [// id=496
                'parent_id' => 488, // root/controllers/TechnicalArchives
                'model' => 'TechnicalArchives',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 984,
                'rght' => 989,
            ],
            [// id=497
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'TestError',
                'lft' => 991,
                'rght' => 996,
            ],
            [// id=498
                'parent_id' => 497, // root/controllers/TestError
                'model' => 'TestError',
                'foreign_key' => null,
                'alias' => 'code',
                'lft' => 992,
                'rght' => 993,
            ],
            [// id=499
                'parent_id' => 497, // root/controllers/TestError
                'model' => 'TestError',
                'foreign_key' => null,
                'alias' => 'throwException',
                'lft' => 994,
                'rght' => 995,
            ],
            [// id=500
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Timestampers',
                'lft' => 997,
                'rght' => 1000,
            ],
            [// id=501
                'parent_id' => 500, // root/controllers/Timestampers
                'model' => 'Timestampers',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 998,
                'rght' => 999,
            ],
            [// id=502
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'TransferAttachments',
                'lft' => 1001,
                'rght' => 1002,
            ],
            [// id=503
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'TransferAttachments',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 1035,
                'rght' => 1036,
            ],
            [// id=504
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'TransferAttachments',
                'foreign_key' => null,
                'alias' => 'download',
                'lft' => 1037,
                'rght' => 1038,
            ],
            [// id=505
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'TransferAttachments',
                'foreign_key' => null,
                'alias' => 'downloadByName',
                'lft' => 1039,
                'rght' => 1040,
            ],
            [// id=506
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'TransferAttachments',
                'foreign_key' => null,
                'alias' => 'getFileInfo',
                'lft' => 1041,
                'rght' => 1042,
            ],
            [// id=507
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'TransferAttachments',
                'foreign_key' => null,
                'alias' => 'open',
                'lft' => 1043,
                'rght' => 1044,
            ],
            [// id=508
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'TransferAttachments',
                'foreign_key' => null,
                'alias' => 'populateSelect',
                'lft' => 1045,
                'rght' => 1046,
            ],
            [// id=509
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Transfers',
                'lft' => 1003,
                'rght' => 1124,
            ],
            [// id=510
                'parent_id' => 517, // root/controllers/Transfers/analyse
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'abortAnalyse',
                'lft' => 1023,
                'rght' => 1024,
            ],
            [// id=511
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'add1',
                'lft' => 1004,
                'rght' => 1021,
            ],
            [// id=512
                'parent_id' => 511, // root/controllers/Transfers/add1
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'add2',
                'lft' => 1005,
                'rght' => 1006,
            ],
            [// id=513
                'parent_id' => 511, // root/controllers/Transfers/add1
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'addByUpload',
                'lft' => 1007,
                'rght' => 1008,
            ],
            [// id=514
                'parent_id' => 511, // root/controllers/Transfers/add1
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'addByUpload2',
                'lft' => 1009,
                'rght' => 1010,
            ],
            [// id=515
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'addCompressed',
                'lft' => 1047,
                'rght' => 1048,
            ],
            [// id=516
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'addMultipleKeywords',
                'lft' => 1049,
                'rght' => 1050,
            ],
            [// id=517
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'analyse',
                'lft' => 1022,
                'rght' => 1025,
            ],
            [// id=518
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 1026,
                'rght' => 1027,
            ],
            [// id=519
                'parent_id' => 511, // root/controllers/Transfers/add1
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'createFromExisting',
                'lft' => 1011,
                'rght' => 1012,
            ],
            [// id=520
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 1028,
                'rght' => 1031,
            ],
            [// id=521
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'deleteAttachment',
                'lft' => 1051,
                'rght' => 1052,
            ],
            [// id=522
                'parent_id' => 559, // root/controllers/Transfers/view
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'displayXml',
                'lft' => 1101,
                'rght' => 1102,
            ],
            [// id=523
                'parent_id' => 559, // root/controllers/Transfers/view
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'download',
                'lft' => 1103,
                'rght' => 1104,
            ],
            [// id=524
                'parent_id' => 559, // root/controllers/Transfers/view
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'downloadFiles',
                'lft' => 1105,
                'rght' => 1106,
            ],
            [// id=525
                'parent_id' => 559, // root/controllers/Transfers/view
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'downloadPdf',
                'lft' => 1107,
                'rght' => 1108,
            ],
            [// id=526
                'parent_id' => 511, // root/controllers/Transfers/add1
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'duplicate',
                'lft' => 1013,
                'rght' => 1014,
            ],
            [// id=527
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 1032,
                'rght' => 1077,
            ],
            [// id=528
                'parent_id' => 559, // root/controllers/Transfers/view
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'explore',
                'lft' => 1109,
                'rght' => 1110,
            ],
            [// id=529
                'parent_id' => 511, // root/controllers/Transfers/add1
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'generateIdentifier',
                'lft' => 1015,
                'rght' => 1016,
            ],
            [// id=530
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'getEditMeta',
                'lft' => 1053,
                'rght' => 1054,
            ],
            [// id=531
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'getJstree',
                'lft' => 1055,
                'rght' => 1056,
            ],
            [// id=532
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'getTree',
                'lft' => 1057,
                'rght' => 1058,
            ],
            [// id=533
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'getXpath',
                'lft' => 1059,
                'rght' => 1060,
            ],
            [// id=534
                'parent_id' => 520, // root/controllers/Transfers/delete
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'hide',
                'lft' => 1029,
                'rght' => 1030,
            ],
            [// id=535
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 1078,
                'rght' => 1079,
            ],
            [// id=536
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'indexAccepted',
                'lft' => 1080,
                'rght' => 1081,
            ],
            [// id=537
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'indexAll',
                'lft' => 1082,
                'rght' => 1083,
            ],
            [// id=538
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'indexMy',
                'lft' => 1084,
                'rght' => 1085,
            ],
            [// id=539
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'indexMyAccepted',
                'lft' => 1086,
                'rght' => 1087,
            ],
            [// id=540
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'indexMyEntity',
                'lft' => 1088,
                'rght' => 1089,
            ],
            [// id=541
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'indexMyRejected',
                'lft' => 1090,
                'rght' => 1091,
            ],
            [// id=542
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'indexPreparating',
                'lft' => 1092,
                'rght' => 1093,
            ],
            [// id=543
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'indexRejected',
                'lft' => 1094,
                'rght' => 1095,
            ],
            [// id=544
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'indexSent',
                'lft' => 1096,
                'rght' => 1097,
            ],
            [// id=545
                'parent_id' => 559, // root/controllers/Transfers/view
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'makePdf',
                'lft' => 1111,
                'rght' => 1112,
            ],
            [// id=546
                'parent_id' => 559, // root/controllers/Transfers/view
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'makeZip',
                'lft' => 1113,
                'rght' => 1114,
            ],
            [// id=547
                'parent_id' => 559, // root/controllers/Transfers/view
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'message',
                'lft' => 1115,
                'rght' => 1116,
            ],
            [// id=548
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'moveNode',
                'lft' => 1061,
                'rght' => 1062,
            ],
            [// id=549
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'partialAdd',
                'lft' => 1063,
                'rght' => 1064,
            ],
            [// id=550
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'partialDelete',
                'lft' => 1065,
                'rght' => 1066,
            ],
            [// id=551
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'partialEdit',
                'lft' => 1067,
                'rght' => 1068,
            ],
            [// id=552
                'parent_id' => 559, // root/controllers/Transfers/view
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'partialExplore',
                'lft' => 1117,
                'rght' => 1118,
            ],
            [// id=553
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'populateGroupSelect',
                'lft' => 1069,
                'rght' => 1070,
            ],
            [// id=554
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'populateSelect',
                'lft' => 1071,
                'rght' => 1072,
            ],
            [// id=555
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'reEdit',
                'lft' => 1073,
                'rght' => 1074,
            ],
            [// id=556
                'parent_id' => 511, // root/controllers/Transfers/add1
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'repair',
                'lft' => 1017,
                'rght' => 1018,
            ],
            [// id=557
                'parent_id' => 527, // root/controllers/Transfers/edit
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'searchInEdit',
                'lft' => 1075,
                'rght' => 1076,
            ],
            [// id=558
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'send',
                'lft' => 1098,
                'rght' => 1099,
            ],
            [// id=559
                'parent_id' => 509, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 1100,
                'rght' => 1123,
            ],
            [// id=560
                'parent_id' => 559, // root/controllers/Transfers/view
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'viewAttachments',
                'lft' => 1119,
                'rght' => 1120,
            ],
            [// id=561
                'parent_id' => 559, // root/controllers/Transfers/view
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'viewErrors',
                'lft' => 1121,
                'rght' => 1122,
            ],
            [// id=562
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'TypeEntities',
                'lft' => 1125,
                'rght' => 1128,
            ],
            [// id=563
                'parent_id' => 562, // root/controllers/TypeEntities
                'model' => 'TypeEntities',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 1126,
                'rght' => 1127,
            ],
            [// id=564
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Upload',
                'lft' => 1129,
                'rght' => 1138,
            ],
            [// id=565
                'parent_id' => 511, // root/controllers/Transfers/add1
                'model' => 'Upload',
                'foreign_key' => null,
                'alias' => 'addTransferAttachment',
                'lft' => 1019,
                'rght' => 1020,
            ],
            [// id=566
                'parent_id' => 564, // root/controllers/Upload
                'model' => 'Upload',
                'foreign_key' => null,
                'alias' => 'archivingSystemCert',
                'lft' => 1130,
                'rght' => 1131,
            ],
            [// id=567
                'parent_id' => 564, // root/controllers/Upload
                'model' => 'Upload',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 1132,
                'rght' => 1133,
            ],
            [// id=568
                'parent_id' => 564, // root/controllers/Upload
                'model' => 'Upload',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 1134,
                'rght' => 1135,
            ],
            [// id=569
                'parent_id' => 564, // root/controllers/Upload
                'model' => 'Upload',
                'foreign_key' => null,
                'alias' => 'minioCert',
                'lft' => 1136,
                'rght' => 1137,
            ],
            [// id=570
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Users',
                'lft' => 1139,
                'rght' => 1194,
            ],
            [// id=571
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'acceptUser',
                'lft' => 1140,
                'rght' => 1143,
            ],
            [// id=572
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 1144,
                'rght' => 1153,
            ],
            [// id=573
                'parent_id' => 572, // root/controllers/Users/add
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'addWebservice',
                'lft' => 1145,
                'rght' => 1146,
            ],
            [// id=574
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'ajaxLogin',
                'lft' => 1154,
                'rght' => 1155,
            ],
            [// id=575
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'ajaxSaveMenu',
                'lft' => 1156,
                'rght' => 1157,
            ],
            [// id=576
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 1158,
                'rght' => 1159,
            ],
            [// id=577
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'changeEntity',
                'lft' => 1160,
                'rght' => 1161,
            ],
            [// id=578
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'changeLoggedArchivalAgency',
                'lft' => 1162,
                'rght' => 1163,
            ],
            [// id=579
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 1164,
                'rght' => 1167,
            ],
            [// id=580
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 1168,
                'rght' => 1169,
            ],
            [// id=581
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'editByAdmin',
                'lft' => 1170,
                'rght' => 1173,
            ],
            [// id=582
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'forgottenPassword',
                'lft' => 1174,
                'rght' => 1175,
            ],
            [// id=583
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'forgottenPasswordMail',
                'lft' => 1176,
                'rght' => 1177,
            ],
            [// id=584
                'parent_id' => 572, // root/controllers/Users/add
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'getRolesOptions',
                'lft' => 1147,
                'rght' => 1148,
            ],
            [// id=585
                'parent_id' => 572, // root/controllers/Users/add
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'getWsRolesOptions',
                'lft' => 1149,
                'rght' => 1150,
            ],
            [// id=586
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 1178,
                'rght' => 1179,
            ],
            [// id=587
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'initializePassword',
                'lft' => 1180,
                'rght' => 1181,
            ],
            [// id=588
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'login',
                'lft' => 1182,
                'rght' => 1183,
            ],
            [// id=589
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'logout',
                'lft' => 1184,
                'rght' => 1185,
            ],
            [// id=590
                'parent_id' => 571, // root/controllers/Users/acceptUser
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'refuseUser',
                'lft' => 1141,
                'rght' => 1142,
            ],
            [// id=591
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'resetSession',
                'lft' => 1186,
                'rght' => 1187,
            ],
            [// id=592
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'unsubscribe',
                'lft' => 1188,
                'rght' => 1189,
            ],
            [// id=593
                'parent_id' => 570, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 1190,
                'rght' => 1193,
            ],
            [// id=594
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'ValidationActors',
                'lft' => 1195,
                'rght' => 1198,
            ],
            [// id=595
                'parent_id' => 601, // root/controllers/ValidationChains/add
                'model' => 'ValidationActors',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 1201,
                'rght' => 1202,
            ],
            [// id=596
                'parent_id' => 594, // root/controllers/ValidationActors
                'model' => 'ValidationActors',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 1196,
                'rght' => 1197,
            ],
            [// id=597
                'parent_id' => 603, // root/controllers/ValidationChains/delete
                'model' => 'ValidationActors',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 1209,
                'rght' => 1210,
            ],
            [// id=598
                'parent_id' => 604, // root/controllers/ValidationChains/edit
                'model' => 'ValidationActors',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 1215,
                'rght' => 1216,
            ],
            [// id=599
                'parent_id' => 605, // root/controllers/ValidationChains/index
                'model' => 'ValidationActors',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 1227,
                'rght' => 1228,
            ],
            [// id=600
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'ValidationChains',
                'lft' => 1199,
                'rght' => 1234,
            ],
            [// id=601
                'parent_id' => 600, // root/controllers/ValidationChains
                'model' => 'ValidationChains',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 1200,
                'rght' => 1205,
            ],
            [// id=602
                'parent_id' => 600, // root/controllers/ValidationChains
                'model' => 'ValidationChains',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 1206,
                'rght' => 1207,
            ],
            [// id=603
                'parent_id' => 600, // root/controllers/ValidationChains
                'model' => 'ValidationChains',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 1208,
                'rght' => 1213,
            ],
            [// id=604
                'parent_id' => 600, // root/controllers/ValidationChains
                'model' => 'ValidationChains',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 1214,
                'rght' => 1225,
            ],
            [// id=605
                'parent_id' => 600, // root/controllers/ValidationChains
                'model' => 'ValidationChains',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 1226,
                'rght' => 1231,
            ],
            [// id=606
                'parent_id' => 604, // root/controllers/ValidationChains/edit
                'model' => 'ValidationChains',
                'foreign_key' => null,
                'alias' => 'resendMail',
                'lft' => 1217,
                'rght' => 1218,
            ],
            [// id=607
                'parent_id' => 604, // root/controllers/ValidationChains/edit
                'model' => 'ValidationChains',
                'foreign_key' => null,
                'alias' => 'setDefault',
                'lft' => 1219,
                'rght' => 1220,
            ],
            [// id=608
                'parent_id' => 600, // root/controllers/ValidationChains
                'model' => 'ValidationChains',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 1232,
                'rght' => 1233,
            ],
            [// id=609
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'ValidationProcesses',
                'lft' => 1235,
                'rght' => 1280,
            ],
            [// id=610
                'parent_id' => 609, // root/controllers/ValidationProcesses
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 1236,
                'rght' => 1237,
            ],
            [// id=611
                'parent_id' => 609, // root/controllers/ValidationProcesses
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'deliveryRequests',
                'lft' => 1238,
                'rght' => 1241,
            ],
            [// id=612
                'parent_id' => 609, // root/controllers/ValidationProcesses
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'destructionRequests',
                'lft' => 1242,
                'rght' => 1247,
            ],
            [// id=613
                'parent_id' => 609, // root/controllers/ValidationProcesses
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'myTransfers',
                'lft' => 1248,
                'rght' => 1253,
            ],
            [// id=614
                'parent_id' => 609, // root/controllers/ValidationProcesses
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'outgoingTransferRequests',
                'lft' => 1254,
                'rght' => 1259,
            ],
            [// id=615
                'parent_id' => 611, // root/controllers/ValidationProcesses/deliveryRequests
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'processDeliveryRequest',
                'lft' => 1239,
                'rght' => 1240,
            ],
            [// id=616
                'parent_id' => 609, // root/controllers/ValidationProcesses
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'processDeliveryRequestMail',
                'lft' => 1260,
                'rght' => 1261,
            ],
            [// id=617
                'parent_id' => 612, // root/controllers/ValidationProcesses/destructionRequests
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'processDestructionRequest',
                'lft' => 1243,
                'rght' => 1244,
            ],
            [// id=618
                'parent_id' => 609, // root/controllers/ValidationProcesses
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'processDestructionRequestMail',
                'lft' => 1262,
                'rght' => 1263,
            ],
            [// id=619
                'parent_id' => 613, // root/controllers/ValidationProcesses/myTransfers
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'processMultiTransfer',
                'lft' => 1249,
                'rght' => 1250,
            ],
            [// id=620
                'parent_id' => 613, // root/controllers/ValidationProcesses/myTransfers
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'processMyTransfer',
                'lft' => 1251,
                'rght' => 1252,
            ],
            [// id=621
                'parent_id' => 614, // root/controllers/ValidationProcesses/outgoingTransferRequests
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'processOutgoingTransferRequest',
                'lft' => 1255,
                'rght' => 1256,
            ],
            [// id=622
                'parent_id' => 609, // root/controllers/ValidationProcesses
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'processOutgoingTransferRequestMail',
                'lft' => 1264,
                'rght' => 1265,
            ],
            [// id=623
                'parent_id' => 627, // root/controllers/ValidationProcesses/restitutionRequests
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'processRestitutionRequest',
                'lft' => 1271,
                'rght' => 1272,
            ],
            [// id=624
                'parent_id' => 609, // root/controllers/ValidationProcesses
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'processRestitutionRequestMail',
                'lft' => 1266,
                'rght' => 1267,
            ],
            [// id=625
                'parent_id' => 609, // root/controllers/ValidationProcesses
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'processTransfer',
                'lft' => 1268,
                'rght' => 1269,
            ],
            [// id=626
                'parent_id' => 628, // root/controllers/ValidationProcesses/transfers
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'resendMail',
                'lft' => 1277,
                'rght' => 1278,
            ],
            [// id=627
                'parent_id' => 609, // root/controllers/ValidationProcesses
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'restitutionRequests',
                'lft' => 1270,
                'rght' => 1275,
            ],
            [// id=628
                'parent_id' => 609, // root/controllers/ValidationProcesses
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'transfers',
                'lft' => 1276,
                'rght' => 1279,
            ],
            [// id=629
                'parent_id' => 612, // root/controllers/ValidationProcesses/destructionRequests
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'viewFreezedArchivesDestruction',
                'lft' => 1245,
                'rght' => 1246,
            ],
            [// id=630
                'parent_id' => 614, // root/controllers/ValidationProcesses/outgoingTransferRequests
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'viewFreezedArchivesOutgoingTransfer',
                'lft' => 1257,
                'rght' => 1258,
            ],
            [// id=631
                'parent_id' => 627, // root/controllers/ValidationProcesses/restitutionRequests
                'model' => 'ValidationProcesses',
                'foreign_key' => null,
                'alias' => 'viewFreezedArchivesRestitution',
                'lft' => 1273,
                'rght' => 1274,
            ],
            [// id=632
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'ValidationStages',
                'lft' => 1281,
                'rght' => 1284,
            ],
            [// id=633
                'parent_id' => 601, // root/controllers/ValidationChains/add
                'model' => 'ValidationStages',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 1203,
                'rght' => 1204,
            ],
            [// id=634
                'parent_id' => 632, // root/controllers/ValidationStages
                'model' => 'ValidationStages',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 1282,
                'rght' => 1283,
            ],
            [// id=635
                'parent_id' => 604, // root/controllers/ValidationChains/edit
                'model' => 'ValidationStages',
                'foreign_key' => null,
                'alias' => 'changeOrder',
                'lft' => 1221,
                'rght' => 1222,
            ],
            [// id=636
                'parent_id' => 603, // root/controllers/ValidationChains/delete
                'model' => 'ValidationStages',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 1211,
                'rght' => 1212,
            ],
            [// id=637
                'parent_id' => 604, // root/controllers/ValidationChains/edit
                'model' => 'ValidationStages',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 1223,
                'rght' => 1224,
            ],
            [// id=638
                'parent_id' => 605, // root/controllers/ValidationChains/index
                'model' => 'ValidationStages',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 1229,
                'rght' => 1230,
            ],
            [// id=639
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Volumes',
                'lft' => 1285,
                'rght' => 1288,
            ],
            [// id=640
                'parent_id' => 639, // root/controllers/Volumes
                'model' => 'Volumes',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 1286,
                'rght' => 1287,
            ],
            [// id=641
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Webservices',
                'lft' => 1289,
                'rght' => 1300,
            ],
            [// id=642
                'parent_id' => 641, // root/controllers/Webservices
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'accessToken',
                'lft' => 1290,
                'rght' => 1291,
            ],
            [// id=643
                'parent_id' => 572, // root/controllers/Users/add
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 1151,
                'rght' => 1152,
            ],
            [// id=644
                'parent_id' => 641, // root/controllers/Webservices
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 1292,
                'rght' => 1293,
            ],
            [// id=645
                'parent_id' => 641, // root/controllers/Webservices
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'authorize',
                'lft' => 1294,
                'rght' => 1295,
            ],
            [// id=646
                'parent_id' => 579, // root/controllers/Users/delete
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 1165,
                'rght' => 1166,
            ],
            [// id=647
                'parent_id' => 581, // root/controllers/Users/editByAdmin
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 1171,
                'rght' => 1172,
            ],
            [// id=648
                'parent_id' => 641, // root/controllers/Webservices
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 1296,
                'rght' => 1297,
            ],
            [// id=649
                'parent_id' => 641, // root/controllers/Webservices
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'refreshToken',
                'lft' => 1298,
                'rght' => 1299,
            ],
            [// id=650
                'parent_id' => 593, // root/controllers/Users/view
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 1191,
                'rght' => 1192,
            ],
            [// id=651
                'parent_id' => 1, // root
                'model' => 'root',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 1306,
                'rght' => 1387,
            ],
            [// id=652
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Agreements',
                'lft' => 1307,
                'rght' => 1308,
            ],
            [// id=653
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'ArchiveUnits',
                'lft' => 1309,
                'rght' => 1314,
            ],
            [// id=654
                'parent_id' => 653, // root/api/ArchiveUnits
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'binaries',
                'lft' => 1310,
                'rght' => 1311,
            ],
            [// id=655
                'parent_id' => 653, // root/api/ArchiveUnits
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'binary',
                'lft' => 1312,
                'rght' => 1313,
            ],
            [// id=656
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Archives',
                'lft' => 1315,
                'rght' => 1318,
            ],
            [// id=657
                'parent_id' => 656, // root/api/Archives
                'model' => 'Archives',
                'foreign_key' => null,
                'alias' => 'import',
                'lft' => 1316,
                'rght' => 1317,
            ],
            [// id=658
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Commands',
                'lft' => 1319,
                'rght' => 1320,
            ],
            [// id=659
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Eaccpfs',
                'lft' => 1321,
                'rght' => 1322,
            ],
            [// id=660
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Oaipmh',
                'lft' => 1323,
                'rght' => 1328,
            ],
            [// id=661
                'parent_id' => 660, // root/api/Oaipmh
                'model' => 'Oaipmh',
                'foreign_key' => null,
                'alias' => 'v1',
                'lft' => 1324,
                'rght' => 1325,
            ],
            [// id=662
                'parent_id' => 660, // root/api/Oaipmh
                'model' => 'Oaipmh',
                'foreign_key' => null,
                'alias' => 'v2',
                'lft' => 1326,
                'rght' => 1327,
            ],
            [// id=663
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'OrgEntities',
                'lft' => 1329,
                'rght' => 1330,
            ],
            [// id=664
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'OutgoingTransferRequests',
                'lft' => 1331,
                'rght' => 1334,
            ],
            [// id=665
                'parent_id' => 664, // root/api/OutgoingTransferRequests
                'model' => 'OutgoingTransferRequests',
                'foreign_key' => null,
                'alias' => 'options',
                'lft' => 1332,
                'rght' => 1333,
            ],
            [// id=666
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'OutgoingTransfers',
                'lft' => 1335,
                'rght' => 1340,
            ],
            [// id=667
                'parent_id' => 666, // root/api/OutgoingTransfers
                'model' => 'OutgoingTransfers',
                'foreign_key' => null,
                'alias' => 'zip',
                'lft' => 1336,
                'rght' => 1337,
            ],
            [// id=668
                'parent_id' => 666, // root/api/OutgoingTransfers
                'model' => 'OutgoingTransfers',
                'foreign_key' => null,
                'alias' => 'deleteZip',
                'lft' => 1338,
                'rght' => 1339,
            ],
            [// id=669
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Profiles',
                'lft' => 1341,
                'rght' => 1342,
            ],
            [// id=670
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Restservices',
                'lft' => 1343,
                'rght' => 1348,
            ],
            [// id=671
                'parent_id' => 672, // root/api/Restservices/sedaMessages
                'model' => 'Restservices',
                'foreign_key' => null,
                'alias' => 'sedaAttachmentsChunkFiles',
                'lft' => 1345,
                'rght' => 1346,
            ],
            [// id=672
                'parent_id' => 670, // root/api/Restservices
                'model' => 'Restservices',
                'foreign_key' => null,
                'alias' => 'sedaMessages',
                'lft' => 1344,
                'rght' => 1347,
            ],
            [// id=673
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Roles',
                'lft' => 1349,
                'rght' => 1350,
            ],
            [// id=674
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'SecureDataSpaces',
                'lft' => 1351,
                'rght' => 1352,
            ],
            [// id=675
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'ServiceLevels',
                'lft' => 1353,
                'rght' => 1354,
            ],
            [// id=676
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Timestampers',
                'lft' => 1355,
                'rght' => 1356,
            ],
            [// id=677
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Transfers',
                'lft' => 1357,
                'rght' => 1364,
            ],
            [// id=678
                'parent_id' => 677, // root/api/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'acknowledgement',
                'lft' => 1358,
                'rght' => 1359,
            ],
            [// id=679
                'parent_id' => 677, // root/api/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'prepareChunked',
                'lft' => 1362,
                'rght' => 1363,
            ],
            [// id=680
                'parent_id' => 677, // root/api/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'reply',
                'lft' => 1360,
                'rght' => 1361,
            ],
            [// id=681
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'TypeEntities',
                'lft' => 1365,
                'rght' => 1366,
            ],
            [// id=682
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Users',
                'lft' => 1367,
                'rght' => 1368,
            ],
            [// id=683
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'ValidationActors',
                'lft' => 1369,
                'rght' => 1370,
            ],
            [// id=684
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'ValidationChains',
                'lft' => 1371,
                'rght' => 1372,
            ],
            [// id=685
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'ValidationStages',
                'lft' => 1373,
                'rght' => 1374,
            ],
            [// id=686
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Volumes',
                'lft' => 1375,
                'rght' => 1376,
            ],
            [// id=687
                'parent_id' => 651, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Webservices',
                'lft' => 1377,
                'rght' => 1386,
            ],
            [// id=688
                'parent_id' => 687, // root/api/Webservices
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'monitoring',
                'lft' => 1378,
                'rght' => 1379,
            ],
            [// id=689
                'parent_id' => 687, // root/api/Webservices
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'ping',
                'lft' => 1380,
                'rght' => 1381,
            ],
            [// id=690
                'parent_id' => 687, // root/api/Webservices
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'version',
                'lft' => 1382,
                'rght' => 1383,
            ],
            [// id=691
                'parent_id' => 687, // root/api/Webservices
                'model' => 'Webservices',
                'foreign_key' => null,
                'alias' => 'whoami',
                'lft' => 1384,
                'rght' => 1385,
            ],
            [// id=692
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Mercure',
                'lft' => 1301,
                'rght' => 1304,
            ],
            [// id=693
                'parent_id' => 692, // root/controllers/Mercure
                'model' => 'Mercure',
                'foreign_key' => null,
                'alias' => 'publish',
                'lft' => 1302,
                'rght' => 1303,
            ],
            [// id=694
                'parent_id' => 175, // root/controllers/ArchiveUnits/catalogMyEntity
                'model' => 'ArchiveUnits',
                'foreign_key' => null,
                'alias' => 'displayXmlPublicMyEntity',
                'lft' => 361,
                'rght' => 362,
            ],
        ];
        TableRegistry::getTableLocator()->clear();
        parent::init();
    }
}
