<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TechnicalArchiveUnitsFixture
 */
class TechnicalArchiveUnitsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'state' => 'available',
                'technical_archive_id' => 1,
                'parent_id' => null,
                'lft' => 1,
                'rght' => 4,
                'archival_agency_identifier' => 'c1037259-43b8-4b90-988a-4239839ac8f3_0001',
                'name' => 'Journaux des événements de asalae : 2021',
                'description' => 'Cette archive technique regroupe les fichiers des 
                    journaux des évènements du SAE asalae pour une période de un an. 
                    Chaque journal contient les événements d\'une journée à partir 
                    de 00:00:00 jusqu\'à 23:59:59. 
                    Les journaux sont des fichiers xml formatés en PREMIS V3',
                'history' => null,
                'oldest_date' => '2021-12-09',
                'latest_date' => '2021-12-24',
                'original_local_size' => 194682,
                'original_local_count' => 5,
                'original_total_size' => 194682,
                'original_total_count' => 5,
                'xml_node_tagname' => '',
            ],
            [// id=2
                'state' => 'available',
                'technical_archive_id' => 1,
                'parent_id' => 1,
                'lft' => 2,
                'rght' => 3,
                'archival_agency_identifier' => 'c1037259-43b8-4b90-988a-4239839ac8f3_0002',
                'name' => 'Journaux des événements de asalae : 2021-12',
                'description' => 'Cette archive technique regroupe les fichiers des 
                    journaux des évènements du SAE asalae pour une période de un an. 
                    Chaque journal contient les événements d\'une journée à partir 
                    de 00:00:00 jusqu\'à 23:59:59. 
                    Les journaux sont des fichiers xml formatés en PREMIS V3',
                'history' => null,
                'oldest_date' => '2021-12-09',
                'latest_date' => '2021-12-24',
                'original_local_size' => 194682,
                'original_local_count' => 5,
                'original_total_size' => 194682,
                'original_total_count' => 5,
                'xml_node_tagname' => '',
            ],
            [// id=3
                'state' => 'available',
                'technical_archive_id' => 2,
                'parent_id' => null,
                'lft' => 5,
                'rght' => 8,
                'archival_agency_identifier' => '6621dae4-ff82-4429-bd2d-73f514c1095d_0001',
                'name' => 'Journaux des événements de asalae : 2022',
                'description' => 'Cette archive technique regroupe les fichiers des 
                    journaux des évènements du SAE asalae pour une période de un an. 
                    Chaque journal contient les événements d\'une journée à partir 
                    de 00:00:00 jusqu\'à 23:59:59. 
                    Les journaux sont des fichiers xml formatés en PREMIS V3',
                'history' => null,
                'oldest_date' => '2022-01-06',
                'latest_date' => '2022-01-11',
                'original_local_size' => 12028,
                'original_local_count' => 3,
                'original_total_size' => 12028,
                'original_total_count' => 3,
                'xml_node_tagname' => '',
            ],
            [// id=4
                'state' => 'available',
                'technical_archive_id' => 2,
                'parent_id' => 3,
                'lft' => 6,
                'rght' => 7,
                'archival_agency_identifier' => '6621dae4-ff82-4429-bd2d-73f514c1095d_0002',
                'name' => 'Journaux des événements de asalae : 2022-01',
                'description' => 'Cette archive technique regroupe les fichiers des 
                    journaux des évènements du SAE asalae pour une période de un an. 
                    Chaque journal contient les événements d\'une journée à partir 
                    de 00:00:00 jusqu\'à 23:59:59. 
                    Les journaux sont des fichiers xml formatés en PREMIS V3',
                'history' => null,
                'oldest_date' => '2022-01-06',
                'latest_date' => '2022-01-11',
                'original_local_size' => 12028,
                'original_local_count' => 3,
                'original_total_size' => 12028,
                'original_total_count' => 3,
                'xml_node_tagname' => '',
            ],
        ];
        parent::init();
    }
}
