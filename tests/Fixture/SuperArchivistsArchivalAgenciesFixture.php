<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SuperArchivistsArchivalAgenciesFixture
 */
class SuperArchivistsArchivalAgenciesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'user_id' => 1,
                'archival_agency_id' => 2,
            ],
        ];
        parent::init();
    }
}
