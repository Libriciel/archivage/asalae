<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BeanstalkWorkersFixture
 */
class BeanstalkWorkersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=32
                'name' => 'archive',
                'tube' => 'archive',
                'path' => 'Asalae\Worker\ArchiveWorker',
                'pid' => 11136,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=33
                'name' => 'batch-treatment',
                'tube' => 'batch-treatment',
                'path' => 'Asalae\Worker\BatchTreatmentWorker',
                'pid' => 11169,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=34
                'name' => 'archive-unit',
                'tube' => 'archive-unit',
                'path' => 'Asalae\Worker\ArchiveUnitWorker',
                'pid' => 11161,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=35
                'name' => 'archive-management',
                'tube' => 'archive-management',
                'path' => 'Asalae\Worker\ArchiveManagementWorker',
                'pid' => 11152,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=36
                'name' => 'control',
                'tube' => 'control',
                'path' => 'Asalae\Worker\ControlWorker',
                'pid' => 11181,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=37
                'name' => 'archive-binary',
                'tube' => 'archive-binary',
                'path' => 'Asalae\Worker\ArchiveBinaryWorker',
                'pid' => 11143,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=38
                'name' => 'delivery',
                'tube' => 'delivery',
                'path' => 'Asalae\Worker\DeliveryWorker',
                'pid' => 11199,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=39
                'name' => 'destruction',
                'tube' => 'destruction',
                'path' => 'Asalae\Worker\DestructionWorker',
                'pid' => 11209,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=40
                'name' => 'analyse',
                'tube' => 'analyse',
                'path' => 'Asalae\Worker\AnalyseWorker',
                'pid' => 11134,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=41
                'name' => 'mail',
                'tube' => 'mail',
                'path' => 'Asalae\Worker\MailWorker',
                'pid' => 11218,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=42
                'name' => 'conversion',
                'tube' => 'conversion',
                'path' => 'Asalae\Worker\ConversionWorker',
                'pid' => 11195,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=43
                'name' => 'hash',
                'tube' => 'hash',
                'path' => 'AsalaeCore\Worker\HashWorker',
                'pid' => 11215,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=44
                'name' => 'outgoing-transfer',
                'tube' => 'outgoing-transfer',
                'path' => 'Asalae\Worker\OutgoingTransferWorker',
                'pid' => 11234,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=45
                'name' => 'restitution-build',
                'tube' => 'restitution-build',
                'path' => 'Asalae\Worker\RestitutionBuildWorker',
                'pid' => 11243,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
            [// id=46
                'name' => 'timestamp',
                'tube' => 'timestamp',
                'path' => 'Asalae\Worker\TimestampWorker',
                'pid' => 11252,
                'last_launch' => '2021-12-10T09:28:21',
                'hostname' => 'testhost',
            ],
        ];
        parent::init();
    }
}
