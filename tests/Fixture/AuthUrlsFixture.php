<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AuthUrlsFixture
 */
class AuthUrlsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'code' => 'e3494f54da00b5d68012a5c62c2419831375669f61b20ea565bee',
                'url' => '/validation-processes/process-transfer/1/2/1',
                'expire' => '2022-01-09T15:11:49',
                'created' => '2021-12-09T15:11:49',
            ],
            [// id=2
                'code' => '54efeb34bb35d0860e6e65317561ed2f5635cb5961b213fd2eeee',
                'url' => '/users/initialize-password/3',
                'expire' => '2021-12-16T15:34:37',
                'created' => '2021-12-09T15:34:37',
            ],
            [// id=3
                'code' => '6f15ce947c3351b59292d214ae660420dca7fd6361b30fa58a1fe',
                'url' => '/validation-processes/process-transfer/8/2/8',
                'expire' => '2022-01-10T09:28:21',
                'created' => '2021-12-10T09:28:21',
            ],
        ];
        parent::init();
    }
}
