<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchiveUnitsBatchTreatmentsFixture
 */
class ArchiveUnitsBatchTreatmentsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archive_unit_id' => 3,
                'batch_treatment_id' => 1,
            ],
            [// id=2
                'archive_unit_id' => 1,
                'batch_treatment_id' => 1,
            ],
        ];
        parent::init();
    }
}
