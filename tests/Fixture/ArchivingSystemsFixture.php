<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchivingSystemsFixture
 */
class ArchivingSystemsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'org_entity_id' => 2,
                'name' => 'Même instance, sur sa2',
                'description' => null,
                'url' => 'https://next',
                'username' => 'sa2',
                'use_proxy' => true,
                'created' => '2021-12-09T16:25:53',
                'modified' => '2021-12-09T16:54:21',
                'active' => true,
                'password' => 'd1d3cb3bf314495cb45d2d7bc2293ce826842010ac5121fa55a0f3777459b51f���ĉ$I�ii��_0S���&u�e7���8�',
                'ssl_verify_peer' => false,
                'ssl_verify_peer_name' => false,
                'ssl_verify_depth' => 5,
                'ssl_verify_host' => false,
                'ssl_cafile' => null,
                'chunk_size' => null,
            ],
        ];
        parent::init();
    }
}
