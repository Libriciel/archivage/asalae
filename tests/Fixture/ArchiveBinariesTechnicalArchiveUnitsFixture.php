<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchiveBinariesTechnicalArchiveUnitsFixture
 */
class ArchiveBinariesTechnicalArchiveUnitsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archive_binary_id' => 6,
                'technical_archive_unit_id' => 2,
            ],
            [// id=2
                'archive_binary_id' => 7,
                'technical_archive_unit_id' => 2,
            ],
            [// id=3
                'archive_binary_id' => 8,
                'technical_archive_unit_id' => 2,
            ],
            [// id=4
                'archive_binary_id' => 9,
                'technical_archive_unit_id' => 2,
            ],
            [// id=5
                'archive_binary_id' => 10,
                'technical_archive_unit_id' => 2,
            ],
            [// id=6
                'archive_binary_id' => 11,
                'technical_archive_unit_id' => 2,
            ],
            [// id=7
                'archive_binary_id' => 12,
                'technical_archive_unit_id' => 2,
            ],
            [// id=8
                'archive_binary_id' => 13,
                'technical_archive_unit_id' => 2,
            ],
            [// id=9
                'archive_binary_id' => 14,
                'technical_archive_unit_id' => 2,
            ],
            [// id=10
                'archive_binary_id' => 15,
                'technical_archive_unit_id' => 2,
            ],
            [// id=11
                'archive_binary_id' => 16,
                'technical_archive_unit_id' => 4,
            ],
            [// id=12
                'archive_binary_id' => 17,
                'technical_archive_unit_id' => 4,
            ],
            [// id=13
                'archive_binary_id' => 18,
                'technical_archive_unit_id' => 4,
            ],
            [// id=14
                'archive_binary_id' => 19,
                'technical_archive_unit_id' => 4,
            ],
            [// id=15
                'archive_binary_id' => 20,
                'technical_archive_unit_id' => 4,
            ],
            [// id=16
                'archive_binary_id' => 21,
                'technical_archive_unit_id' => 4,
            ],
        ];
        parent::init();
    }
}
