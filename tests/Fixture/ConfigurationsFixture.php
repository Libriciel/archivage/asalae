<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ConfigurationsFixture
 */
class ConfigurationsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'org_entity_id' => 2,
                'name' => 'attestation-destruction-certificate-intermediate',
                'setting' => '1',
            ],
        ];
        parent::init();
    }
}
