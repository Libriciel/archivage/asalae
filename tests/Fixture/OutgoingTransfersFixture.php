<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OutgoingTransfersFixture
 */
class OutgoingTransfersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'identifier' => 'sa_OAT_1',
                'comment' => 'Transferts de l\'archive sa_5',
                'outgoing_transfer_request_id' => 1,
                'archive_unit_id' => 5,
                'state' => 'sent',
                'last_state_update' => '2021-12-09T17:30:55',
                'states_history' => '[{"date":"2021-12-09T17:30:54.435165+0100","state":"ready_to_send"},{"date":"2021-12-09T17:30:55.453028+0100","state":"sent"}]',
                'created' => '2021-12-09T17:30:52',
                'modified' => '2021-12-09T17:30:55',
                'error_msg' => null,
            ],
        ];
        parent::init();
    }
}
