<?php
namespace Asalae\Test\Fixture;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArosFixture
 */
class ArosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 1,
                'alias' => 'Archiviste',
                'lft' => 1,
                'rght' => 6,
            ],
            [// id=2
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 2,
                'alias' => 'Opérateur d\'archivage',
                'lft' => 7,
                'rght' => 8,
            ],
            [// id=3
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 3,
                'alias' => 'Référent Archives',
                'lft' => 9,
                'rght' => 10,
            ],
            [// id=4
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 4,
                'alias' => 'Contrôleur',
                'lft' => 11,
                'rght' => 12,
            ],
            [// id=5
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 5,
                'alias' => 'Versant',
                'lft' => 13,
                'rght' => 14,
            ],
            [// id=6
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 6,
                'alias' => 'Producteur',
                'lft' => 15,
                'rght' => 18,
            ],
            [// id=7
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 7,
                'alias' => 'Demandeur',
                'lft' => 19,
                'rght' => 20,
            ],
            [// id=8
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 8,
                'alias' => 'ws versant',
                'lft' => 21,
                'rght' => 26,
            ],
            [// id=9
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 9,
                'alias' => 'ws administrateur',
                'lft' => 27,
                'rght' => 28,
            ],
            [// id=10
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 10,
                'alias' => 'ws requêteur',
                'lft' => 29,
                'rght' => 30,
            ],
            [// id=11
                'parent_id' => 1,
                'model' => 'Users',
                'foreign_key' => 1,
                'alias' => 'admin',
                'lft' => 2,
                'rght' => 3,
            ],
            [// id=12
                'parent_id' => 8,
                'model' => 'Users',
                'foreign_key' => 2,
                'alias' => 'pastell',
                'lft' => 22,
                'rght' => 23,
            ],
            [// id=13
                'parent_id' => 6,
                'model' => 'Users',
                'foreign_key' => 3,
                'alias' => 'prod',
                'lft' => 16,
                'rght' => 17,
            ],
            [// id=14
                'parent_id' => 8,
                'model' => 'Users',
                'foreign_key' => 4,
                'alias' => 'sa2',
                'lft' => 24,
                'rght' => 25,
            ],
            [// id=15
                'parent_id' => 1,
                'model' => 'Users',
                'foreign_key' => 5,
                'alias' => 'admin2',
                'lft' => 4,
                'rght' => 5,
            ],
            [// id=16
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 11,
                'alias' => 'Super archiviste',
                'lft' => 31,
                'rght' => 32,
            ],
            [// id=17
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 12,
                'alias' => 'Référent technique',
                'lft' => 33,
                'rght' => 36,
            ],
            [// id=18
                'parent_id' => 1,
                'model' => 'Users',
                'foreign_key' => 6,
                'alias' => 'tech',
                'lft' => 34,
                'rght' => 35,
            ],
        ];
        TableRegistry::getTableLocator()->clear();
        parent::init();
    }
}
