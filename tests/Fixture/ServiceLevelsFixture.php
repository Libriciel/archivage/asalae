<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ServiceLevelsFixture
 */
class ServiceLevelsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'org_entity_id' => 2,
                'identifier' => 'servicelevel1',
                'name' => 'Mon niveau de service',
                'description' => '',
                'active' => true,
                'created' => '2021-12-09T14:18:39',
                'modified' => '2021-12-09T14:18:39',
                'ts_msg_id' => null,
                'ts_pjs_id' => null,
                'ts_conv_id' => null,
                'default_level' => true,
                'secure_data_space_id' => null,
                'convert_preservation' => 'document_text,document_pdf,document_calc,document_presentation,image,audio,video',
                'convert_dissemination' => null,
                'commitment' => null,
            ],
        ];
        parent::init();
    }
}
