<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchiveUnitsDestructionRequestsFixture
 */
class ArchiveUnitsDestructionRequestsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
        ];
        parent::init();
    }
}
