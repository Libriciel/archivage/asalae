<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ValidationStagesFixture
 */
class ValidationStagesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'validation_chain_id' => 1,
                'name' => 'Etape 1 conformes',
                'description' => '',
                'all_actors_to_complete' => false,
                'ord' => 1,
                'app_type' => null,
                'app_meta' => null,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
            ],
            [// id=2
                'validation_chain_id' => 1,
                'name' => 'Etape 2 conformes',
                'description' => '',
                'all_actors_to_complete' => false,
                'ord' => 2,
                'app_type' => null,
                'app_meta' => null,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
            ],
            [// id=3
                'validation_chain_id' => 2,
                'name' => 'Etape 1 non conformes',
                'description' => '',
                'all_actors_to_complete' => false,
                'ord' => 1,
                'app_type' => null,
                'app_meta' => null,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
            ],
            [// id=4
                'validation_chain_id' => 3,
                'name' => 'Etape 1 comms',
                'description' => '',
                'all_actors_to_complete' => false,
                'ord' => 1,
                'app_type' => null,
                'app_meta' => null,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
            ],
            [// id=5
                'validation_chain_id' => 4,
                'name' => 'Etape 1 elims',
                'description' => '',
                'all_actors_to_complete' => false,
                'ord' => 1,
                'app_type' => null,
                'app_meta' => null,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
            ],
            [// id=6
                'validation_chain_id' => 5,
                'name' => 'Etape 1 transferts sortants',
                'description' => '',
                'all_actors_to_complete' => false,
                'ord' => 1,
                'app_type' => null,
                'app_meta' => null,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
            ],
            [// id=7
                'validation_chain_id' => 6,
                'name' => 'Etape 1',
                'description' => '',
                'all_actors_to_complete' => false,
                'ord' => 1,
                'app_type' => null,
                'app_meta' => '{"created_user_id":1}',
                'created' => '2021-12-09T15:14:09',
                'modified' => '2021-12-09T15:14:09',
            ],
            [// id=8
                'validation_chain_id' => 7,
                'name' => 'Etape 1',
                'description' => '',
                'all_actors_to_complete' => false,
                'ord' => 1,
                'app_type' => null,
                'app_meta' => '{"created_user_id":5,"default":false,"active":false,"modifieds":[{"user_id":5,"date":"2021-12-09 17:28:08","fields":["name","default","app_meta","active"]}]}',
                'created' => '2021-12-09T17:27:55',
                'modified' => '2021-12-09T17:28:08',
            ],
        ];
        parent::init();
    }
}
