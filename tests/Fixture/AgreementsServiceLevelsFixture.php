<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AgreementsServiceLevelsFixture
 */
class AgreementsServiceLevelsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'agreement_id' => 2,
                'service_level_id' => 1,
            ],
        ];
        parent::init();
    }
}
