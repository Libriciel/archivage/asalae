<?php
namespace Asalae\Test\Fixture;

use Adldap\Schemas\ActiveDirectory;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * LdapsFixture
 */
class LdapsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'org_entity_id' => 2,
                'name' => 'LDAP de test',
                'host' => '127.0.0.1',
                'port' => 389,
                'user_query_login' => 'test@adullact.win',
                'user_query_password' => 'test',
                'ldap_root_search' => 'dc=adullact,dc=win',
                'user_login_attribute' => 'sAMAccountName',
                'ldap_users_filter' => '(memberOf=cn=asalae,OU=Groupes,dc=adullact,dc=win)',
                'account_prefix' => '',
                'account_suffix' => '@adullact.win',
                'description' => 'Pour tester la connexion LDAP',
                'use_proxy' => false,
                'use_ssl' => false,
                'use_tls' => false,
                'user_name_attribute' => 'displayname',
                'user_mail_attribute' => 'mail',
                'created' => '2019-05-17T11:18:38',
                'modified' => '2019-05-17T11:18:38',
                'schema' => ActiveDirectory::class,
                'follow_referrals' => true,
                'version' => 3,
                'timeout' => 5,
                'custom_options' => '[]',
                'user_username_attribute' => 'sAMAccountName',
            ],
            [// id=2
                'org_entity_id' => 2,
                'name' => 'LDAP de test 2',
                'host' => '127.0.0.1',
                'port' => 389,
                'user_query_login' => 'test@adullact.win',
                'user_query_password' => 'test',
                'ldap_root_search' => 'dc=adullact,dc=win',
                'user_login_attribute' => 'sAMAccountName',
                'ldap_users_filter' => '(memberOf=cn=asalae,OU=Groupes,dc=adullact,dc=win)',
                'account_prefix' => '',
                'account_suffix' => '@adullact.win',
                'description' => 'Pour tester la connexion LDAP',
                'use_proxy' => false,
                'use_ssl' => false,
                'use_tls' => false,
                'user_name_attribute' => 'displayname',
                'user_mail_attribute' => 'mail',
                'created' => '2019-05-17T11:18:38',
                'modified' => '2019-05-17T11:18:38',
                'schema' => ActiveDirectory::class,
                'follow_referrals' => true,
                'version' => 3,
                'timeout' => 5,
                'custom_options' => '[]',
                'user_username_attribute' => 'sAMAccountName',
            ],
        ];
        parent::init();
    }
}
