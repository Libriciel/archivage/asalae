<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DestructionNotificationsFixture
 */
class DestructionNotificationsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'identifier' => 'sa_ADN_1',
                'destruction_request_id' => 1,
                'comment' => 'Elimination de l\'unité d\'archives :
- sa_3 - seda2.1',
                'created' => '2021-12-09T16:22:20',
            ],
        ];
        parent::init();
    }
}
