<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OaipmhListsetsFixture
 */
class OaipmhListsetsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'oaipmh_token_id' => 1,
                'spec' => 'OriginatingAgency:sa',
                'name' => 'Mon Service d\'archive',
            ],
            [// id=2
                'oaipmh_token_id' => 1,
                'spec' => 'OriginatingAgency:sp',
                'name' => 'Mon service producteur',
            ],
            [// id=3
                'oaipmh_token_id' => 1,
                'spec' => 'TransferringAgency:sa',
                'name' => 'Mon Service d\'archive',
            ],
            [// id=4
                'oaipmh_token_id' => 1,
                'spec' => 'ArchivalAgreement:agreement2',
                'name' => 'Mon accord de versement 2',
            ],
            [// id=5
                'oaipmh_token_id' => 1,
                'spec' => 'ArchivalProfile:profile1',
                'name' => 'Mon profil d\'archives',
            ],
            [// id=6
                'oaipmh_token_id' => 2,
                'spec' => 'OriginatingAgency:sa',
                'name' => 'Mon Service d\'archive',
            ],
            [// id=7
                'oaipmh_token_id' => 2,
                'spec' => 'OriginatingAgency:sp',
                'name' => 'Mon service producteur',
            ],
            [// id=8
                'oaipmh_token_id' => 2,
                'spec' => 'TransferringAgency:sa',
                'name' => 'Mon Service d\'archive',
            ],
            [// id=9
                'oaipmh_token_id' => 2,
                'spec' => 'ArchivalAgreement:agreement2',
                'name' => 'Mon accord de versement 2',
            ],
            [// id=10
                'oaipmh_token_id' => 2,
                'spec' => 'ArchivalProfile:profile1',
                'name' => 'Mon profil d\'archives',
            ],
        ];
        parent::init();
    }
}
