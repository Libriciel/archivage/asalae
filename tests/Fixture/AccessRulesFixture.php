<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AccessRulesFixture
 */
class AccessRulesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'access_rule_code_id' => 1,
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=2
                'access_rule_code_id' => 1,
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=3
                'access_rule_code_id' => 1,
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=4
                'access_rule_code_id' => 1,
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=5
                'access_rule_code_id' => 1,
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=6
                'access_rule_code_id' => 1,
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=7
                'access_rule_code_id' => 1,
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=8
                'access_rule_code_id' => 1,
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=9
                'access_rule_code_id' => 1,
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=10
                'access_rule_code_id' => 1,
                'start_date' => '2021-12-09',
                'end_date' => '2021-12-09',
            ],
            [// id=11
                'access_rule_code_id' => 27,
                'start_date' => '2022-12-21',
                'end_date' => '2023-12-21',
            ],
        ];
        parent::init();
    }
}
