<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MailsFixture
 */
class MailsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'serialized' => 'O:18:"Cake\Mailer\Mailer":11:{s:12:" * transport";O:35:"Cake\Mailer\Transport\MailTransport":3:{s:17:" * _defaultConfig";a:0:{}s:10:" * _config";a:9:{s:9:"className";s:4:"Mail";s:4:"host";s:9:"localhost";s:4:"port";s:2:"25";s:7:"timeout";i:30;s:8:"username";s:0:"";s:8:"password";s:0:"";s:6:"client";N;s:3:"tls";N;s:3:"url";N;}s:21:" * _configInitialized";b:1;}s:15:" * messageClass";s:19:"Cake\Mailer\Message";s:10:" * message";O:19:"Cake\Mailer\Message":11:{s:2:"to";a:1:{s:21:"dupont.martin@test.fr";s:21:"dupont.martin@test.fr";}s:4:"from";a:1:{s:19:"no-reply@asalae2.fr";s:19:"no-reply@asalae2.fr";}s:7:"subject";s:73:"[asalae] Prendre une =?UTF-8?B?ZMOpY2lzaW9uIC0gdHJhbnNmZXJ0IHNlZGExLjA=?=";s:11:"emailFormat";s:4:"both";s:12:"emailPattern";s:60:"/^((?:[\p{L}0-9.!#$%&\'*+\/=?^_`{|}~-]+)*@[\p{L}0-9-._]+)$/ui";s:6:"domain";s:7:"testhost";s:9:"messageId";b:1;s:10:"appCharset";s:5:"UTF-8";s:7:"charset";s:5:"UTF-8";s:11:"textMessage";s:388:"Vous êtes invité à prendre une décision sur asalaeUn transfert nécéssite une prise de décision
Date: 09/12/2021 15:06Identifiant: seda1.0Est conforme ?: OuiEtape de validation: Etape 1 conformes
Afin de vous connecter à l\'application, il vous faudra cliquer sur le lien ci-dessous.
https://next/auth-urls/activate/e3494f54da00b5d68012a5c62c2419831375669f61b20ea565bee
https://next";s:11:"htmlMessage";s:8195:"<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html lang="fr-FR">
<head>
    <title>Prendre une décision</title>
    <style type=\'text/css\'>
    </style>
</head>
<body style="padding:0; margin:0; background:#f1f1f1;">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
    <tr>
        <td>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                <tr>
                    <td width="20" valign="top" align="center" bgcolor="#3e4c5b">&nbsp;</td>
                    <td height="50" align="left" bgcolor="#3e4c5b" width="660">
                        <img alt="asalae" style="height: 40px;" src="https://ressources.libriciel.fr/public/asalae/img/asalae-color-white.png" border="0">
                    </td>
                    <td width="20" valign="top" align="center" bgcolor="#3e4c5b">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table>
                <tbody>
                <tr>
                    <td width="660" align="center">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                            <tr>
                                <td width="600" valign="top" align="center">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td style="padding-top:10px;" bgcolor="#f1f1f1"></td>
                                        </tr>
                                                                                    <tr>
                                                <td style="padding-top:10px; padding-bottom: 10px; border-bottom: 1px solid #d9d9d9; font-family: arial, sans-serif; color: #4c575f; font-size: 32px; font-weight: 500" bgcolor="#f1f1f1">
                                                    Prendre une décision                                                </td>
                                            </tr>
                                                                                <tr>
                                            <td style="padding-top:20px;" bgcolor="#f1f1f1"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#ffffff" align="center" style="color: #4c575f; box-shadow: 0 0 1px rgb(210, 210, 210);">
                                                <table style="padding-bottom:10px; padding-top:10px;margin-bottom: 20px;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                    <tr valign="bottom">
                                                        <td width="20" valign="top" align="center">&nbsp;</td>
                                                        <td style="font-family:Calibri, Trebuchet, Arial, sans serif; font-size:15px; line-height:22px; color:#333333;" valign="top">
                                                            <div style="color: #4c575f">
    <h4 style="margin: 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5">
        Vous êtes invité à prendre une décision sur asalae    </h4>
    <p>Un transfert nécéssite une prise de décision</p>

    <ul>
        <li><b>Date:</b> 09/12/2021 15:06</li>
        <li><b>Identifiant:</b> seda1.0</li>
        <li><b>Est conforme ?:</b> Oui</li>
        <li><b>Etape de validation:</b> Etape 1 conformes</li>
    </ul>
    <p>Afin de vous connecter à l\'application, il vous faudra cliquer sur le lien ci-dessous.</p>

    <p><a href="https://next/auth-urls/activate/e3494f54da00b5d68012a5c62c2419831375669f61b20ea565bee">Lien de prise de décision</a></p>

    <p><a href="https://next">Accéder à la plateforme</a></p>
</div>
                                                        </td>
                                                        <td width="20" valign="top" align="center">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20" bgcolor="#f1f1f1">&nbsp;</td>
    </tr>
    <tr>
        <td align="center">
                            <table>
                <tbody>
                <tr>
                    <td width="660" align="center">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                            <tr>
                                <td style="color: #4c575f; font-family: arial, sans-serif; padding-bottom: 5px; border-bottom: 1px solid #d9d9d9;" bgcolor="#f1f1f1">
                                    Merci de ne pas répondre à cet email.                                </td>
                            </tr>
                            <tr><td height="20" style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td></tr>
                            <tr>
                                <td align="center" bgcolor="#f1f1f1">
                                    <a href="http://www.libriciel.fr" target="_blank">
                                        <img src="https://ressources.libriciel.fr/public/asalae/img/logo_libriciel.png" alt="logo Libriciel" />
                                    </a>
                                </td>
                            </tr>
                            <tr><td height="10" style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td></tr>
                            <tr>
                                <td align="center" bgcolor="#f1f1f1">
                                    <a href="https://twitter.com/Libriciel_SCOP" target="_blank"><img alt="Twitter" src="https://www.libriciel.fr/wp-content/uploads/2016/12/twitter.png" height="16" width="16" /></a>&nbsp;&nbsp;<a href="https://www.facebook.com/LibricielSCOP/" target="_blank"><img style="margin-left: 3px;" alt="Facebook" src="https://www.libriciel.fr/wp-content/uploads/2016/12/facebook.png" height="16" width="16" /></a>&nbsp;&nbsp;<a href="https://fr.linkedin.com/company/libriciel-scop" target="_blank"><img style="margin-left: 3px;" alt="LinkedIn" src="https://www.libriciel.fr/wp-content/uploads/2016/12/linkedinAP.png" height="16" width="16" /></a>&nbsp;&nbsp;<a href="https://www.youtube.com/user/adullactprojet" target="_blank"><img style="margin-left: 3px;" alt="Youtube" src="https://www.libriciel.fr/wp-content/uploads/2016/12/youtube.3.png" height="16" width="16" /></a>
                                </td>
                            </tr>
                            <tr><td height="20" style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td></tr>
                            <tr>
                                <td align="center" style="color: #4c575f; font-family: arial, sans-serif;" bgcolor="#f1f1f1">
                                    Libriciel SCOP -
                                    140 Rue Aglaonice de Thessalie, 34170 Castelnau-le-Lez<br />
                                    Espace client : <a style="color: #42a1e8;" href="https://libriciel.fr/espace-client/" target="_blank">se connecter</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
                    </td>
    </tr>
    </tbody>
</table>
</body>
</html>";}s:11:" * renderer";O:20:"Cake\Mailer\Renderer":1:{s:15:" * _viewBuilder";O:21:"Cake\View\ViewBuilder":6:{s:9:"_template";s:16:"process_transfer";s:7:"_layout";s:18:"AsalaeCore.default";s:11:"_autoLayout";b:1;s:10:"_className";s:14:"Cake\View\View";s:8:"_helpers";a:3:{i:0;s:4:"Html";i:1;s:4:"Html";i:2;s:3:"Url";}s:5:"_vars";a:8:{s:5:"title";s:21:"Prendre une décision";s:4:"code";O:27:"Asalae\Model\Entity\AuthUrl":10:{s:10:" * _fields";a:6:{s:4:"code";s:53:"e3494f54da00b5d68012a5c62c2419831375669f61b20ea565bee";s:6:"expire";s:19:"2022-01-09 15:11:49";s:3:"url";s:44:"/validation-processes/process-transfer/1/2/1";s:13:"auth_sub_urls";a:1:{i:0;O:30:"Asalae\Model\Entity\AuthSubUrl":10:{s:10:" * _fields";a:3:{s:3:"url";s:24:"/transfers/display-xml/1";s:11:"auth_url_id";i:1;s:2:"id";i:1;}s:12:" * _original";a:0:{}s:10:" * _hidden";a:0:{}s:11:" * _virtual";a:0:{}s:9:" * _dirty";a:0:{}s:7:" * _new";b:0;s:10:" * _errors";a:0:{}s:11:" * _invalid";a:0:{}s:14:" * _accessible";a:1:{s:1:"*";b:1;}s:17:" * _registryAlias";s:11:"AuthSubUrls";}}s:7:"created";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 15:11:49.434860";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:2:"id";i:1;}s:12:" * _original";a:0:{}s:10:" * _hidden";a:0:{}s:11:" * _virtual";a:0:{}s:9:" * _dirty";a:0:{}s:7:" * _new";b:0;s:10:" * _errors";a:0:{}s:11:" * _invalid";a:0:{}s:14:" * _accessible";a:1:{s:1:"*";b:1;}s:17:" * _registryAlias";s:8:"AuthUrls";}s:8:"transfer";O:28:"Asalae\Model\Entity\Transfer":17:{s:11:" * _virtual";a:8:{i:0;s:19:"message_versiontrad";i:1;s:9:"statetrad";i:2;s:15:"statetrad_title";i:3;s:8:"editable";i:4;s:9:"deletable";i:5;s:8:"is_large";i:6;s:12:"pdf_basename";i:7;s:3:"pdf";}s:10:" * _fields";a:23:{s:2:"id";i:1;s:16:"transfer_comment";s:7:"seda1.0";s:13:"transfer_date";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 15:06:00.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:19:"transfer_identifier";s:7:"seda1.0";s:18:"archival_agency_id";i:2;s:22:"transferring_agency_id";i:2;s:15:"message_version";s:7:"seda1.0";s:5:"state";s:10:"validating";s:13:"state_history";s:258:"[{"date":"2021-12-09T15:07:10.390266+0100","state":"preparating"},{"date":"2021-12-09T15:11:48.494710+0100","state":"analysing"},{"date":"2021-12-09T15:11:48.638391+0100","state":"controlling"},{"date":"2021-12-09T15:11:49.274446+0100","state":"validating"}]";s:17:"last_state_update";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 15:11:49.274446";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:10:"is_conform";b:1;s:11:"is_modified";b:0;s:11:"is_accepted";N;s:12:"agreement_id";N;s:10:"profile_id";N;s:8:"filename";s:26:"transfer-61b20d8ded833.xml";s:9:"data_size";i:554297;s:15:"created_user_id";i:1;s:7:"created";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 15:07:10.391136";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:8:"modified";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 15:11:49.274868";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:16:"service_level_id";N;s:13:"files_deleted";b:0;s:10:"data_count";i:1;}s:12:" * _original";a:0:{}s:10:" * _hidden";a:0:{}s:9:" * _dirty";a:0:{}s:7:" * _new";b:0;s:10:" * _errors";a:0:{}s:11:" * _invalid";a:0:{}s:14:" * _accessible";a:1:{s:1:"*";b:1;}s:17:" * _registryAlias";s:9:"Transfers";s:4:"icon";a:13:{s:13:"ArchiveObject";s:12:"fa fa-folder";s:11:"ArchiveUnit";s:12:"fa fa-folder";s:7:"Archive";s:13:"fa fa-archive";s:19:"DescriptiveMetadata";s:13:"fa fa-archive";s:8:"Contains";s:13:"fa fa-archive";s:8:"Document";s:15:"fa fa-paperclip";s:16:"BinaryDataObject";s:15:"fa fa-paperclip";s:7:"Keyword";s:9:"fa fa-key";s:18:"ContentDescription";s:17:"fa fa-file-text-o";s:18:"ContentDescriptive";s:9:"fa fa-key";s:14:"ArchivalAgency";s:24:"fa fa-building text-info";s:18:"TransferringAgency";s:24:"fa fa-building text-info";s:17:"OriginatingAgency";s:24:"fa fa-building text-info";}s:17:"appendSubnodeText";a:4:{i:0;s:4:"name";i:1;s:10:"identifier";i:2;s:14:"identification";i:3;s:14:"keywordcontent";}s:3:"dom";N;s:9:"namespace";N;s:5:"xpath";N;s:15:"elementsPerPage";i:100;s:15:"maxCharsPerName";i:30;}s:5:"stage";O:35:"Asalae\Model\Entity\ValidationStage":13:{s:11:" * _virtual";a:1:{i:0;s:26:"all_actors_to_completetrad";}s:14:" * _metaFields";a:5:{i:0;s:6:"active";i:1;s:7:"default";i:2;s:15:"created_user_id";i:3;s:9:"modifieds";i:4;s:14:"actors_id_list";}s:10:" * _fields";a:11:{s:2:"id";i:1;s:19:"validation_chain_id";i:1;s:4:"name";s:17:"Etape 1 conformes";s:11:"description";s:0:"";s:22:"all_actors_to_complete";b:0;s:3:"ord";i:1;s:8:"app_type";N;s:8:"app_meta";N;s:7:"created";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 13:54:15.414212";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:8:"modified";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 13:54:15.414243";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:13:"metaFieldName";s:8:"app_meta";}s:12:" * _original";a:0:{}s:10:" * _hidden";a:0:{}s:9:" * _dirty";a:0:{}s:7:" * _new";b:0;s:10:" * _errors";a:0:{}s:11:" * _invalid";a:0:{}s:14:" * _accessible";a:1:{s:1:"*";b:1;}s:17:" * _registryAlias";s:16:"ValidationStages";s:7:" * meta";O:8:"stdClass":0:{}s:13:" * metaFields";a:5:{i:0;s:6:"active";i:1;s:7:"default";i:2;s:15:"created_user_id";i:3;s:9:"modifieds";i:4;s:14:"actors_id_list";}}s:7:"process";O:37:"Asalae\Model\Entity\ValidationProcess":13:{s:11:" * _virtual";a:3:{i:0;s:13:"processedtrad";i:1;s:13:"validatedtrad";i:2;s:8:"steptrad";}s:14:" * _metaFields";a:6:{i:0;s:6:"active";i:1;s:7:"default";i:2;s:15:"created_user_id";i:3;s:9:"modifieds";i:4;s:8:"stepback";i:5;s:15:"created_user_id";}s:10:" * _fields";a:15:{s:13:"metaFieldName";s:8:"app_meta";s:16:"app_subject_type";s:9:"Transfers";s:15:"app_foreign_key";i:1;s:9:"processed";b:0;s:12:"current_step";i:1;s:19:"validation_chain_id";i:1;s:16:"current_stage_id";i:1;s:15:"created_user_id";i:1;s:8:"app_meta";s:21:"{"created_user_id":1}";s:13:"processedtrad";s:8:"En cours";s:13:"validatedtrad";s:0:"";s:8:"steptrad";s:0:"";s:7:"created";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 15:11:49.388803";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:8:"modified";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 15:11:49.388855";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:2:"id";i:1;}s:12:" * _original";a:0:{}s:10:" * _hidden";a:0:{}s:9:" * _dirty";a:0:{}s:7:" * _new";b:0;s:10:" * _errors";a:0:{}s:11:" * _invalid";a:0:{}s:14:" * _accessible";a:1:{s:1:"*";b:1;}s:17:" * _registryAlias";s:19:"ValidationProcesses";s:7:" * meta";O:8:"stdClass":1:{s:15:"created_user_id";i:1;}s:13:" * metaFields";a:6:{i:0;s:6:"active";i:1;s:7:"default";i:2;s:15:"created_user_id";i:3;s:9:"modifieds";i:4;s:8:"stepback";i:5;s:15:"created_user_id";}}s:5:"actor";O:35:"Asalae\Model\Entity\ValidationActor":13:{s:14:" * _metaFields";a:4:{i:0;s:10:"actor_name";i:1;s:10:"actor_info";i:2;s:15:"created_user_id";i:3;s:15:"type_validation";}s:11:" * _virtual";a:5:{i:0;s:10:"actor_name";i:1;s:12:"app_typetrad";i:2;s:4:"meta";i:3;s:15:"type_validation";i:4;s:19:"type_validationtrad";}s:10:" * _fields";a:10:{s:2:"id";i:2;s:19:"validation_stage_id";i:1;s:8:"app_type";s:4:"MAIL";s:7:"app_key";s:21:"dupont.martin@test.fr";s:8:"app_meta";s:33:"{"actor_name":"M. Dupont Martin"}";s:7:"created";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 13:54:15.430118";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:8:"modified";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 13:54:15.430144";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:15:"app_foreign_key";N;s:10:"actor_name";s:16:"M. Dupont Martin";s:13:"metaFieldName";s:8:"app_meta";}s:12:" * _original";a:0:{}s:10:" * _hidden";a:0:{}s:9:" * _dirty";a:0:{}s:7:" * _new";b:0;s:10:" * _errors";a:0:{}s:11:" * _invalid";a:0:{}s:14:" * _accessible";a:1:{s:1:"*";b:1;}s:17:" * _registryAlias";s:16:"ValidationActors";s:7:" * meta";O:8:"stdClass":1:{s:10:"actor_name";s:16:"M. Dupont Martin";}s:13:" * metaFields";a:4:{i:0;s:10:"actor_name";i:1;s:10:"actor_info";i:2;s:15:"created_user_id";i:3;s:15:"type_validation";}}s:13:"htmlSignature";N;s:13:"textSignature";N;}}}s:18:" * clonedInstances";a:3:{s:7:"message";N;s:8:"renderer";N;s:9:"transport";N;}s:12:" * logConfig";N;s:13:" * modelClass";N;s:18:" * _modelFactories";a:0:{}s:13:" * _modelType";s:5:"Table";s:15:" * defaultTable";N;s:16:" * _tableLocator";N;}',
                'created' => '2021-12-09T15:11:49',
            ],
            [// id=2
                'serialized' => 'O:18:"Cake\Mailer\Mailer":11:{s:12:" * transport";O:35:"Cake\Mailer\Transport\MailTransport":3:{s:17:" * _defaultConfig";a:0:{}s:10:" * _config";a:9:{s:9:"className";s:4:"Mail";s:4:"host";s:9:"localhost";s:4:"port";s:2:"25";s:7:"timeout";i:30;s:8:"username";s:0:"";s:8:"password";s:0:"";s:6:"client";N;s:3:"tls";N;s:3:"url";N;}s:21:" * _configInitialized";b:1;}s:15:" * messageClass";s:19:"Cake\Mailer\Message";s:10:" * message";O:19:"Cake\Mailer\Message":11:{s:2:"to";a:1:{s:21:"dupont.martin@test.fr";s:21:"dupont.martin@test.fr";}s:4:"from";a:1:{s:19:"no-reply@asalae2.fr";s:19:"no-reply@asalae2.fr";}s:7:"subject";s:132:"[asalae] Prendre une =?UTF-8?B?ZMOpY2lzaW9uIC0gdHJhbnNmZXJ0IGF0cl9hbGVh?=
 =?UTF-8?B?XzcwZjkwZWFjZGIyZDNiNTNhZDdlODdmODA4YTgyNDMw?=";s:11:"emailFormat";s:4:"both";s:12:"emailPattern";s:60:"/^((?:[\p{L}0-9.!#$%&\'*+\/=?^_`{|}~-]+)*@[\p{L}0-9-._]+)$/ui";s:6:"domain";s:7:"testhost";s:9:"messageId";b:1;s:10:"appCharset";s:5:"UTF-8";s:7:"charset";s:5:"UTF-8";s:11:"textMessage";s:422:"Vous êtes invité à prendre une décision sur alaeUn transfert nécéssite une prise de décision
Date: 19/10/2018 10:11Identifiant: atr_alea_70f90eacdb2d3b53ad7e87f808a82430Est conforme ?: OuiEtape de validation: Etape 1 conformes
Afin de vous connecter à l\'application, il vous faudra cliquer sur le lien ci-dessous.
https://next/auth-urls/activate/6f15ce947c3351b59292d214ae660420dca7fd6361b30fa58a1fe
https://next";s:11:"htmlMessage";s:8229:"<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html lang="fr-FR">
<head>
    <title>Prendre une décision</title>
    <style type=\'text/css\'>
    </style>
</head>
<body style="padding:0; margin:0; background:#f1f1f1;">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
    <tr>
        <td>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                <tr>
                    <td width="20" valign="top" align="center" bgcolor="#3e4c5b">&nbsp;</td>
                    <td height="50" align="left" bgcolor="#3e4c5b" width="660">
                        <img alt="asalae" style="height: 40px;" src="https://ressources.libriciel.fr/public/asalae/img/asalae-color-white.png" border="0">
                    </td>
                    <td width="20" valign="top" align="center" bgcolor="#3e4c5b">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table>
                <tbody>
                <tr>
                    <td width="660" align="center">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                            <tr>
                                <td width="600" valign="top" align="center">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td style="padding-top:10px;" bgcolor="#f1f1f1"></td>
                                        </tr>
                                                                                    <tr>
                                                <td style="padding-top:10px; padding-bottom: 10px; border-bottom: 1px solid #d9d9d9; font-family: arial, sans-serif; color: #4c575f; font-size: 32px; font-weight: 500" bgcolor="#f1f1f1">
                                                    Prendre une décision                                                </td>
                                            </tr>
                                                                                <tr>
                                            <td style="padding-top:20px;" bgcolor="#f1f1f1"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#ffffff" align="center" style="color: #4c575f; box-shadow: 0 0 1px rgb(210, 210, 210);">
                                                <table style="padding-bottom:10px; padding-top:10px;margin-bottom: 20px;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                    <tr valign="bottom">
                                                        <td width="20" valign="top" align="center">&nbsp;</td>
                                                        <td style="font-family:Calibri, Trebuchet, Arial, sans serif; font-size:15px; line-height:22px; color:#333333;" valign="top">
                                                            <div style="color: #4c575f">
    <h4 style="margin: 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5">
        Vous êtes invité à prendre une décision sur asalae    </h4>
    <p>Un transfert nécéssite une prise de décision</p>

    <ul>
        <li><b>Date:</b> 19/10/2018 10:11</li>
        <li><b>Identifiant:</b> atr_alea_70f90eacdb2d3b53ad7e87f808a82430</li>
        <li><b>Est conforme ?:</b> Oui</li>
        <li><b>Etape de validation:</b> Etape 1 conformes</li>
    </ul>
    <p>Afin de vous connecter à l\'application, il vous faudra cliquer sur le lien ci-dessous.</p>

    <p><a href="https://next/auth-urls/activate/6f15ce947c3351b59292d214ae660420dca7fd6361b30fa58a1fe">Lien de prise de décision</a></p>

    <p><a href="https://next">Accéder à la plateforme</a></p>
</div>
                                                        </td>
                                                        <td width="20" valign="top" align="center">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20" bgcolor="#f1f1f1">&nbsp;</td>
    </tr>
    <tr>
        <td align="center">
                            <table>
                <tbody>
                <tr>
                    <td width="660" align="center">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                            <tr>
                                <td style="color: #4c575f; font-family: arial, sans-serif; padding-bottom: 5px; border-bottom: 1px solid #d9d9d9;" bgcolor="#f1f1f1">
                                    Merci de ne pas répondre à cet email.                                </td>
                            </tr>
                            <tr><td height="20" style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td></tr>
                            <tr>
                                <td align="center" bgcolor="#f1f1f1">
                                    <a href="http://www.libriciel.fr" target="_blank">
                                        <img src="https://ressources.libriciel.fr/public/asalae/img/logo_libriciel.png" alt="logo Libriciel" />
                                    </a>
                                </td>
                            </tr>
                            <tr><td height="10" style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td></tr>
                            <tr>
                                <td align="center" bgcolor="#f1f1f1">
                                    <a href="https://twitter.com/Libriciel_SCOP" target="_blank"><img alt="Twitter" src="https://www.libriciel.fr/wp-content/uploads/2016/12/twitter.png" height="16" width="16" /></a>&nbsp;&nbsp;<a href="https://www.facebook.com/LibricielSCOP/" target="_blank"><img style="margin-left: 3px;" alt="Facebook" src="https://www.libriciel.fr/wp-content/uploads/2016/12/facebook.png" height="16" width="16" /></a>&nbsp;&nbsp;<a href="https://fr.linkedin.com/company/libriciel-scop" target="_blank"><img style="margin-left: 3px;" alt="LinkedIn" src="https://www.libriciel.fr/wp-content/uploads/2016/12/linkedinAP.png" height="16" width="16" /></a>&nbsp;&nbsp;<a href="https://www.youtube.com/user/adullactprojet" target="_blank"><img style="margin-left: 3px;" alt="Youtube" src="https://www.libriciel.fr/wp-content/uploads/2016/12/youtube.3.png" height="16" width="16" /></a>
                                </td>
                            </tr>
                            <tr><td height="20" style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td></tr>
                            <tr>
                                <td align="center" style="color: #4c575f; font-family: arial, sans-serif;" bgcolor="#f1f1f1">
                                    Libriciel SCOP -
                                    140 Rue Aglaonice de Thessalie, 34170 Castelnau-le-Lez<br />
                                    Espace client : <a style="color: #42a1e8;" href="https://libriciel.fr/espace-client/" target="_blank">se connecter</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
                    </td>
    </tr>
    </tbody>
</table>
</body>
</html>";}s:11:" * renderer";O:20:"Cake\Mailer\Renderer":1:{s:15:" * _viewBuilder";O:21:"Cake\View\ViewBuilder":6:{s:9:"_template";s:16:"process_transfer";s:7:"_layout";s:18:"AsalaeCore.default";s:11:"_autoLayout";b:1;s:10:"_className";s:14:"Cake\View\View";s:8:"_helpers";a:3:{i:0;s:4:"Html";i:1;s:4:"Html";i:2;s:3:"Url";}s:5:"_vars";a:8:{s:5:"title";s:21:"Prendre une décision";s:4:"code";O:27:"Asalae\Model\Entity\AuthUrl":10:{s:10:" * _fields";a:6:{s:4:"code";s:53:"6f15ce947c3351b59292d214ae660420dca7fd6361b30fa58a1fe";s:6:"expire";s:19:"2022-01-10 09:28:21";s:3:"url";s:44:"/validation-processes/process-transfer/8/2/8";s:13:"auth_sub_urls";a:1:{i:0;O:30:"Asalae\Model\Entity\AuthSubUrl":10:{s:10:" * _fields";a:3:{s:3:"url";s:24:"/transfers/display-xml/8";s:11:"auth_url_id";i:7;s:2:"id";i:2;}s:12:" * _original";a:0:{}s:10:" * _hidden";a:0:{}s:11:" * _virtual";a:0:{}s:9:" * _dirty";a:0:{}s:7:" * _new";b:0;s:10:" * _errors";a:0:{}s:11:" * _invalid";a:0:{}s:14:" * _accessible";a:1:{s:1:"*";b:1;}s:17:" * _registryAlias";s:11:"AuthSubUrls";}}s:7:"created";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-10 09:28:21.576507";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:2:"id";i:7;}s:12:" * _original";a:0:{}s:10:" * _hidden";a:0:{}s:11:" * _virtual";a:0:{}s:9:" * _dirty";a:0:{}s:7:" * _new";b:0;s:10:" * _errors";a:0:{}s:11:" * _invalid";a:0:{}s:14:" * _accessible";a:1:{s:1:"*";b:1;}s:17:" * _registryAlias";s:8:"AuthUrls";}s:8:"transfer";O:28:"Asalae\Model\Entity\Transfer":17:{s:11:" * _virtual";a:8:{i:0;s:19:"message_versiontrad";i:1;s:9:"statetrad";i:2;s:15:"statetrad_title";i:3;s:8:"editable";i:4;s:9:"deletable";i:5;s:8:"is_large";i:6;s:12:"pdf_basename";i:7;s:3:"pdf";}s:10:" * _fields";a:23:{s:2:"id";i:8;s:16:"transfer_comment";s:62:"Transfert aléatoire atr_alea_70f90eacdb2d3b53ad7e87f808a82430";s:13:"transfer_date";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2018-10-19 10:11:47.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:19:"transfer_identifier";s:41:"atr_alea_70f90eacdb2d3b53ad7e87f808a82430";s:18:"archival_agency_id";i:2;s:22:"transferring_agency_id";i:2;s:15:"message_version";s:7:"seda0.2";s:5:"state";s:10:"validating";s:13:"state_history";s:258:"[{"date":"2021-12-10T09:26:23.120375+0100","state":"preparating"},{"date":"2021-12-10T09:27:50.566878+0100","state":"analysing"},{"date":"2021-12-10T09:28:21.312266+0100","state":"controlling"},{"date":"2021-12-10T09:28:21.499212+0100","state":"validating"}]";s:17:"last_state_update";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-10 09:28:21.499212";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:10:"is_conform";b:1;s:11:"is_modified";b:0;s:11:"is_accepted";N;s:12:"agreement_id";i:2;s:10:"profile_id";i:1;s:8:"filename";s:17:"sample_seda02.xml";s:9:"data_size";i:18650;s:15:"created_user_id";i:1;s:7:"created";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-10 09:26:23.121728";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:8:"modified";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-10 09:28:21.499455";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:16:"service_level_id";i:1;s:13:"files_deleted";b:0;s:10:"data_count";i:3;}s:12:" * _original";a:0:{}s:10:" * _hidden";a:0:{}s:9:" * _dirty";a:0:{}s:7:" * _new";b:0;s:10:" * _errors";a:0:{}s:11:" * _invalid";a:0:{}s:14:" * _accessible";a:1:{s:1:"*";b:1;}s:17:" * _registryAlias";s:9:"Transfers";s:4:"icon";a:13:{s:13:"ArchiveObject";s:12:"fa fa-folder";s:11:"ArchiveUnit";s:12:"fa fa-folder";s:7:"Archive";s:13:"fa fa-archive";s:19:"DescriptiveMetadata";s:13:"fa fa-archive";s:8:"Contains";s:13:"fa fa-archive";s:8:"Document";s:15:"fa fa-paperclip";s:16:"BinaryDataObject";s:15:"fa fa-paperclip";s:7:"Keyword";s:9:"fa fa-key";s:18:"ContentDescription";s:17:"fa fa-file-text-o";s:18:"ContentDescriptive";s:9:"fa fa-key";s:14:"ArchivalAgency";s:24:"fa fa-building text-info";s:18:"TransferringAgency";s:24:"fa fa-building text-info";s:17:"OriginatingAgency";s:24:"fa fa-building text-info";}s:17:"appendSubnodeText";a:4:{i:0;s:4:"name";i:1;s:10:"identifier";i:2;s:14:"identification";i:3;s:14:"keywordcontent";}s:3:"dom";N;s:9:"namespace";N;s:5:"xpath";N;s:15:"elementsPerPage";i:100;s:15:"maxCharsPerName";i:30;}s:5:"stage";O:35:"Asalae\Model\Entity\ValidationStage":13:{s:11:" * _virtual";a:1:{i:0;s:26:"all_actors_to_completetrad";}s:14:" * _metaFields";a:5:{i:0;s:6:"active";i:1;s:7:"default";i:2;s:15:"created_user_id";i:3;s:9:"modifieds";i:4;s:14:"actors_id_list";}s:10:" * _fields";a:11:{s:2:"id";i:1;s:19:"validation_chain_id";i:1;s:4:"name";s:17:"Etape 1 conformes";s:11:"description";s:0:"";s:22:"all_actors_to_complete";b:0;s:3:"ord";i:1;s:8:"app_type";N;s:8:"app_meta";N;s:7:"created";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 13:54:15.414212";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:8:"modified";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 13:54:15.414243";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:13:"metaFieldName";s:8:"app_meta";}s:12:" * _original";a:0:{}s:10:" * _hidden";a:0:{}s:9:" * _dirty";a:0:{}s:7:" * _new";b:0;s:10:" * _errors";a:0:{}s:11:" * _invalid";a:0:{}s:14:" * _accessible";a:1:{s:1:"*";b:1;}s:17:" * _registryAlias";s:16:"ValidationStages";s:7:" * meta";O:8:"stdClass":0:{}s:13:" * metaFields";a:5:{i:0;s:6:"active";i:1;s:7:"default";i:2;s:15:"created_user_id";i:3;s:9:"modifieds";i:4;s:14:"actors_id_list";}}s:7:"process";O:37:"Asalae\Model\Entity\ValidationProcess":13:{s:11:" * _virtual";a:3:{i:0;s:13:"processedtrad";i:1;s:13:"validatedtrad";i:2;s:8:"steptrad";}s:14:" * _metaFields";a:6:{i:0;s:6:"active";i:1;s:7:"default";i:2;s:15:"created_user_id";i:3;s:9:"modifieds";i:4;s:8:"stepback";i:5;s:15:"created_user_id";}s:10:" * _fields";a:15:{s:13:"metaFieldName";s:8:"app_meta";s:16:"app_subject_type";s:9:"Transfers";s:15:"app_foreign_key";i:8;s:9:"processed";b:0;s:12:"current_step";i:1;s:19:"validation_chain_id";i:1;s:16:"current_stage_id";i:1;s:15:"created_user_id";i:1;s:8:"app_meta";s:21:"{"created_user_id":1}";s:13:"processedtrad";s:8:"En cours";s:13:"validatedtrad";s:0:"";s:8:"steptrad";s:0:"";s:7:"created";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-10 09:28:21.548856";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:8:"modified";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-10 09:28:21.548890";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:2:"id";i:8;}s:12:" * _original";a:0:{}s:10:" * _hidden";a:0:{}s:9:" * _dirty";a:0:{}s:7:" * _new";b:0;s:10:" * _errors";a:0:{}s:11:" * _invalid";a:0:{}s:14:" * _accessible";a:1:{s:1:"*";b:1;}s:17:" * _registryAlias";s:19:"ValidationProcesses";s:7:" * meta";O:8:"stdClass":1:{s:15:"created_user_id";i:1;}s:13:" * metaFields";a:6:{i:0;s:6:"active";i:1;s:7:"default";i:2;s:15:"created_user_id";i:3;s:9:"modifieds";i:4;s:8:"stepback";i:5;s:15:"created_user_id";}}s:5:"actor";O:35:"Asalae\Model\Entity\ValidationActor":13:{s:14:" * _metaFields";a:4:{i:0;s:10:"actor_name";i:1;s:10:"actor_info";i:2;s:15:"created_user_id";i:3;s:15:"type_validation";}s:11:" * _virtual";a:5:{i:0;s:10:"actor_name";i:1;s:12:"app_typetrad";i:2;s:4:"meta";i:3;s:15:"type_validation";i:4;s:19:"type_validationtrad";}s:10:" * _fields";a:10:{s:2:"id";i:2;s:19:"validation_stage_id";i:1;s:8:"app_type";s:4:"MAIL";s:7:"app_key";s:21:"dupont.martin@test.fr";s:8:"app_meta";s:33:"{"actor_name":"M. Dupont Martin"}";s:7:"created";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 13:54:15.430118";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:8:"modified";O:20:"Cake\I18n\FrozenTime":3:{s:4:"date";s:26:"2021-12-09 13:54:15.430144";s:13:"timezone_type";i:3;s:8:"timezone";s:12:"Europe/Paris";}s:15:"app_foreign_key";N;s:10:"actor_name";s:16:"M. Dupont Martin";s:13:"metaFieldName";s:8:"app_meta";}s:12:" * _original";a:0:{}s:10:" * _hidden";a:0:{}s:9:" * _dirty";a:0:{}s:7:" * _new";b:0;s:10:" * _errors";a:0:{}s:11:" * _invalid";a:0:{}s:14:" * _accessible";a:1:{s:1:"*";b:1;}s:17:" * _registryAlias";s:16:"ValidationActors";s:7:" * meta";O:8:"stdClass":1:{s:10:"actor_name";s:16:"M. Dupont Martin";}s:13:" * metaFields";a:4:{i:0;s:10:"actor_name";i:1;s:10:"actor_info";i:2;s:15:"created_user_id";i:3;s:15:"type_validation";}}s:13:"htmlSignature";N;s:13:"textSignature";N;}}}s:18:" * clonedInstances";a:3:{s:7:"message";N;s:8:"renderer";N;s:9:"transport";N;}s:12:" * logConfig";N;s:13:" * modelClass";N;s:18:" * _modelFactories";a:0:{}s:13:" * _modelType";s:5:"Table";s:15:" * defaultTable";N;s:16:" * _tableLocator";N;}',
                'created' => '2021-12-10T09:28:21',
            ],
        ];
        parent::init();
    }
}
