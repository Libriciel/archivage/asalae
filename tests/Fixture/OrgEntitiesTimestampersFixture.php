<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OrgEntitiesTimestampersFixture
 */
class OrgEntitiesTimestampersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'org_entity_id' => 3,
                'timestamper_id' => 1,
            ],
        ];
        parent::init();
    }
}
