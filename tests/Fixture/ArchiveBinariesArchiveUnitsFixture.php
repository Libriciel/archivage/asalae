<?php
namespace Asalae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArchiveBinariesArchiveUnitsFixture
 */
class ArchiveBinariesArchiveUnitsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archive_binary_id' => 1,
                'archive_unit_id' => 2,
            ],
            [// id=2
                'archive_binary_id' => 2,
                'archive_unit_id' => 2,
            ],
            [// id=3
                'archive_binary_id' => 3,
                'archive_unit_id' => 3,
            ],
            [// id=4
                'archive_binary_id' => 5,
                'archive_unit_id' => 4,
            ],
            [// id=5
                'archive_binary_id' => 6,
                'archive_unit_id' => 5,
            ],
        ];
        parent::init();
    }
}
