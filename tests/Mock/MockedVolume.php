<?php
namespace Asalae\Test\Mock;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;

class MockedVolume extends VolumeFilesystem
{
    public function fileDelete(string $storageReference)
    {
        throw new VolumeException(VolumeException::FILE_WRITE_ERROR);
    }
}
