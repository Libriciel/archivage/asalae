<?php

namespace Asalae\Test\Mock;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Driver\Volume\VolumeInterface;

class MockVolume extends VolumeFilesystem implements VolumeInterface
{
    public static $constructExceptions = [];
    public static $pings = [];
    public static $absolutePathExceptions = [];
    public static $hashes = [];
    public static $tests = [];
    /** @var bool[]|bool */
    public static $fileExists;
    public static $streamToExceptions = [];
    public static $fileDeleteExceptions = [];

    public function __construct(array $driverSettings)
    {
        if (empty(self::$constructExceptions)) {
            return parent::__construct($driverSettings);
        }
        if ($e = array_shift(self::$constructExceptions)) {
            throw $e === true ? new VolumeException(VolumeException::FILE_NOT_FOUND) : $e;
        }
        return parent::__construct($driverSettings);
    }

    public function ping(): bool
    {
        if (empty(self::$pings)) {
            return parent::ping();
        }
        return array_shift(self::$pings);
    }

    protected function getAbsolutePath(string $storageReference): string
    {
        if (empty(self::$absolutePathExceptions)) {
            return $this->path.DS.$storageReference;
        }
        if (array_shift(self::$absolutePathExceptions)) {
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        return $this->path.DS.$storageReference;
    }

    public function hash(string $storageReference, string $algo = 'sha256'): string
    {
        if (empty(self::$hashes)) {
            return parent::hash($storageReference, $algo);
        }
        return array_shift(self::$hashes);
    }

    public function test(): array
    {
        if (empty(self::$tests)) {
            return parent::test();
        }
        return array_shift(self::$tests);
    }

    public function streamTo(
        string $storageReference,
        VolumeInterface $target,
        string $targetReference = null
    ): string {
        if (empty(self::$streamToExceptions)) {
            return parent::streamTo($storageReference, $target, $targetReference);
        }
        if ($code = array_shift(self::$streamToExceptions)) {
            $code = $code === true ? VolumeException::FILE_NOT_FOUND : $code;
            throw new VolumeException($code);
        }
        return parent::streamTo($storageReference, $target, $targetReference);
    }

    public function fileDelete(string $storageReference)
    {
        if (empty(self::$fileDeleteExceptions)) {
            parent::fileDelete($storageReference);
            return;
        }
        if ($e = array_shift(self::$fileDeleteExceptions)) {
            throw $e === true ? new VolumeException(VolumeException::FILE_NOT_FOUND) : $e;
        }
        parent::fileDelete($storageReference);
    }

    public function fileExists(string $storageReference): bool
    {
        if (!isset(self::$fileExists)) {
            return parent::fileExists($storageReference);
        }
        return is_array(self::$fileExists) ? array_shift(self::$fileExists) : self::$fileExists;
    }
}
