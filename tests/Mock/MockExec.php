<?php

namespace Asalae\Test\Mock;

use AsalaeCore\Utility\Exec;

class MockExec
{
    public static $outputs = [];

    public static $mock;

    public function __call($name, $arguments)
    {
        if (!isset(self::$outputs[$name])) {
            return call_user_func_array([self::$mock, $name], $arguments);
        } elseif (is_callable(self::$outputs[$name])) {
            return call_user_func_array(self::$outputs[$name], $arguments);
        } else {
            return self::$outputs[$name];
        }
    }

    public static function __callStatic($name, $arguments)
    {
        if (!isset(self::$outputs[$name])) {
            return forward_static_call_array([Exec::class, $name], $arguments);
        } elseif (is_callable(self::$outputs[$name])) {
            return call_user_func_array(self::$outputs[$name], $arguments);
        } else {
            return self::$outputs[$name];
        }
    }
}
