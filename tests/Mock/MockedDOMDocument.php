<?php
namespace Asalae\Test\Mock;

use DOMDocument;

class MockedDOMDocument extends DOMDocument
{
    public function schemaValidate($filename, $options = null)
    {
        return true;
    }
}
