<?php
namespace Asalae\Test\Mock;

use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Driver\Volume\VolumeInterface;

class FakeVolume extends VolumeFilesystem implements VolumeInterface
{
    public function ping(): bool
    {
        return false;
    }
}
