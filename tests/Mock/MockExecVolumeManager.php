<?php

namespace Asalae\Test\Mock;

use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Utility\Exec;
use Cake\ORM\TableRegistry;

class MockExecVolumeManager extends Exec
{

    /**
     * Lance une commande de façon asynchrone - modifié pour être synchrone
     * @param string $command
     * @param mixed  ...$args
     * @throws VolumeException
     */
    public function async($command, ...$args)
    {
        if ($command === CAKE_SHELL
            && count($args) > 3
            && $args[0] === 'volume_manager'
            && $args[1] === 'upload'
        ) {
            [,, $space_id, $filename, $destination, $options] = $args;
            $manager = new VolumeManager($space_id);
            if ($manager->fileExists($destination)) {
                $storedFile = TableRegistry::getTableLocator()->get('StoredFiles')
                    ->find()
                    ->where(
                        [
                            'StoredFiles.secure_data_space_id' => $space_id,
                            'StoredFiles.name' => $destination,
                        ]
                    )
                    ->firstOrFail();
                $manager->replace($storedFile->id, $filename);
            } else {
                $manager->fileUpload($filename, $destination);
            }
            if (isset($options['--sleep-then-delete']) && is_file($filename.'.lock')) {
                unlink($filename.'.lock');
            }
        }
    }
}
