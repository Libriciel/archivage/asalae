<?php

namespace Asalae\Test\Mock;

use AsalaeCore\Utility\Session;

class MockSession extends Session
{
    protected static function getSecurityKey(): string
    {
        return hash('sha256', 'testunit');
    }
}
