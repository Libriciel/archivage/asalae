<?php

namespace Asalae\Test\Mock;

use AsalaeCore\Driver\Timestamping\TimestampingInterface;
use Exception;

class TimestampingDriver implements TimestampingInterface
{
    public static $params;

    public function __construct(...$params)
    {
        static::$params = $params;
    }

    /**
     * Vérifie que le service est accessible (ne consomme pas de jeton)
     * @return bool
     */
    public function ping(): bool
    {
        return true;
    }

    /**
     * Test la connexion au service (peut consommer des jetons)
     * @return bool
     */
    public function check(): bool
    {
        return true;
    }

    /**
     * Genère un token à partir d'un fichier
     * @param resource|string $file
     * @return string
     * @throws Exception
     */
    public function generateToken($file): string
    {
        return 'token';
    }
}
