<?php
namespace Asalae\Test\Mock;

class FakeCheck
{
    public function __call($func, $args)
    {
        return ['test' => true];
    }
}
