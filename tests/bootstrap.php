<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

declare(strict_types=1);

use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Entity\Role;
use Asalae\Model\Entity\TypeEntity;
use Asalae\Model\Entity\User;
use Asalae\Model\Table\VolumesTable;
use Asalae\Test\Fixture\OrgEntitiesFixture;
use Asalae\Test\Fixture\RolesFixture;
use Asalae\Test\Fixture\TypeEntitiesFixture;
use Asalae\Test\Fixture\UsersFixture;
use AsalaeCore\Utility\DatabaseUtility;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Libriciel\Filesystem\Utility\Filesystem;
use Migrations\TestSuite\Migrator;

/**
 * Test runner bootstrap.
 *
 * Add additional configuration/setup your application needs when running
 * unit tests in this file.
 */
require dirname(__DIR__) . '/vendor/autoload.php';

require dirname(__DIR__) . '/config/bootstrap.php';

if (empty($_SERVER['HTTP_HOST']) && !Configure::read('App.fullBaseUrl')) {
    Configure::write('App.fullBaseUrl', 'http://localhost');
}

define(
    'FIXTURES_AUTH',
    [
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
        'app.Configurations',
        'app.Ldaps',
        'app.OrgEntities',
        'app.Roles',
        'app.Sessions',
        'app.Timestampers',
        'app.TypeEntities',
        'app.Users',
        'app.Versions',
    ]
);
define(
    'FIXTURES_APP_BEFORE_FILTER',
    [
        'app.Filters',
        'app.Notifications',
        'app.SavedFilters',
    ]
);
define(
    'FIXTURES_EVENT_LOGS',
    [
        'app.AccessRuleCodes',
        'app.Agreements',
        'app.AppraisalRuleCodes',
        'app.ArchiveBinaries',
        'app.ArchiveBinariesTechnicalArchiveUnits',
        'app.ArchiveFiles',
        'app.ArchiveKeywords',
        'app.ArchiveUnits',
        'app.ArchiveUnitsRestitutionRequests',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.Counters',
        'app.CronExecutions',
        'app.Crons',
        'app.Deliveries',
        'app.EventLogs',
        'app.KeywordLists',
        'app.OrgEntities',
        'app.Profiles',
        'app.Roles',
        'app.SecureDataSpaces',
        'app.ServiceLevels',
        'app.TechnicalArchives',
        'app.TechnicalArchiveUnits',
        'app.Timestampers',
        'app.Transfers',
        'app.Users',
        'app.ValidationChains',
        'app.Versions',
        'app.Volumes',
    ]
);
define(
    'FIXTURES_VOLUMES',
    [
        'app.SecureDataSpaces',
        'app.StoredFiles',
        'app.StoredFilesVolumes',
        'app.Volumes',
    ]
);
if (!defined('SAMPLES_CONVERTERS')) {
    define(
        'SAMPLES_CONVERTERS',
        dirname(__DIR__) . DS . 'vendor' . DS . 'libriciel' . DS . 'file-converters-tests' . DS . 'tests'
    );
}
if (!defined('SAMPLES_VALIDATOR')) {
    define(
        'SAMPLES_VALIDATOR',
        dirname(__DIR__) . DS . 'vendor' . DS . 'libriciel' . DS . 'file-validator-tests' . DS . 'tests'
    );
}

$users = UsersFixture::defaultValues();
$user = new User($users[0], ['validate' => false]); // user testunit
$user['id'] = 1;
$entities = OrgEntitiesFixture::defaultValues();
$entity = new OrgEntity($entities[1], ['validate' => false]); // service d'archives
$entity['id'] = 2;
$typeEntities = TypeEntitiesFixture::defaultValues();
$typeEntity = new TypeEntity($typeEntities[1], ['validate' => false]);
$entity['type_entity'] = $typeEntity;
$archivalAgency = new OrgEntity($entities[1], ['validate' => false]);
$archivalAgency['id'] = 2;
$entity['archival_agency'] = $archivalAgency;
$user['org_entity'] = $entity;
$roles = RolesFixture::defaultValues();
$role = new Role($roles[0], ['validate' => false]);
$role['id'] = 1;
$user['role'] = $role; // archiviste
putenv('PHPUNIT=true');
define('GENERIC_SESSION_DATA', serialize(['Auth' => $user, 'Session' => ['token' => 'testunit']]));

if (getenv('TEST_TOKEN')) {
    putenv('NO_COLOR=true'); // désactive les couleurs de debug pour un bon affichage avec paratest
}
$tokenSuffix = getenv('TEST_TOKEN') ? '_' . getenv('TEST_TOKEN') : '';
if (!defined('TMP_TESTDIR')) {
    define('TMP_TESTDIR', TMP . 'testunit' . $tokenSuffix);
}
if (!defined('TMP_VOL1')) {
    define('TMP_VOL1', TMP . 'volume01' . $tokenSuffix);
}
if (!defined('TMP_VOL2')) {
    define('TMP_VOL2', TMP . 'volume02' . $tokenSuffix);
}
if (!defined('TMP_VOL3')) {
    define('TMP_VOL3', TMP . 'volume03' . $tokenSuffix);
}

Configure::write('Keycloak.enabled', false);
Configure::write('OpenIDConnect.enabled', false);

Configure::write('FilesystemUtility.tmpDir', rtrim(TMP, '/'));
Configure::write('FilesystemUtility.useShred', false);
Filesystem::clear();

// Permet les tests en docker standalone
if (!is_dir(Configure::read('App.paths.data'))) {
    mkdir(Configure::read('App.paths.data'), recursive: true);
}

// Fixate sessionid early on, as php7.2+
// does not allow the sessionid to be set after stdout
// has been written to.
session_id('cli');

// create schema
ConnectionManager::alias('test', 'default');
$pdo = DatabaseUtility::getPdo();
if ($pdo->getAttribute(PDO::ATTR_DRIVER_NAME) === 'sqlite') {
    $pdo->exec('PRAGMA journal_mode=WAL;');
}
try {
    VolumesTable::$skipReadCalculation = true;
    $migrator = new Migrator();
    $migrator->run();
    VolumesTable::$skipReadCalculation = false;
} catch (Exception $e) {
    forcedebug($e);
    die;
}
TableRegistry::getTableLocator()->clear();
