<?php

namespace Asalae\Test\TestCase\Http\Exception;

use Asalae\Exception\KilledException;
use AsalaeCore\TestSuite\TestCase;

class KilledExceptionTest extends TestCase
{
    /**
     * testConstruct
     */
    public function testConstruct()
    {
        $e = new KilledException();
        $this->assertEquals(499, $e->getCode());
    }
}
