<?php

namespace Asalae\Test\TestCase;

use AsalaeCore\TestSuite\TestCase;

/**
 * Asalae\Translations Test Case
 */
class TranslationsTest extends TestCase
{
    /**
     * testTraductions
     */
    public function testTraductions()
    {
        exec(sprintf('php %s', escapeshellarg(WWW_ROOT . '/js/asalae.translation.js.php')), $output);
        $output = implode("\n", $output);
        $traductions = include APP . 'translations.php';
        $this->assertTrue(!empty($traductions) && is_array($traductions));
        $this->assertTextContains(array_keys($traductions)[0], $output);
    }
}
