<?php

namespace Asalae\Test\TestCase;

use Asalae\Application;
use Asalae\Auth\AuthenticationService;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client as CoreClient;
use AsalaeCore\TestSuite\TestCase;
use Authentication\AuthenticationServiceInterface;
use Authentication\Identity;
use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Http\Client;
use Cake\Http\ServerRequest;
use Cake\Routing\Router;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use ReflectionException;

class ApplicationTest extends TestCase
{
    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('debug', true);
        Configure::write('App.fullBaseUrl', 'https://test.localhost');
    }

    /**
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Configure::write('Keycloak.enabled', false);
        Configure::write('OpenIDConnect.enabled', false);
    }

    /**
     * @return void
     */
    public function testBootstrap()
    {
        $app = new Application(ROOT . DS . 'config');
        $app->bootstrap();
        $this->assertTrue($app->getPlugins()->has('AsalaeCore'));

        /** @var MockObject|Application $app */
        $app = $this->getMockBuilder(Application::class)
            ->onlyMethods(['addPlugin'])
            ->setConstructorArgs([ROOT . DS . 'config'])
            ->getMock();
        $app->method('addPlugin')->willReturnCallback(
            function ($plugin) {
                if (in_array($plugin, ['DebugKit', 'Bake'])) {
                    throw new MissingPluginException();
                }
            }
        );
        $app->bootstrap();
        $this->assertFalse($app->getPlugins()->has('DebugKit'));
    }

    /**
     * @return void
     * @throws ReflectionException
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testgetAuthenticationService()
    {
        $app = new Application(ROOT . DS . 'config');
        $app->bootstrap();

        $builder = Router::createRouteBuilder('/');
        $app->routes($builder);

        // test admins
        $request = new ServerRequest(['url' => Router::url('/admins/index')]);
        $service = $app->getAuthenticationService($request);
        $this->assertInstanceOf(AuthenticationServiceInterface::class, $service);
        $this->assertEquals(
            '/admins/login',
            $app->authentication['service']['unauthenticatedRedirect']
        );

        // test keycloak
        Configure::write(
            'Keycloak',
            [
                'enabled' => true,
                'base_url' => 'http://localhost:5749',
                'realm' => 'master',
                'client_id' => 'asalae',
                'client_secret' => 'secret',
            ]
        );
        $request = new ServerRequest(['url' => Router::url('/transfers/index')]);
        $service = $app->getAuthenticationService($request);
        $this->assertInstanceOf(AuthenticationServiceInterface::class, $service);
        $this->assertEquals(
            'http://localhost:5749/auth/realms/master/protocol/'
            . 'openid-connect/auth?response_type=code&client_id=asalae',
            $app->authentication['service']['unauthenticatedRedirect']
        );

        // test openid
        $json = json_encode(
            [
                'authorization_endpoint' => 'http://localhost:5749',
            ],
            JSON_UNESCAPED_SLASHES
        );
        $response = $this->createMock(Client\Response::class);
        $response->method('getStringBody')->willReturn($json);
        $response->method('getStatusCode')->willReturn(200);
        $client = $this->createMock(\Cake\Http\Client::class);
        $client->method('get')->willReturn($response);
        Utility::set(CoreClient::class, $client);
        Configure::write('Keycloak.enabled', false);
        Configure::write(
            'OpenIDConnect',
            [
                'enabled' => true,
                'base_url' => 'http://localhost:5749/auth/realms/master',
                'client_id' => 'asalae',
                'username' => 'preferred_username',
                'scope' => 'openid',
                'redirect_uri' => 'auto',
                'client_secret' => 'secret',
            ]
        );
        $service = $app->getAuthenticationService($request);
        $this->assertInstanceOf(AuthenticationServiceInterface::class, $service);
        $this->assertEquals(
            'http://localhost:5749?response_type=code&client_id=asalae'
            . '&redirect_uri=https://test.localhost/&scope=openid',
            urldecode($app->authentication['service']['unauthenticatedRedirect'])
        );

        // test exception openidconnect
        $response = $this->createMock(Client\Response::class);
        $response->method('getStringBody')->willReturn('');
        $client = $this->createMock(\Cake\Http\Client::class);
        $client->method('get')->willReturn($response);
        Utility::set(CoreClient::class, $client);
        $e = null;
        try {
            $app->getAuthenticationService($request);
        } catch (Exception $e) {
        }
        $this->assertNotNull($e);
        Configure::write('OpenIDConnect.enabled', false);

        // test XDEBUG_SESSION_START
        $app->authentication = [
            'service' => [
                'className' => AuthenticationService::class,
                'identityClass' => Identity::class,
                'unauthenticatedRedirect' => '/Users/login',
                'queryParam' => 'redirect',
            ],
            'keycloak' => false,
            'openid-connect' => false,
        ];
        $request = new ServerRequest(['url' => Router::url('/transfers/index?XDEBUG_SESSION_START=test')]);
        $service = $app->getAuthenticationService($request);
        $this->assertInstanceOf(AuthenticationServiceInterface::class, $service);
        $this->assertEquals(
            '/Users/login?XDEBUG_SESSION_START=test',
            $app->authentication['service']['unauthenticatedRedirect']
        );
    }
}
