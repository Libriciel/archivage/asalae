<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Test\Mock\MockExec;
use Asalae\TestSuite\VolumeSample;
use Asalae\Worker\OutgoingTransferWorker;
use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;

use const DS;
use const TMP_TESTDIR;

class OutgoingTransferWorkerTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        FIXTURES_EVENT_LOGS,
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveUnits',
        'app.ArchiveUnitsOutgoingTransferRequests',
        'app.AuthUrls',
        'app.OutgoingTransferRequests',
        'app.OutgoingTransfers',
        'app.TransferLocks',
    ];
    private $debugFixtures = false;
    /**
     * @var OutgoingTransferWorker|MockObject
     */
    private $worker;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.disable_check_ttr', true);

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $this->worker = $this->getMockBuilder(OutgoingTransferWorker::class)
            ->setConstructorArgs([new Entity(['tube' => 'outgoing-transfer']), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $this->worker->logfile = TMP_TESTDIR . DS . 'logfile.log';

        Filesystem::reset();
        VolumeSample::init();
        VolumeSample::insertSample(
            'sample_seda10_description.xml',
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
        );
        Configure::write('App.paths.data', TMP_TESTDIR);
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Utility::reset();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $loc = TableRegistry::getTableLocator();
        $OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
        $OutgoingTransferRequests->updateAll(['state' => 'accepted'], ['id' => 1]);
        $OutgoingTransfers = $loc->get('OutgoingTransfers');
        $OutgoingTransfers->updateAll(['state' => 'creating', 'archive_unit_id' => 1], ['id' => 1]);
        $ArchiveUnitsOutgoingTransferRequests = $loc->get('ArchiveUnitsOutgoingTransferRequests');
        $ArchiveUnitsOutgoingTransferRequests->updateAll(['archive_unit_id' => 1], ['id' => 1]);
        $Exec = $this->createMock(Exec::class);
        $Exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => 'done', 'stderr' => ''])
        );
        MockExec::$mock = $Exec;
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'outgoing-transfers/1/attachments/sample.wmv', 'test');
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'outgoing-transfers/sa_OAT_1.zip', 'test');
        Utility::set('Exec', new MockExec());
        $this->worker->work(['outgoing_transfer_request_id' => 1]);

        $otr = $OutgoingTransferRequests->find()
            ->where(['OutgoingTransferRequests.id' => 1])
            ->contain(['OutgoingTransfers'])
            ->firstOrFail();
        /** @var EntityInterface $outgoingTransfer */
        $outgoingTransfer = Hash::get($otr, 'outgoing_transfers.0');
        $this->assertEquals('transfers_sent', $otr->get('state'));
        $this->assertEquals('sent', $outgoingTransfer->get('state'));
    }
}
