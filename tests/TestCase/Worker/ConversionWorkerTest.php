<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Model\Volume\VolumeManager;
use Asalae\TestSuite\VolumeSample;
use Asalae\Worker\ConversionWorker;
use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use FileConverters\Utility\DetectFileFormat;

use const DS;
use const TMP_TESTDIR;

class ConversionWorkerTest extends TestCase
{
    use InvokePrivateTrait;
    use AutoFixturesTrait;
    use ConsoleIntegrationTestTrait;

    public const string SAMPLE_DIR = ROOT . DS . 'vendor' . DS . 'libriciel' . DS . 'file-converters-tests' . DS . 'tests' . DS . 'samples' . DS;
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_EVENT_LOGS,
        FIXTURES_VOLUMES,
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveBinaryConversions',
        'app.ArchiveIndicators',
        'app.Configurations',
        'app.OrgEntitiesTimestampers',
        'app.Timestampers',
        'app.TransferLocks',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Utility::reset();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('Beanstalk.disable_check_ttr', true);
        Configure::write('Conversion.timeout', 36000);
        Configure::write('Conversion.output_std', true);
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('Conversions.tmpDir', TMP_TESTDIR . DS . 'conversions');
        $Exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(['async', 'command', 'proc'])
            ->getMock();
        $fn = function ($command, ...$args) {
            $h = fopen('/tmp/test.log', 'a');
            fwrite($h, $command . var_export($args, true) . "\n");
            fclose($h);
            if ($command === CAKE_SHELL) {
                $escaped = $this->extractArgsShellOptions($args);
                $command = implode(' ', $escaped);
                $out = new ConsoleOutput();
                $err = new ConsoleOutput();
                $io = new ConsoleIo($out, $err, new ConsoleInput([]));
                $runner = $this->makeRunner();
                $args = $this->commandStringToArgs("cake $command");
                $code = $runner->run($args, $io);
                if (is_file(TMP_TESTDIR . '/update_lifecycle/.lock')) {
                    unlink(TMP_TESTDIR . '/update_lifecycle/.lock');
                }
                return new CommandResult(
                    [
                        'success' => $code === 0,
                        'code' => $code,
                        'stdout' => implode(PHP_EOL, $out->messages()),
                        'stderr' => implode(PHP_EOL, $err->messages()),
                    ]
                );
            } else {
                $Exec = new Exec();
                return $Exec->command($command, ...$args);
            }
        };
        $Exec->method('async')->willReturnCallback($fn);
        $Exec->method('command')->willReturnCallback($fn);
        $Exec->method('proc')->willReturnCallback(fn($command) => $fn($command)->code);
        Utility::set(Exec::class, $Exec);
        Utility::set('Exec', $Exec);
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * extractArgsShellOptions
     * @param array $args
     * @return array
     */
    private function extractArgsShellOptions(array $args): array
    {
        $escaped = [];
        foreach ($args as $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $escaped[] = $k;
                    if ($v) {
                        $escaped[] = $v;
                    }
                }
            } else {
                $escaped[] = $value;
            }
        }
        return $escaped;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $archive = $Archives->get(1);
        $ArchiveBinariesArchiveUnits = $loc->get('ArchiveBinariesArchiveUnits');
        $ArchiveBinariesArchiveUnits->deleteAll(['archive_binary_id !=' => 1]);
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $ArchiveBinaries->deleteAll(['type !=' => 'original_data']);
        $volumeManager = new VolumeManager(1);
        $volumeManager->fileDelete('sa/no_profile/2021-12/sa_1/preservation_data/sample_wmv.mp4');
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = $this->getMockBuilder(ConversionWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();

        // test
        $data = ['archive_id' => 1];
        $worker->beforeWork($data);
        $worker->work($data);

        $tmpFile = TMP_TESTDIR . DS . 'sample_wmv.mp4';
        $volumeManager->fileDownload(
            $archive->get('storage_path') . '/preservation_data/sample_wmv.mp4',
            $tmpFile
        );
        $detector = new DetectFileFormat($tmpFile);
        $this->assertNotEmpty($detector);
        $codecs = $detector->getCodecs();
        $this->assertArrayHasKey('container', $codecs);
        $this->assertArrayHasKey('video', $codecs);
        $this->assertArrayHasKey('audio', $codecs);
        $this->assertEquals('mov,mp4,m4a,3gp,3g2,mj2', $codecs['container']);
        $this->assertEquals('h264', $codecs['video']);
        $this->assertEquals('aac', $codecs['audio']);

        $binary = $ArchiveBinaries->find()
            ->where(['type' => 'preservation_data', 'filename' => 'sample_wmv.mp4'])
            ->first();
        $this->assertNotFalse($binary);
        $this->assertEquals('video/mp4', $binary->get('mime'));
    }
}
