<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Model\Entity\Delivery;
use Asalae\TestSuite\VolumeSample;
use Asalae\Worker\DeliveryWorker;
use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Datacompressor\Utility\DataCompressor;
use DOMDocument;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;

use const DS;
use const SEDA_V21_XSD;
use const TMP_TESTDIR;
use const TMP_VOL1;
use const TMP_VOL2;

class DeliveryWorkerTest extends TestCase
{
    use SaveBufferTrait;
    use AutoFixturesTrait;

    public const string SEDA_10_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml';
    public const string LIFECYCLE_PREMIS = ASALAE_CORE_TEST_DATA . DS . 'sample_premis.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.Acos',
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveFiles',
        'app.ArchiveUnits',
        'app.ArchiveUnitsDeliveryRequests',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.Counters',
        'app.Crons',
        'app.Deliveries',
        'app.DeliveryRequests',
        'app.EventLogs',
        'app.OrgEntities',
        'app.Sequences',
        'app.TransferLocks',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
        'app.ValidationChains',
    ];
    /**
     * @var DeliveryWorker|MockObject $worker
     */
    public $worker;
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.disable_check_ttr', true);
        $deliveryWorker = $this->createMock(DeliveryWorker::class);
        Utility::set(DeliveryWorker::class, $deliveryWorker);

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $this->worker = $this->getMockBuilder(DeliveryWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();

        $exec = $this->createMock(Exec::class);
        $exec->method('command')->willReturn(
            new CommandResult(['success' => false, 'code' => 500, 'stdout' => '', 'stderr' => ''])
        );
        Utility::set('Exec', $exec);

        Configure::write('App.paths.data', TMP_TESTDIR);
        Filesystem::reset();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Filesystem::setNamespace();
        Filesystem::reset();
        Exec::waitUntilAsyncFinish();
        Utility::reset();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $DeliveryRequests = TableRegistry::getTableLocator()->get('DeliveryRequests');
        $Deliveries = TableRegistry::getTableLocator()->get('Deliveries');
        $Deliveries->deleteAll([]);
        $DeliveryRequests->updateAll(['state' => 'accepted'], ['id' => 1]);
        $this->worker->work(['delivery_request_id' => 1]);
        $this->assertTrue(true);
        $this->assertEquals('delivery_available', $DeliveryRequests->get(1)->get('state'));
        $this->assertCount(1, $Deliveries->find());
        /** @var Delivery $delivery */
        $delivery = $Deliveries->find()->first();
        $this->assertEquals('available', $delivery->get('state'));
        $zip = $delivery->get('zip');
        $this->assertFileExists($zip);
        $this->assertTextContains('zip', mime_content_type($zip));

        // test des fichiers à l'interieur du zip
        $path = TMP_TESTDIR . DS . 'zip';
        DataCompressor::uncompress($zip, $path);
        $xml = $path . DS . basename($delivery->get('xml')); // ADL_1_delivery.xml
        $this->assertFileExists($xml);
        $dom = new DOMDocument();
        $dom->load($xml);
        $this->assertTrue($dom->schemaValidate(SEDA_V21_XSD));
        $this->assertFileExists($path . '/sa_1/management_data/sa_1_lifecycle.xml');
        $this->assertFileExists($path . '/sa_1/management_data/sa_1_description.xml');
        $this->assertFileExists($path . '/sa_ADL_2_delivery.xml');
    }
}
