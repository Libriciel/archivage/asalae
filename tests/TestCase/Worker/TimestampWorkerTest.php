<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Model\Table\TimestampersTable;
use Asalae\Test\Mock\TimestampingDriver;
use Asalae\TestSuite\VolumeSample;
use Asalae\Worker\TimestampWorker;
use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Notify;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;

use const DS;
use const TMP_TESTDIR;

class TimestampWorkerTest extends TestCase
{
    public array $fixtures = [
        'app.Transfers',
        'app.Timestampers',
        'app.TransferAttachments',
        'app.OrgEntities',
        'app.OrgEntitiesTimestampers',
        'app.ServiceLevels',
    ];

    /**
     * @var TimestampWorker|MockObject $worker
     */
    public $worker;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $this->worker = $this->getMockBuilder(TimestampWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();

        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('Beanstalk.disable_check_ttr', true);
        Filesystem::reset();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write(
            'Timestamping.drivers.test',
            [
                'name' => 'Mock class',
                'class' => TimestampingDriver::class,
                'fields' => [
                    'read_conf' => [
                        'read_config' => 'Timestamping.drivers.test.name',
                    ],
                    'password' => [
                        'type' => 'password',
                        'value' => Security::encrypt('foo', md5(TimestampersTable::SECURITY_KEY)),
                    ],
                    'id' => [
                        'hidden' => true,
                    ],
                    'test' => [
                        'default' => 'test',
                    ],
                    'no_value' => [],
                ],
            ]
        );
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
        Filesystem::reset();
        VolumeSample::destroy();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');

        // pas de transfer 0
        try {
            $this->worker->work(['transfer_id' => 0]);
            $this->fail();
        } catch (Exception $e) {
            $this->assertInstanceOf(CantWorkException::class, $e);
        }

        // n'est pas dans un etat d'horodatage
        try {
            $this->worker->work(['transfer_id' => 1]);
            $this->fail();
        } catch (Exception $e) {
            $this->assertInstanceOf(CantWorkException::class, $e);
        }

        $Transfers->updateAll(['state' => 'timestamping'], ['id' => 1]);
        $this->worker->work(['transfer_id' => 1, 'user_id' => 1]);
        $this->assertEquals('analysing', $Transfers->get(1)->get('state'));
        $Timestampers = TableRegistry::getTableLocator()->get('Timestampers');
        $Timestampers->updateAll(
            [
                'fields' => json_encode(
                    [
                        'driver' => 'test',
                        'password' => base64_encode(
                            Security::encrypt('foo', md5(TimestampersTable::SECURITY_KEY))
                        ),
                    ],
                    JSON_UNESCAPED_SLASHES
                ),
            ],
            ['id' => 1]
        );

        Filesystem::dumpFile(TMP_TESTDIR . DS . 'transfers/1/message/testfile.xml', 'test');
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'transfers/1/attachments/file', 'test');
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'transfers/1/attachments/sample.wmv', 'test');
        $this->assertFileDoesNotExist(TMP_TESTDIR . DS . 'transfers/1/timestamp/message/testfile.xml.tsr');
        $this->assertFileDoesNotExist(TMP_TESTDIR . DS . 'transfers/1/timestamp/attachments/file.tsr');
        $this->assertFileDoesNotExist(TMP_TESTDIR . DS . 'transfers/1/timestamp/attachments/sample.pdf.tsr');

        $Transfers->updateAll(['state' => 'timestamping'], ['id' => 1]);
        $this->worker->work(
            [
                'transfer_id' => 1,
                'ts_msg_id' => 1,
                'ts_pjs_id' => 1,
                'user_id' => 1,
            ]
        );
        $this->assertEquals('analysing', $Transfers->get(1)->get('state'));
        $this->assertFileExists(TMP_TESTDIR . DS . 'transfers/1/timestamp/message/transfer-61b20d8ded833.xml.tsr');
        $this->assertFileExists(TMP_TESTDIR . DS . 'transfers/1/timestamp/attachments/sample.wmv.tsr');

        $expected = [
            0 => 'Mock class',
            1 => 'foo',
            2 => 1,
            3 => 'test',
            4 => '',
        ];
        $this->assertEquals($expected, TimestampingDriver::$params);
    }
}
