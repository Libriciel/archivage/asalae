<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Volume\VolumeManager;
use Asalae\TestSuite\VolumeSample;
use Asalae\Worker\ArchiveBinaryWorker;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Notify;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\HasOne;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const TMP_TESTDIR;
use const TMP_VOL1;
use const TMP_VOL2;

class ArchiveBinaryWorkerTest extends TestCase
{
    use InvokePrivateTrait;
    use AutoFixturesTrait;

    public const string SEDA_02_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xml';
    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';
    public const string SEDA_02_XSD = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xsd';
    public const array ATTACHMENTS = [
        ASALAE_CORE_TEST_DATA . DS . 'sample.odt',
        ASALAE_CORE_TEST_DATA . DS . 'sample.pdf',
        ASALAE_CORE_TEST_DATA . DS . 'sample.rng',
    ];
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_EVENT_LOGS,
        FIXTURES_VOLUMES,
        'app.AccessRuleCodes',
        'app.AccessRules',
        'app.AppraisalRuleCodes',
        'app.AppraisalRules',
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveBinaryConversions',
        'app.ArchiveUnits',
        'app.CompositeArchiveUnits',
        'app.Configurations',
        'app.TransferAttachments',
        'app.TransferLocks',
        'app.TypeEntities',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('Beanstalk.disable_check_ttr', true);
        Filesystem::reset();
        VolumeSample::init();
        Filesystem::setNamespace();
        Configure::write('ArchiveUnitWorker.saveRefs', TMP_TESTDIR . DS);
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $worker = $this->prepareTransfer();
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $ArchiveBinariesArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveBinariesArchiveUnits');

        $manager = new VolumeManager(1);
        $archiveUnits = $ArchiveUnits->find()
            ->orderBy(['ArchiveUnits.lft' => 'desc'])
            ->contain(
                [
                    // supprime en 1er les original_data_id non null
                    'ArchiveBinariesArchiveUnits' => function (Query $q) {
                        return $q
                            ->innerJoinWith('ArchiveBinaries')
                            ->orderBy(['ArchiveBinaries.original_data_id is null']);
                    },
                ]
            );
        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = TableRegistry::getTableLocator()->get('ArchiveBinaries');
        /** @var EntityInterface $unit */
        foreach ($archiveUnits as $unit) {
            $ArchiveBinariesArchiveUnits->deleteAll(['archive_unit_id' => $unit->id]);
            $binaries = Hash::extract($unit, 'archive_binaries_archive_units.{n}.archive_binary_id');
            $this->createLinkToBinaries($binaries, $manager);
            foreach ($ArchiveUnits->associations() as $assoc) {
                if (
                    ((!$assoc instanceof HasMany) && (!$assoc instanceof hasOne))
                    || $assoc->getSource() instanceof ArchiveUnitsTable
                ) {
                    continue;
                }
                $assoc->deleteAll([$assoc->getForeignKey() => $unit->id]);
            }
        }

        // SEDA 1.0
        $data = ['transfer_id' => 1];
        $worker->beforeWork($data);
        $worker->work($data);
        $this->assertEquals(
            1,
            $ArchiveBinaries->find()->innerJoinWith('ArchiveUnits')->where(['archive_id' => 1])->count()
        );
        $au1 = $ArchiveUnits->get(1);
        $au2 = $ArchiveUnits->get(2);
        $this->assertEquals(1, $au1->get('original_total_count'));
        $this->assertEquals(0, $au1->get('original_local_count'));
        $this->assertEquals(1, $au2->get('original_total_count'));
        $this->assertEquals(1, $au2->get('original_local_count'));
        $a = $Archives->get(2);
        $this->assertNotEmpty($a);
        $this->assertEquals(5, $a->get('transferred_size'));
        $this->assertEquals(5, $a->get('original_size'));
        $this->assertEquals(1, $a->get('transferred_count'));
        $this->assertEquals(1, $a->get('original_count'));
    }

    /**
     * prepareTransfer
     */
    private function prepareTransfer()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        if (!is_dir(TMP_TESTDIR)) {
            mkdir(TMP_TESTDIR);
        }
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01 ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02 ' . TMP_VOL2);

        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $Archives->updateAll(['state' => 'describing'], []);

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        return $this->getMockBuilder(ArchiveBinaryWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
    }

    /**
     * createLinkToBinaries
     * @param int[]         $binaries
     * @param VolumeManager $manager
     * @return void
     * @throws VolumeException
     */
    private function createLinkToBinaries(array $binaries, VolumeManager $manager)
    {
        $ArchiveBinariesArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveBinariesArchiveUnits');
        /** @var ArchiveBinariesTable $ArchiveBinaries */
        $ArchiveBinaries = TableRegistry::getTableLocator()->get('ArchiveBinaries');
        /** @var StoredFilesTable $StoredFiles */
        $StoredFiles = TableRegistry::getTableLocator()->get('StoredFiles');
        foreach ($binaries as $binary_id) {
            // vérifi si il reste un lien vers le binary, si il n'y en a plus on supprime
            $exists = $ArchiveBinariesArchiveUnits->exists(['archive_binary_id' => $binary_id]);
            if (!$exists) {
                $archiveBinary = $ArchiveBinaries->find()
                    ->where(['ArchiveBinaries.id' => $binary_id])
                    ->contain(['StoredFiles'])
                    ->firstOrFail();
                foreach ($ArchiveBinaries->associations() as $assoc) {
                    if ((!$assoc instanceof HasMany) && (!$assoc instanceof hasOne)) {
                        continue;
                    }
                    $assoc->deleteAll([$assoc->getForeignKey() => $binary_id]);
                }
                $ArchiveBinaries->delete($archiveBinary);
                $storedFile = $archiveBinary->get('stored_file');
                $manager->fileDelete($storedFile->get('name'));
                $StoredFiles->delete($storedFile);
            }
        }
    }

    /**
     * testConversions
     */
    public function testConversions()
    {
        $worker = $this->prepareTransfer();
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $ServiceLevels->updateAll(['convert_preservation' => 'video', 'convert_dissemination' => 'video'], []);
        $ArchiveBinaryConversions = TableRegistry::getTableLocator()->get('ArchiveBinaryConversions');
        $ArchiveBinaryConversions->deleteAll([]);

        $data = ['transfer_id' => 1];
        $worker->beforeWork($data);
        $worker->work($data);

        $this->assertCount(2, $ArchiveBinaryConversions->find());
    }

    /**
     * testComposite
     */
    public function testComposite()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $Archives->updateAll(['state' => ArchivesTable::S_UPDATE_DESCRIBING], ['id' => 2]);
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $CompositeArchiveUnits = $loc->get('CompositeArchiveUnits');
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        $ArchivesTransfers = $loc->get('ArchivesTransfers');
        $transfer = $Transfers->newEntityFromXml(ASALAE_CORE_TEST_DATA . DS . 'sample_seda21_composite.xml');
        $transfer->set('state', TransfersTable::S_ARCHIVING);
        $transfer->set('files_deleted', false);
        $Transfers->saveOrFail($transfer);
        Filesystem::copy(
            ASALAE_CORE_TEST_DATA . DS . 'sample_seda21_composite.xml',
            $transfer->get('xml')
        );
        $ArchivesTransfers->findOrCreate(
            [
                'archive_id' => 2,
                'transfer_id' => $transfer->id,
            ]
        );
        $initialBinariesCount = $ArchiveBinaries->find()
            ->innerJoinWith('ArchiveUnits')
            ->where(['archive_id' => 2])->count();

        $ArchiveUnits->updateAll(['lft' => 5, 'rght' => 8], ['id' => 3]);
        $archiveUnit = $ArchiveUnits->findOrCreate(
            [
                'state' => 'creating',
                'archive_id' => 2,
                'access_rule_id' => 3,
                'appraisal_rule_id' => 2,
                'archival_agency_identifier' => 'sa_2_0001',
                'transferring_agency_identifier IS' => null,
                'originating_agency_identifier IS' => null,
                'name' => 'composite',
                'original_local_size' => 0,
                'original_total_size' => 0,
                'parent_id' => 3,
                'lft' => 6,
                'rght' => 7,
                'xml_node_tagname' => 'ArchiveUnit[1]',
                'original_total_count' => 0,
                'original_local_count' => 0,
                'search' => '"sa_2_0001" "composite" "cette unite d\'archives' .
                    ' va etre insere dans sa_2_description.xml"',
                'expired_dua_root' => false,
                'full_search' => '"sa_2_0001" "composite" "cette unite ' .
                    'd\'archives va etre insere dans sa_2_description.xml"',
            ]
        );
        $CompositeArchiveUnits->findOrCreate(
            [
                'transfer_id' => $transfer->id,
                'archive_unit_id' => $archiveUnit->id,
                'transfer_xpath' => '/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata/ns:ArchiveUnit',
            ]
        );
        Filesystem::copy(
            ASALAE_CORE_TEST_DATA . DS . 'logo.png',
            TMP_TESTDIR . DS . 'logo.png'
        );
        $TransferAttachments = $loc->get('TransferAttachments');
        $attachment = $TransferAttachments->newEntity(
            [
                'transfer_id' => $transfer->id,
                'filename' => 'logo.png',
                'size' => filesize(TMP_TESTDIR . DS . 'logo.png'),
                'hash' => 'foo',
                'hash_algo' => 'bar',
            ]
        );
        $TransferAttachments->saveOrFail($attachment);
        Filesystem::rename(
            TMP_TESTDIR . DS . 'logo.png',
            $attachment->get('path')
        );

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = $this->getMockBuilder(ArchiveBinaryWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        $data = ['transfer_id' => $transfer->id, 'composite_id' => 2];
        $worker->beforeWork($data);
        $worker->work($data);

        $this->assertCount(
            $initialBinariesCount + 1,
            $ArchiveBinaries->find()
                ->innerJoinWith('ArchiveUnits')
                ->where(['archive_id' => 2])
        );
        $this->assertEquals(
            ArchivesTable::S_UPDATE_STORING,
            $Archives->get(2)->get('state')
        );
    }
}
