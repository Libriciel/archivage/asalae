<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\TestSuite\VolumeSample;
use Asalae\Worker\ArchiveWorker;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Notify;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use DOMDocument;
use DOMXPath;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const TMP_TESTDIR;

class ArchiveWorkerTest extends TestCase
{
    use InvokePrivateTrait;
    use AutoFixturesTrait;

    public const string SEDA_02_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xml';
    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';
    public const string SEDA_10_DOUBLE_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_double.xml';
    public const string SEDA_02_XSD = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xsd';
    public const array ATTACHMENTS = [
        ASALAE_CORE_TEST_DATA . DS . 'sample.odt',
        ASALAE_CORE_TEST_DATA . DS . 'sample.pdf',
        ASALAE_CORE_TEST_DATA . DS . 'sample.rng',
    ];
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_EVENT_LOGS,
        FIXTURES_VOLUMES,
        'app.Archives',
        'app.AccessRuleCodes',
        'app.AccessRules',
        'app.AppraisalRuleCodes',
        'app.AppraisalRules',
        'app.ArchiveFiles',
        'app.EventLogs',
        'app.SecureDataSpaces',
        'app.Sequences',
        'app.Volumes',
        'app.StoredFiles',
        'app.StoredFilesVolumes',
        'app.TransferAttachments',
        'app.TypeEntities',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('Beanstalk.disable_check_ttr', true);
        Filesystem::reset();
        VolumeSample::init();
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testGetOriginatingAgency
     */
    public function testGetOriginatingAgency()
    {
        Filesystem::copy(self::SEDA_10_XML, $xml = TMP_TESTDIR . DS . 'msg.xml');
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ArchiveWorker(new Entity(), $io);
        $entity = new Entity(
            [
                'transfer_identifier' => 'testunit',
                'xml' => $xml,
                'archival_agency' => new Entity(['id' => 2, 'lft' => 2, 'rght' => 11]),
            ],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);

        $dom = new DOMDocument();
        $dom->load($xml);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $this->invokeProperty($worker, 'dom', 'set', $dom);
        $this->invokeProperty($worker, 'namespace', 'set', $namespace);
        $this->invokeProperty($worker, 'xpath', 'set', $xpath);
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:Archive')->item(0);
        $output = $this->invokeMethod($worker, 'getOriginatingAgency', [$archiveNode]);
        $this->assertInstanceOf(EntityInterface::class, $output);

        $node = $xpath->query('ns:ContentDescription/ns:OriginatingAgency', $archiveNode)->item(0);
        $node->parentNode->removeChild($node);
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:Archive')->item(0);
        $output = $this->invokeMethod($worker, 'getOriginatingAgency', [$archiveNode]);
        $this->assertNull($output);
    }

    /**
     * testGetAgreement
     */
    public function testGetAgreement()
    {
        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $Agreements->updateAll(['identifier' => 'agr-test01'], ['id' => 1]);
        Filesystem::copy(self::SEDA_10_XML, $xml = TMP_TESTDIR . DS . 'msg.xml');
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ArchiveWorker(new Entity(), $io);
        $entity = new Entity(
            [
                'transfer_identifier' => 'testunit',
                'xml' => $xml,
                'archival_agency' => new Entity(['id' => 2, 'lft' => 2, 'rght' => 11]),
            ],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);

        $dom = new DOMDocument();
        $dom->load($xml);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $this->invokeProperty($worker, 'dom', 'set', $dom);
        $this->invokeProperty($worker, 'namespace', 'set', $namespace);
        $this->invokeProperty($worker, 'xpath', 'set', $xpath);
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:Archive')->item(0);
        $output = $this->invokeMethod($worker, 'getAgreement', [$archiveNode]);
        $this->assertInstanceOf(EntityInterface::class, $output);

        $node = $xpath->query('ns:ArchivalAgreement', $archiveNode)->item(0);
        $node->parentNode->removeChild($node);
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:Archive')->item(0);
        $output = $this->invokeMethod($worker, 'getAgreement', [$archiveNode]);
        $this->assertNull($output);
    }

    /**
     * testGetProfile
     */
    public function testGetProfile()
    {
        $Profiles = TableRegistry::getTableLocator()->get('Profiles');
        $Profiles->updateAll(['identifier' => 'test'], ['id' => 1]);
        Filesystem::copy(self::SEDA_10_XML, $xml = TMP_TESTDIR . DS . 'msg.xml');
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ArchiveWorker(new Entity(), $io);
        $entity = new Entity(
            [
                'transfer_identifier' => 'testunit',
                'xml' => $xml,
                'archival_agency' => new Entity(['id' => 2, 'lft' => 2, 'rght' => 11]),
            ],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);

        $dom = new DOMDocument();
        $dom->load($xml);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $this->invokeProperty($worker, 'dom', 'set', $dom);
        $this->invokeProperty($worker, 'namespace', 'set', $namespace);
        $this->invokeProperty($worker, 'xpath', 'set', $xpath);
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:Archive')->item(0);
        $output = $this->invokeMethod($worker, 'getProfile', [$archiveNode]);
        $this->assertInstanceOf(EntityInterface::class, $output);

        $node = $xpath->query('ns:ArchivalProfile', $archiveNode)->item(0);
        $node->parentNode->removeChild($node);
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:Archive')->item(0);
        $output = $this->invokeMethod($worker, 'getProfile', [$archiveNode]);
        $this->assertNull($output);
    }

    /**
     * testGetServiceLevel
     */
    public function testGetServiceLevel()
    {
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $ServiceLevels->updateAll(['identifier' => 'service-level-test'], ['id' => 1]);
        Filesystem::copy(self::SEDA_10_XML, $xml = TMP_TESTDIR . DS . 'msg.xml');
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ArchiveWorker(new Entity(), $io);
        $entity = new Entity(
            [
                'transfer_identifier' => 'testunit',
                'xml' => $xml,
                'archival_agency' => new Entity(['id' => 2, 'lft' => 2, 'rght' => 11]),
            ],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);

        $dom = new DOMDocument();
        $dom->load($xml);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $this->invokeProperty($worker, 'dom', 'set', $dom);
        $this->invokeProperty($worker, 'namespace', 'set', $namespace);
        $this->invokeProperty($worker, 'xpath', 'set', $xpath);
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:Archive')->item(0);
        $output = $this->invokeMethod($worker, 'getServiceLevel', [$archiveNode]);
        $this->assertInstanceOf(EntityInterface::class, $output);

        $node = $xpath->query('ns:ServiceLevel', $archiveNode)->item(0);
        $node->parentNode->removeChild($node);
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:Archive')->item(0);
        $output = $this->invokeMethod($worker, 'getServiceLevel', [$archiveNode]);
        $this->assertNull($output);
    }

    /**
     * testGetAccessRuleId
     */
    public function testGetAccessRuleId()
    {
        Filesystem::copy(self::SEDA_10_XML, $xml = TMP_TESTDIR . DS . 'msg.xml');
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ArchiveWorker(new Entity(), $io);
        $entity = new Entity(
            [
                'transfer_identifier' => 'testunit',
                'xml' => $xml,
                'archival_agency' => new Entity(['id' => 2, 'lft' => 2, 'rght' => 11]),
            ],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);

        $dom = new DOMDocument();
        $dom->load($xml);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $this->invokeProperty($worker, 'dom', 'set', $dom);
        $this->invokeProperty($worker, 'namespace', 'set', $namespace);
        $this->invokeProperty($worker, 'xpath', 'set', $xpath);
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:Archive')->item(0);
        $firstTest = $this->invokeMethod($worker, 'getAccessRuleId', [$archiveNode]);
        $this->assertIsInt($firstTest);

        $node = $xpath->query('ns:AccessRestrictionRule', $archiveNode)->item(0);
        $node->parentNode->removeChild($node);
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:Archive')->item(0);
        $secondTest = $this->invokeMethod($worker, 'getAccessRuleId', [$archiveNode]); // AR062 (default)
        $this->assertIsInt($secondTest);
    }

    /**
     * testGetAppraisalRuleId
     */
    public function testGetAppraisalRuleId()
    {
        Filesystem::copy(self::SEDA_10_XML, $xml = TMP_TESTDIR . DS . 'msg.xml');
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ArchiveWorker(new Entity(), $io);
        $entity = new Entity(
            [
                'transfer_identifier' => 'testunit',
                'xml' => $xml,
                'archival_agency' => new Entity(['id' => 2, 'lft' => 2, 'rght' => 11]),
            ],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);

        $dom = new DOMDocument();
        $dom->load($xml);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $this->invokeProperty($worker, 'dom', 'set', $dom);
        $this->invokeProperty($worker, 'namespace', 'set', $namespace);
        $this->invokeProperty($worker, 'xpath', 'set', $xpath);
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:Archive')->item(0);
        $appraisalRule = $xpath->query('ns:AppraisalRule', $archiveNode)->item(0);
        $appraisalRule->parentNode->removeChild($appraisalRule);
        $firstTest = $this->invokeMethod($worker, 'getAppraisalRuleId', [$archiveNode]);
        $this->assertIsInt($firstTest); // APP0Y (default)
        $AppraisalRules = TableRegistry::getTableLocator()->get('AppraisalRules');
        $appRule = $AppraisalRules->find()
            ->where(['AppraisalRules.id' => $firstTest])
            ->contain(['AppraisalRuleCodes'])
            ->first();
        $this->assertNotNull($appRule);
        $this->assertEquals('keep', $appRule->get('final_action_code'));

        $appraisal = $dom->createElementNS($namespace, 'AppraisalRule');
        $code = $dom->createElementNS($namespace, 'Code');
        $code->setAttribute('listVersionID', 'edition 2009');
        DOMUtility::setValue($code, 'detruire');
        $duration = $dom->createElementNS($namespace, 'Duration');
        DOMUtility::setValue($duration, 'P5Y');
        $startDate = $dom->createElementNS($namespace, 'StartDate');
        DOMUtility::setValue($startDate, date('Y-m-d'));
        $appraisal->appendChild($code);
        $appraisal->appendChild($duration);
        $appraisal->appendChild($startDate);
        $archiveNode->appendChild($appraisal);
        $secondTest = $this->invokeMethod($worker, 'getAppraisalRuleId', [$archiveNode]);
        $this->assertIsInt($secondTest); // APP5Y

        $appRule = $AppraisalRules->find()
            ->where(['AppraisalRules.id' => $secondTest])
            ->contain(['AppraisalRuleCodes'])
            ->first();
        $this->assertNotNull($appRule);
        $this->assertEquals('destroy', $appRule->get('final_action_code'));
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Transfers->updateAll(['state' => TransfersTable::S_ARCHIVING], []);

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = $this->getMockBuilder(ArchiveWorker::class)
            ->setConstructorArgs([new \AsalaeCore\ORM\Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        $data = ['transfer_id' => 8];
        $worker->beforeWork($data);
        $worker->work($data);
        $this->assertCount(
            1,
            $Archives->find()
                ->innerJoinWith('Transfers')
                ->where(['Transfers.id' => 8])
        );

        $e = null;
        try {
            $worker->beforeWork($data);
            $worker->work($data);
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(CantWorkException::class, $e);
    }

    /**
     * testComposite
     */
    public function testComposite()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        $transfer = $Transfers->newEntityFromXml(ASALAE_CORE_TEST_DATA . DS . 'sample_seda21_composite.xml');
        $transfer->set('state', TransfersTable::S_ARCHIVING);
        $transfer->set('files_deleted', false);
        $Transfers->saveOrFail($transfer);
        Filesystem::copy(
            ASALAE_CORE_TEST_DATA . DS . 'sample_seda21_composite.xml',
            $transfer->get('xml')
        );

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = $this->getMockBuilder(ArchiveWorker::class)
            ->setConstructorArgs([new \AsalaeCore\ORM\Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        $data = ['transfer_id' => $transfer->id];
        $worker->beforeWork($data);
        $worker->work($data);
        $archive = $Archives->find()
            ->innerJoinWith('Transfers')
            ->where(['Transfers.id' => $transfer->id])
            ->first();
        $this->assertCount(
            2,
            $Archives->find()
                ->innerJoinWith('Transfers')
                ->where(['Archives.id' => $archive->id])
        );
        $this->assertEquals(
            ArchivesTable::S_UPDATE_CREATING,
            $Archives->get($archive->id)->get('state')
        );

        $e = null;
        try {
            $worker->beforeWork($data);
            $worker->work($data);
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(CantWorkException::class, $e);
    }
}
