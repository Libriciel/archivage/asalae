<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Model\Table\BatchTreatmentsTable;
use Asalae\TestSuite\VolumeSample;
use Asalae\Worker\BatchTreatmentWorker;
use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;

use const DS;
use const TMP_TESTDIR;

class BatchTreatmentWorkerTest extends TestCase
{
    use InvokePrivateTrait;
    use AutoFixturesTrait;
    use ConsoleIntegrationTestTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        FIXTURES_EVENT_LOGS,
        'app.AccessRuleCodes',
        'app.AccessRules',
        'app.AppraisalRuleCodes',
        'app.AppraisalRules',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveBinaryConversions',
        'app.ArchiveDescriptions',
        'app.ArchiveIndicators',
        'app.ArchiveUnits',
        'app.ArchiveUnitsBatchTreatments',
        'app.BatchTreatments',
        'app.Configurations',
        'app.Notifications',
        'app.Sessions',
    ];
    private $debugFixtures = false;
    /**
     * @var BatchTreatmentWorker|MockObject
     */
    private $worker;
    /**
     * @var EntityInterface
     */
    private $batchTreatment;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.disable_check_ttr', true);

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $this->worker = $this->getMockBuilder(BatchTreatmentWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();

        Filesystem::reset();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('App.paths.data', TMP_TESTDIR);

        $loc = TableRegistry::getTableLocator();
        /** @var BatchTreatmentsTable $BatchTreatments */
        $BatchTreatments = $loc->get('BatchTreatments');
        $ArchiveUnitsBatchTreatments = $loc->get('ArchiveUnitsBatchTreatments');
        $batchTreatment = $BatchTreatments->newEntity(
            [
                'archival_agency_id' => 2,
                'user_id' => 1,
                'state' => BatchTreatmentsTable::S_READY,
                'data' => json_encode(
                    [
                        'type' => 'appraisal',
                        'appraisal__duration' => 'APP50Y',
                    ]
                ),
            ]
        );
        $BatchTreatments->saveOrFail($batchTreatment);
        $ArchiveUnitsBatchTreatments->insertQuery()
            ->insert(['archive_unit_id', 'batch_treatment_id'])
            ->values(
                [
                    'archive_unit_id' => 1,
                    'batch_treatment_id' => $batchTreatment->id,
                ]
            )
            ->execute();
        $this->batchTreatment = $batchTreatment;
        $Exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(['async', 'command'])
            ->getMock();
        $Exec->method('command')->willReturnCallback(
            function ($command, ...$args) {
                $h = fopen('/tmp/test.log', 'a');
                fwrite($h, $command . var_export($args, true) . "\n");
                fclose($h);
                if ($command === CAKE_SHELL) {
                    $escaped = $this->extractArgsShellOptions($args);
                    $command = implode(' ', $escaped);
                    $out = new ConsoleOutput();
                    $err = new ConsoleOutput();
                    $io = new ConsoleIo($out, $err, new ConsoleInput([]));
                    $runner = $this->makeRunner();
                    $args = $this->commandStringToArgs("cake $command");
                    $code = $runner->run($args, $io);
                    if (is_file(TMP_TESTDIR . '/update_lifecycle/.lock')) {
                        unlink(TMP_TESTDIR . '/update_lifecycle/.lock');
                    }
                    return new CommandResult(
                        [
                            'success' => $code === 0,
                            'code' => $code,
                            'stdout' => implode(PHP_EOL, $out->messages()),
                            'stderr' => implode(PHP_EOL, $err->messages()),
                        ]
                    );
                } else {
                    $Exec = new Exec();
                    return $Exec->command($command, ...$args);
                }
            }
        );
        Utility::set('Exec', $Exec);
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * extractArgsShellOptions
     * @param array $args
     * @return array
     */
    private function extractArgsShellOptions(array $args): array
    {
        $escaped = [];
        foreach ($args as $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $escaped[] = $k;
                    if ($v) {
                        $escaped[] = $v;
                    }
                }
            } else {
                $escaped[] = $value;
            }
        }
        return $escaped;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Utility::reset();
    }

    /**
     * testWorkAppraisal
     */
    public function testWorkAppraisal()
    {
        $loc = TableRegistry::getTableLocator();
        $BatchTreatments = $loc->get('BatchTreatments');
        $Aubt = $loc->get('ArchiveUnitsBatchTreatments');
        $Archives = $loc->get('Archives');
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $appraisal_id = $ArchiveUnits->get(1)->get('appraisal_rule_id');
        $this->assertEquals($appraisal_id, $Archives->get(1)->get('appraisal_rule_id'));
        $data = ['batch_treatment_id' => $this->batchTreatment->id, 'user_id' => 1];
        $this->worker->beforeWork($data);
        $this->worker->work($data);
        $newAppraisalId = $ArchiveUnits->get(1)->get('appraisal_rule_id');
        $this->assertNotEquals($appraisal_id, $newAppraisalId);
        $this->assertEquals($newAppraisalId, $Archives->get(1)->get('appraisal_rule_id'));
        $this->assertEquals(
            BatchTreatmentsTable::S_FINISHED,
            $BatchTreatments->get($this->batchTreatment->id)->get('state')
        );
        $this->assertCount(0, $Aubt->find()->where(['batch_treatment_id' => $this->batchTreatment->id]));

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
            )
        );
        $node = $util->node('ns:AppraisalRule/ns:Duration');
        $this->assertEquals('P50Y', $node->nodeValue);

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
            )
        );
        $node = $util->node('ns:event[last()]/ns:eventType');
        $this->assertEquals('batch_treatment_appraisal', $node->nodeValue);

        /**
         * test pas de traitment nécessaire
         */
        $BatchTreatments->updateAll(['state' => BatchTreatmentsTable::S_READY], ['id' => $this->batchTreatment->id]);
        $link = $Aubt->newEntity(
            [
                'batch_treatment_id' => $this->batchTreatment->id,
                'archive_unit_id' => 5,
            ]
        );
        $Aubt->saveOrFail($link);
        $appraisal_id = $ArchiveUnits->get(5)->get('appraisal_rule_id');
        $data = ['batch_treatment_id' => $this->batchTreatment->id, 'user_id' => 1];
        $this->worker->beforeWork($data);
        $this->worker->work($data);
        $this->assertEquals($appraisal_id, $ArchiveUnits->get(5)->get('appraisal_rule_id'));

        $EventLogs = $loc->get('EventLogs');
        $lastEvent = $EventLogs->find()->orderBy(['id' => 'desc'])->first();
        $this->assertEquals('batch_treatment_appraisal', $lastEvent->get('type'));
        $this->assertTrue($lastEvent->get('in_lifecycle'));
    }

    /**
     * testWorkRestriction
     */
    public function testWorkRestriction()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var BatchTreatmentsTable $BatchTreatments */
        $BatchTreatments = $loc->get('BatchTreatments');
        $Aubt = $loc->get('ArchiveUnitsBatchTreatments');
        $Archives = $loc->get('Archives');
        $BatchTreatments->updateAll(
            [
                'state' => BatchTreatmentsTable::S_READY,
                'data' => json_encode(
                    [
                        'type' => 'restriction',
                        'restriction__code' => 'AR045',
                    ]
                ),
            ],
            ['id' => $this->batchTreatment->id]
        );
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $access_id = $ArchiveUnits->get(1)->get('access_rule_id');
        $this->assertEquals($access_id, $Archives->get(1)->get('appraisal_rule_id'));
        $data = ['batch_treatment_id' => $this->batchTreatment->id, 'user_id' => 1];
        $this->worker->beforeWork($data);
        $this->worker->work($data);
        $newAccessId = $ArchiveUnits->get(1)->get('access_rule_id');
        $this->assertNotEquals($access_id, $newAccessId);
        $this->assertEquals($newAccessId, $Archives->get(1)->get('access_rule_id'));
        $this->assertEquals(
            $BatchTreatments::S_FINISHED,
            $BatchTreatments->get($this->batchTreatment->id)->get('state')
        );
        $this->assertCount(0, $Aubt->find()->where(['batch_treatment_id' => $this->batchTreatment->id]));

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
            )
        );
        $node = $util->node('ns:AccessRestrictionRule/ns:Code');
        $this->assertEquals('AR045', $node->nodeValue);

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
            )
        );
        $node = $util->node('ns:event[last()]/ns:eventType');
        $this->assertEquals('batch_treatment_restriction', $node->nodeValue);

        /**
         * test pas de traitment nécessaire
         */
        $BatchTreatments->updateAll(['state' => BatchTreatmentsTable::S_READY], ['id' => $this->batchTreatment->id]);
        $link = $Aubt->newEntity(
            [
                'batch_treatment_id' => $this->batchTreatment->id,
                'archive_unit_id' => 1,
            ]
        );
        $Aubt->saveOrFail($link);
        $access_id = $ArchiveUnits->get(3)->get('access_rule_id');
        $data = ['batch_treatment_id' => $this->batchTreatment->id, 'user_id' => 1];
        $this->worker->beforeWork($data);
        $this->worker->work($data);
        $this->assertEquals($access_id, $ArchiveUnits->get(3)->get('access_rule_id'));
    }

    /**
     * testWorkName
     */
    public function testWorkName()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var BatchTreatmentsTable $BatchTreatments */
        $BatchTreatments = $loc->get('BatchTreatments');
        $Aubt = $loc->get('ArchiveUnitsBatchTreatments');
        $Archives = $loc->get('Archives');
        $BatchTreatments->updateAll(
            [
                'state' => BatchTreatmentsTable::S_READY,
                'data' => json_encode(
                    [
                        'type' => 'name',
                        'name__research' => 'archive_unit_',
                        'name__replace' => 'modified_',
                    ]
                ),
            ],
            ['id' => $this->batchTreatment->id]
        );
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateAll(['name' => 'archive_unit_1'], ['id' => 1]);
        $Archives->updateAll(['name' => 'archive_unit_1'], ['id' => 1]);
        $data = ['batch_treatment_id' => $this->batchTreatment->id, 'user_id' => 1];
        $this->worker->beforeWork($data);
        $this->worker->work($data);
        $this->assertEquals('modified_1', $ArchiveUnits->get(1)->get('name'));
        $this->assertEquals('modified_1', $Archives->get(1)->get('name'));
        $this->assertEquals(
            $BatchTreatments::S_FINISHED,
            $BatchTreatments->get($this->batchTreatment->id)->get('state')
        );
        $this->assertCount(0, $Aubt->find()->where(['batch_treatment_id' => $this->batchTreatment->id]));

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
            )
        );
        $node = $util->node('ns:Name');
        $this->assertEquals('modified_1', $node->nodeValue);

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
            )
        );
        $node = $util->node('ns:event[last()]/ns:eventType');
        $this->assertEquals('batch_treatment_name', $node->nodeValue);
    }

    /**
     * testWorkDescription
     */
    public function testWorkDescription()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var BatchTreatmentsTable $BatchTreatments */
        $BatchTreatments = $loc->get('BatchTreatments');
        $Aubt = $loc->get('ArchiveUnitsBatchTreatments');
        $Archives = $loc->get('Archives');
        $ArchiveDescriptions = $loc->get('ArchiveDescriptions');
        $BatchTreatments->updateAll(
            [
                'state' => BatchTreatmentsTable::S_READY,
                'data' => json_encode(
                    [
                        'type' => 'description',
                        'description__research' => 'archive_unit_',
                        'description__replace' => 'modified_',
                    ]
                ),
            ],
            ['id' => $this->batchTreatment->id]
        );
        $ArchiveDescriptions->updateAll(['description' => 'archive_unit_1'], ['id' => 1]);
        $Archives->updateAll(['description' => 'archive_unit_1'], ['id' => 1]);
        $data = ['batch_treatment_id' => $this->batchTreatment->id, 'user_id' => 1];
        $this->worker->beforeWork($data);
        $this->worker->work($data);
        $this->assertEquals('modified_1', $ArchiveDescriptions->get(1)->get('description'));
        $this->assertEquals('modified_1', $Archives->get(1)->get('description'));
        $this->assertEquals(
            $BatchTreatments::S_FINISHED,
            $BatchTreatments->get($this->batchTreatment->id)->get('state')
        );
        $this->assertCount(0, $Aubt->find()->where(['batch_treatment_id' => $this->batchTreatment->id]));

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
            )
        );
        $node = $util->node('ns:ContentDescription/ns:Description');
        $this->assertEquals('modified_1', $node->nodeValue);

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
            )
        );
        $node = $util->node('ns:event[last()]/ns:eventType');
        $this->assertEquals('batch_treatment_description', $node->nodeValue);
    }

    /**
     * testWorkKeywordReplace
     */
    public function testWorkKeywordReplace()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var BatchTreatmentsTable $BatchTreatments */
        $BatchTreatments = $loc->get('BatchTreatments');
        $Aubt = $loc->get('ArchiveUnitsBatchTreatments');
        $BatchTreatments->updateAll(
            [
                'state' => BatchTreatmentsTable::S_READY,
                'data' => json_encode(
                    [
                        'type' => 'keyword',
                        'keyword__type' => 'replace',
                        'keyword__research' => 'Code 1',
                        'keyword__replace' => 'modified_1',
                    ]
                ),
            ],
            ['id' => $this->batchTreatment->id]
        );
        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
            )
        );
        $desc = $util->node('ns:ContentDescription');
        $keyword = $util->createElement('Keyword');
        $keywordContent = $util->createElement('KeywordContent', 'Code 1');
        $keyword->appendChild($keywordContent);
        $desc->appendChild($keyword);
        VolumeSample::volumePutContent(
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            $util->dom->saveXML(),
            true
        );
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $archiveKeyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => 1,
                'content' => 'Code 1',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $ArchiveKeywords->saveOrFail($archiveKeyword);
        $data = ['batch_treatment_id' => $this->batchTreatment->id, 'user_id' => 1];
        $this->worker->beforeWork($data);
        $this->worker->work($data);
        $this->assertEquals('modified_1', $ArchiveKeywords->get(1)->get('content'));
        $this->assertEquals(
            $BatchTreatments::S_FINISHED,
            $BatchTreatments->get($this->batchTreatment->id)->get('state')
        );
        $this->assertCount(0, $Aubt->find()->where(['batch_treatment_id' => $this->batchTreatment->id]));

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
            )
        );
        $node = $util->node('ns:ContentDescription/ns:Keyword/ns:KeywordContent');
        $this->assertEquals('modified_1', $node->nodeValue);

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
            )
        );
        $node = $util->node('ns:event[last()]/ns:eventType');
        $this->assertEquals('batch_treatment_keyword_replace', $node->nodeValue);
    }

    /**
     * testWorkKeywordAdd
     */
    public function testWorkKeywordAdd()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var BatchTreatmentsTable $BatchTreatments */
        $BatchTreatments = $loc->get('BatchTreatments');
        $Aubt = $loc->get('ArchiveUnitsBatchTreatments');
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $BatchTreatments->updateAll(
            [
                'state' => BatchTreatmentsTable::S_READY,
                'data' => json_encode(
                    [
                        'type' => 'keyword',
                        'keyword__type' => 'add',
                        'keyword__add' => 'added keyword',
                    ]
                ),
            ],
            ['id' => $this->batchTreatment->id]
        );
        $data = ['batch_treatment_id' => $this->batchTreatment->id, 'user_id' => 1];
        $this->worker->beforeWork($data);
        $this->worker->work($data);
        $this->assertTrue($ArchiveKeywords->exists(['content' => 'added keyword']));
        $this->assertEquals(
            $BatchTreatments::S_FINISHED,
            $BatchTreatments->get($this->batchTreatment->id)->get('state')
        );
        $this->assertCount(0, $Aubt->find()->where(['batch_treatment_id' => $this->batchTreatment->id]));

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
            )
        );
        $node = $util->node('ns:ContentDescription/ns:Keyword/ns:KeywordContent');
        $this->assertEquals('added keyword', $node->nodeValue);

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
            )
        );
        $node = $util->node('ns:event[last()]/ns:eventType');
        $this->assertEquals('batch_treatment_keyword_add', $node->nodeValue);

        /**
         * Test avec ajout de description
         */
        $BatchTreatments->updateAll(
            [
                'state' => BatchTreatmentsTable::S_READY,
                'data' => json_encode(
                    [
                        'type' => 'keyword',
                        'keyword__type' => 'add',
                        'keyword__add' => 'another keyword',
                    ]
                ),
            ],
            ['id' => $this->batchTreatment->id]
        );
        $link = $Aubt->newEntity(
            [
                'batch_treatment_id' => $this->batchTreatment->id,
                'archive_unit_id' => 5,
            ]
        );
        $Aubt->saveOrFail($link);
    }

    /**
     * testWorkKeywordDelete
     */
    public function testWorkKeywordDelete()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var BatchTreatmentsTable $BatchTreatments */
        $BatchTreatments = $loc->get('BatchTreatments');
        $Aubt = $loc->get('ArchiveUnitsBatchTreatments');
        $BatchTreatments->updateAll(
            [
                'state' => BatchTreatmentsTable::S_READY,
                'data' => json_encode(
                    [
                        'type' => 'keyword',
                        'keyword__type' => 'delete',
                        'keyword__delete' => 'Code 1',
                    ]
                ),
            ],
            ['id' => $this->batchTreatment->id]
        );
        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
            )
        );
        unlink(TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml');
        unlink(TMP_VOL2 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml');
        VolumeSample::volumePutContent(
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            $util->dom->saveXML()
        );
        $desc = $util->node('ns:ContentDescription');
        $keyword = $util->createElement('Keyword');
        $keywordContent = $util->createElement('KeywordContent', 'Code 1');
        $keyword->appendChild($keywordContent);
        $desc->appendChild($keyword);
        unlink(TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml');
        unlink(TMP_VOL2 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml');
        VolumeSample::volumePutContent(
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            $util->dom->saveXML()
        );
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $archiveKeyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => 1,
                'content' => 'Code 1',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $ArchiveKeywords->saveOrFail($archiveKeyword);

        $data = ['batch_treatment_id' => $this->batchTreatment->id, 'user_id' => 1];
        $this->worker->beforeWork($data);
        $this->worker->work($data);
        $this->assertFalse($ArchiveKeywords->exists(['content' => 'Code 1']));
        $this->assertEquals(
            $BatchTreatments::S_FINISHED,
            $BatchTreatments->get($this->batchTreatment->id)->get('state')
        );
        $this->assertCount(0, $Aubt->find()->where(['batch_treatment_id' => $this->batchTreatment->id]));

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
            )
        );
        $node = $util->node('ns:ContentDescription/ns:Keyword/ns:KeywordContent');
        $this->assertNull($node);

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
            )
        );
        $node = $util->node('ns:event[last()]/ns:eventType');
        $this->assertEquals('batch_treatment_keyword_delete', $node->nodeValue);
    }

    /**
     * testWorkConvertDo
     */
    public function testWorkConvertDo()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var BatchTreatmentsTable $BatchTreatments */
        $BatchTreatments = $loc->get('BatchTreatments');
        $Aubt = $loc->get('ArchiveUnitsBatchTreatments');
        $ArchiveBinaryConversions = $loc->get('ArchiveBinaryConversions');
        $ArchiveBinaryConversions->deleteAll([]);
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $ArchiveBinaries->updateAll(['type' => 'test'], ['id' => 2]);
        $BatchTreatments->updateAll(
            [
                'state' => BatchTreatmentsTable::S_READY,
                'data' => json_encode(
                    [
                        'type' => 'convert',
                        'convert' => 'do',
                    ]
                ),
            ],
            ['id' => $this->batchTreatment->id]
        );
        $ArchiveUnitsBatchTreatments = $loc->get('ArchiveUnitsBatchTreatments');
        $ArchiveUnitsBatchTreatments->updateAll(['archive_unit_id' => 2], []);

        $data = ['batch_treatment_id' => $this->batchTreatment->id, 'user_id' => 1];
        $this->worker->beforeWork($data);
        $this->worker->work($data);
        $this->assertEquals(
            $BatchTreatments::S_FINISHED,
            $BatchTreatments->get($this->batchTreatment->id)->get('state')
        );
        $this->assertCount(0, $Aubt->find()->where(['batch_treatment_id' => $this->batchTreatment->id]));
        $this->assertCount(1, $ArchiveBinaryConversions->find());

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
            )
        );
        $node = $util->node('ns:event[last()]/ns:eventType');
        $this->assertEquals('batch_treatment_convert_do', $node->nodeValue);
    }

    /**
     * testWorkConvertDelete
     */
    public function testWorkConvertDelete()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var BatchTreatmentsTable $BatchTreatments */
        $BatchTreatments = $loc->get('BatchTreatments');
        $BatchTreatments->updateAll(
            [
                'state' => BatchTreatmentsTable::S_READY,
                'data' => json_encode(
                    [
                        'type' => 'convert',
                        'convert' => 'delete',
                    ]
                ),
            ],
            ['id' => $this->batchTreatment->id]
        );
        $ArchiveUnitsBatchTreatments = $loc->get('ArchiveUnitsBatchTreatments');
        $ArchiveUnitsBatchTreatments->updateAll(['archive_unit_id' => 2], []);
        $data = ['batch_treatment_id' => $this->batchTreatment->id, 'user_id' => 1];
        $this->worker->beforeWork($data);
        $this->worker->work($data);
        $this->assertEquals(
            $BatchTreatments::S_FINISHED,
            $BatchTreatments->get($this->batchTreatment->id)->get('state')
        );

        $util = DOMUtility::loadXML(
            VolumeSample::volumeGetContent(
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
            )
        );
        $node = $util->node('ns:event[last()]/ns:eventType');
        $this->assertEquals('batch_treatment_convert_delete', $node->nodeValue);
    }

    /**
     * testUpdateAppMeta
     */
    public function testUpdateAppMeta()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $ArchiveBinaries->updateAll(['app_meta' => null], ['id' => 1]);

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = $this->getMockBuilder(BatchTreatmentWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $this->invokeMethod(
            $worker,
            'updateAppMeta',
            [
                $Archives->get(1),
                $ArchiveBinaries->find()
                    ->where(['ArchiveBinaries.id' => 1])
                    ->contain(['StoredFiles'])
                    ->firstOrFail(),
            ]
        );
        $this->assertNotNull($ArchiveBinaries->get(1)->get('app_meta'));
    }
}
