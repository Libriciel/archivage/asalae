<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\TestSuite\VolumeSample;
use Asalae\Worker\RestitutionBuildWorker;
use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Datacompressor\Utility\DataCompressor;
use DOMDocument;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;

use const DS;
use const TMP_TESTDIR;

class RestitutionBuildWorkerTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        FIXTURES_EVENT_LOGS,
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveUnitsRestitutionRequests',
        'app.RestitutionRequests',
        'app.Restitutions',
        'app.Sequences',
        'app.TransferLocks',
    ];
    /**
     * @var RestitutionBuildWorker|MockObject $worker
     */
    public $worker;
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.disable_check_ttr', true);

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $this->worker = $this->getMockBuilder(RestitutionBuildWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();

        Filesystem::reset();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('App.paths.data', TMP_TESTDIR);
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Utility::reset();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $loc = TableRegistry::getTableLocator();
        $Restitutions = $loc->get('Restitutions');
        $RestitutionRequests = $loc->get('RestitutionRequests');
        $RestitutionRequests->updateAll(['state' => RestitutionRequestsTable::S_ACCEPTED], ['id' => 1]);
        $Restitutions->deleteAll([]);
        $this->worker->work(['restitution_request_id' => 1]);

        $this->assertCount(1, $Restitutions->find());
        $restitution = $Restitutions->find()->first();
        $zip = $restitution->get('zip');

        $this->assertFileExists($zip);
        $this->assertStringContainsString('application/zip', mime_content_type($zip));
        $this->assertGreaterThan(1, filesize($zip));

        $this->assertEquals(
            'restitution_available',
            $RestitutionRequests->get(1)->get('state')
        );
        $this->assertEquals(
            'available',
            $restitution->get('state')
        );

        $files = DataCompressor::uncompress($zip, TMP_TESTDIR);
        $expected = [
            0 => TMP_TESTDIR . DS . 'AR_1_restitution.xml',
            1 => TMP_TESTDIR . DS . 'archive_unit_1/original_data/testfile.txt',
            2 => TMP_TESTDIR . DS . 'archive_unit_1/management_data/1_lifecycle.xml',
            3 => TMP_TESTDIR . DS . 'archive_unit_1/management_data/1_description.xml',
        ];
        $this->assertEquals(sort($expected), sort($files));

        $dom = new DOMDocument();
        $dom->load($files[0]);
        $this->assertTrue($dom->schemaValidate(SEDA_ARCHIVE_V21_XSD));
    }
}
