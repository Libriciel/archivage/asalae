<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Utility\TransferAnalyse;
use Asalae\Worker\AnalyseWorker;
use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Object\CommandResult;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;

class AnalyseWorkerTest extends TestCase
{
    use SaveBufferTrait;

    public array $fixtures = [
        'app.OrgEntities',
        'app.TransferAttachments',
        'app.TransferLocks',
        'app.Transfers',
    ];

    /**
     * @var AnalyseWorker|MockObject $worker
     */
    public $worker;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.disable_check_ttr', true);
        $analyser = $this->createMock(TransferAnalyse::class);
        $analyser->method('analyse');
        Utility::set(TransferAnalyse::class, $analyser);

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $this->worker = $this->getMockBuilder(AnalyseWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();

        $exec = $this->createMock(Exec::class);
        $exec->method('command')->willReturn(
            new CommandResult(['success' => false, 'code' => 500, 'stdout' => '', 'stderr' => ''])
        );
        Utility::set('Exec', $exec);
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');

        // pas de transfer 0
        try {
            $this->worker->work(['transfer_id' => 0]);
            $this->fail();
        } catch (Exception $e) {
            $this->assertInstanceOf(CantWorkException::class, $e);
        }

        // n'est pas dans un etat d'analyse
        try {
            $this->worker->work(['transfer_id' => 1]);
            $this->fail();
        } catch (Exception $e) {
            $this->assertInstanceOf(CantWorkException::class, $e);
        }

        $Transfers->updateAll(['state' => 'analysing'], ['id' => 1]);
        $this->worker->work(['transfer_id' => 1]);
        $this->assertEquals('controlling', $Transfers->get(1)->get('state'));
    }
}
