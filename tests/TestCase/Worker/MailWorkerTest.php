<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Model\Entity\Mail;
use Asalae\Worker\MailWorker;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\Mailer\Mailer;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use PHPUnit\Framework\MockObject\MockObject;

class MailWorkerTest extends TestCase
{
    use SaveBufferTrait;

    public array $fixtures = [
        'app.Mails',
    ];

    /**
     * @var MailWorker|MockObject $worker
     */
    public $worker;
    /**
     * @var Mail|MockObject
     */
    public $mailer;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.disable_check_ttr', true);
        $this->mailer = $this->getMockBuilder(Mailer::class)
            ->onlyMethods(['send'])
            ->getMock();
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $this->worker = $this->getMockBuilder(MailWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $this->mailer->setSubject('testunit');
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        $Mails->updateAll(
            ['serialized' => serialize($this->mailer)],
            ['id' => 1]
        );
        $this->worker->work(['mail_id' => 1]);
        $this->worker->afterWork(['mail_id' => 1]);
        $this->assertStringContainsString('testunit', $this->mailer->getMessage()->getSubject());
    }
}
