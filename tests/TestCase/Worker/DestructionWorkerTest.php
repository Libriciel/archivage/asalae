<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Model\Volume\VolumeManager;
use Asalae\TestSuite\VolumeSample;
use Asalae\Worker\DestructionWorker;
use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;

use const TMP_TESTDIR;
use const TMP_VOL1;
use const TMP_VOL2;

class DestructionWorkerTest extends TestCase
{
    use SaveBufferTrait;
    use AutoFixturesTrait;
    use ConsoleIntegrationTestTrait;

    public const string SEDA_10_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml';
    public const string LIFECYCLE_PREMIS = ASALAE_CORE_TEST_DATA . DS . 'sample_premis.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        FIXTURES_EVENT_LOGS,
        'app.AccessRules',
        'app.Acos',
        'app.AppraisalRules',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveBinariesTechnicalArchiveUnits',
        'app.ArchiveBinaryConversions',
        'app.ArchiveDescriptions',
        'app.ArchiveIndicators',
        'app.ArchiveUnitsBatchTreatments',
        'app.ArchiveUnitsDeliveryRequests',
        'app.ArchiveUnitsDestructionRequests',
        'app.ArchiveUnitsOutgoingTransferRequests',
        'app.ArchiveUnitsRestitutionRequests',
        'app.Configurations',
        'app.DeliveryRequests',
        'app.DestructionNotificationXpaths',
        'app.DestructionNotifications',
        'app.DestructionRequests',
        'app.OutgoingTransferRequests',
        'app.OutgoingTransfers',
        'app.RestitutionRequests',
        'app.Sequences',
        'app.TechnicalArchiveUnits',
    ];
    /**
     * @var DestructionWorker|MockObject $worker
     */
    public $worker;
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.disable_check_ttr', true);
        $deliveryWorker = $this->createMock(DestructionWorker::class);
        Utility::set(DestructionWorker::class, $deliveryWorker);

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $this->worker = $this->getMockBuilder(DestructionWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();

        $Exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(['async', 'command'])
            ->getMock();
        $fn = function ($command, ...$args) {
            $h = fopen('/tmp/test.log', 'a');
            fwrite($h, $command . var_export($args, true) . "\n");
            fclose($h);
            if ($command === CAKE_SHELL) {
                $escaped = $this->extractArgsShellOptions($args);
                $command = implode(' ', $escaped);
                $out = new ConsoleOutput();
                $err = new ConsoleOutput();
                $io = new ConsoleIo($out, $err, new ConsoleInput([]));
                $runner = $this->makeRunner();
                $args = $this->commandStringToArgs("cake $command");
                $code = $runner->run($args, $io);
                if (is_file(TMP_TESTDIR . '/update_lifecycle/.lock')) {
                    unlink(TMP_TESTDIR . '/update_lifecycle/.lock');
                }
                return new CommandResult(
                    [
                        'success' => $code === 0,
                        'code' => $code,
                        'stdout' => implode(PHP_EOL, $out->messages()),
                        'stderr' => implode(PHP_EOL, $err->messages()),
                    ]
                );
            } else {
                $Exec = new Exec();
                return $Exec->command($command, ...$args);
            }
        };
        $Exec->method('command')->willReturnCallback($fn);
        Utility::set('Exec', $Exec);

        Configure::write('App.paths.data', TMP_TESTDIR);
        Filesystem::reset();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('Archives.updateLifecycle.path', TMP_TESTDIR);
        Configure::write('Archives.updateDescription.path', TMP_TESTDIR);
        Configure::write('Archives.updateLifecycle.cache_duration', 0);
        Configure::write('Archives.updateDescription.cache_duration', 0);
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * extractArgsShellOptions
     * @param array $args
     * @return array
     */
    private function extractArgsShellOptions(array $args): array
    {
        $escaped = [];
        foreach ($args as $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $escaped[] = $k;
                    if ($v) {
                        $escaped[] = $v;
                    }
                }
            } else {
                $escaped[] = $value;
            }
        }
        return $escaped;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Filesystem::setNamespace();
        Filesystem::reset();
        Exec::waitUntilAsyncFinish();
        Utility::reset();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $Archives->updateAll(['state' => 'destroying'], ['id' => 1]);
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $initialUnitsCount = $ArchiveUnits->find()->count();
        $DestructionRequests = TableRegistry::getTableLocator()->get('DestructionRequests');
        $DestructionNotificationXpaths = TableRegistry::getTableLocator()->get('DestructionNotificationXpaths');
        $DestructionNotificationXpaths->deleteAll([]);
        $DestructionNotifications = TableRegistry::getTableLocator()->get('DestructionNotifications');
        $DestructionNotifications->deleteAll([]);
        $ArchiveFiles = TableRegistry::getTableLocator()->get('ArchiveFiles');
        $ArchiveBinaries = TableRegistry::getTableLocator()->get('ArchiveBinaries');
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $EventLogs->deleteAll([]);
        $initialBinariesCount = $ArchiveBinaries->find()->count();
        $manager = new VolumeManager(1);
        $manager->set(1, 'test');
        $binary = $ArchiveBinaries->find()
            ->where(['ArchiveBinaries.id' => 1])
            ->contain(['StoredFiles'])
            ->firstOrFail();
        $this->assertEquals('test', Hash::get($binary, 'stored_file.file'));
        $DestructionRequests->updateAll(['state' => 'accepted'], ['id' => 1]);
        $ArchiveUnitsDestructionRequests = TableRegistry::getTableLocator()->get('ArchiveUnitsDestructionRequests');
        $auDr = $ArchiveUnitsDestructionRequests->newEntity(
            [
                'archive_unit_id' => 1,
                'destruction_request_id' => 1,
            ]
        );
        $ArchiveUnitsDestructionRequests->saveOrFail($auDr);

        $this->worker->work(['destruction_request_id' => 1]);
        $this->assertTrue(true);

        $archive = $Archives->get(1);
        $archiveUnit = $ArchiveUnits->exists(['id' => 1]);
        $this->assertFalse($archiveUnit);
        $destructionRequest = $DestructionRequests->get(1);
        $history = $destructionRequest->get('states_history');
        $this->assertTextContains('files_destroying', $history);
        $this->assertTextContains('description_deleting', $history);
        $this->assertEquals('destroyed', $destructionRequest->get('state'));
        $this->assertEquals('destroyed', $archive->get('state'));
        $e = null;
        try {
            Hash::get($binary, 'stored_file.file');
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(Exception::class, $e);
        $this->assertLessThan($initialUnitsCount, $ArchiveUnits->find()->count());
        $this->assertLessThan($initialBinariesCount, $ArchiveBinaries->find()->count());
        $this->assertEquals(0, $archive->get('original_size'));
        $this->assertEquals(0, $archive->get('original_count'));
        $this->assertNull($archive->get('next_pubdesc'));
        $deleted = $ArchiveFiles->find()
            ->where(['archive_id' => 1, 'type' => 'deleted'])
            ->first();
        $this->assertInstanceOf(EntityInterface::class, $deleted);
        $this->assertTextContains('Archive', $manager->get($deleted->get('stored_file_id')));
        $this->assertGreaterThanOrEqual(1, $EventLogs->find()->count());
    }
}
