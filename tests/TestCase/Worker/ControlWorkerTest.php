<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Model\Entity\TransferError;
use Asalae\Model\Table\AgreementsTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Worker\ControlWorker;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\PreControlMessages;
use Beanstalk\Exception\CantWorkException;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;

use const DS;
use const TMP_TESTDIR;

class ControlWorkerTest extends TestCase
{
    use InvokePrivateTrait;

    public const string SEDA_02_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xml';
    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';
    public const string SEDA_02_XSD = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xsd';
    public const array ATTACHMENTS = [
        ASALAE_CORE_TEST_DATA . DS . 'sample.odt' => [
            'format' => 'fmt/291',
            'mime' => 'application/vnd.oasis.opendocument.text',
            'valid' => true,
        ],
        ASALAE_CORE_TEST_DATA . DS . 'sample.pdf' => [
            'format' => 'fmt/18',
            'mime' => 'application/pdf',
            'valid' => true,
        ],
        ASALAE_CORE_TEST_DATA . DS . 'sample.rng' => [
            'format' => 'fmt/101',
            'mime' => 'Extensible Markup Language',
            'valid' => true,
        ],
    ];
    public array $fixtures = [
        'app.Agreements',
        'app.AgreementsOriginatingAgencies',
        'app.AgreementsProfiles',
        'app.AgreementsServiceLevels',
        'app.AgreementsTransferringAgencies',
        'app.AuthSubUrls',
        'app.AuthUrls',
        'app.Eaccpfs',
        'app.Fileuploads',
        'app.FileuploadsProfiles',
        'app.Mails',
        'app.Notifications',
        'app.OrgEntities',
        'app.Profiles',
        'app.ServiceLevels',
        'app.Siegfrieds',
        'app.TransferAttachments',
        'app.TransferErrors',
        'app.TransferLocks',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('Beanstalk.disable_check_ttr', true);
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Utility::reset();
        Exec::$pids = [];

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);

        $loc = TableRegistry::getTableLocator();
        $Agreements = $loc->get('Agreements');
        $Agreements->updateAll(['identifier' => 'agr-test01'], ['id' => 1]);
        $Agreements->updateAll(['identifier' => 'agr-test02'], ['id' => 2]);
        $OrgEntities = $loc->get('OrgEntities');
        $OrgEntities->updateAll(['identifier' => 'sv1'], ['id' => 4]);
        $Profiles = $loc->get('Profiles');
        $Profiles->updateAll(['identifier' => 'test'], ['id' => 1]);
        $ServiceLevels = $loc->get('ServiceLevels');
        $ServiceLevels->updateAll(['identifier' => 'service-level-test'], ['id' => 1]);
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        $Transfers->updateAll(['state' => $Transfers::S_ANALYSING], ['id' => 1]);
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Utility::reset();
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testValidateMessage
     */
    public function testValidateMessage()
    {
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ControlWorker(new Entity(), $io);
        $entity = new Entity(
            ['transfer_identifier' => 'testunit', 'xml' => self::SEDA_02_XML],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);
        $preControl = new PreControlMessages(self::SEDA_02_XML);
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);

        $errors = [];
        $this->invokeMethod($worker, 'validateMessage', [&$errors]);
        $this->assertEmpty($errors);

        $namespace = $preControl->dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $archive = $xpath->query('/ns:ArchiveTransfer/ns:Contains[1]')->item(0);
        $archive->parentNode->removeChild($archive);
        $testfile = @tempnam(TMP_TESTDIR, 'testunit-');
        $preControl->dom->save($testfile);
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));

        $entity->set('xml', $testfile);
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);

        $this->invokeMethod($worker, 'validateMessage', [&$errors]);

        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(101, $errors[0]['code']); // Conformité {nom_version_standard} : {message_erreur_xml}
    }

    /**
     * testValidateAttachments
     */
    public function testValidateAttachments()
    {
        /** @var DOMElement $integrity */
        /**
         * Préparation du transfer de test
         */
        $attachments = [];
        $basePath = AbstractGenericMessageForm::getDataDirectory(1) . DS . 'attachments';
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $TransferAttachments->deleteAll([]);
        foreach (self::ATTACHMENTS as $path => $values) {
            $filename = basename($path);
            Filesystem::copy($path, $basePath . DS . $filename);
            $attachments[] = new Entity(
                [
                    'transfer_id' => 1,
                    'filename' => $filename,
                    'path' => $basePath . DS . $filename,
                    'size' => filesize($basePath . DS . $filename),
                    'hash' => hash_file('sha256', $basePath . DS . $filename),
                    'hash_algo' => 'sha256',
                    'pronom_id' => strpos($filename, '.pdf')
                        ? 2
                        : (strpos($filename, '.odt') ? 3 : null),
                ] + $values,
                ['validate' => false]
            );
        }
        $TransferAttachments->saveMany($attachments);
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ControlWorker(new Entity(), $io);
        $entity = new Entity(
            [
                'id' => 1,
                'transfer_identifier' => 'testunit',
                'xml' => self::SEDA_02_XML,
                'transfer_attachments' => $attachments,
            ],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);
        $preControl = new PreControlMessages(self::SEDA_02_XML);
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);

        /**
         * Test ok
         */
        $errors = [];
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertEmpty($errors);
        $initialDom = clone $preControl->dom;
        $testfile = @tempnam(TMP_TESTDIR, 'testunit-');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));

        /**
         * Test integrity en base64
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $integrity = $preControl->dom->getElementsByTagName('Integrity')->item(0);
        $contains = $integrity->getElementsByTagName('Contains')->item(0);
        DOMUtility::setValue($contains, @base64_encode(hex2bin($contains->nodeValue)));
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertEmpty($errors);

        /**
         * Test missing @filename
         */
        $namespace = $preControl->dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $attachment = $xpath->query('//ns:Attachment')->item(0);
        $attachment->removeAttribute('filename');
        $entity->set('xml', $testfile);
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);

        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);

        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(207, $errors[0]['code']); // Nom du fichier de la pièce jointe non renseigné

        /**
         * Test missing @algorithme
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $integrity = $preControl->dom->getElementsByTagName('Integrity')->item(0);
        $integrity->getElementsByTagName('Contains')->item(0)->removeAttribute('algorithme');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(207, $errors[0]['code']); // Algorithme de hachage non transmis

        /**
         * Test bad @algorithme
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $integrity = $preControl->dom->getElementsByTagName('Integrity')->item(0);
        $integrity->getElementsByTagName('Contains')->item(0)->setAttribute('algorithme', 'badhash_algo');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(207, $errors[0]['code']); // Algorithme de hachage badhash_algo non identifié

        /**
         * Test missing Integrity@value
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $integrity = $preControl->dom->getElementsByTagName('Integrity')->item(0);
        DOMUtility::setValue($integrity->getElementsByTagName('Contains')->item(0), '');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(207, $errors[0]['code']); // Empreinte non transmise

        /**
         * Test no Document
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $documents = $preControl->dom->getElementsByTagName('Document');
        /** @var \DOMElement $document */
        foreach (iterator_to_array($documents) as $document) {
            $document->parentNode->removeChild($document);
        }
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        // Aucune pièce jointe référencée dans le bordereau
        $this->assertEquals(TransferError::CODE_ATTACHMENT_MISSING, $errors[0]['code']);

        /**
         * Test Document is dir
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $document = $preControl->dom->getElementsByTagName('Document')->item(0);
        $attachment = $document->getElementsByTagName('Attachment')->item(0);
        $attachment->setAttribute('filename', 'testdir');
        Filesystem::mkdir($basePath . DS . 'testdir');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        // Nom du fichier de la pièce jointe est un dossier
        $this->assertEquals(TransferError::CODE_ATTACHMENT_MISSING, $errors[0]['code']);

        /**
         * Test Document file is missing
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $document = $preControl->dom->getElementsByTagName('Document')->item(0);
        $attachment = $document->getElementsByTagName('Attachment')->item(0);
        $attachment->setAttribute('filename', 'notexists.pdf');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        // Fichier de la pièce jointe absent
        $this->assertEquals(TransferError::CODE_ATTACHMENT_MISSING, $errors[0]['code']);

        /**
         * Test virus
         */
        $errors = [];
        $preControl->dom = clone $initialDom;
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $TransferAttachments->updateAll(['virus_name' => 'testunit'], ['id' => $attachments[0]->get('id')]);
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(TransferError::CODE_ATTACHMENT_MISSING, $errors[0]['code']); // Virus testunit détecté
        $TransferAttachments->updateAll(['virus_name' => ''], ['id' => $attachments[0]->get('id')]);

        /**
         * Test bad hash
         */
        $errors = [];
        $initialHash = $attachments[0]->get('hash');
        $TransferAttachments->updateAll(
            [
                'hash_algo' => 'sha256',
                'hash' => 'testunit',
            ],
            ['id' => $attachments[0]->get('id')]
        );
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(
            TransferError::CODE_ATTACHMENT_MISSING,
            $errors[0]['code']
        ); // Empreinte invalide pour la pièce jointe
        $TransferAttachments->updateAll(
            [
                'hash_algo' => 'sha256',
                'hash' => $initialHash,
            ],
            ['id' => $attachments[0]->get('id')]
        );

        /**
         * Test bad format
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $document = $preControl->dom->getElementsByTagName('Document')->item(0);
        $attachment = $document->getElementsByTagName('Attachment')->item(0);
        $attachment->setAttribute('format', 'fmt/notexists');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(307, $errors[0]['code']); // Format du document fmt/notexists ne correspond pas

        /**
         * Test bad mime
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $document = $preControl->dom->getElementsByTagName('Document')->item(0);
        $attachment = $document->getElementsByTagName('Attachment')->item(0);
        $attachment->setAttribute('mimeCode', 'application/notexists');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(307, $errors[0]['code']); // Mimetype du document application/notexists ne correspond pas

        /**
         * Test bad file
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $TransferAttachments->updateAll(['valid' => false], ['id' => $attachments[0]->get('id')]);
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(307, $errors[0]['code']); // Possible erreur sur la validité du format
        $TransferAttachments->updateAll(['valid' => true], ['id' => $attachments[0]->get('id')]);

        /**
         * Test missing file in xml
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $document = $preControl->dom->getElementsByTagName('Document')->item(0);
        $document->parentNode->removeChild($document);
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(
            TransferError::CODE_ATTACHMENT_MISSING,
            $errors[0]['code']
        ); // Fichier sample.odt non référencé

        /**
         * Test doublon
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $document = $preControl->dom->getElementsByTagName('Document')->item(0);
        $document->parentNode->insertBefore($document->cloneNode(true), $document);
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(
            TransferError::CODE_ATTACHMENT_MISSING,
            $errors[0]['code']
        ); // Pièce jointe déjà référencée dans le bordereau

        /**
         * Test format non détecté
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $TransferAttachments->updateAll(['format' => null], ['id' => $attachments[0]->get('id')]);
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(TransferError::CODE_NON_COMPLIANT_FORMAT, $errors[0]['code']); // format de la pièce jointe
        $TransferAttachments->updateAll(
            ['format' => $attachments[0]->get('format')],
            ['id' => $attachments[0]->get('id')]
        );

        /**
         * Test chanque archive doit avoir au moins 1 fichier
         */
        $preControl->dom = clone $initialDom;
        $errors = [];
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $archive = $preControl->xpath->query('ns:Contains')->item(0);
        $archive->parentNode->insertBefore($archive->cloneNode(true), $archive);
        $documents = $preControl->xpath->query('.//ns:Document', $archive);
        foreach ($documents as $document) {
            $document->parentNode->removeChild($document);
        }
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAttachments', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(
            TransferError::CODE_ATTACHMENT_MISSING,
            $errors[0]['code']
        ); // Aucune pièce jointe référencée dans un des noeuds archive
    }

    /**
     * testValidateSa
     */
    public function testValidateSa()
    {
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ControlWorker(new Entity(), $io);
        $entity = new Entity(
            ['transfer_identifier' => 'testunit', 'xml' => self::SEDA_02_XML],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);
        $preControl = new PreControlMessages(self::SEDA_02_XML);
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs');

        /**
         * OK
         */
        $errors = [];
        $this->invokeMethod($worker, 'validateSa', [&$errors]);
        $this->assertEmpty($errors);
        $initialDom = clone $preControl->dom;
        $namespace = $preControl->dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $testfile = @tempnam(TMP_TESTDIR, 'testunit-');

        /**
         * missing sa
         */
        $errors = [];
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $node = $xpath->query($base = '/ns:ArchiveTransfer/ns:ArchivalAgency/ns:Identification')->item(0);
        DOMUtility::setValue($node, 'bad-sa-identifier');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $preControl->path = $testfile;
        $this->invokeMethod($worker, 'validateSa', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(201, $errors[0]['code']); // Entité du service d'archives du bordereau non trouvée

        /**
         * sa TypeEntity != SA
         */
        $errors = [];
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $node = $xpath->query($base)->item(0);
        DOMUtility::setValue($node, 'sv1');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $preControl->path = $testfile;
        $this->invokeMethod($worker, 'validateSa', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(
            201,
            $errors[0]['code']
        ); // Entité correpondante au Service d'archives du bordereau non de type Service d'Archives

        /**
         * sa active = false
         */
        $errors = [];
        $preControl->dom = clone $initialDom;
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $preControl->path = $testfile;
        $OrgEntities->updateAll(['active' => false], []);
        $this->invokeMethod($worker, 'validateSa', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(
            201,
            $errors[0]['code']
        ); // Entité correspondante au Service d'archives du bordereau non active
        $OrgEntities->updateAll(['active' => true], []);

        /**
         * sa perimé
         */
        $errors = [];
        $preControl->dom = clone $initialDom;
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $preControl->path = $testfile;
        $Eaccpfs->updateAll(['to_date' => '2018-06-27'], []);
        $this->invokeMethod($worker, 'validateSa', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(
            201,
            $errors[0]['code']
        ); // Entité correspondante au Service d'archives du bordereau en dehors des dates d'existence
        $OrgEntities->updateAll(['active' => true], []);
    }

    /**
     * testValidateSv
     */
    public function testValidateSv()
    {
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ControlWorker(new Entity(), $io);
        $entity = new Entity(
            ['transfer_identifier' => 'testunit', 'xml' => self::SEDA_02_XML],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);
        $preControl = new PreControlMessages(self::SEDA_02_XML);
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs');
        $sa = $OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SA'])
            ->first();

        /**
         * OK
         */
        $errors = [];
        $this->invokeMethod($worker, 'validateSv', [&$errors, $sa]);
        $this->assertEmpty($errors);
        $initialDom = clone $preControl->dom;
        $namespace = $preControl->dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $testfile = @tempnam(TMP_TESTDIR, 'testunit-');

        /**
         * missing sv
         */
        $errors = [];
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $node = $xpath->query($base = '/ns:ArchiveTransfer/ns:TransferringAgency/ns:Identification')->item(0);
        DOMUtility::setValue($node, 'bad-sv-identifier');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $preControl->path = $testfile;
        $this->invokeMethod($worker, 'validateSv', [&$errors, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(202, $errors[0]['code']); // Entité du service versant du bordereau non trouvée

        /**
         * sv TypeEntity != SV
         */
        $errors = [];
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $node = $xpath->query($base)->item(0);
        DOMUtility::setValue($node, 'cst');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $preControl->path = $testfile;
        $this->invokeMethod($worker, 'validateSv', [&$errors, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(
            202,
            $errors[0]['code']
        ); // Entité correpondante au Service versant du bordereau non de type Service Versant

        /**
         * sv active = false
         */
        $errors = [];
        $preControl->dom = clone $initialDom;
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $preControl->path = $testfile;
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $OrgEntities->updateAll(['active' => false], []);
        $this->invokeMethod($worker, 'validateSv', [&$errors, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(
            202,
            $errors[0]['code']
        ); // Entité correspondante au Service versant du bordereau non active
        $OrgEntities->updateAll(['active' => true], []);

        /**
         * sv perimé
         */
        $errors = [];
        $preControl->dom = clone $initialDom;
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $preControl->path = $testfile;
        $Eaccpfs->updateAll(['to_date' => '2018-06-27'], []);
        $this->invokeMethod($worker, 'validateSv', [&$errors, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(
            202,
            $errors[0]['code']
        ); // Entité correspondante au Service versant du bordereau en dehors des dates d'existence
        $OrgEntities->updateAll(['active' => true], []);
    }

    /**
     * testValidateSp
     */
    public function testValidateSp()
    {
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ControlWorker(new Entity(), $io);
        $entity = new Entity(
            ['transfer_identifier' => 'testunit', 'xml' => self::SEDA_02_XML],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);
        $preControl = new PreControlMessages(self::SEDA_02_XML);
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $sa = $OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SA'])
            ->first();

        /**
         * OK
         */
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $OrgEntities->updateAll(['identifier' => 'sv2'], ['id' => 4]);
        $errors = [];
        $this->invokeMethod($worker, 'validateSp', [&$errors, $sa]);
        $this->assertEmpty($errors);
        $namespace = $preControl->dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $testfile = @tempnam(TMP_TESTDIR, 'testunit-');

        /**
         * missing sv
         */
        $errors = [];
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $node = $xpath->query($base = '//ns:OriginatingAgency/ns:Identification')->item(0);
        DOMUtility::setValue($node, 'bad-sp-identifier');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $preControl->path = $testfile;
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateSp', [&$errors, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(200, $errors[0]['code']); // Entité du service producteur du bordereau non trouvée

        /**
         * sp TypeEntity != SP
         */
        $errors = [];
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $node = $xpath->query($base)->item(0);
        DOMUtility::setValue($node, 'cst');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $preControl->path = $testfile;
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateSp', [&$errors, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(
            200,
            $errors[0]['code']
        ); // Entité correpondante au Service producteur du bordereau non de type Service Producteur
    }

    /**
     * testValidateKeywords
     */
    public function testValidateKeywords()
    {
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ControlWorker(new Entity(), $io);
        $entity = new Entity(
            ['transfer_identifier' => 'testunit', 'xml' => self::SEDA_02_XML],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);
        $preControl = new PreControlMessages(self::SEDA_02_XML);
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);

        /**
         * ok
         */
        $errors = [];
        $this->invokeMethod($worker, 'validateKeywords', [&$errors]);
        $this->assertEmpty($errors);
        $namespace = $preControl->dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $util = new DOMUtility($preControl->dom);

        $contentDescription1 = $xpath->query('//ns:ContentDescription')->item(0);
        $keyword = $util->createElement('Keyword');
        $keywordContent = $util->createElement('KeywordContent', 'foo');
        $keyword->appendChild($keywordContent);
        $contentDescription1->appendChild($keyword);

        $this->invokeMethod($worker, 'validateKeywords', [&$errors]);
        $this->assertTrue(empty($errors) && is_array($errors));

        /**
         * keyword identique dans une autre archive -> ok
         */
        $contentDescription2 = $xpath->query('//ns:ContentDescription')->item(1);
        $keyword = $util->createElement('Keyword');
        $keywordContent = $util->createElement('KeywordContent', 'foo');
        $keyword->appendChild($keywordContent);
        $contentDescription2->appendChild($keyword);

        $this->invokeMethod($worker, 'validateKeywords', [&$errors]);
        $this->assertTrue(empty($errors) && is_array($errors));

        /**
         * double -> ko
         */
        $errors = [];
        $keyword = $util->createElement('Keyword');
        $keywordContent = $util->createElement('KeywordContent', 'foo');
        $keyword->appendChild($keywordContent);
        $contentDescription1->appendChild($keyword);

        $node = $xpath->query('//ns:TransferringAgency')->item(0);
        $node->parentNode->appendChild($node);

        $this->invokeMethod($worker, 'validateKeywords', [&$errors]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(213, $errors[0]['code']);
    }

    /**
     * testValidateProfiles
     */
    public function testValidateProfiles()
    {
        Filesystem::copy(self::SEDA_02_XSD, TMP_TESTDIR . DS . 'sample_seda02.xsd');
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $FileuploadsProfiles = TableRegistry::getTableLocator()->get('FileuploadsProfiles');
        $FileuploadsProfiles->deleteAll([]);
        $file = $Fileuploads->newEntity(
            [
                'name' => 'sample_seda02.xsd',
                'path' => TMP_TESTDIR . DS . 'sample_seda02.xsd',
                'user_id' => 1,
                'locked' => 1,
            ]
        );
        $Fileuploads->saveOrFail($file, ['silent' => true]);
        $link = $FileuploadsProfiles->newEntity(
            [
                'fileupload_id' => $file->get('id'),
                'profile_id' => 1,
            ]
        );
        $FileuploadsProfiles->save($link);

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ControlWorker(new Entity(), $io);
        $entity = new Entity(
            ['transfer_identifier' => 'testunit', 'xml' => self::SEDA_02_XML],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);
        $preControl = new PreControlMessages(self::SEDA_02_XML);
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $Profiles = TableRegistry::getTableLocator()->get('Profiles');
        $sa = $OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SA'])
            ->first();

        /**
         * OK
         */
        $errors = [];
        $this->invokeMethod($worker, 'validateProfiles', [&$errors, $sa]);
        $this->assertEmpty($errors);
        $namespace = $preControl->dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $testfile = @tempnam(TMP_TESTDIR, 'testunit-');
        $entity->set('xml', $testfile);
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);
        $initialDom = clone $preControl->dom;

        /**
         * missing profile
         */
        $errors = [];
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $node = $xpath->query('//ns:ArchivalProfile')->item(0);
        DOMUtility::setValue($node, 'bad-profile-identifier');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $preControl->dom->save($testfile);
        $this->invokeMethod($worker, 'validateProfiles', [&$errors, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(203, $errors[0]['code']); // profil d'archive non trouvé

        /**
         * profile active = false
         */
        $errors = [];
        $preControl->dom = clone $initialDom;
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $preControl->dom->save($testfile);
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $Profiles->updateAll(['active' => false], []);
        $this->invokeMethod($worker, 'validateProfiles', [&$errors, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(203, $errors[0]['code']); // Profil d'archive non actif
        $Profiles->updateAll(['active' => true], []);

        /**
         * profile check xsd
         */
        $errors = [];
        $preControl->dom = clone $initialDom;
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $node = $xpath->query('//ns:TransferringAgency')->item(0);
        $node->parentNode->removeChild($node);
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $preControl->dom->save($testfile);
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateProfiles', [&$errors, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(
            203,
            $errors[0]['code']
        ); // Schema du profil test : Did not expect element schema there en ligne 2
        $Profiles->updateAll(['active' => true], []);
    }

    /**
     * testValidateServiceLevels
     */
    public function testValidateServiceLevels()
    {
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ControlWorker(new Entity(), $io);
        $entity = new Entity(
            ['transfer_identifier' => 'testunit', 'xml' => self::SEDA_02_XML],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);
        $preControl = new PreControlMessages(self::SEDA_02_XML);
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $sa = $OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SA'])
            ->first();

        /**
         * OK
         */
        $errors = [];
        $this->invokeMethod($worker, 'validateServiceLevels', [&$errors, $sa]);
        $this->assertEmpty($errors);
        $namespace = $preControl->dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $testfile = @tempnam(TMP_TESTDIR, 'testunit-');
        $initialDom = clone $preControl->dom;

        /**
         * missing service level
         */
        $errors = [];
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $node = $xpath->query('//ns:ServiceLevel')->item(0);
        DOMUtility::setValue($node, 'bad-service-level-identifier');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $preControl->path = $testfile;
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateServiceLevels', [&$errors, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(203, $errors[0]['code']); // niveau de service non trouvé

        /**
         * service_level active = false
         */
        $errors = [];
        $preControl->dom = clone $initialDom;
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $ServiceLevels->updateAll(['active' => false], []);
        $this->invokeMethod($worker, 'validateServiceLevels', [&$errors, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(203, $errors[0]['code']); // Niveau de service non actif
    }

    /**
     * testValidateAgreements
     */
    public function testValidateAgreements()
    {
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = new ControlWorker(new Entity(), $io);
        $entity = new Entity(
            ['id' => 1, 'transfer_identifier' => 'testunit', 'xml' => self::SEDA_02_XML],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);
        $preControl = new PreControlMessages(self::SEDA_02_XML);
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $AgreementsTransferringAgencies = TableRegistry::getTableLocator()->get('AgreementsTransferringAgencies');
        $AgreementsOriginatingAgencies = TableRegistry::getTableLocator()->get('AgreementsOriginatingAgencies');
        $AgreementsProfiles = TableRegistry::getTableLocator()->get('AgreementsProfiles');
        $AgreementsServiceLevels = TableRegistry::getTableLocator()->get('AgreementsServiceLevels');
        $sa = $OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SA'])
            ->first();

        /**
         * OK
         */
        $errors = [];
        $this->invokeMethod($worker, 'validateAgreements', [&$errors, $sa, $sa]);
        $this->assertEmpty($errors);
        $namespace = $preControl->dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $initialDom = clone $preControl->dom;

        /**
         * missing agreement
         */
        $errors = [];
        $xpath = new DOMXPath($preControl->dom);
        $xpath->registerNamespace('ns', $namespace);
        $node = $xpath->query('//ns:ArchivalAgreement')->item(0);
        DOMUtility::setValue($node, 'bad-agreement-identifier');
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $this->invokeMethod($worker, 'validateAgreements', [&$errors, $sa, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(300, $errors[0]['code']); // Accord de versement non trouvé

        /**
         * agreement active = false
         */
        $errors = [];
        $preControl->dom = clone $initialDom;
        $preControl->xpath = new DOMXPath($preControl->getDom());
        $preControl->xpath->registerNamespace('ns', $preControl->getNamespace());
        $this->invokeProperty($worker, 'PreControlMessages', 'set', $preControl);
        $this->invokeProperty($worker, 'util', 'set', new DOMUtility($preControl->dom));
        $Agreements->updateAll(['active' => false], []);
        $this->invokeMethod($worker, 'validateAgreements', [&$errors, $sa, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(300, $errors[0]['code']); // Accord de versement non actif
        $Agreements->updateAll(['active' => true], []);

        /**
         * agreement perimé
         */
        $errors = [];
        $Agreements->updateAll(['date_end' => '2018-06-27'], []);
        $this->invokeMethod($worker, 'validateAgreements', [&$errors, $sa, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(312, $errors[0]['code']); // Accord de versement en dehors des dates de validité
        $Agreements->updateAll(['date_end' => null], []);

        /**
         * sv non autorisé
         */
        $errors = [];
        $Agreements->updateAll(['allow_all_transferring_agencies' => false], []);
        $AgreementsTransferringAgencies->deleteAll([]);
        $this->invokeMethod($worker, 'validateAgreements', [&$errors, $sa, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(303, $errors[0]['code']); // Service versant du transfert sa non autorisé

        /**
         * sp non autorisé
         */
        $errors = [];
        $Agreements->updateAll(
            [
                'allow_all_transferring_agencies' => true,
                'allow_all_originating_agencies' => false,
            ],
            []
        );
        $AgreementsOriginatingAgencies->deleteAll([]);
        $this->invokeMethod($worker, 'validateAgreements', [&$errors, $sa, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(303, $errors[0]['code']); // Service producteur du transfert sp non autorisé

        /**
         * profile non autorisé
         */
        $errors = [];
        $Agreements->updateAll(
            [
                'allow_all_originating_agencies' => true,
                'allow_all_profiles' => false,
            ],
            []
        );
        $AgreementsProfiles->deleteAll([]);
        $this->invokeMethod($worker, 'validateAgreements', [&$errors, $sa, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(303, $errors[0]['code']); // Profil d'archive du transfert test non autorisé

        /**
         * service_level non autorisé
         */
        $errors = [];
        $Agreements->updateAll(
            [
                'allow_all_profiles' => true,
                'allow_all_service_levels' => false,
            ],
            []
        );
        $AgreementsServiceLevels->deleteAll([]);
        $this->invokeMethod($worker, 'validateAgreements', [&$errors, $sa, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(303, $errors[0]['code']); // Niveau de service du transfert service-level-test non autorisé

        /**
         * format non autorisé
         */
        $errors = [];
        $Agreements->updateAll(
            [
                'allowed_formats' => '["pronom-fmt/18"]',
                'allow_all_service_levels' => true,
            ],
            []
        );
        $this->invokeMethod($worker, 'validateAgreements', [&$errors, $sa, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(307, $errors[0]['code']); // Format ... non autorisé

        /**
         * taille de fichier non autorisé
         */
        $attachments = [];
        $basePath = AbstractGenericMessageForm::getDataDirectory(1) . DS . 'attachments';
        foreach (self::ATTACHMENTS as $path => $values) {
            $filename = basename($path);
            Filesystem::copy($path, $basePath . DS . $filename);
            $response = exec('sf -json ' . escapeshellarg($path) . ' 2>/dev/null');
            $data = json_decode($response);
            $attachments[] = new Entity(
                [
                    'transfer_id' => 1,
                    'filename' => $filename,
                    'path' => $basePath . DS . $filename,
                    'size' => filesize($basePath . DS . $filename),
                    'hash' => hash_file('sha256', $basePath . DS . $filename),
                    'hash_algo' => 'sha256',
                    'extension' => pathinfo($filename, PATHINFO_EXTENSION),
                    'format' => $data->files[0]->matches[0]->id,
                    'mime' => $data->files[0]->matches[0]->mime,
                ] + $values,
                ['validate' => false]
            );
        }
        $entity = new Entity(
            [
                'id' => 1,
                'transfer_identifier' => 'testunit',
                'xml' => self::SEDA_02_XML,
                'transfer_attachments' => $attachments,
            ],
            ['validate' => false]
        );
        $this->invokeProperty($worker, 'Transfer', 'set', $entity);
        $errors = [];
        $Agreements->updateAll(
            [
                'allowed_formats' => '',
                'max_size_per_transfer' => 1024,
            ],
            []
        );
        $this->invokeMethod($worker, 'validateAgreements', [&$errors, $sa, $sa]);
        $this->assertTrue(!empty($errors) && is_array($errors));
        $this->assertEquals(307, $errors[0]['code']); // Volume des pièces jointes dépasse le volume maximum autorisé
    }

    /**
     * testWork
     */
    public function testWork()
    {
        // Mocking
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = $this->getMockBuilder(ControlWorker::class)
            ->setConstructorArgs([new \AsalaeCore\ORM\Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();

        // pas de transfer 0
        try {
            $worker->work(['transfer_id' => 0]);
            $this->fail();
        } catch (Exception $e) {
            $this->assertInstanceOf(CantWorkException::class, $e);
        }

        // n'est pas dans un etat d'analyse
        try {
            $worker->work(['transfer_id' => 1]);
            $this->fail();
        } catch (Exception $e) {
            $this->assertInstanceOf(CantWorkException::class, $e);
        }

        // Ménage
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $TransferErrors = TableRegistry::getTableLocator()->get('TransferErrors');
        $TransferErrors->deleteAll([]);
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $TransferAttachments->deleteAll([]);
        $FileuploadsProfiles = TableRegistry::getTableLocator()->get('FileuploadsProfiles');
        $FileuploadsProfiles->deleteAll([]);

        /**
         * Préparation du transfer de test
         */
        $attachments = [];
        $basePath = AbstractGenericMessageForm::getDataDirectory(1);
        foreach (self::ATTACHMENTS as $path => $values) {
            $filename = basename($path);
            $fullPath = $basePath . DS . 'attachments' . DS . $filename;
            Filesystem::copy($path, $fullPath);
            $attachments[] = new Entity(
                [
                    'transfer_id' => 1,
                    'filename' => $filename,
                    'path' => $fullPath,
                    'size' => filesize($fullPath),
                    'hash' => hash_file('sha256', $fullPath),
                    'hash_algo' => 'sha256',
                    'pronom_id' => strpos($filename, '.pdf')
                        ? 2
                        : (strpos($filename, '.odt') ? 3 : null),
                ] + $values,
                ['validate' => false]
            );
        }

        // préparation d'un transfert valide
        $entity = $Transfers->get(1);
        $entity->set('state', 'controlling');
        $entity->set('transfer_attachments', $attachments);
        $Transfers->save($entity);
        $fullPath = $basePath . DS . 'message' . DS . $entity->get('filename');
        Filesystem::copy(self::SEDA_10_XML, $fullPath);

        /**
         * Transfer valide avec validation auto
         */
        $TransferErrors->deleteAll([]);
        $data = ['user_id' => 1, 'transfer_id' => 1];
        $worker->beforeWork($data);
        $worker->work($data);
        $this->assertEquals(0, $TransferErrors->find()->count());
        $entity = $Transfers->get(1);
        $this->assertEquals('validating', $entity->get('state'));

        // changement d'accord de versement (!= accord par defaut)
        $dom = new DOMDocument();
        $dom->load($fullPath);
        $node = $dom->getElementsByTagName('ArchivalAgreement')->item(0);
        DOMUtility::setValue($node, 'agr-test02');
        $dom->save($fullPath);
        $entity->set('state', 'controlling');
        $Transfers->save($entity);

        /**
         * Transfer conforme validé auto avec un accords != default
         */
        /** @var AgreementsTable $ValidationProcesses */
        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $Agreements->updateAll(
            [
                'allow_all_transferring_agencies' => true,
                'allow_all_originating_agencies' => true,
                'allow_all_service_levels' => true,
                'allow_all_profiles' => true,
                'active' => true,
            ],
            ['id' => 2]
        );
        /** @var ControlWorker|MockObject $worker */
        $worker = $this->getMockBuilder(ControlWorker::class)
            ->setConstructorArgs([new \AsalaeCore\ORM\Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $worker->beforeWork($data);
        $worker->work($data);
        $this->assertEquals(0, $TransferErrors->find()->count());
        $entity = $Transfers->get(1);
        $this->assertEquals('archiving', $entity->get('state'));

        // préparation d'un transfert non-conforme
        $dom = new DOMDocument();
        $dom->load($fullPath);
        $node = $dom->getElementsByTagName('Attachment')->item(0);
        $node->parentNode->removeChild($node);
        $dom->save($fullPath);
        $entity->set('state', 'controlling');
        $Transfers->save($entity);

        /**
         * Transfer non conforme en attente de validation
         */
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        $worker->beforeWork($data);
        $worker->work($data);
        $this->assertNotEquals(0, $TransferErrors->find()->count());
        $entity = $Transfers->get(1);
        $this->assertEquals('validating', $entity->get('state'));
    }
}
