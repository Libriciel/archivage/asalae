<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\TestSuite\VolumeSample;
use Asalae\Worker\ArchiveUnitWorker;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Notify;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const TMP_TESTDIR;

class ArchiveUnitWorkerTest extends TestCase
{
    use InvokePrivateTrait;
    use AutoFixturesTrait;

    public const string SEDA_02_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xml';
    public const string SEDA_02_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02_description.xml';
    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';
    public const string SEDA_10_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml';
    public const string SEDA_02_XSD = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xsd';
    public const array ATTACHMENTS = [
        ASALAE_CORE_TEST_DATA . DS . 'sample.odt',
        ASALAE_CORE_TEST_DATA . DS . 'sample.pdf',
        ASALAE_CORE_TEST_DATA . DS . 'sample.rng',
    ];
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_EVENT_LOGS,
        'app.AccessRuleCodes',
        'app.AccessRules',
        'app.AppraisalRuleCodes',
        'app.AppraisalRules',
        'app.ArchiveDescriptions',
        'app.Archives',
        'app.CompositeArchiveUnits',
        'app.Fileuploads',
        'app.Siegfrieds',
        'app.StoredFiles',
        'app.TransferAttachments',
        'app.TypeEntities',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('Beanstalk.disable_check_ttr', true);
        Filesystem::reset();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        Configure::write('ArchiveUnitWorker.saveRefs', TMP_TESTDIR . DS);
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $Archives->updateAll(['state' => ArchivesTable::S_CREATING], []);
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $ArchiveUnits->cascadeDelete($ArchiveUnits->get(1));
        $ArchiveUnits->cascadeDelete($ArchiveUnits->get(3));

        // SEDA 1.0
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = $this->getMockBuilder(ArchiveUnitWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $data = ['transfer_id' => 1, 'index' => 1];
        $worker->beforeWork($data);
        $worker->work($data);
        $this->assertCount(2, $ArchiveUnits->find()->where(['archive_id' => 1]));

        // SEDA 2.1
        $data = ['transfer_id' => 3, 'index' => 1];
        $worker->beforeWork($data);
        $worker->work($data);
        $this->assertCount(1, $ArchiveUnits->find()->where(['archive_id' => 2]));
    }

    /**
     * testComposite
     */
    public function testComposite()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $CompositeArchiveUnits = $loc->get('CompositeArchiveUnits');
        $Archives->updateAll(['state' => ArchivesTable::S_UPDATE_CREATING], ['id' => 2]);
        $ArchiveUnits = $loc->get('ArchiveUnits');
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        $ArchivesTransfers = $loc->get('ArchivesTransfers');
        $transfer = $Transfers->newEntityFromXml(ASALAE_CORE_TEST_DATA . DS . 'sample_seda21_composite.xml');
        $transfer->set('state', TransfersTable::S_ARCHIVING);
        $transfer->set('files_deleted', false);
        $Transfers->saveOrFail($transfer);
        Filesystem::copy(
            ASALAE_CORE_TEST_DATA . DS . 'sample_seda21_composite.xml',
            $transfer->get('xml')
        );
        $ArchivesTransfers->findOrCreate(
            [
                'archive_id' => 2,
                'transfer_id' => $transfer->id,
            ]
        );
        $initialUnitsCount = $ArchiveUnits->find()->where(['archive_id' => 2])->count();
        $initialCompositeUnitsCount = $CompositeArchiveUnits->find()->where(['transfer_id' => $transfer->id])->count();

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = $this->getMockBuilder(ArchiveUnitWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        $data = ['transfer_id' => $transfer->id, 'composite_id' => 2];
        $worker->beforeWork($data);
        $worker->work($data);

        $this->assertCount(
            $initialUnitsCount + 1,
            $ArchiveUnits->find()->where(['archive_id' => 2])
        );
        $this->assertCount(
            $initialCompositeUnitsCount + 1,
            $CompositeArchiveUnits->find()->where(['transfer_id' => $transfer->id])
        );
        $this->assertEquals(
            ArchivesTable::S_UPDATE_DESCRIBING,
            $Archives->get(2)->get('state')
        );
    }
}
