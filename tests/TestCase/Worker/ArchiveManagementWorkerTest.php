<?php

namespace Asalae\Test\TestCase\Worker;

use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Entity\LifecycleXmlArchiveFile;
use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\DescriptionXmlArchiveFilesTable;
use Asalae\Model\Table\LifecycleXmlArchiveFilesTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Volume\VolumeManager;
use Asalae\Worker\ArchiveManagementWorker;
use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DOMDocument;
use DOMElement;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const PREMIS_V3;
use const SEDA_V10_XSD;
use const TMP_TESTDIR;
use const TMP_VOL1;
use const TMP_VOL2;

class ArchiveManagementWorkerTest extends TestCase
{
    use InvokePrivateTrait;

    public const string SEDA_02_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xml';
    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';
    public const string SEDA_21_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda21.xml';

    public array $fixtures = [
        'app.AccessRules',
        'app.Agreements',
        'app.AppraisalRules',
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveBinaryConversions',
        'app.ArchiveFiles',
        'app.ArchiveIndicators',
        'app.ArchiveUnits',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.CompositeArchiveUnits',
        'app.EventLogs',
        'app.MessageIndicators',
        'app.OrgEntities',
        'app.Profiles',
        'app.Pronoms',
        'app.SecureDataSpaces',
        'app.ServiceLevels',
        'app.StoredFiles',
        'app.StoredFilesVolumes',
        'app.TransferAttachments',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
        'app.ValidationChains',
        'app.Volumes',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('Beanstalk.disable_check_ttr', true);
        Filesystem::reset();
        VolumeSample::init();
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Utility::reset();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $ArchiveFiles = $loc->get('ArchiveFiles');
        $Transfers = $loc->get('Transfers');
        $Transfers->updateAll(['state' => 'archiving', 'is_accepted' => true], []);
        $EventLogs = $loc->get('EventLogs');
        $Archives->updateAll(['state' => 'storing'], ['id' => 1]);
        $ArchiveFiles->deleteAll([]);
        $EventLogs->deleteAll([]);

        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01 ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02 ' . TMP_VOL2);

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = $this->getMockBuilder(ArchiveManagementWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $data = ['transfer_id' => 1];
        $manager = new VolumeManager(1);
        $manager->fileDelete('sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml');
        $manager->fileDelete('sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml');
        $manager->fileDelete('sa/no_profile/2021-12/sa_1/management_data/sa_1_transfer.xml');
        $worker->beforeWork($data);
        $worker->work($data);
        $this->assertCount(3, $ArchiveFiles->find());

        $archive = $Archives->find()
            ->where(['Archives.id' => 1])
            ->contain(['DescriptionXmlArchiveFiles' => ['StoredFiles']])
            ->first();
        $this->assertTrue((bool)$archive);
        $this->assertEquals('available', $archive->get('state'));
        /** @var DescriptionXmlArchiveFile $desc */
        $desc = $archive->get('description_xml_archive_file');
        $this->assertTrue((bool)$desc);
        $dom = $desc->getDom();
        $this->assertTrue($dom->schemaValidate(SEDA_V10_XSD));

        $premis = Hash::get(
            $ArchiveFiles->find()
                ->where(['type' => 'lifecycle'])
                ->contain(['StoredFiles'])
                ->first(),
            'stored_file.file'
        );
        $dom = new DOMDocument();
        $dom->loadXML($premis);
        $this->assertTrue($dom->schemaValidate(PREMIS_V3));
        /**
         * Transfer + Archive + AU(archive) + 2 AU(archive_object) + 3 AU(document)
         *      + 1(normalement 3) binaries + 3 archive_files
         */
        $this->assertCount(9, $dom->getElementsByTagName('object'));
        $this->assertCount(6, $dom->getElementsByTagName('event'));
        $this->assertCount(1, $dom->getElementsByTagName('agent'));

        // test fichiers existant (mais pas en base)
        $Archives->updateAll(['state' => 'storing'], []);
        $ArchiveFiles->deleteAll([]);

        // transfer_add, archive_add, archiveunit_add, archivebinary_add
        $this->assertCount(6, $EventLogs->find());
    }

    /**
     * testAutocompletion
     */
    public function testAutocompletion()
    {
        $exec = $this->createMock(Exec::class);
        $result1 = new CommandResult(
            [
                'stdout' => json_encode(
                    [
                        'files' => [
                            0 => [
                                'matches' => [
                                    0 => [
                                        'id' => 'fmt/000',
                                        'mime' => 'application/test',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    JSON_UNESCAPED_SLASHES
                ),
                'stderr' => '',
                'code' => 0,
                'success' => true,
            ]
        );
        $result2 = new CommandResult(
            [
                'stdout' => '',
                'stderr' => '',
                'code' => 1,
                'success' => false,
            ]
        );
        $exec->method('command')->willReturn($result1, $result2);

        Utility::set('Exec', $exec);

        $loc = TableRegistry::getTableLocator();
        $TransferAttachments = $loc->get('TransferAttachments');
        $Transfers = $loc->get('Transfers');
        $TransferAttachments->updateAll(
            [
                'hash' => '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',
                'format' => 'fmt/122',
                'mime' => 'text/plain',
            ],
            ['transfer_id' => 1]
        );

        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01 ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02 ' . TMP_VOL2);

        TableRegistry::getTableLocator()->get('Transfers')->updateAll(['is_conform' => null], []);
        $transfer = $Transfers->get(1);
        $util = DOMUtility::load($transfer->get('xml'));

        $this->assertNotFalse((bool)$transfer);
        $this->assertNull($transfer->get('is_conform'));

        $attachment = $TransferAttachments->find()->where(['transfer_id' => 1])->first();
        $path = $attachment->get('path');
        Filesystem::dumpFile($path, 'test');

        /** @var DOMElement $integrity */
        foreach ($util->xpath->query('//ns:Integrity') as $integrity) {
            $integrity->parentNode->removeChild($integrity);
        }
        /** @var DOMElement $size */
        foreach ($util->xpath->query('//ns:Size') as $size) {
            $size->parentNode->removeChild($size);
        }

        $this->assertCount(0, $util->xpath->query('//ns:Integrity'));
        $this->assertCount(0, $util->xpath->query('//ns:Size'));

        /** @var DOMElement $att1 */
        $att1 = $util->node('//ns:Attachment');
        $att1->removeAttribute('mimeCode');
        $att1->removeAttribute('format');
        $this->assertEmpty($att1->getAttribute('mimeCode'));
        $this->assertEmpty($att1->getAttribute('format'));
        $util->dom->save($transfer->get('xml'));

        /**
         * worker
         */
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $Transfers = $loc->get('Transfers');
        $Transfers->updateAll(['state' => 'archiving', 'is_accepted' => true], []);
        Filesystem::mkdir(sys_get_temp_dir() . DS . 'volume01');
        Filesystem::mkdir(sys_get_temp_dir() . DS . 'volume02');
        $Archives->updateAll(['state' => 'storing'], []);

        $ArchiveFiles = $loc->get('ArchiveFiles');
        $ArchiveFiles->deleteAll([]);
        $manager = new VolumeManager(1);
        $manager->fileDelete('sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml');
        $manager->fileDelete('sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml');
        $manager->fileDelete('sa/no_profile/2021-12/sa_1/management_data/sa_1_transfer.xml');

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = $this->getMockBuilder(ArchiveManagementWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $data = ['transfer_id' => 1];
        $worker->beforeWork($data);
        $worker->work($data);

        /** @var DescriptionXmlArchiveFile $desc */
        $desc = $Archives->find()
            ->where(['Archives.id' => 1])
            ->contain(['DescriptionXmlArchiveFiles' => ['StoredFiles']])
            ->firstOrFail()
            ->get('description_xml_archive_file');
        $util = new DOMUtility($desc->getDom());
        $this->assertCount(1, $util->xpath->query('//ns:Integrity'));
        $this->assertCount(1, $util->xpath->query('//ns:Size'));

        $this->assertTrue($util->dom->schemaValidate(SEDA_V10_XSD));

        $attachmentNodes = $util->xpath->query('//ns:Attachment');
        /** @var DOMElement $att1 */
        $att1 = $attachmentNodes->item(0);
        $this->assertEquals('text/plain', $att1->getAttribute('mimeCode'));
        $this->assertEquals('fmt/122', $att1->getAttribute('format'));
    }

    /**
     * testComposite
     */
    public function testComposite()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $Archives->updateAll(['state' => ArchivesTable::S_UPDATE_STORING], ['id' => 2]);
        $archive = $Archives->get(2);
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $ArchiveBinariesArchiveUnits = $loc->get('ArchiveBinariesArchiveUnits');
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $CompositeArchiveUnits = $loc->get('CompositeArchiveUnits');
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        $ArchivesTransfers = $loc->get('ArchivesTransfers');
        $transfer = $Transfers->newEntityFromXml(ASALAE_CORE_TEST_DATA . DS . 'sample_seda21_composite.xml');
        $transfer->set('state', TransfersTable::S_ARCHIVING);
        $transfer->set('created_user_id', 1);
        $transfer->set('is_accepted', true);
        $transfer->set('files_deleted', false);
        $Transfers->saveOrFail($transfer);
        Filesystem::copy(
            ASALAE_CORE_TEST_DATA . DS . 'sample_seda21_composite.xml',
            $transfer->get('xml')
        );
        $ArchivesTransfers->findOrCreate(
            [
                'archive_id' => 2,
                'transfer_id' => $transfer->id,
            ]
        );

        $ArchiveUnits->updateAll(['lft' => 5, 'rght' => 8], ['id' => 3]);
        $archiveUnit = $ArchiveUnits->findOrCreate(
            [
                'state' => 'creating',
                'archive_id' => 2,
                'access_rule_id' => 3,
                'appraisal_rule_id' => 2,
                'archival_agency_identifier' => 'sa_2_0001',
                'transferring_agency_identifier IS' => null,
                'originating_agency_identifier IS' => null,
                'name' => 'composite',
                'original_local_size' => 0,
                'original_total_size' => 0,
                'parent_id' => 3,
                'lft' => 6,
                'rght' => 7,
                'xml_node_tagname' => 'ArchiveUnit[1]',
                'original_total_count' => 0,
                'original_local_count' => 0,
                'search' => '"sa_2_0001" "composite" "cette unite d\'archives ' .
                    'va etre insere dans sa_2_description.xml"',
                'expired_dua_root' => false,
                'full_search' => '"sa_2_0001" "composite" "cette unite ' .
                    'd\'archives va etre insere dans sa_2_description.xml"',
            ]
        );
        $CompositeArchiveUnits->findOrCreate(
            [
                'transfer_id' => $transfer->id,
                'archive_unit_id' => $archiveUnit->id,
                'transfer_xpath' => '/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata/ns:ArchiveUnit',
            ]
        );
        Filesystem::copy(
            ASALAE_CORE_TEST_DATA . DS . 'logo.png',
            TMP_TESTDIR . DS . 'logo.png'
        );
        $TransferAttachments = $loc->get('TransferAttachments');
        $attachment = $TransferAttachments->newEntity(
            [
                'transfer_id' => $transfer->id,
                'filename' => 'logo.png',
                'size' => filesize(TMP_TESTDIR . DS . 'logo.png'),
                'hash' => 'foo',
                'hash_algo' => 'bar',
            ]
        );
        $TransferAttachments->saveOrFail($attachment);
        Filesystem::rename(
            TMP_TESTDIR . DS . 'logo.png',
            $attachment->get('path')
        );

        $path = $archive->get('storage_path') . '/original_data/'
            . $attachment->get('filename');
        $manager = new VolumeManager(1);
        $storedFile = $manager->fileUpload($attachment->get('path'), $path);
        $binary = $ArchiveBinaries->findOrCreate(
            [
                'type' => 'original_data',
                'filename' => 'logo.png',
                'mime' => 'image/png',
                'extension' => 'png',
                'stored_file_id' => $storedFile->id,
                'in_rgi' => true,
            ]
        );
        $ArchiveBinariesArchiveUnits->findOrCreate(
            [
                'archive_binary_id' => $binary->id,
                'archive_unit_id' => $archiveUnit->id,
            ]
        );

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $worker = $this->getMockBuilder(ArchiveManagementWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        $data = ['transfer_id' => $transfer->id, 'composite_id' => 2];
        $worker->beforeWork($data);
        $worker->work($data);

        $loc = TableRegistry::getTableLocator();
        /** @var DescriptionXmlArchiveFilesTable $DescriptionXmlArchiveFiles */
        $DescriptionXmlArchiveFiles = $loc->get('DescriptionXmlArchiveFiles');
        /** @var DescriptionXmlArchiveFile $description */
        $description = $DescriptionXmlArchiveFiles->find()
            ->where(
                [
                    'type' => ArchiveFilesTable::TYPE_DESCRIPTION,
                    'archive_id' => 2,
                ]
            )
            ->contain(['StoredFiles'])
            ->firstOrFail();
        $util = new DOMUtility($description->getDom());
        $this->assertCount(1, $util->xpath->query('//ns:Attachment[@filename="logo.png"]'));
        $this->assertCount(
            1,
            $util->xpath->query('//ns:ArchivalAgencyArchiveUnitIdentifier[text()="sa_2_0001"]')
        );

        /** @var LifecycleXmlArchiveFilesTable $LifecycleXmlArchiveFiles */
        $LifecycleXmlArchiveFiles = $loc->get('LifecycleXmlArchiveFiles');
        /** @var LifecycleXmlArchiveFile $lifecycle */
        $lifecycle = $LifecycleXmlArchiveFiles->find()
            ->where(
                [
                    'type' => 'lifecycle',
                    'archive_id' => 2,
                ]
            )
            ->firstOrFail();
        $util = new DOMUtility($lifecycle->getDom());
        // ancien toujours présent
        $this->assertCount(
            1,
            $util->xpath->query('//ns:objectIdentifierValue[text()="Transfers:3"]')
        );
        $this->assertCount(
            1,
            $util->xpath->query('//ns:linkingObjectIdentifierValue[text()="Transfers:3"]')
        );
        // nouveau présent également
        $this->assertCount(
            1,
            $util->xpath->query('//ns:objectIdentifierValue[text()="Transfers:' . $transfer->id . '"]')
        );
        $this->assertCount(
            1,
            $util->xpath->query('//ns:linkingObjectIdentifierValue[text()="Transfers:' . $transfer->id . '"]')
        );
        // hash de la description mis à jour
        $hash = $util->nodeValue(
            '//ns:objectIdentifierValue[text()="ArchiveFiles:' . $description->id . '"]'
            . '/../../ns:objectCharacteristics/ns:fixity/ns:messageDigest'
        );
        $this->assertEquals(Hash::get($description, 'stored_file.hash'), $hash);

        $this->assertEquals(
            ArchivesTable::S_AVAILABLE,
            $Archives->get(2)->get('state')
        );
    }
}
