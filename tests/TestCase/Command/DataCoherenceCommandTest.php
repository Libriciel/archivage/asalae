<?php

namespace Asalae\Test\TestCase\Command;

use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\ORM\TableRegistry;

use function __;

/**
 * Asalae\Command\DataCoherenceCommand Test Case
 */
class DataCoherenceCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public array $fixtures = [
        'app.Users',
        'app.OrgEntities',
        'app.TypeEntities',
        'app.RolesTypeEntities',
        'app.Roles',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * testMain
     */
    public function testMain()
    {
        $this->exec('data_coherence all --skip-large-tables --large-table-threshold 1');
        $this->assertTextContains('done', implode('|', $this->_out->messages()));

        $Users = TableRegistry::getTableLocator()->get('Users');
        $Users->updateAll(['email' => 'foo'], ['id' => 1]);
        $this->exec('data_coherence all --large-table-threshold 2 --model Users');
        $this->assertTextContains(
            __("Une ou plusieurs erreurs ont été détectées"),
            implode('|', $this->_err->messages())
        );
    }

    /**
     * testGetError
     */
    public function testGetError()
    {
        $this->exec('data_coherence get_error Users1');
        $this->assertTextContains(
            __("Vous devez indiquer une référence au format ModelName:id"),
            implode('|', $this->_err->messages())
        );

        $this->exec('data_coherence get_error Users:1');
        $this->assertTextContains(
            __("Aucune erreur détectée"),
            implode('|', $this->_out->messages())
        );

        $Users = TableRegistry::getTableLocator()->get('Users');
        $Users->updateAll(['email' => 'foo'], ['id' => 1]);
        $this->exec('data_coherence get_error Users:1');
        $this->assertTextContains(
            'email',
            implode('|', $this->_err->messages())
        );
    }
}
