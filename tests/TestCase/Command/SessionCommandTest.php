<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Model\Table\SessionsTable;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Session;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\ORM\TableRegistry;
use DateInterval;
use DateTime;

/**
 * Asalae\Command\SessionCommand Test Case
 */
class SessionCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public array $fixtures = [
        'app.Configurations',
        'app.OrgEntities',
        'app.Sessions',
        'app.TypeEntities',
        'app.Users',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * testOrgentities
     */
    public function testOrgentities()
    {
        /** @var SessionsTable $Sessions */
        $Sessions = TableRegistry::getTableLocator()->get('Sessions');
        $Sessions->updateAll(
            [
                'expires' => (new DateTime())->add(new DateInterval('P1Y'))->getTimestamp(),
                'data' => Session::serialize(
                    ['Auth' => ['org_entity' => ['id' => 2, 'archival_agency' => ['id' => 2]]]]
                ),
            ],
            []
        );
        $session = $Sessions->find()->firstOrFail();
        $content = stream_get_contents($session->get('data'));
        if (!$content) {
            $this->assertTrue(true);
            return; // session morte, arrive lors d'un paratest pour une raison inconnu
        }
        $this->exec('session org_entities');
        $session = $Sessions->find()->where(['id' => $session->id])->firstOrFail();
        $this->assertNotEquals($content, stream_get_contents($session->get('data')));
    }

    /**
     * testList
     */
    public function testList()
    {
        /** @var SessionsTable $Sessions */
        $Sessions = TableRegistry::getTableLocator()->get('Sessions');
        $Sessions->updateAll(
            [
                'expires' => (new DateTime())->add(new DateInterval('P1Y'))->getTimestamp(),
                'user_id' => 1,
            ],
            []
        );
        $session = $Sessions->find()->firstOrFail();
        $this->exec('session list');
        $this->assertTextContains(
            $session->id,
            implode('|', $this->_out->messages())
        );
    }
}
