<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Command\EntitiesOverloadCommand;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\ORM\TableRegistry;
use Faker\Factory;

/**
 * Asalae\Command\EntitiesOverloadCommand Test Case
 */
class EntitiesOverloadCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public array $fixtures = [
        'app.OrgEntities',
        'app.Eaccpfs',
        'app.Sessions',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Utility::reset();
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
    }

    /**
     * Test main method
     *
     * @return void
     */
    public function testMain()
    {
        EntitiesOverloadCommand::$Faker = Factory::create('fr_FR');
        EntitiesOverloadCommand::$Faker->seed(12345);

        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $initialCount = $OrgEntities->find()->count();

        $this->exec(sprintf('EntitiesOverload %d', $initialCount + 5));
        $this->assertEquals($initialCount + 5, $OrgEntities->find()->count());
    }
}
