<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Command\Volume\CatCommand;
use Asalae\Controller\AdminsController;
use AsalaeCore\Console\ConsoleIo;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\Arguments;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

use function __;

use const DS;
use const TMP_TESTDIR;
use const TMP_VOL1;

/**
 * Asalae\Command\VolumeCommand Test Case
 */
class VolumeCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public array $fixtures = [
        'app.Volumes',
        'app.SecureDataSpaces',
        'app.StoredFilesVolumes',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')
            ->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);

        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        Filesystem::mkdir(TMP_VOL1);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
    }

    /**
     * testList
     */
    public function testList()
    {
        $this->exec('volume list');
        $this->assertTextContains(
            TableRegistry::getTableLocator()
                ->get('Volumes')
                ->get(1)
                ->get('name'),
            $this->out()
        );
    }

    /**
     * out
     */
    private function out()
    {
        return implode("\n", $this->_out->messages());
    }

    /**
     * testExists
     */
    public function testExists()
    {
        $this->exec('volume exists 1 foo/bar.baz');
        $this->assertEquals(1, $this->_exitCode);

        Filesystem::dumpFile(TMP_VOL1 . DS . 'foo' . DS . 'bar.baz', 'test');
        $this->cleanupConsoleTrait();
        $this->exec('volume exists 1 foo/bar.baz');
        $this->assertEquals(0, $this->_exitCode);
    }

    /**
     * testDownload
     */
    public function testDownload()
    {
        // file not exists
        $tmpfile = TMP_TESTDIR . DS . uniqid('test');
        $e = null;
        try {
            $this->exec('volume download 1 foo/bar.baz ' . $tmpfile);
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);

        // download OK
        Filesystem::dumpFile(TMP_VOL1 . DS . 'foo' . DS . 'bar.baz', 'test');
        $this->cleanupConsoleTrait();
        $this->exec('volume download 1 foo/bar.baz ' . $tmpfile);
        $this->assertEquals(0, $this->_exitCode);
        $this->assertFileExists($tmpfile);

        // existings destination file -overwrite
        $this->cleanupConsoleTrait();
        $this->exec(
            'volume download 1 foo/bar.baz ' . $tmpfile . ' --overwrite'
        );
        $this->assertEquals(0, $this->_exitCode);

        // existings destination file -keep
        $this->cleanupConsoleTrait();
        $this->exec('volume download 1 foo/bar.baz ' . $tmpfile);
        $this->assertEquals(1, $this->_exitCode);
    }

    /**
     * testUpload
     */
    public function testUpload()
    {
        $tmpfile = TMP_TESTDIR . DS . uniqid('test');
        $volFilename = TMP_VOL1 . DS . 'foo' . DS . 'bar.baz';

        // file not exists
        $this->exec('volume upload 1 ' . $tmpfile . ' foo/bar.baz');
        $this->assertEquals(1, $this->_exitCode);

        // upload OK
        Filesystem::dumpFile($tmpfile, 'test');
        $this->cleanupConsoleTrait();
        $this->exec('volume upload 1 ' . $tmpfile . ' foo/bar.baz');
        $this->assertEquals(0, $this->_exitCode);
        $this->assertFileExists($volFilename);
    }

    /**
     * testRename
     */
    public function testRename()
    {
        $volFilename1 = TMP_VOL1 . DS . 'foo.bar';
        $volFilename2 = TMP_VOL1 . DS . 'foo' . DS . 'bar.baz';

        // file not exists
        $e = null;
        try {
            $this->exec('volume rename 1 foo.bar foo/bar.baz');
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);

        // upload OK
        Filesystem::dumpFile($volFilename1, 'test');
        $this->cleanupConsoleTrait();
        $this->exec('volume rename 1 foo.bar foo/bar.baz');
        $this->assertEquals(0, $this->_exitCode);
        $this->assertFileExists($volFilename2);
    }

    /**
     * testMissings
     */
    public function testMissings()
    {
        $this->exec('volume missing 1');
        $this->assertTextContains(
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            $this->err()
        );

        $Volumes = TableRegistry::getTableLocator()
            ->get('Volumes');
        $Volumes->updateAll(['active' => false], ['id !=' => 1]);
        Filesystem::dumpFile(
            TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            'test'
        );
        $this->cleanupConsoleTrait();
        $this->exec('volume missing');
        $this->assertTextNotContains(
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            $this->err()
        );
    }

    /**
     * err
     */
    private function err()
    {
        return implode("\n", $this->_err->messages());
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $file = TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml';
        Filesystem::dumpFile($file, 'test');

        // file exists in db
        $this->exec(
            'volume delete 1 sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
        );
        $this->assertTextContains(
            __(
                "Une référence du fichier existe encore en base (utiliser --force pour passer outre)"
            ),
            $this->err()
        );
        $this->assertFileExists($file);

        // file exists in db with --force
        $this->cleanupConsoleTrait();
        $this->exec(
            'volume delete 1 sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml --force'
        );
        $this->assertFileDoesNotExist($file);
    }

    /**
     * testCat
     */
    public function testCat()
    {
        $volFilename = TMP_VOL1 . DS . 'foo' . DS . 'bar.baz';
        Filesystem::dumpFile($volFilename, 'test-contain45613789');
        $shell = new CatCommand();
        ob_start();
        $args = new Arguments(['1', 'foo/bar.baz'], [], ['volume_id', 'storage_path']);
        $io = new ConsoleIo();
        $shell->execute($args, $io);
        $content = ob_get_clean();
        $this->assertEquals('test-contain45613789', $content);
    }

    /**
     * testHash
     */
    public function testHash()
    {
        $volFilename = TMP_VOL1 . DS . 'foo' . DS . 'bar.baz';
        Filesystem::dumpFile($volFilename, 'test');
        $this->exec('volume hash 1 foo/bar.baz');
        $this->assertEquals(
            hash('sha256', 'test'),
            $this->out()
        );
        $this->cleanupConsoleTrait();
        $this->exec('volume hash 1 foo/bar.baz --base64');
        $this->assertEquals(
            base64_encode(hex2bin(hash('sha256', 'test'))),
            $this->out()
        );
    }

    /**
     * testLs
     */
    public function testLs()
    {
        $volFilename = TMP_VOL1 . DS . 'foo' . DS . 'bar.baz';
        Filesystem::dumpFile($volFilename, 'test');

        // ls racine
        $this->exec('volume ls 1');
        $this->assertTextContains('foo', $this->out());

        // ls dossier
        $this->cleanupConsoleTrait();
        $this->exec('volume ls 1 foo');
        $this->assertTextContains('bar.baz', $this->out());

        // ls fichier
        $this->cleanupConsoleTrait();
        $this->exec('volume ls 1 foo/bar.baz');
        $this->assertTextContains('bar.baz', $this->out());
    }

    /**
     * testField
     */
    public function testField()
    {
        $this->exec('volume field 1 disk_usage');
        $this->assertTextContains(
            TableRegistry::getTableLocator()
                ->get('Volumes')
                ->get(1)
                ->get('disk_usage'),
            $this->out()
        );
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $key = hash('sha256', AdminsController::DECRYPT_KEY);
        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        $volume = $Volumes->newEntity(
            [
                'name' => 'test',
                'fields' => json_encode(
                    [
                        'endpoint' => 'http://test',
                        'region' => 'EU',
                        'bucket' => 'testunit',
                        'credentials_key' => 'test',
                        'credentials_secret' => base64_encode(
                            Security::encrypt(hash('sha256', 'test'), $key)
                        ),
                        'verify' => '',
                        'use_proxy' => '1',
                    ],
                    JSON_UNESCAPED_SLASHES
                ),
                'driver' => 'MINIO',
                'active' => true,
            ]
        );
        $Volumes->saveOrFail($volume);
        $inputs = [
            'endpoint' => 'http://test/is_modified',
            'region' => '', // pas de modification
            'bucket' => 'testunit2',
            'credentials_key' => 'myusername',
            'credentials_secret' => 'mypassword',
            'verify' => '',
            'use_proxy' => '0',
        ];
        $expected = $inputs;
        $expected['region'] = 'EU';

        // test interactif
        $this->exec('volume edit ' . $volume->id, array_values($inputs));
        $json = json_encode($expected, JSON_UNESCAPED_SLASHES);
        $this->assertOutputContains($json);
        $volume = $Volumes->get($volume->id);
        $this->assertStringContainsString('is_modified', $volume->get('fields'));

        // test headless
        $newJson = str_replace('is_modified', 'modified2', $json);
        $this->exec(
            sprintf('volume edit %d --data %s', $volume->id, escapeshellarg($newJson))
        );
        $this->assertOutputContains($newJson);
        $volume = $Volumes->get($volume->id);
        $this->assertStringContainsString('modified2', $volume->get('fields'));

        // invalide
        $this->exec(
            sprintf('volume edit %d --data invalid-data', $volume->id)
        );
        $this->assertExitCode(1);
    }
}
