<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Test\Mock\ConnectionManager;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\ConnectionManager as CakeConnectionManager;
use ErrorException;
use Libriciel\Filesystem\Utility\Filesystem;

class InstallConfigCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
        'Acos',
        'Agreements',
        'Aros',
        'ArosAcos',
        'Configurations',
        'Counters',
        'Crons',
        'Eaccpfs',
        'Keywords',
        'Profiles',
        'Roles',
        'RolesTypeEntities',
        'ServiceLevels',
        'SuperArchivistsArchivalAgencies',
        'Timestampers',
        'Users',
        'ValidationActors',
        'ValidationStages',
        'Volumes',
    ];
    private string $originalPathToLocal;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->originalPathToLocal = Configure::read('App.paths.path_to_local_config');
        $dir = TMP_TESTDIR . DS . 'testinstallconfig';
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('App.paths.path_to_local_config', $dir . DS . 'path_to_local.php');
        Filesystem::reset();
        Filesystem::dumpFile($dir . DS . 'path_to_local.php', "<?php return '$dir/app_local.json';?>");
        Filesystem::dumpFile($dir . DS . 'app_local.json', "{}");
        Utility::set(CakeConnectionManager::class, new ConnectionManager());
        $conn = $this->createMock(Connection::class);
        $conn->method('queryFactory')->willThrowException(new MissingConnectionException());
        ConnectionManager::$outputs = [
            'drop' => true,
            'setConfig' => true,
            'get' => $conn,
        ];
        $exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(['command', 'rawCommand', 'async'])
            ->getMock();
        $exec->method('async');
        $exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => 'OK', 'stderr' => ''])
        );
        $exec->method('rawCommand')->willReturnCallback(
            function ($command, array &$output = null, &$return_var = null) {
                $output = [];
                $return_var = 0;
                return '';
            }
        );
        Utility::set('Exec', $exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Configure::write('App.paths.path_to_local_config', $this->originalPathToLocal);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Utility::reset();
    }

    /**
     * testExecute
     */
    public function testExecute()
    {
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
            }
        );
        $dir = TMP_TESTDIR . DS . 'testinstallconfig';
        $ini = file_get_contents(CONFIG . 'install_default.ini');
        $ini = preg_replace('/(debug = ).*/', '$1true', $ini);
        $ini = preg_replace('/(config_file = ).*/', '$1' . $dir . DS . 'app_local.json', $ini);
        $ini = preg_replace('/(data_dir = ).*/', "$1$dir", $ini);
        $ini = preg_replace('/(admin_file = ).*/', '$1' . $dir . DS . 'administrators.json', $ini);
        $ini = preg_replace('/(directory = ).*/', '$1' . $dir . DS . 'timestamp', $ini);
        $iniFile = $dir . DS . 'install.ini';
        file_put_contents($dir . DS . 'install.ini', $ini);

        $this->exec("install config --force $iniFile");
        $this->assertOutputContains(__("Installation terminée"));
    }

    /**
     * testPostInstall
     */
    public function testPostInstall()
    {
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
            }
        );
        $dir = TMP_TESTDIR . DS . 'testinstallconfig';
        $ini = <<<EOT
[volumes]
vol1[name] = volume-post-install-01
vol1[description] =test post install
vol1[alert_rate] = 95
vol1[max_disk_usage] = 10 000 000 000
vol1[driver] = FILESYSTEM
vol1[path] = /data/test

[secure_data_spaces]
ecs1[name] = ECS-POST-INSTALL-001
ecs1[description] =test post install
ecs1[volumes] = vol1
EOT;
        $iniFile = $dir . DS . 'install.ini';
        file_put_contents($dir . DS . 'install.ini', $ini);
        $volumesCount = $this->fetchTable('Volumes')->find()->count();
        $secureDataSpacesCount = $this->fetchTable('SecureDataSpaces')->find()->count();

        $this->exec("install config --postinstall $iniFile");

        $this->assertCount($volumesCount + 1, $this->fetchTable('Volumes')->find());
        $this->assertCount($secureDataSpacesCount + 1, $this->fetchTable('SecureDataSpaces')->find());
    }
}
