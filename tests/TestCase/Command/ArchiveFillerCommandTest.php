<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\ORM\TableRegistry;

/**
 * Asalae\Command\ArchiveFillerCommand Test Case
 */
class ArchiveFillerCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
        'ArchiveBinaries',
        'ArchiveBinariesArchiveUnits',
        'ArchiveDescriptions',
        'ArchiveFiles',
        'ArchiveIndicators',
        'ArchiveUnits',
        'Archives',
        'ArchivesTransfers',
        'Counters',
        'EventLogs',
        'MessageIndicators',
        'SecureDataSpaces',
        'StoredFilesVolumes',
        'TransferAttachments',
        'Users',
        'Volumes',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Utility::reset();
    }

    /**
     * testMain
     */
    public function testMain()
    {
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $count = $Archives->find()->count();
        $this->exec('archive filler 1 10 --datasource test');
        $this->assertCount($count + 10, $Archives->find());
    }
}
