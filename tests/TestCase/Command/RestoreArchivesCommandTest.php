<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Command\RestoreArchivesCommand;
use Asalae\Test\Mock\CheckMock;
use Asalae\TestSuite\VolumeSample;
use Asalae\Utility\Check as CheckUtility;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Config;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(RestoreArchivesCommand::class)]
class RestoreArchivesCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public array $fixtures = [
        'app.Agreements',
        'app.OrgEntities',
        'app.Profiles',
        'app.ServiceLevels',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }

        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('App.paths.data', TMP_TESTDIR);

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);

        Utility::set(CheckUtility::class, CheckMock::class);
        CheckMock::$outputs = [
            'missings' => [],
            'missingsExtPhp' => [],
            'database' => true,
            'beanstalkd' => true,
            'workers' => [],
            'ratchet' => true,
            'clamav' => true,
            'phpOk' => true,
        ];

        $conf = Config::readLocal();
        Config::reset();
        $conf['App']['ignore_invalid_fullbaseurl'] = true;
        Filesystem::dumpFile(TMP_TESTDIR . "test_local.json", json_encode($conf));
        Filesystem::dumpFile(TMP_TESTDIR . 'path_to_local.php', '<?php return TMP_TESTDIR."test_local.json";?>');
        Configure::write('App.paths.path_to_local_config', TMP_TESTDIR . 'path_to_local.php');
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Utility::reset();
    }

    /**
     * testExecute
     */
    public function testExecute()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        $this->exec(
            sprintf(
                "restore archives %s %s",
                escapeshellarg($basedir . 'volume01/'),
                'admin'
            )
        );
        $this->assertOutputContains(__("aucun répertoire d'archives à traiter"));
    }
}
