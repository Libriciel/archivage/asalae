<?php

namespace Asalae\Test\TestCase\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use AsalaeCore\Utility\Notify;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/**
 * Asalae\Command\JobCommand Test Case
 */
class PdfFilesCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.ArchiveFiles',
        'app.Archives',
        'app.Configurations',
        'app.Mails',
        'app.Notifications',
        'app.OrgEntities',
        'app.Users',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        VolumeSample::insertSample(
            'sample_seda10_description.xml',
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
        );
        Notify::$instance = $this->createMock(Notify::class);

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
        VolumeSample::destroy();
    }

    /**
     * testMain
     */
    public function testMain()
    {
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $entity = $Archives->find()
            ->where(['Archives.id' => 1])
            ->contain(['DescriptionXmlArchiveFiles' => ['StoredFiles']])
            ->firstOrFail();
        $command = sprintf(
            '%s --force %s %s %s %s %s %s',
            'pdf_files',
            escapeshellarg(Hash::get($entity, 'description_xml_archive_file.stored_file.name')),
            escapeshellarg($entity->get('pdf')),
            escapeshellarg(1),
            escapeshellarg('/archives/download-pdf/' . $entity->id),
            escapeshellarg($entity->get('secure_data_space_id')),
            escapeshellarg($entity->get('pdf'))
        );
        $this->exec($command);
        $content = VolumeSample::volumeGetContent($entity->get('pdf'));
        $this->assertNotEmpty($content);
    }
}
