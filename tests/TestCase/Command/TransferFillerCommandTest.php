<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Command\TransferFillerCommand;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Datacompressor\Utility\DataCompressor;

/**
 * Asalae\Command\TransferFillerCommand Test Case
 */
class TransferFillerCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;
    use InvokePrivateTrait;

    public array $fixtures = [
        'app.Agreements',
        'app.Counters',
        'app.OrgEntities',
        'app.Profiles',
        'app.Sequences',
        'app.ServiceLevels',
        'app.TransferAttachments',
        'app.Transfers',
        'app.Users',
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_VOLUMES,
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
        $exec = $this->createMock(Exec::class);

        if (!is_dir(TMP_TESTDIR)) {
            mkdir(TMP_TESTDIR, 0777, true);
        }
        TransferFillerCommand::$defaultTmpDir = TMP_TESTDIR;
        $result1 = new CommandResult(
            [
                'stdout' => json_encode(
                    [
                        'files' => [
                            0 => [
                                'filename' => TMP_TESTDIR . '/attachments/test.txt',
                                'matches' => [
                                    0 => [
                                        'id' => 'x-fmt/111',
                                        'mime' => 'plain/text',
                                        'format' => 'Plain Text File',
                                        'extension' => 'txt',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    JSON_UNESCAPED_SLASHES
                ),
                'stderr' => '',
                'code' => 0,
                'success' => true,
            ]
        );
        $exec->method('command')->willReturn($result1);
        Utility::set('Exec', $exec);
    }

    /**
     * testdestruct
     */
    public function testdestruct()
    {
        $command = new TransferFillerCommand();
        $tmpDir = $this->invokeProperty($command, 'tmpDir');
        $this->assertDirectoryExists($tmpDir);
        $command->__destruct();
        $this->assertDirectoryDoesNotExist($tmpDir);
    }

    /**
     * testMain
     */
    public function testMain()
    {
        file_put_contents($file = TMP_TESTDIR . DS . 'test.txt', 'test');
        DataCompressor::compress($file, $file . '.zip');
        $options = [
            '--attachment ' . $file . '.zip',
            '--agreement_id 1',
            '--profile_id 1',
            '--service_level_id 1',
            '--originating_agency_id 1',
        ];
        $this->exec(
            sprintf(
                'transfer filler 1 1 %s',
                implode(' ', $options)
            )
        );
        $this->assertOutputContains(
            __("Création de {0} transferts...", 1)
        );
    }
}
