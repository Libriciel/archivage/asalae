<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Model\Volume\VolumeManager;
use Asalae\Test\Mock\FakeVolume;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Driver\Volume\VolumeInterface;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Libriciel\Filesystem\Utility\Filesystem;

use function __;

use const TMP_VOL1;
use const TMP_VOL2;

/**
 * Asalae\Command\VolumeManager\VolumeManagerCommand Test Case
 */
class VolumeManagerCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveFiles',
        'app.ArchiveUnits',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.OrgEntities',
        'app.Transfers',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        TableRegistry::getTableLocator()->clear();
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            chmod(TMP_VOL2, 0777);
            Filesystem::remove(TMP_VOL2);
        }
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            chmod(TMP_VOL2, 0777);
            Filesystem::remove(TMP_VOL2);
        }
        Filesystem::setNamespace();
        Filesystem::reset();
        Utility::reset();
    }

    /**
     * testList
     */
    public function testList()
    {
        $SecureDataSpaces = TableRegistry::getTableLocator()->get('SecureDataSpaces');
        $this->exec('volume_manager list');
        $this->assertOutputContains($SecureDataSpaces->get(1)->get('name'));
    }

    /**
     * testRepair
     */
    public function testRepair()
    {
        /**
         * Préparation du test (insertion de foo.txt dans volume 1 et de bar.txt dans volume 2)
         */
        $loc = TableRegistry::getTableLocator();
        $Volumes = $loc->get('Volumes');
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');

        /** @var VolumeFilesystem $volume1 */
        $volume1 = VolumeManager::getDriver($Volumes->get(1));
        /** @var VolumeFilesystem $volume2 */
        $volume2 = VolumeManager::getDriver($Volumes->get(2));
        $this->prepareTest($volume1, $volume2);

        $this->assertTrue($volume1->fileExists('foo.txt'));
        $this->assertFalse($volume2->fileExists('foo.txt'));
        $this->assertFalse($volume1->fileExists('bar.txt'));
        $this->assertTrue($volume2->fileExists('bar.txt'));

        /**
         * Début du test
         */
        $this->exec('volume_manager repair 1');
        $this->assertTrue($volume1->fileExists('foo.txt'));
        $this->assertTrue($volume2->fileExists('foo.txt'));
        $this->assertTrue($volume1->fileExists('bar.txt'));
        $this->assertTrue($volume2->fileExists('bar.txt'));

        // corruption d'un fichier
        $expected = '2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae';
        $this->assertEquals($expected, $volume2->hash('foo.txt'));
        $volume2->fileDelete('foo.txt');
        $volume2->filePutContent('foo.txt', 'corruption');
        $this->assertNotEquals($expected, $volume2->hash('foo.txt'));
        $this->cleanupConsoleTrait();
        $this->exec('volume_manager repair 1 --integrity-check');
        $this->assertEquals($expected, $volume2->hash('foo.txt'));

        // test réparation fichier avec référence
        $volume1->fileDelete('foo.txt');
        $ref = $StoredFilesVolumes->exists(['volume_id' => 1, 'storage_ref' => 'foo.txt']);
        $this->assertTrue($ref);
        $this->assertFalse($volume1->fileExists('foo.txt'));
        $this->exec('volume_manager repair 1');
        $this->assertTrue($volume1->fileExists('foo.txt'));

        // test réparation référence avec fichier présent
        $StoredFilesVolumes->deleteAll(['volume_id' => 1, 'storage_ref' => 'foo.txt']);
        $ref = $StoredFilesVolumes->exists(['volume_id' => 1, 'storage_ref' => 'foo.txt']);
        $this->assertFalse($ref);
        $this->exec('volume_manager repair 1');
        $ref = $StoredFilesVolumes->exists(['volume_id' => 1, 'storage_ref' => 'foo.txt']);
        $this->assertTrue($ref);

        // test sans aucunes references
        $StoredFilesVolumes->deleteAll(['volume_id IN' => [1, 2], 'storage_ref' => 'foo.txt']);
        $this->_err = new ConsoleOutput();
        $this->exec('volume_manager repair 1');
        $this->assertErrorContains('foo.txt');
    }

    /**
     * prepareTest
     * @param VolumeInterface $volume1
     * @param VolumeInterface $volume2
     * @throws VolumeException
     */
    private function prepareTest(VolumeInterface $volume1, VolumeInterface $volume2)
    {
        $loc = TableRegistry::getTableLocator();
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $StoredFilesVolumes->deleteAll([]);
        $StoredFiles = $loc->get('StoredFiles');

        // fichier foo.txt dans volume 1
        $volume1->filePutContent('foo.txt', 'foo');
        $storedFile = $StoredFiles->newEntity(
            [
                'secure_data_space_id' => 1,
                'name' => 'foo.txt',
                'size' => 3,
                'hash' => '2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae',
                'hash_algo' => 'sha256',
            ]
        );
        $this->assertNotFalse((bool)$StoredFiles->save($storedFile));
        $link = $StoredFilesVolumes->newEntity(
            [
                'storage_ref' => 'foo.txt',
                'volume_id' => 1,
                'stored_file_id' => $storedFile->id,
            ]
        );
        $this->assertNotFalse((bool)$StoredFilesVolumes->save($link));

        // fichier bar.txt dans volume 2
        $volume2->filePutContent('bar.txt', 'bar');
        $storedFile = $StoredFiles->newEntity(
            [
                'secure_data_space_id' => 1,
                'name' => 'bar.txt',
                'size' => 3,
                'hash' => 'fcde2b2edba56bf408601fb721fe9b5c338d10ee429ea04fae5511b68fbf8fb9',
                'hash_algo' => 'sha256',
            ]
        );
        $this->assertNotFalse((bool)$StoredFiles->save($storedFile));
        $link = $StoredFilesVolumes->newEntity(
            [
                'storage_ref' => 'bar.txt',
                'volume_id' => 2,
                'stored_file_id' => $storedFile->id,
            ]
        );
        $this->assertNotFalse((bool)$StoredFilesVolumes->save($link));
    }

    /**
     * testTruncateVolume
     */
    public function testTruncateVolume()
    {
        $loc = TableRegistry::getTableLocator();
        $Volumes = $loc->get('Volumes');

        /** @var VolumeFilesystem $volume1 */
        $volume1 = VolumeManager::getDriver($Volumes->get(1));
        /** @var VolumeFilesystem $volume2 */
        $volume2 = VolumeManager::getDriver($Volumes->get(2));
        $this->prepareTest($volume1, $volume2);

        $this->assertTrue($volume1->fileExists('foo.txt'));
        $this->assertTrue($volume2->fileExists('bar.txt'));

        $this->exec('volume_manager truncate_volume 2');
        $this->assertTrue($volume1->fileExists('foo.txt'));
        $this->assertFalse($volume2->fileExists('bar.txt'));

        // test volume down
        Configure::write(VolumeManager::DRIVERS_CONFIG_PATH . '.FILESYSTEM.class', FakeVolume::class);
        $this->cleanupConsoleTrait();
        $this->exec('volume_manager truncate_volume 2 -f');
        $this->assertErrorContains(__("Impossible de se connecter au volume"));
        $this->cleanupConsoleTrait();
        $this->exec('volume_manager truncate_volume 2', ['y']);
        $this->assertErrorContains(__("Impossible de se connecter au volume"));
        chmod(TMP_VOL2, 0777);
    }

    /**
     * testinfo
     */
    public function testinfo()
    {
        $loc = TableRegistry::getTableLocator();
        $StoredFiles = $loc->get('StoredFiles');
        $Volumes = $loc->get('Volumes');
        $storedFileName = $StoredFiles->get(6)->get('name');
        $vol2 = $Volumes->get(2);
        $vol2->set('driver', 'MINIO');
        $vol2->set('endpoint', 'https://endpoint.test.localhost');
        $Volumes->saveOrFail($vol2);

        $this->cleanupConsoleTrait();
        $this->exec('volume_manager info ' . $storedFileName);
        $this->assertOutputContains('ARCHIVE_BINARY');
        $this->assertOutputContains('https://endpoint.test.localhost');

        $storedFileName = $StoredFiles->get(7)->get('name');
        $this->cleanupConsoleTrait();
        $this->exec('volume_manager info ' . $storedFileName);
        $this->assertOutputContains('ARCHIVE_FILE');
    }
}
