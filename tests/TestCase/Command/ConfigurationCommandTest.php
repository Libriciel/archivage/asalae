<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Command\Configuration\AbstractConfigurationCommand;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Config;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;
use Cake\Database\Driver\Sqlite;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const TMP_TESTDIR;

/**
 * Asalae\Command\ConfigurationCommand Test Case
 */
class ConfigurationCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use SaveBufferTrait;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        Filesystem::mkdir($dir);
        Configure::write('App.paths.path_to_local_config', $dir . DS . 'path_to_local.php');
        Configure::write('App.paths.local_config', $dir . DS . 'app_local.json');
        Config::reset();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        Filesystem::setNamespace();
        Filesystem::reset();
        Utility::reset();
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testMain
     */
    public function testMain()
    {
        $user = posix_getpwuid(posix_geteuid());
        AbstractConfigurationCommand::$datasources = ['testunit'];
        $exec = $this->createMock(Exec::class);
        $exec->method('command')->willReturn(
            new CommandResult(
                [
                    'success' => true,
                    'code' => 0,
                    'stdout' => 'OK',
                    'stderr' => '',
                ]
            )
        );
        $exec->method('rawCommand')->willReturn('OK');
        Utility::set('Exec', $exec);
        $pathToLocal = TMP_TESTDIR . DS . 'local.json';

        $this->preserveBuffer(
            [$this, 'exec'],
            sprintf('configuration wizard --user %s', escapeshellarg($user['name'])),
            [
                'fr_FR', // lang
                $pathToLocal, // App.paths.local_config
                TMP_TESTDIR, // App.paths.data
                'localhost', // App.fullBaseUrl
                'y', // force url
                'y', // set datasource
                Sqlite::class, // driver
                'localhost', // host
                $user['name'], // username
                // password = OK -> $exec->method('command')->willReturn(['stdout' => 'OK'])
                TMP_TESTDIR . DS . 'testunit.sqlite',
                // database
                'g', // generate security key
                'wss://localhost/wss', // ratchet
            ]
        );
        $this->assertFileExists($pathToLocal);
        $this->assertStringContainsString(
            json_encode(TMP_TESTDIR . DS . 'testunit.sqlite', JSON_UNESCAPED_SLASHES),
            file_get_contents($pathToLocal)
        );
    }

    /**
     * testEditor
     */
    public function testEditor()
    {
        $user = posix_getpwuid(posix_geteuid());
        $pathToLocal = $this->dumpConfigJson(
            [
                'App' => [
                    'foo' => 'bar',
                ],
            ]
        );
        // setted App (ex: App.fullBaseUrl)
        $this->preserveBuffer(
            [$this, 'exec'],
            sprintf('configuration editor --user %s', escapeshellarg($user['name'])),
            [
                'App', // Hash path
                'y', // delete (array)
                'y', // continue

                'Foo.boolean', // Hash path
                'true', // value
                'boolean', // type
                'y', // continue

                'Foo.null', // Hash path
                'null', // value
                'null', // type
                'n', // delete ('' || null)
                'y', // continue

                'Foo.float', // Hash path
                '3.14159265359', // value
                'float', // type
                'y', // continue

                'Foo.integer', // Hash path
                '256', // value
                'integer', // type
                'y', // continue

                'Foo.string',
                'bar', // value
                'y', // continue

                'Foo.delete', // Hash path
                'bar', // value
                'y', // continue

                'Foo.delete', // Hash path
                '', // value
                'y', // delete

                'n', // continue
                'y', // replace file
            ]
        );
        $data = json_decode(file_get_contents($pathToLocal), true);
        $this->assertNull(Hash::get($data, 'App'));
        $expected = [
            'boolean' => true,
            'null' => null,
            'float' => 3.14159265359,
            'integer' => 256,
            'string' => 'bar',
        ];
        $this->assertEquals($expected, Hash::get($data, 'Foo'));
    }

    /**
     * dumpConfigJson
     * @param array $config
     * @return string
     * @throws Exception
     */
    private function dumpConfigJson($config = []): string
    {
        if (!$config) {
            $config = [
                'Security' => [
                    'salt' => 'testunit',
                ],
            ];
        }
        Filesystem::dumpFile(
            $local = Configure::read('App.paths.local_config'),
            json_encode($config, JSON_UNESCAPED_SLASHES)
        );
        Filesystem::dumpFile(
            Configure::read('App.paths.path_to_local_config'),
            "<?php return '$local'; ?>"
        );
        return $local;
    }

    /**
     * testGet
     */
    public function testGet()
    {
        $this->dumpConfigJson();
        $this->preserveBuffer(
            [
                $this,
                'exec',
            ],
            'configuration get Security.salt'
        );
        $this->assertOutputContains('testunit');
    }

    /**
     * testSet
     */
    public function testSet()
    {
        $pathToLocal = $this->dumpConfigJson();
        $user = posix_getpwuid(posix_geteuid());
        $this->preserveBuffer(
            [
                $this,
                'exec',
            ],
            sprintf('configuration set Security.salt foo --user %s', escapeshellarg($user['name']))
        );
        $this->assertEquals(
            'foo',
            Hash::get(json_decode(file_get_contents($pathToLocal), true), 'Security.salt')
        );
    }

    /**
     * testAll
     */
    public function testAll()
    {
        $this->dumpConfigJson();
        $this->preserveBuffer([$this, 'exec'], 'configuration all');
        $this->assertOutputContains('foo');
    }
}
