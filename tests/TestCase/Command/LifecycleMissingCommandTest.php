<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;

/**
 * Asalae\Command\Lifecycle\LifecycleMissingCommand Test Case
 */
class LifecycleMissingCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;
    use InvokePrivateTrait;

    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        FIXTURES_EVENT_LOGS,
        'app.EventLogs',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        Configure::write('Archives.updateLifecycle.path', TMP_TESTDIR);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testwork
     */
    public function testwork()
    {
        VolumeSample::fillVolumes();
        $this->exec('lifecycle missing');
        $this->assertOutputContains(
            __(
                "Vérification de {0} cycles de vie",
                $this->fetchTable('ArchiveFiles')->find()
                    ->where(['type' => 'lifecycle'])
                    ->count()
            )
        );
        $this->assertOutputContains(__("Tout les fichiers ont été vérifiés avec succès"));
        $this->_out = $this->_err = null;

        // test fail avec réparation
        $lifecycle1 = TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml';
        unlink($lifecycle1);
        $this->exec('lifecycle missing');
        $this->assertOutputContains(
            __(
                "Le fichier ''{0}'' a été restauré"
                . " à partir d'un autre volume de l'ECS sur le volume {1}",
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml',
                1
            )
        );
        $this->_out = $this->_err = null;

        // test fail avec récupération
        $lifecycle2 = TMP_VOL2 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml';
        rename($lifecycle1, $lifecycle1 . '.123456789.upload');
        unlink($lifecycle2);
        $this->exec('lifecycle missing');
        $this->assertOutputContains(
            __(
                "Le fichier ''{0}'' a été restauré"
                . " à partir du fichier ''{1}'' sur le volume {2}",
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml',
                'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml.123456789.upload',
                1
            )
        );
        $this->_out = $this->_err = null;

        // test fail
        unlink($lifecycle1);
        unlink($lifecycle2);
        $this->exec('lifecycle missing');
        $this->assertOutputContains(
            __(
                "Si le fichier n'est pas restauré à partir "
                . "d'un autre volume, vous pouvez tenter un repair "
                . "avec la commande suivante : {0}",
                "bin/cake lifecycle repair 'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'"
            )
        );
        $this->_out = $this->_err = null;
    }
}
