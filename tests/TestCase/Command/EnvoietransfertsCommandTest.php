<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Command\EnvoietransfertsCommand;
use Asalae\Test\Mock\ClientMock;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;
use Cake\Http\Client\Response;
use Cake\Utility\Hash;
use DateInterval;
use DateTime;
use Faker\Generator;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Stream;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;

use const DS;
use const TMP_TESTDIR;

/**
 * Asalae\Command\EnvoietransfertsCommand Test Case
 */
class EnvoietransfertsCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use InvokePrivateTrait;

    public array $fixtures = [
        'app.Agreements',
        'app.ArchivingSystems',
        'app.AuthUrls',
        'app.OrgEntities',
        'app.OutgoingTransferRequests',
        'app.OutgoingTransfers',
        'app.Profiles',
        'app.TypeEntities',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testMain
     */
    public function testMain()
    {
        if (is_file($file = TMP_TESTDIR . DS . 'fichier_1.txt')) {
            unlink($file);
        }

        $mock = $this->getMockBuilder(Generator::class)->getMock();
        $mock->method('__call')->willReturnCallback(
            function ($v, $args) {
                switch ($v) {
                    case 'numberBetween':
                        return current($args);
                    case 'paragraph':
                        return 'Lorem ipsum';
                    case 'randomElement':
                        return current(current($args));
                    case 'dateTimeThisDecade':
                        return (new DateTime())->sub(new DateInterval('P5Y6M5DT3H'));
                }
                trigger_error('undefined ' . $v);
                return '';
            }
        );
        $i = 1;
        $mock->method('__get')->willReturnCallback(
            function ($v) use (&$i) {
                switch ($v) {
                    case 'uuid':
                        return $i++;
                    case 'number':
                        return 1;
                }
                trigger_error('undefined ' . $v);
                return '';
            }
        );
        EnvoietransfertsCommand::$Faker = $mock;

        $exec = $this->createMock(Exec::class);
        $exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => 'OK', 'stderr' => ''])
        );
        $exec->method('rawCommand')->willReturn('OK');
        Utility::set('Exec', $exec);

        $client = $this->createMock(GuzzleClient::class);

        $requestData = [];
        $client->method('request')->willReturnCallback(
            function ($method, $url, $data) use (&$requestData) {
                $test = $this;
                if ($method === 'GET') {
                    $stream = fopen('php://temp', 'r+');
                    fwrite($stream, '{"version": "2.0.0"}');
                    fseek($stream, 0);
                    $responseVersion = $test->createMock(Response::class);
                    $responseVersion->method('getBody')->willReturn(new Stream($stream));
                    return $responseVersion;
                }
                $requestData[] = $data;

                $stream = fopen('php://temp', 'r+');
                fwrite($stream, '{"success": true}');
                fseek($stream, 0);
                $responseSuccess = $test->createMock(Response::class);
                $responseSuccess->method('getBody')->willReturn(new Stream($stream));
                return $responseSuccess;
            }
        );
        Utility::set(GuzzleClient::class, $client);

        $this->exec(
            'Envoietransferts',
            $data = [
                'ACCORD001', // accordversement
                'txt,odt,pdf,png,jpg', // extpj
                '3', // nbpjs
                '3', // nbpjsuniques
                '1', // nbtransferts
                '', // profilarchive
                'FRAD000', // servicearchives
                'FRPROD001,FRPROD002,FRPROD003', // serviceproducteur
                'FRVERS001', // serviceversant
                'detruire', //sortfinal
                '5', // taillepjmax
                '1', // taillepjmin
                'Ko', // taillepjunite
                'http://localhost', // url
                '1.0',  //versionseda
                'admin', // wslogin
                'admin', // wspasswd
            ]
        );
        $filename = Hash::get($requestData, '0.multipart.0.filename');
        $this->assertEquals(1, preg_match('/atr_alea_\w+\.xml/', $filename));
        $domutil = DOMUtility::loadXML(Hash::get($requestData, '0.multipart.0.contents'));
        $this->assertEquals(
            substr($filename, 0, -4),
            $domutil->xpath->query('//ns:TransferIdentifier')->item(0)->nodeValue
        );

        /** @var Client\Request|MockObject $request */
        $request = $this->createMock(Client\Request::class);
        foreach ([401, 403, 406, Command::CODE_ERROR] as $code) {
            /** @var Response|MockObject $response */
            $response = $this->createMock(Response::class);
            $response->method('getStatusCode')->willReturn($code);
            $e = new ClientException('test', $request, $response);
            $client->method('request')->willThrowException($e);
            Utility::set(GuzzleClient::class, $client);
            $this->cleanupConsoleTrait();
            $this->exec('Envoietransferts', $data);
            // limite du code a 255 -> 401 transformé en 41
            $this->assertExitCode($code > 400 ? ($code - 400 + 40) : $code);
        }
    }

    /**
     * testGenerateFile
     * NOTE: conversion pose problème dans la CI
     */
    public function testGenerateFile()
    {
        EnvoietransfertsCommand::$Faker = true;
        $shell = new EnvoietransfertsCommand();

        $i = 1;
        $mock = $this->getMockBuilder(Generator::class)->getMock();
        $mock->method('__call')->willReturnCallback(
            function ($v, $args) use (&$ext) {
                switch ($v) {
                    case 'numberBetween':
                        return current($args);
                    case 'paragraph':
                        return 'Lorem ipsum';
                    case 'randomElement':
                        return $ext;
                }
                trigger_error('undefined ' . $v);
                return '';
            }
        );
        $mock->method('__get')->willReturnCallback(
            function ($v) use (&$i) {
                if ($v == 'uuid') {
                    return $i++;
                }
                trigger_error('undefined ' . $v);
                return '';
            }
        );
        EnvoietransfertsCommand::$Faker = $mock;
        $ext = 'txt';
        $Arguments = new Arguments([], ['taillepjmin' => '1'], []);
        $this->invokeProperty($shell, 'args', 'set', $Arguments);
        $file = $this->invokeMethod($shell, 'generateFile');
        $this->assertFileExists($file, sprintf('unable to create %s file', $ext));
        unlink($file);
    }

    /**
     * testPostOutgoingTransfer
     */
    public function testPostOutgoingTransfer()
    {
        Configure::write('App.paths.data', TMP_TESTDIR);
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'outgoing-transfers' . DS . 'sa_OAT_1.zip', 'test');
        $response = $this->createMock(Response::class);
        $response->method('getJson')->willReturn(
            [
                'success' => true,
                'id' => 123456,
                'uploaded_response' => 'done',
            ]
        );
        $response->method('getStatusCode')->willReturn(200);
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['post'])
            ->getMock();
        $client->method('post')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());

        $this->exec('envoietransferts outgoing_transfer 1');
        $this->assertOutputContains('123456');
        $this->assertOutputContains('done');
        $this->assertOutputNotContains('failed');
    }
}
