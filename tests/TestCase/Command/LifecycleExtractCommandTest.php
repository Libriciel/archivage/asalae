<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Asalae\Command\Lifecycle\LifecycleExtractCommand Test Case
 */
class LifecycleExtractCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.Archives',
        'app.ArchiveFiles',
        'app.OrgEntities',
        'app.TypeEntities',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('App.paths.data', TMP_TESTDIR);

        $dir = TMP_TESTDIR . DS . 'lifecycle_extract';
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        Filesystem::mkdir($dir);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        $dir = TMP_TESTDIR . DS . 'lifecycle_extract';
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
    }

    /**
     * testexecute
     */
    public function testexecute()
    {
        $Archives = $this->fetchTable('Archives');
        $sas = $this->fetchTable('OrgEntities')->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES])
            ->all();

        $dir = TMP_TESTDIR . DS . 'lifecycle_extract';

        foreach ($sas as $sa) {
            $query = $Archives->find()
                ->where(
                    [
                        'Archives.archival_agency_id' => $sa->get('id'),
                        'Archives.originating_agency_id' => $sa->get('id'),
                        'Archives.state NOT IN' => [
                            ArchivesTable::S_CREATING,
                            ArchivesTable::S_DESCRIBING,
                            ArchivesTable::S_STORING,
                        ],
                    ]
                )
                ->orderByAsc('StoredFiles.id');

            $this->exec("lifecycle extract {$sa->get('identifier')} {$sa->get('identifier')} $dir");
            $count = $query->count();
            $this->assertOutputContains(__("Sauvegarde de {0} fichier", $count));
            $this->assertOutputContains('done');
            $this->assertCount($count, Filesystem::listFiles($dir));

            Filesystem::remove($dir);
            Filesystem::mkdir($dir);
        }
    }
}
