<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Model\Table\BeanstalkJobsTable;
use Asalae\Model\Table\BeanstalkWorkersTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

use function __;

/**
 * Asalae\Command\JobCommand Test Case
 */
class JobCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public array $fixtures = [
        'app.ArchiveUnits',
        'app.BeanstalkJobs',
        'app.BeanstalkWorkers',
        'app.EventLogs',
        'app.OrgEntities',
        'app.Users',
        'app.ValidationChains',
        'app.Versions',
    ];

    public $rdata = [];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.disable_check_ttr', true);
        $loc = TableRegistry::getTableLocator();
        $loc->clear();

        /** @var BeanstalkWorkersTable $BeanstalkWorkers */
        $BeanstalkWorkers = $loc->get('BeanstalkWorkers');
        $BeanstalkWorkers->sync = true;

        $beanstalk = $this->createMock(Beanstalk::class);
        $beanstalk->method('setTube')->willReturnSelf();
        $beanstalk->method('isConnected')->willReturn(true);
        $this->rdata = [];
        $i = 1;
        $beanstalk->method('emit')->willReturnCallback(
            function ($values) use (&$i) {
                $i++;
                $this->rdata[] = $values;
                return $i;
            }
        );
        Utility::set('Beanstalk', $beanstalk);
        Beanstalk::$instance = $beanstalk;
        Configure::write('Beanstalk.classname', get_class($beanstalk));

        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $loc->get('BeanstalkJobs');
        $BeanstalkJobs->sync = true;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
    }

    /**
     * testMain
     */
    public function testMain()
    {
        $this->exec(
            'job',
            [
                'analyse', // tube
                'string', // add value (key)
                'value', // add value (value)
                'y', // add new value ?

                'boolean', // add value (key)
                'true', // add value (value)
                'y', // add new value ?

                'null', // add value (key)
                'null', // add value (value)
                'y', // add new value ?

                'float', // add value (key)
                '3.14159265359', // add value (value)
                'y', // add new value ?

                'integer', // add value (key)
                '2566', // add value (value)
                'n', // add new value ?

                'n', // add new job ?
            ]
        );
        $expected = [
            'string' => 'value',
            'boolean' => true,
            'null' => null,
            'float' => 3.14159265359,
            'integer' => 2566,
        ];
        $this->assertEquals($expected, $this->rdata[0] ?? []);
    }

    /**
     * testList
     */
    public function testList()
    {
        $count = TableRegistry::getTableLocator()->get('BeanstalkJobs')->find()->count();
        $this->exec('job list --fields id,tube,job_state');
        $this->assertOutputContains('test');
        $this->assertOutputContains(BeanstalkJobsTable::S_PENDING);
        $this->assertOutputContains(
            __(
                "Affichage des résultats de {0} à {1} sur un total de {2} jobs",
                0,
                $count,
                $count
            )
        );

        $this->exec('job list test --fields id,tube,job_state --state ready --page 1 --limit 1000');
        $this->assertOutputContains(BeanstalkJobsTable::S_PENDING);
    }

    /**
     * testResume
     */
    public function testResume()
    {
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $logsCount = $EventLogs->find()->count();
        $BeanstalkJobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $jobid = $BeanstalkJobs->find()->first()->get('id');
        $BeanstalkJobs->updateAll(['job_state' => BeanstalkJobsTable::S_FAILED], ['id' => $jobid]);
        $this->exec('job resume ' . $jobid);
        $this->assertCount($logsCount + 1, $EventLogs->find());
    }

    /**
     * testPause
     */
    public function testPause()
    {
        $BeanstalkJobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $jobid = $BeanstalkJobs->find()->first()->get('id');
        $this->exec('job pause ' . $jobid);
        $this->assertOutputContains('done');
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $logsCount = $EventLogs->find()->count();
        $BeanstalkJobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $jobid = $BeanstalkJobs->find()->first()->get('id');
        $this->exec('job delete ' . $jobid);
        $this->assertCount($logsCount + 1, $EventLogs->find());
    }

    /**
     * testView
     */
    public function testView()
    {
        $BeanstalkJobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $jobid = $BeanstalkJobs->find()->first()->get('id');
        $this->exec('job view ' . $jobid);
        $this->assertOutputContains('test');
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $logsCount = $EventLogs->find()->count();

        $BeanstalkJobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $jobid = $BeanstalkJobs->find()->first()->get('id');
        $BeanstalkJobs->updateAll(['data' => json_encode(['test' => 'foo'])], ['id' => $jobid]);
        $this->exec('job edit ' . $jobid);
        $this->assertOutputContains('\'{"test":"foo"}\'');
        $this->assertCount($logsCount, $EventLogs->find());

        $this->exec('job edit ' . $jobid . ' \'{"test":"foo"}\'');
        $this->assertOutputContains('success');
        $this->assertCount($logsCount + 1, $EventLogs->find());
    }

    /**
     * testResumeAll
     */
    public function testResumeAll()
    {
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $BeanstalkJobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $BeanstalkJobs->updateAll(
            ['job_state' => BeanstalkJobsTable::S_FAILED],
            ['tube' => 'test']
        );
        $logsCount = $EventLogs->find()->count();

        $this->exec('job resume_all test');
        $this->assertOutputContains('success');
        $this->assertCount($logsCount + 1, $EventLogs->find());
    }

    /**
     * testDeleteAll
     */
    public function testDeleteAll()
    {
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $logsCount = $EventLogs->find()->count();

        $this->exec('job delete_all test');
        $this->assertOutputContains('success');
        $this->assertCount($logsCount + 1, $EventLogs->find());
    }
}
