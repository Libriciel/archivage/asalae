<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;
use Exception;

/**
 * Asalae\Command\CompletenessCheckCommand Test Case
 */
class CompletenessCheckCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
        'Archives',
        'ArchiveUnits',
        'ArchiveBinariesArchiveUnits',
        'ArchiveBinaries',
        'ArchiveFiles',
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
    ];
    private $debugFixtures = false;

    /**
     * @return void
     * @throws VolumeException
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('App.paths.data', TMP_TESTDIR);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * @return void
     */
    public function testNoProblemo()
    {
        // Aucune erreur détecté
        $this->exec('completeness_check --stop-at-first-error');
        $this->assertOutputContains(__("Nombre d'archives à contrôler : {0}", 5));
    }

    /**
     * @return void
     */
    public function testArchiveUnits()
    {
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');

        // vériifaction du nombre d'unités d'archives
        $archiveUnit = $ArchiveUnits->find()->where(['archive_id' => 1])->first();
        $ArchiveUnits->delete($archiveUnit);
        $this->exec('completeness_check --stop-at-first-error');
        $this->assertErrorContains(
            __(
                "Le nombre d'unités d'archives en base de données ({0}) est différent du nombre " .
                "d'unités d'archives du fichier de description ({1})",
                0,
                2
            )
        );
    }

    /**
     * @return void
     */
    public function testManagementFiles()
    {
        $ArchiveFiles = $this->fetchTable('ArchiveFiles');
        $StoredFilesVolumes = $this->fetchTable('StoredFilesVolumes');

        // Fichier de description absent en base de données dans la table archive_files
        $archiveFile = $ArchiveFiles->find()->where(['archive_id' => 1, 'type' => 'description'])->first();
        $archiveFile->set('type', 'description_masked');
        $ArchiveFiles->save($archiveFile);
        $this->exec('completeness_check --stop-at-first-error');
        $this->assertErrorContains(
            __(
                "Le fichier {0} est absent en base de données dans la table archive_files",
                __("de description")
            )
        );
        $archiveFile->set('type', 'description');
        $ArchiveFiles->save($archiveFile);
        $this->cleanupConsoleTrait();

        // Fichier du transfert absent en base de données dans la table archive_files
        $archiveFile = $ArchiveFiles->find()->where(['archive_id' => 1, 'type' => 'transfer'])->first();
        $archiveFile->set('type', 'transfer_masked');
        $ArchiveFiles->save($archiveFile);
        $this->exec('completeness_check --stop-at-first-error');
        $this->assertErrorContains(
            __(
                "Le fichier {0} est absent en base de données dans la table archive_files",
                __("du transfert")
            )
        );
        $archiveFile->set('type', 'transfer');
        $ArchiveFiles->save($archiveFile);
        $this->cleanupConsoleTrait();

        // Fichier du cycle de vie absent en base de données dans la table archive_files
        $archiveFile = $ArchiveFiles->find()->where(['archive_id' => 1, 'type' => 'lifecycle'])->first();
        $archiveFile->set('type', 'lifecycle_masked');
        $ArchiveFiles->save($archiveFile);
        $this->exec('completeness_check --stop-at-first-error');
        $this->assertErrorContains(
            __(
                "Le fichier {0} est absent en base de données dans la table archive_files",
                __("du cycle de vie")
            )
        );
        $archiveFile->set('type', 'lifecycle');
        $ArchiveFiles->save($archiveFile);
        $this->cleanupConsoleTrait();

        // Fichier des unités d'archives supprimées absent en base de données dans la table archive_files
        $archiveFile = $ArchiveFiles->find()->where(['archive_id' => 3, 'type' => 'deleted'])->first();
        $archiveFile->set('type', 'deleted_masked');
        $ArchiveFiles->save($archiveFile);
        $this->exec('completeness_check --stop-at-first-error');
        $this->assertErrorContains(
            __(
                "Le fichier {0} est absent en base de données dans la table archive_files",
                __("des unités d'archives supprimées")
            )
        );
        $archiveFile->set('type', 'deleted');
        $ArchiveFiles->save($archiveFile);
        $this->cleanupConsoleTrait();

        // Fichier de description absent en base de données dans la table stored_files_volumes
        $archiveFile = $ArchiveFiles->find()->where(['archive_id' => 1, 'type' => 'description'])->first();
        $StoredFilesVolumes->deleteAll(['stored_file_id' => $archiveFile->get('stored_file_id')]);
        $this->exec('completeness_check --stop-at-first-error');
        $this->assertErrorContains(
            __(
                "La référence de stockage du fichier {0} sur le volume de stockage {1} (id:{2}) "
                . "est absente en base de données dans la table stored_files_volumes",
                __("de description"),
                'volume01',
                1
            )
        );
    }

    /**
     * @return void
     */
    public function testBinariesFiles()
    {
        $ArchiveBinaries = $this->fetchTable('ArchiveBinaries');
        $StoredFilesVolumes = $this->fetchTable('StoredFilesVolumes');

        // Fichier original absent en base de données dans la table stored_files_volumes
        $archiveBinary = $ArchiveBinaries->find()->where(['id' => 1])->first();
        $StoredFilesVolumes->deleteAll(['stored_file_id' => $archiveBinary->get('stored_file_id')]);
        $this->exec('completeness_check --stop-at-first-error');
        $this->assertErrorContains(
            __(
                "La référence de stockage du fichier {0} sur le volume de stockage {1} (id:{2}) "
                . "est absente en base de données dans la table stored_files_volumes",
                __("original"),
                'volume01',
                1
            )
        );
        $this->cleanupConsoleTrait();

        // Fichier original absent en base de données dans la table stored_files
        $archiveBinary->set('stored_file_id', null);
        $ArchiveBinaries->save($archiveBinary);
        $this->exec('completeness_check --stop-at-first-error');
        $this->assertErrorContains(
            __(
                "L'id de la référence de stockage du fichier {0} est nulle "
                . "en base de données dans la table archive_binaries",
                __("original")
            )
        );
    }
}
