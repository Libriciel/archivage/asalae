<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;
use Exception;

/**
 * Asalae\Command\IndicatorsUpdateCommand Test Case
 */
class IndicatorsUpdateCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
        'ArchiveIndicators',
        'MessageIndicators',
        'Archives',
        'ArchiveUnits',
        'ArchiveBinariesArchiveUnits',
        'ArchiveBinaries',
        'ArchiveFiles',
        'DestructionRequests',
        'DestructionNotifications',
        'DestructionNotificationXpaths',
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
    ];
    private $debugFixtures = false;

    /**
     * @return void
     * @throws VolumeException
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('App.paths.data', TMP_TESTDIR);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * @return void
     */
    public function testFullSession()
    {
        $archiveCount = $this->fetchTable('Archives')->find()->count();

        // test création session
        $this->exec('indicators_update begin');
        $this->assertOutputContains(__("Création des fichiers de session"));
        $this->assertOutputContains(__("- nombre total d'archives    : {0}", $archiveCount));
        $this->cleanupConsoleTrait();

        // test session déjà présente
        $this->exec('indicators_update begin');
        $this->assertErrorContains(__("une session est déjà en cours {0}", ''));
        $this->cleanupConsoleTrait();

        // test du calcul des indicateurs
        $this->exec('indicators_update calculate --quiet');
        $this->exec('indicators_update status');
        $this->assertOutputContains(__("- nombre d'archives traitées : {0}", $archiveCount));
        $this->cleanupConsoleTrait();

        // test affichage des indicateurs
        $this->exec('indicators_update show');
        $this->assertOutputContains(
            __(
                "Indicateurs par service {0} des archives pour le service d'Archives {1}",
                'versant',
                $this->fetchTable('OrgEntities')->get(2)->get('name')
            )
        );
        $this->cleanupConsoleTrait();

        // test mise à jour des indicateurs
        $this->exec('indicators_update commit');
        $this->assertOutputContains(__("Mise à jour des indicateurs en base de données"));
        $this->cleanupConsoleTrait();

        // test indicateurs mis à jour
        $whereClause = [
            'archival_agency_id' => 2,
            'transferring_agency_id' => 2,
            'originating_agency_id' => 2,
        ];
        $q = $this->fetchTable('ArchiveIndicators')->find();
        $archiveIndicatorSums = $q
            ->select(
                [
                    'original_count' => $q->func()->sum('original_count'),
                    'original_size' => $q->func()->sum('original_size'),
                ]
            )
            ->where($whereClause)
            ->disableHydration()
            ->first();
        $q = $this->fetchTable('Archives')->find();
        $archiveSums = $q
            ->select(
                [
                    'original_count' => $q->func()->sum('original_count'),
                    'original_size' => $q->func()->sum('original_size'),
                ]
            )
            ->where($whereClause)
            ->disableHydration()
            ->first();
        $this->assertEquals(
            $archiveIndicatorSums['original_count'],
            $archiveSums['original_count'],
            __(
                "le nombre des fichiers originaux ({0}) des indicateurs mis à jour "
                . "me correspond pas aux nombre des fichiers originaux des archives ({1})",
                $archiveIndicatorSums['original_count'],
                $archiveSums['original_count']
            )
        );
        $this->assertEquals(
            $archiveIndicatorSums['original_size'],
            $archiveSums['original_size'],
            __(
                "la taille des fichiers originaux ({0}) des indicateurs mis à jour "
                . "me correspond pas à la taille des fichiers originaux des archives ({1})",
                $archiveIndicatorSums['original_size'],
                $archiveSums['original_size']
            )
        );
    }

    /**
     * @return void
     */
    public function testRollbackSession()
    {
        $archiveCount = $this->fetchTable('Archives')->find()->count();

        // test création session
        $this->exec('indicators_update begin');
        $this->assertOutputContains(__("Création des fichiers de session"));
        $this->assertOutputContains(__("- nombre total d'archives    : {0}", $archiveCount));
        $this->cleanupConsoleTrait();

        // test rollback session
        $this->exec('indicators_update rollback');
        $this->assertOutputContains(__("Session annulée : suppression des fichiers de session"));
        $this->cleanupConsoleTrait();
    }
}
