<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Worker\AnalyseWorker;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use Beanstalk\Model\Table\BeanstalkWorkersTable;
use Beanstalk\Test\Mock\MockedBeanstalk;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use DateTime;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const TMP_TESTDIR;

/**
 * Asalae\Command\MonitorCommandTest Test Case
 */
class MonitorCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public array $fixtures = [
        'app.Users',
        'app.BeanstalkWorkers',
        'app.BeanstalkJobs',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        mkdir(TMP_TESTDIR);
        Configure::write('MonitorCommand.logfile', TMP_TESTDIR . DS . 'monitor.log');
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
    }

    /**
     * testMain
     */
    public function testMain()
    {
        $loc = TableRegistry::getTableLocator();
        $workers = $this->getMockBuilder(BeanstalkWorkersTable::class)
            ->onlyMethods(['sync'])
            ->getMock();
        $loc->set('BeanstalkWorkers', $workers);
        $workers->setTable('beanstalk_workers');
        $workers->save(
            new Entity(
                [
                    'name' => 'analyse',
                    'tube' => 'analyse',
                    'path' => AnalyseWorker::class,
                    'pid' => -1000,
                    'last_launch' => new DateTime(),
                    'hostname' => gethostname(),
                ]
            )
        );
        Configure::write('Beanstalk.disable_check_ttr', true);

        Utility::set('Beanstalk', MockedBeanstalk::class);
        Notify::$instance = $this->createMock(Notify::class);

        $this->exec('monitor --oneshot --verbose');
        $this->assertFileExists(TMP_TESTDIR . DS . 'monitor.log');
    }
}
