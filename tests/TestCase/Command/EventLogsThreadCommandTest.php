<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Asalae\Command\EventLogsThreadCommand Test Case
 */
class EventLogsThreadCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAppendFixtures = [
        FIXTURES_EVENT_LOGS,
        FIXTURES_VOLUMES,
        'app.TypeEntities',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('App.paths.data', TMP_TESTDIR);
    }

    /**
     * testexecute
     */
    public function testexecute()
    {
        $this->exec('event_logs thread sa sa_1');

        $this->assertOutputContains('transfer_add');
        $this->assertExitCode(0);

        $this->cleanupConsoleTrait();
        $file = DS . 'sa' . DS . 'technical_archives' . DS . '6621dae4-ff82-4429-bd2d-73f514c1095d'
            . DS . 'asalae_events_log_20220107.xml';
        // xml avec chaînage non fonctionnel
        $s = '<?xml version="1.0"?>
<premis xmlns="http://www.loc.gov/premis/v3" version="3.0">
  <object xmlns="http://www.loc.gov/premis/v3">
    <objectIdentifier>
      <objectIdentifierType>local</objectIdentifierType>
      <objectIdentifierValue>EventsLogsChain</objectIdentifierValue>
    </objectIdentifier>
    <significantProperties>
      <significantPropertiesType>filename</significantPropertiesType>
      <significantPropertiesValue>asalae_events_log_20220106.xml</significantPropertiesValue>
    </significantProperties>
    <significantProperties>
      <significantPropertiesType>size</significantPropertiesType>
      <significantPropertiesValue>4489</significantPropertiesValue>
    </significantProperties>
    <significantProperties>
      <significantPropertiesType>hash_algo</significantPropertiesType>
      <significantPropertiesValue>sha256</significantPropertiesValue>
    </significantProperties>
    <significantProperties>
      <significantPropertiesType>hash</significantPropertiesType>
      <significantPropertiesValue>toto</significantPropertiesValue>
    </significantProperties>
    <originalName>previous file</originalName>
  </object>
</premis>';

        $h1 = fopen(TMP_VOL1 . $file, 'w');
        fwrite($h1, $s);
        fclose($h1);

        $h2 = fopen(TMP_VOL2 . $file, 'w');
        fwrite($h2, $s);
        fclose($h2);

        $StoredFiles = TableRegistry::getTableLocator()->get('StoredFiles');
        $StoredFiles->updateAll(
            ['hash' => $newHash = hash('sha256', $s)],
            ['name' => 'sa/technical_archives/6621dae4-ff82-4429-bd2d-73f514c1095d/asalae_events_log_20220107.xml']
        );

        $this->exec('event_logs thread sa sa_1');
        $this->assertErrorContains(__("valeur du fichier précédent : <error>$newHash</error>"));
        $this->assertExitCode(1);
    }
}
