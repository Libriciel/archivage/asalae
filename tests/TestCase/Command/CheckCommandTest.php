<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Test\Mock\CheckMock;
use Asalae\Utility\Check as CheckUtility;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;

class CheckCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * testExecute
     */
    public function testExecute()
    {
        Utility::set(CheckUtility::class, CheckMock::class);
        CheckMock::$outputs = [
            'missings' => [],
            'missingsExtPhp' => [],
            'database' => true,
            'beanstalkd' => true,
            'workers' => [],
            'ratchet' => true,
            'clamav' => true,
            'phpOk' => true,
        ];

        $this->exec('check');
        $this->assertExitCode(0);

        CheckMock::$outputs = [
            'missings' => ['testunit'],
            'missingsExtPhp' => ['php-testunit'],
            'database' => false,
            'beanstalkd' => false,
            'workers' => [],
            'ratchet' => false,
            'clamav' => false,
            'phpOk' => false,
        ];
        $this->exec('check');
        $this->assertExitCode(1);
    }
}
