<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Test\Mock\ClientMock;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Config;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Core\Configure;
use Cake\Http\Client\Response;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

class OpenIdConnectCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    private string $originalPathToLocal;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->originalPathToLocal = Configure::read('App.paths.path_to_local_config');
        $dir = TMP_TESTDIR . DS . 'testopenid';
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('App.paths.path_to_local_config', $dir . DS . 'path_to_local.php');
        Filesystem::reset();
        Filesystem::dumpFile($dir . DS . 'path_to_local.php', "<?php return '$dir/app_local.json';?>");
        Filesystem::dumpFile($dir . DS . 'app_local.json', "{}");
        Configure::consume('OpenIDConnect');
        Config::reset();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Configure::write('App.paths.path_to_local_config', $this->originalPathToLocal);
        $dir = TMP_TESTDIR . DS . 'testopenid';
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        Config::reset();
        Utility::reset();
    }

    /**
     * testExecute
     */
    public function testExecute()
    {
        // intercepte le promptPassword
        $exec = $this->createMock(Exec::class);
        $exec->method('command')->willReturn(
            new CommandResult(
                [
                    'success' => true,
                    'code' => 0,
                    'stdout' => 'OK',
                    'stderr' => '',
                ]
            )
        );
        $exec->method('rawCommand')->willReturn('OK');
        Utility::set('Exec', $exec);

        $responsePost = $this->createMock(Response::class);
        $responsePost->method('getStatusCode')->willReturn(200);
        $responsePost->method('getStringBody')->willReturn(
            json_encode(['access_token' => 'test'])
        );
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get', 'post'])
            ->getMock();
        $client->method('get')->willReturnCallback(
            function ($url) {
                $baseUrl = 'http://localhost:8080/realms/master/';
                $baseClient = $baseUrl . 'protocol/openid-connect/';
                $baseAdmin = 'http://localhost:8080/admin/realms/master/';
                switch ($url) {
                    case $baseUrl . '.well-known/openid-configuration':
                        $json = [
                            'authorization_endpoint' => $baseClient . 'auth',
                            'token_endpoint' => $baseClient . 'token',
                            'userinfo_endpoint' => $baseClient . 'userinfo',
                            'end_session_endpoint' => $baseClient . 'logout',
                        ];
                        break;
                    case $baseAdmin . 'clients':
                        $json = [
                            'asalae' => [
                                'id' => 'asalae',
                                'clientId' => 'asalae',
                            ],
                        ];
                        break;
                    case $baseAdmin . 'clients/asalae/client-secret':
                        $json = [
                            'value' => 'secret',
                        ];
                        break;
                    case $baseClient . 'userinfo':
                        $json = [
                            'sub' => 'testunit',
                            'preferred_username' => 'testunit',
                        ];
                        break;
                    default:
                        throw new Exception('Not defined ' . $url);
                }
                $response = $this->createMock(Response::class);
                $response->method('getStatusCode')->willReturn(200);
                $response->method('getStringBody')
                    ->willReturn(json_encode($json, JSON_UNESCAPED_SLASHES));
                return $response;
            }
        );
        $client->method('post')->willReturn($responsePost);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());

        $data = [
            'http://localhost:8080/realms/master', // Veuillez saisir l'url de base
            'testunit', // Username de l'admin
            // password = OK -> $exec->method('command')->willReturn(['stdout' => 'OK'])
            'admin-cli', // client-id admin
            'asalae', // client_id
            'auto', // redirect_uri
            'openid', // scope
            'admin', // username
            // password = OK -> $exec->method('command')->willReturn(['stdout' => 'OK'])
            'preferred_username', // username (field)
        ];
        $this->exec('open_id_connect', $data);
        $this->assertOutputContains('done');
        $json = file_get_contents(TMP_TESTDIR . DS . 'testopenid/app_local.json');
        $expected = [
            'OpenIDConnect' => [
                'base_url' => 'http://localhost:8080/realms/master',
                'client_id' => 'asalae',
                'client_secret' => 'secret',
                'redirect_uri' => 'auto',
                'scope' => 'openid',
                'username' => 'preferred_username',
            ],
        ];
        $this->assertEquals(
            $expected,
            json_decode($json, true)
        );
    }
}
