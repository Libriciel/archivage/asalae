<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Command\Lifecycle\UploadCommand;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use DateTime;

/**
 * Asalae\Command\Lifecycle\LifecycleUploadCommand Test Case
 */
class LifecycleUploadCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;
    use InvokePrivateTrait;

    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.EventLogs',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        Configure::write('Archives.updateLifecycle.path', TMP_TESTDIR);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testexec
     */
    public function testexec()
    {
        $StoredFiles = TableRegistry::getTableLocator()->get('StoredFiles');
        $content = VolumeSample::getSampleContent('sa_394_lifecycle.xml');
        $sha256 = hash('sha256', $content);
        $this->assertNotEquals(
            $StoredFiles->get(2)->get('hash'),
            $sha256
        );
        $EventLogs = $this->fetchTable('EventLogs');
        $EventLogs->updateAll(['in_lifecycle' => false], ['id' => 51]);
        $dir = TMP_TESTDIR . '/to_upload';
        $file = (new DateTime())->format('Ymd') . '.serialize';
        UploadCommand::prepareUpload($content, 2, 1, [51]);
        $this->_out = new ConsoleOutput();
        $this->_err = new ConsoleOutput();
        $this->exec('lifecycle upload');
        $this->assertFileDoesNotExist($dir . DS . $file);
        $this->assertFileDoesNotExist(TMP_TESTDIR . '/update_lifecycle/.lock');
        $this->assertEquals(
            $StoredFiles->get(2)->get('hash'),
            $sha256
        );
        $this->assertTrue($EventLogs->get(51)->get('in_lifecycle'));

        // test avec .lock verrouillé
        file_put_contents(TMP_TESTDIR . '/.lock', getmypid());
        $this->exec('lifecycle upload');
        $this->assertErrorContains(__("Un upload semble être déjà en cours (PID={0})", getmypid()));

        // test avec .lock déverrouillé (note: commencer trop bas ne fonctionne pas)
        $pid = 200;
        $m = [1 => true];
        while (!empty($m[1])) {
            $ps = exec(sprintf('ps -p %d', $pid));
            preg_match('/^\s*(\d+)/', $ps, $m);
            $pid++;
            if ($pid > 65535) {
                $this->fail('empty pid not found');
            }
        }
        file_put_contents(TMP_TESTDIR . '/.lock', $pid);
        $this->_out = new ConsoleOutput();
        $this->_err = new ConsoleOutput();
        $this->exec('lifecycle upload');
        $this->assertOutputContains('done');
    }

    /**
     * testdestruct
     */
    public function testdestruct()
    {
        file_put_contents(TMP_TESTDIR . '/.lock', getmypid());
        $lifecycleCommand = new UploadCommand();
        $this->invokeProperty($lifecycleCommand, 'lock', 'set', TMP_TESTDIR . '/.lock');
        $lifecycleCommand->__destruct();
        $this->assertFileDoesNotExist(TMP_TESTDIR . '/.lock');
    }
}
