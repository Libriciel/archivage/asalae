<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Datacompressor\Utility\DataCompressor;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const TMP_TESTDIR;

/**
 * Asalae\Command\ZipFiles\ZipFilesCommand Test Case
 */
class ZipFilesCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public const string SEDA_10_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml';
    public const string LIFECYCLE_PREMIS = ASALAE_CORE_TEST_DATA . DS . 'sample_premis.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_EVENT_LOGS,
        FIXTURES_VOLUMES,
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveDescriptions',
        'app.Configurations',
        'app.EventLogs',
        'app.Mails',
        'app.Notifications',
        'app.TransferAttachments',
        'app.TypeEntities',
    ];
    public $testfile = 'transfers/1/attachments/file';
    public $testfile2 = 'transfers/1/attachments/sample.pdf';
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('App.paths.download', TMP_TESTDIR . DS . 'download');
        Filesystem::reset();
        TableRegistry::getTableLocator()->clear();
        VolumeSample::init();

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Filesystem::rollback();
        Filesystem::setNamespace();
        Filesystem::reset();
        Utility::reset();
    }

    /**
     * testArchive
     */
    public function testArchive()
    {
        VolumeSample::fillVolumes();
        $this->exec('zip_files archive 1 1');
        $tempFolder = Configure::read('App.paths.download') . DS . 'archive' . DS;
        $zipFile = $tempFolder . '1_archive_files.zip';
        $this->assertFileExists($zipFile, implode("\n", $this->_out->messages()));
        DataCompressor::uncompress($zipFile, $tempFolder);
        $this->assertFileExists($tempFolder . 'management_data' . DS . 'sa_1_description.xml');
    }

    /**
     * testArchiveUnit
     */
    public function testArchiveUnit()
    {
        VolumeSample::fillVolumes();
        $tempFolder = Configure::read('App.paths.data') . DS . 'download' . DS . 'archive_unit' . DS;
        $zipFile = $tempFolder . '1_archive_units.zip';
        $documentFile = $tempFolder . 'original_data' . DS . 'sample.wmv';
        $this->assertFileDoesNotExist($zipFile);
        $this->assertFileDoesNotExist($documentFile);
        $this->exec('zip_files archive_unit 1 1');
        $this->assertFileExists($zipFile);
        DataCompressor::uncompress($zipFile, $tempFolder);
        $this->assertFileExists($documentFile);
        $this->assertFileExists($tempFolder . 'management_data' . DS . '1_description.xml');
        $this->assertFileExists($tempFolder . 'original_data' . DS . 'sample.wmv');
    }

    /**
     * testTransfer
     */
    public function testTransfer()
    {
        VolumeSample::fillTransfersData();
        $tempFolder = Configure::read('App.paths.download') . DS . 'transfer' . DS;
        $zipFile = $tempFolder . '1_transfer_files.zip';
        $this->assertFileDoesNotExist($zipFile);
        $this->exec('zip_files transfer 1 1');
        $this->assertFileExists($zipFile);
        DataCompressor::uncompress($zipFile, $tempFolder);
        $this->assertFileExists($tempFolder . 'sample.wmv');
    }
}
