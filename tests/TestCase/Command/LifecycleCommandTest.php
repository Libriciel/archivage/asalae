<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Model\Table\ArchiveFilesTable;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\Table;

use function __;

/**
 * Asalae\Command\Lifecycle\LifecycleCommand Test Case
 * @property Table $StoredFilesVolumes
 * @property Table $StoredFiles
 */
class LifecycleCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.ArchiveFiles',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('App.paths.data', TMP_TESTDIR);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * testexecute
     */
    public function testexecute()
    {
        $StoredFiles = $this->fetchTable('StoredFiles');
        $query = $StoredFiles->find()
            ->innerJoinWith('ArchiveFiles')
            ->where(['ArchiveFiles.type' => ArchiveFilesTable::TYPE_LIFECYCLE])
            ->orderByAsc('StoredFiles.id');


        $this->exec('lifecycle --schema --hash --verbose');
        $count = $query->count();
        $this->assertOutputContains(__("Vérification de {0} cycles de vie", $count));
        $this->assertOutputContains(__("Tout les fichiers ont été vérifiés avec succès"));

        $StoredFilesVolumes = $this->fetchTable('StoredFilesVolumes');
        $StoredFilesVolumes->deleteAll(
            [
                'storage_ref' => 'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml',
                'volume_id' => 2,
            ]
        );
        $this->_out = new ConsoleOutput();
        $this->exec('lifecycle');
        $this->assertOutputNotContains(__("Tout les fichiers ont été vérifiés avec succès"));
        $this->assertErrorContains(
            __(
                "Un ou plusieurs fichiers sont en échec. "
                . "Utilisez la commande `bin/cake lifecycle repair <filename>`"
            )
        );
    }
}
