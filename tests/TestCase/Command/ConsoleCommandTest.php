<?php

namespace Asalae\Test\TestCase\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Psy\Shell as PsyShell;

/**
 * Asalae\Command\ConsoleCommand Test Case
 */
class ConsoleCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
    }

    /**
     * testMain
     */
    public function testMain()
    {
        $psy = $this->createMock(PsyShell::class);
        $psy->method('run');
        Utility::set(PsyShell::class, $psy);
        $this->exec('console');
        $this->assertOutputContains('You can exit with');
    }
}
