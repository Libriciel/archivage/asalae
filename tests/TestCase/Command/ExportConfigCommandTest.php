<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Form\InstallConfigForm;
use Asalae\Test\Mock\ConnectionManager;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Config;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\ConnectionManager as CakeConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;

use const TMP_TESTDIR;

/**
 * Asalae\Command\ExportConfigCommand Test Case
 */
class ExportConfigCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
        'Acos',
        'Agreements',
        'ArchivingSystems',
        'Aros',
        'ArosAcos',
        'Configurations',
        'Counters',
        'Crons',
        'Eaccpfs',
        'Keywords',
        'KeywordLists',
        'Ldaps',
        'OrgEntities',
        'Profiles',
        'Phinxlog',
        'Roles',
        'RolesTypeEntities',
        'SecureDataSpaces',
        'ServiceLevels',
        'Timestampers',
        'TypeEntities',
        'Users',
        'ValidationActors',
        'ValidationChains',
        'ValidationStages',
        'Volumes',
    ];

    private string $timestampingDir;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        mkdir($dir);

        $conf = [
            'Email' => [
                'default' => ['from' => 'test_for_export_config@test.fr'],
            ],
        ];
        Filesystem::dumpFile(TMP_TESTDIR . "test_local.json", json_encode($conf));
        Filesystem::dumpFile(TMP_TESTDIR . 'path_to_local.php', '<?php return TMP_TESTDIR."test_local.json";?>');
        Configure::write('App.paths.path_to_local_config', TMP_TESTDIR . 'path_to_local.php');
        $hasher = new DefaultPasswordHasher();
        $admins = [['username' => 'admin', 'password' => $hasher->hash('admin'), 'email' => 'noreply@libriciel.net']];
        $administators = TMP_TESTDIR . DS . 'administrateurs.json';
        Configure::write('App.paths.administrators_json', $administators);
        Filesystem::dumpFile($administators, json_encode($admins));

        $this->timestampingDir = TMP_TESTDIR . DS . 'timestamping' . DS;
        if (is_dir($this->timestampingDir)) {
            Filesystem::remove($this->timestampingDir);
        }
        mkdir($this->timestampingDir);
        Filesystem::dumpFile($this->timestampingDir . 'tsacert.crt', 'foo');
        Filesystem::dumpFile($this->timestampingDir . 'tsacert.pem', 'bar');
        Filesystem::dumpFile($this->timestampingDir . 'asalae-tsa.cnf', 'baz');

        Utility::set(CakeConnectionManager::class, new ConnectionManager());
        $conn = $this->createMock(Connection::class);
        $conn->method('queryFactory')->willThrowException(new MissingConnectionException());
        ConnectionManager::$outputs = [
            'drop' => true,
            'setConfig' => true,
            'get' => $conn,
        ];
        $exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(
                [
                    'command',
                    'async',
                ]
            )
            ->getMock();
        $exec->method('async');
        $exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => 'OK', 'stderr' => ''])
        );
        Utility::set('Exec', $exec);

        Config::reset();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        $this->timestampingDir = TMP_TESTDIR . DS . 'timestamping' . DS;
        if (is_dir($this->timestampingDir)) {
            Filesystem::remove($this->timestampingDir);
        }
        Utility::reset();
        Config::reset();
    }

    /**
     * testExportAll
     */
    public function testExportAll()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $OrgEntities->saveOrFail(
            $OrgEntities->newEntity(
                [
                    'identifier' => 'test',
                    'name' => 'test',
                    'parent_id' => $OrgEntities->find()
                        ->where(['name' => 'Mon autre service d\'archive'])
                        ->firstOrFail()
                        ->get('id'),
                    'type_entity_id' => TableRegistry::getTableLocator()->get('TypeEntities')->find()
                        ->where(['code' => 'SV'])
                        ->first()
                        ->get('id'),
                ]
            )
        );

        $this->exec('export config all');
        $filename = $this->_out->messages()[0];
        $export = parse_ini_file($filename, true);
        $this->assertEquals('se', Hash::get($export, 'exploitation_service.identifier'));
        $this->assertEquals(2, count(Hash::get($export, 'archival_agencies')));
        $this->assertEquals(3, count(Hash::get($export, 'org_entities')));
        $this->assertEquals(
            [
                '_id' => '1',
                'type' => 'USER',
                'user_username' => 'admin',
                'user_type_validation' => 'V',
            ],
            Hash::get($export, 'validation_actors.validation_actor_1')
        );
        $this->assertEquals(
            TableRegistry::getTableLocator()->get('Users')->get(1)->get('password'),
            Hash::get($export, 'users.user_1.hashed_password')
        );
        $this->assertNull(Hash::get($export, 'users.user_1.password'));

        $files = [
            [
                'path' => $this->timestampingDir . 'tsacert.crt',
                'content' => base64_encode('foo'),
            ],
            [
                'path' => $this->timestampingDir . 'tsacert.crt',
                'content' => base64_encode('foo'),
            ],
            [
                'path' => $this->timestampingDir . 'tsacert.pem',
                'content' => base64_encode('bar'),
            ],
            [
                'path' => $this->timestampingDir . 'asalae-tsa.cnf',
                'content' => base64_encode('baz'),
            ],
        ];
        $this->assertEquals($files, $export['files']);

        // contenu de config
        $this->assertEquals('test_for_export_config@test.fr', $export['email']['from']);

        $data = Hash::flatten($export, '__');
        $form = new InstallConfigForm();

        $this->assertTrue($form->validate($data));
    }

    /**
     * testExportTenant
     */
    public function testExportTenant()
    {
        $this->exec('export config all -t sa');
        $filename = $this->_out->messages()[0];
        $export = parse_ini_file($filename, true);
        $this->assertEquals(1, count(Hash::get($export, 'archival_agencies')));
        $this->assertEquals(2, count(Hash::get($export, 'org_entities')));
        $this->assertEquals(4, count(Hash::get($export, 'users')));
    }

    /**
     * testExportFunctional
     */
    public function testExportFunctional()
    {
        $this->exec('export config');
        $filename = $this->_out->messages()[0];
        $export = parse_ini_file($filename, true);

        $this->assertEquals(null, Hash::get($export, 'exploitation_service'));
        $this->assertEquals(null, Hash::get($export, 'archiving_systems'));
        $this->assertEquals(2, count(Hash::get($export, 'org_entities')));
        $this->assertEquals(6, count(Hash::get($export, 'users')));
    }
}
