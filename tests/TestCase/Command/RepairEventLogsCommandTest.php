<?php

namespace Asalae\Test\TestCase\Command;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\ORM\TableRegistry;

/**
 * Asalae\Command\RepairEventLogsCommandTest Test Case
 */
class RepairEventLogsCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_EVENT_LOGS,
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        TableRegistry::getTableLocator()->clear();
    }

    /**
     * testMain
     */
    public function testMain()
    {
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $EventLogs->updateAll(['object_serialized' => null, 'object_model' => 'Users', 'object_foreign_key' => -1], []);
        $EventLogs->deleteAll(['id >' => 1]);
        $this->exec('event_logs repair', ['unknown']);
        $this->assertGreaterThanOrEqual(
            1,
            $EventLogs->find()->where(['object_serialized IS NOT' => null])->count()
        );
    }
}
