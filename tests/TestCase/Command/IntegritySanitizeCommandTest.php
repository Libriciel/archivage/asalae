<?php

namespace Asalae\Test\TestCase\Command;

use Asalae\Command\IntegritySanitizeCommand;
use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(IntegritySanitizeCommand::class)]
class IntegritySanitizeCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public array $fixtures = [
        'app.ArchiveBinaries',
        'app.ArchiveFiles',
        'app.ArchiveUnits',
        'app.Archives',
        'app.StoredFiles',
        'app.StoredFilesVolumes',
        'app.TechnicalArchiveUnits',
        'app.TechnicalArchives',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * testExecute
     */
    public function testExecute()
    {
        // réinitialise les fixtures
        $this->fetchTable('ArchiveFiles')->updateAll(['is_integrity_ok' => null], []);
        $this->fetchTable('ArchiveBinaries')->updateAll(['is_integrity_ok' => null], []);
        $this->fetchTable('Archives')->updateAll(['is_integrity_ok' => null], []);
        $this->fetchTable('StoredFilesVolumes')->updateAll(['is_integrity_ok' => null], []);
        $this->fetchTable('TechnicalArchives')->updateAll(['is_integrity_ok' => null], []);

        $this->exec("integrity sanitize");
        $this->assertOutputContains('100%');

        $binary = $this->fetchTable('ArchiveBinaries')->get(1);
        $this->assertNull($binary->get('is_integrity_ok'));

        // stored_files_volumes - 1 ok, le 2e mauvais
        $this->fetchTable('StoredFilesVolumes')
            ->updateAll(['is_integrity_ok' => true], ['id' => 1]);
        $this->fetchTable('StoredFilesVolumes')
            ->updateAll(['is_integrity_ok' => false], ['id' => 2]);
        $this->cleanupConsoleTrait();
        $this->exec("integrity sanitize");

        $binary = $this->fetchTable('ArchiveBinaries')->get(1);
        $this->assertFalse($binary->get('is_integrity_ok'));
    }
}
