<?php

namespace Asalae\Test\TestCase\Command;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\ORM\TableRegistry;

/**
 * Asalae\Command\IntegrityHistoCommand Test Case
 */
class IntegrityHistoCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        'app.Archives',
        'app.Crons',
    ];
    private $debugFixtures = false;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @return void
     */
    public function testRun()
    {
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $Archives->updateAll(
            [
                'is_integrity_ok' => true,
                'integrity_date' => time(),
            ],
            ['id' => 1]
        );
        $this->exec('integrity histo');
        $this->assertOutputContains("nombre total d'archives : 5");
        $this->assertOutputContains(
            "| " . date('Y-m-d')
            . "    | 1                 | 20 %                   | 1                  | 20 %               |"
        );
    }
}
