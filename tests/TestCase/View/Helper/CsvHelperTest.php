<?php

namespace Asalae\Test\TestCase\View\Helper;

use Asalae\View\Helper\CsvHelper;
use AsalaeCore\DataType\JsonString;
use AsalaeCore\I18n\Date;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\I18n\DateTime;
use Cake\ORM\TableRegistry;
use Cake\View\View;

/**
 * Asalae\View\Helper\TranslateHelper Test Case
 */
class CsvHelperTest extends TestCase
{
    use IntegrationCronTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        'app.ArchiveBinaries',
        'app.Notifications',
        'app.StoredFiles',
        'app.Users',
    ];
    /**
     * Test subject
     *
     * @var CsvHelper
     */
    public $CsvHelper;
    private $debugFixtures = false;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->CsvHelper = new CsvHelper($view);
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TranslateHelper);

        parent::tearDown();
    }

    /**
     * testresultTable
     */
    public function testresultTable()
    {
        $resultSet = [
            [ // 1st row
                'foo' => 'bar',
                'with' => [
                    'some' => [
                        'depth' => pi(),
                    ],
                ],
                'another' => [
                    'field' => false,
                ],
                'created' => new Date('2001-09-20T05:24:49+01:00'),
            ],
            [ // 2nd row
                'foo' => 'text with single quote (\') and double quote (") and' . "\nline break",
                'with' => [
                    'some' => [
                        'depth' => 124,
                    ],
                ],
                'another' => [
                    'field' => true,
                ],
                'created' => new DateTime('1999-06-06T15:22:13+02:30'),
            ],
            [ // 3rd row
                'foo' => '',
                'with' => [
                    'some' => [
                        'depth' => 12.2,
                    ],
                ],
                'another' => [
                    'field' => JsonString::create('["test"]'),
                ],
                'created' => null,
            ],
        ];

        $results = $this->CsvHelper->resultTable(
            [
                'foo' => 'my first column',
                'with.some.depth' => 'my second column',
                'another.field' => 'my 3rd column',
                'created' => 'my 4th column',
            ],
            $resultSet
        );

        // row 1 (header row)
        $this->assertStringContainsString('"my first column","my second column","', $results);
        $this->assertStringContainsString('my 3rd column","my 4th column"' . "\r\n", $results);

        // row 2 (1st result row)
        $this->assertStringContainsString("\r\n" . '"bar","3,1415926535898",', $results);
        $this->assertStringContainsString(__("Non") . ',20/09/2001' . "\r\n", $results);

        // row 3 (2nd result row)
        $this->assertStringContainsString(
            "\r\n" . '"text with single quote (\') and double quote ("") and' . "\n" . 'line break",124,',
            $results
        );
        $this->assertStringContainsString(__("Oui") . ',06/06/1999 15:22' . "\r\n", $results);

        // row 4 (3nd result row)
        $this->assertStringContainsString("\r\n" . ',"12,2","[""test""]",' . "\r\n", $results);
        file_put_contents('/tmp/test.csv', $results);
    }

    /**
     * testqueryToArray
     */
    public function testqueryToArray()
    {
        $query = $this->fetchTable('ArchiveBinaries')
            ->find()
            ->orderByAsc('ArchiveBinaries.id')
            ->contain(['StoredFiles']);
        $results = $this->CsvHelper->resultTable(
            [
                'filename' => 'filename',
                'stored_file.size' => 'size',
            ],
            $query
        );
        $this->assertStringContainsString('sample_wmv.mp4",172355' . "\r\n", $results);
    }

    /**
     * testQueryToArray65kResults
     */
    public function testQueryToArray65kResults()
    {
        $limit = 65535 - 1;
        $Table = TableRegistry::getTableLocator()->get('Notifications');
        $Table->deleteAll([]);

        $i = 0;
        while ($i++ < $limit) {
            $Table->save(
                $Table->newEntity(
                    [
                        'user_id' => 1,
                        'text' => $i,
                    ]
                )
            );
        }

        $results = $this->CsvHelper->resultTable(
            [
                'text' => 'text',
                'user.username' => 'username',
            ],
            $Table->find()->contain(['Users'])
        );
        $arrResults = explode("\r\n", $results);
        $this->assertEquals($limit, count($arrResults) - 2); // 65534

        $Table->save(
            $Table->newEntity(
                [
                    'user_id' => 1,
                    'text' => $i++,
                ]
            )
        );
        $results = $this->CsvHelper->resultTable(
            [
                'text' => 'text',
                'user.username' => 'username',
            ],
            $Table->find()->contain(['Users'])
        );
        $arrResults = explode("\r\n", $results);
        $this->assertEquals($limit + 1, count($arrResults) - 2); // 65535

        $Table->save(
            $Table->newEntity(
                [
                    'user_id' => 1,
                    'text' => $i,
                ]
            )
        );
        $results = $this->CsvHelper->resultTable(
            [
                'text' => 'text',
                'user.username' => 'username',
            ],
            $Table->find()->contain(['Users'])
        );
        $arrResults = explode("\r\n", $results);
        $this->assertEquals($limit + 2, count($arrResults) - 2); // 65536

        $prev = null;
        foreach (range(1, $limit + 2) as $index) {
            $text = explode(',', $arrResults[$index])[0];
            $this->assertEquals('"' . $index . '"', $text); // c'est dans l'ordre
            $this->assertNotEquals($prev, $arrResults[$index]); // c'est différent d'une ligne à l'autre
            $prev = $arrResults[$index];
        }
    }
}
