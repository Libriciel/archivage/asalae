<?php

namespace Asalae\Test\TestCase\View\Helper;

use Asalae\View\Helper\InputHelper;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\View\View;

/**
 * Asalae\View\Helper\InputHelper Test Case
 */
class InputHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var InputHelper
     */
    public $InputHelper;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->InputHelper = new InputHelper($view);
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * testmult
     */
    public function testmult()
    {
        $result = $this->InputHelper->mult('test', 'go');
        $this->assertStringContainsString('<select name="test">', $result);
        $this->assertStringContainsString(
            '<option value="1073741824"  selected="selected">Go</option>',
            $result
        );

        $result = $this->InputHelper->mult('test', 'mo', ['id' => 226, 'data-test' => 'test']);
        $this->assertStringContainsString('<select name="test" id="226" data-test="test"', $result);
    }

    /**
     * testoperator
     */
    public function testoperator()
    {
        $result = $this->InputHelper->operator('test');
        $this->assertStringContainsString('<select name="test">', $result);
        $this->assertStringContainsString(
            '<option value="="  selected="selected">=</option>',
            $result
        );

        $result = $this->InputHelper->operator('test', '=', ['id' => 226, 'data-test' => 'test']);
        $this->assertStringContainsString('<select name="test" id="226" data-test="test"', $result);
    }
}
