<?php

namespace Asalae\Test\TestCase\View\Helper;

use Asalae\View\Helper\SearchEngineHelper;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\View\View;

/**
 * Asalae\View\Helper\SearchEngineHelper Test Case
 */
class SearchEngineHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var SearchEngineHelper
     */
    public $SearchEngine;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->SearchEngine = new SearchEngineHelper($view);
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SearchEngine);

        parent::tearDown();
    }

    /**
     * testForm
     */
    public function testForm()
    {
        $form = $this->SearchEngine->form(null, ['id' => 'testunit']);
        $this->assertStringContainsString('testunit', $form);
    }

    /**
     * testInput
     */
    public function testInput()
    {
        $input = $this->SearchEngine->input('input', ['id' => 'testunit']);
        $this->assertStringContainsString('testunit', $input);
    }

    /**
     * testScript
     */
    public function testScript()
    {
        $script = $this->SearchEngine->script(
            'testunit',
            '/Foo/bar',
            '/Foo/baz',
            ['Foo.bar']
        );
        $this->assertStringContainsString('testunit', $script);
    }
}
