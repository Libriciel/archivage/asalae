<?php

namespace Asalae\Test\TestCase\View\Helper;

use Asalae\View\Helper\Html2PdfHelper;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\I18n\DateTime;
use Cake\ORM\Entity;
use Cake\View\View;

/**
 * Asalae\View\Helper\Html2PdfHelper Test Case
 */
class Html2PdfHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var Html2PdfHelper
     */
    public $Html2PdfHelper;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->Html2PdfHelper = new Html2PdfHelper($view);
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * testTable
     */
    public function testTable()
    {
        $archival_agency = new Entity(['name' => 'Foo']);
        $transferring_agency = new Entity(['name' => 'Bar']);
        $result = $this->Html2PdfHelper->table(
            [
                __("Date d'édition de l'attestation") => $time = new DateTime(),
                __("Service d'archives") => $archival_agency->get('name'),
                __("Service versant") => $transferring_agency->get('name'),
            ]
        );
        $this->assertStringContainsString(
            sprintf(
                "<table><tbody><tr><th>%s</th><td>%s</td></tr>",
                __("Date d'édition de l'attestation"),
                htmlentities($time)
            ),
            $result
        );
        $this->assertStringContainsString('Foo', $result);
        $this->assertStringContainsString('Bar', $result);
    }
}
