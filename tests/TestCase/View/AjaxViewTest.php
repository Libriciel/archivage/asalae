<?php

namespace Asalae\Test\TestCase\View;

use Asalae\View\AjaxView;
use AsalaeCore\Http\Response;
use AsalaeCore\TestSuite\TestCase;

class AjaxViewTest extends TestCase
{
    /**
     * testConstruct
     */
    public function testConstruct()
    {
        $response = new Response();
        $view = new AjaxView(null, $response);
        $this->assertEquals('text/html', $view->getResponse()->getType());
    }
}
