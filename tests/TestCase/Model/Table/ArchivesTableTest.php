<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\EventLogsTable;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Cache\Cache;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DOMDocument;
use Libriciel\Filesystem\Utility\Filesystem;

use const PREMIS_V3;
use const SEDA_V10_XSD;
use const TMP_TESTDIR;

class ArchivesTableTest extends TestCase
{
    use AutoFixturesTrait;
    use ConsoleIntegrationTestTrait;

    public const string SEDA_10_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml';
    public const string LIFECYCLE_PREMIS = ASALAE_CORE_TEST_DATA . DS . 'sample_premis.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_EVENT_LOGS,
        FIXTURES_VOLUMES,
        'app.AccessRules',
        'app.AccessRuleCodes',
        'app.AppraisalRules',
        'app.AppraisalRuleCodes',
        'app.ArchiveDescriptions',
    ];
    private $debugFixtures = false;
    /**
     * @var ArchivesTable
     */
    private $Archives;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        TableRegistry::getTableLocator()->clear();
        $this->Archives = TableRegistry::getTableLocator()->get('Archives');
        Configure::write('Archives.updateLifecycle.path', TMP_TESTDIR);
        Configure::write('Archives.updateDescription.path', TMP_TESTDIR);
        Configure::write('Archives.updateLifecycle.cache_duration', 0);
        Configure::write('Archives.updateDescription.cache_duration', 0);
        $Exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(['async', 'command'])
            ->getMock();
        $Exec->method('command')->willReturnCallback(
            function ($command, ...$args) {
                if ($command === CAKE_SHELL) {
                    $escaped = $this->extractArgsShellOptions($args);
                    $command = implode(' ', $escaped);
                    $out = new ConsoleOutput();
                    $err = new ConsoleOutput();
                    $io = new ConsoleIo($out, $err, new ConsoleInput([]));
                    $runner = $this->makeRunner();
                    $args = $this->commandStringToArgs("cake $command");
                    $code = $runner->run($args, $io);
                    if (is_file(TMP_TESTDIR . '/update_lifecycle/.lock')) {
                        unlink(TMP_TESTDIR . '/update_lifecycle/.lock');
                    }
                    return new CommandResult(
                        [
                            'success' => $code === 0,
                            'code' => $code,
                            'stdout' => implode(PHP_EOL, $out->messages()),
                            'stderr' => implode(PHP_EOL, $err->messages()),
                        ]
                    );
                } else {
                    $Exec = new Exec();
                    return $Exec->command($command, ...$args);
                }
            }
        );
        Utility::set('Exec', $Exec);
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Cache::clear('lifecycle');
    }

    /**
     * extractArgsShellOptions
     * @param array $args
     * @return array
     */
    private function extractArgsShellOptions(array $args): array
    {
        $escaped = [];
        foreach ($args as $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $escaped[] = $k;
                    if ($v) {
                        $escaped[] = $v;
                    }
                }
            } else {
                $escaped[] = $value;
            }
        }
        return $escaped;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Filesystem::setNamespace();
        Filesystem::reset();
        Exec::waitUntilAsyncFinish();
        Utility::reset();
    }

    /**
     * testUpdateDescriptionXml
     */
    public function testUpdateDescriptionXml()
    {
        $entity = $this->getDescriptionEntity();
        $this->assertInstanceOf(EntityInterface::class, $entity);
        $uniqid = uniqid('foo');
        $entity->set('name', $uniqid);
        $this->assertNotFalse($this->Archives->save($entity));

        $this->assertTrue($this->Archives->updateDescriptionXml($entity));
        /** @var DescriptionXmlArchiveFile $desc */
        $desc = Hash::get($entity, 'description_xml_archive_file');
        $this->assertInstanceOf(DescriptionXmlArchiveFile::class, $desc);
        $dom = $desc->getDom();
        $this->assertInstanceOf(DOMDocument::class, $dom);
        $this->assertStringContainsString($uniqid, $dom->saveXML());

        $uniqid = uniqid('bar');
        $entity = $this->getDescriptionEntity();
        /** @var DescriptionXmlArchiveFile $desc */
        $desc = Hash::get($entity, 'first_archive_unit.archive_description');
        $desc->set('history', $uniqid);
        $this->assertTrue($this->Archives->updateDescriptionXml($entity));
        /** @var DescriptionXmlArchiveFile $desc */
        $desc = Hash::get($entity, 'description_xml_archive_file');
        $dom = $desc->getDom();
        $this->assertStringContainsString($uniqid, $dom->saveXML());
        $this->assertTrue($dom->schemaValidate(SEDA_V10_XSD));
    }

    /**
     * getDescriptionEntity
     */
    private function getDescriptionEntity(): EntityInterface
    {
        return $this->Archives->find()
            ->where(['Archives.id' => 1])
            ->andWhere(['Archives.archival_agency_id' => 2])
            ->contain(
                [
                    'Agreements',
                    'AccessRules' => ['AccessRuleCodes'],
                    'AppraisalRules' => ['AppraisalRuleCodes'],
                    'Profiles',
                    'OriginatingAgencies',
                    'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                    'FirstArchiveUnits' => [
                        'ArchiveDescriptions',
                        'ArchiveKeywords',
                    ],
                ]
            )
            ->first();
    }

    /**
     * testGetLastException
     */
    public function testGetLastException()
    {
        $this->assertNull($this->Archives->getLastException());
    }

    /**
     * testUpdateLifecycleXml
     */
    public function testUpdateLifecycleXml()
    {
        $archive = $this->Archives->find()
            ->where(['Archives.id' => 1])
            ->contain(['LifecycleXmlArchiveFiles' => ['StoredFiles']])
            ->first();
        $stored = Hash::get($archive, 'lifecycle_xml_archive_file.stored_file');
        if (!$stored instanceof EntityInterface) {
            $this->fail();
        }
        $lifecycle = $stored->get('file');

        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        if (!$EventLogs instanceof EventLogsTable) {
            $this->fail();
        }
        $entry = $EventLogs->newEntry('test', 'test', 'test', 1, $archive);
        $this->assertNotFalse($EventLogs->save($entry));

        $this->Archives->updateLifecycleXml(Hash::get($archive, 'lifecycle_xml_archive_file.stored_file'), $entry);
        Exec::waitUntilAsyncFinish();
        $this->assertNotEquals($lifecycle, $xml = $stored->get('file'));

        $dom1 = new DOMDocument();
        $dom1->loadXML($lifecycle);
        $dom2 = new DOMDocument();
        $dom2->loadXML($xml);

        $this->assertCount(
            $dom1->getElementsByTagName('event')->count() + 1,
            $dom2->getElementsByTagName('event')
        );

        $entry2 = $EventLogs->newEntry('test', 'test', 'test', 1, $archive);
        $this->assertNotFalse($EventLogs->save($entry2));

        $this->Archives->updateLifecycleXml(Hash::get($archive, 'lifecycle_xml_archive_file.stored_file'), $entry);
        Exec::waitUntilAsyncFinish();
        $this->assertNotEquals($xml, $xml2 = $stored->get('file'));

        $dom3 = new DOMDocument();
        $dom3->loadXML($xml2);

        $this->assertCount(
            $dom2->getElementsByTagName('event')->count() + 1,
            $dom3->getElementsByTagName('event')
        );
        $this->assertCount(
            $dom2->getElementsByTagName('object')->count(),
            $dom3->getElementsByTagName('object')
        );
        $this->assertCount(
            $dom2->getElementsByTagName('agent')->count(),
            $dom3->getElementsByTagName('agent')
        );

        $this->assertTrue($dom3->schemaValidate(PREMIS_V3));
    }

    /**
     * testChangeIntegrityDateDontChangeModified
     */
    public function testChangeIntegrityDateDontChangeModified()
    {
        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $archive = $Archives->find()
            ->select(['id', 'modified', 'integrity_date'])
            ->where(['Archives.id' => 1])
            ->first();

        $expected = (string)$archive->get('modified');
        $archive->set('integrity_date', time());
        $Archives->save($archive);
        $archive = $Archives->find()
            ->select(['id', 'modified', 'integrity_date'])
            ->where(['Archives.id' => 1])
            ->first();

        $actual = (string)$archive->get('modified');
        $this->assertEquals($expected, $actual);
    }
}
