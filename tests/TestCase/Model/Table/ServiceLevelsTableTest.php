<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\ServiceLevelsTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

class ServiceLevelsTableTest extends TestCase
{
    public array $fixtures = [
        'app.Archives',
        'app.ArchivesTransfers',
        'app.OrgEntities',
        'app.SecureDataSpaces',
        'app.ServiceLevels',
        'app.Transfers',
    ];

    /**
     * testSetDefault
     */
    public function testSetDefault()
    {
        /** @var ServiceLevelsTable $table */
        $table = TableRegistry::getTableLocator()->get('ServiceLevels');
        $table->updateAll(['default_level' => false], ['id !=' => 1]);
        $entity = $table->get(1);
        $table->patchEntity($entity, ['default_level' => false] + $entity->toArray());
        $this->assertNotEmpty($entity->getError('default_level'));

        $data = $entity->toArray();
        unset($data['id']);
        $data['identifier'] .= '_2';
        $data['default_level'] = true;
        $this->assertNotFalse($nEntity = $table->save($table->newEntity($data)));
        $this->assertFalse($table->get(1)->get('default_level'));
        $this->assertEquals($nEntity->id, $table->getDefault($data['org_entity_id'])->id);
    }
}
