<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\ArchiveBinariesTable;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\DOMUtility;
use DOMDocument;
use DOMElement;

class ArchiveBinariesTableTest extends TestCase
{
    /**
     * testfindBinaryByFilename
     */
    public function testfindBinaryByFilename()
    {
        // test en seda0.2
        $xml = VolumeSample::getSampleContent('sample_seda02.xml');
        $util = DOMUtility::loadXML($xml);
        $element = ArchiveBinariesTable::findBinaryByFilename($util, 'sample.odt');
        $this->assertNotEmpty($element);
        $this->assertEquals('Document', $element->tagName);

        // on s'assure que les caractères spéciaux sont également trouvées
        /** @var DOMElement $attachment */
        $attachment = $util->node('ns:Attachment', $element);
        $specials = "foo/bar' & baz $^*ù\n%\t*\"!:;.\\zip";
        $attachment->setAttribute('filename', $specials);
        $element = ArchiveBinariesTable::findBinaryByFilename($util, $specials);
        $this->assertNotEmpty($element);
        $this->assertEquals('Document', $element->tagName);

        // test en seda1.0
        $xml = VolumeSample::getSampleContent('sample_seda10.xml');
        $util = DOMUtility::loadXML($xml);
        $element = ArchiveBinariesTable::findBinaryByFilename($util, 'sample.odt');
        $this->assertNotEmpty($element);
        $this->assertEquals('Document', $element->tagName);

        // on s'assure que les caractères spéciaux sont également trouvées
        /** @var DOMElement $attachment */
        $attachment = $util->node('ns:Attachment', $element);
        $attachment->setAttribute('filename', $specials);
        $element = ArchiveBinariesTable::findBinaryByFilename($util, $specials);
        $this->assertNotEmpty($element);
        $this->assertEquals('Document', $element->tagName);

        // test en seda2.1 (par Fileinfo)
        $xml = VolumeSample::getSampleContent('sample_seda21.xml');
        $util = DOMUtility::loadXML($xml);
        $element = ArchiveBinariesTable::findBinaryByFilename($util, 'sample.pdf');
        $this->assertNotEmpty($element);
        $this->assertEquals('BinaryDataObject', $element->tagName);

        // on s'assure que les caractères spéciaux sont également trouvées
        $fileinfo = $util->node('ns:FileInfo', $element);
        $filename = $util->node('ns:Filename', $fileinfo);
        DOMUtility::setValue($filename, $specials);
        $element = ArchiveBinariesTable::findBinaryByFilename($util, $specials);
        $this->assertNotEmpty($element);
        $this->assertEquals('BinaryDataObject', $element->tagName);

        // test en seda2.1 (par Uri)
        $element->removeChild($fileinfo);
        $element = ArchiveBinariesTable::findBinaryByFilename($util, 'sample.pdf');
        $this->assertNotEmpty($element);
        $this->assertEquals('BinaryDataObject', $element->tagName);

        // on s'assure que les caractères spéciaux sont également trouvées
        $uri = $util->node('ns:Uri', $element);
        DOMUtility::setValue($uri, $specials);
        $element = ArchiveBinariesTable::findBinaryByFilename($util, $specials);
        $this->assertNotEmpty($element);
        $this->assertEquals('BinaryDataObject', $element->tagName);

        // test à vide
        $element->removeChild($uri);
        $notFound = ArchiveBinariesTable::findBinaryByFilename($util, $specials);
        $this->assertEmpty($notFound);

        // test en seda2.1 (par Attachment@filename)
        $attachment = $util->createElement('Attachment');
        $attachment->setAttribute('filename', $specials);
        $util->appendChild($element, $attachment);
        $element = ArchiveBinariesTable::findBinaryByFilename($util, $specials);
        $this->assertNotEmpty($element);
        $this->assertEquals('BinaryDataObject', $element->tagName);

        // test en seda2.2 (par Fileinfo)
        $xml = VolumeSample::getSampleContent('sample_seda22.xml');
        $util = DOMUtility::loadXML($xml);
        $element = ArchiveBinariesTable::findBinaryByFilename($util, 'sample.pdf');
        $this->assertNotEmpty($element);
        $this->assertEquals('BinaryDataObject', $element->tagName);

        // test hors seda
        $dom = new DOMDocument();
        $dom->loadXML('<root></root>');
        $util = new DOMUtility($dom);
        $element = ArchiveBinariesTable::findBinaryByFilename($util, 'sample.pdf');
        $this->assertEmpty($element);
    }
}
