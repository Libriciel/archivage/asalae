<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Table\PublicDescriptionArchiveFilesTable;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\I18n\Date;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use DateTime;
use Libriciel\Filesystem\Utility\Filesystem;

class PublicDescriptionArchiveFilesTableTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.AccessRules',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveKeywords',
        'app.ArchiveUnits',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.Configurations',
        'app.OrgEntities',
        'app.Transfers',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        VolumeSample::init();
        VolumeSample::insertSample(
            'sample_seda10_description.xml',
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
        );
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testCreateFromDescriptionXml
     */
    public function testCreateFromDescriptionXml()
    {
        $loc = TableRegistry::getTableLocator();
        $AccessRules = $loc->get('AccessRules');
        $AccessRules->updateAll(['end_date' => new DateTime('next year')], []);
        $DescriptionXmlArchiveFiles = $loc->get('DescriptionXmlArchiveFiles');
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateAll(['access_rule_id' => 2], ['id >=' => 3]);
        /** @var DescriptionXmlArchiveFile $description */
        $description = $DescriptionXmlArchiveFiles->get(2);
        $initialXml = $description->getDom()->saveXML();

        /** @var PublicDescriptionArchiveFilesTable $PublicDescriptionArchiveFiles */
        $PublicDescriptionArchiveFiles = TableRegistry::getTableLocator()->get('PublicDescriptionArchiveFiles');
        $public = $PublicDescriptionArchiveFiles->createFromDescriptionXml($description);
        // la description est dépourvue des unités d'archives non communicable donc + légère
        $this->assertLessThan(
            strlen(preg_replace('/\s+/', '', $initialXml)),
            strlen(preg_replace('/\s+/', '', $public->get('content')))
        );
        // NOTE: suppression du ContentDescription au niveau de l'archive = invalide au schema
    }

    /**
     * testNextRebuildDate
     */
    public function testNextRebuildDate()
    {
        $loc = TableRegistry::getTableLocator();
        $AccessRules = $loc->get('AccessRules');
        $AccessRules->updateAll(['end_date' => new DateTime('next year')], []);
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateAll(['access_rule_id' => 2], ['id' => 3]);

        /** @var PublicDescriptionArchiveFilesTable $PublicDescriptionArchiveFiles */
        $PublicDescriptionArchiveFiles = TableRegistry::getTableLocator()->get('PublicDescriptionArchiveFiles');
        $next = $PublicDescriptionArchiveFiles->nextRebuildDate(1);
        $this->assertNotNull($next);
        $this->assertInstanceOf(Date::class, $next);
        $this->assertGreaterThan(new Date(), $next);
    }
}
