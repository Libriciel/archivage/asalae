<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\ValidationChainsTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

/**
 * Asalae\Model\Table\ValidationChainsTable Test Case
 */
class ValidationChainsTableTest extends TestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public array $fixtures = [
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationStages',
    ];

    /**
     * testFindListOptionsWithEmptyStage
     */
    public function testFindListOptionsWithEmptyStage()
    {
        /** @var ValidationChainsTable $ValidationChains */
        $loc = TableRegistry::getTableLocator();
        $ValidationChains = $loc->get('ValidationChains');
        $this->assertTrue(
            $ValidationChains->findListOptions(2, ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM)->count() > 0
        );

        // un circuit contenant une étape avec un acteur et une étape sans acteur
        // ne doit pas ressortir comme sélectionnable
        $ValidationStages = $loc->get('ValidationStages');
        $ValidationStages->saveOrFail(
            $ValidationStages->newEntity(
                [
                    'validation_chain_id' => 1,
                    'name' => 'empty_stage',
                    'description' => 'pas d acteur',
                    'all_actors_to_complete' => true,
                ]
            )
        );

        TableRegistry::getTableLocator()->clear();
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = TableRegistry::getTableLocator()->get('ValidationChains');
        $this->assertFalse(
            $ValidationChains->findListOptions(2, ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM)->count() > 0
        );
    }
}
