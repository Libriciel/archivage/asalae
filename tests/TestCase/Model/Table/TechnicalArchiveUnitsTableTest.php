<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\TechnicalArchiveUnitsTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

/**
 * Asalae\Model\Table\TechnicalArchiveUnitsTable Test Case
 */
class TechnicalArchiveUnitsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Asalae\Model\Table\TechnicalArchiveUnitsTable
     */
    public $TechnicalArchiveUnits;

    /**
     * Fixtures
     *
     * @var array
     */
    public array $fixtures = [
        'app.TechnicalArchiveUnits',
        'app.TechnicalArchives',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TechnicalArchiveUnits') ? []
            : ['className' => TechnicalArchiveUnitsTable::class];
        $this->TechnicalArchiveUnits = TableRegistry::getTableLocator()->get('TechnicalArchiveUnits', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TechnicalArchiveUnits);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $entity = $this->TechnicalArchiveUnits->newEntity(
            [
                'state' => $this->TechnicalArchiveUnits->initialState,
                'archival_agency_identifier' => 'foo',
                'name' => 'foo',
                'original_local_size' => 0,
                'original_local_count' => 0,
                'original_total_size' => 0,
                'original_total_count' => 0,
                'xml_node_tagname' => 'Foo[1]',
            ]
        );
        $this->assertEmpty($entity->getErrors());
    }
}
