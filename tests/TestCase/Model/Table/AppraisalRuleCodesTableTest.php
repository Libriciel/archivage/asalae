<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\AppraisalRuleCodesTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

class AppraisalRuleCodesTableTest extends TestCase
{
    public array $fixtures = [
        'app.AppraisalRuleCodes',
        'app.Mediainfos',
        'app.Siegfrieds',
    ];

    /**
     * testValidation
     */
    public function testValidation()
    {
        $table = TableRegistry::getTableLocator()->get('AppraisalRuleCodes');
        $entity = $table->newEntity(['code' => null]);
        $this->assertNotEmpty($entity->getError('code'));
        $entity->set('code', 'foo');
        $this->assertEmpty($entity->getError('code'));

        $this->assertNotEmpty($entity->getError('name'));
        $entity->set('name', 'foo');
        $this->assertEmpty($entity->getError('name'));

        $this->assertNotEmpty($entity->getError('description'));
        $entity->set('description', 'foo');
        $this->assertEmpty($entity->getError('description'));

        $this->assertNotEmpty($entity->getError('duration'));
        $entity->set('duration', 'P1Y');
        $this->assertEmpty($entity->getError('duration'));
        $entity = $table->newEntity(['duration' => '1Y']);
        $this->assertNotEmpty($entity->getError('duration'));

        $entity = $table->newEntity(
            [
                'org_entity_id' => null,
                'code' => 'APP0Y',
                'name' => '0 an',
                'description' => '0 an',
                'duration' => 'P0Y',
            ]
        );
        $this->assertFalse($table->save($entity));
    }

    /**
     * testGetRuleCodes
     */
    public function testGetRuleCodes()
    {
        /** @var AppraisalRuleCodesTable $table */
        $table = TableRegistry::getTableLocator()->get('AppraisalRuleCodes');
        $ruleCodes = $table->getRuleCodes(2, 'seda1.0');
        $this->assertNotEmpty($ruleCodes);
        $this->assertEquals('P0Y', $ruleCodes[0]['value']);
        $ruleCodes = $table->getRuleCodes(2, 'seda2.1');
        $this->assertNotEmpty($ruleCodes["Codes spécifiques"]);
    }
}
