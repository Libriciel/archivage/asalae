<?php

/** @noinspection PhpDeprecationInspection TODO retirer AccessTokensTable et tout ce qui y est lié */

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\AccessTokensTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

class AccessTokensTableTest extends TestCase
{
    /**
     * @var AccessTokensTable
     */
    public $AccessTokens;

    public array $fixtures = [
        'app.AccessTokens',
        'app.Aros',
        'app.Mediainfos',
        'app.Siegfrieds',
        'app.Users',
        'app.Webservices',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->AccessTokens = TableRegistry::getTableLocator()->get('AccessTokens');
    }

    /**
     * testCreateCodeToken
     */
    public function testCreateCodeToken()
    {
        $Webservices = TableRegistry::getTableLocator()->get('Webservices');
        $webservice = $Webservices->newEntity(
            [
                'name' => 'testunit',
                'description' => '',
                'active' => true,
            ]
        );
        $Webservices->saveOrFail($webservice);
        $accessTokenInitialCount = $this->AccessTokens->find()->all()->count();
        $entity = $this->AccessTokens->createCodeToken($webservice);
        $this->assertEquals($webservice->get('id'), $entity->get('webservice_id'));
        $this->assertEquals(
            $accessTokenInitialCount + 1,
            $this->AccessTokens->find()->all()->count()
        );
    }

    /**
     * testCreateAccessTokens
     */
    public function testCreateAccessTokens()
    {
        $Webservices = TableRegistry::getTableLocator()->get('Webservices');
        $webservice = $Webservices->newEntity(
            [
                'name' => 'test',
                'description' => '',
                'active' => true,
            ]
        );
        $Webservices->saveOrFail($webservice);
        $accessTokenInitialCount = $this->AccessTokens->find()->all()->count();
        $this->AccessTokens->createAccessTokens($webservice, 1);

        $this->assertEquals(
            $accessTokenInitialCount + 2,
            $this->AccessTokens->find()->all()->count()
        );
    }
}
