<?php

declare(strict_types=1);

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\TransferLocksTable;
use Asalae\Test\TestCase\Controller\HttpTrait;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use Cake\Datasource\EntityInterface;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use DateInterval;
use DateTime;
use Exception;

/**
 * Asalae\Model\Table\TransferLocksTable Test Case
 */
class TransferLocksTableTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];

    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,
        FIXTURES_VOLUMES,
        'app.Transfers',
    ];
    /**
     * Test subject
     *
     * @var \Asalae\Model\Table\TransferLocksTable
     */
    protected $TransferLocks;
    private $debugFixtures = false;

    /**
     * Test beforeFind method
     *
     * @return void
     * @throws Exception
     */
    public function testLock(): void
    {
        /** @var TransferLocksTable $TransferLocks */
        $TransferLocks = $this->fetchTable('TransferLocks');
        $freePid = 1;
        while (file_exists('/proc/' . $freePid)) {
            $freePid++;
        }
        $fiveMinutesAgo = (new DateTime())->sub(new DateInterval('PT5M'));

        $TransferLocks->deleteAll(['transfer_id' => 1]);
        $this->assertFalse($this->getTransfer()->get('is_locked'));

        $expiredUploading = $TransferLocks->newEntity(
            [
                'transfer_id' => 1,
                'pid' => null,
                'uploading' => true,
                'created' => $fiveMinutesAgo,
                'modified' => $fiveMinutesAgo,
            ]
        );
        $TransferLocks->saveOrFail($expiredUploading);
        $this->assertTrue($this->getTransfer()->get('is_locked'));
        $this->assertTrue($this->getTransfer()->get('has_expired_lock'));

        $TransferLocks->deleteAll(['transfer_id' => 1]);
        $uploading = $TransferLocks->newEntity(
            [
                'transfer_id' => 1,
                'pid' => null,
                'uploading' => true,
            ]
        );
        $TransferLocks->saveOrFail($uploading);
        $this->assertTrue($this->getTransfer()->get('is_locked'));
        $this->assertFalse($this->getTransfer()->get('has_expired_lock'));


        $TransferLocks->deleteAll(['transfer_id' => 1]);
        $endedProcess = $TransferLocks->newEntity(
            [
                'transfer_id' => 1,
                'pid' => $freePid,
                'uploading' => false,
            ]
        );
        $TransferLocks->saveOrFail($endedProcess);
        $this->assertTrue($this->getTransfer()->get('is_locked'));
        $this->assertTrue($this->getTransfer()->get('has_expired_lock'));


        $TransferLocks->deleteAll(['transfer_id' => 1]);
        $runningProcess = $TransferLocks->newEntity(
            [
                'transfer_id' => 1,
                'pid' => getmypid(),
                'uploading' => false,
            ]
        );
        $TransferLocks->saveOrFail($runningProcess);
        $this->assertTrue($this->getTransfer()->get('is_locked'));
        $this->assertFalse($this->getTransfer()->get('has_expired_lock'));
    }

    /**
     * getTransfer
     */
    private function getTransfer(): EntityInterface
    {
        return $this->fetchTable('Transfers')
            ->find()
            ->where(['Transfers.id' => 1])
            ->contain(['TransferLocks'])
            ->firstOrFail();
    }
}
