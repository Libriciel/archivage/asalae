<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\TransfersTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class TransfersTableTest extends TestCase
{
    public const string SEDA10_SAMPLE_FILE = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';

    public array $fixtures = [
        'app.Agreements',
        'app.OrgEntities',
        'app.Profiles',
        'app.ServiceLevels',
        'app.TransferLocks',
        'app.Transfers',
        'app.TypeEntities',
    ];

    /**
     * testApplyChanges
     */
    public function testApplyChanges()
    {
        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $Agreements->updateAll(['identifier' => 'agr-test01'], ['id' => 1]);
        $Profiles = TableRegistry::getTableLocator()->get('Profiles');
        $Profiles->updateAll(['identifier' => 'test'], ['id' => 1]);
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $ServiceLevels->updateAll(['identifier' => 'service-level-test'], ['id' => 1]);
        /** @var TransfersTable $Transfers */
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $entity = new Entity(
            [
                'xml' => self::SEDA10_SAMPLE_FILE,
            ]
        );
        $Transfers->applyChanges($entity);
        $this->assertEquals(
            'Transfert aléatoire atr_alea_70f90eacdb2d3b53ad7e87f808a82430',
            $entity->get('transfer_comment')
        );
        $this->assertEquals('2018-10-19', $entity->get('transfer_date')->format('Y-m-d'));
        $this->assertEquals(1, $entity->get('agreement_id'));
        $this->assertEquals(1, $entity->get('profile_id'));
        $this->assertEquals(1, $entity->get('service_level_id'));
    }
}
