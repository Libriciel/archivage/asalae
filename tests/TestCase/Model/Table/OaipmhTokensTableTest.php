<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\OaipmhTokensTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

/**
 * Asalae\Model\Table\OaipmhTokensTableTest Test Case
 */
class OaipmhTokensTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var OaipmhTokensTable
     */
    public $OaipmhTokens;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->OaipmhTokens = TableRegistry::getTableLocator()->get('OaipmhTokens');
    }

    /**
     * testEncodeDecode
     */
    public function testEncodeDecode()
    {
        $chain = '~!@#$%^&*()-_=+[]\{}|;\':",./<>?|\éà漢';
        $encoded = $this->OaipmhTokens->encode($chain);

        preg_match("([A-Za-z0-9\-_.!~*'()]+)", $encoded, $matches);
        $this->assertEquals([$encoded], $matches);
        $this->assertEquals($chain, $this->OaipmhTokens->decode($encoded));
    }
}
