<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\CountersTable;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Premis\IntellectualEntity;
use Cake\ORM\TableRegistry;
use DateTime;

class CountersTableTest extends TestCase
{
    public array $fixtures = [
        'app.Sequences',
        'app.Counters',
        'app.OrgEntities',
        'app.EventLogs',
    ];

    /**
     * testbeforeDelete
     */
    public function testbeforeDelete()
    {
        $Counters = TableRegistry::getTableLocator()->get('Counters');
        $counter = $Counters->newEntity(
            [
                'org_entity_id' => 2,
                'identifier' => 'test',
                'name' => 'test',
                'description' => 'test',
                'type' => 'test',
                'definition_mask' => '#YYYY_#s#',
                'sequence_id' => 1,
                'active' => true,
            ]
        );
        $Counters->saveOrFail($counter);
        $EventLogs = TableRegistry::getTableLocator()->get('EventLogs');
        $event = $EventLogs->newEntity(
            [
                'type' => 'test',
                'outcome' => 'ok',
                'outcome_detail' => 'test',
                'object_model' => 'Counters',
                'object_foreign_key' => $counter->id,
                'exported' => false,
            ]
        );
        $EventLogs->saveOrFail($event);
        $Counters->delete($counter);
        $event = $EventLogs->get($event->id);
        $this->assertNotEmpty($event->get('object_serialized'));
        $premis = unserialize($event->get('object_serialized'));
        $this->assertInstanceOf(IntellectualEntity::class, $premis);
    }

    /**
     * testGenerate
     */
    public function testGenerate()
    {
        /** @var CountersTable $Counters */
        $Counters = TableRegistry::getTableLocator()->get('Counters');

        $counter = $Counters->generate(125, 'TEST_#s#');
        $this->assertEquals('TEST_125', $counter);

        $counter = $Counters->generate(133, '#AAAA#-#MM#-#JJ#_TEST_#s#');
        $this->assertEquals(date('Y-m-d') . '_TEST_133', $counter);

        $counter = $Counters->generate(246, '#J##M##AA#_TEST_#s#');
        $this->assertEquals(date('jny') . '_TEST_246', $counter);

        $date = new DateTime('2010-01-01');
        $counter = $Counters->generate(246, '#J#/#M#/#AA#_TEST_#s#', $date);
        $this->assertEquals('1/1/10_TEST_246', $counter);

        $counter = $Counters->generate(389, 'TEST#SS#');
        $this->assertEquals('TEST89', $counter);
        $counter = $Counters->generate(389, 'TEST#SSS#');
        $this->assertEquals('TEST389', $counter);
        $counter = $Counters->generate(389, 'TEST#SSSS#');
        $this->assertEquals('TEST_389', $counter);
        $counter = $Counters->generate(389, 'TEST#SSSSS#');
        $this->assertEquals('TEST__389', $counter);

        $counter = $Counters->generate(389, 'TEST#00#');
        $this->assertEquals('TEST89', $counter);
        $counter = $Counters->generate(389, 'TEST#000#');
        $this->assertEquals('TEST389', $counter);
        $counter = $Counters->generate(389, 'TEST#0000#');
        $this->assertEquals('TEST0389', $counter);
        $counter = $Counters->generate(389, 'TEST#00000#');
        $this->assertEquals('TEST00389', $counter);
    }

    /**
     * testNext
     */
    public function testNext()
    {
        /** @var CountersTable $Counters */
        $Counters = TableRegistry::getTableLocator()->get('Counters');
        $Counters->updateAll(
            [
                'definition_mask' => 'TEST_#TestString#_#AAAA#-#MM#-#JJ#_#AA#-#M#-#J#x#S#x#SS#x#SSS#xxx#0#x#00#x#000#xxx#s#',
                'sequence_reset_mask' => '#AAAA#',
            ],
            ['id' => 13]
        );
        $Sequences = TableRegistry::getTableLocator()->get('Sequences');
        $Sequences->updateAll(['value' => 0], []);
        $params = [
            'date' => new DateTime('2010-01-01'),
            'replace' => ['TestString' => 'Foo'],
        ];
        $output = $Counters->next(2, 'ArchiveTransfer', $params);
        $this->assertEquals("TEST_Foo_2010-01-01_10-1-1x1x_1x__1xxx1x01x001xxx1", $output);

        $sequenceValue = $Sequences->find()
            ->innerJoinWith('Counters')
            ->where(['Counters.identifier' => 'ArchiveTransfer'])
            ->first()
            ->get('value');
        $this->assertEquals(1, $sequenceValue);

        $output = $Counters->next(2, 'ArchiveTransfer', $params);
        $this->assertEquals("TEST_Foo_2010-01-01_10-1-1x2x_2x__2xxx2x02x002xxx2", $output);

        $sequenceValue = $Sequences->find()
            ->innerJoinWith('Counters')
            ->where(['Counters.identifier' => 'ArchiveTransfer'])
            ->first()
            ->get('value');
        $this->assertEquals(2, $sequenceValue);

        $params['value'] = 101;
        $output = $Counters->next(2, 'ArchiveTransfer', $params);
        $this->assertEquals("TEST_Foo_2010-01-01_10-1-1x1x01x101xxx1x01x101xxx101", $output);

        $sequenceValue = $Sequences->find()
            ->innerJoinWith('Counters')
            ->where(['Counters.identifier' => 'ArchiveTransfer'])
            ->first()
            ->get('value');
        $this->assertEquals(101, $sequenceValue);

        $params['value'] = null;
        $params['date'] = new DateTime('2012-02-15'); // sequence_reset_mask = #AAAA#
        $output = $Counters->next(2, 'ArchiveTransfer', $params);
        $this->assertEquals("TEST_Foo_2012-02-15_12-2-15x1x_1x__1xxx1x01x001xxx1", $output);
    }
}
