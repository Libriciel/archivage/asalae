<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\OrgEntitiesTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use PHPUnit\Framework\MockObject\Exception;
use ReflectionException;

/**
 * Asalae\Model\Table\OrgEntitiesTable Test Case
 */
class OrgEntitiesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var OrgEntitiesTable
     */
    public $OrgEntities;

    /**
     * Fixtures
     *
     * @var array
     */
    public array $fixtures = [
        'app.AppraisalRules',
        'app.AccessRules',
        'app.AccessRuleCodes',
        'app.Agreements',
        'app.AgreementsOriginatingAgencies',
        'app.AgreementsTransferringAgencies',
        'app.AppraisalRuleCodes',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.Configurations',
        'app.Counters',
        'app.Eaccpfs',
        'app.EventLogs',
        'app.KeywordLists',
        'app.Ldaps',
        'app.OrgEntities',
        'app.Profiles',
        'app.Roles',
        'app.SecureDataSpaces',
        'app.Sequences',
        'app.ServiceLevels',
        'app.Sessions',
        'app.Timestampers',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
        'app.ValidationChains',
    ];

    /**
     * setUp method
     *
     * @return void
     * @throws ReflectionException
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->OrgEntities);
        Utility::reset();

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        // Création SE
        $se = $this->OrgEntities->newEntity(
            [
                'name' => 'test',
                'identifier' => 'test',
                'parent_id' => null,
                'type_entity_id' => 1,
                'timestamper_id' => null,
                'active' => true,
                'hierarchical_view' => true,
                'default_secure_data_space_id' => null,
                'is_main_archival_agency' => null,
            ]
        );
        $this->assertNotFalse($this->OrgEntities->save($se));

        // Création SA
        $sa = $this->OrgEntities->newEntity(
            [
                'name' => 'sa',
                'identifier' => 'sa',
                'parent_id' => $se->get('id'),
                'type_entity_id' => 2,
                'timestamper_id' => null,
                'active' => true,
                'hierarchical_view' => true,
                'default_secure_data_space_id' => 1,
                'is_main_archival_agency' => true,
            ]
        );
        $this->assertFalse($this->OrgEntities->save($sa));
        $this->assertNotEmpty($sa->getError('timestamper_id'));

        $sa = $this->OrgEntities->newEntity(
            [
                'name' => 'sa',
                'identifier' => 'sa',
                'parent_id' => $se->get('id'),
                'type_entity_id' => 2,
                'timestamper_id' => 1,
                'active' => true,
                'hierarchical_view' => true,
                'default_secure_data_space_id' => null,
                'is_main_archival_agency' => true,
            ]
        );
        $this->assertFalse($this->OrgEntities->save($sa));
        $this->assertNotEmpty($sa->getError('default_secure_data_space_id'));

        $this->OrgEntities->patchEntity(
            $sa,
            [
                'default_secure_data_space_id' => 1,
                'is_main_archival_agency' => true,
            ] + $sa->toArray()
        );
        $this->assertNotFalse($this->OrgEntities->save($sa));
        $sa = $this->OrgEntities->find()
            ->where(['OrgEntities.id' => $sa->get('id')])
            ->contain(['ArchivalAgencies'])
            ->firstOrFail();
        $this->assertEquals($sa->get('id'), Hash::get($sa, 'archival_agency.id'));

        // test modification type autre (prod -> versant)
        $spSv = $this->OrgEntities->newEntity(
            [
                'name' => 'test_prod_vers',
                'identifier' => 'test_prod_vers',
                'parent_id' => 2,
                'type_entity_id' => 5, // SP
                'timestamper_id' => null,
                'active' => true,
                'hierarchical_view' => true,
                'default_secure_data_space_id' => null,
                'is_main_archival_agency' => null,
            ]
        );
        $this->assertNotFalse($this->OrgEntities->save($spSv));
        $spSv->set('type_entity_id', 4); // SV
        $this->assertNotFalse($this->OrgEntities->save($spSv));

        // test modification type versant (versant -> prod)
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $transfer = $Transfers->newEntity(
            [
                'transfer_date' => '2018-04-25 09:34:15.000000',
                'transfer_identifier' => uniqid(),
                'archival_agency_id' => 2,
                'transferring_agency_id' => $spSv->id,
                'originating_agency_id' => $spSv->id,
                'message_version' => 'seda0.2',
                'state' => 'available',
                'is_modified' => false,
                'filename' => 'testfile.xml',
                'files_deleted' => false,
                'created_user_id' => 1,
            ]
        );
        $Transfers->saveOrFail($transfer);
        $spSv->set('type_entity_id', 5); // SP
        $this->assertFalse($this->OrgEntities->save($spSv));

        // force le changement de type d'entité pour le prochain test
        $this->OrgEntities->updateAll(
            ['type_entity_id' => 5],
            ['id' => $spSv->id]
        );
        $spSv = $this->OrgEntities->get($spSv->id);

        // test modification type versant (prod -> autre (!= versant))
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $archive = $Archives->newEntity(
            [
                'state' => 'available',
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'originating_agency_id' => $spSv->id,
                'transfer_id' => $transfer->id,
                'access_rule_id' => 1,
                'appraisal_rule_id' => 1,
                'archival_agency_identifier' => $archiveIdentifier = uniqid(),
                'transferring_agency_identifier' => null,
                'originating_agency_identifier' => null,
                'secure_data_space_id' => 1,
                'storage_path' => implode(
                    '/',
                    [
                        'no_profile',
                        date('Y-m'),
                        urlencode($archiveIdentifier),
                    ]
                ),
                'name' => $archiveIdentifier,
                'original_size' => 0,
                'transferred_size' => 1,
                'management_size' => 0,
                'preservation_size' => 0,
                'dissemination_size' => 0,
                'timestamp_size' => 0,
                'transferred_count' => 1,
                'management_count' => 0,
                'original_count' => 1,
                'preservation_count' => 0,
                'dissemination_count' => 0,
                'timestamp_count' => 0,
                'xml_node_tagname' => 'Archive[1]',
            ]
        );
        $Archives->saveOrFail($archive);

        $spSv->set('type_entity_id', 7); // OV
        $this->assertFalse($this->OrgEntities->save($spSv));

        // On peut transformer un producteur en versant par contre
        $spSv->set('type_entity_id', 4); // SV
        $this->assertNotFalse($this->OrgEntities->save($spSv));
    }

    /**
     * testCst
     */
    public function testCst()
    {
        $cst = $this->OrgEntities->cst(2);
        $this->assertEquals('sa', $cst->get('identifier'));

        $orgEntity = $this->OrgEntities->newEntity(
            [
                'name' => 'cst',
                'identifier' => 'cst',
                'parent_id' => 2,
                'type_entity_id' => 3, // CST
                'active' => true,
            ]
        );
        $this->OrgEntities->saveOrFail($orgEntity);
        $cst = $this->OrgEntities->cst(2);
        $this->assertEquals('cst', $cst->get('identifier'));
        $this->assertEquals($orgEntity->id, $cst->id);
    }
}
