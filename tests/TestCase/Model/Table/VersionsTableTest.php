<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\VersionsTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use DateTime;

class VersionsTableTest extends TestCase
{
    public array $fixtures = [
        'app.Phinxlog',
        'app.Versions',
    ];

    /**
     * testdeleteAllSerialize
     */
    public function testdeleteAllSerialize()
    {
        /** @var VersionsTable $table */
        $table = TableRegistry::getTableLocator()->get('Versions');
        $this->assertEmpty($table->deleteAllSerialize([]));
    }

    /**
     * testinsertMissingVersions
     */
    public function testinsertMissingVersions()
    {
        $Phinxlog = TableRegistry::getTableLocator()->get('Phinxlog');
        $Phinxlog->saveOrFail(
            $Phinxlog->newEntity(
                [
                    'version' => '20220215104711',
                    'migration_name' => 'Patch215',
                    'breakpoint' => false,
                    'start_time' => new DateTime(),
                    'end_time' => new DateTime(),
                ]
            )
        );
        /** @var VersionsTable $versions */
        $versions = TableRegistry::getTableLocator()->get('Versions');
        $versions->deleteQuery()
            ->where(['version' => '2.1.5'])
            ->execute();
        $this->assertFalse($versions->exists(['version' => '2.1.5']));
        $versions->insertMissingVersions();
        $this->assertTrue($versions->exists(['version' => '2.1.5']));
    }
}
