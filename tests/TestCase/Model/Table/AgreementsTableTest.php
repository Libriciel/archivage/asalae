<?php

namespace Asalae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

class AgreementsTableTest extends TestCase
{
    public array $fixtures = [
        'app.Agreements',
        'app.OrgEntities',
    ];

    /**
     * testValidationDefaultAgreement
     */
    public function testValidationDefaultAgreement(): void
    {
        $table = TableRegistry::getTableLocator()->get('Agreements');
        $table->updateAll(['default_agreement' => false], []);
        $entity = $table->newEntity(
            [
                'default_agreement' => false,
                'org_entity_id' => 2,
            ]
        );
        $this->assertNotEmpty($entity->getError('default_agreement'));

        $entity = $table->newEntity(
            [
                'default_agreement' => true,
                'active' => false,
            ]
        );
        $this->assertNotEmpty($entity->getError('default_agreement'));
    }
}
