<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Entity\TransferError;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationHistoriesTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Asalae\Model\Table\ValidationHistoriesTable Test Case
 */
class ValidationHistoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Asalae\Model\Table\ValidationHistoriesTable
     */
    public $ValidationHistories;

    /**
     * Fixtures
     *
     * @var array
     */
    public array $fixtures = [
        'app.ValidationHistories',
        'app.ValidationProcesses',
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationStages',
        'app.Transfers',
        'app.TransferErrors',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ValidationHistories') ? []
            : ['className' => ValidationHistoriesTable::class];
        $this->ValidationHistories = TableRegistry::getTableLocator()->get('ValidationHistories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ValidationHistories);

        parent::tearDown();
    }


    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $base = ['comment' => '', 'current_step' => 1];
        $history = $this->ValidationHistories->newEntity(['action' => 'validate'] + $base);
        $this->assertEmpty($history->getErrors());
        $history = $this->ValidationHistories->newEntity(['action' => 'bad-action'] + $base);
        $this->assertNotEmpty($history->getErrors());

        $history = $this->ValidationHistories->newEntity(['action' => 'stepback', 'prev' => false] + $base);
        $this->assertNotEmpty($history->getErrors());
        $history = $this->ValidationHistories->newEntity(['action' => 'stepback', 'prev' => true] + $base);
        $this->assertEmpty($history->getErrors());

        $history = $this->ValidationHistories->newEntity(
            [
                'validation_process_id' => 1,
                'action' => 'validate',
                'next' => false,
            ] + $base
        );
        $this->assertEmpty($history->getErrors());

        TableRegistry::getTableLocator()->get('TransferErrors')
            ->updateAll(['code' => TransferError::INVALIDATE_CODES[0]], []);
        $history = $this->ValidationHistories->newEntity(
            [
                'validation_process_id' => 1,
                'action' => 'validate',
                'next' => false,
            ]
        );
        $this->assertNotEmpty($history->getErrors());
    }

    /**
     * Test optionsAction method
     *
     * @return void
     * @throws Exception
     */
    public function testOptionsAction()
    {
        $this->assertCount(3, $this->ValidationHistories->optionsAction(true));
        $this->assertCount(2, $this->ValidationHistories->optionsAction(false));
        $this->assertCount(2, $this->ValidationHistories->optionsAction(true, false));
        $this->assertCount(1, $this->ValidationHistories->optionsAction(false, false));
    }

    /**
     * Test des fixtures/model que tout s'imbrique bien
     */
    public function testNestedFind()
    {
        $this->ValidationHistories->updateAll(['validation_actor_id' => 1], ['id' => 1]);
        TableRegistry::getTableLocator()->get('ValidationProcesses')
            ->updateAll(['current_stage_id' => 1], ['id' => 1]);
        $result = $this->ValidationHistories->find()
            ->contain(
                [
                    'ValidationProcesses' => [
                        'ValidationChains',
                        'CurrentStages',
                        'Transfers',
                    ],
                    'ValidationActors',
                ]
            )
            ->first();
        $actor = $result->get('validation_actor');
        $process = $result->get('validation_process');
        /** @var EntityInterface $stage */
        $stage = $process->get('current_stage');
        /** @var EntityInterface $chain */
        $chain = $process->get('validation_chain');
        $this->assertEquals('USER 1', $actor->get('app_type') . ' ' . $actor->get('app_foreign_key'));
        $this->assertEquals('Transfers 1', $process->get('app_subject_type') . ' ' . $process->get('app_foreign_key'));
        $this->assertNotEmpty($process->get('transfer'));
        $this->assertEquals('seda1.0', $process->get('transfer')->get('transfer_identifier'));
        $this->assertEquals('Etape 1 conformes', $stage->get('name'));
        $this->assertEquals(ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM, $chain->get('app_type'));
    }
}
