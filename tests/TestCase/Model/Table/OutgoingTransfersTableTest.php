<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\OutgoingTransferRequestsTable;
use Asalae\Model\Table\OutgoingTransfersTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

/**
 * Asalae\Model\Table\OutgoingTransfersTable Test Case
 */
class OutgoingTransfersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Asalae\Model\Table\OutgoingTransfersTable
     */
    public $OutgoingTransfers;

    /**
     * Fixtures
     *
     * @var array
     */
    public array $fixtures = [
        'app.ArchiveUnits',
        'app.ArchiveUnitsOutgoingTransferRequests',
        'app.Archives',
        'app.OutgoingTransferRequests',
        'app.OutgoingTransfers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OutgoingTransfers') ? []
            : ['className' => OutgoingTransfersTable::class];
        $this->OutgoingTransfers = TableRegistry::getTableLocator()->get('OutgoingTransfers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->OutgoingTransfers);
        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $outgoingTransfer = $this->OutgoingTransfers->newEntity(
            [
                'identifier' => 'test',
                'outgoing_transfer_request_id' => 1,
                'archive_unit_id' => 1,
                'state' => $this->OutgoingTransfers->initialState,
            ]
        );
        $this->assertEmpty($outgoingTransfer->getErrors());

        $this->assertNotFalse($this->OutgoingTransfers->save($outgoingTransfer));
        $outgoingTransfer = $this->OutgoingTransfers->newEntity(
            [
                'identifier' => '',
            ]
        );
        $this->assertNotEmpty($outgoingTransfer->getErrors());
    }

    /**
     * testAfterDelete
     */
    public function testAfterDelete()
    {
        $outgoingTransfer = $this->OutgoingTransfers->get(1);
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $ArchiveUnits->updateAll(['state' => ArchiveUnitsTable::S_RESTITUTING], ['id' => 5]);

        // rend le transfert sortant supprimable
        $outgoingTransfer->set('state', OutgoingTransfersTable::S_ERROR);
        $outgoingTransfer->set(
            'outgoing_transfer_request',
            ['state' => OutgoingTransferRequestsTable::S_TRANSFERS_ERRORS]
        );

        $this->OutgoingTransfers->delete($outgoingTransfer);
        $this->assertEquals(
            ArchiveUnitsTable::S_AVAILABLE,
            $ArchiveUnits->get(5)->get('state')
        );
    }
}
