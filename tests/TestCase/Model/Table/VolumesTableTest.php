<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\VolumesTable;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Libriciel\Filesystem\Utility\Filesystem;

class VolumesTableTest extends TestCase
{
    use AutoFixturesTrait;

    /**
     * Test subject
     *
     * @var VolumesTable
     */
    public $Volumes;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        if (is_dir(TMP_VOL1)) {
            chmod(TMP_VOL1, 0777);
            Filesystem::remove(TMP_VOL1);
        }
        Filesystem::mkdir(TMP_VOL1);

        $this->Volumes = TableRegistry::getTableLocator()->get('Volumes');
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }

        unset($this->Volumes);
    }

    /**
     * testValidationDefault
     */
    public function testValidationDefault()
    {
        $volume = $this->Volumes->newEntity(
            [
                'name' => 'testvol',
                'fields' => json_encode(['path' => 'folder']),
                'driver' => 'FILESYSTEM',
            ]
        );
        $this->assertEmpty($volume->getErrors());

        $this->assertNotFalse($this->Volumes->save($volume));
        $volume = $this->Volumes->newEntity(
            [
                'name' => '',
                'fields' => '',
                'driver' => '',
            ]
        );
        $this->assertNotEmpty($volume->getErrors());
    }

    /**
     * testCalcReadDuration
     */
    public function testCalcReadDuration()
    {
        $this->Volumes->updateAll(
            ['read_duration' => 0],
            ['id' => 1]
        );
        $volume = $this->Volumes->get(1);
        $this->Volumes->calcReadDuration($volume);
        $volume = $this->Volumes->get(1);
        $this->assertNotEquals(0, $volume->get('read_duration'));
    }

    /**
     * testFindUnreferencedFiles
     */
    public function testFindUnreferencedFiles()
    {
        $this->assertEmpty($this->Volumes->findUnreferencedFiles(1));
        Filesystem::createDummyFile(TMP_VOL1 . DS . 'testfile', 10);
        $this->assertEquals(
            ['testfile'],
            $this->Volumes->findUnreferencedFiles(1, true)
        );
        $this->assertEmpty($this->Volumes->findUnreferencedFiles(1));
    }
}
