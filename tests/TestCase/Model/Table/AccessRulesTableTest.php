<?php

namespace Asalae\Test\TestCase\Model\Table;

use AsalaeCore\I18n\Date;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use PDOException;

class AccessRulesTableTest extends TestCase
{
    public array $fixtures = [
        'app.AccessRules',
        'app.AccessRuleCodes',
        'app.Mediainfos',
        'app.Siegfrieds',
    ];

    /**
     * testBeforeSave
     */
    public function testBeforeSave()
    {
        $table = TableRegistry::getTableLocator()->get('AccessRules');
        $start = new Date();
        $entity = $table->newEntity(
            ['start_date' => $start, 'access_rule_code_id' => 25],
            ['validate' => false]
        );// code 25 = AR062
        $this->assertNotFalse($table->save($entity));
        $entity = $table->get($entity->id);
        $end = $entity->get('end_date');
        $this->assertNotNull($entity->get('end_date'));
        $this->assertInstanceOf(Date::class, $end);
        $end = new Date($end->format('Y-m-d') . ' 00:00:00');
        $this->assertGreaterThan($start, $end);

        $entity->set('access_rule_code_id', 3); // AR040
        $this->assertNotFalse($table->save($entity));
        $this->assertLessThan($end, $entity->get('end_date'));

        $entity->set('access_rule_code_id');
        $this->expectException(PDOException::class);
        $table->save($entity);
    }
}
