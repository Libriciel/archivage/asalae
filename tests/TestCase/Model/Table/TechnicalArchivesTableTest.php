<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\TechnicalArchivesTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

/**
 * Asalae\Model\Table\TechnicalArchivesTable Test Case
 */
class TechnicalArchivesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Asalae\Model\Table\TechnicalArchivesTable
     */
    public $TechnicalArchives;

    /**
     * Fixtures
     *
     * @var array
     */
    public array $fixtures = [
        'app.TechnicalArchives',
        'app.OrgEntities',
        'app.SecureDataSpaces',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TechnicalArchives') ? []
            : ['className' => TechnicalArchivesTable::class];
        $this->TechnicalArchives = TableRegistry::getTableLocator()->get('TechnicalArchives', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TechnicalArchives);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $entity = $this->TechnicalArchives->newEntity(
            [
                'name' => 'test',
                'state' => $this->TechnicalArchives->initialState,
                'type' => TechnicalArchivesTable::TYPE_EVENTS_LOGS,
                'archival_agency_identifier' => '1234',
                'storage_path' => 'foo/bar',
                'original_size' => 0,
                'original_count' => 0,
                'timestamp_size' => 0,
                'timestamp_count' => 0,
                'management_size' => 0,
                'management_count' => 0,
            ]
        );
        $this->assertEmpty($entity->getErrors());
    }
}
