<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\LifecycleXmlArchiveFilesTable;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use DOMDocument;
use Libriciel\Filesystem\Utility\Filesystem;

class LifecycleXmlArchiveFilesTableTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.ArchiveFiles',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testPaginate
     */
    public function testPaginate()
    {
        /** @var LifecycleXmlArchiveFilesTable $LifecycleXmlArchiveFiles */
        $LifecycleXmlArchiveFiles = TableRegistry::getTableLocator()->get('LifecycleXmlArchiveFiles');
        $dom = new DOMDocument();
        $dom->loadXML(VolumeSample::getSampleContent('sample_premis.xml'));
        $results = $LifecycleXmlArchiveFiles->paginate(
            $dom,
            ['format identification']
        );
        $this->assertNotEmpty($results);
    }

    /**
     * testCountTypes
     */
    public function testCountTypes()
    {
        /** @var LifecycleXmlArchiveFilesTable $LifecycleXmlArchiveFiles */
        $LifecycleXmlArchiveFiles = TableRegistry::getTableLocator()->get('LifecycleXmlArchiveFiles');
        $dom = new DOMDocument();
        $dom->loadXML(VolumeSample::getSampleContent('sample_premis.xml'));
        $results = $LifecycleXmlArchiveFiles->countTypes(
            $dom,
            ['format identification']
        );
        $this->assertEquals(1, $results);
    }
}
