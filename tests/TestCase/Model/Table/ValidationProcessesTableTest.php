<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Entity\ValidationHistory;
use Asalae\Model\Table\ValidationProcessesTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;

class ValidationProcessesTableTest extends TestCase
{
    public array $fixtures = [
        'app.Transfers',
        'app.ValidationChains',
        'app.ValidationStages',
        'app.ValidationActors',
        'app.ValidationProcesses',
        'app.ValidationHistories',
        'app.Agreements',
    ];

    /**
     * Test des fixtures/model que tout s'imbrique bien
     */
    public function testProcess()
    {
        $Histories = TableRegistry::getTableLocator()->get('ValidationHistories');
        $Histories->updateAll(['validation_actor_id' => 1, 'current_step' => 1], ['id' => 1]);
        /** @var ValidationProcessesTable $Processes */
        $Processes = TableRegistry::getTableLocator()->get('ValidationProcesses');
        $Processes->updateAll(
            ['current_stage_id' => 1, 'current_step' => 1, 'processed' => false, 'validated' => null],
            ['id' => 1]
        );

        /** @var EntityInterface $process */
        $process = $Processes->find()
            ->contain(['CurrentStages' => ['ValidationActors']])
            ->orderBy(['ValidationProcesses.id' => 'asc'])
            ->first();
        $this->assertNotNull($process);
        $this->assertNotNull($process->get('current_stage'));
        $this->assertNotNull($process->get('current_stage')->get('validation_actors'));

        /** @var EntityInterface $actor1 */
        $actor1 = $process->get('current_stage')->get('validation_actors')[0];

        /** @var ValidationHistory $history */
        $history = $Histories->newEntity(
            [
                'validation_process_id' => $process->get('id'),
                'validation_actor_id' => $actor1->get('id'),
                'action' => 'validate',
                'comment' => 'test',
            ]
        );
        $oldProcess = $process->toArray();
        $process = $Processes->validate($history);

        $this->assertEquals(1, $oldProcess['current_step']);
        $this->assertEquals(2, $process->get('current_step'));
        $this->assertEquals(2, $history->get('current_step'));
        $this->assertNotEmpty($Histories->save($history));
        $this->assertNotEmpty($Processes->save($process));
        $this->assertEquals(1, $oldProcess['current_stage_id']);
        $this->assertEquals(2, $process->get('current_stage_id'));

        // Récupération du nouvel acteur (et vérification en base)
        $process = $Processes->find()
            ->contain(['CurrentStages' => ['ValidationActors']])
            ->orderBy(['ValidationProcesses.id' => 'asc'])
            ->first();
        $this->assertNotNull($process);
        $this->assertNotNull($process->get('current_stage'));
        $this->assertNotNull($process->get('current_stage')->get('validation_actors'));

        /** @var EntityInterface $actor2 */
        $actor2 = $process->get('current_stage')->get('validation_actors')[0];

        $history = $Histories->newEntity(
            [
                'validation_process_id' => $process->get('id'),
                'validation_actor_id' => $actor2->get('id'),
                'action' => 'stepback',
                'comment' => 'test',
            ]
        );
        $oldProcess = $process->toArray();
        $process = $Processes->stepback($history);

        $this->assertEquals(2, $oldProcess['current_step']);
        $this->assertEquals(3, $process->get('current_step'));
        $this->assertEquals(3, $history->get('current_step'));
        $this->assertNotEmpty($Histories->save($history));
        $this->assertNotEmpty($Processes->save($process));
        $this->assertEquals(2, $oldProcess['current_stage_id']);
        $this->assertEquals(1, $process->get('current_stage_id'));

        $history = $Histories->newEntity(
            [
                'validation_process_id' => $process->get('id'),
                'validation_actor_id' => $actor1->get('id'),
                'action' => 'invalidate',
                'comment' => 'test',
            ]
        );
        $oldProcess = $process->toArray();
        $process = $Processes->invalidate($history);

        $this->assertNotEmpty($Histories->save($history));
        $this->assertNotEmpty($Processes->save($process));
        $this->assertNull($process->get('current_stage_id'));
        $this->assertEquals(4, $process->get('current_step'));
        $this->assertFalse($oldProcess['processed']);
        $this->assertTrue($process->get('processed'));
        $this->assertNull($oldProcess['validated']);
        $this->assertFalse($process->get('validated'));
    }

    /**
     * testCreateNewProcess
     */
    public function testCreateNewProcess()
    {
        /** @var ValidationProcessesTable $Processes */
        $Processes = TableRegistry::getTableLocator()->get('ValidationProcesses');
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $params = [
            'org_entity_id' => 2,
            'valid' => true,
        ];
        $process = $Processes->createNewProcess($Transfers->get(1), $params);
        $this->assertNotEmpty($process->get('validation_chain_id'));
        $Agreements->updateAll(['auto_validate' => false], []);
        $process = $Processes->createNewProcess($Transfers->get(1), $params);
        $this->assertNotEmpty($process->get('validation_chain_id'));
        $params = [
            'org_entity_id' => 2,
            'valid' => false,
        ];
        $process = $Processes->createNewProcess($Transfers->get(1), $params);
        $this->assertNotEmpty($process->get('validation_chain_id'));
    }
}
