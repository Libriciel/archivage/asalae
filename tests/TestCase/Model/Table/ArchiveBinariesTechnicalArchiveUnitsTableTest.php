<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\ArchiveBinariesTechnicalArchiveUnitsTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

/**
 * Asalae\Model\Table\ArchiveBinariesTechnicalArchiveUnitsTable Test Case
 */
class ArchiveBinariesTechnicalArchiveUnitsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Asalae\Model\Table\ArchiveBinariesTechnicalArchiveUnitsTable
     */
    public $ArchiveBinariesTechnicalArchiveUnits;

    /**
     * Fixtures
     *
     * @var array
     */
    public array $fixtures = [
        'app.ArchiveBinariesTechnicalArchiveUnits',
        'app.ArchiveBinaries',
        'app.TechnicalArchiveUnits',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ArchiveBinariesTechnicalArchiveUnits') ? []
            : ['className' => ArchiveBinariesTechnicalArchiveUnitsTable::class];
        $this->ArchiveBinariesTechnicalArchiveUnits = TableRegistry::getTableLocator()->get(
            'ArchiveBinariesTechnicalArchiveUnits',
            $config
        );
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ArchiveBinariesTechnicalArchiveUnits);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $entity = $this->ArchiveBinariesTechnicalArchiveUnits->newEntity([], ['validate' => false]);
        $this->assertEmpty($entity->getErrors());
    }
}
