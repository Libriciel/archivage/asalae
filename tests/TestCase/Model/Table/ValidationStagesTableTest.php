<?php

namespace Asalae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\TestCase;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;

class ValidationStagesTableTest extends TestCase
{
    public array $fixtures = [
        'app.ValidationChains',
        'app.ValidationStages',
    ];

    /**
     * testNewEntry
     */
    public function testNewEntry()
    {
        $Stages = TableRegistry::getTableLocator()->get('ValidationStages');
        $prev = $Stages->find()
            ->where(['validation_chain_id' => 1])
            ->orderBy(['ord' => 'desc'])
            ->firstOrFail()
            ->get('ord');
        $stage6 = $Stages->newEntity(
            [
                'validation_chain_id' => 1,
                'name' => 'Stage 6',
                'description' => 'For testing purposes',
                'all_actors_to_complete' => false,
                'app_type' => 'DEFAULT',
                'app_meta' => '{"created_user_id": 1, "modifieds": [{"user_id": 1, "date": "2018-05-11 10:41:31"}]}',
            ]
        );
        $this->assertNotFalse($Stages->save($stage6));
        $this->assertEquals($prev + 1, $stage6->get('ord'));
    }

    /**
     * testChangeOrd
     */
    public function testChangeOrd()
    {
        $Stages = TableRegistry::getTableLocator()->get('ValidationStages');

        $actual = [];
        $query = $Stages->find()->where(['validation_chain_id' => 1])->orderBy(['ord' => 'asc']);
        /** @var EntityInterface $stage */
        foreach (clone $query as $stage) {
            $actual[] = sprintf("%s(%d)", $stage->get('name'), $stage->get('ord'));
        }
        $this->assertEquals('Etape 1 conformes(1), Etape 2 conformes(2)', implode(', ', $actual));

        // On affecte la position 4 au Stage 2, les numéros entre 2 et 4 se décalent d'un cran
        $stage2 = $Stages->find()->where(['id' => 2])->first()->set('ord', 4);
        $this->assertNotFalse($Stages->save($stage2));

        $new = [];
        foreach (clone $query as $stage) {
            $new[] = sprintf("%s(%d)", $stage->get('name'), $stage->get('ord'));
        }
        $this->assertEquals('Etape 1 conformes(1), Etape 2 conformes(4)', implode(', ', $new));

        // On affecte la position 2 au Stage 2, ce qui remet tout comme au début
        $stage2->set('ord', 2);
        $this->assertNotFalse($Stages->save($stage2));

        $new2 = [];
        foreach (clone $query as $stage) {
            $new2[] = sprintf("%s(%d)", $stage->get('name'), $stage->get('ord'));
        }

        $this->assertEquals(implode(', ', $actual), implode(', ', $new2));
    }

    /**
     * testNewEntryWithOrd
     */
    public function testNewEntryWithOrd()
    {
        $Stages = TableRegistry::getTableLocator()->get('ValidationStages');
        $stage6 = $Stages->newEntity(
            [
                'validation_chain_id' => 1,
                'name' => 'Stage 6',
                'description' => 'For testing purposes',
                'ord' => 3,
                'all_actors_to_complete' => false,
                'app_type' => 'DEFAULT',
                'app_meta' => '{"created_user_id": 1, "modifieds": [{"user_id": 1, "date": "2018-05-11 10:41:31"}]}',
            ]
        );
        $this->assertNotFalse($Stages->save($stage6));
        $new = [];
        $query = $Stages->find()->where(['validation_chain_id' => 1])->orderBy(['ord' => 'asc']);
        /** @var EntityInterface $stage */
        foreach ($query as $stage) {
            $new[] = sprintf("%s(%d)", $stage->get('name'), $stage->get('ord'));
        }
        // Le stage 6 s'est inséré en position 3 et a poussé les positions supérieures d'un cran
        $this->assertEquals('Etape 1 conformes(1), Etape 2 conformes(2), Stage 6(3)', implode(', ', $new));
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $Stages = TableRegistry::getTableLocator()->get('ValidationStages');
        $stage2 = $Stages->find()->where(['id' => 2])->first();
        $this->assertNotFalse($Stages->delete($stage2));

        $query = $Stages->find()->where(['validation_chain_id' => 1])->orderBy(['ord' => 'asc']);
        $new = [];
        /** @var EntityInterface $stage */
        foreach ($query as $stage) {
            $new[] = sprintf("%s(%d)", $stage->get('name'), $stage->get('ord'));
        }
        // Le stage 2 supprimé décale les positions supérieures d'un cran
        $this->assertEquals('Etape 1 conformes(1)', implode(', ', $new));
    }
}
