<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\TestCase;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use DOMDocument;

class ArchiveUnitsTableTest extends TestCase
{
    public array $fixtures = [
        'app.Archives',
        'app.ArchiveUnits',
        'app.AccessRules',
        'app.AppraisalRules',
        'app.ArchiveDescriptions',
        'app.ArchiveKeywords',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testfindArchiveUnitByDOMElement
     */
    public function testfindArchiveUnitByDOMElement()
    {
        $archive1 = TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml';
        $dom = new DOMDocument();
        $dom->load($archive1);
        $element = $dom->getElementsByTagName('Archive')->item(0);

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $au = $ArchiveUnits->findArchiveUnitByDOMElement(1, $element);
        $this->assertInstanceOf(EntityInterface::class, $au);
        $this->assertEquals(1, $au->id);

        // On ajoute de la profondeur pour tester les jointures
        $newNode = $dom->createElementNS(NAMESPACE_SEDA_10, 'ArchiveObject');
        $newNodeName = $dom->createElementNS(NAMESPACE_SEDA_10, 'Name', 'test lv1-1');
        $newNode->appendChild($newNodeName);
        $element->appendChild($newNode);
        $newArchiveUnit = $ArchiveUnits->newEntity(
            [
                'state' => $ArchiveUnits->initialState,
                'archive_id' => 1,
                'access_rule_id' => 1,
                'appraisal_rule_id' => 1,
                'archival_agency_identifier' => 'test1',
                'name' => 'test lv1-1',
                'original_local_size' => 0,
                'original_total_size' => 0,
                'parent_id' => 1,
                'xml_node_tagname' => 'ArchiveObject[1]',
                'original_total_count' => 0,
                'original_local_count' => 1,
                'expired_dua_root' => false,
            ]
        );
        $ArchiveUnits->saveOrFail($newArchiveUnit);
        $au2 = $ArchiveUnits->findArchiveUnitByDOMElement(1, $newNode);
        $this->assertInstanceOf(EntityInterface::class, $au2);
        $this->assertEquals($newArchiveUnit->id, $au2->id);

        // On ajoute un 2e noeud pour tester le xpath
        $newNode2 = $dom->createElementNS(NAMESPACE_SEDA_10, 'ArchiveObject');
        $newNodeName2 = $dom->createElementNS(NAMESPACE_SEDA_10, 'Name', 'test lv1-2');
        $newNode2->appendChild($newNodeName2);
        $element->appendChild($newNode2);
        $newArchiveUnit2 = $ArchiveUnits->newEntity(
            [
                'state' => $ArchiveUnits->initialState,
                'archive_id' => 1,
                'access_rule_id' => 1,
                'appraisal_rule_id' => 1,
                'archival_agency_identifier' => 'test2',
                'name' => 'test lv1-2',
                'original_local_size' => 0,
                'original_total_size' => 0,
                'parent_id' => 1,
                'xml_node_tagname' => 'ArchiveObject[2]',
                'original_total_count' => 0,
                'original_local_count' => 1,
                'expired_dua_root' => false,
            ]
        );
        $ArchiveUnits->saveOrFail($newArchiveUnit2);
        $au3 = $ArchiveUnits->findArchiveUnitByDOMElement(1, $newNode2);
        $this->assertInstanceOf(EntityInterface::class, $au3);
        $this->assertEquals($newArchiveUnit2->id, $au3->id);

        // on ajoute un autre niveau de profondeur pour être sûr
        $newNode3 = $dom->createElementNS(NAMESPACE_SEDA_10, 'ArchiveObject');
        $newNodeName3 = $dom->createElementNS(NAMESPACE_SEDA_10, 'Name', 'test lv2');
        $newNode3->appendChild($newNodeName3);
        $newNode2->appendChild($newNode3);
        $newArchiveUnit3 = $ArchiveUnits->newEntity(
            [
                'state' => $ArchiveUnits->initialState,
                'archive_id' => 1,
                'access_rule_id' => 1,
                'appraisal_rule_id' => 1,
                'archival_agency_identifier' => 'test3',
                'name' => 'test lv2',
                'original_local_size' => 0,
                'original_total_size' => 0,
                'parent_id' => $newArchiveUnit2->id,
                'xml_node_tagname' => 'ArchiveObject[1]',
                'original_total_count' => 0,
                'original_local_count' => 1,
                'expired_dua_root' => false,
            ]
        );
        $ArchiveUnits->saveOrFail($newArchiveUnit3);
        $au4 = $ArchiveUnits->findArchiveUnitByDOMElement(1, $newNode3);
        $this->assertInstanceOf(EntityInterface::class, $au4);
        $this->assertEquals($newArchiveUnit3->id, $au4->id);
    }

    /**
     * testfindArchiveUnitByDOMElementSeda21
     */
    public function testfindArchiveUnitByDOMElementSeda21()
    {
        $archive1 = TMP_VOL1 . '/sp/profile1/2021-12/sa_2/management_data/sa_2_description.xml';
        $dom = new DOMDocument();
        $dom->load($archive1);
        $element = $dom->getElementsByTagName('ArchiveUnit')->item(0);

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $au = $ArchiveUnits->findArchiveUnitByDOMElement(2, $element);
        $this->assertInstanceOf(EntityInterface::class, $au);
        $this->assertEquals(3, $au->id);

        // On ajoute de la profondeur pour tester les jointures
        $newNode = $dom->createElementNS(NAMESPACE_SEDA_21, 'ArchiveUnit');
        $newNodeContent = $dom->createElementNS(NAMESPACE_SEDA_21, 'Content');
        $newNodeTitle = $dom->createElementNS(NAMESPACE_SEDA_21, 'Title', 'test lv1-1');
        $newNode->appendChild($newNodeContent);
        $newNodeContent->appendChild($newNodeTitle);
        $element->appendChild($newNode);
        $newArchiveUnit = $ArchiveUnits->newEntity(
            [
                'state' => $ArchiveUnits->initialState,
                'archive_id' => 2,
                'access_rule_id' => 1,
                'appraisal_rule_id' => 1,
                'archival_agency_identifier' => 'test1',
                'name' => 'test lv1-1',
                'original_local_size' => 0,
                'original_total_size' => 0,
                'parent_id' => 3,
                'xml_node_tagname' => 'ArchiveUnit[1]',
                'original_total_count' => 0,
                'original_local_count' => 1,
                'expired_dua_root' => false,
            ]
        );
        $ArchiveUnits->saveOrFail($newArchiveUnit);
        $au2 = $ArchiveUnits->findArchiveUnitByDOMElement(2, $newNode);
        $this->assertInstanceOf(EntityInterface::class, $au2);
        $this->assertEquals($newArchiveUnit->id, $au2->id);

        // On ajoute un 2e noeud pour tester le xpath
        $newNode2 = $dom->createElementNS(NAMESPACE_SEDA_21, 'ArchiveUnit');
        $newNodeContent2 = $dom->createElementNS(NAMESPACE_SEDA_21, 'Content');
        $newNodeTitle2 = $dom->createElementNS(NAMESPACE_SEDA_21, 'Title', 'test lv1-1');
        $newNode2->appendChild($newNodeContent2);
        $newNodeContent2->appendChild($newNodeTitle2);
        $element->appendChild($newNode2);
        $newArchiveUnit2 = $ArchiveUnits->newEntity(
            [
                'state' => $ArchiveUnits->initialState,
                'archive_id' => 2,
                'access_rule_id' => 1,
                'appraisal_rule_id' => 1,
                'archival_agency_identifier' => 'test2',
                'name' => 'test lv1-2',
                'original_local_size' => 0,
                'original_total_size' => 0,
                'parent_id' => 3,
                'xml_node_tagname' => 'ArchiveUnit[2]',
                'original_total_count' => 0,
                'original_local_count' => 1,
                'expired_dua_root' => false,
            ]
        );
        $ArchiveUnits->saveOrFail($newArchiveUnit2);
        $au3 = $ArchiveUnits->findArchiveUnitByDOMElement(2, $newNode2);
        $this->assertInstanceOf(EntityInterface::class, $au3);
        $this->assertEquals($newArchiveUnit2->id, $au3->id);

        // on ajoute un autre niveau de profondeur pour être sûr
        $newNode3 = $dom->createElementNS(NAMESPACE_SEDA_21, 'ArchiveUnit');
        $newNodeContent3 = $dom->createElementNS(NAMESPACE_SEDA_21, 'Content');
        $newNodeTitle3 = $dom->createElementNS(NAMESPACE_SEDA_21, 'Title', 'test lv2');
        $newNode3->appendChild($newNodeContent3);
        $newNodeContent3->appendChild($newNodeTitle3);
        $newNode2->appendChild($newNode3);
        $newArchiveUnit3 = $ArchiveUnits->newEntity(
            [
                'state' => $ArchiveUnits->initialState,
                'archive_id' => 2,
                'access_rule_id' => 1,
                'appraisal_rule_id' => 1,
                'archival_agency_identifier' => 'test3',
                'name' => 'test lv2',
                'original_local_size' => 0,
                'original_total_size' => 0,
                'parent_id' => $newArchiveUnit2->id,
                'xml_node_tagname' => 'ArchiveUnit[1]',
                'original_total_count' => 0,
                'original_local_count' => 1,
                'expired_dua_root' => false,
            ]
        );
        $ArchiveUnits->saveOrFail($newArchiveUnit3);
        $au4 = $ArchiveUnits->findArchiveUnitByDOMElement(2, $newNode3);
        $this->assertInstanceOf(EntityInterface::class, $au4);
        $this->assertEquals($newArchiveUnit3->id, $au4->id);
    }

    /**
     * @return void
     */
    public function testCalc(): void
    {
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $this->fetchTable('ArchiveUnits');
        $ArchiveKeywords = $this->fetchTable('ArchiveKeywords');
        $AccessRules = $this->fetchTable('AccessRules');

        // Données initiales : (au=ArchiveUnit,ar=AccessRule,ad=description,ak=keyword)
        // au1 [ar1, ad[ar2]]
        //     - au2 [ar1 (test2: ak[ar3])]
        $AccessRules->updateAll(['end_date' => '2000-01-01'], ['id' => 1]);
        $AccessRules->updateAll(['end_date' => '2046-01-01'], ['id' => 2]);
        $AccessRules->updateAll(['end_date' => '2073-01-01'], ['id' => 3]);
        $ArchiveUnits->updateLinksToAccessRules($ArchiveUnits->get(1));
        $ArchiveUnits->updateLinksToAccessRules($ArchiveUnits->get(2));

        $ArchiveUnits->calcMaxAggregatedAccessEndDate();
        $year1 = $ArchiveUnits->get(1)->get('max_aggregated_access_end_date')->format('Y');
        $year2 = $ArchiveUnits->get(2)->get('max_aggregated_access_end_date')->format('Y');
        $this->assertEquals('2046', $year1);
        $this->assertEquals('2000', $year2);

        // reset
        $ArchiveUnits->updateAll(['max_aggregated_access_end_date' => null], []);

        /**
         * Test de remonté de la date sur le parent, on ajoute un mot clé avec
         * un accès en 2073 sur l'unité d'archive (2) et son parent (1) doit
         * avoir la même date
         */
        $ArchiveKeywords->insertQuery()
            ->insert(['archive_unit_id', 'access_rule_id', 'content', 'xml_node_tagname'])
            ->values(
                [
                    'archive_unit_id' => 2,
                    'access_rule_id' => 3,
                    'content' => 'motclé',
                    'xml_node_tagname' => 'Keyword[1]',
                ]
            )
            ->execute();
        $ArchiveUnits->updateLinksToAccessRules($ArchiveUnits->get(1));
        $ArchiveUnits->updateLinksToAccessRules($ArchiveUnits->get(2));
        $ArchiveUnits->calcMaxAggregatedAccessEndDate();
        $year1 = $ArchiveUnits->get(1)->get('max_aggregated_access_end_date')->format('Y');
        $year2 = $ArchiveUnits->get(2)->get('max_aggregated_access_end_date')->format('Y');
        $this->assertEquals('2073', $year1);
        $this->assertEquals('2073', $year2);
    }
}
