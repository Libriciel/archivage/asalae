<?php

namespace Asalae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

class ArchiveBinariesArchiveUnitsTableTest extends TestCase
{
    public array $fixtures = [
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveUnits',
    ];

    /**
     * testValidation
     */
    public function testValidation()
    {
        $table = TableRegistry::getTableLocator()->get('ArchiveBinariesArchiveUnits');
        $entity = $table->newEntity([], ['validate' => false]);
        $this->assertEmpty($entity->getError('id'));
    }
}
