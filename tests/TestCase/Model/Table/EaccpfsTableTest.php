<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Table\EaccpfsTable;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Asalae\Model\Table\EaccpfsTable Test Case
 */
class EaccpfsTableTest extends TestCase
{
    use InvokePrivateTrait;

    /**
     * Test subject
     *
     * @var EaccpfsTable
     */
    public $Eaccpfs;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Eaccpfs') ? [] : ['className' => EaccpfsTable::class];
        $this->Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs', $config);
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Eaccpfs);
        parent::tearDown();
    }

    /**
     * Test translate method
     */
    public function testTranslate()
    {
        $strings = [
            "Namespace prefix foo for bar on source is not defined in Entity, line: 123",
            "Element 'foo', attribute 'bar': The attribute 'baz' is not allowed.",
        ];
        foreach ($strings as $str) {
            $this->assertNotEquals($str, $this->Eaccpfs->translate($str));
        }
        $noTrad = "no traduction for this string";
        $this->assertEquals($noTrad, $this->Eaccpfs->translate($noTrad));
    }
}
