<?php

namespace Asalae\Test\TestCase\Model\Table;

use Asalae\Model\Entity\Archive;
use Asalae\Model\Entity\ArchiveBinary;
use Asalae\Model\Entity\ArchiveFile;
use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\EventLogsTable;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

use const PREMIS_V3;

/**
 * Asalae\Model\Table\EventLogsTable Test Case
 */
class EventLogsTableTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_EVENT_LOGS,
        'app.AccessRules',
        'app.ArchiveBinariesArchiveUnits',
        'app.TypeEntities',
    ];
    /**
     * Test subject
     *
     * @var \Asalae\Model\Table\EventLogsTable
     */
    public $EventLogs;
    private $debugFixtures = false;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('EventLogs') ? [] : ['className' => EventLogsTable::class];
        $this->EventLogs = TableRegistry::getTableLocator()->get('EventLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->EventLogs);

        parent::tearDown();
    }

    /**
     * testInitialize
     */
    public function testInitialize()
    {
        $loc = TableRegistry::getTableLocator();
        $event = $this->EventLogs->newEntry(
            'testunit',
            'success',
            'this is a test',
            $loc->get('Users')->find()->first(),
            $loc->get('Transfers')->find()->first()
        );
        $expected = [
            'type' => 'testunit',
            'outcome' => 'success',
            'outcome_detail' => 'this is a test',
            'exported' => false,
            'user_id' => 1,
            'object_model' => 'Transfers',
            'object_foreign_key' => 1,
            'archival_agency_id' => 2,
        ];
        $this->assertEquals($expected, $event->toArray());
    }

    /**
     * testExport
     */
    public function testExport()
    {
        // note: pas de fixtures dans ArchiveKeywords, donc on crée une entrée
        $ArchiveKeywords = TableRegistry::getTableLocator()->get('ArchiveKeywords');
        $keyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => 1,
                'content' => 'test',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $ArchiveKeywords->saveOrFail($keyword);

        $this->EventLogs->deleteAll([]);
        $event = $this->EventLogs->newEntity(
            [
                'type' => 'test transfer',
                'outcome' => 'success',
                'outcome_detail' => 'this is a test',
                'exported' => false,
                'user_id' => 1,
                'object_model' => 'Transfers',
                'object_foreign_key' => 1,
            ]
        );
        $this->assertNotFalse($this->EventLogs->save($event));
        $event = $this->EventLogs->newEntity(
            [
                'type' => 'login',
                'outcome' => 'fail',
                'outcome_detail' => 'this is a test',
                'exported' => false,
                'user_id' => null,
                'object_model' => 'Versions',
                'object_foreign_key' => 1,
            ]
        );
        $this->assertNotFalse($this->EventLogs->save($event));
        $event = $this->EventLogs->newEntity(
            [
                'type' => 'test cron',
                'outcome' => 'some state',
                'outcome_detail' => 'this is a test',
                'exported' => false,
                'cron_id' => 1,
                'object_model' => 'Archives',
                'object_foreign_key' => 1,
            ]
        );
        $this->assertNotFalse($this->EventLogs->save($event));
        $event = $this->EventLogs->newEntity(
            [
                'type' => 'test cron same agent',
                'outcome' => 'some state',
                'outcome_detail' => 'this is a test',
                'exported' => false,
                'cron_id' => 1,
                'object_model' => 'Archives',
                'object_foreign_key' => 1,
            ]
        );
        $this->assertNotFalse($this->EventLogs->save($event));
        $event = $this->EventLogs->newEntity(
            [
                'type' => 'test cron archive keyword',
                'outcome' => 'some state',
                'outcome_detail' => 'this is a test',
                'exported' => false,
                'cron_id' => 1,
                'object_model' => 'ArchiveKeywords',
                'object_foreign_key' => 1,
            ]
        );
        $this->assertNotFalse($this->EventLogs->save($event));
        $premis = $this->EventLogs->export();
        $this->assertTrue($premis->schemaValidate(PREMIS_V3));

        // Archives:1 + Transfers:1 + Versions:1 + ArchiveKeyword:1
        $this->assertCount(4, $premis->getElementsByTagName('object'));
        // user_id:1 + cron_id:1
        $this->assertCount(2, $premis->getElementsByTagName('agent'));
        // 4 add(event) (1 agent et 1 objet par event, sauf pour la connexion loupée (1 agent en moins))
        $this->assertCount(5, $premis->getElementsByTagName('event'));
        $this->assertCount(4, $premis->getElementsByTagName('linkingAgentIdentifier'));
        $this->assertCount(5, $premis->getElementsByTagName('linkingObjectIdentifier'));

        // TODO test blockchain
    }

    /**
     * testaddArchivalAgency
     */
    public function testaddArchivalAgency()
    {
        // par user_id
        $entry = $this->EventLogs->newEntry(
            'test',
            'test',
            'test',
            1
        );
        $this->assertEquals(2, $entry->get('archival_agency_id'));

        // par object archive
        $entry = $this->EventLogs->newEntry(
            'test',
            'test',
            'test',
            null,
            new Archive(['id' => 1])
        );
        $this->assertEquals(2, $entry->get('archival_agency_id'));

        // par object archive file
        $entry = $this->EventLogs->newEntry(
            'test',
            'test',
            'test',
            null,
            new ArchiveFile(['id' => 1])
        );
        $this->assertEquals(2, $entry->get('archival_agency_id'));

        // par object archive binary
        $entry = $this->EventLogs->newEntry(
            'test',
            'test',
            'test',
            null,
            new ArchiveBinary(['id' => 1])
        );
        $this->assertEquals(2, $entry->get('archival_agency_id'));

        // par object transfer
        $entry = $this->EventLogs->newEntry(
            'test',
            'test',
            'test',
            null,
            new Transfer(['id' => 1])
        );
        $this->assertEquals(2, $entry->get('archival_agency_id'));
    }
}
