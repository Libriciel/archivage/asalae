<?php

namespace Asalae\Test\TestCase\Model\Volume;

use ArgumentCountError;
use Asalae\Model\Entity\Volume;
use Asalae\Model\Table\StoredFilesTable;
use Asalae\Model\Volume\VolumeManager;
use Asalae\Test\Mock\MockedVolume;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\ORM\Exception\PersistenceFailedException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const TMP_TESTDIR;
use const TMP_VOL1;
use const TMP_VOL2;

class VolumeManagerTest extends TestCase
{
    use InvokePrivateTrait;

    public array $fixtures = [
        'app.Volumes',
        'app.SecureDataSpaces',
        'app.StoredFiles',
        'app.StoredFilesVolumes',
        'app.ServiceLevels',
        'app.Archives',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        $dir = sys_get_temp_dir() . DS . 'testunit';
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        if (is_dir($dir = TMP_VOL1)) {
            Filesystem::remove($dir);
        }
        mkdir($dir);
        if (is_dir($dir = TMP_VOL2)) {
            Filesystem::remove($dir);
        }
        mkdir($dir);
        if (is_dir($dir = TMP_VOL3)) {
            Filesystem::remove($dir);
        }
        mkdir($dir);
        Configure::write('VolumeManager.retry', 1);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        $dir = sys_get_temp_dir() . DS . 'testunit';
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        if (is_dir($dir = TMP_VOL1)) {
            Filesystem::remove($dir);
        }
        if (is_dir($dir = TMP_VOL2)) {
            Filesystem::remove($dir);
        }
        Filesystem::setNamespace();
        Filesystem::reset();
    }

    /**
     * testFilePutContent
     */
    public function testFilePutContent()
    {
        $loc = TableRegistry::getTableLocator();
        $Volumes = $loc->get('Volumes');
        /** @var EntityInterface $volume1 */
        $volume1 = $Volumes->get(1);
        /** @var EntityInterface $volume2 */
        $volume2 = $Volumes->get(2);
        $prevDiskUsage1 = $volume1->get('disk_usage');
        $prevDiskUsage2 = $volume2->get('disk_usage');

        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');
        $volume1 = $Volumes->get(1);
        $volume2 = $Volumes->get(2);
        $this->assertEquals($prevDiskUsage1 + 3, $volume1->get('disk_usage'));
        $this->assertEquals($prevDiskUsage2 + 3, $volume2->get('disk_usage'));
        $this->assertEquals('foo', $manager->fileGetContent('test.txt'));

        // cas plantage partiel
        $driver = new VolumeFilesystem(
            [
                'path' => TMP_VOL1,
                'volume_entity' => $volume1,
            ]
        );
        $driver->fileDelete('test.txt');
        $this->assertFileDoesNotExist(TMP_VOL1 . DS . 'test.txt');
        $StoredFiles = $loc->get('StoredFiles');
        $StoredFiles->updateAll(['name' => 'test.txt.bak'], ['name' => 'test.txt']);
        $manager->filePutContent('test.txt', 'foo');
        $this->assertFileExists(TMP_VOL1 . DS . 'test.txt');
    }

    /**
     * testFileGetContent
     */
    public function testFileGetContent()
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');
        $this->assertEquals('foo', $manager->fileGetContent('test.txt'));
    }

    /**
     * testFileUpload
     */
    public function testFileUpload()
    {
        $file = @tempnam(TMP_TESTDIR, 'testunit-');
        file_put_contents($file, 'bar');
        $manager = new VolumeManager(1);
        $manager->fileUpload($file, 'test.txt');
        $this->assertEquals('bar', $manager->fileGetContent('test.txt'));
        unlink($file);
    }

    /**
     * testFileDownload
     */
    public function testFileDownload()
    {
        $file = TMP_TESTDIR . DS . uniqid('testunit-');
        $dir = dirname($file);
        if (is_dir($dir)) {
            Filesystem::remove($dir);// Test la création du dossier dans la fonction
        }
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'hello');
        $manager->fileDownload('test.txt', $file);
        $this->assertFileExists($file);
        $this->assertEquals('hello', file_get_contents($file));
        unlink($file);
        Filesystem::remove($dir);
    }

    /**
     * testFileDelete
     */
    public function testFileDelete()
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', $content = 'hello');

        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        /** @var EntityInterface $volume1 */
        $volume1 = $Volumes->get(1);
        /** @var EntityInterface $volume2 */
        $volume2 = $Volumes->get(2);
        $volume1->set('disk_usage', 3);
        $Volumes->save($volume1);

        $initialSize = $volume2->get('disk_usage');
        $manager->fileDelete('test.txt');
        $this->assertTrue($manager->fileNotExists('test.txt'));
        $volume1 = $Volumes->get(1);
        $volume2 = $Volumes->get(2);
        $this->assertEquals(0, $volume1->get('disk_usage')); // assure qu'on passe pas dans le négatif
        $this->assertEquals($initialSize - strlen($content), $volume2->get('disk_usage'));
        $file1 = json_decode($volume1->get('fields'))->path . DS . 'test.txt';
        $file2 = json_decode($volume2->get('fields'))->path . DS . 'test.txt';
        $this->assertFileDoesNotExist($file1);
        $this->assertFileDoesNotExist($file2);
    }

    /**
     * testFileExists
     */
    public function testFileExists()
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');

        $this->assertTrue($manager->fileExists('test.txt'));
        $this->assertTrue($manager->fileExists('test.txt', true));

        $manager::remove('test.txt', 1);
        $this->assertTrue($manager->fileExists('test.txt'));
        $this->assertFalse($manager->fileExists('test.txt', true));

        $manager::remove('test.txt', 2);
        $this->assertFalse($manager->fileExists('test.txt'));
        $this->assertFalse($manager->fileExists('test.txt', true));
    }

    /**
     * testFileNotExists
     */
    public function testFileNotExists()
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');

        $this->assertFalse($manager->fileNotExists('test.txt'));

        $manager::remove('test.txt', 1);
        $this->assertFalse($manager->fileNotExists('test.txt'));

        $manager::remove('test.txt', 2);
        $this->assertTrue($manager->fileNotExists('test.txt'));
    }

    /**
     * testRollbackKilledProcesses
     */
    public function testRollbackKilledProcesses()
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');

        /** @var Volume[] $volumes */
        $volumes = TableRegistry::getTableLocator()->get('Volumes')->find()->orderBy(['id'])->all()->toArray();
        $file1 = json_decode($volumes[0]->get('fields'))->path . DS . 'test.txt';
        $file2 = json_decode($volumes[1]->get('fields'))->path . DS . 'test.txt';

        do {
            $pid = rand(1, 65535);
            $base = VolumeManager::TRANSACTIONS_BASE_DIR . DS . $pid;
        } while (file_exists('/proc/' . $pid) || is_dir($base));
        mkdir($base, 0777, true);

        $data = json_encode(
            [
                'callback' => [VolumeManager::class, 'remove'],
                'args' => ['test.txt', 1],
            ],
            JSON_UNESCAPED_SLASHES
        );
        file_put_contents($base . DS . '1.rollback', VolumeManager::encrypt($data));

        $this->assertFileExists($file1);
        $this->assertFileExists($file2);
        VolumeManager::rollbackKilledProcesses();
        $this->assertFileDoesNotExist($file1);
        $this->assertFileExists($file2);

        $manager->fileDelete('test.txt');
        $manager->filePutContent('test.txt', 'foo');

        mkdir($base, 0777, true);
        $data = json_encode(
            [
                'callback' => [VolumeManager::class, 'remove'],
                'args' => ['test.txt', 1],
            ],
            JSON_UNESCAPED_SLASHES
        );
        file_put_contents($base . DS . '1.rollback', VolumeManager::encrypt($data));
        file_put_contents($base . DS . 'incorect_file', 'foo');
        $data = json_encode(
            [
                'callback' => [VolumeManager::class, 'remove'],
                'args' => ['test.txt', 2],
            ],
            JSON_UNESCAPED_SLASHES
        );
        file_put_contents($base . DS . '2.rollback', VolumeManager::encrypt($data));

        $this->assertFileExists($file1);
        $this->assertFileExists($file2);
        VolumeManager::rollbackKilledProcesses();
        $this->assertFileDoesNotExist($file1);
        $this->assertFileDoesNotExist($file2);
    }

    /**
     * testConstructException1
     */
    public function testConstructException1()
    {
        $this->expectException(ArgumentCountError::class);
        /** @noinspection PhpParamsInspection */
        new VolumeManager();
    }

    /**
     * testConstructException2
     */
    public function testConstructException2()
    {
        $this->expectException(RecordNotFoundException::class);
        new VolumeManager(2456);
    }

    /**
     * testConstructException3
     */
    public function testConstructException3()
    {
        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        foreach ($Volumes->find() as $volume) {
            $fields = json_decode($volume->get('fields'));
            $fields->path = '/' . uniqid('testunit-', true);
            $volume->set('fields', json_encode($fields, JSON_UNESCAPED_SLASHES));
            $Volumes->save($volume);
        }
        $this->expectException(VolumeException::class);
        new VolumeManager(1);
    }

    /**
     * testUploadFulldisk
     */
    public function testUploadFulldisk()
    {
        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        $Volumes->updateAll(['max_disk_usage' => 10000], []);
        $file = TMP_TESTDIR . DS . uniqid('testunit-');
        Filesystem::createDummyFile($file, 11000);
        $manager = new VolumeManager(1);
        $this->expectException(VolumeException::class);
        try {
            $manager->fileUpload($file, 'test.txt');
        } catch (\Exception $e) {
            unlink($file);
            throw $e;
        }
    }

    /**
     * testRollback
     */
    public function testRollback()
    {
        /** @var Volume[] $volumes */
        $volumes = TableRegistry::getTableLocator()->get('Volumes')->find()->all()->toArray();
        $file = json_decode($volumes[1]->get('fields'))->path . DS . 'test.txt';
        $dir = dirname($file);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        file_put_contents($file, 'foo');

        $manager = new VolumeManager(1);
        // fichier identique (skipped)
        $manager->filePutContent('test.txt', 'foo');

        // fichier different (error)
        $e = null;
        try {
            $manager->filePutContent('test.txt', 'bar');
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);

        $this->invokeMethod($manager, 'begin');
        $transactionDir = $this->invokeProperty(
            $manager,
            'transactionDir'
        );
        $this->invokeProperty(
            $manager,
            'toCallForRollback',
            'set',
            [
                [
                    'callback' => [$manager, 'fileGetContent'],
                    'args' => ['foo'],
                ],
            ]
        );
        $manager->__destruct();
        $filename = $transactionDir . '.serialize';
        $content = file_get_contents($filename);
        unlink($filename);
        $this->assertNotFalse($unserialize = unserialize($content));
        $this->assertEquals('fileGetContent', Hash::get($unserialize, '0.callback.1'));
    }

    /**
     * testPutOnVolumeDown
     */
    public function testPutOnVolumeDown()
    {
        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        $volume = $Volumes->find()->firstOrFail();
        $fields = json_decode($volume->get('fields'));
        $fields->path = '/' . uniqid('testunit-', true);
        $volume->set('fields', json_encode($fields, JSON_UNESCAPED_SLASHES));
        $Volumes->save($volume);
        $manager = new VolumeManager(1);
        $this->expectException(VolumeException::class);
        $manager->filePutContent('test.txt', 'foo');
    }

    /**
     * testEmptyFilename
     */
    public function testEmptyFilename()
    {
        $manager = new VolumeManager(1);
        $this->expectException(VolumeException::class);
        $manager->filePutContent('', 'foo');
    }

    /**
     * testSaveFalse
     */
    public function testSaveFalse()
    {
        $manager = new VolumeManager(1);
        $manager->StoredFiles = $this->createMock(StoredFilesTable::class);
        $manager->StoredFiles->method('save')->willReturn(false);
        $this->expectException(VolumeException::class);
        $manager->filePutContent('test.txt', 'foo'); // throw Exception
    }

    /**
     * Inexistance du fichier
     */
    public function testFileGetContentException1()
    {
        $manager = new VolumeManager(1);
        $this->expectException(RecordNotFoundException::class);
        $manager->fileGetContent('test.txt');
    }

    /**
     * Altération de la base de données
     */
    public function testFileGetContentException2()
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');
        TableRegistry::getTableLocator()->get('StoredFilesVolumes')->deleteAll([]);
        $this->expectException(RecordNotFoundException::class);
        $manager->fileGetContent('test.txt');
    }

    /**
     * Modification du storage_ref
     */
    public function testFileGetContentException3()
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');
        TableRegistry::getTableLocator()->get('StoredFilesVolumes')->updateAll(
            ['storage_ref' => 'foo.txt'],
            []
        );
        $this->expectException(VolumeException::class);
        $manager->fileGetContent('test.txt');
    }

    /**
     * Inexistance du fichier
     */
    public function testFileDownloadException1()
    {
        $file = TMP_TESTDIR . DS . uniqid('testunit-');
        $manager = new VolumeManager(1);
        $this->expectException(VolumeException::class);
        try {
            $manager->fileDownload('test.txt', $file);
        } finally {
            if (is_dir(TMP_TESTDIR)) {
                Filesystem::remove(TMP_TESTDIR);
            }
        }
    }

    /**
     * Altération de la base de données
     */
    public function testFileDownloadException2()
    {
        $file = TMP_TESTDIR . DS . uniqid('testunit-');
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');
        TableRegistry::getTableLocator()->get('StoredFilesVolumes')->deleteAll([]);
        $this->expectException(VolumeException::class);
        try {
            $manager->fileDownload('test.txt', $file);
        } catch (\Exception $e) {
            Filesystem::remove(TMP_TESTDIR);
            throw $e;
        }
    }

    /**
     * Modification du storage_ref
     */
    public function testFileDownloadException3()
    {
        $file = TMP_TESTDIR . DS . uniqid('testunit-');
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');
        TableRegistry::getTableLocator()->get('StoredFilesVolumes')->updateAll(
            ['storage_ref' => 'foo.txt'],
            []
        );
        $this->expectException(VolumeException::class);
        try {
            $manager->fileDownload('test.txt', $file);
        } catch (\Exception $e) {
            Filesystem::remove(TMP_TESTDIR);
            throw $e;
        }
    }

    /**
     * Fichier existe déjà
     */
    public function testFileDownloadException4()
    {
        $file = TMP_TESTDIR . DS . uniqid('testunit-');
        Filesystem::begin();
        Filesystem::createDummyFile($file, 10);
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');
        $this->expectException(VolumeException::class);
        try {
            $manager->fileDownload('test.txt', $file);
            Filesystem::rollback();
        } catch (\Exception $e) {
            Filesystem::rollback();
            throw $e;
        }
    }

    /**
     * testFileDeleteOnVolumeDown
     */
    public function testFileDeleteOnVolumeDown()
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');

        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        $volume = $Volumes->find()->firstOrFail();
        $fields = json_decode($volume->get('fields'));
        $fields->path = '/' . uniqid('testunit-', true);
        $volume->set('fields', json_encode($fields, JSON_UNESCAPED_SLASHES));
        $Volumes->save($volume);
        $manager = new VolumeManager(1);
        $this->expectException(VolumeException::class);
        $manager->fileDelete('test.txt');
    }

    /**
     * testFileExistsOnVolumeDown
     */
    public function testFileExistsOnVolumeDown()
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');

        $Volumes = TableRegistry::getTableLocator()->get('Volumes');
        $volume = $Volumes->find()->firstOrFail();
        $fields = json_decode($volume->get('fields'));
        $fields->path = '/' . uniqid('testunit-', true);
        $volume->set('fields', json_encode($fields, JSON_UNESCAPED_SLASHES));
        $Volumes->save($volume);

        $manager = new VolumeManager(1);
        $this->assertTrue($manager->fileExists('test.txt'));
        $this->assertFalse($manager->fileExists('test2.txt'));
        $this->expectException(VolumeException::class);
        $manager->fileExists('test.txt', true);
    }

    /**
     * testRollbackKilledProcessesException1
     */
    public function testRollbackKilledProcessesException1()
    {
        do {
            $pid = rand(1, 65535);
            $base = VolumeManager::TRANSACTIONS_BASE_DIR . DS . $pid;
        } while (file_exists('/proc/' . $pid) || is_dir($base));
        mkdir($base, 0777, true);

        file_put_contents($base . DS . '1.rollback', 'corrupted string');
        $this->expectException(\Exception::class);
        try {
            VolumeManager::rollbackKilledProcesses();
        } catch (\Exception $e) {
            Filesystem::remove($base);
            throw $e;
        }
    }

    /**
     * testRemoveException
     */
    public function testRemoveException()
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');
        /** @var Volume[] $volumes */
        $volumes = TableRegistry::getTableLocator()->get('Volumes')->find()->all()->toArray();
        Configure::write('Volumes.drivers.FILESYSTEM.class', MockedVolume::class);
        $this->expectException(VolumeException::class);
        // NOTE: ref === name dans le cas d'un VolumeFilesystem
        VolumeManager::remove('test.txt', $volumes[0]->get('id'));
    }

    /**
     * testDestruct
     */
    public function testDestruct()
    {
        $manager = new VolumeManager(1);
        $manager->Filesystem = $this->createMock(get_class($manager->Filesystem));
        $manager->Filesystem->method('remove')->willThrowException(new \Exception());
        unset($manager);
        $this->assertTrue(true);
    }

    /**
     * testGetDriverById
     */
    public function testGetDriverById()
    {
        $volume = VolumeManager::getDriverById(1);
        $this->assertInstanceOf(VolumeFilesystem::class, $volume);
    }

    /**
     * testSet
     */
    public function testSet()
    {
        $manager = new VolumeManager(1);
        $storedFile = $manager->filePutContent('test.txt', 'foo');
        $this->assertEquals('foo', $manager->fileGetContent('test.txt'));
        $manager->set($storedFile->id, 'bar');
        $this->assertEquals('bar', $manager->fileGetContent('test.txt'));
    }

    /**
     * testReplace
     */
    public function testReplace()
    {
        $loc = TableRegistry::getTableLocator();
        $Volumes = $loc->get('Volumes');
        $glob1 = TMP_VOL1 . DS . '*';
        $glob2 = TMP_VOL2 . DS . '*';

        $manager = new VolumeManager(1);
        $storedFile = $manager->filePutContent('test.txt', 'foo');
        $this->assertCount(1, glob($glob1));
        $this->assertCount(1, glob($glob2));
        $initialVolumes = $manager->getVolumes();

        $dir = sys_get_temp_dir() . DS . 'testunit';
        Filesystem::dumpFile($filename = $dir . DS . 'test.txt', 'bar');
        $manager->replace($storedFile->id, $filename);
        $this->assertEquals('bar', $manager->fileGetContent('test.txt'));

        /**
         * test fail lors de la vérification du hash du fichier uploadé
         */
        $loc->get('StoredFilesVolumes')->deleteAll(['volume_id !=' => 1]);
        $volume = $this->getMockBuilder(VolumeFilesystem::class)
            ->setConstructorArgs(
                [
                    [
                        'path' => TMP_VOL1,
                        'volume_entity' => $Volumes->get(1),
                    ],
                ]
            )
            ->onlyMethods(['hash'])
            ->getMock();
        $volume->method('hash')->willReturn('false');
        $this->invokeProperty(
            $manager,
            'volumes',
            'set',
            [1 => $volume]
        );
        $this->assertEquals('bar', $manager->fileGetContent('test.txt'));
        Filesystem::dumpFile($filename, 'foo');
        $e = null;
        try {
            $manager->replace($storedFile->id, $filename);

            // FIXME fichier test.txt altéré après exception
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);
        $this->assertEquals('bar', $manager->fileGetContent('test.txt'));
        $this->assertCount(1, glob($glob1));

        /**
         * test fail lors du backup de l'ancien fichier
         */
        $volume = $this->getMockBuilder(VolumeFilesystem::class)
            ->setConstructorArgs(
                [
                    [
                        'path' => TMP_VOL1,
                        'volume_entity' => $Volumes->get(1),
                    ],
                ]
            )
            ->onlyMethods(['rename'])
            ->getMock();
        $volume->method('rename')
            ->willThrowException(new VolumeException(VolumeException::FILE_WRITE_ERROR));
        $this->invokeProperty(
            $manager,
            'volumes',
            'set',
            [1 => $volume]
        );
        $this->assertEquals('bar', $manager->fileGetContent('test.txt'));
        Filesystem::dumpFile($filename, 'foo');
        try {
            $manager->replace($storedFile->id, $filename);
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);
        $this->assertEquals('bar', $manager->fileGetContent('test.txt'));
        $this->assertCount(1, glob($glob1));

        /**
         * test fail lors du renommage du fichier uploadé (avec fail du rollback)
         */
        $volume = $this->getMockBuilder(VolumeFilesystem::class)
            ->setConstructorArgs(
                [
                    [
                        'path' => TMP_VOL1,
                        'volume_entity' => $Volumes->get(1),
                    ],
                ]
            )
            ->onlyMethods(['rename'])
            ->getMock();
        $i = 0;
        $volume->method('rename')
            ->willReturnCallback(
                function ($a, $b) use (&$i, $initialVolumes) {
                    if ($i === 0) {
                        $i++;
                        $initialVolumes[1]->rename($a, $b);
                    } else {
                        throw new VolumeException(VolumeException::FILE_WRITE_ERROR);
                    }
                    return $b;
                }
            );
        $this->invokeProperty(
            $manager,
            'volumes',
            'set',
            [1 => $volume]
        );
        $this->assertEquals('bar', $manager->fileGetContent('test.txt'));
        Filesystem::dumpFile($filename, 'foo');
        $e = null;
        try {
            $manager->replace($storedFile->id, $filename);
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);
        $this->assertFalse($manager->fileExists('test.txt')); // rollback échoué
        $this->assertCount(2, glob($glob1)); // fichiers upload + backup
        Filesystem::remove(dirname($glob1));
        foreach ($initialVolumes as $vol) {
            $vol->filePutContent('test.txt', 'bar');
        }
        $this->assertCount(1, glob($glob1)); // retour à la normale

        /**
         * test fail lors du renommage du fichier uploadé (avec réussite du rollback)
         */
        $volume = $this->getMockBuilder(VolumeFilesystem::class)
            ->setConstructorArgs(
                [
                    [
                        'path' => TMP_VOL1,
                        'volume_entity' => $Volumes->get(1),
                    ],
                ]
            )
            ->onlyMethods(['rename'])
            ->getMock();
        $i = 0;
        $volume->method('rename')
            ->willReturnCallback(
                function ($a, $b) use (&$i, $initialVolumes) {
                    $i++;
                    if ($i === 2) {
                        throw new VolumeException(VolumeException::FILE_WRITE_ERROR);
                    } else {
                        $initialVolumes[1]->rename($a, $b);
                    }
                    return $b;
                }
            );
        $this->invokeProperty(
            $manager,
            'volumes',
            'set',
            [1 => $volume]
        );
        $this->assertEquals('bar', $manager->fileGetContent('test.txt'));
        Filesystem::dumpFile($filename, 'foo');
        $e = null;
        try {
            $manager->replace($storedFile->id, $filename);
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);
        $this->assertEquals('bar', $manager->fileGetContent('test.txt'));
        $this->assertCount(1, glob($glob1));

        /**
         * test fail lors du renommage du fichier uploadé dans le 2e volume -> rollback du volume 1
         */
        $volume = $this->getMockBuilder(VolumeFilesystem::class)
            ->setConstructorArgs(
                [
                    [
                        'path' => TMP_VOL2,
                        'volume_entity' => $Volumes->get(2),
                    ],
                ]
            )
            ->onlyMethods(['rename'])
            ->getMock();
        $i = 0;
        $volume->method('rename')
            ->willReturnCallback(
                function ($a, $b) use (&$i, $initialVolumes) {
                    $i++;
                    if ($i === 2) {
                        throw new VolumeException(VolumeException::FILE_WRITE_ERROR);
                    } else {
                        $initialVolumes[2]->rename($a, $b);
                    }
                    return $b;
                }
            );
        $this->invokeProperty(
            $manager,
            'volumes',
            'set',
            [1 => $initialVolumes[1], 2 => $volume]
        );
        $this->assertEquals('bar', $manager->fileGetContent('test.txt'));
        Filesystem::dumpFile($filename, 'foo');
        $e = null;
        try {
            $manager->replace($storedFile->id, $filename);
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);
        $this->assertEquals('bar', $manager->fileGetContent('test.txt'));
        $this->assertCount(1, glob($glob1));
        $this->assertCount(1, glob($glob2));
    }

    /**
     * testReadfile
     */
    public function testReadfile()
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');
        ob_start();
        $manager->readfile('test.txt');
        $content = ob_get_clean();
        $this->assertEquals('foo', $content);
    }

    /**
     * testFileIntegrity
     */
    public function testFileIntegrity()
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');
        $this->assertTrue($manager->fileIntegrity('test.txt'));
        file_put_contents(TMP_VOL1 . DS . 'test.txt', 'bar');
        $this->assertFalse($manager->fileIntegrity('test.txt'));
    }

    /**
     * testUploadToVolume
     */
    public function testUploadToVolume()
    {
        $loc = TableRegistry::getTableLocator();
        $Volumes = $loc->get('Volumes');
        $Volumes->updateAll(['secure_data_space_id' => null], ['id !=' => 1]);

        $manager = new VolumeManager(1);
        $this->assertCount(1, $manager->getVolumes());
        $manager->filePutContent('test.txt', 'foo');

        $volume2 = VolumeManager::getDriverById(2);
        $this->assertFalse($volume2->fileExists('test.txt'));
        $manager->uploadToVolume('test.txt', $volume2, 2);
        $this->assertTrue($volume2->fileExists('test.txt'));
    }

    /**
     * @return void
     * @throws VolumeException
     */
    public function testDownloadResumption(): void
    {
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $manager->fileDelete('test.txt'); // supprime sur disque
        $conn->rollback(); // garde la mention en bdd
        try {
            $manager->filePutContent('test.txt', 'foo');
        } catch (VolumeException $e) {
            $this->fail($e->getMessage());
        }
        $this->assertTrue(true);

        $volume1 = VolumeManager::getDriverById(1);
        $volume2 = VolumeManager::getDriverById(2);
        $volume2->fileDelete('test.txt');
        $this->assertTrue($volume1->fileExists('test.txt'));
        $this->assertFalse($volume2->fileExists('test.txt'));
        try {
            $manager->filePutContent('test.txt', 'foo');
        } catch (VolumeException $e) {
            $this->fail($e->getMessage());
        }
        $this->assertTrue($volume2->fileExists('test.txt'));

        $e = null;
        try {
            $manager->filePutContent('test.txt', 'bar');
        } catch (Exception $e) {
        }
        $this->assertTrue($e instanceof VolumeException);
    }
}
