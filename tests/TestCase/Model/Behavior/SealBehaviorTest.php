<?php

namespace Asalae\Test\TestCase\Model\Behavior;

use Asalae\Controller\AppController;
use Asalae\Controller\Component\SealComponent;
use Asalae\Model\Entity\OrgEntity;
use AsalaeCore\TestSuite\TestCase;
use Authorization\Identity;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\Utility\Hash;
use Exception;

class SealBehaviorTest extends TestCase
{
    public array $fixtures = [
        'app.Agreements',
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveFiles',
        'app.ArchiveKeywords',
        'app.ArchiveUnits',
        'app.ArchiveUnitsBatchTreatments',
        'app.ArchiveUnitsDestructionRequests',
        'app.ArchiveUnitsOutgoingTransferRequests',
        'app.ArchiveUnitsRestitutionRequests',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.ArchivingSystems',
        'app.BatchTreatments',
        'app.BeanstalkJobs',
        'app.Counters',
        'app.Deliveries',
        'app.DeliveryRequests',
        'app.DestructionNotifications',
        'app.DestructionRequests',
        'app.Eaccpfs',
        'app.Fileuploads',
        'app.KeywordLists',
        'app.Keywords',
        'app.Ldaps',
        'app.OrgEntities',
        'app.OrgEntitiesTimestampers',
        'app.OutgoingTransferRequests',
        'app.OutgoingTransfers',
        'app.Profiles',
        'app.RestitutionRequests',
        'app.Restitutions',
        'app.Roles',
        'app.SecureDataSpaces',
        'app.Sequences',
        'app.ServiceLevels',
        'app.Siegfrieds',
        'app.TechnicalArchives',
        'app.TransferAttachments',
        'app.TransferErrors',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
        'app.ValidationChains',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];

    /**
     * @var SealComponent
     */
    private SealComponent $Seal;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $identity = $this->createMock(Identity::class);
        $identity->method('getOriginalData')->willReturn($this->genericSessionData['Auth']);
        $request = (new ServerRequest())->withAttribute('identity', $identity);
        $response = new Response();
        /** @var AppController $controller */
        $controller = $this->getMockBuilder('Asalae\Controller\AppController')
            ->setConstructorArgs([$request, $response])
            ->onlyMethods([])
            ->getMock();
        $controller->initialize();
        $this->Seal = $controller->Seal;
    }

    /**
     * testallModels
     */
    public function testallModels()
    {
        $models = [
            'Agreements',
            'ArchiveBinaries',
            'ArchiveBinariesArchiveUnits',
            'ArchiveFiles',
            'ArchiveKeywords',
            'ArchiveUnits',
            'ArchiveUnitsBatchTreatments',
            'ArchiveUnitsDestructionRequests',
            'ArchiveUnitsOutgoingTransferRequests',
            'ArchiveUnitsRestitutionRequests',
            'Archives',
            'ArchivingSystems',
            'BatchTreatments',
            'BeanstalkJobs',
            'Counters',
            'Deliveries',
            'DeliveryRequests',
            'DestructionNotifications',
            'DestructionRequests',
            'Eaccpfs',
            'Fileuploads',
            'KeywordLists',
            'Keywords',
            'Ldaps',
            'OrgEntities',
            'OrgEntitiesTimestampers',
            'OutgoingTransferRequests',
            'OutgoingTransfers',
            'Profiles',
            'RestitutionRequests',
            'Restitutions',
            'Roles',
            'SecureDataSpaces',
            'Sequences',
            'ServiceLevels',
            'TechnicalArchives',
            'TransferAttachments',
            'TransferErrors',
            'Transfers',
            'Users',
            'ValidationChains',
            'ValidationProcesses',
            'ValidationStages',
        ];
        foreach ($models as $model) {
            $e = null;
            try {
                $this->Seal->archivalAgency()
                    ->table($model)
                    ->find('seal')
                    ->first();
            } catch (Exception $e) {
                $e = $e->getMessage();
            }
            $this->assertNull($e);
        }
    }

    /**
     * testFindSealList
     */
    public function testFindSealList()
    {
        $list = $this->Seal->archivalAgency()
            ->table('OrgEntities')->find('sealList')
            ->toArray();
        $this->assertTrue(isset($list[2]));
        $this->assertFalse(isset($list[3]));

        $this->Seal->setConfig('archivalAgencyId', 3);
        $list = $this->Seal->archivalAgency()
            ->table('OrgEntities')->find('sealList')
            ->toArray();
        $this->assertFalse(isset($list[2]));
        $this->assertTrue(isset($list[3]));
    }

    /**
     * testFindSealThreaded
     */
    public function testFindSealThreaded()
    {
        $threaded = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')->find('sealThreaded')
            ->toArray();
        $sv = Hash::get($threaded, '0.children.0');
        $sp = Hash::get($threaded, '0.children.0.children.0');
        $this->assertInstanceOf(OrgEntity::class, $sv);
        $this->assertEquals('sv', $sv->get('identifier'));
        $this->assertInstanceOf(OrgEntity::class, $sp);
        $this->assertEquals('sp', $sp->get('identifier'));

        $this->Seal->setConfig('archivalAgency', $this->Seal->table('OrgEntities')->get(3));
        $this->Seal->setConfig('archivalAgencyId', 3);
        $threaded = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')->find('sealThreaded')
            ->toArray();
        $other = Hash::get($threaded, '0');
        $this->assertInstanceOf(OrgEntity::class, $other);
        $this->assertEquals('sa2', $other->get('identifier'));
    }
}
