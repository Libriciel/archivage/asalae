<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\ValidationHistory;
use Asalae\Model\Table\ValidationHistoriesTable;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cake\Utility\Hash;

class ValidationHistoryTest extends TestCase
{
    public array $fixtures = [
        'app.OrgEntities',
        'app.ValidationActors',
        'app.ValidationHistories',
        'app.Users',
    ];

    /**
     * testGetCreatedOrgEntityName
     */
    public function testGetCreatedOrgEntityName()
    {
        /** @var ValidationHistoriesTable $ValidationHistories */
        $ValidationHistories = TableRegistry::getTableLocator()->get('ValidationHistories');
        /** @var ValidationHistory $histo */
        $histo = $ValidationHistories->get(1);
        $this->assertEquals('admin', $histo->get('created_username')); // set
        $this->assertEquals("Mon Service d'archive", $histo->get('created_org_entity_name'));
    }

    /**
     * testGetActorInfo
     */
    public function testGetActorInfo()
    {
        /** @var ValidationHistoriesTable $ValidationHistories */
        $ValidationHistories = TableRegistry::getTableLocator()->get('ValidationHistories');
        /** @var ValidationHistory $histo */
        $histo = $ValidationHistories->find()
            ->where(['ValidationHistories.id' => 2])
            ->contain(['ValidationActors'])
            ->firstOrFail();
        $this->assertEquals(__("Utilisateur: {0}", h('M. Martin Dupont')), $histo->get('actor_info'));

        /** @var EntityInterface $actor */
        $actor = Hash::get($histo, 'validation_actor');
        $actor->set('app_type', 'MAIL');
        $actor->set('actor_name', 'test');
        $actor->set('app_key', 'test');
        $this->assertEquals(__("E-mail: {0} ({1})", 'test', 'test'), $histo->get('actor_info'));

        $actor->set('app_type', 'SERVICE_ARCHIVES');
        $this->assertEquals(
            __("Service d'Archives: {0} ({1})", "Mon Service d'archive", 'admin'),
            $histo->get('actor_info')
        );

        $actor->set('app_type', 'SERVICE_DEMANDEUR');
        $this->assertEquals(
            __("Service demandeur: {0} ({1})", "Mon Service d'archive", 'admin'),
            $histo->get('actor_info')
        );

        $actor->set('app_type', 'SERVICE_PRODUCTEUR');
        $this->assertEquals(
            __("Service producteur: {0} ({1})", "Mon Service d'archive", 'admin'),
            $histo->get('actor_info')
        );

        $actor->set('app_type', '');
        $this->assertEmpty($histo->get('actor_info'));
    }
}
