<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\Volume;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class VolumeTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write(
            'Volumes.drivers.test',
            [
                'name' => 'test',
                'fields' => [
                    'name' => ['type' => 'text'],
                    'password' => ['type' => 'password'],
                ],
            ]
        );
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * testGetDrivertrad
     */
    public function testGetDrivertrad()
    {
        $volume = new Volume();
        $this->assertEmpty($volume->get('drivertrad'));

        $volume->set('driver', 'test');
        Configure::write('Volumes.drivers.test.name', 'test');
        $this->assertEquals('test', $volume->get('drivertrad'));
    }

    /**
     * testDeletable
     */
    public function testDeletable()
    {
        $volume = new Volume();
        $this->assertFalse($volume->get('deletable'));

        $volume = new Volume(['id' => 1]);
        $this->assertTrue($volume->get('deletable'));

        $volume->set('secure_data_space_id', 1);
        $this->assertFalse($volume->get('deletable'));
    }

    /**
     * testFieldsObject
     */
    public function testFieldsObject()
    {
        $volume = new Volume(['driver' => 'test']);
        $this->assertEmpty($volume->get('fields_object'));

        $volume->set('fields', $expected = ['foo' => 'bar']);
        $this->assertEquals($expected, $volume->get('fields_object'));

        $volume->set('fields', json_encode($expected));
        $this->assertEquals((object)$expected, $volume->get('fields_object'));
    }

    /**
     * testEditable
     */
    public function testEditable()
    {
        $volume = new Volume();
        $this->assertFalse($volume->get('is_used'));

        $volume = new Volume(['id' => 1]);
        $this->assertTrue($volume->get('is_used'));

        TableRegistry::getTableLocator()->get('StoredFilesVolumes')->deleteAll([]);
        $volume = new Volume(['id' => 1]);
        $this->assertFalse($volume->get('is_used'));
    }

    /**
     * testRemovable
     */
    public function testRemovable()
    {
        $volume = new Volume();
        $this->assertFalse($volume->get('removable'));

        $volume = new Volume(['id' => 1]);
        $this->assertFalse($volume->get('removable'));

        TableRegistry::getTableLocator()->get('StoredFilesVolumes')->deleteAll([]);
        $volume = new Volume(['id' => 1]);
        $this->assertTrue($volume->get('removable'));
    }
}
