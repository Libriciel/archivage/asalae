<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\ServiceLevel;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Exception;

class ServiceLevelTest extends TestCase
{
    public array $fixtures = [
        'app.EventLogs',
        'app.Fileuploads',
        'app.OrgEntities',
        'app.ServiceLevels',
        'app.Siegfrieds',
        'app.Transfers',
        'app.Users',
    ];

    /**
     * testGetEditableIdentifier
     */
    public function testGetEditableIdentifier()
    {
        $loc = TableRegistry::getTableLocator();
        $ServiceLevels = $loc->get('ServiceLevels');
        $Transfers = $loc->get('Transfers');

        $serviceLevel1 = new ServiceLevel();
        $this->assertTrue($serviceLevel1->get('editable_identifier'));

        $Transfers->updateAll(
            [
                'service_level_id' => 1,
                'state' => 'rejected',
            ],
            []
        );
        /** @var EntityInterface $serviceLevel2 */
        $serviceLevel2 = $ServiceLevels->get(1);
        $this->assertTrue($serviceLevel2->get('editable_identifier'));

        $Transfers->updateAll(['state' => 'preparating'], []);

        $this->assertFalse($serviceLevel2->get('editable_identifier'));
        $serviceLevel2->set('identifier', 'bar');
        try {
            $ServiceLevels->save($serviceLevel2);
            $this->fail();
        } catch (Exception) {
        }
    }
}
