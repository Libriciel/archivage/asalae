<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\Fileupload;
use Asalae\Model\Table\FileuploadsTable;
use Cake\Core\Configure;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const TMP_TESTDIR;

class FileuploadTest extends TestCase
{
    public array $fixtures = [
        'app.EventLogs',
        'app.Fileuploads',
        'app.FileuploadsProfiles',
        'app.Profiles',
        'app.Siegfrieds',
        'app.Users',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('OrgEntities.public_dir', TMP_TESTDIR . DS . 'webroot');
    }

    /**
     * testDeletable
     */
    public function testDeletable()
    {
        $upload = new Fileupload();
        $this->assertTrue($upload->get('deletable'));

        Filesystem::dumpFile(TMP_TESTDIR . DS . 'test.txt', 'test');
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $file = $Fileuploads->newUploadedEntity(
            'test.txt',
            TMP_TESTDIR . DS . 'test.txt',
            1
        );
        $Fileuploads->saveOrFail($file);
        $FileuploadsProfiles = TableRegistry::getTableLocator()->get('FileuploadsProfiles');
        $FileuploadsProfiles->insertQuery()
            ->insert(['fileupload_id', 'profile_id'])
            ->values(['fileupload_id' => $file->id, 'profile_id' => 1])
            ->execute();
        $this->assertFalse($file->get('deletable'));

        TableRegistry::getTableLocator()->get('Profiles')->deleteAll([]);
        $this->assertTrue($file->get('deletable'));
    }

    /**
     * testCopyInEntityDirectory
     */
    public function testCopyInEntityDirectory()
    {
        $configuration = new Entity(
            [
                'org_entity_id' => 2,
                'name' => 'logo-client',
                'setting' => 'test.png',
            ]
        );
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'test.txt', 'test');
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        /** @var Fileupload $upload */
        $upload = $Fileuploads->newUploadedEntity(
            'test.txt',
            TMP_TESTDIR . DS . 'test.txt',
            1
        );
        $Fileuploads->saveOrFail($upload);
        $upload->copyInEntityDirectory($configuration);
        $this->assertFileExists(TMP_TESTDIR . DS . 'webroot' . DS . $configuration->setting);
    }
}
