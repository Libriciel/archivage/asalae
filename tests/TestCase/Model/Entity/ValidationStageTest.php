<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class ValidationStageTest extends TestCase
{
    public array $fixtures = [
        'app.AccessRules',
        'app.OrgEntities',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];

    /**
     * testGetDeletable
     */
    public function testGetDeletable()
    {
        $loc = TableRegistry::getTableLocator();
        $ValidationStages = $loc->get('ValidationStages');
        /** @var EntityInterface $stage */
        $stage = $ValidationStages->get(1);
        $this->assertFalse((bool)$stage->get('deletable'));
        $stage = $ValidationStages->get(2);
        $this->assertTrue((bool)$stage->get('deletable'));
        $stage = $ValidationStages->get(3);
        $this->assertTrue((bool)$stage->get('deletable'));
    }
}
