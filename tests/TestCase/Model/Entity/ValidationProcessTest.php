<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\ValidationActor;
use Asalae\Model\Entity\ValidationProcess;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

use function __dx;

class ValidationProcessTest extends TestCase
{
    public array $fixtures = [
        'app.ValidationActors',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];

    /**
     * testSetStepback
     */
    public function testSetStepback()
    {
        $process = new ValidationProcess();
        $process->set('stepback', 1);
        $this->assertEquals('{"stepback":1}', $process->get('app_meta'));
    }

    /**
     * testGetProcessedtrad
     */
    public function testGetProcessedtrad()
    {
        $process = new ValidationProcess();
        $this->assertEmpty($process->get('processedtrad'));

        $process->set('processed', true);
        $this->assertEquals(
            __dx('validation_process', 'processed', "true"),
            $process->get('processedtrad')
        );
        $process->set('processed', false);
        $this->assertEquals(
            __dx('validation_process', 'processed', "false"),
            $process->get('processedtrad')
        );
    }

    /**
     * testGetValidatedtrad
     */
    public function testGetValidatedtrad()
    {
        $process = new ValidationProcess();
        $this->assertEquals(
            '',
            $process->get('validatedtrad')
        );

        $process->set('validated', true);
        $this->assertEquals(
            __dx('validation_process', 'validated', "true"),
            $process->get('validatedtrad')
        );
        $process->set('validated', false);
        $this->assertEquals(
            __dx('validation_process', 'validated', "false"),
            $process->get('validatedtrad')
        );
    }

    /**
     * testSteptrad
     */
    public function testSteptrad()
    {
        $process = new ValidationProcess();
        $this->assertEmpty($process->get('steptrad'));

        $process->set('processed', true);
        $this->assertEquals(
            '',
            $process->get('steptrad')
        );
        $process->set('processed', false);
        $this->assertEmpty($process->get('steptrad'));

        $process->set('current_stage_id', 1);
        $this->assertEquals('Etape 1 conformes', $process->get('steptrad'));
    }

    /**
     * testHasMailAgent
     */
    public function testHasMailAgent()
    {
        $process = new ValidationProcess();
        $this->assertEmpty($process->get('has_mail_agent'));

        $process->set('current_stage_id', 1);
        $this->assertTrue($process->get('has_mail_agent'));
    }

    /**
     * testProcessable
     */
    public function testProcessable()
    {
        $process = new ValidationProcess();
        $this->assertEmpty($process->get('processable'));

        $ValidationStages = TableRegistry::getTableLocator()->get('ValidationStages');
        $currentState = $ValidationStages->get(1);
        $Processes = TableRegistry::getTableLocator()->get('ValidationProcesses');
        $process = $Processes->find()
            ->where(['ValidationProcesses.id' => 1])
            ->contain(['CurrentStages'])
            ->firstOrFail();
        $this->assertFalse($process->get('processable'));
        $process->set('current_stage', $currentState);
        $process->set('current_stage_id', 1);

        // actor_id
        $process->set('validation_actor_id', 1);
        $this->assertInstanceOf(ValidationActor::class, $process->get('processable'));
        $process->unset('validation_actor_id');

        // actor.user_id === validation_user_id
        $process->set('validation_user_id', 1);
        $this->assertInstanceOf(ValidationActor::class, $process->get('processable'));
        $process->unset('validation_user_id');

        // type=SERVICE_ARCHIVES -> transfer.archival_agency_id === validation_org_entity_id
        $Actors = TableRegistry::getTableLocator()->get('ValidationActors');
        $Actors->updateAll(['app_type' => 'SERVICE_ARCHIVES'], ['id' => 1]);
        $process->get('current_stage')->unset('validation_actors');
        $process->set('validation_org_entity_id', 2);
        // pour identifier le service d'archives
        $process->set('app_subject_type', 'Transfers');
        $process->set('app_foreign_key', 1);
        $process->set('transfer', new Entity(['archival_agency_id' => 2]));
        $this->assertInstanceOf(ValidationActor::class, $process->get('processable'));
    }

    /**
     * testSubjectArchivalAgencyId
     */
    public function testSubjectArchivalAgencyId()
    {
        $process = new ValidationProcess();
        $this->assertEmpty($process->get('subject_archival_agency_id'));

        $process->set('app_subject_type', 'Transfers');
        $process->set('app_foreign_key', 1);
        $process->set('transfer', new Entity(['archival_agency_id' => 2]));
        $this->assertEquals(
            2,
            $process->get('subject_archival_agency_id')
        );
    }

    /**
     * testSubjectRequesterAgencyId
     */
    public function testSubjectRequesterAgencyId()
    {
        $process = new ValidationProcess();
        $this->assertEmpty($process->get('subject_requester_agency_id'));

        $process->set('app_subject_type', 'DeliveryRequests');
        $process->set('app_foreign_key', 1);
        $process->set('delivery_request', new Entity(['requester_id' => 2]));
        $this->assertEquals(
            2,
            $process->get('subject_requester_agency_id')
        );
    }

    /**
     * testSubjectOriginatingAgencyId
     */
    public function testSubjectOriginatingAgencyId()
    {
        $process = new ValidationProcess();
        $this->assertEmpty($process->get('subject_originating_agency_id'));

        $process->set('app_subject_type', 'DestructionRequests');
        $process->set('app_foreign_key', 1);
        $process->set('destruction_request', new Entity(['originating_agency_id' => 2]));
        $this->assertEquals(
            2,
            $process->get('subject_originating_agency_id')
        );
    }
}
