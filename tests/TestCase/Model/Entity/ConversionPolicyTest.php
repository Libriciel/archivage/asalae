<?php

/** @noinspection PhpDeprecationInspection TODO retirer ConversionPolicy et tout ce qui y est lié */

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\ConversionPolicy;
use Cake\TestSuite\TestCase;

class ConversionPolicyTest extends TestCase
{
    /**
     * testSetParams
     */
    public function testSetParams()
    {
        $conv = new ConversionPolicy();
        $conv->set('params', ['codec' => 'foo']);
        $this->assertEquals('foo', $conv->get('codec'));

        $conv->set('params', json_encode(['codec' => 'bar']));
        $this->assertEquals('bar', $conv->get('codec'));
    }
}
