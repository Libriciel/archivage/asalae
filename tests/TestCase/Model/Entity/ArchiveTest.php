<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\Archive;
use Asalae\Model\Table\ArchivesTable;
use Cake\I18n\DateTime;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class ArchiveTest extends TestCase
{
    /**
     * testGetDates
     */
    public function testGetDates()
    {
        $archive = new Archive();
        $this->assertStringMatchesFormat('%A[sd]%A[sd]%A', $archive->get('dates'));

        $time = new DateTime();
        $archive->set('oldest_date', $time);
        $archive->set('latest_date', $time);

        $this->assertStringContainsString((string)$time, $archive->get('dates'));
    }

    /**
     * testGetStatetrad
     */
    public function testGetStatetrad()
    {
        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $archive = new Archive();
        $config = $Archives->getBehavior('Options')->getConfig();
        $this->assertCount(15, $config['state']);
        foreach ($config['state'] as $state) {
            $archive->set('state', $state);
            $this->assertNotEmpty($trad = $archive->get('statetrad'));
            $this->assertNotEquals($state, $trad);
        }
        $archive->set('state', 'unknown');
        $this->assertEquals('unknown', $archive->get('statetrad'));
    }

    /**
     * testGetStatetradTitle
     */
    public function testGetStatetradTitle()
    {
        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $archive = new Archive();
        $config = $Archives->getBehavior('Options')->getConfig();
        $this->assertCount(15, $config['state']);
        foreach ($config['state'] as $state) {
            $archive->set('state', $state);
            $this->assertNotEmpty($trad = $archive->get('statetrad_title'));
            $this->assertNotEquals($state, $trad);
        }
        $archive->set('state', 'unknown');
        $this->assertEquals('unknown', $archive->get('statetrad_title'));
    }
}
