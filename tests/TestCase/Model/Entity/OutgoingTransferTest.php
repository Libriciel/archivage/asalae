<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Entity\OutgoingTransfer;
use Cake\ORM\Entity;
use Cake\TestSuite\TestCase;
use DateTime;
use DOMUtility;

use const ASALAE_CORE_TEST_DATA;
use const DS;
use const SEDA_V02_XSD;
use const SEDA_V10_XSD;
use const SEDA_V21_XSD;

class OutgoingTransferTest extends TestCase
{
    /**
     * testGenerateXml
     */
    public function testGenerateXml()
    {
        $otr = new Entity(
            [
                'archival_agency_identifier' => 'aai',
                'archival_agency_name' => 'aan',
                'transferring_agency_identifier' => 'tai',
                'transferring_agency_name' => 'tan',
                'agreement_identifier' => 'ai',
                'profile_identifier' => 'pi',
                'service_level_identifier' => 'sli',
            ]
        );
        $util = DOMUtility::load(ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml');
        $descriptionXml = $this->createMock(DescriptionXmlArchiveFile::class);
        $descriptionXml->method('getDom')->willReturn($util->dom);
        $descriptionXml->method('getDomUtilWithoutDestructedBinaries')->willReturn($util);
        $archive = new Entity(
            [
                'archival_agency_identifier' => 'aai',
                'description_xml_archive_file' => $descriptionXml,
                'transferring_agency' => ['identifier' => 'atai', 'name' => 'atan'],
            ]
        );
        $entity = new OutgoingTransfer(
            [
                'identifier' => 'testunit',
                'comment' => 'test',
                'outgoing_transfer_request_id' => 1,
                'archive_unit_id' => 1,
                'state' => 'available',
                'outgoing_transfer_request' => $otr,
                'archive_unit' => [
                    'archive' => $archive,
                ],
                'created' => new DateTime(),
            ]
        );
        $dom = $entity->generateDom();
        $result = $dom->saveXML();
        foreach ($otr->toArray() as $val) {
            $this->assertStringContainsString($val, $result);
        }
        $this->assertTrue($dom->schemaValidate(SEDA_V10_XSD));

        // test seda 0.2
        $util = DOMUtility::load(ASALAE_CORE_TEST_DATA . DS . 'sample_seda02_description.xml');
        $descriptionXml = $this->createMock(DescriptionXmlArchiveFile::class);
        $descriptionXml->method('getDom')->willReturn($util->dom);
        $archive->set('description_xml_archive_file', $descriptionXml);
        $descriptionXml->method('getDomUtilWithoutDestructedBinaries')->willReturn($util);
        $dom = $entity->generateDom();
        $result = $dom->saveXML();
        $this->assertTrue($dom->schemaValidate(SEDA_V02_XSD));
        foreach ($otr->toArray() as $val) {
            $this->assertStringContainsString($val, $result);
        }

        // test seda 2.1
        $util = DOMUtility::load(ASALAE_CORE_TEST_DATA . DS . 'sample_seda21_description.xml');
        $descriptionXml = $this->createMock(DescriptionXmlArchiveFile::class);
        $descriptionXml->method('getDom')->willReturn($util->dom);
        $descriptionXml->method('getDomUtilWithoutDestructedBinaries')->willReturn($util);
        $archive->set('description_xml_archive_file', $descriptionXml);
        $dom = $entity->generateDom();
        $result = $dom->saveXML();
        $this->assertTrue($dom->schemaValidate(SEDA_V21_XSD));
        foreach ($otr->toArray() as $val) {
            $this->assertStringContainsString($val, $result);
        }
    }
}
