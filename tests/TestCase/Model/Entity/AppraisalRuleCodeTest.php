<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\AppraisalRuleCode;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class AppraisalRuleCodeTest extends TestCase
{
    public array $fixtures = [
        'app.AppraisalRuleCodes',
        'app.AppraisalRules',
        'app.OrgEntities',
    ];

    /**
     * testGetRule
     */
    public function testGetRule()
    {
        $entity = new AppraisalRuleCode(['duration' => 'P5Y']);
        $this->assertEquals('APP5Y', $entity->get('rule'));
    }

    /**
     * testGetDeletable
     */
    public function testGetDeletable()
    {
        $loc = TableRegistry::getTableLocator();
        $AppraisalRuleCodes = $loc->get('AppraisalRuleCodes');
        /** @var EntityInterface $serviceLevel2 */
        $appraisalRuleCode = $AppraisalRuleCodes->get(1);
        $this->assertFalse($appraisalRuleCode->get('deletable'));
        $appraisalRuleCode = $AppraisalRuleCodes->get(152);
        $this->assertTrue((bool)$appraisalRuleCode->get('deletable'));
        $appraisalRuleCode = $AppraisalRuleCodes->get(153);
        $this->assertFalse($appraisalRuleCode->get('deletable'));
    }
}
