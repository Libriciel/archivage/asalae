<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\TechnicalArchive;
use Asalae\Model\Table\TechnicalArchivesTable;
use Cake\Core\Configure;
use Cake\TestSuite\TestCase;

/**
 * Asalae\Model\Entity\TechnicalArchive Test Case
 */
class TechnicalArchiveTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Asalae\Model\Entity\TechnicalArchive
     */
    public $TechnicalArchive;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->TechnicalArchive = new TechnicalArchive();
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TechnicalArchive);

        parent::tearDown();
    }

    /**
     * testGetStatetrad
     */
    public function testGetStatetrad()
    {
        $trad = $this->TechnicalArchive->get('statetrad');
        $this->assertEmpty($trad);

        $states = [
            'creating',
            'describing',
            'storing',
            'available',
            'destroying',
        ];
        foreach ($states as $state) {
            $this->TechnicalArchive->set('state', $state);
            $trad = $this->TechnicalArchive->get('statetrad');
            $this->assertNotEmpty($trad);
            $this->assertNotEquals($state, $trad);
        }

        $this->TechnicalArchive->set('state', 'unknown');
        $trad = $this->TechnicalArchive->get('statetrad');
        $this->assertEquals('unknown', $trad);
    }

    /**
     * testEditable
     */
    public function testEditable()
    {
        $this->TechnicalArchive->set('type', TechnicalArchivesTable::TYPE_ADDED_BY_USER);
        $this->assertTrue($this->TechnicalArchive->get('editable'));
        $this->TechnicalArchive->set('type', TechnicalArchivesTable::TYPE_EVENTS_LOGS);
        $this->assertFalse($this->TechnicalArchive->get('editable'));
    }

    /**
     * testDeletable
     */
    public function testDeletable()
    {
        $this->TechnicalArchive->set('type', TechnicalArchivesTable::TYPE_ADDED_BY_USER);
        $this->assertTrue($this->TechnicalArchive->get('editable'));
        $this->TechnicalArchive->set('type', TechnicalArchivesTable::TYPE_EVENTS_LOGS);
        $this->assertFalse($this->TechnicalArchive->get('editable'));
    }
}
