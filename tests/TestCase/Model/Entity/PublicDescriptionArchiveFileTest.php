<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\PublicDescriptionArchiveFile;
use Asalae\Model\Volume\VolumeManager;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use Cake\TestSuite\TestCase;
use DOMDocument;
use DOMElement;

use const TMP_VOL1;
use const TMP_VOL2;

class PublicDescriptionArchiveFileTest extends TestCase
{
    use AutoFixturesTrait;

    public const string SEDA_02_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02_description.xml';
    public const string SEDA_10_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testGetDom
     */
    public function testGetDom()
    {
        $entity = new PublicDescriptionArchiveFile(
            [
                'stored_file_id' => 3,
            ]
        );
        $this->assertInstanceOf(DOMDocument::class, $entity->getDom());
    }

    /**
     * testGetHtml
     */
    public function testGetHtml()
    {
        $volumeManager = new VolumeManager(1);
        $volumeManager->replace(3, self::SEDA_10_DESCRIPTION);

        $entity = new PublicDescriptionArchiveFile(['stored_file_id' => 3]);
        $this->assertTextContains('<!DOCTYPE html', $entity->get('HTML'));

        $volumeManager->set(3, '<Archive></Archive>');
        $entity = new PublicDescriptionArchiveFile(['stored_file_id' => 3]);
        $this->assertFalse($entity->get('HTML'));

        $dom = new DOMDocument();
        $dom->load(self::SEDA_02_DESCRIPTION);
        foreach ($dom->getElementsByTagName('Attachment') as $attachment) {
            if ($attachment instanceof DOMElement && $attachment->getAttribute('filename')) {
                $attachment->removeAttribute('filename');
            }
        }
        $volumeManager->set(3, $dom->saveXML());
        $entity = new PublicDescriptionArchiveFile(['stored_file_id' => 3]);
        $this->assertTextNotContains('b64filename', $entity->get('HTML'));
    }

    /**
     * testGetIsLarge
     */
    public function testGetIsLarge()
    {
        $entity = new PublicDescriptionArchiveFile();
        $entity->set('stored_file', ['size' => 1]);
        $this->assertFalse($entity->get('is_large'));
        $entity->set('stored_file', ['size' => 50000000]);
        $this->assertTrue($entity->get('is_large'));
    }
}
