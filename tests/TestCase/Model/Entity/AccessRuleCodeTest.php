<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class AccessRuleCodeTest extends TestCase
{
    public array $fixtures = [
        'app.AccessRuleCodes',
        'app.AccessRules',
        'app.OrgEntities',
    ];

    /**
     * testGetDeletable
     */
    public function testGetDeletable()
    {
        $loc = TableRegistry::getTableLocator();
        $AccessRuleCodes = $loc->get('AccessRuleCodes');
        /** @var EntityInterface $serviceLevel2 */
        $accessRuleCode = $AccessRuleCodes->get(1);
        $this->assertFalse($accessRuleCode->get('deletable'));
        $accessRuleCode = $AccessRuleCodes->get(26);
        $this->assertTrue((bool)$accessRuleCode->get('deletable'));
        $accessRuleCode = $AccessRuleCodes->get(27);
        $this->assertFalse($accessRuleCode->get('deletable'));
    }
}
