<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\TransfersTable;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\XmlUtility;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cake\Utility\Hash;
use DOMDocument;
use DOMElement;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const SEDA_V21_XSD;
use const TMP_TESTDIR;
use const TMP_VOL1;
use const TMP_VOL2;

class TransferTest extends TestCase
{
    public const string SEDA_02_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xml';
    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';
    public const string SEDA_21_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda21.xml';
    public const string SEDA_02_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02_description.xml';
    public const string SEDA_10_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml';

    public array $fixtures = [
        'app.Acos',
        'app.Agreements',
        'app.ArchiveFiles',
        'app.ArchiveUnits',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.Configurations',
        'app.Counters',
        'app.EventLogs',
        'app.Fileuploads',
        'app.OrgEntities',
        'app.Profiles',
        'app.SecureDataSpaces',
        'app.Sequences',
        'app.ServiceLevels',
        'app.Siegfrieds',
        'app.StoredFiles',
        'app.StoredFilesVolumes',
        'app.TransferAttachments',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
        'app.Volumes',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('hash_algo', 'md5');
        Configure::write('App.defaultLocale', 'fr_FR');

        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Filesystem::setNamespace();
        Filesystem::reset();
        VolumeSample::destroy();
    }

    /**
     * testCreateResponse
     */
    public function testCreateResponse()
    {
        TableRegistry::getTableLocator()->get('Archives')->updateAll(['state' => 'available'], []);

        $transferXml = TMP_TESTDIR . DS . 'transfers' . DS . '1' . DS . 'message' . DS . 'transfer-61b20d8ded833.xml';
        Filesystem::copy(self::SEDA_10_XML, $transferXml);

        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        /** @var Transfer $transfer */
        $transfer = $Transfers->get(1);

        $transfer->set('message_version', 'seda0.2');
        $response = $transfer->createResponse('Acknowledgement');
        $this->assertEquals(
            $transfer->get('transfer_identifier'),
            Hash::get($response, 'ArchiveTransferReply.TransferIdentifier')
        );

        $transfer->set('message_version', 'seda1.0');
        $response = $transfer->createResponse('Acknowledgement');
        $this->assertEquals(
            $transfer->get('transfer_identifier'),
            Hash::get($response, 'Acknowledgement.MessageReceivedIdentifier')
        );

        $transfer->set('is_accepted', true);
        $transfer->set('message_version', 'seda0.2');
        $response = $transfer->createResponse('ArchiveTransferReply');
        $this->assertEquals('seda1.0', Hash::get($response, 'ArchiveTransferAcceptance.Archive.Name'));

        $transfer->set('is_accepted', true);
        $transfer->set('message_version', 'seda1.0');
        $response = $transfer->createResponse('ArchiveTransferReply');
        $this->assertEquals('seda1.0', Hash::get($response, 'ArchiveTransferReply.Archive.Name'));

        $transfer->set('is_accepted', false);
        $transfer->set('message_version', 'seda0.2');
        $response = $transfer->createResponse('ArchiveTransferReply');
        $this->assertEquals(
            $transfer->get('transfer_identifier'),
            Hash::get($response, 'ArchiveTransferReply.TransferIdentifier')
        );

        $transfer->set('is_accepted', false);
        $transfer->set('message_version', 'seda1.0');
        $response = $transfer->createResponse('ArchiveTransferReply');
        $this->assertNull(Hash::get($response, 'ArchiveTransferReply.Archive.Name'));
        $this->assertEquals('seda1.0', Hash::get($response, 'ArchiveTransferReply.TransferIdentifier'));
    }

    // createAcknowledgement
    // createAcknowledgement10

    /**
     * testCreateAcknowledgement21
     */
    public function testCreateAcknowledgement21()
    {
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        /** @var Transfer $transfer */
        $transfer = $Transfers->get(3);
        $xml = XmlUtility::arrayToXmlString(
            $transfer->createResponse('Acknowledgement')
        );
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $this->assertTrue($dom->schemaValidate(SEDA_V21_XSD));
    }

    // createArchiveTransferAcceptance02
    // createArchiveTransferReply
    // createArchiveTransferReply02
    // createArchiveTransferReply10

    /**
     * testCreateArchiveTransferReply21
     */
    public function testCreateArchiveTransferReply21()
    {
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        /** @var Transfer $transfer */
        $transfer = $Transfers->get(1);
        $xml = XmlUtility::arrayToXmlString(
            $transfer->createResponse('ArchiveTransferReply')
        );
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $this->assertTrue($dom->schemaValidate(SEDA_V10_XSD));
    }

    // fillMissingHashes

    /**
     * testFillMissingHashes02
     */
    public function testFillMissingHashes02()
    {
        $tmpFile = uniqid('testunit-');
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        /** @var Transfer $transfer */
        $transfer = $Transfers->newEntity(
            [
                'id' => 9999,
                'filename' => $tmpFile,
                'message_version' => 'seda0.2',
                'files_deleted' => false,
            ]
        );

        $tmpXml = $transfer->get('xml');
        Filesystem::copy(self::SEDA_02_XML, $tmpXml);

        $files = [
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.odt',
                ],
                ['validate' => false]
            ),
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.pdf',
                ],
                ['validate' => false]
            ),
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.rng',
                ],
                ['validate' => false]
            ),
        ];
        /** @var EntityInterface $file */
        foreach ($files as $key => $file) {
            Filesystem::dumpFile($file->get('path'), sha1($key));
        }

        /**
         * Test 1: aucuns Integrity
         */
        $domutil = $transfer->getUtil();
        /** @var \DOMElement $Integrity */
        foreach ($domutil->xpath->query('//ns:Integrity') as $Integrity) {
            $Integrity->parentNode->removeChild($Integrity);
        }
        $this->assertEquals(0, $domutil->xpath->query('//ns:Integrity')->count());
        $this->assertTrue($transfer->fillMissingHashes($files));
        $this->assertEquals(3, $domutil->xpath->query('//ns:Integrity')->count());

        /**
         * Integrity OK
         */
        $this->assertTrue($transfer->fillMissingHashes($files));
        /** @var DOMElement $integrity */
        $integrity = $domutil->xpath->query('//ns:Integrity/ns:Contains')->item(0);
        $this->assertNotNull($integrity);
        $this->assertEquals('md5', $integrity->getAttribute('algorithme'));
        $this->assertEquals(md5_file($files[0]->get('path')), $integrity->nodeValue);

        /**
         * Integrity from TransferAttachments
         */
        foreach ($domutil->xpath->query('//ns:Integrity') as $Integrity) {
            $Integrity->parentNode->removeChild($Integrity);
        }
        $files = [
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.odt',
                    'hash' => 'foo',
                    'hash_algo' => 'md5',
                ],
                ['validate' => false]
            ),
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.pdf',
                    'hash' => 'foo',
                    'hash_algo' => 'md5',
                ],
                ['validate' => false]
            ),
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.rng',
                    'hash' => 'foo',
                    'hash_algo' => 'md5',
                ],
                ['validate' => false]
            ),
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'notexists',
                    'hash' => 'foo',
                    'hash_algo' => 'md5',
                ],
                ['validate' => false]
            ),
        ];
        $this->assertTrue($transfer->fillMissingHashes($files));
        $domutil = DOMUtility::load($tmpXml);
        /** @var DOMElement $integrity */
        $integrity = $domutil->xpath->query('//ns:Integrity/ns:Contains')->item(0);
        $this->assertNotNull($integrity);
        $this->assertEquals('md5', $integrity->getAttribute('algorithme'));
        $this->assertEquals('foo', $integrity->nodeValue);

        /**
         * Integrity checked (repaired)
         */
        $this->assertTrue($transfer->fillMissingHashes($files));
        $domutil = DOMUtility::load($tmpXml);
        /** @var DOMElement $integrity */
        $integrity = $domutil->xpath->query('//ns:Integrity/ns:Contains')->item(0);
        $this->assertNotNull($integrity);
        $this->assertEquals('md5', $integrity->getAttribute('algorithme'));
        $this->assertEquals(md5_file($files[0]->get('path')), $integrity->nodeValue);

        /**
         * Unknown algo
         */
        /** @var \DOMElement $contain */
        foreach ($domutil->xpath->query('//ns:Integrity/ns:Contains') as $contain) {
            $contain->setAttribute('algorithme', 'http://asalae.fr#foo');
        }
        $this->assertTrue($transfer->fillMissingHashes($files));
        $domutil = DOMUtility::load($tmpXml);
        /** @var DOMElement $integrity */
        $integrity = $domutil->xpath->query('//ns:Integrity/ns:Contains')->item(0);
        $this->assertNotNull($integrity);
        $this->assertEquals('md5', $integrity->getAttribute('algorithme'));
        $this->assertEquals(md5_file($files[0]->get('path')), $integrity->nodeValue);
    }

    /**
     * testFillMissingHashes10
     */
    public function testFillMissingHashes10()
    {
        $tmpFile = uniqid('testunit-');
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        /** @var Transfer $transfer */
        $transfer = $Transfers->newEntity(
            [
                'id' => 9999,
                'filename' => $tmpFile,
                'message_version' => 'seda1.0',
                'files_deleted' => false,
            ]
        );

        $tmpXml = $transfer->get('xml');
        Filesystem::copy(self::SEDA_10_XML, $tmpXml);

        $files = [
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.odt',
                ],
                ['validate' => false]
            ),
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.pdf',
                ],
                ['validate' => false]
            ),
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.rng',
                ],
                ['validate' => false]
            ),
        ];
        /** @var EntityInterface $file */
        foreach ($files as $key => $file) {
            Filesystem::dumpFile($file->get('path'), sha1($key));
        }

        /**
         * Test 1: aucuns Integrity
         */
        $domutil = $transfer->getUtil();
        /** @var \DOMElement $Integrity */
        foreach ($domutil->xpath->query('//ns:Integrity') as $Integrity) {
            $Integrity->parentNode->removeChild($Integrity);
        }
        $this->assertEquals(0, $domutil->xpath->query('//ns:Integrity')->count());
        $this->assertTrue($transfer->fillMissingHashes($files));
        $this->assertEquals(3, $domutil->xpath->query('//ns:Integrity')->count());

        /**
         * Integrity OK
         */
        $this->assertTrue($transfer->fillMissingHashes($files));
        /** @var DOMElement $integrity */
        $integrity = $domutil->xpath->query('//ns:Integrity')->item(0);
        $this->assertNotNull($integrity);
        $this->assertEquals('md5', $integrity->getAttribute('algorithme'));
        $this->assertEquals(md5_file($files[2]->get('path')), $integrity->nodeValue);

        /**
         * Integrity from TransferAttachments
         */
        foreach ($domutil->xpath->query('//ns:Integrity') as $Integrity) {
            $Integrity->parentNode->removeChild($Integrity);
        }
        $domutil->dom->save($tmpXml);
        $files = [
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.odt',
                    'hash' => 'foo',
                    'hash_algo' => 'md5',
                ],
                ['validate' => false]
            ),
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.pdf',
                    'hash' => 'foo',
                    'hash_algo' => 'md5',
                ],
                ['validate' => false]
            ),
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.rng',
                    'hash' => 'foo',
                    'hash_algo' => 'md5',
                ],
                ['validate' => false]
            ),
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'notexists',
                    'hash' => 'foo',
                    'hash_algo' => 'md5',
                ],
                ['validate' => false]
            ),
        ];
        $transfer->reloadUtil();
        $this->assertTrue($transfer->fillMissingHashes($files));
        $domutil = DOMUtility::load($tmpXml);
        /** @var DOMElement $integrity */
        $integrity = $domutil->xpath->query('//ns:Integrity')->item(0);
        $this->assertNotNull($integrity);
        $this->assertEquals('md5', $integrity->getAttribute('algorithme'));
        $this->assertEquals('foo', $integrity->nodeValue);

        /**
         * Integrity checked (repaired)
         */
        $this->assertTrue($transfer->fillMissingHashes($files));
        $domutil = DOMUtility::load($tmpXml);
        /** @var DOMElement $integrity */
        $integrity = $domutil->xpath->query('//ns:Integrity')->item(0);
        $this->assertNotNull($integrity);
        $this->assertEquals('md5', $integrity->getAttribute('algorithme'));
        $this->assertEquals(md5_file($files[2]->get('path')), $integrity->nodeValue);

        /**
         * Unknown algo
         */
        /** @var \DOMElement $contain */
        foreach ($domutil->xpath->query('//ns:Integrity') as $contain) {
            $contain->setAttribute('algorithme', 'http://asalae.fr#foo');
        }
        $domutil->dom->save($tmpXml);
        $this->assertTrue($transfer->fillMissingHashes($files));
        $domutil = DOMUtility::load($tmpXml);
        /** @var DOMElement $integrity */
        $integrity = $domutil->xpath->query('//ns:Integrity')->item(0);
        $this->assertNotNull($integrity);
        $this->assertEquals('md5', $integrity->getAttribute('algorithme'));
        $this->assertEquals(md5_file($files[2]->get('path')), $integrity->nodeValue);
    }

    /**
     * testFillMissingHashes21
     */
    public function testFillMissingHashes21()
    {
        $tmpFile = uniqid('testunit-');
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        /** @var Transfer $transfer */
        $transfer = $Transfers->newEntity(
            [
                'id' => 9999,
                'filename' => $tmpFile,
                'message_version' => 'seda2.1',
                'files_deleted' => false,
            ]
        );

        $tmpXml = $transfer->get('xml');
        Filesystem::copy(self::SEDA_21_XML, $tmpXml);

        $files = [
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.pdf',
                ],
                ['validate' => false]
            ),
        ];
        /** @var EntityInterface $file */
        foreach ($files as $key => $file) {
            Filesystem::dumpFile($file->get('path'), sha1($key));
        }

        /**
         * Test 1: aucuns Integrity
         */
        $domutil = $transfer->getUtil();
        /** @var \DOMElement $Integrity */
        foreach ($domutil->xpath->query('//ns:MessageDigest') as $Integrity) {
            $Integrity->parentNode->removeChild($Integrity);
        }
        $this->assertEquals(0, $domutil->xpath->query('//ns:MessageDigest')->count());
        $this->assertTrue($transfer->fillMissingHashes($files));
        $this->assertEquals(1, $domutil->xpath->query('//ns:MessageDigest')->count());

        /**
         * Integrity OK
         */
        $this->assertTrue($transfer->fillMissingHashes($files));
        /** @var DOMElement $integrity */
        $integrity = $domutil->xpath->query('//ns:MessageDigest')->item(0);
        $this->assertNotNull($integrity);
        $this->assertEquals('md5', $integrity->getAttribute('algorithm'));
        $this->assertEquals(md5_file($files[0]->get('path')), $integrity->nodeValue);

        /**
         * Integrity from TransferAttachments
         */
        foreach ($domutil->xpath->query('//ns:MessageDigest') as $Integrity) {
            $Integrity->parentNode->removeChild($Integrity);
        }
        $domutil->dom->save($tmpXml);
        $files = [
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'sample.pdf',
                    'hash' => 'foo',
                    'hash_algo' => 'md5',
                ],
                ['validate' => false]
            ),
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => 9999,
                    'filename' => 'notexists',
                    'hash' => 'foo',
                    'hash_algo' => 'md5',
                ],
                ['validate' => false]
            ),
        ];
        $this->assertTrue($transfer->fillMissingHashes($files));
        $domutil = DOMUtility::load($tmpXml);
        /** @var DOMElement $integrity */
        $integrity = $domutil->xpath->query('//ns:MessageDigest')->item(0);
        $this->assertNotNull($integrity);
        $this->assertEquals('md5', $integrity->getAttribute('algorithm'));
        $this->assertEquals('foo', $integrity->nodeValue);

        /**
         * Integrity checked (repaired)
         */
        $this->assertTrue($transfer->fillMissingHashes($files));
        $domutil = DOMUtility::load($tmpXml);
        /** @var DOMElement $integrity */
        $integrity = $domutil->xpath->query('//ns:MessageDigest')->item(0);
        $this->assertNotNull($integrity);
        $this->assertEquals('md5', $integrity->getAttribute('algorithm'));
        $this->assertEquals(md5_file($files[0]->get('path')), $integrity->nodeValue);

        /**
         * Unknown algo
         */
        /** @var \DOMElement $contain */
        foreach ($domutil->xpath->query('//ns:MessageDigest') as $contain) {
            $contain->setAttribute('algorithm', 'http://asalae.fr#foo');
        }
        $domutil->dom->save($tmpXml);
        $this->assertTrue($transfer->fillMissingHashes($files));
        $domutil = DOMUtility::load($tmpXml);
        /** @var DOMElement $integrity */
        $integrity = $domutil->xpath->query('//ns:MessageDigest')->item(0);
        $this->assertNotNull($integrity);
        $this->assertEquals('md5', $integrity->getAttribute('algorithm'));
        $this->assertEquals(md5_file($files[0]->get('path')), $integrity->nodeValue);
    }

    // _getXSL
    // _getHTML
    // _getMessageVersiontrad
    // _getStatetrad
    // _getStatetradTitle

    /**
     * testZipCreationTimeEstimation
     */
    public function testZipCreationTimeEstimation()
    {
        $loc = TableRegistry::getTableLocator();
        $TransferAttachments = $loc->get('TransferAttachments');
        $entity = $TransferAttachments->get(1);
        $entity->set('size', 2000000000);
        $TransferAttachments->saveOrFail($entity);
        $Transfers = $loc->get('Transfers');
        /** @var Transfer $transfer */
        $transfer = $Transfers->get(1);
        $this->assertGreaterThan(100, $transfer->zipCreationTimeEstimation());
    }

    // getForm
    // validateFile

    /**
     * testAutoRepair
     */
    public function testAutoRepair()
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $file = $TransferAttachments->get(3);
        Filesystem::dumpFile($file->get('path'), 'test');

        /** @var Transfer $entity */
        $entity = $Transfers->get(3);
        $tmpXml = $entity->get('xml');
        Filesystem::copy(self::SEDA_21_XML, $tmpXml);
        $util = DOMUtility::load($tmpXml);
        $this->assertCount(1, $util->xpath->query('//ns:MessageDigest'));
        /** @var DOMElement $integrity */
        foreach ($util->xpath->query('//ns:MessageDigest') as $integrity) {
            $integrity->parentNode->removeChild($integrity);
        }
        $this->assertCount(0, $util->xpath->query('//ns:MessageDigest'));
        $util->dom->save($tmpXml);
        $entity->autoRepair();
        $util = DOMUtility::load($tmpXml);
        $this->assertCount(1, $util->xpath->query('//ns:MessageDigest'));
    }

    // appendNode
    // listFiles
    // _getXml
    // _setStartDate

    /**
     * testSetFinalStartDate
     */
    public function testSetFinalStartDate()
    {
        $entity = new Transfer();
        $entity->set('final_start_date', '01/02/2003');
        $this->assertEquals('2003-02-01', $entity->get('final_start_date'));
    }

    // generateXml

    /**
     * testGenerateArchiveContain
     */
    public function testGenerateArchiveContain()
    {
        // Préparation du test
        $loc = TableRegistry::getTableLocator();
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        $TransferAttachments = $loc->get('TransferAttachments');
        $xml = uniqid('transfer-') . '.xml';
        /** @var Transfer $transfer */
        $transfer = $Transfers->newEntity(
            [
                'message_version' => 'seda1.0',
                'state' => $Transfers->initialState,
                'is_modified' => false,
                'filename' => $xml,
                'archival_agency_id' => 2,
                'transfer_identifier' => 'testunit',
                'transfer_date' => '03/04/2019 14:23',
                'transfer_comment' => '',
                'transferring_agency_id' => '2',
                'originating_agency_id' => '2',
                'agreement_id' => '',
                'profile_id' => '',
                'service_level_id' => '',
                'name' => 'Archive',
                'code' => 'AR038',
                'start_date' => '03/04/2019',
                'created_user_id' => 1,
                'files_deleted' => false,
            ]
        );
        $Transfers->saveOrFail($transfer);
        Filesystem::dumpFile($transfer->get('xml'), $transfer->generateXml());
        $attachment = $TransferAttachments->newEntity(
            [
                'transfer_id' => $transfer->get('id'),
                'filename' => 'object1/object2/filename.zip',
                'size' => 1,
                'hash' => 'foo',
                'hash_algo' => 'bar',
                'valid' => null,
                'mime' => 'application/zip',
                'extension' => 'zip',
                'format' => 'fmt/14',
            ]
        );
        $TransferAttachments->saveOrFail($attachment);

        // test
        $transfer->generateArchiveContain([$attachment]);
        $xpath = $transfer->getUtil()->xpath;
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:Archive')->item(0);
        $this->assertNotEmpty($archiveNode);
        $obj1 = $xpath->query('ns:ArchiveObject', $archiveNode)->item(0);
        $this->assertNotEmpty($obj1);
        $this->assertEquals('object1', (string)$xpath->query('ns:Name', $obj1)->item(0)->nodeValue);
        $obj2 = $xpath->query('ns:ArchiveObject', $obj1)->item(0);
        $this->assertNotEmpty($obj2);
        $this->assertEquals('object2', (string)$xpath->query('ns:Name', $obj2)->item(0)->nodeValue);
        $doc = $xpath->query('ns:Document', $obj2)->item(0);
        $this->assertNotEmpty($doc);
        /** @var DOMElement $attachmentNode */
        $attachmentNode = $xpath->query('ns:Attachment', $doc)->item(0);
        $this->assertEquals('object1/object2/filename.zip', $attachmentNode->getAttribute('filename'));
        $this->assertEquals('application/zip', $attachmentNode->getAttribute('mimeCode'));
        $this->assertEquals('fmt/14', $attachmentNode->getAttribute('format'));
    }

    /**
     * testGenerateArchiveContainSeda21
     */
    public function testGenerateArchiveContainSeda21()
    {
        // Préparation du test
        $loc = TableRegistry::getTableLocator();
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        $TransferAttachments = $loc->get('TransferAttachments');
        $xml = uniqid('transfer-') . '.xml';
        /** @var Transfer $transfer */
        $transfer = $Transfers->newEntity(
            [
                'message_version' => 'seda2.1',
                'state' => $Transfers->initialState,
                'is_modified' => false,
                'filename' => $xml,
                'archival_agency_id' => 2,
                'transfer_identifier' => 'testunit',
                'transfer_date' => '03/04/2019 14:23',
                'transfer_comment' => '',
                'transferring_agency_id' => '2',
                'originating_agency_id' => '2',
                'description_level' => 'file',
                'agreement_id' => '',
                'profile_id' => '',
                'service_level_id' => '',
                'name' => 'Archive',
                'code' => 'AR038',
                'start_date' => '03/04/2019',
                'created_user_id' => 1,
                'files_deleted' => false,
            ]
        );
        $Transfers->saveOrFail($transfer);
        Filesystem::dumpFile($transfer->get('xml'), $transfer->generateXml());
        $attachment = $TransferAttachments->newEntity(
            [
                'transfer_id' => $transfer->get('id'),
                'filename' => 'object1/object2/filename.zip',
                'size' => 1,
                'hash' => 'foo',
                'hash_algo' => 'bar',
                'valid' => null,
                'mime' => 'application/zip',
                'extension' => 'zip',
                'format' => 'fmt/14',
            ]
        );
        $TransferAttachments->saveOrFail($attachment);

        // test
        $transfer->generateArchiveContain([$attachment]);
        $xpath = $transfer->getUtil()->xpath;
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata')->item(0);
        $this->assertNotEmpty($archiveNode);
        $obj1 = $xpath->query('ns:ArchiveUnit', $archiveNode)->item(0);
        $this->assertNotEmpty($obj1);
        $this->assertEquals(
            'object1',
            (string)$xpath->query('ns:ArchiveUnit/ns:Content/ns:Title', $obj1)->item(0)->nodeValue
        );
        $obj2 = $xpath->query('ns:ArchiveUnit', $obj1)->item(0);
        $this->assertNotEmpty($obj2);
        $this->assertEquals(
            'object2',
            (string)$xpath->query('ns:ArchiveUnit/ns:Content/ns:Title', $obj2)->item(0)->nodeValue
        );


        $doc = $xpath->query('ns:ArchiveUnit/ns:ArchiveUnit/ns:Content', $obj2)->item(0);
        $this->assertNotEmpty($doc);
        /** @var DOMElement $attachment */
        $attachment = $xpath->query('//ns:Attachment', $doc)->item(0);
        $this->assertEquals('object1/object2/filename.zip', $attachment->getAttribute('filename'));
    }

    /**
     * testGenerateArchiveContain21
     */
    public function testGenerateArchiveContain21()
    {
        // Préparation du test
        $loc = TableRegistry::getTableLocator();
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        $TransferAttachments = $loc->get('TransferAttachments');
        $xml = uniqid('transfer-') . '.xml';
        /** @var Transfer $transfer */
        $transfer = $Transfers->newEntity(
            [
                'message_version' => 'seda2.1',
                'state' => $Transfers->initialState,
                'is_modified' => false,
                'filename' => $xml,
                'archival_agency_id' => 2,
                'transfer_identifier' => 'testunit',
                'transfer_date' => '03/04/2019 14:23',
                'transfer_comment' => '',
                'transferring_agency_id' => '2',
                'originating_agency_id' => '2',
                'agreement_id' => '',
                'profile_id' => '',
                'service_level_id' => '',
                'name' => 'Archive',
                'code' => 'AR038',
                'start_date' => '03/04/2019',
                'created_user_id' => 1,
                'files_deleted' => false,
            ]
        );
        $Transfers->saveOrFail($transfer);
        Filesystem::dumpFile($transfer->get('xml'), $transfer->generateXml());
        $attachment = $TransferAttachments->newEntity(
            [
                'transfer_id' => $transfer->get('id'),
                'filename' => 'object1/object2/filename.zip',
                'size' => 1,
                'hash' => 'foo',
                'hash_algo' => 'bar',
                'valid' => null,
                'mime' => 'application/zip',
                'extension' => 'zip',
                'format' => 'fmt/14',
            ]
        );
        $TransferAttachments->saveOrFail($attachment);

        // test
        $transfer->generateArchiveContain([$attachment]);
        $xpath = $transfer->getUtil()->xpath;
        $archiveNode = $xpath->query('/ns:ArchiveTransfer/ns:DataObjectPackage')->item(0);
        $doc = $xpath->query('ns:BinaryDataObject', $archiveNode)->item(0);
        $this->assertNotEmpty($doc);
        /** @var DOMElement $attachmentNode */
        $attachmentNode = $xpath->query('ns:Attachment', $doc)->item(0);
        $this->assertEquals('object1/object2/filename.zip', $attachmentNode->getAttribute('filename'));
    }

    // findOrCreateArchiveObject
    // appendArchiveObjectToArchiveObject
    // _getDeletable
    // getTempXml
    // getZipDownloadFolder

    /**
     * testGetZipTempFolder
     */
    public function testGetZipTempFolder()
    {
        $transfer = new Transfer(['id' => 1]);
        $this->assertNotEmpty($transfer->getZipTempFolder());
    }

    // getSerializedMeta
    // getEditedLogs
    // saveChangesAsXml
    // _getIsLarge
    // _getEditable

    /**
     * testGetArchiveXml
     */
    public function testGetArchiveXml()
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $ArchiveUnits->deleteAll(['id' => 16]);
        /** @var Transfer $entity */
        $entity = $Transfers->get(3);
        $tmpXml = $entity->get('xml');
        Filesystem::copy(self::SEDA_21_XML, $tmpXml);
        $this->assertNotEmpty($entity->getArchiveXml(4));
    }

    /**
     * testGetAcknowledgementFilename
     */
    public function testGetAcknowledgementFilename()
    {
        $transfer = new Transfer();
        $this->assertNull($transfer->get('acknowledgement_filename'));

        $transfer->set('state', 'accepted');
        $transfer->set('message_version', 'seda0.2');
        $transfer->set('transfer_identifier', 'testunit');
        $this->assertStringContainsString('ArchiveTransferReply', $transfer->get('acknowledgement_filename'));
        $transfer->set('message_version', 'seda1.0');
        $this->assertStringContainsString('Acknowledgement', $transfer->get('acknowledgement_filename'));
    }

    /**
     * testGetNotifyFilename
     */
    public function testGetNotifyFilename()
    {
        $transfer = new Transfer();
        $this->assertNull($transfer->get('acknowledgement_filename'));

        $transfer->set('state', 'accepted');
        $transfer->set('message_version', 'seda0.2');
        $transfer->set('transfer_identifier', 'testunit');
        $this->assertStringContainsString('ArchiveTransferAcceptance', $transfer->get('notify_filename'));
        $transfer->set('message_version', 'seda1.0');
        $this->assertStringContainsString('ArchiveTransferReply', $transfer->get('notify_filename'));
        $transfer->set('state', 'rejected');
        $transfer->set('message_version', 'seda0.2');
        $this->assertStringContainsString('ArchiveTransferReply-rep', $transfer->get('notify_filename'));
    }

    // _getHash
    // toPremisObject

    /**
     * testSaveAttachmentChangeTransferDataSizeAndDataCount
     */
    public function testSaveAttachmentChangeTransferDataSizeAndDataCount()
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');

        $transfer = $Transfers->get(1);
        $this->assertEquals(554297, $transfer->get('data_size'));
        $this->assertEquals(1, $transfer->get('data_count'));

        $attachment = $TransferAttachments->newEntity(
            [
                'filename' => 'foo',
                'transfer_id' => 1,
                'size' => 10,
            ]
        );
        $TransferAttachments->save($attachment);
        $transfer = $Transfers->get(1);
        $this->assertEquals(554307, $transfer->get('data_size'));
        $this->assertEquals(2, $transfer->get('data_count'));
    }

    /**
     * testDeleteAttachmentChangeTransferDataSizeAndDataCount
     */
    public function testDeleteAttachmentChangeTransferDataSizeAndDataCount()
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Transfers->updateAll(['state' => TransfersTable::S_PREPARATING], ['id' => 1]);
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');

        $transfer = $Transfers->get(1);
        $this->assertEquals(554297, $transfer->get('data_size'));
        $this->assertEquals(1, $transfer->get('data_count'));

        $attachment = $TransferAttachments->get(1);
        $attachment->set('force_delete', true);
        $TransferAttachments->delete($attachment);
        $transfer = $Transfers->get(1);
        $this->assertEquals(0, $transfer->get('data_size'));
        $this->assertEquals(0, $transfer->get('data_count'));
    }
}
