<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class ValidationChainsTest extends TestCase
{
    public array $fixtures = [
        'app.Agreements',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.OrgEntities',
        'app.Transfers',
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];

    /**
     * testDeletable
     */
    public function testDeletable()
    {
        $chains = TableRegistry::getTableLocator()->get('ValidationChains')
            ->find()
            ->where(['id <=' => 5])
            ->all()
            ->map(
                function ($e) {
                    return [$e->get('id'), $e->get('deletable')];
                }
            )
            ->toArray();

        $expect = [
            [1, false], // a un process
            [2, false], // ?
            [3, false],  // ?
            [4, false], // default, possede stages
            [5, false], // default, possede stages
        ];
        $this->assertEquals($expect, $chains);

        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $Agreements->saveOrFail(
            $Agreements->newEntity(
                [
                    'identifier' => 'test',
                    'name' => 'test',
                    'description' => 'test',
                    'active' => true,
                    'allowed_formats' => '["mime-application/vnd.oasis.opendocument.text","pronom-fmt/18","ext-rng"]',
                    'org_entity_id' => 2,
                    'improper_chain_id' => 2,
                ]
            )
        );
        $chain = TableRegistry::getTableLocator()->get('ValidationChains')->get(2);
        $this->assertFalse($chain->get('deletable'));
    }

    /**
     * testUsable
     */
    public function testUsable()
    {
        $ValidationChains = TableRegistry::getTableLocator()->get('ValidationChains');
        /** @var EntityInterface $usableChain */
        $usableChain = $ValidationChains->get(1);
        $this->assertTrue($usableChain->get('usable'));

        $notUsableChain = $ValidationChains->newEntity(
            [
                'name' => uniqid(),
                'description' => '',
                'org_entity_id' => 2,
            ]
        );
        $ValidationChains->saveOrFail($notUsableChain);
        // pas de stages ni d'actors, n'est donc pas usable
        $this->assertFalse($notUsableChain->get('usable'));
    }
}
