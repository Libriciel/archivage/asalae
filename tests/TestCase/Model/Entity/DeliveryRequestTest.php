<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\DeliveryRequest;
use Cake\TestSuite\TestCase;
use DateTime;
use DOMDocument;
use const SEDA_V21_XSD;

class DeliveryRequestTest extends TestCase
{
    public array $fixtures = [
        'app.Acos',
        'app.ArchiveUnits',
        'app.ArchiveUnitsDeliveryRequests',
    ];

    /**
     * testGenerateXml
     */
    public function testGenerateXml()
    {
        $entity = new DeliveryRequest(
            [
                'id' => 1,
                'identifier' => 'test001',
                'derogation' => true,
                'archive_units' => [
                    ['archival_agency_identifier' => 'archive_unit_1'],
                    ['archival_agency_identifier' => 'archive_unit_2'],
                ],
                'created' => new DateTime(),
                'archival_agency' => ['identifier' => 'archival_agency'],
                'requester' => ['identifier' => 'requester'],
            ],
            ['validate' => false]
        );
        $dom = new DOMDocument();
        $this->assertNotNull($xml = $entity->generateXml());
        $this->assertTrue($dom->loadXML($xml));
        $this->assertTrue($dom->schemaValidate(SEDA_V21_XSD));
    }
}
