<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Controller\ArchiveUnitsController;
use Asalae\Model\Entity\ArchiveUnit;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use DOMDocument;
use Exception;

class ArchiveUnitTest extends TestCase
{
    use InvokePrivateTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.AccessRules',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveKeywords',
        'app.ArchiveUnits',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.OrgEntities',
        'app.Transfers',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
    }

    /**
     * tearDown
     * @return void
     * @throws Exception
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testZipCreationTimeEstimation
     */
    public function testZipCreationTimeEstimation()
    {
        $archiveUnit = new ArchiveUnit(['id' => 1]);
        $archiveUnit->zipCreationTimeEstimation();// pour la couverture du code uniquement
        $archiveUnit->set('original_total_size', 100000000);
        $archiveUnit->set('original_total_count', 100000000);
        $this->assertGreaterThan(100, $archiveUnit->zipCreationTimeEstimation());
    }

    /**
     * testGetXpath
     */
    public function testGetXpath()
    {
        $archiveUnit = new ArchiveUnit();
        $this->assertEmpty($archiveUnit->get('xpath'));

        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $unit = $ArchiveUnits->get(2);
        $xpath = $unit->get('xpath');
        $this->assertEquals('/ns:Archive/ns:Document[1]', $xpath);

        $unit->set('context', 'Archive');
        $xpath = $unit->get('xpath');
        $this->assertEquals('/ns:Archive/ns:Document[1]', $xpath);

        $unit->set('context', 'ArchiveTransfer');
        $xpath = $unit->get('xpath');
        $this->assertEquals('/ns:ArchiveTransfer/ns:Archive[1]/ns:Document[1]', $xpath);
    }

    /**
     * testGetPublicDom
     */
    public function testGetPublicDom()
    {
        VolumeSample::insertSample(
            'sample_seda10_description.xml',
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
        );

        $controller = new ArchiveUnitsController(new ServerRequest());
        $component = $controller->loadComponent('Seal');
        $component->setConfig(['userId' => 1, 'archivalAgencyId' => 2]);
        $this->invokeProperty($controller, 'archivalAgencyId', 'set', 2);
        $this->invokeMethod($controller, 'getPublicDescription', [1]);

        $archiveUnit = new ArchiveUnit(
            ['archive_id' => 1, 'archival_agency_identifier' => "archive_unit_1"],
            ['validation' => false]
        );
        $dom = $archiveUnit->getPublicDom();
        $this->assertInstanceOf(DOMDocument::class, $dom);
    }

    /**
     * testGetPublicHTML
     */
    public function testGetPublicHTML()
    {
        VolumeSample::insertSample(
            'sample_seda10_description.xml',
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
        );

        $controller = new ArchiveUnitsController(new ServerRequest());
        $component = $controller->loadComponent('Seal');
        $component->setConfig(['userId' => 1, 'archivalAgencyId' => 2]);
        $this->invokeProperty($controller, 'archivalAgencyId', 'set', 2);
        $this->invokeMethod($controller, 'getPublicDescription', [1]);

        $archiveUnit = new ArchiveUnit(
            ['archive_id' => 1, 'archival_agency_identifier' => "archive_unit_1", 'original_local_count' => 1],
            ['validation' => false]
        );
        $html = $archiveUnit->get('publicHTML');
        $this->assertNotEmpty($html);
    }

    /**
     * testGetHTML
     */
    public function testGetHTML()
    {
        VolumeSample::insertSample(
            'sample_seda10_description.xml',
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
        );

        $archiveUnit = new ArchiveUnit(
            ['archive_id' => 1, 'archival_agency_identifier' => "archive_unit_1", 'original_local_count' => 1],
            ['validation' => false]
        );
        $html = $archiveUnit->get('HTML');
        $this->assertNotEmpty($html);
    }
}
