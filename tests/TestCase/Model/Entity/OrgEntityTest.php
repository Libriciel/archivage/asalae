<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\OrgEntity;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\Exec;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class OrgEntityTest extends TestCase
{
    public array $fixtures = [
        'app.AccessRuleCodes',
        'app.Agreements',
        'app.AgreementsOriginatingAgencies',
        'app.AgreementsTransferringAgencies',
        'app.AppraisalRuleCodes',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.ArchivingSystems',
        'app.Configurations',
        'app.EventLogs',
        'app.Ldaps',
        'app.OrgEntities',
        'app.Profiles',
        'app.Roles',
        'app.ServiceLevels',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
        'app.ValidationChains',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
    }

    /**
     * testDeletable
     */
    public function testDeletable()
    {
        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities');

        $org = new OrgEntity();
        $this->assertTrue($org->get('deletable'));

        // SE = false
        $org = $OrgEntities->get(1);
        $this->assertFalse($org->get('deletable'));

        // Blocage Transfers
        $org = $OrgEntities->get(2);
        $this->assertFalse($org->get('deletable'));

        $org = $OrgEntities->newEntity(
            [
                'name' => uniqid(),
                'identifier' => uniqid(),
                'parent_id' => 2,
                'active' => true,
            ]
        );
        $OrgEntities->saveOrFail($org);
        $this->assertTrue($org->get('deletable'));
    }

    /**
     * testGetEditableIdentifier
     */
    public function testGetEditableIdentifier()
    {
        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities');

        $org = new OrgEntity();
        $this->assertTrue($org->get('editable_identifier'));
        $org = $OrgEntities->get(2);

        // Blocage Transfers
        $this->assertFalse($org->get('editable_identifier'));

        $org = $OrgEntities->newEntity(
            [
                'name' => uniqid(),
                'identifier' => uniqid(),
                'parent_id' => 2,
                'active' => true,
            ]
        );
        $OrgEntities->saveOrFail($org);
        $this->assertTrue($org->get('editable_identifier'));
    }

    /**
     * testgetConfig
     */
    public function testgetConfig()
    {
        $Configurations = $this->fetchTable('Configurations');
        $config1 = $Configurations->newEntity(
            [
                'org_entity_id' => 2,
                'name' => 'my-config-key',
                'setting' => 'my-config-value',
            ]
        );
        $config2 = $Configurations->newEntity(
            [
                'org_entity_id' => 2,
                'name' => 'my-other-key',
                'setting' => 'my-other-value',
            ]
        );
        $Configurations->saveOrFail($config1);
        $Configurations->saveOrFail($config2);

        /** @var OrgEntity $archivalAgency */
        $archivalAgency = $this->fetchTable('OrgEntities')
            ->find()
            ->where(['OrgEntities.id' => 2])
            ->contain(['Configurations'])
            ->firstOrFail();
        $this->assertEquals('my-config-value', $archivalAgency->getConfig('my-config-key'));
        $this->assertEquals('my-other-value', $archivalAgency->getConfig('my-other-key'));
        $this->assertNull($archivalAgency->getConfig('undefined-key'));
        $this->assertFalse($archivalAgency->getConfig('undefined-key', false));
    }
}
