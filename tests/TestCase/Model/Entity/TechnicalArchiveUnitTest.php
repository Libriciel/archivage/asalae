<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\TechnicalArchiveUnit;
use Cake\Core\Configure;
use Cake\TestSuite\TestCase;

/**
 * Asalae\Model\Entity\TechnicalArchiveUnit Test Case
 */
class TechnicalArchiveUnitTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Asalae\Model\Entity\TechnicalArchiveUnit
     */
    public $TechnicalArchiveUnit;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->TechnicalArchiveUnit = new TechnicalArchiveUnit();
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TechnicalArchiveUnit);

        parent::tearDown();
    }

    /**
     * testGetStatetrad
     */
    public function testGetStatetrad()
    {
        $trad = $this->TechnicalArchiveUnit->get('statetrad');
        $this->assertEmpty($trad);

        foreach (['available', 'destroying'] as $state) {
            $this->TechnicalArchiveUnit->set('state', $state);
            $trad = $this->TechnicalArchiveUnit->get('statetrad');
            $this->assertNotEmpty($trad);
            $this->assertNotEquals($state, $trad);
        }

        $this->TechnicalArchiveUnit->set('state', 'unknown');
        $trad = $this->TechnicalArchiveUnit->get('statetrad');
        $this->assertEquals('unknown', $trad);
    }
}
