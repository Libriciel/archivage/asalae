<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\ArchiveUnitsRestitutionRequest;
use AsalaeCore\Utility\Premis;
use Cake\TestSuite\TestCase;

class ArchiveUnitsRestitutionRequestTest extends TestCase
{
    /**
     * testToPremisObject
     */
    public function testToPremisObject()
    {
        $entity = new ArchiveUnitsRestitutionRequest(['id' => 1, 'name' => 'test']);
        $this->assertInstanceOf(Premis\ObjectInterface::class, $entity->toPremisObject());
    }
}
