<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\Profile;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Exception;

class ProfileTest extends TestCase
{
    public array $fixtures = [
        'app.Fileuploads',
        'app.OrgEntities',
        'app.Profiles',
        'app.Siegfrieds',
        'app.Transfers',
        'app.Users',
    ];

    /**
     * testGetEditableIdentifier
     */
    public function testGetEditableIdentifier()
    {
        $loc = TableRegistry::getTableLocator();
        $Profiles = $loc->get('Profiles');
        $Transfers = $loc->get('Transfers');

        $profile1 = new Profile();
        $this->assertTrue($profile1->get('editable_identifier'));

        $Transfers->updateAll(
            [
                'profile_id' => 1,
                'state' => 'rejected',
            ],
            []
        );
        /** @var EntityInterface $profile2 */
        $profile2 = $Profiles->get(1);
        $this->assertTrue($profile2->get('editable_identifier'));

        $Transfers->updateAll(['state' => 'preparating'], []);

        $this->assertFalse($profile2->get('editable_identifier'));
        $profile2->set('identifier', 'bar');
        try {
            $Profiles->save($profile2);
            $this->fail();
        } catch (Exception) {
        }
    }
}
