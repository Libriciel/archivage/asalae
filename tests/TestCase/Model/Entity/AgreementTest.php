<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\Agreement;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Exception;

class AgreementTest extends TestCase
{
    public array $fixtures = [
        'app.Agreements',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.EventLogs',
        'app.Fileuploads',
        'app.OrgEntities',
        'app.Siegfrieds',
        'app.Transfers',
        'app.Users',
    ];

    /**
     * testMaxSizePerTransferConv
     */
    public function testMaxSizePerTransferConv()
    {
        $agreement = new Agreement(
            [
                'max_size_per_transfer' => 1073741824, // 1024 * 1024 * 1024
            ]
        );
        $this->assertEquals(1073741824, $agreement->get('max_size_per_transfer'));
        $agreement->set('mult_max_size_per_transfer', 1024 * 1024);
        $agreement->set('max_size_per_transfer_conv', 5);
        $this->assertEquals(5242880, $agreement->get('max_size_per_transfer'));
        $agreement->set('mult_max_size_per_transfer');
        $agreement->set('max_size_per_transfer_conv', 5);
        $this->assertEquals(5, $agreement->get('max_size_per_transfer'));
        $agreement->set('max_size_per_transfer_conv');
        $this->assertNull($agreement->get('max_size_per_transfer'));
    }

    /**
     * testTransferPeriodTrad
     */
    public function testTransferPeriodTrad()
    {
        $agreement = new Agreement();
        foreach (['year', 'month', 'week', 'day'] as $period) {
            $agreement->set('transfer_period', $period);
            $this->assertNotEmpty($trad = $agreement->get('transfer_periodtrad'));
            $this->assertNotEquals($period, $trad);
        }
        $agreement->set('transfer_period', 'unknown');
        $this->assertEquals('unknown', $agreement->get('transfer_periodtrad'));
    }

    /**
     * testDeletable
     */
    public function testDeletable()
    {
        $agreement = new Agreement();
        $this->assertTrue($agreement->get('deletable'));

        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $Archives->updateAll(['agreement_id' => 1], ['id' => 1]);
        $agreement = new Agreement(['id' => 1]);
        $this->assertFalse($agreement->get('deletable'));

        $Archives->updateAll(['agreement_id' => null], ['agreement_id' => 1]);
        $this->assertTrue($agreement->get('deletable'));
    }

    /**
     * testGetEditableIdentifier
     */
    public function testGetEditableIdentifier()
    {
        $loc = TableRegistry::getTableLocator();
        $Agreements = $loc->get('Agreements');
        $Transfers = $loc->get('Transfers');

        $agreement1 = new Agreement();
        $this->assertTrue($agreement1->get('editable_identifier'));

        $Transfers->updateAll(
            [
                'agreement_id' => 1,
                'state' => 'rejected',
            ],
            []
        );
        /** @var EntityInterface $agreement2 */
        $agreement2 = $Agreements->get(1);
        $this->assertTrue($agreement2->get('editable_identifier'));

        $Transfers->updateAll(['state' => 'preparating'], []);

        $this->assertFalse($agreement2->get('editable_identifier'));
        $agreement2->set('identifier', 'bar');
        try {
            $Agreements->save($agreement2);
            $this->fail();
        } catch (Exception) {
        }
    }
}
