<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\Transfer;
use Asalae\Model\Entity\TransferAttachment;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use Cake\Core\Configure;
use Cake\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;

use const TMP_TESTDIR;

class TransferAttachmentTest extends TestCase
{
    use AutoFixturesTrait;

    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';
    public $appAutoFixtures = [
        'Transfers',
    ];
    public $appAppendFixtures = [
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        Filesystem::mkdir($dir);
        Configure::write('App.paths.data', $dir);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Filesystem::reset();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
    }

    /**
     * testGetPath
     */
    public function testGetPath()
    {
        $entity = new TransferAttachment();
        $this->assertEmpty($entity->get('path'));

        $entity->set('transfer_id', 1);
        $entity->set('filename', 'foo.bar');
        $this->assertStringContainsString('foo.bar', $entity->get('path'));
    }

    /**
     * testDeletable
     */
    public function testDeletable()
    {
        TransferAttachment::$listFiles = [];
        $entity = new TransferAttachment();
        $this->assertTrue($entity->get('deletable'));

        $transfer = new Transfer(
            [
                'id' => 1,
                'filename' => 'transfer-61b20d8ded833.xml',
                'message_version' => 'seda1.0',
                'files_deleted' => false,
            ]
        );
        $entity = new TransferAttachment(
            [
                'id' => 1,
                'transfer_id' => 1,
                'filename' => 'foo.bar',
            ]
        );

        Filesystem::copy(self::SEDA_10_XML, $transfer->get('xml'));
        $this->assertTrue($entity->get('deletable'));

        $entity->set('filename', 'sample.pdf');
        $this->assertFalse($entity->get('deletable'));
    }
}
