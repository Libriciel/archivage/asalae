<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\Timestamper;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class TimestamperTest extends TestCase
{
    public array $fixtures = [
        'app.OrgEntities',
        'app.Timestampers',
    ];

    /**
     * testDeletable
     */
    public function testDeletable()
    {
        $loc = TableRegistry::getTableLocator();
        $Timestampers = $loc->get('Timestampers');

        $time = new Timestamper();
        $this->assertTrue($time->get('deletable'));

        $time = $Timestampers->get(1);
        $this->assertFalse($time->get('deletable'));

        $time = $Timestampers->newEntity(
            [
                'name' => uniqid(),
                'org_entity' => 2,
            ]
        );
        $Timestampers->saveOrFail($time);
        $this->assertTrue($time->get('deletable'));
    }
}
