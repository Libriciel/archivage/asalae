<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\StoredFile;
use Asalae\TestSuite\VolumeSample;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;

use const TMP_VOL1;
use const TMP_VOL2;

class StoredFileTest extends TestCase
{
    public const string SEDA_10_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml';

    public array $fixtures = [
        'app.StoredFiles',
        'app.SecureDataSpaces',
        'app.Volumes',
        'app.StoredFilesVolumes',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Filesystem::setNamespace();
        Filesystem::reset();
    }

    /**
     * testGetFile
     */
    public function testGetFile()
    {
        $stored = new StoredFile();
        $this->assertEmpty($stored->get('file'));

        $stored = TableRegistry::getTableLocator()->get('StoredFiles')->get(3);
        $this->assertEquals(
            file_get_contents(TMP_VOL1 . DS . 'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'),
            $stored->get('file')
        );
    }

    /**
     * testCheckVolumes
     */
    public function testCheckVolumes()
    {
        $stored = new StoredFile(['id' => 1]);
        $result = $stored->checkVolumes();
        $this->assertArrayHasKey(1, $result);
        $this->assertArrayHasKey(2, $result);
        $this->assertTrue($result[1]);
        $this->assertTrue($result[2]);
    }
}
