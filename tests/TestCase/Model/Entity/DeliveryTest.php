<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\Delivery;
use Cake\TestSuite\TestCase;
use DateTime;
use DOMDocument;

use const SEDA_V21_XSD;

class DeliveryTest extends TestCase
{
    public array $fixtures = [
        'app.Acos',
    ];

    /**
     * testGenerateXml
     */
    public function testGenerateXml()
    {
        $entity = new Delivery(
            [
                'identifier' => 'test001',
                'created' => new DateTime(),
                'delivery_request' => [
                    'identifier' => 'test001',
                    'comment' => 'test',
                    'derogation' => true,
                    'archive_units' => [
                        ['archival_agency_identifier' => 'archive_unit_1'],
                        ['archival_agency_identifier' => 'archive_unit_2'],
                    ],
                    'created' => new DateTime(),
                    'requester' => [
                        'identifier' => 'requester',
                        'archival_agency' => ['identifier' => 'archival_agency'],
                    ],
                ],
            ],
            ['validate' => false]
        );
        $dom = new DOMDocument();
        $this->assertNotNull($xml = $entity->generateXml());
        $this->assertTrue($dom->loadXML($xml));
        $this->assertTrue($dom->schemaValidate(SEDA_V21_XSD));
        $xml = $dom->saveXML();
        $this->assertTextContains('test001', $xml);
        $this->assertTextContains('archive_unit_1', $xml);
        $this->assertTextContains('archival_agency', $xml);
        $this->assertTextContains('requester', $xml);
    }
}
