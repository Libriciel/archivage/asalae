<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\Eaccpf;
use Cake\Core\Configure;
use Cake\TestSuite\TestCase;

class EaccpfTest extends TestCase
{
    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * testGetEntityTypeTrad
     */
    public function testGetEntityTypeTrad()
    {
        $eac = new Eaccpf();
        $this->assertEmpty($eac->get('entity_typetrad'));

        foreach (['person', 'family', 'corporateBody'] as $type) {
            $eac->set('entity_type', $type);
            $this->assertNotEquals($type, $eac->get('entity_typetrad'));
        }
        $eac->set('entity_type', 'unknown');
        $this->assertEquals('unknown', $eac->get('entity_typetrad'));
    }
}
