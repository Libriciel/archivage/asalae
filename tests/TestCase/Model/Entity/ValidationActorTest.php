<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\ValidationActor;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use Beanstalk\Utility\Beanstalk;
use Cake\ORM\Entity;
use Cake\TestSuite\TestCase;

class ValidationActorTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
        'ValidationStages',
    ];
    public $appAppendFixtures = [
    ];
    public array $fixtures = [
        'app.AuthUrls',
        'app.DeliveryRequests',
        'app.DestructionRequests',
        'app.Mails',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    /**
     * testNotify
     */
    public function testNotify()
    {
        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);

        $actor = new ValidationActor(
            [
                'app_type' => 'MAIL',
                'app_key' => 'foo@bar.baz',
            ]
        );
        $process = new Entity(
            [
                'id' => 1,
                'app_subject_type' => 'DeliveryRequests',
                'app_foreign_key' => 1, // delivery_request_id
                'current_stage_id' => 1,
            ]
        );
        $this->assertNotEmpty($actor->notify($process));

        $process->set('app_subject_type', 'DestructionRequests');
        $this->assertNotEmpty($actor->notify($process));
    }
}
