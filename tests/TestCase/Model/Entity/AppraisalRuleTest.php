<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\AppraisalRule;
use Cake\TestSuite\TestCase;

class AppraisalRuleTest extends TestCase
{
    /**
     * testgetFinalActionCodetrad
     */
    public function testgetFinalActionCodetrad()
    {
        $appraisalRule = new AppraisalRule();
        foreach (['keep', 'destroy'] as $code) {
            $appraisalRule->set('final_action_code', $code);
            $this->assertNotEmpty($trad = $appraisalRule->get('final_action_codetrad'));
            $this->assertNotEquals($code, $trad);
        }
        $appraisalRule->set('final_action_code', 'unknown');
        $this->assertEquals('unknown', $appraisalRule->get('final_action_codetrad'));
    }

    /**
     * testgetFinalActionCodeXml21
     */
    public function testgetFinalActionCodeXml21()
    {
        $appraisalRule = new AppraisalRule(['final_action_code' => 'keep']);
        $this->assertEquals('Keep', $appraisalRule->get('final_action_code_xml21'));
        $appraisalRule = new AppraisalRule(['final_action_code' => 'destroy']);
        $this->assertEquals('Destroy', $appraisalRule->get('final_action_code_xml21'));
        $appraisalRule = new AppraisalRule(['final_action_code' => 'Destroy']);
        $this->assertEquals('Destroy', $appraisalRule->get('final_action_code_xml21'));
    }
}
