<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\DeletedJsonArchiveFile;
use Asalae\Model\Volume\VolumeManager;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use Cake\TestSuite\TestCase;

class DeletedJsonArchiveFileTest extends TestCase
{
    use InvokePrivateTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testGetData
     */
    public function testGetData()
    {
        $manager = new VolumeManager(1);
        $storedFile = $manager->filePutContent(
            'test.json',
            json_encode($expected = ['Archive/ArchiveObject[1]/Document'])
        );
        $entity = new DeletedJsonArchiveFile(['stored_file_id' => $storedFile->id]);
        $this->assertEquals($expected, $entity->getData());
    }
}
