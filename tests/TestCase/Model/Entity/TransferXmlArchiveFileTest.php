<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\TransferXmlArchiveFile;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use Cake\TestSuite\TestCase;
use DOMDocument;

use const TMP_VOL1;
use const TMP_VOL2;

class TransferXmlArchiveFileTest extends TestCase
{
    use AutoFixturesTrait;

    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testGetDom
     */
    public function testGetDom()
    {
        $entity = new TransferXmlArchiveFile(
            [
                'stored_file_id' => 3,
            ]
        );
        $this->assertInstanceOf(DOMDocument::class, $entity->getDom());
    }
}
