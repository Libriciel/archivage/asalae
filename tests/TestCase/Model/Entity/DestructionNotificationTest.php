<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\DestructionNotification;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use DateTime;
use DOMDocument;

use const SEDA_V21_XSD;

class DestructionNotificationTest extends TestCase
{
    public array $fixtures = [
        'app.Acos',
        'app.ArchiveUnits',
        'app.ArchiveUnitsDestructionRequests',
        'app.DestructionRequests',
    ];

    /**
     * testGenerateXml
     */
    public function testGenerateXml()
    {
        $ArchiveUnitsDestructionRequests = TableRegistry::getTableLocator()->get('ArchiveUnitsDestructionRequests');
        $ArchiveUnitsDestructionRequests->insertQuery()
            ->insert(['archive_unit_id', 'destruction_request_id'])
            ->values(['archive_unit_id' => 1, 'destruction_request_id' => 1])
            ->execute();
        $entity = new DestructionNotification(
            [
                'id' => 1,
                'identifier' => 'test001',
                'destruction_request' => [
                    'created' => new DateTime(),
                    'identifier' => 'code001',
                    'archival_agency' => ['identifier' => 'archival_agency'],
                    'originating_agency' => ['identifier' => 'originating_agency'],
                    'archive_units' => [
                        ['archival_agency_identifier' => 'archive_unit_1'],
                        ['archival_agency_identifier' => 'archive_unit_2'],
                    ],
                ],
            ],
            ['validate' => false]
        );
        $dom = new DOMDocument();
        $this->assertNotNull($xml = $entity->generateXml());
        $this->assertTrue($dom->loadXML($xml));
        $this->assertTrue($dom->schemaValidate(SEDA_V21_XSD));
        $xml = $dom->saveXML();
        $this->assertTextContains('test001', $xml);
        $this->assertTextContains('archive_unit_1', $xml);
        $this->assertTextContains('archival_agency', $xml);
    }
}
