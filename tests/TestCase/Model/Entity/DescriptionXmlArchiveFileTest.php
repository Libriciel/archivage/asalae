<?php

namespace Asalae\Test\TestCase\Model\Entity;

use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use Cake\Core\Configure;
use Cake\ORM\Entity;
use Cake\TestSuite\TestCase;
use DOMDocument;
use Libriciel\Filesystem\Utility\Filesystem;

use const NAMESPACE_SEDA_21;
use const TMP_TESTDIR;

class DescriptionXmlArchiveFileTest extends TestCase
{
    use InvokePrivateTrait;

    public array $fixtures = [
        'app.ArchiveFiles',
        'app.SecureDataSpaces',
        'app.Volumes',
        'app.StoredFiles',
        'app.StoredFilesVolumes',
        'app.AppraisalRuleCodes',
        'app.AccessRuleCodes',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Filesystem::reset();
        VolumeSample::init();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Filesystem::setNamespace();
        Filesystem::reset();
    }

    /**
     * testGetHtml
     */
    public function testGetHtml()
    {
        $filePath = 'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml';
        VolumeSample::insertSample('sample_seda10_description.xml', $filePath);
        $entity = new DescriptionXmlArchiveFile(
            [
                'stored_file_id' => 3,
            ]
        );
        $entity->getDom();
        $this->assertStringContainsString('<!DOCTYPE html', $entity->get('HTML'));

        VolumeSample::insertSample('sample_seda10_description.xml', $filePath);
        $entity = new DescriptionXmlArchiveFile(
            [
                'stored_file_id' => 3,
            ]
        );
        $entity->getDom();
        $this->assertStringContainsString('<!DOCTYPE html', $entity->get('HTML'));

        $entity = new DescriptionXmlArchiveFile();
        $this->assertEmpty($entity->get('HTML'));
        $xml = VolumeSample::getSampleContent('sample_seda21_description.xml');
        $entity->dom = new DOMDocument();
        $entity->dom->loadXML($xml);
        $entity->namespace = NAMESPACE_SEDA_21;
        $this->assertStringContainsString('<!DOCTYPE html', $entity->get('HTML'));
    }

    /**
     * testGetXsl
     */
    public function testGetXsl()
    {
        $entity = new DescriptionXmlArchiveFile();
        $this->assertEmpty($entity->get('XSL'));
    }

    /**
     * testIsLarge
     */
    public function testIsLarge()
    {
        $entity = new DescriptionXmlArchiveFile();
        $entity->getDom();
        $this->assertFalse($entity->get('is_large'));

        $entity->set('stored_file', new Entity(['size' => 10000000])); // 10Mo
        $this->assertTrue($entity->get('is_large')); // > 5Mo
        $entity->set('stored_file', new Entity(['size' => 1000000])); // 1Mo
        $this->assertFalse($entity->get('is_large'));
    }
}
