<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\OutgoingTransferRequestsController;
use Asalae\Test\Mock\ClientMock;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use Cake\Http\Client\Response;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;
use stdClass;

#[UsesClass(OutgoingTransferRequestsController::class)]
class OutgoingTransferRequestsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,
        FIXTURES_VOLUMES,
        'app.Agreements',
        'app.AppraisalRules',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveUnits',
        'app.ArchiveUnitsOutgoingTransferRequests',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.ArchivingSystems',
        'app.AuthUrls',
        'app.Counters',
        'app.EventLogs',
        'app.Mails',
        'app.OutgoingTransferRequests',
        'app.Profiles',
        'app.Sequences',
        'app.Transfers',
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationHistories',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        $response = $this->createMock(Response::class);
        $response->method('getJson')->willReturn(
            [
                'archival_agency_identifier' => 'sa',
                'archival_agency_name' => 'SA Libriciel',
                'transferring_agencies' => [
                    'sa' => 'sa - SA Libriciel',
                    'sv1' => 'sv1 - Service Versant 1',
                    'sv2' => 'sv2 - Service Versant 2',
                ],
                'profiles' => [
                    'test' => 'test - Test',
                ],
                'agreements' => [
                    'agr-test01' => 'agr-test01 - Accord de versement de test 01',
                    'agr-test02' => 'agr-test02 - Accord de versement de test 02',
                ],
            ]
        );
        $response->method('getStatusCode')->willReturn(200);
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get'])
            ->getMock();
        $client->method('get')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    /**
     * testIndexPreparating
     */
    public function testIndexPreparating()
    {
        TableRegistry::getTableLocator()->get('OutgoingTransferRequests')
            ->updateAll(['state' => 'creating'], ['id' => 1]);
        $response = $this->httpGet('/outgoing-transfer-requests/index-preparating');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testIndexMy
     */
    public function testIndexMy()
    {
        $response = $this->httpGet('/outgoing-transfer-requests/index-my');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testIndexAll
     */
    public function testIndexAll()
    {
        TableRegistry::getTableLocator()->get('OutgoingTransferRequests')
            ->updateAll(['state' => 'validating'], ['id' => 1]);
        $response = $this->httpGet('/outgoing-transfer-requests/index-all');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testIndexProcessed
     */
    public function testIndexProcessed()
    {
        TableRegistry::getTableLocator()->get('OutgoingTransferRequests')
            ->updateAll(['state' => 'transfers_processed'], ['id' => 1]);
        $response = $this->httpGet('/outgoing-transfer-requests/index-processed');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testAdd1
     */
    public function testAdd1()
    {
        $this->httpGet('/outgoing-transfer-requests/add1?agreement_id=1');
        $this->assertResponseCode(200);

        $data = [
            'archiving_system_id' => 1,
        ];
        $this->httpPost('/outgoing-transfer-requests/add1?agreement_id=1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testAdd2
     */
    public function testAdd2()
    {
        $loc = TableRegistry::getTableLocator();
        $OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
        $ArchiveUnitsOutgoingTransferRequests = $loc->get('ArchiveUnitsOutgoingTransferRequests');
        $initialCountOtr = $OutgoingTransferRequests->find()->count();
        $initialCountLink = $ArchiveUnitsOutgoingTransferRequests->find()->count();
        $this->httpGet('/outgoing-transfer-requests/add2/1?agreement_id=1');
        $this->assertResponseCode(200);

        $data = [
            'archiving_system_id' => 1,
            'archival_agency_identifier' => 'sa_1',
            'archival_agency_name' => 'SA Libriciel',
            'comment' => 'testunit',
            'transferring_agency_identifier' => null,
            'transferring_agency_name' => null,
            'agreement_identifier' => null,
            'profile_identifier' => null,
            'service_level_identifier' => null,
        ];
        $this->httpPost('/outgoing-transfer-requests/add2/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertCount($initialCountOtr + 1, $OutgoingTransferRequests->find());
        $this->assertGreaterThan($initialCountLink, $ArchiveUnitsOutgoingTransferRequests->find()->count());
    }

    /**
     * testAdd3
     */
    public function testAdd3()
    {
        $loc = TableRegistry::getTableLocator();
        $OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
        $OutgoingTransferRequests->updateAll(['original_size' => -1], []);

        $this->httpGet('/outgoing-transfer-requests/add3/1');
        $this->assertResponseCode(200);

        $data = [
            'id' => 1,
        ];
        $this->httpPut('/outgoing-transfer-requests/add3/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertGreaterThan(1, $OutgoingTransferRequests->get(1)->get('original_size'));
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $loc = TableRegistry::getTableLocator();
        $OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
        $OutgoingTransferRequests->updateAll(['state' => 'creating', 'original_size' => -1], []);

        $this->httpGet('/outgoing-transfer-requests/edit/1');
        $this->assertResponseCode(200);

        $data = [
            'archiving_system_id' => 1,
            'archival_agency_identifier' => 'sa',
            'archival_agency_name' => 'SA Libriciel',
            'comment' => 'testunit',
            'transferring_agency_identifier' => null,
            'transferring_agency_name' => null,
            'agreement_identifier' => null,
            'profile_identifier' => null,
            'service_level_identifier' => null,
        ];
        $this->httpPut('/outgoing-transfer-requests/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertGreaterThan(1, $OutgoingTransferRequests->get(1)->get('original_size'));
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        TableRegistry::getTableLocator()->get('OutgoingTransferRequests')
            ->updateAll(['state' => 'creating'], []);
        $response = $this->httpDelete('/outgoing-transfer-requests/delete/1', [], true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('report', $response);
        $this->assertEquals('done', $response['report']);
    }

    /**
     * testPaginateArchiveUnits
     */
    public function testPaginateArchiveUnits()
    {
        $this->httpGet('/outgoing-transfer-requests/paginate-archive-units/1');
        $this->assertResponseCode(200);
    }

    /**
     * testRemoveArchiveUnit
     */
    public function testRemoveArchiveUnit()
    {
        $ArchiveUnitsOutgoingTransferRequests = TableRegistry::getTableLocator()->get(
            'ArchiveUnitsOutgoingTransferRequests'
        );
        $auOtr = $ArchiveUnitsOutgoingTransferRequests->newEntity(
            [
                'archive_unit_id' => 1,
                'otr_id' => 1,
            ]
        );
        $ArchiveUnitsOutgoingTransferRequests->saveOrFail($auOtr);
        $this->httpDelete('/outgoing-transfer-requests/remove-archive-unit/1/1');
        $this->assertResponseCode(200);
    }

    /**
     * testSend
     */
    public function testSend()
    {
        $loc = TableRegistry::getTableLocator();
        $OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
        $OutgoingTransferRequests->updateAll(['state' => 'creating'], []);
        $ValidationProcesses = $loc->get('ValidationProcesses');
        $initialCount = $ValidationProcesses->find()->count();

        $this->httpPost('/outgoing-transfer-requests/send/1');

        $this->assertResponseCode(200);
        $req = $OutgoingTransferRequests->get(1);
        $this->assertEquals('validating', $req->get('state'));
        $this->assertCount($initialCount + 1, $ValidationProcesses->find());
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpGet('/outgoing-transfer-requests/view/1');
        $this->assertResponseCode(200);
    }
}
