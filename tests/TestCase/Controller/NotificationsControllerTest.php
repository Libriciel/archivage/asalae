<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\NotificationsController;
use AsalaeCore\Factory;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(NotificationsController::class)]
class NotificationsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,

        // Pour ce controller
        'app.EventLogs',
        'app.Webservices',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Factory\Utility::set('Notify', 'Asalae\Test\Mock\FakeClass');
        Factory\Utility::set('Beanstalk', 'Asalae\Test\Mock\FakeClass');
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Factory\Utility::reset();
    }

    /**
     * testTest
     */
    public function testTest()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $data = [
            'msg' => 'test du test',
        ];
        $this->httpPost('/notifications/test', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['result']);
        $this->assertNotEmpty($response['result']['id']);
        $this->assertNotEmpty($response['result']['text']);
        $this->assertEquals('test du test', $response['result']['text']);

        $Notifications = TableRegistry::getTableLocator()->get('Notifications');
        $notif = $Notifications->find()->where(['id' => $response['result']['id']])->first();
        $this->assertTrue((bool)$notif);
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $Notifications = TableRegistry::getTableLocator()->get('Notifications');
        $notification = $Notifications->newEntity(
            [
                'user_id' => 1,
                'text' => 'test',
            ]
        );
        $Notifications->saveOrFail($notification);
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpDelete('/notifications/delete/' . $notification->id);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']);
        $this->assertEquals('done', $response['report']);
    }
}
