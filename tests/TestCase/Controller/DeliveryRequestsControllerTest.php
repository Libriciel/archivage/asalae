<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\DeliveryRequestsController;
use Asalae\Model\Entity\DeliveryRequest;
use Asalae\Model\Table\ConfigurationsTable;
use Asalae\Model\Table\DeliveryRequestsTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use DOMDocument;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;
use stdClass;

/**
 * Asalae\Controller\DeliveryRequestsController Test Case
 *
 */
#[UsesClass(DeliveryRequestsController::class)]
class DeliveryRequestsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use SaveBufferTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        'app.AccessRules',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveKeywords',
        'app.ArchiveUnits',
        'app.ArchiveUnitsDeliveryRequests',
        'app.Archives',
        'app.Counters',
        'app.DeliveryRequests',
        'app.EventLogs',
        'app.Mails',
        'app.MessageIndicators',
        'app.Sequences',
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationHistories',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        $this->cookie('catalog_basket', '1,2');

        Utility::set('Notify', 'Asalae\Test\Mock\FakeClass');
        Utility::set('Beanstalk', 'Asalae\Test\Mock\FakeClass');

        Filesystem::setNamespace();
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::mkdir(TMP_TESTDIR);
        Configure::write('App.paths.data', TMP_TESTDIR);

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
    }

    /**
     * testIndexPreparating
     */
    public function testIndexPreparating()
    {
        TableRegistry::getTableLocator()->get('DeliveryRequests')
            ->updateAll(['state' => 'creating'], ['id' => 1]);
        $response = $this->httpGet('/delivery-requests/index-preparating');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testIndexMy
     */
    public function testIndexMy()
    {
        $response = $this->httpGet('/delivery-requests/index-my');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testIndexAll
     */
    public function testIndexAll()
    {
        TableRegistry::getTableLocator()->get('DeliveryRequests')
            ->updateAll(['state' => 'validating'], ['id' => 1]);
        $response = $this->httpGet('/delivery-requests/index-all');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $this->httpGet('/delivery-requests/add');
        $this->assertResponseCode(200);

        $data = [
            'comment' => 'testunit',
            'archive_units' => [
                '_ids' => [1],
            ],
        ];
        $this->httpPost('/delivery-requests/add', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        TableRegistry::getTableLocator()->get('DeliveryRequests')
            ->updateAll(['state' => 'creating'], []);
        $this->httpGet('/delivery-requests/edit/1');
        $this->assertResponseCode(200);

        $data = [
            'comment' => 'testunit',
            'archive_units' => [
                '_ids' => [1],
            ],
        ];
        $this->httpPut('/delivery-requests/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $response = $this->httpDelete('/delivery-requests/delete/1', [], true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('report', $response);
        $this->assertEquals('done', $response['report']);
    }

    /**
     * testDerogationNeeded
     */
    public function testDerogationNeeded()
    {
        $data = [
            'archive_units' => '1,2',
        ];
        $response = $this->httpPost('/delivery-requests/add', $data);
        $this->assertFalse($response->derogation ?? null);
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpDelete('/delivery-requests/view/1');
        $this->assertResponseCode(200);
    }

    /**
     * testPaginateArchiveUnits
     */
    public function testPaginateArchiveUnits()
    {
        $this->httpDelete('/delivery-requests/paginateArchiveUnits/1');
        $this->assertResponseCode(200);
    }

    /**
     * testSend
     */
    public function testSend()
    {
        $DeliveryRequests = TableRegistry::getTableLocator()->get('DeliveryRequests');
        $DeliveryRequests->updateAll(['state' => 'creating'], ['id' => 1]);
        $this->httpPost('/delivery-requests/send/1');
        $this->assertResponseCode(200);
        /** @var DeliveryRequest $request */
        $request = $DeliveryRequests->get(1);
        $this->assertEquals('validating', $request->get('state'));
        $dom = new DOMDocument();
        $dom->load($request->get('xml'));
        $this->assertTrue($dom->schemaValidate(SEDA_V21_XSD));

        $this->session(
            [
                'ConfigArchivalAgency' => [
                    ConfigurationsTable::CONF_DR_NO_DEROGATION_CHAIN => 'auto',
                ],
            ]
        );
        $DeliveryRequests->updateAll(
            ['state' => 'creating', 'derogation' => true],
            ['id' => 1]
        );
        $this->httpPost('/delivery-requests/send/1');
        $this->assertResponseCode(200);
        /** @var DeliveryRequest $request */
        $request = $DeliveryRequests->get(1);
        $this->assertEquals(DeliveryRequestsTable::S_VALIDATING, $request->get('state'));

        $dom = new DOMDocument();
        $dom->load($request->get('xml'));
        $this->assertTrue($dom->schemaValidate(SEDA_V21_XSD));
    }

    /**
     * testDownloadPdf
     */
    public function testDownloadPdf()
    {
        $DeliveryRequests = TableRegistry::getTableLocator()->get('DeliveryRequests');
        /** @var DeliveryRequest $deli */
        $deli = $DeliveryRequests->find()
            ->where(['DeliveryRequests.id' => 1])
            ->contain(['ArchivalAgencies', 'Requesters', 'ArchiveUnits'])
            ->firstOrFail();
        Filesystem::dumpFile($deli->get('xml'), $deli->generateXml());
        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/delivery-requests/downloadPdf/1'
        );
        $pdf = TMP_TESTDIR . DS . $deli->get('pdf_basename');
        Filesystem::dumpFile($pdf, $output);
        $this->assertEquals('application/pdf', mime_content_type($pdf));
    }
}
