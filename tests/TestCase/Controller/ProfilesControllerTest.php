<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\ProfilesController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(ProfilesController::class)]
class ProfilesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        'app.AgreementsProfiles',
        'app.Fileuploads',
        'app.FileuploadsProfiles',
        'app.Siegfrieds',
        'app.Transfers',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->httpGet('/profiles/index/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);

        $this->httpGet(
            '/profiles/index?SaveFilterSelect=&name[0]=Mon profil*&identifier[0]=prof*&active[0]=0&active[0]=1'
        );
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $this->httpGet('/profiles/add');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $data = [
            'identifier' => 'test_2',
            'name' => 'Test 2',
            'description' => 'sample',
            'active' => true,
            'fileuploads' => ['_ids' => 1],
        ];

        $this->httpPost('/profiles/add', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['id']);
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        if (is_file($uri = TMP_TESTDIR . DS . 'testfile.txt')) {
            unlink($uri);
        }
        Filesystem::createDummyFile($uri, 10);

        $this->httpGet('/profiles/edit/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $Transfers = TableRegistry::getTableLocator()
            ->get('Transfers');
        $Transfers->updateAll(['profile_id' => null], []);

        $data = [
            'identifier' => 'test_2',
            'name' => 'Test 2',
            'description' => 'sample',
            'active' => true,
            'fileuploads' => ['_ids' => 1],
        ];

        $this->httpPut('/profiles/edit/1', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['id']);
        $this->assertEquals('test_2', $response['identifier']);
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $Profiles = TableRegistry::getTableLocator()->get('Profiles');
        $profile = $Profiles->newEntity(
            [
                'org_entity_id' => 2,
                'identifier' => 'testunit',
                'name' => 'testunit',
                'description' => '',
                'active' => true,
            ]
        );
        $Profiles->saveOrFail($profile);
        $this->httpDelete('/profiles/delete/' . $profile->id);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']);
        $this->assertEquals('done', $response['report']);
    }

    /**
     * testView
     */
    public function testView()
    {
        $response = $this->httpGet('/profiles/view/1');
        $this->assertResponseCode(200);
        $this->assertTrue(
            isset($response->entity->identifier)
        );
        $this->assertEquals('profile1', $response->entity->identifier);
    }

    /**
     * testApiAdd
     */
    public function testApiAdd()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'Profiles'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        $data = [
            'org_entity_id' => 2,
            'name' => 'api_test',
            'identifier' => 'api_test',
            'description' => 'api_test',
            'active' => 'true',
        ];
        $json = $this->httpPost('/api/profiles', $data, true);
        $this->assertResponseCode(200);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('success', $json);
        $this->assertArrayHasKey('id', $json);
        $this->assertTrue($json['success']);
    }
}
