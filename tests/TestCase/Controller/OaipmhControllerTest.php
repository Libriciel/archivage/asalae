<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\OaipmhController;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Cake\Utility\Xml;
use PHPUnit\Framework\Attributes\UsesClass;
use SimpleXMLElement;
use Throwable;

/**
 * Asalae\Controller\OaipmhController Test Case
 */
#[UsesClass(OaipmhController::class)]
class OaipmhControllerTest extends TestCase
{
    use AutoFixturesTrait;
    use HttpTrait;
    use IntegrationTestTrait;
    use SaveBufferTrait;

    public const string SEDA_02_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02_description.xml';
    public const string SEDA_10_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml';
    public const string SEDA_21_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda21_description.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_VOLUMES,
        FIXTURES_EVENT_LOGS,
        'app.OaipmhArchives',
        'app.OaipmhListsets',
        'app.OaipmhTokens',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('OAIPMH.maxListSize', 10);

        $this->session($this->genericSessionData);

        VolumeSample::init();
        VolumeSample::insertSample(
            'sample_seda10_description.xml',
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
        );
        VolumeSample::insertSample(
            'sample_seda21_description.xml',
            'sp/profile1/2021-12/sa_2/management_data/sa_2_description.xml'
        );
        VolumeSample::insertSample(
            'sample_seda21_description.xml',
            'sp/profile1/2021-12/sa_3/management_data/sa_3_description.xml'
        );
        VolumeSample::insertSample(
            'sample_seda21_description.xml',
            'sp/profile1/2021-12/sa_4/management_data/sa_4_description.xml'
        );
        VolumeSample::insertSample(
            'sample_seda21_description.xml',
            'sp/profile1/2021-12/sa_5/management_data/sa_5_description.xml'
        );

        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'Oaipmh'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $actual = $this->getResponse('/api/oaipmh');
        $this->assertNotEmpty($actual);
    }

    /**
     * @param string $url
     * @return array
     * @throws Throwable
     */
    protected function getResponse(string $url): array
    {
        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            $url
        );

        $xml = Xml::toArray(new SimpleXMLElement($output));
        $this->assertArrayHasKey('OAI-PMH', $xml);
        $this->assertArrayHasKey('request', $xml['OAI-PMH']);
        return $xml['OAI-PMH'];
    }

    /**
     * testIndexV1
     */
    public function testIndexV1()
    {
        $actual = $this->getResponse('/api/oaipmh/v1');
        $this->assertNotEmpty($actual);
    }

    /**
     * testIndexV2
     */
    public function testIndexV2()
    {
        $actual = $this->getResponse('/api/oaipmh/v2');
        $this->assertNotEmpty($actual);
    }

    /**
     * testWrongVerb
     */
    public function testWrongVerb()
    {
        $response = $this->getResponse('/api/oaipmh?verb=NotAValidVerb');

        $actual = Hash::get($response, 'error');
        $expected = [
            '@code' => 'badVerb',
            '@' => 'Value of the verb argument is not a legal OAI-PMH verb.',
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * testIdentify
     */
    public function testIdentify()
    {
        $response = $this->getResponse('/api/oaipmh?verb=Identify');
        $expected = [
            'repositoryName',
            'baseURL',
            'protocolVersion',
            'adminEmail',
            'earliestDatestamp',
            'deletedRecord',
            'granularity',
        ];
        $actual = array_keys(Hash::get($response, 'Identify'));
        $this->assertEquals($expected, $actual);
    }

    /**
     * testIdentifyWithResumptionTokenFail
     */
    public function testIdentifyWithResumptionTokenFail()
    {
        $response = $this->getResponse('/api/oaipmh?verb=Identify&resumptionToken=token');

        $actual = Hash::get($response, 'error');
        $expected = [
            '@code' => 'badArgument',
            '@' => 'This verb does not support resumption token.',
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * testListMetadataFormats
     */
    public function testListMetadataFormats()
    {
        $response = $this->getResponse('/api/oaipmh?verb=ListMetadataFormats');

        $expected = [
            [
                'metadataPrefix' => 'oai_dc',
                'schema' => 'http://www.openarchives.org/OAI/2.0/oai_dc.xsd', // NOSONAR
                'metadataNamespace' => 'http://www.openarchives.org/OAI/2.0/oai_dc/', // NOSONAR
            ],
            [
                'metadataPrefix' => 'seda0.2',
                'schema' => 'https://francearchives.fr/seda/seda_v0-2.xsd',
                'metadataNamespace' => NAMESPACE_SEDA_02,
            ],
            [
                'metadataPrefix' => 'seda1.0',
                'schema' => 'https://francearchives.fr/seda/seda_v1-0.xsd',
                'metadataNamespace' => NAMESPACE_SEDA_10,
            ],
            [
                'metadataPrefix' => 'seda2.1',
                'schema' => 'https://francearchives.fr/seda/seda_v2-1.xsd',
                'metadataNamespace' => NAMESPACE_SEDA_21,
            ],
        ];

        $actual = Hash::get($response, 'ListMetadataFormats.metadataFormat');
        $this->assertEquals($expected, $actual);
    }

    /**
     * testListSetsInitialNoToken
     */
    public function testListSetsInitialNoToken()
    {
        Configure::write('OAIPMH.maxListSize', 10);
        $response = $this->getResponse('/api/oaipmh?verb=ListSets');
        $token = Hash::get($response, 'ListSets.resumptionToken');
        $this->assertNull($token);
        $sets = Hash::get($response, 'ListSets.set');
        $expected = [
            [
                'setSpec' => 'OriginatingAgency:sa',
                'setName' => 'Mon Service d\'archive',
            ],
            [
                'setSpec' => 'OriginatingAgency:sp',
                'setName' => 'Mon service producteur',
            ],
            [
                'setSpec' => 'TransferringAgency:sa',
                'setName' => 'Mon Service d\'archive',
            ],
            [
                'setSpec' => 'ArchivalAgreement:agreement2',
                'setName' => 'Mon accord de versement 2',
            ],
            [
                'setSpec' => 'ArchivalProfile:profile1',
                'setName' => 'Mon profil d\'archives',
            ],
        ];
        $this->assertEquals($expected, $sets);
    }

    /**
     * testListSetsInitialNoTokenV1
     */
    public function testListSetsInitialNoTokenV1()
    {
        Configure::write('OAIPMH.maxListSize', 10);
        $response = $this->getResponse('/api/oaipmh/v1?verb=ListSets');
        $token = Hash::get($response, 'ListSets.resumptionToken');
        $this->assertNull($token);
        $sets = Hash::get($response, 'ListSets.set');
        $expected = [
            [
                'setSpec' => 'OriginatingAgency:sa',
                'setName' => 'Mon Service d\'archive',
            ],
            [
                'setSpec' => 'OriginatingAgency:sp',
                'setName' => 'Mon service producteur',
            ],
            [
                'setSpec' => 'TransferringAgency:sa',
                'setName' => 'Mon Service d\'archive',
            ],
            [
                'setSpec' => 'ArchivalAgreement:agreement2',
                'setName' => 'Mon accord de versement 2',
            ],
            [
                'setSpec' => 'ArchivalProfile:profile1',
                'setName' => 'Mon profil d\'archives',
            ],
        ];
        $this->assertEquals($expected, $sets);
    }

    /**
     * testListSetsInitialWithToken
     */
    public function testListSetsInitialWithToken()
    {
        Configure::write('OAIPMH.maxListSize', 3);
        $response = $this->getResponse('/api/oaipmh?verb=ListSets');
        $token = Hash::get($response, 'ListSets.resumptionToken');
        $this->assertEquals(5, $token['@completeListSize']);
        $this->assertEquals(0, $token['@cursor']);
        $expected = [
            [
                'setSpec' => 'OriginatingAgency:sa',
                'setName' => 'Mon Service d\'archive',
            ],
            [
                'setSpec' => 'OriginatingAgency:sp',
                'setName' => 'Mon service producteur',
            ],
            [
                'setSpec' => 'TransferringAgency:sa',
                'setName' => 'Mon Service d\'archive',
            ],
        ];
        $results = Hash::get($response, 'ListSets.set');
        $this->assertEquals($expected, $results);

        $response = $this->getResponse('/api/oaipmh?verb=ListSets&resumptionToken=' . $token['@']);
        $token = Hash::get($response, 'ListSets.resumptionToken');
        $this->assertEquals(3, $token['@cursor']);
        $this->assertArrayNotHasKey('@', $token);
    }

    /**
     * testListSetsInitialWithTokenV1
     */
    public function testListSetsInitialWithTokenV1()
    {
        Configure::write('OAIPMH.maxListSize', 3);
        $response = $this->getResponse('/api/oaipmh/v1?verb=ListSets');

        $token = Hash::get($response, 'ListSets.resumptionToken');
        $this->assertEquals(5, $token['@completeListSize']);
        $this->assertEquals(0, $token['@cursor']);
        $expected = [
            [
                'setSpec' => 'OriginatingAgency:sa',
                'setName' => 'Mon Service d\'archive',
            ],
            [
                'setSpec' => 'OriginatingAgency:sp',
                'setName' => 'Mon service producteur',
            ],
            [
                'setSpec' => 'TransferringAgency:sa',
                'setName' => 'Mon Service d\'archive',
            ],
        ];
        $results = Hash::get($response, 'ListSets.set');
        $this->assertEquals($expected, $results);

        $response = $this->getResponse('/api/oaipmh/v1?verb=ListSets&resumptionToken=' . $token['@']);
        $newtoken = Hash::get($response, 'ListSets.resumptionToken');
        $this->assertArrayHasKey('@expirationDate', $newtoken);
        $this->assertEquals(5, $newtoken['@completeListSize']);
        $this->assertEquals(3, $newtoken['@cursor']);
        $this->assertArrayNotHasKey('@', $newtoken);

        // vérif que le token a été supprimé :
        $dbToken = TableRegistry::getTableLocator()
            ->get('OaipmhTokens')
            ->find()
            ->where(['value' => $token['@']])
            ->first();
        $this->assertNull($dbToken);
    }

    /**
     * testListSetsWithExpiredToken
     */
    public function testListSetsWithExpiredToken()
    {
        $response = $this->getResponse('/api/oaipmh?verb=ListSets&resumptionToken=expired_listsets_token');
        $expected = [
            '@code' => 'badResumptionToken',
            '@' => 'The value of the resumptionToken argument is invalid or expired.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testListSetsWithInvalidToken
     */
    public function testListSetsWithInvalidToken()
    {
        $response = $this->getResponse('/api/oaipmh?verb=ListSets&resumptionToken=bad_token');
        $expected = [
            '@code' => 'badResumptionToken',
            '@' => 'The value of the resumptionToken argument is invalid or expired.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testListSetsWithToken
     */
    public function testListSetsWithToken()
    {
        Configure::write('OAIPMH.maxListSize', 10);

        $response = $this->getResponse('/api/oaipmh?verb=ListSets&resumptionToken=valid_listsets_token');
        $tokenField = Hash::get($response, 'ListSets.resumptionToken');
        $this->assertEquals('0', $tokenField['@cursor']);
        $this->assertArrayNotHasKey('@', $tokenField);
    }

    /**
     * testListIdentifiersWithoutMetadataprefixFail
     */
    public function testListIdentifiersWithoutMetadataprefixFail()
    {
        $response = $this->getResponse('/api/oaipmh?verb=ListIdentifiers');
        $expected = [
            '@code' => 'badArgument',
            '@' => 'The request is missing the "metadataPrefix" argument.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testListIdentifiersWithoTokenWithParamFails
     */
    public function testListIdentifiersWithoTokenWithParamFails()
    {
        $response = $this->getResponse(
            '/api/oaipmh?verb=ListIdentifiers&resumptionToken=valid_listidentifiers_token&from=anything'
        );
        $expected = [
            '@code' => 'badArgument',
            '@' => 'The request includes illegal arguments.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testListIdentifiersWithWrongMetadataprefixFail
     */
    public function testListIdentifiersWithWrongMetadataprefixFail()
    {
        $response = $this->getResponse('/api/oaipmh?verb=ListIdentifiers&metadataPrefix=wrong');
        $expected = [
            '@code' => 'cannotDisseminateFormat',
            '@' => 'The metadata format identified by the value given for the metadataPrefix
         argument is not supported by the item or by the repository.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testListIdentifiersWithWrongParam
     */
    public function testListIdentifiersWithWrongParam()
    {
        $response = $this->getResponse('/api/oaipmh?verb=ListIdentifiers&metadataPrefix=oai_dc&wrongArg=wrong');
        $expected = [
            '@code' => 'badArgument',
            '@' => 'The request includes illegal arguments.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testListIdentifiersWithBadFromArgument
     */
    public function testListIdentifiersWithBadFromArgument()
    {
        $response = $this->getResponse('/api/oaipmh/?verb=ListIdentifiers&metadataPrefix=oai_dc&from=1234567890');
        $expected = [
            '@code' => 'badArgument',
            '@' => 'The the value of the "from" argument have an illegal syntax.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testListIdentifiersWithFrom
     */
    public function testListIdentifiersWithFrom()
    {
        $response = $this->getResponse(
            '/api/oaipmh/?verb=ListIdentifiers&metadataPrefix=oai_dc&from=5015-01-01T00:00:00Z'
        );
        $expected = [
            '@code' => 'noRecordsMatch',
            '@' => 'The combination of the values of the from, until, set and metadataPrefix
         arguments results in an empty list.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testListIdentifiersSeda02
     */
    public function testListIdentifiersSeda02()
    {
        TableRegistry::getTableLocator()
            ->get('Transfers')
            ->updateAll(
                ['message_version' => 'seda0.2'],
                []
            );
        $response = $this->getResponse('/api/oaipmh/v1/?verb=ListIdentifiers&metadataPrefix=seda0.2');
        $this->assertIsArray($response['ListIdentifiers']);
        $this->assertIsArray($response['ListIdentifiers']['header']);
        $this->assertEquals(5, count($response['ListIdentifiers']['header']));
    }

    /**
     * testListIdentifiersSeda10
     */
    public function testListIdentifiersSeda10()
    {
        $response = $this->getResponse('/api/oaipmh/?verb=ListIdentifiers&metadataPrefix=seda1.0');
        $this->assertIsArray($response['ListIdentifiers']);
        $this->assertIsArray($response['ListIdentifiers']['header']);
        $this->assertEquals(2, count($response['ListIdentifiers']['header']));
        $this->assertArrayHasKey(
            'identifier',
            $response['ListIdentifiers']['header']
        );
    }

    /**
     * testListIdentifiersSeda21
     */
    public function testListIdentifiersSeda21()
    {
        $response = $this->getResponse('/api/oaipmh/?verb=ListIdentifiers&metadataPrefix=seda2.1');
        $this->assertIsArray($response['ListIdentifiers']);
        $this->assertEquals(4, count($response['ListIdentifiers']['header']));
    }

    /**
     * testListIdentifiersInitialWithToken
     */
    public function testListIdentifiersInitialWithToken()
    {
        Configure::write('OAIPMH.maxListSize', 3);
        $response = $this->getResponse('/api/oaipmh?verb=ListIdentifiers&metadataPrefix=oai_dc');
        $token = Hash::get($response, 'ListIdentifiers.resumptionToken');
        $results = Hash::get($response, 'ListIdentifiers.header');
        $this->assertEquals(5, $token['@completeListSize']);
        $this->assertEquals(0, $token['@cursor']);
        $expected = [
            [
                'identifier' => 'sa_1',
                'datestamp' => '2021-12-09T15:24:21Z',
            ],
            [
                'identifier' => 'sa_2',
                'datestamp' => '2021-12-09T15:30:29Z',
            ],
            [
                'identifier' => 'sa_3',
                'datestamp' => '2021-12-09T16:22:20Z',
                '@status' => 'deleted',
            ],
        ];

        $this->assertEquals($expected, $results);

        $response = $this->getResponse('/api/oaipmh?verb=ListIdentifiers&resumptionToken=' . $token['@']);
        $newtoken = Hash::get($response, 'ListIdentifiers.resumptionToken');
        $this->assertEquals(3, $newtoken['@cursor']);
        $this->assertArrayNotHasKey('@', $newtoken);
        $results = Hash::get($response, 'ListIdentifiers.header');
        $expected = [
            [
                'identifier' => 'sa_4',
                'datestamp' => '2021-12-09T16:56:19Z',
                '@status' => 'deleted',
            ],
            [
                'identifier' => 'sa_5',
                'datestamp' => '2021-12-09T17:19:06Z',
                '@status' => 'deleted',
            ],
        ];
        $this->assertEquals($expected, $results);
    }

    /**
     * testListIdentifiersInitialWithTokenV1
     */
    public function testListIdentifiersInitialWithTokenV1()
    {
        Configure::write('OAIPMH.maxListSize', 3);
        $response = $this->getResponse('/api/oaipmh/v1?verb=ListIdentifiers&metadataPrefix=oai_dc');
        $token = Hash::get($response, 'ListIdentifiers.resumptionToken');
        $results = Hash::get($response, 'ListIdentifiers.header');
        $this->assertEquals(5, $token['@completeListSize']);
        $this->assertEquals(0, $token['@cursor']);
        $expected = [
            [
                'identifier' => 'sa_1',
                'datestamp' => '2021-12-09T15:24:21Z',
            ],
            [
                'identifier' => 'sa_2',
                'datestamp' => '2021-12-09T15:30:29Z',
            ],
            [
                'identifier' => 'sa_3',
                'datestamp' => '2021-12-09T16:22:20Z',
                '@status' => 'deleted',
            ],
        ];
        $this->assertEquals($expected, $results);

        $response = $this->getResponse('/api/oaipmh/v1?verb=ListIdentifiers&resumptionToken=' . $token['@']);
        $newToken = Hash::get($response, 'ListIdentifiers.resumptionToken');
        $this->assertEquals(3, $newToken['@cursor']);
        $this->assertArrayNotHasKey('@', $newToken);
        $results = Hash::get($response, 'ListIdentifiers.header');

        $expected = [
            [
                'identifier' => 'sa_4',
                'datestamp' => '2021-12-09T16:56:19Z',
                '@status' => 'deleted',
            ],
            [
                'identifier' => 'sa_5',
                'datestamp' => '2021-12-09T17:19:06Z',
                '@status' => 'deleted',
            ],
        ];
        $this->assertEquals($expected, $results);

        // vérif que le token a été supprimé :
        $dbToken = TableRegistry::getTableLocator()
            ->get('OaipmhTokens')
            ->find()
            ->where(['value' => $token['@']])
            ->first();
        $this->assertNull($dbToken);
    }

    /**
     * testListIdentifiersWithSet
     */
    public function testListIdentifiersWithSet()
    {
        $response = $this->getResponse('/api/oaipmh?verb=ListSets');
        $sets = $response['ListSets']['set'];

        foreach ($sets as $set) {
            $response = $this->getResponse(
                '/api/oaipmh/?verb=ListIdentifiers&metadataPrefix=oai_dc&set=' . $set['setSpec']
            );
            $this->assertTrue(
                !empty($response['ListIdentifiers']['header'])
                || $response['error']['@code'] === 'noRecordsMatch'
            );
        }
    }

    /**
     * testListIdentifiersWithWrongSet
     */
    public function testListIdentifiersWithWrongSet()
    {
        $sets = $this->getResponse('/api/oaipmh?verb=ListSets')['ListSets']['set'];

        $response = $this->getResponse(
            '/api/oaipmh/?verb=ListIdentifiers&metadataPrefix=oai_dc&set=' . $sets[0]['setSpec'] . '0'
        );
        $expected = [
            '@code' => 'noRecordsMatch',
            '@' => 'The combination of the values of the from, until, set and metadataPrefix
         arguments results in an empty list.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testTokenBetweenVersionFail
     */
    public function testTokenBetweenVersionFail()
    {
        Configure::write('OAIPMH.maxListSize', 2);

        $response = $this->getResponse('/api/oaipmh/v1/?verb=ListIdentifiers&metadataPrefix=oai_dc');
        $token = Hash::get($response, 'ListIdentifiers.resumptionToken');

        $expected = [
            '@code' => 'badResumptionToken',
            '@' => 'The value of the resumptionToken argument is invalid or expired.',
        ];

        $response = $this->getResponse('/api/oaipmh/?verb=ListIdentifiers&resumptionToken=' . $token['@']);
        $this->assertEquals($expected, $response['error']);

        $response = $this->getResponse('/api/oaipmh/?verb=ListIdentifiers&metadataPrefix=oai_dc');
        $token = Hash::get($response, 'ListIdentifiers.resumptionToken');
        $response = $this->getResponse('/api/oaipmh/v1/?verb=ListIdentifiers&resumptionToken=' . $token['@']);
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testListIdentifierEmptyFromUntil
     */
    public function testListIdentifierEmptyFromUntil()
    {
        $response = $this->getResponse(
            '/api/oaipmh/?verb=ListIdentifiers&metadataPrefix=oai_dc'
            . '&from=3000-00-00T00:00:00Z&until=2000-00-00T00:00:00Z'
        );
        $expected = [
            '@code' => 'noRecordsMatch',
            '@' => 'The combination of the values of the from, until, set and metadataPrefix
         arguments results in an empty list.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testListRecordsOAIDC
     */
    public function testListRecordsOAIDC()
    {
        Configure::write('OAIPMH.maxListSize', 2);

        $response = $this->getResponse('/api/oaipmh?verb=ListRecords&metadataPrefix=oai_dc');
        $token = Hash::get($response, 'ListRecords.resumptionToken');
        $results = Hash::get($response, 'ListRecords.record');
        $this->assertEquals(5, $token['@completeListSize']);
        $this->assertEquals(0, $token['@cursor']);
        $expected = [
            [
                'header' => [
                    'identifier' => 'sa_1',
                    'datestamp' => '2021-12-09T15:24:21Z',
                ],
                'metadata' => [
                    'oai_dc:dc' => [
                        '@xsi:schemaLocation' => 'http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd',
                        // NOSONAR
                        'dc:date' => '2021-12-09T15:24:21Z',
                        'dc:title' => 'seda1.0',
                        'dc:description' => '',
                        'dc:identifier' => 'sa_1',
                    ],
                ],
            ],
            [
                'header' => [
                    'identifier' => 'sa_2',
                    'datestamp' => '2021-12-09T15:30:29Z',
                ],
                'metadata' => [
                    'oai_dc:dc' => [
                        '@xsi:schemaLocation' => 'http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd',
                        // NOSONAR
                        'dc:date' => '2021-12-09T15:30:29Z',
                        'dc:title' => 'seda2.1',
                        'dc:description' => '',
                        'dc:identifier' => 'sa_2',
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $results);

        $response = $this->getResponse('/api/oaipmh?verb=ListRecords&resumptionToken=' . $token['@']);
        $token = Hash::get($response, 'ListRecords.resumptionToken');
        $this->assertEquals(2, $token['@cursor']);
        $results = Hash::get($response, 'ListRecords.record');
        $expected = [
            [
                'header' => [
                    'identifier' => 'sa_3',
                    'datestamp' => '2021-12-09T16:22:20Z',
                    '@status' => 'deleted',
                ],
                'metadata' => [
                    'oai_dc:dc' => [
                        '@xsi:schemaLocation' => 'http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd',
                        // NOSONAR
                        'dc:date' => '2021-12-09T16:22:20Z',
                        'dc:title' => 'seda2.1',
                        'dc:description' => '',
                        'dc:identifier' => 'sa_3',
                    ],
                ],
            ],
            [
                'header' => [
                    'identifier' => 'sa_4',
                    'datestamp' => '2021-12-09T16:56:19Z',
                    '@status' => 'deleted',
                ],
                'metadata' => [
                    'oai_dc:dc' => [
                        '@xsi:schemaLocation' => 'http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd',
                        // NOSONAR
                        'dc:date' => '2021-12-09T16:56:19Z',
                        'dc:title' => 'seda2.1',
                        'dc:description' => '',
                        'dc:identifier' => 'sa_4',
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $results);

        $response = $this->getResponse('/api/oaipmh?verb=ListRecords&resumptionToken=' . $token['@']);
        $token = Hash::get($response, 'ListRecords.resumptionToken');
        $this->assertArrayNotHasKey('@', $token);
        $this->assertEquals(4, $token['@cursor']);
        $results = Hash::get($response, 'ListRecords.record');
        $expected = [
            'header' => [
                'identifier' => 'sa_5',
                'datestamp' => '2021-12-09T17:19:06Z',
                '@status' => 'deleted',
            ],
            'metadata' => [
                'oai_dc:dc' => [
                    '@xsi:schemaLocation' => 'http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd',
                    // NOSONAR
                    'dc:date' => '2021-12-09T17:19:06Z',
                    'dc:title' => 'seda2.1',
                    'dc:description' => '',
                    'dc:identifier' => 'sa_5',
                ],
            ],
        ];
        $this->assertEquals($expected, $results);
    }

    /**
     * testListRecordsSEDA02
     */
    public function testListRecordsSEDA02()
    {
        TableRegistry::getTableLocator()
            ->get('Transfers')
            ->updateAll(
                ['message_version' => 'seda0.2'],
                []
            );

        VolumeSample::volumePutContent(
            'sp/profile1/2021-12/sa_2/management_data/sa_2_description.xml',
            VolumeSample::getSampleContent('sample_seda02_description.xml'),
            true
        );
        Configure::write('OAIPMH.maxListSize', 1);

        $response = $this->getResponse('/api/oaipmh?verb=ListRecords&metadataPrefix=seda0.2');
        $token = Hash::get($response, 'ListRecords.resumptionToken');
        $this->assertEquals(0, $token['@cursor']);
        $results = Hash::get($response, 'ListRecords.record');
        $expected = [
            'header' => [
                'identifier' => 'sa_1',
                'datestamp' => '2021-12-09T15:24:21Z',
            ],
            'metadata' => [
                'seda' => $results['metadata']['seda'],
            ],
            'about' => [
                [
                    'OriginatingAgency' => $sa = [
                        'Identification' => 'sa',
                        'Name' => 'Mon Service d\'archive',
                    ],
                ],
                [
                    'TransferringAgency' => $sa,
                ],
            ],
        ];
        $this->assertEquals($expected, $results);
    }

    /**
     * testListRecordsSEDA10
     */
    public function testListRecordsSEDA10()
    {
        $response = $this->getResponse('/api/oaipmh?verb=ListRecords&metadataPrefix=seda1.0');
        $token = Hash::get($response, 'ListRecords.resumptionToken');
        $this->assertNull($token);
        $results = Hash::get($response, 'ListRecords.record');
        $expected = [
            'header' => [
                'identifier' => 'sa_1',
                'datestamp' => '2021-12-09T15:24:21Z',
            ],
            'metadata' => [
                'seda' => $results['metadata']['seda'],
            ],
            'about' => [
                [
                    'OriginatingAgency' => [
                        'Identification' => 'sa',
                        'Name' => 'Mon Service d\'archive',
                    ],
                ],
                [
                    'TransferringAgency' => [
                        'Identification' => 'sa',
                        'Name' => 'Mon Service d\'archive',
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $results);
    }

    /**
     * testListRecordsSEDA21
     */
    public function testListRecordsSEDA21()
    {
        $response = $this->getResponse('/api/oaipmh?verb=ListRecords&metadataPrefix=seda2.1');
        $token = Hash::get($response, 'ListRecords.resumptionToken');
        $this->assertNull($token);
        $results = Hash::get($response, 'ListRecords.record');
        $expected = [
            [
                'header' => [
                    'identifier' => 'sa_2',
                    'datestamp' => '2021-12-09T15:30:29Z',
                ],
                'metadata' => [
                    'seda' => $results[0]['metadata']['seda'],
                ],
                'about' => $about = [
                    [
                        'OriginatingAgency' => [
                            'Identifier' => 'sp',
                            'OrganizationDescriptiveMetadata' => ['Name' => 'Mon service producteur'],
                        ],
                    ],
                    [
                        'TransferringAgency' => [
                            'Identifier' => 'sa',
                            'OrganizationDescriptiveMetadata' => ['Name' => 'Mon Service d\'archive'],
                        ],
                    ],
                ],
            ],
            [
                'header' => [
                    'identifier' => 'sa_3',
                    'datestamp' => '2021-12-09T16:22:20Z',
                    '@status' => 'deleted',
                ],
                'metadata' => [
                    'seda' => $results[1]['metadata']['seda'],
                ],
                'about' => $about,
            ],
            [
                'header' => [
                    'identifier' => 'sa_4',
                    'datestamp' => '2021-12-09T16:56:19Z',
                    '@status' => 'deleted',
                ],
                'metadata' => [
                    'seda' => $results[2]['metadata']['seda'],
                ],
                'about' => $about,
            ],
            [
                'header' => [
                    'identifier' => 'sa_5',
                    'datestamp' => '2021-12-09T17:19:06Z',
                    '@status' => 'deleted',
                ],
                'metadata' => [
                    'seda' => $results[3]['metadata']['seda'],
                ],
                'about' => $about,
            ],
        ];
        $this->assertEquals($expected, $results);
    }

    /**
     * testListRecordEmptyResult
     */
    public function testListRecordEmptyResult()
    {
        $response = $this->getResponse(
            '/api/oaipmh?verb=ListRecords&metadataPrefix=seda1.0&from=2015-01-01T00:00:00Z'
            . '&until=2016-01-01T00:00:00Z&set=OriginatingAgency:sa'
        );
        $expected = [
            '@code' => 'noRecordsMatch',
            '@' => 'The combination of the values of the from, until, set and metadataPrefix
         arguments results in an empty list.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testGetRecordWithoutIdentifierFails
     */
    public function testGetRecordWithoutIdentifierFails()
    {
        $response = $this->getResponse('/api/oaipmh/?verb=GetRecord');
        $expected = [
            '@code' => 'badArgument',
            '@' => 'The request is missing the "identifier" argument.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testGetRecordWithoutMetadataPrefixFails
     */
    public function testGetRecordWithoutMetadataPrefixFails()
    {
        $response = $this->getResponse('/api/oaipmh/?verb=GetRecord&identifier=test');
        $expected = [
            '@code' => 'badArgument',
            '@' => 'The request is missing the "metadataPrefix" argument.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testGetRecordNotExists
     */
    public function testGetRecordNotExists()
    {
        $response = $this->getResponse('/api/oaipmh/?verb=GetRecord&metadataPrefix=oai_dc&identifier=test');
        $expected = [
            '@code' => 'idDoesNotExist',
            '@' => 'The value of the identifier argument is unknown or illegal in this repository.',
        ];
        $this->assertEquals($expected, $response['error']);
    }

    /**
     * testGetRecordOaidc
     */
    public function testGetRecordOaidc()
    {
        $response = $this->getResponse('/api/oaipmh/?verb=GetRecord&metadataPrefix=oai_dc&identifier=sa_1');
        $expected = [
            'responseDate' => $response['responseDate'],
            'request' => [
                '@verb' => 'GetRecord',
                '@metadataPrefix' => 'oai_dc',
                '@identifier' => 'sa_1',
                '@' => Router::url('/api/oaipmh', true),
            ],
            'GetRecord' => [
                'record' => [
                    'header' => [
                        'identifier' => 'sa_1',
                        'datestamp' => '2021-12-09T15:24:21Z',
                    ],
                    'metadata' => [
                        'oai_dc:dc' => [
                            '@xsi:schemaLocation' => 'http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd',
                            // NOSONAR
                            'dc:date' => '2021-12-09T15:24:21Z',
                            'dc:title' => 'seda1.0',
                            'dc:description' => '',
                            'dc:identifier' => 'sa_1',
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $response);
    }

    /**
     * testGetRecordSeda02
     */
    public function testGetRecordSeda02()
    {
        TableRegistry::getTableLocator()
            ->get('Transfers')
            ->updateAll(
                ['message_version' => 'seda0.2'],
                []
            );

        $response = $this->getResponse('/api/oaipmh/?verb=GetRecord&metadataPrefix=seda0.2&identifier=sa_1');
        $expected = [
            'responseDate' => $response['responseDate'],
            'request' => [
                '@verb' => 'GetRecord',
                '@metadataPrefix' => 'seda0.2',
                '@identifier' => 'sa_1',
                '@' => Router::url('/api/oaipmh', true),
            ],
            'GetRecord' => [
                'record' => [
                    'header' => [
                        'identifier' => 'sa_1',
                        'datestamp' => '2021-12-09T15:24:21Z',
                    ],
                    'metadata' => [
                        'seda' => $response['GetRecord']['record']['metadata']['seda'],
                    ],
                    'about' => [
                        [
                            'OriginatingAgency' => [
                                'Identification' => 'sa',
                                'Name' => 'Mon Service d\'archive',
                            ],
                        ],
                        [
                            'TransferringAgency' => [
                                'Identification' => 'sa',
                                'Name' => 'Mon Service d\'archive',
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $response);
    }

    /**
     * testGetRecordSeda10
     */
    public function testGetRecordSeda10()
    {
        $response = $this->getResponse('/api/oaipmh/?verb=GetRecord&metadataPrefix=seda1.0&identifier=sa_1');
        $expected = [
            'responseDate' => $response['responseDate'],
            'request' => [
                '@verb' => 'GetRecord',
                '@metadataPrefix' => 'seda1.0',
                '@identifier' => 'sa_1',
                '@' => Router::url('/api/oaipmh', true),
            ],
            'GetRecord' => [
                'record' => [
                    'header' => [
                        'identifier' => 'sa_1',
                        'datestamp' => '2021-12-09T15:24:21Z',
                    ],
                    'metadata' => [
                        'seda' => $response['GetRecord']['record']['metadata']['seda'],
                    ],
                    'about' => [
                        [
                            'OriginatingAgency' => [
                                'Identification' => 'sa',
                                'Name' => 'Mon Service d\'archive',
                            ],
                        ],
                        [
                            'TransferringAgency' => [
                                'Identification' => 'sa',
                                'Name' => 'Mon Service d\'archive',
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $response);
    }
    //------------------------------------------------------------------------------------------------------------------
    // Utils
    //------------------------------------------------------------------------------------------------------------------

    /**
     * testGetRecordSeda21
     */
    public function testGetRecordSeda21()
    {
        $response = $this->getResponse('/api/oaipmh/?verb=GetRecord&metadataPrefix=seda2.1&identifier=sa_2');
        $expected = [
            'responseDate' => $response['responseDate'],
            'request' => [
                '@verb' => 'GetRecord',
                '@metadataPrefix' => 'seda2.1',
                '@identifier' => 'sa_2',
                '@' => Router::url('/api/oaipmh', true),
            ],
            'GetRecord' => [
                'record' => [
                    'header' => [
                        'identifier' => 'sa_2',
                        'datestamp' => '2021-12-09T15:30:29Z',
                    ],
                    'metadata' => [
                        'seda' => $response['GetRecord']['record']['metadata']['seda'],
                    ],
                    'about' => [
                        [
                            'OriginatingAgency' => [
                                'Identifier' => 'sp',
                                'OrganizationDescriptiveMetadata' => ['Name' => 'Mon service producteur'],
                            ],
                        ],
                        [
                            'TransferringAgency' => [
                                'Identifier' => 'sa',
                                'OrganizationDescriptiveMetadata' => ['Name' => 'Mon Service d\'archive'],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $response);
    }
}
