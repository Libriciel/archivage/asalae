<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\ArchiveFilesController;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use AsalaeCore\Utility\Exec;
use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\ArchiveFilesController Test Case
 */
#[UsesClass(ArchiveFilesController::class)]
class ArchiveFilesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use SaveBufferTrait;
    use AutoFixturesTrait;

    public const string LIFECYCLE_PREMIS = ASALAE_CORE_TEST_DATA . DS . 'sample_premis.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        FIXTURES_VOLUMES,
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Configure::write('App.paths.data', TMP_TESTDIR);
        VolumeSample::init();
        VolumeSample::insertSample(
            'sample_premis.xml',
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
        );
        Configure::write('Archives.updateLifecycle.path', TMP_TESTDIR);
        Configure::write('Archives.updateDescription.path', TMP_TESTDIR);
        Configure::write('Archives.updateLifecycle.cache_duration', 0);
        Configure::write('Archives.updateDescription.cache_duration', 0);
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Filesystem::setNamespace();
        Filesystem::reset();
        Utility::reset();
    }

    /**
     * Test download method
     */
    public function testDownload()
    {
        VolumeSample::$volume2->filePutContent(
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            'testfile'
        );

        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/archive-files/download/2'
        );
        $this->assertEquals('testfile', $output);
    }
}
