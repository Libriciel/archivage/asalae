<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\AccessRuleCodesController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(AccessRuleCodesController::class)]
class AccessRuleCodesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        'app.AccessRules',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->httpGet('/access-rule-codes/index');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);

        $this->httpGet('/access-rule-codes?code[0]=AR_001');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $this->httpGet('/access-rule-codes/add');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $data = [
            'code' => 'test_add_1',
            'name' => 'Test Add 1',
            'description' => 'sample',
            'duration' => 'P1Y',
        ];

        $this->httpPost('/access-rule-codes/add', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['id']);
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->httpGet('/access-rule-codes/edit/26');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $data = [
            'code' => 'AR_101',
            'name' => 'Test 2',
            'description' => 'sample',
            'duration' => 'P1Y',
        ];

        $this->httpPut('/access-rule-codes/edit/26', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['id']);
        $this->assertEquals('AR_101', $response['code']);
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $this->httpDelete('/access-rule-codes/delete/26');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']);
        $this->assertEquals('done', $response['report']);

        $this->httpDelete('/access-rule-codes/delete/27');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']);
        $this->assertNotEquals('done', $response['report']);
    }

    /**
     * testView
     */
    public function testView()
    {
        $response = $this->httpGet('/access-rule-codes/view/26');
        $this->assertResponseCode(200);
        $this->assertTrue(
            isset($response->entity->code)
        );
        $this->assertEquals('AR_001', $response->entity->code);
    }
}
