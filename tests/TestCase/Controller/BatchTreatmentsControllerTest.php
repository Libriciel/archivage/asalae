<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\BatchTreatmentsController;
use Asalae\Test\Mock\FakeEmitter;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\BatchTreatmentsController Test Case
 */
#[UsesClass(BatchTreatmentsController::class)]
class BatchTreatmentsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        'app.Agreements',
        'app.AppraisalRules',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveUnits',
        'app.ArchiveUnitsBatchTreatments',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.BatchTreatments',
        'app.EventLogs',
        'app.Profiles',
        'app.StoredFiles',
        'app.Transfers',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Utility::set('Beanstalk', FakeEmitter::class);
        $loc = TableRegistry::getTableLocator();
        $BatchTreatments = $loc->get('BatchTreatments');
        $bt = $BatchTreatments->newEntity(
            [
                'archival_agency_id' => 2,
                'user_id' => 1,
                'state' => 'created',
                'last_state_update' => null,
                'states_history' => null,
                'data' => '{"type":"name","name__research":"foo","name__replace":"bar"}',
            ]
        );
        $BatchTreatments->saveOrFail($bt);
        $ArchiveUnitsBatchTreatments = $loc->get('ArchiveUnitsBatchTreatments');
        $auBt = $ArchiveUnitsBatchTreatments->newEntity(
            [
                'archive_unit_id' => 1,
                'batch_treatment_id' => $bt->id,
            ]
        );
        $ArchiveUnitsBatchTreatments->saveOrFail($auBt);
    }

    /**
     * testAdd1
     */
    public function testAdd1()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnitsBatchTreatments = $loc->get('ArchiveUnitsBatchTreatments');
        $BatchTreatments = $loc->get('BatchTreatments');
        $linksCount = $ArchiveUnitsBatchTreatments->find()->count();
        $baCount = $BatchTreatments->find()->count();

        $this->get('/batch-treatments/add1');
        $this->assertResponseCode(200);

        $data = [
            'type' => 'name',
            'name__research' => 'foo',
            'name__replace' => 'bar',
        ];
        $this->post('/batch-treatments/add1', $data);
        $this->assertResponseCode(200);
        $this->assertHeaderContains('X-Asalae-Success', 'true');
        $this->assertCount($baCount + 1, $BatchTreatments->find());
        $this->assertGreaterThan($linksCount, $ArchiveUnitsBatchTreatments->find()->count());
    }

    /**
     * testAdd2
     */
    public function testAdd2()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnitsBatchTreatments = $loc->get('ArchiveUnitsBatchTreatments');
        $BatchTreatments = $loc->get('BatchTreatments');
        $linksCount = $ArchiveUnitsBatchTreatments->find()->count();
        $baCount = $BatchTreatments->find()->count();

        $this->get('/batch-treatments/add2/1');
        $this->assertResponseCode(200);

        $this->put('/batch-treatments/add2/1');
        $this->assertResponseCode(200);
        $this->assertHeaderContains('X-Asalae-Success', 'true');

        $this->delete('/batch-treatments/add2/1');
        $this->assertResponseCode(200);
        $this->assertHeaderContains('X-Asalae-Success', 'true');
        $this->assertCount($baCount - 1, $BatchTreatments->find());
        $this->assertLessThan($linksCount, $ArchiveUnitsBatchTreatments->find()->count());
    }

    /**
     * testAdd3
     */
    public function testAdd3()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnitsBatchTreatments = $loc->get('ArchiveUnitsBatchTreatments');
        $BatchTreatments = $loc->get('BatchTreatments');
        $linksCount = $ArchiveUnitsBatchTreatments->find()->count();
        $baCount = $BatchTreatments->find()->count();

        $this->get('/batch-treatments/add3/1');
        $this->assertResponseCode(200);

        $this->put('/batch-treatments/add3/1');
        $this->assertResponseCode(200);
        $this->assertHeaderContains('X-Asalae-Success', 'true');

        $this->delete('/batch-treatments/add3/1');
        $this->assertResponseCode(200);
        $this->assertHeaderContains('X-Asalae-Success', 'true');
        $this->assertCount($baCount - 1, $BatchTreatments->find());
        $this->assertLessThan($linksCount, $ArchiveUnitsBatchTreatments->find()->count());
    }

    /**
     * testPaginateArchiveUnits
     */
    public function testPaginateArchiveUnits()
    {
        $this->httpGet('/batch-treatments/paginate-archive-units/1');
        $this->assertResponseCode(200);
    }

    /**
     * testRemoveArchiveUnit
     */
    public function testRemoveArchiveUnit()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnitsBatchTreatments = $loc->get('ArchiveUnitsBatchTreatments');
        $linksCount = $ArchiveUnitsBatchTreatments->find()->count();
        $this->httpDelete('/batch-treatments/remove-archive-unit/1/1');
        $this->assertResponseCode(200);
        $this->assertCount($linksCount - 1, $ArchiveUnitsBatchTreatments->find());
    }
}
