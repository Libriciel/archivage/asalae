<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\AppraisalRuleCodesController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(AppraisalRuleCodesController::class)]
class AppraisalRuleCodesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        'app.AppraisalRules',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->httpGet('/appraisal-rule-codes/index');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);

        $this->httpGet('/appraisal-rule-codes?code[0]=AP_001');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $this->httpGet('/appraisal-rule-codes/add');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $data = [
            'code' => 'test_add_1',
            'name' => 'Test Add 1',
            'description' => 'sample',
            'duration' => 'P1Y',
        ];

        $this->httpPost('/appraisal-rule-codes/add', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['id']);
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->httpGet('/appraisal-rule-codes/edit/152');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $data = [
            'code' => 'AR_101',
            'name' => 'Test 2',
            'description' => 'sample',
            'duration' => 'P1Y',
        ];

        $this->httpPut('/appraisal-rule-codes/edit/152', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['id']);
        $this->assertEquals('AR_101', $response['code']);
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $this->httpDelete('/appraisal-rule-codes/delete/152');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']);
        $this->assertEquals('done', $response['report']);

        $this->httpDelete('/appraisal-rule-codes/delete/153');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']);
        $this->assertNotEquals('done', $response['report']);
    }

    /**
     * testView
     */
    public function testView()
    {
        $response = $this->httpGet('/appraisal-rule-codes/view/152');
        $this->assertResponseCode(200);
        $this->assertTrue(
            isset($response->entity->code)
        );
        $this->assertEquals('AP_001', $response->entity->code);
    }
}
