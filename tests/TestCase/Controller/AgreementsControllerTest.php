<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\AgreementsController;
use Asalae\Model\Entity\Agreement;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(AgreementsController::class)]
class AgreementsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        'app.AgreementsOriginatingAgencies',
        'app.AgreementsProfiles',
        'app.AgreementsServiceLevels',
        'app.AgreementsTransferringAgencies',
        'app.FileExtensions',
        'app.Pronoms',
        'app.Transfers',
        'app.TypeEntities',
        'app.ValidationActors',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $response = $this->httpGet('/agreements/index');
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response->data) && is_array($response->data));
        $this->assertEquals(2, count($response->data));

        $response = $this->httpGet(
            '/agreements/index/?name[0]=Mon accord*&identifier[0]=agreem*&active[0]=0&active[0]=1&sort=name&direction=asc'
        );
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response->data);

        $response = $this->httpGet('/agreements/index/?favoris[0]=1&sort=favorite&direction=desc');
        $this->assertResponseCode(200);
        $this->assertEmpty($response->data);
    }

    /**
     * testAdd1
     */
    public function testAdd1()
    {
        $this->httpGet('/agreements/add1');
        $this->assertResponseCode(200);

        $data = [
            'identifier' => 'test-add',
            'name' => 'Test add',
            'description' => "test d'un ajout d'accord de versement",
            'active' => true,
            'default_agreement' => false,
            'auto_validate' => true,
            'proper_chain_id' => 1,
            'improper_chain_id' => 2,
            'secure_data_space_id' => 1,
            'date_begin' => '2018-01-01',
            'date_end' => '2019-01-01',
        ];
        $this->httpPost('/agreements/add1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertHeader('X-Asalae-Step', 'addAgreementStep2()');
    }

    /**
     * testAdd2
     */
    public function testAdd2()
    {
        $this->httpGet('/agreements/add2');
        $this->assertResponseCode(200);

        $data = [
            'allow_all_transferring_agencies' => false,
            'transferring_agencies' => ['_ids' => [1]],
            'allow_all_originating_agencies' => false,
            'originating_agencies' => ['_ids' => [1]],
            'allow_all_service_levels' => false,
            'service_levels' => ['_ids' => [1]],
            'allow_all_profiles' => false,
            'profiles' => ['_ids' => [1]],
        ];
        $this->httpPost('/agreements/add2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $this->session(
            $this->genericSessionData + [
                'Agreement' => [
                    'entity' => new Agreement(
                        [
                            'identifier' => 'test-add',
                            'name' => 'Test add',
                            'description' => "test d'un ajout d'accord de versement",
                            'active' => true,
                            'default_agreement' => false,
                            'auto_validate' => true,
                            'proper_chain_id' => 1,
                            'improper_chain_id' => 1,
                            'secure_data_space_id' => 1,
                            'date_begin' => '2018-01-01',
                            'date_end' => '2019-01-01',
                        ]
                    ),
                ],
            ]
        );
        $this->httpPost('/agreements/add2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertHeader('X-Asalae-Step', 'addAgreementStep3()');
    }

    /**
     * testAdd3
     */
    public function testAdd3()
    {
        $this->httpGet('/agreements/add3');
        $this->assertResponseCode(200);

        $data = [
            'default_agreement' => false,
            'allowed_formats' => '["fmt/101", "x-fmt/222"]',
            'require_files' => true,
            'alert_on_invalid_files' => true,
            'max_transfers' => 100,
            'transfer_period' => 'P1M',
            'max_size_per_transfer' => pow(1024, 3),
            'total_size' => pow(1024 * 2, 4),
        ];
        $this->httpPost('/agreements/add3', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $this->session(
            $this->genericSessionData + [
                'Agreement' => [
                    'entity' => new Agreement(
                        [
                            'org_entity_id' => 2,
                            'identifier' => 'test-add',
                            'name' => 'Test add',
                            'description' => "test d'un ajout d'accord de versement",
                            'active' => true,
                            'default_agreement' => false,
                            'auto_validate' => true,
                            'proper_chain_id' => 1,
                            'improper_chain_id' => 1,
                            'secure_data_space_id' => 1,
                            'date_begin' => '2018-01-01',
                            'date_end' => '2019-01-01',
                            'allow_all_transferring_agencies' => false,
                            'transferring_agencies' => ['_ids' => [1]],
                            'allow_all_originating_agencies' => false,
                            'originating_agencies' => ['_ids' => [1]],
                            'allow_all_service_levels' => false,
                            'service_levels' => ['_ids' => [1]],
                            'allow_all_profiles' => false,
                            'profiles' => ['_ids' => [1]],
                        ]
                    ),
                ],
            ]
        );
        $this->httpPost('/agreements/add3', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->httpGet('/agreements/edit/1');
        $this->assertResponseCode(200);

        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Transfers->updateAll(['agreement_id' => null], []);

        $data = [
            'org_entity_id' => 2,
            'identifier' => 'test-add',
            'name' => 'Test add',
            'description' => "test d'un ajout d'accord de versement",
            'active' => true,
            'default_agreement' => true,
            'auto_validate' => true,
            'proper_chain_id' => 1,
            'improper_chain_id' => 2,
            'secure_data_space_id' => 1,
            'date_begin' => '2018-01-01',
            'date_end' => '2019-01-01',
            'allow_all_transferring_agencies' => false,
            'transferring_agencies' => ['_ids' => [1]],
            'allow_all_originating_agencies' => false,
            'originating_agencies' => ['_ids' => [1]],
            'allow_all_service_levels' => false,
            'service_levels' => ['_ids' => [1]],
            'allow_all_profiles' => false,
            'profiles' => ['_ids' => [1]],
            'allowed_formats' => '["fmt/101", "x-fmt/222"]',
            'require_files' => true,
            'alert_on_invalid_files' => true,
            'max_transfers' => 100,
            'transfer_period' => 'P1M',
            'max_size_per_transfer' => pow(1024, 3),
            'total_size' => pow(1024 * 2, 4),
        ];
        $this->httpPut('/agreements/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $locator = TableRegistry::getTableLocator();
        $locator->get('Archives')->updateAll(['agreement_id' => null], []);
        $locator->get('Agreements')->updateAll(['default_agreement' => false], []);
        $response = $this->httpDelete('/agreements/delete/1');
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->report));
        $this->assertStringContainsString('done', $response->report);
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpDelete('/agreements/view/1');
        $this->assertResponseCode(200);
    }

    /**
     * testSetDefault
     */
    public function testSetDefault()
    {
        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $this->assertTrue((bool)$Agreements->get(1)->get('default_agreement'));
        $this->assertFalse($Agreements->get(2)->get('default_agreement'));

        $this->httpPut('/agreements/set-default/2');
        $this->assertResponseCode(200);

        $this->assertFalse($Agreements->get(1)->get('default_agreement'));
        $this->assertTrue((bool)$Agreements->get(2)->get('default_agreement'));
    }

    /**
     * testApiAdd
     */
    public function testApiAdd()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'Agreements'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        $data = [
            'org_entity_id' => 2,
            'identifier' => 'api_test',
            'name' => 'api_test',
            'description' => 'api_test',
            'active' => 'true',
            'default_agreement' => 'false',
            'transferring_agencies' => null,
            'originating_agencies' => '4,5',
            'profiles' => null,
            'service_levels' => null,
            'proper_chain_id' => null,
            'improper_chain_id' => 2,
            'date_begin' => '2020-01-01',
            'date_end' => '2120-01-01',
            'allowed_formats' => 'ext-pdf,mime-application/pdf,mime-application/x-pdf',
            'max_tranfers' => null,
            'transfer_period' => null,
            'max_size_per_transfer' => null,
        ];
        $json = $this->httpPost('/api/agreements', $data, true);
        $this->assertResponseCode(200);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('success', $json);
        $this->assertTrue($json['success']);
        $this->assertArrayHasKey('id', $json);
    }

    /**
     * testApiAddFail
     */
    public function testApiAddFail()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'Agreements'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        $data = [
            'org_entity_id' => 2,
            'identifier' => 'api_test',
            'name' => 'api_test',
            'description' => 'api_test',
            'active' => 'false',
            'default_agreement' => 'true',
            'transferring_agencies' => null,
            'originating_agencies' => '4,5',
            'profiles' => null,
            'service_levels' => null,
            'proper_chain_id' => null,
            'improper_chain_id' => 2,
            'date_begin' => '2020-01-01',
            'date_end' => '2120-01-01',
            'allowed_formats' => 'ext-pdf,mime-application/pdf,mime-application/x-pdf',
            'max_tranfers' => null,
            'transfer_period' => null,
            'max_size_per_transfer' => null,
        ];
        $json = $this->httpPost('/api/agreements', $data, true);
        $this->assertResponseCode(200);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('success', $json);
        $this->assertFalse($json['success']);
    }
}
