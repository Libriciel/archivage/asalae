<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\DestructionNotificationsController;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\DestructionNotificationsController Test Case
 *
 */
#[UsesClass(DestructionNotificationsController::class)]
class DestructionNotificationsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;
    use SaveBufferTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_VOLUMES,
        'app.ArchiveBinaries',
        'app.ArchiveBinariesTechnicalArchiveUnits',
        'app.ArchiveUnits',
        'app.ArchiveUnitsDestructionRequests',
        'app.DestructionNotificationXpaths',
        'app.DestructionNotifications',
        'app.DestructionRequests',
        'app.EventLogs',
        'app.TechnicalArchives',
        'app.TechnicalArchiveUnits',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Configure::write('App.paths.data', TMP_TESTDIR);
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        VolumeSample::init();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        VolumeSample::destroy();
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->httpGet('/destruction-notifications/index');
        $this->assertResponseCode(200);
    }

    /**
     * testDownload
     */
    public function testDownload()
    {
        if (is_file($uri = TMP_TESTDIR . DS . 'destruction_notifications/1/sa_ADN_1_destruction_notification.xml')) {
            unlink($uri);
        }
        Filesystem::createDummyFile($uri, 10);
        $content = file_get_contents($uri);
        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/destruction-notifications/download/1'
        );
        $this->assertResponseCode(200);
        unlink($uri);
        $this->assertEquals($content, $output);
    }

    /**
     * testdestructionCertificate
     */
    public function testdestructionCertificate()
    {
        TableRegistry::getTableLocator()->get('DestructionRequests')
            ->updateAll(['certified' => true], ['id' => 1]);
        $this->preserveBuffer(
            [$this, 'httpGet'],
            '/destruction-notifications/destruction-certificate/1'
        );
        $this->assertResponseCode(200);
        $this->assertHeader('Content-Type', 'application/pdf');
    }
}
