<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\OrgEntitiesController;
use Asalae\Model\Table\ConfigurationsTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(OrgEntitiesController::class)]
class OrgEntitiesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        'app.AccessRuleCodes',
        'app.AgreementsOriginatingAgencies',
        'app.AgreementsTransferringAgencies',
        'app.AppraisalRuleCodes',
        'app.ArchivingSystems',
        'app.Configurations',
        'app.ConversionPolicies',
        'app.Eaccpfs',
        'app.Fileuploads',
        'app.Siegfrieds',
        'app.Ldaps',
        'app.OrgEntitiesTimestampers',
        'app.RolesTypeEntities',
        'app.SecureDataSpaces',
        'app.Sequences',
        'app.Sessions',
        'app.ValidationStages',
        'app.ValidationActors',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('OrgEntities.public_dir', TMP_TESTDIR . DS . 'webroot');
        Filesystem::reset();
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        $Exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => '', 'stderr' => ''])
        );
        Utility::set('Exec', $Exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Filesystem::setNamespace();
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Utility::reset();
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpGet('/OrgEntities/index', true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['chartDatasource']);
        $this->assertNotEmpty($response['chartDatasource']['children']);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/OrgEntities/add/4');
        $this->assertResponseCode(200);

        $data = [
            'name' => 'test',
            'identifier' => 'test',
            'type_entity_id' => 4,
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpPost('/OrgEntities/add/4', $data, true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['id']);
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpGet('/OrgEntities/edit/4', true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $org = $OrgEntities->newEntity(
            [
                'name' => 'test',
                'identifier' => uniqid(),
                'active' => true,
                'parent_id' => 2,
                'archival_agency_id' => 2,
                'type_entity_id' => 3,
            ]
        );
        $OrgEntities->saveOrFail($org);

        $parent = $OrgEntities->get(2);
        $this->genericSessionData['Auth']->get('org_entity')->get('archival_agency')->set('rght', $parent->get('rght'));
        $this->session($this->genericSessionData);

        $data = [
            'name' => 'Un test',
            'identifier' => 'test766',
            'type_entity_id' => 4,
            'parent_id' => $org->id,
            'active' => true,
        ];
        $response = $this->httpPut('/OrgEntities/edit/4', $data, true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['parent_id']);
        $this->assertEquals($org->id, $response['parent_id']);
        $this->assertHeaderContains('X-Asalae-Reload', 'true');
    }

    /**
     * testEditSA
     */
    public function testEditSA()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpGet('/OrgEntities/edit/2', true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $data = [
            'name' => 'Un test',
            'identifier' => 'test766',
            'type_entity_id' => 2,
            'active' => true,
            'description' => 'new description',
        ];
        $response = $this->httpPut('/OrgEntities/edit/2', $data, true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['description']);
        $this->assertEquals('new description', $response['description']);
        $this->assertHeaderContains('X-Asalae-Reload', 'true');
    }

    /**
     * testEditConversions
     */
    public function testEditConversions()
    {
        $data = [
            'document' => [
                'preservation' => [
                    'method' => 'always',
                    'format' => 'jpg',
                ],
            ],
        ];
        $this->httpPut('/org-entities/edit-conversions/2', $data);
        $this->assertResponseContains('done');
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $org = $OrgEntities->newEntity(
            [
                'name' => 'test',
                'identifier' => uniqid(),
                'active' => true,
                'parent_id' => 2,
                'archival_agency_id' => 2,
                'type_entity_id' => 3,
            ]
        );
        $OrgEntities->saveOrFail($org);

        $parent = $OrgEntities->get(2);
        $this->genericSessionData['Auth']->get('org_entity')->get('archival_agency')->set('rght', $parent->get('rght'));
        $this->session($this->genericSessionData);

        $response = $this->httpDelete('/OrgEntities/delete/' . $org->id, [], true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']);
        $this->assertEquals('done', $response['report']);

        $this->session(['Auth' => ['admin' => true]]);
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->delete('/OrgEntities/delete/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(403);
        $this->assertNotEmpty($response['message']);
        $this->assertEquals(__("Tentative de suppression d'une entité protégée"), $response['message']);
    }

    /**
     * testEditParentId
     */
    public function testEditParentId()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $org = $OrgEntities->newEntity(
            [
                'name' => 'test',
                'identifier' => uniqid(),
                'active' => true,
                'parent_id' => 2,
                'archival_agency_id' => 2,
                'type_entity_id' => 3,
            ]
        );
        $OrgEntities->saveOrFail($org);

        $parent = $OrgEntities->get(2);
        $this->genericSessionData['Auth']->get('org_entity')->get('archival_agency')->set('rght', $parent->get('rght'));
        $this->session($this->genericSessionData);

        $data = [
            'parent_id' => $org->id,
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpPut('/OrgEntities/editParentId/4', $data, true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['success']));
        $this->assertTrue($response['success']);

        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $orgChild = $OrgEntities->newEntity(
            [
                'name' => 'test',
                'identifier' => uniqid(),
                'active' => true,
                'parent_id' => 4,
                'archival_agency_id' => 2,
                'type_entity_id' => 3,
            ]
        );
        $OrgEntities->saveOrFail($orgChild);

        $parent = $OrgEntities->get(2);
        $this->genericSessionData['Auth']->get('org_entity')->get('archival_agency')->set('rght', $parent->get('rght'));
        $this->session($this->genericSessionData);

        // 4 est le parent de orgChild... tentative de mettre parent_id=orChild sur id 4
        $data2 = [
            'parent_id' => $orgChild->id,
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpPut('/OrgEntities/editParentId/4', $data2, true);
        $this->assertResponseCode(200);
        $this->assertEmpty($response['success']);
    }

    /**
     * testDeleteBackground
     */
    public function testDeleteBackground()
    {
        $Configurations = TableRegistry::getTableLocator()->get('Configurations');
        $Configurations->saveOrFail(
            $Configurations->newEntity(
                [
                    'org_entity_id' => 4,
                    'name' => 'background-image',
                    'setting' => DS . 'org-entity-data' . DS . '4' . DS . 'testunit.png',
                ]
            )
        );

        $dataDir = rtrim(Configure::read('App.paths.data'), '/');
        $publicDir = Configure::read('App.paths.config', $dataDir . DS . 'config');
        $webrootDir = rtrim(Configure::read('OrgEntities.public_dir', WWW_ROOT), DS);
        Filesystem::dumpFile($publicDir . DS . 'org-entity-data/4/background.css', '* { background: red; }');
        Filesystem::dumpFile($webrootDir . DS . 'org-entity-data/4/background.css', '* { background: red; }');
        Filesystem::dumpFile($file1 = $publicDir . DS . 'org-entity-data/4/testunit.png', 'testfile');
        Filesystem::dumpFile($file2 = $webrootDir . DS . 'org-entity-data/4/testunit.png', 'testfile');
        $this->assertFileExists($file1);
        $this->assertFileExists($file2);

        $this->httpDelete('/OrgEntities/deleteBackground/4');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
        $this->assertFileDoesNotExist($file1);
        $this->assertFileDoesNotExist($file2);
    }

    /**
     * testGetAvailablesRoles
     */
    public function testGetAvailablesRoles()
    {
        $this->httpGet('/OrgEntities/getAvailablesRoles/2');
        $this->assertResponseCode(200);
        $this->assertResponseContains('Archiviste');
        $this->assertResponseContains('ws versant');
    }

    /**
     * testGetAvailablesRolesPerson
     */
    public function testGetAvailablesRolesPerson()
    {
        $this->httpGet('/OrgEntities/getAvailablesRoles/2?agent_type=person');
        $this->assertResponseCode(200);
        $this->assertResponseContains('Archiviste');
        $this->assertResponseNotContains('Webservice versant');
    }

    /**
     * testGetAvailablesRolesSoftware
     */
    public function testGetAvailablesRolesSoftware()
    {
        $this->httpGet('/OrgEntities/getAvailablesRoles/2?agent_type=software');
        $this->assertResponseCode(200);
        $this->assertResponseNotContains('Archiviste');
        $this->assertResponseContains('ws versant');
    }

    /**
     * testEditConfig
     */
    public function testEditConfig()
    {
        $Configurations = TableRegistry::getTableLocator()->get('Configurations');
        $Configurations->saveOrFail(
            $conf = $Configurations->newEntity(
                [
                    'org_entity_id' => 2,
                    'name' => 'background-image',
                    'setting' => DS . 'org-entity-data' . DS . '4' . DS . 'testunit.png',
                ]
            )
        );
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        Filesystem::dumpFile($conf->get('data_filename'), 'testfile');
        Filesystem::dumpFile($conf->get('webroot_filename'), 'testfile');

        $file = TMP_TESTDIR . $conf->get('setting');
        Filesystem::createDummyFile($file, 1024);
        Filesystem::createDummyFile($Fileuploads->get(1)->get('path'), 1024);

        $data = [
            'fileuploads' => [
                'id' => 1,
            ],
            ConfigurationsTable::CONF_BACKGROUND_IMAGE => '',
            ConfigurationsTable::CONF_BACKGROUND_POSITION => '20px 20px',
            ConfigurationsTable::CONF_HEADER_TITLE => 'test',
            ConfigurationsTable::CONF_MAIL_TITLE_PREFIX => 'test',
            ConfigurationsTable::CONF_MAIL_HTML_SIGNATURE => 'test',
            ConfigurationsTable::CONF_MAIL_TEXT_SIGNATURE => 'test',
            ConfigurationsTable::CONF_DESTR_REQUEST_CHAIN => 4,
            ConfigurationsTable::CONF_DR_NO_DEROGATION_CHAIN => 3,
            ConfigurationsTable::CONF_DR_DEROGATION_CHAIN => 3,
            ConfigurationsTable::CONF_DELIVERY_RETENTION_PERIOD => 7,
            ConfigurationsTable::CONF_RR_CHAIN => 5,
            ConfigurationsTable::CONF_RR_DESTR_DELAY => 1,
            ConfigurationsTable::CONF_OTR_CHAIN => 6,
            ConfigurationsTable::CONF_OTR_DESTR_DELAY => 7,
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_TEXT => 'pdf',
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_TEXT => 'pdf',
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_PDF => 'pdf',
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_PDF => 'pdf',
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_CALC => 'pdf',
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_CALC => 'pdf',
            ConfigurationsTable::CONF_CONV_PRESERV_DOC_PRES => 'pdf',
            ConfigurationsTable::CONF_CONV_DISSEM_DOC_PRES => 'pdf',
            ConfigurationsTable::CONF_CONV_PRESERV_IMAGE => 'png',
            ConfigurationsTable::CONF_CONV_DISSEM_IMAGE => 'png',
            ConfigurationsTable::CONF_CONV_PRESERV_AUDIO => 'ogg',
            ConfigurationsTable::CONF_CONV_DISSEM_AUDIO => 'ogg',
            ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CONTAINER => 'mp4',
            ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CODEC_VIDEO => 'h264',
            ConfigurationsTable::CONF_CONV_PRESERV_VIDEO_CODEC_AUDIO => 'aac',
            ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CONTAINER => 'mp4',
            ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CODEC_VIDEO => 'h264',
            ConfigurationsTable::CONF_CONV_DISSEM_VIDEO_CODEC_AUDIO => 'aac',
            ConfigurationsTable::CONF_ATTEST_ARCHIVING_CERT => 'always',
            ConfigurationsTable::CONF_ATTEST_ARCHIVING_CERT_COMMITMENT => '',
        ];

        $this->httpPut('/OrgEntities/editConfig/4', $data);
        $this->assertResponseCode(200);
        unlink($Fileuploads->get(1)->get('path'));
    }

    /**
     * testApiCreate
     */
    public function testApiCreate()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'OrgEntities'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $this->configRequest(
            [
                'headers' => [
                    'authorization' => 'Basic ' . base64_encode('admin:admin'),
                ],
            ]
        );
        $data = [
            'name' => 'testapi',
            'description' => 'testapi',
            'identifier' => 'testapi',
            'parent_id' => 2,
            'type_entity_id' => 3,
            'timestamper_id' => 1,
            'active' => 'true',
            'default_secure_data_space_id' => null,
            'archival_agency_id' => 2,
            'is_main_archival_agency' => null,
            'secure_data_spaces' => null,
        ];
        $json = $this->httpPost('/api/org-entities', $data, true);
        $this->assertResponseCode(200);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('success', $json);
        $this->assertArrayHasKey('id', $json);
        $this->assertTrue($json['success']);
    }
}
