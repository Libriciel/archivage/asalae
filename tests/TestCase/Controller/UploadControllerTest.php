<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\UploadController;
use Asalae\Test\Mock\FakeEmitter;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\UploadController Test Case
 */
#[UsesClass(UploadController::class)]
class UploadControllerTest extends TestCase
{
    use HttpTrait;
    use IntegrationTestTrait;
    use SaveBufferTrait;
    use AutoFixturesTrait;

    public const string TESTFILE = TESTS . 'Data' . DS . 'logo.png';
    public const string XML = ASALAE_CORE_TEST_DATA . DS . 'seda02.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,

        // Pour ce controller
        'app.EventLogs',
        'app.Fileuploads',
        'app.FileuploadsProfiles',
        'app.Mediainfos',
        'app.Profiles',
        'app.Siegfrieds',
        'app.TransferAttachments',
        'app.TransferLocks',
        'app.Transfers',
    ];
    public $server = [];
    public $files = [];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->files = $_FILES;
        $this->server = $_SERVER;
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_FILES = [
            'file' => [
                'name' => 'logo.png',
                'type' => 'image/png',
                'tmp_name' => TMP_TESTDIR . DS . uniqid('php'),
                'error' => 0,
                'size' => 7215,
            ],
        ];
        Filesystem::reset();
        Filesystem::begin();
        Filesystem::copy(self::TESTFILE, $_FILES['file']['tmp_name']);
        $this->session($this->genericSessionData);
        Utility::set('Beanstalk', FakeEmitter::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_file($_FILES['file']['tmp_name'])) {
            unlink($_FILES['file']['tmp_name']);
        }
        $_FILES = $this->files;
        $_SERVER = $this->server;
        Filesystem::rollback();
        Utility::reset();
        Filesystem::setNamespace();
        Filesystem::reset();
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        Configure::write('Upload.dir', $baseDir = TMP_TESTDIR . DS . 'testunit-upload-dir');
        if (!is_dir($baseDir . DS . 'user_1')) {
            Filesystem::mkdir($baseDir . DS . 'user_1');
        }

        $chunkPath = $baseDir . DS . 'user_1' . DS . sha1('7215-logopng') . '_1';
        Filesystem::copy($_FILES['file']['tmp_name'], $chunkPath);
        $data = [
            'flowChunkNumber' => '1',
            'flowChunkSize' => '1048570',
            'flowCurrentChunkSize' => 7215,
            'flowTotalSize' => '7215',
            'flowIdentifier' => '7215-logopng',
            'flowFilename' => 'logo.png',
            'flowRelativePath' => 'logo.png',
            'flowTotalChunks' => '1',
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->saveBuffer();
        $this->httpPost('/upload', $data);
        $partialEmit = $this->restoreBuffer();
        $response = json_decode($partialEmit . ((string)$this->_response->getBody()), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']['id']);

        $_FILES = [
            'file' => [
                'name' => 'seda.xml',
                'type' => 'application/xml',
                'tmp_name' => TMP_TESTDIR . DS . uniqid('php'),
                'error' => 0,
                'size' => filesize(self::XML),
            ],
        ];
        Filesystem::copy(self::XML, $_FILES['file']['tmp_name']);
        $chunkPath = $baseDir . DS . 'user_1' . DS . sha1($_FILES['file']['size'] . '-sedaxml') . '_1';
        Filesystem::copy($_FILES['file']['tmp_name'], $chunkPath);
        $data = [
            'flowChunkNumber' => '1',
            'flowChunkSize' => '1048570',
            'flowCurrentChunkSize' => $_FILES['file']['size'],
            'flowTotalSize' => $_FILES['file']['size'],
            'flowIdentifier' => $_FILES['file']['size'] . '-sedaxml',
            'flowFilename' => 'seda.xml',
            'flowRelativePath' => 'seda.xml',
            'flowTotalChunks' => '1',
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);

        $this->saveBuffer();
        $this->httpPost('/upload/index/seda', $data);
        $partialEmit = $this->restoreBuffer();
        $response = json_decode($partialEmit . ((string)$this->_response->getBody()), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']['uid']);

        $filename = TMP_TESTDIR . DS . uniqid('php');
        Filesystem::createDummyFile($filename, 1024);
        Filesystem::copy($filename, $baseDir . DS . 'user_1' . DS . '0fe00ffcf6bb8259084a2bb575b47f96ecc9efee_1');

        $_FILES = [
            'file' => [
                'name' => 'foo.png',
                'type' => mime_content_type($filename),
                'tmp_name' => $filename,
                'error' => 0,
                'size' => filesize($filename),
            ],
        ];
        $data = [
            'flowChunkNumber' => 1,
            'flowChunkSize' => 1024,
            'flowCurrentChunkSize' => 1024,
            'flowTotalSize' => 2048,
            'flowIdentifier' => '2048-foopng',
            'flowFilename' => $_FILES['file']['name'],
            'flowRelativePath' => $_FILES['file']['name'],
            'flowTotalChunks' => 2,
        ];
        $response = $this->httpPost('/upload/index/seda', $data, true);
        $this->assertResponseCode(200);
        $this->assertFalse(Hash::get($response, 'report.completion'));
        Filesystem::createDummyFile($filename, 1024);
        Filesystem::copy($filename, $baseDir . DS . 'user_1' . DS . '0fe00ffcf6bb8259084a2bb575b47f96ecc9efee_2');

        /** @noinspection PhpArrayWriteIsNotUsedInspection */
        $_FILES = [
            'file' => [
                'name' => 'foo.png',
                'type' => mime_content_type($filename),
                'tmp_name' => $filename,
                'error' => 0,
                'size' => filesize($filename),
            ],
        ];

        $data['flowChunkNumber'] = 2;
        $this->saveBuffer();
        $this->httpPost('/upload/index/seda', $data);
        $partialEmit = $this->restoreBuffer();
        $response = json_decode($partialEmit . ((string)$this->_response->getBody()), true);
        $this->assertNotFalse(Hash::get($response, 'report.completion'));
        $this->assertNotEmpty(Hash::get($response, 'report.error')); // transfert seda invalide
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $entity = TableRegistry::getTableLocator()->get('Fileuploads')->get(1);
        Filesystem::reset();
        Filesystem::dumpFile($entity->get('path'), 'test');
        $this->assertFileExists($entity->get('path'));
        $this->delete('/upload/delete/1');
        $this->assertResponseCode(200);
        $this->assertFileDoesNotExist($entity->get('path'));
    }

    /**
     * testAddTransferAttachment
     */
    public function testAddTransferAttachment()
    {
        $this->session(['Admin' => ['tech' => true]]);
        Filesystem::reset();
        $dir = TMP_TESTDIR;
        if (!is_dir($dir)) {
            Filesystem::mkdir($dir);
        }
        Configure::write('App.paths.data', $dir);
        $transferXml = $dir . DS . 'transfers' . DS . '1' . DS . 'message' . DS . 'testfile.xml';
        Filesystem::copy(self::XML, $transferXml);
        Filesystem::copy(self::XML, $dir . DS . 'transfer_1_1_edit.xml');
        if (is_file($xml = TMP_TESTDIR . DS . 'transfers/1/message/transfer-61b20d8ded833.xml')) {
            unlink($xml);
        }
        Filesystem::copy(ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xml', $xml);
        Utility::set('Exec', Exec::class);
        Notify::$instance = $this->createMock(Notify::class);
        $attachmentsDir = AbstractGenericMessageForm::getDataDirectory(1) . DS . 'attachments';
        $tmpDir = AbstractGenericMessageForm::getDataDirectory(1) . DS . 'tmp';
        if (!file_exists($attachmentsDir)) {
            Filesystem::mkdir($attachmentsDir);
        }
        if (!file_exists($tmpDir)) {
            Filesystem::mkdir($tmpDir);
        }

        $chunkPath = $tmpDir . DS . sha1('7215-logopng') . '_1';
        Filesystem::copy($_FILES['file']['tmp_name'], $chunkPath);
        $data = [
            'flowChunkNumber' => '1',
            'flowChunkSize' => '1048570',
            'flowCurrentChunkSize' => 7215,
            'flowTotalSize' => '7215',
            'flowIdentifier' => '7215-logopng',
            'flowFilename' => 'logo.png',
            'flowRelativePath' => 'logo.png',
            'flowTotalChunks' => '1',
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpPost('/upload/add-transfer-attachment/1', $data, true);
        $this->assertNotEmpty($response['report']['id']);

        $exec = $this->createMock(Exec::class);
        $exec->method('command')->willThrowException(new Exception('test exception'));
        Utility::set('Exec', $exec);

        $filename = TMP_TESTDIR . DS . uniqid('php');
        Filesystem::createDummyFile($filename, 1024);
        Filesystem::copy($filename, $tmpDir . DS . '340e2634a20d8ebbde75fc26355a062a31656191_1');

        $_FILES = [
            'file' => [
                'name' => 'logo.png',
                'type' => mime_content_type($filename),
                'tmp_name' => $filename,
                'error' => 0,
                'size' => filesize($filename),
            ],
        ];
        $data = [
            'flowChunkNumber' => 1,
            'flowChunkSize' => 1024,
            'flowCurrentChunkSize' => 1024,
            'flowTotalSize' => 2048,
            'flowIdentifier' => '2048-logopng',
            'flowFilename' => $_FILES['file']['name'],
            'flowRelativePath' => $_FILES['file']['name'],
            'flowTotalChunks' => 2,
        ];
        $response = $this->httpPost('/upload/add-transfer-attachment/1', $data, true);
        $this->assertResponseCode(200);
        $this->assertFalse(Hash::get($response, 'report.completion'));
        Filesystem::createDummyFile($filename, 1024);
        Filesystem::copy($filename, $tmpDir . DS . '340e2634a20d8ebbde75fc26355a062a31656191_2');

        /** @noinspection PhpArrayWriteIsNotUsedInspection */
        $_FILES = [
            'file' => [
                'name' => 'logo.png',
                'type' => mime_content_type($filename),
                'tmp_name' => $filename,
                'error' => 0,
                'size' => filesize($filename),
            ],
        ];
        $data['flowChunkNumber'] = 2;
        $response = $this->httpPost('/upload/add-transfer-attachment/1', $data, true);
        $this->assertNotFalse(Hash::get($response, 'report.completion'));
        $this->assertNotEmpty(Hash::get($response, 'report.error')); // $exec->method('command')->willThrowException()
    }
}
