<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\HomeController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use DateTime;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\HomeController Test Case
 */
#[UsesClass(HomeController::class)]
class HomeControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,

        // Pour ce controller
        'app.Agreements',
        'app.EventLogs',
        'app.Profiles',
        'app.ServiceLevels',
        'app.TransferAttachments',
        'app.TransferErrors',
        'app.Transfers',
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationHistories',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        $ValidationHistories = $loc->get('ValidationHistories');
        $ValidationHistories->deleteAll([]);
        $Transfers->updateAll(['is_conform' => true, 'transfer_date' => new DateTime()], []);
        $response = $this->httpGet('/home/index');
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && isset($response->to_validate_count));
        $this->assertGreaterThanOrEqual(1, $response->to_validate_count);
    }
}
