<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\KeywordsController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Notify;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(KeywordsController::class)]
class KeywordsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public const string TEST_SKOS = ASALAE_CORE_TEST_DATA . DS . 'continents-skos.rdf';
    public const string TEST_CSV = ASALAE_CORE_TEST_DATA . DS . 'mots_clefs.csv';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        'app.EventLogs',
        'app.Fileuploads',
        'app.KeywordLists',
        'app.Keywords',
        'app.Siegfrieds',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Configure::write('App.defaultLocale', 'fr_FR');

        $loc = TableRegistry::getTableLocator();
        $KeywordLists = $loc->get('KeywordLists');
        $keywordList = $KeywordLists->newEntity(
            [
                'org_entity_id' => 2,
                'identifier' => 'test',
                'name' => 'Test',
                'description' => 'Liste de test',
                'active' => true,
                'version' => 0,
                'has_workspace' => true,
            ]
        );
        $KeywordLists->saveOrFail($keywordList);
        $keywordList2 = $KeywordLists->newEntity(
            [
                'org_entity_id' => 2,
                'identifier' => 'test2',
                'name' => 'Test2',
                'description' => 'Liste de test 2',
                'active' => true,
                'version' => 1,
            ]
        );
        $KeywordLists->saveOrFail($keywordList2);
        $Keywords = $loc->get('Keywords');
        $keyword = $Keywords->newEntity(
            [
                'keyword_list_id' => $keywordList->id,
                'version' => 0,
                'code' => 'code-1',
                'name' => 'Code 1',
                'app_meta' => '{"created_user_id":1}',
                'parent_id' => null,
            ],
            ['validate' => false]
        );
        $Keywords->saveOrFail($keyword);
        $keyword = $Keywords->newEntity(
            [
                'keyword_list_id' => $keywordList2->id,
                'version' => 1,
                'code' => 'testunit1',
                'name' => 'testunit 1',
                'app_meta' => '{"created_user_id":1}',
                'parent_id' => null,
            ],
            ['validate' => false]
        );
        $Keywords->saveOrFail($keyword);
        $keyword = $Keywords->newEntity(
            [
                'keyword_list_id' => $keywordList2->id,
                'version' => 1,
                'code' => 'testunit2',
                'name' => 'testunit 2',
                'app_meta' => '{"created_user_id":1}',
                'parent_id' => null,
            ],
            ['validate' => false]
        );
        $Keywords->saveOrFail($keyword);
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/index/2');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/index/2?SaveFilterSelect=&name[0]=%3Fode*&code[0]=%3Fode*&sort=name&direction=asc');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/index/2?SaveFilterSelect=&favoris[0]=0&favoris[0]=1&sort=favorite&direction=desc');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertEmpty($response['data']);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/add/2');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entity']));

        $data = [
            'code' => 'test',
            'name' => 'test',
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/keywords/add/2', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['id']);

        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $key = $Keywords->find()->where(['code' => 'test'])->first();
        $this->assertTrue((bool)$key);
        $this->assertEquals($response['id'], $key->get('id'));
    }

    /**
     * testAddMultiple
     */
    public function testAddMultiple()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/addMultiple/2');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entities']));

        $data = [
            0 => [
                'code' => 'test1',
                'name' => 'test1',
            ],
            1 => [
                'code' => 'test2',
                'name' => 'test2',
            ],
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/keywords/addMultiple/2', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response[0]);
        $this->assertNotEmpty($response[0]['id']);
        $this->assertNotEmpty($response[1]);
        $this->assertNotEmpty($response[1]['id']);

        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $key = $Keywords->find()->where(['code' => 'test1'])->first();
        $this->assertTrue((bool)$key);
        $this->assertEquals($response[0]['id'], $key->get('id'));
        $key = $Keywords->find()->where(['code' => 'test2'])->first();
        $this->assertTrue((bool)$key);
        $this->assertEquals($response[1]['id'], $key->get('id'));
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/edit/3');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entity']));

        $data = [
            'code' => 'test',
            'name' => 'test',
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPut('/keywords/edit/3', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['code']);
        $this->assertEquals('test', $response['code']);

        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $key = $Keywords->find()->where(['code' => 'test'])->first();
        $this->assertTrue((bool)$key);
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpDelete('/keywords/delete/3');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']);
        $this->assertEquals('done', $response['report']);

        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $key = $Keywords->newEntity(
            [
                'keyword_list_id' => 1,
                'code' => 'test',
                'name' => 'test',
                'version' => 1,
            ]
        );
        $this->assertNotFalse($Keywords->save($key));
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->delete('/keywords/delete/' . $key->get('id'));
        $this->assertResponseCode(404);
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/view/3');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entity']));
    }

    /**
     * testImport
     */
    public function testImport()
    {
        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $Keywords->deleteAll([]);
        $file = $this->appendSkos();

        $this->httpGet('/keywords/import/2');
        $this->assertResponseCode(200);

        $data = [
            'fileupload_id' => $file->get('id'),
        ];
        $this->httpPost('/keywords/import/2', $data);
        $this->assertResponseCode(200);
        $this->assertEquals(1, $Keywords->find()->where(['name' => 'Europe'])->count());

        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $file = $Fileuploads->newEntity(
            [
                'name' => basename(self::TEST_CSV),
                'path' => self::TEST_CSV,
                'locked' => 1,
            ]
        );
        $this->assertNotFalse($Fileuploads->save($file, ['silent' => true]));

        $data = [
            'fileupload_id' => $file->get('id'),
            'Csv' => [
                'separator' => ';',
                'delimiter' => '"',
                'way' => 'row',
                'pos_label_col' => '2',
                'pos_label_row' => '1',
                'auto_code' => '0',
                'pos_code_col' => '1',
                'pos_code_row' => '1',
            ],
        ];
        $this->httpPost('/keywords/import/2', $data);
        $this->assertResponseCode(200);
        $this->assertEquals(1, $Keywords->find()->where(['name' => 'Chambéry'])->count());
    }

    /**
     * appendSkos
     */
    private function appendSkos()
    {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $file = $Fileuploads->newEntity(
            [
                'name' => basename(self::TEST_SKOS),
                'path' => self::TEST_SKOS,
                'locked' => 1,
                'user_id' => 1,
            ]
        );
        $this->assertNotFalse($Fileuploads->save($file, ['silent' => true]));
        return $file;
    }

    /**
     * testGetImportFileInfo
     */
    public function testGetImportFileInfo()
    {
        $file = $this->appendSkos();
        $this->httpPost('/keywords/get-import-file-info/1/' . $file->get('id'));
        $this->assertResponseCode(200);

        // injection d'un futur doublon
        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $keyword = $Keywords->newEntity(
            [
                'keyword_list_id' => 1,
                'version' => 0,
                'code' => 'europe',
                'name' => 'Europe',
            ]
        );
        $this->assertNotFalse($Keywords->save($keyword));

        $file = $this->appendSkos();
        $this->httpPost(
            '/keywords/get-import-file-info/2/' . $file->get('id'),
            [
                'conflicts' => 'overwrite',
            ]
        );
        $this->assertResponseCode(200);

        $file = $this->appendSkos();
        $this->httpPost(
            '/keywords/get-import-file-info/2/' . $file->get('id'),
            [
                'conflicts' => 'ignore',
            ]
        );
        $this->assertResponseCode(200);

        $file = $this->appendSkos();
        $this->httpPost(
            '/keywords/get-import-file-info/2/' . $file->get('id'),
            [
                'conflicts' => 'rename',
            ]
        );
        $this->assertResponseCode(200);
    }

    /**
     * testPopulateSelect
     */
    public function testPopulateSelect()
    {
        $data = [
            'term' => 'test',
        ];
        $response = $this->httpPost('/keywords/populate-select', $data);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response->results);

        $data = [
            'term' => 'test',
            'page' => 2,
        ];
        $this->httpPost('/keywords/populate-select', $data);
        $this->assertResponseCode(200);
    }

    /**
     * testGetData
     */
    public function testGetData()
    {
        $response = $this->httpGet('/keywords/get-data/3');
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response->version));
    }
}
