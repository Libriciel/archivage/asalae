<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\ArchivesController;
use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Entity\LifecycleXmlArchiveFile;
use Asalae\Model\Entity\TransferXmlArchiveFile;
use Asalae\Model\Table\ArchivesTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Datacompressor\Utility\DataCompressor;
use DOMDocument;
use ErrorException;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Exception;
use PHPUnit\Framework\Attributes\UsesClass;
use Throwable;

/**
 * Asalae\Controller\ArchivesController Test Case
 */
#[UsesClass(ArchivesController::class)]
class ArchivesControllerTest extends TestCase
{
    use HttpTrait;
    use IntegrationTestTrait;
    use SaveBufferTrait;
    use AutoFixturesTrait;
    use ConsoleIntegrationTestTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        FIXTURES_VOLUMES,
        'app.AccessRuleCodes',
        'app.AccessRules',
        'app.AppraisalRuleCodes',
        'app.AppraisalRules',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveDescriptions',
        'app.CompositeArchiveUnits',
        'app.Fileuploads',
        'app.Keywords',
        'app.MessageIndicators',
        'app.Pronoms',
        'app.Siegfrieds',
    ];
    private $debugFixtures = false;
    private $files = [];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('Archives.import.tmp_dir', TMP_TESTDIR);
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('Archives.updateLifecycle.path', TMP_TESTDIR);
        Configure::write('Archives.updateDescription.path', TMP_TESTDIR);
        Configure::write('Archives.updateLifecycle.cache_duration', 0);
        Configure::write('Archives.updateDescription.cache_duration', 0);
        $this->files = $_FILES;
        $Exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(['async', 'command'])
            ->getMock();
        $fn = function ($command, ...$args) {
            $h = fopen('/tmp/test.log', 'a');
            fwrite($h, $command . var_export($args, true) . "\n");
            fclose($h);
            if ($command === CAKE_SHELL) {
                $escaped = $this->extractArgsShellOptions($args);
                $command = implode(' ', $escaped);
                $out = new ConsoleOutput();
                $err = new ConsoleOutput();
                $io = new ConsoleIo($out, $err, new ConsoleInput([]));
                $runner = $this->makeRunner();
                $args = $this->commandStringToArgs("cake $command");
                $code = $runner->run($args, $io);
                if (is_file(TMP_TESTDIR . '/update_lifecycle/.lock')) {
                    unlink(TMP_TESTDIR . '/update_lifecycle/.lock');
                }
                return new CommandResult(
                    [
                        'success' => $code === 0,
                        'code' => $code,
                        'stdout' => implode(PHP_EOL, $out->messages()),
                        'stderr' => implode(PHP_EOL, $err->messages()),
                    ]
                );
            } elseif (substr($command, 0, strlen('sleep')) === 'sleep') {
                $Exec = new Exec();
                return $Exec->command('echo 1');
            } else {
                $Exec = new Exec();
                return $Exec->command($command, ...$args);
            }
        };
        $Exec->method('async')->willReturnCallback($fn);
        $Exec->method('command')->willReturnCallback($fn);
        Utility::set('Exec', $Exec);
    }

    /**
     * extractArgsShellOptions
     * @param array $args
     * @return array
     */
    private function extractArgsShellOptions(array $args): array
    {
        $escaped = [];
        foreach ($args as $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $escaped[] = $k;
                    if ($v) {
                        $escaped[] = $v;
                    }
                }
            } else {
                $escaped[] = $value;
            }
        }
        return $escaped;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        $_FILES = $this->files;
        Utility::reset();
    }

    /**
     * Test index method
     *
     * @return void
     * @throws ErrorException
     * @throws Exception
     * @throws Throwable
     */
    public function testIndex()
    {
        // générique
        $this->httpGet('/archives/index');
        $this->assertResponseCode(200);

        // L'utilisateur à le droit de voir les archives des entités filles
        $this->session(
            Hash::merge(
                [
                    'Auth' => [
                        'id' => 1,
                        'org_entity' => [
                            'id' => 2,
                        ],
                        'role' => [
                            'id' => 1,
                            'hierarchical_view' => true,
                        ],
                    ],
                ],
                $this->genericSessionData
            )
        );
        $this->httpGet('/archives/index');
        $this->assertResponseCode(200);

        // L'utilisateur fait parti du service d'archive / du CST
        // (voit toutes les archives des entités filles du service d'archives)
        $this->configRequest(
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );
        $this->session(
            Hash::merge(
                [
                    'User' => [
                        'id' => 1,
                        'org_entity' => [
                            'id' => 2,
                            'type_entity' => [
                                'code' => 'CST', // SA fonctionne aussi
                            ],
                        ],
                    ],
                ],
                $this->genericSessionData
            )
        );
        $this->get('/archives/index');
        $this->assertResponseCode(200);

        // recherche sur tous les termes
        $this->httpGet(
            '/archives/index?access_rules_end_date_vs_now[0]=<&'
            . 'archival_agency_identifier[0]=%3F%3F&created[0]=NOW&'
            . 'dateoperator_created[0]=<%3D&created[0]=04%2F06%2F2019&'
            . 'description[0]=*test*&appraisal_rules_end_date_vs_now[0]=>%3D&'
            . 'state[0][]=creating&state[0][]=describing&state[0][]=storing&'
            . 'state[0][]=available&favoris[0]=1&name[0]=*test*&profile_id[0]=1&'
            . 'originating_agency_id[0]=2&transferring_agency_id[0]=2&'
            . 'keyword[0]=*test*&filename[0]=*test*'
        );
        $this->assertResponseCode(200);
    }

    /**
     * Test description method
     *
     * @return void
     * @throws ErrorException
     * @throws Exception
     * @throws Throwable
     */
    public function testView()
    {
        $this->httpGet('/archives/view/1');
        $this->assertResponseCode(200);
    }

    /**
     * Test description method
     *
     * @return void
     * @throws ErrorException
     * @throws Exception
     * @throws Throwable
     */
    public function testDescription()
    {
        $this->httpGet('/archives/description/1');
        $this->assertResponseCode(200);
    }

    /**
     * testGetTree
     */
    public function testGetTree()
    {
        $response = $this->httpGet('/archives/get-tree/1');
        $this->assertResponseCode(200);
        $this->assertTrue($response && isset($response->children[0]->id));
        $this->assertEquals('1_Archive_ContentDescription', $response->children[0]->id);
        $this->assertEquals('ContentDescription', $response->children[0]->element);
        $this->assertStringContainsString('ContentDescription', $response->children[0]->text);
        $this->assertEquals('Archive/ContentDescription', $response->children[0]->url);

        $response = $this->httpGet('/archives/get-tree/1?id=1_Archive-1');
        $this->assertResponseCode(200);
        $this->assertTrue($response && isset($response[0]->id));
        $this->assertEquals('1_Archive_ContentDescription', $response[0]->id);
        $this->assertEquals('ContentDescription', $response[0]->element);
        $this->assertStringContainsString('ContentDescription', $response[0]->text);
        $this->assertEquals('Archive/ContentDescription', $response[0]->url);
    }

    /**
     * testViewDescriptionNode
     */
    public function testViewDescriptionNode()
    {
        $response = $this->httpGet('/archives/view-description-node/1/Archive/ContentDescription', true);
        $this->assertResponseCode(200);
        $this->assertEquals('recordgrp', Hash::get($response, 'nodes.DescriptionLevel.0.value'));
    }

    /**
     * testViewBinaries
     */
    public function testViewBinaries()
    {
        $response = $this->httpGet('/archives/view-binaries/1');
        $this->assertResponseCode(200);
        $this->assertTrue($response && isset($response[0]->type));
        $this->assertEquals('original_data', $response[0]->type);

        $response = $this->httpGet(
            '/archives/view-binaries/1?filename=sample*&format=*fmt*'
            . '&mime=*wmv&size[0][value]=1&size[0][mult]=1&size[0][operator]=>%3D',
            true
        );
        $this->assertResponseCode(200);
        $this->assertGreaterThanOrEqual(1, count($response));
        $this->assertNotEmpty($response[0]['other_datas']);
    }

    /**
     * testViewBinary
     */
    public function testViewBinary()
    {
        $response = $this->httpGet('/archives/view-binary/1');
        $this->assertResponseCode(200);
        $this->assertTrue($response && isset($response->entity->type));
        $this->assertEquals('original_data', $response->entity->type);
    }

    /**
     * testSearch
     */
    public function testSearch()
    {
        $this->httpGet('/archives/search');
        $this->assertResponseCode(200);

        $filters = [
            'Archives[dateoperator_created][0]=<%3D&Archives[created][0]=30%2F08%2F2019',
            'Archives[description][0]=%25test%25',
            'Archives[archival_agency_identifier][0]=%25test%25',
            'Archives[originating_agency_identifier][0]=%25test%25',
            'Archives[transferring_agency_identifier][0]=%25test%25',
            'Archives[name][0]=%25test%25',
            'Archives[access_rules_end_date_vs_now][0]=<',
            'Archives[dateoperator_dua_end][0]=<%3D&Archives[dua_end][0]=30%2F08%2F2019',
            'Archives[dua_duration_operator][0]=>&Archives[dua_duration][0]=P1Y',
            'Archives[dua][0]=<',
            'Archives[keyword][0]=%25test%25',
            'Archives[keyword_list][0]=&Archives[keyword_list][0][]=1',
            'Archives[appraisal_rules_final_action_code][0]=keep',
            'ArchiveUnits[access_rules_end_date_vs_now][0]=<',
        ];
        foreach ($filters as $filter) {
            $this->httpGet('/archives/search?' . $filter);
            $this->assertResponseCode(200);
        }
    }

    /**
     * testGetFilters
     */
    public function testGetFilters()
    {
        $loc = TableRegistry::getTableLocator();
        $SavedFilters = $loc->get('SavedFilters');
        $savedFilter = $SavedFilters->newEntity(
            [
                'user_id' => 1,
                'name' => 'filter-name',
                'controller' => 'ctrl',
                'action' => 'act',
                'created' => 1567170083,
            ],
        );
        $SavedFilters->saveOrFail($savedFilter);
        $Filters = $loc->get('Filters');
        $filter = $Filters->newEntity(
            [
                'saved_filter_id' => $savedFilter->id,
                'key' => 'Lorem ipsum dolor sit amet',
                'value' => 'Lorem ipsum dolor sit amet',
                'created' => 1568965756,
            ],
        );
        $Filters->saveOrFail($filter);
        $this->httpGet('/archives/get-filters/' . $savedFilter->id);
        $this->assertResponseCode(200);
    }

    /**
     * testCheckIntegrity
     */
    public function testCheckIntegrity()
    {
        $StoredFiles = TableRegistry::getTableLocator()->get('StoredFiles');
        $initialHash = $StoredFiles->get(1)->get('hash');

        $this->post('/archives/check-integrity/1');
        $this->assertHeader('X-Asalae-Success', 'true');

        $StoredFiles->updateAll(['hash' => 'bad_hash'], ['id' => 1]);
        $this->post('/archives/check-integrity/1');
        $this->assertHeader('X-Asalae-Success', 'false');

        $StoredFiles->updateAll(['hash' => base64_encode(hex2bin($initialHash))], ['id' => 1]);
        $this->post('/archives/check-integrity/1');
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testMakeZip
     */
    public function testMakeZip()
    {
        Configure::write('App.paths.data', TMP_TESTDIR);
        $zipPath = TMP_TESTDIR . DS . 'download' . DS . 'archive' . DS . '1_archive_files.zip';
        Filesystem::createDummyFile($zipPath, 10);
        $this->httpGet('/archives/make-zip/1');
        $this->assertResponseCode(200);
        Filesystem::remove($zipPath);
    }

    /**
     * testDownloadFiles
     */
    public function testDownloadFiles()
    {
        Configure::write('App.paths.data', TMP_TESTDIR);
        $zipPath = TMP_TESTDIR . DS . 'download' . DS . 'archive' . DS . '1_archive_files.zip';
        Filesystem::createDummyFile($zipPath, 10);
        $this->httpGet('/archives/download-files/1');
        $this->assertResponseCode(200);
        Filesystem::remove($zipPath);
    }

    /**
     * testTreatments
     */
    public function testTreatments()
    {
        $this->httpGet('/archives/treatments');
        $this->assertResponseCode(200);
    }

    /**
     * testImportDryRun
     */
    public function testImportDryRun()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'Archives', 'alias' => 'import'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        // test dryrun OK
        $data = [
            'dryrun' => '1',
            'archiveIdentifier' => 'archive001',
            'transferringAgencyIdentifier' => 'sv',
            'originatingAgencyIdentifier' => 'sp',
            'agreementIdentifier' => 'agreement1',
            'profileIdentifier' => 'profile1',
        ];
        $this->httpPost('/api/archives/import', $data);
        $this->assertResponseCode(200);

        // archive déjà existante
        $data['archiveIdentifier'] = 'sa_1';
        $this->post('/api/archives/import', $data);
        $this->assertResponseCode(400);
        $data['archiveIdentifier'] = 'archive001';

        // transferringAgency du mauvais type
        $data['transferringAgencyIdentifier'] = 'sp';
        $this->post('/api/archives/import', $data);
        $this->assertResponseCode(400);

        // transferringAgency n'existe pas
        $data['transferringAgencyIdentifier'] = 'notexists';
        $this->post('/api/archives/import', $data);
        $this->assertResponseCode(404);
        $data['transferringAgencyIdentifier'] = 'sv';

        // originatingAgency du mauvais type
        $OrgEntities = $this->fetchTable('OrgEntities');
        $org = $OrgEntities->newEntity(
            [
                'name' => uniqid(),
                'identifier' => 'test-bad-type',
                'parent_id' => 2,
                'active' => true,
                'type_entity' => 3, // CST
            ]
        );
        $OrgEntities->saveOrFail($org);
        $data['originatingAgencyIdentifier'] = 'test-bad-type';
        $this->post('/api/archives/import', $data);
        $this->assertResponseCode(400);

        // originatingAgency n'existe pas
        $data['originatingAgencyIdentifier'] = 'notexists';
        $this->post('/api/archives/import', $data);
        $this->assertResponseCode(404);
        $data['originatingAgencyIdentifier'] = 'sp';

        // agreement n'existe pas
        $data['agreementIdentifier'] = 'notexists';
        $this->post('/api/archives/import', $data);
        $this->assertResponseCode(404);
        $data['agreementIdentifier'] = 'agreement1';

        // profile n'existe pas
        $data['profileIdentifier'] = 'notexists';
        $this->post('/api/archives/import', $data);
        $this->assertResponseCode(404);
    }

    /**
     * testImportDryRunOnInactives
     */
    public function testImportDryRunOnInactives()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'Archives', 'alias' => 'import'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id, // admin
                'aco_id' => $aco->id, // api/Archives
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $data = [
            'dryrun' => '1',
            'archiveIdentifier' => 'archive001',
            'transferringAgencyIdentifier' => 'sv',
            'originatingAgencyIdentifier' => 'sp',
            'agreementIdentifier' => 'agreement1',
            'profileIdentifier' => 'profile1',
            'serviceLevel' => 'servicelevel1',
        ];

        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');

        // test transferringAgencyIdentifier inactive
        $OrgEntities->updateAll(
            ['active' => false],
            ['identifier' => 'sv'],
        );
        $this->post('/api/archives/import', $data);
        $this->assertResponseCode(400);
        $this->assertResponseContains(__("L'entité 'sv' n'est pas active"));
        $OrgEntities->updateAll(
            ['active' => true],
            ['identifier' => 'sv'],
        );

        // test originatingAgencyIdentifier inactive
        $OrgEntities->updateAll(
            ['active' => false],
            ['identifier' => 'sp'],
        );
        $this->post('/api/archives/import', $data);
        $this->assertResponseCode(400);
        $this->assertResponseContains(__("L'entité 'sp' n'est pas active"));
        $OrgEntities->updateAll(
            ['active' => true],
            ['identifier' => 'sp'],
        );

        // test agreement inactive
        $Agreements = TableRegistry::getTableLocator()->get('Agreements');
        $Agreements->updateAll(
            ['active' => false],
            ['identifier' => 'agreement1'],
        );
        $this->post('/api/archives/import', $data);
        $this->assertResponseCode(400);
        $this->assertResponseContains(__("L'accord de versement 'agreement1' n'est pas actif"));
        $Agreements->updateAll(
            ['active' => true],
            ['identifier' => 'agreement1'],
        );

        // test profile inactive
        $Profiles = TableRegistry::getTableLocator()->get('Profiles');
        $Profiles->updateAll(
            ['active' => false],
            ['identifier' => 'profile1'],
        );
        $this->post('/api/archives/import', $data);
        $this->assertResponseCode(400);
        $this->assertResponseContains(__("Le profil d'archives 'profile1' n'est pas actif"));
        $Profiles->updateAll(
            ['active' => true],
            ['identifier' => 'profile1'],
        );

        // test service level inactive
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $ServiceLevels->updateAll(
            ['active' => false],
            ['identifier' => 'servicelevel1'],
        );
        $this->post('/api/archives/import', $data);
        $this->assertResponseCode(400);
        $this->assertResponseContains(__("Le niveau de service 'servicelevel1' n'est pas actif"));
    }

    /**
     * testPostImport
     */
    public function testPostImport()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'Archives', 'alias' => 'import'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        // Préparation du zip à importer
        VolumeSample::tmpPutContent(
            'import' . DS . 'management_data' . DS . 'test_transfer.xml',
            VolumeSample::getSampleContent('sample_seda10.xml')
        );
        VolumeSample::tmpPutContent(
            'import' . DS . 'management_data' . DS . 'test_description.xml',
            VolumeSample::getSampleContent('sample_seda10_description.xml')
        );
        VolumeSample::tmpPutContent('import' . DS . 'original_data' . DS . 'sample.rng', 'test');
        VolumeSample::tmpPutContent('import' . DS . 'original_data' . DS . 'sample.pdf', 'test');
        VolumeSample::tmpPutContent('import' . DS . 'original_data' . DS . 'sample.odt', 'test');
        DataCompressor::compress(
            TMP_TESTDIR . DS . 'import',
            $zip = TMP_TESTDIR . DS . 'import.zip'
        );
        $_FILES = [
            'archive' => [
                'name' => 'import.zip',
                'type' => mime_content_type($zip),
                'tmp_name' => $zip,
                'error' => UPLOAD_ERR_OK,
                'size' => filesize($zip),
            ],
        ];
        $this->_request['files'] = $_FILES;
        $data = [];
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        $Transfers->updateAll(
            ['transfer_identifier' => uniqid()],
            ['transfer_identifier' => 'atr_alea_70f90eacdb2d3b53ad7e87f808a82430']
        );
        $OrgEntities = $loc->get('OrgEntities');
        $OrgEntities->updateAll(['identifier' => 'sv1'], ['identifier' => 'sv']);
        $Archives = $loc->get('Archives');
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $ArchiveFiles = $loc->get('ArchiveFiles');
        $initialCountTransfers = $Transfers->find()->count();
        $initialCountArchives = $Archives->find()->count();
        $initialCountArchiveUnits = $ArchiveUnits->find()->count();
        $initialCountArchiveBinaries = $ArchiveBinaries->find()->count();
        $initialCountArchiveFiles = $ArchiveFiles->find()->count();

        // Import
        $this->httpPost('/api/archives/import', $data);
        $this->assertResponseCode(200);
        $this->assertCount($initialCountTransfers + 1, $Transfers->find());
        $this->assertCount($initialCountArchives + 1, $Archives->find());
        $this->assertCount($initialCountArchiveUnits + 6, $ArchiveUnits->find());
        $this->assertCount($initialCountArchiveBinaries + 3, $ArchiveBinaries->find());
        $this->assertCount($initialCountArchiveFiles + 3, $ArchiveFiles->find());

        $archive = $Archives->find()
            ->orderBy(['Archives.id' => 'desc'])
            ->contain(
                [
                    'Transfers' => ['TransferringAgencies'],
                    'TransferringAgencies',
                    'OriginatingAgencies',
                    'Agreements',
                    'Profiles',
                    'AccessRules' => ['AccessRuleCodes'],
                    'AppraisalRules' => ['AppraisalRuleCodes'],
                    'SecureDataSpaces',
                    'DescriptionXmlArchiveFiles',
                    'LifecycleXmlArchiveFiles',
                    'TransferXmlArchiveFiles',
                ]
            )
            ->first();

        // check transfer
        $transferXmlPath = Hash::get($archive, 'transfers.0.xml');
        $this->assertFileExists($transferXmlPath);
        $dom = new DOMDocument();
        $dom->load($transferXmlPath);
        $this->assertTrue($dom->schemaValidate(SEDA_V10_XSD));

        // check transfer (archiveFile)
        /** @var TransferXmlArchiveFile $trans */
        $trans = Hash::get($archive, 'transfer_xml_archive_files.0');
        $this->assertInstanceOf(TransferXmlArchiveFile::class, $trans);
        $dom = $trans->getDom();
        $this->assertGreaterThanOrEqual(4611, mb_strlen($dom->saveXML()));
        $this->assertTrue($dom->schemaValidate(SEDA_V10_XSD));

        // check description
        /** @var DescriptionXmlArchiveFile $desc */
        $desc = Hash::get($archive, 'description_xml_archive_file');
        $this->assertInstanceOf(DescriptionXmlArchiveFile::class, $desc);
        $dom = $desc->getDom();
        $this->assertGreaterThanOrEqual(4038, mb_strlen($dom->saveXML()));
        $this->assertTrue($dom->schemaValidate(SEDA_ARCHIVE_V10_XSD));

        // check lifecycle
        /** @var LifecycleXmlArchiveFile $desc */
        $desc = Hash::get($archive, 'lifecycle_xml_archive_file');
        $this->assertInstanceOf(LifecycleXmlArchiveFile::class, $desc);
        $dom = $desc->getDom();
        $this->assertGreaterThanOrEqual(23955, mb_strlen($dom->saveXML()));
        $this->assertTrue($dom->schemaValidate(PREMIS_V3));
    }

    /**
     * testImportGet
     */
    public function testImportGet()
    {
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $Archives->updateAll(['archival_agency_identifier' => 'ark:/654654/001'], ['id' => 1]);
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'Archives', 'alias' => 'import'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $this->httpGet('/api/archives/import/ark:/654654/001');
        $this->assertResponseCode(200);

        $this->get('/api/archives/import/notexists');
        $this->assertResponseCode(404);
    }

    /**
     * testFreeze
     */
    public function testFreeze()
    {
        $this->httpGet('/archives/freeze/1');
        $this->assertResponseCode(200);

        $this->httpPost('/archives/freeze/1', ['reason' => 'testunit']);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testUnfreeze
     */
    public function testUnfreeze()
    {
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $Archives->updateAll(['state' => ArchivesTable::S_FREEZED], ['id' => 1]);
        $this->httpGet('/archives/unfreeze/1');
        $this->assertResponseCode(200);

        $this->httpPost('/archives/unfreeze/1', ['reason' => 'testunit']);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testExportEad
     */
    public function testExportEad()
    {
        $this->httpGet('/archives/index?export_ead=true');
        $this->assertResponseCode(200);
        $this->assertResponseContains('<unitid type="identifiant du service d\'archives">sa_1</unitid>');
    }
}
