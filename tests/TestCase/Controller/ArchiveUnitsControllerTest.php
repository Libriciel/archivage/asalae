<?php

namespace Asalae\Test\TestCase\Controller;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use AsalaeCore\Utility\DOMUtility;
use Asalae\Controller\ArchiveUnitsController;
use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use Asalae\Test\Mock\MockExecVolumeManager;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Date as CakeDate;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use DOMElement;
use DateTime;
use DateTimeInterface;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\ArchiveUnitsController Test Case
 *
 */
#[UsesClass(ArchiveUnitsController::class)]
class ArchiveUnitsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use SaveBufferTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_VOLUMES,
        'app.AccessRuleCodes',
        'app.AccessRules',
        'app.Agreements',
        'app.AppraisalRuleCodes',
        'app.AppraisalRules',
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveKeywords',
        'app.ArchiveUnits',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.Crons',
        'app.EventLogs',
        'app.Profiles',
        'app.Transfers',
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationStages',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        VolumeSample::init();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('Archives.updateLifecycle.path', TMP_TESTDIR);
        Configure::write('Archives.updateDescription.path', TMP_TESTDIR);
        Configure::write('Archives.updateLifecycle.cache_duration', 0);
        Configure::write('Archives.updateDescription.cache_duration', 0);
        $this->files = $_FILES;
        Utility::set('Exec', MockExecVolumeManager::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Utility::reset();
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->httpGet('/archive-units/index');
        $this->assertResponseCode(200);
    }

    /**
     * testCatalog
     */
    public function testCatalog()
    {
        $this->httpGet('/archive-units/catalog');
        $this->assertResponseCode(200);
    }

    /**
     * testCatalogMyEntity
     */
    public function testCatalogMyEntity()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'ArchiveUnits', 'alias' => 'catalogMyEntity'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $this->httpGet('/archive-units/catalog-my-entity');
        $this->assertResponseCode(200);
    }

    /**
     * testDuaExpired
     */
    public function testDuaExpired()
    {
        $this->httpGet('/archive-units/dua-expired');
        $this->assertResponseCode(200);
        $response = json_decode((string)$this->_response->getBody(), true);
        $now = new \DateTime();
        foreach ($response['data'] as $archiveUnit) {
            $this->assertLessThan($now, $archiveUnit['access_rule']['end_date']);
        }
    }

    /**
     * testSearchBar
     */
    public function testSearchBar()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->find()->where(['name REGEXP' => '.*'])->count();
        $ArchiveDescriptions = $loc->get('ArchiveDescriptions');
        $ArchiveDescriptions->updateAll(
            [
                'description' => "Ceci est une description de test, BlaBlaBla... deuh! <script>alert('fooo!');</script>",
            ],
            ['id' => 1]
        );
        $ArchiveUnits->updateSearchField([]);

        $data = ['search' => "description de test blabla"];
        $response = json_encode(
            $this->httpPost('/archive-units/search-bar', $data, true),
            JSON_UNESCAPED_SLASHES
        );
        $this->assertResponseCode(200);
        $expected = json_encode(
            "Ceci est une <b>description de test</b>, Bla<b>BlaBla</b>... deuh! "
            . h("<script>alert('fooo!');</script>"),
            JSON_UNESCAPED_SLASHES
        );
        $this->assertTextContains($expected, $response);

        $ArchiveDescriptions->updateAll(
            [
                'description' => "Test de recherche de partie spécifique d'un texte",
            ],
            ['id' => 1]
        );
        $ArchiveUnits->updateSearchField([]);

        $data = ['search' => '"test de recherche"'];
        $response = json_encode(
            $this->httpPost('/archive-units/search-bar', $data, true),
            JSON_UNESCAPED_SLASHES
        );
        $this->assertResponseCode(200);
        $expected = "<b>Test de recherche</b> de partie sp\u00e9cifique d&#039;un texte";
        $this->assertTextContains($expected, $response);

        $data = ['search' => '"test de recherche" -spécifique'];
        $response = json_encode($this->httpPost('/archive-units/search-bar', $data, true));
        $expected = 'sp\u00e9cifique';
        $this->assertResponseCode(200);
        $this->assertTextNotContains($expected, $response);
    }

    /**
     * testFullSearchBar
     */
    public function testFullSearchBar()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveDescriptions = $loc->get('ArchiveDescriptions');
        $ArchiveDescriptions->updateAll(
            [
                'description' => "Ceci est une description de test, BlaBlaBla... deuh! <script>alert('fooo!');</script>",
            ],
            ['id' => 1]
        );
        $ArchiveUnits->updateFullSearchField([]);

        $data = ['search' => "description de test blabla"];
        $response = json_encode($this->httpPost('/archive-units/full-search-bar', $data, true));
        $this->assertResponseCode(200);
        $expected = json_encode(
            "Ceci est une <b>description de test</b>, Bla<b>BlaBla</b>... deuh! "
            . h("<script>alert('fooo!');</script>")
        );
        $this->assertTextContains($expected, $response);

        $ArchiveDescriptions->updateAll(
            [
                'description' => "Test de recherche de partie spécifique d'un texte",
            ],
            ['id' => 1]
        );
        $ArchiveUnits->updateFullSearchField([]);

        $data = ['search' => '"test de recherche"'];
        $response = json_encode($this->httpPost('/archive-units/full-search-bar', $data, true));
        $this->assertResponseCode(200);
        $expected = "<b>Test de recherche<\/b> de partie sp\u00e9cifique d&#039;un texte";
        $this->assertTextContains($expected, $response);

        $data = ['search' => '"test de recherche" -spécifique'];
        $response = json_encode($this->httpPost('/archive-units/full-search-bar', $data, true));
        $expected = 'sp\u00e9cifique';
        $this->assertResponseCode(200);
        $this->assertTextNotContains($expected, $response);
    }

    /**
     * testGetNavTree
     */
    public function testGetNavTree()
    {
        $this->httpGet('/archive-units/get-nav-tree/1');
        $this->assertResponseCode(200);
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpGet('/archive-units/view/1');
        $this->assertResponseCode(200);
    }

    /**
     * testViewBinaries
     */
    public function testViewBinaries()
    {
        $this->httpGet('/archive-units/viewBinaries/1');
        $this->assertResponseCode(200);
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnits = $loc->get('ArchiveUnits');

        $this->httpGet('/archive-units/edit/1');
        $this->assertResponseCode(200);

        /**
         * ============================= Archive ===============================
         */
        $data = [
            'archive' => [
                'name' => 'foo',
                'agreement_id' => 1,
                'profile_id' => 1,
                'originating_agency_id' => 4,
            ],
            'archive_description' => [
                'description' => 'bar',
                'oldest_date' => '1900-01-01',
                'latest_date' => '2000-01-01',
                'access_rule' => [
                    'access_rule_code_id' => 1,
                    'start_date' => '1980-01-01',
                ],
            ],
            'access_rule' => [
                'access_rule_code_id' => 1,
                'start_date' => '1980-01-01',
            ],
            'appraisal_rule' => [
                'final_action_code' => 'keep',
                'appraisal_rule_code_id' => 1,
                'start_date' => '1980-01-01',
            ],
        ];
        $this->httpPut('/archive-units/edit/1', $data);
        $this->assertAsalaeSuccess();
        $entity = $ArchiveUnits->find()
            ->where(['ArchiveUnits.id' => 1])
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                        'LifecycleXmlArchiveFiles' => ['StoredFiles'],
                        'OriginatingAgencies',
                    ],
                    'ArchiveDescriptions' => ['AccessRules'],
                    'AccessRules',
                    'AppraisalRules',
                ]
            )
            ->firstOrFail();
        $checks = [
            'archive.name' => 'ns:Name',
            'archive_description.description' => 'ns:ContentDescription/ns:Description',
            'archive_description.access_rule.start_date' => 'ns:ContentDescription/ns:AccessRestrictionRule/ns:StartDate',
            'archive.originating_agency.identifier' => 'ns:ContentDescription/ns:OriginatingAgency/ns:Identification',
            'access_rule.start_date' => 'ns:AccessRestrictionRule/ns:StartDate',
            'appraisal_rule.start_date' => 'ns:AppraisalRule/ns:StartDate',
        ];
        $this->assertEntityMatch($entity, $checks, $data);

        /**
         * ========================== ArchiveObject ============================
         */
        $data = [
            'name' => 'bar',
            'archive_description' => [
                'description' => 'bar',
                'oldest_date' => '1900-01-01',
                'latest_date' => '2000-01-01',
                'access_rule' => [
                    'access_rule_code_id' => 1,
                    'start_date' => '1980-01-01',
                ],
            ],
            'access_rule' => [
                'access_rule_code_id' => 2,
                'start_date' => '1990-01-02',
            ],
            'appraisal_rule' => [
                'final_action_code' => 'keep',
                'appraisal_rule_code_id' => 2,
                'start_date' => '1990-01-01',
            ],
        ];
        $this->httpPut('/archive-units/edit/1', $data);
        $this->assertAsalaeSuccess();
        $entity = $ArchiveUnits->find()
            ->where(['ArchiveUnits.id' => 1])
            ->contain(
                [
                    'Archives' => [
                        'DescriptionXmlArchiveFiles' => ['StoredFiles'],
                    ],
                    'AccessRules',
                    'AppraisalRules',
                    'ArchiveDescriptions' => ['AccessRules'],
                ]
            )
            ->firstOrFail();
        $checks = [
            'name' => 'ns:Name',
            'access_rule.start_date' => 'ns:AccessRestrictionRule/ns:StartDate',
            'appraisal_rule.start_date' => 'ns:AppraisalRule/ns:StartDate',
            'archive_description.access_rule.start_date' => 'ns:ContentDescription/ns:AccessRestrictionRule/ns:StartDate',
        ];
        $this->assertEntityMatch($entity, $checks, $data, 'ns:ArchiveObject');
    }

    /**
     * Compare l'entité avec les données saisies dans le formulaire et le DOM
     * @param EntityInterface $entity
     * @param array           $checks
     * @param array           $data
     * @param string          $parentXpath
     * @throws VolumeException
     */
    private function assertEntityMatch(EntityInterface $entity, array $checks, array $data, string $parentXpath = '.')
    {
        /** @var DescriptionXmlArchiveFile $desc */
        $desc = Hash::get($entity, 'archive.description_xml_archive_file');
        $desc = $desc->getDom();
        $util = new DOMUtility($desc);
        $parentNode = $util->xpath->query($parentXpath)->item(0);
        foreach ($checks as $path => $xpath) {
            $value = Hash::get($entity, $path);
            $value = ($value instanceof DateTimeInterface || $value instanceof CakeDate)
                ? $value->format('Y-m-d')
                : $value;
            if ($v = Hash::get($data, $path)) {
                $this->assertEquals(
                    $v,
                    $value,
                    "expected: formDataValue, actual: bdd value in " . $path
                );
            }
            $node = $util->xpath->query($xpath, $parentNode)->item(0);
            $this->assertInstanceOf(DOMElement::class, $node, $xpath);
            $this->assertEquals($value, $node->nodeValue, $xpath);
        }
    }

    /**
     * testDownloadByName
     */
    public function testDownloadByName()
    {
        $expected = VolumeSample::getSampleContent('sample_seda10_description.xml');
        VolumeSample::volumePutContent(
            'sa/no_profile/2021-12/sa_1/original_data/sample.wmv',
            $expected
        );
        $response = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/archive-units/downloadByName/2/' . base64_encode('sample.wmv')
        );
        $this->assertResponseCode(200);
        $this->assertEquals($expected, $response);

        // test non communicable
        TableRegistry::getTableLocator()->get('AccessRules')->updateAll(
            ['end_date' => new DateTime('tomorrow')],
            []
        );
        $this->preserveBuffer(
            [$this, 'get'],
            '/archive-units/downloadByName/2/' . base64_encode('sample.wmv')
        );
        $this->assertResponseCode(403);
    }

    /**
     * testReturnable
     */
    public function testReturnable()
    {
        $this->httpGet('/archive-units/returnable');
        $this->assertResponseCode(200);
    }

    /**
     * testEliminable
     */
    public function testEliminable()
    {
        $this->httpGet('/archive-units/eliminable');
        $this->assertResponseCode(200);
    }

    /**
     * testUnlist
     */
    public function testUnlist()
    {
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $oa = $OrgEntities->listOptions()
            ->where(
                [
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES,
                ]
            )
            ->orderBy(['OrgEntities.name' => 'asc']);
        $unlisted = ArchiveUnitsController::unListOptions($oa->toArray());
        sort($unlisted);

        $ids = $OrgEntities->find()
            ->where(
                [
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES,
                ]
            )
            ->contain(['TypeEntities'])
            ->all()
            ->extract('id')
            ->toArray();
        sort($ids);

        $this->assertEquals($ids, $unlisted);
    }
}
