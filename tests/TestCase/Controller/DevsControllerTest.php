<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\DevsController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(DevsController::class)]
class DevsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,
        'app.EventLogs',
    ];
    public $originalControllersRules;
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        $rulesPath = TMP_TESTDIR . DS . uniqid('controllers-') . '.json';
        $this->originalControllersRules = Configure::read('App.paths.controllers_rules');
        Filesystem::copy($this->originalControllersRules, $rulesPath);
        Configure::write('App.paths.controllers_rules', $rulesPath);
        Configure::write('Devs.export_roles_path', $f = TMP_TESTDIR . DS . 'export_roles.json');
        Filesystem::copy(RESOURCES . 'export_roles.json', $f, true);

        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'controllers', 'alias' => 'Devs'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::reset();
        Configure::write('App.paths.controllers_rules', $this->originalControllersRules);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        // Index my-transfers (1 résultat provenant des fixtures)
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/devs/index');
        $this->assertResponseCode(200);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertNotEmpty($response['controllers']);
        $this->assertNotEmpty($response['controllers']['Devs']);
        $this->assertContains('index', $response['controllers']['Devs']);
        $this->assertNotEmpty($response['data']);
        $this->assertNotEmpty($response['data']['Devs']);
        $this->assertNotEmpty($response['data']['Devs']['index']);
        $expected = [
            "invisible" => "1",
            "accesParDefaut" => "0",
            "commeDroits" => "",
        ];
        $this->assertEquals($expected, $response['data']['Devs']['index']);

        $data = [
            'new' => [
                'Tests::fake' => '1',
                'Tests::fake2' => '1',
            ],
            'update' => [
                'Tests::fake' => '1',
                'Tests::fake2' => '1',
                'Devs::index' => '1',
            ],
            'Tests' => [
                'fake' => [
                    'commeDroits' => 'Devs::index',
                ],
                'fake2' => [
                    'invisible' => '0',
                    'accesParDefaut' => '1',
                    'commeDroits' => '',
                ],
            ],
            'Devs' => [
                'index' => [
                    'invisible' => '0',
                    'accesParDefaut' => '1',
                    'commeDroits' => '',
                ],
            ],
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/devs/index', $data);
        $this->assertResponseCode(200);
        $rules = json_decode(file_get_contents(Configure::read('App.paths.controllers_rules')), true);
        $this->assertNotEmpty($rules);
        $this->assertNotEmpty($rules['Tests']);
        $this->assertNotEmpty($rules['Tests']['fake']);
        $this->assertNotEmpty($rules['Devs']);
        $this->assertNotEmpty($rules['Devs']['index']);
        $expected = [
            'invisible' => '0',
            'accesParDefaut' => '1',
            'commeDroits' => '',
        ];
        $this->assertEquals($expected, $rules['Devs']['index']);
        $expected = [
            'commeDroits' => 'Devs::index',
        ];
        $this->assertEquals($expected, $rules['Tests']['fake']);
    }

    /**
     * testPermissions
     */
    public function testPermissions()
    {
        $this->httpGet('/devs/permissions');
        $this->assertResponseCode(200);
    }

    /**
     * testSetPermission
     */
    public function testSetPermission()
    {
        $data = [
            'role' => 'Archiviste',
            'path' => 'root/controllers/Devs/setPermission',
            'value' => 'true',
        ];
        $this->httpPost('/devs/set-permission', $data);
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
        $export = json_decode(file_get_contents(TMP_TESTDIR . DS . 'export_roles.json'), true);
        $this->assertEquals('1111', Hash::get($export, 'Archiviste.root/controllers/Devs/setPermission'));
    }
}
