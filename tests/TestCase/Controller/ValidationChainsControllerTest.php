<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\ValidationChainsController;
use Asalae\Model\Table\ValidationChainsTable;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\ValidationChainsController Test Case
 */
#[UsesClass(ValidationChainsController::class)]
class ValidationChainsControllerTest extends TestCase
{
    use HttpTrait;
    use IntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        'app.ValidationActors',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->httpGet('/validation-chains/index');
        $this->assertResponseCode(200);

        $this->configRequest(
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );
        $this->get(
            '/validation-chains/index?name[0]=foo&app_type[0]=&app_type[0][]=ARCHIVE_TRANSFER&dateoperator_created[0]=<%3D&created[0]=14%2F01%2F2019&favoris[0]=0&favoris[0]=1&sort=favorite&direction=asc'
        );
        $this->assertResponseCode(200);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $this->httpGet('/validation-chains/add');
        $this->assertResponseCode(200);

        $this->httpPost('/validation-chains/add');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'name' => 'foo',
            'description' => 'bar',
            'app_type' => 'ARCHIVE_TRANSFER',
            'default' => true,
            'active' => true,
        ];
        $this->httpPost('/validation-chains/add', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->httpGet('/validation-chains/edit/1');
        $this->assertResponseCode(200);

        $this->httpPut('/validation-chains/edit/1');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'name' => 'foo',
            'description' => 'bar',
            'app_type' => 'ARCHIVE_TRANSFER',
            'default' => true,
            'active' => true,
        ];
        $this->httpPut('/validation-chains/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $this->delete('/validation-chains/delete/1');
        $this->assertResponseCode(403);

        $this->httpDelete('/validation-chains/delete/8');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpGet('/validation-chains/view/1');
        $this->assertResponseCode(200);
    }

    /**
     * testSetDefault
     */
    public function testSetDefault()
    {
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = TableRegistry::getTableLocator()->get('ValidationChains');
        $this->assertTrue((bool)$ValidationChains->get(1)->get('default'));
        $this->assertFalse($ValidationChains->get(8)->get('default'));

        $this->httpPut('/validation-chains/set-default/8');
        $this->assertResponseCode(200);

        $this->assertFalse($ValidationChains->get(1)->get('default'));
        $this->assertTrue((bool)$ValidationChains->get(8)->get('default'));
    }

    /**
     * testApiAdd
     */
    public function testApiAdd()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'ValidationChains'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $data = [
            'org_entity_id' => 2,
            'name' => 'api_test',
            'description' => 'api_test',
            'app_type' => 'ARCHIVE_TRANSFER_CONFORM',
            'default' => 'true',
            'active' => 'true',
        ];
        $json = $this->httpPost('/api/validation-chains', $data, true);
        $this->assertResponseCode(200);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('success', $json);
        $this->assertArrayHasKey('id', $json);
        $this->assertTrue($json['success']);
    }
}
