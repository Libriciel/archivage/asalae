<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\ValidationStagesController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\ValidationStagesController Test Case
 */
#[UsesClass(ValidationStagesController::class)]
class ValidationStagesControllerTest extends TestCase
{
    use HttpTrait;
    use IntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,

        // Pour ce controller
        'app.EventLogs',
        'app.ValidationStages',
        'app.ValidationChains',
        'app.ValidationActors',
        'app.ValidationProcesses',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->httpGet('/validation-stages/index/1');
        $this->assertResponseCode(200);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $this->httpGet('/validation-stages/add/1');
        $this->assertResponseCode(200);

        $this->httpPost('/validation-stages/add/1');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'name' => 'foo',
            'description' => 'bar',
            'all_actors_to_complete' => false,
        ];
        $this->httpPost('/validation-stages/add/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->httpGet('/validation-stages/edit/1');
        $this->assertResponseCode(200);

        $this->httpPut('/validation-stages/edit/1');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'name' => 'foo',
            'description' => 'bar',
            'all_actors_to_complete' => false,
            'default' => true,
            'active' => true,
        ];
        $this->httpPut('/validation-stages/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $switch = true;
        $ValidationStages = TableRegistry::getTableLocator()->get('ValidationStages');
        $ValidationStages->getEventManager()->on(
            'Model.beforeDelete',
            function () use (&$switch) {
                return $switch = !$switch;
            }
        );
        $response = $this->httpDelete('/validation-stages/delete/2');
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response->report));
        $this->assertNotEquals('done', $response->report);

        $count = $ValidationStages->find()->count();
        $this->httpDelete('/validation-stages/delete/2');
        $this->assertResponseCode(200);
        $this->assertEquals($count - 1, $ValidationStages->find()->count());
    }

    /**
     * testChangeOrder
     */
    public function testChangeOrder()
    {
        $switch = true;
        $ValidationStages = TableRegistry::getTableLocator()->get('ValidationStages');
        $ValidationStages->getEventManager()->on(
            'Model.beforeSave',
            function () use (&$switch) {
                return $switch = !$switch;
            }
        );

        $response = $this->httpPatch('/validation-stages/change-order/1/4');
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response->report));
        $this->assertNotEquals('done', $response->report);

        $response = $this->httpPatch('/validation-stages/change-order/1/2');
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response[0]->id));
        $this->assertEquals(2, $response[0]->id);
        $this->assertTrue(isset($response[1]->id));
        $this->assertEquals(1, $response[1]->id);
    }

    /**
     * testApiAdd
     */
    public function testApiAdd()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'ValidationStages'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $data = [
            'validation_chain_id' => 1,
            'name' => 'api_test',
            'description' => 'api_test',
            'all_actors_to_complete' => 'false',
            'ord' => null,
        ];
        $json = $this->httpPost('/api/validation-stages', $data, true);
        $this->assertResponseCode(200);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('success', $json);
        $this->assertTrue($json['success']);
        $this->assertArrayHasKey('id', $json);
    }
}
