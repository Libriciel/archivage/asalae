<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\ArchiveBinariesController;
use Asalae\Model\Volume\VolumeManager;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\ArchiveBinariesController Test Case
 */
#[UsesClass(ArchiveBinariesController::class)]
class ArchiveBinariesControllerTest extends TestCase
{
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use IntegrationTestTrait;
    use SaveBufferTrait;
    use AutoFixturesTrait;

    public const string SEDA_10_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml';
    public const string LIFECYCLE_PREMIS = ASALAE_CORE_TEST_DATA . DS . 'sample_premis.xml';
    public const string SAMPLE_PDF = ASALAE_CORE_TEST_DATA . DS . 'sample.pdf';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        FIXTURES_VOLUMES,
        'app.ArchiveBinariesArchiveUnits',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('Archives.updateLifecycle.path', TMP_TESTDIR);
        Configure::write('Archives.updateDescription.path', TMP_TESTDIR);
        Configure::write('Archives.updateLifecycle.cache_duration', 0);
        Configure::write('Archives.updateDescription.cache_duration', 0);
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Filesystem::setNamespace();
        Filesystem::reset();
        Utility::reset();
    }

    /**
     * testOpen
     */
    public function testOpen()
    {
        // Préparation d'un PDF pour le test
        $storedFile = (new VolumeManager(1))
            ->fileUpload(self::SAMPLE_PDF, 'sample.pdf');
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $ArchiveBinaries = TableRegistry::getTableLocator()->get('ArchiveBinaries');
        $ArchiveBinariesArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveBinariesArchiveUnits');
        $binary = $ArchiveBinaries->newEntity(
            [
                'is_integrity_ok' => null,
                'type' => 'test',
                'filename' => 'sample.pdf',
                'format' => null,
                'mime' => 'application/pdf',
                'extension' => 'pdf',
                'stored_file_id' => $storedFile->get('id'),
                'original_data_id' => null,
                'in_rgi' => false,
                'app_meta' => ['category' => 'document_pdf'],
            ]
        );
        $this->assertNotFalse($ArchiveBinaries->save($binary));

        $archive = $Archives->find()
            ->where(['Archives.id' => 1])
            ->contain(['ArchiveUnits'])
            ->first();
        $this->assertNotNull($archive);
        $abAu = $ArchiveBinariesArchiveUnits->newEntity(
            [
                'archive_binary_id' => $binary->id,
                'archive_unit_id' => Hash::get($archive, 'archive_units.0.id'),
            ]
        );
        $ArchiveBinariesArchiveUnits->save($abAu);

        // TEST
        $this->get('/archive-binaries/open/' . $binary->id);
        $this->assertRedirect(
            '/archive-binaries/download/' . $binary->id . '/sample.pdf?action=open'
        );

        $this->get('/archive-binaries/open/1');
        $this->assertResponseCode(400, __("Pas de visionneuse pour un fichier {0}", 'text/plain'));
    }

    /**
     * testDownload
     */
    public function testDownload()
    {
        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/archive-binaries/download/1'
        );
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        $expected = file_get_contents($basedir . 'volume01/sa/no_profile/2021-12/sa_1/original_data/sample.wmv');
        $this->assertEquals($expected, $output);
    }
}
