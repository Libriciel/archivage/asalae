<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\SecureDataSpacesController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\SecureDataSpacesController Test Case
 */
#[UsesClass(SecureDataSpacesController::class)]
class SecureDataSpacesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_VOLUMES,
    ];
    private $debugFixtures = false;

    /**
     * testPost
     */
    public function testPost()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'SecureDataSpaces'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        $this->configRequest(
            [
                'headers' => [
                    'authorization' => 'Basic ' . base64_encode('admin:admin'),
                ],
            ]
        );
        $loc = TableRegistry::getTableLocator();
        $loc->get('Volumes')->updateAll(['secure_data_space_id' => null], []);
        $data = [
            'name' => 'api_post',
            'description' => 'test',
            'volumes' => '1,2',
        ];
        $this->post('/api/secure-data-spaces', $data);
        $this->assertResponseCode(200);
        $this->assertTrue($loc->get('SecureDataSpaces')->exists(['name' => 'api_post']));
    }
}
