<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\FileuploadsController;
use AsalaeCore\Model\Table\FileuploadsTable;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Notify;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(FileuploadsController::class)]
class FileuploadsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,

        // Pour ce controller
        'app.EventLogs',
        'app.Fileuploads',
        'app.Siegfrieds',
        'app.Mediainfos',
        'app.Siegfrieds',
        'app.Profiles',
        'app.FileuploadsProfiles',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Filesystem::setNamespace();
        Filesystem::reset();
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $file = TMP_TESTDIR . DS . uniqid('test-');
        Filesystem::createDummyFile($file, 500);

        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $entity = $Fileuploads->newUploadedEntity('file.bin', $file, 1);
        $this->assertNotFalse($Fileuploads->save($entity, ['silent' => true]));

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpDelete('/fileuploads/delete/' . $entity->get('id'));
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity_deleted']);
        $this->assertNotEmpty($response['file_deleted']);
    }
}
