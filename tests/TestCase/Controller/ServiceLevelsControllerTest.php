<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\ServiceLevelsController;
use Asalae\Model\Entity\ServiceLevel;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(ServiceLevelsController::class)]
class ServiceLevelsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        'app.AgreementsServiceLevels',
        'app.OrgEntitiesTimestampers',
        'app.SecureDataSpaces',
        'app.Transfers',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->httpGet('/ServiceLevels/index');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['data']) && is_array($response['data']));
        $this->assertEquals(
            1,
            count(
                $response['data']
            )
        );
    }

    /**
     * testAdd1
     */
    public function testAdd1()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/ServiceLevels/add1');
        $this->assertResponseCode(200);

        $data = [
            'identifier' => 'test-add',
            'name' => 'Test add',
            'description' => "test d'un ajout d'accord de versement",
            'active' => true,
            'default_level' => false,
        ];
        $this->httpPost('/ServiceLevels/add1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testAdd2
     */
    public function testAdd2()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/ServiceLevels/add2');
        $this->assertResponseCode(200);

        $data = [
            'ts_msg_id' => 1,
            'ts_pjs_id' => 1,
            'ts_conv_id' => 1,
        ];
        $this->httpPost('/ServiceLevels/add2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $this->session(
            $this->genericSessionData + [
                'ServiceLevel' => [
                    'entity' => new ServiceLevel(
                        [
                            'org_entity_id' => 2,
                            'identifier' => 'test-add',
                            'name' => 'Test add',
                            'description' => "test d'un ajout d'accord de versement",
                            'active' => true,
                            'default_level' => false,
                        ]
                    ),
                ],
            ]
        );
        $this->httpPost('/ServiceLevels/add2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->httpGet('/ServiceLevels/edit/1');
        $this->assertResponseCode(200);

        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Transfers->updateAll(['service_level_id' => null], []);

        $data = [
            'identifier' => 'service-level-test',
            'name' => 'Test add',
            'description' => "test d'un ajout d'accord de versement",
            'active' => true,
            'default_level' => true,
            'ts_msg_id' => 1,
            'ts_pjs_id' => 1,
            'ts_conv_id' => 1,
        ];
        $this->httpPut('/ServiceLevels/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $this->delete('/ServiceLevels/delete/1');
        $this->assertResponseCode(403);

        $loc = TableRegistry::getTableLocator();
        $loc->get('Transfers')->updateAll(['service_level_id' => null], []);
        $loc->get('Archives')->updateAll(['service_level_id' => null], []);
        $loc->get('ServiceLevels')->updateAll(['default_level' => false], []);
        $this->httpDelete('/ServiceLevels/delete/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpGet('/service-levels/view/1');
        $this->assertResponseCode(200);
    }
}
