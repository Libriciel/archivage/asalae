<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\TransfersController;
use Asalae\Controller\UploadController;
use Asalae\Model\Table\TransfersTable;
use Asalae\Test\Mock\ClientMock;
use Asalae\Test\Mock\FakeAntivirus;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AbstractGenericMessageForm;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Object\CommandResult;
use AsalaeCore\Utility\PreControlMessages;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use Datacompressor\Utility\DataCompressor;
use DateInterval;
use DateTime;
use DOMDocument;
use DOMElement;
use Laminas\Diactoros\Stream;
use Laminas\Diactoros\UploadedFile;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

use const DS;
use const TMP_TESTDIR;

/**
 * Asalae\Controller\TransfersController Test Case
 */
#[UsesClass(TransfersController::class)]
class TransfersControllerTest extends TestCase
{
    use HttpTrait;
    use IntegrationTestTrait;
    use InvokePrivateTrait;
    use AutoFixturesTrait;
    use ConsoleIntegrationTestTrait;

    public const string XML = ASALAE_CORE_TEST_DATA . DS . 'seda02.xml';
    public const string SEDA_02_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xml';
    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';
    public const string SEDA_21_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda21.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        FIXTURES_VOLUMES,
        'app.AgreementsOriginatingAgencies',
        'app.AgreementsProfiles',
        'app.AgreementsServiceLevels',
        'app.AgreementsTransferringAgencies',
        'app.BeanstalkJobs',
        'app.Eaccpfs',
        'app.Fileuploads',
        'app.FileuploadsProfiles',
        'app.Keywords',
        'app.Killables',
        'app.Mails',
        'app.Mediainfos',
        'app.OrgEntitiesTimestampers',
        'app.Sequences',
        'app.Siegfrieds',
        'app.Siegfrieds',
        'app.TransferAttachments',
        'app.TransferErrors',
        'app.TransferLocks',
        'app.Users',
        'app.ValidationActors',
        'app.ValidationHistories',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Filesystem::reset();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        Filesystem::begin();
        Filesystem::mkdir($dir);
        Configure::write('App.paths.data', $dir);
        $transferXml = $dir . DS . 'transfers' . DS . '1' . DS . 'message' . DS . 'testfile.xml';
        Filesystem::copy(self::XML, $transferXml);
        Filesystem::copy(self::XML, $dir . DS . 'transfer_1_1_edit.xml');

        $Exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(['async', 'command'])
            ->getMock();
        $fn = function ($command, ...$args) {
            $h = fopen('/tmp/test.log', 'a');
            fwrite($h, $command . var_export($args, true) . "\n");
            fclose($h);
            if ($command === CAKE_SHELL) {
                $escaped = $this->extractArgsShellOptions($args);
                $command = implode(' ', $escaped);
                $out = new ConsoleOutput();
                $err = new ConsoleOutput();
                $io = new ConsoleIo($out, $err, new ConsoleInput([]));
                $runner = $this->makeRunner();
                $args = $this->commandStringToArgs("cake $command");
                $code = $runner->run($args, $io);
                if (is_file(TMP_TESTDIR . '/update_lifecycle/.lock')) {
                    unlink(TMP_TESTDIR . '/update_lifecycle/.lock');
                }
                return new CommandResult(
                    [
                        'success' => $code === 0,
                        'code' => $code,
                        'stdout' => implode(PHP_EOL, $out->messages()),
                        'stderr' => implode(PHP_EOL, $err->messages()),
                    ]
                );
            } else {
                $Exec = new Exec();
                return $Exec->command($command, ...$args);
            }
        };
        $Exec->method('async')->willReturnCallback($fn);
        $Exec->method('command')->willReturnCallback($fn);
        Utility::set(Exec::class, $Exec);
        Utility::set('Exec', $Exec);
        Configure::write('AjaxPaginator.limit', 100);

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')
            ->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
        VolumeSample::init();
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * extractArgsShellOptions
     * @param array $args
     * @return array
     */
    private function extractArgsShellOptions(array $args): array
    {
        $escaped = [];
        foreach ($args as $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $escaped[] = $k;
                    if ($v) {
                        $escaped[] = $v;
                    }
                }
            } else {
                $escaped[] = $value;
            }
        }
        return $escaped;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Filesystem::rollback();
        Filesystem::setNamespace();
        Filesystem::reset();
        Utility::reset();
        Exec::waitUntilAsyncFinish();
        VolumeSample::destroy();
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->httpGet('/transfers/index');
        $this->assertResponseCode(200);
    }

    /**
     * testAddByUpload
     */
    public function testAddByUpload()
    {
        if (is_file($uri = TMP_TESTDIR . DS . 'sample_seda10.xml')) {
            unlink($uri);
        }
        Filesystem::copy(self::SEDA_10_XML, $uri);
        $Fileuploads = TableRegistry::getTableLocator()
            ->get('Fileuploads');
        $file = $Fileuploads->newEntity(
            [
                'name' => 'sample_seda10.xml',
                'path' => $uri,
                'user_id' => 1,
                'locked' => 1,
            ]
        );
        $Fileuploads->saveOrFail($file, ['silent' => true]);

        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $OrgEntities->updateAll(['identifier' => 'sv1'], ['identifier' => 'sv']);
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Transfers->updateAll(
            ['transfer_identifier' => 'renamed'],
            ['transfer_identifier' => 'atr_alea_70f90eacdb2d3b53ad7e87f808a82430']
        );

        $this->httpGet('/transfers/addByUpload');
        $this->assertResponseCode(200);

        $data = [
            'fileuploads' => [
                'id' => $file->get('id'),
            ],
        ];
        $response = $this->httpPost('/transfers/addByUpload', $data);
        $this->assertResponseCode(200);
        $this->assertTrue(
            isset($response->entity->id) && $response
        );
        $this->assertEquals(
            1,
            $Transfers->find()
                ->where(['id' => $response->entity->id])
                ->count()
        );
        if (is_file($uri)) {
            unlink($uri);
        }
    }

    /**
     * testAdd1
     */
    public function testAdd1()
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');

        $this->httpGet('/transfers/add1');
        $this->assertResponseCode(200);

        $data = [
            'message_version' => 'seda1.0',
            'transfer_identifier' => '',
            'transfer_date' => '06/06/2019 15:17',
            'transfer_comment' => '',
            'transferring_agency_id' => '2',
            'originating_agency_id' => '2',
            'agreement_id' => '',
            'profile_id' => '',
            'service_level_id' => '',
            'name' => 'Archive',
            'description_level' => 'collection',
            'code' => 'AR038',
            'start_date' => '06/06/2019',
        ];
        $response = $this->httpPost('/transfers/add1', $data);
        $this->assertResponseCode(200);
        $this->assertTrue(
            isset($response->entity->id) && $response
        );
        $this->assertTrue($Transfers->exists(['id' => $response->entity->id]));

        $data['message_version'] = 'seda2.0';
        $response = $this->httpPost('/transfers/add1', $data);
        $this->assertResponseCode(200);
        $this->assertTrue(
            isset($response->entity->id) && $response
        );
        $this->assertTrue($Transfers->exists(['id' => $response->entity->id]));
    }

    /**
     * testAdd1Seda21
     */
    public function testAdd1Seda21()
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');

        $this->httpGet('/transfers/add1');
        $this->assertResponseCode(200);

        $data = [
            'message_version' => 'seda2.1',
            'transfer_identifier' => '',
            'transfer_date' => '06/06/2019 15:17',
            'transfer_comment' => '',
            'transferring_agency_id' => '2',
            'originating_agency_id' => '2',
            'agreement_id' => '',
            'profile_id' => '',
            'service_level_id' => '',
            'name' => 'Archive',
            'description_level' => 'collection',
            'code' => 'AR038',
            'start_date' => '06/06/2019',
        ];
        $response = $this->httpPost('/transfers/add1', $data);
        $this->assertResponseCode(200);
        $this->assertTrue(
            isset($response->entity->id) && $response
        );
        $this->assertTrue($Transfers->exists(['id' => $response->entity->id]));

        $data['message_version'] = 'seda2.0';
        $response = $this->httpPost('/transfers/add1', $data);
        $this->assertResponseCode(200);
        $this->assertTrue(
            isset($response->entity->id) && $response
        );
        $this->assertTrue($Transfers->exists(['id' => $response->entity->id]));
    }

    /**
     * testAdd2
     */
    public function testAdd2()
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Transfers->updateAll(['state' => TransfersTable::S_PREPARATING], ['id IN' => [1, 3]]);
        $transfer = $Transfers->get(1);

        if (is_file($uri = $transfer->get('xml'))) {
            unlink($uri);
        }
        Filesystem::copy(self::SEDA_10_XML, $uri);

        $this->httpGet('/transfers/add2/1');
        $this->assertResponseCode(200);

        $domutil = DOMUtility::load($uri);
        $countDocs = $domutil->xpath->query('//ns:Document')
            ->count();

        $this->httpPut(
            '/transfers/add2/1',
            ['generate_tree' => true]
        ); // génère un Document à partir des TransferAttachments
        $this->assertResponseCode(200);

        $domutil = DOMUtility::load($uri);
        $this->assertGreaterThanOrEqual(
            $countDocs + 1,
            $domutil->xpath->query('//ns:Document')
                ->count()
        );

        /**
         * Seda 2.1
         */
        $transfer = $Transfers->get(3);
        if (is_file($uri = $transfer->get('xml'))) {
            unlink($uri);
        }
        $domutil = DOMUtility::load(self::SEDA_21_XML);
        foreach ($domutil->xpath->query('//ns:BinaryDataObject') as $elem) {
            if ($elem instanceof DOMElement) {
                $elem->parentNode->removeChild($elem);
            }
        }
        Filesystem::mkdir(dirname($uri));
        $domutil->dom->save($uri);

        $this->httpGet('/transfers/add2/1');
        $this->assertResponseCode(200);

        $domutil = DOMUtility::load($uri);
        $countDocs = $domutil->xpath->query('//ns:BinaryDataObject')
            ->count();

        $this->httpPut(
            '/transfers/add2/3',
            ['generate_tree' => true]
        ); // génère un Document à partir des TransferAttachments
        $this->assertResponseCode(200);

        $domutil = DOMUtility::load($uri);
        $this->assertGreaterThanOrEqual(
            $countDocs + 1,
            $domutil->xpath->query('//ns:BinaryDataObject')
                ->count()
        );
    }

    /**
     * testAddByUpload2
     */
    public function testAddByUpload2()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Transfers->updateAll(['state' => TransfersTable::S_PREPARATING], ['id' => 1]);
        $this->httpGet('/transfers/addByUpload2/1');
        $this->assertResponseCode(200);
    }

    /**
     * testDisplayXml
     */
    public function testDisplayXml()
    {
        $Transfers = TableRegistry::getTableLocator()
            ->get('Transfers');
        $transfer = $Transfers->get(1);

        if (is_file($uri = $transfer->get('xml'))) {
            unlink($uri);
        }
        Filesystem::copy(self::SEDA_10_XML, $uri);

        $this->httpGet('/transfers/display-xml/1');
        $this->assertResponseCode(200);
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpGet('/transfers/view/1');
        $this->assertResponseCode(200);
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $this->httpDelete('/transfers/delete/8');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);

        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Transfers->updateAll(['state' => TransfersTable::S_PREPARATING], ['id' => 1]);
        if (is_file(TMP_TESTDIR . DS . 'transfer_1_1_edit.xml')) {
            unlink(TMP_TESTDIR . DS . 'transfer_1_1_edit.xml');
        }

        // Test save
        $this->httpGet('/transfers/edit/1');
        $this->assertResponseCode(200);

        $this->httpGet('/transfers/partialEdit/1/ArchiveTransfer');
        $this->assertResponseCode(200);

        $data = [
            'TransferIdentifier' => [
                ['@' => $uid = uniqid('identifier-', true)],
            ],
        ];
        $this->httpPost('/transfers/partialEdit/1/ArchiveTransfer', $data);
        $this->assertResponseCode(200);

        $this->httpPost('/transfers/edit/1');
        $this->assertResponseCode(200);

        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $xml = file_get_contents($Transfers->get(1)->get('xml'));
        $this->assertStringContainsString($uid, $xml);

        // Test delete
        $this->httpGet('/transfers/edit/1');
        $this->assertResponseCode(200);

        $this->httpGet('/transfers/partialEdit/1/ArchiveTransfer');
        $this->assertResponseCode(200);

        $data = [
            'TransferIdentifier' => [
                ['@' => 'foo'],
            ],
        ];
        $this->httpPost('/transfers/partialEdit/1/ArchiveTransfer', $data);
        $this->assertResponseCode(200);

        $this->httpDelete('/transfers/edit/1');
        $this->assertResponseCode(200);

        $xml = file_get_contents(
            $Transfers->get(1)
                ->get('xml')
        );
        $this->assertStringNotContainsString('foo', $xml);
    }

    /**
     * testGetTree
     */
    public function testGetTree()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        $response = $this->httpGet('/transfers/getTree/8');
        $this->assertResponseCode(200);
        $this->assertTrue($response && isset($response->url));
        $this->assertEquals('ArchiveTransfer', $response->url);

        $response = $this->httpGet(
            '/transfers/getTree/8?id=1_ArchiveTransfer_Contains&addable=true'
        );
        $this->assertResponseCode(200);
        $this->assertTrue(
            isset($response[0]->url) && $response
        );
        $this->assertEquals(
            'ArchiveTransfer/Contains/ContentDescription',
            $response[0]->url
        );
    }

    /**
     * testPartialEdit
     */
    public function testPartialEdit()
    {
        if (is_file($xml = TMP_TESTDIR . DS . 'transfer_1_1_edit.xml')) {
            unlink($xml);
        }
        Filesystem::copy(self::SEDA_02_XML, $xml);
        $domutil = DOMUtility::load($xml);
        $initialIdentifier = $domutil->xpath->query('//ns:TransferIdentifier')
            ->item(0)->nodeValue;

        $this->get('/transfers/partialEdit/1/ArchiveTransfer');
        $this->assertResponseCode(200);

        $data = [
            'TransferIdentifier' => [
                ['@' => 'foo'],
            ],
        ];
        $this->httpPost('/transfers/partialEdit/1/ArchiveTransfer', $data);
        $this->assertResponseCode(200);
        $domutil = DOMUtility::load($xml);
        $newIdentifier = $domutil->xpath->query('//ns:TransferIdentifier')
            ->item(0)->nodeValue;
        $this->assertNotEquals($initialIdentifier, $newIdentifier);
        $this->assertEquals('foo', $newIdentifier);
    }

    /**
     * testPartialDelete
     */
    public function testPartialDelete()
    {
        Filesystem::copy(self::XML, TMP_TESTDIR . DS . 'transfer_1_1_edit.xml');
        $this->delete(
            '/transfers/partialDelete/1/ArchiveTransfer/Contains/ContentDescription/ContentDescriptive[2]'
        );
        $this->assertResponseCode(200);
    }

    /**
     * testPartialAdd
     */
    public function testPartialAdd()
    {
        if (is_file($xml = TMP_TESTDIR . DS . 'transfer_8_1_edit.xml')) {
            unlink($xml);
        }
        Filesystem::copy(self::SEDA_02_XML, $xml);
        $this->httpGet('/transfers/partialAdd/8/Document/ArchiveTransfer/Contains');
        $this->assertResponseCode(200);
    }

    /**
     * testAddCompressed
     */
    public function testAddCompressed()
    {
        if (is_file($xml = TMP_TESTDIR . DS . 'transfers/1/message/transfer-61b20d8ded833.xml')) {
            unlink($xml);
        }
        Filesystem::copy(self::SEDA_02_XML, $xml);
        // On prépare un zip à ajouter au bordereau
        Filesystem::createDummyFile(
            $file = TMP_TESTDIR . DS . uniqid('testunit-file'),
            10
        );
        $zip = AbstractGenericMessageForm::getDataDirectory(
            1
        ) . DS . 'attachments' . DS . basename($file) . '.zip';
        DataCompressor::compress($file, $zip);

        $TransferAttachments = TableRegistry::getTableLocator()
            ->get('TransferAttachments');
        $attachment = $TransferAttachments->newEntity(
            [
                'transfer_id' => 1,
                'filename' => basename($zip),
                'size' => filesize($zip),
                'hash' => 'foo',
                'hash_algo' => 'bar',
            ]
        );
        $TransferAttachments->saveOrFail($attachment);

        $fullpath = Configure::read(
            'App.paths.data'
        ) . DS . 'transfers' . DS . '1' . DS . 'attachments' . DS . basename(
            $file
        );
        Filesystem::addToDeleteIfRollback([$fullpath, $file, $zip]);
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost(
            '/transfers/add-compressed/' . $attachment->get('id')
        );
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(
            !empty($response[0]) && !empty($response[0]['filename'])
        );
        $this->assertTrue(
            in_array(basename($file), Hash::extract($response, '{n}.filename'))
        );
    }

    /**
     * testMoveNode
     */
    public function testMoveNode()
    {
        Filesystem::copy(self::XML, TMP_TESTDIR . DS . 'transfer_1_1_edit.xml');
        $data = [
            'move' => 'ArchiveTransfer/Contains/ContentDescription/ContentDescriptive[1]',
            'direction' => 'after',
            'target' => 'ArchiveTransfer/Contains/ContentDescription/ContentDescriptive[2]',
        ];
        $this->httpPost('/transfers/move-node/1', $data);
        $this->assertResponseCode(200);
    }

    /**
     * testAddMultipleKeywords
     */
    public function testAddMultipleKeywords()
    {
        if (is_file($xml = TMP_TESTDIR . DS . 'transfer_8_1_edit.xml')) {
            unlink($xml);
        }
        Filesystem::copy(self::SEDA_02_XML, $xml);
        $domutil = DOMUtility::load($xml);
        $initialCount = $domutil->xpath->query('//ns:ContentDescriptive')
            ->count();

        $this->httpGet(
            '/transfers/add-multiple-keywords/8/ArchiveTransfer/Contains/ContentDescription'
        );
        $this->assertResponseCode(200);

        $data = [
            'load-keywords' => [
                1,
                2,
            ],
            'customKeyword' => [
                [
                    'name' => 'foo',
                    'code' => 'bar',
                ],
                [
                    'name' => 'baz',
                    'code' => '',
                ],
            ],
        ];
        $this->httpPost(
            '/transfers/add-multiple-keywords/8/ArchiveTransfer/Contains/ContentDescription',
            $data
        );
        $this->assertResponseCode(200);

        $domutil = DOMUtility::load($xml);
        $this->assertEquals(
            $initialCount + 4,
            $domutil->xpath->query('//ns:ContentDescriptive')
                ->count()
        );
    }

    /**
     * testIndexPreparating
     */
    public function testIndexPreparating()
    {
        $this->httpGet('/transfers/indexPreparating');
        $this->assertResponseCode(200);
    }

    /**
     * testIndexAccepted
     */
    public function testIndexAccepted()
    {
        $this->get('/transfers/indexAccepted');
        $this->assertResponseCode(200, $this->extractResponseError());
    }

    /**
     * testIndexRejected
     */
    public function testIndexRejected()
    {
        $this->get('/transfers/indexRejected');
        $this->assertResponseCode(200);
    }

    /**
     * testIndexMyAccepted
     */
    public function testIndexMyAccepted()
    {
        $this->get('/transfers/indexMyAccepted');
        $this->assertResponseCode(200);
    }

    /**
     * testIndexMyRejected
     */
    public function testIndexMyRejected()
    {
        $this->get('/transfers/indexMyRejected');
        $this->assertResponseCode(200);
    }

    /**
     * testIndexSent
     */
    public function testIndexSent()
    {
        $this->get('/transfers/indexSent');
        $this->assertResponseCode(200);
    }

    /**
     * testIndexMy
     */
    public function testIndexMy()
    {
        $this->get('/transfers/indexMy');
        $this->assertResponseCode(200);
    }

    /**
     * testIndexMyEntity
     */
    public function testIndexMyEntity()
    {
        $this->get('/transfers/indexMyEntity');
        $this->assertResponseCode(200);
    }

    /**
     * testSend
     */
    public function testSend()
    {
        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')
            ->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
        TableRegistry::getTableLocator()->get('Transfers')
            ->updateAll(['state' => TransfersTable::S_PREPARATING], ['id' => 1]);

        $this->httpPost('/transfers/send/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testViewAttachments
     */
    public function testViewAttachments()
    {
        $response = $this->httpGet(
            '/transfers/viewAttachments/1?filename=sample.wmv&size[0][value]=1&size[0][mult]=1&size[0][operator]=>%3D'
        );
        $this->assertResponseCode(200);
        $this->assertGreaterThanOrEqual(
            1,
            count($response)
        );
    }

    /**
     * testViewErrors
     */
    public function testViewErrors()
    {
        $response = $this->httpGet('/transfers/viewErrors/2');
        $this->assertResponseCode(200);
        $this->assertGreaterThanOrEqual(
            1,
            count($response)
        );

        $filteredResponse = $this->httpGet('/transfers/viewErrors/2?code=1');
        $this->assertResponseCode(200);
        $this->assertGreaterThanOrEqual(
            count(
                $filteredResponse
            ),
            count($response)
        );

        $filteredResponse = $this->httpGet(
            '/transfers/viewErrors/2?code=1234567890'
        );
        $this->assertResponseCode(200);
        $this->assertEquals(
            0,
            count($filteredResponse)
        );
    }

    /**
     * testAnalyse
     */
    public function testAnalyse()
    {
        if (is_file($xml = TMP_TESTDIR . DS . 'transfers/1/message/transfer-61b20d8ded833.xml')) {
            unlink($xml);
        }
        Filesystem::copy(self::SEDA_02_XML, $xml);
        Configure::write('Antivirus.classname', FakeAntivirus::class);

        $TransferErrors = TableRegistry::getTableLocator()->get('TransferErrors');
        $TransferErrors->deleteAll([]);

        $response = $this->httpGet('/transfers/analyse/1');
        $this->assertResponseCode(200);
        $this->assertTrue(
            isset($response->entity->transfer_errors) && $response
        );
        $this->assertGreaterThanOrEqual(
            4,
            count($response->entity->transfer_errors)
        );
    }

    /**
     * testSearchInEdit
     */
    public function testSearchInEdit()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        $data = [
            'search' => 'file1.txt',
        ];
        $response = $this->httpPost('/transfers/searchInEdit/3', $data);
        $this->assertResponseCode(200);
        $this->assertTrue(
            $response && $response[0] && $response[1] && $response[0]->path && $response[1]->path
        );
        $this->assertEquals(
            '/ArchiveTransfer/DataObjectPackage/BinaryDataObject/FileInfo/Filename',
            $response[0]->path
        );
        $this->assertEquals(
            '/ArchiveTransfer/DataObjectPackage/BinaryDataObject/Attachment@filename',
            $response[1]->path
        );
    }

    /**
     * testExplore
     */
    public function testExplore()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        $this->httpGet('/transfers/explore/1');
        $this->assertResponseCode(200);
    }

    /**
     * testPartialExplore
     */
    public function testPartialExplore()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        $response = $this->httpGet(
            '/transfers/partialExplore/1/ArchiveTransfer'
        );
        $this->assertResponseCode(200);
        $this->assertTrue(
            isset($response->nodes->Comment[0]->value) && $response
        );
        $this->assertStringContainsString(
            'seda1.0',
            $response->nodes->Comment[0]->value
        );
    }

    /**
     * testRemoveEmptySubFolders
     */
    public function testRemoveEmptySubFolders()
    {
        $controller = new TransfersController(new ServerRequest());
        mkdir($dir1 = TMP_TESTDIR . DS . 'testdir1');
        mkdir($dir1 . DS . 'subdir');
        mkdir($dir2 = TMP_TESTDIR . DS . 'testdir2');
        mkdir($subdir = $dir2 . DS . 'subdir');
        file_put_contents($subdir . DS . 'test.file', 'test');
        $this->invokeMethod($controller, 'removeEmptySubFolders', [TMP_TESTDIR]);
        $this->assertDirectoryDoesNotExist($dir1);
        $this->assertDirectoryExists($dir2);
    }

    /**
     * testDeleteAttachment
     */
    public function testDeleteAttachment()
    {
        // contourne le _getDeletable (le filename ne doit pas être dans le transfert)
        TableRegistry::getTableLocator()->get('TransferAttachments')->updateAll(
            ['filename' => 'sample.pdf.bak'],
            ['id' => 9]
        );
        $data = [
            'filename' => 'sample.pdf.bak',
        ];
        $this->httpDelete('/transfers/deleteAttachment/8', $data);
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testDownload
     */
    public function testDownload()
    {
        if (is_file($xml = TMP_TESTDIR . DS . 'transfers/1/message/transfer-61b20d8ded833.xml')) {
            unlink($xml);
        }
        Filesystem::copy(self::SEDA_02_XML, $xml);
        $this->get('/transfers/download/1');
        $this->assertResponseCode(200);
        $this->assertHeaderContains('Content-Type', 'application/xml');
    }

    /**
     * testMakeZip
     */
    public function testMakeZip()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        Configure::write('App.paths.data', TMP_TESTDIR);
        Filesystem::createDummyFile(
            TMP_TESTDIR . DS . 'download' . DS . 'transfer' . DS . '1_transfer_files.zip',
            10
        );
        $this->httpGet('/transfers/make-zip/1');
        $this->assertResponseCode(200);
        Filesystem::remove(
            TMP_TESTDIR . DS . 'download' . DS . 'transfer' . DS . '1_transfer_files.zip'
        );

        Filesystem::createDummyFile(
            TMP_TESTDIR . DS . 'transfers' . DS . '1' . DS . 'attachments' . DS . 'file',
            10
        );
        Filesystem::createDummyFile(
            TMP_TESTDIR . DS . 'transfers' . DS . '1' . DS . 'attachments' . DS . 'sample.pdf',
            10
        );
        $this->httpGet('/transfers/make-zip/1');
        $this->assertResponseCode(200);
        Filesystem::remove(
            TMP_TESTDIR . DS . 'download' . DS . 'transfer' . DS . '1_transfer_files.zip'
        );

        Configure::write('Downloads.asyncFileCreationTimeLimit', -1);
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')
            ->willReturnSelf();
        Utility::set(Exec::class, $Exec);
        $this->httpGet('/transfers/make-zip/1');
        $this->assertResponseCode(200);
        $this->assertHeaderContains('X-Asalae-ZipFileDownload', 'email');
    }

    /**
     * testDownloadFiles
     */
    public function testDownloadFiles()
    {
        Configure::write('App.paths.data', TMP_TESTDIR);
        Filesystem::createDummyFile(
            TMP_TESTDIR . DS . 'download' . DS . 'transfer' . DS . '1_transfer_files.zip',
            10
        );
        $this->httpGet('/transfers/download-files/1');
        $this->assertResponseCode(200);
        Filesystem::remove(
            TMP_TESTDIR . DS . 'download' . DS . 'transfer' . DS . '1_transfer_files.zip'
        );
    }

    /**
     * testDuplicate
     */
    public function testDuplicate()
    {
        if (is_file($xml = TMP_TESTDIR . DS . 'transfers/1/message/transfer-61b20d8ded833.xml')) {
            unlink($xml);
        }
        Filesystem::copy(self::SEDA_02_XML, $xml);
        $this->get('/transfers/duplicate/1');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testReEdit
     */
    public function testReEdit()
    {
        $Transfers = TableRegistry::getTableLocator()
            ->get('Transfers');
        $transfer = $Transfers->newEntity(
            [
                'transfer_comment' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'transfer_date' => '2018-04-25 09:34:15.000000',
                'transfer_identifier' => 'rejected',
                'archival_agency_id' => 2,
                'transferring_agency_id' => 4,
                'originating_agency_id' => 4,
                'message_version' => 'seda0.2',
                'state' => 'rejected',
                'state_history' => '',
                'last_state_update' => new \DateTime(),
                'is_conform' => null,
                'is_modified' => false,
                'is_accepted' => null,
                'agreement_id' => null,
                'profile_id' => null,
                'filename' => 'testfile.xml',
                'data_size' => 2,
                'data_count' => 2,
                'created_user_id' => 1,
                'service_level_id' => 1,
                'files_deleted' => false,
            ]
        );
        $Transfers->saveOrFail($transfer);

        $this->get('/transfers/re-edit/' . $transfer->get('id'));
        $this->assertResponseCode(200);
        $this->assertEquals(
            'preparating',
            $Transfers->get($transfer->get('id'))
                ->get('state')
        );
    }

    /**
     * testReEditFailOnNotCreatorUser
     */
    public function testReEditFailOnNotCreatorUser()
    {
        $Transfers = TableRegistry::getTableLocator()
            ->get('Transfers');
        $transfer = $Transfers->newEntity(
            [
                'transfer_comment' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'transfer_date' => '2018-04-25 09:34:15.000000',
                'transfer_identifier' => 'rejected',
                'archival_agency_id' => 2,
                'transferring_agency_id' => 4,
                'originating_agency_id' => 4,
                'message_version' => 'seda0.2',
                'state' => 'rejected',
                'state_history' => '',
                'last_state_update' => new \DateTime(),
                'is_conform' => null,
                'is_modified' => false,
                'is_accepted' => null,
                'agreement_id' => null,
                'profile_id' => null,
                'filename' => 'testfile.xml',
                'data_size' => 2,
                'data_count' => 2,
                'created_user_id' => 2,
                'service_level_id' => 1,
                'files_deleted' => false,
            ]
        );
        $Transfers->saveOrFail($transfer);

        $this->get('/transfers/re-edit/' . $transfer->get('id'));
        $this->assertResponseCode(404);
    }

    /**
     * testRepair
     */
    public function testRepair()
    {
        TableRegistry::getTableLocator()->get('Transfers')
            ->updateAll(
                ['transfer_identifier' => 'test'],
                ['transfer_identifier' => 'atr_alea_70f90eacdb2d3b53ad7e87f808a82430']
            );
        TableRegistry::getTableLocator()->get('OrgEntities')
            ->updateAll(
                ['identifier' => 'sv1'],
                ['identifier' => 'sv']
            );
        $path = TMP_TESTDIR . DS . 'user_1' . DS . 'seda10.xml';
        Filesystem::copy(self::SEDA_10_XML, $path);
        $b64 = str_replace(
            ['+', '/'],
            ['-', '_'],
            base64_encode(
                Security::encrypt(
                    $path,
                    hash(UploadController::HASH_ALGO, UploadController::FILE_ENCRYPT_KEY)
                )
            )
        );
        $this->get('/transfers/repair/' . $b64);
        $this->assertResponseCode(200);

        $this->post('/transfers/repair/' . $b64, ['precontrol' => true]);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');

        $this->delete('/transfers/repair/' . $b64);
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testGenerateIdentifier
     */
    public function testGenerateIdentifier()
    {
        TableRegistry::getTableLocator()->get('OrgEntities')
            ->updateAll(
                ['identifier' => 'sv1'],
                ['identifier' => 'sv']
            );
        $path = TMP_TESTDIR . DS . 'user_1' . DS . 'seda10.xml';
        Filesystem::copy(self::SEDA_10_XML, $path);
        $b64 = str_replace(
            ['+', '/'],
            ['-', '_'],
            base64_encode(
                Security::encrypt(
                    $path,
                    hash(UploadController::HASH_ALGO, UploadController::FILE_ENCRYPT_KEY)
                )
            )
        );

        $precontrol = new PreControlMessages($path);
        $identifier = $precontrol->getXpath()
            ->query($precontrol->identifierIdPaths[$precontrol->getNamespace()])
            ->item(0)
            ->nodeValue;

        $this->post('/transfers/generateIdentifier/' . $b64);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');

        $precontrol = new PreControlMessages($path);
        $identifier2 = $precontrol->getXpath()
            ->query($precontrol->identifierIdPaths[$precontrol->getNamespace()])
            ->item(0)
            ->nodeValue;

        $this->assertNotEquals($identifier, $identifier2);
    }

    /**
     * testCreateFromExisting
     */
    public function testCreateFromExisting()
    {
        TableRegistry::getTableLocator()->get('OrgEntities')
            ->updateAll(
                ['identifier' => 'sv1'],
                ['identifier' => 'sv']
            );
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Transfers->updateAll(
            ['transfer_identifier' => 'renamed'],
            ['transfer_identifier' => 'atr_alea_70f90eacdb2d3b53ad7e87f808a82430']
        );
        $path = TMP_TESTDIR . DS . 'user_1' . DS . 'seda10.xml';
        Filesystem::copy(self::SEDA_10_XML, $path);
        $b64 = str_replace(
            ['+', '/'],
            ['-', '_'],
            base64_encode(
                Security::encrypt(
                    $path,
                    hash(UploadController::HASH_ALGO, UploadController::FILE_ENCRYPT_KEY)
                )
            )
        );
        $this->get('/transfers/createFromExisting/' . $b64);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertResponseContains('"id":');
    }

    /**
     * testGetEditMeta
     */
    public function testGetEditMeta()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $Transfers->updateAll(
            ['state' => TransfersTable::S_PREPARATING],
            ['id' => 1]
        );
        $this->get('/transfers/getEditMeta/1');
        $this->assertResponseCode(200);
    }

    /**
     * testAbortAnalyse
     */
    public function testAbortAnalyse()
    {
        $Killables = TableRegistry::getTableLocator()
            ->get('Killables');
        $killable = $Killables->newEntity(
            [
                'name' => 'analyse',
                'user_id' => 1,
                'session_token' => 'testunit',
                'pid' => getmypid(),
                'killed' => false,
                'timeout' => (new DateTime())->add(new DateInterval('P1D')),
            ]
        );
        $Killables->saveOrFail($killable);
        $this->get('/transfers/abortAnalyse');
        $this->assertResponseCode(200);
        $killed = $Killables->get($killable->id)->get('killed');
        $this->assertNotFalse($killed);
    }

    /**
     * testIndexAll
     */
    public function testIndexAll()
    {
        $this->get('/transfers/indexAll');
        $this->assertResponseCode(200);
    }

    /**
     * testMessage
     */
    public function testMessage()
    {
        $this->get('/transfers/message/1/acknowledgement');
        $this->assertResponseCode(200);
    }

    /**
     * testPopulateSelect
     */
    public function testPopulateSelect()
    {
        VolumeSample::init();
        VolumeSample::copySample('sample_seda21.xml', 'transfer_4_1_edit.xml');
        $this->get('/transfers/populateSelect/4');
        $this->assertResponseCode(200);
    }

    /**
     * testUpdateAttachmentWithLogs
     */
    public function testUpdateAttachmentWithLogs()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        $controller = new TransfersController(new ServerRequest());
        $Transfers = TableRegistry::getTableLocator()
            ->get('Transfers');
        $TransferAttachments = TableRegistry::getTableLocator()
            ->get('TransferAttachments');
        $attachement = $TransferAttachments->find()
            ->where(['transfer_id' => 1, ' filename' => 'sample.wmv'])
            ->firstOrFail();
        $attachement->set('xpath', '');
        $TransferAttachments->saveOrFail($attachement);
        $transfer = $Transfers->get(1);
        $logs = fopen(TMP_TESTDIR . DS . 'logs.log', 'w');
        fwrite($logs, "detach:sample.wmv\n");
        fwrite($logs, "move:sample.wmv\n");
        fwrite($logs, "delete:sample.wmv\n");
        fwrite($logs, "attach:sample.wmv\n");
        fclose($logs);
        $this->invokeMethod(
            $controller,
            'updateAttachmentWithLogs',
            [$transfer, TMP_TESTDIR . DS . 'logs.log']
        );
        $this->assertNotEmpty(
            $TransferAttachments->get($attachement->id)
                ->get('xpath')
        );
    }

    /**
     * testGetXpath
     */
    public function testGetXpath()
    {
        if (is_file($xml = TMP_TESTDIR . DS . 'transfer_1_1_edit.xml')) {
            unlink($xml);
        }
        Filesystem::copy(self::SEDA_02_XML, $xml);
        TableRegistry::getTableLocator()->get('TransferAttachments')
            ->updateAll(['filename' => 'sample.pdf'], ['id' => 1]);
        $this->get('/transfers/getXpath/1/1');
        $this->assertResponseCode(200);
        $this->assertEquals(
            '/ArchiveTransfer/Contains/Contains/Document',
            json_decode($this->getResponseAsString())
        );
    }

    /**
     * testDownloadPdf
     */
    public function testDownloadPdf()
    {
        VolumeSample::init();
        VolumeSample::copySample(
            'sample_seda10.xml',
            'transfers/3/message/transfer-61b2124888ab2.xml'
        );
        $this->get('/transfers/downloadPdf/3');
        $this->assertResponseCode(200);
        $this->assertHeaderContains('Content-Disposition', 'transfer-61b2124888ab2.pdf');
    }

    /**
     * testMakePdf
     */
    public function testMakePdf()
    {
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')
            ->willReturnSelf();
        Utility::set(Exec::class, $Exec);
        VolumeSample::init();
        VolumeSample::copySample(
            'sample_seda10.xml',
            'transfers/3/message/testfile.xml'
        );
        $this->get('/transfers/makePdf/3');
        $this->assertResponseCode(200);
        $this->assertHeaderContains('X-Asalae-PdfFileDownload', 'email');
    }

    /**
     * testApiPost
     */
    public function testApiPost()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'Transfers'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $data = [
            'dryrun' => true,
            'transferIdentifier' => 'testunit',
            'archivalAgencyIdentifier' => 'sa',
            'transferringAgencyIdentifier' => 'sa',
        ];
        $this->post('/api/transfers', $data);
        $this->assertResponseCode(200);
        $this->assertResponseContains('ok');

        $path = VolumeSample::copySample(
            'sample_transfer10.zip',
            'transfer.zip'
        );
        $zip = new UploadedFile(
            $path,
            filesize($path),
            UPLOAD_ERR_OK,
            basename($path),
            mime_content_type($path)
        );
        $this->configRequest(['files' => ['transfer' => $zip]]);
        $data = [
            'transferIdentifier' => 'testunit',
            'archivalAgencyIdentifier' => 'sa',
            'transferringAgencyIdentifier' => 'sa',
        ];
        $json = $this->httpPost('/api/transfers', $data, true);
        $this->assertResponseCode(200);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('success', $json);
        $this->assertArrayHasKey('id', $json);
        $this->assertTrue($json['success']);

        // envoi par morceaux
        $path = VolumeSample::copySample(
            'sample_transfer10.zip',
            'transfer.zip'
        );
        $this->_request = [];
        $sha256 = hash_file('sha256', $path);
        $size = filesize($path);
        $chunkCount = ceil($size / 512);
        $handle = fopen($path, 'r');
        $this->assertNotFalse($handle);
        $i = 0;
        $uniqid = md5('test');
        while (!feof($handle)) {
            $i++;
            $chunk = fread($handle, 512);
            $queryParams = http_build_query(
                [
                    'filename' => 'sample_transfer10.zip',
                    'currentSize' => strlen($chunk),
                    'currentChunk' => $i,
                    'totalSize' => $size,
                    'totalChunks' => $chunkCount,
                    'crc32b' => hash('crc32b', $chunk),
                    'finalSha256' => $sha256,
                ]
            );
            $this->post(
                '/api/transfers/prepare-chunked/' . $uniqid . '?' . $queryParams,
                $chunk
            );
        }
        fclose($handle);
        $this->assertResponseCode(201);

        $data = [
            'transfer' => $uniqid,
            'transferIdentifier' => 'testunit',
            'archivalAgencyIdentifier' => 'sa',
            'transferringAgencyIdentifier' => 'sa',
        ];
        $json = $this->httpPost('/api/transfers', $data, true);
        $this->assertTrue($json['success']);
    }

    /**
     * testApiPostDistant
     */
    public function testApiPostDistant()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'Transfers'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        VolumeSample::init();
        $path = VolumeSample::copySample(
            'sample_transfer10.zip',
            'transfer.zip'
        );
        $response = $this->createMock(Client\Response::class);
        $response->method('getJson')
            ->willReturn(
                [
                    'success' => true,
                    'id' => 123456,
                    'uploaded_response' => 'done',
                ]
            );
        $response->method('getStatusCode')
            ->willReturn(200);
        $body = new Stream(fopen($path, 'r'));
        $response->method('getBody')
            ->willReturn($body);
        $response->method('getHeaderLine')
            ->willReturn(
                'attachment; filename="transfer.zip"',
                (string)filesize($path)
            );
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get'])
            ->getMock();
        $client->method('get')
            ->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
        $data = [
            'transfer' => 'https://fake.url/mytransfer.zip',
            'transferIdentifier' => 'testunit',
            'archivalAgencyIdentifier' => 'sa',
            'transferringAgencyIdentifier' => 'sa',
        ];
        $json = $this->httpPost('/api/transfers', $data, true);
        $this->assertResponseCode(200);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('success', $json);
        $this->assertArrayHasKey('id', $json);
        $this->assertTrue($json['success']);
    }

    /**
     * testApiAcknowledgment
     */
    public function testApiAcknowledgment()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'Transfers', 'alias' => 'acknowledgement'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        $this->get(
            '/api/transfers/acknowledgement/seda1.0'
        );
        $this->assertResponseCode(200);
        $xml = $this->getResponseAsString();
        $dom = new DOMDocument();
        $this->assertTrue($dom->loadXML($xml));
    }

    /**
     * testApiReply
     */
    public function testApiReply()
    {
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'Transfers', 'alias' => 'reply'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $this->get(
            '/api/transfers/reply/seda1.0'
        ); // transfer_id = 3 ; archive_id = 4
        $this->assertResponseCode(200);
        $xml = $this->getResponseAsString();
        $dom = new DOMDocument();
        $this->assertTrue($dom->loadXML($xml));
    }

    /**
     * testPrepareChunked
     */
    public function testPrepareChunked()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'Transfers'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'transfers ' . TMP_TESTDIR);
        $chunk1 = 'foo';
        $chunk2 = 'bar';
        $sha256 = hash('sha256', $chunk1 . $chunk2);
        $queryParamsChunk1 = http_build_query(
            [
                'filename' => 'foo.bar',
                'currentSize' => strlen($chunk1),
                'currentChunk' => 1,
                'totalSize' => strlen($chunk1) + strlen($chunk2),
                'totalChunks' => 2,
                'crc32b' => hash('crc32b', $chunk1),
                'finalSha256' => $sha256,
            ]
        );
        $queryParamsChunk2 = http_build_query(
            [
                'filename' => 'foo.bar',
                'currentSize' => strlen($chunk2),
                'currentChunk' => 2,
                'totalSize' => strlen($chunk1) + strlen($chunk2),
                'totalChunks' => 2,
                'crc32b' => hash('crc32b', $chunk2),
                'finalSha256' => $sha256,
            ]
        );

        $this->post(
            '/api/transfers/prepare-chunked/' . md5(
                'test'
            ) . '?' . $queryParamsChunk1,
            $chunk1
        );
        $this->assertResponseCode(206); // envoi partiel (manque des chunks)

        $this->post(
            '/api/transfers/prepare-chunked/' . md5(
                'test'
            ) . '?' . $queryParamsChunk1,
            $chunk1
        );
        $this->assertResponseCode(204); // no content (chunk existe déjà)

        $this->post(
            '/api/transfers/prepare-chunked/' . md5(
                'test'
            ) . '?' . $queryParamsChunk2,
            $chunk2
        );
        $this->assertResponseCode(201); // created (terminé)

        // chunks dans le désordre
        $chunks = ['foo', 'bar', 'baz', 'biz'];
        $sha256 = hash('sha256', implode('', $chunks));
        $totalSize = strlen(implode('', $chunks));
        $queryParams = [];
        foreach ($chunks as $i => $chunk) {
            $queryParams[] = http_build_query(
                [
                    'filename' => 'foo.bar',
                    'currentSize' => strlen($chunk),
                    'currentChunk' => $i + 1,
                    'totalSize' => $totalSize,
                    'totalChunks' => count($chunks),
                    'crc32b' => hash('crc32b', $chunk),
                    'finalSha256' => $sha256,
                ]
            );
        }
        $this->post(
            '/api/transfers/prepare-chunked/' . md5(
                'test2'
            ) . '?' . $queryParams[0],
            $chunks[0]
        );
        $this->assertResponseCode(206);
        $this->post(
            '/api/transfers/prepare-chunked/' . md5(
                'test2'
            ) . '?' . $queryParams[1],
            $chunks[1]
        );
        $this->assertResponseCode(206);
        $this->post(
            '/api/transfers/prepare-chunked/' . md5(
                'test2'
            ) . '?' . $queryParams[3],
            $chunks[3]
        );
        $this->assertResponseCode(206);
        $this->post(
            '/api/transfers/prepare-chunked/' . md5(
                'test2'
            ) . '?' . $queryParams[2],
            $chunks[2]
        );
        $this->assertResponseCode(201);
    }

    /**
     * testhide
     */
    public function testhide()
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $transfer = $Transfers->find()->where(['Transfers.id' => 2])->contain(['CreatedUsers'])->firstOrFail();
        $this->assertFalse($transfer->get('hiddable'));
        $this->get('/transfers/hide/2');
        $this->assertResponseCode(404);

        $Transfers->updateAll(['created_user_id' => 2], ['id' => 2]); // user pastell
        $transfer = $Transfers->find()->where(['Transfers.id' => 2])->contain(['CreatedUsers'])->firstOrFail();
        $this->assertTrue($transfer->get('hiddable'));
        $this->get('/transfers/hide/2');
        $this->assertResponseCode(200);
        $this->assertTrue((bool)$Transfers->get(2)->get('hidden'));
    }
}
