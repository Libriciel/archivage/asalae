<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\ValidationActorsController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\ValidationActorsController Test Case
 */
#[UsesClass(ValidationActorsController::class)]
class ValidationActorsControllerTest extends TestCase
{
    use HttpTrait;
    use IntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,

        // Pour ce controller
        'app.EventLogs',
        'app.ValidationChains',
        'app.ValidationActors',
        'app.ValidationStages',
        'app.ValidationProcesses',
        'app.ValidationHistories',
        'app.RolesTypeEntities',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->httpGet('/validation-actors/index/1');
        $this->assertResponseCode(200);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $this->httpGet('/validation-actors/add/1');
        $this->assertResponseCode(200);

        $data = [
            'app_type' => 'USER',
        ];
        $this->post('/validation-actors/add/1', $data);
        $this->assertResponseCode(404, $this->extractResponseError());

        $data = [
            'app_type' => 'USER',
            'app_foreign_key' => 1,     // allready used by this validationActors
            'type_validation' => 'V',
        ];
        $this->httpPost('/validation-actors/add/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'app_type' => 'USER',
            'app_foreign_key' => 2,     // available
            'type_validation' => 'V',
        ];
        $this->httpPost('/validation-actors/add/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->httpGet('/validation-actors/edit/1');
        $this->assertResponseCode(200);

        $data = [
            'app_foreign_key' => 1,
            'type_validation' => 'S',
        ];
        $this->httpPut('/validation-actors/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $this->httpDelete('/validation-actors/delete/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testApiAdd
     */
    public function testApiAdd()
    {
        $loc = TableRegistry::getTableLocator();
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'ValidationActors'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $loc->get('ValidationActors')->deleteAll(['app_foreign_key' => 1]);
        $data = [
            'validation_stage_id' => 1,
            'app_type' => 'USER',
            'app_foreign_key' => 1,
            'type_validation' => 'V',
        ];
        $json = $this->httpPost('/api/validation-actors', $data, true);
        $this->assertResponseCode(200);
        $this->assertIsArray($json);
        $this->assertArrayHasKey('success', $json);
        $this->assertTrue($json['success']);
        $this->assertArrayHasKey('id', $json);
    }
}
