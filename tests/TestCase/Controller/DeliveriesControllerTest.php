<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\DeliveryRequestsController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;
use stdClass;

/**
 * Asalae\Controller\DeliveriesController Test Case
 *
 */
#[UsesClass(DeliveryRequestsController::class)]
class DeliveriesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveUnits',
        'app.ArchiveUnitsDeliveryRequests',
        'app.Archives',
        'app.Deliveries',
        'app.DeliveryRequests',
        'app.EventLogs',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        $loc = TableRegistry::getTableLocator();
        $DeliveryRequests = $loc->get('DeliveryRequests');
        $dR = $DeliveryRequests->newEntity(
            [
                'identifier' => 'test_001',
                'archival_agency_id' => 2,
                'requester_id' => 2,
                'state' => 'creating',
                'comment' => 'Lorem ipsum dolor sit amet',
                'derogation' => false,
                'created_user_id' => 1,
                'created' => 1587740475,
                'modified' => 1587740475,
                'last_state_update' => null,
                'states_history' => null,
                'filename' => 'SA_ADLR_1_delivery_request.xml',
                'validation_chain_id' => null,
                'archive_units_count' => 1,
                'original_count' => 3,
                'original_size' => 12,
            ]
        );
        $DeliveryRequests->saveOrFail($dR);
        $Deliveries = $loc->get('Deliveries');
        $d = $Deliveries->newEntity(
            [
                'identifier' => 'Lorem ipsum dolor sit amet',
                'delivery_request_id' => $dR->id,
                'state' => 'Lorem ipsum dolor sit amet',
                'comment' => 'Lorem ipsum dolor sit amet',
                'archive_units_count' => 1,
                'original_count' => 1,
                'original_size' => 1,
            ]
        );
        $Deliveries->saveOrFail($d);
    }

    /**
     * testRetrieval
     */
    public function testRetrieval()
    {
        TableRegistry::getTableLocator()->get('Deliveries')
            ->updateAll(['state' => 'available'], ['id' => 1]);
        $response = $this->httpGet('/deliveries/retrieval');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testAcquittal
     */
    public function testAcquittal()
    {
        TableRegistry::getTableLocator()->get('Deliveries')
            ->updateAll(['state' => 'downloaded'], ['id' => 1]);
        $response = $this->httpGet('/deliveries/acquittal');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpGet('/deliveries/view/1');
        $this->assertResponseCode(200);
    }

    /**
     * testDownload
     */
    public function testDownload()
    {
        Configure::write('App.paths.data', TMP_TESTDIR);
        $zipPath = TMP_TESTDIR . DS . 'deliveries' . DS . 'Lorem+ipsum+dolor+sit+amet_delivery.zip';
        Filesystem::dumpFile($zipPath, hex2bin('504B0304'));
        $this->get('/deliveries/download/1');
        $this->assertResponseCode(200);
        $this->assertHeaderContains('Content-Type', 'application/zip');
    }

    /**
     * testAcquit
     */
    public function testAcquit()
    {
        Configure::write('App.paths.data', TMP_TESTDIR);
        $zipPath = TMP_TESTDIR . DS . 'deliveries' . DS . 'Lorem+ipsum+dolor+sit+amet_delivery.zip';
        Filesystem::dumpFile($zipPath, hex2bin('504B0304'));

        $Deliveries = TableRegistry::getTableLocator()->get('Deliveries');
        $Deliveries->updateAll(['state' => 'downloaded'], ['id' => 1]);
        $this->httpDelete('/deliveries/acquit/1');
        $this->assertResponseCode(200);
        $this->assertFalse($Deliveries->exists(['id' => 1]));
    }
}
