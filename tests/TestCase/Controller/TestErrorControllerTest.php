<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\TestErrorController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use BadFunctionCallException;
use Cake\Http\ServerRequest;
use Cake\TestSuite\IntegrationTestTrait;
use Exception;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\TestErrorController Test Case
 */
#[UsesClass(TestErrorController::class)]
class TestErrorControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,
    ];
    private $debugFixtures = false;

    /**
     * testCode
     */
    public function testCode()
    {
        $controller = new TestErrorController(new ServerRequest());
        $codes = [400, 401, 403, 404, 405, 406, 409, 410, 451, 501, 503, 666];
        // On evite on mode "integration" car extremement gourmand en ressource (gestion d'erreur)
        foreach ($codes as $code) {
            try {
                $controller->code($code);
            } catch (Exception $e) {
                $this->assertEquals($code, $e->getCode());
            }
        }
    }

    /**
     * testThrowException
     */
    public function testThrowException()
    {
        $this->get('/test-error/throw-exception/' . BadFunctionCallException::class);
        $this->assertResponseCode(500);
    }
}
