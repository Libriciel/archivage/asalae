<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\RestservicesController;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Php;
use AsalaeCore\Utility\XmlUtility;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Datacompressor\Utility\DataCompressor;
use DOMDocument;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\RestservicesController Test Case
 */
#[UsesClass(RestservicesController::class)]
class RestservicesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public const string SEDA_02_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xml';
    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';
    public const string SEDA_02_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02_description.xml';
    public const string SEDA_10_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        'app.AccessRules',
        'app.Agreements',
        'app.AppraisalRules',
        'app.Archives',
        'app.BeanstalkJobs',
        'app.Fileuploads',
        'app.OrgEntitiesTimestampers',
        'app.Profiles',
        'app.SecureDataSpaces',
        'app.Sequences',
        'app.ServiceLevels',
        'app.ServiceLevels',
        'app.Siegfrieds',
        'app.StoredFiles',
        'app.StoredFilesVolumes',
        'app.TransferAttachments',
        'app.TypeEntities',
    ];
    public $files;
    public $server;
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->files = $_FILES;
        $this->server = $_SERVER;
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_FILES = [];

        Filesystem::reset();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }
        Filesystem::begin();
        Filesystem::mkdir($dir);
        Configure::write('App.paths.data', $dir);
        Configure::write('Beanstalk.disable_check_ttr', true);
        Utility::reset();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Filesystem::rollback();
        Filesystem::setNamespace();
        Filesystem::reset();
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }
        Utility::reset();
        $_FILES = $this->files;
        $_SERVER = $this->server;
    }

    /**
     * testPing
     */
    public function testPing()
    {
        $this->httpGet('/restservices/ping');
        $this->assertResponseCode(200);
        $this->assertResponseContains('webservices asalae accessibles');
    }

    /**
     * testVersions
     */
    public function testVersions()
    {
        $this->get('/restservices/versions');
        $this->assertResponseCode(401);

        Configure::write('App.name', 'foo@bar');
        $this->session($this->genericSessionData);
        $response = $this->httpGet('/restservices/versions', true);
        $this->assertResponseCode(200);

        $expected = [
            'application' => 'foo@bar',
            'denomination' => 'Mon Service d&#039;archive',
            'version' => ASALAE_VERSION_LONG,
        ];
        $this->assertEquals($expected, $response);
    }

    /**
     * testSedaMessages
     */
    public function testSedaMessages()
    {
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        $Transfers->updateAll(
            ['transfer_identifier' => uniqid()],
            ['transfer_identifier' => 'atr_alea_70f90eacdb2d3b53ad7e87f808a82430']
        );
        $OrgEntities = $loc->get('OrgEntities');
        $OrgEntities->updateAll(['identifier' => 'sv1'], ['identifier' => 'sv']);

        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'controllers', 'alias' => 'Restservices'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);

        $this->session($this->genericSessionData);
        Filesystem::copy(self::SEDA_10_XML, $xml = TMP_TESTDIR . DS . 'msg.xml');
        DataCompressor::compress($xml, $zip = TMP_TESTDIR . DS . 'msg.zip');
        $_FILES = [
            'seda_message' => [
                'type' => 'application/xml',
                'error' => UPLOAD_ERR_OK,
                'tmp_name' => $xml,
                'name' => 'msg.xml',
                'size' => filesize($xml),
            ],
            'attachments' => [
                'type' => 'application/zip',
                'error' => UPLOAD_ERR_OK,
                'tmp_name' => $zip,
                'name' => 'msg.zip',
                'size' => filesize($zip),
            ],
        ];
        $this->_request['files'] = $_FILES;
        $mock = $this->createMock(Php::class);
        $mock->method('__call')->willReturnCallback(
            function ($name, $args) {
                if ($name === 'move_uploaded_file') {
                    Filesystem::rename($args[0], $args[1]);// equivalent move_uploaded_file()
                    return true;
                }
            }
        );
        Utility::set('Php', $mock);
        $response = $this->httpPost('/restservices/seda-messages', [], true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('date_du_depot', $response);
    }

    /**
     * testSedaAttachmentsChunkFiles
     */
    public function testSedaAttachmentsChunkFiles()
    {
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        $Transfers->updateAll(
            ['transfer_identifier' => uniqid()],
            ['transfer_identifier' => 'atr_alea_70f90eacdb2d3b53ad7e87f808a82430']
        );
        $OrgEntities = $loc->get('OrgEntities');
        $OrgEntities->updateAll(['identifier' => 'sv1'], ['identifier' => 'sv']);

        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'controllers', 'alias' => 'Restservices'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);

        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $TransferAttachments->deleteAll([]);

        // Create files
        $filename1 = TMP_TESTDIR . DS . 'testunit-to-split';
        Filesystem::createDummyFile($filename1, 9999999);//~9.54Mo
        $md5 = md5_file($filename1);
        $filename2 = TMP_TESTDIR . DS . 'testunit-to-split.zip';
        DataCompressor::compress($filename1, $filename2);

        // préparation du transfert
        $this->session($this->genericSessionData);
        Filesystem::copy(self::SEDA_10_XML, $xml = TMP_TESTDIR . DS . 'msg.xml');
        $data = [
            'seda_message' => [
                'type' => 'application/xml',
                'error' => UPLOAD_ERR_OK,
                'tmp_name' => $xml,
                'name' => 'msg.xml',
                'size' => filesize($xml),
            ],
            'send_chunked_attachments' => '1',
        ];
        $_FILES = [
            'seda_message' => [
                'type' => 'application/xml',
                'error' => UPLOAD_ERR_OK,
                'tmp_name' => $xml,
                'name' => 'msg.xml',
                'size' => filesize($xml),
            ],
        ];
        $this->_request['files'] = $_FILES;
        $mock = $this->createMock(Php::class);
        $mock->method('__call')->willReturnCallback(
            function ($name, $args) {
                if ($name === 'move_uploaded_file') {
                    Filesystem::rename($args[0], $args[1]);// equivalent move_uploaded_file()
                    return true;
                }
            }
        );
        Utility::set('Php', $mock);
        $response = $this->httpPost('/restservices/seda-messages', $data, true);
        $this->assertResponseCode(200);
        $baseData = [
            'session_identifier' => Hash::get($response, 'chunk_session_identifier'),
            'security_identifier' => Hash::get($response, 'chunk_security_identifier'),
            'number_of_files' => 3,
            'file_index' => 1,
            'file_name' => 'foo/testunit-to-split',
            'compression_algorithm' => 'NONE',
            'number_of_chunks' => 5,// 1000000 / 2000000
        ];

        // envoi des chunks
        $fh = fopen($filename1, 'r');
        $i = 0;
        while (!feof($fh)) {
            $data = [
                'chunk_index' => $i++,
                'chunk_content' => fread($fh, 2000000),//~1.91Mo
            ] + $baseData;
            $response = $this->httpPost('/restservices/sedaAttachmentsChunkFiles', $data, true);
            $this->assertResponseCode(200);
            $this->assertResponseContains('date_du_depot');
        }
        fclose($fh);
        unlink($filename1);
        $this->assertFalse(Hash::get($response, 'all_files_uploaded'));

        // envoi du zip
        $data = [
            'file_index' => 2,
            'file_name' => 'bar/testunit-to-split.zip',
            'compression_algorithm' => 'ZIP',
            'number_of_chunks' => 1,
            'chunk_index' => 1,
            'chunk_content' => file_get_contents($filename2),
        ] + $baseData;
        $response = $this->httpPost('/restservices/sedaAttachmentsChunkFiles', $data, true);
        $this->assertResponseCode(200);
        $this->assertResponseContains('date_du_depot');
        $this->assertFalse(Hash::get($response, 'all_files_uploaded'));

        // envoi d'un chunk sous forme de fichier
        Filesystem::copy(self::SEDA_10_XML, $xml);
        $data = [
            'file_index' => 3,
            'file_name' => 'baz/msg.xml',
            'compression_algorithm' => 'NONE',
            'number_of_chunks' => 1,
            'chunk_index' => 1,
            'chunk_content' => [
                'type' => 'application/xml',
                'error' => UPLOAD_ERR_OK,
                'tmp_name' => $xml,
                'name' => 'msg.xml',
                'size' => filesize($xml),
            ],
        ] + $baseData;
        // erreur sur CI: "rename(): The first argument to copy() function cannot be a directory"
        // provoqué sur la fonction php "rename()", qui accepte les dossiers.
        // l'erreur est différente si on a pas les perms ou si le dossier n'existe pas
        $this->errorToException = false;
        $response = $this->httpPost('/restservices/sedaAttachmentsChunkFiles', $data, true);
        $this->errorToException = true;
        $this->assertResponseCode(200);
        $this->assertResponseContains('date_du_depot');
        $this->assertTrue(Hash::get($response, 'all_files_uploaded'));

        $attachment = $TransferAttachments->find()->where(['filename' => 'foo/testunit-to-split'])->first();
        $this->assertNotNull($attachment);
        $this->assertEquals($md5, md5_file($attachment->get('path')));
        $attachment = $TransferAttachments->find()->where(['filename' => 'testunit-to-split'])->first();// dézippé
        $this->assertNotNull($attachment);
        $this->assertEquals($md5, md5_file($attachment->get('path')));
        $this->assertEquals(3, $TransferAttachments->find()->count());
    }

    /**
     * testSedaAttachmentsChunkFilesDelete
     */
    public function testSedaAttachmentsChunkFilesDelete()
    {
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        $Transfers->updateAll(
            ['transfer_identifier' => uniqid()],
            ['transfer_identifier' => 'atr_alea_70f90eacdb2d3b53ad7e87f808a82430']
        );
        $OrgEntities = $loc->get('OrgEntities');
        $OrgEntities->updateAll(['identifier' => 'sv1'], ['identifier' => 'sv']);

        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'controllers', 'alias' => 'Restservices'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        // Create files
        $filename1 = TMP_TESTDIR . DS . 'testunit-to-split';
        Filesystem::createDummyFile($filename1, 9999999);//~9.54Mo

        // préparation du transfert
        $this->session($this->genericSessionData);
        Filesystem::copy(self::SEDA_10_XML, $xml = TMP_TESTDIR . DS . 'msg.xml');
        $data = [
            'send_chunked_attachments' => '1',
        ];
        $_FILES = [
            'seda_message' => [
                'type' => 'application/xml',
                'error' => UPLOAD_ERR_OK,
                'tmp_name' => $xml,
                'name' => 'msg.xml',
                'size' => filesize($xml),
            ],
        ];
        $this->_request['files'] = $_FILES;
        $mock = $this->createMock(Php::class);
        $mock->method('__call')->willReturnCallback(
            function ($name, $args) {
                if ($name === 'move_uploaded_file') {
                    Filesystem::rename($args[0], $args[1]);// equivalent move_uploaded_file()
                    return true;
                }
            }
        );
        Utility::set('Php', $mock);
        $response = $this->httpPost('/restservices/seda-messages', $data, true);
        $this->assertResponseCode(200);
        $baseData = [
            'session_identifier' => Hash::get($response, 'chunk_session_identifier'),
            'security_identifier' => Hash::get($response, 'chunk_security_identifier'),
            'number_of_files' => 3,
            'file_index' => 1,
            'file_name' => 'foo/testunit-to-split',
            'compression_algorithm' => 'NONE',
            'number_of_chunks' => 5,// 1000000 / 2000000
        ];

        // envoi d'un seul chunks
        $fh = fopen($filename1, 'r');
        $data = [
            'chunk_index' => 0,
            'chunk_content' => fread($fh, 2000000),//~1.91Mo
        ] + $baseData;
        $response = $this->httpPost('/restservices/sedaAttachmentsChunkFiles', $data, true);
        $this->assertResponseCode(200);
        $this->assertResponseContains('date_du_depot');

        fclose($fh);
        unlink($filename1);
        $this->assertFalse(Hash::get($response, 'all_files_uploaded'));

        // delete session transfert
        Filesystem::setNamespace('test-unit');// on garde les fichiers construits
        $this->httpDelete('/restservices/sedaAttachmentsChunkFiles', $data, true);
        Filesystem::rollback();// on rollback la suppression
        Filesystem::setNamespace(); // on rebascule sur default pour le nettoyage des fichiers
        $this->assertResponseCode(200);
        $this->assertResponseContains('date_de_suppression');
    }

    /**
     * testGetSedaMessages
     */
    public function testGetSedaMessages()
    {
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        $Transfers->updateAll(
            ['transferring_agency_id' => 4],
            []
        );
        $OrgEntities = $loc->get('OrgEntities');
        $OrgEntities->updateAll(['identifier' => 'sv1'], ['identifier' => 'sv']);

        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'controllers', 'alias' => 'Restservices'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $Archives = $loc->get('Archives');
        $Archives->updateAll(['state' => 'available'], []);

        // Préparation volumes
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);
        Filesystem::dumpFile(
            TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            file_get_contents(self::SEDA_10_DESCRIPTION)
        );
        Filesystem::dumpFile(
            TMP_VOL2 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            file_get_contents(self::SEDA_10_DESCRIPTION)
        );
        Filesystem::dumpFile(
            TMP_VOL1 . '/sa/no_profile/2021-12/sa_2/management_data/sa_2_description.xmldescription.xml',
            file_get_contents(self::SEDA_02_DESCRIPTION)
        );
        Filesystem::dumpFile(
            TMP_VOL2 . '/sa/no_profile/2021-12/sa_2/management_data/sa_2_description.xmldescription.xml',
            file_get_contents(self::SEDA_02_DESCRIPTION)
        );

        $this->session($this->genericSessionData);

        $dir = TMP_TESTDIR;
        $transferXml = $dir . DS . 'transfers' . DS . '1' . DS . 'message' . DS . 'testfile.xml';
        Filesystem::copy(self::SEDA_02_XML, $transferXml);
        $transferXml = $dir . DS . 'transfers' . DS . '2' . DS . 'message' . DS . 'testfile.xml';
        Filesystem::copy(self::SEDA_10_XML, $transferXml);

        // Type Acknowledgement
        $params = [
            'sequence=ArchiveTransfer',
            'message=Acknowledgement',
            'originOrganizationIdentification=sv1', // OrgEntity.id = 4
            'originMessageIdentifier=atr_alea_70f90eacdb2d3b53ad7e87f808a82430', // Transfer.id = 8
        ];

        // seda 0.2
        $this->configRequest(['headers' => ['Accept' => 'application/xml']]);
        $this->get('/restservices/sedaMessages?' . implode('&', $params));
        $this->assertResponseCode(200);
        $this->assertResponseContains('ArchiveTransferReply');
        $dom = new DOMDocument();
        $dom->loadXML((string)$this->_response->getBody());
        $this->assertTrue(XmlUtility::validateXmlString((string)$this->_response->getBody(), SEDA_V02_XSD, true));

        // seda 1.0
        $params = [
            'sequence=ArchiveTransfer',
            'message=Acknowledgement',
            'originOrganizationIdentification=sv1', // OrgEntity.id = 4
            'originMessageIdentifier=seda1.0', // Transfer.id = 1
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/xml']]);
        $this->get('/restservices/sedaMessages?' . implode('&', $params));
        $this->assertResponseCode(200);

        $this->assertResponseContains('Acknowledgement');
        $dom = new DOMDocument();
        $dom->loadXML((string)$this->_response->getBody());
        $this->assertTrue(XmlUtility::validateXmlString((string)$this->_response->getBody(), SEDA_V10_XSD, true));

        // param manquant
        array_shift($params);
        $this->get('/restservices/sedaMessages?' . implode('&', $params));
        $this->assertResponseCode(400);

        // mauvais param
        $params[] = 'sequence=Foo';
        $this->get('/restservices/sedaMessages?' . implode('&', $params));
        $this->assertResponseCode(400);
    }

    /**
     * testGetSedaMessagesWithoutArchive
     */
    public function testGetSedaMessagesWithoutArchive()
    {
        $loc = TableRegistry::getTableLocator();

        $loc->get('ArchiveFiles')->deleteAll([]);
        $Archives = $loc->get('Archives');
        $Archives->deleteAll([]);

        $Transfers = $loc->get('Transfers');
        $Transfers->updateAll(
            ['transferring_agency_id' => 4],
            []
        );
        $OrgEntities = $loc->get('OrgEntities');
        $OrgEntities->updateAll(['identifier' => 'sv1'], ['identifier' => 'sv']);

        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'controllers', 'alias' => 'Restservices'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        // Préparation volumes
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);
        Filesystem::dumpFile(
            TMP_VOL1 . DS . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            file_get_contents(self::SEDA_10_DESCRIPTION)
        );
        Filesystem::dumpFile(
            TMP_VOL2 . DS . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            file_get_contents(self::SEDA_10_DESCRIPTION)
        );
        Filesystem::dumpFile(
            TMP_VOL1 . DS . '/sa/no_profile/2021-12/sa_2/management_data/sa_2_description.xmldescription.xml',
            file_get_contents(self::SEDA_02_DESCRIPTION)
        );
        Filesystem::dumpFile(
            TMP_VOL2 . DS . '/sa/no_profile/2021-12/sa_2/management_data/sa_2_description.xmldescription.xml',
            file_get_contents(self::SEDA_02_DESCRIPTION)
        );

        $this->session($this->genericSessionData);

        $dir = TMP_TESTDIR;
        $transferXml = $dir . DS . 'transfers' . DS . '1' . DS . 'message' . DS . 'testfile.xml';
        Filesystem::copy(self::SEDA_02_XML, $transferXml);
        $transferXml = $dir . DS . 'transfers' . DS . '2' . DS . 'message' . DS . 'testfile.xml';
        Filesystem::copy(self::SEDA_10_XML, $transferXml);

        // refused seda 0.2
        $params = [
            'sequence=ArchiveTransfer',
            'message=Acknowledgement',
            'originOrganizationIdentification=sv1', // OrgEntity.id = 4
            'originMessageIdentifier=atr_alea_70f90eacdb2d3b53ad7e87f808a82430', // Transfer.id = 8
        ];
        $Transfers->updateAll(['is_accepted' => false], []);
        $this->configRequest(['headers' => ['Accept' => 'application/xml']]);
        $this->get('/restservices/sedaMessages?' . implode('&', $params));
        $this->assertResponseCode(200);
        $this->assertResponseContains('ArchiveTransferReply');
        $dom = new DOMDocument();
        $dom->loadXML((string)$this->_response->getBody());
        $this->assertTrue(XmlUtility::validateXmlString((string)$this->_response->getBody(), SEDA_V02_XSD, true));

        // refused seda 1.0
        $params = [
            'sequence=ArchiveTransfer',
            'message=Reply',
            'originOrganizationIdentification=sv1', // OrgEntity.id = 2
            'originMessageIdentifier=seda1.0', // Transfer.id = 1
        ];
        $Transfers->updateAll(
            [
                'is_accepted' => false,
                'state' => 'refused',
            ],
            []
        );
        $this->configRequest(['headers' => ['Accept' => 'application/xml']]);
        $this->get('/restservices/sedaMessages?' . implode('&', $params));
        $this->assertResponseCode(200);
        $this->assertResponseContains('ArchiveTransferReply');
        $dom = new DOMDocument();
        $dom->loadXML((string)$this->_response->getBody());
        $this->assertTrue(XmlUtility::validateXmlString((string)$this->_response->getBody(), SEDA_V10_XSD, true));
    }
}
