<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\ArchiveKeywordsController;
use Asalae\Model\Volume\VolumeManager;
use Asalae\Test\Mock\MockExecVolumeManager;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use DateTime;
use DOMDocument;
use DOMElement;
use Exception;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\ArchiveKeywordsController Test Case
 */
#[UsesClass(ArchiveKeywordsController::class)]
class ArchiveKeywordsControllerTest extends TestCase
{
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use IntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        FIXTURES_VOLUMES,
        'app.ArchiveDescriptions',
        'app.AccessRules',
        'app.AccessRuleCodes',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        VolumeSample::init();
        VolumeSample::insertSample(
            'sample_seda10_description.xml',
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
        );
        VolumeSample::insertSample(
            'sample_premis.xml',
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
        );
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('Archives.updateLifecycle.path', TMP_TESTDIR);
        Configure::write('Archives.updateDescription.path', TMP_TESTDIR);
        Configure::write('Archives.updateLifecycle.cache_duration', 0);
        Configure::write('Archives.updateDescription.cache_duration', 0);
        Utility::set('Exec', MockExecVolumeManager::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Utility::reset();
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $archiveKeyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => 1,
                'content' => 'test',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $ArchiveKeywords->saveOrFail($archiveKeyword);
        $this->httpGet('/archive-keywords/add/' . $archiveKeyword->id);
        $this->assertResponseCode(200);

        $data = [
            'content' => 'foo',
            'reference' => 'bar',
            'reference_schemeID' => 'baz',
            'type' => 'corpname',
            'use_parent_access_rule' => false,
            'access_rule' => [
                'access_rule_code_id' => 1,
                'start_date' => '15/04/2019',
            ],
        ];
        $this->httpPost('/archive-keywords/add/' . $archiveKeyword->id, $data);
        $this->assertHeader('X-Asalae-Success', 'true');
        $manager = new VolumeManager(1);
        $dom = new DOMDocument();
        $dom->loadXML($manager->fileGetContent('sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'));
        $found = false;
        /** @var DOMElement $node */
        $node = null;
        foreach ($dom->getElementsByTagName('KeywordContent') as $node) {
            if (!$node instanceof DOMElement) {
                continue;
            }
            if ($node->nodeValue === 'foo') {
                $found = true;
                break;
            }
        }
        $this->assertTrue($found);
        /** @var DOMElement $parentNode */
        $parentNode = $node->parentNode;
        $ref = $parentNode->getElementsByTagName('KeywordReference')->item(0);
        $this->assertInstanceOf(DOMElement::class, $ref);
        if (!$ref instanceof DOMElement) {
            throw new Exception();
        }
        $this->assertEquals('bar', $ref->nodeValue);
        $schemeId = $ref->getAttribute('schemeID');
        $this->assertEquals('baz', $schemeId);
        $type = $parentNode->getElementsByTagName('KeywordType')->item(0);
        $this->assertInstanceOf(DOMElement::class, $type);
        $this->assertEquals('corpname', $type->nodeValue);
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $archiveKeyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => 2,
                'content' => 'test',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $ArchiveKeywords->saveOrFail($archiveKeyword);

        $this->get('/archive-keywords/edit/0');
        $this->assertResponseCode(404);

        $this->httpGet('/archive-keywords/edit/' . $archiveKeyword->id);
        $this->assertResponseCode(200);

        $this->httpPut('/archive-keywords/edit/' . $archiveKeyword->id, ['content' => $uid = uniqid()]);
        $this->assertAsalaeSuccess();
        $manager = new VolumeManager(1);
        $dom = new DOMDocument();
        $dom->loadXML($manager->fileGetContent('sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'));
        $found = false;
        foreach ($dom->getElementsByTagName('KeywordContent') as $node) {
            if ($node instanceof DOMElement && $node->nodeValue === $uid) {
                $found = true;
                break;
            }
        }
        $this->assertTrue($found);
        $this->assertEquals($uid, $ArchiveKeywords->get(1)->get('content'));

        $data = [
            'use_parent_access_rule' => false,
            'access_rule' => [
                'access_rule_code_id' => 1,
                'start_date' => '15/04/2019',
            ],
        ];
        $this->httpPut('/archive-keywords/edit/' . $archiveKeyword->id, $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $AccessRules = $loc->get('AccessRules');
        $this->assertEquals(1, $AccessRules->find()->where(['start_date' => '2019-04-15'])->count());

        $data = [
            'use_parent_access_rule' => false,
            'access_rule' => [
                'access_rule_code_id' => 1,
                'start_date' => '16/04/2019',
            ],
        ];
        $this->httpPut('/archive-keywords/edit/' . $archiveKeyword->id, $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertEquals(0, $AccessRules->find()->where(['start_date' => '2019-04-15'])->count());
        $this->assertEquals(1, $AccessRules->find()->where(['start_date' => '2019-04-16'])->count());
        $this->httpPut('/archive-keywords/edit/' . $archiveKeyword->id, ['use_parent_access_rule' => true]);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertEquals(0, $AccessRules->find()->where(['start_date' => '2019-04-16'])->count());
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $archiveKeyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => 1,
                'content' => 'test',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $ArchiveKeywords->saveOrFail($archiveKeyword);

        $this->get('/archive-keywords/delete/' . $archiveKeyword->id);
        $this->assertResponseCode(405);

        $response = $this->httpDelete('/archive-keywords/delete/' . $archiveKeyword->id, [], true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('report', $response);
        $this->assertEquals('done', $response['report']);
        $manager = new VolumeManager(1);
        $dom = new DOMDocument();
        $dom->loadXML($manager->fileGetContent('sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'));
        $found = false;
        /** @var DOMElement $node */
        foreach ($dom->getElementsByTagName('KeywordContent') as $node) {
            if (!$node instanceof DOMElement) {
                continue;
            }
            if ($node->nodeValue === 'test') {
                $found = true;
                break;
            }
        }
        $this->assertFalse($found);

        // test suppression des règles liés
        $AccessRules = $loc->get('AccessRules');
        $rule = $AccessRules->newEntity(
            [
                'access_rule_code_id' => 1,
                'start_date' => new DateTime(),
                'end_date' => new DateTime(),
            ]
        );
        $this->assertNotFalse($AccessRules->save($rule));
        $keyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => $rule->id,
                'content' => 'test',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $this->assertNotFalse($ArchiveKeywords->save($keyword));
        $this->assertEquals(1, $rule->get('has_many_count'));
        $this->httpDelete('/archive-keywords/delete/' . $keyword->id);
        $this->assertResponseCode(200);
        $this->assertEquals(0, $rule->get('has_many_count'));
    }
}
