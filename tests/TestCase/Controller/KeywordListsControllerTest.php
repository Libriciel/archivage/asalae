<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\KeywordListsController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(KeywordListsController::class)]
class KeywordListsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        'app.Keywords',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);

        $loc = TableRegistry::getTableLocator();
        $KeywordLists = $loc->get('KeywordLists');
        $keywordList = $KeywordLists->newEntity(
            [
                'org_entity_id' => 2,
                'identifier' => 'test',
                'name' => 'Test',
                'description' => 'Liste de test',
                'active' => true,
                'version' => 0,
                'has_workspace' => true,
            ]
        );
        $KeywordLists->saveOrFail($keywordList);
        $Keywords = $loc->get('Keywords');
        $keyword = $KeywordLists->newEntity(
            [
                'keyword_list_id' => $keywordList->id,
                'version' => 0,
                'code' => 'code-1',
                'name' => 'Code 1',
                'app_meta' => '{"created_user_id":1}',
                'parent_id' => null,
            ],
            ['validate' => false]
        );
        $Keywords->saveOrFail($keyword);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keyword-lists/index');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet(
            '/keyword-lists/index?SaveFilterSelect=&name[0]=%3Fes*&identifier[0]=%3Fes*&active[0]=1&sort=identifier&direction=asc'
        );
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keyword-lists/index?SaveFilterSelect=&favoris[0]=1&sort=favorite&direction=desc');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertEmpty($response['data']);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keyword-lists/add');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $data = [
            'identifier' => 'test-add',
            'name' => 'Test add',
            'description' => "test d'un ajout de liste de mots-clés",
            'active' => true,
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/keyword-lists/add', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['id']);
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keyword-lists/edit/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $data = [
            'identifier' => 'test-edit',
            'name' => 'Test edit',
            'description' => "test d'édition d'une liste de mots-clés",
            'active' => true,
        ];
        $this->httpPut('/keyword-lists/edit/1', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['identifier']);
        $this->assertEquals('test-edit', $response['identifier']);
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpDelete('/keyword-lists/delete/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']);
        $this->assertEquals('done', $response['report']);
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keyword-lists/view/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);
    }

    /**
     * testNewVersion
     */
    public function testNewVersion()
    {
        $KeywordLists = TableRegistry::getTableLocator()->get('KeywordLists');
        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $KeywordLists->updateAll(['version' => 1], []);
        $Keywords->updateAll(['version' => 1], []);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/keyword-lists/new-version/1');
        $this->assertRedirect('/Keywords/index/1/0');

        $keyword = $Keywords->find()->where(
            [
                'keyword_list_id' => 1,
                'version' => 0,
            ]
        )->first();
        $this->assertTrue((bool)$keyword);
    }

    /**
     * testPublishVersion
     */
    public function testPublishVersion()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/keyword-lists/publish-version/2');
        $this->assertRedirect('/KeywordLists/index');

        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $keyword = $Keywords->find()->where(
            [
                'keyword_list_id' => 2,
                'version' => 1,
            ]
        )->first();
        $this->assertTrue((bool)$keyword);
    }

    /**
     * testRemoveVersion
     */
    public function testRemoveVersion()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/keyword-lists/remove-version/2');
        $this->assertRedirect('/KeywordLists/index');

        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $keyword = $Keywords->find()->where(
            [
                'keyword_list_id' => 2,
            ]
        )->first();
        $this->assertFalse((bool)$keyword);
    }
}
