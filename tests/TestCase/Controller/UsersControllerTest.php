<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\Component\LoginComponent;
use Asalae\Controller\UsersController;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Test\Mock\StaticGenericUtility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use Cake\View\ViewBuilder;
use DateInterval;
use DateTime;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\UsersController Test Case
 */
#[UsesClass(UsersController::class)]
class UsersControllerTest extends TestCase
{
    use InvokePrivateTrait;
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        'app.AccessTokens',
        'app.AuthSubUrls',
        'app.AuthUrls',
        'app.BeanstalkJobs',
        'app.Configurations',
        'app.Fileuploads',
        'app.FileuploadsProfiles',
        'app.Killables',
        'app.Ldaps',
        'app.Mails',
        'app.RolesTypeEntities',
        'app.Sessions',
        'app.Siegfrieds',
        'app.ValidationActors',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Password.complexity', 1);

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
    }

    /**
     * testLogin
     */
    public function testLogin()
    {
        $this->get('/users/login');
        $this->assertResponseCode(200);

        $data = [
            'username' => 'admin',
            'password' => 'bad-password',
        ];
        $this->httpPost('/users/login', $data);
        $this->assertResponseCode(200);

        LoginComponent::$attempts = [];
        $data = [
            'username' => 'admin',
            'password' => 'admin',
        ];
        $this->post('/users/login', $data);
        $this->assertRedirect('/');

        $this->logged();
        $this->get('/users/login');
        $this->assertRedirect('/');
    }

    /**
     * logged
     */
    private function logged()
    {
        $this->session($this->genericSessionData);
        Utility::reset();
    }

    /**
     * testLogout
     */
    public function testLogout()
    {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $Fileuploads->saveOrFail(
            $file = $Fileuploads->newEntity(
                [
                    'name' => 'testfile.txt',
                    'path' => TMP_TESTDIR . DS . 'testfile.txt',
                    'size' => 1,
                    'hash' => 'ABD1472D2DDB6BC98565B56CE59A5FB4C66D108EB9643E5E503C35C243EB69C9',
                    'hash_algo' => 'sha256',
                    'user_id' => 1,
                    'valid' => 1,
                    'state' => 'Lorem ipsum dolor sit amet',
                    'mime' => 'plain/text',
                    'locked' => 1,
                ]
            )
        );
        $file->set('path', TMP . 'testunit.tmpfile');
        $file->set('locked', false);
        $Fileuploads->save($file);
        file_put_contents($file->get('path'), 'Lorem ipsum dolor sit amet');

        $this->cookie('test', 'foo');
        $this->logged();
        $this->get('/users/logout');
        if (is_file($file->get('path'))) {
            unlink($file->get('path'));
            $this->fail("le fichier doit être supprimé au logout");
        }
        $this->assertRedirect('/');
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->logged();
        $this->configRequest(
            $req = [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );
        $this->httpGet('/users/edit');
        $this->assertResponseCode(200);

        $data = [
            'username' => 'test',
            'name' => 'foo bar',
            'email' => 'foo.bar@libriciel.fr',
            'high_contrast' => false,
            'password' => 'testunit',
            'confirm-password' => 'error',
            'is_validator' => true,
            'use_cert' => false,
        ];
        $this->configRequest($req);
        $this->put('/users/edit', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data['confirm-password'] = $data['password'];
        $this->configRequest($req);
        $this->put('/users/edit', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEditByAdmin
     */
    public function testEditByAdmin()
    {
        $this->logged();
        $this->configRequest(
            $req = [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );
        $this->get('/users/edit-by-admin/1');
        $this->assertResponseCode(200);

        $data = [
            'username' => 'test',
            'name' => 'foo bar',
            'email' => 'foo.bar@libriciel.fr',
            'high_contrast' => false,
            'role_id' => 1,
            'active' => false,
            'is_validator' => true,
            'use_cert' => false,
        ];
        $this->configRequest($req);
        $this->put('/users/edit-by-admin/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        $this->logged();
        $this->configRequest(
            $req = [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );
        $this->get('/users/add/2');
        $this->assertResponseCode(200);

        $this->configRequest($req);
        $this->post('/users/add/2');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'role_id' => 1,
            'username' => 'testunit2',
            'name' => 'testunit2 testunit2',
            'email' => 'testunit2@test.fr',
            'high_contrast' => false,
            'is_validator' => true,
            'use_cert' => false,
        ];
        $this->configRequest($req);
        $response = $this->httpPost('/users/add/2', $data, true);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertTrue(Hash::get($response, 'deletable'));
        $this->assertFalse(Hash::get($response, 'is_linked_to_ldap'));
    }

    /**
     * testLoadCookies
     */
    public function testLoadCookies()
    {
        $this->cookie('test', 'foo');
        $Users = TableRegistry::getTableLocator()->get('Users');
        $user = $Users->get(1);
        $user->set('cookies', json_encode(['test' => 'bar']));
        $Users->saveOrFail($user);

        $data = [
            'prev_archival_agency_id' => '2',
            'prev_username' => 'admin',
            'username' => 'admin',
            'password' => 'admin',
        ];
        $this->post('/users/ajax-login', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testAjaxLogin
     */
    public function testAjaxLogin()
    {
        $this->get('/users/ajax-login');
        $this->assertResponseCode(200);

        LoginComponent::$attempts = [];
        $data = [
            'prev_archival_agency_id' => '2',
            'prev_username' => 'testunit',
            'name' => 'testunit',
            'password' => 'badpassword',
        ];
        $this->post('/users/ajax-login', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        LoginComponent::$attempts = [];
        $data = [
            'prev_archival_agency_id' => '2',
            'prev_username' => 'admin',
            'username' => 'admin',
            'password' => 'admin',
        ];
        $this->post('/users/ajax-login', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testAjaxSaveMenu
     */
    public function testAjaxSaveMenu()
    {
        $this->logged();
        $Users = TableRegistry::getTableLocator()->get('Users');
        $menu = $Users->get(1)->get('menu');
        $data = [
            'menu' => json_encode(
                [
                    [
                        "href" => "#",
                        "class" => "navbar-link",
                        "@" => "Foo",
                        "@close" => true,
                    ],
                ]
            ),
        ];
        $this->configRequest(
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );
        $this->post('/users/ajax-save-menu', $data);
        $this->assertResponseCode(200);
        $this->assertNotEquals($menu, $Users->get(1)->get('menu'));
    }

    /**
     * testApiAdd
     */
    public function testApiAdd()
    {
        $loc = TableRegistry::getTableLocator();
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'api', 'alias' => 'Users'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $this->configRequest(
            [
                'headers' => [
                    'authorization' => 'Basic ' . base64_encode('admin:admin'),
                ],
            ]
        );
        $data = [
            'org_entity_id' => 2,
            'agent_type' => 'person',
            'role_id' => 1,
            'username' => 'api_test',
            'password' => null,
            'confirm-password' => null,
            'name' => 'api_test',
            'email' => 'api_test@test.fr',
            'high_contrast' => 'false',
            'active' => 'true',
            'is_validator' => 'true',
            'use_cert' => 'false',
            'ldap_id' => null,
            'ldap_login' => null,
        ];
        $this->post('/api/users', $data);
        $this->assertResponseCode(200);
        $this->assertTrue($loc->get('Users')->exists(['username' => 'api_test']));
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $loc = TableRegistry::getTableLocator();
        $Users = $loc->get('Users');
        $user = $Users->newEntity(
            [
                'id' => '',
                'username' => uniqid('test-'),
            ] + $Users->get(1)->toArray(),
            ['validate' => false]
        );
        $this->assertNotFalse((bool)$Users->save($user));
        $this->logged();
        $this->httpDelete('/users/delete/' . $user->id);
        $response = json_decode((string)$this->_response);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response->report);
        $this->assertEquals('done', $response->report);
    }

    /**
     * testResetSession
     */
    public function testResetSession()
    {
        $this->logged();
        $this->configRequest(
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );
        StaticGenericUtility::$methods['doReset'] = true;
        Utility::set('Session', StaticGenericUtility::class);
        $message = base64_encode(
            Security::encrypt(
                json_encode(
                    [
                        'id' => 1, // user_id
                        'key' => $key = hash('sha256', 'admin'),
                        'expire' => (new DateTime())->add(new DateInterval('P1Y')),
                    ]
                ),
                hash('sha256', 'admin')
            )
        );
        $data = [
            'message' => $message,
        ];
        $this->post('/users/reset-session', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testInitializePassword
     */
    public function testInitializePassword()
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        $code1 = $AuthUrls->newEntity(['url' => '/users/initialize-password/1']);
        $code2 = $AuthUrls->newEntity(['url' => '/users/forgotten-password/1']);
        $initialCount = $AuthUrls->find()->count();
        $AuthUrls->saveOrFail($code1);
        $AuthUrls->saveOrFail($code2);
        $data = [
            'password' => 'admin',
            'confirm-password' => 'admin',
        ];

        // syntaxe url incorrecte
        $this->session(
            [
                'Auth' => [
                    'code' => $code2->get('code') . 'a',
                    'url' => '/users/badurl',
                ],
            ]
        );
        $this->put('/users/initialize-password/1', $data);
        $this->assertResponseContains(
            __(
                "La page demandée n'est pas/plus valide. La durée d'accès peut être dépassée ou le traitement déjà effectué."
            )
        );

        // get
        $this->session(
            [
                'Auth' => [
                    'code' => $code1->get('code'),
                    'url' => $code1->get('url'),
                ],
            ]
        );
        $this->get('/users/initialize-password/1');
        $this->assertResponseCode(200);

        // Réussite
        $this->put('/users/initialize-password/1', $data);
        $this->assertRedirect('/');
        // il reste $code2
        $this->assertCount($initialCount + 1, $AuthUrls->find());

        $user = TableRegistry::getTableLocator()->get('Users')->get(1);

        $log = TableRegistry::getTableLocator()->get('EventLogs')
            ->find()
            ->orderBy(['id' => 'desc'])
            ->first();
        $this->assertTextContains(
            __(
                "Réinitialisation du mot de passe de L'utilisateur ''{0}''",
                $user->get('username')
            ),
            $log->get('outcome_detail')
        );
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->logged();
        $this->httpGet('/users/index');
        $this->assertResponseCode(200);
    }

    /**
     * testSetAvailablesRoles
     */
    public function testSetAvailablesRoles()
    {
        $UsersController = new UsersController(new ServerRequest());
        $this->invokeMethod($UsersController, 'setAvailablesRoles', [null]);
        $this->assertEquals([], $UsersController->immutableViewVars['roles']);
    }

    /**
     * testForgottenPassword
     */
    public function testForgottenPassword()
    {
        $loc = TableRegistry::getTableLocator();
        $AuthUrls = $loc->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        $viewBuilder = $this->createMock(ViewBuilder::class);
        $viewBuilder->method('setTemplate')->willReturnSelf();
        $viewBuilder->method('setLayout')->willReturnSelf();

        $this->httpGet('/users/forgottenPassword');
        $this->assertResponseCode(200);
    }

    /**
     * testRefuseUser
     */
    public function testRefuseUser()
    {
        $this->session($this->genericSessionData);

        $ChangeEntityRequests = TableRegistry::getTableLocator()->get('ChangeEntityRequests');
        $ChangeEntityRequests->saveOrFail(
            $request = $ChangeEntityRequests->newEntity(
                [
                    'base_archival_agency_id' => 3,
                    'target_archival_agency_id' => 2,
                    'user_id' => 4,
                    'reason' => 'foo',
                ]
            )
        );

        $this->httpDelete('/users/refuseUser/4');
        $this->assertFalse($ChangeEntityRequests->exists(['id' => $request->get('id')]));
    }
}
