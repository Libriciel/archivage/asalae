<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\AdminTenantsController;
use Asalae\Model\Entity\OrgEntity;
use Asalae\Model\Entity\Role;
use Asalae\Model\Entity\TypeEntity;
use Asalae\Model\Entity\User;
use Asalae\Model\Table\BeanstalkJobsTable;
use Asalae\Model\Table\BeanstalkWorkersTable;
use Asalae\Test\Fixture\OrgEntitiesFixture;
use Asalae\Test\Fixture\RolesFixture;
use Asalae\Test\Fixture\TypeEntitiesFixture;
use Asalae\Test\Fixture\UsersFixture;
use Asalae\Test\Mock\FakeEmitter;
use AsalaeCore\Factory;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Beanstalk\Test\Mock\MockedPheanstalk;
use Beanstalk\Utility\Beanstalk;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use DateTime;
use Libriciel\Filesystem\Utility\Filesystem;
use Pheanstalk\Connection;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\MockObject\Exception;

#[UsesClass(AdminTenantsController::class)]
class AdminTenantsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;

    public array $fixtures = [
        'app.AccessRuleCodes',
        'app.Acos',
        'app.Agreements',
        'app.AgreementsOriginatingAgencies',
        'app.AgreementsTransferringAgencies',
        'app.AppraisalRuleCodes',
        'app.ArchiveUnits',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.ArchivingSystems',
        'app.Aros',
        'app.ArosAcos',
        'app.AuthUrls',
        'app.BeanstalkJobs',
        'app.BeanstalkWorkers',
        'app.Configurations',
        'app.Counters',
        'app.CronExecutions',
        'app.Crons',
        'app.Eaccpfs',
        'app.EventLogs',
        'app.Fileuploads',
        'app.Ldaps',
        'app.OrgEntities',
        'app.OrgEntitiesTimestampers',
        'app.Profiles',
        'app.Roles',
        'app.RolesTypeEntities',
        'app.SecureDataSpaces',
        'app.Sequences',
        'app.ServiceLevels',
        'app.Sessions',
        'app.Siegfrieds',
        'app.StoredFiles',
        'app.StoredFilesVolumes',
        'app.Timestampers',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
        'app.ValidationChains',
        'app.Versions',
        'app.Volumes',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::dumpFile(TMP . "test_local.json", '{}');
        $hasher = new DefaultPasswordHasher();
        $admins = [['username' => 'admin', 'password' => $hasher->hash('admin')]];
        $administators = TMP_TESTDIR . DS . 'administrateurs.json';
        Configure::write('App.paths.administrators_json', $administators);
        Filesystem::dumpFile($administators, json_encode($admins));
        Filesystem::dumpFile(TMP . 'path_to_local.php', '<?php return TMP."test_local.json";?>');
        Configure::write('App.paths.path_to_local_config', TMP . 'path_to_local.php');
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('OrgEntities.public_dir', TMP_TESTDIR . DS . 'webroot');
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('Beanstalk.disable_check_ttr', true);
        Configure::write('Beanstalk.table_jobs', 'BeanstalkJobs');
        Configure::write('Beanstalk.table_workers', 'BeanstalkWorkers');

        $users = UsersFixture::defaultValues();
        $user = new User($users[5], ['validate' => false]); // user tech
        $user['id'] = 6;
        $entities = OrgEntitiesFixture::defaultValues();
        $entity = new OrgEntity($entities[1], ['validate' => false]); // service d'archives
        $entity['id'] = 2;
        $typeEntities = TypeEntitiesFixture::defaultValues();
        $typeEntity = new TypeEntity($typeEntities[1], ['validate' => false]);
        $entity['type_entity'] = $typeEntity;
        $archivalAgency = new OrgEntity($entities[1], ['validate' => false]);
        $archivalAgency['id'] = 2;
        $entity['archival_agency'] = $archivalAgency;
        $user['org_entity'] = $entity;
        $roles = RolesFixture::defaultValues();
        $role = new Role($roles[11], ['validate' => false]);
        $role['id'] = 12;
        $user['role'] = $role; // Référent technique
        $sessionData = ['Auth' => $user, 'Session' => ['token' => 'testunit']];

        $this->session($sessionData);
        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit', 'socketEmit', 'socketQuery'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
        Beanstalk::$instance = $beanstalk;
        Factory\Utility::set('Notify', FakeEmitter::class);
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        $Exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => '', 'stderr' => ''])
        );
        Factory\Utility::set('Exec', $Exec);
        /** @var BeanstalkJobsTable $model */
        $model = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $model->sync = true;
        $model->setEntityClass(Entity::class);
        /** @var BeanstalkWorkersTable $model */
        $model = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        $model->sync = true;
        $model->setEntityClass(Entity::class);
        Configure::write('Ip.enable_whitelist', false);
        Configure::write('debug_mails', false);
    }

    /**
     * Adds additional event spies to the controller/view event manager.
     *
     * @return void
     */
    public function controllerSpy()
    {
        if (isset($this->_controller)) {
            $registry = new ComponentRegistry($this->_controller);
            $tasks = $this->getMockBuilder('AsalaeCore\\Controller\\Component\\TasksComponent')
                ->setConstructorArgs([$registry])
                ->onlyMethods(['deleteTube'])
                ->getMock();
            $tasks->expects($this->any())
                ->method('deleteTube')
                ->willReturn(true);
            $this->_controller->components()->set('Tasks', $tasks);
        }
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::setNamespace();
        Filesystem::reset();
        Factory\Utility::reset();
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $response = $this->httpGet('/admin-tenants/index', true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['system']));
        $this->assertTrue(is_bool($response['system']));
    }

    /**
     * testAjaxLogs
     */
    public function testAjaxLogs()
    {
        Filesystem::dumpFile(Configure::read('App.paths.logs', LOGS) . "test_ajax_logs.log", $expected = 'test');
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admin-tenants/ajaxLogs');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response);
        $this->assertNotEmpty($response['logs']);
        $this->assertNotEmpty($response['logs']['test_ajax_logs.log']);
        $this->assertEquals($expected, $response['logs']['test_ajax_logs.log']);
    }

    /**
     * testAjaxTasks
     */
    public function testAjaxTasks()
    {
        $this->setIsConnected(true);
        /** @var BeanstalkJobsTable $jobs */
        $jobs = TableRegistry::getTableLocator()->get('Beanstalk.BeanstalkJobs');
        $jobs->sync = true;
        $jobs->setEntityClass('Asalae\Test\Mock\EmptyEntity');
        $jobs->deleteAll([]);
        $jobs->save(
            $jobs->newEntity(
                [
                    'jobid' => 0,
                    'job_state' => BeanstalkJobsTable::S_PENDING,
                    'tube' => 'testunit',
                ]
            )
        );

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admin-tenants/ajaxTasks');
        json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
    }

    /**
     * setIsConnected
     * @param $bool
     * @throws Exception
     */
    private function setIsConnected($bool)
    {
        $Connection = $this->createMock(Connection::class);
        $Connection->method('isServiceListening')->willReturn($bool);
        MockedPheanstalk::$MockedConnection = $Connection;
        if (is_file(TMP . 'test_local.json')) {
            unlink(TMP . 'test_local.json');
        }
    }

    /**
     * testAjaxCheckDisk
     */
    public function testAjaxCheckDisk()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admin-tenants/ajaxCheckDisk');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response);
        $this->assertNotEmpty($response['disks']);
    }

    /**
     * testNotifyAll
     */
    public function testNotifyAll()
    {
        $this->httpGet('/admin-tenants/notifyAll');
        $this->assertResponseCode(200);
        $this->httpPost('/admin-tenants/notifyAll', ['activeusers' => true, 'msg' => 'test', 'color' => 'alert-info']);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testIndexServicesArchives
     */
    public function testIndexServicesArchives()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admin-tenants/index-services-archives');
        $this->assertResponseCode(200);
    }

    /**
     * Requete type ajax
     */
    private function setAjaxRequest()
    {
        $this->configRequest(
            [
                'headers' => [
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );
    }

    /**
     * testEditServiceArchive
     */
    public function testEditServiceArchive()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admin-tenants/edit-service-archive/2');
        $this->assertResponseCode(200);

        $this->setAjaxRequest();
        $this->httpPut('/admin-tenants/edit-service-archive/2');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'name' => 'testunit',
            'identifier' => 'testunit',
            'timestamper_id' => 1,
            'default_secure_data_space_id' => 1,
            'timestampers' => [
                '_ids' => [1],
            ],
        ];
        $this->setAjaxRequest();
        $this->httpPut('/admin-tenants/edit-service-archive/2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testTail
     */
    public function testTail()
    {
        file_put_contents(Configure::read('App.paths.logs', LOGS) . 'testunit.log', 'test');
        ob_start();
        $this->httpGet('/admin-tenants/tail/testunit.log/0.1');
        $partialEmit = ob_get_clean();
        $this->assertResponseCode(200);
        $this->assertEquals('test', $partialEmit);
        unlink(Configure::read('App.paths.logs', LOGS) . 'testunit.log');
    }

    /**
     * testIndexVolumes
     */
    public function testIndexVolumes()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admin-tenants/index-volumes');
        $this->assertResponseCode(200);
    }

    /**
     * testTestVolume
     */
    public function testTestVolume()
    {
        $dir = TMP_VOL1;
        if (!is_dir($dir)) {
            Filesystem::mkdir($dir);
        }
        $response = $this->httpPost('/admin-tenants/testVolume/1');
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->success));
    }

    /**
     * testWorkerLog
     */
    public function testWorkerLog()
    {
        $exec = $this->createMock(Exec::class);
        $cmdOutput = new CommandResult(
            [
                'success' => true,
                'code' => 0,
                'stdout' => 'the log value',
                'stderr' => '',
            ]
        );
        $exec->method('command')->willReturn($cmdOutput);
        Factory\Utility::set('Exec', $exec);
        $this->httpGet('/admin-tenants/worker-log/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('the log value');
    }

    /**
     * testPhpInfo
     */
    public function testPhpInfo()
    {
        $this->httpGet('/admin-tenants/php-info');
        $this->assertResponseCode(200);
        $this->assertResponseContains('upload_max_filesize');
    }

    /**
     * testIndexLdaps
     */
    public function testIndexLdaps()
    {
        $this->httpGet('/admin-tenants/index-ldaps');
        $this->assertResponseCode(200);
    }

    /**
     * testAddLdap
     */
    public function testAddLdap()
    {
        $this->httpGet('/admin-tenants/add-ldap');
        $this->assertResponseCode(200);

        $data = [
            'org_entity_id' => 2,
            'name' => 'test',
            'host' => '127.0.0.1',
            'port' => 389,
            'user_query_login' => 'test@adullact.win',
            'user_query_password' => 'test',
            'ldap_root_search' => 'dc=adullact,dc=win',
            'user_login_attribute' => 'sAMAccountName',
            'user_username_attribute' => 'sAMAccountName',
            'ldap_users_filter' => '(memberOf=cn=asalae,OU=Groupes,dc=adullact,dc=win)',
            'account_prefix' => '',
            'account_suffix' => '@adullact.win',
            'description' => 'Pour tester la connexion LDAP',
            'use_proxy' => false,
            'use_ssl' => false,
            'use_tls' => false,
            'user_name_attribute' => 'displayname',
            'user_mail_attribute' => 'mail',
        ];
        $this->httpPost('/admin-tenants/add-ldap', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEditLdap
     */
    public function testEditLdap()
    {
        $Ldaps = TableRegistry::getTableLocator()->get('Ldaps');
        $ldap = $Ldaps->newEntity(
            [
                'name' => 'test',
                'host' => 'localhost',
                'port' => '12345',
                'ldap_root_search' => '(*)',
                'user_login_attribute' => 'samaccountname',
            ]
        );
        $Ldaps->saveOrFail($ldap);
        $this->httpGet('/admin-tenants/edit-ldap/' . $ldap->id);
        $this->assertResponseCode(200);

        $data = [
            'name' => 'foo',
        ];
        $this->httpPut('/admin-tenants/edit-ldap/' . $ldap->id, $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testDeleteLdap
     */
    public function testDeleteLdap()
    {
        $Ldaps = TableRegistry::getTableLocator()->get('Ldaps');
        $ldap = $Ldaps->newEntity(
            [
                'name' => 'test',
                'host' => 'localhost',
                'port' => '12345',
                'ldap_root_search' => '(*)',
                'user_login_attribute' => 'samaccountname',
            ]
        );
        $Ldaps->saveOrFail($ldap);
        $this->httpDelete('/admin-tenants/delete-ldap/' . $ldap->id);
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testIndexSessions
     */
    public function testIndexSessions()
    {
        $this->httpGet('/admin-tenants/index-sessions');
        $this->assertResponseCode(200);
    }

    /**
     * testDeleteSessions
     */
    public function testDeleteSessions()
    {
        TableRegistry::getTableLocator()->get('Sessions')->updateAll(
            ['expires' => (new DateTime())->getTimestamp() + 10],
            ['id' => '77ovlobsthtb5ngll7lts2fpcl']
        );
        $this->httpDelete('/admin-tenants/delete-session/77ovlobsthtb5ngll7lts2fpcl');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testNotifyUser
     */
    public function testNotifyUser()
    {
        TableRegistry::getTableLocator()->get('Sessions')->updateAll(
            ['expires' => (new DateTime())->getTimestamp() + 10],
            ['id' => '77ovlobsthtb5ngll7lts2fpcl']
        );
        $this->httpGet('/admin-tenants/notify-user/77ovlobsthtb5ngll7lts2fpcl');
        $this->assertResponseCode(200);

        Factory\Utility::set('Notify', FakeEmitter::class);

        $data = [
            'msg' => 'foo',
            'color' => 'danger',
        ];
        $response = $this->httpPost(
            '/admin-tenants/notify-user/77ovlobsthtb5ngll7lts2fpcl',
            $data,
            true
        );
        $this->assertResponseCode(200);
        $this->assertTrue(Hash::get($response, 'success'));
    }

    /**
     * testIndexJobs
     */
    public function testIndexJobs()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $job = $Jobs->newEntity(
            [
                'jobid' => 1,
                'tube' => 'test',
                'user_id' => 1,
            ]
        );
        $this->assertNotFalse($Jobs->save($job));

        $this->get('/admin-tenants/indexJobs/test');
        $this->assertResponseCode(200);

        MockedPheanstalk::$statsJob['state'] = 'ready';
        $response = $this->httpGet('/admin-tenants/indexJobs/test?job_state[]=' . BeanstalkJobsTable::S_PENDING);
        $this->assertResponseCode(200);
        $this->assertGreaterThanOrEqual(1, $response);
    }

    /**
     * testJobInfo
     */
    public function testJobInfo()
    {
        $response = $this->httpGet('/admin-tenants/jobInfo/1', true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('id', $response);
    }

    /**
     * testJobResume
     */
    public function testJobResume()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $Jobs->updateAll(['job_state' => BeanstalkJobsTable::S_FAILED], ['id' => 1]);
        $query = $Jobs->find()->where(['job_state' => BeanstalkJobsTable::S_PENDING]);
        $initialCount = $query->count();
        $response = $this->httpGet('/admin-tenants/jobResume/1', true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('report', $response);
        $this->assertEquals('done', $response['report']);
        $this->assertCount($initialCount + 1, $query);
    }

    /**
     * testJobPause
     */
    public function testJobPause()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $Jobs->updateAll(['job_state' => BeanstalkJobsTable::S_PENDING], ['id' => 1]);
        $response = $this->httpGet('/admin-tenants/jobPause/1', true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('report', $response);
        $this->assertEquals('done', $response['report']);
    }
}
