<?php

declare(strict_types=1);

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\ArchivingSystemsController;
use Asalae\Test\Mock\ClientMock;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Http\Client\Response;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\ArchivingSystemsController Test Case
 *
 */
#[UsesClass(ArchivingSystemsController::class)]
class ArchivingSystemsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        'app.ArchivingSystems',
        'app.OrgEntities',
        'app.EventLogs',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->genericSessionData['Auth']->set(
            'admin',
            [
                'tech' => true,
                'data' => [
                    'username' => 'testunit',
                ],
            ]
        );
        $this->genericSessionData['Auth']->set('org_entity', null);
        $this->session($this->genericSessionData);
        $response = $this->createMock(Response::class);
        $response->method('getJson')->willReturn([]);
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get'])
            ->getMock();
        $client->method('get')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
    }

    /**
     * testIndex
     */
    public function testIndex(): void
    {
        $this->httpGet('/archiving-systems/index');
        $this->assertResponseCode(200);
    }

    /**
     * testAdd
     */
    public function testAdd(): void
    {
        $this->httpGet('/archiving-systems/add');
        $this->assertResponseCode(200);

        $ArchivingSystems = TableRegistry::getTableLocator()->get('ArchivingSystems');
        $ArchivingSystems->deleteAll([]);
        $data = [
            'org_entity_id' => 2,
            'name' => 'testunit',
            'description' => 'test',
            'url' => 'https://libriciel.fr',
            'username' => 'pastell',
            'use_proxy' => false,
            'active' => true,
            'password' => 'foo',
            'ssl_verify_peer' => true,
            'ssl_verify_peer_name' => true,
            'ssl_verify_depth' => 5,
            'ssl_verify_host' => true,
            'ssl_cafile' => '',
        ];
        $this->httpPost('/archiving-systems/add', $data);
        $this->assertResponseCode(200);
        $this->assertCount(1, $ArchivingSystems->find());
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEdit
     */
    public function testEdit(): void
    {
        $this->httpGet('/archiving-systems/edit/1');
        $this->assertResponseCode(200);

        $data = [
            'org_entity_id' => 2,
            'name' => 'testunit',
            'description' => 'test',
            'url' => 'https://libriciel.fr',
            'username' => 'pastell',
            'use_proxy' => false,
            'active' => true,
            'password' => 'foo',
            'ssl_verify_peer' => true,
            'ssl_verify_peer_name' => true,
            'ssl_verify_depth' => 5,
            'ssl_verify_host' => true,
            'ssl_cafile' => '',
        ];
        $this->httpPut('/archiving-systems/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testDelete
     */
    public function testDelete(): void
    {
        $this->httpDelete('/archiving-systems/delete/1');
        $this->assertResponseCode(200);
        $this->assertResponseNotContains('done');

        $ArchivingSystems = TableRegistry::getTableLocator()->get('ArchivingSystems');
        $ArchivingSystems->updateAll(['org_entity_id' => null], []);
        $this->httpDelete('/archiving-systems/delete/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testView
     */
    public function testView(): void
    {
        $this->httpGet('/archiving-systems/view/1');
        $this->assertResponseCode(200);
    }

    /**
     * testTestConnection
     */
    public function testTestConnection(): void
    {
        $data = [
            'name' => 'test',
            'url' => 'https://next',
            'username' => 'admin',
            'password' => 'admin',
            'use_proxy' => false,
            'active' => true,
            'ssl_verify_peer' => true,
            'ssl_verify_peer_name' => true,
            'ssl_verify_depth' => 5,
            'ssl_verify_host' => true,
            'ssl_cafile' => '',
        ];
        $this->httpPost('/archiving-systems/test-connection', $data);
        $this->assertResponseCode(200);
    }
}
