<?php

/** @noinspection SqlDialectInspection */

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\InstallController;
use Asalae\Form\InstallForm;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\EmptyFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Config;
use AsalaeCore\Utility\Exec;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Driver\Sqlite;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\DateTime;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;
use PDOException;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\InstallController Test Case
 */
#[UsesClass(InstallController::class)]
class InstallControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;
    use EmptyFixturesTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public array $fixtures = [
        "app.Acos",
        "app.Configurations",
        "app.Eaccpfs",
        "app._EmptyOrgEntities",
        "app.EventLogs",
        "app.Ldaps",
        "app.Roles",
        "app.Sessions",
        "app.Timestampers",
        "app.TypeEntities",
        "app.Users",
    ];

    private $originalPathToLocal;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $dir = sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN');
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        mkdir($dir);
        Configure::write('App.paths.data', $dir);
        Configure::write('debug_mails', false);
        Filesystem::reset();
        $email = $this->getMockBuilder(Mailer::class)
            ->onlyMethods(['send'])
            ->getMock();
        $email->method('send')->willReturn(['email content']);
        Utility::set(Mailer::class, $email);
        $this->originalPathToLocal = Configure::read('App.paths.path_to_local_config');
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
        Config::reset();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        $dir = sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN');
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        Filesystem::setNamespace();
        Filesystem::reset();
        Configure::write('App.paths.path_to_local_config', $this->originalPathToLocal);
        Utility::reset();
        Config::reset();
    }

    /**
     * Test index method
     */
    public function testIndex()
    {
        $loc = TableRegistry::getTableLocator();
        $TypeEntities = $loc->get('TypeEntities');
        $TypeEntities->saveOrFail(
            $TypeEntities->newEntity(
                [
                    'name' => 'Service d\'Exploitation',
                    'active' => true,
                    'created' => new DateTime(),
                    'modified' => new DateTime(),
                    'code' => 'SE',
                ],
            )
        );

        // cas début d'installation (1er get)
        Configure::write(
            'App.paths.path_to_local_config',
            sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN') . DS . 'path_to_local.php'
        );
        $OrgEntities = $loc->get('OrgEntities');
        $this->get('/install/index');
        $this->assertResponseCode(200);

        // 1er post (initialisation de la conf)
        $data = [
            'local_config_file' => $file = sys_get_temp_dir() . DS . 'testinstall' . getenv(
                'TEST_TOKEN'
            ) . DS . 'app_local.json',
            'Config__App__paths__data' => sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN'),
        ];
        if (is_file($file)) {
            unlink($file);
        }
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertFileExists($file);

        // 2e post (config de base)
        $data = [
            'Config__App__defaultLocale' => 'fr_FR',
            'Config__App__timezone' => 'Europe/Paris',
            'Config__App__fullBaseUrl' => 'https://www.libriciel.fr',
            'ignore_invalid_fullbaseurl' => false,
        ];
        $this->assertStringNotContainsString('https://www.libriciel.fr', file_get_contents($file));
        // NOTE: si test échoue, libriciel.fr est potentiellement down - passer ignore_invalid_fullbaseurl à true
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertStringContainsString('https://www.libriciel.fr', file_get_contents($file));

        // 3e post (ratchet)
        $data = [
            'Config__Ratchet__connect' => 'wss://libriciel.fr/wss',
        ];
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);

        // 4e post (datasource)
        $data = [
            'Config__Datasources__default__driver' => Sqlite::class,
            'Config__Datasources__default__host' => 'localhost',
            'Config__Datasources__default__username' => 'fakeuser',
            'Config__Datasources__default__password' => 'fakepassword',
            'confirm_database_password' => 'fakepassword',
            'Config__Datasources__default__database' => sys_get_temp_dir() . DS . 'testinstall' . getenv(
                'TEST_TOKEN'
            ) . DS . 'database.sqlite',
        ];
        $this->assertStringNotContainsString('database.sqlite', file_get_contents($file));
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertStringContainsString('database.sqlite', file_get_contents($file));

        // 5e post (Security.salt)
        $data = [
            'security_salt_method' => 'hash',
            'security_salt_method_hash' => 'testunit',
        ];
        $hash = 'e4690848b6417abf1978ccf9190c7cbf44dd66e8805618dfbe18f14915400cf3';
        $this->assertStringNotContainsString($hash, file_get_contents($file));
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertStringContainsString($hash, file_get_contents($file));

        // 6e post email
        $data = [
            'Config__Email__default__from' => 'no-reply@asalae2.fr',
            'Config__EmailTransport__default__className' => 'Mail',
            'Config__EmailTransport__default__host' => 'localhost',
            'Config__EmailTransport__default__port' => 25,
            'Config__EmailTransport__default__username' => 'username',
            'Config__EmailTransport__default__password' => 'password',
        ];
        $this->assertStringNotContainsString('no-reply@asalae2.fr', file_get_contents($file));
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertStringContainsString('no-reply@asalae2.fr', file_get_contents($file));

        // patch database (migration migrate) (note: datasource crée lors du 4e post)
        /** @var Connection $conn */
        $conn = ConnectionManager::get('validate_datasource');
        try {
            $conn->execute('select id from users limit 1');
            $success = true;
        } catch (PDOException) {
            $success = false;
        }
        $this->assertFalse($success);

        $Exec = $this->createMock(Exec::class);
        $Exec->method('rawCommand')->willReturnCallback(
            function ($command, array &$output = null, &$return_var = null) {
                $output = [];
                $return_var = 0;
                return '';
            }
        );
        Utility::set('Exec', $Exec);
        $data = [
            'initializeDatabase' => true,
        ];
        $this->httpPatch('/install/index', $data);
        Filesystem::rollback();
        $this->assertResponseCode(200);

        // 7e post (administrateur technique)
        $data = [
            'Admins__username' => 'testunit',
            'Admins__email' => 'testunit@libriciel.fr',
            'Admins__password' => 'testunit',
            'Admins__confirm-password' => 'testunit',
        ];
        $admins = sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN') . DS . 'administrateurs.json';
        Configure::write('App.paths.administrators_json', $admins);
        $this->assertFileDoesNotExist($admins);
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertFileExists($admins);

        // 8e post (horodatage interne)
        $data = [
            'Timestamper__countryName' => 'FR',
            'Timestamper__stateOrProvinceName' => 'France',
            'Timestamper__localityName' => 'Paris',
            'Timestamper__organizationName' => 'testunit',
            'Timestamper__organizationalUnitName' => 'php',
            'Timestamper__commonName' => 'libriciel',
            'Timestamper__emailAddress' => 'testunit@libriciel.fr',
            'Timestamper__extfile' => InstallForm::DEFAULT_TIMESTAMPER_TSA_EXTFILE,
            'Timestamper__directory' => sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN'),
            'Timestamper__cnf' => str_replace(
                '{{dir}}',
                sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN'),
                file_get_contents(RESOURCES . 'openssl.cnf')
            ),
            'Timestamper__name' => 'Openssl',
        ];
        $Timestampers = $loc->get('Timestampers');
        $count = $Timestampers->find()->count();
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertEquals($count + 1, $Timestampers->find()->count());

        // 9e post
        $OrgEntities->setTable('org_entities')->deleteAll([]);
        $data = [
            'ServiceExploitation__name' => 'SE-Testunit',
            'ServiceExploitation__identifier' => 'se-Testunit',
            'Eaccpfs__agent' => 'testunit',
        ];
        $this->assertEquals(0, $OrgEntities->find()->count());
        $this->post('/install/index', $data);
        $this->assertRedirect('/admins');
        $this->assertEquals(1, $OrgEntities->find()->count());
    }

    /**
     * testSendTestMail
     */
    public function testSendTestMail()
    {
        TableRegistry::getTableLocator()->get('OrgEntities')->deleteAll([]);
        $data = [
            'Config__EmailTransport__default__className' => 'Mail',
            'Config__EmailTransport__default__host' => 'localhost',
            'Config__EmailTransport__default__port' => '25',
            'Config__EmailTransport__default__username' => 'test',
            'Config__EmailTransport__default__password' => 'test',
            'email' => 'test@test.fr',
            'Config__Email__default__from' => 'test@test.fr',
        ];
        $response = $this->httpPost('/install/send-test-mail', $data, true);
        $this->assertResponseCode(200);
        $this->assertTrue(Hash::get($response, 'success'));
    }

    /**
     * testTimestamper
     */
    public function testTimestamper()
    {
        $data = [
            'Timestamper__directory' => $dir = sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN'),
            'Timestamper__cnf' => str_replace(
                '{{dir}}',
                $dir,
                file_get_contents(RESOURCES . 'openssl.cnf')
            ),
            'Timestamper__countryName' => 'FR',
            'Timestamper__stateOrProvinceName' => 'France',
            'Timestamper__localityName' => 'Paris',
            'Timestamper__organizationName' => 'test',
            'Timestamper__organizationalUnitName' => 'test',
            'Timestamper__commonName' => 'test',
            'Timestamper__emailAddress' => 'test@test.fr',
            'Timestamper__extfile' => InstallForm::DEFAULT_TIMESTAMPER_TSA_EXTFILE,
        ];
        $response = $this->httpPost('/install/test-timestamper', $data, true);
        $this->assertResponseCode(200);
        $this->assertTrue(Hash::get($response, 'success'));
    }
}
