<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\Component\LoginComponent;
use Asalae\Controller\WebservicesController;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\WebservicesController Test Case
 */
#[UsesClass(WebservicesController::class)]
class WebservicesControllerTest extends TestCase
{
    use HttpTrait;
    use IntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_EVENT_LOGS,
        'app.AccessTokens',
        'app.BeanstalkJobs',
        'app.Configurations',
        'app.Fileuploads',
        'app.Killables',
        'app.Ldaps',
        'app.Notifications',
        'app.RolesTypeEntities',
        'app.SavedFilters',
        'app.Sessions',
        'app.Siegfrieds',
        'app.ValidationActors',
    ];
    private $debugFixtures = false;

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        LoginComponent::$attempts = [];
        Utility::reset();
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->setJsonRequest();
        $this->get('/webservices/index');
        $this->assertResponseCode(401);

        $this->logSessionAdmin();
        $this->setJsonRequest();
        $this->httpGet('/webservices/index');
        $this->assertResponseCode(200);
    }

    /**
     * setJsonRequest
     */
    private function setJsonRequest()
    {
        $this->configRequest(
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );
        Configure::write('Password.complexity', 1);
    }

    /**
     * logSessionAdmin
     */
    private function logSessionAdmin()
    {
        $this->session(
            [
                'Admin' => ['tech' => true],
                'Auth' => ['username' => 'admin', 'admin' => true],
            ]
        );
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $this->logSessionAdmin();
        $this->setJsonRequest();
        $this->httpGet('/webservices/add');
        $this->assertResponseCode(200);

        $this->setJsonRequest();
        $this->httpPost('/webservices/add');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'name' => 'foo',
            'description' => 'bar',
            'active' => true,
            'username' => 'foo',
            'password' => 'bar',
            'confirm-password' => 'bar',
            'org_entity_id' => 2,
            'role_id' => 4,
        ];
        $this->setJsonRequest();
        $this->httpPost('/webservices/add', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testAddWrongEntity
     */
    public function testAddWrongEntity()
    {
        $this->session($this->genericSessionData);
        $this->setJsonRequest();
        $this->get('/webservices/add/3');
        $this->assertResponseCode(404);

        $data = [
            'name' => 'foo',
            'description' => 'bar',
            'active' => true,
            'username' => 'foo',
            'password' => 'bar',
            'confirm-password' => 'bar',
            'org_entity_id' => 3,
            'role_id' => 4,
        ];
        $this->setJsonRequest();
        $this->post('/webservices/add', $data);
        $this->assertResponseCode(404);
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $Users = TableRegistry::getTableLocator()->get('Users');
        $Users->updateAll(['agent_type' => 'software'], ['id' => 1]);

        $this->logSessionAdmin();
        $this->setJsonRequest();
        $this->httpGet('/webservices/edit/1');
        $this->assertResponseCode(200);

        $this->setJsonRequest();
        $this->httpPut('/webservices/edit/1');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'name' => 'foo',
            'description' => 'bar',
            'active' => true,
            'username' => 'foo',
            'password' => 'bar',
            'confirm-password' => 'bar',
            'role_id' => 8,
        ];
        $this->setJsonRequest();
        $this->httpPut('/webservices/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $this->logSessionAdmin();
        $response = $this->httpDelete('/webservices/delete/2');
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response->report));
        $this->assertEquals('done', $response->report);
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->logSessionAdmin();
        $this->httpGet('/webservices/view/2');
        $this->assertResponseCode(200);
    }

    /**
     * testMonitoring
     */
    public function testMonitoring()
    {
        $this->get('/api/webservices/monitoring');
        $this->assertResponseCode(401);

        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'Webservices', 'alias' => 'monitoring'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        $this->session($this->genericSessionData);

        $Exec = $this->createMock(Exec::class);
        $Exec->method('command')->willReturn(
            new CommandResult(
                [
                    'success' => true,
                    'code' => 0,
                    'stdout' => 'OK',
                    'stderr' => '',
                ]
            )
        );
        Utility::set('Exec', $Exec);

        $this->httpGet('/api/webservices/monitoring');
        $this->assertResponseCode(200);
        $this->assertResponseContains('success');
    }

    /**
     * testPing
     */
    public function testPing()
    {
        $this->httpGet('/api/webservices/ping');
        $this->assertResponseCode(200);
        $this->assertResponseContains('Pong');
    }

    /**
     * testVersion
     */
    public function testVersion()
    {
        $this->get('/api/webservices/version');
        $this->assertResponseCode(401);

        $this->session(
            Hash::merge(
                [
                    'User' => [
                        'id' => 2,
                    ],
                ],
                $this->genericSessionData
            )
        );
        $this->httpGet('/api/webservices/version');
        $this->assertResponseCode(200);
        $this->assertResponseContains(ASALAE_VERSION_LONG);
    }

    /**
     * testWhoami
     */
    public function testWhoami()
    {
        $this->get('/api/webservices/whoami');
        $this->assertResponseCode(401);

        $this->configRequest(
            [
                'headers' => [
                    'authorization' => 'Basic ' . base64_encode('pastell:pastell'),
                ],
            ]
        );
        $this->httpGet('/api/webservices/whoami');
        $this->assertResponseCode(200);
        $this->assertResponseContains('pastell');
    }
}
