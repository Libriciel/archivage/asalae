<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\OutgoingTransfersController;
use AsalaeCore\Model\Table\AuthUrlsTable;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(OutgoingTransfersController::class)]
class OutgoingTransfersControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,
        'app.ArchiveUnits',
        'app.ArchiveUnitsOutgoingTransferRequests',
        'app.ArchivingSystems',
        'app.AuthSubUrls',
        'app.AuthUrls',
        'app.EventLogs',
        'app.OutgoingTransferRequests',
        'app.OutgoingTransfers',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Configure::write('App.paths.data', TMP_TESTDIR);
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'outgoing-transfers' . DS . 'sa_OAT_1.zip', 'test');
    }

    /**
     * testIndexPreparating
     */
    public function testIndexPreparating()
    {
        $this->httpGet('/outgoing-transfers/index-preparating');
        $this->assertResponseCode(200);
    }

    /**
     * testIndexAccepted
     */
    public function testIndexAccepted()
    {
        $this->httpGet('/outgoing-transfers/index-accepted');
        $this->assertResponseCode(200);
    }

    /**
     * testIndexRejected
     */
    public function testIndexRejected()
    {
        $this->httpGet('/outgoing-transfers/index-rejected');
        $this->assertResponseCode(200);
    }

    /**
     * testIndexAll
     */
    public function testIndexAll()
    {
        $this->httpGet('/outgoing-transfers/index-all');
        $this->assertResponseCode(200);
    }

    /**
     * testZip
     */
    public function testZip()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'OutgoingTransfers', 'alias' => 'zip'])
            ->orderByDesc('lft')
            ->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $loc = TableRegistry::getTableLocator();
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $loc->get('AuthUrls');
        $authUrl = $AuthUrls->newEntity(
            [
                'url' => '/api/outgoing-transfers/zip/1',
                'expire' => date('Y-m-d H:i:s', strtotime('+1 minute')),
            ]
        );
        $AuthUrls->saveOrFail($authUrl);
        $code = $authUrl->get('code');
        $this->get('/auth-urls/activate/' . $code);
        $this->assertRedirect('/api/outgoing-transfers/zip/1');
        $this->assertHeaderContains('code', $code);

        $this->configRequest(['headers' => $this->_response->getHeaders()]);
        $this->get('/api/outgoing-transfers/zip/1');
        $this->assertResponseCode(200);
        $this->assertHeaderContains('Content-Type', 'application/zip');
        $this->assertHeaderContains('Content-Disposition', 'sa_OAT_1.zip');
        $this->assertFalse($AuthUrls->exists(['id' => $authUrl->id]));
    }

    /**
     * testDeleteZip
     */
    public function testDeleteZip()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'OutgoingTransfers', 'alias' => 'deleteZip'])
            ->orderByDesc('lft')
            ->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $loc = TableRegistry::getTableLocator();
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $loc->get('AuthUrls');
        $authUrl = $AuthUrls->newEntity(
            [
                'url' => '/api/outgoing-transfers/deleteZip/1',
                'expire' => date('Y-m-d H:i:s', strtotime('+1 minute')),
            ]
        );
        $AuthUrls->saveOrFail($authUrl);
        $code = $authUrl->get('code');
        $this->get('/auth-urls/activate/' . $code);
        $this->assertRedirect('/api/outgoing-transfers/deleteZip/1');
        $this->assertHeaderContains('code', $code);

        $this->configRequest(['headers' => $this->_response->getHeaders()]);
        $this->get('/api/outgoing-transfers/deleteZip/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
        $this->assertFalse($AuthUrls->exists(['id' => $authUrl->id]));
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpGet('/outgoing-transfers/view/1');
        $this->assertResponseCode(200);
    }
}
