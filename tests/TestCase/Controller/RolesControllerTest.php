<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\RolesController;
use AsalaeCore\Controller\Component\AclComponent;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(RolesController::class)]
class RolesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        'app.RolesTypeEntities',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Configure::write('Roles.global_is_editable', true);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $response = $this->httpGet('/roles/index/1', true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['roles_globals']);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $response = $this->httpGet('/roles/add/2', true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $data = [
            'name' => 'test_2',
            'active' => true,
            'hierarchical_view' => false,
        ];

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpPost('/roles/add/2', $data, true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['id']);
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $response = $this->httpGet('/roles/edit/1', true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $data = [
            'name' => 'test_2',
            'active' => true,
            'hierarchical_view' => false,
        ];

        $response = $this->httpPut('/roles/edit/1', $data, true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['id']);
        $this->assertEquals('test_2', $response['name']);
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $Roles = TableRegistry::getTableLocator()->get('Roles');
        $Roles->saveOrFail(
            $role = $Roles->newEntity(
                [
                    'name' => 'test_role',
                    'active' => true,
                    'org_entity_id' => 2,
                    'code' => 'TST',
                    'parent_id' => 1,
                    'hierarchical_view' => false,
                    'description' => null,
                    'agent_type' => 'person',
                ],
            )
        );
        $response = $this->httpDelete('/roles/delete/' . $role->id, [], true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']);
        $this->assertEquals('done', $response['report']);
    }

    /**
     * testView
     */
    public function testView()
    {
        $registry = new ComponentRegistry(new Controller(new ServerRequest()));
        $Acl = new AclComponent($registry);
        $Acl->allow(['model' => 'Roles', 'foreign_key' => 1], 'controllers/Roles/view');
        Configure::write('App.paths.controllers_rules', $dir = TMP_TESTDIR . DS . 'controllers.json');
        Filesystem::dumpFile($dir, json_encode([]));
        $response = $this->httpGet('/roles/view/1', true);
        $this->assertResponseCode(200);
        $this->assertTrue(Hash::get($response, 'accesses.controllers.Roles.action.view'));
    }
}
