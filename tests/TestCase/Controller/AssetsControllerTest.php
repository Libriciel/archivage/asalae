<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\AssetsController;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(AssetsController::class)]
class AssetsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use SaveBufferTrait;

    public const string TEST_IMG = TMP . 'test-assets.png';
    public const string TEST_IMG2 = TMP . 'test-assets2.png';
    public array $fixtures = [
        'app.Acos',
        'app.Configurations',
        'app.OrgEntities',
        'app.Ldaps',
        'app.TypeEntities',
        'app.Roles',
        'app.Users',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $handle = imagecreatetruecolor(50, 50);
        $white = imagecolorallocate($handle, 255, 255, 255);
        imagefilledrectangle($handle, 0, 0, 49, 49, $white);
        imagepng($handle, self::TEST_IMG);
        imagedestroy($handle);
        $handle = imagecreatetruecolor(50, 50);
        $red = imagecolorallocate($handle, 255, 0, 0);
        imagefilledrectangle($handle, 0, 0, 49, 49, $red);
        imagepng($handle, self::TEST_IMG2);
        imagedestroy($handle);
        Configure::write(
            'Assets',
            [
                'login' => [
                    'logo-client' => self::TEST_IMG2,
                ],
                'global-background' => self::TEST_IMG,
                'global-background-position' => '50px 30px',
            ]
        );
        Configure::write('App.fullBaseUrl', false);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_file(self::TEST_IMG)) {
            unlink(self::TEST_IMG);
        }
        if (is_file(self::TEST_IMG2)) {
            unlink(self::TEST_IMG2);
        }
    }

    /**
     * testConfiguredBackground
     */
    public function testConfiguredBackground()
    {
        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/assets/configuredBackground'
        );
        $this->assertResponseCode(200);
        $response = md5($output);
        $this->assertHeader('Content-type', 'image/png');
        $this->assertEquals('858cb21a4087e839e21ae3c6c26bfa48', $response);
    }

    /**
     * testConfiguredBackground404
     */
    public function testConfiguredBackground404()
    {
        Configure::delete('Assets.global-background');
        $this->get('/assets/configuredBackground');
        $this->assertResponseCode(404);
    }

    /**
     * testConfiguredLogoClient
     */
    public function testConfiguredLogoClient()
    {
        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/assets/configuredLogoClient'
        );
        $this->assertResponseCode(200);
        $this->assertHeader('Content-type', 'image/png');
        $this->assertEquals('d9534b6771aede98dc7bdbe2cfc0ea73', md5($output));
    }

    /**
     * testConfiguredLogoClient404
     */
    public function testConfiguredLogoClient404()
    {
        Configure::delete('Assets.login.logo-client');
        $this->get('/assets/configuredLogoClient');
        $this->assertResponseCode(404);
    }

    /**
     * testCss
     */
    public function testCss()
    {
        Router::url("/Assets/configured-background", true);
        $this->httpGet('/assets/css');
        $this->assertResponseCode(200);
        $body = $this->_response->getBody();
        $body->rewind();
        $response = md5($body->getContents());
        $this->assertHeader('Content-type', 'text/css');
        $this->assertEquals('52f0503fd8a3e232ff6fa5862dc1d136', $response);
    }
}
