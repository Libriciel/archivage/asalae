<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\RestitutionRequestsController;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Notify;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use DOMDocument;
use PHPUnit\Framework\Attributes\UsesClass;
use stdClass;

/**
 * Asalae\Controller\RestitutionRequestsController Test Case
 */
#[UsesClass(RestitutionRequestsController::class)]
class RestitutionRequestsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;
    use SaveBufferTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_VOLUMES,
        'app.Agreements',
        'app.AppraisalRules',
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveBinariesTechnicalArchiveUnits',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveUnits',
        'app.ArchiveUnitsRestitutionRequests',
        'app.Archives',
        'app.ArchivesRestitutionRequests',
        'app.ArchivesTransfers',
        'app.AuthUrls',
        'app.Counters',
        'app.EventLogs',
        'app.Mails',
        'app.Profiles',
        'app.RestitutionRequests',
        'app.Sequences',
        'app.StoredFiles',
        'app.TechnicalArchiveUnits',
        'app.TechnicalArchives',
        'app.Transfers',
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationHistories',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Configure::write('App.paths.data', TMP_TESTDIR);

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
        Notify::$instance = $this->createMock(Notify::class);
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Utility::reset();
    }

    /**
     * testIndexPreparating
     */
    public function testIndexPreparating()
    {
        TableRegistry::getTableLocator()->get('RestitutionRequests')
            ->updateAll(['state' => 'creating'], ['id' => 1]);
        $response = $this->httpGet('/restitution-requests/index-preparating');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testIndexMy
     */
    public function testIndexMy()
    {
        $response = $this->httpGet('/restitution-requests/index-my');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testIndexAll
     */
    public function testIndexAll()
    {
        TableRegistry::getTableLocator()->get('RestitutionRequests')
            ->updateAll(['state' => 'validating'], ['id' => 1]);
        $response = $this->httpGet('/restitution-requests/index-all');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $loc = TableRegistry::getTableLocator();
        $RestitutionRequests = $loc->get('RestitutionRequests');
        $RestitutionRequests->deleteAll([]);
        $AppraisalRules = $loc->get('AppraisalRules');
        $AppraisalRules->updateAll(['final_action_code' => 'destroy'], []);
        $ArchiveUnitsRestitutionRequests = $loc->get('ArchiveUnitsRestitutionRequests');
        $ArchiveUnitsRestitutionRequests->deleteAll([]);
        $this->httpGet('/restitution-requests/add?originating_agency_id=2');
        $this->assertResponseCode(200);

        $this->assertCount(1, $RestitutionRequests->find());
        $this->assertGreaterThanOrEqual(1, $ArchiveUnitsRestitutionRequests->find()->count());

        $data = [
            'id' => $RestitutionRequests->find()->first()->id,
            'comment' => 'testunit',
        ];
        $this->httpPost('/restitution-requests/add?originating_agency_id=2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        TableRegistry::getTableLocator()->get('RestitutionRequests')
            ->updateAll(['state' => 'creating'], []);
        $this->httpGet('/restitution-requests/edit/1');
        $this->assertResponseCode(200);

        $data = [
            'comment' => 'testunit',
            'archive_units' => [
                '_ids' => [1],
            ],
        ];
        $this->httpPut('/restitution-requests/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        TableRegistry::getTableLocator()->get('RestitutionRequests')
            ->updateAll(['state' => 'creating'], []);
        $response = $this->httpDelete('/restitution-requests/delete/1', [], true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('report', $response);
        $this->assertEquals('done', $response['report']);
    }

    /**
     * testPaginateArchiveUnits
     */
    public function testPaginateArchiveUnits()
    {
        $this->httpGet('/restitution-requests/paginate-archive-units/1');
        $this->assertResponseCode(200);
    }

    /**
     * testRemoveArchiveUnit
     */
    public function testRemoveArchiveUnit()
    {
        $this->httpDelete('/restitution-requests/remove-archive-unit/1/4');
        $this->assertResponseCode(200);
    }

    /**
     * testSend
     */
    public function testSend()
    {
        $loc = TableRegistry::getTableLocator();
        $RestitutionRequests = $loc->get('RestitutionRequests');
        $RestitutionRequests->updateAll(['state' => 'creating'], []);
        $ValidationProcesses = $loc->get('ValidationProcesses');
        $initialCount = $ValidationProcesses->find()->count();

        $this->httpPost('/restitution-requests/send/1');

        $this->assertResponseCode(200);
        $req = $RestitutionRequests->get(1);
        $this->assertFileExists($xml = $req->get('xml'));
        $dom = new DOMDocument();
        $dom->load($xml);
        $this->assertTrue($dom->schemaValidate(SEDA_V21_XSD));
        $this->assertEquals('validating', $req->get('state'));
        $this->assertCount($initialCount + 1, $ValidationProcesses->find());
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpGet('/restitution-requests/view/1');
        $this->assertResponseCode(200);
    }

    /**
     * testIndexAcquitted
     */
    public function testIndexAcquitted()
    {
        TableRegistry::getTableLocator()->get('RestitutionRequests')
            ->updateAll(['state' => 'restitution_acquitted'], ['id' => 1]);
        $response = $this->httpGet('/restitution-requests/index-acquitted');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testScheduleArchiveDestruction
     */
    public function testScheduleArchiveDestruction()
    {
        TableRegistry::getTableLocator()->get('RestitutionRequests')
            ->updateAll(['state' => 'restitution_acquitted'], ['id' => 1]);
        $response = $this->httpPost('/restitution-requests/schedule-archive-destruction/1');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->id));
    }

    /**
     * testCancelArchiveDestruction
     */
    public function testCancelArchiveDestruction()
    {
        TableRegistry::getTableLocator()->get('RestitutionRequests')
            ->updateAll(['state' => 'archive_destruction_scheduled'], ['id' => 1]);
        $response = $this->httpPost('/restitution-requests/cancel-archive-destruction/1');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->id));
    }

    /**
     * testrestitutionCertificate
     */
    public function testrestitutionCertificate()
    {
        $this->preserveBuffer(
            [$this, 'httpGet'],
            '/restitution-requests/restitution-certificate/1?sessionFile=test'
        );
        $this->assertResponseCode(200);
        $this->assertHeader('Content-Type', 'application/pdf');
    }

    /**
     * testdestructionCertificate
     */
    public function testdestructionCertificate()
    {
        $this->preserveBuffer(
            [$this, 'httpGet'],
            '/restitution-requests/destruction-certificate/1?sessionFile=test'
        );
        $this->assertResponseCode(200);
        $this->assertHeader('Content-Type', 'application/pdf');
    }
}
