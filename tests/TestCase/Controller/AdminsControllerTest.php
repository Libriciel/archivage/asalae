<?php

namespace Asalae\Test\TestCase\Controller;

use Adldap\Adldap;
use Adldap\Connections\Ldap;
use Adldap\Connections\Provider;
use Adldap\Models\Entry;
use Adldap\Query\Paginator;
use ArrayIterator;
use Asalae\Controller\AdminsController;
use Asalae\Model\Table\BeanstalkJobsTable;
use Asalae\Model\Table\BeanstalkWorkersTable;
use Asalae\Test\Mock\FakeEmitter;
use Asalae\Test\Mock\GenericCaller;
use AsalaeCore\Factory;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\TSACertForm;
use AsalaeCore\I18n\Date;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Object\CommandResult;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Mailer\Mailer;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use DateTime;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;
use stdClass;

#[UsesClass(AdminsController::class)]
class AdminsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;

    public array $fixtures = [
        'app.AccessRuleCodes',
        'app.Acos',
        'app.Agreements',
        'app.AgreementsOriginatingAgencies',
        'app.AgreementsTransferringAgencies',
        'app.AppraisalRuleCodes',
        'app.ArchiveUnits',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.ArchivingSystems',
        'app.Aros',
        'app.ArosAcos',
        'app.AuthUrls',
        'app.BeanstalkJobs',
        'app.BeanstalkWorkers',
        'app.Configurations',
        'app.Counters',
        'app.CronExecutions',
        'app.Crons',
        'app.Eaccpfs',
        'app.EventLogs',
        'app.Fileuploads',
        'app.Ldaps',
        'app.OrgEntities',
        'app.OrgEntitiesTimestampers',
        'app.Profiles',
        'app.Roles',
        'app.RolesTypeEntities',
        'app.SecureDataSpaces',
        'app.Sequences',
        'app.ServiceLevels',
        'app.Sessions',
        'app.Siegfrieds',
        'app.StoredFiles',
        'app.StoredFilesVolumes',
        'app.Timestampers',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
        'app.ValidationChains',
        'app.Versions',
        'app.Volumes',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::dumpFile(TMP_TESTDIR . "test_local.json", '{}');
        $hasher = new DefaultPasswordHasher();
        $admins = [['username' => 'admin', 'password' => $hasher->hash('admin')]];
        $administators = TMP_TESTDIR . DS . 'administrateurs.json';
        Configure::write('App.paths.administrators_json', $administators);
        Filesystem::dumpFile($administators, json_encode($admins, JSON_UNESCAPED_SLASHES));
        Filesystem::dumpFile(TMP_TESTDIR . 'path_to_local.php', '<?php return TMP_TESTDIR."test_local.json";?>');
        Configure::write('App.paths.path_to_local_config', TMP_TESTDIR . 'path_to_local.php');
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('OrgEntities.public_dir', TMP_TESTDIR . DS . 'webroot');
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('Beanstalk.disable_check_ttr', true);
        Configure::write('Beanstalk.table_jobs', 'BeanstalkJobs');
        Configure::write('Beanstalk.table_workers', 'BeanstalkWorkers');
        $this->genericSessionData['Auth']->set(
            'admin',
            [
                'tech' => true,
                'data' => [
                    'username' => 'testunit',
                ],
            ]
        );
        $this->session($this->genericSessionData);

        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        $Exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => '', 'stderr' => ''])
        );
        Factory\Utility::set('Exec', $Exec);
        /** @var BeanstalkJobsTable $model */
        $model = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $model->sync = true;
        $model->setEntityClass(Entity::class);
        /** @var BeanstalkWorkersTable $model */
        $model = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        $model->sync = true;
        $model->setEntityClass(Entity::class);
        Configure::write('Ip.enable_whitelist', false);
        Configure::write('debug_mails', false);
        Notify::$instance = $this->createMock(Notify::class);
        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit', 'socketEmit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
        Beanstalk::$instance = $beanstalk;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::setNamespace();
        Filesystem::reset();
        Factory\Utility::reset();
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $response = $this->httpGet('/admins/index', true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['system']));
        $this->assertTrue(is_bool($response['system']));
    }

    /**
     * testDeleteTube
     */
    public function testDeleteTube()
    {
        $this->httpGet('/admins/deleteTube/testunit');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testAjaxLogs
     */
    public function testAjaxLogs()
    {
        Filesystem::dumpFile(Configure::read('App.paths.logs', LOGS) . "test_ajax_logs.log", $expected = 'test');
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admins/ajaxLogs');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response);
        $this->assertNotEmpty($response['logs']);
        $this->assertNotEmpty($response['logs']['test_ajax_logs.log']);
        $this->assertEquals($expected, $response['logs']['test_ajax_logs.log']);
    }

    /**
     * testAjaxTasks
     */
    public function testAjaxTasks()
    {
        /** @var BeanstalkJobsTable $jobs */
        $jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $jobs->sync = true;
        $jobs->setEntityClass('Asalae\Test\Mock\EmptyEntity');
        $jobs->deleteAll([]);
        $jobs->save(
            $jobs->newEntity(
                [
                    'job_state' => BeanstalkJobsTable::S_PENDING,
                    'tube' => 'testunit',
                ]
            )
        );

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admins/ajaxTasks');
        json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
    }

    /**
     * testAjaxCheckDisk
     */
    public function testAjaxCheckDisk()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admins/ajaxCheckDisk');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response);
        $this->assertNotEmpty($response['disks']);
    }

    /**
     * testAjaxToggleDebug
     */
    public function testAjaxToggleDebug()
    {
        file_put_contents(TMP_TESTDIR . "test_local.json", json_encode(['debug' => true]));
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/admins/ajaxToggleDebug', ['state' => 'false']);
        $this->assertResponseCode(200);
        $conf = json_decode(file_get_contents(TMP_TESTDIR . "test_local.json"));
        $this->assertNotEmpty($conf);
        $this->assertFalse($conf->debug);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/admins/ajaxToggleDebug', ['state' => 'true']);
        $this->assertResponseCode(200);
        $conf = json_decode(file_get_contents(TMP_TESTDIR . "test_local.json"));
        $this->assertNotEmpty($conf);
        $this->assertTrue($conf->debug);
    }

    /**
     * testNotifyAll
     */
    public function testNotifyAll()
    {
        $this->httpGet('/admins/notifyAll');
        $this->assertResponseCode(200);
        $this->httpPost('/admins/notifyAll', ['activeusers' => true, 'msg' => 'test', 'color' => 'alert-info']);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testLogin
     */
    public function testLogin()
    {
        $this->session(
            [
                'Admin' => ['tech' => false],
                'Auth' => null,
            ]
        );
        $this->httpGet('/admins/login');
        $this->assertResponseCode(200);

        $this->post('/admins/login', ['username' => 'admin', 'password' => 'admin']);
        $this->assertRedirect('/admins');
    }

    /**
     * testLogout
     */
    public function testLogout()
    {
        $this->get('/admins/logout');
        $this->assertRedirect('/admins/login');
    }

    /**
     * testStopAllWorkers
     */
    public function testStopAllWorkers()
    {
        $this->httpGet('/admins/stopAllWorkers');
        $this->assertResponseCode(200);
    }

    /**
     * testStopWorker
     */
    public function testStopWorker()
    {
        /** @var BeanstalkWorkersTable $workers */
        $workers = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        $workers->sync = true;
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admins/stopWorker/1');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testAjaxEditConf
     */
    public function testAjaxEditConf()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admins/ajaxEditConf');
        $this->assertResponseCode(200);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost(
            '/admins/ajaxEditConf',
            [
                'config' => '{}',
                'app_fullbaseurl' => 'http://libriciel.fr',
                'ignore_invalid_fullbaseurl' => true,
                'password_complexity' => '',
                'datacompressor_encodefilename' => '',
                'filesystem_useshred' => '',
                'tokens-duration_code' => '',
                'tokens-duration_access-token' => '',
                'tokens-duration_refresh-token' => '',
                'beanstalk_tests_timeout' => '',
                'downloads_asyncfilecreationtimelimit' => '',
                'downloads_tempfileconservationtimelimit' => '',
                'proxy_host' => '',
                'proxy_port' => '',
                'proxy_username' => '',
                'proxy_password' => '',
                'paginator_limit' => 20,
                'ajaxpaginator_limit' => 100,
            ]
        );
        $this->assertResponseCode(200);
        $conf = json_decode(file_get_contents(TMP_TESTDIR . "test_local.json"));
        $this->assertNotEmpty($conf);
        $this->assertNotEmpty($conf->App);
        $this->assertEquals('http://libriciel.fr', $conf->App->fullBaseUrl);
    }

    /**
     * testDeleteLogo
     */
    public function testDeleteLogo()
    {
        $Configurations = TableRegistry::getTableLocator()->get('Configurations');
        $Configurations->deleteAll([]);
        $conf = $Configurations->newEntity(
            [
                'org_entity_id' => 1,
                'name' => 'logo-client',
                'setting' => '/testunit/deletelogo.jpg',
            ]
        );
        $Configurations->saveOrFail($conf);
        $dataDir = Configure::read('App.paths.data');
        $publicDir = Configure::read('App.paths.config', $dataDir . DS . 'config');
        $webrootDir = rtrim(Configure::read('OrgEntities.public_dir', WWW_ROOT), DS);
        Filesystem::createDummyFile($f1 = $publicDir . '/testunit/deletelogo.jpg', 1024);
        Filesystem::copy($f1, $f2 = $webrootDir . '/testunit/deletelogo.jpg');
        $this->assertFileExists($f1);
        $this->assertFileExists($f2);
        $response = $this->httpDelete('/admins/deleteLogo', [], true);
        $this->assertResponseCode(200);
        $this->assertContains('done', $response);
        $this->assertFileDoesNotExist($f1);
        $this->assertFileDoesNotExist($f2);
    }

    /**
     * testAjaxLogin
     */
    public function testAjaxLogin()
    {
        $this->session(['Admin.tech' => false]);
        $this->httpGet('/admins/ajaxLogin');
        $this->assertResponseCode(200);

        $this->httpPost('/admins/ajaxLogin', ['name' => 'admin', 'password' => 'admin']);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testAjaxInterrupt
     */
    public function testAjaxInterrupt()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admins/ajaxInterrupt');
        $this->assertResponseCode(200);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost(
            '/admins/ajaxInterrupt',
            [
                'enabled' => true,
                'scheduled__begin' => '01/01/1950 11:20:00',
                'scheduled__end' => '01/01/2500 13:20:00',
                'periodic__begin' => '11:00:00',
                'periodic__end' => '12:00:00',
                'enable_whitelist' => true,
                'whitelist_headers_inline' => 'X-Asalae-Webservice, Foo-Bar',
                'message' => 'test',
                'message_periodic' => 'test',
                'enable_workers' => false,
            ]
        );
        $this->assertResponseCode(200);
        $conf = json_decode(file_get_contents(TMP_TESTDIR . "test_local.json"));
        $this->assertNotEmpty($conf);
        $this->assertNotEmpty($conf->Interruption);
        $this->assertTrue($conf->Interruption->enabled);
    }

    /**
     * testKillWorker
     */
    public function testKillWorker()
    {
        /** @var BeanstalkWorkersTable $workers */
        $workers = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        $workers->sync = true;
        Configure::write('Beanstalk.workers.test.kill', 'echo testunit');
        $this->httpGet('/admins/kill-worker/1');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEditServiceExploitation
     */
    public function testEditServiceExploitation()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admins/edit-service-exploitation');
        $this->assertResponseCode(200);

        $data = [
            'name' => 'testunit',
            'identifier' => 'testunit',
        ];
        $this->setAjaxRequest();
        $this->httpPut('/admins/edit-service-exploitation', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * Requete type ajax
     */
    private function setAjaxRequest()
    {
        $this->configRequest(
            [
                'headers' => [
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );
    }

    /**
     * testIndexServicesArchives
     */
    public function testIndexServicesArchives()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admins/index-services-archives');
        $this->assertResponseCode(200);
    }

    /**
     * testAddServiceArchive
     */
    public function testAddServiceArchive()
    {
        $loc = TableRegistry::getTableLocator();
        $SecureDataSpaces = $loc->get('SecureDataSpaces');
        $SecureDataSpaces->saveOrFail(
            $ecs = $SecureDataSpaces->newEntity(
                [
                    'name' => 'ECS-003',
                    'description' => 'available',
                    'created' => '2021-12-09T13:54:15',
                    'modified' => '2021-12-09T13:54:15',
                    'org_entity_id' => null,
                    'is_default' => false,
                ]
            )
        );

        $Volumes = $loc->get('Volumes');
        $Volumes->saveOrFail(
            $Volumes->newEntity(
                [
                    'name' => 'volume04',
                    'fields' => json_encode(['path' => TMP_TESTDIR . DS . 'vol4']),
                    'created' => '2021-12-09T13:54:15',
                    'modified' => '2021-12-09T13:54:15',
                    'driver' => 'FILESYSTEM',
                    'description' => '',
                    'active' => true,
                    'alert_rate' => null,
                    'disk_usage' => null,
                    'max_disk_usage' => 10000000000,
                    'secure_data_space_id' => $ecs->get('id'),
                    'read_duration' => 0.00856686,
                ]
            )
        );

        $this->setAjaxRequest();
        $this->httpGet('/admins/add-service-archive');
        $this->assertResponseCode(200);

        $this->setAjaxRequest();
        $this->httpPost('/admins/add-service-archive', ['default_secure_data_space_id' => 3]);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'name' => 'testunit',
            'identifier' => 'testunit',
            'timestamper_id' => 1,
            'default_secure_data_space_id' => 3,
            'timestampers' => [
                '_ids' => [3],
            ],
        ];
        $this->setAjaxRequest();
        $this->httpPost('/admins/add-service-archive', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEditServiceArchive
     */
    public function testEditServiceArchive()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admins/edit-service-archive/2');
        $this->assertResponseCode(200);

        $this->setAjaxRequest();
        $this->httpPut('/admins/edit-service-archive/2');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'name' => 'testunit',
            'identifier' => 'testunit',
            'timestamper_id' => 1,
            'default_secure_data_space_id' => 1,
            'timestampers' => [
                '_ids' => [1],
            ],
        ];
        $this->setAjaxRequest();
        $this->httpPut('/admins/edit-service-archive/2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testImportLdapUser
     */
    public function testImportLdapUser()
    {
        $Ldaps = TableRegistry::getTableLocator()->get('Ldaps');
        $ldapEntity = $Ldaps->newEntity(
            [
                'org_entity_id' => 2,
                'name' => 'test',
                'host' => 'localhost',
                'port' => '12345',
                'ldap_root_search' => '(*)',
                'user_login_attribute' => 'sAMAccountName',
                'user_name_attribute' => 'displayname',
                'user_mail_attribute' => 'mail',
                'user_username_attribute' => 'sAMAccountName',
            ]
        );
        $Ldaps->saveOrFail($ldapEntity);
        $ldap = $this->createMock(Ldap::class);
        $ldap->method('search')->willReturn('');

        $entry = $this->createMock(Entry::class);
        $entry->method('getAttribute')->willReturnCallback(
            function ($attr) {
                $return = [
                    'sAMAccountName' => 'testldap',
                    'displayname' => 'ldapuser',
                    'mail' => 'ldap@testunit.org',
                    'cn' => 'cn',
                ];
                return isset($return[$attr]) ? [$return[$attr]] : [];
            }
        );
        $paginator = $this->createMock(Paginator::class);
        $paginator->method('count')->willReturn(1);
        $paginator->method('getIterator')->willReturn(new ArrayIterator([$entry]));
        $search = $this->createMock(\Adldap\Query\Factory::class);
        $search->method('__call')
            ->willReturnCallback(
                function ($name) use ($entry, $paginator) {
                    switch ($name) {
                        case 'paginate':
                            return $paginator;
                        case 'firstOrFail':
                            return $entry;
                    }
                }
            );
        $search->method('newQuery')->willReturnSelf();

        $provider = $this->createMock(Provider::class);
        $provider->method('search')->willReturn($search);

        $ad = $this->createMock(Adldap::class);
        $ad->method('addProvider')->willReturnSelf();
        $ad->method('connect')->willReturn($provider);
        Utility::set(Adldap::class, $ad);

        $response = $this->httpGet('/admins/get-ldap-users/' . $ldapEntity->id, true);
        $this->assertResponseCode(200);
        $this->assertEquals([['id' => 'cn', 'text' => 'ldapuser']], $response['results']);

        $this->httpGet('/admins/import-ldap-user/2');
        $this->assertResponseCode(200);

        $data = [
            'ldap_id' => 1,
            'ldap_user' => 'ldapuser',
            'role_id' => 1,
            'is_validator' => true,
            'use_cert' => false,
        ];

        $Users = TableRegistry::getTableLocator()->get('Users');
        $user = $Users->newEntity(
            [
                'username' => 'ldap_user',
                'active' => true,
                'role_id' => 1,
                'org_entity_id' => 2,
                'email' => 'test@test.fr',
                'name' => 'test ldap',
                'ldap_id' => $ldapEntity->id,
                'agent_type' => 'person',
                'is_validator' => 1,
                'use_cert' => 0,
                'ldap_login' => 'ldap_user',
            ]
        );
        $Users->saveOrFail($user);
        $response = $this->httpPost('admins/importLdapUser/2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertEquals('testldap', $response->username);
    }

    /**
     * testAddEaccpf
     */
    public function testAddEaccpf()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admins/add-eaccpf/2');
        $this->assertResponseCode(200);

        $this->setAjaxRequest();
        $this->httpPost('/admins/add-eaccpf/2');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        /** @noinspection PhpUndefinedMethodInspection __callStatic */
        $data = [
            'record_id' => 'sa',
            'name' => 'test',
            'agency_name' => 'test',
            'entity_id' => 'test',
            'entity_type' => 'test',
            'from_date' => (string)Date::parse('now'),
            'to_date' => null,
        ];
        $this->setAjaxRequest();
        $this->httpPost('/admins/add-eaccpf/2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testAddUser
     */
    public function testAddUser()
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        $this->setAjaxRequest();
        $this->httpGet('/admins/add-user/2');
        $this->assertResponseCode(200);

        $this->setAjaxRequest();
        $this->httpPost('/admins/add-user/2');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'username' => 'test',
            'name' => 'testunit',
            'email' => 'testunit@test.fr',
            'high_contrast' => false,
            'is_validator' => true,
            'use_cert' => false,
        ];
        $this->setAjaxRequest();
        $this->httpPost('/admins/add-user/2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testTail
     */
    public function testTail()
    {
        file_put_contents(Configure::read('App.paths.logs', LOGS) . 'testunit.log', 'test');
        ob_start();
        $this->httpGet('/admins/tail/testunit.log/0.1');
        $partialEmit = ob_get_clean();
        $this->assertResponseCode(200);
        $this->assertEquals('test', $partialEmit);
        unlink(Configure::read('App.paths.logs', LOGS) . 'testunit.log');
    }

    /**
     * testTestTimestamper
     */
    public function testTestTimestamper()
    {
        $Timestampers = TableRegistry::getTableLocator()->get('Timestampers');
        $timestamper = $Timestampers->newEntity(
            [
                'name' => 'test',
                'fields' => '{"driver":"test","name":"test","myfield":"test"}',
            ]
        );
        $Timestampers->save($timestamper);
        Configure::write(
            'Timestamping.drivers.test',
            [
                'name' => 'test',
                'class' => GenericCaller::class,
                'fields' => ['myfield' => ['label' => 'my field']],
            ]
        );
        GenericCaller::$methods['check'] = true;
        $response = $this->httpPost('/admins/testTimestamper/' . $timestamper->get('id'));
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->success));
    }

    /**
     * testTimestampers
     */
    public function testTimestampers()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admins/timestampers');
        $this->assertResponseCode(200);
    }

    /**
     * testAddTimestamper
     */
    public function testAddTimestamper()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admins/add-timestamper');
        $this->assertResponseCode(200);

        $this->setAjaxRequest();
        $response = $this->httpPost(
            '/admins/add-timestamper',
            [
                'name' => 'test',
                'class' => GenericCaller::class,
                'url' => 'https://libriciel.fr',
            ]
        );
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->id));
    }

    /**
     * testEditTimestamper
     */
    public function testEditTimestamper()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admins/edit-timestamper/1');
        $this->assertResponseCode(200);

        $this->setAjaxRequest();
        $response = $this->httpPut(
            '/admins/edit-timestamper/1',
            [
                'name' => 'test',
                'class' => GenericCaller::class,
                'url' => 'https://libriciel.fr',
            ]
        );
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->url));
        $this->assertEquals('https://libriciel.fr', $response->url);
    }

    /**
     * testDeleteTimestamper
     */
    public function testDeleteTimestamper()
    {
        $this->setAjaxRequest();
        $response = $this->httpDelete('/admins/delete-timestamper/1');
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->report));
        $this->assertStringNotContainsString('done', $response->report);

        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $OrgEntities->updateAll(['timestamper_id' => null], []);
        $OrgEntitiesTimestampers = TableRegistry::getTableLocator()->get('OrgEntitiesTimestampers');
        $OrgEntitiesTimestampers->deleteAll([]);
        $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
        $ServiceLevels->updateAll(
            [
                'ts_msg_id' => null,
                'ts_pjs_id' => null,
                'ts_conv_id' => null,
            ],
            []
        );
        $response = $this->httpDelete('/admins/delete-timestamper/1');
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->report));
        $this->assertStringContainsString('done', $response->report);
    }

    /**
     * testIndexVolumes
     */
    public function testIndexVolumes()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admins/index-volumes');
        $this->assertResponseCode(200);
    }

    /**
     * testDeleteVolume
     */
    public function testDeleteVolume()
    {
        $this->setAjaxRequest();
        $response = $this->httpDelete('/admins/delete-volume/1');
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->report));
        $this->assertStringNotContainsString('done', $response->report);

        $SecureDataSpaces = TableRegistry::getTableLocator()->get('Volumes');
        $SecureDataSpaces->updateAll(['secure_data_space_id' => null], ['id' => 1]);
        $response = $this->httpDelete('/admins/delete-volume/1');
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->report));
        $this->assertStringContainsString('done', $response->report);
    }

    /**
     * testAddVolume
     */
    public function testAddVolume()
    {
        $this->httpGet('/admins/add-volume');
        $this->assertResponseCode(200);

        $response = $this->httpPost(
            '/admins/add-volume',
            [
                'name' => 'test',
                'description' => '',
                'driver' => 'FILESYSTEM',
                'active' => true,
                'alert_rate' => '',
                'fields' => [0 => ['path' => TMP_TESTDIR]],
            ]
        );
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->id));
    }

    /**
     * testEditVolume
     */
    public function testEditVolume()
    {
        TableRegistry::getTableLocator()->get('StoredFilesVolumes')->deleteAll([]);
        $this->httpGet('/admins/edit-volume/1');
        $this->assertResponseCode(200);

        $response = $this->httpPut(
            '/admins/edit-volume/1',
            [
                'name' => 'test',
                'description' => '',
                'driver' => 'FILESYSTEM',
                'active' => true,
                'alert_rate' => '',
                'fields' => [0 => ['path' => TMP_TESTDIR]],
            ]
        );
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->id));
    }

    /**
     * testChangeVolumesInSpace
     */
    public function testChangeVolumesInSpace()
    {
        $Crons = TableRegistry::getTableLocator()->get('Crons');
        $initialCount = $Crons->find()->count();
        $this->httpGet('/admins/changeVolumesInSpace/1');
        $this->assertResponseCode(200);

        $data = [
            'add' => [],
            'remove' => [1],
            'cron' => new DateTime(),
        ];
        $this->httpPost('/admins/changeVolumesInSpace/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');

        $this->assertEquals($initialCount + 1, $Crons->find()->count());
    }

    /**
     * testTestVolume
     */
    public function testTestVolume()
    {
        $dir = TMP_VOL1;
        if (!is_dir($dir)) {
            Filesystem::mkdir($dir);
        }
        $response = $this->httpPost('/admins/testVolume/1');
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->success));
    }

    /**
     * testAddSpace
     */
    public function testAddSpace()
    {
        $this->httpGet('/admins/add-space');
        $this->assertResponseCode(200);

        $response = $this->httpPost(
            '/admins/add-space',
            [
                'name' => 'test',
                'description' => '',
                'volumes' => [
                    '_ids' => [1],
                ],
            ]
        );
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->id));
    }

    /**
     * testEditSpace
     */
    public function testEditSpace()
    {
        $this->httpGet('/admins/edit-space/1');
        $this->assertResponseCode(200);

        $response = $this->httpPut(
            '/admins/edit-space/1',
            [
                'name' => 'test',
                'description' => '',
                'org_entity_id' => 2,
                'volumes' => [
                    '_ids' => [1],
                ],
            ]
        );
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->id));
    }

    /**
     * testDeleteSpace
     */
    public function testDeleteSpace()
    {
        $SecureDataSpaces = TableRegistry::getTableLocator()->get('SecureDataSpaces');
        $space = $SecureDataSpaces->newEntity(
            [
                'name' => 'test',
            ]
        );
        $SecureDataSpaces->saveOrFail($space);

        $response = $this->httpDelete('/admins/delete-space/' . $space->id);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->report));
        $this->assertStringContainsString('done', $response->report);
    }

    /**
     * testIndexCrons
     */
    public function testIndexCrons()
    {
        $this->httpGet('/admins/index-crons');
        $this->assertResponseCode(200);
    }

    /**
     * testEditCrons
     */
    public function testEditCrons()
    {
        $this->httpGet('/admins/edit-cron/1');
        $this->assertResponseCode(200);

        $data = [
            'next' => '2100-01-01 12:00:00',
            'frequency_build' => '1',
            'frequency_build_unit' => 'P_D',
            'active' => true,
            'hidden_locked' => true,
            'locked' => false,
            'use_proxy' => false,
            'update_url' => '',
            'update_md5' => '',
        ];
        $response = $this->httpPut('/admins/edit-cron/1', $data);
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->id));
        $this->assertTrue(isset($response->next));
        $this->assertTextContains('2100', $response->next);

        $data['next'] = '01/01/2100 12:00:00';
        $response = $this->httpPut('/admins/edit-cron/1', $data);
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->id));
        $this->assertTrue(isset($response->next));
        $this->assertTextContains('2100', $response->next);
    }

    /**
     * testViewCrons
     */
    public function testViewCrons()
    {
        $this->httpGet('/admins/view-cron/1');
        $this->assertResponseCode(200);
    }

    /**
     * testRunCron
     */
    public function testRunCron()
    {
        $this->httpGet('/admins/run-cron/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testGetCronState
     */
    public function testGetCronState()
    {
        $response = $this->httpGet('/admins/get-cron-state/1');
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->id));
    }

    /**
     * testAddWorker
     */
    public function testAddWorker()
    {
        $this->httpGet('/admins/add-worker');
        $this->assertResponseCode(200);

        $this->httpPost(
            '/admins/add-worker',
            [
                'unique' => true,
                'one-job' => true,
                'tube' => 'testunit',
                'name' => 'test',
            ]
        );
        $this->assertResponseCode(200);
    }

    /**
     * testWorkerLog
     */
    public function testWorkerLog()
    {
        $exec = $this->createMock(Exec::class);
        $cmdOutput = new CommandResult(
            [
                'success' => true,
                'code' => 0,
                'stdout' => 'the log value',
                'stderr' => '',
            ]
        );
        $exec->method('command')->willReturn($cmdOutput);
        Factory\Utility::set('Exec', $exec);
        $this->httpGet('/admins/worker-log/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('the log value');
    }

    /**
     * testPhpInfo
     */
    public function testPhpInfo()
    {
        $this->httpGet('/admins/php-info');
        $this->assertResponseCode(200);
        $this->assertResponseContains('upload_max_filesize');
    }

    /**
     * testIndexLdaps
     */
    public function testIndexLdaps()
    {
        $this->httpGet('/admins/index-ldaps');
        $this->assertResponseCode(200);
    }

    /**
     * testAddLdap
     */
    public function testAddLdap()
    {
        $this->httpGet('/admins/add-ldap');
        $this->assertResponseCode(200);

        $data = [
            'org_entity_id' => 2,
            'name' => 'test',
            'host' => '127.0.0.1',
            'port' => 389,
            'user_query_login' => 'test@adullact.win',
            'user_query_password' => 'test',
            'ldap_root_search' => 'dc=adullact,dc=win',
            'user_login_attribute' => 'sAMAccountName',
            'user_username_attribute' => 'sAMAccountName',
            'ldap_users_filter' => '(memberOf=cn=asalae,OU=Groupes,dc=adullact,dc=win)',
            'account_prefix' => '',
            'account_suffix' => '@adullact.win',
            'description' => 'Pour tester la connexion LDAP',
            'use_proxy' => false,
            'use_ssl' => false,
            'use_tls' => false,
            'user_name_attribute' => 'displayname',
            'user_mail_attribute' => 'mail',
        ];
        $this->httpPost('/admins/add-ldap', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testEditLdap
     */
    public function testEditLdap()
    {
        $Ldaps = TableRegistry::getTableLocator()->get('Ldaps');
        $ldap = $Ldaps->newEntity(
            [
                'name' => 'test',
                'host' => 'localhost',
                'port' => '12345',
                'ldap_root_search' => '(*)',
                'user_login_attribute' => 'samaccountname',
            ]
        );
        $Ldaps->saveOrFail($ldap);
        $this->httpGet('/admins/edit-ldap/' . $ldap->id);
        $this->assertResponseCode(200);

        $data = [
            'name' => 'foo',
        ];
        $this->httpPut('/admins/edit-ldap/' . $ldap->id, $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testDeleteLdap
     */
    public function testDeleteLdap()
    {
        $Ldaps = TableRegistry::getTableLocator()->get('Ldaps');
        $ldap = $Ldaps->newEntity(
            [
                'name' => 'test',
                'host' => 'localhost',
                'port' => '12345',
                'ldap_root_search' => '(*)',
                'user_login_attribute' => 'samaccountname',
            ]
        );
        $Ldaps->saveOrFail($ldap);
        $this->httpDelete('/admins/delete-ldap/' . $ldap->id);
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testSetMainArchivalAgency
     */
    public function testSetMainArchivalAgency()
    {
        $this->httpGet('/admins/set-main-archival-agency/1');
        $this->assertResponseCode(200);

        $data = [
            'timestamper_id' => 1,
        ];
        $this->httpPut('/admins/set-main-archival-agency/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testSendTestMail
     */
    public function testSendTestMail()
    {
        $email = $this->getMockBuilder(Mailer::class)
            ->onlyMethods(['send'])
            ->getMock();
        $email->method('send')->willReturn(['the mail content']);
        Factory\Utility::set(Mailer::class, $email);

        $data = [
            'email' => 'test@test.fr',
        ];
        $response = $this->httpPost('/admins/send-test-mail', $data, true);
        $this->assertResponseCode(200);
        $this->assertTrue(Hash::get($response, 'success'));

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    /**
     * testIndexSessions
     */
    public function testIndexSessions()
    {
        $this->httpGet('/admins/index-sessions');
        $this->assertResponseCode(200);
    }

    /**
     * testDeleteSessions
     */
    public function testDeleteSessions()
    {
        TableRegistry::getTableLocator()->get('Sessions')->updateAll(
            ['expires' => (new DateTime())->getTimestamp() + 10],
            ['id' => '77ovlobsthtb5ngll7lts2fpcl']
        );
        $this->httpDelete('/admins/delete-session/77ovlobsthtb5ngll7lts2fpcl');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * testNotifyUser
     */
    public function testNotifyUser()
    {
        TableRegistry::getTableLocator()->get('Sessions')->updateAll(
            ['expires' => (new DateTime())->getTimestamp() + 10],
            ['id' => '77ovlobsthtb5ngll7lts2fpcl']
        );
        $this->httpGet('/admins/notify-user/77ovlobsthtb5ngll7lts2fpcl');
        $this->assertResponseCode(200);

        Factory\Utility::set('Notify', FakeEmitter::class);

        $data = [
            'msg' => 'foo',
            'color' => 'danger',
        ];
        $response = $this->httpPost(
            '/admins/notify-user/77ovlobsthtb5ngll7lts2fpcl',
            $data,
            true
        );
        $this->assertResponseCode(200);
        $this->assertTrue(Hash::get($response, 'success'));
    }

    /**
     * testIndexJobs
     */
    public function testIndexJobs()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $job = $Jobs->newEntity(
            [
                'tube' => 'test',
                'user_id' => 1,
            ]
        );
        $this->assertNotFalse($Jobs->save($job));

        $this->get('/admins/indexJobs/test');
        $this->assertResponseCode(200);

        $response = $this->httpGet('/admins/indexJobs/test?job_state[]=' . BeanstalkJobsTable::S_PENDING);
        $this->assertResponseCode(200);
        $this->assertGreaterThanOrEqual(1, $response);
    }

    /**
     * testJobInfo
     */
    public function testJobInfo()
    {
        $response = $this->httpGet('/admins/jobInfo/1', true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('id', $response);
    }

    /**
     * testJobCancel
     */
    public function testJobCancel()
    {
        /** @var BeanstalkJobsTable $Jobs */
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $Jobs->sync = true;
        $initialCount = $Jobs->find()->count();
        $response = $this->httpGet('/admins/jobCancel/1', true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('report', $response);
        $this->assertEquals('done', $response['report']);
        $this->assertCount($initialCount - 1, $Jobs->find());
    }

    /**
     * testJobResume
     */
    public function testJobResume()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $Jobs->updateAll(['job_state' => BeanstalkJobsTable::S_FAILED], ['id' => 1]);
        $query = $Jobs->find()->where(['job_state' => BeanstalkJobsTable::S_PENDING]);
        $initialCount = $query->count();
        $response = $this->httpGet('/admins/jobResume/1', true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('report', $response);
        $this->assertEquals('done', $response['report']);
        $this->assertCount($initialCount + 1, $query);
    }

    /**
     * testJobPause
     */
    public function testJobPause()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $Jobs->updateAll(['job_state' => BeanstalkJobsTable::S_PENDING], ['id' => 1]);
        $response = $this->httpGet('/admins/jobPause/1', true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('report', $response);
        $this->assertEquals('done', $response['report']);
    }

    /**
     * testAddTimestamperCert
     */
    public function testAddTimestamperCert()
    {
        $this->httpGet('/admins/add-timestamper-cert');
        $this->assertResponseCode(200);
        $dir = TMP_TESTDIR . DS . 'timestamper';
        if (!is_dir($dir)) {
            mkdir($dir);
        }

        $data = [
            "countryName" => "FR",
            "stateOrProvinceName" => "France",
            "localityName" => "Paris",
            "organizationName" => "Ma+collectivité",
            "organizationalUnitName" => "Mon+service",
            "commonName" => "Mon+nom",
            "emailAddress" => "Mon+email",
            "extfile" => TSACertForm::DEFAULT_TIMESTAMPER_TSA_EXTFILE,
            "directory" => $dir,
            "cnf" => str_replace('{{dir}}', $dir, file_get_contents(RESOURCES . 'openssl.cnf')),
            "name" => "Openssl+local",
        ];
        $this->httpPost('/admins/add-timestamper-cert', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');

        $this->assertDirectoryExists($dir);
        Filesystem::remove($dir);
    }

    /**
     * testTestTimestamperTSA
     */
    public function testTestTimestamperTSA()
    {
        $dir = TMP_TESTDIR . DS . 'timestamper';
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        $data = [
            "countryName" => "FR",
            "stateOrProvinceName" => "France",
            "localityName" => "Paris",
            "organizationName" => "Ma+collectivité",
            "organizationalUnitName" => "Mon+service",
            "commonName" => "Mon+nom",
            "emailAddress" => "Mon+email",
            "extfile" => TSACertForm::DEFAULT_TIMESTAMPER_TSA_EXTFILE,
            "directory" => $dir,
            "cnf" => str_replace('{{dir}}', $dir, file_get_contents(RESOURCES . 'openssl.cnf')),
            "name" => "Openssl+local",
        ];
        $response = $this->httpPost('/admins/test-timestamper-t-s-a', $data, true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['success']) && $response['success']);
    }
}
