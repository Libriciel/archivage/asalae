<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\TransferAttachmentsController;
use Asalae\Model\Entity\Transfer;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\TransferAttachmentsController Test Case
 */
#[UsesClass(TransferAttachmentsController::class)]
class TransferAttachmentsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;
    use SaveBufferTrait;

    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,

        // Pour ce controller
        'app.EventLogs',
        'app.TransferAttachments',
        'app.Transfers',
    ];
    public $testfile = 'transfers/1/attachments/sample.wmv';
    public $testfile2 = 'transfers/3/attachments/file1.txt';
    private $debugFixtures = false; // Dans le seda

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->testfile = TMP_TESTDIR . DS . $this->testfile;
        $this->testfile2 = TMP_TESTDIR . DS . $this->testfile2;
        Configure::write('App.paths.data', TMP_TESTDIR);
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::begin();
        Filesystem::mkdir(TMP_TESTDIR . DS . dirname($this->testfile));
        Filesystem::dumpFile($this->testfile, 'test');
        Filesystem::mkdir(TMP_TESTDIR . DS . dirname($this->testfile2));
        Filesystem::dumpFile($this->testfile2, 'test');
        $this->session($this->genericSessionData);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Filesystem::rollback();
        Filesystem::setNamespace();
        Filesystem::reset();
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $this->assertFileExists($this->testfile);
        $this->delete('/transfer-attachments/delete/1');
        $this->assertResponseCode(200);
        $this->assertFileDoesNotExist($this->testfile);

        if (is_file($xml = TMP_TESTDIR . DS . 'transfers/3/message/transfer-61b2124888ab2.xml')) {
            unlink($xml);
        }
        Filesystem::copy(
            ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS . 'transfers' . DS . '3' . DS . 'message' . DS . 'transfer-61b2124888ab2.xml',
            $xml
        );
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        /** @var Transfer $transfer */
        $transfer = $Transfers->get(3);
        Filesystem::dumpFile($meta = $transfer->getSerializedMeta(1), 'a:0:{}');
        chmod($meta, 0777);
        $this->delete('/transfer-attachments/delete/2');
        $this->assertResponseCode(200);
        $this->assertNotEquals('a:0:{}', file_get_contents($meta));
    }

    /**
     * testGetFileInfo
     */
    public function testGetFileInfo()
    {
        Filesystem::dumpFile($this->testfile, '<?xml version="1.0" encoding="UTF-8"?><Test><Info>Yes</Info></Test>');
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $data = [
            'filename' => basename($this->testfile),
        ];
        $this->post('/transfer-attachments/get-file-info/1', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['format']);
        $this->assertEquals('fmt/101', $response['format']);
        $this->assertNotEmpty($response['mime']);
        $this->assertEquals('text/xml', $response['mime']);
    }

    /**
     * testDownload
     */
    public function testDownload()
    {
        if (is_file($uri = $this->testfile2)) {
            unlink($uri);
        }
        Filesystem::createDummyFile($uri, 10);
        $content = file_get_contents($uri);

        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            'transfer-attachments/download/2'
        );
        unlink($uri);
        $this->assertResponseCode(200);
        $this->assertEquals($content, $output);
    }

    /**
     * testDownloadByName
     */
    public function testDownloadByName()
    {
        if (is_file($uri = $this->testfile2)) {
            unlink($uri);
        }
        Filesystem::createDummyFile($uri, 10);
        $content = file_get_contents($uri);

        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/transfer-attachments/downloadByName/3/' . base64_encode('file1.txt')
        );
        unlink($uri);
        $this->assertResponseCode(200);
        $this->assertEquals($content, $output);
    }

    /**
     * testPopulateSelect
     */
    public function testPopulateSelect()
    {
        if (is_file($xml = TMP_TESTDIR . DS . 'transfers/1/message/transfer-61b20d8ded833.xml')) {
            unlink($xml);
        }
        Filesystem::copy(
            ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS . 'transfers' . DS . '1' . DS . 'message' . DS . 'transfer-61b20d8ded833.xml',
            $xml
        );

        $results = $this->httpGet('/transfer-attachments/populate-select/1');
        $this->assertResponseCode(200);
        $this->assertTrue(isset($results->results[0]->children[0]->id) && $results);
        $this->assertEquals('sample.wmv', $results->results[0]->children[0]->id);

        if (is_file($uri = TMP_TESTDIR . DS . 'transfer_1_1_edit.xml')) {
            unlink($uri);
        }
        Filesystem::dumpFile($uri, file_get_contents(self::SEDA_10_XML));
        /** @var Transfer $transfer */
        $transfer = new Transfer(['id' => 1]);
        $meta = $transfer->getSerializedMeta(1);
        if (is_file($meta)) {
            unlink($meta);
        }
        Filesystem::dumpFile($meta, 'a:0:{}');

        $this->httpGet('/transfer-attachments/populate-select/1?tempfile=1');
        $this->assertResponseCode(200);
    }
}
