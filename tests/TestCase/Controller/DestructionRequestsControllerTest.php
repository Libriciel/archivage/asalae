<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\DestructionRequestsController;
use Asalae\Model\Entity\DestructionRequest;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use DOMDocument;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\DestructionRequestsController Test Case
 *
 */
#[UsesClass(DestructionRequestsController::class)]
class DestructionRequestsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use SaveBufferTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        'app.AccessRuleCodes',
        'app.AccessRules',
        'app.Agreements',
        'app.AppraisalRules',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveUnits',
        'app.ArchiveUnitsDestructionRequests',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.Counters',
        'app.DestructionRequests',
        'app.EventLogs',
        'app.Mails',
        'app.Profiles',
        'app.Sequences',
        'app.StoredFiles',
        'app.Transfers',
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationHistories',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);

        Utility::set('Notify', 'Asalae\Test\Mock\FakeClass');
        Utility::set('Beanstalk', 'Asalae\Test\Mock\FakeClass');

        Filesystem::setNamespace();
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::mkdir(TMP_TESTDIR);
        Configure::write('App.paths.data', TMP_TESTDIR);

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
    }

    /**
     * testIndexPreparating
     */
    public function testIndexPreparating()
    {
        $this->httpGet('/destruction-requests/index-preparating');
        $this->assertResponseCode(200);
    }

    /**
     * testIndexMy
     */
    public function testIndexMy()
    {
        $this->httpGet('/destruction-requests/index-my');
        $this->assertResponseCode(200);
    }

    /**
     * testIndexAll
     */
    public function testIndexAll()
    {
        $this->httpGet('/destruction-requests/index-all');
        $this->assertResponseCode(200);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $loc = TableRegistry::getTableLocator();
        $DestructionRequests = $loc->get('DestructionRequests');
        $DestructionRequests->deleteAll([]);
        $AppraisalRules = $loc->get('AppraisalRules');
        $AppraisalRules->updateAll(['final_action_code' => 'destroy'], []);
        $ArchiveUnitsDestructionRequests = $loc->get('ArchiveUnitsDestructionRequests');
        $ArchiveUnitsDestructionRequests->deleteAll([]);
        $this->httpGet('/destruction-requests/add?final_action_code=destroy&originating_agency_id=2');
        $this->assertResponseCode(200);

        $this->assertCount(1, $DestructionRequests->find());
        $this->assertGreaterThanOrEqual(1, $ArchiveUnitsDestructionRequests->find()->count());
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->httpGet('/destruction-requests/edit/1');
        $this->assertResponseCode(200);
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $loc = TableRegistry::getTableLocator();
        $DestructionRequests = $loc->get('DestructionRequests');
        $DestructionRequests->updateAll(['state' => 'creating'], ['id' => 1]);
        $this->httpDelete('/destruction-requests/delete/1');
        $this->assertResponseCode(200);
    }

    /**
     * testPaginateArchiveUnits
     */
    public function testPaginateArchiveUnits()
    {
        $this->httpGet('/destruction-requests/paginate-archive-units/1');
        $this->assertResponseCode(200);
    }

    /**
     * testRemoveArchiveUnit
     */
    public function testRemoveArchiveUnit()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnitsDestructionRequests = $loc->get('ArchiveUnitsDestructionRequests');
        $auDr = $ArchiveUnitsDestructionRequests->newEntity(
            [
                'archive_unit_id' => 1,
                'destruction_request_id' => 1,
            ]
        );
        $ArchiveUnitsDestructionRequests->saveOrFail($auDr);
        $DestructionRequests = $loc->get('DestructionRequests');
        $DestructionRequests->updateAll(['state' => 'creating'], ['id' => 1]);
        $this->httpDelete('/destruction-requests/remove-archive-unit/1/1');
        $this->assertResponseCode(200);
    }

    /**
     * testSend
     */
    public function testSend()
    {
        $loc = TableRegistry::getTableLocator();
        $DestructionRequests = $loc->get('DestructionRequests');
        $DestructionRequests->updateAll(['state' => 'creating'], ['id' => 1]);
        $ArchiveUnitsDestructionRequests = $loc->get('ArchiveUnitsDestructionRequests');
        $auDr = $ArchiveUnitsDestructionRequests->newEntity(
            [
                'archive_unit_id' => 1,
                'destruction_request_id' => 1,
            ]
        );
        $ArchiveUnitsDestructionRequests->saveOrFail($auDr);
        $ValidationProcesses = $loc->get('ValidationProcesses');
        $initialCount = $ValidationProcesses->find()->count();
        $req = $DestructionRequests->get(1);
        $this->assertEquals('creating', $req->get('state'));

        $this->httpPost('/destruction-requests/send/1');

        $this->assertResponseCode(200);
        $req = $DestructionRequests->get(1);
        $this->assertFileExists($xml = $req->get('xml'));
        $dom = new DOMDocument();
        $dom->load($xml);
        $this->assertTrue($dom->schemaValidate(SEDA_V21_XSD));
        $this->assertEquals('validating', $req->get('state'));
        $this->assertCount($initialCount + 1, $ValidationProcesses->find());
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpGet('/destruction-requests/view/1');
        $this->assertResponseCode(200);
    }

    /**
     * testDownloadPdf
     */
    public function testDownloadPdf()
    {
        $DestructionRequests = TableRegistry::getTableLocator()->get('DestructionRequests');
        /** @var DestructionRequest $dest */
        $dest = $DestructionRequests->find()
            ->where(['DestructionRequests.id' => 1])
            ->contain(['ArchivalAgencies', 'OriginatingAgencies'])
            ->firstOrFail();
        Filesystem::dumpFile($dest->get('xml'), $dest->generateXml());
        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/destruction-requests/downloadPdf/1'
        );
        $pdf = TMP_TESTDIR . DS . $dest->get('pdf_basename');
        Filesystem::dumpFile($pdf, $output);
        $this->assertEquals('application/pdf', mime_content_type($pdf));
    }
}
