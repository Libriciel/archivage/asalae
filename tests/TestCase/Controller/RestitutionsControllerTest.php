<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\RestitutionsController;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Table\RestitutionsTable;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;
use stdClass;

/**
 * Asalae\Controller\RestitutionsController Test Case
 */
#[UsesClass(RestitutionsController::class)]
class RestitutionsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_VOLUMES,
        'app.ArchiveBinaries',
        'app.ArchiveBinariesTechnicalArchiveUnits',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveUnits',
        'app.ArchiveUnitsRestitutionRequests',
        'app.Archives',
        'app.ArchivesRestitutionRequests',
        'app.EventLogs',
        'app.RestitutionRequests',
        'app.Restitutions',
        'app.TechnicalArchives',
        'app.TechnicalArchiveUnits',
        'app.ValidationChains',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Configure::write('App.paths.data', TMP_TESTDIR);
        VolumeSample::init();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testRetrieval
     */
    public function testRetrieval()
    {
        TableRegistry::getTableLocator()->get('Restitutions')
            ->updateAll(['state' => 'available'], ['id' => 1]);
        $response = $this->httpGet('/restitutions/retrieval');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testDownload
     */
    public function testDownload()
    {
        $restitution = TableRegistry::getTableLocator()->get('Restitutions')->get(1);
        $zipPath = $restitution->get('zip');
        Filesystem::dumpFile($zipPath, hex2bin('504B0304'));
        $this->get('/restitutions/download/1');
        $this->assertResponseCode(200);
        $this->assertHeaderContains('Content-Type', 'application/zip');
    }

    /**
     * testAcquittal
     */
    public function testAcquittal()
    {
        TableRegistry::getTableLocator()->get('Restitutions')
            ->updateAll(['state' => 'downloaded'], ['id' => 1]);
        $response = $this->httpGet('/restitutions/acquittal');
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->data));
        $this->assertNotEmpty($response->data);
    }

    /**
     * testAcquit
     */
    public function testAcquit()
    {
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        $restitution = TableRegistry::getTableLocator()->get('Restitutions')->get(1);
        $zipPath = $restitution->get('zip');
        Filesystem::dumpFile($zipPath, hex2bin('504B0304'));

        $RestitutionRequests = TableRegistry::getTableLocator()->get('RestitutionRequests');
        $RestitutionRequests->updateAll(['state' => RestitutionRequestsTable::S_RESTITUTION_DOWNLOADED], ['id' => 1]);
        $Restitutions = TableRegistry::getTableLocator()->get('Restitutions');
        $Restitutions->updateAll(['state' => RestitutionsTable::S_DOWNLOADED], ['id' => 1]);
        $this->httpDelete('/restitutions/acquit/1');
        $this->assertResponseCode(200);
        $this->assertFalse($Restitutions->exists(['id' => 1]));
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpGet('/restitutions/view/1');
        $this->assertResponseCode(200);
    }
}
