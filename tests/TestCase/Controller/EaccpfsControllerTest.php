<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\EaccpfsController;
use Asalae\Model\Table\EaccpfsTable;
use Asalae\Test\Mock\MockedDOMDocument;
use AsalaeCore\Model\Table\FileuploadsTable;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\XmlUtility;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Datacompressor\Utility\DataCompressor;
use DOMDocument;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(EaccpfsController::class)]
class EaccpfsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;
    use SaveBufferTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,

        // Pour ce controller
        'app.Eaccpfs',
        'app.EventLogs',
        'app.Fileuploads',
        'app.Mediainfos',
        'app.Siegfrieds',
    ];
    public $eaccpfTest = ASALAE_CORE_TEST_DATA . DS . 'sample-eaccpf.xml';
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Configure::write('App.defaultLocale', 'fr_FR');
        Filesystem::reset();
        Filesystem::begin('testunit');
        EaccpfsTable::$domDocumentClassname = MockedDOMDocument::class;
        XmlUtility::$domClass = MockedDOMDocument::class;
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Filesystem::rollback('testunit');
        Filesystem::setNamespace();
        Filesystem::reset();
        EaccpfsTable::$domDocumentClassname = DOMDocument::class;
        XmlUtility::$domClass = DOMDocument::class;
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/eaccpfs/index');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet(
            '/eaccpfs/index?SaveFilterSelect=&dateoperator_created[0]=>&created[0]=01%2F01%2F1900&record_id[0]=%3F*&entity_type[0]=&entity_type[0][]=person&entity_type[0][]=corporateBody&sort=favorite&direction=asc'
        );
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet(
            '/eaccpfs/index?SaveFilterSelect=&dateoperator_created[0]=>&created[0]=01%2F01%2F1900&record_id[0]=%3F*&entity_type[0][0]=person&entity_type[0][1]=corporateBody&favoris[0]=1&sort=record_id&direction=asc'
        );
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertEmpty($response['data']);
    }

    /**
     * testAdd
     */
    public function testAdd()
    {
        $this->configRequest(
            $req = [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );
        $this->httpGet('/eaccpfs/add/2');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs');
        $this->assertEmpty($Eaccpfs->get(2)->get('to_date'));

        $this->configRequest($req);
        $data = ['to_date' => '28/06/2018'];
        $this->httpPut('/eaccpfs/add/2', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);

        $this->assertNotEmpty($Eaccpfs->get(2)->get('to_date'));

        $Eaccpfs->deleteAll([]);
        $this->configRequest($req);
        $data = [
            'record_id' => 'sa',
            'entity_id' => null,
            'entity_type' => 'corporateBody',
            'name' => 'DSI Libriciel',
            'agency_name' => 'DSI Libriciel',
            'org_entity_id' => 2,
            'from_date' => '28/06/2018',
            'to_date' => null,
        ];
        $this->post('/eaccpfs/add/2', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response[0]['record_id']);
        $this->assertEquals('sa', $response[0]['record_id']);

        $new = $Eaccpfs->find()->where(['record_id' => 'sa'])->first();
        $this->assertNotEmpty($new);
    }

    /**
     * testAddByUpload
     */
    public function testAddByUpload()
    {
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs');

        $file = $Fileuploads->newUploadedEntity('eaccpf.xml', $this->eaccpfTest, 1);
        $this->assertNotFalse($Fileuploads->save($file, ['validate' => 'eaccpfs']));

        $this->configRequest(
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );
        $this->httpPost('/eaccpfs/addByUpload/2/' . $file->get('id'));
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response[0]['record_id']);
        $this->assertEquals('sa', $response[0]['record_id']);

        $new = $Eaccpfs->find()->where(['record_id' => 'sa'])->first();
        $this->assertNotEmpty($new);
    }

    /**
     * testDelete
     */
    public function testDelete()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpDelete('/eaccpfs/delete/2');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']);
        $this->assertEquals('done', $response['report']);

        $Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs');
        $new = $Eaccpfs->find()->where(['id' => 2])->first();
        $this->assertEmpty($new);
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->httpGet('/eaccpfs/edit/2');
        $this->assertResponseCode(200);

        $data = [
            'data' => json_encode(
                [
                    "eac-cpf" => [
                        "control" => [
                            "recordId" => "sa",
                            "maintenanceStatus" => "new",
                            "maintenanceAgency" => [
                                "agencyName" => "phpunit",
                            ],
                            "maintenanceHistory" => [
                                "maintenanceEvent" => [
                                    [
                                        "eventType" => "created",
                                        "eventDateTime" => [
                                            "@standardDateTime" => "2019-03-21T14:51:35",
                                            "@" => "2019-03-21T14:51:35",
                                        ],
                                        "agentType" => "test",
                                        "agent" => "phpunit",
                                        "eventDescription" => "Created from Asalae",
                                    ],
                                ],
                            ],
                        ],
                        "cpfDescription" => [
                            "identity" => [
                                "entityId" => "",
                                "entityType" => "corporateBody",
                                "nameEntry" => [
                                    "part" => "test",
                                ],
                            ],
                            "description" => [
                                "existDates" => [
                                    "date" => null,
                                ],
                            ],
                        ],
                        "@xmlns" => "urn:isbn:1-931666-33-4",
                        "@xmlns:xlink" => "http://www.w3.org/1999/xlink",
                    ],
                ],
                JSON_UNESCAPED_SLASHES
            ),
        ];
        $this->httpPut('/eaccpfs/edit/2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * testDownload
     */
    public function testDownload()
    {
        $Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs');
        $eaccpf = $Eaccpfs->get(2);

        $md5Initial = md5($eaccpf->get('XML'));
        $this->httpGet('/eaccpfs/download/2');
        $response = (string)$this->_response->getBody();
        $this->assertResponseCode(200);
        $this->assertEquals($md5Initial, md5($response));
    }

    /**
     * testZip
     */
    public function testZip()
    {
        $Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs');
        $cpfs = $Eaccpfs->find()->limit(2)->all()->toArray();
        $file1 = TMP_TESTDIR . DS . uniqid('test-');
        $file2 = TMP_TESTDIR . DS . uniqid('test-');
        Filesystem::dumpFile($file1, current($cpfs)->get('XML'));
        Filesystem::dumpFile($file2, next($cpfs)->get('XML'));
        $md5Initial2 = md5_file($file2);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);

        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/eaccpfs/zip/my_cpfs.zip?id[]=2'
        );
        $this->assertResponseCode(200);
        $location = TMP_TESTDIR . DS . 'test-zip.zip';
        Filesystem::dumpFile($location, $output);
        $unzipDir = TMP_TESTDIR . DS . 'testdir-unzip';
        DataCompressor::useTransaction('testunit');
        DataCompressor::uncompress($location, $unzipDir);
        $this->assertFileExists($unzipDir . DS . 'sa_1.xml');
        $this->assertEquals($md5Initial2, md5_file($unzipDir . DS . 'sa_1.xml'));
    }
}
