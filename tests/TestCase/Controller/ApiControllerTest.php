<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\ApiController;
use AsalaeCore\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\ArchiveBinariesController Test Case
 */
#[UsesClass(ApiController::class)]
class ApiControllerTest extends TestCase
{
    use IntegrationTestTrait;

    public array $fixtures = [
        "app.Configurations",
        "app.Ldaps",
        "app.OrgEntities",
        "app.Roles",
        "app.TypeEntities",
        "app.Users",
    ];

    /**
     * testSwagger
     */
    public function testSwagger()
    {
        $this->get('/api');
        $this->_response->getBody()->rewind();
        $this->assertResponseCode(302);
    }
}
