<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\DownloadController;
use AsalaeCore\Model\Table\FileuploadsTable;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Notify;
use Cake\Core\Configure;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Datacompressor\Utility\DataCompressor;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(DownloadController::class)]
class DownloadControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use InvokePrivateTrait;
    use AutoFixturesTrait;
    use SaveBufferTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,

        // Pour ce controller
        'app.EventLogs',
        'app.Fileuploads',
        'app.MediainfoTexts',
        'app.Mediainfos',
        'app.Siegfrieds',
    ];
    public $imgTest = ASALAE_CORE_TEST_DATA . DS . 'logo.png';
    public $thumb = TMP . 'thumbnail' . DS . 'user_1' . DS . 'logo-test-unit.png';
    public $pdfTest = ASALAE_CORE_TEST_DATA . DS . 'sample.pdf';
    public $odtTest = ASALAE_CORE_TEST_DATA . DS . 'sample.odt';
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Filesystem::reset();
        Filesystem::begin('testunit');
        Notify::$instance = $this->createMock(Notify::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_file($this->thumb)) {
            unlink($this->thumb);
        }
        Filesystem::rollback('testunit');
        Filesystem::setNamespace();
        Filesystem::reset();
    }

    /**
     * testThumbnailImage
     */
    public function testThumbnailImage()
    {
        // Création de l'image
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $entity = $Fileuploads->newUploadedEntity('logo-test-unit.png', $this->imgTest, 1);
        $Fileuploads->save($entity, ['silent' => true]);

        foreach (['gd', 'imagick'] as $driver) {
            if (is_file($this->thumb)) {
                unlink($this->thumb);
            }
            clearstatcache();
            Configure::write('DriverImage', $driver);
            $output = $this->preserveBuffer(
                [$this, 'httpGet'],
                '/download/thumbnailImage/' . $entity->get('id')
            );
            $this->assertResponseCode(200, 'Driver: ' . $driver);
            $this->assertHeaderContains('Content-Type', 'image/png');
            $this->assertEquals(md5($output), md5_file($this->thumb));
            $this->assertNotEmpty($this->_response->getHeader('Content-Length'));
            $this->assertNotEmpty($this->_response->getHeader('Content-Length')[0]);
        }
    }

    /**
     * testFile
     */
    public function testFile()
    {
        /** @var \AsalaeCore\Model\Table\FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $entity = $Fileuploads->newUploadedEntity('logo-test-unit.png', $this->imgTest, 1);
        $Fileuploads->save($entity, ['silent' => true]);

        $md5Initial = md5_file($this->imgTest);
        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/download/file/' . $entity->get('id')
        );
        $this->assertResponseCode(200);
        $this->assertEquals($md5Initial, md5($output));
    }

    /**
     * testZip
     */
    public function testZip()
    {
        $file1 = TMP_TESTDIR . DS . uniqid('test-');
        $file2 = TMP_TESTDIR . DS . uniqid('test-');
        Filesystem::createDummyFile($file1, 500);
        Filesystem::createDummyFile($file2, 500);
        $md5Initial1 = md5_file($file1);
        $md5Initial2 = md5_file($file2);

        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $entity1 = $Fileuploads->newUploadedEntity('file1.txt', $file1, 1);
        $entity2 = $Fileuploads->newUploadedEntity('file2.txt', $file2, 1);
        $Fileuploads->save($entity1, ['silent' => true]);
        $Fileuploads->save($entity2, ['silent' => true]);

        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/download/zip/my_uploads.zip?id=' . $entity1->get('id') . ',' . $entity2->get('id')
        );
        $this->assertResponseCode(200);
        $location = TMP_TESTDIR . DS . 'test-zip.zip';
        Filesystem::dumpFile($location, $output);
        $unzipDir = TMP_TESTDIR . DS . 'testdir-unzip';
        DataCompressor::useTransaction('testunit');
        DataCompressor::uncompress($location, $unzipDir);
        $this->assertFileExists($unzipDir . DS . 'file1.txt');
        $this->assertFileExists($unzipDir . DS . 'file2.txt');
        $this->assertEquals($md5Initial1, md5_file($unzipDir . DS . 'file1.txt'));
        $this->assertEquals($md5Initial2, md5_file($unzipDir . DS . 'file2.txt'));
    }

    /**
     * testOpen
     */
    public function testOpen()
    {
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $entity = $Fileuploads->newUploadedEntity('logo-test-unit.png', $this->imgTest, 1);
        $Fileuploads->save($entity, ['silent' => true]);

        $md5Initial = md5_file($this->imgTest);
        $output = $this->preserveBuffer(
            [$this, 'httpGet'],
            '/download/open/' . $entity->get('id')
        );
        $this->assertResponseCode(200);
        $this->assertEquals($md5Initial, md5($output));

        $entity = $Fileuploads->newUploadedEntity('sample-test-unit.pdf', $this->pdfTest, 1);
        $Fileuploads->save($entity, ['silent' => true]);
        $this->get('/download/open/' . $entity->get('id'));
        $this->assertResponseCode(200);
        $this->assertHeader('Content-Type', 'application/pdf');
    }

    /**
     * testExtensionByMime
     */
    public function testExtensionByMime()
    {
        $controller = new DownloadController(new ServerRequest());
        $mimes = [
            'image/bmp',
            'image/cis-cod',
            'image/gif',
            'image/ief',
            'image/jpeg',
            'image/pipeg',
            'image/tiff',
            'image/x-cmu-raster',
            'image/x-cmx',
            'image/x-icon',
            'image/x-portable-anymap',
            'image/x-portable-bitmap',
            'image/x-portable-graymap',
            'image/x-portable-pixmap',
            'image/x-rgb',
            'image/x-xbitmap',
            'image/x-xpixmap',
            'image/x-xwindowdump',
            'image/png',
            'image/x-jps',
            'image/x-freehand',
            'another/mime',
        ];
        foreach ($mimes as $mime) {
            $res = $this->invokeMethod($controller, 'extensionByMime', [$mime]);
            $this->assertNotEmpty($res);
        }
    }
}
