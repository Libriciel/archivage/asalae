<?php

namespace Asalae\Test\TestCase\Controller\Component;

use Asalae\Controller\AppController;
use Asalae\Controller\Component\SealComponent;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use Authorization\Identity;
use Cake\Controller\Controller;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\MockObject\MockObject;

#[UsesClass(SealComponent::class)]
class SealComponentTest extends TestCase
{
    public array $fixtures = [
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveUnits',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.OrgEntities',
        'app.Roles',
        'app.Transfers',
        'app.Users',
        'app.TypeEntities',
        'app.DeliveryRequests',
    ];

    /**
     * @var SealComponent $Seal
     */
    public $Seal = null;

    /**
     * @var MockObject|null|Controller
     */
    public $controller = null;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->buildComponent();
        $mock = $this->createMock(Exec::class);
        Utility::set('Exec', $mock);
    }

    /**
     * buildComponent
     */
    private function buildComponent()
    {
        $auth = $this->fetchTable('Users')->find()
            ->where(['Users.id' => 3])
            ->contain(
                [
                    'OrgEntities' => [
                        'TypeEntities',
                        'ArchivalAgencies',
                    ],
                    'Roles',
                ]
            )
            ->firstOrFail();
        $identity = $this->createMock(Identity::class);
        $identity->method('getOriginalData')->willReturn($auth);
        $request = (new ServerRequest())->withAttribute('identity', $identity);
        $response = new Response();
        /** @var AppController $controller */
        $this->controller = $this->getMockBuilder('Asalae\Controller\AppController')
            ->setConstructorArgs([$request, $response])
            ->onlyMethods([])
            ->getMock();
        $this->controller->initialize();
        $this->Seal = $this->controller->Seal;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
    }

    /**
     * testarchivalAgency
     */
    public function testarchivalAgency()
    {
        $count = $this->Seal->archivalAgency()
            ->table('ArchiveBinaries')->find('seal')
            ->where(['ArchiveBinaries.id' => 1]);
        $this->assertCount(1, $count);

        $Archives = $this->fetchTable('Archives');
        $Archives->updateAll(['archival_agency_id' => 3], ['id' => 1]);

        $count = $this->Seal->archivalAgency()
            ->table('ArchiveBinaries')->find('seal')
            ->where(['ArchiveBinaries.id' => 1]);
        $this->assertCount(0, $count);
    }

    /**
     * testarchivalAgencyChilds
     */
    public function testarchivalAgencyChilds()
    {
        $count = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->where(['OrgEntities.id' => 2]);
        $this->assertCount(1, $count);

        $count = $this->Seal->archivalAgencyChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->where(['OrgEntities.id' => 3]);
        $this->assertCount(0, $count);
    }

    /**
     * testhierarchicalView
     */
    public function testhierarchicalView()
    {
        $count = $this->Seal->hierarchicalView()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => 2]);
        $this->assertCount(1, $count);

        $Archives = $this->fetchTable('Archives');
        $Archives->updateAll(['archival_agency_id' => 3], ['id' => 2]);

        $count = $this->Seal->hierarchicalView()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => 2]);
        $this->assertCount(0, $count);

        $Archives->updateAll(
            [
                'archival_agency_id' => 2,
                'originating_agency_id' => 4, // sv
            ],
            ['id' => 2]
        );

        // On ramène le service producteur à la racine du service d'archives
        // puis on défini le service versant comme entité fille du service producteur
        // on test alors avec et sans le hierarchical_view
        $OrgEntities = $this->fetchTable('OrgEntities');
        $prod = $OrgEntities->get(5);
        $prod->set('parent_id', 2);
        $OrgEntities->saveOrFail($prod);
        $vers = $OrgEntities->get(4);
        $vers->set('parent_id', 5);
        $OrgEntities->saveOrFail($vers);
        $this->buildComponent();

        $this->Seal->setConfig('user.role.hierarchical_view', false);
        $count = $this->Seal->hierarchicalView()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => 2]);
        $this->assertCount(0, $count);

        $this->Seal->setConfig('user.role.hierarchical_view', true);
        $count = $this->Seal->hierarchicalView()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => 2]);
        $this->assertCount(1, $count);

        // archive tout de même accessible si le transferring_agency ou originating_agency
        // correspond à celui de l'utilisateur
        $this->Seal->setConfig('user.role.hierarchical_view', false);
        $Archives->updateAll(['transferring_agency_id' => 5], ['id' => 2]);
        $count = $this->Seal->hierarchicalView()
            ->table('Archives')
            ->find('seal')
            ->where(['Archives.id' => 2]);
        $this->assertCount(1, $count);
    }

    /**
     * testorgEntity
     */
    public function testorgEntity()
    {
        $count = $this->Seal->orgEntity()
            ->table('DeliveryRequests')
            ->find('seal')
            ->where(['DeliveryRequests.id' => 1]);
        $this->assertCount(1, $count);

        $DeliveryRequests = $this->fetchTable('DeliveryRequests');
        $DeliveryRequests->updateAll(['requester_id' => 2], ['id' => 1]);

        $count = $this->Seal->orgEntity()
            ->table('DeliveryRequests')
            ->find('seal')
            ->where(['DeliveryRequests.id' => 1]);
        $this->assertCount(0, $count);
    }

    /**
     * testorgEntityChilds
     */
    public function testorgEntityChilds()
    {
        // On ramène le service producteur à la racine du service d'archives
        // puis on défini le service versant comme entité fille du service producteur
        $OrgEntities = $this->fetchTable('OrgEntities');
        $prod = $OrgEntities->get(5);
        $prod->set('parent_id', 2);
        $OrgEntities->saveOrFail($prod);
        $vers = $OrgEntities->get(4);
        $vers->set('parent_id', 5);
        $OrgEntities->saveOrFail($vers);
        $this->buildComponent();

        $count = $this->Seal->orgEntityChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->where(['OrgEntities.id' => 5]);
        $this->assertCount(1, $count);

        $count = $this->Seal->orgEntityChilds()
            ->table('OrgEntities')
            ->find('seal')
            ->where(['OrgEntities.id' => 3]);
        $this->assertCount(0, $count);
    }
}
