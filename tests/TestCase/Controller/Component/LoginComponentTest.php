<?php

namespace Asalae\Test\TestCase\Controller\Component;

use Adldap\Adldap;
use Adldap\Auth\Guard;
use Adldap\Connections\Provider;
use Adldap\Models\Model;
use Adldap\Query\Builder;
use Adldap\Query\Factory;
use Asalae\Controller\AppController;
use Asalae\Controller\Component\LoginComponent;
use Asalae\Model\Table\UsersTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\MockObject\MockObject;

#[UsesClass(LoginComponent::class)]
class LoginComponentTest extends TestCase
{
    public array $fixtures = [
        'app.ArchiveUnits',
        'app.Aros',
        'app.Configurations',
        'app.EventLogs',
        'app.Ldaps',
        'app.OrgEntities',
        'app.Roles',
        'app.RolesTypeEntities',
        'app.TypeEntities',
        'app.Users',
        'app.ValidationChains',
        'app.Versions',
    ];

    /**
     * @var LoginComponent $component
     */
    public $component = null;
    /**
     * @var MockObject|AppController
     */
    public $controller = null;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $request = new ServerRequest();
        $response = new Response();
        $this->controller = $this->getMockBuilder('Cake\Controller\Controller')
            ->setConstructorArgs([$request, $response])
            ->onlyMethods([])
            ->getMock();
        $registry = new ComponentRegistry($this->controller);
        $this->component = new LoginComponent($registry);
        $this->controller->Authentication = $this->createMock(
            AuthenticationComponent::class
        );
        $this->controller->Authentication->method('setIdentity');
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
    }

    /**
     * testLogUser
     */
    public function testLogUser()
    {
        $this->assertTrue($this->component->logUser('admin', 'admin'));
        LoginComponent::$attempts = [];
        $this->assertFalse(
            $this->component->logUser('testunit', 'bad_password')
        );

        $adldap = $this->createMock(Adldap::class);
        $adldap->method('addProvider');
        $provider = $this->createMock(Provider::class);
        $adldap->method('connect')
            ->willReturn($provider);
        $guard = $this->createMock(Guard::class);
        $provider->method('auth')
            ->willReturn($guard);
        $guard->method('attempt')
            ->willReturn(true);
        $factory = $this->createMock(Factory::class);
        $provider->method('search')
            ->willReturn($factory);
        $users = $this->createMock(Builder::class);
        $factory->method('users')
            ->willReturn($users);
        $ldapUser = $this->createMock(Model::class);
        $users->method('findBy')
            ->willReturn($ldapUser);
        $ldapUser->method('getAttribute')
            ->willReturn(['foo'], ['bar@baz.fr']);
        Utility::set(Adldap::class, $adldap);
        $Ldaps = TableRegistry::getTableLocator()->get('Ldaps');
        $ldap = $Ldaps->newEntity(
            [
                'org_entity_id' => 2,
                'name' => 'ldap',
                'host' => 'localhost',
                'port' => 1234,
                'ldap_root_search' => '(*)',
                'user_login_attribute' => 'samaccountname',
            ]
        );
        $Ldaps->saveOrFail($ldap);

        /** @var UsersTable $Users */
        $Users = TableRegistry::getTableLocator()->get('Users');
        $password = $Users->generatePassword();
        $user = $Users->newEntity(
            [
                'org_entity_id' => 2,
                'agent_type' => 'person',
                'role_id' => 1,
                'username' => 'ldap_user',
                'password' => $password,
                'confirm-password' => $password,
                'name' => null,
                'email' => 'foo@test.fr',
                'high_contrast' => false,
                'active' => true,
                'is_validator' => false,
                'use_cert' => false,
                'ldap_id' => 1,
                'ldap_login' => 'ldap_user',
            ]
        );
        $Users->saveOrFail($user);
        $this->assertTrue($this->component->logUser('ldap_user', 'test_ldap'));
    }
}
