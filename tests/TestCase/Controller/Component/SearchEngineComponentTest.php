<?php

namespace Asalae\Test\TestCase\Controller\Component;

use Asalae\Controller\Component\SearchEngineComponent;
use Asalae\Model\Table\ArchiveUnitsTable;
use AsalaeCore\Controller\Controller;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\DatabaseUtility;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\EntityInterface;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\MockObject\MockObject;

#[UsesClass(SearchEngineComponent::class)]
class SearchEngineComponentTest extends TestCase
{
    public array $fixtures = [
        'app.ArchiveUnits',
        'app.AccessRules',
        'app.Archives',
        'app.ArchiveDescriptions',
        'app.ArchiveKeywords',
    ];

    /**
     * @var SearchEngineComponent $component
     */
    public $component = null;

    /**
     * @var MockObject|null|Controller
     */
    public $controller = null;

    /**
     * @var Table|null|ArchiveUnitsTable
     */
    public $model = null;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->model = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $this->model->updateAll(
            ['archival_agency_identifier' => 'Ceci est le texte qu\'on peut rechercher'],
            ['id' => 1]
        );
        $this->model->updateSearchField([]);
        // NOTE: la fonction semble supprimée entre le bootstrap et le setUp (pdo identique pourtant)
        $pdo = DatabaseUtility::getPdo();
        $pdo->sqliteCreateFunction(
            'regexp',
            function ($pattern, $data) {
                return $pattern && $data ? preg_match(
                    "~$pattern~isuS",
                    $data
                ) : null;
            }
        );
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
    }

    /**
     * testQuery
     */
    public function testQuery()
    {
        $this->setSearch('Lorem ipsum');
        $query = $this->model->find();
        $this->component->query($query);
        $this->assertStringContainsString('[^\w]lorem[^\w]', $query->sql());
    }

    /**
     * setSearch
     * @param string $search
     */
    private function setSearch(string $search)
    {
        $request = new ServerRequest(['query' => ['search' => $search]]);
        $response = new Response();
        $this->controller = $this->getMockBuilder(Controller::class)
            ->setConstructorArgs([$request, $response])
            ->onlyMethods([])
            ->getMock();
        $registry = new ComponentRegistry($this->controller);
        $this->component = new SearchEngineComponent(
            $registry,
            ['model' => 'ArchiveUnits']
        );
    }

    /**
     * testGetFormatedSearch
     */
    public function testGetFormatedSearch()
    {
        $this->setSearch('Lorem ipsum');
        $query = $this->model->find();
        $this->component->query($query);
        $this->assertContains('lorem', $this->component->getFormatedSearch());
    }

    /**
     * testResults
     */
    public function testResults()
    {
        // Recherche phrase
        $this->setSearch('"peut rechercher"');
        $query = $this->model->find();
        $this->component->query($query);
        $results = $this->component->results(
            ['archival_agency_identifier'],
            function ($entity) {
                $this->assertInstanceOf(EntityInterface::class, $entity);
            }
        );
        $this->assertGreaterThanOrEqual(1, count($results));

        // test recherche négative
        $prevCount = count($results);
        $this->setSearch('"Ceci est le" -texte');
        $query = $this->model->find();
        $this->component->query($query);
        $results = $this->component->results(
            ['archival_agency_identifier'],
            function ($entity) {
                $this->assertInstanceOf(EntityInterface::class, $entity);
            }
        );
        $this->assertLessThan($prevCount, count($results));

        // test recherche approximative
        $this->setSearch('recherch');
        $query = $this->model->find();
        $this->component->query($query);
        $results = $this->component->results(
            ['archival_agency_identifier'],
            function ($entity) {
                $this->assertInstanceOf(EntityInterface::class, $entity);
            }
        );
        $this->assertGreaterThanOrEqual(1, count($results));

        // test recherche terme exacte
        $this->setSearch('Ceci est le texte qu\'on peut rechercher');
        $query = $this->model->find();
        $this->component->query($query);
        $results = $this->component->results(
            ['archival_agency_identifier'],
            function ($entity) {
                $this->assertInstanceOf(EntityInterface::class, $entity);
            }
        );
        $this->assertGreaterThanOrEqual(1, count($results));
        $this->assertEquals(100, $results[0]['score']);
    }
}
