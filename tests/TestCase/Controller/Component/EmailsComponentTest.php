<?php

namespace Asalae\Test\TestCase\Controller\Component;

use Asalae\Controller\Component\EmailsComponent;
use Asalae\Model\Entity\User;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\MockObject\MockObject;

#[UsesClass(EmailsComponent::class)]
class EmailsComponentTest extends TestCase
{
    public array $fixtures = [
        'app.AuthUrls',
        'app.Mails',
        'app.OrgEntities',
        'app.Roles',
        'app.Users',
    ];

    /**
     * @var EmailsComponent $component
     */
    public $component = null;

    /**
     * @var MockObject|null|Controller
     */
    public $controller = null;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $request = new ServerRequest();
        $response = new Response();
        $this->controller = $this->getMockBuilder('Cake\Controller\Controller')
            ->setConstructorArgs([$request, $response])
            ->onlyMethods([])
            ->getMock();
        $registry = new ComponentRegistry($this->controller);
        $this->component = new EmailsComponent($registry);

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
    }

    /**
     * testSubmitEmailNewUser
     */
    public function testSubmitEmailNewUser()
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        $Users = TableRegistry::getTableLocator()->get('Users');
        /** @var User $user */
        $user = $Users->find()->contain(['Roles', 'OrgEntities'])->first();
        $user->set('email', 'foo@bar.com');
        $result = $this->component->submitEmailNewUser($user);
        $this->assertTrue($result);
    }
}
