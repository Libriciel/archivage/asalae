<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\TechnicalArchivesController;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\TechnicalArchivesController Test Case
 *
 */
#[UsesClass(TechnicalArchivesController::class)]
class TechnicalArchivesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public const string SEDA_10_DESCRIPTION = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10_description.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,

        // Pour ce controller
        'app.ArchiveBinaries',
        'app.ArchiveBinariesTechnicalArchiveUnits',
        'app.Archives',
        'app.EventLogs',
        'app.OrgEntities',
        'app.SecureDataSpaces',
        'app.StoredFiles',
        'app.StoredFilesVolumes',
        'app.TechnicalArchiveUnits',
        'app.TechnicalArchives',
        'app.Volumes',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        VolumeSample::init();
        VolumeSample::insertSample(
            'sample_seda10_description.xml',
            'sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'
        );
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->httpGet('/technical-archives/index');
        $this->assertResponseCode(200);
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->httpGet('/technical-archives/view/1');
        $this->assertResponseCode(200);
    }

    /**
     * testDownloadFile
     */
    public function testDownloadFile()
    {
        VolumeSample::volumePutContent(
            'sa/technical_archives/c1037259-43b8-4b90-988a-4239839ac8f3/asalae_events_log_20211209.xml',
            'test'
        );
        $this->httpGet('/technical-archives/download-file/6');
        $this->assertResponseCode(200);
        $this->assertResponseEquals('test');
    }

    /**
     * testPaginateFiles
     */
    public function testPaginateFiles()
    {
        $this->httpGet('/technical-archives/paginate-files/1');
        $this->assertResponseCode(200);
        $this->httpGet(
            '/technical-archives/paginate-files/1?sort=filename&amp;direction=asc&size[0][operator]=>=&size[0][value]=1&size[0][mult]=1024&size[1][operator]=<=&size[1][value]=&size[1][mult]=1048576'
        );
        $this->assertResponseCode(200);
        $this->get(
            '/technical-archives/paginate-files/1?sort=filename&amp;direction=asc&size[0][operator]=!!!!!bad_operator!!!!!=&size[0][value]=1&size[0][mult]=1024&size[1][operator]=<=&size[1][value]=&size[1][mult]=1048576'
        );
        $this->assertResponseCode(400);
    }
}
