<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\CountersController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(CountersController::class)]
class CountersControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_EVENT_LOGS,
        'app.Sequences',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    /**
     * testIndex
     */
    public function testIndex()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/counters/index');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet(
            '/counters/index?SaveFilterSelect=&name[0]=Compteur*&identifier[0]=%3Fes*&active[0]=1&favoris[0]=1&sort=name&direction=asc'
        );
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertEmpty($response['data']);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet(
            '/counters/index?SaveFilterSelect=&name[0]=*ArchiveTransfer*&identifier[0]=ArchiveTransfer*&active[0]=1&sort=favorite&direction=desc'
        );
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);
    }

    /**
     * testInitCounters
     */
    public function testInitCounters()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'Counters', 'alias' => 'initCounters'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $ArosAcos->deleteAll(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
            ]
        );
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $Counters = TableRegistry::getTableLocator()->get('Counters');
        $Counters->deleteAll([]);
        $Sequences = TableRegistry::getTableLocator()->get('Sequences');
        $Sequences->deleteAll([]);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/counters/initCounters');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['success']));
        $this->assertTrue($response['success']);
        $this->assertGreaterThan(0, $Counters->find()->count());
    }

    /**
     * testEdit
     */
    public function testEdit()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/counters/edit/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entity']));
        $data = $response['entity'];
        $data['description'] = "Ceci n'est pas un test";

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPut('/counters/edit/1', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertEquals($data['name'], $response['name']);

        $Counters = TableRegistry::getTableLocator()->get('Counters');
        $this->assertEquals("Ceci n'est pas un test", $Counters->get(1)->get('description'));
    }

    /**
     * testView
     */
    public function testView()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/counters/view/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entity']));
    }
}
