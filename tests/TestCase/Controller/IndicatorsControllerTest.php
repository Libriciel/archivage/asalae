<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\IndicatorsController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\IndicatorsController Test Case
 *
 * \Asalae\Controller\IndicatorsController
 */
#[UsesClass(IndicatorsController::class)]
class IndicatorsControllerTest extends TestCase
{
    use \Asalae\Test\TestCase\Controller\HttpTrait;
    use IntegrationTestTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        'app.Agreements',
        'app.ArchiveIndicators',
        'app.EventLogs',
        'app.Filters',
        'app.MessageIndicators',
        'app.Profiles',
        'app.SavedFilters',
    ];
    /**
     * @var array expected result
     */
    protected $expected = [
        'transfers_volume' => 554307,
        'transfers_pj_count' => 3,
        'transfers_count' => 4,
        'transfers_max_traitement' => 870,
        'transfers_rejected_count' => 1,
        'archives_volume_total' => 554297,
        'archives_volume_doc' => 554297,
        'archives_volume_cons' => 0,
        'archives_volume_diff' => 0,
        'archives_volume_horo' => 0,
        'archives_count' => 1,
        'archives_unit_count' => 2,
        'archives_doc_count' => 1,
        'communications_volume_total' => 554297,
        'communications_doc_count' => 1,
        'communications_count' => 1,
        'communications_request_rejected' => 0,
        'communications_derogation_intructed' => 0,
        'communications_derogation_rejected' => 0,
        'eliminations_volume_total' => 5,
        'eliminations_doc_count' => 1,
        'eliminations_count' => 1,
        'eliminations_rejected' => 0,
        'restitutions_volume_total' => 0,
        'restitutions_doc_count' => 0,
        'restitutions_count' => 0,
        'restitutions_rejected' => 0,
        'outgoing_volume_total' => 0,
        'outgoing_doc_count' => 0,
        'outgoing_count' => 0,
        'outgoing_max_traitement' => 0,
        'outgoing_rejected' => 0,
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        TableRegistry::getTableLocator()
            ->get('MessageIndicators')
            ->deleteAll(['id >' => 6]);
        TableRegistry::getTableLocator()
            ->get('ArchiveIndicators')
            ->deleteAll(['id >' => 1]);
    }

    /**
     * testIndexTransferringAgency
     */
    public function testIndexTransferringAgency()
    {
        TableRegistry::getTableLocator()->get('Users')->get(1);
        $this->session($this->genericSessionData);
        $resp = $this->httpGet('/indicators/index-transferring-agency', true);
        $this->assertEquals($this->expected, $resp['totalSA']);
        $this->assertResponseCode(200);
    }

    /**
     * testIndexOriginatingAgency
     */
    public function testIndexOriginatingAgency()
    {
        TableRegistry::getTableLocator()->get('Users')->get(1);
        $this->session($this->genericSessionData);
        $resp = $this->httpGet('/indicators/index-originating-agency', true);
        $this->assertEquals($this->expected, $resp['totalSA']);
        $this->assertResponseCode(200);
    }

    /**
     * testCsv
     */
    public function testCsv()
    {
        $this->session($this->genericSessionData);
        $sv = TableRegistry::getTableLocator()
            ->get('OrgEntities')
            ->find()
            ->where(['identifier' => 'sa'])
            ->firstOrFail();

        $this->httpPost(
            '/indicators/csv?by=transferring',
            [
                'delimiter' => ';',
                'enclosure' => '"',
                'unit' => 0,
            ]
        );
        $this->assertResponseCode(200);
        $this->assertHeaderContains('Content-Type', 'text/csv');
        $this->assertResponseContains('"' . $sv->get('name') . '";'); // échappé
        $this->assertResponseContains(
            ';' . $this->expected['transfers_count'] . ';' // non échappé
        );
        $this->assertResponseContains( // échappé (forcé)
            Number::format($this->expected['archives_volume_total'], ['precision' => 2, 'pattern' => '#.##'])
        );

        $this->httpPost(
            '/indicators/csv?by=transferring',
            [
                'delimiter' => ',',
                'enclosure' => "'",
                'unit' => 0,
            ]
        );
        $this->assertResponseCode(200);
        $this->assertHeaderContains('Content-Type', 'text/csv');
        $this->assertResponseContains("'" . str_replace("'", "''", $sv->get('name')) . "',"); // échappé
        $this->assertResponseContains("," . $this->expected['transfers_count'] . ","); // non échappé

        $this->assertResponseContains( // échappé (forcé)
            Number::format($this->expected['archives_volume_total'], ['precision' => 2, 'pattern' => '#.##'])
        );
    }
}
