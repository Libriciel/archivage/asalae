<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\ValidationProcessesController;
use Asalae\Model\Table\AuthUrlsTable;
use Asalae\Model\Table\DeliveryRequestsTable;
use Asalae\Model\Table\DestructionRequestsTable;
use Asalae\Model\Table\OutgoingTransferRequestsTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Model\Table\ValidationActorsTable;
use Asalae\Model\Table\ValidationChainsTable;
use Asalae\Model\Table\ValidationHistoriesTable;
use Asalae\Model\Table\ValidationProcessesTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\Attributes\UsesClass;

/**
 * Asalae\Controller\ValidationProcessesController Test Case
 */
#[UsesClass(ValidationProcessesController::class)]
class ValidationProcessesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public const string SEDA_02_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda02.xml';
    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        'app.AccessTokens',
        'app.Agreements',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveUnits',
        'app.ArchiveUnitsDeliveryRequests',
        'app.ArchiveUnitsDestructionRequests',
        'app.ArchiveUnitsOutgoingTransferRequests',
        'app.ArchiveUnitsRestitutionRequests',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.ArchivingSystems',
        'app.AuthSubUrls',
        'app.AuthUrls',
        'app.DeliveryRequests',
        'app.DestructionRequests',
        'app.EventLogs',
        'app.Mails',
        'app.MessageIndicators',
        'app.OutgoingTransferRequests',
        'app.Profiles',
        'app.RestitutionRequests',
        'app.ServiceLevels',
        'app.TransferAttachments',
        'app.TransferErrors',
        'app.Transfers',
        'app.TypeEntities',
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationHistories',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);

        Configure::write('App.paths.data', TMP_TESTDIR);
        VolumeSample::init();

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
        VolumeSample::destroy();
    }

    /**
     * testMyTransfers
     */
    public function testMyTransfers()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var ValidationHistoriesTable $ValidationHistories */
        $ValidationHistories = $loc->get('ValidationHistories');
        $ValidationHistories->deleteAll([]);
        /** @var ValidationChainsTable $ValidationChains */
        $ValidationChains = $loc->get('ValidationChains');

        // index validation des transferts conformes
        $ValidationChains->updateAll(['app_type' => ValidationChainsTable::ARCHIVE_TRANSFER_CONFORM], []);
        $this->httpGet('/validation-processes/my-transfers/validate');
        $this->assertResponseCode(200);

        // index validation des transferts non conformes
        $ValidationChains->updateAll(['app_type' => ValidationChainsTable::ARCHIVE_TRANSFER_NOT_CONFORM], []);
        $this->httpGet('/validation-processes/my-transfers/invalidate');
        $this->assertResponseCode(200);

        // index validation avec traitement par lots
        $this->httpGet('/validation-processes/my-transfers/invalidate?validation_stage_id[0]=1');
        $this->assertResponseCode(200);

        // index validation par section
        $this->_request['headers']['X-Asalae-Update'] = 'section';
        $this->httpGet('/validation-processes/my-transfers/invalidate');
        $this->assertResponseCode(200);
    }

    /**
     * testTransfers
     */
    public function testTransfers()
    {
        $response = $this->httpGet('/validation-processes/transfers', true);
        $this->assertResponseCode(200);
        $this->assertEquals('Transfers', Hash::get($response, 'data.0.app_subject_type'));

        $this->httpGet('/validation-processes/transfers?favoris=true&sort=favorite', true);
        $this->assertResponseCode(200);
    }

    /**
     * testProcessMyTransfer
     */
    public function testProcessMyTransfer()
    {
        VolumeSample::copySample('sample_seda02.xml', 'transfers/8/message/sample_seda02.xml');

        $loc = TableRegistry::getTableLocator();
        $AuthUrls = $loc->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        /** @var ValidationActorsTable $ValidationActors */
        $ValidationActors = $loc->get('ValidationActors');
        $actor = $ValidationActors->get(1);
        $actor->set('type_validation', 'S');
        $ValidationActors->saveOrFail($actor);
        /** @var ValidationHistoriesTable $ValidationHistories */
        $ValidationHistories = $loc->get('ValidationHistories');
        $ValidationHistories->deleteAll([]);
        /** @var TransfersTable $Transfers */
        $Transfers = $loc->get('Transfers');
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $loc->get('ValidationProcesses');

        // get du formulaire
        $this->httpGet('/validation-processes/process-my-transfer/8');
        $this->assertResponseCode(200);

        // validation
        $actor->set('type_validation', 'V');
        $ValidationActors->saveOrFail($actor);
        $this->httpPost('/validation-processes/process-my-transfer/8', ['action' => 'validate', 'comment' => 'test']);
        $this->assertResponseCode(200);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertEquals(2, $response['current_step']);
        $process = $ValidationProcesses->get(8);
        $this->assertEquals(2, $process->get('current_stage_id'));
        $this->assertFalse($process->get('processed'));
        $this->assertNull($process->get('validated'));

        // retour à l'étape précédente
        $this->httpPost(
            '/validation-processes/process-my-transfer/8',
            ['action' => 'stepback', 'prev_stage_id' => 1, 'comment' => 'test']
        );
        $this->assertResponseCode(200);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertEquals(3, $response['current_step']);
        $process = $ValidationProcesses->get(8);
        $this->assertEquals(1, $process->get('current_stage_id'));

        // user pas présent parmis les acteurs de validation
        $ValidationActors->updateAll(
            ['app_type' => 'test'],
            ['validation_stage_id' => 1]
        );
        $this->get('/validation-processes/process-my-transfer/8');
        $this->assertResponseCode(404);
        $actor->set('app_type', 'USER');
        $ValidationActors->saveOrFail($actor);

        // validation impossible sur non conforme avec erreur fatale
        $Transfers->updateAll(['state' => 'validating'], []);
        $this->httpPost(
            '/validation-processes/process-my-transfer/8',
            ['action' => 'validate', 'prev_stage_id' => 1, 'comment' => 'test']
        );
        $this->assertResponseCode(200);
        $process = $ValidationProcesses->get(8);
        $this->assertNotNull($process->get('current_stage_id'));
        $this->assertFalse($process->get('processed'));
        $this->assertNull($process->get('validated'));

        // invalidation
        $this->httpPost(
            '/validation-processes/process-my-transfer/8',
            ['action' => 'invalidate', 'prev_stage_id' => 1, 'comment' => 'test']
        );
        $this->assertResponseCode(200);
        $process = $ValidationProcesses->get(8);
        $this->assertNull($process->get('current_stage_id'));
        $this->assertTrue((bool)$process->get('processed'));
        $this->assertFalse($process->get('validated'));

        // le transfert est déjà traité
        $this->get('/validation-processes/process-my-transfer/8');
        $this->assertResponseCode(404);
    }

    /**
     * testProcessTransfer
     */
    public function testProcessTransfer()
    {
        if (is_file($uri = TMP_TESTDIR . DS . 'transfers/8/message/sample_seda02.xml')) {
            unlink($uri);
        }
        Filesystem::dumpFile($uri, file_get_contents(self::SEDA_02_XML));

        $loc = TableRegistry::getTableLocator();
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $loc->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        $code1 = $AuthUrls->newEntity(['url' => '/validation-processes/process-transfer/8/2/8']);
        $code2 = $AuthUrls->newEntity(['url' => '/validation-processes/process-transfer/8/4/8']);
        $code3 = $AuthUrls->newEntity(['url' => '/validation-processes/process-transfer/8/123456789/8']);
        $code4 = $AuthUrls->newEntity(['url' => '/validation-processes/process-transfer/8/2/8']);
        $AuthUrls->saveOrFail($code1);
        $AuthUrls->saveOrFail($code2);
        $AuthUrls->saveOrFail($code3);
        $AuthUrls->saveOrFail($code4);
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $loc->get('ValidationProcesses');
        /** @var ValidationHistoriesTable $ValidationHistories */
        $ValidationHistories = $loc->get('ValidationHistories');
        $ValidationHistories->deleteAll([]);

        // syntaxe url incorrecte
        $this->session(
            [
                'Auth' => [
                    'code' => $code1->get('code'),
                    'url' => $code1->get('url'),
                ],
            ]
        );

        // get du formulaire
        $this->get('/validation-processes/process-transfer/8/2/8');
        $this->assertResponseCode(200);

        // validation
        $response = $this->httpPost(
            '/validation-processes/process-transfer/8/2/8',
            ['action' => 'validate', 'comment' => 'test'],
            true
        );
        $this->assertResponseCode(200);
        $process = $ValidationProcesses->get($response['process']['id']);
        $this->assertEquals(2, $process->get('current_step'));
        $process = $ValidationProcesses->get(8);
        $this->assertEquals(2, $process->get('current_stage_id'));
        $this->assertFalse($process->get('processed'));
        $this->assertNull($process->get('validated'));

        // retour à l'étape précédente
        $this->session(
            [
                'Auth' => [
                    'code' => $code2->get('code'),
                    'url' => $code2->get('url'),
                ],
            ]
        );
        $response = $this->httpPost(
            '/validation-processes/process-transfer/8/4/8',
            ['action' => 'stepback', 'prev_stage_id' => 1, 'comment' => 'test'],
            true
        );
        $this->assertResponseCode(200);
        $process = $ValidationProcesses->get($response['process']['id']);
        $this->assertEquals(3, $process['current_step']);
        $process = $ValidationProcesses->get(8);
        $this->assertEquals(1, $process->get('current_stage_id'));

        // acteur non présent
        $this->session(
            [
                'Auth' => [
                    'code' => $code3->get('code'),
                    'url' => $code3->get('url'),
                ],
            ]
        );
        $this->get('/validation-processes/process-transfer/8/123456789/8');
        $this->assertResponseCode(404);

        // invalidation
        $this->session(
            [
                'Auth' => [
                    'code' => $code4->get('code'),
                    'url' => $code4->get('url'),
                ],
            ]
        );

        $Transfers = $loc->get('Transfers');
        $Transfers->updateAll(['state' => 'validating'], []);
        $this->httpPost(
            '/validation-processes/process-transfer/8/2/8',
            ['action' => 'invalidate', 'prev_stage_id' => 1, 'comment' => 'test']
        );
        $this->assertResponseCode(200);
        $process = $ValidationProcesses->get(8);
        $this->assertNull($process->get('current_stage_id'));
        $this->assertTrue((bool)$process->get('processed'));
        $this->assertFalse($process->get('validated'));

        // le transfert est déjà traité
        // FIXME affichage d'une vue d'erreur particulière pour ce cas
//        $this->httpGet('/validation-processes/process-transfer/8/2/8');
//        $this->assertResponseContains('notAuthorized');
    }

    /**
     * testProcessMultiTransfer
     */
    public function testProcessMultiTransfer()
    {
        if (is_file($uri = TMP_TESTDIR . DS . 'transfers/8/message/sample_seda02.xml')) {
            unlink($uri);
        }
        Filesystem::dumpFile($uri, file_get_contents(self::SEDA_02_XML));

        TableRegistry::getTableLocator()->get('Transfers')->updateAll(
            ['state' => 'validating'],
            []
        );

        $data = ['ids' => '8', 'action' => 'invalidate', 'comment' => 'test'];
        $this->httpPost('/validation-processes/process-multi-transfer', $data);
        $this->assertResponseCode(200);
    }

    /**
     * testResendMail
     */
    public function testResendMail()
    {
        $this->httpPost('/validation-processes/resend-mail/8');
        $this->assertResponseCode(200);
    }

    /**
     * testDeliveryRequests
     */
    public function testDeliveryRequests()
    {
        $this->httpGet('/validation-processes/delivery-requests');
        $this->assertResponseCode(200);
    }

    /**
     * testProcessDeliveryRequest
     */
    public function testProcessDeliveryRequest()
    {
        $loc = TableRegistry::getTableLocator();
        $loc->get('ValidationProcesses')->updateAll(
            ['current_stage_id' => 1],
            ['app_subject_type' => 'DeliveryRequests']
        );
        $loc->get('DeliveryRequests')->updateAll(
            ['state' => 'validating'],
            []
        );

        $this->httpGet('/validation-processes/process-delivery-request/3');
        $this->assertResponseCode(200);

        $data = ['action' => 'validate', 'comment' => 'test', 'signature' => 'test'];
        $this->httpPost('/validation-processes/process-delivery-request/3', $data);
        $this->assertResponseCode(200);
    }

    /**
     * testProcessDeliveryRequestMail
     */
    public function testProcessDeliveryRequestMail()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var DeliveryRequestsTable $DeliveryRequests */
        $DeliveryRequests = $loc->get('DeliveryRequests');
        $DeliveryRequests->updateAll(['state' => 'validating'], []);

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $loc->get('ValidationProcesses');
        $ValidationProcesses->saveOrFail(
            $validation = $ValidationProcesses->newEntity(
                [
                    'validation_chain_id' => 3,
                    'current_stage_id' => 1,
                    'app_subject_type' => 'DeliveryRequests',
                    'app_subject_key' => null,
                    'processed' => true,
                    'validated' => true,
                    'app_meta' => null,
                    'app_foreign_key' => 1,
                    'current_step' => 2,
                ],
            )
        );

        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $loc->get('AuthUrls');
        $code1 = $AuthUrls->newEntity(
            ['url' => '/validation-processes/process-delivery-request-mail/' . $validation->id . '/2']
        );
        $AuthUrls->saveOrFail($code1);
        $this->session(
            [
                'Auth' => [
                    'code' => $code1->get('code'),
                    'url' => $code1->get('url'),
                ],
            ]
        );

        $this->httpGet('/validation-processes/process-delivery-request-mail/' . $validation->id . '/2');
        $this->assertResponseCode(200);

        $data = ['action' => 'validate', 'comment' => 'test', 'signature' => 'test'];
        $this->httpPost('/validation-processes/process-delivery-request-mail/' . $validation->id . '/2', $data);
        $this->assertResponseCode(200);
    }

    /**
     * testDestructionRequests
     */
    public function testDestructionRequests()
    {
        $this->httpGet('/validation-processes/destruction-requests');
        $this->assertResponseCode(200);
    }

    /**
     * testProcessDestructionRequests
     */
    public function testProcessDestructionRequests()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var DestructionRequestsTable $DestructionRequests */
        $DestructionRequests = $loc->get('DestructionRequests');
        $DestructionRequests->updateAll(['state' => 'validating'], ['id' => 1]);

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $loc->get('ValidationProcesses');
        $ValidationProcesses->saveOrFail(
            $validation = $ValidationProcesses->newEntity(
                [
                    'validation_chain_id' => 4,
                    'current_stage_id' => 1,
                    'app_subject_type' => 'DestructionRequests',
                    'app_subject_key' => null,
                    'processed' => true,
                    'validated' => true,
                    'app_meta' => null,
                    'app_foreign_key' => 1,
                    'current_step' => 2,
                ],
            )
        );

        $this->httpGet('/validation-processes/process-destruction-request/' . $validation->id);
        $this->assertResponseCode(200);

        $this->http(
            'post',
            '/validation-processes/process-destruction-request/' . $validation->id,
            403,
            ['action' => 'stepback', 'comment' => 'test', 'signature' => 'test', 'prev_stage_id' => 0]
        );

        $this->httpPost(
            '/validation-processes/process-destruction-request/' . $validation->id,
            ['action' => 'validate', 'comment' => 'test', 'signature' => 'test']
        );
        $this->assertResponseCode(200);

        // la demande d'élimination est déjà traité
        $this->get('/validation-processes/process-destruction-request/' . $validation->id);
        $this->assertResponseCode(404);
    }

    /**
     * testProcessDestructionRequestMail
     */
    public function testProcessDestructionRequestMail()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var DestructionRequestsTable $DestructionRequests */
        $DestructionRequests = $loc->get('DestructionRequests');
        $DestructionRequests->updateAll(['state' => 'validating'], ['id' => 1]);

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $loc->get('ValidationProcesses');
        $ValidationProcesses->saveOrFail(
            $validation = $ValidationProcesses->newEntity(
                [
                    'validation_chain_id' => 4,
                    'current_stage_id' => 1,
                    'app_subject_type' => 'DestructionRequests',
                    'app_subject_key' => null,
                    'processed' => true,
                    'validated' => true,
                    'app_meta' => null,
                    'app_foreign_key' => 1,
                    'current_step' => 2,
                ],
            )
        );

        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $loc->get('AuthUrls');
        $code1 = $AuthUrls->newEntity(
            ['url' => '/validation-processes/processDestructionRequestMail/' . $validation->id . '/2']
        );
        $AuthUrls->saveOrFail($code1);

        $this->session(
            [
                'Auth' => [
                    'code' => $code1->get('code'),
                    'url' => $code1->get('url'),
                ],
            ]
        );
        $this->httpGet('/validation-processes/processDestructionRequestMail/' . $validation->id . '/2');
        $this->assertResponseCode(200);

        $this->httpPost(
            '/validation-processes/processDestructionRequestMail/' . $validation->id . '/2',
            ['action' => 'validate', 'comment' => 'test', 'signature' => 'test']
        );
        $this->assertResponseCode(200);
    }

    /**
     * testProcessRestitutionRequest
     */
    public function testProcessRestitutionRequest()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var RestitutionRequestsTable $RestitutionRequests */
        $RestitutionRequests = $loc->get('RestitutionRequests');
        $RestitutionRequests->updateAll(['state' => 'validating'], ['id' => 1]);
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $loc->get('ValidationProcesses');
        $ValidationProcesses->saveOrFail(
            $validation = $ValidationProcesses->newEntity(
                [
                    'validation_chain_id' => 4,
                    'current_stage_id' => 1,
                    'app_subject_type' => 'RestitutionRequests',
                    'app_subject_key' => null,
                    'processed' => true,
                    'validated' => true,
                    'app_meta' => null,
                    'app_foreign_key' => 1,
                    'current_step' => 2,
                ],
            )
        );

        $this->httpGet('/validation-processes/process-restitution-request/' . $validation->id);
        $this->assertResponseCode(200);

        $this->http(
            'post',
            '/validation-processes/process-restitution-request/' . $validation->id,
            403,
            ['action' => 'stepback', 'comment' => 'test', 'signature' => 'test', 'prev_stage_id' => 0]
        );

        $this->httpPost(
            '/validation-processes/process-restitution-request/' . $validation->id,
            ['action' => 'validate', 'comment' => 'test', 'signature' => 'test']
        );
        $this->assertResponseCode(200);

        // la demande d'élimination est déjà traité
        $this->get('/validation-processes/process-restitution-request/' . $validation->id);
        $this->assertResponseCode(404);
    }

    /**
     * testProcessRestitutionRequestMail
     */
    public function testProcessRestitutionRequestMail()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var RestitutionRequestsTable $RestitutionRequests */
        $RestitutionRequests = $loc->get('RestitutionRequests');
        $RestitutionRequests->updateAll(['state' => 'validating'], ['id' => 1]);

        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $loc->get('ValidationProcesses');
        $ValidationProcesses->saveOrFail(
            $validation = $ValidationProcesses->newEntity(
                [
                    'validation_chain_id' => 4,
                    'current_stage_id' => 1,
                    'app_subject_type' => 'RestitutionRequests',
                    'app_subject_key' => null,
                    'processed' => true,
                    'validated' => true,
                    'app_meta' => null,
                    'app_foreign_key' => 1,
                    'current_step' => 2,
                ],
            )
        );
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $loc->get('AuthUrls');
        $code1 = $AuthUrls->newEntity(
            ['url' => '/validation-processes/process-restitution-request-mail/' . $validation->id . '/2']
        );
        $AuthUrls->saveOrFail($code1);

        $this->session(
            [
                'Auth' => [
                    'code' => $code1->get('code'),
                    'url' => $code1->get('url'),
                ],
            ]
        );
        $this->httpGet('/validation-processes/process-restitution-request-mail/' . $validation->id . '/2');
        $this->assertResponseCode(200);

        $this->httpPost(
            '/validation-processes/process-restitution-request-mail/' . $validation->id . '/2',
            ['action' => 'validate', 'comment' => 'test', 'signature' => 'test']
        );
        $this->assertResponseCode(200);
    }

    /**
     * testProcessOutgoingTransferRequest
     */
    public function testProcessOutgoingTransferRequest()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
        $OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
        $OutgoingTransferRequests->updateAll(['state' => 'validating'], ['id' => 1]);
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $loc->get('ValidationProcesses');
        $ValidationProcesses->saveOrFail(
            $validation = $ValidationProcesses->newEntity(
                [
                    'validation_chain_id' => 4,
                    'current_stage_id' => 1,
                    'app_subject_type' => 'OutgoingTransferRequests',
                    'app_subject_key' => null,
                    'processed' => true,
                    'validated' => true,
                    'app_meta' => null,
                    'app_foreign_key' => 1,
                    'current_step' => 2,
                ],
            )
        );

        $this->httpGet('/validation-processes/process-outgoing-transfer-request/' . $validation->id);
        $this->assertResponseCode(200);
        $this->http(
            'post',
            '/validation-processes/process-outgoing-transfer-request/' . $validation->id,
            403,
            ['action' => 'stepback', 'comment' => 'test', 'signature' => 'test', 'prev_stage_id' => 0]
        );

        $this->httpPost(
            '/validation-processes/process-outgoing-transfer-request/' . $validation->id,
            ['action' => 'invalidate', 'comment' => 'test', 'signature' => 'test']
        );
        $this->assertResponseCode(200);

        // la demande d'élimination est déjà traité
        $this->get('/validation-processes/process-outgoing-transfer-request/' . $validation->id);
        $this->assertResponseCode(404);
    }

    /**
     * testProcessOutgoingTransferRequestMail
     */
    public function testProcessOutgoingTransferRequestMail()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var OutgoingTransferRequestsTable $OutgoingTransferRequests */
        $OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
        $OutgoingTransferRequests->updateAll(['state' => 'validating'], ['id' => 1]);
        /** @var ValidationProcessesTable $ValidationProcesses */
        $ValidationProcesses = $loc->get('ValidationProcesses');
        $ValidationProcesses->saveOrFail(
            $validation = $ValidationProcesses->newEntity(
                [
                    'validation_chain_id' => 4,
                    'current_stage_id' => 1,
                    'app_subject_type' => 'OutgoingTransferRequests',
                    'app_subject_key' => null,
                    'processed' => true,
                    'validated' => true,
                    'app_meta' => null,
                    'app_foreign_key' => 1,
                    'current_step' => 2,
                ],
            )
        );
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = $loc->get('AuthUrls');
        $code1 = $AuthUrls->newEntity(
            ['url' => '/validation-processes/processOutgoingTransferRequestMail/' . $validation->id . '/2']
        );
        $AuthUrls->saveOrFail($code1);

        $this->session(
            [
                'Auth' => [
                    'code' => $code1->get('code'),
                    'url' => $code1->get('url'),
                ],
            ]
        );
        $this->httpGet('/validation-processes/processOutgoingTransferRequestMail/' . $validation->id . '/2');
        $this->assertResponseCode(200);

        $this->httpPost(
            '/validation-processes/processOutgoingTransferRequestMail/' . $validation->id . '/2',
            ['action' => 'invalidate', 'comment' => 'test', 'signature' => 'test']
        );
        $this->assertResponseCode(200);
    }
}
