<?php

namespace Asalae\Test\TestCase\Controller;

use Asalae\Controller\AuthUrlsController;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use DateTime;
use PHPUnit\Framework\Attributes\UsesClass;

#[UsesClass(AuthUrlsController::class)]
class AuthUrlsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_EVENT_LOGS,
        FIXTURES_AUTH,
        'app.AuthSubUrls',
        'app.AuthUrls',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Password.complexity', 1);
    }

    /**
     * testNewUser
     */
    public function testNewUser()
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $AuthUrls->deleteAll([]);

        // On créé un code d'accès
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $code = $AuthUrls->newEntity([], ['validate' => false]);
        $AuthUrls->patchEntity($code, ['url' => '/users/initialize-password/1']);
        $AuthUrls->save($code);

        // On vérifi que l'url n'est pas accessible (sans activer le code)
        $this->get($code->get('url'));
        $this->assertResponseContains(
            __(
                "La page demandée n'est pas/plus valide. La durée d'accès peut être dépassée ou le traitement déjà effectué."
            )
        );

        // On active le code
        $this->get('/auth-urls/activate/' . $code->get('code'));
        $this->assertRedirect($code->get('url'));

        // On reconstruit la session et le header (La session est perdu entre les appels)
        $this->session(
            [
                'Auth.code' => $code->get('code'),
                'Auth.url' => $code->get('url'),
            ]
        );
        $this->configRequest(['headers' => ['Code' => $code->get('code')]]);

        // On choisi un nouveau mot de passe, on est redirigé sur la page d'accueil (connecté)
        $password = uniqid('test');
        $this->put($code->get('url'), ['password' => $password, 'confirm-password' => $password]);
        $this->assertRedirect('/');

        // Vérification de l'enregistrement en base
        $Users = TableRegistry::getTableLocator()->get('Users');
        $user = $Users->get(1);
        $hasher = new DefaultPasswordHasher();
        $this->assertTrue($hasher->check($password, $user->get('password')));
    }

    /**
     * testActivate
     */
    public function testActivate()
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        $code = $AuthUrls->newEntity([], ['validate' => false]);
        $AuthUrls->patchEntity(
            $code,
            [
                'url' => '/users/initialize-password/1',
                'expire' => new DateTime('1900-01-01'),
            ]
        );
        $AuthUrls->save($code);
        $this->get('/auth-urls/activate/' . $code->get('code'));
        $this->assertResponseCode(410); // 410 Gone
    }
}
