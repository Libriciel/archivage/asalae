<?php

namespace Asalae\Test\TestCase\MinkSuite;

use Asalae\Exception\GenericException;
use Asalae\MinkSuite\MinkArchive;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Datasource\EntityInterface;
use Exception;

class MinkArchiveTest extends TestCase
{
    use IntegrationCronTrait;

    public array $fixtures = [
        'app.Archives',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
    }

    /**
     * testCreate
     */
    public function testCreate()
    {
        $exec = $this->createMock(Exec::class);
        $exec->method('command')->willReturn(
            new CommandResult(['success' => false, 'code' => 500, 'stdout' => '', 'stderr' => ''])
        );
        Utility::set('Exec', $exec);
        $e = null;
        try {
            MinkArchive::create();
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(GenericException::class, $e);

        $exec = $this->createMock(Exec::class);
        $exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => '', 'stderr' => ''])
        );
        Utility::set('Exec', $exec);

        $e = null;
        $archive = null;
        try {
            $archive = MinkArchive::create();
        } catch (Exception $e) {
        }
        $this->assertNull($e);
        $this->assertInstanceOf(EntityInterface::class, $archive);

        $exec = $this->createMock(Exec::class);
        $exec->method('command')->willReturn(
            new CommandResult(['success' => false, 'code' => 500, 'stdout' => '', 'stderr' => ''])
        );
        Utility::set('Exec', $exec);
        $this->assertFalse(MinkArchive::delete($archive));

        $exec = $this->createMock(Exec::class);
        $exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => '', 'stderr' => ''])
        );
        Utility::set('Exec', $exec);
        $this->assertTrue(MinkArchive::delete($archive));
    }
}
