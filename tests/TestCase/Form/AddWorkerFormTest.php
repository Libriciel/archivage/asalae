<?php

namespace Asalae\Test\TestCase\Form;

use Asalae\Form\AddWorkerForm;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;

class AddWorkerFormTest extends TestCase
{
    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $exec = $this->createMock(Exec::class);
        $result = new CommandResult(
            [
                'success' => true,
                'code' => 0,
                'stdout' => '',
                'stderr' => '',
            ]
        );
        $exec->method('command')->willReturn($result);
        Utility::set('Exec', $exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testSchema
     */
    public function testSchema()
    {
        $form = new AddWorkerForm();
        $fields = $form->getSchema()->fields();
        $this->assertContains('keep-alive', $fields);
    }

    /**
     * testValidation
     */
    public function testValidation()
    {
        $form = new AddWorkerForm();
        $valid = $form->validate(
            [
                'tube' => 'valid-format',
            ]
        );
        $this->assertTrue($valid);

        $valid = $form->validate(
            [
                'tube' => '',
            ]
        );
        $this->assertFalse($valid);
    }

    /**
     * testExecute
     */
    public function testExecute()
    {
        $form = new AddWorkerForm();
        $valid = $form->execute(
            [
                'tube' => 'valid-format',
                'keep-alive' => true,
            ]
        );
        $this->assertTrue($valid);
    }
}
