<?php

namespace Asalae\Test\TestCase\Form;

use Asalae\Form\PermissionsForm;
use AsalaeCore\Controller\Component\AclComponent;
use AsalaeCore\TestSuite\TestCase;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;

class PermissionsFormTest extends TestCase
{
    public array $fixtures = [
        'app.ArosAcos',
        'app.Acos',
        'app.Aros',
        'app.Roles',
        'app.Users',
    ];

    /**
     * testSchema
     */
    public function testSchema()
    {
        $form = new PermissionsForm();
        $fields = $form->getSchema()->fields();
        $this->assertContains('controllers.Transfers.action.add1', $fields);
    }

    /**
     * testList
     */
    public function testList()
    {
        $registry = new ComponentRegistry(new Controller(new ServerRequest()));
        $Acl = new AclComponent($registry);
        $form = new PermissionsForm();
        $Acl->inherit(1, 'controllers/Transfers/add1');
        $list = $form->list(1);
        $this->assertArrayHasKey('controllers.Transfers.action.add1', $list);
        $this->assertEquals('0', $list['controllers.Transfers.action.add1']);
        $Acl->allow(1, 'controllers/Transfers/add1');
        $list = $form->list(1);
        $this->assertEquals('1', $list['controllers.Transfers.action.add1']);
        $Acl->deny(1, 'controllers/Transfers/add1');
        $list = $form->list(1);
        $this->assertEquals('-1', $list['controllers.Transfers.action.add1']);
        $Acl->allow(1, 'controllers/Permissions', 'read');
        $list = $form->list(1);
        $this->assertArrayHasKey('controllers.Permissions.read', $list);
        $this->assertEquals('1', $list['controllers.Permissions.read']);
    }

    /**
     * testExecute
     */
    public function testExecute()
    {
        $registry = new ComponentRegistry(new Controller(new ServerRequest()));
        $Acl = new AclComponent($registry);
        $form = new PermissionsForm();
        $aro = TableRegistry::getTableLocator()->get('Aros')->get(1);

        // test validation
        $result = $form->execute(
            [
                'aro' => $aro,
                'controllers' => ['Transfers' => ['action' => ['add1' => 'bad_value']]],
            ]
        );
        $this->assertFalse($result);

        // test grant
        $result = $form->execute(
            [
                'aro' => $aro,
                'controllers' => ['Transfers' => ['action' => ['add1' => '1']]],
            ]
        );
        $this->assertTrue($result);
        $this->assertTrue($Acl->check($aro->get('id'), 'controllers/Transfers/add1'));

        // test deny
        $result = $form->execute(
            [
                'aro' => $aro,
                'controllers' => ['Transfers' => ['action' => ['add1' => '-1']]],
            ]
        );
        $this->assertTrue($result);
        $this->assertFalse($Acl->check($aro->get('id'), 'controllers/Transfers/add1'));

        // test api
        $result = $form->execute(
            [
                'aro' => $aro,
                'api' => [
                    'Transfers' => [
                        'create' => '-1',
                        'read' => '-1',
                        'update' => '0',
                        'delete' => '1',
                    ],
                ],
            ]
        );
        $this->assertTrue($result);

        $Acl->deny($aro->get('id'), 'root');
        $this->assertFalse($Acl->check($aro->get('id'), 'api/Transfers', 'create'));
        $this->assertFalse($Acl->check($aro->get('id'), 'api/Transfers', 'read'));
        $this->assertFalse($Acl->check($aro->get('id'), 'api/Transfers', 'update'));
        $this->assertTrue($Acl->check($aro->get('id'), 'api/Transfers', 'delete'));
    }
}
