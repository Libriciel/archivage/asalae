<?php

namespace Asalae\Test\TestCase\Form;

use Asalae\Form\SearchArchiveForm;
use AsalaeCore\I18n\Date;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\I18n;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateInterval;
use DateTime;
use DateTimeInterface;
use ReflectionException;

class SearchArchiveFormTest extends TestCase
{
    public array $fixtures = [
        'app.AccessRules',
        'app.AppraisalRules',
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveDescriptions',
        'app.ArchiveKeywords',
        'app.ArchiveUnits',
        'app.Archives',
        'app.EventLogs',
        'app.KeywordLists',
        'app.Keywords',
        'app.OrgEntities',
        'app.Roles',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * testSchema
     */
    public function testSchema()
    {
        $form = $this->getForm(null, ['Archives' => [0 => ['name' => 'foo']]]);
        $fields = $form->getSchema()->fields();
        $this->assertContains('Archives.0.name', $fields);
    }

    /**
     * getForm
     * @param Query|null $query
     * @param array      $queryData
     * @param array|null $entities
     * @return SearchArchiveForm
     * @throws ReflectionException
     */
    private function getForm(Query $query = null, array $queryData = [], array $entities = null)
    {
        $loc = TableRegistry::getTableLocator();
        if (!$query) {
            $query = $loc->get('Archives')->find()
                ->innerJoinWith('AppraisalRules');
        }
        if (!$entities) {
            $entities = [
                'ArchivalAgency' => $sa = $loc->get('OrgEntities')->get(2),
                'UserOrgEntity' => $sa,
                'UserRole' => $loc->get('Roles')->get(3),
            ];
        }
        return SearchArchiveForm::create($query, $queryData, $entities);
    }

    /**
     * testValidation
     */
    public function testValidation()
    {
        $requestData = [
            'Archives' => [
                0 => [
                    'name' => 'foo',
                    'created' => '01/01/1980',
                ],
            ],
        ];
        $form = $this->getForm(null, $requestData);
        $this->assertEmpty($form->getErrors());

        $requestData = ['Archives' => [0 => ['name' => '']]];
        $form = $this->getForm(null, $requestData);
        $this->assertNotEmpty($form->getValidator()->validate($form->getData()));
    }

    /**
     * testExecute
     */
    public function testExecute()
    {
        $requestData = ['Archives' => [0 => ['name' => 'seda*.*']]];
        $loc = TableRegistry::getTableLocator();
        $query = $loc->get('Archives')->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['Archives' => [0 => ['name' => 'name not found']]];
        $loc = TableRegistry::getTableLocator();
        $query = $loc->get('Archives')->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterArchiveKeyword
     */
    public function testFilterArchiveKeyword()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateAll(
            ['search' => '"sa_1" "seda1.0" "Code 1"', 'full_search' => '"sa_1" "seda1.0" "Code 1"'],
            ['id' => 1]
        );
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $archiveKeyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => 1,
                'content' => 'Code 1',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $ArchiveKeywords->saveOrFail($archiveKeyword);
        $requestData = ['Archives' => [0 => ['keyword' => 'Code*']]];
        $loc = TableRegistry::getTableLocator();
        $query = $loc->get('Archives')->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['Archives' => [0 => ['keyword' => 'content not found']]];
        $query = $loc->get('Archives')->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterArchiveKeywordList
     */
    public function testFilterArchiveKeywordList()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateAll(
            ['search' => '"sa_1" "seda1.0" "Code 1"', 'full_search' => '"sa_1" "seda1.0" "Code 1"'],
            ['id' => 1]
        );
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $archiveKeyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => 1,
                'content' => 'foo',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $ArchiveKeywords->saveOrFail($archiveKeyword);

        $requestData = ['Archives' => [0 => ['keyword_list' => [1]]]];
        $loc = TableRegistry::getTableLocator();
        $query = $loc->get('Archives')->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['Archives' => [0 => ['keyword_list' => [0 => 2]]]];
        $query = $loc->get('Archives')->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterArchiveDua
     */
    public function testFilterArchiveDua()
    {
        $locator = TableRegistry::getTableLocator();
        $Archives = $locator->get('Archives');
        $Archives->deleteAll(['id !=' => 1]);

        $requestData = ['Archives' => [0 => ['dua' => '<']]];
        $query = $Archives->find()->innerJoinWith('AppraisalRules');
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['Archives' => [0 => ['dua' => '>=']]];
        $query = $Archives->find()->innerJoinWith('AppraisalRules');
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterArchiveDuaDuration
     */
    public function testFilterArchiveDuaDuration()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $AppraisalRules = $loc->get('AppraisalRules');
        $AppraisalRules->updateAll(
            ['start_date' => $date = new DateTime(), ['end_date' => (clone $date)->add(new DateInterval('P5Y'))]],
            []
        );

        $requestData = [
            'Archives' => [
                0 => [
                    'dua_duration' => 'P5Y',
                    'dua_duration_operator' => '=',
                ],
            ],
        ];
        $query = $Archives->find()->innerJoinWith('AppraisalRules');
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = [
            'Archives' => [
                0 => [
                    'dua_duration' => 'P10Y',
                    'dua_duration_operator' => '=',
                ],
            ],
        ];
        $query = $Archives->find()->innerJoinWith('AppraisalRules');
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);

        $requestData = [
            'Archives' => [
                0 => [
                    'dua_duration' => 'P5Y',
                    'dua_duration_operator' => '.=',
                ],
            ],
        ];
        $query = $Archives->find()->innerJoinWith('AppraisalRules');
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);

        $requestData = [
            'Archives' => [
                0 => [
                    'dua_duration' => 'P10Y',
                    'dua_duration_operator' => '<',
                ],
            ],
        ];
        $query = $Archives->find()->innerJoinWith('AppraisalRules');
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));
    }

    /**
     * testFilterArchiveFinalActionCode
     */
    public function testFilterArchiveFinalActionCode()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $AppraisalRules = $loc->get('AppraisalRules');
        $AppraisalRules->updateAll(['final_action_code' => 'keep'], []);
        $requestData = ['Archives' => [0 => ['appraisal_rules_final_action_code' => 'keep']]];
        $query = $Archives->find()->innerJoinWith('AppraisalRules');
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['Archives' => [0 => ['appraisal_rules_final_action_code' => 'destroy']]];
        $query = $Archives->find()->innerJoinWith('AppraisalRules');
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterArchiveAccessRulesEndDateVsNowCode
     */
    public function testFilterArchiveAccessRulesEndDateVsNowCode()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $requestData = ['Archives' => [0 => ['access_rules_end_date_vs_now' => '<']]];
        $query = $Archives->find()->innerJoinWith('AccessRules');
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['Archives' => [0 => ['access_rules_end_date_vs_now' => '>=']]];
        $query = $Archives->find()->innerJoinWith('AccessRules');
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterArchiveUnitKeyword
     */
    public function testFilterArchiveUnitKeyword()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateAll(
            ['search' => '"sa_1" "seda1.0" "Code 1"', 'full_search' => '"sa_1" "seda1.0" "Code 1"'],
            ['id' => 1]
        );
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $archiveKeyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => 1,
                'content' => 'Code 1',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $ArchiveKeywords->saveOrFail($archiveKeyword);
        $requestData = ['ArchiveUnits' => [0 => ['keyword' => 'Code*']]];
        $loc = TableRegistry::getTableLocator();
        $query = $loc->get('Archives')->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['ArchiveUnits' => [0 => ['keyword' => 'content not found']]];
        $query = $loc->get('Archives')->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterArchiveUnitKeywordList
     */
    public function testFilterArchiveUnitKeywordList()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateAll(
            ['search' => '"sa_1" "seda1.0" "Code 1"', 'full_search' => '"sa_1" "seda1.0" "Code 1"'],
            ['id' => 1]
        );
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $archiveKeyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => 1,
                'content' => 'foo',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $ArchiveKeywords->saveOrFail($archiveKeyword);
        $requestData = ['Archives' => [0 => ['keyword_list' => [0 => 1]]]];
        $loc = TableRegistry::getTableLocator();
        $query = $loc->get('Archives')->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['Archives' => [0 => ['keyword_list' => [0 => 2]]]];
        $query = $loc->get('Archives')->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterArchiveUnitDua
     */
    public function testFilterArchiveUnitDua()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');

        $requestData = ['ArchiveUnits' => [0 => ['dua' => '<']]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['ArchiveUnits' => [0 => ['dua' => '>=']]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterArchiveUnitDuaDuration
     */
    public function testFilterArchiveUnitDuaDuration()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $AppraisalRules = $loc->get('AppraisalRules');
        $AppraisalRules->updateAll(
            ['start_date' => $date = new DateTime(), ['end_date' => (clone $date)->add(new DateInterval('P5Y'))]],
            []
        );

        $requestData = [
            'ArchiveUnits' => [
                'dua_duration' => [
                    'P5Y',
                ],
                'dua_duration_operator' => [
                    '=',
                ],
            ],
        ];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = [
            'ArchiveUnits' => [
                0 => [
                    'dua_duration' => 'P10Y',
                    'dua_duration_operator' => '=',
                ],
            ],
        ];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);

        $requestData = [
            'ArchiveUnits' => [
                0 => [
                    'dua_duration' => 'P5Y',
                    'dua_duration_operator' => '!=',
                ],
            ],
        ];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);

        $requestData = [
            'ArchiveUnits' => [
                0 => [
                    'dua_duration' => 'P10Y',
                    'dua_duration_operator' => '<',
                ],
            ],
        ];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));
    }

    /**
     * testFilterArchiveUnitDuaEnd
     */
    public function testFilterArchiveUnitDuaEnd()
    {
        I18n::setLocale('FR_fr');
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');

        $requestData = [
            'ArchiveUnits' => [
                0 => [
                    'dua_end' => '09/12/2021',
                    'dateoperator_dua_end' => '=',
                ],
            ],
        ];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = [
            'ArchiveUnits' => [
                0 => [
                    'dua_end' => '09/12/2021',
                    'dateoperator_dua_end' => '<',
                ],
            ],
        ];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterArchiveUnitFinalActionCode
     */
    public function testFilterArchiveUnitFinalActionCode()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $AppraisalRules = $loc->get('AppraisalRules');
        $AppraisalRules->updateAll(['final_action_code' => 'keep'], []);
        $requestData = ['ArchiveUnits' => [0 => ['appraisal_rules_final_action_code' => 'keep']]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['ArchiveUnits' => [0 => ['appraisal_rules_final_action_code' => 'destroy']]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterArchiveUnitAccessRulesEndDateVsNowCode
     */
    public function testFilterArchiveUnitAccessRulesEndDateVsNowCode()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $requestData = ['ArchiveUnits' => [0 => ['access_rules_end_date_vs_now' => '<']]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['ArchiveUnits' => [0 => ['access_rules_end_date_vs_now' => '>=']]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterArchiveUnitDescription
     */
    public function testFilterArchiveUnitDescription()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $ArchiveDescriptions = $loc->get('ArchiveDescriptions');
        $ArchiveDescriptions->updateAll(['description' => 'Lorem ipsum dolor sit amet.'], ['id' => 1]);
        $requestData = ['ArchiveUnits' => [0 => ['description' => 'Lorem ipsum*']]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['ArchiveUnits' => [0 => ['description' => 'description not found']]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testGenericFilters
     */
    public function testGenericFilters()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $archive = $Archives->find()
            ->where(['Archives.id' => 1])
            ->contain(['ArchiveUnits' => ['ArchiveBinaries']])
            ->firstOrFail();
        $Archives->patchEntity(
            $archive,
            [
                'description' => 'Lorem ipsum dolor sit amet.',
                'transferring_agency_identifier' => 't_001',
                'originating_agency_identifier' => 'o_001',
            ]
        );
        $Archives->saveOrFail($archive);
        $requestData = [
            'Archives' => [
                0 => [
                    'name' => $archive->get('name'),
                    'description' => $archive->get('description'),
                    'archival_agency_identifier' => $archive->get('archival_agency_identifier'),
                    'transferring_agency_identifier' => $archive->get('transferring_agency_identifier'),
                    'originating_agency_identifier' => $archive->get('originating_agency_identifier'),
                    'created' => $archive->get('created'),
                    'dateoperator_created' => '=',
                ],
            ],
            'ArchiveUnits' => [
                0 => [
                    'name' => Hash::get($archive, 'archive_units.0.name'),
                    'archival_agency_identifier' => Hash::get($archive, 'archive_units.0.archival_agency_identifier'),
                ],
            ],
        ];
        $query = $Archives->find()->innerJoinWith('AppraisalRules');
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        foreach ($requestData as $model => $indexes) {
            foreach ($indexes as $index => $values) {
                foreach ($values as $field => $value) {
                    if (strpos($field, 'dateoperator_') === 0) {
                        continue;
                    }
                    $testFail = $requestData;
                    if ($value instanceof DateTimeInterface || $value instanceof Date) {
                        $testFail[$model][$index]['dateoperator_' . $field] = '=';
                        $testFail[$model][$index][$field] = (clone $value)->sub(new DateInterval('P1000Y'));
                    } else {
                        $testFail[$model][$index][$field] = 'will not found that string!';
                    }

                    $query = $Archives->find()->innerJoinWith('AppraisalRules');
                    $form = $this->getForm($query, $testFail);
                    $this->assertTrue($form->execute($form->getData()));
                    $result = $query->first();
                    $v = $testFail[$model][0][$field];
                    $this->assertNull(
                        $result,
                        sprintf(
                            'not null with $requestData[%s][0][%s] = "%s"',
                            $model,
                            $field,
                            $v instanceof DateTimeInterface ? $v->format('Y-m-d') : $v
                        )
                    );
                }
            }
        }
    }

    /**
     * testFilterDocumentName
     */
    public function testFilterDocumentName()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $requestData = ['Documents' => [0 => ['name' => 'sample.*']]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['Documents' => [0 => ['name' => 'filename not found']]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testFilterDocumentMime
     */
    public function testFilterDocumentMime()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $requestData = ['Documents' => [0 => ['mime' => ['video/x-ms-wmv']]]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNotNull($result);
        $this->assertStringContainsString('seda1.0', Hash::get($result, 'name'));

        $requestData = ['Documents' => [0 => ['mime' => ['mime not found']]]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);
        $this->assertTrue($form->execute($form->getData()));
        $result = $query->first();
        $this->assertNull($result);
    }

    /**
     * testToEntity
     */
    public function testToEntity()
    {
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $requestData = ['Archives' => [0 => ['name' => '']]];
        $query = $Archives->find();
        $form = $this->getForm($query, $requestData);

        $entity = $form->toEntity();

        $this->assertInstanceOf(EntityInterface::class, $entity);
        $this->assertNotEmpty($entity->getError('Archives.0.name'));
        $this->assertEmpty($entity->get('Archives.0.name'));

        $requestData = ['Archives' => [0 => ['name' => 'foo']]];
        $form = $this->getForm($query, $requestData);

        $entity = $form->toEntity();

        $this->assertInstanceOf(EntityInterface::class, $entity);
        $this->assertEmpty($entity->getError('Archives.0.name'));
        $this->assertEquals('foo', $entity->get('Archives.0.name'));
    }
}
