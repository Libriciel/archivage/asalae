<?php

namespace Asalae\Test\TestCase\Form;

use Asalae\Form\InstallConfigForm;
use Asalae\Test\Mock\ConnectionManager;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Core\Configure;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\ConnectionManager as CakeConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const TMP_TESTDIR;

class InstallConfigFormTest extends TestCase
{
    public array $fixtures = [
        'app.Agreements',
        'app.AgreementsProfiles',
        'app.AgreementsServiceLevels',
        'app.AgreementsTransferringAgencies',
        'app.ArchivingSystems',
        'app.Aros',
        'app.Configurations',
        'app.Counters',
        'app.Crons',
        'app.Eaccpfs',
        'app.EventLogs',
        'app.KeywordLists',
        'app.Keywords',
        'app.Ldaps',
        'app.OrgEntities',
        'app.OrgEntitiesTimestampers',
        'app.Profiles',
        'app.Roles',
        'app.RolesTypeEntities',
        'app.SecureDataSpaces',
        'app.Sequences',
        'app.ServiceLevels',
        'app.SuperArchivistsArchivalAgencies',
        'app.Timestampers',
        'app.TypeEntities',
        'app.Users',
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationStages',
        'app.Volumes',
    ];

    private string $originalPathToLocal;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->originalPathToLocal = Configure::read('App.paths.path_to_local_config');
        $dir = TMP_TESTDIR . DS . 'testinstallconfig';
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('App.paths.path_to_local_config', $dir . DS . 'path_to_local.php');
        Filesystem::reset();
        Filesystem::dumpFile($dir . DS . 'path_to_local.php', "<?php return '$dir/app_local.json';?>");
        Filesystem::dumpFile($dir . DS . 'app_local.json', "{}");
        Utility::set(CakeConnectionManager::class, new ConnectionManager());
        ConnectionManager::$outputs = [
            'drop' => true,
            'setConfig' => true,
        ];
        $exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(['command', 'rawCommand', 'async'])
            ->getMock();
        $exec->method('async');
        $exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => 'OK', 'stderr' => ''])
        );
        $exec->method('rawCommand')->willReturnCallback(
            function ($command, array &$output = null, &$return_var = null) {
                $output = [];
                $return_var = 0;
                return '';
            }
        );
        Utility::set('Exec', $exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Configure::write('App.paths.path_to_local_config', $this->originalPathToLocal);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Utility::reset();
    }

    /**
     * testOut
     */
    public function testOut()
    {
        $form = new InstallConfigForm();

        $out = '';
        $form->stdOut = function ($v) use (&$out) {
            $out .= $v;
        };
        $form->out('test');
        $this->assertEquals('test', $out);
    }

    /**
     * testRuleIsWritable
     */
    public function testRuleIsWritable()
    {
        $form = new InstallConfigForm();
        $rule = $form->ruleIsWritable();
        $this->assertTrue($rule['rule'](TMP_TESTDIR . DS . 'testnotexists'));
        file_put_contents(TMP_TESTDIR . DS . 'testexists', 'foo');
        $this->assertTrue($rule['rule'](TMP_TESTDIR . DS . 'testexists'));
    }

    /**
     * testExecute
     */
    public function testExecute()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $OrgEntities->updateAll(['type_entity_id' => null], []);
        $OrgEntities->updateQuery()
            ->set(['identifier' => new QueryExpression("identifier  || '_test'")])
            ->execute();

        $SecureDataSpaces = TableRegistry::getTableLocator()->get('SecureDataSpaces');
        $SecureDataSpaces->updateQuery()
            ->set(['name' => new QueryExpression("name  || '_test'")])
            ->execute();

        $Users = TableRegistry::getTableLocator()->get('Users');
        $Users->updateQuery()
            ->set(['username' => new QueryExpression("username  || '_test'")])
            ->execute();

        $form = new InstallConfigForm();
        $out = '';
        $form->stdOut = function ($v) use (&$out) {
            $out .= $v;
        };
        $this->assertFalse($form->execute([]));
        $dir = TMP_TESTDIR . DS . 'testinstallconfig';
        $config = parse_ini_file(CONFIG . 'install_default.ini', true);
        $config = Hash::merge(
            $config,
            [
                'config' => [
                    'config_file' => $dir . DS . 'app_local.json',
                    'data_dir' => $dir,
                    'admin_file' => $dir . DS . 'administrators.json',
                ],
                'timestamp' => [
                    'directory' => $dir . DS . 'timestamping',
                ],
            ]
        );

        $result = $form->execute(Hash::flatten($config, '__'));
        Configure::write('debug', true);
        $this->assertTrue($result);
        $this->assertCount(3, $OrgEntities->find()->where(['type_entity_id IS NOT' => null]));

        $archivingSystems = $OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SA']);

        foreach ($archivingSystems as $as) {
            $this->assertNotNull($as->get('parent_id'));
        }

        $user = $Users->find()
            ->where(['username' => 'superadmin'])
            ->contain(['ArchivalAgencies'])
            ->first();
        $this->assertEquals('Mon Service d\'archive', Hash::get($user, 'archival_agencies.0.name'));

        $ecsId = $SecureDataSpaces->find()
            ->where(['name' => 'ECS-001'])
            ->firstOrFail()
            ->get('id');
        $defaultEcsId = $OrgEntities->find()
            ->where(['identifier' => 'sa'])
            ->firstOrFail()
            ->get('default_secure_data_space_id');
        $this->assertEquals($ecsId, $defaultEcsId);
    }

    /**
     * testExecuteWithBiggerIniFile
     */
    public function testExecuteWithBiggerIniFile()
    {
        $tables = [
            'timestampers',
            'volumes',
            'secure_data_spaces',
            'ldaps',
            'archiving_systems',
            'org_entities',
            'eaccpfs',
            'roles',
            'users',
            'validation_actors',
            'validation_stages',
            'validation_chains',
            'profiles',
            'service_levels',
            'agreements',
            'configurations',
            'keyword_lists',
            'keywords',
        ];
        $initialCounts = array_reduce(
            $tables,
            fn($c, $t) => $c + [$t => TableRegistry::getTableLocator()->get($t)->find()->all()->count()],
            []
        );

        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $OrgEntities->updateAll(['type_entity_id' => null], []);
        $OrgEntities->updateQuery()
            ->set(['identifier' => new QueryExpression("identifier  || '_test'")])
            ->execute();
        $form = new InstallConfigForm();
        $out = '';
        $form->stdOut = function ($v) use (&$out) {
            $out .= $v;
        };
        $dir = TMP_TESTDIR . DS . 'testinstallconfig';
        $config = parse_ini_file(ASALAE_CORE_TEST_DATA . DS . 'asalae2_export_v2x.ini', true);
        Filesystem::mkdir(sys_get_temp_dir() . '/timestamp');
        $config = Hash::merge(
            $config,
            [
                'config' => [
                    'config_file' => $dir . DS . 'app_local.json',
                    'data_dir' => $dir,
                    'admin_file' => $dir . DS . 'administrators.json',
                ],
                'files' => array_map(
                    fn($f) => [
                        'path' => str_replace(
                            '{TEMP_DIR}',
                            sys_get_temp_dir(),
                            $f['path']
                        ),
                        'content' => $f['content'],
                    ],
                    $config['files']
                ),
            ]
        );

        $config = Hash::flatten($config, '__');
        $form->setData($config);
        $this->assertTrue($form->validate($form->getData()));

        $form->overwrite = 'yes';
        $result = $form->execute($config);
        Configure::write('debug', true);
        $this->assertTrue($result);
        $this->assertCount(32, $OrgEntities->find()->where(['type_entity_id IS NOT' => null]));

        $finalCounts = array_reduce(
            $tables,
            fn($c, $t) => $c + [$t => TableRegistry::getTableLocator()->get($t)->find()->all()->count()],
            []
        );
        foreach ($finalCounts as $t => $c) {
            $this->assertTrue($c > $initialCounts[$t], "$c is not > to $initialCounts[$t] for $t");
        }
    }

    /**
     * testTimestampValidation
     */
    public function testTimestampValidation()
    {
        $form = new InstallConfigForm();
        $initialConfig = parse_ini_file(CONFIG . 'install_default.ini', true);
        if ($initialConfig === false) {
            $this->fail('unable to load ini file');
        } else {
            unset($initialConfig['timestamp']);
            /** @noinspection PhpAutovivificationOnFalseValuesInspection bug IDE */
            unset($initialConfig['timestampers']);
        }
        $dir = TMP_TESTDIR . DS . 'testinstallconfig';
        $initialConfig = Hash::merge(
            $initialConfig,
            [
                'config' => [
                    'config_file' => $dir . DS . 'app_local.json',
                    'data_dir' => $dir,
                    'admin_file' => $dir . DS . 'administrators.json',
                ],
            ]
        );

        $config = Hash::merge(
            $initialConfig,
            [
                'timestamp' => [
                    'name' => 'toto',
                ],
            ]
        );
        $config = Hash::flatten($config, '__');
        $form->setData($config);
        $this->assertTrue($form->validate($form->getData()));

        $form = new InstallConfigForm();
        $config = Hash::merge(
            $initialConfig,
            [
                'timestampers' => [
                    1 => ['name' => 'toto'],
                ],
            ]
        );
        $config = Hash::flatten($config, '__');
        $form->setData($config);
        $this->assertTrue($form->validate($form->getData()));

        $form = new InstallConfigForm();
        $config = Hash::merge(
            $initialConfig,
            [
                'timestamp' => [
                    'name' => 'toto',
                ],
                'timestampers' => [
                    1 => ['name' => 'toto'],
                ],
            ]
        );
        $config = Hash::flatten($config, '__');
        $form->setData($config);
        $this->assertFalse($form->validate($form->getData()));
    }
}
