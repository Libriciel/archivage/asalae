<?php

namespace Asalae\Test\TestCase\Form;

use Asalae\Form\ChangeVolumesInSpaceForm;
use Asalae\Model\Volume\VolumeManager;
use AsalaeCore\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use DateInterval;
use DateTime;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const TMP_TESTDIR;
use const TMP_VOL1;
use const TMP_VOL2;

class ChangeVolumesInSpaceFormTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public array $fixtures = [
        'app.SecureDataSpaces',
        'app.Volumes',
        'app.StoredFilesVolumes',
        'app.StoredFiles',
        'app.Crons',
        'app.CronExecutions',
        'app.EventLogs',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Filesystem::reset();
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }
    }

    /**
     * testSchema
     */
    public function testSchema()
    {
        $form = new ChangeVolumesInSpaceForm();
        $fields = $form->getSchema()->fields();
        $this->assertContains('cron', $fields);
    }

    /**
     * testExecute
     */
    public function testExecute()
    {
        $Crons = TableRegistry::getTableLocator()->get('Crons');
        $Crons->updateAll(['active' => false], []);
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01 ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02 ' . TMP_VOL2);
        $manager = new VolumeManager(1);
        $manager->filePutContent('test.txt', 'foo');
        $this->assertFileExists($file1 = TMP_VOL1 . DS . 'test.txt');
        $this->assertFileExists($file2 = TMP_VOL2 . DS . 'test.txt');

        $form = new ChangeVolumesInSpaceForm();

        $data = [
            'cron' => (new DateTime())->sub(new DateInterval('P1Y')),
            'space_id' => 1,
            'add' => [],
            'remove' => [],
        ];
        $this->assertFalse($form->validate($data));

        // retrait du volume02
        $data['remove'] = [2];
        $this->assertTrue($form->execute($data));
        $this->exec('AsalaeCore.cron');
        $this->assertOutputContains('success');

        $this->assertFileExists($file1);
        $this->assertFileDoesNotExist($file2);

        // remplacement du volume01 par le volume02
        $Crons->updateAll(['active' => false], []);
        $data['add'] = [2];
        $data['remove'] = [1];
        $this->assertTrue($form->execute($data));
        $this->exec('AsalaeCore.cron');
        $this->assertOutputContains('success');

        $this->assertFileDoesNotExist($file1);
        $this->assertFileExists($file2);
    }
}
