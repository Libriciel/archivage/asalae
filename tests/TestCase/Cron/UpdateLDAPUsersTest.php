<?php

namespace Asalae\Test\TestCase\Cron;

use Adldap\Adldap;
use Adldap\Connections\ConnectionException;
use Adldap\Connections\Provider;
use Adldap\Models\Entry;
use Adldap\Query\Builder;
use Adldap\Query\Factory;
use Asalae\Cron\UpdateLDAPUsers;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

use function __;

class UpdateLDAPUsersTest extends TestCase
{
    use IntegrationCronTrait;

    public array $fixtures = [
        'app.Aros',
        'app.CronExecutions',
        'app.Crons',
        'app.Ldaps',
        'app.OrgEntities',
        'app.Users',
    ];

    /**
     * testWork
     */
    public function testWork()
    {
        // Test update
        $ldapUser = $this->createMock(Entry::class);
        $ldapUser->method('getAttribute')->willReturnCallback(
            function ($attr) {
                if ($attr === 'displayname') {
                    return [uniqid('testupdate-')];
                } elseif ($attr === 'mail') {
                    return [uniqid('testupdate-') . '@test.fr'];
                }
            }
        );
        $Ldaps = TableRegistry::getTableLocator()->get('Ldaps');
        $ldap = $Ldaps->newEntity(
            [
                'org_entity_id' => 2,
                'name' => 'ldap',
                'host' => 'localhost',
                'port' => 1234,
                'ldap_root_search' => '(*)',
                'user_login_attribute' => 'samaccountname',
                'user_name_attribute' => 'displayname',
                'user_mail_attribute' => 'mail',
                'user_username_attribute' => 'dn',
            ]
        );
        $Ldaps->saveOrFail($ldap);
        $Users = TableRegistry::getTableLocator()->get('Users');
        $Users->updateAll(['ldap_id' => $ldap->id], ['id' => 1]);
        $users = $this->createMock(Builder::class);
        $users->method('findBy')->willReturn($ldapUser);
        $search = $this->createMock(Factory::class);
        $search->method('users')->willReturn($users);
        $provider = $this->createMock(Provider::class);
        $provider->method('search')->willReturn($search);
        $adldap = $this->createMock(Adldap::class);
        $adldap->method('connect')->willReturn($provider);
        Utility::set(Adldap::class, $adldap);
        $this->runCron(UpdateLDAPUsers::class);
        $user = TableRegistry::getTableLocator()->get('Users')->find()->where(['ldap_id' => $ldap->id])->first();
        $this->assertTextContains('testupdate-', $user->get('name'));

        // ldap test user not found
        $users = $this->createMock(Builder::class);
        $users->method('findBy')->willReturn(null);
        $search = $this->createMock(Factory::class);
        $search->method('users')->willReturn($users);
        $provider = $this->createMock(Provider::class);
        $provider->method('search')->willReturn($search);
        $adldap = $this->createMock(Adldap::class);
        $adldap->method('connect')->willReturn($provider);
        Utility::set(Adldap::class, $adldap);
        $this->runCron(UpdateLDAPUsers::class);
        $this->assertTextContains(
            __("L'utilisateur {0} n'a pas été trouvé dans le LDAP", 'admin'),
            implode('|', $this->_out->messages())
        );

        // test connexion failed
        $adldap->method('connect')->willThrowException(new ConnectionException());
        Utility::set(Adldap::class, $adldap);
        $this->runCron(UpdateLDAPUsers::class);
        $this->assertEquals(1, $this->_exitCode);
    }

    /**
     * testGetVirtualFields
     */
    public function testGetVirtualFields()
    {
        $this->assertEquals([], UpdateLDAPUsers::getVirtualFields());
    }
}
