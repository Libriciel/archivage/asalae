<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\ArchiveUnitCalcExpiredDUARoot;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

class ArchiveUnitCalcExpiredDUARootTest extends TestCase
{
    use IntegrationCronTrait;

    public array $fixtures = [
        'app.Archives',
        'app.ArchiveUnits',
        'app.AppraisalRules',
        'app.CronExecutions',
        'app.Crons',
    ];

    /**
     * testGetVirtualFields
     */
    public function testGetVirtualFields()
    {
        $this->assertEquals([], ArchiveUnitCalcExpiredDUARoot::getVirtualFields());
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $ArchiveUnits = TableRegistry::getTableLocator()->get('ArchiveUnits');
        $ArchiveUnits->updateAll(['expired_dua_root' => false], []);
        $this->runCron(ArchiveUnitCalcExpiredDUARoot::class);
        $this->assertGreaterThanOrEqual(1, $ArchiveUnits->find()->count());
    }
}
