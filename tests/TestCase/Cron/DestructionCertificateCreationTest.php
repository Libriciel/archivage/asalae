<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\DestructionCertificateCreation;
use Asalae\Model\Table\DestructionRequestsTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use DateTime;

/**
 * Asalae\Cron\DestructionCertificateCreation Test Case
 */
class DestructionCertificateCreationTest extends TestCase
{
    use IntegrationCronTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        FIXTURES_EVENT_LOGS,
        'app.ArchiveBinariesTechnicalArchiveUnits',
        'app.ArchiveUnitsDestructionRequests',
        'app.ArchiveUnitsRestitutionRequests',
        'app.ArchivesRestitutionRequests',
        'app.Configurations',
        'app.DestructionNotificationXpaths',
        'app.DestructionNotifications',
        'app.DestructionRequests',
        'app.RestitutionRequests',
        'app.Restitutions',
        'app.TechnicalArchiveUnits',
        'app.TechnicalArchives',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testGetVirtualFields
     */
    public function testGetVirtualFields()
    {
        $this->assertEquals([], DestructionCertificateCreation::getVirtualFields());
    }

    /**
     * testWorkIntermediate
     */
    public function testWorkIntermediate()
    {
        Configure::write('Attestations.destruction_delay', 7);

        $DestructionRequests = $this->fetchTable('DestructionRequests');
        $DestructionRequests->updateAll(
            [
                'state' => DestructionRequestsTable::S_DESTROYED,
                'created' => new DateTime(),
                'certified' => null,
                'states_history' => json_encode(
                    [
                        [
                            'state' => DestructionRequestsTable::S_DESTROYED,
                            'date' => (new DateTime())->format(DATE_RFC3339),
                        ],
                    ]
                ),
            ],
            ['id' => 1]
        );
        $RestitutionRequests = $this->fetchTable('RestitutionRequests');
        $RestitutionRequests->updateAll(
            [
                'state' => RestitutionRequestsTable::S_RESTITUTED,
                'created' => new DateTime(),
                'restitution_certified' => true,
                'destruction_certified' => null,
                'states_history' => json_encode(
                    [
                        [
                            'state' => RestitutionRequestsTable::S_RESTITUTED,
                            'date' => (new DateTime())->format(DATE_RFC3339),
                        ],
                    ]
                ),
            ],
            ['id' => 1]
        );
        $this->runCron(DestructionCertificateCreation::class);
        $this->assertOutputContains(
            __(
                "Création de l'attestation intermédiaire pour l'élimination {0}",
                'sa_ADR_1'
            )
        );
        $this->assertFalse($DestructionRequests->get(1)->get('certified'));
        $this->assertOutputContains(
            __(
                "Création de l'attestation intermédiaire pour la restitution {0}",
                'sa_ARR_1'
            )
        );
        $this->assertFalse($RestitutionRequests->get(1)->get('destruction_certified'));
        $this->assertOutputContains('done');
    }

    /**
     * testWorkDefinitive
     */
    public function testWorkDefinitive()
    {
        Configure::write('Attestations.destruction_delay', 0);

        $DestructionRequests = $this->fetchTable('DestructionRequests');
        $DestructionRequests->updateAll(
            [
                'state' => DestructionRequestsTable::S_DESTROYED,
                'created' => new DateTime(),
                'certified' => null,
                'states_history' => json_encode(
                    [
                        [
                            'state' => DestructionRequestsTable::S_DESTROYED,
                            'date' => (new DateTime())->format(DATE_RFC3339),
                        ],
                    ]
                ),
            ],
            ['id' => 1]
        );
        $RestitutionRequests = $this->fetchTable('RestitutionRequests');
        $RestitutionRequests->updateAll(
            [
                'state' => RestitutionRequestsTable::S_RESTITUTED,
                'created' => new DateTime(),
                'restitution_certified' => true,
                'destruction_certified' => null,
                'states_history' => json_encode(
                    [
                        [
                            'state' => RestitutionRequestsTable::S_RESTITUTED,
                            'date' => (new DateTime())->format(DATE_RFC3339),
                        ],
                    ]
                ),
            ],
            ['id' => 1]
        );
        $this->runCron(DestructionCertificateCreation::class);
        $this->assertOutputContains(
            __(
                "Création de l'attestation définitive pour l'élimination {0}",
                'sa_ADR_1'
            )
        );
        $this->assertTrue($DestructionRequests->get(1)->get('certified'));
        $this->assertOutputContains(
            __(
                "Création de l'attestation définitive pour la restitution {0}",
                'sa_ARR_1'
            )
        );
        $this->assertTrue($RestitutionRequests->get(1)->get('destruction_certified'));
        $this->assertOutputContains('done');
    }
}
