<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\GarbageCollector;
use Asalae\Model\Entity\Archive;
use Asalae\Model\Entity\ArchiveUnit;
use Asalae\Model\Entity\Transfer;
use Asalae\Model\Table\AuthUrlsTable;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const TMP_TESTDIR;

/**
 * Asalae\Cron\GarbageCollector Test Case
 */
class GarbageCollectorTest extends TestCase
{
    use IntegrationCronTrait;

    public array $fixtures = [
        'app.AccessTokens',
        'app.Archives',
        'app.AuthUrls',
        'app.CronExecutions',
        'app.Crons',
        'app.Fileuploads',
        'app.FileuploadsProfiles',
        'app.OaipmhArchives',
        'app.OaipmhListsets',
        'app.OaipmhTokens',
        'app.OrgEntities',
        'app.Profiles',
        'app.Siegfrieds',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        Filesystem::mkdir($dir);
        Filesystem::mkdir($dir . DS . 'download');
        Filesystem::mkdir($dir . DS . 'download' . DS . 'archive');
        Filesystem::mkdir($dir . DS . 'download' . DS . 'transfer');
        Configure::write('App.paths.data', TMP_TESTDIR);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        Filesystem::setNamespace();
        Filesystem::reset();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $AuthUrls->sync = true;
        Configure::write('Downloads.tempFileConservationTimeLimit', 0);

        $destArchive = Archive::getZipDownloadFolder() . '1_archive_files.zip';
        Filesystem::dumpFile($destArchive, '');

        $destArchiveUnit = ArchiveUnit::getZipDownloadFolder() . '1_archive_files.zip';
        Filesystem::dumpFile($destArchiveUnit, '');

        $destTransfer = Transfer::getZipDownloadFolder() . '1_transfer_files.zip';
        Filesystem::dumpFile($destTransfer, '');

        if (is_file($uri = TMP_TESTDIR . DS . 'bar')) {
            unlink($uri);
        }
        Filesystem::createDummyFile($uri, 10);

        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $Fileuploads->save(
            $Fileuploads->newEntity(
                [
                    'name' => 'foo',
                    'path' => TMP_TESTDIR . DS . 'bar',
                    'locked' => 0,
                    'created' => '2018-05-11 10:39:23',
                ]
            )
        );

        $authUrlCount = $AuthUrls->find()->count();

        $this->runCron(GarbageCollector::class);

        $this->assertErrorEmpty();
        $this->assertLessThan($authUrlCount, $AuthUrls->find()->count());
        $this->assertOutputContains(
            __n(
                "{0} fichier d'upload supprimé",
                "{0} fichiers d'upload supprimés",
                1,
                1
            )
        );
        $this->assertOutputContains(
            __n(
                "{0} zip d'archive supprimé",
                "{0} zip d'archive supprimés",
                1,
                1
            )
        );
        $this->assertOutputContains(
            __n(
                "{0} zip d'unité d'archives supprimé",
                "{0} zip d'unité d'archives supprimés",
                1,
                1
            )
        );
        $this->assertOutputContains(
            __n(
                "{0} fichier de transfert supprimé",
                "{0} fichiers de transfert supprimés",
                1,
                1
            )
        );

        $fileCount = count(glob(Archive::getZipDownloadFolder() . '*'));
        $this->assertEquals(0, $fileCount);
        $fileCount = count(glob(Transfer::getZipDownloadFolder() . '*'));
        $this->assertEquals(0, $fileCount);
    }

    /**
     * testWorkWithUndeletableFile
     */
    public function testWorkWithUndeletableFile()
    {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $Fileuploads->save(
            $file = $Fileuploads->newEntity(
                [
                    'name' => 'foo',
                    'path' => TMP_TESTDIR . DS . 'bar',
                    'locked' => 0,
                    'created' => '2018-05-11 10:39:23',
                ]
            )
        );
        $FileuploadsProfiles = TableRegistry::getTableLocator()->get('FileuploadsProfiles');
        $FileuploadsProfiles->save(
            $FileuploadsProfiles->newEntity(
                [
                    'fileupload_id' => $file->get('id'),
                    'profile_id' => TableRegistry::getTableLocator()->get('Profiles')->find()->first()->get('id'),
                ]
            )
        );

        touch(TMP_TESTDIR . DS . 'bar');
        $this->runCron(GarbageCollector::class);

        $this->assertErrorContains(
            __("Le fichier {0} ({1}) n'a pas pu être supprimé.", 'foo', TMP_TESTDIR . DS . 'bar')
        );
        $this->assertOutputContains(
            __n(
                "{0} fichier d'upload supprimé",
                "{0} fichiers d'upload supprimés",
                0,
                0
            )
        );
    }

    /**
     * testWorkEvenWithNoFolder
     */
    public function testWorkEvenWithNoFolder()
    {
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        $this->runCron(GarbageCollector::class);
        $this->assertOutputContains(
            __n(
                "{0} zip d'archive supprimé",
                "{0} zip d'archive supprimés",
                0,
                0
            )
        );
        $this->assertOutputContains(
            __n(
                "{0} fichier de transfert supprimé",
                "{0} fichiers de transfert supprimés",
                0,
                0
            )
        );
    }
}
