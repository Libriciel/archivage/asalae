<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\MailNotifications;
use Asalae\Model\Table\BeanstalkJobsTable;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use DateTime;

class MailNotificationsTest extends TestCase
{
    use IntegrationCronTrait;

    public array $fixtures = [
        'app.Agreements',
        'app.AuthUrls',
        'app.BeanstalkJobs',
        'app.CronExecutions',
        'app.Crons',
        'app.Mails',
        'app.OrgEntities',
        'app.Roles',
        'app.SuperArchivistsArchivalAgencies',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
        'app.ValidationActors',
        'app.ValidationChains',
        'app.ValidationProcesses',
        'app.ValidationStages',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    /**
     * testWork
     */
    public function testWork()
    {
        // on veux que seul le User 1 reçoive les mails
        $Users = $this->fetchTable('Users');
        $Users->updateAll(
            [
                'notify_validation_frequency' => null,
                'notify_job_frequency' => null,
                'notify_agreement_frequency' => null,
                'notify_transfer_report_frequency' => null,
            ],
            []
        );
        $Users->updateAll(
            [
                'notify_validation_frequency' => 1,
                'notify_job_frequency' => 1,
                'notify_agreement_frequency' => 1,
                'notify_transfer_report_frequency' => 1,
            ],
            ['id' => 1]
        );

        $Mails = $this->fetchTable('Mails');
        $Mails->deleteAll([]);

        // notify validation
        $ValidationProcesses = $this->fetchTable('ValidationProcesses');
        $ValidationProcesses->deleteAll(['id !=' => 8]);

        // notify jobs
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $BeanstalkJobs->deleteAll([]);
        $job = $BeanstalkJobs->newEntity(
            [
                'job_state' => BeanstalkJobsTable::S_FAILED,
                'object_model' => 'Transfers',
                'object_foreign_key' => 1,
                'user_id' => 1,
            ]
        );
        $BeanstalkJobs->saveOrFail($job);

        // notify agreement
        $Agreements = $this->fetchTable('Agreements');
        $Agreements->updateAll(['notify_delay' => 1], ['id' => 2]);

        // notify transfert
        $Transfers = $this->fetchTable('Transfers');
        $Transfers->updateAll(['created' => new DateTime()], ['id' => 2]);

        $this->runCron(MailNotifications::class);
        $this->assertEquals('success', $this->workOutput);
        $this->assertGreaterThanOrEqual(4, $Mails->find()->count());

        // super archiviste
        $Users->updateAll(
            ['role_id' => 11, 'org_entity_id' => 1],
            ['id' => 1]
        );
        $this->runCron(MailNotifications::class);
        $this->assertEquals('success', $this->workOutput);
        $this->assertGreaterThanOrEqual(7, $Mails->find()->count());
    }
}
