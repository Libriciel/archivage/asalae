<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\ArchiveUnitCalcSearch;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/**
 * Asalae\Cron\ArchiveUnitCalcSearch Test Case
 */
class ArchiveUnitCalcSearchTest extends TestCase
{
    use IntegrationCronTrait;
    use InvokePrivateTrait;

    public array $fixtures = [
        'app.AccessRules',
        'app.ArchiveDescriptions',
        'app.ArchiveKeywords',
        'app.ArchiveUnits',
        'app.Archives',
        'app.CronExecutions',
        'app.Crons',
        'app.OrgEntities',
    ];

    /**
     * testWork
     */
    public function testWork()
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $Archives = $loc->get('Archives');
        $ArchiveUnits->updateAll(['search' => null], []);
        $Archives->updateAll(['next_search' => '1900-01-01'], []);

        $this->runCron(ArchiveUnitCalcSearch::class);
        $search = Hash::get($ArchiveUnits->get(1), 'search');
        $this->assertNotNull($search);
        $this->assertTextContains('"sa_1"', $search);
        $this->assertNull(Hash::get($Archives->get(1), 'next_search'));
    }
}
