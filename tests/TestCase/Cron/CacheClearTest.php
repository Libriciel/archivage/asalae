<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\CacheClear;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Cache\Cache;

/**
 * Asalae\Cron\ArchiveUnitCalcSearch Test Case
 */
class CacheClearTest extends TestCase
{
    use IntegrationCronTrait;

    private bool $cacheEnabled;
    private array $cacheConfig = [];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->cacheEnabled = Cache::enabled();
        $this->cacheConfig = [];
        foreach (Cache::configured() as $configName) {
            if ($configName !== '_cake_core_') { // cause un bug si il n'existe plus
                $this->cacheConfig[$configName] = Cache::getConfig($configName);
                Cache::drop($configName);
            }
        }
        if (!$this->cacheEnabled) {
            Cache::enable();
        }
        Cache::setConfig(
            'test',
            [
                'className' => 'File',
                'prefix' => 'test_',
                'path' => CACHE . 'test' . DS,
                'url' => env('CACHE_DEFAULT_URL'),
                'duration' => '+1 hour',
            ]
        );
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Cache::clear('test');
        Cache::drop('test');
        Cache::setConfig($this->cacheConfig);
        if (!$this->cacheEnabled) {
            Cache::disable();
        }
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $filename1 = CACHE . 'test' . DS . 'test_test1';
        $filename2 = CACHE . 'test' . DS . 'test_test2';
        if (!is_dir(dirname($filename1))) {
            mkdir(dirname($filename1), 0777, true);
        }
        if (is_file($filename1)) {
            unlink($filename1);
        }
        if (is_file($filename2)) {
            unlink($filename2);
        }
        touch($filename1, strtotime('-61 minutes'));
        touch($filename2, strtotime('-59 minutes'));
        $this->assertFileExists($filename1);
        $this->assertFileExists($filename2);
        $this->runCron(CacheClear::class);
        $this->assertFileDoesNotExist($filename1);
        $this->assertFileExists($filename2);
    }
}
