<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\OutgoingTransferTracking;
use Asalae\Test\Mock\ClientMock;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use Cake\Core\Configure;
use Cake\Http\Client\Response;
use Cake\ORM\TableRegistry;
use DateInterval;
use DateTime;

use const TMP_TESTDIR;

/**
 * Asalae\Cron\OutgoingTransferTracking Test Case
 */
class OutgoingTransferTrackingTest extends TestCase
{
    use IntegrationCronTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveKeywords',
        'app.ArchiveUnits',
        'app.ArchiveUnitsOutgoingTransferRequests',
        'app.Archives',
        'app.ArchivingSystems',
        'app.CronExecutions',
        'app.Crons',
        'app.EventLogs',
        'app.OrgEntities',
        'app.MessageIndicators',
        'app.OutgoingTransferRequests',
        'app.OutgoingTransfers',
        'app.ValidationChains',
        'app.Users',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();

        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('App.paths.data', TMP_TESTDIR);
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $xml1 = <<<EOT
<?xml version="1.0"?>
<Acknowledgement xmlns="fr:gouv:culture:archivesdefrance:seda:v1.0">
  <Comment/>
  <Date>2020-12-03T13:54:00+01:00</Date>
  <AcknowledgementIdentifier>sa_ACK_32</AcknowledgementIdentifier>
  <MessageReceivedIdentifier>sa_AT_297</MessageReceivedIdentifier>
  <Receiver>
    <Identification>sa</Identification>
    <Name>SA Libriciel</Name>
  </Receiver>
  <Sender>
    <Identification>sa</Identification>
    <Name>SA Libriciel</Name>
  </Sender>
</Acknowledgement>
EOT;
        $xml2 = <<<EOT
<?xml version="1.0"?>
<ArchiveTransferReply xmlns="fr:gouv:culture:archivesdefrance:seda:v1.0">
  <Comment/>
  <Date>2020-12-03T13:54:00+01:00</Date>
  <GrantDate>2020-12-03T13:54:00+01:00</GrantDate>
  <ReplyCode listVersionID="edition 2009">000</ReplyCode>
  <TransferIdentifier>sa_AT_297</TransferIdentifier>
  <TransferReplyIdentifier>sa_ATR_38</TransferReplyIdentifier>
  <ArchivalAgency>
    <Identification>sa</Identification>
    <Name>SA Libriciel</Name>
  </ArchivalAgency>
  <TransferringAgency>
    <Identification>sa</Identification>
    <Name>SA Libriciel</Name>
  </TransferringAgency>
  <Archive>
    <ArchivalAgencyArchiveIdentifier>sa_377</ArchivalAgencyArchiveIdentifier>
    <DescriptionLanguage>fra</DescriptionLanguage>
    <Name>758c18c4-5cd6-41c2-98f6-233686a964fd</Name>
    <ContentDescription>
      <DescriptionLevel>file</DescriptionLevel>
      <Language>fra</Language>
    </ContentDescription>
    <AccessRestrictionRule>
      <Code listVersionID="edition 2009">AR038</Code>
      <StartDate>2020-12-03</StartDate>
    </AccessRestrictionRule>
    <AppraisalRule>
      <Code>detruire</Code>
      <Duration>P0Y</Duration>
      <StartDate>2020-12-03</StartDate>
    </AppraisalRule>
    <Document>
      <ArchivalAgencyDocumentIdentifier>sa_377_0001</ArchivalAgencyDocumentIdentifier>
      <Attachment mimeCode="text/plain" filename="file1.txt"/>
      <Integrity algorithme="sha256">f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2</Integrity>
      <Size unitCode="Q12">5</Size>
      <Type>CDO</Type>
    </Document>
  </Archive>
</ArchiveTransferReply>
EOT;

        $response = $this->createMock(Response::class);
        $response->method('getStringBody')->willReturn($xml1, $xml2);
        $response->method('getStatusCode')->willReturn(200);
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get'])
            ->getMock();
        $client->method('get')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());

        // initialisation
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $Archives->updateAll(['state' => 'transferring'], ['id' => 1]);
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateAll(['state' => 'transferring'], []);
        $OutgoingTransfers = $loc->get('OutgoingTransfers');
        $OutgoingTransfers->updateAll(['state' => 'sent', 'archive_unit_id' => 1], []);
        $OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
        $ArchiveUnitsOutgoingTransferRequests = $loc->get('ArchiveUnitsOutgoingTransferRequests');
        $ArchiveUnitsOutgoingTransferRequests->updateAll(['archive_unit_id' => 1], ['id' => 1]);
        $OutgoingTransferRequests->updateAll(
            [
                'state' => 'transfers_sent',
                'last_state_update' => (new DateTime())->sub(new DateInterval('P100Y')),
            ],
            ['id' => 1]
        );

        // test
        $this->runCron(OutgoingTransferTracking::class);
        $this->assertExitCode(0);
        $this->assertEquals(
            'transfers_processed',
            $OutgoingTransferRequests->get(1)->get('state')
        );
    }
}
