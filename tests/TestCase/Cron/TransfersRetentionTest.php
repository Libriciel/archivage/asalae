<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\TransfersRetention;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use DateInterval;
use Libriciel\Filesystem\Utility\Filesystem;

use const DS;
use const TMP_TESTDIR;

/**
 * Asalae\Cron\TransfersRetention Test Case
 */
class TransfersRetentionTest extends TestCase
{
    use IntegrationCronTrait;

    public array $fixtures = [
        'app.AccessRuleCodes',
        'app.AccessRules',
        'app.AppraisalRuleCodes',
        'app.AppraisalRules',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.CronExecutions',
        'app.Crons',
        'app.EventLogs',
        'app.Fileuploads',
        'app.OrgEntities',
        'app.SecureDataSpaces',
        'app.ServiceLevels',
        'app.Siegfrieds',
        'app.TransferAttachments',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
    ];

    public $testfile = 'transfers/%d/attachments/file';
    public $testfile2 = 'transfers/%d/attachments/sample.pdf';

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $transfer1 = $Transfers->newEntity(
            [
                'transfer_comment' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'transfer_date' => '2018-04-25 09:34:15.000000',
                'transfer_identifier' => 'atr_alea_c764221aa44c399baf82d422dc432333',
                'archival_agency_id' => 2,
                'transferring_agency_id' => 4,
                'originating_agency_id' => 4,
                'message_version' => 'seda1.0',
                'state' => 'accepted',
                'is_modified' => false,
                'filename' => 'testfile.xml',
                'data_size' => 0,
                'created_user_id' => 1,
                'service_level_id' => 1,
                'files_deleted' => false,
            ]
        );
        $Transfers->saveOrFail($transfer1);
        Filesystem::createDummyFile(sprintf(TMP_TESTDIR . DS . $this->testfile, $transfer1->id), 10);
        Filesystem::createDummyFile(sprintf(TMP_TESTDIR . DS . $this->testfile2, $transfer1->id), 10);

        $transfer2 = $Transfers->newEntity(
            [
                'transfer_comment' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'transfer_date' => '2018-04-25 09:34:15.000000',
                'transfer_identifier' => 'atr_alea_c764221aa44c399baf82d422dc432333',
                'archival_agency_id' => 2,
                'transferring_agency_id' => 4,
                'originating_agency_id' => 4,
                'message_version' => 'seda1.0',
                'state' => 'accepted',
                'is_modified' => false,
                'filename' => 'testfile.xml',
                'data_size' => 0,
                'created_user_id' => 1,
                'service_level_id' => 1,
                'files_deleted' => false,
            ]
        );
        $Transfers->saveOrFail($transfer2);
        $archive = $Archives->newEntity(
            [ // 5
                'state' => 'available',
                'is_integrity_ok' => true,
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'originating_agency_id' => 2,
                'transfer_id' => $transfer1->id,
                'access_rule_id' => 1,
                'appraisal_rule_id' => 1,
                'archival_agency_identifier' => 'control_integrity_2',
                'secure_data_space_id' => 1,
                'storage_path' => 'control_integrity',
                'name' => 'control_integrity',
                'transferred_size' => 0,
                'management_size' => 0,
                'original_size' => 0,
                'preservation_size' => 0,
                'dissemination_size' => 0,
                'timestamp_size' => 0,
                'transferred_count' => 0,
                'management_count' => 0,
                'original_count' => 0,
                'preservation_count' => 0,
                'dissemination_count' => 0,
                'timestamp_count' => 0,
                'xml_node_tagname' => 'Archive[2]',
                'created' => '2018-04-25 09:34:17.000000',
            ]
        );
        $Archives->saveOrFail($archive);
        $archive->set('integrity_date', $archive->get('created')->add(new DateInterval('P5D')));
        $Archives->saveOrFail($archive);
        $TransferAttachments->saveOrFail(
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => $transfer1->id,
                    'filename' => 'file',
                    'size' => 1,
                    'hash' => 'ABD1472D2DDB6BC98565B56CE59A5FB4C66D108EB9643E5E503C35C243EB69C9',
                    'hash_algo' => 'sha256',
                    'mime' => 'Lorem ipsum dolor sit amet',
                    'extension' => 'Lorem ipsum dolor sit amet',
                    'format' => 'Lorem ipsum dolor sit amet',
                ]
            )
        );
        $TransferAttachments->saveOrFail(
            $TransferAttachments->newEntity(
                [
                    'transfer_id' => $transfer1->id,
                    'filename' => 'sample.pdf',
                    'size' => 1,
                    'hash' => 'ABD1472D2DDB6BC98565B56CE59A5FB4C66D108EB9643E5E503C35C243EB69C9',
                    'hash_algo' => 'sha256',
                    'mime' => 'Lorem ipsum dolor sit amet',
                    'extension' => 'Lorem ipsum dolor sit amet',
                    'format' => 'Lorem ipsum dolor sit amet',
                ]
            )
        );

        // transfer without attachment
        $Transfers->saveOrFail(
            $Transfers->newEntity(
                [ // 5
                    'transfer_comment' => 'transfer without attachment',
                    'transfer_date' => '2018-04-25 09:34:15.000000',
                    'transfer_identifier' => 'atr_alea_c764221aa44c399baf82d422dc432333',
                    'archival_agency_id' => 2,
                    'transferring_agency_id' => 4,
                    'originating_agency_id' => 4,
                    'message_version' => 'seda1.0',
                    'state' => 'accepted',
                    'is_modified' => false,
                    'filename' => 'testfile.xml',
                    'data_size' => 0,
                    'created_user_id' => 1,
                    'service_level_id' => 1,
                    'files_deleted' => false,
                ]
            )
        );
        $archive = $Archives->newEntity(
            [ // 6
                'state' => 'available',
                'is_integrity_ok' => true,
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'originating_agency_id' => 2,
                'transfer_id' => $transfer2->id,
                'access_rule_id' => 1,
                'appraisal_rule_id' => 1,
                'archival_agency_identifier' => 'control_integrity_3',
                'secure_data_space_id' => 1,
                'storage_path' => 'control_integrity',
                'name' => 'control_integrity',
                'transferred_size' => 0,
                'management_size' => 0,
                'original_size' => 0,
                'preservation_size' => 0,
                'dissemination_size' => 0,
                'timestamp_size' => 0,
                'transferred_count' => 0,
                'management_count' => 0,
                'original_count' => 0,
                'preservation_count' => 0,
                'dissemination_count' => 0,
                'timestamp_count' => 0,
                'xml_node_tagname' => 'Archive[3]',
            ]
        );
        $Archives->saveOrFail($archive);
        $archive->set('integrity_date', $archive->get('created')->add(new DateInterval('P5D')));
        $Archives->saveOrFail($archive);

        $this->runCron(TransfersRetention::class);
        $this->assertOutputContains('3 fichiers supprimés');
        $this->assertOutputContains('3 transferts traité');
        $this->assertFileDoesNotExist(TMP_TESTDIR . DS . $this->testfile);
        $this->assertFileDoesNotExist(TMP_TESTDIR . DS . $this->testfile2);
        $this->assertTrue(
            (bool)$Transfers->get($transfer1->id)->get('files_deleted')
        );
        $this->assertFalse($Transfers->get($transfer2->id)->get('files_deleted'));
    }
}
