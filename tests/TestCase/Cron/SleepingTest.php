<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\Sleeping;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;

/**
 * Asalae\Cron\Sleeping Test Case
 */
class SleepingTest extends TestCase
{
    use IntegrationCronTrait;

    public array $fixtures = [
        'app.CronExecutions',
        'app.Crons',
    ];

    /**
     * testWork
     */
    public function testWork()
    {
        $this->runCron(Sleeping::class, ['sleepTime' => 0, 'busyWait' => true]);
        $this->assertExitCode(0);
        $this->runCron(Sleeping::class, ['sleepTime' => 0, 'busyWait' => false]);
        $this->assertExitCode(0);
    }

    /**
     * testGetOutput
     */
    public function testGetOutput()
    {
        $cron = new Sleeping();
        $this->assertEquals('sleeping', $cron->getOutput());
    }
}
