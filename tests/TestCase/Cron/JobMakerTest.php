<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\JobMaker;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\BeanstalkJobsTable;
use Asalae\Model\Table\DeliveryRequestsTable;
use Asalae\Model\Table\DestructionRequestsTable;
use Asalae\Model\Table\RestitutionRequestsTable;
use Asalae\Model\Table\TransfersTable;
use Asalae\Test\Mock\GenericCaller;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateTime;

use function __;
use function __n;

/**
 * Asalae\Cron\JobMaker Test Case
 */
class JobMakerTest extends TestCase
{
    use IntegrationCronTrait;

    public array $fixtures = [
        'app.ArchiveUnits',
        'app.Archives',
        'app.ArchivesTransfers',
        'app.BatchTreatments',
        'app.BeanstalkJobs',
        'app.CronExecutions',
        'app.Crons',
        'app.DeliveryRequests',
        'app.DestructionRequests',
        'app.EventLogs',
        'app.Fileuploads',
        'app.OrgEntities',
        'app.OrgEntitiesTimestampers',
        'app.OutgoingTransferRequests',
        'app.RestitutionRequests',
        'app.ServiceLevels',
        'app.Siegfrieds',
        'app.Timestampers',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
    ];

    public $emitedData;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Utility::reset();
        Configure::write('Beanstalk.disable_check_ttr', true);
        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $this->emitedData = [];
        $beanstalk->method('emit')->willReturnCallback(
            function ($data) {
                $this->emitedData[] = $data;
            }
        );
        $beanstalk->method('isConnected')->willReturn(true);
        $beanstalk->method('setTube')->willReturnSelf();
        Utility::set('Beanstalk', $beanstalk);

        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $BeanstalkJobs->sync = true;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
    }

    /**
     * testgetVirtualFields
     */
    public function testgetVirtualFields()
    {
        $this->assertEmpty(JobMaker::getVirtualFields());
    }

    /**
     * testWorkTransfers
     */
    public function testWorkTransfers()
    {
        $this->fetchTable('BeanstalkJobs')->deleteAll([]);
        $loc = TableRegistry::getTableLocator();
        $loc->get('BatchTreatments')->deleteAll([]);
        $this->runCron(JobMaker::class);
        $this->assertOutputContains(__("Aucun job n'a besoin d'être lancé"));

        $Timestampers = TableRegistry::getTableLocator()->get('Timestampers');
        $timestamper = $Timestampers->newEntity(
            [
                'name' => 'test',
                'fields' => '{"driver":"test","name":"test","myfield":"test"}',
            ]
        );
        $Timestampers->saveOrFail($timestamper);
        Configure::write(
            'Timestamping.drivers.test',
            [
                'name' => 'test',
                'class' => GenericCaller::class,
                'fields' => ['myfield' => ['label' => 'my field']],
            ]
        );
        GenericCaller::$methods['ping'] = true;
        $OrgEntities = $loc->get('OrgEntities');
        $OrgEntities->updateAll(['timestamper_id' => $timestamper->id], ['id' => 2]);
        $ServiceLevels = $loc->get('ServiceLevels');
        $ServiceLevels->updateAll(['ts_msg_id' => 1], []);

        $Archives = $loc->get('Archives');
        $Transfers = $loc->get('Transfers');
        $baseData = [
            'message_version' => 'seda1.0',
            'state' => 'timestamping',
            'is_modified' => false,
            'filename' => 'foo.xml',
            'archival_agency_id' => 2,
            'created_user_id' => null,
            'transfer_identifier' => 'testunit',
            'transfer_date' => '03/04/2019 14:23',
            'transfer_comment' => '',
            'transferring_agency_id' => '2',
            'originating_agency_id' => '2',
            'agreement_id' => '',
            'profile_id' => '',
            'service_level_id' => 1,
            'name' => 'Archive',
            'code' => 'AR038',
            'start_date' => '03/04/2019',
            'last_state_update' => new DateTime('1900-01-01'),
            'files_deleted' => false,
        ];

        $unlockList = [
            ['timestamping', 'timestamp'],
            ['analysing', 'analyse'],
            ['controlling', 'control'],
            ['archiving', 'archive'],
        ];
        foreach ($unlockList as $i => $state) {
            if (is_array($state)) {
                $tube = $state[1];
                $state = $state[0];
            } else {
                $tube = $state;
            }
            $Archives->deleteAll([]);
            $Transfers->deleteAll([]);
            $transfer = $Transfers->newEntity(
                [
                    'state' => $state,
                    'created_user_id' => 1,
                ] + $baseData
            );
            $this->assertNotFalse($Transfers->save($transfer));
            $this->runCron(JobMaker::class);
            $this->assertOutputContains(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    1,
                    1,
                    $tube
                )
            );
            $this->assertEquals(
                $transfer->get('id'),
                Hash::get($this->emitedData, $i . '.transfer_id')
            );
        }
    }

    /**
     * testWorkArchives
     */
    public function testWorkArchives()
    {
        $this->fetchTable('BeanstalkJobs')->deleteAll([]);
        $loc = TableRegistry::getTableLocator();
        $loc->get('BatchTreatments')->deleteAll([]);
        $this->runCron(JobMaker::class);
        $this->assertOutputContains(__("Aucun job n'a besoin d'être lancé"));

        $ServiceLevels = $loc->get('ServiceLevels');
        $ServiceLevels->updateAll(['ts_msg_id' => 1], []);
        $Archives = $loc->get('Archives');
        $Archives->deleteAll(['id !=' => 1]);
        $unlockList = [
            [ArchivesTable::S_CREATING, 'archive-unit'],
            [ArchivesTable::S_DESCRIBING, 'archive-binary'],
            [ArchivesTable::S_STORING, 'archive-management'],
            [ArchivesTable::S_MANAGING, 'archive-management'],
        ];
        $Transfers = $loc->get('Transfers');
        $Transfers->updateAll(['state' => TransfersTable::S_ARCHIVING], ['id' => 1]);
        foreach ($unlockList as $i => $state) {
            if (is_array($state)) {
                $tube = $state[1];
                $state = $state[0];
            } else {
                $tube = $state;
            }
            $Archives->updateAll(['state' => $state], []);
            $this->runCron(JobMaker::class);
            $this->assertOutputContains(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    1,
                    1,
                    $tube
                )
            );
            $this->assertEquals(
                1,
                Hash::get($this->emitedData, $i . '.archive_id')
            );
        }
    }

    /**
     * testcheckBatchTreatments
     */
    public function testcheckBatchTreatments()
    {
        $this->fetchTable('BeanstalkJobs')->deleteAll(['id !=' => 2]);
        $this->runCron(JobMaker::class);
        $this->assertOutputContains(
            __("Job {0} relancé pour {1}:{2}", 'batch-treatment', 'BatchTreatments', 1)
        );
    }

    /**
     * testcheckDeliveries
     */
    public function testcheckDeliveries()
    {
        $this->fetchTable('BeanstalkJobs')->deleteAll([]);
        $this->fetchTable('DeliveryRequests')
            ->updateAll(['state' => DeliveryRequestsTable::S_ACCEPTED], ['id' => 1]);
        $this->runCron(JobMaker::class);
        $this->assertOutputContains(
            __("Job {0} créé pour {1}:{2}", 'delivery', 'DeliveryRequests', 1)
        );
    }

    /**
     * testcheckDestructions
     */
    public function testcheckDestructions()
    {
        $this->fetchTable('BeanstalkJobs')->deleteAll([]);
        $this->fetchTable('DestructionRequests')
            ->updateAll(['state' => DestructionRequestsTable::S_ACCEPTED], ['id' => 1]);
        $this->runCron(JobMaker::class);
        $this->assertOutputContains(
            __("Job {0} créé pour {1}:{2}", 'destruction', 'DestructionRequests', 1)
        );
    }

    /**
     * testcheckRestitutions
     */
    public function testcheckRestitutions()
    {
        $this->fetchTable('BeanstalkJobs')->deleteAll([]);
        $this->fetchTable('RestitutionRequests')
            ->updateAll(['state' => RestitutionRequestsTable::S_ACCEPTED], ['id' => 1]);
        $this->runCron(JobMaker::class);
        $this->assertOutputContains(
            __("Job {0} créé pour {1}:{2}", 'restitution-build', 'RestitutionRequests', 1)
        );
    }
}
