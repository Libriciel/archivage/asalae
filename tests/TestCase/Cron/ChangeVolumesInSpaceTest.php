<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\ChangeVolumesInSpace;
use Asalae\Model\Table\VolumesTable;
use Asalae\Model\Volume\VolumeManager;
use Asalae\Test\Mock\MockVolume;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;

use const DS;
use const TMP_TESTDIR;
use const TMP_VOL1;
use const TMP_VOL2;

/**
 * Asalae\Cron\ChangeVolumesInSpace Test Case
 */
class ChangeVolumesInSpaceTest extends TestCase
{
    use IntegrationCronTrait;

    public array $fixtures = [
        'app.Crons',
        'app.CronExecutions',
        'app.StoredFiles',
        'app.StoredFilesVolumes',
        'app.Volumes',
        'app.SecureDataSpaces',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }
        Configure::write('ChangeVolumesInSpace.log_dir', TMP_TESTDIR . DS);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        mkdir(TMP_TESTDIR);
        ChangeVolumesInSpace::$sleepTime = 0;
        ChangeVolumesInSpace::$feedBackCooldown = .0;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Utility::reset();
        Filesystem::setNamespace();
        Filesystem::reset();
        Configure::write(
            VolumeManager::DRIVERS_CONFIG_PATH . '.FILESYSTEM.class',
            'AsalaeCore\Driver\Volume\VolumeFilesystem'
        );
        TableRegistry::getTableLocator()->clear();
    }

    /**
     * testGetVirtualFields
     */
    public function testGetVirtualFields()
    {
        $this->assertNotEmpty(ChangeVolumesInSpace::getVirtualFields());
    }

    /**
     * testWork
     */
    public function testWork()
    {
        Filesystem::mkdir($vol1 = TMP_VOL1);
        Filesystem::mkdir($vol2 = TMP_VOL2);

        // provoque un unlink (juste pour la couverture de code)
        touch(
            sprintf(
                '%schange_volume_%d.log',
                Configure::read('App.paths.logs', LOGS),
                1
            )
        );

        $loc = TableRegistry::getTableLocator();
        $Volumes = $loc->get('Volumes');
        $Volumes->updateAll(
            [
                'secure_data_space_id' => null,
                'disk_usage' => 0,
            ],
            ['id !=' => 1]
        );
        $Volumes->updateAll(['disk_usage' => 0], ['id' => 1]);

        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $StoredFilesVolumes->deleteAll([]);

        $StoredFiles = $loc->get('StoredFiles');
        $StoredFiles->deleteAll([]);

        $manager = new VolumeManager(1);
        $file1 = hash('sha256', uniqid('test', true));
        $manager->filePutContent($filename = 'foo/bar/baz.txt', $file1);

        $this->assertFileExists($vol1 . DS . $filename);
        $this->assertFileDoesNotExist($vol2 . DS . $filename);

        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [2],
                'remove' => [1],
                'uid' => 'test',
            ]
        );
        $this->assertExitSuccess();
        $this->assertOutputContains(
            __(
                "Copie de {0,number,#,###} fichiers vers {1}",
                1,
                'volume02'
            )
        );
        $this->assertOutputContains(
            __(
                "Suppression de {0,number,#,###} fichiers du volume {1}",
                1,
                'volume01'
            )
        );
        $this->assertOutputContains('1 / 1');
        $this->_out = $this->_err = null;

        $this->assertFileExists($vol2 . DS . $filename);
        $this->assertFileDoesNotExist($vol1 . DS . $filename);
        $this->assertNull($Volumes->get(1)->get('secure_data_space_id'));
        $this->assertEquals(1, $Volumes->get(2)->get('secure_data_space_id'));

        // test à vide
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [],
                'remove' => [],
                'uid' => 'test',
            ]
        );
        $this->assertExitSuccess();
    }

    /**
     * testvolumeTestFailed
     */
    public function testvolumeTestFailed()
    {
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);
        Configure::write(
            VolumeManager::DRIVERS_CONFIG_PATH . '.FILESYSTEM.class',
            MockVolume::class
        );

        $this->fetchTable('Volumes')->updateAll(
            ['secure_data_space_id' => null],
            ['id' => 2]
        );

        MockVolume::$tests = [['success' => false]];
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [2],
                'remove' => [1],
                'uid' => 'test',
            ]
        );
        $this->assertExitError();
        $this->assertOutputContains(
            __(
                "Le test d'écriture sur le volume {0} a échoué",
                'volume02'
            )
        );
    }

    /**
     * testvolumeIsUsed
     */
    public function testvolumeIsUsed()
    {
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);

        $this->fetchTable('Volumes')->updateAll(
            ['secure_data_space_id' => null],
            ['id' => 2]
        );

        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [2],
                'remove' => [1],
                'uid' => 'test',
            ]
        );
        $this->assertExitError();
        $this->assertOutputContains(
            __(
                "Des données existent déjà dans le volume {0}",
                'volume02'
            )
        );
    }

    /**
     * testvolumeSizeIsLowerThanFileSize
     */
    public function testvolumeSizeIsLowerThanFileSize()
    {
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);
        $this->fetchTable('Volumes')
            ->updateAll(['disk_usage' => 0, 'max_disk_usage' => 1], ['id' => 2]);

        $this->fetchTable('Volumes')->updateAll(
            ['secure_data_space_id' => null],
            ['id' => 2]
        );

        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [2],
                'remove' => [1],
                'uid' => 'test',
            ]
        );
        $this->assertExitError();
        $this->assertOutputContains(
            __(
                "La taille du volume {0} est insuffisante",
                'volume02'
            )
        );
    }

    /**
     * testaddVolumeInEmptySpace
     */
    public function testaddVolumeInEmptySpace()
    {
        Filesystem::mkdir(TMP_VOL1);
        $this->fetchTable('Volumes')->updateAll(
            [
                'secure_data_space_id' => null,
                'disk_usage' => 0,
            ],
            []
        );
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [1],
                'remove' => [],
                'uid' => 'test',
            ]
        );
        $this->assertExitSuccess();
    }

    /**
     * testaddVolumeInEmptySpaceSaveError
     */
    public function testaddVolumeInEmptySpaceSaveError()
    {
        Filesystem::mkdir(TMP_VOL1);
        $this->fetchTable('Volumes')->updateAll(
            [
                'secure_data_space_id' => null,
                'disk_usage' => 0,
            ],
            []
        );
        /** @var VolumesTable|MockObject $Volumes */
        $Volumes = $this->getMockBuilder(VolumesTable::class)
            ->onlyMethods(['save'])
            ->getMock();
        $Volumes->setTable('volumes');
        $Volumes->method('save')->willReturn(false);
        TableRegistry::getTableLocator()->set('Volumes', $Volumes);
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [1],
                'remove' => [],
                'uid' => 'test',
            ]
        );
        $this->assertExitError();
        $this->assertOutputContains(
            __(
                "L'attribution du volume {0} a échoué",
                'volume01'
            )
        );
    }

    /**
     * testaddVolumeException
     */
    public function testaddVolumeException()
    {
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);

        $loc = TableRegistry::getTableLocator();
        $Volumes = $loc->get('Volumes');
        $Volumes->updateAll(
            [
                'secure_data_space_id' => null,
                'disk_usage' => 0,
            ],
            ['id !=' => 1]
        );
        $Volumes->updateAll(['disk_usage' => 0], ['id' => 1]);

        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $StoredFilesVolumes->deleteAll([]);

        $StoredFiles = $loc->get('StoredFiles');
        $StoredFiles->deleteAll([]);

        $manager = new VolumeManager(1);
        $file1 = hash('sha256', uniqid('test', true));
        $manager->filePutContent('foo/bar/baz.txt', $file1);
        Configure::write(
            VolumeManager::DRIVERS_CONFIG_PATH . '.FILESYSTEM.class',
            MockVolume::class
        );
        MockVolume::$streamToExceptions = [false, 1]; // test(), uploadToVolume()
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [2],
                'remove' => [1],
                'uid' => 'test',
            ]
        );
        $this->assertExitSuccess(); // ok après un sleep
        $this->assertOutputContains(
            __(
                "Nouvelle tentative d'upload de {0} vers le volume {1} dans {2} seconde...",
                'foo/bar/baz.txt',
                2,
                0
            )
        );
        $this->_out = $this->_err = null;

        // test() + 10 uploadToVolume()
        MockVolume::$streamToExceptions = array_merge(
            [false],
            array_fill(0, 10, 1)
        );
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [1],
                'remove' => [2],
                'uid' => 'test',
            ]
        );
        $this->assertExitError();
        $this->assertOutputContains(
            __(
                "Echec lors de la copie de {0} vers {1}",
                'foo/bar/baz.txt',
                'volume01'
            )
        );
        $this->_out = $this->_err = null;

        // echec lors du Volume->save()
        $Volumes = $loc->get('Volumes');
        $Volumes->updateAll(
            [
                'secure_data_space_id' => null,
                'disk_usage' => 0,
            ],
            ['id !=' => 1]
        );
        $Volumes->updateAll(['disk_usage' => 0], ['id' => 1]);

        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $StoredFilesVolumes->deleteAll([]);

        $StoredFiles = $loc->get('StoredFiles');
        $StoredFiles->deleteAll([]);
        /** @var VolumesTable|MockObject $Volumes */
        $Volumes = $this->getMockBuilder(VolumesTable::class)
            ->onlyMethods(['save'])
            ->getMock();
        $Volumes->setTable('volumes');
        $Volumes->method('save')->willReturn(false);
        TableRegistry::getTableLocator()->set('Volumes', $Volumes);
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [2],
                'remove' => [1],
                'uid' => 'test',
            ]
        );
        $this->assertExitError();
        $this->assertOutputContains(
            __(
                "Les fichiers ont bien été copiés dans {0} mais l'attribution du volume a échoué",
                'volume02'
            )
        );
    }

    /**
     * testremoveVolumeException
     */
    public function testremoveVolumeException()
    {
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);

        $loc = TableRegistry::getTableLocator();
        $Volumes = $loc->get('Volumes');
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $StoredFilesVolumes->deleteAll([]);

        $StoredFiles = $loc->get('StoredFiles');
        $StoredFiles->deleteAll([]);

        $manager = new VolumeManager(1);
        $file1 = hash('sha256', uniqid('test', true));
        $manager->filePutContent('foo/bar/baz.txt', $file1);
        Configure::write(
            VolumeManager::DRIVERS_CONFIG_PATH . '.FILESYSTEM.class',
            MockVolume::class
        );
        MockVolume::$pings = [false];
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [],
                'remove' => [1],
                'uid' => 'test',
            ]
        );
        $this->assertExitError();
        $this->assertOutputContains(
            __(
                "Le volume {0} a été retiré sans supprimer"
                . " ses fichiers car il n'étais pas accessible",
                'volume01'
            )
        );

        // Test echec du retrait forcé du volume de l'ecs
        $Volumes->updateAll(['secure_data_space_id' => 1], ['id' => 1]);
        /** @var VolumesTable|MockObject $Volumes */
        $Volumes = $this->getMockBuilder(VolumesTable::class)
            ->onlyMethods(['save'])
            ->getMock();
        $Volumes->setTable('volumes');
        $Volumes->method('save')->willReturn(false);
        TableRegistry::getTableLocator()->set('Volumes', $Volumes);

        MockVolume::$pings = [false];
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [],
                'remove' => [1],
                'uid' => 'test',
            ]
        );
        $this->assertExitError();
        $this->assertErrorContains(
            __(
                "Le retrait du volume {0} a échoué",
                'volume01'
            )
        );
        $this->_out = $this->_err = null;

        // test exception != volume_not_accessible
        $Volumes->updateAll(['secure_data_space_id' => 1], ['id' => 1]);
        MockVolume::$constructExceptions = [new VolumeException(VolumeException::FULL_DISK)];
        $e = null;
        try {
            $this->runCron(
                ChangeVolumesInSpace::class,
                [
                    'space_id' => 1,
                    'add' => [],
                    'remove' => [1],
                    'uid' => 'test',
                ]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);
    }

    /**
     * testremoveVolumeTestFailed
     */
    public function testremoveVolumeTestFailed()
    {
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);
        Configure::write(
            VolumeManager::DRIVERS_CONFIG_PATH . '.FILESYSTEM.class',
            MockVolume::class
        );
        MockVolume::$tests = [['success' => false]];
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [],
                'remove' => [1],
                'uid' => 'test',
            ]
        );
        $this->assertExitError();
    }

    /**
     * testdeleteException
     */
    public function testdeleteException()
    {
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);
        Configure::write(
            VolumeManager::DRIVERS_CONFIG_PATH . '.FILESYSTEM.class',
            MockVolume::class
        );
        MockVolume::$fileDeleteExceptions = [false, false, false, false, false, true];
        MockVolume::$fileExists = true;
        MockVolume::$tests = [['success' => true]];
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [],
                'remove' => [1],
                'uid' => 'test',
            ]
        );
        $this->assertExitError();
        $this->assertOutputContains(
            __(
                "Echec lors de la suppression de {0} sur {1}",
                'sa/no_profile/2021-12/sa_1/original_data/sample.wmv',
                'volume01'
            )
        );
    }

    /**
     * testremoveVolumeSaveError
     */
    public function testremoveVolumeSaveError()
    {
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);

        $loc = TableRegistry::getTableLocator();
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $StoredFilesVolumes->deleteAll([]);
        $StoredFiles = $loc->get('StoredFiles');
        $StoredFiles->deleteAll([]);

        /** @var VolumesTable|MockObject $Volumes */
        $Volumes = $this->getMockBuilder(VolumesTable::class)
            ->onlyMethods(['save'])
            ->getMock();
        $Volumes->setTable('volumes');
        $Volumes->method('save')->willReturn(false);
        TableRegistry::getTableLocator()->set('Volumes', $Volumes);
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [],
                'remove' => [1],
                'uid' => 'test',
            ]
        );
        $this->assertExitError();
        $this->assertOutputContains(
            __(
                "Les fichiers ont bien été supprimés dans {0} mais le retrait du volume a échoué",
                'volume01'
            )
        );
    }

    /**
     * testremoveVolumeFileUnexistatError
     */
    public function testremoveVolumeFileUnexistatError()
    {
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);
        $file = TableRegistry::getTableLocator()->get('StoredFiles')->get(1);

        /** @var VolumesTable|MockObject $Volumes */
        $Volumes = $this->getMockBuilder(VolumesTable::class)
            ->onlyMethods(['save'])
            ->getMock();
        $Volumes->setTable('volumes');
        $Volumes->method('save')->willReturn(false);
        TableRegistry::getTableLocator()->set('Volumes', $Volumes);
        $this->runCron(
            ChangeVolumesInSpace::class,
            [
                'space_id' => 1,
                'add' => [],
                'remove' => [1],
                'uid' => 'test',
            ]
        );
        $this->assertExitError();
        $this->assertOutputContains(
            __(
                "Le fichier ''{0}'' n'existe pas sur les autres volumes",
                $file->get('name')
            )
        );
    }
}
