<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\OutgoingTransferRequestArchiveDestruction;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\OutgoingTransferRequestsTable;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use DateInterval;
use DateTime;

use const TMP_TESTDIR;

/**
 * Asalae\Cron\OutgoingTransferRequestArchiveDestruction Test Case
 */
class OutgoingTransferRequestArchiveDestructionTest extends TestCase
{
    use IntegrationCronTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.AccessRuleCodes',
        'app.AccessRules',
        'app.AppraisalRuleCodes',
        'app.AppraisalRules',
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveBinariesTechnicalArchiveUnits',
        'app.ArchiveBinaryConversions',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveIndicators',
        'app.ArchiveKeywords',
        'app.ArchiveUnits',
        'app.ArchiveUnitsBatchTreatments',
        'app.ArchiveUnitsDeliveryRequests',
        'app.ArchiveUnitsDestructionRequests',
        'app.ArchiveUnitsOutgoingTransferRequests',
        'app.ArchiveUnitsRestitutionRequests',
        'app.Archives',
        'app.Configurations',
        'app.CronExecutions',
        'app.Crons',
        'app.EventLogs',
        'app.OrgEntities',
        'app.OutgoingTransferRequests',
        'app.OutgoingTransfers',
        'app.RestitutionRequests',
        'app.TechnicalArchiveUnits',
        'app.TypeEntities',
        'app.ValidationChains',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();

        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('App.paths.data', TMP_TESTDIR);
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        $Exec->method('command')->willReturn(
            new CommandResult(
                [
                    'success' => true,
                    'code' => 0,
                    'stdout' => '',
                    'stderr' => '',
                ]
            )
        );
        Utility::set('Exec', $Exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Utility::reset();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        // initialisation
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $Archives->updateAll(['state' => ArchivesTable::S_TRANSFERRING], ['id' => 1]);
        $EventLogs = $loc->get('EventLogs');
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $ArchiveDescriptions = $loc->get('ArchiveDescriptions');
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $archiveKeyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => 1,
                'content' => 'test',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $ArchiveKeywords->saveOrFail($archiveKeyword);
        $ArchiveUnitsOutgoingTransferRequests = $loc->get('ArchiveUnitsOutgoingTransferRequests');
        $ArchiveUnitsOutgoingTransferRequests->updateAll(['archive_unit_id' => 1], ['id' => 1]);
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateAll(['state' => ArchiveUnitsTable::S_TRANSFERRING], []);
        $OutgoingTransfers = $loc->get('OutgoingTransfers');
        $OutgoingTransfers->updateAll(
            ['state' => 'accepted', 'archive_unit_id' => 1],
            []
        );
        $OutgoingTransferRequests = $loc->get('OutgoingTransferRequests');
        $OutgoingTransferRequests->updateAll(
            ['state' => OutgoingTransferRequestsTable::S_ARCHIVE_DESTRUCTION_SCHEDULED],
            ['id' => 1]
        );
        $ArchiveUnitsOutgoingTransferRequests = $loc->get('ArchiveUnitsOutgoingTransferRequests');

        // counts
        $eventCount = $EventLogs->find()->count();
        $abCount = $ArchiveBinaries->find()->count();
        $adCount = $ArchiveDescriptions->find()->count();
        $akCount = $ArchiveKeywords->find()->count();

        // test
        $this->runCron(OutgoingTransferRequestArchiveDestruction::class);
        $this->assertExitCode(0);
        $this->assertCount($eventCount + 1, $EventLogs->find());
        $this->assertLessThan($abCount, $ArchiveBinaries->find()->count());
        $this->assertCount(0, $ArchiveUnits->find()->where(['archive_id' => 1]));
        $this->assertLessThan($adCount, $ArchiveDescriptions->find()->count());
        $this->assertLessThan($akCount, $ArchiveKeywords->find()->count());

        // test élimination partielle
        $Archives->updateAll(['state' => 'transferring'], ['id' => 5]);
        $archiveUnit = $ArchiveUnits->newEntity(
            [
                'state' => 'transferring',
                'archive_id' => 5,
                'access_rule_id' => 1,
                'appraisal_rule_id' => 1,
                'archival_agency_identifier' => 'test_au',
                'original_local_size' => 0,
                'original_total_size' => 0,
                'xml_node_tagname' => 'Archive',
                'original_total_count' => 0,
                'original_local_count' => 0,
                'expired_dua_root' => false,
            ]
        );
        $ArchiveUnits->saveOrFail($archiveUnit);
        $OutgoingTransfers->updateAll(['state' => 'rejected'], []);
        $auOtr = $ArchiveUnitsOutgoingTransferRequests->newEntity(
            [
                'archive_unit_id' => $archiveUnit->id,
                'otr_id' => 1,
            ]
        );
        $ArchiveUnitsOutgoingTransferRequests->saveOrFail($auOtr);
        $OutgoingTransferRequests->updateAll(
            [
                'state' => 'transfers_processed',
                'last_state_update' => (new DateTime())->sub(new DateInterval('P100Y')),
            ],
            ['id' => 1]
        );
        $this->runCron(OutgoingTransferRequestArchiveDestruction::class);
        $this->assertExitCode(0);
    }
}
