<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Controller\AdminsController;
use Asalae\Cron\Check;
use Asalae\Test\Mock\FakeEmitter;
use Asalae\Test\Mock\GenericCaller;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Beanstalk\Utility\Beanstalk;
use Cake\Command\Command;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use DateTime;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;

use const TMP_VOL1;
use const TMP_VOL2;

/**
 * Asalae\Cron\Check Test Case
 */
class CheckTest extends TestCase
{
    use IntegrationCronTrait;
    use InvokePrivateTrait;

    public array $fixtures = [
        'app.BeanstalkWorkers',
        'app.CronExecutions',
        'app.Crons',
        'app.EventLogs',
        'app.OrgEntities',
        'app.OrgEntitiesTimestampers',
        'app.Phinxlog',
        'app.ServiceLevels',
        'app.Timestampers',
        'app.Volumes',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        if (is_dir($dir = TMP_TESTDIR . DS . 'check-test')) {
            Filesystem::remove($dir);
        }
        Filesystem::mkdir($dir);

        file_put_contents(
            $localpath = $dir . DS . 'path_to_local.php',
            sprintf('<?php return "%s";?>', addcslashes($localjson = $dir . DS . 'local.json', '"'))
        );
        file_put_contents(
            $localjson,
            json_encode(
                [
                    'Datasources' => [
                        'default' => ConnectionManager::get('default')->config(),
                    ],
                ]
            )
        );
        Configure::write('App.paths.path_to_local_config', $localpath);

        $Phinxlog = TableRegistry::getTableLocator()->get('Phinxlog');
        $Phinxlog->deleteAll([]);
        $Phinxlogs = TableRegistry::getTableLocator()->get('Phinxlog');
        $migrations = glob(CONFIG . 'Migrations/*.php');
        foreach ($migrations as $path) {
            $filename = basename($path, '.php');
            [$version, $migration_name] = explode('_', $filename, 2);
            $phinx = $Phinxlogs->newEntity(
                [
                    'version' => $version,
                    'migration_name' => $migration_name,
                    'start_time' => $time = new DateTime(),
                    'end_time' => $time,
                    'breakpoint' => false,
                ]
            );
            $Phinxlogs->saveOrFail($phinx);
        }

        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }

        /** @var MockObject|Beanstalk $instance */
        $instance = $this->getMockBuilder(Beanstalk::class)
            ->onlyMethods(['socketEmit', 'isConnected'])
            ->getMock();
        $instance->method('isConnected')->willReturn(true);
        Beanstalk::$instance = $instance;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir($dir = TMP_TESTDIR . DS . 'check-test')) {
            Filesystem::remove($dir);
        }
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }
        Utility::reset();
        Filesystem::setNamespace();
        Filesystem::reset();
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        Filesystem::mkdir(TMP_VOL1);
        Filesystem::mkdir(TMP_VOL2);

        $check = $this->createMock(FakeEmitter::class);
        $check->method('__call')->willReturnCallback(
            function ($name) {
                switch ($name) {
                    case 'volumes':
                        return ['foo' => ['success' => true]];
                    case 'extPhp':
                    case 'missings':
                    case 'php':
                        return ['test' => true];
                    default:
                        return true;
                }
            }
        );

        Utility::set('Check', $check);
        $exec = $this->createMock(Exec::class);
        $exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => Command::CODE_SUCCESS, 'stdout' => '', 'stderr' => ''])
        );
        Utility::set('Exec', $exec);
        TableRegistry::getTableLocator()->get('OrgEntitiesTimestampers')->deleteAll([]);
        $Timestampers = TableRegistry::getTableLocator()->get('Timestampers');
        $timestamper = $Timestampers->newEntity(
            [
                'name' => 'test',
                'fields' => '{"driver":"test","name":"test","myfield":"test"}',
            ]
        );
        $Timestampers->saveOrFail($timestamper);
        Configure::write(
            'Timestamping.drivers.test',
            [
                'name' => 'test',
                'class' => GenericCaller::class,
                'fields' => ['myfield' => ['label' => 'my field']],
            ]
        );
        GenericCaller::$methods['ping'] = true;

        $Crons = TableRegistry::getTableLocator()->get('Crons');
        $cron = $Crons->newEntity(
            [
                'name' => 'Vérification du fonctionnement de asalae',
                'description' => 'test',
                'classname' => Check::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'PT1S',
                'next' => new DateTime('yesterday'),
                'app_meta' => null,
                'parallelisable' => true,
            ]
        );
        $Crons->saveOrFail($cron);

        $this->runCron(Check::class);
        $this->assertOutputContains('success');
    }

    /**
     * testGetDriverArgs
     */
    public function testGetDriverArgs()
    {
        Configure::write('Some.random.config', 'foo');
        $fields = [
            'one' => [
                'read_config' => 'Some.random.config',
            ],
            'two' => [
                'default' => 'will_not_be_used',
            ],
            'three' => [
                'default' => 'baz',
            ],
            'four' => [],
        ];
        $data = (object)[
            'two' => 'bar',
        ];
        $expected = [
            'one' => 'foo',
            'two' => 'bar',
            'three' => 'baz',
            'four' => '',
        ];
        $check = new Check();
        $args = $this->invokeMethod($check, 'getDriverArgs', [$fields, $data]);
        $this->assertEquals($expected, $args);

        $decryptKey = hash('sha256', AdminsController::DECRYPT_KEY);
        $fields = ['five' => ['type' => 'password']];
        $data = (object)['five' => base64_encode(Security::encrypt('test', $decryptKey))];
        $args = $this->invokeMethod($check, 'getDriverArgs', [$fields, $data]);
        $this->assertEquals('test', $args['five']);
    }
}
