<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\EventsLogsExport;
use Asalae\Model\Table\EventLogsTable;
use Asalae\Model\Table\TechnicalArchivesTable;
use Asalae\Test\Mock\GenericCaller;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Premis;
use AsalaeCore\Utility\ValidatorXml;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use DateInterval;
use DateTime;
use Libriciel\Filesystem\Utility\Filesystem;

use function __;

use const PREMIS_V3;
use const TMP_VOL1;
use const TMP_VOL2;

class EventsLogsExportTest extends TestCase
{
    use IntegrationCronTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        FIXTURES_EVENT_LOGS,
        'app.TechnicalArchives',
        'app.TechnicalArchiveUnits',
        'app.ArchiveBinariesTechnicalArchiveUnits',
        'app.Timestampers',
        'app.TypeEntities',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }
        Utility::reset();
        Filesystem::setNamespace();
        Filesystem::reset();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        Filesystem::mkdir($vol1 = TMP_VOL1);
        Filesystem::mkdir($vol2 = TMP_VOL2);

        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $Archives = $loc->get('Archives');
        $Versions = $loc->get('Versions');
        $archive = $Archives->get(1); // sujet d'evenement valide
        $TechnicalArchives = $loc->get('TechnicalArchives');
        $TechnicalArchiveUnits = $loc->get('TechnicalArchiveUnits');
        $StoredFiles = $loc->get('StoredFiles');
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $Timestampers = TableRegistry::getTableLocator()->get('Timestampers');
        $timestamper = $Timestampers->newEntity(
            [
                'name' => 'test',
                'fields' => '{"driver":"test","name":"test","myfield":"test"}',
            ]
        );
        $Timestampers->saveOrFail($timestamper);
        Configure::write(
            'Timestamping.drivers.test',
            [
                'name' => 'test',
                'class' => GenericCaller::class,
                'fields' => ['myfield' => ['label' => 'my field']],
            ]
        );
        GenericCaller::$methods['ping'] = true;
        $OrgEntities = $loc->get('OrgEntities');
        $OrgEntities->updateAll(['timestamper_id' => $timestamper->id], ['id' => 2]);

        // préparation du 1er export d'evenements
        $EventLogs->deleteAll([]);
        $event = $EventLogs->newEntry(
            'test',
            'success',
            'test',
            1,
            $archive
        );
        $event->set('created', new DateTime('2000-01-01'));
        $this->assertNotFalse($EventLogs->save($event));
        $event2 = $EventLogs->newEntry(
            'test',
            'success',
            'test 2',
            1,
            $archive
        );
        $this->assertNotFalse($EventLogs->save($event2));
        $event3 = $EventLogs->newEntry(
            'login',
            'success',
            'operating',
            new Premis\Agent('admin', 'test'),
            $Versions->get(1)
        );
        $this->assertNotFalse($EventLogs->save($event3));
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);

        $this->runCron(EventsLogsExport::class);
        $this->assertOutputContains(__("Terminé avec succès"));
        $ta = $TechnicalArchives->find()->orderBy(['id'])->first();
        $this->assertNotNull($ta);
        $files1 = glob($vol1 . DS . $ta->get('storage_path') . DS . '*');
        $files2 = glob($vol2 . DS . $ta->get('storage_path') . DS . '*');
        $this->assertCount(10, $files1); // xml + tsr
        $this->assertTrue((new ValidatorXml(PREMIS_V3))->validate($files1[0]));
        $sha256 = hash_file('sha256', $files1[0]);

        $query = $EventLogs->find()->where(['exported' => true]);
        $this->assertCount(1, $query);
        $this->assertCount(
            2,
            $TechnicalArchiveUnits->find()->where(['technical_archive_id' => 1])
        ); // archive + mois en cours

        // on décale le jour pour tester la blockchain
        $target = (new DateTime())->format('Ymd');
        $replace = (new DateTime())->sub(new DateInterval('P1D'))->format('Ymd');
        rename($files1[0], str_replace($target, $replace, $files1[0]));
        rename($files1[1], str_replace($target, $replace, $files1[1]));
        rename($files2[0], str_replace($target, $replace, $files2[0]));
        rename($files2[1], str_replace($target, $replace, $files2[1]));
        foreach ($StoredFiles->find() as $stored) {
            $stored->set('name', str_replace($target, $replace, $stored->get('name')));
            $StoredFiles->saveOrFail($stored);
        }
        foreach ($StoredFilesVolumes->find() as $storedVolume) {
            $storedVolume->set('storage_ref', str_replace($target, $replace, $storedVolume->get('storage_ref')));
            $StoredFilesVolumes->saveOrFail($storedVolume);
        }
        foreach ($ArchiveBinaries->find() as $binary) {
            $binary->set('storage_ref', str_replace($target, $replace, $binary->get('storage_ref') ?: ''));
            $ArchiveBinaries->saveOrFail($binary);
        }

        // 2e export
        $event2->set('created', new DateTime('2000-01-02'));
        $event3->set('created', new DateTime('2000-01-02'));
        $this->assertNotFalse($EventLogs->save($event2));
        $this->assertNotFalse($EventLogs->save($event3));
        $this->runCron(EventsLogsExport::class);
        $this->assertOutputContains(__("Terminé avec succès"));
        $files = glob($vol1 . DS . $ta->get('storage_path') . DS . '*');
        $this->assertCount(10, $files); // xml + tsr
        $this->assertTrue((new ValidatorXml(PREMIS_V3))->validate($files[2]));
        $util = DOMUtility::load($files[2]);
        $this->assertEquals(
            $sha256,
            $util->xpath->query('//ns:object/ns:significantProperties/ns:significantPropertiesValue')->item(
                3
            )->nodeValue
        );

        // pas d'events à exporter
        $this->runCron(EventsLogsExport::class);
        $this->assertOutputContains(__("Aucun événements à exporter"));

        $this->assertTrue( // $event2
            $TechnicalArchives->exists(['type' => TechnicalArchivesTable::TYPE_ARCHIVAL_AGENCY_EVENTS_LOGS])
        );
        $this->assertTrue( // $event3
            $TechnicalArchives->exists(['type' => TechnicalArchivesTable::TYPE_OPERATING_EVENTS_LOGS])
        );
    }
}
