<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\ArchivesIntegrityControl;
use Asalae\Model\Entity\Archive;
use Asalae\Model\Entity\LifecycleXmlArchiveFile;
use Asalae\Test\Mock\MockVolume;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Console\ConsoleIo;
use Cake\Console\TestSuite\StubConsoleInput as ConsoleInput;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Libriciel\Filesystem\Utility\Filesystem;

use function __;

use const TMP_TESTDIR;
use const TMP_VOL1;
use const TMP_VOL2;

/**
 * Asalae\Cron\ArchivesIntegrityControl Test Case
 */
class ArchivesIntegrityControlTest extends TestCase
{
    use IntegrationCronTrait;
    use AutoFixturesTrait;

    public const array ATTACHMENTS = [
        ASALAE_CORE_TEST_DATA . DS . 'sample.odt',
        ASALAE_CORE_TEST_DATA . DS . 'sample.pdf',
        ASALAE_CORE_TEST_DATA . DS . 'sample.rng',
    ];
    public const string LIFECYCLE_PREMIS = ASALAE_CORE_TEST_DATA . DS . 'sample_premis.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        FIXTURES_EVENT_LOGS,
        'app.ArchiveBinariesArchiveUnits',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Utility::reset();
        Filesystem::setNamespace();
        Filesystem::reset();
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Configure::write('Archives.updateLifecycle.path', TMP_TESTDIR);
        Configure::write('Archives.updateDescription.path', TMP_TESTDIR);
        Configure::write('Archives.updateLifecycle.cache_duration', 0);
        Configure::write('Archives.updateDescription.cache_duration', 0);
        $Exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(['async', 'command'])
            ->getMock();
        $Exec->method('command')->willReturnCallback(
            function ($command, ...$args) {
                $h = fopen('/tmp/test.log', 'a');
                fwrite($h, $command . var_export($args, true) . "\n");
                fclose($h);
                if ($command === CAKE_SHELL) {
                    $escaped = $this->extractArgsShellOptions($args);
                    $command = implode(' ', $escaped);
                    $out = new ConsoleOutput();
                    $err = new ConsoleOutput();
                    $io = new ConsoleIo($out, $err, new ConsoleInput([]));
                    $runner = $this->makeRunner();
                    $args = $this->commandStringToArgs("cake $command");
                    $code = $runner->run($args, $io);
                    if (is_file(TMP_TESTDIR . '/update_lifecycle/.lock')) {
                        unlink(TMP_TESTDIR . '/update_lifecycle/.lock');
                    }
                    return new CommandResult(
                        [
                            'success' => $code === 0,
                            'code' => $code,
                            'stdout' => implode(PHP_EOL, $out->messages()),
                            'stderr' => implode(PHP_EOL, $err->messages()),
                        ]
                    );
                } else {
                    $Exec = new Exec();
                    return $Exec->command($command, ...$args);
                }
            }
        );
        Utility::set('Exec', $Exec);

        Configure::write(
            'Archives.updateLifecycle.path',
            TMP_TESTDIR . DS . 'update_lifecycle'
        );
    }

    /**
     * extractArgsShellOptions
     * @param array $args
     * @return array
     */
    private function extractArgsShellOptions(array $args): array
    {
        $escaped = [];
        foreach ($args as $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $escaped[] = $k;
                    if ($v) {
                        $escaped[] = $v;
                    }
                }
            } else {
                $escaped[] = $value;
            }
        }
        return $escaped;
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_VOL1)) {
            Filesystem::remove(TMP_VOL1);
        }
        if (is_dir(TMP_VOL2)) {
            Filesystem::remove(TMP_VOL2);
        }
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Utility::reset();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        $loc = TableRegistry::getTableLocator();
        $EventLogs = $loc->get('EventLogs');
        $EventLogs->deleteAll([]);
        $Archives = $loc->get('Archives');
        $TechnicalArchives = $loc->get('TechnicalArchives');
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $StoredFilesVolumes = $loc->get('StoredFilesVolumes');

        $Archives->updateAll(['is_integrity_ok' => null], []);
        $TechnicalArchives->updateAll(['is_integrity_ok' => null], []);
        $ArchiveBinaries->updateAll(['is_integrity_ok' => null], []);
        $StoredFilesVolumes->updateAll(['is_integrity_ok' => null], []);

        // volumes hs
        $this->runCron(ArchivesIntegrityControl::class);
        $output = implode(PHP_EOL, $this->_out->messages())
            . implode(PHP_EOL, $this->_err->messages());
        $this->assertOutputNotContains('success', $output);
        $this->assertErrorContains('volume 1', $output);
        $this->assertErrorContains('volume 2', $output);

        // volumes vide
        Filesystem::mkdir(TMP_VOL1);
        $this->runCron(ArchivesIntegrityControl::class);
        $output = implode(PHP_EOL, $this->_out->messages())
            . implode(PHP_EOL, $this->_err->messages());
        $this->assertOutputNotContains('success', $output);
        $this->assertErrorNotContains('volume 1', $output);
        $this->assertErrorContains('volume 2', $output);

        // volumes ok
        Filesystem::mkdir(TMP_VOL2);
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        $initialCount = $EventLogs->find()->count();
        $this->runCron(ArchivesIntegrityControl::class);
        $output = implode(PHP_EOL, $this->_out->messages())
            . implode(PHP_EOL, $this->_err->messages());
        $this->assertOutputNotContains('error', $output);
        $this->assertOutputNotContains('warning', $output);
        $this->assertGreaterThan($initialCount, $EventLogs->find()->count());
        $event = $EventLogs->find()
            ->where(['type !=' => 'check_technical_archive_integrity'])
            ->orderBy(['id' => 'desc'])
            ->first();
        $this->assertNotFalse((bool)$event);
        $this->assertEquals('check_archive_integrity', $event->get('type'));
        /** @var Archive $archive */
        $archive = $Archives->find()
            ->where(['Archives.id' => $event->get('object_foreign_key')])
            ->contain(['LifecycleXmlArchiveFiles' => ['StoredFiles']])
            ->first();
        $this->assertNotFalse((bool)$archive);
        /** @var LifecycleXmlArchiveFile $filecycle */
        $filecycle = $archive->get('lifecycle_xml_archive_file');
        $util = new DOMUtility($filecycle->getDom());
        $this->assertEquals(
            'EventLogs:' . $event->id,
            $util->nodeValue("ns:event[last()]/ns:eventIdentifier/ns:eventIdentifierValue")
        );

        // aucunes actions
        $this->runCron(ArchivesIntegrityControl::class);
        $output = implode(PHP_EOL, $this->_out->messages())
            . implode(PHP_EOL, $this->_err->messages());
        $this->assertOutputContains(__("Aucune vérification d'intégrité n'est nécessaire"), $output);
        $this->assertEquals('success', $this->workOutput);

        // mauvais hash
        $Archives->updateAll(['integrity_date' => null], []);
        $testfile = TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/original_data/sample.wmv';
        $this->assertFileExists($testfile);
        $testfileSize = filesize($testfile);
        rename($testfile, $testfile . '.bak');
        Filesystem::createDummyFile($testfile, $testfileSize);
        $this->assertEquals($testfileSize, filesize($testfile));
        $StoredFilesVolumes->updateAll(['is_integrity_ok' => null], []);
        $this->runCron(ArchivesIntegrityControl::class);
        $this->assertErrorContains('sample.wmv');

        // mauvaise size
        file_put_contents($testfile, 'test');
        $this->runCron(ArchivesIntegrityControl::class);
        $this->assertErrorContains('sample.wmv');

        // temp dépassé
        $Archives->updateAll(['integrity_date' => null], []);
        $this->runCron(ArchivesIntegrityControl::class, ['max_execution_time' => -3601, 'update_time' => -61]);
        $output = implode(PHP_EOL, $this->_out->messages())
            . implode(PHP_EOL, $this->_err->messages());
        $this->assertOutputContains(__("Limite de durée d'exécution atteinte."), $output);

        // "coupure" du volume en cours de route
        $Volumes = $loc->get('Volumes');
        $Volumes->updateAll(['driver' => 'test'], ['id' => 1]);
        Configure::write(
            'Volumes.drivers.test',
            [
                'name' => 'test',
                'class' => MockVolume::class,
                'fields' => [
                    'path' => [
                        'label' => 'Dossier',
                        'placeholder' => '/data/Volume01',
                        'required' => 'required',
                    ],
                ],
            ]
        );
        MockVolume::$absolutePathExceptions = [true, false];
        $this->runCron(ArchivesIntegrityControl::class);
        $this->assertErrorContains('volume 1');
    }
}
