<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\RestitutionRequestArchiveDestruction;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use DateInterval;
use DateTime;

use const TMP_TESTDIR;

/**
 * Asalae\Cron\RestitutionRequestArchiveDestruction Test Case
 */
class RestitutionRequestArchiveDestructionTest extends TestCase
{
    use IntegrationCronTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.AccessRuleCodes',
        'app.AccessRules',
        'app.AppraisalRuleCodes',
        'app.AppraisalRules',
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveBinariesTechnicalArchiveUnits',
        'app.ArchiveBinaryConversions',
        'app.ArchiveDescriptions',
        'app.ArchiveFiles',
        'app.ArchiveIndicators',
        'app.ArchiveKeywords',
        'app.ArchiveUnits',
        'app.ArchiveUnitsBatchTreatments',
        'app.ArchiveUnitsDeliveryRequests',
        'app.ArchiveUnitsDestructionRequests',
        'app.ArchiveUnitsOutgoingTransferRequests',
        'app.ArchiveUnitsRestitutionRequests',
        'app.Archives',
        'app.Configurations',
        'app.CronExecutions',
        'app.Crons',
        'app.EventLogs',
        'app.OrgEntities',
        'app.OutgoingTransfers',
        'app.RestitutionRequests',
        'app.TechnicalArchiveUnits',
        'app.TechnicalArchives',
        'app.TypeEntities',
        'app.Users',
        'app.ValidationChains',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();

        VolumeSample::init();
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01/* ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02/* ' . TMP_VOL2);
        Configure::write('App.paths.data', TMP_TESTDIR);
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Utility::reset();
    }

    /**
     * testWork
     */
    public function testWork()
    {
        // initialisation
        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $Archives->updateAll(['state' => 'restituting'], ['id' => 1]);
        $ArchiveBinaries = $loc->get('ArchiveBinaries');
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveDescriptions = $loc->get('ArchiveDescriptions');
        $EventLogs = $loc->get('EventLogs');
        $ArchiveUnits->updateAll(['state' => 'restituting'], []);
        $RestitutionRequests = $loc->get('RestitutionRequests');
        $ArchiveUnitsRestitutionRequests = $loc->get('ArchiveUnitsRestitutionRequests');
        $ArchiveUnitsRestitutionRequests->updateAll(['archive_unit_id' => 1], ['id' => 1]);
        $RestitutionRequests->updateAll(
            [
                'state' => 'restitution_acquitted',
                'last_state_update' => (new DateTime())->sub(new DateInterval('P100Y')),
            ],
            ['id' => 1]
        );
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $archiveKeyword = $ArchiveKeywords->newEntity(
            [
                'archive_unit_id' => 1,
                'access_rule_id' => 1,
                'content' => 'test',
                'xml_node_tagname' => 'Keyword[1]',
            ]
        );
        $ArchiveKeywords->saveOrFail($archiveKeyword);

        // counts
        $eventCount = $EventLogs->find()->count();
        $abCount = $ArchiveBinaries->find()->count();
        $auCount = $ArchiveUnits->find()->count();
        $adCount = $ArchiveDescriptions->find()->count();
        $akCount = $ArchiveKeywords->find()->count();

        // test
        $this->runCron(RestitutionRequestArchiveDestruction::class);
        $this->assertExitCode(0);
        $this->assertCount($eventCount + 1, $EventLogs->find());
        $this->assertLessThan($abCount, $ArchiveBinaries->find()->count());
        $this->assertLessThan($auCount, $ArchiveUnits->find()->count());
        $this->assertLessThan($adCount, $ArchiveDescriptions->find()->count());
        $this->assertLessThan($akCount, $ArchiveKeywords->find()->count());
    }
}
