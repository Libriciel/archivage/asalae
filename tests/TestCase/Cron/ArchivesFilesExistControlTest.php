<?php

namespace Asalae\Test\TestCase\Cron;

use Asalae\Cron\ArchivesFilesExistControl;
use Asalae\Model\Table\ArchivesTable;
use Asalae\TestSuite\VolumeSample;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use Cake\Console\TestSuite\StubConsoleOutput as ConsoleOutput;
use Cake\ORM\TableRegistry;
use DateTime;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

use function __;

use const TMP_VOL1;
use const TMP_VOL2;

/**
 * Asalae\Cron\ArchivesFilesExistControl Test Case
 */
class ArchivesFilesExistControlTest extends TestCase
{
    use IntegrationCronTrait;
    use InvokePrivateTrait;
    use AutoFixturesTrait;

    public const string LIFECYCLE_PREMIS = ASALAE_CORE_TEST_DATA . DS . 'sample_premis.xml';
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.ArchiveBinaries',
        'app.ArchiveBinariesArchiveUnits',
        'app.ArchiveBinariesTechnicalArchiveUnits',
        'app.Archives',
        'app.ArchiveUnits',
        'app.ArchiveFiles',
        'app.CronExecutions',
        'app.Crons',
        'app.EventLogs',
        'app.OrgEntities',
        'app.TechnicalArchives',
        'app.TechnicalArchiveUnits',
    ];
    private $debugFixtures = false;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Utility::reset();
    }

    /**
     * testGetVirtualFields
     */
    public function testGetVirtualFields()
    {
        $expected = ['max_execution_time', 'min_delay'];
        $this->assertEquals($expected, array_keys(ArchivesFilesExistControl::getVirtualFields()));
    }

    /**
     * testWork
     */
    public function testWork()
    {
        /** @var ArchivesTable $Archives */
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $TechnicalArchives = TableRegistry::getTableLocator()->get('TechnicalArchives');
        $ArchiveBinaries = TableRegistry::getTableLocator()->get('ArchiveBinaries');
        $StoredFilesVolumes = TableRegistry::getTableLocator()->get('StoredFilesVolumes');

        $Archives->updateAll(['is_integrity_ok' => true, 'filesexist_date' => null], []);
        $ArchiveBinaries->updateAll(['is_integrity_ok' => true], []);
        $StoredFilesVolumes->updateAll(['is_integrity_ok' => true], []);

        Filesystem::remove(TMP_VOL1);
        Filesystem::remove(TMP_VOL2);

        // Volume HS
        $this->runCron(ArchivesFilesExistControl::class);
        $this->assertOutputContains(
            __("Archive n°{0}", 1) . ' <warning>Skipped</warning>'
        );

        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data' . DS;
        exec('cp -r ' . $basedir . 'volume01 ' . TMP_VOL1);
        exec('cp -r ' . $basedir . 'volume02 ' . TMP_VOL2);

        // Fichier manquant
        rename(
            TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/original_data/sample.wmv',
            TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/original_data/sample.wmv.bak'
        );
        $this->runCron(ArchivesFilesExistControl::class);
        $this->assertOutputContains(
            __("Archive n°{0}", 1) . ' <error>Fail</error>'
        );
        rename(
            TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/original_data/sample.wmv.bak',
            TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/original_data/sample.wmv'
        );

        // Durée max dépassée
        $Archives->updateAll(['is_integrity_ok' => true, 'filesexist_date' => null], []);
        $ArchiveBinaries->updateAll(['is_integrity_ok' => true], []);
        $StoredFilesVolumes->updateAll(['is_integrity_ok' => true], []);
        $this->runCron(ArchivesFilesExistControl::class, ['max_execution_time' => -3600]);
        $this->assertOutputContains(
            __("Limite de durée d'exécution atteinte.")
        );

        // Fichier ok
        $Archives->updateAll(['is_integrity_ok' => true, 'filesexist_date' => null], []);
        $TechnicalArchives->updateAll(['is_integrity_ok' => true, 'filesexist_date' => null], []);
        Filesystem::dumpFile(
            TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            'no hash check'
        );
        Filesystem::dumpFile(
            TMP_VOL2 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml',
            'no hash check'
        );
        Filesystem::copy(
            self::LIFECYCLE_PREMIS,
            TMP_VOL1 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
        );
        Filesystem::copy(
            self::LIFECYCLE_PREMIS,
            TMP_VOL2 . '/sa/no_profile/2021-12/sa_1/management_data/sa_1_lifecycle.xml'
        );

        $this->runCron(ArchivesFilesExistControl::class);
        $count = $Archives->findByIntegrity()->distinct(['Archives.id'])->count();
        $this->assertOutputContains(
            __n(
                "Archive testée avec succès: {0}",
                "Archives testées avec succès: {0}",
                $count,
                $count
            )
        );
        $count = $TechnicalArchives->find()->count();
        $this->assertOutputContains(
            __n(
                "Archive technique testée avec succès: {0}",
                "Archives techniques testées avec succès: {0}",
                $count,
                $count
            )
        );

        // fichiers déjà vérifiés
        $this->runCron(ArchivesFilesExistControl::class);
        $this->assertOutputContains(
            __("Aucune vérification de présence des fichiers n'est nécessaire")
        );
    }

    /**
     * testCheckArchive
     */
    public function testCheckArchive()
    {
        $Archives = TableRegistry::getTableLocator()->get('Archives');
        $ArchiveBinaries = TableRegistry::getTableLocator()->get('ArchiveBinaries');
        $StoredFilesVolumes = TableRegistry::getTableLocator()->get('StoredFilesVolumes');
        $Archives->updateAll(['is_integrity_ok' => true, 'filesexist_date' => null], []);
        $ArchiveBinaries->updateAll(['is_integrity_ok' => true], []);
        $StoredFilesVolumes->updateAll(['is_integrity_ok' => true], []);

        $volume = $this->createMock(VolumeFilesystem::class);
        $volume->method('fileExists')->willThrowException(new VolumeException(VolumeException::FILE_NOT_FOUND));
        $volume->method('ping')->willReturn(false);
        $ArchivesFilesExistControl = new ArchivesFilesExistControl([[], new ConsoleOutput(), new ConsoleOutput()]);
        $this->invokeProperty($ArchivesFilesExistControl, 'volumes', 'set', [1 => $volume]);
        $success = $this->invokeMethod($ArchivesFilesExistControl, 'checkArchive', [1]);
        $this->assertNull($success);

        $volume = $this->createMock(VolumeFilesystem::class);
        $volume->method('fileExists')->willThrowException(new VolumeException(Exception::class));
        $ArchivesFilesExistControl = new ArchivesFilesExistControl([[], new ConsoleOutput(), new ConsoleOutput()]);
        $this->invokeProperty($ArchivesFilesExistControl, 'volumes', 'set', [1 => $volume]);
        $this->expectException(Exception::class);
        $this->invokeMethod($ArchivesFilesExistControl, 'checkArchive', [1]);
    }

    /**
     * testCheckTechnicalArchive
     */
    public function testCheckTechnicalArchive()
    {
        $TechnicalArchives = TableRegistry::getTableLocator()->get('TechnicalArchives');
        $TechnicalArchives->updateAll(['is_integrity_ok' => true, 'filesexist_date' => null], []);
        $ArchiveBinaries = TableRegistry::getTableLocator()->get('ArchiveBinaries');
        $ArchiveBinaries->updateAll(['is_integrity_ok' => true], []);
        $StoredFilesVolumes = TableRegistry::getTableLocator()->get('StoredFilesVolumes');
        $StoredFilesVolumes->updateAll(['is_integrity_ok' => true], []);

        $volume = $this->createMock(VolumeFilesystem::class);
        $volume->method('fileExists')->willThrowException(new VolumeException(VolumeException::FILE_NOT_FOUND));
        $volume->method('ping')->willReturn(false);
        $ArchivesFilesExistControl = new ArchivesFilesExistControl([[], new ConsoleOutput(), new ConsoleOutput()]);
        $this->invokeProperty($ArchivesFilesExistControl, 'volumes', 'set', [1 => $volume]);
        $success = $this->invokeMethod($ArchivesFilesExistControl, 'checkTechnicalArchive', [1]);
        $this->assertNull($success);

        $volume = $this->createMock(VolumeFilesystem::class);
        $volume->method('fileExists')->willThrowException(new VolumeException(Exception::class));
        $ArchivesFilesExistControl = new ArchivesFilesExistControl([[], new ConsoleOutput(), new ConsoleOutput()]);
        $this->invokeProperty($ArchivesFilesExistControl, 'volumes', 'set', [1 => $volume]);
        $this->expectException(Exception::class);
        $this->invokeMethod($ArchivesFilesExistControl, 'checkTechnicalArchive', [1]);
    }

    /**
     * testTouchAndReport
     */
    public function testTouchAndReport()
    {
        $CronExecutions = TableRegistry::getTableLocator()->get('CronExecutions');
        $this->_out = new ConsoleOutput();
        $this->_err = new ConsoleOutput();
        $ArchivesFilesExistControl = new ArchivesFilesExistControl([], $this->_out, $this->_err);
        $this->invokeProperty(
            $ArchivesFilesExistControl,
            'exec',
            'set',
            $CronExecutions->newEntity(
                [
                    'cron_id' => 1,
                    'date_begin' => new DateTime(),
                    'date_end' => null,
                    'state' => 'running',
                    'report' => '',
                ]
            )
        );
        $this->invokeProperty($ArchivesFilesExistControl, 'timer', 'set', 0);
        $this->invokeProperty($ArchivesFilesExistControl, 'minDelayBeforeSave', 'set', 0);
        $this->invokeProperty($ArchivesFilesExistControl, 'total', 'set', 123);
        $this->invokeProperty($ArchivesFilesExistControl, 'total', 'set', 123);
        $this->invokeProperty($ArchivesFilesExistControl, 'CronExecutions', 'set', $CronExecutions);
        $ArchivesFilesExistControl->touchAndReport();
        $this->assertOutputContains('0 / 123');
    }
}
