<?php

namespace Asalae\Test\TestCase\Middleware;

use Asalae\Middleware\RateLimiterMiddleware;
use Asalae\Model\Table\EventLogsTable;
use Cake\Core\Configure;
use Cake\Http\Exception\HttpException;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RateLimiterMiddlewareTest extends TestCase
{
    public array $fixtures = [
        'app.EventLogs',
        'app.Users',
        'app.Versions',
    ];

    /**
     * testprocess
     */
    public function testprocess()
    {
        $loc = TableRegistry::getTableLocator();
        /** @var EventLogsTable $EventLogs */
        $EventLogs = $loc->get('EventLogs');
        $initialCount = $EventLogs->find()->count();

        $request = $this->createMock(ServerRequest::class);
        $response = $this->createMock(ResponseInterface::class);
        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method('handle')->willReturn($response);
        $rateLimiter = new RateLimiterMiddleware();

        Configure::write('Login.attempt_limit.enabled', true);
        Configure::write('Login.attempt_limit.max_amount', 100);
        Configure::write('Login.attempt_limit.ignore_first_intrusions_warnings', 3);
        RateLimiterMiddleware::$username = 'admin';
        RateLimiterMiddleware::$attempts = 0;
        RateLimiterMiddleware::$consecutiveFailedAttempts = 0;

        // on ne fait rien
        $e = null;
        try {
            $rateLimiter->process($request, $handler);
        } catch (Exception) {
        }
        $this->assertNull($e);

        // inscription des tentatives dans le journal
        RateLimiterMiddleware::$attempts = 10;
        RateLimiterMiddleware::$consecutiveFailedAttempts = 10;
        $request->method('clientIp')->willReturn('192.168.0.1');
        RateLimiterMiddleware::$username = 'not_exists';
        try {
            $rateLimiter->process($request, $handler);
        } catch (Exception $e) {
        }
        $this->assertNull($e);
        $this->assertGreaterThan($initialCount, $EventLogs->find()->count());

        // on bloque l'accès
        RateLimiterMiddleware::$attempts = 101;
        RateLimiterMiddleware::$consecutiveFailedAttempts = 100;
        RateLimiterMiddleware::$username = 'admin';
        $e = null;
        try {
            $rateLimiter->process($request, $handler);
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(HttpException::class, $e);
    }
}
