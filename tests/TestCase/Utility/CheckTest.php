<?php

namespace Asalae\Test\TestCase\Utility;

use Asalae\TestSuite\VolumeSample;
use Asalae\Utility\Check;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use DateTime;

class CheckTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAppendFixtures = [
        FIXTURES_VOLUMES,
        'app.Acos',
        'app.BeanstalkWorkers',
        'app.CronExecutions',
    ];

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        Check::clear();
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        VolumeSample::destroy();
        Check::clear();
    }

    /**
     * testVolumes
     */
    public function testVolumes()
    {
        $volumes = Check::volumes();
        $this->assertIsArray($volumes);
        $this->assertArrayHasKey('volume01', $volumes);
        $this->assertArrayHasKey('success', $volumes['volume01']);
        $this->assertTrue($volumes['volume01']['success']);
    }

    /**
     * testVolumesOk
     */
    public function testVolumesOk()
    {
        $this->assertTrue(Check::volumesOk());
    }

    /**
     * testtreeSanity
     */
    public function testtreeSanity()
    {
        $Acos = $this->fetchTable('Acos');
        $Acos->deleteAll([]);

        // table vide
        $treeSanity = Check::treeSanity();
        $this->assertTrue($treeSanity['Acos']['success'], $treeSanity['Acos']['message']);

        // table ok
        $Acos->insertQuery()
            ->insert(['lft', 'rght'])
            ->values(
                [
                    'lft' => 1,
                    'rght' => 2,
                ]
            )
            ->execute();
        $Acos->insertQuery()
            ->insert(['lft', 'rght'])
            ->values(
                [
                    'lft' => 3,
                    'rght' => 4,
                ]
            )
            ->execute();
        Check::clear();
        $treeSanity = Check::treeSanity();
        $this->assertTrue($treeSanity['Acos']['success'], $treeSanity['Acos']['message']);

        // lft / rght inversés
        $Acos->insertQuery()
            ->insert(['lft', 'rght'])
            ->values(
                [
                    'lft' => 6,
                    'rght' => 5,
                ]
            )
            ->execute();
        Check::clear();
        $treeSanity = Check::treeSanity();
        $this->assertFalse($treeSanity['Acos']['success'], $treeSanity['Acos']['message']);

        // lft / rght en doubles
        $Acos->deleteAll([]);
        $Acos->insertQuery()
            ->insert(['lft', 'rght'])
            ->values(
                [
                    'lft' => 1,
                    'rght' => 2,
                ]
            )
            ->execute();
        $Acos->insertQuery()
            ->insert(['lft', 'rght'])
            ->values(
                [
                    'lft' => 1,
                    'rght' => 2,
                ]
            )
            ->execute();
        Check::clear();
        $treeSanity = Check::treeSanity();
        $this->assertFalse($treeSanity['Acos']['success'], $treeSanity['Acos']['message']);

        // lft / rght en doubles v2
        $Acos->deleteAll([]);
        $Acos->insertQuery()
            ->insert(['lft', 'rght'])
            ->values(
                [
                    'lft' => 1,
                    'rght' => 2,
                ]
            )
            ->execute();
        $Acos->insertQuery()
            ->insert(['lft', 'rght'])
            ->values(
                [
                    'lft' => 2,
                    'rght' => 3,
                ]
            )
            ->execute();
        Check::clear();
        $treeSanity = Check::treeSanity();
        $this->assertFalse($treeSanity['Acos']['success'], $treeSanity['Acos']['message']);
    }

    /**
     * testtreeSanityOk
     */
    public function testtreeSanityOk()
    {
        $Acos = $this->fetchTable('Acos');
        $Acos->deleteAll([]);

        $this->assertTrue(Check::treeSanityOk());

        // lft / rght en doubles
        $Acos->deleteAll([]);
        $Acos->insertQuery()
            ->insert(['lft', 'rght'])
            ->values(
                [
                    'lft' => 1,
                    'rght' => 1,
                ]
            )
            ->execute();
        Check::clear();
        $this->assertFalse(Check::treeSanityOk());
    }

    /**
     * testcronOk
     */
    public function testcronOk()
    {
        $CronExecutions = $this->fetchTable('CronExecutions');
        $dateLastMonth = new DateTime('first day of last month');
        $CronExecutions->updateAll(['date_end' => $dateLastMonth], []);
        $this->assertFalse(Check::cronOk());

        $dateYesterday = new DateTime('yesterday');
        $CronExecutions->updateAll(['date_end' => $dateYesterday], []);
        $this->assertTrue(Check::cronOk());
    }
}
