<?php

namespace Asalae\Test\TestCase\Utility;

use Asalae\Utility\SedaAttachmentsIterator;
use AsalaeCore\TestSuite\TestCase;
use DOMDocument;

class SedaAttachmentsIteratorTest extends TestCase
{
    /**
     * testIterator
     */
    public function testIterator()
    {
        $basedir = ASALAE_CORE_TEST_DATA . DS . 'asalae_data';
        $file1 = $basedir . '/transfers/8/message/sample_seda02.xml'; // seda 0.2 (transfert pour integrity/hash)
        $file2 = $basedir . '/volume01/sa/no_profile/2021-12/sa_1/management_data/sa_1_description.xml'; // seda 1.0
        $file3 = $basedir . '/volume01/sp/profile1/2021-12/sa_2/management_data/sa_2_description.xml'; // seda 2.1

        // seda 0.2
        $dom = new DOMDocument();
        $dom->load($file1);
        $iterator = new SedaAttachmentsIterator($dom);
        $array = iterator_to_array($iterator);
        $this->assertNotEmpty($array);
        $expected = [
            'filename' => 'sample.odt',
            'size' => 'N/A', // pas de size en seda 0.2
            'archival_agency_identifier' => 'N/C', // vide car transfert (renseigné si archive)
            'hash' => '95072adb607708af039d1e7a4368ad3a9ede8280e367991ec1206405c81eab97',
            'hash_algo' => 'sha256',
        ];
        $this->assertEquals($expected, $array[0]);
        $this->assertCount(3, $iterator);

        // seda 1.0
        $dom = new DOMDocument();
        $dom->load($file2);
        $iterator = new SedaAttachmentsIterator($dom);
        $array = iterator_to_array($iterator);
        $this->assertNotEmpty($array);
        $expected = [
            'filename' => 'sample.wmv',
            'size' => '554297',
            'archival_agency_identifier' => 'sa_1',
            'hash' => '60084bf1d83c48fa33c46a0d51346b11a62a3a8112fa7cf1115479cd96f42353',
            'hash_algo' => 'sha256',
        ];
        $this->assertEquals($expected, $array[0]);
        $this->assertCount(1, $iterator);

        // seda 2.1
        $dom = new DOMDocument();
        $dom->load($file3);
        $iterator = new SedaAttachmentsIterator($dom);
        $array = iterator_to_array($iterator);
        $this->assertNotEmpty($array);
        $expected = [
            'filename' => 'file1.txt',
            'size' => '5',
            'archival_agency_identifier' => 'sa_2',
            'hash' => 'f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2',
            'hash_algo' => 'sha256',
        ];
        $this->assertEquals($expected, $array[0]);
        $this->assertCount(1, $iterator);
    }
}
