<?php

namespace Asalae\Test\TestCase\Utility;

use Asalae\Test\Mock\FakeAntivirus;
use Asalae\Utility\TransferAnalyse;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use AsalaeCore\Utility\ValidatorXml;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Libriciel\Filesystem\Utility\Filesystem;

use const TMP_TESTDIR;

class TransferAnalyseTest extends TestCase
{
    public const string SEDA_10_XML = ASALAE_CORE_TEST_DATA . DS . 'sample_seda10.xml';

    public array $fixtures = [
        'app.Agreements',
        'app.AgreementsOriginatingAgencies',
        'app.AgreementsProfiles',
        'app.AgreementsServiceLevels',
        'app.AgreementsTransferringAgencies',
        'app.Eaccpfs',
        'app.Fileuploads',
        'app.FileuploadsProfiles',
        'app.OrgEntities',
        'app.Profiles',
        'app.Pronoms',
        'app.ServiceLevels',
        'app.Siegfrieds',
        'app.TransferAttachments',
        'app.TransferErrors',
        'app.Transfers',
        'app.TypeEntities',
    ];

    private $originalAntivirus;

    /**
     * setUp
     */
    public function setUp(): void
    {
        parent::setUp();
        Utility::reset();
        Filesystem::reset();
        Configure::write('App.paths.data', TMP_TESTDIR);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        $this->originalAntivirus = Configure::read('Antivirus.classname');
        Configure::write('Antivirus.classname', FakeAntivirus::class);
    }

    /**
     * tearDown
     */
    public function tearDown(): void
    {
        parent::tearDown();
        Utility::reset();
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Configure::write('Antivirus.classname', $this->originalAntivirus);
        Exec::waitUntilAsyncFinish();
    }

    /**
     * testAnalyse
     */
    public function testAnalyse()
    {
        $exec = $this->createMock(Exec::class);
        $result1 = new CommandResult(
            [
                'stdout' => json_encode(
                    [
                        'files' => [
                            0 => [
                                'matches' => [
                                    0 => [
                                        'id' => 'fmt/000',
                                        'mime' => 'application/test',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    JSON_UNESCAPED_SLASHES
                ),
                'stderr' => '',
                'code' => 0,
                'success' => true,
            ]
        );
        $result2 = new CommandResult(
            [
                'stdout' => '',
                'stderr' => '',
                'code' => 1,
                'success' => false,
            ]
        );
        $exec->method('command')->willReturn($result1, $result2);

        Utility::set('Exec', $exec);

        $loc = TableRegistry::getTableLocator();
        $TransferAttachments = $loc->get('TransferAttachments');
        $Transfers = $loc->get('Transfers');
        $TransferAttachments->updateAll(['hash' => null], ['id' => 1]);

        $transfer = $Transfers->get(1);
        $this->assertNotFalse((bool)$transfer);
        $transfer->set('is_conform');

        $attachment = $TransferAttachments->find()->where(['transfer_id' => 1])->first();
        $path = $attachment->get('path');
        Filesystem::dumpFile($path, 'test');

        $utility = new TransferAnalyse($transfer);
        Filesystem::copy(self::SEDA_10_XML, $transfer->get('xml'));
        $utility->analyse();

        $attachment = $TransferAttachments->find()->where(['transfer_id' => 1])->first();
        $valid = $attachment->get('valid');
        $this->assertTrue($valid);

        /**
         * Fichiers absents
         */
        Filesystem::remove($path);

        $utility->analyse();
        $attachment = $TransferAttachments->find()->where(['transfer_id' => 1])->first();
        $valid = $attachment->get('valid');
        $this->assertFalse($valid);
    }

    /**
     * testControl
     */
    public function testControl()
    {
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        $validator = $this->createMock(ValidatorXml::class);
        $validator->method('validate')->willReturn(false);
        $validator->errorsDetails = [
            ['message' => 'test', 'line' => 0],
        ];
        Utility::set('ValidatorXml', $validator);

        $transfer = $Transfers->get(1);
        $this->assertNotFalse((bool)$transfer);
        Filesystem::copy(self::SEDA_10_XML, $transfer->get('xml'));

        $utility = new TransferAnalyse($transfer);
        $utility->control();
        $transfer = $Transfers->get(1);
        $this->assertFalse($transfer->get('is_conform'));
    }
}
