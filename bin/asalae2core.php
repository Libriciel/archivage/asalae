#!/usr/bin/php -q
<?php
/**
 * Permets de copier automatiquement les fichiers modifiés dans
 * vendor/libriciel/asalae-core vers le dossier de destination choisi
 * afin d'envoyer facilement les modifications sur git
 *
 * Pour créer le FileWatcher (phpstorm) (note: ne pas modifier les termes entourés de $) :
 *
 * Watched Files
 *  File type : PHP
 *  Scope : file[asalae2]:vendor/libriciel/asalae-core/src//*
 *
 * Watcher Settings
 *  Program     /chemin/absolu/vers/asalae2/bin/asalae2core.php
 *  Arguments   /chemin/absolu/vers/dossier/de/destination/ $FileRelativeDir$/$FileName$ $FilePath$
 */
$destination = rtrim($argv[1], '/') . '/';
$relative = substr($argv[2], strlen('vendor/libriciel/asalae-core/'));
$absolute = $argv[3];

if (is_file($absolute)) {
    $targetFilename = $destination . $relative;
    $path = '';
    $dirs = array_filter(explode('/', dirname($targetFilename)));
    foreach ($dirs as $dirname) {
        $path .= '/'.$dirname;
        if (!is_dir($path)) {
            mkdir($path);
        }
    }
    if (!is_file($targetFilename)) {
        unlink($targetFilename);
    }
    copy($absolute, $targetFilename);
}