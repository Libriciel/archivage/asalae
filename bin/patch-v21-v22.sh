#!/bin/bash
################################################################################
# Mise à jour d'une instance asalae en production
#
# Il est conseillé de redémarrer le serveur postgres et de couper apache avant
# toutes mises à jour.
################################################################################

SIEGFRIED_DEB_URL=${SIEGFRIED_DEB_URL:-'https://nexus.libriciel.fr/repository/deploiement/s/siegfried_1.11.0-1_amd64.deb'}
SIEGFRIED_RPM_URL=${SIEGFRIED_RPM_URL:-'https://nexus.libriciel.fr/repository/deploiement/s/siegfried-1.11.0-1.el8.x86_64.rpm'}
SF_TARGET_VERSION=${SF_TARGET_VERSION:-"1.11"}

failed_with_error () {
  echo "$1" >&2
  exit 1
}
apache_user=$(sudo apachectl -S 2>/dev/null | grep User | sed 's/.*name="\([^"]*\)".*/\1/');

# on vérifie les arguments (zip + path)
if [ "$#" -lt 2 ]; then
 failed_with_error "Usage: ./patch.sh /path/to/file.zip /path/to/install_dir"
fi
ZIP=$(realpath "$1")
if [[ $(file --mime-type -b "$ZIP") != application/*zip* ]]; then
  failed_with_error "Bad mime-type"
fi

# on se place sur le dossier racine d'asalae
workdir=$(realpath "$2")
cd "$workdir" || exit

# shellcheck disable=SC2016
sudo -u "$apache_user" php -r '$v=file("VERSION.txt");$v=trim(array_pop($v));exit(version_compare($v, "2.1", ">=")&&version_compare($v, "2.2", "<")?0:1);' || failed_with_error "Version initiale requise: 2.1.x"

# si on a beanstalkd installé
beanstalkd=$(which beanstalkd)
if [ -z "$beanstalkd" ]; then
  failed_with_error "Beanstalkd n'existe pas, donc on est pas sur une version 2.1.x"
fi

# vérification de présence de la configuration
path_to_local="$workdir/config/path_to_local.php"
echo "$path_to_local"

configured=false
if [ -f "$path_to_local" ]
then
  configured=$(sudo php -r "echo json_decode(file_get_contents(include '$workdir/config/path_to_local.php', true)) ? 1 : 0;")
fi
if [ "$configured" != 1 ]
then
  failed_with_error "configuration not found"
fi

# vérification de la base de données
sudo -u "$apache_user" bin/cake configuration check-database || failed_with_error "unable to connect to the database"

# Passage on mode maintenance
pathToData=$(sudo -u "$apache_user" bin/cake configuration get App.paths.data | tr -d '"')
sudo chown "$apache_user" "$workdir" -R
sudo chown "$apache_user" "$pathToData"/* -R
sudo -u "$apache_user" bin/cake configuration set Interruption.enabled true

# Modification des services
sudo systemctl stop asalae-worker-manager.timer
sudo systemctl stop asalae-worker-manager.service
sudo systemctl stop beanstalkd
sudo -u "$apache_user" bin/cake worker_manager kill_all 2>/dev/null
if [  -f /etc/lsb-release ]; then
    sudo apt remove beanstalkd -y
else
    sudo dnf remove beanstalkd -y
fi
echo "
[Unit]
Description=Asalae worker manager service
After=network.target

[Service]
Type=simple
ExecStart=/usr/bin/php $workdir/bin/cake.php beanstalk_server
User=$apache_user
Group=$apache_user
WorkingDirectory=$workdir
Restart=on-failure

[Install]
WantedBy=multi-user.target
" | sudo tee /etc/systemd/system/asalae-worker-manager.service > /dev/null
sudo rm /etc/systemd/system/asalae-worker-manager.timer
sudo systemctl daemon-reload

# coupure des workers
sleep 2

# sauvegarde les fichiers existants
if [ ! -f "_VERSION.txt" ]; then
  sudo mv -f webroot _webroot 2>/dev/null
  sudo mv -f vendor _vendor 2>/dev/null
  sudo mv -f src _src 2>/dev/null
  sudo mv -f templates _templates 2>/dev/null
  sudo mv -f config _config 2>/dev/null
  sudo mv -f bin _bin 2>/dev/null
  sudo mv -f VERSION.txt _VERSION.txt 2>/dev/null
  sudo mv -f LICENSE.md _LICENSE.md 2>/dev/null
  sudo mv -f index.php _index.php 2>/dev/null
  sudo mv -f composer.lock _composer.lock 2>/dev/null
  sudo mv -f composer.json _composer.json 2>/dev/null
  sudo mv -f .htaccess _.htaccess 2>/dev/null
fi

# dezippage
sudo unzip -o "$ZIP" -d "$workdir" || failed_with_error "unzip failed"
sudo chown "$apache_user" "$workdir" -R

# récupération de certains fichiers/dossiers
if [ -f "_config/app_local.json" ]; then
  sudo cp _config/app_local.json config/app_local.json
fi
if [ -f "_config/path_to_local.php" ]; then
  sudo cp _config/path_to_local.php config/path_to_local.php
fi
if [ -d "_webroot/org-entity-data" ]; then
  sudo cp -r _webroot/org-entity-data webroot
fi

# suppression du cache
sudo rm -rf tmp/cache

# On s'assure que l'utilisateur apache a les permissions sur le dossier asalae2
sudo chown "$apache_user" "$workdir" -R

# On s'assure que l'utilisateur apache a les permissions sur son dossier home
apache_home_dir=$(bash -c "cd ~$(printf %q "$apache_user") && pwd")
sudo chown "$apache_user" "$apache_home_dir"

# mise à jour du schema de base de données
sudo -u "$apache_user" bin/cake migrations migrate || failed_with_error "migration failed"
sudo -u "$apache_user" bin/cake migrations seed || failed_with_error "seed failed"

# re-suppression du cache
sudo rm -rf tmp/cache

# mise à jour des acos (nouvelles permissions d'actions)
sudo -u "$apache_user" bin/cake update || failed_with_error "update acos failed"

# on met à jour les roles globaux
echo "Import des roles..."
sudo -u "$apache_user" bin/cake roles_perms import resources/export_roles.json --keep || failed_with_error "import roles permissions failed"

#Mise à jour de siegfried en v 2.11
osversion=$(sudo awk -F= '$1=="ID" { print $2 ;}' /etc/os-release | tr -d '"')

# Extraire uniquement la version majeure et mineure (par exemple "1.11")
sf_version=$(sf --version | head -n 1 | awk '{print $2}' | cut -d. -f1,2)

echo "Version de Siegfried détectée : $sf_version"
echo "Version de l'OS : $osversion"

if [ "$sf_version" == "$SF_TARGET_VERSION" ]; then
  echo "Siegfried est déjà à jour (version $SF_TARGET_VERSION.x)"
else
  if [ "$osversion" == "ubuntu" ]; then
    echo "Téléchargement et installation de Siegfried sur Ubuntu..."
    curl "$SIEGFRIED_DEB_URL" -o siegfried.deb
    if sudo dpkg -i siegfried.deb; then
      echo "Siegfried installé avec succès sur Ubuntu."
    else
      echo "Erreur lors de l'installation de Siegfried sur Ubuntu."
    fi
  elif [ "$osversion" == "rhel" ]; then
    echo "Installation de Siegfried sur RHEL..."
    if sudo dnf install -y "$SIEGFRIED_RPM_URL"; then
      echo "Siegfried installé avec succès sur RHEL."
    else
      echo "Erreur lors de l'installation de Siegfried sur RHEL."
    fi
  else
    echo "OS non pris en charge."
  fi
fi

# vérification des sources
echo "Vérification des sources"
sudo -u "$apache_user" bin/cake hash_dir compare resources/hashes.txt || failed_with_error "hashes failed"
sudo -u "$apache_user" bin/cake hash_dir check resources/hashes.txt || failed_with_error "hashes failed"

# Retrait du mode maintenance
sudo chown "$apache_user" "$workdir" -R
sudo -u "$apache_user" bin/cake configuration set Interruption.enabled false

# lancement des workers
beanstalkService=$(sudo -u "$apache_user" bin/cake configuration get Beanstalk.service | tr -d '"')
if [ -n "$beanstalkService" ]; then
  sudo systemctl start "$beanstalkService" || failed_with_error "worker launcher failed"
  sudo systemctl enable "$beanstalkService" || failed_with_error "worker launcher failed"
  sudo usermod -a -G adm "$apache_user"
fi

# suppression des fichiers sauvegardés
sudo rm -rf _webroot _vendor _src _templates _config _bin _VERSION.txt _LICENSE.md _index.php _composer.lock _composer.json _.htaccess

# shellcheck disable=SC2016
version=$(php -r '$v=file("VERSION.txt");echo trim(array_pop($v));') || failed_with_error "unable to fetch version"
echo "Mise à jour vers ${version} effectuée"
