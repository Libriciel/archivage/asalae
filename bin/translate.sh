#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
ROOT=$(dirname "$DIR")
./cake i18n extract --marker-error --extract-core yes --merge no --overwrite --output $ROOT/resources/locales --paths $ROOT/src/,$ROOT/vendor/libriciel/
./cake traduction