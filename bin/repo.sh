#!/bin/bash
# shellcheck disable=SC2164

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$DIR/.."
cd vendor/libriciel/asalae-assets || exit 1
pwd
git checkout master
git remote set-url composer git@gitlab.libriciel.fr:libriciel/pole-archivage/asalae/asalae-assets.git
git remote set-url origin git@gitlab.libriciel.fr:libriciel/pole-archivage/asalae/asalae-assets.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/asalae-core || exit 1
pwd
git checkout php83
git remote set-url composer git@gitlab.libriciel.fr:libriciel/pole-archivage/asalae/asalae-core.git
git remote set-url origin git@gitlab.libriciel.fr:libriciel/pole-archivage/asalae/asalae-core.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/cakephp-beanstalk || exit 1
pwd
git checkout php83
git remote set-url composer git@gitlab.libriciel.fr:CakePHP/cakephp-beanstalk.git
git remote set-url origin git@gitlab.libriciel.fr:CakePHP/cakephp-beanstalk.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/cakephp-datacompressor || exit 1
pwd
git checkout master
git remote set-url composer git@gitlab.libriciel.fr:CakePHP/cakephp-datacompressor.git
git remote set-url origin git@gitlab.libriciel.fr:CakePHP/cakephp-datacompressor.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/cakephp-filesystem || exit 1
pwd
git checkout master
git remote set-url composer git@gitlab.libriciel.fr:CakePHP/cakephp-filesystem.git
git remote set-url origin git@gitlab.libriciel.fr:CakePHP/cakephp-filesystem.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/cakephp-login-page || exit 1
pwd
git checkout master
git remote set-url composer git@gitlab.libriciel.fr:CakePHP/cakephp-login-page.git
git remote set-url origin git@gitlab.libriciel.fr:CakePHP/cakephp-login-page.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/cakephp-state-machine || exit 1
pwd
git checkout php83
git remote set-url composer git@gitlab.libriciel.fr:libriciel/pole-archivage/cakephp4-state-machine.git
git remote set-url origin git@gitlab.libriciel.fr:libriciel/pole-archivage/cakephp4-state-machine.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/cakephp4-bootstrap-helpers || exit 1
pwd
git checkout php83
git remote set-url composer git@gitlab.libriciel.fr:libriciel/pole-archivage/cakephp4-bootstrap-helpers.git
git remote set-url origin git@gitlab.libriciel.fr:libriciel/pole-archivage/cakephp4-bootstrap-helpers.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/cakephp5-acl || exit 1
pwd
git checkout master
git remote set-url composer git@gitlab.libriciel.fr:libriciel/pole-archivage/cakephp5-acl.git
git remote set-url origin git@gitlab.libriciel.fr:libriciel/pole-archivage/cakephp5-acl.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/file-converters || exit 1
pwd
git checkout php83
git remote set-url composer git@gitlab.libriciel.fr:file-converters/file-converters.git
git remote set-url origin git@gitlab.libriciel.fr:file-converters/file-converters.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/file-converters-tests || exit 1
pwd
git remote set-url composer git@gitlab.libriciel.fr:file-converters/file-converters-tests.git
git remote set-url origin git@gitlab.libriciel.fr:file-converters/file-converters-tests.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/file-validator || exit 1
pwd
git checkout php83
git remote set-url composer git@gitlab.libriciel.fr:file-validator/file-validator.git
git remote set-url origin git@gitlab.libriciel.fr:file-validator/file-validator.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/file-validator-tests || exit 1
pwd
git remote set-url composer git@gitlab.libriciel.fr:file-validator/file-validator-tests.git
git remote set-url origin git@gitlab.libriciel.fr:file-validator/file-validator-tests.git
git pull
git rev-parse HEAD

cd "$DIR/.."
cd vendor/libriciel/seda2pdf || exit 1
pwd
git checkout php83
git remote set-url composer git@gitlab.libriciel.fr:libriciel/pole-archivage/seda2pdf.git
git remote set-url origin git@gitlab.libriciel.fr:libriciel/pole-archivage/seda2pdf.git
git pull
git rev-parse HEAD
