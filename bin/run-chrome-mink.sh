#!/bin/bash

cd "$(dirname "$0")/.." || exit 1

cleanup() {
    if kill -0 $chrome_pid 2>/dev/null; then
        kill $chrome_pid
    fi
}

./bin/chrome-headless > /dev/null 2>&1 &
chrome_pid=$!
echo "chrome pid: $chrome_pid"

trap cleanup EXIT

docker compose kill
docker compose -f docker-compose-mink.yml kill
docker compose -f docker-compose-mink.yml down -v --remove-orphans
docker compose -f docker-compose-mink.yml up -d --wait

url="https://localhost:3443/api/webservices/ping"
service_ready=false
max_attempts=5
attempt=0

while [ "$attempt" -lt "$max_attempts" ]; do
    http_code=$(curl --silent --write-out "%{http_code}" --insecure "$url" --silent --output /dev/null )

    if [ "$http_code" -eq "200" ]; then
        service_ready=true
        break
    fi

    sleep 2
    attempt=$((attempt + 1))
done

if [ "$service_ready" = false ]; then
    echo "Le service n'est pas opérationnel après plusieurs tentatives. Abandon."
    exit 1
fi

./bin/mink "$@"
mink_exit_code=$?

cleanup

docker compose -f docker-compose-mink.yml kill
docker compose -f docker-compose-mink.yml down -v --remove-orphans
docker compose up -d

if [ $mink_exit_code -eq 0 ]; then
    echo "Test mink OK"
else
    echo "Une erreur a eu lieu"
fi
exit $mink_exit_code
