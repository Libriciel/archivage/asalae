#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR"/.. || exit

mv config/Migrations/schema-dump-default.lock /tmp/schema-dump-default.lock

bin/changelog_builder.php > CHANGELOG.md

bin/cake hash-dir \
 bin/cake.php \
 src/ \
 webroot/ \
 resources/ \
 templates/ \
 vendor/ \
 -r \
 --filter webroot/org-entity-data/\* \
 --filter resources/hashes.txt \
 --filter resources/hashes.txt.md5 \
 --filter webroot/libersign/\* \
 --filter webroot/libersign \
 --filter webroot/coverage/\* \
 --filter webroot/coverage \
 --filter '*.pot' \
 -o resources/hashes.txt

if [ $# -eq 0 ]; then
  version=$(php -r "\$v=file(\"VERSION.txt\");\$v=trim(array_pop(\$v));echo \$v;")
else
  version=$1
fi
ZIPNAME="asalae2-""$(date +"%Y%m%d")""-""$version"".zip"
touch tmp/empty logs/empty
rm -f "$ZIPNAME"
zip -q -r@ "$ZIPNAME" -x '*.git*' < package_list.txt
rm tmp/empty logs/empty

mv /tmp/schema-dump-default.lock config/Migrations/schema-dump-default.lock

echo "$ZIPNAME"

URL1=$(curl -T "$ZIPNAME" https://curl.libriciel.fr)
URL2=$(curl -T bin/patch.sh https://curl.libriciel.fr)
URL3=$(curl -T bin/install.sh https://curl.libriciel.fr)
echo "$URL1"
echo "$URL2"
echo "$URL3"
