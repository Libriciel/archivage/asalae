#!/bin/bash

# Ce script permet la mise à jour des sources via gitlab
user=$(whoami)

if [ $# -eq 0 ]; then
  branch="master"
else
  branch="$1"
  existed_in_local=$(git branch --list "$branch")
  if [[ -z ${existed_in_local} ]]; then
    echo "branch $branch does not exists"
    exit;
  fi
  existed_in_remote=$(git ls-remote --heads origin "$branch")
  if [[ -z ${existed_in_remote} ]]; then
    echo "remote branch $branch does not exists"
  else
    echo "using branch $branch"
  fi
fi

apache_user=$(sudo apachectl -S | grep User | sed 's/.*name="\([^"]*\)".*/\1/');

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR" || exit
cd ..

## Capture du CTRL+C
antikill() {
  echo ""
  echo "========================="
  echo "Stoping... Please wait..."
  echo "========================="
  sudo chown "$apache_user" . -R
  sudo -u "$apache_user" bin/cake configuration set Interruption.enabled false
  sudo -u "$apache_user" bin/cake worker_manager
  sudo -u "$apache_user" bin/autostart.sh
  exit 1
}
trap 'antikill' SIGINT

# vérification de l'existance du git
if [ ! -d ".git/" ]
then
  echo "Git n'a pas été trouvé sur ce dossier"
  exit 1
fi

# arret de ratchet
# shellcheck disable=SC2009
ratchet_pid=$(ps aux | grep -v grep | grep ratchet | sed 's/[^ ]* *\([0-9]*\).*/\1/');
if [ -n "$ratchet_pid" ]
then
      sudo kill "$ratchet_pid"
fi

# on assure que le dossier soit en apache_user
sudo chown "$apache_user" . -R

# parreil pour le fichier de configuration
sudo chown "$apache_user" "$(sudo php -r 'echo include "config/path_to_local.php";')"

# on arrete les workers
sudo -u "$apache_user" bin/cake worker_manager kill-all

# Passage on mode maintenance
sudo -u "$apache_user" bin/cake configuration set Interruption.enabled true

# On s'assure que toutes les connexions sont fermés pour la mise à jour de la bdd
sudo systemctl restart postgresql

# on connecte l'utilisateur pour composer
eval "$(ssh-agent -s)"
ssh-add

# Source update
sudo chown "$user" . -R
git checkout "$branch"
pullreport=$(git pull)

# Si le pull à échoué
if  [ $? -eq 1 ]
then
  echo "===================================================================" >&2
  echo "Échec lors de la mise à jour, des fichiers ont été ajoutés/modifiés" >&2
  echo "===================================================================" >&2
  git diff
  echo "-------------------------------------------------------------------"
  echo "Tapez la commande suivante si vous êtes prêt à perdre toutes les modifications. La suppression manuelle des nouveaux fichiers peut être nécessaire."
  echo "chown $user $(realpath .) -R ; git reset --hard ; chown $apache_user $(realpath .) -R"
  antikill
fi

echo "${pullreport}"
composermodified=$(echo "${pullreport}" | grep composer.json)

if [ -n "$composermodified" ]
then
    composer update
else
    composer update libriciel/asalae-*
fi

sudo chmod 777 tmp/
sudo chmod 777 logs/
sudo chown "$apache_user" . -R

# Update database
sudo rm -rf tmp/cache/*
sudo -u "$apache_user" bin/cake migrations migrate
sudo -u "$apache_user" bin/cake migrations seed
sudo rm -rf tmp/cache/* # il faut supprimer avant ET apres la migration
sudo -u "$apache_user" bin/cake update
sudo -u "$apache_user" bin/cake roles_perms import resources/export_roles.json --keep

# Annulation de l'interruption de service
sudo -u "$apache_user" bin/cake configuration set Interruption.enabled false

# Reboot de ratchet et des workers
sudo -u "$apache_user" bin/autostart.sh
sudo -u "$apache_user" bin/cake worker_manager
