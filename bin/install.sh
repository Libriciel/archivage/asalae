#!/bin/bash
################################################################################
# Installation d'une instance asalae en production
################################################################################

failed_with_error () {
  echo "$1" >&2
  exit 1
}

apache_user=$(sudo apachectl -S 2>/dev/null | grep User | sed 's/.*name="\([^"]*\)".*/\1/');

# on vérifie les arguments (zip + path)
if [ "$#" -lt 2 ]; then
 failed_with_error "Usage: ./install.sh /path/to/file.zip /path/to/install_dir"
fi
ZIP=$(realpath "$1")
if [[ $(file --mime-type -b "$ZIP") != application/*zip* ]]; then
  failed_with_error "Bad mime-type"
fi

# on se place sur le dossier racine d'asalae
workdir=$(realpath "$2")
sudo mkdir -p "$workdir"
sudo chown "$apache_user" "$workdir"
cd "$workdir" || exit

# On s'assure que l'utilisateur apache a les permissions sur son dossier home
apache_home_dir=$(bash -c "cd ~$(printf %q "$apache_user") && pwd")
sudo chown "$apache_user" "$apache_home_dir"

# dezippage
sudo unzip -o "$ZIP" -d "$workdir" || failed_with_error "unzip failed"
sudo chown "$apache_user" "$workdir" -R

# création des dossiers tmp et logs
sudo -u "$apache_user" mkdir -p tmp/ logs/

# vérification des sources
echo "Vérification des sources"
sudo -u "$apache_user" bin/cake hash_dir compare resources/hashes.txt || failed_with_error "hashes failed"
sudo -u "$apache_user" bin/cake hash_dir check resources/hashes.txt || failed_with_error "hashes failed"

sudo chown "$apache_user" "$workdir" -R
# shellcheck disable=SC2016
version=$(php -r '$v=file("VERSION.txt");echo trim(array_pop($v));') || failed_with_error "unable to fetch version"
echo "Installation de la version ${version} effectuée"
