#!/bin/bash

failed_with_error () {
  echo "$1" >&2
  exit 1
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

sudo apt-get install bash-completion

sudo cp $DIR/bash_cake_completion /etc/bash_completion.d/cake || failed_with_error "copy failed"

echo "written /etc/bash_completion.d/cake"
