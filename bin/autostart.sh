#!/bin/bash

# Ce shell permet un lancement automatique par cron de ratchet
# Il est compatible avec un système Ubuntu 16.04 lts par défaut

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
OUTPUT=$(ps aux | grep -v grep | egrep 'cake(\.php)? ratchet')

if [ "$OUTPUT" = "" ]
then
	php cake.php ratchet > /dev/null 2> /dev/null &
fi
