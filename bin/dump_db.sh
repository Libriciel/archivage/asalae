#!/bin/bash

workdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$workdir" || exit

DB_HOST=$(./cake configuration get Datasources.default.host --raw) || >&2 echo "config not found" && exit 1
DB_PORT=$(./cake configuration get Datasources.default.port --raw)
DB_USER=$(./cake configuration get Datasources.default.username --raw)
DB_PASSWORD=$(./cake configuration get Datasources.default.password --raw)
DB_DATABASE=$(./cake configuration get Datasources.default.database --raw)

dumpname=dbdump_"$(date +%Y-%m-%d_%H_%M_%S)".sql
sudo "$DB_PASSWORD" | sudo tee .pgpass > /dev/null
sudo -u postgres pg_dump --blobs \
  -h "${DB_HOST:-localhost}" \
  -p "${DB_PORT:-5432}" \
  -U "$DB_USER" \
  -d "$DB_DATABASE" \
  | tee "../tmp/$dumpname" > /dev/null
realpath "../tmp/$dumpname"
