#!/usr/bin/php -q
<?php

use Cake\I18n\Number;
use Cake\Utility\Hash;

require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/config/bootstrap.php';

$config = json_decode(file_get_contents($json), true);

$db = Hash::get($config, 'Datasources.default.database');
$un = Hash::get($config, 'Datasources.default.username');
$pw = Hash::get($config, 'Datasources.default.password');
$duration = Number::format($argv[1] ?? 0.1);
$cmd = "watch -n'$duration' -d \"PGPASSWORD=$pw psql -U $un -d $db -c 'SELECT datname, pid, usename, query_start, state, query FROM pg_stat_activity;'\"";
passthru($cmd);
