#/bin/bash
URL="$1"
shift;
params="$@"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

if [ -z "$URL" ]; then
    echo "Vous devez spécifier une URL pour behat"
    exit
fi

if [[ ! "$URL" =~ ^https?:// ]]; then
    URL="http://${URL}"
fi

cmd="export BEHAT_PARAMS='{\"extensions\":{\"Behat\\\\MinkExtension\":{\"base_url\":\"${URL}\"}}}'"
eval "$cmd"

cd ..
./vendor/bin/behat $params
