# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2020-10-12 09:47+0200\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: asalae-core/tests/Mock/Testmodel.php:12
#: asalae-core/tests/TestCase/Model/Behavior/OptionsBehaviorTest.php:43
msgctxt "my_field_name"
msgid "value 1"
msgstr ""

#: asalae-core/tests/Mock/Testmodel.php:14
#: asalae-core/tests/TestCase/Model/Behavior/OptionsBehaviorTest.php:44
msgctxt "my_field_name"
msgid "value 2"
msgstr ""

#: asalae-core/tests/Mock/Testmodel.php:16
#: asalae-core/tests/TestCase/Model/Behavior/OptionsBehaviorTest.php:45
msgctxt "my_field_name"
msgid "value 3"
msgstr ""

