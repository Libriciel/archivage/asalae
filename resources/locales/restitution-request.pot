# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2021-01-25 16:42+0100\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Model/Entity/RestitutionRequest.php:51
msgctxt "state"
msgid "création"
msgstr ""

#: Model/Entity/RestitutionRequest.php:54
msgctxt "state"
msgid "en cours de validation"
msgstr ""

#: Model/Entity/RestitutionRequest.php:57
msgctxt "state"
msgid "acceptée"
msgstr ""

#: Model/Entity/RestitutionRequest.php:60
msgctxt "state"
msgid "rejetée"
msgstr ""

#: Model/Entity/RestitutionRequest.php:63
msgctxt "state"
msgid "restitution générée"
msgstr ""

#: Model/Entity/RestitutionRequest.php:66
msgctxt "state"
msgid "restitution téléchargée"
msgstr ""

#: Model/Entity/RestitutionRequest.php:69
msgctxt "state"
msgid "restitution acquittée"
msgstr ""

#: Model/Entity/RestitutionRequest.php:72
msgctxt "state"
msgid "élimination planifiée"
msgstr ""

#: Model/Entity/RestitutionRequest.php:75
msgctxt "state"
msgid "fichiers détruits"
msgstr ""

#: Model/Entity/RestitutionRequest.php:78
msgctxt "state"
msgid "unités d'archives détruites"
msgstr ""

#: Model/Entity/RestitutionRequest.php:81
msgctxt "state"
msgid "restitué"
msgstr ""

