#!/bin/bash

apache_user=$(sudo apachectl -S 2>/dev/null | grep User | sed 's/.*name="\([^"]*\)".*/\1/');

sudo -u "$apache_user" bin/cake worker_manager kill_all
echo "préparation du App.paths.data (./data/mink)"
sudo rm -fr ./data/mink ./tmp/cache
echo "Copie des fichiers de tests..."
sudo -u "$apache_user" cp -r ./tests/Data/asalae_data ./data/mink
DATA_DIR=$(realpath ./data/mink)
sudo -u "$apache_user" bin/cake configuration set App.paths.data "$DATA_DIR"
echo "Drop et création de la BDD mink..."
sudo -u postgres psql -c 'drop database mink;'
sudo -u postgres psql -c 'create database mink owner asalae2;'
sudo -u "$apache_user" bin/cake migrations migrate --connection mink
sudo -u "$apache_user" bin/cake fixtures database --datasource mink --with-volumes
sudo -u "$apache_user" vendor/bin/mink --remote-debugging-port=9223 "$@"
