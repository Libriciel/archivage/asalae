# Changelog

## \[3.0.0\] - 06/01/2025

### Évolutions

- Dockerisation #1989
- Upgrade de Ubuntu, CakePHP 5, PHP 8.3 et Postgresql #1995

### Améliorations

- Ajout des notifications d'élimination dans les attestations d'éliminations #2105

### Corrections

- Héritage des rôles Super archiviste et Administrateur technique dans les rôles custom #2086
- Index dans le nom du fichier d'un transfert lié #2113
- Retrait des liens "mes transferts conformes" et "mes transferts non conformes" de l'index si l'utilisateur n'a pas les droits #2115
- Correction du retrait des favoris #2114
- Filigrane UA éliminées jolie vue SEDA 2.2 manquant  #2129
- Select étape de validation vide pour super archiviste #2131
- Correction filtrage par favoris #2116
- Perte des circuits de validations lors de la promotion en super archiviste #2135

### + modifications liées à la version 2.1.15
### + modifications liées à la version 2.2.6


## \[2.2.6\] - 06/01/2025

### Corrections

- Absence du bouton de révocation d'un Super archiviste #2088
- Désactivation de la case à cocher Validation par signature électronique #2108
- Locks des transferts lors de la validation #2118
- Correction des inscriptions dans le cycle de vie lors de l'édition d'une unité d'archives #1725
- Upload possible si fichier existe en base, mais pas sur le volume #2026
- Correction des bugs sur le téléchargement de CSV #1884

### + modifications liées à la version 2.1.15


## \[2.2.5\] - 01/10/2024

### + modifications liées à la version 2.1.14


## \[2.2.4\] - 24/09/2024

### Corrections

- Gestion des archives composites dans JobMaker #2003
- Les transferts à valider apparaissent toujours quand on change de tenant #1991
- Gestion des transferts simultanés sur une même archive composite #2006
- Blocage des workers par une interruption de service programmé dépassée #2014
- Étanchéité sur le contrôle de l'identifiant de l'unité d'archives de rattachement pour un transfert lié #2015
- Envoi des mails de notifications de transfert pour les super archivistes #2000

### + modifications liées à la version 2.1.13


## \[2.2.3\] - 12/07/2024

### Corrections

- patch de migration 2.2.2 lorsqu'il y a plus de 65535 archives


## \[2.2.2\] - 08/07/2024

### Corrections

- Conversion suite à un transfert lié (archive composite) #1781
- Notification mails des super archivistes #1835
- Choix des rôles spéciaux dans le changement d'entité #1852

### + modifications liées à la version 2.1.12


## \[2.2.1\] - 27/10/2023

### Amélioration

- Ajout du hash de précédent asalae_seal_events_logs #1845

### Corrections

- Créateur de jobs : jobs ajoutés avec le mauvais nom de tube #1748
- Export csv des indicateurs : nom de colonne erroné #1797
- Sélection d'un service d'archives obligatoire à la création / édition d'un superarchiviste
- Export csv de plus de 65k entrées #1809
- Ajout de la promotion et de la révocation d'un super archiviste
- Retrait des commandes asynchrones au sein des workers (conversion, destruction et outgoing-transfert)


## \[2.2.0\] - 17/02/2023

### Évolutions

- Ajout de l'export du registre des entrées en EAD 2002 #931
- Prise en charge du SEDA 2.2 #1210
- Intégration du nouveau logo #528
- Ajout de la colonne TransferringAgencyArchiveIdentifier dans le catalogue #1215
- Ajout de l'index Catalogue de mon service #781
- Gestion des transferts rejetés issus des webservices #1127
- Les tentatives d'intrusions via login/mdp sont inscrites dans le journal #999
- Moteur de recherche à résultats multiple #826
- Référent technique du service d'archives #1138
- Export des journaux d'événements par service d'archives #782
- Nouveau système de panier pour les communications #688
- Communications par lots #840
- Filtre rapide pour les favoris  #1520
- Archives composites #1139
- Archives techniques (ajout / modification / suppression) #549


## \[2.1.15\] - 06/01/2025

### Évolutions

- Affichage des logs de conversions dans les logs du worker #2076
- Dossier temporaire de téléchargement par défaut dans /data/tmp #2032

### Corrections

- Édition du service producteur sur une archive change également le "Name" #1953
- Vérification des tâches planifiées dans la commande check #2128
- Correction des pertes de configuration des tableaux de résultats #1669 


## \[2.1.14\] - 01/10/2024

### Corrections

- seda 2.x - visualisation de la balise répétable Description des entrées #1622
- volume_manager repair : correction des doublons des références de stockage des fichiers réparés #2024
- asw sdk : désactivation des alertes de dépréciations de php 7.4 #2028


## \[2.1.13\] - 24/09/2024

### Évolutions

- Ajout d'un contrôle de cohérence des unités d'archives à éliminer/conserver #1496

### Corrections

- Persistance de la session lors d'une connexion OpenID #2002
- Retour en étape 2 suite à un upload dans l'édition d'un transfert #1987
- Échappement des caractères de contrôle dans les XML #1998
- Validation de transfert impossible après connexion OpenID #2001
- Correction de l'affichage des erreurs #2010
- Compatibilité Siegfried 1.11.1 sur RedHat #1885
- Doublons dans les validations de demandes #1807
- Ajout des "ArchiveUnit.Content.Event" dans le PDF de l'archive #1791
- Nombre et taille des fichiers de management après une élimination #1500
- Élimination d'une archive avec plus de 65535 unités d'archives #2007
- Correction téléchargement des ZIPs volumineux #1900


## \[2.1.12\] - 08/07/2024

### Évolutions

- Affichage des unités d'archives éliminées dans "Afficher la description" #1413
- CustodialHistoryItem est désormais présenté sous forme de bloc de texte #1322
- La modification d'un mot de passe de l'utilisateur est inscrite dans le journal NF461 #1713
- Déplacement des fichiers temporaires pour l'OAI-PMH afin de permettre leur suppression par le ramasse-miettes en cas d'erreur #1646
- Vérification de la date de dernière exécution des tâches planifiées #1863
- Retrait du rôle ws administrateur #1972

### Corrections

- Demandes de transferts sortants : sélection par services producteurs des unités d'archives #1929
- Demandes de transferts sortants : calcul du nombre d'unités d'archives des demandes #1929
- Demandes de transferts sortants : liste des unités d'archives limitées par la pagination #1876
- Éliminations partielles : correction du path des unités d'archives éliminées dans le fichier de management deleted.json #1930
- Correction des intitulés des règles en SEDA 2.1+ #1287
- Blocage de la validation d'un transfert après son édition s'il n'a pas été réanalysé #1782 #1903
- Correction enregistrement sur volume S3 #1918
- Prise en compte de la config Transfers.elementsPerPage #1944
- Bouton Télécharger le transfert dans Réparer un transfert #1974
- Référence vers un BinaryDataObject dans un DataObjectGroup en SEDA v2.x #1945
- Transferts sortants sans les éléments éliminés #1948
- Blocage de retrait d'un volume s'il manque des fichiers #1955
- Nettoyage des fichiers temporaires lors de l'envoi d'un transfert #1961
- Optimisation registre des entrées et visualisation des archives #1975
- Correction de la gestion des versions de travail des mots clés #1872
- Correction affichage de la corbeille dans les niveaux de service #1779
- Les filtres saisis avant la sélection du circuit de validation sont repris après sélection #1947
- Correction calcul de la taille d'une restitution #1802
- Suppression d'un transfert impossible lorsqu'il est en cours de validation #1977
- Exception détaillée pour l'OaiPMH #1705
- Présence d'archives gelées dans les demandes de transferts sortants #1985


## \[2.1.11\] - 27/10/2023

### Évolutions

- Amélioration de la vue détaillée des archives techniques #1691

### Corrections

- Ldap sans Service d'Archives après install par fichier.ini #1612
- Codes seda v2 non présents en traitement par lot #1688
- Erreur de suppresion horodatage #1693
- Ajout des notices EAC-CPF des entités ajoutées avec install config #1738
- Correction création des archives des transferts SEDA 2.x comportant des ArchiveUnitRefId #1484
- Transferts sortants : retour à l'état disponible des archives rejetées par la SAE destinataire #1761
- Journal des événements : ajout des événements des transferts sortants et des restitutions #1759
- Bug lors de la suppression du cron de modification des volumes #1523
- Présence obligatoire de la séquence dans un compteur #1752
- Contrôle et suppression des références suite à une suppression d'un BinaryDataObject #1700
- Déplacement du org-entity-data dans /data/config #605
- Correction de l'option shred pour FilesystemUtility #1765
- Refonte du cron de contrôle d'intégrité #1827
- Corrections des versions de travail des mots clés #1829
- Inscription des requêtes OAI-PMH dans le journal des événements #1830
- Problèmes liées à l'ajout d'Espaces de Conservation Sécurisés #1833


## \[2.1.10\] - 08/02/2022

### Évolutions

- Ajout du référentiel des règles de gestion pour le SEDA V2.x #1486
- Vue détaillée des archives gelées bloquant une demande d'élimination, restitution, transfert sortant #1461
- Ajout d'un événement lors du contrôle de présence des fichiers d'archives #1545
- Suppression des fichiers des transferts acceptés après le délai de rétention #1608
- Ajout d'un Check de la connexion au websocket (ratchet) #1623
- ajout des identifiants des demandes de communication pour les communications #1547

### Corrections

- Échappement des $ dans les commandes #1507
- Problème de persistance des données du formulaire d'ajout de webservice en cas d'erreur #1584
- Problèmes sur les transferts en seda 2.1 sans Attachment #1536
- Problème de lien dans le mail de téléchargement des zips trop lourds #1616
- Métriques de l'entité de l'utilisateur connecté à la place du service d'archives sur le tableau de bord #1543
- Événement de contrôle d'intégrité d'une archive technique #1544
- Finalisation des transactions des conversions par lots lorsqu'il n'y a rien à convertir #1617
- Erreur d'affichage des transfers à valider sur la page d'accueil #1530
- Contrôle de l'intégrité des fichiers de transfert en seda 2.1 #1531
- Meilleur calcul de l'espace disque occupé sur les volumes #1546
- Retrait des points à la fin du nom des répertoires dans les zip #1595
- Il n'est plus possible de dé-assigner un service d'archive d'un ECS #1464
- La commande bin/cake check vérifie également les workers #1538
- Retrait du contournement en @test.fr #1603
- Il n'est plus possible de publier une liste de mots-clés avec doublons #1347
- Action manquante pour le renvoit de demande de transfert sortant #1508
- Elimination d'une archive avec fichier de taille 0 #1470
- Correction de l'ajout du logo du service d'archives #1516
- Envoi du data en multipart/form-data sur des methods != POST #1511
- api/users : autorisation pour le rôle ws administrateur #1510
- Suppression possible d'une étape d'un circuit alors qu'elle est en cours d'utilisation #1509
- Meilleurs gestion des transferts sortants larges #1525
- Correction de l'édition d'un volume S3 utilisé #1514
- Correction logout_redirection dans OpenIDConnect #1615
- Problème d'édition d'un transfert lors de la validation #1624
- Edition de la communicabilité d'une description d'unité d'archive en seda 2.1 #1619
- Problèmle d'édition du délais de communication description et keyword en SEDA 2.1 #1604
- Affichage de l'accéssibilité de l'unité d'archives plutôt que celle de l'archive dans le catalogue #1553
- SEDA 2.1 - modification du délais de communication non pris en compte #1604
- Correction de la suppression d'un élément dans le seda 2.1 #1673
- Permissions sur l'édition d'un transfert #1674
- Homogénéisation des icônes d'attestations #1607


## \[2.1.9\] - 27/10/2022

### Évolutions

- Gel/dégel des archives (NF461) #934
- Attestations d'archivage (NF461) #1433
- Attestations d'élimination (NF461) #1433
- Attestations de restitution (NF461) #1433
- Ajout d'un bouton de test de notification
- Amélioration de la tache planifiée JobMaker #1332
- Commande de modification des volumes verrouillés #1371

### Corrections

- Evénement de login depuis une connexion OpenId #1429
- Amélioration du cron d'ajout d'un volume #1406
- Optimisation du filtre de recherche par mot clés
- Unicité des noms des Systèmes d'archivage électronique au sein d'un même Service d'Archives #1436
- Echappement de certains caractères dans les XML #1439
- Correction de sécurité #1435
- Uniformisation des labels Identifiant et Nom d'utilisateur #1417
- Libreoffice v7 valide pour les conversions #1451
- Bug d'affichage du moteur de recherche #1454
- Optimisation de contrôle de transfert pour le SEDA 2.1 #1485
- Correction du réparateur de transfert #1502


## \[2.1.8\] - 30/08/2022

### Évolutions

- Suppression journalière du cache expiré #1355
- Suppression possible des transferts par webservice en préparation #1246
- Saisie manuelle possible de références (externe ou pas) dans le seda 2.1 #1319
- Lien cliquable entre DataObjectReference et ContentDataObject #1295
- Commande d'aide à la configuration OpenID #1409
- Affichage d'un timer sur notification #1414

### Corrections

- Colonne Demandeur manquante dans les menus "demandes de" #241
- Erreur 500 si suppression valeur d'un champ dans l'édition de la configuration #1286
- Affichage des noms long dans le panier de communication #1293
- SEDA 2.1 : souci d'affichage des fichiers dans "Visualiser" du menu Archives consultables #1296
- Demande d'élimination : mauvais renvoi à un sous menu #1304
- Requête fausse sur bouton de vérification d'intégrité dans Stockages -> Parcourir #1341
- Transferts conformes/non conformes à valider : bug sur le tri des colonnes #1353
- Validation par lot : masquage les cases à cocher avant de sélectionner une étape #1354
- Problème d'affichage des indicateurs
- Validation des transferts par lots sur toutes les pages #1324
- Correction de l'analyse sur la taille du transfert #1373
- Diverses améliorations de performance
- Correction des validations automatique dans la configuration du service d'archives #1346
- Notifications sur la construction des fichiers zip #1365
- Téléchargement (download) des fichiers en streaming #1379
- Affichage dans la liste juste après la création d'un utilisateur #1391
- Permissions du rôle ws_admin manquantes #1397 #1398
- Correction du calcul du poids des fichiers suite à une élimination
- Utilisation du Type MIME du fichier plutôt que celui donné par siegfried #1389
- Diverses corrections dans l'édition d'un transfert
- Correction communicabilité en seda 2.1 #1369
- Affichage des fichiers avec multiple références #1292
- Mise à jour des zip/pdf/description publique suite à une modification  #1412
- Mise à jour du cycle de vie en cas de conversion #1291
- Pas de contrôle d'intégrité sur une archive en construction #1359
- Problème d'étanchéité dans les options d'édition d'une archive #1394
- Logout OPENIDConnect sans endpoint end_session #1407
- Affichage d'une erreur si connexion d'un utilisateur OpenID qui n'existe pas sur asalae #1408
- Retrait des "Référence à un objet-données" dans les unités d'archives non communicables #1340
- Edition d'une sous-unité d'archives sans description en seda 1.0 #1418
- Corrections étanchéité entre versants #1423 ~ID001
- Protection injection HTML #1424 ~ID003
- Retrait d'un chemin de fichier dans la réponse d'un upload #1425 ~ID004
- Ajout de la directive Content-Security-Policy
- Affichage des formats de fichier dans l'édition d'un accord de versement #1428


## \[2.1.7\] - 16/06/2022

### Évolutions

- API d'accès aux fichiers des archives accessibles #1316
- Ajout d'un connecteur OpenID-Connect #1254
- Console technique : system check : amélioration du retour de configuration de l'application #1315
- Ajout des filtres de recherche sur le profil et l'accord de versement dans les validations #1260
- Filtre par producteur obligatoire dans les Archives éliminables et Archives restituables #1264

### Corrections

- Bug pagination des unités d'archives des transferts dans la vue détaillée #1274
- Traductions manquantes dans les vues pdf et détaillées en SEDA 2.1 #1331
- Oaipmh : retrait du filename dans le header #1335
- Suppression des liens de téléchargement des fichiers dans la description publique du catalogue #1339


## \[2.1.6\] - 25/04/2022

### Évolutions

- Vérification des lft/rght stockés en base de données #1279

### Corrections

- Suppression de la colonne "Peut visualiser les éléments des entités filles" pour les webservices (inutilisée) #1250
- Bug bloquant sur l'édition du service d'archives #1252
- Échappement des caractères spéciaux dans les identifiants pour l'OAI-PMH #1256
- Retrait du lien Indicateurs pour les entités non concernées #1268
- Suppression des séparateurs de milliers dans l'export csv des indicateurs #1259
- Contrôle de l'accord de versement sur un document sans format ou type MIME #1266
- Retrait de la valeur vide dans les formats d'un accord de versement #1265
- Affichage de la valeur issue d'un AR062 #1282
- Ajout de ArchivalAgencyArchiveUnitIdentifier dans l'export PDF en seda 2.1 #1272


## \[2.1.5\] - 14/03/2022

### Évolutions

- Ajout de la sonde de check des jobs en échec #1071
- Ajout dans le cron check, les jobs et l'espace disque #1078
- Limitation du nombre de tentatives de connexion sur un même login #1209
- Définition du proxy dans l'installation par fichier ini #1146
- Filtre de recherche par référence de mots-clés dans le catalogue et les archives consultables #1145
- Ajout de la validation du transfert dans le cycle de vie de l'archive #1101
- Les noms d'unités d'archives ne peuvent plus être vide (transfert non conforme) #1222
- Envoie par morceaux possibles pour les transferts sortants #1223
- Ajout de la validation de la config dans le Check #1211
- Affichage de l'interruption de service en json pour les webservices #1235

### Corrections

- Droits sur les Transferts pour le rôle Référent Archiviste #1100
- Colonne description en double dans l'index des archives consultables #1110
- Erreur plus explicite en cas de champs date mal renseigné à la création d'un transfert #1112
- Unicité des identifiants de transferts par service d'archives #1119
- Filtres de dates sur les indicateurs #1166
- Sélection d'une entité parente obligatoire #1172
- Erreur sur certains traitements par lots sur les ArchiveUnit type Document #1120
- La tâche "Rétention des transferts acceptés" gère maintenant les archives éliminées, restituées ou transférées #1191
- Réduction du rythme de mise à jour de l'état d'un job pour éviter une saturation de Beanstalkd #1193
- Récupération de métadonnées de description d'unité d'archives en seda 2.1 pour la recherche #1201 #1111
- Récupération de métadonnées de DUA des sous-unités d'archives en seda 2.1 #1244
- Utilisation du proxy possible dans l'horodatage Universign #1060
- Code de l'algo pour les transferts en seda 1.0 avec un algo différent de celle de l'application #1199
- Meilleur marquage du texte recherché dans les moteurs de recherche #1088
- Transferts sortants : ajout du choix de niveau de service #1153
- Transferts par arbo avec des caractères spéciaux en seda 2.1 #1221
- Imports des bordereaux non UTF-8 #1225
- Identifiants absents de l'export PDF #1189
- Meilleure gestion d'erreurs sur les tâches planifiées #1224
- Mise à jour du tableau de résultats après une modification d'un transfert #1065
- Import de liste de mot clef avec des lignes vides #1205
- Suppression des communications ayant dépassé le délai de rétention #1151
- Bug sur communication pour une unité d'archive sans fichier #1183
- Réparation du bouton "test" sur les SAEs configurés #1218
- Ordre de passage des archives pour le contrôle d'intégrité #1213
- Meilleure gestion d'erreurs sur les SAEs mal configurés #1219
- Un utilisateur désactivé ne doit pas pouvoir se connecter #1234
- Erreur sur la connexion avec utilisateur non existant #1237
- Extensions autorisées par l'accord de versement insensibles à la casse #1231
- Accès à certains webservices à nouveau fonctionnels #1236
- DestructionWorker peut reprendre suite à un d'échec #1241
- Filtre de recherche sur le sort final dans le registre des entrées #1245
- Téléchargement de fichiers avec certains caractères spéciaux corrigé #1249


## \[2.1.4\] - 08/02/2022

### Évolutions

- Api de transferts par morceaux
- Nom court pour les entités #1113
- Gestion du SEDA 2.1 pour l'OAI-PMH #1164
- Contrôle de la taille des mots-clés #1053
- Mise à jour de bootstrap #1148
- Transactions fichiers configurable #1156

### Corrections

- Mise à jour des métriques de l'archive suite à une élimination
- Etanchéité des étapes de validation dans le select #1058
- Erreur sur l'édition d'un Service d'Archive #1086
- Ajout de xvfb et ghostscript dans le system check #1115
- Erreur lors du rejet d'un transfert invalide sans aucun fichier #1129
- bug sur l'import d'utilisateur ldap déjà dans l'application #1185
- Erreur sur notification d'acception en seda 2.1 #1190
- Correction de l'affichage des fichiers lié d'une archive #1192
- Collision sur nom de fichier entre plusieurs services d'archives dans le worker ArchiveBinary #1158
- Conversions par lots possible sur archive de plus de 65535 fichiers #1194
- Refonte du système d'upload du cycle de vie #1197
- Remplissage auto du KeywordReference en seda 2.1 #1133
- Réencodage des transferts entrants #1083


## \[2.1.3\] - 01/09/2021

### Évolutions

- Edition du service d'horodatage du service d'archives gestionnaire #1041
- Edition de l'admin technique #1000
- Désactivation possible des notifications d'exploitation #1000
- Mise à jour de la validation RNG Jing #1014
- Filtres supplémentaires sur les archives #1037
- Suppression de référence lors d'un changement de mot clé par traitement #1021

### Corrections

- Corrections export du journal #887
- Correction du cron Rétention des fichiers de transferts
- Correction analyse : apostrophes dans les mots-clés #1015
- Correction de sécurité #1002
- Correction liens vers fichiers dans transferts/archives #1008
- Correction suppression d'un circuit de validation utilisé par un accord de versement #1039
- Correction navigation en seda v0.2 #1026
- on ajoute seulement les formats listés dans le seda #1030
- Correction pagination dans analyse d'un transfert #1024
- Correction bug sur "&" dans archives et journal des événements #1012
- Correction du décalage de la mise en gras du texte dans le moteur de recherche #1006
- Correction perte de session et transfert par arbo #1025
- Evénement de téléchargement depuis la description des entrées #1047
- Visualisation d'une unité d'archives #996
- Correction ajout d'un webservice #1018
- Correction du ArchiveTransferReply en seda 2.1 #1016
- Réponses vide dans Restservices #1043
- Correction élimination des transferts sortants refusées #1048


## \[2.1.2\] - 01/07/2021

### Évolutions

- Moins de fichiers temporaires #977 #978
- Plusieurs tentatives avant échec du cron Pronoms #937
- Ajout de l'option verify_peer au cron Pronoms
- Ajout de conditions sur l'édition de certains identifiants #985 #986 #987
- Métadonnées auto sur fichiers de transferts #982
- Des mots-clés en double invalident un transfert #970
- Ajouts de filtres sur les demandes de transferts sortants #995

### Corrections

- Affichage des rapports des crons en échec
- Correction du cron Rétention des transferts avec un grand nombre de fichiers liés #988
- Ajout des contrôles manquants sur les accords de versements #975
- Bug sur le choix des entités d'un webservice #994


## \[2.1.1\] - 02/06/2021

### Évolutions

- Compteurs capables de trouver le prochain identifiant disponible #915 #916
- Contrôle d'intégrité des fichiers de management des archives #926
- Désactivation possible des entités #962
- Plusieurs tentatives lors du check des volumes #953
- Configuration des ips dans admin technique #547

### Corrections

- Correctif de sécurité #935
- Correction affichage historique validation vue détaillée des transferts #846
- Correction upload possible de fichiers vide #956
- Resynchronisation du disk_usage #928
- Une archive ne peut plus être sans fichier #963
- Correction des filtres de recherches des fichiers d'une archive consultable #960
- Correction bug conversion vidéo #959


## \[2.1.0\] - 23/04/2021

### Nouveautés

- Prise en charge du SEDA v2.1
- Ajout du module "Archive consultables"
- Transfert en SEDA 2.1 par formulaire #550
- Nouvelle page de connexion #525
- Améliorations sur le moteur de recherche #520
- Commande shell de check #573
- Filtrage des IPs dans l'administration technique #547
- Authentification possible via Keycloak #546
- Téléchargement des fichiers SEDA en PDF #13
- Conversions des fichiers #73
- Utilisateurs webservice dans la liste des utilisateurs #541
- Traitement par lots #7

### Améliorations

- Email optionnel pour les administrateurs techniques #478
- Optimisation du moteur de recherche #454
- Options lié à la validation pour l'utilisateur #450
- Filtres de recherche au sein de la page #471
- Recherche insensible à la casse et aux accents dans l'éditeur de transferts #502
- Ajout de la colonne "Hérite des droits" dans les rôles spécifiques
- Ajout de nombreux filtres de recherches #507
- Amélioration de la gestion des LDAPs #510
- Retrait de l'affichage des /dev/loop* dans Etat des disques #192
- Ajout du service d'archives dans la liste des sessions actives #553
- Ajout de l'état création pour les unités d'archives #495
- Modification des informations du cycle de vie PREMIS #523
- Catalogue : accès aux fichiers des archives librement communicables #540
- Installation en ligne de commande #544
- Ajout des actions de l'administration technique dans le journal des événements #548
- Indicateurs : ajout des volumes pour les fichiers de conservation et de diffusion des archives #715
- Indicateurs : export csv #716
- Seuls les validateurs peuvent modifier en transfert envoyé #776
- Un rôle doit être rattaché à un type d'entité au moins #939
- Un rôle doit être rattaché à au moins un type d'entité #939

### Autre

- Suppression de l'option de compteur obsolète "RelativeIdentifier" #601
- Retrait de "Recherche sur les entrées" #773


## \[2.0.10\] - 01/09/2021

### Évolutions

- Edition du service d'horodatage du service d'archives gestionnaire #1041
- Edition de l'admin technique #1000
- Désactivation possible des notifications d'exploitation #1000
- Mise à jour de la validation RNG Jing #1014
- Filtres supplémentaires sur les archives #1037

### Corrections

- Corrections export du journal #887
- Correction du cron Rétention des fichiers de transferts
- Correction analyse : apostrophes dans les mots-clés #1015
- Correction de sécurité #1002
- Correction liens vers fichiers dans transferts/archives #1008
- Correction suppression d'un circuit de validation utilisé par un accord de versement #1039
- Correction navigation en seda v0.2 #1026
- on ajoute seulement les formats listés dans le seda #1030
- Correction pagination dans analyse d'un transfert #1024
- Correction bug sur "&" dans archives et journal des événements #1012
- Correction du décalage de la mise en gras du texte dans le moteur de recherche #1006
- Correction perte de session et transfert par arbo #1025
- Evénement de téléchargement depuis la description des entrées #1047


## \[2.0.9\] - 01/07/2021

### Évolutions

- Moins de fichiers temporaires #977 #978
- Plusieurs tentatives avant échec du cron Pronoms #937
- Ajout de l'option verify_peer au cron Pronoms
- Ajout de conditions sur l'édition de certains identifiants #985 #986 #987
- Métadonnées auto sur fichiers de transferts #982
- Des mots-clés en double invalident un transfert #970

### Corrections

- Affichage des rapports des crons en échec
- Correction du cron Rétention des transferts avec un grand nombre de fichiers liés #988
- Ajout des contrôles manquants sur les accords de versements #975


## \[2.0.8\] - 02/06/2021

### Évolutions

- Compteurs capables de trouver le prochain identifiant disponible #915 #916
- Contrôle d'intégrité des fichiers de management des archives #926
- Désactivation possible des entités #962
- Plusieurs tentatives lors du check des volumes #953

### Corrections

- Correctif de sécurité #935
- Correction affichage historique validation vue détaillée des transferts #846
- Correction upload possible de fichiers vide #956
- Resynchronisation du disk_usage #928
- Une archive ne peut plus être sans fichier #963


## \[2.0.7\] - 01/04/2021

### Améliorations

- Mapping auto des champs LDAP #770
- Modification du mapping LDAP répercuté sur les utilisateurs #770

### Corrections

- Correction de la conformité du transfert dans la validation par mail #768
- Correction de l'événement visualisation d'un fichier #779
- Correction double téléchargement dans la visionneuse #779
- Correction ajout d'utilisateur LDAP dans la console #774
- Ajout des mots-clés dans la visualisation du cycle de vie #780
- Correction test de connexion LDAP #801
- Correction de la remise à zéro du compteur d'archive units


## \[2.0.6\] - 02/03/2021

### Evolutions

- Check des volumes asynchrone
- Alerte en cas d'espace disque faible #732

### Corrections

- Correction niveau de service si ECS non sélectionné #696
- Compatibilité css pour Chrome v88 #697
- Vérification du taux d'occupation des volumes #739
- Cron Check iso avec la vérification dans l'administration technique #719
- Correction des messages Acceptance et Reply pour du SEDA 0.2 #722
- Optimisation du ZIP d'archives et de transferts #728
- Correction casse des champs LDAP #738


## \[2.0.5\] - 01/02/2021

### Evolutions

- Ajout de tests sur les volumes #691
- Ajout de l'état des volumes sur la vérification de l'application #692
- Ajout de l'état des workers sur la vérification de l'application #693

### Corrections

- Correction pagination dans validation par email #646
- Affichage d'une erreur en cas d'échec de duplication de transfert #651
- Correction de l'export d'une TechnicalArchives #672
- Changement de l'arborescence du zip d'une entrée #279
- Ajout d'éléments dans la vérification de l'application #670
- Correction de l'édition d'un mot clé d'une entrée #679
- Correction de l'erreur d'affichage du total de données dans les Indicateurs #683
- Correction affichage de l'action "ouvrir dans le navigateur" #685
- Correction de la désactivation d'un volume HS #694
- Refonte de la mise à jour du cycle de vie/description d'archives #695


## \[2.0.4\] - 08/01/2021

### Evolutions

- Ajout de l'action parcourir dans les volumes
- Mise à jour en temps réel de l'état des Jobs (administration technique)

### Corrections

- Correction envoie d'email multiple #623
- Création des archives avec mots-clés vide #624
- Correction de la validation par lots par Service d'archives #638
- Upload de fichiers > 5Go sur volume S3 #639
- Correction des noms de fichier invalides dans les fichiers ZIP #635
- Meilleure détection des Programmes manquant dans System check #641
- Correction des liens dans le panier pour les unités d'archives filles #630 #642
- Modification de l'intégrité lors de la modification d'un fichier seda 0.2 #637
- Correction des permissions sur la visualisation dans le catalogue #626
- Correction des filtres de recherche à valeurs multiples #632
- Correction du remplissage auto du format dans l'éditeur de transferts #643


## \[2.0.3\] - 02/12/2020

### Evolutions

- Ajout des dossiers inscriptibles dans le system check #608

### Corrections

- Correction validation en lot par signature #593
- Correction bug sur nom de fichier trop long #595
- Correction webservice sedaMessage Get transfert rejeté #596
- Correction affichage de l'email du validateur
- Correction de la sauvegarde des filtres de recherche #594
- Demande de destruction possible après un refus #591
- Correction du cycle de vie d'une archive non terminée #611
- Gestion des volumes inactifs #615 #616
- Correction du tri (pagination ajax) #617


## \[2.0.2\] - 30/10/2020

### Evolutions

- Ajout de feedback pour la création d'un transfert et son contrôle
- Meilleure détection du travail en cours dans les workers #585

### Corrections

- Mise à jour de la liste des utilisateurs d'un service d'archives #562
- Optimisation du registre des entrées #569
- Correction bug sur le paramétrage des tableaux de résultats #560
- Affichage de la totalité des utilisateurs de LDAP dans un import #561
- Corrections sur les résultats du moteur de recherche (catalogue)
- Correction sur l’étanchéité des entités entre services d'archives #581
- Correction validation sur les services d'archives #582
- Inscription de la communication dans le journal des événements #524
- Affichage du "Cycle de vie d'une archive" possible après destruction
- Correction bug sur analyses consécutives #583


## \[2.0.1\] - 06/10/2020

### Évolutions

- Groupement par Service Organisationnel pour l'import des utilisateurs LDAP #462
- Import d'utilisateur Ldap depuis la console technique #460
- Filtres pour les erreurs de transfert #481
- Retour du cron probe_shell plus lisible #482
- Ajout d'une commande shell pour déverrouiller un cron
- Retrait du volume OpenStackSwift
- Changement de licence (AGPL V3) #522
- Meilleure gestion du LDAP #556
- Vérification de la présence de JAVA #559

### Corrections

- Modification de l'affichage de l'icône de suppression d'un web service #458
- Modification des options de rôles dans l'import des utilisateurs LDAP #462 #464
- Correction des boutons de test d'un LDAP sur certains LDAPs #465 #480
- Correction de l'affichage du contenu du panier #461
- Correction sur les résultats de recherche dans le catalogue #468 #475
- Correction memory limit dans édition d'un transfert #469
- Correction action groupée pour fichiers de transfert #473
- Correction pagination sur l'import d'utilisateur ldap #483
- Contrôle d'intégrité des fichiers des archives techniques #485
- Champ Region configurable sur Volume Minio #487
- Redirection des exceptions des crons dans les logs #489
- Amélioration de la détection d'algorithmes de hachage dans un transfert #362
- Correction de l'affichage lors de filtrages consécutifs en modal #488
- Correction de la détection de l'intégrité du seda 0.2 #490
- Correction de la suppression des cookies (php >= 7.3)
- Correction de l'action "vider le panier"
- Les connexions de webservices ne sont plus loggés #512 
- Amélioration et correction de la validation des fichiers
- Correction du parseur de dates
- Modification de la taille max des communications en base #552
- Modification de la taille max des noms de fichiers en base #554
- Amélioration et correction de la gestion des LDAPs #556
- Unicité des utilisateurs du LDAP #477
- Ajout des services d'archives dans les filtres d'indicateurs de volumétrie
- Correction du cache du zip d'archives #557


