## \[2.0.7\] - 01/04/2021

### Améliorations

- Mapping auto des champs LDAP #770
- Modification du mapping LDAP répercuté sur les utilisateurs #770

### Corrections

- Correction de la conformité du transfert dans la validation par mail #768
- Correction de l'événement visualisation d'un fichier #779
- Correction double téléchargement dans la visionneuse #779
- Correction ajout d'utilisateur LDAP dans la console #774
- Ajout des mots-clés dans la visualisation du cycle de vie #780
- Correction test de connexion LDAP #801
- Correction de la remise à zéro du compteur d'archive units
