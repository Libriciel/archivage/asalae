## \[2.1.8\] - 30/08/2022

### Évolutions

- Suppression journalière du cache expiré #1355
- Suppression possible des transferts par webservice en préparation #1246
- Saisie manuelle possible de références (externe ou pas) dans le seda 2.1 #1319
- Lien cliquable entre DataObjectReference et ContentDataObject #1295
- Commande d'aide à la configuration OpenID #1409
- Affichage d'un timer sur notification #1414

### Corrections

- Colonne Demandeur manquante dans les menus "demandes de" #241
- Erreur 500 si suppression valeur d'un champ dans l'édition de la configuration #1286
- Affichage des noms long dans le panier de communication #1293
- SEDA 2.1 : souci d'affichage des fichiers dans "Visualiser" du menu Archives consultables #1296
- Demande d'élimination : mauvais renvoi à un sous menu #1304
- Requête fausse sur bouton de vérification d'intégrité dans Stockages -> Parcourir #1341
- Transferts conformes/non conformes à valider : bug sur le tri des colonnes #1353
- Validation par lot : masquage les cases à cocher avant de sélectionner une étape #1354
- Problème d'affichage des indicateurs
- Validation des transferts par lots sur toutes les pages #1324
- Correction de l'analyse sur la taille du transfert #1373
- Diverses améliorations de performance
- Correction des validations automatique dans la configuration du service d'archives #1346
- Notifications sur la construction des fichiers zip #1365
- Téléchargement (download) des fichiers en streaming #1379
- Affichage dans la liste juste après la création d'un utilisateur #1391
- Permissions du rôle ws_admin manquantes #1397 #1398
- Correction du calcul du poids des fichiers suite à une élimination
- Utilisation du Type MIME du fichier plutôt que celui donné par siegfried #1389
- Diverses corrections dans l'édition d'un transfert
- Correction communicabilité en seda 2.1 #1369
- Affichage des fichiers avec multiple références #1292
- Mise à jour des zip/pdf/description publique suite à une modification  #1412
- Mise à jour du cycle de vie en cas de conversion #1291
- Pas de contrôle d'intégrité sur une archive en construction #1359
- Problème d'étanchéité dans les options d'édition d'une archive #1394
- Logout OPENIDConnect sans endpoint end_session #1407
- Affichage d'une erreur si connexion d'un utilisateur OpenID qui n'existe pas sur asalae #1408
- Retrait des "Référence à un objet-données" dans les unités d'archives non communicables #1340
- Edition d'une sous-unité d'archives sans description en seda 1.0 #1418
- Corrections étanchéité entre versants #1423 ~ID001
- Protection injection HTML #1424 ~ID003
- Retrait d'un chemin de fichier dans la réponse d'un upload #1425 ~ID004
- Ajout de la directive Content-Security-Policy
- Affichage des formats de fichier dans l'édition d'un accord de versement #1428
