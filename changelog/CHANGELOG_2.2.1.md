## \[2.2.1\] - 27/10/2023

### Amélioration

- Ajout du hash de précédent asalae_seal_events_logs #1845

### Corrections

- Créateur de jobs : jobs ajoutés avec le mauvais nom de tube #1748
- Export csv des indicateurs : nom de colonne erroné #1797
- Sélection d'un service d'archives obligatoire à la création / édition d'un superarchiviste
- Export csv de plus de 65k entrées #1809
- Ajout de la promotion et de la révocation d'un super archiviste
- Retrait des commandes asynchrones au sein des workers (conversion, destruction et outgoing-transfert)
