## \[2.0.9\] - 01/07/2021

### Évolutions

- Moins de fichiers temporaires #977 #978
- Plusieurs tentatives avant échec du cron Pronoms #937
- Ajout de l'option verify_peer au cron Pronoms
- Ajout de conditions sur l'édition de certains identifiants #985 #986 #987
- Métadonnées auto sur fichiers de transferts #982
- Des mots-clés en double invalident un transfert #970

### Corrections

- Affichage des rapports des crons en échec
- Correction du cron Rétention des transferts avec un grand nombre de fichiers liés #988
- Ajout des contrôles manquants sur les accords de versements #975
