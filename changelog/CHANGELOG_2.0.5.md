## \[2.0.5\] - 01/02/2021

### Evolutions

- Ajout de tests sur les volumes #691
- Ajout de l'état des volumes sur la vérification de l'application #692
- Ajout de l'état des workers sur la vérification de l'application #693

### Corrections

- Correction pagination dans validation par email #646
- Affichage d'une erreur en cas d'échec de duplication de transfert #651
- Correction de l'export d'une TechnicalArchives #672
- Changement de l'arborescence du zip d'une entrée #279
- Ajout d'éléments dans la vérification de l'application #670
- Correction de l'édition d'un mot clé d'une entrée #679
- Correction de l'erreur d'affichage du total de données dans les Indicateurs #683
- Correction affichage de l'action "ouvrir dans le navigateur" #685
- Correction de la désactivation d'un volume HS #694
- Refonte de la mise à jour du cycle de vie/description d'archives #695
