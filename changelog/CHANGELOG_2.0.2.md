## \[2.0.2\] - 30/10/2020

### Evolutions

- Ajout de feedback pour la création d'un transfert et son contrôle
- Meilleure détection du travail en cours dans les workers #585

### Corrections

- Mise à jour de la liste des utilisateurs d'un service d'archives #562
- Optimisation du registre des entrées #569
- Correction bug sur le paramétrage des tableaux de résultats #560
- Affichage de la totalité des utilisateurs de LDAP dans un import #561
- Corrections sur les résultats du moteur de recherche (catalogue)
- Correction sur l’étanchéité des entités entre services d'archives #581
- Correction validation sur les services d'archives #582
- Inscription de la communication dans le journal des événements #524
- Affichage du "Cycle de vie d'une archive" possible après destruction
- Correction bug sur analyses consécutives #583
