## \[2.1.4\] - 08/02/2022

### Évolutions

- Api de transferts par morceaux
- Nom court pour les entités #1113
- Gestion du SEDA 2.1 pour l'OAI-PMH #1164
- Contrôle de la taille des mots-clés #1053
- Mise à jour de bootstrap #1148
- Transactions fichiers configurable #1156

### Corrections

- Mise à jour des métriques de l'archive suite à une élimination
- Etanchéité des étapes de validation dans le select #1058
- Erreur sur l'édition d'un Service d'Archive #1086
- Ajout de xvfb et ghostscript dans le system check #1115
- Erreur lors du rejet d'un transfert invalide sans aucun fichier #1129
- bug sur l'import d'utilisateur ldap déjà dans l'application #1185
- Erreur sur notification d'acception en seda 2.1 #1190
- Correction de l'affichage des fichiers lié d'une archive #1192
- Collision sur nom de fichier entre plusieurs services d'archives dans le worker ArchiveBinary #1158
- Conversions par lots possible sur archive de plus de 65535 fichiers #1194
- Refonte du système d'upload du cycle de vie #1197
- Remplissage auto du KeywordReference en seda 2.1 #1133
- Réencodage des transferts entrants #1083
