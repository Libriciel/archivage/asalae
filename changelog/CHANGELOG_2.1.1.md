## \[2.1.1\] - 02/06/2021

### Évolutions

- Compteurs capables de trouver le prochain identifiant disponible #915 #916
- Contrôle d'intégrité des fichiers de management des archives #926
- Désactivation possible des entités #962
- Plusieurs tentatives lors du check des volumes #953
- Configuration des ips dans admin technique #547

### Corrections

- Correctif de sécurité #935
- Correction affichage historique validation vue détaillée des transferts #846
- Correction upload possible de fichiers vide #956
- Resynchronisation du disk_usage #928
- Une archive ne peut plus être sans fichier #963
- Correction des filtres de recherches des fichiers d'une archive consultable #960
- Correction bug conversion vidéo #959
