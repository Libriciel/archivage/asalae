## \[2.1.7\] - 16/06/2022

### Évolutions

- API d'accès aux fichiers des archives accessibles #1316
- Ajout d'un connecteur OpenID-Connect #1254
- Console technique : system check : amélioration du retour de configuration de l'application #1315
- Ajout des filtres de recherche sur le profil et l'accord de versement dans les validations #1260
- Filtre par producteur obligatoire dans les Archives éliminables et Archives restituables #1264

### Corrections

- Bug pagination des unités d'archives des transferts dans la vue détaillée #1274
- Traductions manquantes dans les vues pdf et détaillées en SEDA 2.1 #1331
- Oaipmh : retrait du filename dans le header #1335
- Suppression des liens de téléchargement des fichiers dans la description publique du catalogue #1339
