## \[2.2.6\] - 06/01/2025

### Corrections

- Absence du bouton de révocation d'un Super archiviste #2088
- Désactivation de la case à cocher Validation par signature électronique #2108
- Locks des transferts lors de la validation #2118
- Correction des inscriptions dans le cycle de vie lors de l'édition d'une unité d'archives #1725
- Upload possible si fichier existe en base, mais pas sur le volume #2026
- Correction des bugs sur le téléchargement de CSV #1884

### + modifications liées à la version 2.1.15
