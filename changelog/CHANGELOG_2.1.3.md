## \[2.1.3\] - 01/09/2021

### Évolutions

- Edition du service d'horodatage du service d'archives gestionnaire #1041
- Edition de l'admin technique #1000
- Désactivation possible des notifications d'exploitation #1000
- Mise à jour de la validation RNG Jing #1014
- Filtres supplémentaires sur les archives #1037
- Suppression de référence lors d'un changement de mot clé par traitement #1021

### Corrections

- Corrections export du journal #887
- Correction du cron Rétention des fichiers de transferts
- Correction analyse : apostrophes dans les mots-clés #1015
- Correction de sécurité #1002
- Correction liens vers fichiers dans transferts/archives #1008
- Correction suppression d'un circuit de validation utilisé par un accord de versement #1039
- Correction navigation en seda v0.2 #1026
- on ajoute seulement les formats listés dans le seda #1030
- Correction pagination dans analyse d'un transfert #1024
- Correction bug sur "&" dans archives et journal des événements #1012
- Correction du décalage de la mise en gras du texte dans le moteur de recherche #1006
- Correction perte de session et transfert par arbo #1025
- Evénement de téléchargement depuis la description des entrées #1047
- Visualisation d'une unité d'archives #996
- Correction ajout d'un webservice #1018
- Correction du ArchiveTransferReply en seda 2.1 #1016
- Réponses vide dans Restservices #1043
- Correction élimination des transferts sortants refusées #1048
