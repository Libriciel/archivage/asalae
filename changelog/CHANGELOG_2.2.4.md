## \[2.2.4\] - 24/09/2024

### Corrections

- Gestion des archives composites dans JobMaker #2003
- Les transferts à valider apparaissent toujours quand on change de tenant #1991
- Gestion des transferts simultanés sur une même archive composite #2006
- Blocage des workers par une interruption de service programmé dépassée #2014
- Étanchéité sur le contrôle de l'identifiant de l'unité d'archives de rattachement pour un transfert lié #2015
- Envoi des mails de notifications de transfert pour les super archivistes #2000

### + modifications liées à la version 2.1.13
