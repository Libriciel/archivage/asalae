## \[3.0.0\] - 06/01/2025

### Évolutions

- Dockerisation #1989
- Upgrade de Ubuntu, CakePHP 5, PHP 8.3 et Postgresql #1995

### Améliorations

- Ajout des notifications d'élimination dans les attestations d'éliminations #2105

### Corrections

- Héritage des rôles Super archiviste et Administrateur technique dans les rôles custom #2086
- Index dans le nom du fichier d'un transfert lié #2113
- Retrait des liens "mes transferts conformes" et "mes transferts non conformes" de l'index si l'utilisateur n'a pas les droits #2115
- Correction du retrait des favoris #2114
- Filigrane UA éliminées jolie vue SEDA 2.2 manquant  #2129
- Select étape de validation vide pour super archiviste #2131
- Correction filtrage par favoris #2116
- Perte des circuits de validations lors de la promotion en super archiviste #2135

### + modifications liées à la version 2.1.15
### + modifications liées à la version 2.2.6
