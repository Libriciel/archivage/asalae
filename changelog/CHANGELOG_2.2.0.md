## \[2.2.0\] - 17/02/2023

### Évolutions

- Ajout de l'export du registre des entrées en EAD 2002 #931
- Prise en charge du SEDA 2.2 #1210
- Intégration du nouveau logo #528
- Ajout de la colonne TransferringAgencyArchiveIdentifier dans le catalogue #1215
- Ajout de l'index Catalogue de mon service #781
- Gestion des transferts rejetés issus des webservices #1127
- Les tentatives d'intrusions via login/mdp sont inscrites dans le journal #999
- Moteur de recherche à résultats multiple #826
- Référent technique du service d'archives #1138
- Export des journaux d'événements par service d'archives #782
- Nouveau système de panier pour les communications #688
- Communications par lots #840
- Filtre rapide pour les favoris  #1520
- Archives composites #1139
- Archives techniques (ajout / modification / suppression) #549
