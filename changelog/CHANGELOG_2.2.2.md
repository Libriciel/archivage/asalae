## \[2.2.2\] - 08/07/2024

### Corrections

- Conversion suite à un transfert lié (archive composite) #1781
- Notification mails des super archivistes #1835
- Choix des rôles spéciaux dans le changement d'entité #1852

### + modifications liées à la version 2.1.12
