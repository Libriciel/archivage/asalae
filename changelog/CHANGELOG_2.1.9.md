## \[2.1.9\] - 27/10/2022

### Évolutions

- Gel/dégel des archives (NF461) #934
- Attestations d'archivage (NF461) #1433
- Attestations d'élimination (NF461) #1433
- Attestations de restitution (NF461) #1433
- Ajout d'un bouton de test de notification
- Amélioration de la tache planifiée JobMaker #1332
- Commande de modification des volumes verrouillés #1371

### Corrections

- Evénement de login depuis une connexion OpenId #1429
- Amélioration du cron d'ajout d'un volume #1406
- Optimisation du filtre de recherche par mot clés
- Unicité des noms des Systèmes d'archivage électronique au sein d'un même Service d'Archives #1436
- Echappement de certains caractères dans les XML #1439
- Correction de sécurité #1435
- Uniformisation des labels Identifiant et Nom d'utilisateur #1417
- Libreoffice v7 valide pour les conversions #1451
- Bug d'affichage du moteur de recherche #1454
- Optimisation de contrôle de transfert pour le SEDA 2.1 #1485
- Correction du réparateur de transfert #1502
