## \[2.0.4\] - 08/01/2021

### Evolutions

- Ajout de l'action parcourir dans les volumes
- Mise à jour en temps réel de l'état des Jobs (administration technique)

### Corrections

- Correction envoie d'email multiple #623
- Création des archives avec mots-clés vide #624
- Correction de la validation par lots par Service d'archives #638
- Upload de fichiers > 5Go sur volume S3 #639
- Correction des noms de fichier invalides dans les fichiers ZIP #635
- Meilleure détection des Programmes manquant dans System check #641
- Correction des liens dans le panier pour les unités d'archives filles #630 #642
- Modification de l'intégrité lors de la modification d'un fichier seda 0.2 #637
- Correction des permissions sur la visualisation dans le catalogue #626
- Correction des filtres de recherche à valeurs multiples #632
- Correction du remplissage auto du format dans l'éditeur de transferts #643
