## \[2.1.14\] - 01/10/2024

### Corrections

- seda 2.x - visualisation de la balise répétable Description des entrées #1622
- volume_manager repair : correction des doublons des références de stockage des fichiers réparés #2024
- asw sdk : désactivation des alertes de dépréciations de php 7.4 #2028
