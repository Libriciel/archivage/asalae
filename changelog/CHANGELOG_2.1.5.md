## \[2.1.5\] - 14/03/2022

### Évolutions

- Ajout de la sonde de check des jobs en échec #1071
- Ajout dans le cron check, les jobs et l'espace disque #1078
- Limitation du nombre de tentatives de connexion sur un même login #1209
- Définition du proxy dans l'installation par fichier ini #1146
- Filtre de recherche par référence de mots-clés dans le catalogue et les archives consultables #1145
- Ajout de la validation du transfert dans le cycle de vie de l'archive #1101
- Les noms d'unités d'archives ne peuvent plus être vide (transfert non conforme) #1222
- Envoie par morceaux possibles pour les transferts sortants #1223
- Ajout de la validation de la config dans le Check #1211
- Affichage de l'interruption de service en json pour les webservices #1235

### Corrections

- Droits sur les Transferts pour le rôle Référent Archiviste #1100
- Colonne description en double dans l'index des archives consultables #1110
- Erreur plus explicite en cas de champs date mal renseigné à la création d'un transfert #1112
- Unicité des identifiants de transferts par service d'archives #1119
- Filtres de dates sur les indicateurs #1166
- Sélection d'une entité parente obligatoire #1172
- Erreur sur certains traitements par lots sur les ArchiveUnit type Document #1120
- La tâche "Rétention des transferts acceptés" gère maintenant les archives éliminées, restituées ou transférées #1191
- Réduction du rythme de mise à jour de l'état d'un job pour éviter une saturation de Beanstalkd #1193
- Récupération de métadonnées de description d'unité d'archives en seda 2.1 pour la recherche #1201 #1111
- Récupération de métadonnées de DUA des sous-unités d'archives en seda 2.1 #1244
- Utilisation du proxy possible dans l'horodatage Universign #1060
- Code de l'algo pour les transferts en seda 1.0 avec un algo différent de celle de l'application #1199
- Meilleur marquage du texte recherché dans les moteurs de recherche #1088
- Transferts sortants : ajout du choix de niveau de service #1153
- Transferts par arbo avec des caractères spéciaux en seda 2.1 #1221
- Imports des bordereaux non UTF-8 #1225
- Identifiants absents de l'export PDF #1189
- Meilleure gestion d'erreurs sur les tâches planifiées #1224
- Mise à jour du tableau de résultats après une modification d'un transfert #1065
- Import de liste de mot clef avec des lignes vides #1205
- Suppression des communications ayant dépassé le délai de rétention #1151
- Bug sur communication pour une unité d'archive sans fichier #1183
- Réparation du bouton "test" sur les SAEs configurés #1218
- Ordre de passage des archives pour le contrôle d'intégrité #1213
- Meilleure gestion d'erreurs sur les SAEs mal configurés #1219
- Un utilisateur désactivé ne doit pas pouvoir se connecter #1234
- Erreur sur la connexion avec utilisateur non existant #1237
- Extensions autorisées par l'accord de versement insensibles à la casse #1231
- Accès à certains webservices à nouveau fonctionnels #1236
- DestructionWorker peut reprendre suite à un d'échec #1241
- Filtre de recherche sur le sort final dans le registre des entrées #1245
- Téléchargement de fichiers avec certains caractères spéciaux corrigé #1249
