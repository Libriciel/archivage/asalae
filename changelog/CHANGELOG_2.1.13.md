## \[2.1.13\] - 24/09/2024

### Évolutions

- Ajout d'un contrôle de cohérence des unités d'archives à éliminer/conserver #1496

### Corrections

- Persistance de la session lors d'une connexion OpenID #2002
- Retour en étape 2 suite à un upload dans l'édition d'un transfert #1987
- Échappement des caractères de contrôle dans les XML #1998
- Validation de transfert impossible après connexion OpenID #2001
- Correction de l'affichage des erreurs #2010
- Compatibilité Siegfried 1.11.1 sur RedHat #1885
- Doublons dans les validations de demandes #1807
- Ajout des "ArchiveUnit.Content.Event" dans le PDF de l'archive #1791
- Nombre et taille des fichiers de management après une élimination #1500
- Élimination d'une archive avec plus de 65535 unités d'archives #2007
- Correction téléchargement des ZIPs volumineux #1900
