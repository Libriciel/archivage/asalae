## \[2.0.8\] - 02/06/2021

### Évolutions

- Compteurs capables de trouver le prochain identifiant disponible #915 #916
- Contrôle d'intégrité des fichiers de management des archives #926
- Désactivation possible des entités #962
- Plusieurs tentatives lors du check des volumes #953

### Corrections

- Correctif de sécurité #935
- Correction affichage historique validation vue détaillée des transferts #846
- Correction upload possible de fichiers vide #956
- Resynchronisation du disk_usage #928
- Une archive ne peut plus être sans fichier #963
