## \[2.0.1\] - 06/10/2020

### Évolutions

- Groupement par Service Organisationnel pour l'import des utilisateurs LDAP #462
- Import d'utilisateur Ldap depuis la console technique #460
- Filtres pour les erreurs de transfert #481
- Retour du cron probe_shell plus lisible #482
- Ajout d'une commande shell pour déverrouiller un cron
- Retrait du volume OpenStackSwift
- Changement de licence (AGPL V3) #522
- Meilleure gestion du LDAP #556
- Vérification de la présence de JAVA #559

### Corrections

- Modification de l'affichage de l'icône de suppression d'un web service #458
- Modification des options de rôles dans l'import des utilisateurs LDAP #462 #464
- Correction des boutons de test d'un LDAP sur certains LDAPs #465 #480
- Correction de l'affichage du contenu du panier #461
- Correction sur les résultats de recherche dans le catalogue #468 #475
- Correction memory limit dans édition d'un transfert #469
- Correction action groupée pour fichiers de transfert #473
- Correction pagination sur l'import d'utilisateur ldap #483
- Contrôle d'intégrité des fichiers des archives techniques #485
- Champ Region configurable sur Volume Minio #487
- Redirection des exceptions des crons dans les logs #489
- Amélioration de la détection d'algorithmes de hachage dans un transfert #362
- Correction de l'affichage lors de filtrages consécutifs en modal #488
- Correction de la détection de l'intégrité du seda 0.2 #490
- Correction de la suppression des cookies (php >= 7.3)
- Correction de l'action "vider le panier"
- Les connexions de webservices ne sont plus loggés #512 
- Amélioration et correction de la validation des fichiers
- Correction du parseur de dates
- Modification de la taille max des communications en base #552
- Modification de la taille max des noms de fichiers en base #554
- Amélioration et correction de la gestion des LDAPs #556
- Unicité des utilisateurs du LDAP #477
- Ajout des services d'archives dans les filtres d'indicateurs de volumétrie
- Correction du cache du zip d'archives #557
