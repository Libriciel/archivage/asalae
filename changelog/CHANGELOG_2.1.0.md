## \[2.1.0\] - 23/04/2021

### Nouveautés

- Prise en charge du SEDA v2.1
- Ajout du module "Archive consultables"
- Transfert en SEDA 2.1 par formulaire #550
- Nouvelle page de connexion #525
- Améliorations sur le moteur de recherche #520
- Commande shell de check #573
- Filtrage des IPs dans l'administration technique #547
- Authentification possible via Keycloak #546
- Téléchargement des fichiers SEDA en PDF #13
- Conversions des fichiers #73
- Utilisateurs webservice dans la liste des utilisateurs #541
- Traitement par lots #7

### Améliorations

- Email optionnel pour les administrateurs techniques #478
- Optimisation du moteur de recherche #454
- Options lié à la validation pour l'utilisateur #450
- Filtres de recherche au sein de la page #471
- Recherche insensible à la casse et aux accents dans l'éditeur de transferts #502
- Ajout de la colonne "Hérite des droits" dans les rôles spécifiques
- Ajout de nombreux filtres de recherches #507
- Amélioration de la gestion des LDAPs #510
- Retrait de l'affichage des /dev/loop* dans Etat des disques #192
- Ajout du service d'archives dans la liste des sessions actives #553
- Ajout de l'état création pour les unités d'archives #495
- Modification des informations du cycle de vie PREMIS #523
- Catalogue : accès aux fichiers des archives librement communicables #540
- Installation en ligne de commande #544
- Ajout des actions de l'administration technique dans le journal des événements #548
- Indicateurs : ajout des volumes pour les fichiers de conservation et de diffusion des archives #715
- Indicateurs : export csv #716
- Seuls les validateurs peuvent modifier en transfert envoyé #776
- Un rôle doit être rattaché à un type d'entité au moins #939
- Un rôle doit être rattaché à au moins un type d'entité #939

### Autre

- Suppression de l'option de compteur obsolète "RelativeIdentifier" #601
- Retrait de "Recherche sur les entrées" #773
