## \[2.1.10\] - 08/02/2022

### Évolutions

- Ajout du référentiel des règles de gestion pour le SEDA V2.x #1486
- Vue détaillée des archives gelées bloquant une demande d'élimination, restitution, transfert sortant #1461
- Ajout d'un événement lors du contrôle de présence des fichiers d'archives #1545
- Suppression des fichiers des transferts acceptés après le délai de rétention #1608
- Ajout d'un Check de la connexion au websocket (ratchet) #1623
- ajout des identifiants des demandes de communication pour les communications #1547

### Corrections

- Échappement des $ dans les commandes #1507
- Problème de persistance des données du formulaire d'ajout de webservice en cas d'erreur #1584
- Problèmes sur les transferts en seda 2.1 sans Attachment #1536
- Problème de lien dans le mail de téléchargement des zips trop lourds #1616
- Métriques de l'entité de l'utilisateur connecté à la place du service d'archives sur le tableau de bord #1543
- Événement de contrôle d'intégrité d'une archive technique #1544
- Finalisation des transactions des conversions par lots lorsqu'il n'y a rien à convertir #1617
- Erreur d'affichage des transfers à valider sur la page d'accueil #1530
- Contrôle de l'intégrité des fichiers de transfert en seda 2.1 #1531
- Meilleur calcul de l'espace disque occupé sur les volumes #1546
- Retrait des points à la fin du nom des répertoires dans les zip #1595
- Il n'est plus possible de dé-assigner un service d'archive d'un ECS #1464
- La commande bin/cake check vérifie également les workers #1538
- Retrait du contournement en @test.fr #1603
- Il n'est plus possible de publier une liste de mots-clés avec doublons #1347
- Action manquante pour le renvoit de demande de transfert sortant #1508
- Elimination d'une archive avec fichier de taille 0 #1470
- Correction de l'ajout du logo du service d'archives #1516
- Envoi du data en multipart/form-data sur des methods != POST #1511
- api/users : autorisation pour le rôle ws administrateur #1510
- Suppression possible d'une étape d'un circuit alors qu'elle est en cours d'utilisation #1509
- Meilleurs gestion des transferts sortants larges #1525
- Correction de l'édition d'un volume S3 utilisé #1514
- Correction logout_redirection dans OpenIDConnect #1615
- Problème d'édition d'un transfert lors de la validation #1624
- Edition de la communicabilité d'une description d'unité d'archive en seda 2.1 #1619
- Problèmle d'édition du délais de communication description et keyword en SEDA 2.1 #1604
- Affichage de l'accéssibilité de l'unité d'archives plutôt que celle de l'archive dans le catalogue #1553
- SEDA 2.1 - modification du délais de communication non pris en compte #1604
- Correction de la suppression d'un élément dans le seda 2.1 #1673
- Permissions sur l'édition d'un transfert #1674
- Homogénéisation des icônes d'attestations #1607
