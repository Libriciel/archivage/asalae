## \[2.1.11\] - 27/10/2023

### Évolutions

- Amélioration de la vue détaillée des archives techniques #1691

### Corrections

- Ldap sans Service d'Archives après install par fichier.ini #1612
- Codes seda v2 non présents en traitement par lot #1688
- Erreur de suppresion horodatage #1693
- Ajout des notices EAC-CPF des entités ajoutées avec install config #1738
- Correction création des archives des transferts SEDA 2.x comportant des ArchiveUnitRefId #1484
- Transferts sortants : retour à l'état disponible des archives rejetées par la SAE destinataire #1761
- Journal des événements : ajout des événements des transferts sortants et des restitutions #1759
- Bug lors de la suppression du cron de modification des volumes #1523
- Présence obligatoire de la séquence dans un compteur #1752
- Contrôle et suppression des références suite à une suppression d'un BinaryDataObject #1700
- Déplacement du org-entity-data dans /data/config #605
- Correction de l'option shred pour FilesystemUtility #1765
- Refonte du cron de contrôle d'intégrité #1827
- Corrections des versions de travail des mots clés #1829
- Inscription des requêtes OAI-PMH dans le journal des événements #1830
- Problèmes liées à l'ajout d'Espaces de Conservation Sécurisés #1833
