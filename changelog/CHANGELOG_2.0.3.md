## \[2.0.3\] - 02/12/2020

### Evolutions

- Ajout des dossiers inscriptibles dans le system check #608

### Corrections

- Correction validation en lot par signature #593
- Correction bug sur nom de fichier trop long #595
- Correction webservice sedaMessage Get transfert rejeté #596
- Correction affichage de l'email du validateur
- Correction de la sauvegarde des filtres de recherche #594
- Demande de destruction possible après un refus #591
- Correction du cycle de vie d'une archive non terminée #611
- Gestion des volumes inactifs #615 #616
- Correction du tri (pagination ajax) #617
