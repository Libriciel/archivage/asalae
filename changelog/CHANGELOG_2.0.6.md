## \[2.0.6\] - 02/03/2021

### Evolutions

- Check des volumes asynchrone
- Alerte en cas d'espace disque faible #732

### Corrections

- Correction niveau de service si ECS non sélectionné #696
- Compatibilité css pour Chrome v88 #697
- Vérification du taux d'occupation des volumes #739
- Cron Check iso avec la vérification dans l'administration technique #719
- Correction des messages Acceptance et Reply pour du SEDA 0.2 #722
- Optimisation du ZIP d'archives et de transferts #728
- Correction casse des champs LDAP #738
