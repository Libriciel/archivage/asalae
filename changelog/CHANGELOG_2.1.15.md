## \[2.1.15\] - 06/01/2025

### Évolutions

- Affichage des logs de conversions dans les logs du worker #2076
- Dossier temporaire de téléchargement par défaut dans /data/tmp #2032

### Corrections

- Édition du service producteur sur une archive change également le "Name" #1953
- Vérification des tâches planifiées dans la commande check #2128
- Correction des pertes de configuration des tableaux de résultats #1669 
