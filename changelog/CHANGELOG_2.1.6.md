## \[2.1.6\] - 25/04/2022

### Évolutions

- Vérification des lft/rght stockés en base de données #1279

### Corrections

- Suppression de la colonne "Peut visualiser les éléments des entités filles" pour les webservices (inutilisée) #1250
- Bug bloquant sur l'édition du service d'archives #1252
- Échappement des caractères spéciaux dans les identifiants pour l'OAI-PMH #1256
- Retrait du lien Indicateurs pour les entités non concernées #1268
- Suppression des séparateurs de milliers dans l'export csv des indicateurs #1259
- Contrôle de l'accord de versement sur un document sans format ou type MIME #1266
- Retrait de la valeur vide dans les formats d'un accord de versement #1265
- Affichage de la valeur issue d'un AR062 #1282
- Ajout de ArchivalAgencyArchiveUnitIdentifier dans l'export PDF en seda 2.1 #1272
