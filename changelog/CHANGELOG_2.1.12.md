## \[2.1.12\] - 08/07/2024

### Évolutions

- Affichage des unités d'archives éliminées dans "Afficher la description" #1413
- CustodialHistoryItem est désormais présenté sous forme de bloc de texte #1322
- La modification d'un mot de passe de l'utilisateur est inscrite dans le journal NF461 #1713
- Déplacement des fichiers temporaires pour l'OAI-PMH afin de permettre leur suppression par le ramasse-miettes en cas d'erreur #1646
- Vérification de la date de dernière exécution des tâches planifiées #1863
- Retrait du rôle ws administrateur #1972

### Corrections

- Demandes de transferts sortants : sélection par services producteurs des unités d'archives #1929
- Demandes de transferts sortants : calcul du nombre d'unités d'archives des demandes #1929
- Demandes de transferts sortants : liste des unités d'archives limitées par la pagination #1876
- Éliminations partielles : correction du path des unités d'archives éliminées dans le fichier de management deleted.json #1930
- Correction des intitulés des règles en SEDA 2.1+ #1287
- Blocage de la validation d'un transfert après son édition s'il n'a pas été réanalysé #1782 #1903
- Correction enregistrement sur volume S3 #1918
- Prise en compte de la config Transfers.elementsPerPage #1944
- Bouton Télécharger le transfert dans Réparer un transfert #1974
- Référence vers un BinaryDataObject dans un DataObjectGroup en SEDA v2.x #1945
- Transferts sortants sans les éléments éliminés #1948
- Blocage de retrait d'un volume s'il manque des fichiers #1955
- Nettoyage des fichiers temporaires lors de l'envoi d'un transfert #1961
- Optimisation registre des entrées et visualisation des archives #1975
- Correction de la gestion des versions de travail des mots clés #1872
- Correction affichage de la corbeille dans les niveaux de service #1779
- Les filtres saisis avant la sélection du circuit de validation sont repris après sélection #1947
- Correction calcul de la taille d'une restitution #1802
- Suppression d'un transfert impossible lorsqu'il est en cours de validation #1977
- Exception détaillée pour l'OaiPMH #1705
- Présence d'archives gelées dans les demandes de transferts sortants #1985
