<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         3.0.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use AsalaeCore\Error\ContextDebugger;
use AsalaeCore\Utility\DOMUtility;
use Cake\Core\Configure;
use Cake\I18n\Date;
use Cake\I18n\DateTime;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;

/**
 * Additional bootstrapping and configuration for CLI environments should
 * be put here.
 */

// Set logs to different files so they don't have permission conflicts.
Configure::write('Log.debug.file', 'cli-debug');
Configure::write('Log.error.file', 'cli-error');

class_alias(AsalaeCore\ORM\TableRegistry::class, 'T');
class_alias(AsalaeCore\MinkSuite\MinkBeanstalk::class, 'MinkBeanstalk');
class_alias(Asalae\MinkSuite\MinkArchive::class, 'MinkArchive');
class_alias(Asalae\Model\Table\ConfigurationsTable::class, 'ConfigurationsTable');
class_alias(Cake\Datasource\EntityInterface::class, 'EntityInterface');
class_alias(Cake\Core\Configure::class, 'Configure');
class_alias(AsalaeCore\Utility\Config::class, 'Config');
class_alias(Cake\ORM\Entity::class, 'Entity');
class_alias(Cake\ORM\TableRegistry::class, 'TableRegistry');
class_alias(Cake\Datasource\ConnectionManager::class, 'ConnectionManager');
class_alias(DOMUtility::class, 'DOMUtility');
class_alias(DateTime::class, 'Time');
class_alias(Date::class, 'Date');
class_alias(ContextDebugger::class, 'ContextDebugger');
class_alias(IdentifierExpression::class, 'IdentifierExpression');
class_alias(QueryExpression::class, 'QueryExpression');
