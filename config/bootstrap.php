<?php
/** @noinspection PhpInternalEntityUsedInspection Formatters pour le debug */
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.8
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

/*
 * Configure paths required to find CakePHP + general filepath constants
 */
require __DIR__ . '/paths.php';
require __DIR__ . '/constants.php';

/**
 * Ajoute/remplace des fonctions basics de Cakephp
 */
if (is_file(ASALAE_CORE . DS . 'basics.php')) {
    require ASALAE_CORE . DS . 'basics.php';
}
if (is_file(APP . 'basics.php')) {
    require APP . 'basics.php';
}

/*
 * Bootstrap CakePHP.
 *
 * Does the various bits of setup that CakePHP needs to do.
 * This includes:
 *
 * - Registering the CakePHP autoloader.
 * - Setting the default application paths.
 */
require CORE_PATH . 'config' . DS . 'bootstrap.php';

use AsalaeCore\Database\Type\DateTimeFractionalType;
use AsalaeCore\Database\Type\DateTimeType;
use AsalaeCore\Database\Type\DateType;
use AsalaeCore\Error\Debug\HtmlFormatter;
use AsalaeCore\Error\ErrorTrap;
use AsalaeCore\Error\ExceptionTrap;
use AsalaeCore\Factory;
use AsalaeCore\ORM\Locator\TableLocator;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Validation\RulesProvider;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Database\TypeFactory;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\FactoryLocator;
use Cake\Error\Debug\ConsoleFormatter;
use Cake\Error\Debug\TextFormatter;
use Cake\Error\Debugger;
use Cake\Log\Log;
use Cake\Http\ServerRequest;
use Cake\Mailer\Mailer;
use Cake\Mailer\TransportFactory;
use Cake\Routing\Router;
use Cake\Utility\Security;
use Cake\Validation\Validator;
use Detection\MobileDetect;

/**
 * Load global functions.
 */
require_once CAKE . 'functions.php';

/*
 * See https://github.com/josegonzalez/php-dotenv for API details.
 *
 * Uncomment block of code below if you want to use `.env` file during development.
 * You should copy `config/.env.example` to `config/.env` and set/modify the
 * variables as required.
 *
 * The purpose of the .env file is to emulate the presence of the environment
 * variables like they would be present in production.
 *
 * If you use .env files, be careful to not commit them to source control to avoid
 * security risks. See https://github.com/josegonzalez/php-dotenv#general-security-information
 * for more information for recommended practices.
*/
// if (!env('APP_NAME') && file_exists(CONFIG . '.env')) {
//     $dotenv = new \josegonzalez\Dotenv\Loader([CONFIG . '.env']);
//     $dotenv->parse()
//         ->putenv()
//         ->toEnv()
//         ->toServer();
// }

/*
 * Read configuration file and inject configuration into various
 * CakePHP classes.
 *
 * By default there is only one configuration file. It is often a good
 * idea to create multiple configuration files, and separate the configuration
 * that changes from configuration that does not. This makes deployment simpler.
 */
try {
    Configure::config('default', new PhpConfig());
    Configure::load('app_default', 'default', false);
} catch (\Exception $e) {
    exit($e->getMessage() . "\n");
}

/*
 * Load an environment local configuration file to provide overrides to your configuration.
 * Notice: For security reasons app_local.php **should not** be included in your git repo.
 */
$pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
$write = false;
if (is_readable($pathToLocalConfig)) {
    $json = require $pathToLocalConfig;
} elseif (is_readable('/data/config/app_local.json')) {
    $json = '/data/config/app_local.json';
    $write = is_writable(dirname($pathToLocalConfig));
} elseif (is_readable(CONFIG . 'app_local.json')) {
    $json = CONFIG . 'app_local.json';
    $write = is_writable(dirname($pathToLocalConfig));
}
if ($write) {
    $escaped = addcslashes($json, "'");
    file_put_contents($pathToLocalConfig, "<?php return '$escaped';?>");
}
if (!empty($json) && is_file($json)) {
    $local = json_decode(file_get_contents($json), true);
    $configs = [
        Configure::read(),
        $local
    ];
    $workdir = getcwd();
    chdir(__DIR__);
    foreach ($local['Config']['files'] ?? [] as $path) {
        if (!is_file($path)) {
            throw new Exception("Config file not found");
        }
        $configs[] = require $path;
    }
    $merged = call_user_func_array(["Cake\Utility\Hash", "merge"], $configs);
    Configure::write($merged);
    chdir($workdir);

    // met le dossier temporaire de download dans /data/tmp par défaut (au lieu du TMP de l'application)
    if (!empty($local['App']['paths']['data']) && empty($local['App']['paths']['download'])) {
        Configure::write('App.paths.download', $local['App']['paths']['data'] . '/tmp/download');
    }
}
if (empty(Configure::read('App.paths.administrators_json'))) {
    Configure::write(
        'App.paths.administrators_json',
        rtrim(Configure::read('App.paths.data'), DS) . DS . 'administrateurs.json'
    );
}

/*
 * When debug = true the metadata cache should only last
 * for a short time.
 */
if (Configure::read('debug')) {
    Configure::write('Cache._cake_model_.duration', '+2 minutes');
    Configure::write('Cache._cake_core_.duration', '+2 minutes');
}

/*
 * Set the default server timezone. Using UTC makes time calculations / conversions easier.
 * Check https://php.net/manual/en/timezones.php for list of valid timezone strings.
 */
date_default_timezone_set(Configure::read('App.defaultTimezone'));

/*
 * Configure the mbstring extension to use the correct encoding.
 */
mb_internal_encoding(Configure::read('App.encoding'));

/*
 * Set the default locale. This controls how dates, number and currency is
 * formatted and sets the default language to use for translations.
 */
ini_set('intl.default_locale', Configure::read('App.defaultLocale'));

/*
 * Register application error and exception handlers.
 */
(new ErrorTrap(Configure::read('Error')))->register();
(new ExceptionTrap(Configure::read('Error')))->register();

/*
 * Include the CLI bootstrap overrides.
 */
if (PHP_SAPI === 'cli') {
    require CONFIG . 'bootstrap_cli.php';
}

/*
 * Set the full base URL.
 * This URL is used as the base of all absolute links.
 */
$fullBaseUrl = Configure::read('App.fullBaseUrl');
if (!$fullBaseUrl) {
    /*
     * When using proxies or load balancers, SSL/TLS connections might
     * get terminated before reaching the server. If you trust the proxy,
     * you can enable `$trustProxy` to rely on the `X-Forwarded-Proto`
     * header to determine whether to generate URLs using `https`.
     *
     * See also https://book.cakephp.org/5/en/controllers/request-response.html#trusting-proxy-headers
     */
    $trustProxy = false;

    $s = null;
    if (env('HTTPS') || ($trustProxy && env('HTTP_X_FORWARDED_PROTO') === 'https')) {
        $s = 's';
    }

    $httpHost = env('HTTP_HOST');
    if ($httpHost) {
        $fullBaseUrl = 'http' . $s . '://' . $httpHost;
    }
    unset($httpHost, $s);
}
if ($fullBaseUrl) {
    Router::fullBaseUrl($fullBaseUrl);
}
unset($fullBaseUrl);

$cacheDisabled = Configure::consume('Cache_disabled');
Cache::setConfig(Configure::consume('Cache'));
if ($cacheDisabled === true || ($cacheDisabled === 'on_debug' && Configure::read('debug'))) {
    Cache::disable();
}
ConnectionManager::setConfig(Configure::consume('Datasources'));
TransportFactory::setConfig(Configure::consume('EmailTransport'));
Mailer::setConfig(Configure::consume('Email'));
$logPath = Configure::read('App.paths.logs');
if ($logPath[-1] !== DS) {
    $logPath = $logPath.DS;
    Configure::write('App.paths.logs', $logPath);
}
$configLog = Configure::consume('Log');
$configLog['debug']['path'] = $logPath;
$configLog['error']['path'] = $logPath;
Log::setConfig($configLog);
Security::setSalt(Configure::consume('Security.salt'));

/*
 * Setup detectors for mobile and tablet.
 * If you don't use these checks you can safely remove this code
 * and the mobiledetect package from composer.json.
 */
ServerRequest::addDetector('mobile', function () {
    $detector = new MobileDetect();

    return $detector->isMobile();
});
ServerRequest::addDetector('tablet', function () {
    $detector = new MobileDetect();

    return $detector->isTablet();
});

/*
 * You can enable default locale format parsing by adding calls
 * to `useLocaleParser()`. This enables the automatic conversion of
 * locale specific date formats. For details see
 * @link http://book.cakephp.org/3.0/en/core-libraries/internationalization-and-localization.html#parsing-localized-datetime-data
 */
TypeFactory::set('datetime', new DateTimeType);
TypeFactory::set('timestamp', new DateTimeType);
TypeFactory::set('datetimefractional', new DateTimeFractionalType);
TypeFactory::set('timestampfractional', new DateTimeFractionalType);
TypeFactory::set('date', new DateType);
$dateTypes = ['time', 'date', 'datetime', 'timestamp', 'datetimefractional', 'timestampfractional'];
foreach ($dateTypes as $name) {
    /** @var DateTimeType $timer */
    $timer = TypeFactory::build($name);
    $timer->useLocaleParser();
    if (method_exists($timer, 'setDatabaseTimezone')) {
        $timer->setDatabaseTimezone(Configure::read('App.timezone'));
    }
}

Factory\Utility::addNamespace('Beanstalk\Utility');
Factory\Utility::addNamespace('Libriciel\Filesystem\Utility');

putenv('XDG_CONFIG_HOME='.(getenv('XDG_CONFIG_HOME') ?: TMP.'.config'));

/**
 * Validation custom
 */
Validator::addDefaultProvider('default', new RulesProvider);

/**
 * Affichage custom du debug
 */
if (ConsoleFormatter::environmentMatches()) {
    $class = ConsoleFormatter::class;
} elseif (HtmlFormatter::environmentMatches()) {
    $class = HtmlFormatter::class;
} else {
    $class = TextFormatter::class;
}
Debugger::getInstance()->setConfig('exportFormatter', $class);

FactoryLocator::add('Table', new TableLocator);

/**
 * Notify passe de Ratchet à Mercure
 */
if (Configure::read('Mercure.enabled') && Configure::read('Mercure.jwt_secret')) {
    Notify::setMercure(Configure::read('Mercure.url'), Configure::read('Mercure.jwt_secret'));
}
