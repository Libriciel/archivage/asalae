<?php

use Asalae\Worker\AnalyseWorker;
use Asalae\Worker\ArchiveBinaryWorker;
use Asalae\Worker\ArchiveManagementWorker;
use Asalae\Worker\ArchiveUnitWorker;
use Asalae\Worker\ArchiveWorker;
use Asalae\Worker\BatchTreatmentWorker;
use Asalae\Worker\ControlWorker;
use Asalae\Worker\ConversionWorker;
use Asalae\Worker\DeliveryWorker;
use Asalae\Worker\DestructionWorker;
use Asalae\Worker\MailWorker;
use Asalae\Worker\OutgoingTransferWorker;
use Asalae\Worker\RestitutionBuildWorker;
use Asalae\Worker\TimestampWorker;
use AsalaeCore\Adapter\DbAcl;
use AsalaeCore\Auth\Identifier\UnknownUserException;
use AsalaeCore\Controller\Component\Reader\ImageReader;
use AsalaeCore\Controller\Component\Reader\OpenDocumentReader;
use AsalaeCore\Controller\Component\Reader\PdfReader;
use AsalaeCore\Driver\Timestamping\TimestampingLocal;
use AsalaeCore\Http\Session\DatabaseSession;
use AsalaeCore\Utility\Antivirus\Clamav;
use AsalaeCore\Worker\HashWorker;
use AsalaeCore\Worker\TestWorker;
use Cake\Cache\Engine\FileEngine;
use Cake\Database\Connection;
use Cake\Database\Driver\Postgres;
use Cake\Database\Driver\Sqlite;
use Cake\Log\Engine\FileLog;
use Cake\Mailer\Transport\MailTransport;
use Symfony\Component\HttpFoundation\File\UploadedFile;

// Force la locale pour les commandes shell
$encoding = env('APP_ENCODING', 'UTF-8');
$locale = env('APP_DEFAULT_LOCALE', 'fr_FR');
putenv('LC_ALL=' . $locale . '.' . $encoding);
setlocale(LC_ALL, $locale . '.' . $encoding);
if (!getenv('HOME')) {
    putenv('HOME=' . exec('realpath ~'));
}

return [
    /*
     * Debug Level:
     *
     * Production Mode:
     * false: No error messages, errors, or warnings shown.
     *
     * Development Mode:
     * true: Errors and warnings shown.
     */
    'debug' => false,

    /*
     * Configure basic information about the application.
     *
     * - namespace - The namespace to find app classes under.
     * - defaultLocale - The default locale for translation, formatting currencies and numbers, date and time.
     * - encoding - The encoding used for HTML + database connections.
     * - base - The base directory the app resides in. If false this
     *   will be auto-detected.
     * - dir - Name of app directory.
     * - webroot - The webroot directory.
     * - wwwRoot - The file path to webroot.
     * - baseUrl - To configure CakePHP to *not* use mod_rewrite and to
     *   use CakePHP pretty URLs, remove these .htaccess
     *   files:
     *      /.htaccess
     *      /webroot/.htaccess
     *   And uncomment the baseUrl key below.
     * - fullBaseUrl - A base URL to use for absolute links. When set to false (default)
     *   CakePHP generates required value based on `HTTP_HOST` environment variable.
     *   However, you can define it manually to optimize performance or if you
     *   are concerned about people manipulating the `Host` header.
     * - imageBaseUrl - Web path to the public images/ directory under webroot.
     * - cssBaseUrl - Web path to the public css/ directory under webroot.
     * - jsBaseUrl - Web path to the public js/ directory under webroot.
     * - paths - Configure paths for non class-based resources. Supports the
     *   `plugins`, `templates`, `locales` subkeys, which allow the definition of
     *   paths for plugins, view templates and locale files respectively.
     */
    'App' => [
        'name' => 'asalae',
        'version' => ASALAE_VERSION_LONG,
        'namespace' => 'Asalae',
        'encoding' => $encoding,
        'defaultLocale' => $locale,
        'base' => false,
        'defaultTimezone' => 'Europe/Paris',
        'timezone' => 'Europe/Paris', // legacy
        'dir' => 'src',
        'webroot' => 'webroot',
        'wwwRoot' => WWW_ROOT,
        // 'baseUrl' => env('SCRIPT_NAME'),
        'fullBaseUrl' => false,
        'imageBaseUrl' => 'img/',
        'cssBaseUrl' => 'css/',
        'jsBaseUrl' => 'js/',
        'paths' => [
            'plugins' => [ROOT . DS . 'plugins' . DS],
            'templates' => [
                ROOT . DS . 'templates' . DS,
                ROOT . DS . 'vendor' . DS . 'libriciel' . DS . 'asalae-core' . DS . 'templates' . DS,
            ],
            'locales' => [
                RESOURCES . 'locales' . DS,
                ROOT . DS . 'vendor' . DS . 'libriciel' . DS . 'cakephp-filesystem' . DS . 'src' . DS . 'Locale' . DS,
            ],
            'data' => ROOT . DS . 'data',
            'apis_rules' => RESOURCES . 'apis.json',
            'controllers_rules' => RESOURCES . 'controllers.json',
            'controllers_group' => RESOURCES . 'controllers_group.json',
            'path_to_local_config' => CONFIG . 'path_to_local.php',
            'logs' => LOGS,
            'download' => TMP . 'download',
        ],
    ],

    /*
     * Acls
     */
    'Acl' => [
        'database' => 'default',
        'classname' => DbAcl::class,
    ],

    /*
     * Security and encryption configuration
     *
     * - salt - A random string used in security hashing methods.
     *   The salt value is also used as the encryption key.
     *   You should treat it as extremely sensitive data.
     */
    'Security' => [
        'salt' => env('SECURITY_SALT', '__SALT__'),
    ],

    /*
     * Pagination
     * Attention, augmenter les valeurs peut avoir un impact négatif sur les
     * performances.
     *
     * - default: nombre de lignes d'un tableau de résultat (par défaut)
     * - options: choix possible du nombre de lignes
     */
    'Pagination' => [
        'default' => 20,
        'options' => '10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 150, 200',
    ],

    /*
     * Apply timestamps with the last modified time to static assets (js, css, images).
     * Will append a querystring parameter containing the time the file was modified.
     * This is useful for busting browser caches.
     *
     * Set to true to apply timestamps when debug is true. Set to 'force' to always
     * enable timestamping regardless of debug value.
     */
    'Asset' => [
        'timestamp' => 'force',
    ],

    /*
     * Valeurs possible:
     *      true - Désactive le cache
     *      false - Le cache est activé
     *      'on_debug' - Le cache est désactivé lorsque debug = true
     */
    'Cache_disabled' => false,

    /*
     * Configure the cache adapters.
     */
    'Cache' => [
        'default' => [
            'className' => FileEngine::class,
            'path' => CACHE,
            'url' => env('CACHE_DEFAULT_URL'),
        ],
        'lifecycle' => [
            'className' => 'File',
            'prefix' => 'lifecycle_',
            'path' => CACHE . 'lifecycle/',
            'serialize' => false,
            'duration' => '+10 minutes',
            'mask' => 0660,
        ],
        'login' => [
            'className' => 'File',
            'prefix' => 'login_',
            'path' => CACHE . 'login/',
            'serialize' => true,
            'duration' => '+60 minutes',
            'mask' => 0660,
        ],

        /*
         * Configure the cache used for general framework caching.
         * Translation cache files are stored with this configuration.
         * Duration will be set to '+2 minutes' in bootstrap.php when debug = true
         * If you set 'className' => 'Null' core cache will be disabled.
         */
        '_cake_core_' => [
            'className' => FileEngine::class,
            'prefix' => 'myapp_cake_core_',
            'path' => CACHE . 'persistent' . DS,
            'serialize' => true,
            'duration' => '+1 years',
            'url' => env('CACHE_CAKECORE_URL'),
        ],

        /*
         * Configure the cache for model and datasource caches. This cache
         * configuration is used to store schema descriptions, and table listings
         * in connections.
         * Duration will be set to '+2 minutes' in bootstrap.php when debug = true
         */
        '_cake_model_' => [
            'className' => FileEngine::class,
            'prefix' => 'myapp_cake_model_',
            'path' => CACHE . 'models' . DS,
            'serialize' => true,
            'duration' => '+1 years',
            'url' => env('CACHE_CAKEMODEL_URL'),
        ],
    ],

    /*
     * Configure the Error and Exception handlers used by your application.
     *
     * By default errors are displayed using Debugger, when debug is true and logged
     * by Cake\Log\Log when debug is false.
     *
     * In CLI environments exceptions will be printed to stderr with a backtrace.
     * In web environments an HTML page will be displayed for the exception.
     * With debug true, framework errors like Missing Controller will be displayed.
     * When debug is false, framework errors will be coerced into generic HTTP errors.
     *
     * Options:
     *
     * - `errorLevel` - int - The level of errors you are interested in capturing.
     * - `trace` - boolean - Whether backtraces should be included in
     *   logged errors/exceptions.
     * - `log` - boolean - Whether you want exceptions logged.
     * - `exceptionRenderer` - string - The class responsible for rendering uncaught exceptions.
     *   The chosen class will be used for both CLI and web environments. If you want different
     *   classes used in CLI and web environments you'll need to write that conditional logic as well.
     *   The conventional location for custom renderers is in `src/Error`. Your exception renderer needs to
     *   implement the `render()` method and return either a string or Http\Response.
     *   `errorRenderer` - string - The class responsible for rendering PHP errors. The selected
     *   class will be used for both web and CLI contexts. If you want different classes for each environment
     *   you'll need to write that conditional logic as well. Error renderers need to
     *   to implement the `Cake\Error\ErrorRendererInterface`.
     * - `skipLog` - array - List of exceptions to skip for logging. Exceptions that
     *   extend one of the listed exceptions will also be skipped for logging.
     *   E.g.:
     *   `'skipLog' => ['Cake\Http\Exception\NotFoundException', 'Cake\Http\Exception\UnauthorizedException']`
     * - `extraFatalErrorMemory` - int - The number of megabytes to increase the memory limit by
     *   when a fatal error is encountered. This allows
     *   breathing room to complete logging or error handling.
     * - `ignoredDeprecationPaths` - array - A list of glob-compatible file paths that deprecations
     *   should be ignored in. Use this to ignore deprecations for plugins or parts of
     *   your application that still emit deprecations.
     */
    'Error' => [
        'errorLevel' => E_ALL & ~ E_DEPRECATED,
        'errorController' => 'Asalae\Controller\ErrorController',
        'skipLog' => [],
        'log' => true,
        'trace' => true,
        'ignoredDeprecationPaths' => [
            'vendor/cakephp/acl/src/Model/Table/PermissionsTable.php',
//            'vendor/cakephp/cakephp/src/ORM/Table.php',
        ],
    ],

    /*
     * Debugger configuration
     *
     * Define development error values for Cake\Error\Debugger
     *
     * - `editor` Set the editor URL format you want to use.
     *   By default atom, emacs, macvim, phpstorm, sublime, textmate, and vscode are
     *   available. You can add additional editor link formats using
     *   `Debugger::addEditor()` during your application bootstrap.
     * - `outputMask` A mapping of `key` to `replacement` values that
     *   `Debugger` should replace in dumped data and logs generated by `Debugger`.
     */
    'Debugger' => [
        'editor' => 'phpstorm',
    ],

    /*
     * Email configuration.
     *
     * By defining transports separately from delivery profiles you can easily
     * re-use transport configuration across multiple profiles.
     *
     * You can specify multiple configurations for production, development and
     * testing.
     *
     * Each transport needs a `className`. Valid options are as follows:
     *
     *  Mail   - Send using PHP mail function
     *  Smtp   - Send using SMTP
     *  Debug  - Do not send the email, just return the result
     *
     * You can add custom transports (or override existing transports) by adding the
     * appropriate file to src/Mailer/Transport. Transports should be named
     * 'YourTransport.php', where 'Your' is the name of the transport.
     */
    'EmailTransport' => [
        'default' => [
            'className' => MailTransport::class,
            /*
             * The keys host, port, timeout, username, password, client and tls
             * are used in SMTP transports
             */
            'host' => 'localhost',
            'port' => 25,
            'timeout' => 30,
            /*
             * It is recommended to set these options through your environment or app_local.php
             */
            //'username' => null,
            //'password' => null,
            'client' => null,
            'tls' => false,
            'url' => env('EMAIL_TRANSPORT_DEFAULT_URL'),
        ],
    ],

    /*
     * Email delivery profiles
     *
     * Delivery profiles allow you to predefine various properties about email
     * messages from your application and give the settings a name. This saves
     * duplication across your application and makes maintenance and development
     * easier. Each profile accepts a number of keys. See `Cake\Mailer\Mailer`
     * for more information.
     */
    'Email' => [
        'default' => [
            'transport' => 'default',
            'from' => 'you@localhost',
            /*
             * Will by default be set to config value of App.encoding, if that exists otherwise to UTF-8.
             */
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
    ],

    /*
     * Connection information used by the ORM to connect
     * to your application's datastores.
     *
     * ### Notes
     * - Drivers include Mysql Postgres Sqlite Sqlserver
     *   See vendor\cakephp\cakephp\src\Database\Driver for the complete list
     * - Do not use periods in database name - it may lead to errors.
     *   See https://github.com/cakephp/cakephp/issues/6471 for details.
     * - 'encoding' is recommended to be set to full UTF-8 4-Byte support.
     *   E.g set it to 'utf8mb4' in MariaDB and MySQL and 'utf8' for any
     *   other RDBMS.
     */
    'Datasources' => [
        'default' => [
            'className' => Connection::class,
            'driver' => Postgres::class,
            'persistent' => false,
            'host' => 'asalae_db',
            /*
             * CakePHP will use the default DB port based on the driver selected
             * MySQL on MAMP uses port 8889, MAMP users will want to uncomment
             * the following line and set the port accordingly
             */
            //'port' => 'non_standard_port_number',
            'username' => 'asalae2',
            'password' => 'secret',
            'database' => 'asalae2',
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'flags' => [],
            'cacheMetadata' => true,
            'log' => false,

            /*
             * Set identifier quoting to true if you are using reserved words or
             * special characters in your table or column names. Enabling this
             * setting will result in queries built using the Query Builder having
             * identifiers quoted when creating SQL. It should be noted that this
             * decreases performance because each query needs to be traversed and
             * manipulated before being executed.
             */
            'quoteIdentifiers' => false,

            /*
             * During development, if using MySQL < 5.6, uncommenting the
             * following line could boost the speed at which schema metadata is
             * fetched from the database. It can also be set directly with the
             * mysql configuration directive 'innodb_stats_on_metadata = 0'
             * which is the recommended value in production environments
             */
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
        ],

        /*
         * The test connection is used during the test suite.
         */
        'test' => [
            'className' => Connection::class,
            'driver' => Sqlite::class,
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'non_standard_port_number',
            'username' => 'my_app',
            'password' => 'secret',
            'database' => ':memory:',
            'memory' => true,
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'cacheMetadata' => true,
            'quoteIdentifiers' => false,
            'log' => false,
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
        ],

        /*
         * Utilisé pour générer les fixtures de permissions
         * Sera supprimé à chaque execution de la commande 'bin/cake fixtures perms'
         */
        'fixtures_perms' => [
            'className' => Connection::class,
            'driver' => Sqlite::class,
            'database' => sys_get_temp_dir() . DS . 'fixtures.sqlite',
        ],
    ],

    /*
     * Configures logging options
     */
    'Log' => [
        'debug' => [
            'className' => FileLog::class,
            'path' => LOGS,
            'file' => 'debug',
            'url' => env('LOG_DEBUG_URL'),
            'scopes' => null,
            'levels' => ['notice', 'info', 'debug'],
        ],
        'error' => [
            'className' => FileLog::class,
            'path' => LOGS,
            'file' => 'error',
            'url' => env('LOG_ERROR_URL'),
            'scopes' => null,
            'levels' => ['warning', 'error', 'critical', 'alert', 'emergency'],
        ],
        // To enable this dedicated query log, you need to set your datasource's log flag to true
        'queries' => [
            'className' => FileLog::class,
            'path' => LOGS,
            'file' => 'queries',
            'url' => env('LOG_QUERIES_URL'),
            'scopes' => ['cake.database.queries'],
        ],
    ],

    /*
     * Session configuration.
     *
     * Contains an array of settings to use for session configuration. The
     * `defaults` key is used to define a default preset to use for sessions, any
     * settings declared here will override the settings of the default config.
     *
     * ## Options
     *
     * - `cookie` - The name of the cookie to use. Defaults to value set for `session.name` php.ini config.
     *    Avoid using `.` in cookie names, as PHP will drop sessions from cookies with `.` in the name.
     * - `cookiePath` - The url path for which session cookie is set. Maps to the
     *   `session.cookie_path` php.ini config. Defaults to base path of app.
     * - `timeout` - The time in minutes a session can be 'idle'. If no request is received in
     *    this duration, the session will be expired and rotated. Pass 0 to disable idle timeout checks.
     * - `defaults` - The default configuration set to use as a basis for your session.
     *    There are four built-in options: php, cake, cache, database.
     * - `handler` - Can be used to enable a custom session handler. Expects an
     *    array with at least the `engine` key, being the name of the Session engine
     *    class to use for managing the session. CakePHP bundles the `CacheSession`
     *    and `DatabaseSession` engines.
     * - `ini` - An associative array of additional 'session.*` ini values to set.
     *
     * Within the `ini` key, you will likely want to define:
     *
     * - `session.cookie_lifetime` - The number of seconds that cookies are valid for. This
     *    should be longer than `Session.timeout`.
     * - `session.gc_maxlifetime` - The number of seconds after which a session is considered 'garbage'
     *    that can be deleted by PHP's session cleanup behavior. This value should be greater than both
     *    `Sesssion.timeout` and `session.cookie_lifetime`.
     *
     * The built-in `defaults` options are:
     *
     * - 'php' - Uses settings defined in your php.ini.
     * - 'cake' - Saves session files in CakePHP's /tmp directory.
     * - 'database' - Uses CakePHP's database sessions.
     * - 'cache' - Use the Cache class to save sessions.
     *
     * To define a custom session handler, save it at src/Http/Session/<name>.php.
     * Make sure the class implements PHP's `SessionHandlerInterface` and set
     * Session.handler to <name>
     *
     * To use database sessions, load the SQL file located at config/schema/sessions.sql
     */
    'Session' => [
        'defaults' => 'database',
        'handler' => [
            'engine' => DatabaseSession::class,
            'model' => 'Sessions'
        ]
    ],

    /*
     * Algorithme de hash lors de l'ajout de nouveaux fichiers
     *
     * Algorithmes populaires :
     *
     * - crc32      Très rapide
     * - md5        Très connu
     * - sha1       Très connu
     * - sha256     Lourd
     * - sha512     Très lourd
     * - whirlpool  Ultra lourd (+ secure)
     */
    'hash_algo' => 'sha256',

    /*
     * Configuration du système d'upload
     *
     * @see https://github.com/flowjs/flow.js/blob/master/README.md#full-documentation
     *
     * - chunkSize : Taille de découpage des fichiers, permet la reprise de
     *      l'upload après pause (dernier chunk perdu)
     *      saisir une valeur <= 50% à UPLOAD_MAX_FILESIZE
     *      ou saisir une valeur <= à UPLOAD_MAX_FILESIZE si option forceChunkSize=true
     * - target : chemin vers lequel envoyer les chunks (ne pas modifier)
     * - simultaneousUploads : Nombre de téléchargements simultanés
     *      Influe sur la vitesse de téléchargement et sur la consommation en CPU
     *      Pour des vitesses de téléchargement optimales, augmenter la valeur jusqu'à
     *      obtenir 95%+ sur chaque coeur du processeur
     */
    'Flow' => [
        'chunkSize' => min(67108864, UploadedFile::getMaxFilesize() / 2),// max 64Mo
        'target' => '/upload',
        'simultaneousUploads' => 3,
        'maxChunkRetries' => 3,
        'chunkRetryInterval' => 10000,
    ],

    /*
     * Driver pour le traitement des images
     *
     * Disponible :
     * - gd     (sudo apt-get install php-gd)
     * - imagick (sudo apt-get install php-imagick)
     * - gmagick (sudo apt-get install php-gmagick) (obsolète)
     */
    'DriverImage' => 'gd',

    /*
     * Configuration des workers et jobs beanstalk
     *
     * - client_host : hôte utilisé par les clients beanstalk (application web, workers...)
     * - client_port : port utilisé par les clients beanstalk
     * - server_host : hôte utilisé par le serveur beanstalk, utiliser 0.0.0.0
     *                 pour ouvrir aux autres containers docker
     * - server_port : port utilisé par le serveur beanstalk
     * - worker_auto : lance automatiquement les workers (désactiver pour debug)
     */
    'Beanstalk' => [
        /*
         * Connection au serveur Beanstalkd
         */
        'client_host' => 'asalae_beanstalk',
        'client_port' => 11300,
        'server_host' => '0.0.0.0',
        'server_port' => 11300,
        'worker_auto' => true,

        /*
         * Nom du service systemd
         */
        'service' => false,

        /*
         * Chemin des classes de workers
         */
        'workerPaths' => [
            APP . 'Worker',
            ASALAE_CORE . DS . 'Worker',
        ],

        /*
         * Configurations liées aux tests des workers
         *
         * - timeout: durée maximum en secondes entre l'émission du test et la
         *            consultation du résultat
         */
        'tests' => [
            'timeout' => 10,
        ],

        /*
         * Définition des workers pour le manager
         *
         * - classname : Nom de classe à utiliser
         * - run : commande qui sera exécutée pour lancer le worker
         *      "bin/cake worker <nom_du_worker>" est le fonctionnement par défaut
         *      "docker run -d <nom_de_l_image_docker> <commande>" lancé par docker
         *      "ssh w02.asalae2.fr "/var/www/asalae2/bin worker <nom_du_worker>"" pour du ssh
         *
         * - kill : commande pour forcer l'arrêt d'un worker
         *      "kill {pid}" fonctionnement par défaut
         *      "docker kill {{hostname}}" pour du docker
         *      "ssh {{hostname}} "kill {{pid}}"" pour du ssh
         *
         * - quantity : définit le nombre de workers qui seront lancés avec cette configuration
         *
         * - active : permet d'activer/désactiver cette option
         *
         * Variables pour le kill:
         *
         *  {{hostname}} : nom de l'hôte sur lequel est lancé le worker / ID du container docker
         *  {{pid}} : identifiant unique du processus
         */
        'workers' => [
            'analyse' => [
                'classname' => AnalyseWorker::class,
                'run' => 'bin/cake worker analyse',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'archive' => [
                'classname' => ArchiveWorker::class,
                'run' => 'bin/cake worker archive',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'archive-binary' => [
                'classname' => ArchiveBinaryWorker::class,
                'run' => 'bin/cake worker archive-binary',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'archive-management' => [
                'classname' => ArchiveManagementWorker::class,
                'run' => 'bin/cake worker archive-management',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'archive-unit' => [
                'classname' => ArchiveUnitWorker::class,
                'run' => 'bin/cake worker archive-unit',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'batch-treatment' => [
                'classname' => BatchTreatmentWorker::class,
                'run' => 'bin/cake worker batch-treatment',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'control' => [
                'classname' => ControlWorker::class,
                'run' => 'bin/cake worker control',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'conversion' => [
                'classname' => ConversionWorker::class,
                'run' => 'bin/cake worker conversion',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'delivery' => [
                'classname' => DeliveryWorker::class,
                'run' => 'bin/cake worker delivery',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'destruction' => [
                'classname' => DestructionWorker::class,
                'run' => 'bin/cake worker destruction',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'hash' => [
                'classname' => HashWorker::class,
                'run' => 'bin/cake worker hash',
                'kill' => 'kill {{pid}}',
                'quantity' => 0,
                'active' => false,
            ],
            'mail' => [
                'classname' => MailWorker::class,
                'run' => 'bin/cake worker mail',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'outgoing-transfer' => [
                'classname' => OutgoingTransferWorker::class,
                'run' => 'bin/cake worker outgoing-transfer',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'restitution-build' => [
                'classname' => RestitutionBuildWorker::class,
                'run' => 'bin/cake worker restitution-build',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'test' => [
                'classname' => TestWorker::class,
                'run' => 'bin/cake worker test',
                'kill' => 'kill {{pid}}',
                'quantity' => 0,
                'active' => true,
            ],
            'timestamp' => [
                'classname' => TimestampWorker::class,
                'run' => 'bin/cake worker timestamp',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
        ],
    ],

    /*
     * Serveur de notifications
     *
     * NOTE : certains proxies client peuvent empêcher l'écoute sur port 8080
     * Pour des solutions altérnatives, allez voir le lien ci-dessous
     *
     * @see http://socketo.me/docs/deploy#serverconfiguration
     *
     * -------------------------------------------------------------------------
     * Pour une utilisation en ssl (Recommandé) :
     * -------------------------------------------------------------------------
     * sudo a2enmod proxy
     * sudo a2enmod proxy_wstunnel
     *
     * Modifier votre VirtualHost et ajoutez :
     * <Location "/wss">
     *      ProxyPass ws://localhost:8080/
     * </location>
     *
     * sudo service apache2 restart
     * -------------------------------------------------------------------------
     *
     * - ping : Permet de vérifier que le serveur tourne
     * - pong_delay : Temps de réponse du client à un ping en secondes
     * - check_timeout : Temps de réponse du navigateur à un ping en millisecondes
     * - connect : Adresse de consultation du WebSocket pour le client
     * - host : Sources autorisé (0.0.0.0 pour autoriser toutes les sources)
     * - listen_port : Port sur lequel écouter
     * - zmq : Client interne, il n'est pas recommendé de modifier cette valeur
     * - verbose : Permet de logger toutes les actions de ratchet
     */
    'Ratchet' => [
        // Client
        'ping' => 'http://localhost:8080',
        'pong_delay' => 1,
        'check_timeout' => 3000,
        'connect' => 'ws://asalae2.fr/ws',
        //'connect' => 'wss://asalae2.fr/wss',

        // Serveur
        'host' => '0.0.0.0',
        'listen_port' => 8080,
        'zmq' => [
            'protocol' => 'tcp',
            'domain' => '0.0.0.0',
            'service' => 'asalae_ratchet',
            'port' => 5555,
        ],
        'verbose' => false,
    ],

    /*
     * Réglages lié au password
     * Complexité minimale autorisée
     *
     * @see https://www.ssi.gouv.fr/administration/precautions-elementaires/calculer-la-force-dun-mot-de-passe/
     *
     * - PASSWORD_ULTRA_WEAK Mot de passe de 8 caractères dans un alphabet de 70 symboles
     *                       Taille usuelle
     *
     * - PASSWORD_WEAK       Mot de passe de 12 caractères dans un alphabet de 90 symboles
     *                       Taille minimale recommandée par l’ANSSI pour des
     *                       mots de passe ergonomiques ou utilisés de façon locale.
     *
     * - PASSWORD_MEDIUM     Mot de passe de 16 caractères dans un alphabet de 90 symboles
     *                       Taille recommandée par l’ANSSI pour des mots de passe plus sûrs.
     *
     * - PASSWORD_STRONG     Mot de passe de 20 caractères dans un alphabet de 90 symboles
     *                       Force équivalente à la plus petite taille de clé de
     *                       l’algorithme de chiffrement standard AES (128 bits).
     */
    'Password' => [
        'complexity' => PASSWORD_ULTRA_WEAK,
        'admin' => [
            'complexity' => PASSWORD_WEAK
        ],
    ],

    /*
     * Réglages de l'utilitaire du système de fichier
     *
     * - useShred : utilise ou pas shred, qui détruit sans possibilité de
     *              récupération les fichiers (suppression beaucoup plus longue)
     *
     * - tmpDir :   dossier temporaire pour les transactions fichiers
     */
    'FilesystemUtility' => [
        'useShred' => true,
    ],

    /*
     * Réglages de l'utilitaire de compression des fichiers
     *
     * - encodeFilename :
     *      Permet d'encoder le nom de fichier.
     *      Par défaut, l'encodage est UTF-8, compatible unix
     *      et windows > 8 (un patch existe pour windows 7 sur
     *      @link https://support.microsoft.com/en-us/hotfix/kbhotfix?kbnum=2704299&kbln=en-US )
     *
     *      Si le nom de fichier est mal encodé (selon le systeme),
     *      Des caractères étrange pourrons être visible à la place
     *      des accents.
     *
     *      valeurs conseillés :
     *      - Laisser à false pour une compatibilité tout systèmes récent
     *      - Utiliser la valeur IBM850 pour une compatibilité sur Windows <= 7
     *
     *      valeur déconseillé :
     *      - ASCII//TRANSLIT compatibilité maximale mais remplace les
     *      caractères spéciaux/accents par leurs equivalent : é => e, € => EUR
     */
    'DataCompressorUtility' => [
        'encodeFilename' => false,
    ],

    /*
     * Permet de renseigner des assets à utiliser dans l'application
     *
     * -login
     *      -logo-client : Uri du logo à afficher sur la page de login
     *                     ex: ROOT . DS . 'data' . DS . 'background.jpg'
     *      -background : Couleur (valeur css) du fond derrière le logo-client
     *                    ex : '#D7001A'
     *
     * -global-background : Sera affiché en haut à gauche sur les pages
     *                      Créé afin d'afficher un logo/emblème
     *                      ex: ROOT . DS . 'data' . DS . 'background.jpg'
     *
     * -global-background-position : attribut css background-position
     *                               '50px 50px'
     */
    'Assets' => [
        'login' => [
            'logo-client' => null,
            'background' => null,
        ],
        'global-background' => '',
        'global-background-position' => '',
    ],

    /*
     * Configurations pour les webservices
     *
     * -tokens-duration : Durée de vie des jetons, exprimé en interval
     *      -code : code de connexion à usage unique
     *      -access-token : Similaire à une session
     *      -refresh-token : Permet de renouveller l'access_token si celui-ci à expiré
     */
    'Webservices' => [
        'tokens-duration' => [
            'code' => '1 minute',
            'access-token' => '1 hour',
            'refresh-token' => '1 day'
        ]
    ],

    /*
     * Configuration pour les filtres de recherche
     */
    'Filters' => [
        'urls' => [
            'delete' => 'filters/ajax-delete-save',
            'overwrite' => 'filters/ajax-overwrite',
            'new' => 'filters/ajax-new-save',
            'load' => 'filters/ajax-get-filters',
        ]
    ],

    /*
     * Permet d'ajouter des fichiers de configuration supplémentaire
     * Sera fusionné avec un Hash::merge()
     */
    'Config' => [
        'files' => [],
    ],

    /*
     * Permet de désactiver, lorsque le debug est à true, certaines fonctionnalités
     * qui prennent du temps (utile pour le développement)
     */
    'Skip' => [
        'validation' => false,
    ],

    /*
     * Configuration de la validation
     *  -use_domdocument:
     *      Utilisation de la class DOMDocument de php pour effectuer la validation.
     *      Si cette valeur est à false, la commande shell "cmd" sera lancée
     *  -cmd:
     *      Commande à lancer suivi du fichier de validation et du fichier à valider
     *      Sera lancé uniquement si use_domdocument est à false
     *      ex: xmllint --noout --relaxng "/my/validator.rng" "/my/document.xml"
     */
    'Validation' => [
        'relaxng' => [
            'use_domdocument' => false,
//            'cmd' => 'xmllint --noout --relaxng',
//            'cmd' => 'java -jar "'.RESOURCES.'jing.jar"',
            'cmd' => 'java -Dfile.encoding=ISO-8859-1 -jar "'
                . ASALAE_CORE_INCLUDE_PATH . DS . 'src' . DS . 'Data' . DS . 'jing_fr.jar"',
        ],
        'schema' => [
            'use_domdocument' => true,
            'cmd' => 'xmllint --noout --schema'
        ]
    ],

    /*
     * Horodatage
     *
     * drivers : liste les systèmes d'horodatages
     *      name: Nom affiché dans l'application
     *      class: Classe PHP du driver (instance of TimestampingInterface)
     *      fields: Liste les champs paramétrables (l'ordre d'affichage a de l'importance)
     *          type: type de champ html (default: text)
     *          placeholder: indice sur le contenu
     *          help: message d'aide
     *          pattern: expression régulière de validation
     *          default: valeur par défaut
     *          display: affiche ou non le champ dans le paramétrage de l'application (default: true)
     *          read_config: La valeur sera lu ailleur dans la configuration (Chemin.vers.la.conf)
     */
    'Timestamping' => [
        'drivers' => [
            'OPENSIGN' => [
                'name' => 'OpenSign',
                'class' => 'AsalaeCore\Driver\Timestamping\TimestampingOpensign',
                'fields' => [
                    'url' => [
                        'type' => 'url',
                        'label' => 'Url',
                        'placeholder' => 'http://horodatage.local/opensign.wsdl',
                        'required' => 'required',
                    ],
                    'use_proxy' => [
                        'type' => 'checkbox',
                        'label' => 'Use proxy',
                    ],
                    'proxy_host' => [
                        'display' => false,
                        'read_config' => 'Proxy.host',
                    ],
                    'proxy_port' => [
                        'display' => false,
                        'read_config' => 'Proxy.port',
                    ],
                    'proxy_login' => [
                        'display' => false,
                        'read_config' => 'Proxy.login',
                    ],
                    'proxy_password' => [
                        'display' => false,
                        'read_config' => 'Proxy.password',
                        'type' => 'password',
                    ],
                ],
            ],
            'IAIK' => [
                'name' => 'IAIK',
                'class' => 'AsalaeCore\Driver\Timestamping\TimestampingIaik',
                'fields' => [
                    'url' => [
                        'type' => 'url',
                        'label' => 'Url',
                        'placeholder' => 'http://tsp.iaik.tugraz.at/tsp/TspRequest',
                        'required' => 'required',
                    ],
                    'timestamp_format' => [
                        'display' => false,
                        'default' => 'M d H:i:s.u Y T',
                    ],
                    'use_proxy' => [
                        'type' => 'checkbox',
                        'label' => 'Use proxy',
                    ],
                    'proxy_host' => [
                        'display' => false,
                        'read_config' => 'Proxy.host',
                    ],
                    'proxy_port' => [
                        'display' => false,
                        'read_config' => 'Proxy.port',
                    ],
                    'proxy_login' => [
                        'display' => false,
                        'read_config' => 'Proxy.login',
                    ],
                    'proxy_password' => [
                        'display' => false,
                        'read_config' => 'Proxy.password',
                        'type' => 'password',
                    ],
                ],
            ],
            'CRYPTOLOG' => [
                'name' => 'Universign',
                'class' => 'AsalaeCore\Driver\Timestamping\TimestampingGeneric',
                'fields' => [
                    'url' => [
                        'type' => 'url',
                        'label' => 'Url',
                        'required' => 'required',
                    ],
                    'timestamp_format' => [
                        'display' => false,
                        'default' => 'M d H:i:s.u Y T',
                    ],
                    'hash_algo' => [
                        'display' => false,
                        'default' => 'sha256',
                    ],
                    'login' => [
                        'label' => 'Identifiant de connexion au TSA',
                    ],
                    'password' => [
                        'label' => 'Mot de passe de connexion au TSA',
                        'type' => 'password',
                    ],
                    'useProxy' => [
                        'label' => 'Utiliser le proxy configuré (Configuration/Utilitaires)',
                        'type' => 'checkbox',
                    ],
                    'proxyHost' => [
                        'display' => false,
                        'read_config' => 'Proxy.host',
                    ],
                    'proxyPort' => [
                        'display' => false,
                        'read_config' => 'Proxy.port',
                    ],
                    'proxyLogin' => [
                        'display' => false,
                        'read_config' => 'Proxy.login',
                    ],
                    'proxyPassword' => [
                        'display' => false,
                        'read_config' => 'Proxy.password',
                        'type' => 'password',
                    ],
                ],
            ],
            'OPENSSL' => [
                'name' => 'Internal OpenSSL TSA',
                'class' => TimestampingLocal::class,
                'fields' => [
                    'ca' => [
                        'label' => '/path/to/root_ca.crt',
                        'required' => 'required',
                    ],
                    'crt' => [
                        'label' => '/path/to/ts.crt',
                        'required' => 'required',
                    ],
                    'pem' => [
                        'label' => '/path/to/ts_pkey.pem',
                        'required' => 'required',
                    ],
                    'cnf' => [
                        'label' => '/path/to/openssl_config.cnf',
                        'required' => 'required',
                    ],
                ]
            ],
        ],
    ],

    /*
     * Volumes
     *
     * drivers : liste les systèmes de volumes
     *      name: Nom affiché dans l'application
     *      class: Classe PHP du driver (instance of TimestampingInterface)
     *      fields: Liste les champs paramétrables (l'ordre d'affichage a de l'importance)
     *          type: type de champ html (default: text)
     *          placeholder: indice sur le contenu
     *          help: message d'aide
     *          pattern: expression régulière de validation
     *          default: valeur par défaut
     *          display: affiche ou non le champ dans le paramétrage de l'application (default: true)
     *          read_config: La valeur sera lu ailleur dans la configuration (Chemin.vers.la.conf)
     */
    'Volumes' => [
        'drivers' => [
            'FILESYSTEM' => [
                'name' => 'Filesystem',
                'class' => 'AsalaeCore\Driver\Volume\VolumeFilesystem',
                'fields' => [
                    'path' => [
                        'label' => 'Dossier',
                        'placeholder' => '/data/Volume01',
                        'required' => 'required',
                    ],
                ],
            ],
            'MINIO' => [
                'name' => 'S3 standard object storage',
                'class' => AsalaeCore\Driver\Volume\VolumeS3::class,
                'fields' => [
                    'endpoint' => [
                        'label' => "Url des webservices (endpoint)",
                        'placeholder' => 'http://127.0.0.1:9000',
                        'required' => 'required',
                        'type' => 'url',
                    ],
                    'region' => [
                        'label' => "Region",
                        'default' => 'EU',
                        'required' => 'required',
                    ],
                    'bucket' => [
                        'label' => "Nom de conteneur (Bucket)",
                        'placeholder' => 'volume01',
                        'required' => 'required',
                    ],
                    'credentials_key' => [
                        'label' => "Nom d'utilisateur (credentials key)",
                        'required' => 'required',
                    ],
                    'credentials_secret' => [
                        'label' => "Mot de passe (credentials secret)",
                        'type' => 'password',
                        'required' => 'required',
                    ],
                    'verify' => [
                        'placeholder' => "Optionnel",
                        'class' => 'verify-minio-cert',
                        'help' => "false - Désactive la vérification SSL<br>
                            /path/to/certificate.pem - Indique le chemin vers le certificat public (autosigné)",
                    ],
                    'use_proxy' => [
                        'type' => 'checkbox',
                        'label' => "Utiliser le proxy configuré ?",
                    ],
                    'proxy_host' => [
                        'display' => false,
                        'read_config' => 'Proxy.host',
                    ],
                    'proxy_port' => [
                        'display' => false,
                        'read_config' => 'Proxy.port',
                    ],
                    'proxy_login' => [
                        'display' => false,
                        'read_config' => 'Proxy.login',
                    ],
                    'proxy_password' => [
                        'display' => false,
                        'read_config' => 'Proxy.password',
                    ]
                ],
                'uniqueFields' => ['endpoint', 'bucket'],
                'toString' => ['%s/%s', 'endpoint', 'bucket']
            ],
        ],
    ],

    /*
     * Paramétrage du proxy
     */
    'Proxy' => [
        'host' => null,
        'port' => null,
        'login' => null,
        'password' => null,
    ],

    /*
     * Configuration de la page de login
     */
    'Libriciel' => [
        'login' => [
            'favicon' => [
                'filename' => 'favicon.ico',
                'url' => '/favicon.ico',
            ],
            'lib-login' => [
                'logo' => '/img/asalae-color.svg',
                'forgot-form-action' => '/users/forgotten-password',
            ],
            'lib-footer' => [
                'active' => 'asalae',
            ],
            'css' => [
                'as-bootstrap-4.css',
                '/libriciel-login/forkawesome/css/fork-awesome.css',
                'login.css',
            ],
            'js' => [
                'as-bootstrap-4.js',
                'asalae.login.js',
            ],
        ]
    ],

    /*
     * Antivirus
     */
    'Antivirus' => [
        'classname' => Clamav::class,
        'host' => 'asalae_clamav',
        'port' => 3310,
    ],

    /*
     * Permissions
     */
    'Permissions' => [
        'display_technical_name' => false,
    ],

    /*
     * Maintenance
     *
     * - enabled: activation immédiate de l'interruption
     * - scheduled:
     *      - begin: Date et heure du début de la prochaine interruption (ex: 2020-01-01T00:01:30)
     *      - end: Date et heure de fin de la prochaine interruption
     * - periodic:
     *      - begin: Heure de début de l'interruption journalière
     *      - end: Heure de fin de l'interruption journalière
     * - whitelist_file: Liste des urls accessibles lors de l'interruption (les urls d'administration)
     * - enable_whitelist: Permet aux requêtes possédant un entête précis de passer outre l'interruption
     * - whitelist_headers: Liste d'entêtes HTTP autorisées pendant une interruption de service
     * - message: Message destiné aux utilisateurs lors de l'interruption
     * - message_periodic: Message lors de l'interruption journalière
     */
    'Interruption' => [
        'enabled' => false,
        'scheduled' => [
            'begin' => null,
            'end' => null,
        ],
        'periodic' => [
            'begin' => null,
            'end' => null,
        ],
        'whitelist_file' => RESOURCES . 'interruption_whitelist.conf',
        'enable_whitelist' => true,
        'whitelist_headers' => [
            'X-Asalae-Webservice' => true,
        ],
        'message' => null,
        'message_periodic' => null,
        'enable_workers' => false,
    ],

    /*
     * - probe: affichage de l'état du serveur
     *      - loadavg: load average
     *      - mem: RAM utilisé / disponible
     */
    'Admins' => [
        'probe' => [
            'loadavg' => [
                'low' => 0.7, // vert
                'medium' => 1.2, // bleu
                'high' => 1.6, // jaune
                'max' => 5, // rouge
            ],
            'mem' => [
                'low' => 25, // vert
                'medium' => 50, // bleu
                'high' => 75, // jaune
                'max' => 100, // rouge
            ],
            'swap' => [
                'low' => 25, // vert
                'medium' => 50, // bleu
                'high' => 75, // jaune
                'max' => 100, // rouge
            ],
        ],
    ],

    /*
     * Liste les readers disponibles
     */
    'Readers' => [
        'pdf' => PdfReader::class,
        'open' => OpenDocumentReader::class,
        'image' => ImageReader::class,
    ],

    /*
     * Configuration OAI-PMH
     */
    'OAIPMH' => [
        'maxListSize' => 500, // items pagination
        'expiration' => 3600,  // seconds
    ],

    /*
     * Gestion des téléchargements
     *      - asyncFileCreationTimeLimit
     *          seconds, temps limite à partir duquel on propose l'envoit d'un
     *          mail au lieu de lancer le téléchargement (estimation)
     *      - tempFileConservationTimeLimit
     *          hours, temps de conservation des fichiers disponibles au téléchargement
     */
    'Downloads' => [
        'asyncFileCreationTimeLimit' => 30,
        'tempFileConservationTimeLimit' => 48,
    ],

    /*
     * Configuration commune entre les crons
     */
    'Crons' => [
        'emails' => [
            'min_interval' => '1 day'
        ]
    ],

    /*
     * Filtre les ips qui peuvent accéder à l'interface d'administration technique
     *
     * - enable_whitelist: active le filtrage des ips
     * - whitelist: liste des ips autorisés. Supporte le wildcard *
     *      ex:
     *          - 192.168.*.*
     *          - 10.0.0.*
     *          - fe80::*:*:*:*
     */
    'Ip' => [
        'enable_whitelist' => false,
        'whitelist' => [
            '127.0.0.1',
            '::1',
            '10.0.2.2',
        ],
    ],

    /*
     * Keycloak
     * - enabled: active l'authentification via keycloak
     * - realm: nom du royaume keycloak
     * - client_id: nom du client keycloak
     * - client_secret: secret du client
     * - clientParams: attributs supplémentaire pour le client HTTP. liste non exaustive:
     *     - ssl_verify_peer
     *     - ssl_verify_peer_name
     *     - ssl_verify_host
     *     - ssl_verify_depth
     *     - ssl_cafile
     *     - redirect
     * - base_url: url du keycloak
     * - username: champ keyclock utilisé pour l'identification de l'utilisateur
     */
    'Keycloak' => [
        'enabled' => false,
        'realm' => 'master',
        'client_id' => 'asalae',
        'base_url' => 'http://localhost:8080',
        'username' => 'preferred_username',
    ],

    /*
     * Pour connecteurs autre que keycloak (note: mais fonctionne avec keycloak)
     * - enabled: active l'authentification via openid-connect
     * - client_id: id du client
     * - client_secret: secret du client
     * - clientParams: attributs supplémentaire pour le client HTTP. liste non exaustive:
     *     - ssl_verify_peer
     *     - ssl_verify_peer_name
     *     - ssl_verify_host
     *     - ssl_verify_depth
     *     - ssl_cafile
     *     - redirect
     * - base_url: url du service (/.well-known/openid-configuration doit exister)
     * - username: champ utilisé pour l'identification de l'utilisateur
     * - redirect_uri: redirige sur cette url (doit correspondre à App.fullBaseUrl)
     *      si valeur auto => App.fullBaseUrl est utilisé
     * - scope: si besoin on peut fixer un scope
     * - refresh_expires_in: si besoin on peut fixer la durée de vie d'un refresh_token
     */
    'OpenIDConnect' => [
        'enabled' => false,
        'client_id' => 'asalae',
        'base_url' => 'http://localhost:8080/auth/realms/master',
        'username' => 'sub',
        'scope' => 'openid',
        'redirect_uri' => 'auto',
        'refresh_expires_in' => 3600,
    ],

    /*
     * Destiné au debugging, ne pas activer en cas d'utilisation normale.
     * /!\ Les requêtes peuvent contenir des informations sensibles
     * supprimer le dossier logs/debug_requests/ après usage
     *
     * - enabled : active le log des requests
     * - controllers : liste des controlleurs
     *      - liste des actions ou * pour toutes les actions
     */
    'log_requests' => [
        'enabled' => false,
        'controllers' => [
            'Restservices' => '*',
            'Transfers' => ['api'],
            'Archives' => ['api'],
        ],
    ],

    /*
     * Permet d'empêcher un trop grand nombre d'essais de mot de passe infructueux
     *
     * - attempt_limit : limite le nombre de tentatives de login sur un utilisateur en particulier sur une période
     *      - enabled : si valeur true, la limite est activée
     *      - max_amount : si le nombre d'essais dépasse cette valeur, une erreur 429 sera envoyée
     *      - cache_duration : durée en secondes pendant laquelle on conserve les tentatives infructueuses (max 1h)
     *      - ignore_first_intrusions_warnings : nombre de tentatives avant que le journal des evenements enregistre une
     *                                           tentative d'intrusion
     *      - log_cooldown : Nombre de secondes entre 2 inscriptions en base des warnings d'intrusions
     */
    'Login' => [
        'attempt_limit' => [
            'enabled' => true,
            'max_amount' => 100,
            'cache_duration' => 3600,
            'ignore_first_intrusions_warnings' => 3,
            'log_cooldown' => 10,
        ],
    ],

    /*
     * Style des emails
     */
    'AsalaeCore' => [
        'Template' => [
            'Layout' => [
                'Email' => [
                    'html' => [
                        'default' => [
                            'logoUrl' => 'https://ressources.libriciel.fr/public/asalae/img/asalae-color-white.png',
                            'logoAlt' => 'asalae',
                            'headerColor' => '#222',
                        ],
                    ],
                ],
            ],
        ],
    ],

    /*
     * Configuration des tableaux de résultats
     *  - exportCsv
     *      - limit: Nombre de lignes maximum d'un export CSV. Une valeur trop importante peut avoir un impact négatif
     *               sur les performances (potentiellement plusieurs minutes d'attente).
     *               Appliquez des filtres de recherche afin de diminuer le nombre de résultats.
     *               Ne pas dépasser 1 million "1000000" sous peine de rendre le fichier illisible.
     */
    'Index' => [
        'exportCsv' => [
            'limit' => 1000,
        ],
    ],

    /*
     * Attestations
     *      - destruction_delay : l'attestation d'élimination sera produite une
     *                            fois le délai (en jours) passé à partir de l'élimination des archives
     */
    'Attestations' => [
        'destruction_delay' => 0,
    ],

    /**
     * Moteur de migrations pour les tests
     */
    'Migrations' => [
        'backend' => 'phinx',
    ],

    /**
     * Configuration pour le serveur de messagerie Mercure
     * - enabled: active Mercure (remplace Ratchet)
     * - url: serveur mercure
     * - jwt_secret: mot de passe défini lors du composer up
     */
    'Mercure' => [
        'enabled' => true,
        'url' => 'http://asalae_mercure:3000/.well-known/mercure',
        'jwt_secret' => '',
    ],

    /**
     * Rendu custom pour les connexions openid sans utilisateurs
     */
    'AppExceptionRenderer' => [
        'custom_templates' => [
            UnknownUserException::class => [
                'template' => 'unknown_user',
                'layout' => 'not_connected_error',
            ],
        ],
    ],
];
