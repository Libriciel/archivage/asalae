<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Cron\ArchiveUnitCalcExpiredDUARoot;
use Asalae\Cron\ArchiveUnitCalcSearch;
use Cake\Database\Query\SelectQuery;
use Migrations\AbstractSeed;

/**
 * Patch200 seed.
 *
 * 2e Seed
 */
class Patch001v200Seed extends AbstractSeed
{
    /**
     * @return void
     */
    public function run(): void
    {
        $adapter = $this->getAdapter();
        /** @var SelectQuery $queryBuilder */
        $queryBuilder = $adapter->getQueryBuilder('select');
        $func = $queryBuilder->func();
        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('crons')
            ->where(['classname' => ArchiveUnitCalcSearch::class])
            ->execute()
            ->fetchColumn(0);

        // Si ce cron existe, c'est qu'il n'y a pas besoin de ce seed
        if ((int)$count > 0) {
            return;
        }

        $this->insert(
            'crons',
            [
                'name' => __("Recalcul du champ de recherche du catalogue"),
                'description' => __(
                    "Recalcul du champ de recherche du catalogue lorsque 
                    les règles de restriction d'accès des descriptions arrivent à leur terme."
                ),
                'classname' => ArchiveUnitCalcSearch::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DateTime::ATOM),
                'app_meta' => null,
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ]
        );
        $this->insert(
            'crons',
            [
                'name' => __("Recalcul du champ de dua échue racine des unités d'archives"),
                'description' => __(
                    "Recalcul du champ de dua échue racine qui sert à
                     la sélection des unités d'archives dans la vue DUA échue."
                ),
                'classname' => ArchiveUnitCalcExpiredDUARoot::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DateTime::ATOM),
                'app_meta' => null,
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ]
        );
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            Patch000InitialSeed::class,
        ];
    }
}
