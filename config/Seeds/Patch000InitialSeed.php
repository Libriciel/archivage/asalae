<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Cron\ArchivesFilesExistControl;
use Asalae\Cron\ArchivesIntegrityControl;
use Asalae\Cron\Check;
use Asalae\Cron\EventsLogsExport;
use Asalae\Cron\GarbageCollector;
use Asalae\Cron\JobMaker;
use Asalae\Cron\TransfersRetention;
use Asalae\Cron\UpdateLDAPUsers;
use Asalae\Model\Entity\Role;
use Asalae\Model\Table\RolesTable;
use AsalaeCore\Cron\LiberSign;
use AsalaeCore\Cron\Pronom;
use AsalaeCore\Cron\Unlocker;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use Cake\Core\Configure;
use Cake\Database\Query\SelectQuery;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\I18n\DateTime as Time;
use Cake\Utility\Hash;
use Migrations\AbstractSeed;

/**
 * Initial seed.
 *
 * 1er Seed
 */
class Patch000InitialSeed extends AbstractSeed
{
    /**
     * @var array Liste de champ name de la table roles
     */
    const array ROLES = [
        "Archiviste" => RolesTable::CODE_ARCHIVIST,
        "Opérateur d'archivage" => RolesTable::CODE_ARCHIVING_OPERATOR,
        "Référent Archives" => RolesTable::CODE_ARCHIVES_REFERENT,
        "Contrôleur" => RolesTable::CODE_CONTROLLER,
        "Versant" => RolesTable::CODE_SUBMITTER,
        "Producteur" => RolesTable::CODE_PRODUCER,
        "Demandeur" => RolesTable::CODE_APPLICANT,
        "ws versant" => RolesTable::CODE_WS_SUBMITTER,
        "ws administrateur" => RolesTable::CODE_WS_ADMINISTRATOR,
        "ws requêteur" => RolesTable::CODE_WS_REQUESTER,
    ];
    /**
     * @var array Champ name en clef, name[] des roles liés en valeur
     */
    const array TYPES = [
        [
            'name' => "Service d'Exploitation",
            'code' => 'SE',
            'roles' => [],
        ],
        [
            'name' => "Service d'Archives",
            'code' => 'SA',
            'roles' => [
                "Archiviste",
                "Opérateur d'archivage",
                "Contrôleur",
            ],
        ],
        [
            'name' => "Service de Contrôle Scientifique et Technique",
            'code' => 'CST',
            'roles' => [
                "Contrôleur",
            ],
        ],
        [
            'name' => "Service Versant",
            'code' => 'SV',
            'roles' => [
                "Versant",
                "Producteur",
                "Demandeur",
                "ws requêteur",
                "ws versant",
            ],
        ],
        [
            'name' => "Service Producteur",
            'code' => 'SP',
            'roles' => [
                "Demandeur",
                "Producteur",
            ],
        ],
        [
            'name' => "Service Demandeur",
            'code' => 'SD',
            'roles' => [
                "Demandeur",
            ],
        ],
        [
            'name' => "Opérateur de versement",
            'code' => 'OV',
            'roles' => [
                "ws versant",
            ],
        ],
        [
            'name' => "Organisationnel",
            'code' => 'SO',
            'roles' => [
                "Référent Archives",
            ],
        ],
    ];
    /**
     * @var int[] Mémorise les entités créé
     */
    private $roles;

    /**
     * @return void
     */
    public function run(): void
    {
        $adapter = $this->getAdapter();
        /** @var SelectQuery $queryBuilder */
        $queryBuilder = $adapter->getQueryBuilder('select');
        $func = $queryBuilder->func();
        $conn = $queryBuilder->getConnection();
        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('versions')
            ->execute()
            ->fetchColumn(0);

        // Si il y a des entrées dans version, c'est qu'il n'y a pas besoin de ce seed
        if ((int)$count > 0) {
            return;
        }

        $i = 1;
        $driver = $conn->getDriver();
        foreach (self::ROLES as $role => $code) {
            $this->insert(
                'roles',
                [
                    'name' => $role,
                    'code' => $code,
                    'active' => true,
                    'lft' => $i++,
                    'rght' => $i++,
                    'hierarchical_view' => (int)in_array($code, ['AA', 'AC', 'ARA']),
                    'created' => (new DateTime())->format(DATE_RFC3339),
                    'modified' => (new DateTime())->format(DATE_RFC3339),
                ]
            );
            $this->roles[$role] = $driver->lastInsertId();
        }
        foreach (self::TYPES as $values) {
            $this->insert(
                'type_entities',
                [
                    'name' => $values['name'],
                    'code' => $values['code'],
                    'active' => true,
                    'created' => (new DateTime())->format(DATE_RFC3339),
                    'modified' => (new DateTime())->format(DATE_RFC3339),
                ]
            );
            $type_entity_id = $driver->lastInsertId();

            foreach ($values['roles'] as $role) {
                $this->insert(
                    'roles_type_entities',
                    [
                        'role_id' => $this->roles[$role],
                        'type_entity_id' => $type_entity_id,
                    ]
                );
            }
        }
        $description = <<<EOT
<p>La tâche vérifie que asalae fonctionne dans les conditions nominales et qu'il
est en mesure d'assurer le service demandé à un SAE.</p>
<p>Les vérifications portent sur :
<ul>
  <li>l'installation du SAE : vérification du serveur, de php
  (version et modules supplémentaires), de postgres,
  des sources de l'application (version, plugin, ...),
  de la version du schéma de la base de données</li>

  <li>les outils et services externes : vérification des outils de conversion,
  de détection du format des fichiers, de l'antivirus, du service d'horodatage, ...</li>

  <li>les volumes de stockage : accessible, alerte taux d'occupation et date de fin programmée</li>
</ul>
</p>
EOT;
        $crons = [
            [
                'name' => "Vérification du fonctionnement de asalae",
                'description' => $description,
                'classname' => Check::class,
                'active' => true,
                'locked' => 0,
                'frequency' => 'P1D',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DATE_RFC3339),
                'app_meta' => null,
            ],
            [
                'name' => "Mise à jour Libersign",
                'description' => "Met à jour le logiciel de signature",
                'classname' => LiberSign::class,
                'active' => true,
                'locked' => 0,
                'frequency' => 'P1D',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DATE_RFC3339),
                'app_meta' => '{"use_proxy":true}',
            ],
            [
                'name' => "Déverrouillage des crons KO",
                'description' => "En cas de plantage d'un cron, celui-ci est déverrouillé"
                    . " après une certaine période (1h par défaut)",
                'classname' => Unlocker::class,
                'active' => true,
                'locked' => 0,
                'frequency' => 'PT1H',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('PT1H'))
                    ->format(DATE_RFC3339),
                'app_meta' => null,
            ],
            [
                'name' => "Mise à jour de la liste des pronoms",
                'description' => "Fait appel à un webservice pour mettre à jour à table des pronoms (fmt)",
                'classname' => Pronom::class,
                'active' => true,
                'locked' => 0,
                'frequency' => 'P1D',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DATE_RFC3339),
                'app_meta' => json_encode(
                    [
                        'wsdl' => 'https://www.nationalarchives.gov.uk/PRONOM/Services/Contract/PRONOM.wsdl',
                        'endpoint' => 'https://www.nationalarchives.gov.uk/PRONOM/service.asmx',
                        'action_version' => 'getSignatureFileVersionV1',
                        'action_data' => 'getSignatureFileV1',
                        'extract_version' => 'Version.Version',
                        'extract_data' => 'SignatureFile.FFSignatureFile.FileFormatCollection.FileFormat',
                        'use_proxy' => true,
                    ]
                ),
            ],
            [
                'name' => __("Créateur de jobs"),
                'description' => __(
                    "Recherche des cas bloqués dans un état intermédiaire afin de lancer les jobs qui pourraient le débloquer"
                ),
                'classname' => JobMaker::class,
                'active' => true,
                'locked' => 0,
                'frequency' => 'P1D',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DATE_RFC3339),
                'app_meta' => '{"freezedInterval":"P1D"}',
                'parallelisable' => false,
            ],
            [
                'name' => __("Création des journaux des événements"),
                'description' => __(
                    "Création des fichiers des journaux des événements sous forme de fichiers xml PREMIS"
                    . " et stockage dans les archives techniques des journaux des événements. "
                    . "Le service d'Archives gestionnaire conserve les journaux d'exploitation"
                    . " ainsi que les fichiers sceau des journaux"
                ),
                'classname' => EventsLogsExport::class,
                'active' => true,
                'locked' => 0,
                'frequency' => 'P1D',
                'next' => (new \DateTime('01:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DATE_RFC3339),
                'app_meta' => null,
                'parallelisable' => false,
            ],
        ];
        foreach ($crons as $cron) {
            $this->insert('crons', $cron);
        }

        for ($i = 0; $i <= 150; $i++) {
            $interval = sprintf('P%dY', $i);
            $this->insert(
                'appraisal_rule_codes',
                [
                    'code' => 'AP' . $interval,
                    'name' => $name = __n("{0} an", "{0} ans", $i, $i),
                    'description' => $name,
                    'duration' => $interval,
                ]
            );
        }

        $data = Seda10Schema::getXsdOptions('access_code');
        foreach ($data as $values) {
            $exp = explode(' - ', $values['text']);
            $duration = is_numeric($exp[1][0]) ? (int)$exp[1] : 1000; // [2]5 ans = 25 ; [i]llimité = 1000
            $this->insert(
                'access_rule_codes',
                [
                    'code' => $values['value'],
                    'name' => $exp[1],
                    'description' => $values['title'],
                    'duration' => 'P' . $duration . 'Y',
                ]
            );
        }

        $this->insert(
            'versions',
            [
                'subject' => Configure::read('App.name', 'asalae'),
                'version' => '2.0.0a1',
                'created' => (new Time())->format(DATE_RFC3339),
            ]
        );
        $this->insert(
            'crons',
            [
                'name' => __("Vérification de l'intégrité des fichiers des archives"),
                'description' => sprintf(
                    '<p>%s</p><ul><li>%s</li><li>%s</li></ul>',
                    __(
                        "Effectue la lecture des fichiers des archives et vérifie que les tailles 
                        et empreintes recalculées correspondent à celles stockées en base de données."
                    ),
                    __("Paramètre 1, optionnel, défaut = 1, temps maximum d'exécution de la tâche en heures."),
                    __("Paramètre 2, optionnel, défaut = 1, délai minimmum entre deux vérifications en jours.")
                ),
                'classname' => ArchivesIntegrityControl::class,
                'active' => true,
                'locked' => 0,
                'frequency' => 'P1D',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DATE_RFC3339),
                'app_meta' => json_encode(
                    [
                        'max_execution_time' => 1, // durée en heures
                        'min_delay' => 7, // jours entre 2 vérifications pour une même archive
                    ]
                ),
                'parallelisable' => 0,
            ]
        );

        $this->insert(
            'versions',
            [
                'subject' => Configure::read('App.name', 'as@lae'),
                'version' => '2.0.0a2',
                'created' => (new Time())->format(DATE_RFC3339),
            ]
        );

        $this->insert(
            'versions',
            [
                'subject' => Configure::read('App.name', 'as@lae'),
                'version' => '2.0.0a3',
                'created' => (new Time())->format(DATE_RFC3339),
            ]
        );

        $this->insert(
            'versions',
            [
                'subject' => Configure::read('App.name', 'as@lae'),
                'version' => '2.0.0a4',
                'created' => (new Time())->format(DATE_RFC3339),
            ]
        );
        $adapter->getUpdateBuilder()
            ->update('phinxlog')
            ->set(['migration_name' => 'Patch200a3'])
            ->where(['migration_name' => 'CreateKillables'])
            ->execute();

        $filesCount = $adapter->getSelectBuilder()
            ->select($func->count('*'))
            ->from(['binaries_units' => 'archive_binaries_archive_units'])
            ->innerJoin(
                ['au' => 'archive_units'],
                ['binaries_units.archive_unit_id' => new IdentifierExpression('au.id')]
            )
            ->where(
                ['au.archive_id' => new IdentifierExpression('Archives.id')]
            )
            ->sql();

        $unitsCount = $adapter->getSelectBuilder()
            ->select($func->count('*'))
            ->from(['units' => 'archive_units'])
            ->where(
                [
                    'units.archive_id' => new IdentifierExpression('Archives.id'),
                ]
            )
            ->sql();

        $archives = $adapter->getSelectBuilder()
            ->select(
                [
                    'Archives.id',
                    'fcount' => new QueryExpression($filesCount),
                    'ucount' => new QueryExpression($unitsCount),
                ]
            )
            ->from(['Archives' => 'archives'])
            ->execute()
            ->fetchAll('assoc');
        foreach ($archives as $archive) {
            $adapter->getUpdateBuilder()
                ->update('archives')
                ->set(
                    [
                        'files_count' => $archive['fcount'],
                        'units_count' => $archive['ucount'],
                    ]
                )
                ->where(['id' => $archive['id']]);
        }

        $saId = $adapter->getSelectBuilder()
            ->select(['OrgEntities.id'])
            ->from(['OrgEntities' => 'org_entities'])
            ->innerJoin(
                ['TypeEntities' => 'type_entities'],
                ['TypeEntities.id' => new IdentifierExpression('type_entity_id')]
            )
            ->where(['code' => 'SA'])
            ->execute()
            ->fetchColumn(0);

        if ($saId) {
            $adapter->getUpdateBuilder()
                ->update('org_entities')
                ->set(['archival_agency_id' => $saId])
                ->where(['archival_agency_id IS' => null])
                ->execute();
        }

        $this->insert(
            'crons',
            [
                'name' => __("Nettoyage des entrées expirées en base de donnée"),
                'description' => sprintf(
                    '<p>%s</p>',
                    __(
                        "Effectue une suppression des entrées expirées dans les tables liées aux Tokens Oai-Pmh, Access tokens et Auth urls."
                    )
                ),
                'classname' => GarbageCollector::class,
                'active' => true,
                'locked' => 0,
                'frequency' => 'PT1H',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('PT1H'))
                    ->format(DATE_RFC3339),
                'app_meta' => null,
                'parallelisable' => true,
            ]
        );

        $archives = $adapter->getSelectBuilder()
            ->select(
                [
                    'Archives.id',
                    'Archives.created',
                    'Archives.last_state_update',
                ]
            )
            ->from(['Archives' => 'archives'])
            ->execute()
            ->fetchAll('assoc');
        foreach ($archives as $archive) {
            $adapter->getUpdateBuilder()
                ->update('archives')
                ->set(
                    [
                        'modified' => $archive['last_state_update'] ?: $archive['created'],
                    ]
                )
                ->where(['id' => $archive['id']])
                ->execute();
        }

        $stmp = $adapter->getSelectBuilder()
            ->select(['Users.id'])
            ->from(['Users' => 'users'])
            ->innerJoin(
                ['Roles' => 'roles'],
                ['Roles.id' => new IdentifierExpression('Users.role_id')]
            )
            ->where(['Roles.code IN' => Role::WEBSERVICES])
            ->execute()
            ->fetchAll();
        $webservices = Hash::extract($stmp, '{n}.0');
        if (!empty($webservices)) {
            $adapter->getUpdateBuilder()
                ->update('users')
                ->set(['agent_type' => 'software'])
                ->where(['id IN' => $webservices])
                ->execute();
        }
        $adapter->getUpdateBuilder()
            ->update('roles')
            ->set(['agent_type' => 'software'])
            ->where(['code IN' => Role::WEBSERVICES])
            ->execute();

        $this->insert(
            'versions',
            [
                'subject' => Configure::read('App.name', 'as@lae'),
                'version' => '2.0.0a5',
                'created' => (new Time())->format(DATE_RFC3339),
            ]
        );

        $adapter->getUpdateBuilder()
            ->update('crons')
            ->set(
                [
                    'name' => __("Nettoyage des entrées expirées en base de donnée et des fichiers temporaires"),
                    'description' => sprintf(
                        '<p>%s</p>',
                        __(
                            "Effectue une suppression des entrées expirées dans les tables liées aux Tokens Oai-Pmh, Access tokens et Auth urls, ainsi que des fichiers temporaires liés aux zip d'archives."
                        )
                    ),
                ]
            )
            ->where(['classname' => 'Asalae\Cron\GarbageCollector'])
            ->execute();

        $this->insert(
            'crons',
            [
                'name' => __("Rétention des transferts acceptés : suppression des fichiers"),
                'description' => sprintf(
                    '<p>%s</p>',
                    __(
                        "Suppression des fichiers des transferts acceptés une fois le délai de rétention passé.
                        Il faut également que les archives issues des transferts soient intègres."
                    )
                ),
                'classname' => TransfersRetention::class,
                'active' => true,
                'locked' => 0,
                'frequency' => 'P1D',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DATE_RFC3339),
                'app_meta' => json_encode(
                    [
                        'retention_delay' => 1,
                    ]
                ),
                'parallelisable' => 0,
            ]
        );

        $adapter->getUpdateBuilder()
            ->update('crons')
            ->set(
                [
                    'description' => sprintf(
                        '<p>%s</p>',
                        __(
                            "Effectue la lecture des fichiers des archives et vérifie que les tailles 
                            et empreintes recalculées correspondent à celles stockées en base de données."
                        )
                    ),
                ]
            )
            ->where(['classname' => ArchivesIntegrityControl::class])
            ->execute();

        $subquery = $adapter->getSelectBuilder()
            ->select(['sum' => $func->sum('size')])
            ->from(['TransferAttachments' => 'transfer_attachments'])
            ->where(['transfer_id' => new IdentifierExpression('Transfers.id')])
            ->limit(1)
            ->sql();
        $adapter->getUpdateBuilder()
            ->update('transfers')
            ->set(['data_size' => new QueryExpression($subquery)])
            ->where(['files_deleted IS NOT' => true])
            ->execute();

        $adapter->getUpdateBuilder()
            ->update('crons')
            ->set(['max_duration' => 'PT6H'])
            ->where(
                [
                    'classname IN' => [
                        'Asalae\Cron\ArchivesIntegrityControl',
                        'Asalae\Cron\EventsLogsExport',
                    ],
                ]
            )
            ->execute();

        $this->insert(
            'versions',
            [
                'subject' => Configure::read('App.name', 'as@lae'),
                'version' => '2.0.0a6',
                'created' => (new Time())->format(DATE_RFC3339),
            ]
        );
        $this->insert(
            'crons',
            [
                'name' => __("Mise à jour des informations des utilisateurs LDAP/AD"),
                'description' => __(
                    "Mise à jour du nom et du mail des utilisateurs issus de LDAP/AD.
                    Les utilisateurs ne faisant plus partie des LDAP/AD liés sont listés 
                    dans le rapport d'exécution et doivront être désactivés par 
                    l'admistrateur fonctionnel de as@lae."
                ),
                'classname' => UpdateLDAPUsers::class,
                'active' => true,
                'locked' => 0,
                'frequency' => 'P1D',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DATE_RFC3339),
                'app_meta' => null,
                'parallelisable' => 0,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ]
        );
        $this->insert(
            'crons',
            [
                'name' => __("Vérification de la présence des fichiers des archives"),
                'description' => __(
                    "Effectue la vérification de la présence des fichiers des archives. 
                    Paramètre 1, optionnel, défaut = 1, temps maximum d'exécution de la tâche en heures. 
                    Paramètre 2, optionnel, défaut = 1, délai minimmum entre deux vérifications en jours."
                ),
                'classname' => ArchivesFilesExistControl::class,
                'active' => true,
                'locked' => 0,
                'frequency' => 'P1D',
                'next' => (new DateTime('02:00:00'))
                    ->sub(new DateInterval('P1D'))
                    ->format(DATE_RFC3339),
                'app_meta' => json_encode(
                    [
                        'max_execution_time' => 1, // durée en heures
                        'min_delay' => 1, // jours entre 2 vérifications pour une même archive
                    ]
                ),
                'parallelisable' => 0,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ]
        );
        $stmp = $adapter->getSelectBuilder()
            ->select(['id'])
            ->from('roles')
            ->where(['code IN' => ['WSV', 'WSA', 'WSR']])
            ->execute()
            ->fetchAll();
        $roles = Hash::extract($stmp, '{n}.0');

        $typeEntity = $adapter->getSelectBuilder()
            ->select(['id'])
            ->from('type_entities')
            ->where(['code' => 'SA'])
            ->execute()
            ->fetchColumn(0);

        foreach ($roles as $role) {
            $this->insert(
                'roles_type_entities',
                [
                    'role_id' => $role,
                    'type_entity_id' => $typeEntity,
                ]
            );
        }

        $this->insert(
            'versions',
            [
                'subject' => Configure::read('App.name', 'as@lae'),
                'version' => '2.0.0a7',
                'created' => (new Time())->format(DATE_RFC3339),
            ]
        );
    }
}
