<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Cron\CacheClear;
use Asalae\Cron\DestructionCertificateCreation;
use Cake\Core\Configure;
use Cake\Database\Query\SelectQuery;
use Migrations\AbstractSeed;

/**
 * Patch217 seed.
 *
 * 16e Seed
 */
class Patch015v219Seed extends AbstractSeed
{
    public const string VERSION = '2.1.9';

    /**
     * @return void
     */
    public function run(): void
    {
        $adapter = $this->getAdapter();
        /** @var SelectQuery $queryBuilder */
        $queryBuilder = $adapter->getQueryBuilder('select');
        $func = $queryBuilder->func();
        $subject = Configure::read('App.name', 'asalae');
        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('versions')
            ->where(['subject IN' => ['asalae', 'as@lae'], 'version' => self::VERSION])
            ->execute()
            ->fetchColumn(0);

        if ((int)$count === 0) {
            $this->insert(
                'versions',
                [
                    'subject' => $subject,
                    'version' => self::VERSION,
                    'created' => (new DateTime())->format(DATE_RFC3339),
                ]
            );
        }

        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('crons')
            ->where(['description' => '', 'classname' => CacheClear::class])
            ->execute()
            ->fetchColumn(0);
        if ((int)$count === 1) {
            $adapter->getUpdateBuilder()
                ->update('crons')
                ->set(
                    [
                        'description' => __(
                            "Suppression des fichiers de cache sur le "
                            . "serveur en fonction de leur date d'expiration."
                        ),
                    ]
                )
                ->where(['classname' => CacheClear::class])
                ->execute();
        }

        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('crons')
            ->where(['classname' => DestructionCertificateCreation::class])
            ->execute()
            ->fetchColumn(0);
        if ((int)$count === 0) {
            $this->insert(
                'crons',
                [
                    'name' => __("Attestations d'élimination"),
                    'description' => __(
                        "Génération des attestations d'élimination après"
                        . " le délai de destruction des sauvegardes."
                    ),
                    'classname' => DestructionCertificateCreation::class,
                    'active' => true,
                    'locked' => false,
                    'frequency' => 'P1D',
                    'next' => (new \DateTime('02:00:00'))
                        ->sub(new \DateInterval('P1D'))
                        ->format(DATE_RFC3339),
                    'app_meta' => null,
                    'parallelisable' => true,
                ]
            );
        }
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            Patch014v218Seed::class,
        ];
    }
}
