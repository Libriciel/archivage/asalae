<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Cake\Core\Configure;
use Cake\Database\Query\SelectQuery;
use Migrations\AbstractSeed;

/**
 * Patch208 seed.
 *
 * 4e Seed
 */
class Patch003v208Seed extends AbstractSeed
{
    public const string VERSION = '2.0.8';

    /**
     * @return void
     */
    public function run(): void
    {
        $adapter = $this->getAdapter();
        /** @var SelectQuery $queryBuilder */
        $queryBuilder = $adapter->getQueryBuilder('select');
        $func = $queryBuilder->func();
        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('versions')
            ->where(['subject IN' => ['asalae', 'as@lae'], 'version' => self::VERSION])
            ->execute()
            ->fetchColumn(0);

        // Si cette version existe, c'est qu'il n'y a pas besoin de ce seed
        if ((int)$count > 0) {
            return;
        }

        $this->insert(
            'versions',
            [
                'subject' => Configure::read('App.name', 'as@lae'),
                'version' => self::VERSION,
                'created' => (new DateTime())->format(DATE_RFC3339),
            ]
        );
        $migrations = glob(CONFIG . DS . 'Migrations' . DS . '*_Patch*.php');
        foreach ($migrations as $migration) {
            [$timestamp, $name] = explode('_', basename($migration, '.php'));
            $version = implode('.', str_split(str_replace('Patch', '', $name)));

            $versionExists = $adapter->getSelectBuilder()
                ->select(['id'])
                ->from('versions')
                ->where(['subject IN' => ['asalae', 'as@lae'], 'version' => $version])
                ->execute()
                ->fetchColumn(0);

            if (!$versionExists) {
                $end_time = $adapter->getSelectBuilder()
                    ->select(['end_time'])
                    ->from('phinxlog')
                    ->where(['version' => $timestamp])
                    ->execute()
                    ->fetchColumn(0);
                $this->insert(
                    'versions',
                    [
                        'subject' => 'asalae',
                        'version' => $version,
                        'created' => $end_time,
                    ]
                );
            }
        }
        $adapter->getDeleteBuilder()
            ->delete()
            ->from('versions')
            ->where(
                [
                    'subject IN' => ['asalae', 'as@lae'],
                    'version LIKE' => '2.0.0a%',
                ]
            )
            ->execute();
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            Patch002v210Seed::class,
        ];
    }
}
