<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Cron\ApplyPatch;
use Asalae\Cron\AutoTreeRepair;
use Cake\Core\Configure;
use Migrations\AbstractSeed;

class Patch028v226Seed extends AbstractSeed
{
    public const string VERSION = '2.2.6';

    /**
     * @return void
     * @throws Exception
     */
    public function run(): void
    {
        $adapter = $this->getAdapter();
        $queryBuilder = $adapter->getQueryBuilder('select');
        $func = $queryBuilder->func();
        $subject = Configure::read('App.name', 'asalae');
        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('versions')
            ->where(['subject IN' => ['asalae', 'as@lae'], 'version' => self::VERSION])
            ->execute()
            ->fetchColumn(0);

        // Si cette version existe, c'est qu'il n'y a pas besoin de ce seed
        if ((int)$count > 0) {
            return;
        }

        $this->insert(
            'versions',
            [
                'subject' => $subject,
                'version' => self::VERSION,
                'created' => (new DateTime())->format(DATE_RFC3339),
            ]
        );

        $this->insert(
            'crons',
            [
                'name' => __("Réparation automatique des tables (tree)"),
                'description' => __("Recalcule les champs lft et rght des tables qui ont un décalage"),
                'classname' => AutoTreeRepair::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'PT4H',
                'next' => (new DateTime('02:00:00'))
                    ->sub(new DateInterval('P1D'))
                    ->format(DATE_RFC3339),
                'app_meta' => json_encode(
                    ['tables' => ['ArchiveUnits', 'TechnicalArchiveUnits']],
                    JSON_UNESCAPED_SLASHES
                ),
                'parallelisable' => false,
            ]
        );

        // Le patch doit être lancé si il existe des archives
        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('archives')
            ->execute()
            ->fetchColumn(0);
        if (!$count) {
            return;
        }
        $cmd = 'patch' . str_replace('.', '', self::VERSION);
        $this->insert(
            'crons',
            [
                'name' => __("Patch {0}", self::VERSION),
                'description' => __("Modifications nécessaires au passage en version {0}", self::VERSION),
                'classname' => ApplyPatch::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'one_shot',
                'next' => (new DateTime())->format(DATE_RFC3339),
                'app_meta' => json_encode(['patch' => $cmd], JSON_UNESCAPED_SLASHES),
                'parallelisable' => false,
            ]
        );
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            Patch027v300Seed::class,
        ];
    }
}
