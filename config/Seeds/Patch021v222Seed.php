<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Cron\ApplyPatch;
use Cake\Core\Configure;
use Cake\Database\Query\SelectQuery;
use Migrations\AbstractSeed;

/**
 * Patch217 seed.
 *
 * 13e Seed
 */
class Patch021v222Seed extends AbstractSeed
{
    public const string VERSION = '2.2.2';

    /**
     * @return void
     */
    public function run(): void
    {
        $adapter = $this->getAdapter();
        /** @var SelectQuery $queryBuilder */
        $queryBuilder = $adapter->getQueryBuilder('select');
        $func = $queryBuilder->func();
        $subject = Configure::read('App.name', 'asalae');
        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('versions')
            ->where(['subject IN' => ['asalae', 'as@lae'], 'version' => self::VERSION])
            ->execute()
            ->fetchColumn(0);

        // Si cette version existe, c'est qu'il n'y a pas besoin de ce seed
        if ((int)$count > 0) {
            return;
        }

        $this->insert(
            'versions',
            [
                'subject' => $subject,
                'version' => self::VERSION,
                'created' => (new DateTime())->format(DATE_RFC3339),
            ]
        );

        // Le patch doit être lancé si il existe des archives
        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('archives')
            ->execute()
            ->fetchColumn(0);
        if (!$count) {
            return;
        }
        $cmd = 'patch' . str_replace('.', '', self::VERSION);
        $this->insert(
            'crons',
            [
                'name' => __("Patch {0}", self::VERSION),
                'description' => __("Modifications nécessaires au passage en version {0}", self::VERSION),
                'classname' => ApplyPatch::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'one_shot',
                'next' => (new DateTime())->format(DATE_RFC3339),
                'app_meta' => json_encode(['patch' => $cmd], JSON_UNESCAPED_SLASHES),
                'parallelisable' => false,
            ]
        );
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            Patch020v2112Seed::class,
        ];
    }
}
