<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Cron\CacheClear;
use Cake\Core\Configure;
use Cake\Database\Query\SelectQuery;
use Migrations\AbstractSeed;

/**
 * Patch217 seed.
 *
 * 15e Seed
 */
class Patch014v218Seed extends AbstractSeed
{
    public const string VERSION = '2.1.8';

    /**
     * @return void
     */
    public function run(): void
    {
        $adapter = $this->getAdapter();
        /** @var SelectQuery $queryBuilder */
        $queryBuilder = $adapter->getQueryBuilder('select');
        $func = $queryBuilder->func();
        $subject = Configure::read('App.name', 'asalae');
        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('versions')
            ->where(['subject IN' => ['asalae', 'as@lae'], 'version' => self::VERSION])
            ->execute()
            ->fetchColumn(0);

        if ((int)$count === 0) {
            $this->insert(
                'versions',
                [
                    'subject' => $subject,
                    'version' => self::VERSION,
                    'created' => (new DateTime())->format(DATE_RFC3339),
                ]
            );
        }

        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('crons')
            ->where(['classname' => CacheClear::class])
            ->execute()
            ->fetchColumn(0);
        if ((int)$count === 0) {
            $this->insert(
                'crons',
                [
                    'name' => __("Suppression du cache expiré"),
                    'description' => '',
                    'classname' => CacheClear::class,
                    'active' => true,
                    'locked' => false,
                    'frequency' => 'P1D',
                    'next' => (new \DateTime('00:15:00'))
                        ->sub(new \DateInterval('P1D'))
                        ->format(DATE_RFC3339),
                    'app_meta' => null,
                    'parallelisable' => true,
                ]
            );
        }
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            Patch013v217Seed::class,
        ];
    }
}
