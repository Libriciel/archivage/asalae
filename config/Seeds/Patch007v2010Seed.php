<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Cake\Core\Configure;
use Cake\Database\Query\SelectQuery;
use Migrations\AbstractSeed;

/**
 * Patch2010 seed.
 *
 * 8e Seed
 */
class Patch007v2010Seed extends AbstractSeed
{
    public const string VERSION = '2.0.10';

    /**
     * @return void
     */
    public function run(): void
    {
        $adapter = $this->getAdapter();
        /** @var SelectQuery $queryBuilder */
        $queryBuilder = $adapter->getQueryBuilder('select');
        $func = $queryBuilder->func();
        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('versions')
            ->where(['subject IN' => ['asalae', 'as@lae'], 'version' => self::VERSION])
            ->execute()
            ->fetchColumn(0);

        // Si cette version existe, c'est qu'il n'y a pas besoin de ce seed
        if ((int)$count > 0) {
            return;
        }

        $this->insert(
            'versions',
            [
                'subject' => Configure::read('App.name', 'as@lae'),
                'version' => self::VERSION,
                'created' => (new DateTime())->format(DATE_RFC3339),
            ]
        );
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            Patch006v212Seed::class,
        ];
    }
}
