<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Cron\OutgoingTransferRequestArchiveDestruction;
use Asalae\Cron\OutgoingTransferTracking;
use Asalae\Cron\RestitutionRequestArchiveDestruction;
use Cake\Database\Query\SelectQuery;
use Migrations\AbstractSeed;

/**
 * Patch210 seed.
 *
 * 3e Seed
 */
class Patch002v210Seed extends AbstractSeed
{
    /**
     * @return void
     */
    public function run(): void
    {
        $adapter = $this->getAdapter();
        /** @var SelectQuery $queryBuilder */
        $queryBuilder = $adapter->getQueryBuilder('select');
        $func = $queryBuilder->func();
        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('crons')
            ->where(['classname' => RestitutionRequestArchiveDestruction::class])
            ->execute()
            ->fetchColumn(0);

        // Si ce cron existe, c'est qu'il n'y a pas besoin de ce seed
        if ((int)$count > 0) {
            return;
        }

        $this->insert(
            'crons',
            [
                'name' => __("Restitutions : élimination des archives restituées"),
                'description' => __(
                    "Elimination des archives liées aux restitutions aquittées. "
                    . "Sans aucune actions, les archives seront automatiquement éliminées"
                    . " après le délai spécifié dans la configuration des restitutions après l'acquittement"
                ),
                'classname' => RestitutionRequestArchiveDestruction::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DateTime::ATOM),
                'app_meta' => null,
                'parallelisable' => false,
            ]
        );
        $this->insert(
            'crons',
            [
                'name' => __("Transferts sortants : suivi"),
                'description' => __(
                    "Lecture des accusés de réception et des réponses aux"
                    . " transferts des transferts sortants en cours"
                ),
                'classname' => OutgoingTransferTracking::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DateTime::ATOM),
                'app_meta' => null,
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ]
        );
        $this->insert(
            'crons',
            [
                'name' => __("Transferts sortants : élimination des archives transférées"),
                'description' => __(
                    "Elimination des archives liées aux transferts sortants acceptés."
                    . " Sans aucune actions, les archives seront automatiquement éliminées"
                    . " après le délai spécifié dans la configuration des transferts sortants"
                ),
                'classname' => OutgoingTransferRequestArchiveDestruction::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => (new \DateTime('02:00:00'))
                    ->sub(new \DateInterval('P1D'))
                    ->format(DateTime::ATOM),
                'app_meta' => null,
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ]
        );
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            Patch001v200Seed::class,
        ];
    }
}
