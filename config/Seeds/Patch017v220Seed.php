<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Cron\MailNotifications;
use Asalae\Model\Table\RolesTable;
use AsalaeCore\Factory\Utility;
use Cake\Core\Configure;
use Cake\Database\Query\SelectQuery;
use Migrations\AbstractSeed;

/**
 * Patch220 seed.
 *
 * 15e Seed
 */
class Patch017v220Seed extends AbstractSeed
{
    /**
     * @return void
     * @throws Exception
     */
    public function run(): void
    {
        $adapter = $this->getAdapter();
        /** @var SelectQuery $queryBuilder */
        $queryBuilder = $adapter->getQueryBuilder('select');
        $func = $queryBuilder->func();
        $conn = $queryBuilder->getConnection();
        $driver = $conn->getDriver();
        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('versions')
            ->where(['version' => '2.2.0'])
            ->execute()
            ->fetchColumn(0);

        $now = (new DateTime())->format(DATE_RFC3339);
        // Si cette version existe, c'est qu'il n'y a pas besoin de ce seed
        if (!$count) {
            $this->insert(
                'versions',
                [
                    'subject' => Configure::read('App.name', 'asalae'),
                    'version' => '2.2.0',
                    'created' => $now,
                ]
            );

            $adapter->getUpdateBuilder()
                ->update('versions')
                ->set(['subject' => 'asalae'])
                ->where(['subject' => 'as@lae'])
                ->execute();

            // TODO - lorsque tous les clients seront en version >= 2.2.0, on peut
            // TODO - sans risque supprimer ce bloc et la commande liée
            $Exec = Utility::get('Exec');
            $Exec->setDefaultStdout(LOGS . 'patch220.log');
            $Exec->async("sleep 5 && " . CAKE_SHELL, 'patch220');
        }

        $supera = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('roles')
            ->where(['code' => RolesTable::CODE_SUPER_ARCHIVIST])
            ->execute()
            ->fetchColumn(0);
        if (!$supera) {
            $rght = $adapter->getSelectBuilder()
                ->select(['rght'])
                ->from('roles')
                ->orderByDesc('rght')
                ->limit(1)
                ->execute()
                ->fetchColumn(0);

            $this->insert(
                'roles',
                [
                    'name' => 'Super archiviste',
                    'active' => true,
                    'created' => $now,
                    'modified' => $now,
                    'org_entity_id' => null,
                    'code' => RolesTable::CODE_SUPER_ARCHIVIST,
                    'parent_id' => null,
                    'lft' => $rght + 1,
                    'rght' => $rght + 2,
                    'hierarchical_view' => true,
                    'description' => null,
                    'agent_type' => 'person',
                ]
            );
            $roleId = $adapter->getSelectBuilder()
                ->select(['id'])
                ->from('roles')
                ->where(['code' => RolesTable::CODE_SUPER_ARCHIVIST])
                ->limit(1)
                ->execute()
                ->fetchColumn(0);
            $typeEntityId = $adapter->getSelectBuilder()
                ->select(['id'])
                ->from('type_entities')
                ->where(['code' => 'SE'])
                ->limit(1)
                ->execute()
                ->fetchColumn(0);
            $this->insert(
                'roles_type_entities',
                [
                    'role_id' => $roleId,
                    'type_entity_id' => $typeEntityId,
                ]
            );
        }

        $admin = $adapter->getSelectBuilder()
            ->select(['name'])
            ->from('roles')
            ->where(['code' => RolesTable::CODE_ADMIN])
            ->execute()
            ->fetchColumn(0);
        if (!$admin) {
            $rght = $adapter->getSelectBuilder()
                ->select(['rght'])
                ->from('roles')
                ->orderByDesc('rght')
                ->limit(1)
                ->execute()
                ->fetchColumn(0);
            $this->insert(
                'roles',
                [
                    'name' => 'Référent technique',
                    'active' => true,
                    'created' => (new DateTime())->format(DATE_RFC3339),
                    'modified' => (new DateTime())->format(DATE_RFC3339),
                    'org_entity_id' => null,
                    'code' => RolesTable::CODE_ADMIN,
                    'parent_id' => null,
                    'lft' => $rght + 1,
                    'rght' => $rght + 2,
                    'hierarchical_view' => false,
                    'description' => null,
                    'agent_type' => 'person',
                ]
            );
            $roleId = $driver->lastInsertId();
            $typeEntityId = $adapter->getSelectBuilder()
                ->select(['id'])
                ->from('type_entities')
                ->where(['code' => 'SA'])
                ->limit(1)
                ->execute()
                ->fetchColumn(0);
            $this->insert(
                'roles_type_entities',
                [
                    'role_id' => $roleId,
                    'type_entity_id' => $typeEntityId,
                ]
            );
        } else {
            if ($admin === 'Administrateur technique') {
                $adapter->getUpdateBuilder()
                    ->update('roles')
                    ->set(['name' => 'Référent technique'])
                    ->where(['name' => 'Administrateur technique'])
                    ->execute();

                $adapter->getUpdateBuilder()
                    ->update('aros')
                    ->set(['alias' => 'Référent technique'])
                    ->where(['alias' => 'Administrateur technique'])
                    ->execute();
            }
        }

        $count = $adapter->getSelectBuilder()
            ->select(['count' => $func->count('*')])
            ->from('crons')
            ->where(['classname' => MailNotifications::class])
            ->execute()
            ->fetchColumn(0);
        if ((int)$count === 0) {
            $this->insert(
                'crons',
                [
                    'name' => __("Notifications par mail"),
                    'description' => __(
                        "Envoi des mails de notification à destination des "
                        . "utilisateurs pour les validations à effectuer, les "
                        . "alertes de jobs en erreur et d'absence de transfert "
                        . "et le récapitulatif des transfers."
                    ),
                    'classname' => MailNotifications::class,
                    'active' => true,
                    'locked' => false,
                    'frequency' => 'P1D',
                    'next' => (new \DateTime('02:00:00'))
                        ->sub(new \DateInterval('P1D'))
                        ->format(DATE_RFC3339),
                    'app_meta' => null,
                    'parallelisable' => true,
                ]
            );
        }
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            Patch016v2110Seed::class,
        ];
    }
}
