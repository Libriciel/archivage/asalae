<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Cake\Core\Configure;
use Cake\Database\Query\SelectQuery;
use Migrations\AbstractSeed;

/**
 * Patch2114 seed.
 *
 * 25e Seed
 */
class Patch025v2114Seed extends AbstractSeed
{
    public const string VERSION = '2.1.14';

    /**
     * @return void
     */
    public function run(): void
    {
        $adapter = $this->getAdapter();
        /** @var SelectQuery $queryBuilder */
        $queryBuilder = $adapter->getQueryBuilder('select');
        $func = $queryBuilder->func();
        $subject = Configure::read('App.name', 'asalae');
        $count = $queryBuilder
            ->select(['count' => $func->count('*')])
            ->from('versions')
            ->where(['subject IN' => ['asalae', 'as@lae'], 'version' => self::VERSION])
            ->execute()
            ->fetchColumn(0);

        // Si cette version existe, c'est qu'il n'y a pas besoin de ce seed
        if ((int)$count > 0) {
            return;
        }

        $this->insert(
            'versions',
            [
                'subject' => $subject,
                'version' => self::VERSION,
                'created' => (new DateTime())->format(DATE_RFC3339),
            ]
        );
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            Patch024v224Seed::class,
        ];
    }
}
