<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Migrations\AbstractMigration;

class Patch2110 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $table = $this->table('transfers');
        $table->addColumn(
            'archive_file_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addForeignKey(
            'archive_file_id',
            'archive_files',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'SET_NULL',
            ]
        );
        $table->update();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $this->table('transfers')
            ->removeColumn('archive_file_id')
            ->update();
    }
}
