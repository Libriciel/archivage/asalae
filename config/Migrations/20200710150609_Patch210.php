<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Model\Entity\DescriptionXmlArchiveFile;
use Asalae\Model\Table\ArchivesTable;
use Asalae\Model\Table\ArchiveUnitsTable;
use Asalae\Model\Table\OrgEntitiesTable;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Form\MessageSchema\MessageSchema;
use AsalaeCore\Form\MessageSchema\Seda02Schema;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Form\MessageSchema\Seda21Schema;
use AsalaeCore\Form\MessageSchema\Seda22Schema;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Translit;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class Patch210 extends AbstractMigration
{
    /**
     * @return void
     * @throws VolumeException
     */
    public function up(): void
    {
        $table = $this->table('archive_keywords');
        $table->addColumn(
            'xml_node_tagname',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->update();

        $loc = TableRegistry::getTableLocator();
        $Archives = $loc->get('Archives');
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        $subquery = $ArchiveKeywords->find()
            ->select(['archive_id' => 'ArchiveUnits.archive_id'])
            ->innerJoinWith('ArchiveUnits')
            ->groupBy(['ArchiveUnits.archive_id']);
        $query = $Archives->find()
            ->where(['Archives.id IN' => $subquery])
            ->contain(['DescriptionXmlArchiveFiles' => ['StoredFiles']]);
        if (!getenv('PHPUNIT')) {
            foreach ($query as $archive) {
                /** @var DescriptionXmlArchiveFile $desc */
                $desc = $archive->get('description_xml_archive_file');
                if (!$desc) {
                    continue;
                }
                $util = new DOMUtility($desc->getDom());
                /** @var Seda02Schema|Seda10Schema|Seda21Schema|Seda22Schema $schema */
                $schema = MessageSchema::getSchemaClassname($util->namespace);
                $keywordContents = $util->xpath->query(
                    '//ns:' . $schema::getTagname('Keyword', 'Archive')
                    . '/ns:' . $schema::getTagname('KeywordContent', 'Archive')
                );
                $this->setKeywordContents($keywordContents, $archive->id);
            }
        }
        $ArchiveKeywords->updateAll(
            ['xml_node_tagname' => 'not_found'],
            ['xml_node_tagname IS' => null]
        );

        $table->changeColumn(
            'xml_node_tagname',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('archives');
        $table->changeColumn(
            'name',
            'text',
            [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('archive_units');
        $table->changeColumn(
            'name',
            'text',
            [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]
        );
        $table->update();

        $ValidationProcess = $loc->get('ValidationProcesses');
        $processes = $ValidationProcess->find()
            ->where(['app_meta LIKE' => '%modified"%'])
            ->all();

        foreach ($processes as $process) {
            $appMeta = json_decode($process->get('app_meta'), true);
            if (isset($appMeta['modified']) && is_string($appMeta['modified'])) {
                unset($appMeta['modified']);
            } elseif (isset($appMeta['modified']) && is_array($appMeta['modified'])) {
                $appMeta['modifieds'] = $appMeta['modified'];
                unset($appMeta['modified']);
            }

            $process->set('app_meta', json_encode($appMeta));
            $ValidationProcess->updateQuery()
                ->set(['app_meta' => $process->get('app_meta')])
                ->where(['id' => $process->get('id')])
                ->execute();
        }

        $ValidationStages = $loc->get('ValidationStages');
        $stages = $ValidationStages->find()
            ->where(['app_meta LIKE' => '%modified"%'])
            ->all();

        foreach ($stages as $stage) {
            $appMeta = json_decode($stage->get('app_meta'), true);
            if (isset($appMeta['modified']) && is_string($appMeta['modified'])) {
                unset($appMeta['modified']);
            } elseif (isset($appMeta['modified']) && is_array($appMeta['modified'])) {
                $appMeta['modifieds'] = $appMeta['modified'];
                unset($appMeta['modified']);
            }
            $stage->set('app_meta', $appMeta);
            $ValidationStages->updateQuery()
                ->set(['app_meta' => $stage->get('app_meta')])
                ->where(['id' => $stage->get('id')])
                ->execute();
        }

        $ArchiveUnits = $loc->get('ArchiveUnits');
        $Archives = $loc->get('Archives');
        $query = $Archives->find();

        /** @var EntityInterface $archive */
        foreach ($query as $archive) {
            $concat = $query->func()->concat(
                [
                    $archive->get('storage_path'),
                    '/original_data/',
                    new IdentifierExpression('ArchiveUnits.name'),
                ]
            );
            $archiveUnits = $ArchiveUnits->find()
                ->select(
                    [
                        'ArchiveUnits.id',
                        'ArchiveUnits.name',
                        'ArchiveUnits.search',
                        'filename' => 'StoredFiles.name',
                    ]
                )
                ->innerJoinWith('ArchiveBinaries')
                ->innerJoinWith('ArchiveBinaries.StoredFiles')
                ->where(
                    [
                        'ArchiveUnits.archive_id' => $archive->id,
                        'ArchiveUnits.xml_node_tagname LIKE' => 'Document[%]',
                        'ArchiveBinaries.original_data_id IS' => null,
                        'StoredFiles.name !=' => $concat,
                    ]
                );
            $len = strlen($archive->get('storage_path') . '/original_data/');
            foreach ($archiveUnits as $archiveUnit) {
                $name = substr($archiveUnit->get('filename'), $len);
                $search = str_replace(
                    '"' . Translit::asciiToLower($archiveUnit->get('name')) . '"',
                    '"' . Translit::asciiToLower($name) . '"',
                    $archiveUnit->get('search')
                );
                $ArchiveUnits->updateAll(
                    [
                        'name' => $name,
                        'search' => $search,
                    ],
                    ['id' => $archiveUnit->id]
                );
            }
        }

        $table = $this->table('archive_units');
        $table->addColumn(
            'full_search',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateFullSearchField([]);

        $table = $this->table('users');
        $table->addColumn(
            'is_validator',
            'boolean',
            [
                'default' => true,
                'null' => false,
            ]
        );
        $table->addColumn(
            'use_cert',
            'boolean',
            [
                'default' => false,
                'null' => false,
            ]
        );
        $table->update();

        $Archives = $loc->get('Archives');
        $subquery = $Archives->find()
            ->select(['id'])
            ->where(
                [
                    'state IN' => [
                        ArchivesTable::S_DESCRIBING,
                        ArchivesTable::S_STORING,
                        ArchivesTable::S_MANAGING,
                    ],
                ]
            );
        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateAll(
            ['state' => $ArchiveUnits->initialState],
            ['archive_id IN' => $subquery]
        );

        $Crons = $loc->get('Crons');
        foreach ($Crons->find()->disableHydration() as $cron) {
            $classname = str_replace('\\Shell\\Cron\\', '\\Cron\\', $cron['classname']);
            $Crons->updateAll(['classname' => $classname], ['id' => $cron['id']]);
        }

        $table = $this->table('delivery_requests');
        $table->addColumn(
            'archive_units_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'original_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'original_size',
            'biginteger',
            [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ]
        );
        $table->update();

        $DeliveryRequests = $loc->get('DeliveryRequests');
        $ArchiveUnitsDeliveryRequests = $loc->get('ArchiveUnitsDeliveryRequests');
        $query = $DeliveryRequests->find()->select(['id'])->disableHydration();
        foreach ($query as $deliveryRequest) {
            $volumetry = $ArchiveUnitsDeliveryRequests->find()
                ->select(
                    [
                        'archive_units_count' => $query->func()->count('*'),
                        'original_count' => $query->func()->sum('ArchiveUnits.original_total_count'),
                        'original_size' => $query->func()->sum('ArchiveUnits.original_total_size'),
                    ]
                )
                ->innerJoinWith('ArchiveUnits')
                ->where(['delivery_request_id' => $deliveryRequest['id']])
                ->disableHydration()
                ->all()
                ->toArray()
            [0];
            $DeliveryRequests->updateAll($volumetry, ['id' => $deliveryRequest['id']]);
        }

        $table = $this->table('restitution_requests');
        $table->addColumn(
            'identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'comment',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'originating_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'state',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'last_state_update',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'states_history',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'archive_units_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'original_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'original_size',
            'biginteger',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created_user_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archival_agency_id',
            'org_entities',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'originating_agency_id',
            'org_entities',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'created_user_id',
            'users',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('archive_units_restitution_requests');
        $table->addColumn(
            'archive_unit_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'restitution_request_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archive_unit_id',
            'archive_units',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'restitution_request_id',
            'restitution_requests',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('restitutions');
        $table->addColumn(
            'identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'restitution_request_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'state',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'comment',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'archive_units_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'original_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'original_size',
            'biginteger',
            [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->create();

        $table = $this->table('service_levels');
        $table->addColumn(
            'convert_preservation',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'convert_dissemination',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('archive_binary_conversions');
        $table->addColumn(
            'archive_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archive_binary_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'type',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'format',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'audio_codec',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'video_codec',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archive_id',
            'archives',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'archive_binary_id',
            'archive_binaries',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('restitutions');
        $table->addForeignKey(
            'restitution_request_id',
            'restitution_requests',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->update();

        $table = $this->table('archive_binary_conversions');
        $table->changeColumn(
            'audio_codec',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->changeColumn(
            'video_codec',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('event_logs');
        $table->addColumn(
            'agent_serialized',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('archiving_systems');
        $table->addColumn(
            'org_entity_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'name',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'description',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'url',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'username',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'password',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'use_proxy',
            'boolean',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'org_entity_id',
            'org_entities',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('archiving_systems');
        $table->removeColumn('password');
        $table->update();

        $table->addColumn(
            'active',
            'boolean',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'password',
            'binary',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->changeColumn(
            'org_entity_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'ssl_verify_peer',
            'boolean',
            [
                'default' => true,
                'null' => false,
            ]
        );
        $table->addColumn(
            'ssl_verify_peer_name',
            'boolean',
            [
                'default' => true,
                'null' => false,
            ]
        );
        $table->addColumn(
            'ssl_verify_depth',
            'integer',
            [
                'default' => 5,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'ssl_verify_host',
            'boolean',
            [
                'default' => true,
                'null' => false,
            ]
        );
        $table->addColumn(
            'ssl_cafile',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('outgoing_transfer_requests');
        $table->addColumn(
            'identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'comment',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archiving_system_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archival_agency_identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archival_agency_name',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'transferring_agency_identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'transferring_agency_name',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'agreement_identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'profile_identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'state',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'last_state_update',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'states_history',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'archives_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archive_units_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'original_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'original_size',
            'biginteger',
            [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created_user_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archival_agency_id',
            'org_entities',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'archiving_system_id',
            'archiving_systems',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'created_user_id',
            'users',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        $query = $OrgEntities->find()
            ->select(['OrgEntities.id'])
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES])
            ->disableHydration();

        foreach ($query as $orgEntity) {
            $OrgEntities->initCounters($orgEntity['id']);
        }

        $table = $this->table('archive_units_outgoing_transfer_requests');
        $table->addColumn(
            'archive_unit_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'otr_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archive_unit_id',
            'archive_units',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'otr_id',
            'outgoing_transfer_requests',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('outgoing_transfer_requests');
        $table->removeColumn('last_state_update');
        $table->update();
        $table->addColumn(
            'last_state_update',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('outgoing_transfers');
        $table->addColumn(
            'identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'comment',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'outgoing_transfer_request_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archive_unit_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'state',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'last_state_update',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'states_history',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'outgoing_transfer_request_id',
            'outgoing_transfer_requests',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'archive_unit_id',
            'archive_units',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('outgoing_transfer_requests');
        $table->changeColumn(
            'states_history',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('archives');
        $table->renameColumn('transfered_size', 'transferred_size');
        $table->renameColumn('transfered_count', 'transferred_count');
        $table->update();

        $this->table('message_indicators')
            ->addColumn(
                'message_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'message_type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'message_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'accepted',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'derogation',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'validation_duration',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'originating_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_files_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_files_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addForeignKey(
                'archival_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'transferring_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'originating_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                ]
            )
            ->create();

        $this->table('archive_indicators')
            ->addColumn(
                'archive_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'originating_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_unit_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_files_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_files_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timestamp_files_count',
                'integer',
                [
                    'default' => 0,
                    'limit' => 11,
                    'null' => true,
                ]
            )
            ->addColumn(
                'timestamp_files_size',
                'biginteger',
                [
                    'default' => 0,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'preservation_files_count',
                'integer',
                [
                    'default' => 0,
                    'limit' => 11,
                    'null' => true,
                ]
            )
            ->addColumn(
                'preservation_files_size',
                'biginteger',
                [
                    'default' => 0,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'dissemination_files_count',
                'integer',
                [
                    'default' => 0,
                    'limit' => 11,
                    'null' => true,
                ]
            )
            ->addColumn(
                'dissemination_files_size',
                'biginteger',
                [
                    'default' => 0,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'management_files_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ]
            )
            ->addColumn(
                'management_files_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addForeignKey(
                'archival_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'transferring_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'originating_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                ]
            )
            ->create();

        $table = $this->table('batch_treatments');
        $table->addColumn(
            'archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'user_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'state',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'last_state_update',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'states_history',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'data',
            'text',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archival_agency_id',
            'org_entities',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'user_id',
            'users',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('archive_units_batch_treatments');
        $table->addColumn(
            'archive_unit_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'batch_treatment_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archive_unit_id',
            'archive_units',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'batch_treatment_id',
            'batch_treatments',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('archive_binaries');
        $table->addColumn(
            'in_rgi',
            'boolean',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'app_meta',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $this->table('archive_indicators')
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addForeignKey(
                'agreement_id',
                'agreements',
                'id',
                [
                    'update' => 'CASCADE',
                ]
            )
            ->addColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addForeignKey(
                'profile_id',
                'profiles',
                'id',
                [
                    'update' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('message_indicators')
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addForeignKey(
                'agreement_id',
                'agreements',
                'id',
                [
                    'update' => 'CASCADE',
                ]
            )
            ->addColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addForeignKey(
                'profile_id',
                'profiles',
                'id',
                [
                    'update' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('archive_indicators')
            ->changeColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->changeColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->update();
        $this->table('message_indicators')
            ->changeColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->changeColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->update();

        $table = $this->table('archive_indicators');
        $table->renameColumn('archive_unit_count', 'units_count');
        $table->renameColumn('original_files_count', 'original_count');
        $table->renameColumn('original_files_size', 'original_size');
        $table->renameColumn('timestamp_files_count', 'timestamp_count');
        $table->renameColumn('timestamp_files_size', 'timestamp_size');
        $table->renameColumn('preservation_files_count', 'preservation_count');
        $table->renameColumn('preservation_files_size', 'preservation_size');
        $table->renameColumn('dissemination_files_count', 'dissemination_count');
        $table->renameColumn('dissemination_files_size', 'dissemination_size');
        $table->removeColumn('management_files_count');
        $table->removeColumn('management_files_size');
        $table->update();

        $table = $this->table('auth_sub_urls');
        $table->addColumn(
            'auth_url_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'url',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'auth_url_id',
            'auth_urls',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('archives');
        $table->removeColumn('files_count');
        $table->update();

        $table = $this->table('outgoing_transfers');
        $table->addColumn(
            'error_msg',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('mails');
        $table->addColumn(
            'serialized',
            'binary',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->create();

        $loc->get('Archives')->updateAll(['state' => ArchivesTable::S_RESTITUTED], ['state' => 'restitued']);

        $table = $this->table('outgoing_transfer_requests');
        $table->addColumn(
            'originating_agency_identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'originating_agency_name',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->update();

        $loc->clear();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $loc = TableRegistry::getTableLocator();

        $table = $this->table('archive_keywords');
        $table->removeColumn('xml_node_tagname');
        $table->update();

        $ValidationProcess = $loc->get('ValidationProcesses');
        $processes = $ValidationProcess->find()
            ->where(['app_meta LIKE' => '%modifieds%'])
            ->all();

        foreach ($processes as $process) {
            $newAppMeta = str_replace('modifieds', 'modified', $process->get('app_meta'));
            $process->set('app_meta', $newAppMeta);
            $ValidationProcess->updateQuery()
                ->set(['app_meta' => $newAppMeta])
                ->where(['id' => $process->get('id')])
                ->execute();
        }

        $ValidationStages = $loc->get('ValidationStages');
        $stages = $ValidationStages->find()
            ->where(['app_meta LIKE' => '%modifieds%'])
            ->all();

        foreach ($stages as $stage) {
            $newAppMeta = str_replace('modifieds', 'modified', $stage->get('app_meta'));
            $stage->set('app_meta', $newAppMeta);
            $ValidationStages->updateQuery()
                ->set(['app_meta' => $newAppMeta])
                ->where(['id' => $stage->get('id')])
                ->execute();
        }

        $table = $this->table('archive_units');
        $table->removeColumn('full_search');
        $table->update();

        $table = $this->table('users');
        $table->removeColumn('is_validator');
        $table->removeColumn('use_cert');
        $table->update();

        /** @var ArchiveUnitsTable $ArchiveUnits */
        $ArchiveUnits = $loc->get('ArchiveUnits');
        $ArchiveUnits->updateAll(
            ['state' => ArchiveUnitsTable::S_AVAILABLE],
            ['state' => $ArchiveUnits->initialState]
        );

        $Crons = $loc->get('Crons');
        foreach ($Crons->find()->disableHydration() as $cron) {
            $classname = str_replace('\\Cron\\', '\\Shell\\Cron\\', $cron['classname']);
            $Crons->updateAll(['classname' => $classname], ['id' => $cron['id']]);
        }

        $table = $this->table('delivery_requests');
        $table->removeColumn('archive_units_count');
        $table->removeColumn('original_count');
        $table->removeColumn('original_size');
        $table->update();

        $table = $this->table('archive_units_restitution_requests');
        $table->drop();
        $table->save();

        $table = $this->table('restitution_requests');
        $table->drop();
        $table->save();

        $table = $this->table('restitutions');
        $table->drop();
        $table->update();

        $table = $this->table('service_levels');
        $table->removeColumn('convert_preservation');
        $table->removeColumn('convert_dissemination');
        $table->update();

        $table = $this->table('archive_binary_conversions');
        $table->drop();
        $table->update();

        $table = $this->table('restitutions');
        $table->dropForeignKey('restitution_request_id');
        $table->update();

        $table = $this->table('event_logs');
        $table->removeColumn('agent_serialized');
        $table->update();

        $table = $this->table('archiving_systems');
        $table->drop();
        $table->update();

        $table = $this->table('archiving_systems');
        $table->removeColumn('active');
        $table->removeColumn('ssl_verify_peer');
        $table->removeColumn('ssl_verify_peer_name');
        $table->removeColumn('ssl_verify_depth');
        $table->removeColumn('ssl_verify_host');
        $table->removeColumn('ssl_cafile');
        $table->update();

        $table = $this->table('outgoing_transfer_requests');
        $table->drop();
        $table->update();

        $table = $this->table('archive_units_outgoing_transfer_requests');
        $table->drop();
        $table->update();

        $table = $this->table('outgoing_transfers');
        $table->drop();
        $table->update();

        $table = $this->table('archives');
        $table->renameColumn('transferred_size', 'transfered_size');
        $table->renameColumn('transferred_count', 'transfered_count');
        $table->update();

        $this->table('message_indicators')
            ->drop()
            ->save();

        $this->table('archive_indicators')
            ->drop()
            ->save();

        $table = $this->table('archive_units_batch_treatments');
        $table->drop();
        $table->update();

        $table = $this->table('batch_treatments');
        $table->drop();
        $table->update();

        $table = $this->table('archive_binaries');
        $table->removeColumn('in_rgi');
        $table->removeColumn('app_meta');
        $table->update();

        $this->table('archive_indicators')
            ->removeColumn('agreement_id')
            ->removeColumn('profile_id')
            ->update();

        $this->table('message_indicators')
            ->removeColumn('agreement_id')
            ->removeColumn('profile_id')
            ->update();

        $table = $this->table('archive_indicators');
        $table->renameColumn('units_count', 'archive_unit_count');
        $table->renameColumn('original_count', 'original_files_count');
        $table->renameColumn('original_size', 'original_files_size');
        $table->renameColumn('timestamp_count', 'timestamp_files_count');
        $table->renameColumn('timestamp_size', 'timestamp_files_size');
        $table->renameColumn('preservation_count', 'preservation_files_count');
        $table->renameColumn('preservation_size', 'preservation_files_size');
        $table->renameColumn('dissemination_count', 'dissemination_files_count');
        $table->renameColumn('dissemination_size', 'dissemination_files_size');
        $table->addColumn(
            'management_files_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'management_files_size',
            'biginteger',
            [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('auth_sub_urls');
        $table->drop();
        $table->update();

        $table = $this->table('archives');
        $table->addColumn(
            'files_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('outgoing_transfers');
        $table->removeColumn('error_msg');
        $table->update();

        $table = $this->table('mails');
        $table->drop();
        $table->update();

        $table = $this->table('outgoing_transfer_requests');
        $table->removeColumn('originating_agency_identifier');
        $table->removeColumn('originating_agency_name');
        $table->update();
    }

    /**
     * @param DOMNodeList $keywordContents
     * @param int         $archive_id
     * @return void
     */
    private function setKeywordContents(DOMNodeList $keywordContents, int $archive_id)
    {
        $loc = TableRegistry::getTableLocator();
        $ArchiveKeywords = $loc->get('ArchiveKeywords');
        /** @var DOMElement $keywordContent */
        foreach ($keywordContents as $keywordContent) {
            $keyword = $ArchiveKeywords->find()
                ->select(['ArchiveKeywords.id'])
                ->innerJoinWith('ArchiveUnits')
                ->where(
                    [
                        'ArchiveUnits.archive_id' => $archive_id,
                        'ArchiveKeywords.content' => trim(
                            $keywordContent->nodeValue
                        ),
                    ]
                )
                ->first();
            if ($keyword) {
                /** @var DOMElement $keywordNode */
                $keywordNode = $keywordContent->parentNode;
                $index = 0;
                foreach ($keywordNode->parentNode->childNodes as $child) {
                    if ($child instanceof DOMElement && $child->tagName === $keywordNode->tagName) {
                        $index++;
                    }
                    if ($child === $keywordNode) {
                        break;
                    }
                }
                $ArchiveKeywords->updateAll(
                    ['xml_node_tagname' => $keywordNode->tagName . '[' . $index . ']'],
                    ['id' => $keyword->id]
                );
            }
        }
    }
}
