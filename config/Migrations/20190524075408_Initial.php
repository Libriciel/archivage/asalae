<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Adldap\Schemas\ActiveDirectory;
use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $this->table('access_rule_codes')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'code',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('access_rules')
            ->addColumn(
                'access_rule_code_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'start_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'end_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'access_rule_code_id',
                ]
            )
            ->create();

        $this->table('access_tokens')
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'expiration_date',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('acos')
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'model',
                'string',
                [
                    'default' => '',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'alias',
                'string',
                [
                    'default' => '',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->addIndex(
                [
                    'alias',
                ]
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ]
            )
            ->create();

        $this->table('agreements')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'allow_all_transferring_agencies',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'allow_all_originating_agencies',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'allow_all_service_levels',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'auto_validate',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'proper_chain_id',
                'integer',
                [
                    'comment' => 'circuit de validation des transferts conformes',
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'improper_chain_id',
                'integer',
                [
                    'comment' => 'circuit de validation des transferts non conformes',
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'date_begin',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'date_end',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'default_agreement',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'allowed_formats',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'max_tranfers',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'transfer_period',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'max_size_per_transfer',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'allow_all_profiles',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'improper_chain_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'proper_chain_id',
                ]
            )
            ->create();

        $this->table('agreements_originating_agencies')
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('agreements_profiles')
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'profile_id',
                ]
            )
            ->create();

        $this->table('agreements_service_levels')
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'service_level_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'service_level_id',
                ]
            )
            ->create();

        $this->table('agreements_transferring_agencies')
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('appraisal_rule_codes')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'code',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('appraisal_rules')
            ->addColumn(
                'appraisal_rule_code_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'final_action_code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'start_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'end_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'appraisal_rule_code_id',
                ]
            )
            ->create();

        $this->table('archive_binaries')
            ->addColumn(
                'is_integrity_ok',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'filename',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'extension',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stored_file_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_data_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'original_data_id',
                ]
            )
            ->addIndex(
                [
                    'stored_file_id',
                ]
            )
            ->addIndex(
                [
                    'filename',
                ]
            )
            ->create();

        $this->table('archive_binaries_archive_units')
            ->addColumn(
                'archive_binary_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_binary_id',
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->create();

        $this->table('archive_binaries_technical_archive_units')
            ->addColumn(
                'archive_binary_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'technical_archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_binary_id',
                ]
            )
            ->addIndex(
                [
                    'technical_archive_unit_id',
                ]
            )
            ->create();

        $this->table('archive_descriptions')
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'access_rule_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'file_plan_position',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'oldest_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'latest_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'access_rule_id',
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->create();

        $this->table('archive_files')
            ->addColumn(
                'archive_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'stored_file_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'is_integrity_ok',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'filename',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_id',
                ]
            )
            ->addIndex(
                [
                    'stored_file_id',
                ]
            )
            ->create();

        $this->table('archive_keywords')
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'access_rule_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'content',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'reference',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'access_rule_id',
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->addIndex(
                [
                    'content',
                ]
            )
            ->create();

        $this->table('archive_units')
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'access_rule_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'appraisal_rule_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'originating_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_local_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_total_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'xml_node_tagname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_total_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_local_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'access_rule_id',
                ]
            )
            ->addIndex(
                [
                    'appraisal_rule_id',
                ]
            )
            ->addIndex(
                [
                    'archive_id',
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->addIndex(
                [
                    'state',
                ]
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ]
            )
            ->create();

        $this->table('archives')
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'is_integrity_ok',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'originating_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_operator_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'transfer_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'service_level_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'access_rule_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'appraisal_rule_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'originating_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'secure_data_space_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'storage_path',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'oldest_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'latest_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'transfered_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'management_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'preservation_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'dissemination_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timestamp_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transfered_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'management_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'preservation_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'dissemination_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timestamp_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'xml_node_tagname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'transfer_id',
                    'xml_node_tagname',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'access_rule_id',
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'appraisal_rule_id',
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'originating_agency_id',
                ]
            )
            ->addIndex(
                [
                    'profile_id',
                ]
            )
            ->addIndex(
                [
                    'secure_data_space_id',
                ]
            )
            ->addIndex(
                [
                    'service_level_id',
                ]
            )
            ->addIndex(
                [
                    'transfer_id',
                ]
            )
            ->addIndex(
                [
                    'transferring_agency_id',
                ]
            )
            ->addIndex(
                [
                    'transferring_operator_id',
                ]
            )
            ->create();

        $this->table('aros')
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'model',
                'string',
                [
                    'default' => '',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'alias',
                'string',
                [
                    'default' => '',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'alias',
                ]
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ]
            )
            ->create();

        $this->table('aros_acos')
            ->addColumn(
                'aro_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'aco_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                '_create',
                'string',
                [
                    'default' => '0',
                    'limit' => 2,
                    'null' => false,
                ]
            )
            ->addColumn(
                '_read',
                'string',
                [
                    'default' => '0',
                    'limit' => 2,
                    'null' => false,
                ]
            )
            ->addColumn(
                '_update',
                'string',
                [
                    'default' => '0',
                    'limit' => 2,
                    'null' => false,
                ]
            )
            ->addColumn(
                '_delete',
                'string',
                [
                    'default' => '0',
                    'limit' => 2,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'aco_id',
                ]
            )
            ->addIndex(
                [
                    'aro_id',
                ]
            )
            ->create();

        $this->table('auth_urls')
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'url',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'expire',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->create();

        $this->table('beanstalk_jobs')
            ->addColumn(
                'jobid',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'tube',
                'string',
                [
                    'default' => 'default',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'priority',
                'integer',
                [
                    'default' => '1024',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'last_status',
                'string',
                [
                    'default' => 'ready',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'delay',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ttr',
                'integer',
                [
                    'default' => '60',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'errors',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('beanstalk_workers')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'tube',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'path',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'pid',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'last_launch',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hostname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->create();

        $this->table('configurations')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'setting',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'name',
                ]
            )
            ->create();

        $this->table('conversion_policies')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'media',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'usage',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'method',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'params',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'media',
                    'usage',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('counters')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'definition_mask',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'sequence_reset_mask',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'sequence_reset_value',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'sequence_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'sequence_id',
                ]
            )
            ->create();

        $this->table('cron_executions')
            ->addColumn(
                'cron_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'date_begin',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'date_end',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'report',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'cron_id',
                ]
            )
            ->create();

        $this->table('crons')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'classname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'locked',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'frequency',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'next',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'parallelisable',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->create();

        $this->table('eaccpfs')
            ->addColumn(
                'record_id',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'entity_id',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'entity_type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'data',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'agency_name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'from_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'to_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('event_logs')
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'outcome',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'outcome_detail',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'cron_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'object_model',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'object_foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'life_cycle',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'exported',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'object_serialized',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'cron_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->addIndex(
                [
                    'type',
                ]
            )
            ->addIndex(
                [
                    'created',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                    'cron_id',
                ]
            )
            ->create();

        $this->table('file_extensions')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('file_extensions_pronoms')
            ->addColumn(
                'file_extension_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'pronom_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'file_extension_id',
                ]
            )
            ->addIndex(
                [
                    'pronom_id',
                ]
            )
            ->create();

        $this->table('fileuploads')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'path',
                'string',
                [
                    'default' => null,
                    'limit' => 2048,
                    'null' => true,
                ]
            )
            ->addColumn(
                'size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hash',
                'string',
                [
                    'default' => null,
                    'limit' => 4000,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hash_algo',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'valid',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 50,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 128,
                    'null' => true,
                ]
            )
            ->addColumn(
                'locked',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('fileuploads_profiles')
            ->addColumn(
                'fileupload_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'fileupload_id',
                ]
            )
            ->addIndex(
                [
                    'profile_id',
                ]
            )
            ->create();

        $this->table('filters')
            ->addColumn(
                'saved_filter_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'key',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'value',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'saved_filter_id',
                ]
            )
            ->create();

        $this->table('keyword_lists')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'version',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('keywords')
            ->addColumn(
                'keyword_list_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'version',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_meta',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'exact_match',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'change_note',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'keyword_list_id',
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->create();

        $this->table('ldaps')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'host',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'port',
                'integer',
                [
                    'default' => '389',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_query_login',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'user_query_password',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ldap_root_search',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_login_attribute',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ldap_users_filter',
                'string',
                [
                    'default' => null,
                    'limit' => 65535,
                    'null' => true,
                ]
            )
            ->addColumn(
                'account_prefix',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'account_suffix',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'use_proxy',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'use_ssl',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'use_tls',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'user_name_attribute',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'user_mail_attribute',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('mediainfo_audios')
            ->addColumn(
                'mediainfo_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_info',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mode_extension',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_settings_endianness',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'codec_id',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_rate_mode',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'channel_s_',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'channel_positions',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'sampling_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'frame_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_depth',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'compression_mode',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'delay_relative_to_video',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stream_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'title',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'language',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                '_default',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'forced',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'mediainfo_id',
                ]
            )
            ->create();

        $this->table('mediainfo_images')
            ->addColumn(
                'mediainfo_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_info',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'width',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'height',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_depth',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'compression_mode',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stream_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'mediainfo_id',
                ]
            )
            ->create();

        $this->table('mediainfo_texts')
            ->addColumn(
                'mediainfo_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'codec_id',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'codec_id_info',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'count_of_elements',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stream_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'title',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'language',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                '_default',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'forced',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'mediainfo_id',
                ]
            )
            ->create();

        $this->table('mediainfo_videos')
            ->addColumn(
                'mediainfo_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_info',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_profile',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_settings_cabac',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_settings_reframes',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'codec_id',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'width',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'height',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'display_aspect_ratio',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'frame_rate_mode',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'frame_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'color_space',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'chroma_subsampling',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_depth',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'scan_type',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bits_pixel_frame_',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stream_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'title',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'writing_library',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'encoding_settings',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'language',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                '_default',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'forced',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'color_range',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'color_primaries',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'transfer_characteristics',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'matrix_coefficients',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'mediainfo_id',
                ]
            )
            ->create();

        $this->table('mediainfos')
            ->addColumn(
                'fileupload_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'unique_id',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'complete_name',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_version',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'file_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'overall_bit_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'movie_name',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'encoded_date',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'writing_application',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'writing_library',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'fileupload_id',
                ]
            )
            ->create();

        $this->table('notifications')
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'text',
                'string',
                [
                    'default' => null,
                    'limit' => 2048,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'css_class',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('org_entities')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'timestamper_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'default_secure_data_space_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'is_main_archival_agency',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'default_secure_data_space_id',
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->addIndex(
                [
                    'timestamper_id',
                ]
            )
            ->addIndex(
                [
                    'type_entity_id',
                ]
            )
            ->addIndex(
                [
                    'id',
                    'name',
                ]
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ]
            )
            ->create();

        $this->table('org_entities_timestampers')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timestamper_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'timestamper_id',
                ]
            )
            ->create();

        $this->table('profiles')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('pronoms')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'puid',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'puid',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('roles')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hierarchical_view',
                'boolean',
                [
                    'comment' => 'autorise ce rôle à voir les éléments de ses sous-entités',
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'code',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->create();

        $this->table('roles_type_entities')
            ->addColumn(
                'role_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'role_id',
                ]
            )
            ->addIndex(
                [
                    'type_entity_id',
                ]
            )
            ->create();

        $this->table('saved_filters')
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'controller',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'action',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('secure_data_spaces')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('sequences')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'value',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('service_levels')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ts_msg_id',
                'integer',
                [
                    'comment' => 'reference org_entities_timestampers',
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ts_pjs_id',
                'integer',
                [
                    'comment' => 'reference org_entities_timestampers',
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ts_conv_id',
                'integer',
                [
                    'comment' => 'reference org_entities_timestampers',
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'default_level',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'secure_data_space_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'secure_data_space_id',
                ]
            )
            ->addIndex(
                [
                    'ts_conv_id',
                ]
            )
            ->addIndex(
                [
                    'ts_msg_id',
                ]
            )
            ->addIndex(
                [
                    'ts_pjs_id',
                ]
            )
            ->create();

        $this->table('sessions', ['id' => false, 'primary_key' => ['id']])
            ->addColumn(
                'id',
                'string',
                [
                    'default' => null,
                    'limit' => 40,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'data',
                'binary',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'expires',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'token',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('siegfrieds')
            ->addColumn(
                'fileupload_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'pronom',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'version',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'basis',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'warning',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'fileupload_id',
                ]
            )
            ->create();

        $this->table('stored_files')
            ->addColumn(
                'secure_data_space_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'hash',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'hash_algo',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'secure_data_space_id',
                ]
            )
            ->create();

        $this->table('stored_files_volumes')
            ->addColumn(
                'storage_ref',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'volume_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'stored_file_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'stored_file_id',
                ]
            )
            ->addIndex(
                [
                    'volume_id',
                ]
            )
            ->create();

        $this->table('technical_archive_units')
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'technical_archive_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archival_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'oldest_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'latest_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'original_local_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_local_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_total_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_total_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'xml_node_tagname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->addIndex(
                [
                    'technical_archive_id',
                ]
            )
            ->create();

        $this->table('technical_archives')
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'is_integrity_ok',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'secure_data_space_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'storage_path',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timestamp_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timestamp_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'management_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'management_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'secure_data_space_id',
                ]
            )
            ->addIndex(
                [
                    'type',
                ]
            )
            ->addIndex(
                [
                    'created',
                ]
            )
            ->create();

        $this->table('timestampers')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'fields',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('transfer_attachments')
            ->addColumn(
                'transfer_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'filename',
                'string',
                [
                    'default' => null,
                    'limit' => 1024,
                    'null' => false,
                ]
            )
            ->addColumn(
                'size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'hash',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hash_algo',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'virus_name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'valid',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'extension',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'transfer_id',
                    'filename',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'transfer_id',
                ]
            )
            ->create();

        $this->table('transfer_errors')
            ->addColumn(
                'transfer_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'level',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'code',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'message',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'transfer_id',
                ]
            )
            ->create();

        $this->table('transfers')
            ->addColumn(
                'transfer_comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'transfer_date',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transfer_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'message_version',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state_history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'is_conform',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'is_modified',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'is_accepted',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'filename',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'data_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created_user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'service_level_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'created_user_id',
                ]
            )
            ->addIndex(
                [
                    'profile_id',
                ]
            )
            ->addIndex(
                [
                    'service_level_id',
                ]
            )
            ->addIndex(
                [
                    'transferring_agency_id',
                ]
            )
            ->addIndex(
                [
                    'state',
                    'is_conform',
                    'is_modified',
                    'is_accepted',
                ]
            )
            ->create();

        $this->table('type_entities')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'code',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('users')
            ->addColumn(
                'username',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'password',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'cookies',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'menu',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'high_contrast',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'role_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'email',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ldap_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'ldap_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'role_id',
                ]
            )
            ->create();

        $this->table('validation_actors')
            ->addColumn(
                'validation_stage_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_key',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'validation_stage_id',
                ]
            )
            ->create();

        $this->table('validation_chains')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'name',
                    'org_entity_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('validation_histories')
            ->addColumn(
                'validation_process_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'validation_actor_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'action',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'current_step',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'validation_actor_id',
                ]
            )
            ->addIndex(
                [
                    'validation_process_id',
                ]
            )
            ->create();

        $this->table('validation_processes')
            ->addColumn(
                'validation_chain_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'current_stage_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_subject_type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_subject_key',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'processed',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'validated',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'current_step',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'current_stage_id',
                ]
            )
            ->addIndex(
                [
                    'validation_chain_id',
                ]
            )
            ->create();

        $this->table('validation_stages')
            ->addColumn(
                'validation_chain_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'all_actors_to_complete',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ord',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'validation_chain_id',
                ]
            )
            ->create();

        $this->table('versions')
            ->addColumn(
                'subject',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'version',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'subject',
                    'version',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('volumes')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'fields',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'driver',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'date_begin',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'date_end',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'alert_date',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'alert_rate',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'disk_usage',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'max_disk_usage',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'secure_data_space_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'secure_data_space_id',
                ]
            )
            ->create();

        $this->table('webservices')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'username',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'password',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'homepage',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'callback',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'client_id',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'client_secret',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->create();

        $this->table('access_rule_codes')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('access_rules')
            ->addForeignKey(
                'access_rule_code_id',
                'access_rule_codes',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('access_tokens')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('acos')
            ->addForeignKey(
                'parent_id',
                'acos',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('agreements')
            ->addForeignKey(
                'improper_chain_id',
                'validation_chains',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'proper_chain_id',
                'validation_chains',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('agreements_originating_agencies')
            ->addForeignKey(
                'agreement_id',
                'agreements',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('agreements_profiles')
            ->addForeignKey(
                'agreement_id',
                'agreements',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'profile_id',
                'profiles',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('agreements_service_levels')
            ->addForeignKey(
                'agreement_id',
                'agreements',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'service_level_id',
                'service_levels',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('agreements_transferring_agencies')
            ->addForeignKey(
                'agreement_id',
                'agreements',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('appraisal_rule_codes')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('appraisal_rules')
            ->addForeignKey(
                'appraisal_rule_code_id',
                'appraisal_rule_codes',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('archive_binaries')
            ->addForeignKey(
                'original_data_id',
                'archive_binaries',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'stored_file_id',
                'stored_files',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('archive_binaries_archive_units')
            ->addForeignKey(
                'archive_binary_id',
                'archive_binaries',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'archive_unit_id',
                'archive_units',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('archive_binaries_technical_archive_units')
            ->addForeignKey(
                'archive_binary_id',
                'archive_binaries',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'technical_archive_unit_id',
                'technical_archive_units',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('archive_descriptions')
            ->addForeignKey(
                'access_rule_id',
                'access_rules',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'archive_unit_id',
                'archive_units',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('archive_files')
            ->addForeignKey(
                'archive_id',
                'archives',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'stored_file_id',
                'stored_files',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('archive_keywords')
            ->addForeignKey(
                'access_rule_id',
                'access_rules',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'archive_unit_id',
                'archive_units',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('archive_units')
            ->addForeignKey(
                'access_rule_id',
                'access_rules',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'appraisal_rule_id',
                'appraisal_rules',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'archive_id',
                'archives',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'parent_id',
                'archive_units',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('archives')
            ->addForeignKey(
                'access_rule_id',
                'access_rules',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'agreement_id',
                'agreements',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'appraisal_rule_id',
                'appraisal_rules',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'archival_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'originating_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'profile_id',
                'profiles',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'secure_data_space_id',
                'secure_data_spaces',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'service_level_id',
                'service_levels',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'transfer_id',
                'transfers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'transferring_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'transferring_operator_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('aros_acos')
            ->addForeignKey(
                'aco_id',
                'acos',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'aro_id',
                'aros',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('beanstalk_jobs')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET_NULL',
                ]
            )
            ->update();

        $this->table('configurations')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('conversion_policies')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('counters')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'sequence_id',
                'sequences',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('cron_executions')
            ->addForeignKey(
                'cron_id',
                'crons',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('eaccpfs')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('event_logs')
            ->addForeignKey(
                'cron_id',
                'crons',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('file_extensions_pronoms')
            ->addForeignKey(
                'file_extension_id',
                'file_extensions',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'pronom_id',
                'pronoms',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('fileuploads')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET_NULL',
                ]
            )
            ->update();

        $this->table('fileuploads_profiles')
            ->addForeignKey(
                'fileupload_id',
                'fileuploads',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'profile_id',
                'profiles',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('filters')
            ->addForeignKey(
                'saved_filter_id',
                'saved_filters',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('keyword_lists')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('keywords')
            ->addForeignKey(
                'keyword_list_id',
                'keyword_lists',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'parent_id',
                'keywords',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('ldaps')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('mediainfo_audios')
            ->addForeignKey(
                'mediainfo_id',
                'mediainfos',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('mediainfo_images')
            ->addForeignKey(
                'mediainfo_id',
                'mediainfos',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('mediainfo_texts')
            ->addForeignKey(
                'mediainfo_id',
                'mediainfos',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('mediainfo_videos')
            ->addForeignKey(
                'mediainfo_id',
                'mediainfos',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('mediainfos')
            ->addForeignKey(
                'fileupload_id',
                'fileuploads',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('notifications')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('org_entities')
            ->addForeignKey(
                'parent_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'timestamper_id',
                'timestampers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'type_entity_id',
                'type_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET_NULL',
                ]
            )
            ->update();

        $this->table('org_entities_timestampers')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'timestamper_id',
                'timestampers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('profiles')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('roles')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'parent_id',
                'roles',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('roles_type_entities')
            ->addForeignKey(
                'role_id',
                'roles',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'type_entity_id',
                'type_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('saved_filters')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('secure_data_spaces')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('sequences')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('service_levels')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'secure_data_space_id',
                'secure_data_spaces',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'ts_conv_id',
                'org_entities_timestampers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'ts_msg_id',
                'org_entities_timestampers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'ts_pjs_id',
                'org_entities_timestampers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('sessions')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('siegfrieds')
            ->addForeignKey(
                'fileupload_id',
                'fileuploads',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('stored_files')
            ->addForeignKey(
                'secure_data_space_id',
                'secure_data_spaces',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('stored_files_volumes')
            ->addForeignKey(
                'stored_file_id',
                'stored_files',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'volume_id',
                'volumes',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('technical_archive_units')
            ->addForeignKey(
                'parent_id',
                'technical_archive_units',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'technical_archive_id',
                'technical_archives',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('technical_archives')
            ->addForeignKey(
                'archival_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'secure_data_space_id',
                'secure_data_spaces',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('transfer_attachments')
            ->addForeignKey(
                'transfer_id',
                'transfers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('transfer_errors')
            ->addForeignKey(
                'transfer_id',
                'transfers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('transfers')
            ->addForeignKey(
                'agreement_id',
                'agreements',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'archival_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'created_user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'profile_id',
                'profiles',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'service_level_id',
                'service_levels',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'transferring_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('users')
            ->addForeignKey(
                'ldap_id',
                'ldaps',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'role_id',
                'roles',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('validation_actors')
            ->addForeignKey(
                'validation_stage_id',
                'validation_stages',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('validation_chains')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('validation_histories')
            ->addForeignKey(
                'validation_actor_id',
                'validation_actors',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET_NULL',
                ]
            )
            ->addForeignKey(
                'validation_process_id',
                'validation_processes',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('validation_processes')
            ->addForeignKey(
                'current_stage_id',
                'validation_stages',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET_NULL',
                ]
            )
            ->addForeignKey(
                'validation_chain_id',
                'validation_chains',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('validation_stages')
            ->addForeignKey(
                'validation_chain_id',
                'validation_chains',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('volumes')
            ->addForeignKey(
                'secure_data_space_id',
                'secure_data_spaces',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        // a2
        $table = $this->table('archives');
        $table->changeColumn(
            'is_integrity_ok',
            'boolean',
            [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'integrity_date',
            'timestamp',
            [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]
        );
        $table->update();

        // a3
        $table = $this->table('sessions');
        $table->addIndex(['token'], ['unique' => true]);
        $table->update();

        $table = $this->table('killables');
        $table->addColumn(
            'pid',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'name',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'user_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'session_token',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'timeout',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'killed',
            'boolean',
            [
                'default' => null,
                'limit' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addIndex(['pid'], ['unique' => true]);
        $table->addForeignKey(
            'user_id',
            'users',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'session_token',
            'sessions',
            'token',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE',
            ]
        );
        $table->create();

        // a4
        $table = $this->table('archives');
        $table->addColumn(
            'files_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'units_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('org_entities');
        $table->addColumn(
            'description',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        // a5
        $table = $this->table('oaipmh_tokens');
        $table->addColumn(
            'value',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'verb',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'metadata_prefix',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'expiration_date',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'complete_list_size',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'cursor',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'prev_id',
            'integer',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'version',
            'integer',
            [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'prev_id',
            'oaipmh_tokens',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->addIndex(['value', 'verb']);
        $table->create();

        $table = $this->table('archives');
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('oaipmh_listsets');
        $table->addColumn(
            'oaipmh_token_id',
            'integer',
            [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ]
        );
        $table->addColumn(
            'spec',
            'string',
            [
                'default' => null,
                'limit' => 512,
                'null' => false,
            ]
        );
        $table->addColumn(
            'name',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'oaipmh_token_id',
            'oaipmh_tokens',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('users');
        $table->addColumn(
            'agent_type',
            'string',
            [
                'default' => 'person',
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('roles');
        $table->addColumn(
            'agent_type',
            'string',
            [
                'default' => 'person',
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('oaipmh_archives');
        $table->addColumn(
            'oaipmh_token_id',
            'integer',
            [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archive_id',
            'integer',
            [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ]
        );
        $table->addColumn(
            'datetime',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'deleted',
            'boolean',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'oaipmh_token_id',
            'oaipmh_tokens',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'archive_id',
            'archives',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('event_logs');
        $table->removeColumn('life_cycle');
        $table->update();

        $this->table('access_tokens')
            ->addIndex('expiration_date')
            ->update();

        $this->table('auth_urls')
            ->addIndex('expire')
            ->update();

        $this->table('oaipmh_tokens')
            ->addIndex('expiration_date')
            ->update();

        $table = $this->table('archives');
        $table->changeColumn(
            'modified',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->update();

        // a6
        $table = $this->table('transfers');
        $table->addColumn(
            'files_deleted',
            'boolean',
            [
                'default' => false,
                'limit' => null,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('crons');
        $table->addColumn(
            'max_duration',
            'string',
            [
                'default' => 'PT1H',
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('cron_executions');
        $table->addColumn(
            'pid',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('archives');
        $table->addIndex(['archival_agency_id', 'archival_agency_identifier'], ['unique' => true]);
        $table->update();

        // a7
        $table = $this->table('ldaps');
        $table->addColumn(
            'schema',
            'string',
            [
                'default' => ActiveDirectory::class,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'follow_referrals',
            'boolean',
            [
                'default' => false,
                'null' => false,
            ]
        );
        $table->addColumn(
            'version',
            'integer',
            [
                'default' => 3,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'timeout',
            'integer',
            [
                'default' => 5,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'custom_options',
            'text',
            [
                'default' => '[]',
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('crons');
        $table->addColumn(
            'last_email',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('archives');
        $table->addColumn(
            'filesexist_date',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('org_entities');
        $table->addIndex(['identifier', 'archival_agency_id'], ['unique' => true]);
        $table->update();

        $table = $this->table('secure_data_spaces');
        $table->addIndex(['name'], ['unique' => true]);
        $table->update();

        $table = $this->table('roles');
        $table->addIndex(['name'], ['unique' => true]);
        $table->removeIndex(['org_entity_id', 'code']);
        $table->addIndex(['code'], ['unique' => true]);
        $table->update();

        $table = $this->table('users');
        $table->addIndex(['username'], ['unique' => true]);
        $table->update();

        $table = $this->table('ldaps');
        $table->addIndex(['name'], ['unique' => true]);
        $table->update();

        $table = $this->table('validation_stages');
        $table->addIndex(['validation_chain_id', 'name'], ['unique' => true]);
        $table->update();

        $table = $this->table('validation_actors');
        $table->addIndex(['validation_stage_id', 'app_type', 'app_foreign_key'], ['unique' => true]);
        $table->update();

        $table = $this->table('profiles');
        $table->addIndex(['identifier', 'org_entity_id'], ['unique' => true]);
        $table->update();

        $table = $this->table('agreements');
        $table->addIndex(['identifier', 'org_entity_id'], ['unique' => true]);
        $table->update();

        $table = $this->table('service_levels');
        $table->addIndex(['identifier', 'org_entity_id'], ['unique' => true]);
        $table->update();

        $table = $this->table('keyword_lists');
        $table->addIndex(['identifier', 'org_entity_id'], ['unique' => true]);
        $table->update();

        $table = $this->table('counters');
        $table->addIndex(['identifier', 'org_entity_id'], ['unique' => true]);
        $table->update();

        $table = $this->table('sequences');
        $table->addIndex(['name', 'org_entity_id'], ['unique' => true]);
        $table->update();

        $table = $this->table('volumes');
        $table->addIndex(['name'], ['unique' => true]);
        $table->update();

        $table = $this->table('stored_files');
        $table->addIndex(['secure_data_space_id', 'name'], ['unique' => true]);
        $table->update();

        $table = $this->table('transfer_attachments');
        $table->addColumn(
            'xpath',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('transfers');
        $table->addColumn(
            'data_count',
            'integer',
            [
                'default' => 0,
                'limit' => 10,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('archive_units');
        $table->addColumn(
            'search',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $this->table('access_rule_codes')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('access_rules')
            ->dropForeignKey(
                'access_rule_code_id'
            )->save();

        $this->table('access_tokens')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('acos')
            ->dropForeignKey(
                'parent_id'
            )->save();

        $this->table('agreements')
            ->dropForeignKey(
                'improper_chain_id'
            )
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'proper_chain_id'
            )->save();

        $this->table('agreements_originating_agencies')
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('agreements_profiles')
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'profile_id'
            )->save();

        $this->table('agreements_service_levels')
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'service_level_id'
            )->save();

        $this->table('agreements_transferring_agencies')
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('appraisal_rule_codes')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('appraisal_rules')
            ->dropForeignKey(
                'appraisal_rule_code_id'
            )->save();

        $this->table('archive_binaries')
            ->dropForeignKey(
                'original_data_id'
            )
            ->dropForeignKey(
                'stored_file_id'
            )->save();

        $this->table('archive_binaries_archive_units')
            ->dropForeignKey(
                'archive_binary_id'
            )
            ->dropForeignKey(
                'archive_unit_id'
            )->save();

        $this->table('archive_binaries_technical_archive_units')
            ->dropForeignKey(
                'archive_binary_id'
            )
            ->dropForeignKey(
                'technical_archive_unit_id'
            )->save();

        $this->table('archive_descriptions')
            ->dropForeignKey(
                'access_rule_id'
            )
            ->dropForeignKey(
                'archive_unit_id'
            )->save();

        $this->table('archive_files')
            ->dropForeignKey(
                'archive_id'
            )
            ->dropForeignKey(
                'stored_file_id'
            )->save();

        $this->table('archive_keywords')
            ->dropForeignKey(
                'access_rule_id'
            )
            ->dropForeignKey(
                'archive_unit_id'
            )->save();

        $this->table('archive_units')
            ->dropForeignKey(
                'access_rule_id'
            )
            ->dropForeignKey(
                'appraisal_rule_id'
            )
            ->dropForeignKey(
                'archive_id'
            )
            ->dropForeignKey(
                'parent_id'
            )->save();

        $this->table('archives')
            ->dropForeignKey(
                'access_rule_id'
            )
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'appraisal_rule_id'
            )
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'originating_agency_id'
            )
            ->dropForeignKey(
                'profile_id'
            )
            ->dropForeignKey(
                'secure_data_space_id'
            )
            ->dropForeignKey(
                'service_level_id'
            )
            ->dropForeignKey(
                'transfer_id'
            )
            ->dropForeignKey(
                'transferring_agency_id'
            )
            ->dropForeignKey(
                'transferring_operator_id'
            )->save();

        $this->table('aros_acos')
            ->dropForeignKey(
                'aco_id'
            )
            ->dropForeignKey(
                'aro_id'
            )->save();

        $this->table('beanstalk_jobs')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('configurations')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('conversion_policies')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('counters')
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'sequence_id'
            )->save();

        $this->table('cron_executions')
            ->dropForeignKey(
                'cron_id'
            )->save();

        $this->table('eaccpfs')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('event_logs')
            ->dropForeignKey(
                'cron_id'
            )
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('file_extensions_pronoms')
            ->dropForeignKey(
                'file_extension_id'
            )
            ->dropForeignKey(
                'pronom_id'
            )->save();

        $this->table('fileuploads')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('fileuploads_profiles')
            ->dropForeignKey(
                'fileupload_id'
            )
            ->dropForeignKey(
                'profile_id'
            )->save();

        $this->table('filters')
            ->dropForeignKey(
                'saved_filter_id'
            )->save();

        $this->table('keyword_lists')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('keywords')
            ->dropForeignKey(
                'keyword_list_id'
            )
            ->dropForeignKey(
                'parent_id'
            )->save();

        $this->table('ldaps')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('mediainfo_audios')
            ->dropForeignKey(
                'mediainfo_id'
            )->save();

        $this->table('mediainfo_images')
            ->dropForeignKey(
                'mediainfo_id'
            )->save();

        $this->table('mediainfo_texts')
            ->dropForeignKey(
                'mediainfo_id'
            )->save();

        $this->table('mediainfo_videos')
            ->dropForeignKey(
                'mediainfo_id'
            )->save();

        $this->table('mediainfos')
            ->dropForeignKey(
                'fileupload_id'
            )->save();

        $this->table('notifications')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('org_entities')
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'default_secure_data_space_id'
            )
            ->dropForeignKey(
                'parent_id'
            )
            ->dropForeignKey(
                'timestamper_id'
            )
            ->dropForeignKey(
                'type_entity_id'
            )->save();

        $this->table('org_entities_timestampers')
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'timestamper_id'
            )->save();

        $this->table('profiles')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('roles')
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'parent_id'
            )->save();

        $this->table('roles_type_entities')
            ->dropForeignKey(
                'role_id'
            )
            ->dropForeignKey(
                'type_entity_id'
            )->save();

        $this->table('saved_filters')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('secure_data_spaces')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('sequences')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('service_levels')
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'secure_data_space_id'
            )
            ->dropForeignKey(
                'ts_conv_id'
            )
            ->dropForeignKey(
                'ts_msg_id'
            )
            ->dropForeignKey(
                'ts_pjs_id'
            )->save();

        $this->table('sessions')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('siegfrieds')
            ->dropForeignKey(
                'fileupload_id'
            )->save();

        $this->table('stored_files')
            ->dropForeignKey(
                'secure_data_space_id'
            )->save();

        $this->table('stored_files_volumes')
            ->dropForeignKey(
                'stored_file_id'
            )
            ->dropForeignKey(
                'volume_id'
            )->save();

        $this->table('technical_archive_units')
            ->dropForeignKey(
                'parent_id'
            )
            ->dropForeignKey(
                'technical_archive_id'
            )->save();

        $this->table('technical_archives')
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'secure_data_space_id'
            )->save();

        $this->table('transfer_attachments')
            ->dropForeignKey(
                'transfer_id'
            )->save();

        $this->table('transfer_errors')
            ->dropForeignKey(
                'transfer_id'
            )->save();

        $this->table('transfers')
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'created_user_id'
            )
            ->dropForeignKey(
                'profile_id'
            )
            ->dropForeignKey(
                'service_level_id'
            )
            ->dropForeignKey(
                'transferring_agency_id'
            )->save();

        $this->table('users')
            ->dropForeignKey(
                'ldap_id'
            )
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'role_id'
            )->save();

        $this->table('validation_actors')
            ->dropForeignKey(
                'validation_stage_id'
            )->save();

        $this->table('validation_chains')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('validation_histories')
            ->dropForeignKey(
                'validation_actor_id'
            )
            ->dropForeignKey(
                'validation_process_id'
            )->save();

        $this->table('validation_processes')
            ->dropForeignKey(
                'current_stage_id'
            )
            ->dropForeignKey(
                'validation_chain_id'
            )->save();

        $this->table('validation_stages')
            ->dropForeignKey(
                'validation_chain_id'
            )->save();

        $this->table('volumes')
            ->dropForeignKey(
                'secure_data_space_id'
            )->save();

        $this->table('access_rule_codes')->drop()->save();
        $this->table('access_rules')->drop()->save();
        $this->table('access_tokens')->drop()->save();
        $this->table('acos')->drop()->save();
        $this->table('agreements')->drop()->save();
        $this->table('agreements_originating_agencies')->drop()->save();
        $this->table('agreements_profiles')->drop()->save();
        $this->table('agreements_service_levels')->drop()->save();
        $this->table('agreements_transferring_agencies')->drop()->save();
        $this->table('appraisal_rule_codes')->drop()->save();
        $this->table('appraisal_rules')->drop()->save();
        $this->table('archive_binaries')->drop()->save();
        $this->table('archive_binaries_archive_units')->drop()->save();
        $this->table('archive_binaries_technical_archive_units')->drop()->save();
        $this->table('archive_descriptions')->drop()->save();
        $this->table('archive_files')->drop()->save();
        $this->table('archive_keywords')->drop()->save();
        $this->table('archive_units')->drop()->save();
        $this->table('archives')->drop()->save();
        $this->table('aros')->drop()->save();
        $this->table('aros_acos')->drop()->save();
        $this->table('auth_urls')->drop()->save();
        $this->table('beanstalk_jobs')->drop()->save();
        $this->table('beanstalk_workers')->drop()->save();
        $this->table('configurations')->drop()->save();
        $this->table('conversion_policies')->drop()->save();
        $this->table('counters')->drop()->save();
        $this->table('cron_executions')->drop()->save();
        $this->table('crons')->drop()->save();
        $this->table('eaccpfs')->drop()->save();
        $this->table('event_logs')->drop()->save();
        $this->table('file_extensions')->drop()->save();
        $this->table('file_extensions_pronoms')->drop()->save();
        $this->table('fileuploads')->drop()->save();
        $this->table('fileuploads_profiles')->drop()->save();
        $this->table('filters')->drop()->save();
        $this->table('keyword_lists')->drop()->save();
        $this->table('keywords')->drop()->save();
        $this->table('ldaps')->drop()->save();
        $this->table('mediainfo_audios')->drop()->save();
        $this->table('mediainfo_images')->drop()->save();
        $this->table('mediainfo_texts')->drop()->save();
        $this->table('mediainfo_videos')->drop()->save();
        $this->table('mediainfos')->drop()->save();
        $this->table('notifications')->drop()->save();
        $this->table('org_entities')->drop()->save();
        $this->table('org_entities_timestampers')->drop()->save();
        $this->table('profiles')->drop()->save();
        $this->table('pronoms')->drop()->save();
        $this->table('roles')->drop()->save();
        $this->table('roles_type_entities')->drop()->save();
        $this->table('saved_filters')->drop()->save();
        $this->table('secure_data_spaces')->drop()->save();
        $this->table('sequences')->drop()->save();
        $this->table('service_levels')->drop()->save();
        $this->table('sessions')->drop()->save();
        $this->table('siegfrieds')->drop()->save();
        $this->table('stored_files')->drop()->save();
        $this->table('stored_files_volumes')->drop()->save();
        $this->table('technical_archive_units')->drop()->save();
        $this->table('technical_archives')->drop()->save();
        $this->table('timestampers')->drop()->save();
        $this->table('transfer_attachments')->drop()->save();
        $this->table('transfer_errors')->drop()->save();
        $this->table('transfers')->drop()->save();
        $this->table('type_entities')->drop()->save();
        $this->table('users')->drop()->save();
        $this->table('validation_actors')->drop()->save();
        $this->table('validation_chains')->drop()->save();
        $this->table('validation_histories')->drop()->save();
        $this->table('validation_processes')->drop()->save();
        $this->table('validation_stages')->drop()->save();
        $this->table('versions')->drop()->save();
        $this->table('volumes')->drop()->save();
        $this->table('webservices')->drop()->save();
    }
}
