<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Migrations\AbstractMigration;

class Patch215 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $this->table('outgoing_transfer_requests')
            ->addColumn(
                'service_level_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->update();

        $table = $this->table('archiving_systems');
        $table->addColumn(
            'chunk_size',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->update();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $this->table('outgoing_transfer_requests')
            ->removeColumn('service_level_identifier')
            ->update();

        $table = $this->table('archiving_systems');
        $table->removeColumn('chunk_size');
        $table->update();
    }
}
