<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Migrations\AbstractMigration;

class Patch204 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $table = $this->table('stored_files_volumes');
        $table->addColumn(
            'is_integrity_ok',
            'boolean',
            [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'integrity_date',
            'timestamp',
            [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]
        );
        $table->update();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $table = $this->table('stored_files_volumes');
        $table->removeColumn('is_integrity_ok');
        $table->removeColumn('integrity_date');
        $table->update();
    }
}
