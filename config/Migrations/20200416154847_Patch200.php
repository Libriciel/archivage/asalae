<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Model\Table\VolumesTable;
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class Patch200 extends AbstractMigration
{
    /**
     * @return void
     * @throws Exception
     */
    public function up(): void
    {
        $table = $this->table('archives');
        $table->addColumn(
            'next_pubdesc',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('archives');
        $table->addColumn(
            'next_search',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('delivery_requests');
        $table->addColumn(
            'identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'requester_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'state',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'comment',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'derogation',
            'boolean',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created_user_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archival_agency_id',
            'org_entities',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'requester_id',
            'org_entities',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'created_user_id',
            'users',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('archive_units_delivery_requests');
        $table->addColumn(
            'archive_unit_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'delivery_request_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archive_unit_id',
            'archive_units',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'delivery_request_id',
            'delivery_requests',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('delivery_requests');
        $table->addColumn(
            'last_state_update',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'states_history',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('delivery_requests');
        $table->addColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('deliveries');
        $table->addColumn(
            'identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'delivery_request_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'state',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'comment',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'archive_units_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'original_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'original_size',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'delivery_request_id',
            'delivery_requests',
            'id',
            ['update' => 'CASCADE']
        );
        $table->create();

        $table = $this->table('destruction_requests');
        $table->addColumn(
            'identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'comment',
            'text',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'originating_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'control_autority_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'state',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'last_state_update',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'states_history',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'archive_units_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'original_count',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'original_size',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created_user_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archival_agency_id',
            'org_entities',
            'id',
            ['update' => 'CASCADE']
        );
        $table->addForeignKey(
            'originating_agency_id',
            'org_entities',
            'id',
            ['update' => 'CASCADE']
        );
        $table->addForeignKey(
            'control_autority_id',
            'org_entities',
            'id',
            ['update' => 'CASCADE']
        );
        $table->addForeignKey(
            'created_user_id',
            'users',
            'id',
            ['update' => 'CASCADE']
        );
        $table->create();

        $table = $this->table('archive_units_destruction_requests');
        $table->addColumn(
            'archive_unit_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'destruction_request_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archive_unit_id',
            'archive_units',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'destruction_request_id',
            'destruction_requests',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('archive_units');
        $table->addColumn(
            'expired_dua_root',
            'boolean',
            [
                'default' => false,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('destruction_requests');
        $table->renameColumn('control_autority_id', 'control_authority_id');
        $table->update();

        $table = $this->table('destruction_requests');
        $table->changeColumn(
            'original_size',
            'biginteger',
            [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('destruction_notifications');
        $table->addColumn(
            'identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'destruction_request_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'comment',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'destruction_request_id',
            'destruction_requests',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('archive_binaries');
        $table->changeColumn(
            'stored_file_id',
            'integer',
            [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('delivery_requests');
        $table->addColumn(
            'validation_chain_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addForeignKey(
            'validation_chain_id',
            'validation_chains',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'SET NULL',
            ]
        );
        $table->update();

        $table = $this->table('destruction_requests');
        $table->addColumn(
            'validation_chain_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addForeignKey(
            'validation_chain_id',
            'validation_chains',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'SET NULL',
            ]
        );
        $table->update();

        $table = $this->table('volumes');
        $table->addColumn(
            'read_duration',
            'float',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $loc = TableRegistry::getTableLocator();
        /** @var VolumesTable $Volumes */
        $Volumes = $loc->get('Volumes');
        foreach ($Volumes->find() as $volume) {
            $Volumes->calcReadDuration($volume);
        }

        $table = $this->table('destruction_notification_xpaths');
        $table->addColumn(
            'destruction_notification_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archive_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archive_unit_identifier',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archive_unit_name',
            'text',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'description_xpath',
            'text',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'destruction_notification_id',
            'destruction_notifications',
            'id',
            ['update' => 'CASCADE']
        );
        $table->addForeignKey(
            'archive_id',
            'archives',
            'id',
            ['update' => 'CASCADE']
        );
        $table->create();

        $loc->clear();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $table = $this->table('archives');
        $table->removeColumn('next_pubdesc');
        $table->update();

        $table = $this->table('archives');
        $table->removeColumn('next_search');
        $table->update();

        $table = $this->table('delivery_requests');
        $table->drop();
        $table->save();

        $table = $this->table('archive_units_delivery_requests');
        $table->drop();
        $table->save();

        $table = $this->table('delivery_requests');
        $table->removeColumn('last_state_update');
        $table->removeColumn('states_history');
        $table->update();

        $table = $this->table('delivery_requests');
        $table->removeColumn('filename');
        $table->update();

        $table = $this->table('deliveries');
        $table->drop();
        $table->update();

        $table = $this->table('destruction_requests');
        $table->drop();
        $table->update();

        $table = $this->table('archive_units_destruction_requests');
        $table->drop();
        $table->update();

        $table = $this->table('archive_units');
        $table->removeColumn('expired_dua_root');
        $table->update();

        $table = $this->table('destruction_requests');
        $table->renameColumn('control_authority_id', 'control_autority_id');
        $table->update();

        $table = $this->table('destruction_notifications');
        $table->drop();
        $table->update();

        $table = $this->table('archive_binaries');
        $table->changeColumn(
            'stored_file_id',
            'integer',
            [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('delivery_requests');
        $table->removeColumn('validation_chain_id');
        $table->update();

        $table = $this->table('destruction_requests');
        $table->removeColumn('validation_chain_id');
        $table->update();

        $table = $this->table('volumes');
        $table->removeColumn('read_duration');
        $table->update();

        $table = $this->table('destruction_notification_xpaths');
        $table->drop();
        $table->update();
    }
}
