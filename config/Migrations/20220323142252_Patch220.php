<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Cron\EventsLogsExport;
use Cake\Database\Expression\IdentifierExpression;
use Migrations\AbstractMigration;
use Phinx\Db\Table\Column;

class Patch220 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $table = $this->table('super_archivists_archival_agencies');
        $table->addColumn(
            'user_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'user_id',
            'users',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'archival_agency_id',
            'org_entities',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $adapter = $this->getAdapter();

        $table = $this->table('secure_data_spaces');
        $table->addColumn(
            'is_default',
            'boolean',
            [
                'default' => false,
                'limit' => null,
                'null' => false,
            ]
        )
            ->update();

        $subquery = $adapter->getSelectBuilder()
            ->select('default_secure_data_space_id')
            ->from('org_entities')
            ->where(['default_secure_data_space_id IS NOT' => null]);

        $adapter->getUpdateBuilder()
            ->update('secure_data_spaces')
            ->set(['is_default' => true])
            ->where(['id IN' => $subquery])
            ->execute();

        // sqlite ne supporte pas le drop des foreign_key
        $table = $this->table('org_entities');
        if ($adapter->getAdapterType() !== 'sqlite') {
            $table->removeColumn('archival_agency_id');
            $table->removeColumn('default_secure_data_space_id');
        } else {
            $table = $this->table('new_org_entities');
            $table
                ->addColumn(
                    'name',
                    'string',
                    [
                        'default' => null,
                        'limit' => 255,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'identifier',
                    'string',
                    [
                        'default' => null,
                        'limit' => 255,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'parent_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'lft',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'rght',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'created',
                    'timestamp',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'modified',
                    'timestamp',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'type_entity_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'timestamper_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'active',
                    'boolean',
                    [
                        'default' => true,
                        'limit' => null,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'is_main_archival_agency',
                    'boolean',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'description',
                    'text',
                    [
                        'default' => null,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'short_name',
                    'string',
                    [
                        'default' => null,
                        'limit' => 40,
                        'null' => true,
                    ]
                )
                ->addIndex(
                    [
                        'parent_id',
                    ]
                )
                ->addIndex(
                    [
                        'timestamper_id',
                    ]
                )
                ->addIndex(
                    [
                        'type_entity_id',
                    ]
                )
                ->addIndex(
                    [
                        'id',
                        'name',
                    ]
                )
                ->addIndex(
                    [
                        'lft',
                        'rght',
                    ]
                )
                ->addForeignKey(
                    'parent_id',
                    'org_entities',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'CASCADE',
                    ]
                )
                ->addForeignKey(
                    'timestamper_id',
                    'timestampers',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'NO_ACTION',
                    ]
                )
                ->addForeignKey(
                    'type_entity_id',
                    'type_entities',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'SET_NULL',
                    ]
                );
            $table->create();

            $columns = $adapter->getColumns('org_entities');
            $columns = array_map(fn(Column $col) => $col->getName(), $columns);
            $columns = array_diff($columns, ['archival_agency_id', 'default_secure_data_space_id']);
            $subquery = $adapter->getSelectBuilder()
                ->select($columns)
                ->from('org_entities')
                ->orderByAsc('id');
            $adapter->getInsertBuilder()
                ->insert($columns)
                ->into('new_org_entities')
                ->values($subquery)
                ->execute();

            $table = $this->table('org_entities');
            $table->drop();
            $table->update();

            $table = $this->table('new_org_entities');
            $table->rename('org_entities');
        }
        $table->update();

        $table = $this->table('change_entity_requests');
        $table->addColumn(
            'base_archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'target_archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'user_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'reason',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'base_archival_agency_id',
            'org_entities',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'target_archival_agency_id',
            'org_entities',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'user_id',
            'users',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('transfers');
        $table->addColumn(
            'hidden',
            'boolean',
            [
                'default' => false,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('transfers');
        $table->addColumn(
            'originating_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->update();

        $adapter = $this->getAdapter();
        $stmt = $adapter->getSelectBuilder()
            ->select(
                [
                    'transfers.id',
                    'originating_agency_id' => 'archives.originating_agency_id',
                    'transfers.transferring_agency_id',
                ]
            )
            ->from('transfers')
            ->leftJoin(
                'archives',
                ['archives.transfer_id' => new IdentifierExpression('transfers.id')]
            )
            ->execute();

        while ($transfer = $stmt->fetch('assoc')) {
            $adapter->getUpdateBuilder()
                ->update('transfers')
                ->set(
                    [
                        'originating_agency_id' => $transfer['originating_agency_id']
                            ?? $transfer['transferring_agency_id'],
                    ]
                )
                ->where(['id' => $transfer['id']])
                ->execute();
        }
        $table->changeColumn(
            'originating_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'originating_agency_id',
            'org_entities',
            'id',
            ['update' => 'CASCADE']
        );
        $table->update();

        $adapter->getUpdateBuilder()
            ->update('crons')
            ->set(
                [
                    'name' => __("Création des journaux des événements"),
                    'description' => __(
                        "Création des fichiers des journaux des événements sous forme de fichiers xml PREMIS"
                        . " et stockage dans les archives techniques des journaux des événements. "
                        . "Le service d'Archives gestionnaire conserve les journaux d'exploitation"
                        . " ainsi que les fichiers sceau des journaux"
                    ),
                ]
            )
            ->where(['classname' => EventsLogsExport::class])
            ->execute();

        $table = $this->table('event_logs');
        $table->addColumn(
            'archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addForeignKey(
            'archival_agency_id',
            'org_entities',
            'id',
            [
                'delete' => 'SET NULL',
                'update' => 'CASCADE',
            ]
        );
        $table->update();

        $table = $this->table('archives_transfers');
        $table->addColumn(
            'archive_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'transfer_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archive_id',
            'archives',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'transfer_id',
            'transfers',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE',
            ]
        );
        $table->create();

        $adapter = $this->getAdapter();
        $stmt = $adapter->getSelectBuilder()
            ->select(['id', 'transfer_id'])
            ->from('archives')
            ->orderByAsc('id')
            ->execute();
        while ($archive = $stmt->fetch('assoc')) {
            $table->insert(['archive_id' => $archive['id'], 'transfer_id' => $archive['transfer_id']]);
            $table->update();
        }

        // sqlite ne supporte pas le drop des foreign_key
        $table = $this->table('archives');
        if ($adapter->getAdapterType() !== 'sqlite') {
            $table->removeColumn('transfer_id');
        } else {
            $table = $this->table('new_archives');
            $table
                ->addColumn(
                    'state',
                    'string',
                    [
                        'default' => null,
                        'limit' => 255,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'is_integrity_ok',
                    'boolean',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'archival_agency_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'transferring_agency_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'originating_agency_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'transferring_operator_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'agreement_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'profile_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'service_level_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'access_rule_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'appraisal_rule_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'archival_agency_identifier',
                    'string',
                    [
                        'default' => null,
                        'limit' => 255,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'transferring_agency_identifier',
                    'string',
                    [
                        'default' => null,
                        'limit' => 255,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'originating_agency_identifier',
                    'string',
                    [
                        'default' => null,
                        'limit' => 255,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'secure_data_space_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'storage_path',
                    'string',
                    [
                        'default' => null,
                        'limit' => 255,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'name',
                    'text',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'created',
                    'timestamp',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'description',
                    'text',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'oldest_date',
                    'date',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'latest_date',
                    'date',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'transferred_size',
                    'biginteger',
                    [
                        'default' => '0',
                        'limit' => 20,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'management_size',
                    'biginteger',
                    [
                        'default' => '0',
                        'limit' => 20,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'original_size',
                    'biginteger',
                    [
                        'default' => '0',
                        'limit' => 20,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'preservation_size',
                    'biginteger',
                    [
                        'default' => '0',
                        'limit' => 20,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'dissemination_size',
                    'biginteger',
                    [
                        'default' => '0',
                        'limit' => 20,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'timestamp_size',
                    'biginteger',
                    [
                        'default' => '0',
                        'limit' => 20,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'transferred_count',
                    'integer',
                    [
                        'default' => '0',
                        'limit' => 10,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'management_count',
                    'integer',
                    [
                        'default' => '0',
                        'limit' => 10,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'original_count',
                    'integer',
                    [
                        'default' => '0',
                        'limit' => 10,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'preservation_count',
                    'integer',
                    [
                        'default' => '0',
                        'limit' => 10,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'dissemination_count',
                    'integer',
                    [
                        'default' => '0',
                        'limit' => 10,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'timestamp_count',
                    'integer',
                    [
                        'default' => '0',
                        'limit' => 10,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'xml_node_tagname',
                    'string',
                    [
                        'default' => null,
                        'limit' => 255,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'last_state_update',
                    'timestamp',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => true,
                    ]
                )
                ->addIndex(
                    [
                        'access_rule_id',
                    ]
                )
                ->addIndex(
                    [
                        'agreement_id',
                    ]
                )
                ->addIndex(
                    [
                        'appraisal_rule_id',
                    ]
                )
                ->addIndex(
                    [
                        'archival_agency_id',
                    ]
                )
                ->addIndex(
                    [
                        'originating_agency_id',
                    ]
                )
                ->addIndex(
                    [
                        'profile_id',
                    ]
                )
                ->addIndex(
                    [
                        'secure_data_space_id',
                    ]
                )
                ->addIndex(
                    [
                        'service_level_id',
                    ]
                )
                ->addIndex(
                    [
                        'transferring_agency_id',
                    ]
                )
                ->addIndex(
                    [
                        'transferring_operator_id',
                    ]
                )
                ->addForeignKey(
                    'access_rule_id',
                    'access_rules',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'NO_ACTION',
                    ]
                )
                ->addForeignKey(
                    'agreement_id',
                    'agreements',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'NO_ACTION',
                    ]
                )
                ->addForeignKey(
                    'appraisal_rule_id',
                    'appraisal_rules',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'NO_ACTION',
                    ]
                )
                ->addForeignKey(
                    'archival_agency_id',
                    'org_entities',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'NO_ACTION',
                    ]
                )
                ->addForeignKey(
                    'originating_agency_id',
                    'org_entities',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'NO_ACTION',
                    ]
                )
                ->addForeignKey(
                    'profile_id',
                    'profiles',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'NO_ACTION',
                    ]
                )
                ->addForeignKey(
                    'secure_data_space_id',
                    'secure_data_spaces',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'NO_ACTION',
                    ]
                )
                ->addForeignKey(
                    'service_level_id',
                    'service_levels',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'NO_ACTION',
                    ]
                )
                ->addForeignKey(
                    'transferring_agency_id',
                    'org_entities',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'NO_ACTION',
                    ]
                )
                ->addForeignKey(
                    'transferring_operator_id',
                    'org_entities',
                    'id',
                    [
                        'update' => 'CASCADE',
                        'delete' => 'NO_ACTION',
                    ]
                );
            $table->changeColumn(
                'is_integrity_ok',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            );
            $table->addColumn(
                'integrity_date',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            );
            $table->addColumn(
                'units_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 11,
                    'null' => true,
                ]
            );
            $table->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'null' => false,
                ]
            );
            $table->addColumn(
                'filesexist_date',
                'datetime',
                [
                    'default' => null,
                    'null' => true,
                ]
            );
            $table->addColumn(
                'next_pubdesc',
                'datetime',
                [
                    'default' => null,
                    'null' => true,
                ]
            );
            $table->addColumn(
                'next_search',
                'datetime',
                [
                    'default' => null,
                    'null' => true,
                ]
            );
            $table->addIndex(['archival_agency_id', 'archival_agency_identifier'], ['unique' => true]);
            $table->create();

            $columns = $adapter->getColumns('archives');
            $columns = array_map(fn(Column $col) => $col->getName(), $columns);
            $columns = array_diff($columns, ['transfer_id']);
            $subquery = $adapter->getSelectBuilder()
                ->select($columns)
                ->from('archives')
                ->orderByAsc('id');
            $adapter->getInsertBuilder()
                ->insert($columns)
                ->into('new_archives')
                ->values($subquery)
                ->execute();

            $table = $this->table('archives');
            $table->drop();
            $table->update();

            $table = $this->table('new_archives');
            $table->rename('archives');
        }
        $table->update();

        $table = $this->table('composite_archive_units');
        $table->addColumn(
            'transfer_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'archive_unit_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'transfer_xpath',
            'text',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'transfer_id',
            'transfers',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'archive_unit_id',
            'archive_units',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('archives');
        $table->addColumn(
            'archive_unit_sequence',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->update();

        $adapter = $this->getAdapter();
        $query = $adapter->getSelectBuilder();
        $stmt = $query
            ->select(['count' => $query->func()->count('*')])
            ->from('archives')
            ->execute();
        $count = $stmt->fetchAll('assoc')[0]['count'];
        // on affiche le message que si on n'est pas dans les tests
        if (!defined('TMP_TESTDIR')) {
            fwrite(STDOUT, sprintf("archive count: %d\n", $count));
        }

        $stmt = $adapter->getSelectBuilder()
            ->select(['id'])
            ->from('archives')
            ->execute();
        $i = 0;
        while ($row = $stmt->fetch('assoc')) {
            if ($i && $i % 100 === 0) {
                fwrite(STDOUT, sprintf("archives : %d / %d\n", $i, $count));
            }
            $i++;
            $au = $adapter->getSelectBuilder()
                ->select(['archival_agency_identifier'])
                ->from('archive_units')
                ->where(['archive_id' => $row['id']])
                ->orderByDesc('archival_agency_identifier')
                ->limit(1)
                ->execute()
                ->fetch('assoc');
            if (!$au) {
                continue;
            }
            if (preg_match('/_(\d{4,})$/', $au['archival_agency_identifier'], $m)) {
                $index = (int)$m['1'];
            } else {
                $index = 0;
            }
            $adapter->getUpdateBuilder()
                ->update('archives')
                ->set(['archive_unit_sequence' => $index])
                ->where(['id' => $row['id']])
                ->execute();
        }

        $table = $this->table('users');
        $table->addColumn(
            'notify_download_pdf_frequency',
            'integer',
            [
                'default' => 1,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'notify_download_zip_frequency',
            'integer',
            [
                'default' => 1,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'notify_validation_frequency',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'notify_job_frequency',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'notify_agreement_frequency',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'notify_transfer_report_frequency',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('agreements');
        $table->addColumn(
            'notify_delay',
            'integer',
            [
                'default' => 0,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('beanstalk_jobs');
        $table->addColumn(
            'data',
            'binary',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('beanstalk_jobs');
        $table->removeColumn('jobid');
        $table->removeColumn('last_status');
        $table->update();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $table = $this->table('super_archivists_archival_agencies');
        $table->drop();
        $table->update();

        $table = $this->table('org_entities');
        $table->addColumn(
            'archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('org_entities');
        $table->addColumn(
            'default_secure_data_space_id',
            'integer',
            [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('secure_data_spaces');
        $table->removeColumn('is_default');
        $table->update();

        $table = $this->table('change_entity_requests');
        $table->drop();
        $table->update();

        $table = $this->table('transfers');
        $table->removeColumn('hidden');
        $table->update();

        $table = $this->table('transfers');
        $table->removeColumn('originating_agency_id');
        $table->removeIndex('originating_agency_id');
        $table->update();

        $table = $this->table('event_logs');
        $table->removeColumn('archival_agency_id');
        $table->update();

        $table = $this->table('archives');
        $table->addColumn(
            'transfer_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addForeignKey(
            'transfer_id',
            'transfers',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'NO_ACTION',
            ]
        );
        $table->update();

        $adapter = $this->getAdapter();
        $stmt = $adapter->getSelectBuilder()
            ->select(['archive_id', 'transfer_id'])
            ->from('archives_transfers')
            ->execute();
        while ($link = $stmt->fetch('assoc')) {
            $adapter->getUpdateBuilder()
                ->update('archives')
                ->set(['transfer_id' => $link['transfer_id']])
                ->where(['id' => $link['archive_id']])
                ->execute();
        }

        $table = $this->table('archives');
        $table->changeColumn(
            'transfer_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('archives_transfers');
        $table->drop();
        $table->update();

        $table = $this->table('composite_archive_units');
        $table->drop();
        $table->update();

        $table = $this->table('archives');
        $table->removeColumn('archive_unit_sequence');
        $table->update();

        $table = $this->table('users');
        $table->removeColumn('notify_download_pdf_frequency');
        $table->removeColumn('notify_download_zip_frequency');
        $table->removeColumn('notify_validation_frequency');
        $table->removeColumn('notify_job_frequency');
        $table->removeColumn('notify_agreement_frequency');
        $table->removeColumn('notify_transfer_report_frequency');
        $table->update();

        $table = $this->table('agreements');
        $table->removeColumn('notify_delay');
        $table->update();

        $table = $this->table('beanstalk_jobs');
        $table->removeColumn('data');
        $table->update();

        $table = $this->table('beanstalk_jobs');
        $table->addColumn(
            'jobid',
            'integer',
            [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ]
        );
        $table->addColumn(
            'last_status',
            'string',
            [
                'default' => 'ready',
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->update();
    }
}
