<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Model\Table\ArchiveUnitsTable;
use Cake\Database\Expression\IdentifierExpression;
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class Patch203 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $table = $this->table('archive_binaries');
        $table->changeColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 2048,
                'null' => false,
            ]
        );
        $table->update();

        $opts = ['connection' => $this->getAdapter()->getOption('connection')];
        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities', $opts);
        $orgEntities = $OrgEntities->find()
            ->select(['OrgEntities.id'])
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'OrgEntities.archival_agency_id'
                    != new IdentifierExpression('OrgEntities.id'),
                    'TypeEntities.code' => 'SA',
                ]
            )
            ->disableHydration()
            ->toArray();
        foreach ($orgEntities as $orgEntity) {
            $OrgEntities->updateAll(
                ['archival_agency_id' => $orgEntity['id']],
                ['id' => $orgEntity['id']]
            );
        }

        $ArchiveUnits = $loc->get('ArchiveUnits');
        $archiveUnits = $ArchiveUnits->find()
            ->select(['ArchiveUnits.lft', 'ArchiveUnits.rght'])
            ->innerJoinWith('DestructionRequests')
            ->innerJoinWith('DestructionRequests.ValidationProcesses')
            ->where(
                [
                    'ArchiveUnits.state' => ArchiveUnitsTable::S_DESTROYING,
                    'ValidationProcesses.processed IS' => true,
                    'ValidationProcesses.validated IS' => false,
                ]
            )
            ->disableHydration();
        foreach ($archiveUnits as $archiveUnit) {
            $ArchiveUnits->updateAll(
                ['state' => ArchiveUnitsTable::S_AVAILABLE],
                [
                    'state' => ArchiveUnitsTable::S_DESTROYING,
                    'lft >=' => $archiveUnit['lft'],
                    'rght <=' => $archiveUnit['rght'],
                ]
            );
        }

        $loc->clear();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $table = $this->table('archive_binaries');
        $table->changeColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 512,
                'null' => false,
            ]
        );
        $table->update();
    }
}
