<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Migrations\AbstractMigration;

class Patch209 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $table = $this->table('agreements');
        $table->renameColumn('max_tranfers', 'max_transfers');
        $table->update();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $table = $this->table('agreements');
        $table->renameColumn('max_transfers', 'max_tranfers');
        $table->update();
    }
}
