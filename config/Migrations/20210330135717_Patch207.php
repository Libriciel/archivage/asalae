<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Cake\Database\Expression\IdentifierExpression;
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class Patch207 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $loc = TableRegistry::getTableLocator();
        $Users = $loc->get('Users');
        $Users->updateAll(
            ['ldap_login' => new IdentifierExpression('username')],
            [
                'OR' => [
                    'ldap_login IS' => null,
                    'ldap_login' => '',
                ],
                'ldap_id IS NOT' => null,
            ]
        );

        $loc->clear();
    }

    /**
     * @return void
     */
    public function down(): void
    {
    }
}
