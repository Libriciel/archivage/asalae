<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Asalae\Model\Table\TechnicalArchivesTable;
use Migrations\AbstractMigration;

class Patch2111 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $loc = TableRegistry::getTableLocator();
        /** @var TechnicalArchivesTable $TechnicalArchives */
        $TechnicalArchives = $loc->get('TechnicalArchives');
        $TechnicalArchives->updateAll(
            ['state' => TechnicalArchivesTable::S_AVAILABLE],
            []
        );

        $table = $this->table('transfer_locks');
        $table->addColumn(
            'transfer_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'pid',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'uploading',
            'boolean',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->create();

        $table = $this->table('keyword_lists');
        $table->addColumn(
            'has_workspace',
            'boolean',
            [
                'default' => false,
                'null' => false,
            ]
        );
        $table->update();

        $adapter = $this->getAdapter();
        $keywordLists = $adapter->getSelectBuilder()
            ->select(['keyword_lists.id'])
            ->from('keyword_lists')
            ->innerJoin(
                ['k' => 'keywords'],
                ['keyword_list_id' => new IdentifierExpression('keyword_lists.id')]
            )
            ->where(['k.version' => 0])
            ->distinct(['keyword_lists.id']);
        $adapter->getUpdateBuilder()
            ->update('keyword_lists')
            ->set(['has_workspace' => true])
            ->where(['id IN' => $keywordLists])
            ->execute();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $this->table('transfer_locks')->drop()->save();

        $table = $this->table('keyword_lists');
        $table->removeColumn('has_workspace');
        $table->update();
    }
}
