<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Migrations\AbstractMigration;

class Patch226 extends AbstractMigration
{
    /**
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('archive_units');
        $table->addColumn(
            'max_keywords_access_end_date',
            'date',
            [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'description_access_rule_id',
            'integer',
            [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ]
        );
        $table->addForeignKey(
            'description_access_rule_id',
            'access_rules',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'SET_NULL',
            ]
        );
        $table->addColumn(
            'max_aggregated_access_end_date',
            'date',
            [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]
        );
        $table->addIndex(['max_aggregated_access_end_date']);
        $table->update();
    }
}
