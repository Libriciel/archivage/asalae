<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Cake\Database\Expression\IdentifierExpression;
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class Patch201 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $table = $this->table('technical_archives');
        $table->addColumn(
            'integrity_date',
            'timestamp',
            [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'filesexist_date',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('users');
        $table->addColumn(
            'ldap_login',
            'string',
            [
                'default' => null,
                'limit' => 512,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('ldaps');
        $table->changeColumn(
            'user_login_attribute',
            'string',
            [
                'default' => null,
                'limit' => 512, // match users.username limit
                'null' => false,
            ]
        );
        $table->addColumn(
            'user_username_attribute',
            'string',
            [
                'default' => null,
                'limit' => 512,
                'null' => true,
            ]
        );
        $table->update();

        $loc = TableRegistry::getTableLocator();
        $Users = $loc->get('Users');
        $Users->updateQuery()
            ->set(['ldap_login' => new IdentifierExpression('username')])
            ->where(['ldap_id IS NOT' => null])
            ->execute();

        $Ldaps = $loc->get('Ldaps');
        $Ldaps->updateQuery()
            ->set(['user_username_attribute' => new IdentifierExpression('user_login_attribute')])
            ->execute();

        $table = $this->table('deliveries');
        $table->changeColumn(
            'original_size',
            'biginteger',
            [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('delivery_requests');
        $table->changeColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 512,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('archive_binaries');
        $table->changeColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 512,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('archive_files');
        $table->changeColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 512,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('transfer_attachments');
        $table->changeColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 2048,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('transfers');
        $table->changeColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 512,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('stored_files_volumes');
        $table->changeColumn(
            'storage_ref',
            'string',
            [
                'default' => null,
                'limit' => 2048,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('stored_files');
        $table->changeColumn(
            'name',
            'string',
            [
                'default' => null,
                'limit' => 2048,
                'null' => false,
            ]
        );
        $table->update();

        $loc->clear();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $table = $this->table('technical_archives');
        $table->removeColumn('integrity_date');
        $table->removeColumn('filesexist_date');
        $table->update();

        $table = $this->table('users');
        $table->removeColumn('ldap_login');
        $table->update();

        $table = $this->table('ldaps');
        $table->removeColumn('user_username_attribute');
        $table->update();

        $table = $this->table('deliveries');
        $table->changeColumn(
            'original_size',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('delivery_requests');
        $table->changeColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('archive_binaries');
        $table->changeColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('archive_files');
        $table->changeColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('transfer_attachments');
        $table->changeColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 1024,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('transfers');
        $table->changeColumn(
            'filename',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('stored_files_volumes');
        $table->changeColumn(
            'storage_ref',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->update();

        $table = $this->table('stored_files');
        $table->changeColumn(
            'name',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->update();
    }
}
