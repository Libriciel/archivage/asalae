<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Migrations\AbstractMigration;

class Patch224 extends AbstractMigration
{
    /**
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('transfers');
        $table->addColumn(
            'composite_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addForeignKey(
            'composite_id',
            'archives',
            'id',
            [
                'update' => 'CASCADE',
            ]
        );
        $table->update();
    }
}
