<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class Patch205 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $table = $this->table('event_logs');
        $table->addColumn(
            'in_lifecycle',
            'boolean',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $setExported = [
            'ArchiveBinaries',
            'ArchiveFiles',
            'ArchiveKeywords',
            'ArchiveUnits',
            'Archives',
            'Deliveries',
            'Transfers',
        ];

        $loc = TableRegistry::getTableLocator();
        $EventLogs = $loc->get('EventLogs');
        $EventLogs->updateAll(['in_lifecycle' => true], ['object_model IN' => $setExported]);

        $loc->clear();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $table = $this->table('event_logs');
        $table->removeColumn('in_lifecycle');
        $table->update();
    }
}
