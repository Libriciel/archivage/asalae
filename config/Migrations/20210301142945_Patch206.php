<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Migrations\AbstractMigration;

class Patch206 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $table = $this->table('volumes');
        $table->removeColumn('date_begin');
        $table->removeColumn('date_end');
        $table->removeColumn('alert_date');
        $table->update();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $table = $this->table('volumes');
        $table->addColumn(
            'date_begin',
            'date',
            [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'date_end',
            'date',
            [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'alert_date',
            'integer',
            [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ]
        );
        $table->update();
    }
}
