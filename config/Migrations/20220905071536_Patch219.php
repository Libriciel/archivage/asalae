<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Migrations\AbstractMigration;

class Patch219 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $this->table('volumes')
            ->addColumn(
                'location',
                'text',
                [
                    'default' => null,
                    'null' => true,
                ]
            )
            ->update();

        $this->table('service_levels')
            ->addColumn(
                'commitment',
                'text',
                [
                    'default' => null,
                    'null' => true,
                ]
            )
            ->update();

        $table = $this->table('archiving_systems');
        $table->addIndex(['org_entity_id', 'name'], ['unique' => true]);
        $table->update();

        $table = $this->table('beanstalk_jobs');
        $table->addColumn(
            'beanstalk_worker_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'object_model',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'object_foreign_key',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'job_state',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'last_state_update',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'states_history',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addForeignKey(
            'beanstalk_worker_id',
            'beanstalk_workers',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'SET NULL',
            ]
        );
        $table->update();

        $table = $this->table('destruction_requests');
        $table->addColumn(
            'certified',
            'boolean',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();
        $table = $this->table('restitution_requests');
        $table->addColumn(
            'restitution_certified',
            'boolean',
            [
                'default' => false,
                'null' => false,
            ]
        );
        $table->addColumn(
            'destruction_certified',
            'boolean',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();
        $table = $this->table('archives_restitution_requests');
        $table->addColumn(
            'archive_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'restitution_request_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'archive_id',
            'archives',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'restitution_request_id',
            'restitution_requests',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE',
            ]
        );
        $table->create();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $this->table('volumes')
            ->removeColumn('location')
            ->update();

        $this->table('service_levels')
            ->removeColumn('commitment')
            ->update();

        $table = $this->table('archiving_systems');
        $table->removeIndex(['org_entity_id', 'name']);
        $table->update();

        $table = $this->table('beanstalk_jobs');
        $table->removeColumn('beanstalk_worker_id');
        $table->removeColumn('object_model');
        $table->removeColumn('object_foreign_key');
        $table->removeColumn('job_state');
        $table->removeColumn('last_state_update');
        $table->removeColumn('states_history');
        $table->save();

        $table = $this->table('destruction_requests');
        $table->removeColumn('certified');
        $table->update();

        $table = $this->table('archives_restitution_requests');
        $table->drop();
        $table->update();

        $table = $this->table('restitution_requests');
        $table->removeColumn('restitution_certified');
        $table->removeColumn('destruction_certified');
        $table->update();
    }
}
