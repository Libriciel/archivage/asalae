<?php

/**
 * @phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
 */

declare(strict_types=1);

use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class Patch214 extends AbstractMigration
{
    /**
     * @return void
     */
    public function up(): void
    {
        $table = $this->table('org_entities');
        $table->addColumn(
            'short_name',
            'string',
            [
                'default' => null,
                'limit' => 40,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('roles');
        $table->removeIndex(['name']);
        $table->addIndex(['org_entity_id', 'name'], ['unique' => true]);
        $table->update();

        $loc = TableRegistry::getTableLocator();
        $OrgEntities = $loc->get('OrgEntities');

        $se = $OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SE'])
            ->first();

        if (!$se) {
            return;
        }

        $saCodeId = $loc->get('TypeEntities')
            ->find()
            ->select(['id'])
            ->where(['code' => 'SA'])
            ->firstOrFail()
            ->get('id');

        $OrgEntities->updateAll(
            ['parent_id' => $se->get('id')],
            [
                'parent_id IS' => null,
                'type_entity_id' => $saCodeId,
            ]
        );

        $loc->clear();
    }

    /**
     * @return void
     */
    public function down(): void
    {
        $table = $this->table('org_entities');
        $table->removeColumn('short_name');
        $table->update();

        $table = $this->table('roles');
        $table->removeIndex(['name']);
        $table->addIndex(['name'], ['unique' => true]);
        $table->update();
    }
}
