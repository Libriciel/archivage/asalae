<?php

$versionFile = file(ROOT.DS.'VERSION.txt');
$version = trim(array_pop($versionFile));

/**
 * Version d'asalae
 */
define('ASALAE_VERSION_LONG', $version);

/**
* Version d'asalae
*/
define('ASALAE_VERSION_SHORT', $version);

/**
 * Version MongoDB
 */
if (!defined('MONGODB_VERSION')) {
    define('MONGODB_VERSION', "3.6.2");
}

/**
 * Namespace du seda
 */
define('NAMESPACE_SEDA_02', 'fr:gouv:ae:archive:draft:standard_echange_v0.2');
define('NAMESPACE_SEDA_10', 'fr:gouv:culture:archivesdefrance:seda:v1.0');
define('NAMESPACE_SEDA_20', 'fr:gouv:culture:archivesdefrance:seda:v2.0');
define('NAMESPACE_SEDA_21', 'fr:gouv:culture:archivesdefrance:seda:v2.1');
define('NAMESPACE_SEDA_22', 'fr:gouv:culture:archivesdefrance:seda:v2.2');
define('NAMESPACE_SEDA_23', 'fr:gouv:culture:archivesdefrance:seda:v2.3');

/**
 * Namespace du premis
 */
define('NAMESPACE_PREMIS_V3', 'http://www.loc.gov/premis/v3');

/**
 * change la limite de 65535 lignes d'un xml si $dom->loadXML($xml, XML_PARSE_BIG_LINES);
 */
if (!defined('XML_PARSE_BIG_LINES')) {
    define('XML_PARSE_BIG_LINES', 4194304);
}

/**
 * Force des mot de passe
 * @see https://www.ssi.gouv.fr/administration/precautions-elementaires/calculer-la-force-dun-mot-de-passe/
 */
/**
 * Mot de passe de 8 caractères dans un alphabet de 70 symboles
 * Taille usuelle
 */
define('PASSWORD_ULTRA_WEAK', 49);
/**
 * Mot de passe de 12 caractères dans un alphabet de 90 symboles
 * Taille minimale recommandée par l’ANSSI pour des mots de passe ergonomiques ou utilisés de façon locale.
 */
define('PASSWORD_WEAK', 78);
/**
 * Mot de passe de 16 caractères dans un alphabet de 36 symboles
 * Taille recommandée par l’ANSSI pour des mots de passe plus sûrs.
 */
define('PASSWORD_MEDIUM', 82);
/**
 * Mot de passe de 20 caractères dans un alphabet de 90 symboles
 * Force équivalente à la plus petite taille de clé de l’algorithme de chiffrement standard AES (128 bits).
 */
define('PASSWORD_STRONG', 130);
