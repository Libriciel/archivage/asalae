# Application Asalae v2
[![License](https://img.shields.io/badge/license-AGPL-blue)](https://www.gnu.org/licenses/agpl-3.0.txt)
[![build status](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae/pipelines/latest)
[![coverage report](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae/badges/master/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://asalae-master.dev.libriciel.net/coverage/asalae2/index.html)

Dépendances:

| Projet                                                                                     | Build                                                                                                                                                                                                               | Coverage                                                                                                                                                                                                                                          |
|--------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [asalae-assets](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae-assets) | [![build status](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae-assets/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae-assets/pipelines/latest) | [![coverage report](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae-assets/badges/master/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://asalae-master.dev.libriciel.net/coverage/asalae-assets/index.html) |
| [asalae-core](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae-core)     | [![build status](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae-core/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae-core/pipelines/latest)     | [![coverage report](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae-core/badges/php83/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://asalae-master.dev.libriciel.net/coverage/asalae-core/index.html)      |
| [cakephp-beanstalk](https://gitlab.libriciel.fr/CakePHP/cakephp-beanstalk)                 | [![build status](https://gitlab.libriciel.fr/CakePHP/cakephp-beanstalk/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/CakePHP/cakephp-beanstalk/pipelines/latest)                                         | [![coverage report](https://gitlab.libriciel.fr/CakePHP/cakephp-beanstalk/badges/master/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://asalae-master.dev.libriciel.net/coverage/cakephp-beanstalk/index.html)                 |
| [cakephp-datacompressor](https://gitlab.libriciel.fr/CakePHP/cakephp-datacompressor)       | [![build status](https://gitlab.libriciel.fr/CakePHP/cakephp-datacompressor/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/CakePHP/cakephp-datacompressor/pipelines/latest)                               | [![coverage report](https://gitlab.libriciel.fr/CakePHP/cakephp-datacompressor/badges/master/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://asalae-master.dev.libriciel.net/coverage/cakephp-datacompressor/index.html)       |
| [cakephp-filesystem](https://gitlab.libriciel.fr/CakePHP/cakephp-filesystem)               | [![build status](https://gitlab.libriciel.fr/CakePHP/cakephp-filesystem/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/CakePHP/cakephp-filesystem/pipelines/latest)                                       | [![coverage report](https://gitlab.libriciel.fr/CakePHP/cakephp-filesystem/badges/master/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://asalae-master.dev.libriciel.net/coverage/cakephp-filesystem/index.html)               |
| [cakephp-login-page](https://gitlab.libriciel.fr/CakePHP/cakephp-login-page)               | [![build status](https://gitlab.libriciel.fr/CakePHP/cakephp-login-page/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/CakePHP/cakephp-login-page/pipelines/latest)                                       | [![coverage report](https://gitlab.libriciel.fr/CakePHP/cakephp-login-page/badges/master/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://asalae-master.dev.libriciel.net/coverage/cakephp-login-page/index.html)               |
| [file-converters](https://gitlab.libriciel.fr/file-converters/file-converters)             | [![build status](https://gitlab.libriciel.fr/file-converters/file-converters/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/file-converters/file-converters/pipelines/latest)                             | [![coverage report](https://gitlab.libriciel.fr/file-converters/file-converters/badges/master/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://asalae-master.dev.libriciel.net/coverage/file-converters/index.html)             |
| [file-validator](https://gitlab.libriciel.fr/file-validator/file-validator)                | [![build status](https://gitlab.libriciel.fr/file-validator/file-validator/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/file-validator/file-validator/pipelines/latest)                                 | [![coverage report](https://gitlab.libriciel.fr/file-validator/file-validator/badges/master/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://asalae-master.dev.libriciel.net/coverage/file-validator/index.html)                |
| [seda2pdf](https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf)                  | [![build status](https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf/pipelines/latest)                         | [![coverage report](https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf/badges/master/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://asalae-master.dev.libriciel.net/coverage/seda2pdf/index.html)                  |

## Prérequis serveur
[Doc des prérequis](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/Documentations/installation-exploitation/AS-DOC-pre_requis)

### Liste des systèmes d'exploitation supportés

| OS                   | Statut       | Commentaires                             |
|----------------------|--------------|------------------------------------------|
| Ubuntu 20.04 LTS x64 | **Supporté** | Version Asalae 2 - OS de référence       |
| RHEL 8 x64 (Licence) | **Supporté** | >8.6 - dépôts additionnels nécessaires * |


### Dimensionnement et ressources

Le dimensionnement disque peut être effectué 'tout en une même partition' ou via disque /volume séparé suivant le contexte d'utilisation.

Nous conseillons le formatage en LVM afin de pouvoir augmenter à chaud l'espace disque.

| Indicateurs                             | Ressources test | Ressources production | Commentaires                                                                                             |
|-----------------------------------------|-----------------|-----------------------|----------------------------------------------------------------------------------------------------------|
| Espace disque système (racine)          | 	~50 Go         | ~100 Go               | L'espace disque contiendra : L'OS, les logs, les sources de l'application, la base de données PostgreSQL |
| Espace disque échanges (/data)          | 	XXX Go         | XXX Go                | Espace tampon pour la réception des transferts entrants **                                               |
| Espace disque données (/data-archives ) | 	XXX Go         | XXX Go                | Volume d'archive ***                                                                                     |
| CPU                                     | 	2              | 4                     | Indicateurs conseillés                                                                                   |
| RAM                                     | 	8 Go           | 16G                   | Indicateurs conseillés (en prod un espace de swap de 8G peut être alloué si 8G de RAM disponible)        |


> ** Taille estimée à 3X le volume de données à envoyer au SAE.

> En cas d'usage intensif du SAE notamment avec des flux de plusieurs centaines de Go,  les ressources RAM seront à augmenter.

> En cas d'utilisation du SAE en mode multi-services d'archives (mutualisé) il conviendra d'adopter les pré requis de production pour les instances de test et d'optimiser le dimensionnement.


### Briques techniques

Voici la liste des briques techniques.

Ces briques sont des pré requis, et seront déployés à l'installation, inutile de procéder à leur mise en place.

| Composant  | Version  | 	Commentaires                                   |
|------------|:--------:|:------------------------------------------------|
| PHP        |   	7.4   | Version uniquement supportée                    |
| CAKEPHP    |   4.x    | Version embarquée dans les sources de Asalae V2 |
| APACHE     |   2.4    | Modules rewrite, ssl                            |
| POSTGRESQL |    12    | Version supportée                               |
| JING       | 20091111 | Validation de formats de fichier RelaxNG        |
| SIEGFRIED  |   1.9    | détection du format des fichiers                |
| Beanstalkd |   1.10   | Gestion des jobs                                |
| CLAMAV     | 0.100.x  | Antivirus                                       |
| OpenJDK    |    11    | Utilisé par Jing                                |


## Install ubuntu 20.04
[Doc d'install ubuntu](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/Documentations/installation-exploitation/AS-DOC-manuel_installation_ubuntu)

## Install redhat 8
[Doc d'install redhat](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/Documentations/installation-exploitation/AS-DOC-manuel_installation_centos)
